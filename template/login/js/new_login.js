$("#btn-registrarme").click(function (e) {
  e.preventDefault();
  $("#login").css("display", "none");
  $("#frm-registrarme").css("display", "block");
  //$('#frm-registrarme').fadeIn();
  listarTrayectorias();
  $("#trayectoria").select2({
    placeholder: "Trayectoria",
    allowClear: true,
  });
});
function listarTrayectorias() {
  $.ajax({
    url: "controllers/usuarios.php",
    type: "POST",
    dataType: "json",
    data: { opcn: "listarTrayectorias" },
  }).done(function (res) {
    let tr = '<option value="">Trayectoria a iniciar</option>';
    res.forEach((el) => {
      tr += `<option value="${el.charge_id}">${el.charge}</option>`;
    });
    $("#trayectoria").html(tr);
  });
}

$("#btn-ingresar").click(function (e) {
  e.preventDefault();
  $("#frm-registrarme").css("display", "none");
  $("#login").fadeIn();
});

$("#registrarme").click(function (e) {
  e.preventDefault();
  var data = new FormData();
  var other_data = $("#frm-registrarme").serializeArray();
  
  $.each(other_data, function (key, input) {
    data.append(input.name, input.value);
  });

  if ($("#gender").val() == "") {
    alert("Por favor selecciona un genero");
    return false;
  }
  if ($("#trayectoria").val() == "") {
    alert("Por favor selecciona una trayectoria");
    return false;
  }

  if (!$("#treatment_policy").is(":checked")) {
    alert("Por favor aceptas nuestras políticas de tratamiento de datos");
    return;
    false;
  }

  if ($("#identification").val() == "") {
    alert("Por escribe tu nimero de identificación");
    return false;
  }
  if ($("#first_name").val() == "") {
    alert("Por favor escribe tus nombres");
  }
  if ($("#last_name").val() == "") {
    alert("Por favor escribe tus apellidos");
    return false;
  }
  if ($("#email").val() == "") {
    alert("Por favor escribe tu email");
  }
  if ($("#mobile_phone").val() == "") {
    alert("Por favor escribe tu teléfono");
    return false;
  }
  if ($("#date_birthday").val() == "") {
    alert("Por favor escribe tu fecha de nacimiento");
    return;
    false;
  }
  $.ajax({
    url: "controllers/usuarios.php",
    type: "POST",
    dataType: "JSON",
    data: data,
    cache: false,
    contentType: false,
    processData: false,
    data: data,
  })
    .done(function (data) {
      alert(data.msj);
      $("#frm-registrarme").css("display", "none");
      $("#login").fadeIn();
    })
    .fail(function () {
      console.log("error");
    });
});
