<?php
class Database
{

	//conexion produccion
	/* static private $hostname_conexion	= "localhost";
	static private $database_conexion	= "acdelco_lms";
	static private $username_conexion	= "acdelco_usrbd";
	static private $password_conexion	= "L5du2Col2023*"; */


	static private $hostname_conexion	= "localhost";
	static private $database_conexion	= "acdelco_lms";
	static private $username_conexion	= "root";
	static private $password_conexion	= "";


	public function SQL_SelectRows($SqlInstruction)
	{
		$conexion_database = mysqli_connect(Database::$hostname_conexion, Database::$username_conexion, Database::$password_conexion, Database::$database_conexion);
		$consulta_database = mysqli_query($conexion_database, $SqlInstruction);
		$rows_consulta = mysqli_fetch_assoc($consulta_database);
		mysqli_free_result($consulta_database);
		$thread_id = mysqli_thread_id($conexion_database);
		mysqli_kill($conexion_database, $thread_id);
		mysqli_close($conexion_database);
		return $rows_consulta;
	}

	public function SQL_SelectCantidad($SqlInstruction)
	{
		$conexion_database = mysqli_connect(Database::$hostname_conexion, Database::$username_conexion, Database::$password_conexion, Database::$database_conexion);
		$consulta_database = mysqli_query($conexion_database, $SqlInstruction);
		$totalRows_consulta = mysqli_num_rows($consulta_database);
		mysqli_free_result($consulta_database);
		$thread_id = mysqli_thread_id($conexion_database);
		mysqli_kill($conexion_database, $thread_id);
		mysqli_close($conexion_database);
		return $totalRows_consulta;
	}

	public function SQL_Insert($SqlInstruction)
	{
		$conexion_database = mysqli_connect(Database::$hostname_conexion, Database::$username_conexion, Database::$password_conexion, Database::$database_conexion);
		$resultado_insert = mysqli_query($conexion_database, $SqlInstruction);
		$resultado_insert = mysqli_insert_id($conexion_database);
		$thread_id = mysqli_thread_id($conexion_database);
		mysqli_kill($conexion_database, $thread_id);
		mysqli_close($conexion_database);
		return $resultado_insert;
	}

	public function SQL_Update($SqlInstruction)
	{
		$conexion_database = mysqli_connect(Database::$hostname_conexion, Database::$username_conexion, Database::$password_conexion, Database::$database_conexion);
		mysqli_query($conexion_database, $SqlInstruction);
		$resultado_update = mysqli_affected_rows($conexion_database);
		$thread_id = mysqli_thread_id($conexion_database);
		mysqli_kill($conexion_database, $thread_id);
		mysqli_close($conexion_database);
		return $resultado_update;
	}

	public function SQL_SelectMultipleRows($SqlInstruction)
	{
		$conexion_database = mysqli_connect(Database::$hostname_conexion, Database::$username_conexion, Database::$password_conexion, Database::$database_conexion);
		$consulta_database = mysqli_query($conexion_database, $SqlInstruction) or die(mysqli_error($conexion_database));
		for ($rows_consulta = array(); $row = mysqli_fetch_assoc($consulta_database); $rows_consulta[] = $row);
		mysqli_free_result($consulta_database);
		$thread_id = mysqli_thread_id($conexion_database);
		mysqli_kill($conexion_database, $thread_id);
		mysqli_close($conexion_database);
		return $rows_consulta;
	}
}
