<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_evaluacion_c.php');
}
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Evaluacion.xls");
header ("Content-Transfer-Encoding: binary");
if(isset($_GET['start_date_day'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th class="center"> <?php echo utf8_decode("CÓDIGO"); ?></th>
                <th class="center"> <?php echo utf8_decode("EVALUACIÓN"); ?></th>
                <th class="center">EVALUADO</th>
                <th class="center"> <?php echo utf8_decode("IDENTIFICACIÓN"); ?></th>
                <th class="center">CONCESIONARIO</th>
                <th class="center">SEDE</th>
                <th class="center">FECHA</th>
                <th class="center">TIEMPO</th>
                <th class="center">META</th>
                <th class="center">PUNTOS</th>
                <th class="center">PUNTAJE</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php 
                if($cantidad_datos > 0){ 
					foreach ($datosCursos_all as $iID => $data) { 
                        $Var_Superado = number_format(($data['score']/$data['available_score'])*100,0);
                        ?>
                        <tr class="selectable">
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['code']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['review']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['first_name'].' '.$data['last_name']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['identification']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['dealer']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['headquarter']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['date_creation']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['time_response']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo utf8_decode($data['available_score']); ?></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php if($Var_Superado>79){ ?>label-success<?php }else{ ?>label-danger<?php } ?>"><?php echo($data['score']); ?></span></td>
                            <td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php if($Var_Superado>79){ ?>label-success<?php }else{ ?>label-danger<?php } ?>"><?php echo(number_format(($data['score']/$data['available_score'])*100,0)); ?></span></td>
                        </tr>
                <?php } } ?>
        </tbody>
    </table>
<?php } ?>