<?php include('src/seguridad.php'); ?>
<?php 
include('controllers/multimedias_detail.php');
$location = 'tutor';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons picture"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/fotografias.php">Galería Fotográfica</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left"><?php echo($galeria_detailData['media']); ?></h2>
						<div class="btn-group pull-right">
							<?php if(isset($_GET['back'])){ ?>
								<a href="argumentarios.php?id=<?php echo($_GET['back']); ?>" class="glyphicons no-js unshare" ><i></i>Regresar</a><br><br>
							<?php }else if(isset($_GET['club'])){ ?>
								<?php if($_GET['club']=="marca_chevrolet"){ ?>
									<a href="marca_chevrolet.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><br><br>
								<?php }else{ ?>
									<a href="club_lideres.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><br><br>
								<?php } ?>
							<?php }else{ ?>
								<a href="fotografias.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><br><br>
							<?php } ?>
							<h5><a href="#ModalCrearNueva" data-toggle="modal" class="glyphicons no-js circle_plus" ><i></i>Agregar Fotografía</a><h5>
						</div>
						<div class="clearfix"></div>
						<p style="text-align: justify; "><?php echo($galeria_detailData['description']); ?></p>
					</div>
					<!-- // END heading -->
<!-- Blueimp Gallery -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title">Titulo de la fografía</h3>
    <a class="prev no-ajaxify">‹</a>
    <a class="next no-ajaxify">›</a>
    <a class="close no-ajaxify">×</a>
    <a class="play-pause no-ajaxify"></a>
    <ol class="indicator"></ol>
</div>
<!-- // Blueimp Gallery END -->
					<!-- contenido interno -->
<div class="widget-gallery" data-toggle="collapse-widget">
	<!-- Gallery Layout -->
	<div class="gallery gallery-2">
		<ul class="row">
			<?php foreach ($galeria_datos as $key => $Data_Gal) { ?>
				<li class="col-md-3">
					<a class="thumb no-ajaxify" data-description="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" data-title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" data-gallery="gallery-2" href="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>">
						<img src="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>" title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" alt="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" class="img-responsive" />
					</a>
					<div class="fixed-bottom" style="background: rgba(66,66,66,0.9) !important; z-index:100;">
						<div class="media margin-none innerAll">
							<div class="media-body text-white">
							<?php 
							$titulo_otr = "";
							if(isset($Data_Gal['likes_data'])){
								foreach ($Data_Gal['likes_data'] as $iID => $otras) { 
									$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
								}
							} 
							?>
								<strong><i class="fa fa-fw fa-user-md"></i> <?php echo($Data_Gal['first_name'].' '.$Data_Gal['last_name']); ?> | 
									<a href="Likes" title="<?php echo $titulo_otr; ?>" onclick="return false;">
										<i class="fa fa-fw fa-thumbs-o-up"></i> 
										<span id="CantLikes<?php echo($Data_Gal['media_detail_id']); ?>"><?php echo(number_format($Data_Gal['likes'],0)); ?></span>
									</a>
								</strong>
								<p class="text-small margin-none">
									<?php if($_SESSION['max_rol']>=6){ ?>
										<a href="#ModalEditar" data-toggle="modal" onclick="CargaDatosEditar_Img(<?php echo($Data_Gal['media_detail_id']); ?>,'<?php echo($Data_Gal['source']); ?>','<?php echo($Data_Gal['media_detail']); ?>','<?php echo($Data_Gal['description']); ?>');"><i class="fa fa-fw fa-pencil"></i> Editar </a>
									<?php } ?>
									<a href="#" data-toggle="modal" onclick="LikeGalery(<?php echo($Data_Gal['media_detail_id']); ?>); return false;"><i class="fa fa-fw fa-thumbs-o-up"></i> Me gusta</a>
								</p>
								<p class="text-small margin-none"><?php echo($Data_Gal['media_detail']); ?></p>
							</div>
						</div>
					</div>
				</li>
			<?php } ?>
		</ul>
	</div>
	<!-- // Gallery Layout END -->
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>



<!-- Modal -->
<form action="fotografias_detail.php?opcn=verGal&id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
	<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Subir Nueva Fotografía</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Titulo: </label>
						<input type="text" id="media_detail" name="media_detail" class="form-control col-md-8" placeholder="Título de la imagen" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Descripción: </label>
						<input type="text" id="description" name="description" class="form-control col-md-8" placeholder="Descripción de la imagen" />
					</div>
					<div class="col-md-1">
						<input type="hidden" id="id_gal" value="<?php echo($_GET['id']); ?>" name="id_gal">
						<input type="hidden" id="opcn" value="crear_img" name="opcn">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-10">
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="image_new" name="image_new" />
							</span>
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->

<!-- Modal -->
<form action="fotografias_detail.php?opcn=verGal&id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
	<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Editar Fotografía</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Titulo: </label>
						<input type="text" id="media_detail_ed" name="media_detail_ed" class="form-control col-md-8" placeholder="Título de la imagen" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Descripción: </label>
						<input type="text" id="description_ed" name="description_ed" class="form-control col-md-8" placeholder="Descripción de la imagen" />
					</div>
					<div class="col-md-1">
						<input type="hidden" id="media_detail_id" value="0" name="media_detail_id">
						<input type="hidden" id="opcn" value="editar_img" name="opcn">
						<input type="hidden" id="imgant" value="0" name="imgant">
						<input type="hidden" id="id_gal" value="<?php echo($_GET['id']); ?>" name="id_gal">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="image_new_ed" name="image_new_ed" />
							</span>
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
					</div>
					<div class="col-md-1">
					</div>
					<div class="col-md-4">
						<label class="control-label">Estado: </label>
						<select style="width: 100%;" id="estado" name="estado">
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->



		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/media_detail.js"></script>
</body>
</html>