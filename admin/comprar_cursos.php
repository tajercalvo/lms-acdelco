<?php include_once('src/seguridad.php'); ?>
<?php
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>

<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	
</head>

<style type="text/css" media="screen">
	.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
  background-color: #ddd;

}

.titulo{
	font-size: 2.0em;
	text-transform: uppercase;
	text-align: center;
	color: #ab1212;

}

.centrado{
	text-align: center;
	padding-bottom: 2%;

}
.img_virtuales{
 display: none;
}

.img_precenciales{
	display: none;
}
</style>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: Comprar_Cursos</li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Administrador del menú LMS</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget-body">

						<!-- IMG Pricipales-->	
							<div class="row img_pricipales">
								<div class="col-md-1"></div>
								<div class="col-md-5 centrado ">
									<h2 class="titulo">cursos virtuales</h2>
									<img class="img1_princ" src="../assets/img_comprar_cursos/Biblio-27.jpg" alt="" style="width: 532px;cursor: pointer;">
								</div>
								<div class="col-md-5 centrado ">
									<h2 class="titulo">cursos presenciales</h2>
									<img class="img2_princ" src="../assets/img_comprar_cursos/Biblio-26.jpg" alt="" style="width: 532px;cursor: pointer;">
								</div>
								<div class="col-md-1"></div>
							</div>
						<!-- IMG Pricipales FIN-->


						  <!--ocultos Cursos Virtuales-->
							<div class="row img_virtuales">
								<div class="row">
									<div class="col-md-1">
										<button class="atras btn btn-danger" type="button" >Atrás</button>
									</div>
									<div class="col-md-10" style="text-align: center;font-size: 3em;color: #ab1212;;text-transform: uppercase;padding-bottom: 3%;">CURSOS virtuales</div>
									<div class="col-md-1"></div>
								</div>

								<div class="col-md-1"></div>
								<div class="col-md-5 centrado">
									<h2 class="titulo">Curso Virtual – Bioseguridad en los Talleres Automotrices.</h2>
									<a href="https://autotrain.com.co/index.php/categoria-producto/virtuales/" target="_blank"><img src="../assets/img_comprar_cursos/Bioseguridad.jpg" alt="" style="width: 400px" class="efecto"></a>
									
								</div>
								<div class="col-md-5 centrado">
									<h2 class="titulo">Curso Virtual – Bioseguridad en los Talleres Automotrices.</h2>
									<a href="https://autotrain.com.co/index.php/categoria-producto/virtuales/" target="_blank"><img src="../assets/img_comprar_cursos/bioseguridad_agotado.jpg" alt="" style="width: 400px" class="efecto"></a>
								</div>
								<div class="col-md-1"></div>
							</div>
								


							      <!-- FIN-->
							<!-- Cursos Presenciales-->
							<div class="row img_precenciales">
								<div class="col-md-1"></div>
								<div class="col-md-5 centrado">
									<h2 class="titulo">cursos virtuales</h2>
									<a href="" ></a><img src="../assets/img_comprar_cursos/Biblio-27.jpg" alt="" style="width: 532px">
								</div>
								<div class="col-md-5 centrado">
									<h2 class="titulo">cursos presenciales</h2>
									<img src="../assets/img_comprar_cursos/Biblio-26.jpg" alt="" style="width: 532px">
								</div>
								<div class="col-md-1"></div>
							</div>
					 <!-- FIN img_pricipales-->

						</div><!-- FIN-->
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/comprar_cursos.js?v=<?php echo md5(time()); ?>"></script>
</body>
</html>