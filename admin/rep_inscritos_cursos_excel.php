<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['course_id'])){
    include('controllers/rep_inscritos_cursos.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_InscritosCursos.xls");
if(isset($_GET['course_id'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet"> <?php echo utf8_decode("CÓDIGO"); ?></th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">ALUMNO</th>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">INICIO</th>
                <th data-hide="phone,tablet">FINAL</th>
                <th data-hide="phone,tablet">LUGAR</th>
                <th data-hide="phone,tablet">PROFESOR</th>
                <th data-hide="phone,tablet">FECHA INVITACIÓN</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php if($cantidad_datos > 0){ 
                        foreach ($datosCursos_all as $iID => $data) { ?>
                <tr>
                    <td><?php echo utf8_decode($data['newcode']); ?></td>
                    <td><?php echo utf8_decode($data['course']); ?></td>
                    <td><?php echo utf8_decode($data['first_name'].' '.$data['last_name']); ?></td>
                    <td><?php echo utf8_decode($data['headquarter']); ?></td>
                    <td><?php echo utf8_decode($data['start_date']); ?></td>
                    <td><?php echo utf8_decode($data['end_date']); ?></td>
                    <td><?php echo utf8_decode($data['living']); ?></td>
                    <td><?php echo utf8_decode($data['first_prof'].' '.$data['last_prof']); ?></td>
                    <td><?php echo utf8_decode($data['date_send']); ?></td>
                </tr>
                <?php } } ?>
        </tbody>
    </table>
<?php } ?>