<?php include('src/seguridad.php'); ?>
<?php include('controllers/banners.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<style media="screen">



/*TABLE { table-layout: fixed }

td:nth-child(0n+4){
text-align: center;

}
td:nth-last-child(0n+1){
text-align: center;

}
td:nth-child(3n+8){
background: red;
	overflow-wrap:
	 break-word;

}*/

</style>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons sheriffs_star"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/especialidades.php">Administrador de Banners</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Administrador de Banners </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-9">
				<h5 style="text-align: justify; ">A continuación encontrará las opciones necesarias para administrar los Banners; tenga en cuenta que una vez creado un elemento de lista, este no podrá ser eliminado. Únicamente podrá inactivarse para que no aparezca en el sistema, pero en este listado aparecerá como inactivo.</h5>
			</div>
			<div class="col-md-1">
			</div>
			<div class="col-md-2">
				<?php if($_SESSION['max_rol']>=5){ ?><h5><a href="#ModalBanner" data-toggle="modal" class="glyphicons no-js circle_plus" ><i></i>Agregar Banner</a><h5><?php } ?>
			</div>
		</div>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Administrar ítems de lista</h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<!-- Total elements-->
		<div class="form-inline separator bottom small">
			Total de registros: <?php echo($cantidad_datos); ?>
		</div>
		<!-- // Total elements END -->
		<!-- Table elements-->

		<table id="TableData" class="display table tableb table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
			<!-- Table heading -->
			<thead>
				<tr>
					<th data-class="expand">TIPO</th>
					<th data-hide="phone,tablet">NOMBRE</th>
					<th data-hide="phone,tablet">ARCHIVO</th>
					<th style="text-align: center;" data-hide="phone,tablet">ESTADO</th>
					<th data-hide="phone,tablet">USUARIO</th>
					<th data-hide="phone,tablet">INICIO</th>
					<th data-hide="phone,tablet">FINAL</th>
					<th  data-hide="phone,tablet">URL</th>
					<th data-hide="phone,tablet">CREACIÓN</th>
					<th data-hide="phone,tablet">CLICKS</th>
					<th style="text-align: center;"data-hide="phone,tablet"> - </th>
				</tr>
			</thead>
			<!-- // Table heading END -->
			<!-- -->
			<div style="display: inline-block;">
				<tbody>
					<?php /*foreach ($tablaBanners as $key => $tdbanner) { ?>
						<tr>
							<td><?php echo($tdbanner['banners_type']); ?></td>
							<td><?php echo($tdbanner['banner_name']); ?></td>
							<td><?php echo($tdbanner['file_name']); ?></td>
							<td><?php switch($tdbanner['status_id']){
								case 1: ?>
								<span class="label label-success">Activo</span>
								<?php
								break;
								case 2: ?>
								<span class="label label-danger">Inactivo</span>
								<?php
								break;
								default: ?>
								<span class="label label-important">Error</span>
							<?php } ?>
							</td>
							<td><?php echo($tdbanner['first_name']." ".$tdbanner['last_name']); ?></td>
							<td><?php echo($tdbanner['start_date']); ?></td>
							<td><?php echo($tdbanner['end_date']); ?></td>
							<td><?php echo($tdbanner['banner_url']); ?></td>
							<td><?php echo($tdbanner['create_date']); ?></td>
							<td><?php echo($tdbanner['clicks']); ?></td>
							<td><a href="op_banners.php?opcn=editar&id=<?php echo($tdbanner['banner_id']); ?>" class="btn-action glyphicons pencil btn-success"><i></i></a></td>
						</tr>
					<?php } */
					include_once('models/banners.php');
					$banner_Class = new Banners();?>
				</tbody>
			<!-- -->
		</table>
		<!-- // Table elements END -->


						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- -->
		<!-- Modal para agregar un nuevo banner-->
		<?php if($_SESSION['max_rol']>5){ ?>
			<form method="post" id="form_CrearBanner" enctype="multipart/form-data"><br><br>
				<div class="modal fade" id="ModalBanner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Agregar un nuevo Banner</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-5">
												<label class="control-label">Tipo de Banner: </label>
												<select style="width: 100%;" id="banner_type" name="banner_type">
													<?php foreach ($tipo_banner as $key => $tbanner) { ?>
														<option value="<?php echo($tbanner['banner_type_id']);?>"><?php echo($tbanner['banners_type']); ?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-md-5">
												<!-- Opción a ejecutar en el contralador-->
												<input type="hidden" id="opcn" name="opcn" value="crear">
												<!-- -->
												<label class="control-label" for="banner_name" >Nombre del Banner</label>
												<input type="text" id="banner_name" name="banner_name" class="form-control" placeholder="Título del Banner">
											</div>
											<div class="col-md-1"></div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="start_date" style="padding-top:8px;">Fecha Inicial:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control" type="text" id="start_date" name="start_date" placeholder="Inicia el" <?php if(isset($_POST['start_date'])) { ?>value="<?php echo $_POST['start_date']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="end_date" style="padding-top:8px;">Fecha Final:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control" type="text" id="end_date" name="end_date" placeholder="Finaliza el" <?php if(isset($_POST['end_date'])) { ?>value="<?php echo $_POST['end_date']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
										</div>
										<!-- Row END-->
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="end_date" style="padding-top:8px;">Género:</label>
													<div class="col-md-7 input-group date">
														<select style="width: 100%;" id="gender" name="gender" class="select2-offscreen" tabindex="-1">
															<option value="0">No Indicado</option>
															<option value="1">Masculino</option>
															<option value="2">Femenino</option>
														</select>
													</div>
												</div>
												<!-- // Group END -->
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<label class="control-label" for="banner_url" >Ruta del Banner</label>
												<input type="text" id="banner_url" name="banner_url" class="form-control" placeholder="url:">
											</div>
											<div class="col-md-1"></div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
											    <p style="">Seleccionar imagen con extension .jpg / .jpeg</p>
												<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												  	<span class="btn btn-default btn-file">
												  		<span class="fileupload-new">Seleccionar Imágen</span>
												  		<span class="fileupload-exists">Cambiar Imágen</span>
													  	<input type="file" class="margin-none" id="file_name" name="file_name" accept="image/jpeg" />
													</span>
												</div>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<label class="control-label" for="cargos" >Trayectorias</label>
												<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]">
													<optgroup label="Activos">
														<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
															$selected = "";
															if(isset($charge_user) && count($charge_user) > 0 ){
																foreach ($charge_user as $key => $Data_ChargeUser) {
																	if($Data_ActiveCharge['charge_id'] == $Data_ChargeUser['charge_id']){
																		$selected = "selected";
																	}
																}
															}
															?>
															<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php echo $selected; ?>><?php echo $Data_ActiveCharge['charge']; ?></option>
														<?php } ?>
														<?php if($_SESSION['max_rol']<=6){ ?>
															<?php if(isset($_GET['opcn'])&&($_GET['opcn']=="nuevo")){ ?>
																<option value="123" selected="selected">VOLUNTARIO</option>
																<option value="125" selected="selected">CURSOS LIBRES</option>
															<?php } ?>
														<?php } ?>
													</optgroup>
									        	</select>
											</div>
											<div class="col-md-1"></div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<!-- Concesionarios-->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<label class="control-label" for="concesionarios" >Empresas</label>
												<select multiple="multiple" style="width: 100%;" id="concesionarios" name="concesionarios[]">
													<optgroup label="Activos">
														<?php foreach ($listaConcesionarios as $key => $Data_Dealers) {
															$selected = "";
															if(count($dealersSeleccionados) > 0 ){
																foreach ($dealersSeleccionados as $key => $Data_DealerUser) {
																	if($Data_Dealers['dealer_id'] == $Data_DealerUser){
																		$selected = "selected";
																	}
																}
															}
															?>
															<option value="<?php echo $Data_Dealers['dealer_id']; ?>" <?php echo $selected; ?>><?php echo $Data_Dealers['dealer']; ?></option>
														<?php } ?>
													</optgroup>
												</select>
											</div>
											<div class="col-md-1"></div>
										</div>
										<!-- Fin Concesionarios-->
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-4">
												<label class="control-label">Estado: </label>
												<select style="width: 100%;" id="status_id" name="status_id">
													<option value="1">Activo</option>
													<option value="2">Inactivo</option>
												</select>
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- Row END-->
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" id="BtnEjecutar" class="btn btn-primary">Ejecutar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		<?php } ?>
		<!-- Fin Modal Nuevo Banner -->
		</div>
		</div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/banners.js"></script>
</body>
</html>
