<?php include('src/seguridad.php'); ?>
<?php include('controllers/comparativo.php');
$location = 'reporting';
$locData = true;
//$Asist = true;
$DashBoard = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons charts"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/comparativo.php">Comparativo trayectorias</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Informe de Comparativo trayectorias </h2>
							<br><br>
							<!--<button type="button" data-toggle="print" class="btn btn-default print hidden-print"><i class="fa fa-fw fa-print"></i> Imprimir</button>-->
						</div>
					<!-- // END heading -->
					<!-- contenido filtros -->
					<!-- -->
						<div class="widget widget-heading-simple widget-body-gray hidden-print">
							<div class="widget-body">
								<div class="row">
									<div class="row">
										<div class="col-md-10">

										</div>
										<div class="col-md-2">
											<?php if(isset($cantidad_datos) && $cantidad_datos > 0){ ?>
												<h5><a href="comparativo_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
											<?php } ?>
										</div>
									</div>
									<div class="separator bottom"></div>
									<div class="row">
										<form action="comparativo.php" method="post">
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-2">
												<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<!-- // END contenido filtros -->
						<p class="separator text-center"></p>
						<div class="well">
							<div class="table table-invoice">
								<div id="contenedorTablas">
									<?php
										if(isset($CargosDatosListos)){
										foreach($cargos as $keyC => $trayectoria) { ?>
										<div class="row">
											<div class="col-md-12">
												<h2 class="margin-none pull-left"><?php echo($trayectoria['charge']); ?></h2>
											</div>
											<div class="col-md-6 left"><!-- tabla por concesionario -->
												<table class="table table-condensed table-vertical-center table-thead-simple">
													<thead>
														<tr>
															<th class="center">Concesionario</th>
															<th class="center"># Cursos</th>
															<th class="center"># Usuarios</th>
															<th class="center">Promedio</th>
															<th class="center">% Avance</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">
														<?php foreach ($concesionario as $key2 => $kconce) {
															$numeroCursos = 0;
															$numeroUsuarios = 0;
															$promedioNotas = 0;
															$sumaNotas = 0;
															$contadorNota = 0;
															$porcentajeAvance = 0;
															$cantidadCursosPaso = 0;
															$cantidadCursosPersona = 0;
															$colorZona = "";
															foreach ($CargosDatosListos as $key => $Data_InfoCargosAna) {
																if($kconce == $Data_InfoCargosAna['dealer'] && $Data_InfoCargosAna['charge_id'] == $trayectoria['charge_id']){
																	if(isset($Data_InfoCargosAna['_CantidadCursos'])){
																		$numeroCursos = $Data_InfoCargosAna['_CantidadCursos'] > $numeroCursos ? $Data_InfoCargosAna['_CantidadCursos'] : $numeroCursos ;
																	}
																	if(isset($Data_InfoCargosAna['_PromedioXCargo'])){
																		$sumaNotas += $Data_InfoCargosAna['_PromedioXCargo'] ;
																		$contadorNota++;
																	}
																	if(isset($Data_InfoCargosAna['cantidadPersonas'])){
																		$numeroUsuarios += $Data_InfoCargosAna['cantidadPersonas'];
																	}
																	if(isset($Data_InfoCargosAna['_CantidadCursosPaso'])){
																		$cantidadCursosPaso+= $Data_InfoCargosAna['_CantidadCursosPaso'];
																	}
																	if(isset($Data_InfoCargosAna['_CantidadCursosPersonas'])){
																		$cantidadCursosPersona += $Data_InfoCargosAna['_CantidadCursosPersonas'] ;
																	}
																	$colorZona = $Data_InfoCargosAna['zone'];
																}//fin if

															}
															$porcentajeAvance = $cantidadCursosPersona > 0 ? number_format($cantidadCursosPaso/$cantidadCursosPersona*100,2) : 0;
															$promedioNotas = $contadorNota > 0 ? ($sumaNotas / $contadorNota) : 0;
															if( $numeroCursos > 0 ){ ?>
															<tr class="selectable">
															  <td class="left" style="padding: 2px; font-size: 80%;">
																<a href="#" style="color: <?php echo($colores[$colorZona]); ?>"><!--rep_general.php?charge_id=<?php echo(($Data_InfoCargosAna['charge_id'])); ?> -->
																  <?php echo(strtoupper($kconce)); ?>
																</a>
															  </td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($numeroCursos == 0){echo('color: #bbb');} ?>"><?php echo($numeroCursos); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($numeroUsuarios == 0){echo('color: #bbb');} ?>"><?php echo($numeroUsuarios); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($promedioNotas == 0){echo('color: #bbb');} ?>"><?php echo(number_format($promedioNotas,1)); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($porcentajeAvance == 0){echo('color: #bbb');} ?>"><?php echo($porcentajeAvance); ?> %</td>
															</tr>
															<?php
																}
															}//fin foreach
														?>
													</tbody>
												</table><br>
											</div>
											<div class="col-md-6 right"><!-- tabla por ciudades -->
												<table class="table table-condensed table-vertical-center table-thead-simple"><!-- tabla de resumen por ciudad/trayectoria-->
													<thead>
														<tr>
															<th class="center">Zona</th>
															<th class="center"># Cursos</th>
															<th class="center"># Usuarios</th>
															<th class="center">Promedio</th>
															<th class="center">% Avance</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">
														<?php foreach ($zona as $key3 => $kzona) {
															$numeroCursos = 0;
															$numeroUsuarios = 0;
															$promedioNotas = 0;
															$sumaNotas = 0;
															$contadorNota = 0;
															$porcentajeAvance = 0;
															$cantidadCursosPaso = 0;
															$cantidadCursosPersona = 0;
															$colorZona = "";
															foreach ($CargosDatosListos as $key => $Data_InfoCargosAna) {
																if($kzona == $Data_InfoCargosAna['zone'] && $Data_InfoCargosAna['charge_id'] == $trayectoria['charge_id']){
																	if(isset($Data_InfoCargosAna['_CantidadCursos'])){
																		$numeroCursos = $Data_InfoCargosAna['_CantidadCursos'] > $numeroCursos ? $Data_InfoCargosAna['_CantidadCursos'] : $numeroCursos ;
																	}
																	if(isset($Data_InfoCargosAna['_PromedioXCargo'])){
																		$sumaNotas += $Data_InfoCargosAna['_PromedioXCargo'] ;
																		$contadorNota++;
																	}
																	if(isset($Data_InfoCargosAna['cantidadPersonas'])){
																		$numeroUsuarios += $Data_InfoCargosAna['cantidadPersonas'];
																	}
																	if(isset($Data_InfoCargosAna['_CantidadCursosPaso'])){
																		$cantidadCursosPaso+= $Data_InfoCargosAna['_CantidadCursosPaso'];
																	}
																	if(isset($Data_InfoCargosAna['_CantidadCursosPersonas'])){
																		$cantidadCursosPersona += $Data_InfoCargosAna['_CantidadCursosPersonas'] ;
																	}
																	$colorZona = $Data_InfoCargosAna['zone'];
																}//fin if

															}
															$porcentajeAvance = $cantidadCursosPersona > 0 ? number_format($cantidadCursosPaso/$cantidadCursosPersona*100,2) : 0;
															$promedioNotas = $contadorNota > 0 ? ($sumaNotas / $contadorNota) : 0;
															if( $numeroCursos > 0 ){ ?>
															<tr class="selectable">
															  <td class="left" style="padding: 2px; font-size: 80%;">
																<a href="#" style="color: <?php echo($colores[$colorZona]); ?>"><!--rep_general.php?charge_id=<?php echo(($Data_InfoCargosAna['charge_id'])); ?> -->
																  <?php echo(strtoupper($kzona)); ?>
																</a>
															  </td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($numeroCursos == 0){echo('color: #bbb');} ?>"><?php echo($numeroCursos); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($numeroUsuarios == 0){echo('color: #bbb');} ?>"><?php echo($numeroUsuarios); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($promedioNotas == 0){echo('color: #bbb');} ?>"><?php echo(number_format($promedioNotas,1)); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($porcentajeAvance == 0){echo('color: #bbb');} ?>"><?php echo($porcentajeAvance); ?> %</td>
															</tr>
															<?php
																}
															}//fin foreach
														?>
													</tbody>
												</table>
												<div class="separator bottom"></div>
												<table class="table table-condensed table-vertical-center table-thead-simple"><!-- tabla de resumen por pais/trayectoria-->
													<thead>
														<tr>
															<th class="center">País</th>
															<th class="center"># Cursos</th>
															<th class="center"># Usuarios</th>
															<th class="center">Promedio</th>
															<th class="center">% Avance</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">
														<?php
															$numeroCursos = 0;
															$numeroUsuarios = 0;
															$promedioNotas = 0;
															$sumaNotas = 0;
															$contadorNota = 0;
															$porcentajeAvance = 0;
															$cantidadCursosPaso = 0;
															$cantidadCursosPersona = 0;
															$colorZona = "";
															foreach ($CargosDatosListos as $key => $Data_InfoCargosAna) {
																if($Data_InfoCargosAna['charge_id'] == $trayectoria['charge_id']){
																	if(isset($Data_InfoCargosAna['_CantidadCursos'])){
																		$numeroCursos = $Data_InfoCargosAna['_CantidadCursos'] > $numeroCursos ? $Data_InfoCargosAna['_CantidadCursos'] : $numeroCursos ;
																	}
																	if(isset($Data_InfoCargosAna['_PromedioXCargo'])){
																		$sumaNotas += $Data_InfoCargosAna['_PromedioXCargo'] ;
																		$contadorNota++;
																	}
																	if(isset($Data_InfoCargosAna['cantidadPersonas'])){
																		$numeroUsuarios += $Data_InfoCargosAna['cantidadPersonas'];
																	}
																	if(isset($Data_InfoCargosAna['_CantidadCursosPaso'])){
																		$cantidadCursosPaso+= $Data_InfoCargosAna['_CantidadCursosPaso'];
																	}
																	if(isset($Data_InfoCargosAna['_CantidadCursosPersonas'])){
																		$cantidadCursosPersona += $Data_InfoCargosAna['_CantidadCursosPersonas'] ;
																	}
																	$colorZona = $Data_InfoCargosAna['zone'];
																}//fin if

															}
															$porcentajeAvance = $cantidadCursosPersona > 0 ? number_format($cantidadCursosPaso/$cantidadCursosPersona*100,2) : 0;
															$promedioNotas = $contadorNota > 0 ? ($sumaNotas / $contadorNota) : 0;
															if( $numeroCursos > 0 ){ ?>
															<tr class="selectable">
															  <td class="left" style="padding: 2px; font-size: 80%;">
																<a href="#" style="color: <?php echo($colores[$colorZona]); ?>"><!--rep_general.php?charge_id=<?php echo(($Data_InfoCargosAna['charge_id'])); ?> -->
																  COLOMBIA
																</a>
															  </td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($numeroCursos == 0){echo('color: #bbb');} ?>"><?php echo($numeroCursos); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($numeroUsuarios == 0){echo('color: #bbb');} ?>"><?php echo($numeroUsuarios); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($promedioNotas == 0){echo('color: #bbb');} ?>"><?php echo(number_format($promedioNotas,1)); ?></td>
															  <td class="center" style="padding: 2px; font-size: 80%;<?php if($porcentajeAvance == 0){echo('color: #bbb');} ?>"><?php echo($porcentajeAvance); ?> %</td>
															</tr>
															<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<div class="separator bottom"></div>
										<div class="separator bottom"></div>
									<?php } //fin for principal ?>
									<!-- tablas resumen-->
									<!-- -->
									<!-- -->
									<!-- -->
									<!-- -->
									<!-- -->
									<!-- -->
									<!-- Fin tabla resumen-->
									<?php }//fin if isset?>
								</div><!-- fin contenedor tablas-->
							</div>
						</div>
						<div class="clearfix"><br></div>
						<!-- // END inner -->
				</div>
					<!-- // END Contenido proyectos -->
			</div>
			<div class="clearfix"></div>
			<!-- // Sidebar menu & content wrapper END -->

		</div>
		<?php include('src/footer.php'); ?>
		<!-- // Main Container Fluid END -->
		<?php include('src/global.php'); ?>
		<script src="js/comparativo.js"></script>
	</div>
</body>
</html>
