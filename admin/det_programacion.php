<?php 
//include('src/seguridad.php'); 
$living_id = $_POST['living_id'];
$course_id = $_POST['course_id'];
$module_id = $_POST['module_id'];
$max_size = $_POST['max_size'];
$min_size = $_POST['min_size'];
$city_id = $_POST['city_id'];
$Schedule_id_sel = $_POST['Schedule_id_sel'];

include('models/programaciones.php');
$programa_Class = new Cargos();
$datosProgramaciones = $programa_Class->consultaDisponibles_lugar('',$living_id,$course_id,$city_id,$Schedule_id_sel,$module_id);
$datosProgramacionesNo = $programa_Class->consultaDisponibles_nolugar('',$living_id,$course_id,$city_id,$Schedule_id_sel,$module_id);
if($Schedule_id_sel>0){
	$datosProgCreada = $programa_Class->consultaCreada('',$Schedule_id_sel);
}

unset($programa_Class);
?>
<!-- Table elements-->
<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
	<!-- Table heading -->
	<thead>
		<tr>
			<th data-hide="phone,tablet" class="center" colspan="6">CUPOS PARA ASIGNAR EN LA MISMA CIUDAD</th>
		</tr>
		<tr>
			<th data-hide="phone,tablet" style="width: 5%;">#</th>
			<th data-hide="phone,tablet" style="width: 10%;">DISPONIBLES</th>
			<th data-hide="phone,tablet" style="width: 40%;">EMPRESA</th>
			<th data-hide="phone,tablet" style="width: 25%;">CIUDAD</th>
			<th data-hide="phone,tablet" style="width: 10%;">CUPO MAX</th>
			<th data-hide="phone,tablet" style="width: 10%;">CUPO MIN</th>
		</tr>
	</thead>
	<!-- // Table heading END -->
	<?php
	$cantidad_max = 0;
	$cantidad_min = 0;
	$cantidad_usr = 0;
	foreach ($datosProgramaciones as $key => $Data_En) { 
		$cantidad_usr = $cantidad_usr + count($Data_En['personas']);
	}
	?>
	<tbody>
		<?php 
		$var_Seleccionados_en = "";
		$cant_ctrl = 0;
		$cant_max_ctrl = 0;
		$cant_min_ctrl = 0;
		foreach ($datosProgramaciones as $key => $Data_En) { 
			$cant_ctrl++;
			$porcentaje = number_format( ($Data_En['cant_usr'] / $cantidad_usr)*100 );
			$var_Seleccionados_en .= $Data_En['dealer_id'].",";
			?>
			<tr class="reg_busqueda">
				<td class="center"><?php echo $cant_ctrl; ?></td>
				<td class="center"><strong><?php echo $Data_En['cant_usr']; ?></strong> | <?php echo( $porcentaje ); ?> %
				<?php if(isset($Data_En['personas'])){
						$titulo_otr = "";
						foreach ($Data_En['personas'] as $iID => $personas) { 
							$titulo_otr .= "<i class='fa fa-group'></i> Sede: <strong class='text-danger'>".$personas['headquarter']."</strong> | Alumno: <strong class='text-danger'>".$personas['first_name'].' '.$personas['last_name']."</strong> | Identificación: <strong class='text-danger'>".$personas['identification']."</strong> <br>";
						}
					} ?>
					<?php if(isset($Data_En['personas'])){ ?><br><a href="Otr_Asignacion" onclick="return false;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-group"></i> Usuarios!!</a><br><?php } ?>
				</td>
				<td><?php echo $Data_En['dealer']; ?></td>
				<td><?php echo $Data_En['area']; ?></td>
				<td class="center"><?php 
				$var_usr_max = number_format( ($max_size * $porcentaje) / 100);
				if(isset($Data_En['maxsize_ya'])){
					?>
					<input class="form-control" id="S_max_size_<?php echo $Data_En['dealer_id']; ?>" name="S_max_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad máxima de participantes" value="<?php echo ( $Data_En['maxsize_ya'] ); ?>" />
					<?php $cant_max_ctrl = $cant_max_ctrl + $Data_En['maxsize_ya'];
				}else if($var_usr_max<$Data_En['cant_usr']){
						?>
				<input class="form-control" id="S_max_size_<?php echo $Data_En['dealer_id']; ?>" name="S_max_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad máxima de participantes" value="<?php echo ( $var_usr_max ); ?>" />
						<?php $cant_max_ctrl = $cant_max_ctrl + $var_usr_max;
					}else{ 
						?>
				<input class="form-control" id="S_max_size_<?php echo $Data_En['dealer_id']; ?>" name="S_max_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad máxima de participantes" value="<?php echo ( $Data_En['cant_usr'] ); ?>" />
						<?php $cant_max_ctrl = $cant_max_ctrl + $Data_En['cant_usr'];
					}
				?></td>
				<td class="center"><?php 
				$var_usr_min = number_format( ($min_size * $porcentaje) / 100);
				if(isset($Data_En['minsize_ya'])){
					?>
					<input class="form-control" id="S_min_size_<?php echo $Data_En['dealer_id']; ?>" name="S_min_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad mínima de participantes" value="<?php echo ( $Data_En['minsize_ya'] ); ?>" />
					<?php
					$cant_min_ctrl = $cant_min_ctrl + $Data_En['minsize_ya'];
				}else if($var_usr_min<$Data_En['cant_usr']){ ?>
				<input class="form-control" id="S_min_size_<?php echo $Data_En['dealer_id']; ?>" name="S_min_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad mínima de participantes" value="<?php echo ( $var_usr_min ); ?>" />
						<?php $cant_min_ctrl = $cant_min_ctrl + $var_usr_min;
					}else{?>
				<input class="form-control" id="S_min_size_<?php echo $Data_En['dealer_id']; ?>" name="S_min_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad mínima de participantes" value="<?php echo ( $Data_En['cant_usr'] ); ?>" />
						<?php $cant_min_ctrl = $cant_min_ctrl + $Data_En['cant_usr'];
					}
				?></td>
			</tr>
		<?php } ?>
		<tr>
			<td></td>
			<td class="center"><strong class='text-success center'><?php echo $cantidad_usr; ?></strong></td>
			<td class="center"><strong class='text-success center'>TOTAL ALUMNOS DISPONIBLES</strong></td>
			<td class="center"><strong class='text-success center'></strong></td>
			<td class="center"><strong class='text-success center'><?php echo $cant_max_ctrl." / ".$max_size; ?></strong></td>
			<td class="center"><strong class='text-success center'><?php echo $cant_min_ctrl." / ".$min_size; ?></strong></td>
		</tr>
	</tbody>
</table>
<br>
<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
	<!-- Table heading -->
	<thead>
		<tr>
			<th data-hide="phone,tablet" class="center" colspan="6">CUPOS PARA ASIGNAR EN UNA CIUDAD DIFERENTE</th>
		</tr>
		<tr>
			<th data-hide="phone,tablet" style="width: 5%;">#</th>
			<th data-hide="phone,tablet" style="width: 10%;">DISPONIBLES</th>
			<th data-hide="phone,tablet" style="width: 40%;">EMPRESA</th>
			<th data-hide="phone,tablet" style="width: 25%;">CIUDAD</th>
			<th data-hide="phone,tablet" style="width: 10%;">CUPO MAX</th>
			<th data-hide="phone,tablet" style="width: 10%;">CUPO MIN</th>
		</tr>
	</thead>
	<!-- // Table heading END -->
	<?php
	$cantidad_max = 0;
	$cantidad_min = 0;
	$cantidad_usr = 0;
	$var_Seleccionados_no = "";
	foreach ($datosProgramacionesNo as $key => $Data_En) { 
		$cantidad_usr = $cantidad_usr + count($Data_En['personas']);
	}
	?>
	<tbody>
		<?php 
		$cant_ctrl = 0;
		foreach ($datosProgramacionesNo as $key => $Data_En) { 
			$cant_ctrl++;
			$porcentaje = number_format( ($Data_En['cant_usr'] / $cantidad_usr)*100 );
			$var_Seleccionados_no .= $Data_En['dealer_id'].",";
			?>
			<tr class="reg_busqueda">
				<td class="center"><?php echo $cant_ctrl; ?></td>
				<td class="center"><strong><?php echo $Data_En['cant_usr']; ?></strong> | <?php echo( $porcentaje ); ?> %
				<?php if(isset($Data_En['personas'])){
						$titulo_otr = "";
						foreach ($Data_En['personas'] as $iID => $personas) { 
							$titulo_otr .= "<i class='fa fa-group'></i> Sede: <strong class='text-danger'>".$personas['headquarter']."</strong> | Alumno: <strong class='text-danger'>".$personas['first_name'].' '.$personas['last_name']."</strong> | Identificación: <strong class='text-danger'>".$personas['identification']."</strong> <br>";
						}
					} ?>
					<?php if(isset($Data_En['personas'])){ ?><br><a href="Otr_Asignacion" onclick="return false;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-group"></i> Usuarios!!</a><br><?php } ?>
				</td>
				<td><?php echo $Data_En['dealer']; ?></td>
				<td><?php 
					foreach ($Data_En['ciudades'] as $iID => $ciudades) { 
						echo "<i class='fa fa-map-marker'></i> ".$ciudades['area']." ";
					}
				//echo $Data_En['area']; ?></td>
				<td class="center">
					<?php if(isset($Data_En['maxsize_ya'])){ ?>
						<input class="form-control" id="N_max_size_<?php echo $Data_En['dealer_id']; ?>" name="N_max_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad máxima de participantes" value="<?php echo ( $Data_En['maxsize_ya'] ); ?>" />
					<?php }else{?>
						<input class="form-control" id="N_max_size_<?php echo $Data_En['dealer_id']; ?>" name="N_max_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad máxima de participantes" value="0" />
					<?php } ?>
				</td>
				<td class="center">
					<?php if(isset($Data_En['minsize_ya'])){ ?>
						<input class="form-control" id="N_min_size_<?php echo $Data_En['dealer_id']; ?>" name="N_min_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad mínima de participantes" value="<?php echo ( $Data_En['minsize_ya'] ); ?>" />
					<?php }else{?>
						<input class="form-control" id="N_min_size_<?php echo $Data_En['dealer_id']; ?>" name="N_min_size_<?php echo $Data_En['dealer_id']; ?>" type="text" placeholder="Cantidad mínima de participantes" value="0" />
					<?php } ?>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<td></td>
			<td class="center"><strong class='text-success'><?php echo $cantidad_usr; ?></strong></td>
			<td class="center"><strong class='text-success'>TOTAL ALUMNOS DISPONIBLES</strong></td>
			<td class="center"><strong class='text-success'></strong></td>
			<td class="center"><strong class='text-success'></strong></td>
			<td class="center"><strong class='text-success'></strong></td>
		</tr>
		<input type="hidden" name="SeleccionadosID_si" id="SeleccionadosID_si" value="<?php echo $var_Seleccionados_en; ?>" />
		<input type="hidden" name="SeleccionadosID_no" id="SeleccionadosID_no" value="<?php echo $var_Seleccionados_no; ?>" />
	</tbody>
</table>
<!-- // Table elements END -->