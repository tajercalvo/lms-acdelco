<?php include('src/seguridad.php'); ?>
<?php //include('controllers/tickets.php');
$location = '';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body" ng-app="ludusApp" ng-controller="bandejaTicketsCtrl as b">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/tickets.php">Tickets</a></li>
				</ul>
				<!-- Titulo Tickets -->
				<div class="innerAll"><!-- Titulo Tickets-->
					<div class="row">
						<div class="col-md-6">
							<h1 class="margin-none padding-none"><i class="fa fa-fw fa-ticket text-primary"></i> Ticket</h1>
						</div>
						<div class="col-md-6">
							<div class="alert alert-primary text-center" style="max-height: 30px" ng-if=" b.ticket.cargando ">
								<h5 style="color: white;"><i class="fa fa-spinner"></i> Cargando</h5>
							</div>
						</div>
					</div>
				</div>
				<!--
				Vista
				-->
				<section class="innerAll spacing-x2">
					<!-- Widget -->
					<div class="widget finances_summary widget-inverse">
						<div class="row row-merge">
							<div class="col-sm-2">
								<ul class="list-group list-group-1 margin-none borders-none">
									<li class=" {{ b.mPendientes }} list-group-item animated fadeInUp"><a href="" ng-click=" b.bandeja('Bandeja Entrada', 'mPendientes' ) "><span class="badge pull-right badge-default hidden-md">{{ b.ticket.num_tickets }}</span><i class="fa fa-exclamation-circle"></i> Bandeja Tickets</a></li>
									<li class=" {{ b.mBandeja }} list-group-item animated fadeInUp"><a href="" ng-click=" b.bandeja( 'Pendientes', 'mBandeja'  ) "><span class="badge pull-right badge-default hidden-md">{{ b.ticket.pendientes }}</span><i class="fa fa-spinner"></i> Pendientes</a></li>
									<li class=" {{ b.mCerrados }} list-group-item border-bottom animated fadeInUp"><a href="" ng-click=" b.bandeja( 'Cerrados', 'mCerrados'  ) "><i class="fa fa-pencil"></i> Cerrados</a></li>
									<li class=" {{ b.mReabiertos }} list-group-item border-bottom animated fadeInUp"><a href="" ng-click=" b.bandeja( 'Reabiertos', 'mReabiertos'  ) "><span class="badge pull-right badge-default hidden-md">{{ b.ticket.reabiertos }}</span><i class="fa fa-pencil"></i> Reabiertos</a></li>
								</ul>
								<div class="col-separator-h"></div>
								<form role="form">
									<div class="innerAll">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user text-faded"></i></span>
												<input type="text" class="form-control" id="exampleInputEmail1" placeholder="nombre o #ticket" ng-model=" b.objFiltro.nombre ">
											</div>
										</div>
										<div class="form-group">
										  	<div class="input-group date" id="fechaFiltro">
	                                            <input class="form-control" type="text" placeholder="yyyy-mm-dd" ng-model=" b.objFiltro.fecha ">
	                                            <span class="input-group-addon"><i class="fa fa-th"></i></span>
	                                        </div>
										</div>
										<div class="text-center border-top innerTB">
									  		<a href="" class="btn btn-primary" ng-click=" b.filtrar() "><i class="fa fa-spinner"></i> Filtrar</a>
										</div>
									</div>
								</form>
								<div class="col-separator-h"></div>
								<div class="innerAll">
									<div class="form-group margin-none">
										<div class="form-group">
											<label class="checkbox-inline checkbox-custom">
												<i class="fa fa-fw fa-square-o"></i>
												<input type="checkbox" ng-model=" b.nuevosCheck "> Nuevos
											</label>
											<label class="checkbox-inline checkbox-custom">
												<i class="fa fa-fw fa-square-o checked"></i>
												<input type="checkbox" ng-model=" b.cerradosCheck "> Cerrados
											</label>
											<br>
											<label for="descargarTickets" class="innerAll half bg-gray border-bottom margin-bottom-none" style="width: 100%; text-align: center">
												<a id="descargarTickets" href="controllers/tickets.php?opcn=descarga&nuevos={{ b.nuevosCheck ? 'si' : 'no' }}&cerrados={{ b.cerradosCheck ? 'si' : 'no' }}">Descargar tickets</a>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-sm-5">
										<h4 class="innerAll border-bottom margin-bottom-none"> {{ b.seccion }}
											<div class="pull-right display-block">
												<div class="action">
													<label id="paginacion"> {{ ( pagina - 1 ) * 15 }} - {{ pagina * 15 }} de {{ b.ticket.totalBandeja }} </label>
													<span class="btn btn-primary btn-sm" ng-click=" b.anterior() "><i class="fa fa-angle-left"></i></span>
													<span class="btn btn-primary btn-sm" ng-click=" b.siguiente() "><i class="fa fa-angle-right"></i></span>
													<!-- Pendiente -- >
													<!-- <a href="" class="btn btn-default btn-xs ">Select All</a>
													<a href="" class="btn btn-default btn-xs ">Mark as Read</a> -->
												</div>
											</div>
										</h4>
										<div style="width: 100%; height: 620px; overflow-x: scroll">
											<div class="innerAll border-bottom tickets" ng-repeat=" ticket in b.ticket.tickets " ng-click=" b.cambiarSeleccion( $index ) ">
												<div class="row">
													<div class="col-sm-12">
														<ul class="media-list">
															<li class="media">
															  	<div class="pull-left">
																	<div class="center">
																		<div class="checkbox">
																		    <label class="checkbox-custom">
																		    	<i class="fa fa-fw fa-square-o"></i>
																		      	<input type="checkbox">
																		    </label>
																		 </div>
																	 </div>
																</div>
															    <div class="media-body">
															    	<a href="ver_perfil.php?id={{ ticket.user_id }}" target="_blank" class="media-heading">{{ ticket.first_name+' '+ticket.last_name }}</a><label class="label label-default">#{{ ticket.ticket_id }}</label>
															  		<span ng-if=" ticket.status_id == 3 ">
															  			<label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i></a></label>
															  		</span>
															  		<span ng-if=" ticket.status_id == 1 ">
															  			<label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i></a></label>
															  		</span>
																	<span ng-if=" ticket.status_id == 0 ">
																		<label class="label label-success"> <a href="" class="user-ticket-action"><i class=" icon-checkmark-thick fw"></i></a></label>
																	</span>
																	<div class="clearfix"></div>
															    		{{ ticket.description | mensajecorto }}
															    	<div class="clearfix"></div>
															    	<strong><i class="icon-time-clock fw"></i> {{ ticket.date_creation }}</strong>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-7">
										<div class="innerAll border-bottom tickets">
											<div class="row">
												<div class="col-sm-12">
													<div style="height: 620px; width: 100%; overflow-x: scroll;">
														<ul class="media-list">
															<li class="media">
																<a class="pull-left" href="ver_perfil.php?id={{ b.ticket_sel.user_id }}" target="_blank">
															    	<div ng-if=" b.ticket_sel.perfil != '' && b.ticket_sel.perfil.indexOf('default') < 0 ">
															    		<img class="media-object" src="../assets/images/usuarios/{{ b.ticket_sel.perfil }}" alt="..." width="50px" height="50px">
															    	</div>
																	<div ng-if=" b.ticket_sel.perfil == '' || b.ticket_sel.perfil.indexOf('default') >= 0  ">
																		<span class="empty-photo">
																      		<i class="fa fa-user"></i>
																      	</span>
																	</div>
															    </a>
																<span ng-if=" b.ticket_sel.status_id == 1 || b.ticket_sel.status_id == 3 ">
																	<div class="pull-right">
																		<a href="" class="btn btn-success btn-xs" ng-click=" b.abrirModal( b.ticket_sel.ticket_id ) "><i class="fa fa-check"></i> Marcar resuelto</a>
																	</div>
																</span>
															    <div class="media-body">
															    	<a href="ver_perfil.php?id={{ b.ticket_sel.user_id }}" target="_blank" class="media-heading">{{ b.ticket_sel.first_name+' '+b.ticket_sel.last_name }}</a><label class="label label-default">#{{ b.ticket_sel.ticket_id }}</label>
															  		<span ng-if=" b.ticket_sel.status_id == 3 ">
															  			<strong>estado:</strong> <label class="label label-primary"><a href="" class="user-ticket-action "><i class="icon-reply-fill fw"></i> Reabierto</a> </label>
															  		</span>
															  		<span ng-if=" b.ticket_sel.status_id == 1 ">
															  			<strong>estado:</strong> <label class="label label-info"><a href="" class="user-ticket-action "><i class=" icon-refresh-6 fw"></i> Pendiente</a> </label>
															  		</span>
																	<span ng-if=" b.ticket_sel.status_id == 0 ">
																		<strong>estado:</strong> <label class="label label-success"> <a href="" class="user-ticket-action"><i class=" icon-checkmark-thick fw"></i> Cerrado</a></label>
																	</span>
																	<span class="label label-default">{{ b.ticket_sel.key_description }}</span>
																	<div class="clearfix"></div>
															    	<strong><i class="icon-time-clock fw"></i> Creada : {{ b.ticket_sel.date_creation }}</strong>
																	<div class="clearfix"></div>
																	{{ b.ticket_sel.description }}
															    	<div class="clearfix"></div>
																	<br>
																	<span ng-if=" b.ticket_sel.image != '' ">
																		<i class=" icon-life-raft innerL"></i><strong>Captura de pantalla:</strong>
																		<br>
																		<a href="" style="width:90%"><img class="media-object" src="../assets/FAQ/{{ b.ticket_sel.image }}" alt="{{ b.ticket_sel.image }}" ng-click=" b.abrirModalImagen( b.ticket_sel.image ) "></a>
																	</span>
																	<!-- inserta la respuesta si hay alguna -->
																	<br>
																	<div ng-if=" b.ticket_sel.respuestas ">
																		<div class="media" ng-repeat="respuesta in b.ticket_sel.respuestas">
																            <a class="pull-left" href="#">
																	             <span class="empty-photo agent">
																	      			<i class="icon-life-raft"></i>
																	      		</span>
																            </a>
																            <div class="media-body">
																				<a href="" class="media-heading "> {{ respuesta.first_name+' '+respuesta.last_name }}</a> <strong class="pull-right">{{ respuesta.date_creation }} <i class="icon-time-clock "></i></strong>
																				<div class="clearfix"></div>
																				<div class="innerT half">
																					<span class="prettyprint">{{ respuesta.description }}</span>
																					<!-- <pre class="prettyprint"></pre> -->
																				</div>
																				<span ng-if=" respuesta.image_answer != '' ">
																					<i class=" icon-life-raft innerL"></i><strong>Captura de pantalla:</strong>
																					<br>
																					<a href="" style="width:90%"><span class="text-primary" ng-click=" b.abrirModalImagen( respuesta.image_answer ) ">{{ respuesta.image_answer }}</span></a>
																				</span>
																            </div>
																        </div>
																	</div>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- -->
		<form method="post" enctype="multipart/form-data" id="formCerrarTicket" ng-submit=" b.cerrarTicket() ">
			<div class="modal fade" id="modalCerrarTicket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cerrarBtnCTicket">&times;</button>
							<h4 class="modal-title">Cerrar Ticket</h4>
						</div>
						<div class="modal-body">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-10">
											<h4 class="text-uppercase strong"><i class="fa fa-ticket text-regular fa-fw"></i> Ticket <strong class="text-primary">{{ b.respuesta.ticket }}</strong></h4>
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<!-- Row -->
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-10">
											<h5 class="text-uppercase strong"><i class="fa fa-ticket text-primary fa-fw"></i> Texto de Respuesta </h5>
											<textarea placeholder="Respuesta: " class="form-control col-md-12" id="faq_text_answer" rows="7" ng-model=" b.respuesta.textoRespuesta "></textarea>
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-10">
											<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> Captura de pantalla </h5>
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												<span class="btn btn-default btn-file">
													<span class="fileupload-new">Sube captura de pantalla: </span>
													<span class="fileupload-exists">Cambiar</span>
													<input type="file" class="margin-none" uploader-model="b.respuesta.imagen" accept="image/*" >
												</span>
												<span class="fileupload-preview"></span>
												<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none" id="faq_img_answer_close">&times;</a>
											</div>
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="reset" style="display: none" id="limpiar">reset</button>
							<button type="submit" name="sub_answer" class="btn btn-primary">Resuelto</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="modal fade" id="ModalImagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<!-- <div class="modal-dialog" style="min-width: 600px;max-width: 1000px;"> -->
				<div class="modal-content" style="top:50px;width: 90%; margin: auto;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cerrarBtnC">&times;</button>
						<h4 class="modal-title">Imagen</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<img style="width: 100%;" src="../assets/FAQ/{{ b.imagen }}" alt="{{ b.imagen }}">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="cerrar_modal" class="btn btn-primary">Cerrar</button>
					</div>
				</div>
			<!-- </div> -->
		</div>
		<!-- -->
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/bandeja_tickets.js"></script>
</body>
</html>
