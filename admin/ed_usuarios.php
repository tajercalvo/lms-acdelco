<?php include('src/seguridad.php'); ?>
<?php include('controllers/ed_usuarios.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons old_man"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/ver_perfil.php">Perfil</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Mi información </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<form id="formulario_usuarios" method="post" autocomplete="off">
	<div class="widget widget-heading-simple widget-body-gray widget-employees">
		<div class="widget-body padding-none">		
			<div class="row">
				<div class="col-md-12 detailsWrapper">
					<div class="ajax-loading hide">
						<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
					</div>
					<div class="innerAll">
						<div class="title">
							<div class="row">
								<div class="col-md-6">
									<h3 class="text-primary"><?php if(isset($datosUsuario['first_name'])){ echo($datosUsuario['first_name'].' '.$datosUsuario['last_name']); }else{ echo "Nuevo Usuario";}?></h3>
								</div>
								<div class="col-md-6 text-right">
								</div>
							</div>
						</div>
						<hr/>
						<div class="body">
							<div class="row">
								<div class="col-md-3 overflow-hidden">
									<!-- Profile Photo -->
									<div>
										<?php if(isset($datosUsuario['first_name'])){ ?>
											<img id="ImgUsuario" src="../assets/images/usuarios/<?php echo($datosUsuario['image']); ?>" style="width: 200px;" alt="<?php echo($datosUsuario['first_name'].' '.$datosUsuario['last_name']); ?>" />
										<?php }else{ ?>
											<img src="../assets/images/usuarios/default.png" style="width: 200px;" alt="Ludus LMS" />
										<?php } ?>
										<div class="ajax-loading hide" id="loading_imagen">
											<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
										</div>
									</div>
									<div class="separator bottom"></div>
									<!-- // Profile Photo END -->
									<?php if(isset($datosUsuario['first_name'])){ ?>
										<ul class="icons-ul separator bottom">
											<li><i class="fa fa-envelope fa fa-li fa-fw"></i> @: <?php echo($datosUsuario['email']); ?></li>
											<li><i class="fa fa-phone fa fa-li fa-fw"></i> Tel: <?php echo($datosUsuario['phone']); ?></li>
											<li><i class="fa fa-skype fa fa-li fa-fw"></i> <?php echo($datosUsuario['headquarter']); ?> </li>
										</ul>
									<?php } ?>
									<?php if(isset($_GET['back'])&&$_GET['back']=="ver_perfil"){ ?>
										<h5><a href="../admin/ver_perfil.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
								<div class="col-md-9 padding-none">
									<?php if(isset($datosUsuario['first_name'])){ ?>
									<h5 class="strong">Datos disponibles:</h5>
									<p style="text-align:justify">A continuación se presenta la información disponible del usuario, dichos datos han sido ingresados en la herramienta para la disponbilidad del directorio.</p>
									<?php }else{ ?>
									<h5 class="strong">Datos necesarios:</h5>
									<p style="text-align:justify">A continuación se solicita la información mínima necesaria para crear un usuario, dichos datos han serán ingresados en la herramienta para la disponbilidad del directorio.</p>
									<?php } ?>
									<div class="row">
										<div class="col-md-6 padding-none">
										    <p class="muted"><strong>Nombres:</strong>
												<div class="col-md-10 input-group">
													<input required type="text" class="form-control" id="first_name" name="first_name" placeholder="Nombre del usuario" <?php if(isset($datosUsuario['first_name'])){ ?> value="<?php echo($datosUsuario['first_name']); ?>" <?php } ?>/>
												</div>
											</p>
											<p class="muted"><strong>Nacimiento:</strong>
												<div class="col-md-4 input-group date">
											    	<input class="form-control" type="text" id="date_birthday" name="date_birthday" placeholder="Fecha" <?php if(isset($datosUsuario['date_birthday'])){ ?> value="<?php echo($datosUsuario['date_birthday']); ?>" <?php } ?>/>
											    	<span class="input-group-addon">
											    		<i class="fa fa-th"></i>
											    	</span>
												</div>
											</p>
											<p class="muted"><strong>Ingreso a Acdelco:</strong>
												<div class="col-md-4 input-group date">
											    	<input class="form-control" type="text" id="date_startgm" name="date_startgm" placeholder="Fecha" <?php if(isset($datosUsuario['date_startgm'])){ ?> value="<?php echo($datosUsuario['date_startgm']); ?>" <?php } ?>/>
											    	<span class="input-group-addon">
											    		<i class="fa fa-th"></i>
											    	</span>
												</div>
											</p>
											<p class="muted"><strong>Género:</strong>
												<div class="col-md-4 input-group date">
													<select style="width: 100%;" id="gender" name="gender">
														<option value="1" <?php if(isset($datosUsuario['gender'])&&$datosUsuario['gender']=="1"){ ?> selected="selected" <?php } ?>>Masculino</option>
														<option value="2" <?php if(isset($datosUsuario['gender'])&&$datosUsuario['gender']=="2"){ ?> selected="selected" <?php } ?>>Femenino</option>
														<option value="0" <?php if(isset($datosUsuario['gender'])&&$datosUsuario['gender']=="0"){ ?> selected="selected" <?php } ?>>Por Definir</option>
													</select>
												</div>
											</p>
										</div>
										<div class="col-md-6 padding-none">
										    <p class="muted"><strong>Apellidos:</strong>
												<div class="col-md-10 input-group">
													<input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Apellido del usuario" <?php if(isset($datosUsuario['last_name'])){ ?> value="<?php echo($datosUsuario['last_name']); ?>" <?php } ?>/>
												</div>
											</p>
											<p class="muted"><strong>Correo:</strong>
												<div class="col-md-10 input-group">
													<input type="text" class="form-control" id="email" name="email" placeholder="eMail" <?php if(isset($datosUsuario['email'])){ ?> value="<?php echo($datosUsuario['email']); ?>" <?php } ?>/>
												</div>
											</p>
											<p class="muted"><strong>Teléfono:</strong>
												<div class="col-md-8 input-group">
													<input type="text" class="form-control" id="phone" name="phone" onkeypress="return justNumbers(event);" placeholder="Teléfono" <?php if(isset($datosUsuario['phone'])){ ?> value="<?php echo($datosUsuario['phone']); ?>" <?php } ?>/>
												</div>
											</p>
											 <p class="muted"><strong>Empresa:</strong>
												<div class="col-md-10 input-group">
													<input required type="text" class="form-control" id="empresa" name="empresa" placeholder="Apellido del usuario" <?php if(isset($datosUsuario['empresa'])){ ?> value="<?php echo($datosUsuario['empresa']); ?>" <?php } ?>/>
												</div>
											</p>
											<!-- <?php echo "<pre>"; print_r($datosUsuario); ?> -->
										    <p class="muted"><strong>Pais:</strong>
												<div class="col-md-4 input-group date">
													<select style="width: 100%;" id="pais" name="pais">
														<option value="Colombia" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Colombia"){ ?> selected="selected" <?php } ?>>Colombia</option>
														<option value="Venezuela" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Venezuela"){ ?> selected="selected" <?php } ?>>Venezuela</option>
														<option value="Chile" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Chile"){ ?> selected="selected" <?php } ?>>Chile</option>
														<option value="Perú" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Perú"){ ?> selected="selected" <?php } ?>>Perú</option>
														<option value="Ecuador" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Ecuador"){ ?> selected="selected" <?php } ?>>Ecuador</option>
														<option value="Brasil" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Brasil"){ ?> selected="selected" <?php } ?>>Brasil</option>
														<option value="Argentina" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Argentina"){ ?> selected="selected" <?php } ?>>Argentina</option>
														<option value="Bolivia" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Bolivia"){ ?> selected="selected" <?php } ?>>Bolivia</option>
														<option value="Paraguay" <?php if(isset($datosUsuario['pais'])&&$datosUsuario['pais']=="Paraguay"){ ?> selected="selected" <?php } ?>>Paraguay</option>
													</select>
												</div>
											</p> 
											<div class="separator bottom"></div>
										</div>
									</div>
									<div class="separator bottom"></div>
									<div class="row">
										<div class="col-md-9">
										</div>		
										<div class="col-md-3">
											<div class="form-actions">
												<?php if(isset($datosUsuario['user_id'])){ ?>
													<input type="hidden" id="user_id" name="user_id" value="<?php echo($datosUsuario['user_id']);?>">
													<input type="hidden" id="opcn" name="opcn" value="editar">
													<button type="submit" class="btn btn-success" id="guardarEstudio"><i class="fa fa-check-circle"></i> Actualizar</button>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/ed_usuarios.js"></script>
</body>
</html>