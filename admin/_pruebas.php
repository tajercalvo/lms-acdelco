<?php include('src/seguridad.php'); ?>
<?php
include_once('../config/config.php');
include_once('../config/database.php');
include_once('controllers/email.php');

if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}
}
include('controllers/index.php');
$location = 'home';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Pruebas</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>

						<div class="row">
							<div class="col-md-12">
								<?php
									include_once('models/programacion_gz.php');
									$meses = [ "01" => "Enero", "02" => "Febrero", "03" => "Marzo", "04" => "Abril",
											"05" => "Mayo", "06" => "Junio", "07" => "Julio", "08" => "Agosto",
											"09" => "Septiembre", "10" => "Octubre", "11" => "Noviembre", "12" => "Diciembre" ];
									$pgz = new ProgramacionGZ();
									$email = new EmailLudus();
									$mes = date('m');
									$prog = $pgz->getProgramacion( $mes );
									$lideres = $pgz->getLideresGMAcademy();
									$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
									foreach ($lideres as $key => $lid) {
										$destinos = "";
										$titulo = "Programación ".$meses[ $mes ]." ".$lid['dealer'];
										// $para = $lid['email'];
										$para = "a.vega@luduscolombia.com.co";
										$mensaje_cuerpo = '<div style="background-color: white;">
																<table class="table">
																	<thead>
																		<tr>
																			<th>Tipo</th>
																			<th>Curso</th>
																			<th>Instructor</th>
																			<th>F. Inicio</th>
																			<th>F. Fin</th>
																			<th>Minimo</th>
																			<th>Maximo</th>
																			<th>Inscritos</th>
																		</tr>
																	</thead>
																	<tbody>';
										foreach ($prog as $key => $valProg) {
											if ( $lid['dealer_id'] == $valProg['dealer_id'] ){
												if( $valProg['end_date'] < $NOW_data ){
													$mensaje_cuerpo .= '<tr style="text-decoration:line-through;color:silver">';
												}else{
													$mensaje_cuerpo .= "<tr>";
												}
												$mensaje_cuerpo .= "<td>".$valProg['type']."</td>";
												$mensaje_cuerpo .= "<td>".$valProg['course']."</td>";
												$mensaje_cuerpo .= "<td>".$valProg['teacher_f_name']." ".$valProg['teacher_ls_name']."</td>";
												$mensaje_cuerpo .= "<td>".$valProg['start_date']."</td>";
												$mensaje_cuerpo .= "<td>".$valProg['end_date']."</td>";
												$mensaje_cuerpo .= "<td>".$valProg['min_size']."</td>";
												$mensaje_cuerpo .= "<td>".$valProg['max_size']."</td>";
												$mensaje_cuerpo .= "<td>".$valProg['inscriptions']."</td>";
												$mensaje_cuerpo .= "</tr>";
											}
										}
										$mensaje_cuerpo .= "</tbody>
														</table>
													</div><br>";
										echo( $mensaje_cuerpo );
										$resultado = $email->enviaNewEmail($titulo,$para,$mensaje_cuerpo,$destinos);
										break;
									}


								?>

							</div>
						</div>

					</div>
					<!-- // END heading -->

				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
</body>
</html>
