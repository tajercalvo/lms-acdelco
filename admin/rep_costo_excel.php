<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['year']) && isset($_GET['month']) && isset($_GET['type'])){
    include('controllers/rep_costo.php');
}
//echo('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Costo.xls");
header ("Content-Transfer-Encoding: binary");
if( isset($_GET['year']) && isset($_GET['month']) && isset($_GET['type']) ){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet"> <?php echo utf8_decode("CÓDIGO"); ?></th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">SEDE</th>
                <th data-hide="phone,tablet">CIUDAD</th>
                <th data-hide="phone,tablet">ZONA</th>
                <th data-hide="phone,tablet">ALUMNO</th>
                <th data-hide="phone,tablet"> <?php echo utf8_decode("IDENTIFICACIÓN"); ?></th>
                <th data-hide="phone,tablet">CARGO</th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet">FINAL</th>
                <th data-hide="phone,tablet">LUGAR</th>
                <th data-hide="phone,tablet">PROFESOR</th>
                <th data-hide="phone,tablet">ASISTENCIA</th>
                <th data-hide="phone,tablet">PUNTAJE</th>
                <th data-hide="phone,tablet"><?php echo utf8_decode("SATISFACCIÓN"); ?></th>
                <th data-hide="phone,tablet">HOJARUTA</th>
                <th data-hide="phone,tablet">RESULTADO</th>
                <th data-hide="phone,tablet">CUPOS INICIALES</th>
                <th data-hide="phone,tablet">CUPOS FINALES</th>
                <th data-hide="phone,tablet">INSCRITOS</th>
                <th data-hide="phone,tablet">EXCUSA</th>
                <!-- COBRO SI/NO -->
                <th data-hide="phone,tablet">HORAS CURSO</th>
                <th data-hide="phone,tablet">DIAS CURSO</th>
                <th data-hide="phone,tablet">ESPECIALIDAD</th>
                <th data-hide="phone,tablet">PROVEEDOR</th>
                <th data-hide="phone,tablet">TIPO DE CURSO</th>
                <th data-hide="phone,tablet">COBRO SI/NO</th>
                <th data-hide="phone,tablet">TOTAL</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php
                    $asistieron_tot = 0;
                    $noasistieron_tot = 0;
                    $aprobaron = 0;
                    $noaprobaron = 0;
                    $invitados = 0;
                    $puntaje = 0;
                    $aprobo = 0;
                    $puntaje_e = "0";
                    $puntaje_h = "0";
                    $var_Ctrl = 0;
                    $NoIscritos = 0;
                    $dealer_id_Cambio = 0;
                    $dealer_actual = "";
                    $schedule_actual = 0;
            if($cantidad_datos > 0){
                foreach ($datosCursos_all as $iID => $data) {
                    foreach ($data['detail'] as $iID => $dataDetail) {
                                $var_Ctrl++;
                                $result_org = "Reprobo";
                                $estado_inv = "Invitado";
                                //print_r($dataDetail);
                                if($dataDetail['estado']==1){
                                    $invitados++;
                                    $estado_inv = "Invitado";
                                }elseif($dataDetail['estado']==2){
                                    $asistieron_tot++;
                                    $estado_inv = "Asistio";
                                }else{
                                    $noasistieron_tot++;
                                    $estado_inv = "No Asistio";
                                }
                                if(isset($dataDetail['resultados'][0]['score'])){
                                    $puntaje = $dataDetail['resultados'][0]['score'];
                                    $aprobo = $dataDetail['resultados'][0]['approval'];
                                    $puntaje_e = $dataDetail['resultados'][0]['score_review'];
                                    $puntaje_h = $dataDetail['resultados'][0]['score_waybill'];
                                    if($aprobo=="SI"){
                                        $aprobaron++;
                                        $result_org = "Aprobo";
                                    }else{
                                        $noaprobaron++;
                                        $result_org = "Reprobo";
                                    }
                                }
                                $minimos = "0";
                                $maximos = "0";
                                $inscritos = "0";
                                if(isset($data['asistencia'])){
                                    foreach ($data['asistencia'] as $iID => $dataAsistencia) {
                                        if( $dataAsistencia['dealer_id']== $dataDetail['dealer_id']){
                                            $minimos = $dataAsistencia['min_size'];
                                            $maximos = $dataAsistencia['max_size'];
                                            $inscritos = $dataAsistencia['inscriptions'];
                                            if(($maximos - $inscritos)>0){
                                              $NoIscritos = $maximos - $inscritos;
                                            }else{
                                              $NoIscritos = 0;
                                            }
                                        }
                                    }
                                }
                                if(isset($data['excusas_dealer'])){ 
                                    foreach ($data['excusas_dealer'] as $iID => $dataExcusaCupo) {
                                        if( $dataExcusaCupo['dealer_id']== $dataDetail['dealer_id']){
                                            $NoIscritos -= $dataExcusaCupo['num_invitation'];
                                        }
                                    }
                                }
                                //echo($dealer_actual."::".$dataDetail['dealer']."<br>");
                                if($dealer_actual != $dataDetail['dealer'] || $schedule_actual != $dataDetail['schedule_id']){
                                  $dealer_id_Cambio = 1;
                                }else{
                                  $dealer_id_Cambio = 0;
                                }
                            ?>
                                <?php if($dealer_id_Cambio>0 && $NoIscritos>0){
                                  for($xx=0; $xx<$NoIscritos; $xx++){ ?>
                                  <tr>
                                    <td><?php echo utf8_decode(($dataDetail['dealer'])); ?></td>
                                    <td><?php echo utf8_decode(($data['newcode'])); ?></td>
                                    <td><?php echo utf8_decode(($data['course'])); ?></td>
                                    <td>NO DEFINIDO</td>
                                    <td><?php echo utf8_decode(($dataDetail['area'])); ?></td>
                                    <td><?php echo utf8_decode(($dataDetail['zone'])); ?></td>
                                    <td>SIN USUARIO</td>
                                    <td>SIN IDENTIFICACION</td>
                                    <td>SIN CARGOS</td>
                                    <td><?php echo utf8_decode(($data['start_date'])); ?></td>
                                    <td><?php echo utf8_decode(($data['end_date'])); ?></td>
                                    <td><?php echo utf8_decode(($data['living'])); ?></td>
                                    <td><?php echo utf8_decode(($data['first_prof'].' '.$data['last_prof'])); ?></td>
                                    <td>No Inscrito</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>No Inscrito</td>
                                    <td><?php echo utf8_decode(($minimos)); ?></td>
                                    <td><?php echo utf8_decode(($maximos)); ?></td>
                                    <td><?php echo utf8_decode(($inscritos)); ?></td>
                                    <td></td>
                                    <!-- <td></td> -->
                                    <td><?php echo utf8_decode($data['duration_time']);?></td>
                                    <td><?php echo utf8_decode($data['duration_time']/8);?></td>
                                    <td><?php echo utf8_decode(($data['specialty'])); ?></td>
                                    <!-- <td></td> -->
                                    <td><?php echo utf8_decode(($data['supplier'])); ?></td>
                                    <td><?php echo utf8_decode(($data['type'])); ?></td>
                                    <td>SI</td>
                                    <td><?php echo utf8_decode( number_format( $dataDetail['rate'], 0, ' ', '.' ) ) ; ?></td> <!-- <td></td> -->
                                  </tr>
                                <?php } } ?>
                    <tr>
                        <td><?php echo utf8_decode(($dataDetail['dealer'])); ?><?php //echo("..".$NoIscritos."..".$dealer_id_Cambio."..");?></td>
                        <td><?php echo utf8_decode(($data['newcode'])); ?></td>
                        <td><?php echo utf8_decode(($data['course'])); ?></td>
                        <td><?php echo utf8_decode(($dataDetail['headquarter'])); ?></td>
                        <td><?php echo utf8_decode(($dataDetail['area'])); ?></td>
                        <td><?php echo utf8_decode(($dataDetail['zone'])); ?></td>
                        <td><?php echo utf8_decode(($dataDetail['first_name'].' '.$dataDetail['last_name'])); ?></td>
                        <td><?php echo utf8_decode($dataDetail['identification']); ?></td>
                        <td>
                            <?php
                                if(isset($dataDetail['charges'])){
                                    // foreach ($dataDetail['charges'] as $iID => $dataCharges) {
                                    //     echo(" - ".$dataCharges['charge']);
                                    // }
                                    echo( $dataDetail['charges'] );
                                }
                            ?>
                        </td>
                        <td><?php echo utf8_decode(($data['start_date'])); ?></td>
                        <td><?php echo utf8_decode(($data['end_date'])); ?></td>
                        <td><?php echo utf8_decode(($data['living'])); ?></td>
                        <td><?php echo utf8_decode(($data['first_prof'].' '.$data['last_prof'])); ?></td>
                        <td><?php echo utf8_decode(($estado_inv)); ?></td>
                        <td><?php echo utf8_decode($puntaje); ?></td>
                        <td><?php echo utf8_decode($puntaje_e); ?></td>
                        <td><?php echo utf8_decode($puntaje_h); ?></td>
                        <td><?php echo utf8_decode(($result_org)); ?></td>
                        <td><?php echo utf8_decode(($minimos)); ?></td>
                        <td><?php echo utf8_decode(($maximos)); ?></td>
                        <td><?php echo utf8_decode(($inscritos)); ?></td>
                        <td></td>
                        <!-- <td></td> -->
                        <td><?php echo utf8_decode($data['duration_time']);?></td>
                        <td><?php echo utf8_decode($data['duration_time']/8);?></td>
                        <td><?php echo utf8_decode(($data['specialty'])); ?></td>
                       <!--  <td></td> -->
                        <td><?php echo utf8_decode(($data['supplier'])); ?></td>
                        <td><?php echo utf8_decode(($data['type'])); ?></td>
                        <td>SI</td>
                        <td><?php echo utf8_decode( number_format( $dataDetail['rate'], 0, ' ', '.' ) ) ; ?></td>
                    </tr>
                    <?php
                        $puntaje = 0;
                        $dealer_actual = $dataDetail['dealer'];
                        $schedule_actual = $dataDetail['schedule_id'];
                    } ?>

                    <?php if(isset($data['excusas'])){ foreach ($data['excusas'] as $iID => $dataDetail) {?>
                        <!-- Item -->
                        <tr>
                            <td><?php echo utf8_decode(($dataDetail['dealer'])); ?></td>
                            <td><?php echo utf8_decode(($data['newcode'])); ?></td>
                            <td><?php echo utf8_decode(($data['course'])); ?></td>
                            <td><?php echo utf8_decode(($dataDetail['headquarter'])); ?></td>
                            <td><?php echo utf8_decode(($dataDetail['area'])); ?></td>
                            <td><?php echo utf8_decode(($dataDetail['zone'])); ?></td>
                            <td><?php echo utf8_decode($dataDetail['first_name'].' '.$dataDetail['last_name']); ?></td>
                            <td><?php echo utf8_decode($dataDetail['identification']); ?></td>
                            <td>
                            <?php
                                if(isset($dataDetail['charges'])){
                                    // foreach ($dataDetail['charges'] as $iID => $dataCharges) {
                                    //     echo(" - ".$dataCharges['charge']);
                                    // }
                                    echo( $dataDetail['charges'] );
                                }
                            ?>
                            </td>
                            <td><?php echo utf8_decode(($data['start_date'])); ?></td>
                            <td><?php echo utf8_decode(($data['end_date'])); ?></td>
                            <td><?php echo utf8_decode(($data['living'])); ?></td>
                            <td><?php echo utf8_decode(($data['first_prof'].' '.$data['last_prof'])); ?></td>
                            <td>No Asistio</td>
                            <td>0<!-- <?php //echo utf8_decode($puntaje); ?> --></td>
                            <td>0<!-- <?php //echo utf8_decode($puntaje_e); ?> --></td>
                            <td>0<!-- <?php //echo utf8_decode($puntaje_h); ?> --></td>
                            <td>Reprobo</td>
                            <td><?php echo utf8_decode($dataDetail['min_size']);//echo utf8_decode(($minimos)); ?></td>
                            <td><?php echo utf8_decode($dataDetail['max_size']);//echo utf8_decode(($maximos)); ?></td>
                            <td><?php echo utf8_decode($dataDetail['inscriptions']);//echo utf8_decode(($inscritos)); ?></td>
                            <td>Excusa para Cobro</td>
                            <!-- <td></td> -->
                            <td><?php echo utf8_decode($data['duration_time']);?></td>
                            <td><?php echo utf8_decode($data['duration_time']/8);?></td>
                            <td><?php echo utf8_decode(($data['specialty'])); ?></td>
                            <!-- <td></td> -->
                            <td><?php echo utf8_decode(($data['supplier'])); ?></td>
                            <td><?php echo utf8_decode(($data['type'])); ?></td>
                            <td>NO</td>
                            <td></td>
                        </tr>
                        <!-- // Item END -->
                    <?php } }?>

                    <?php if(isset($data['excusas_dealer'])){ foreach ($data['excusas_dealer'] as $iID => $dataDetail) {
                        $total_excusas_dealer = $dataDetail['num_invitation'];
                        for($zz=0;$zz<$total_excusas_dealer;$zz++){ ?>
                        <!-- Item -->
                        <tr>
                            <td><?php echo utf8_decode(($dataDetail['dealer'])); ?></td>
                            <td><?php echo utf8_decode(($data['newcode'])); ?></td>
                            <td><?php echo utf8_decode(($data['course'])); ?></td>
                            <td>NO DEFINIDO</td>
                            <td><?php echo utf8_decode(($dataDetail['area'])); ?></td>
                            <td><?php echo utf8_decode(($dataDetail['zone'])); ?></td>
                            <td>SIN USUARIO</td>
                            <td>SIN IDENTIFICACION</td>
                            <td>SIN CARGOS</td>
                            <td><?php echo utf8_decode(($data['start_date'])); ?></td>
                            <td><?php echo utf8_decode(($data['end_date'])); ?></td>
                            <td><?php echo utf8_decode(($data['living'])); ?></td>
                            <td><?php echo utf8_decode(($data['first_prof'].' '.$data['last_prof'])); ?></td>
                            <td>No Inscrito</td> <!-- No Asignado -->
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>No Inscrito</td> <!-- No Asignado -->
                            <td><?php echo utf8_decode($dataDetail['min_size']); ?></td>
                            <td><?php echo utf8_decode($dataDetail['max_size']); ?></td>
                            <td><?php echo utf8_decode($dataDetail['inscriptions']); ?></td>
                            <td>Excusa Soporte Cupo para Cobro</td>
                            <!-- <td></td> -->
                            <td><?php echo utf8_decode($data['duration_time']);?></td>
                            <td><?php echo utf8_decode($data['duration_time']/8);?></td>
                            <td><?php echo utf8_decode(($data['specialty'])); ?></td>
                            <!-- <td></td> -->
                            <td><?php echo utf8_decode(($data['supplier'])); ?></td>
                            <td><?php echo utf8_decode(($data['type'])); ?></td>
                            <td>NO</td>
                            <td></td>
                            <!-- <td></td> -->
                        </tr>
                        <!-- // Item END -->
                    <?php } } }?>

          <?php } ?>
      <?php } ?>
            <?php
            //print_r($datosCursosNoInv);
            if(count($datosCursosNoInv)>0){
              foreach ($datosCursosNoInv as $iID => $data) {
                $comparar = $data['minimos'] - $data['excusas'];
                for($yy=0;$yy<$comparar;$yy++){
                    $valor = 0;
                ?>
              <tr>
                <td><?php echo utf8_decode(($data['dealer'])); ?></td>
                <td><?php echo utf8_decode(($data['newcode'])); ?></td>
                <td><?php echo utf8_decode(($data['course'])); ?></td>
                <td>NO DEFINIDO</td>
                <td><?php echo utf8_decode(($data['area'])); ?></td> 
                <td><?php echo utf8_decode(($data['zone'])); ?></td>
                <td>SIN USUARIO</td>
                <td>SIN IDENTIFICACION</td>
                <td>SIN CARGOS</td>
                <td><?php echo utf8_decode(($data['start_date'])); ?></td>
                <td><?php echo utf8_decode(($data['end_date'])); ?></td>
                <td><?php echo utf8_decode(($data['living'])); ?></td>
                <td><?php echo utf8_decode(($data['first_prof'].' '.$data['last_prof'])); ?></td>
                <td>No Inscrito</td> <!-- No invitado -->
                <td>0</td>
                <td>0</td>
                <td>0</td>
                <td>No Inscrito</td> <!-- No invitado -->
                <td><?php echo utf8_decode(($data['minimos'])); ?></td>
                <td><?php echo utf8_decode(($data['maximos'])); ?></td>
                <td><?php echo utf8_decode(0); ?></td>
                <td></td>
                <!-- <td></td> -->
                <td><?php echo utf8_decode($data['duration_time']);?></td>
                <td><?php echo utf8_decode($data['duration_time']/8);?></td>
                <td><?php echo utf8_decode(($data['specialty'])); ?></td>
                <!-- <td></td> -->
                <td><?php echo utf8_decode(($data['supplier'])); ?></td>
                <td><?php echo utf8_decode(($data['type'])); ?></td>
                <?php if( $data['valor'] == 0 ){ ?> <td>NO</td> <td></td>
                <?php } else{ ?>
                <td>SI</td> 
                <td><?php
                    $valor = $data['valor']/$data['cupos'];
                    echo utf8_decode( number_format( $valor, 0, ' ', '.' ) ); 
                 ?></td>
                <?php } ?>

              </tr>
            <?php } } }?>
        </tbody>
    </table>
<?php } ?>
