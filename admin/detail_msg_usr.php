<?php include('src/seguridad.php');
$detail_msg = "SI";
include('controllers/mensajes.php');
?>
<div class="innerAll">
	<div class="input-group">
    	<input class="form-control" id="busqueda_pal" name="busqueda_pal" type="text" placeholder="Buscar Remitente .." value="" />
    	<span class="input-group-addon">
    		<i class="fa fa-search"></i>
    	</span>
	</div>
</div>
<span class="results">Mensajes: <?php echo $cantidadTotal; ?> | Remitentes: <?php echo $cantidadRemitentes; ?> | Destinatarios: <?php echo $cantidadDestinatarios; ?><i class="fa fa-circle-arrow-down"></i></span>
<ul class="list unstyled">
	<?php foreach ($listadoPersonas as $iID => $dataPersona) { ?>
		<li class="UsrMsg<?php if($dataPersona['user_id']==$idPer_sel){ ?> active<?php } ?>" data-id="<?php echo $dataPersona['user_id']; ?>" data-name="<?php echo $dataPersona['first_name'].' '.$dataPersona['last_name']; ?>">
			<div class="media innerAll">
				<div class="media-object pull-left thumb hidden-phone"><img alt="Image" src="../assets/images/usuarios/<?php echo $dataPersona['image']; ?>" style="width: 50px; height: 50px;" /></div>
				<div class="media-body">
					<span class="usr_list strong"><?php echo $dataPersona['first_name'].' '.$dataPersona['last_name']; ?></span>
					<small>Ultimo mensaje enviado el:</small><br/>
					<small class="text-italic text-primary-light"><i class="fa fa-calendar fa fa-fixed-width"></i> <?php echo $dataPersona['date_creation']; ?></small>
				</div>
			</div>
		</li>
	<?php } ?>
	<li class="MsgNvo">
		<div class="media innerAll">
			<div class="media-object pull-left thumb hidden-phone"><img alt="Image" src="../assets/images/new_messages.png" style="width: 50px; height: 50px;" /></div>
			<div class="media-body">
				<span class="strong">
					<select style="width: 100%;" id="user_msg_id" name="user_msg_id">
					<?php foreach ($listadosUsuarios as $key => $Data_Usr) { ?>
						<option value="<?php echo($Data_Usr['user_id']); ?>" ><?php echo($Data_Usr['first_name'].' '.$Data_Usr['last_name']); ?></option>
					<?php } ?>
					</select>
				</span>
				<small>Escribir un mensaje nuevo</small><br/>
				<small class="text-italic text-primary-light"><i class="fa fa-calendar fa fa-fixed-width"></i> Ahora</small>
			</div>
		</div>
	</li>
</ul>