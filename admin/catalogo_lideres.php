<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/clublideres.php');
$location = 'club';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/club.css" />
	<link rel="stylesheet" href="css/argumentario.css" media="screen" title="argumentario">
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="club_lideres.php" class="glyphicons gift"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Catálogo de premios</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
				<!-- Sección club de líderes -->


<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
<!-- Las modales de redención-->
<?php foreach ($listadoProductos as $key => $producto) {?>
<div id="Prod<?php echo($producto['clubproduct_id']);?>" class="modal fade">
    <div class="modal-dialog" style="left: -120px;">
        <div class="modal-content" style="width:800px;height:550px;top:50px;overflow-y: scroll;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="TituloVideo"><?php echo(utf8_encode($producto['name_product'])); ?> - <span class="<?php if(isset($ClubMisDatos['km_act']) && $ClubMisDatos['km_act']>=$producto['km']){?>text-success<?php }else{?>text-danger<?php }?>">KM NECESARIOS: <?php echo(number_format(($producto['km']),0)); ?></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                	<div class="col-md-6">
                		<img src="../assets/gallery/banners/ClubLideres/productos/<?php echo($producto['image']); ?>" alt="<?php echo(utf8_encode($producto['name_product'])); ?>" class="img-responsive padding-none border-none" />
                	</div>
                	<div class="col-md-6" style="background-color: #393939; height: 550px; min-height: 550px;">
                		<div class="separator bottom"></div>
                		<h4 class="text-primary text-center"><b><?php echo(utf8_encode($producto['name_product'])); ?></b></h4><br>
                		<p class="text-white"><b><?php echo(number_format(($producto['km']),0)); ?> Kilómetros Chevrolet</b></p>
                		<p class="text-white"><b>Descripción:</b> <?php echo(utf8_encode($producto['description'])); ?></p>
                		<p class="text-white"><b>Marca:</b> <?php echo(utf8_encode($producto['brand'])); ?></p>
                		<div class="separator bottom"></div>
                		<div class="separator bottom"></div>
                		<?php if(isset($ClubMisDatos['km_act']) && $ClubMisDatos['km_act']>=$producto['km']){?>
											<p class="text-white">Actualmente las redenciones se encuentran inactivas!</p>
	                		<!--<form action="catalogo_lideres.php" method="post" onsubmit="ProcesoRedencion('Form<?php echo($producto['clubproduct_id']);?>');" id="Form<?php echo($producto['clubproduct_id']);?>">
	                			<input type="hidden" name="opcn" id="opcn" value="redimir">
	                			<input type="hidden" name="clubproduct_id" id="clubproduct_id" value="<?php echo($producto['clubproduct_id']); ?>">
		                		<p class="text-white"><b>Nota:</b> Si el producto que se describe aquí requiere una talla específica, por favor indicarla a continuación (Lea muy bien la descripción del mismo por favor antes de redimirlo):</p><br>
		                		<p class="text-white"><b>Talla:</b><br><input type="text" class="form-control" id="talla" name="talla" placeholder="Por ejemplo: S, M, L, XL, 33, 36" /></p><br>
	                			<div class="col-md-12 text-center"><button type="submit" class="btn btn-primary" id="guardarRedencion"><i class="fa fa-check-circle"></i> REDÍMELO AHORA</button></div>
	                		</form>-->
                		<?php }else{?>
											<p class="text-white">Actualmente las redenciones se encuentran inactivas, estamos revisando los puntos para que puedas redimir mucho más!</p>
                			<!--<p class="text-white">Tus kilómetros te acercan a lo que más te gusta, pronto podrás redimir este producto</p><br>-->
                		<?php }?>
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>
<!-- Las modales de redención-->
	<div class="widget-body padding-none border-none">
		<div class="row">
			<div class="col-md-1 detailsWrapper"></div>
			<div class="col-md-10 detailsWrapper padding-none" style="box-shadow: 1px 1px 10px #888 !important;height: 210px;">
				<img style="position: absolute;" align="center" src="../assets/gallery/banners/ClubLideres/catalogo.jpg" alt="" class="img-responsive padding-none border-none" />
				<div style="position: inherit;z-index: 1000;float: right;top: 30px;right: 180px;font-size: 15px;font-family: LouisRegular">
					<b style="font-size: 15px;align-content: center;" id="TituloCatalogo"><span class="text-primary">1. </span> SELECCIONA EL PRODUCTO DE TU PREFERENCIA <br>DE ACUERDO A LOS PUNTOS QUE TIENES ACUMULADOS</b><br>
					<b style="font-size: 15px;align-content: center;" id="TituloCatalogo"><span class="text-primary">2. </span> ASEGÚRATE QUE ESTÁS REDIMIENTO EL PRODUCTO DE TU ELECCIÓN</b><br>
					<b style="font-size: 15px;align-content: center;" id="TituloCatalogo"><span class="text-primary">3. </span> LLENA LA INFORMACIÓN QUE ES SOLICITADA</b><br>
					<b style="font-size: 15px;align-content: center;" id="TituloCatalogo"><span class="text-primary">4. </span> UNA VEZ APROBADO INICIAMOS EL PROCESO DE ALISTAMIENTO Y ENTREGA</b><br><br>
					<b>MIS KILÓMETROS CHEVROLET: <span class="text-inverse"><?php if(isset($ClubMisDatos['km_act'])){ echo(number_format($ClubMisDatos['km_act'])); }else{ echo("0");}?></span><br></b>
				</div>
				<div class="separator bottom"></div>
			</div>
		</div>
		<div class="separator bottom"></div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10 detailsWrapper padding-none" style="box-shadow: 1px 1px 10px #888 !important; background-color: #eee;">
				<div class="row">
					<div class="col-md-12 center">
						<div class="relativeWrap" >
							<div class="wizard">
								<div class="widget widget-tabs widget-tabs-vertical row margin-none border-none " style="background-color: #eee;">
									<div class="widget-head col-md-3">
										<ul id="listaMenu">
										<?php
										$ctrl = 0;
										foreach ($consultaProductos as $key => $valores) {
										?>
											<li <?php if($ctrl==0){ ?>class="active"<?php }?>><a href="#<?php echo(utf8_encode(str_replace(" ","",$valores['category']))); ?>" data-toggle="tab"><span class="text-uppercase text-center"><?php echo(utf8_encode($valores['category'])); ?></span></a></li>
										<?php $ctrl++; }?>
										</ul>
									</div>
									<div class="widget-body col-md-9" style="background-color: #eee;">
										<div class="tab-content">
										<?php
										$ctrl = 0;
										foreach ($consultaProductos as $key => $valores) {
										?>
											<div class="tab-pane <?php if($ctrl==0){ ?>active<?php }?>" id="<?php echo(utf8_encode(str_replace(" ","",$valores['category']))); ?>" style="background-color: #eee;">
												<?php foreach ($valores['products'] as $key => $producto) {?>
												<div class="col-md-4">
													<a data-toggle="modal" href="#Prod<?php echo($producto['clubproduct_id']);?>">
														<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
															<div class="relativeWrap overflow-hidden" style="height:320px">
																	<img src="../assets/gallery/banners/ClubLideres/productos/<?php echo($producto['image']); ?>" alt="<?php echo($producto['name_product']); ?>" class="img-responsive padding-none border-none" />
																<!--<div class="fixed-bottom bg-inverse-faded">-->
																<div class="fixed-bottom" style="background: #d65050 !important;">
																	<div class="media margin-none innerAll">
																		<div class="media-body text-white">
																			<strong class="text-center text-uppercase" style="font-size: 80%;">
																				<?php echo(utf8_encode($producto['name_product'])); ?>
																			</strong>
																			<div class="row border-none" style="font-size: 80%;">
																				<?php echo(number_format($producto['km'])); ?> Km
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</a>
												</div>
												<?php }?>
											</div>
										<?php $ctrl++; }?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(isset($resultado_REDENCION)){?>
	<input type="hidden" name="ResReden" id="ResReden" value="<?php echo($resultado_REDENCION);?>">
<?php }?>
				<!-- Sección club de líderes -->
				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/clublideres.js?rand=<?php echo rand();?>"></script>
</body>
</html>
