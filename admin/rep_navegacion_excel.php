<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_navegacion.php');
}
//echo('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
/*header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Navegacion.xls");
header ("Content-Transfer-Encoding: binary");*/
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_Navegacion.csv');
if(isset($_GET['start_date_day'])){
    echo("CONCESIONARIO;SEDE;NOMBRES;APELLIDOS;IDENTIFICACION;CARGOS;TELEFONOS;EMAIL;SECCION;FECHA Y HORA;TIEMPO NAVEGACION (seg)\n");
    if( count($DetalleVisitas) > 0){ 
        foreach ($DetalleVisitas as $iID => $data) {
            $Section = $data['section'];
            if($Section=='hojas.php'){ $Section = 'Hojas de Ruta'; }
            if($Section=='op_hoja.php'){ $Section = 'Operación Hojas de Ruta'; }
            $Section = str_replace('.php', '', $Section);
            $Section = str_replace('rep_', 'Reporte ', $Section);
            $Section = str_replace('op_', 'Operación ', $Section);
            $Section = str_replace('_detail', ' Detalle', $Section);
            $Section = str_replace('_', ' ', $Section);
            $Section = ucwords($Section);
            $charge = "";
            $timeNavigation = str_replace('.', ',', $data['time_navigation']);
            $telefonos = $data['phone'];
            $emai_p = str_replace(';', ',', $data['email']);
            foreach ($data['charges'] as $idC => $Charge) {
                $charge .= $Charge['cargo'];
            }
            echo(utf8_decode($data['dealer'].";".$data['headquarter'].";".$data['first_name'].";".$data['last_name'].";".$data['identification'].";".$charge.";".$telefonos.";".$emai_p.";".$Section.";".$data['date_navigation'].";".$timeNavigation."\n"));
        }
    }

    echo "\n \n";
    echo(utf8_decode ("CONCESIONARIO;TOTAL USUARIOS;USUARIOS ÚNICOS CON ITERACCIÓN;PROMEDIO;\n"));
    if(count($usuarios_iteracion) > 0){ 

        foreach ($usuarios_iteracion as $key => $value2) {

        echo(utf8_decode($value2['dealer'].";".$value2['todos'].";".$value2['con_iteracion'].";".round(($value2['con_iteracion']/$value2['todos'])*100, 1).";"."\n"));
       
            } 
    }

     echo "\n \n";
    echo(utf8_decode ("CONCESIONARIO;TOTAL USUARIOS;USUARIOS ÚNICOS CON ITERACCIÓN;PROMEDIO;\n"));
    if(count($usuarios_iteracion_GM) > 0){ 

        foreach ($usuarios_iteracion_GM as $key => $value) {

        echo(utf8_decode($value['dealer'].";".$value['todos'].";".$value['con_iteracion'].";".round(($value['con_iteracion']/$value['todos'])*100, 1).";"."\n"));
       
            } 
    }

  

} 
?>