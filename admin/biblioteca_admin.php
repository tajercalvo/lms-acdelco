<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/biblioteca_admin.php');
$location = 'biblioteca';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head><meta charset="gb18030">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php" >Biblioteca</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<!-- Sección biblioteca -->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">

								<div class="col-md-12 detailsWrapper">
									<div class="innerAll" style="padding: 0px;">
										<div class="body">
											<div class="row padding">
												<div class="col-md-12">
													<!-- // Inicio cuerpo de la pagina -->

														<div class="row">
															<h4 class="margin-none pull-left">  <a href="#Modalprogress_bar" data-toggle="modal" style=" color: black;" id="agregar" ><i class="fa fa-cloud-upload" ></i> Agregar Archivo </a>
															 </h4>
														</div>
														<div clas="separator"></div>

														<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
	</div>
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i>Información:</i> <strong>   </strong> Total de registros:<strong class="text-primary" id="cant_registros"> </strong>  </h4>
	</div>
	<!-- // Widget heading END -->
	<style type="text/css" media="screen">
		table>thead>tr>th{
			background: white !important;
			color:#d65050 !important;
			text-align: center;
			border-top: 1px solid #d65050  !important;
			border-bottom: 1px solid #d65050  !important;
			width: 25%;
			height: 25px;
			text-transform: uppercase;
		}

		table>tbody>tr>td{
			background: white !important;
			color:black !important;
			text-align: center;
			border: 1px solid lightgray  !important;
			width: 25%;
			height: 100px;
			font-size: 10px;
		}

		table>tbody>tr>tr:nth-child(odd) {
		    background-color:red !important;
		}
		table>tbody>tr>tr:nth-child(even) {
		    background-color:green !important;
		}

		table>tbody>tr>tr:hover {
		background-color: yellow;
		}

	</style>
	<div class="widget-body" style="border: none;">
		<!-- Table elements-->
		<table id="tabla">

		</table>
		<!-- // Table elements END -->
		<!-- Total elements-->
		<div class="form-inline separator bottom small">
		 <?php if(isset($cantidad_datos)){echo "Total de registros:";} ?><strong class='text-primary'> <?php if(isset($cantidad_datos)){echo($cantidad_datos);} ?> </strong>
		</div>
		<!-- // Total elements END -->
	</div>
</div>

						<!-- Inicio Modal carga de archivos con progressBar  -->
						<form id="upload_form" enctype="multipart/form-data" method="post">
							<div class="modal fade" id="Modalprogress_bar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog" style="z-index: 100000;">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title">Crear Nuevo archivo para la biblioteca</h4>
										</div>
										<div class="modal-body">
											<div class="widget widget-heading-simple widget-body-gray">
												<div class="widget-body">
													<!-- Row  -->
													<div class="row">
														<div class="col-md-12">
															<label class="control-label">Nombre: </label>
															<input type="text" id="name" name="name" class="form-control col-md-8" />
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<label class="control-label">Descripcion: </label>
															<textarea id="description" name="description" class="form-control col-md-8"></textarea>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<br>
															<label class="control-label">Especialidad: </label> <br>
															<select name="segment_id" id="segment_id">
																<?php foreach ($especialidades as $key2 => $value2) {?>
																	<option value="<?php echo $value2['specialty_id']; ?>"><?php echo utf8_encode($value2['specialty']); ?></option>
																<?php } ?>
															</select>
														</div>
														<div class="col-md-6">
																<br>
																<label class="control-label">Estado: </label> <br>
																<select name="status_id" id="status_id">
																	<option value="1">Activo</option>
																	<option value="2">Inactivo</option>
																</select>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<br>
															<p style="text-align: center;color: #bd272c ">Recuerde que el Archivo debe pesar menos de <b>100 MegaByte (MB)</b></p>
														</div>
														<div class="col-md-6">
																<div class="checkbox">
																	<label class="checkbox-custom">
																		<input type="checkbox" name="checkbox" id="checkbox">
																		<i id="prueba" class="fa fa-fw fa-square-o checked"></i>URL de youtube
																	</label>
																</div>
																<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																	<span class="btn btn-default btn-file">
																		<span class="fileupload-new">Seleccionar Imagen</span>
																		<input type="file" class="margin-none" id="image" name="image" accept="image/jpeg, image/png" />
																	</span>
																</div>
														</div>
														<div class="col-md-6">
															<br>
															<div id="video_youtube" style="display: none;">
																<label class="control-label">Url Youtube: </label> <br>
																<input type="text" placeholder="URL Ej: https://www.youtube.com" class="form-control" id="url" name="url" style="color: black" />
															</div>
															<div id="archivo_biblioteca" class="fileupload fileupload-new margin-none" data-provides="fileupload">
																	<label class="control-label">Seleccione el archivo</label>
																	<br>
																	<span class="btn btn-default btn-file">
																		<span class="fileupload-new">Seleccionar Archivo</span>
																		<input type="file" class="margin-none" id="file1" name="file1"/>
																	</span>
																</div>
														</div>
													</div>
														<div class="clearfix"><br></div>
														<!-- Row  -->
														<div class="row">
															<div class="col-md-12">
																<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Trayectoria </h5>
																<select multiple="multiple" style="width: 100%;" id="charge_id" name="charge_id[]">
																		<?php foreach ($cargos as $key => $Data_Charge) { ?>
																			<option value="<?php echo $Data_Charge['charge_id']; ?>"><?php echo $Data_Charge['charge']; ?></option>
																		<?php } ?>
																</select>
															</div>
														</div>
														<!-- Row END -->
														<div class="clearfix"><br></div>
														<!-- Row  -->
														<div class="row">
															<div class="col-md-12">
																<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Empresas </h5>
																<select multiple="multiple" style="width: 100%;" id="dealer_id" name="dealer_id[]">
																		<?php foreach ($concesionarios as $key => $Data_dealers) { ?>
																			<option value="<?php echo $Data_dealers['dealer_id']; ?>"><?php echo $Data_dealers['dealer']; ?></option>
																		<?php } ?>
																</select>
															</div>
														</div>
														<!-- Row END -->
														<div class="clearfix"><br></div>
														<!-- Row  -->
														<div class="row">
															<div class="col-md-12">
																<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Perfil </h5>
																<select multiple="multiple" style="width: 100%;" id="rol_id" name="rol_id[]">
																		<?php foreach ($roles as $key => $Data_rol) { ?>
																			<option value="<?php echo $Data_rol['rol_id']; ?>"><?php echo $Data_rol['rol']; ?></option>
																		<?php } ?>
																</select>
															</div>
														</div>
														<!-- Row END -->
														<div class="clearfix"><br></div>
												<div class="modal-footer">
													<div class="progress progress-mini" style="margin-bottom: 5px;">
														<div class="progress-bar progress-bar-primary" id="progressBar" style="width: 0%;"></div>
													</div>
													<div class="row">
														<div class="col-md-10" style="text-align: left">
															<label id="status" class="control-label"></label>
														</div>
														<div class="col-md-2"><a onclick="uploadFile()" class="btn btn-primary">Subir <i class="fa fa-cloud-upload"></i></a></div>
													</div>
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
																<!-- Fin Modal carga de archivos con progressBar  -->
														<!-- // Fin modal editar archi-->
<!-- inico modal visto por -->

				<div class="modal fade" id="Visto_por" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 id="nombre_archivo_visto" class="modal-title"></h4>
								</div>
								<div class="modal-body">
									<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
							<br>
							<br>
								<!-- Row -->
								<div class="row">

										<div class="block block-inline">
											<div class="caret"></div>
											<div class="box-generic">
												<div class="timeline-top-info content-filled border-bottom">
													<i class="fa fa-user"></i> Subido por: <a href="#"><img src="../assets/images/usuarios/default.png" id="imagen" alt="photo" width="20"></a> <a href="#"><label id="nombre_usuario" class="control-label"> </label></a>
														<i class="fa fa-clock-o"></i> <label id="fecha" class="control-label"> </label>
												</div>

												<!-- <section id="miTablaaa"></section> -->

											    <div id="vistos"></div>
											 </div>
										</div>
								 </div>

								<!-- Row END-->
								<div class="clearfix"><br></div>

								<div class="clearfix"><br></div>
							</div>
						</div>
								</div>
							</div>
						</div>
				</div>

																<!-- // Fin cuerpo de la pagina -->
																<!-- // Inicio pie de pagina -->
														<div class="separator bottom"></div>
														<div class="row">
															<div class="col-md-3 center"></div>
															<div class="col-md-6 center">
															</div>
															<div class="col-md-3 center">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div> </div>
								<!-- // Fin class widget-heading  -->
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- // Sidebar menu & content wrapper END -->
				<?php include('src/footer.php'); ?>
			</div>
			<!-- // Main Container Fluid END -->
			<?php include('src/global.php'); ?>
			<script src="js/biblioteca_admin.js?v=<?php echo md5(time()); ?>"></script>
		</body>
		</html>
