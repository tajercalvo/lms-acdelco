<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_tickets.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons temple_buddhist"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Reporte de Tickets</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Tickets </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
				<div class="widget widget-heading-simple widget-body-gray">
					<div class="widget-body">
						<div class="row">
							<div class="col-md-10">
								<h5 style="text-align: justify; ">Encontrará en esta opción la forma de consultar los tickets realizados y sus respectivos estados entre 2 fechas.</h5></br>
							</div>
							<div class="col-md-2">
								<?php if($cantidad_datos > 0){ ?>
									<h5><a href="rep_tickets_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<form action="rep_tickets.php" method="post">
							<div class="col-md-5">
								<!-- Group -->
								<div class="form-group">
									<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
									<div class="col-md-7 input-group date">
								    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
								    	<span class="input-group-addon">
								    		<i class="fa fa-th"></i>
								    	</span>
									</div>
								</div>
								<!-- // Group END -->
							</div>
							<div class="col-md-5">
								<!-- Group -->
								<div class="form-group">
									<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Final:</label>
									<div class="col-md-7 input-group date">
								    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
								    	<span class="input-group-addon">
								    		<i class="fa fa-th"></i>
								    	</span>
									</div>
								</div>
								<!-- // Group END -->
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
							</div>
							</form>
						</div>
					</div>
				</div>
				<?php
				if($cantidad_datos > 0){ ?>
					<div class="well">
						<p class="lead">Tickets reportados</p>
						<div class="row row-merge">
							<div class="col-md-4 bg-gray">
								<div class="innerAll inner-2x text-center">
									<div class="sparkline" sparkHeight="100" data-colors="#cacaca,#609450,#5cc7dd"><?php echo($grafica['abiertos']); ?>,<?php echo($grafica['cerrados']); ?>,<?php echo($grafica['reabiertos']); ?></div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="innerAll">
									<ul class="list-unstyled">
										<li class="innerAll half"><i class="fa fa-fw fa-square text-info"></i> <span class="strong"><?php echo($grafica['abiertos']); ?></span> Abiertos</li>
										<li class="innerAll half"><i class="fa fa-fw fa-square text-success"></i> <span class="strong"><?php echo($grafica['cerrados']); ?></span> Cerrados</li>
										<li class="innerAll half"><i class="fa fa-fw fa-square text-muted"></i> <span class="strong"><?php echo($grafica['reabiertos']); ?></span> Reabiertos</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="well">
						<table class="table table-invoice">
							<thead>
								<tr>
									<th>TICKET</th>
									<th>USUARIO</th>
									<th>CONCESIONARIO</th>
									<th>ESTADO</th>
									<th>DESCRIPCIÓN</th>
									<th>PRIORIDAD</th>
									<th>TIPO</th>
									<th>CREACIÓN</th>
									<th>RESPUESTA</th>
									<th>ENCARGADO</th>
									<th>RESPUESTA</th>
									<th>RESPUESTAS</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($datos_tickets as $iID => $data) { ?>
									<tr>
										<td><?php echo($data['ticket_id']); ?></td>
										<td><?php echo($data['first_name']." ".$data['last_name']); ?></td>
										<td><?php echo($data['dealer']); ?></td>
										<td><?php switch ($data['status_id']) {
											case '0': ?>
												<label class="label label-success">Cerrado</label>
											<?php break;
											case '1': ?>
												<label class="label label-info">Pendiente</label>
											<?php break;
											case '3': ?>
												<label class="label label-info">Reabierto</label>
											<?php break;
											}//fin switch ?>
										</td>
										<td><?php echo($data['description']); ?></td>
										<td><?php switch ($data['priority']) {
											case '1': ?>
												<label class="label label-important">Alta</label>
											<?php break;
											case '2': ?>
												<label class="label label-warning">Media</label>
											<?php break;
											case '3': ?>
												<label class="label label-default">Baja</label>
											<?php break;
										}//fin switch ?></td>
										<td><?php echo($data['key_description']); ?></td>
										<td><?php echo($data['date_creation']); ?></td>
										<td><?php if(isset($data['date_respuesta'])){echo($data['date_respuesta']);} ?></td>
										<td><?php if(isset($data['first_s_name']) && isset($data['last_s_name']) ){ echo($data['first_s_name']." ".$data['last_s_name']); } ?></td>
										<td><?php if(isset($data['date_respuesta'])){
												$datetime1 = date_create($data['date_creation']);
												$datetime2 = date_create($data['date_respuesta']);
												$interval = date_diff($datetime1, $datetime2);
												echo $interval->format('%R%a días');
												} ?>
										</td>
										<td><?php if(isset($data['respuestas'])){ echo($data['respuestas']); }else{ echo(0); } ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				<?php }?>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_tickets.js"></script>
</body>
</html>
