<?php include('src/seguridad.php'); ?>
<?php include('controllers/dashboard.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons charts"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/dashboard.php">Dashboard</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Análisis de información general </h2>
							<br><br><!--<div class="clearfix"> &nbsp;[<?php echo $fecha_inicial.' - '.$fecha_final; ?>] <?php echo $diferencia_mes; ?> Meses </div>-->
							<button type="button" data-toggle="print" class="btn btn-default print hidden-print"><i class="fa fa-fw fa-print"></i> Imprimir</button>
						</div>
					<!-- // END heading -->
<!-- contenido filtros -->
	<div class="widget widget-heading-simple widget-body-gray hidden-print">
		<div class="widget-body">
			<div class="row">
				<div class="row">
					<form action="dashboard_gral.php" method="post">
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-3 control-label" for="dealer_id" style="padding-top:8px;">Concesionario:</label>
									<div class="col-md-8">
										<select style="width: 100%;" id="dealer_id" name="dealer_id">
											<?php foreach ($dealer_active as $key => $Data_ActiveDealer) { 
												?>
												<option value="<?php echo $Data_ActiveDealer['dealer_id']; ?>" <?php if(isset($_POST['dealer_id']) && ($_POST['dealer_id']==$Data_ActiveDealer['dealer_id']) ){ ?>selected="selected"<?php } ?> ><?php echo $Data_ActiveDealer['dealer']; ?></option>
											<?php } ?>
							        	</select>
							        </div>
							</div>
							<!-- // Group END -->
						</div>
						<div class="col-md-6">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-3 control-label" for="start_date" style="padding-top:8px;">Hasta:</label>
								<div class="col-md-4">
									<select style="width: 100%;" id="ano_id_end" name="ano_id_end" >
										<?php for ($y=date("Y");$y>2010;$y--){ ?>
											<option value="<?php echo $y; ?>" <?php if($y==$year_end){ ?>selected="selected"<?php } ?>><?php echo $y; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-5">
									<select style="width: 100%;" id="mes_id_end" name="mes_id_end" >
										<option value="01" <?php if($mes_end == "01"){ $mesName = "Enero"; ?>selected="selected"<?php } ?> >Enero</option>
										<option value="02" <?php if($mes_end == "02"){ $mesName = "Febrero"; ?>selected="selected"<?php } ?> >Febrero</option>
										<option value="03" <?php if($mes_end == "03"){ $mesName = "Marzo"; ?>selected="selected"<?php } ?> >Marzo</option>
										<option value="04" <?php if($mes_end == "04"){ $mesName = "Abril"; ?>selected="selected"<?php } ?> >Abril</option>
										<option value="05" <?php if($mes_end == "05"){ $mesName = "Mayo"; ?>selected="selected"<?php } ?> >Mayo</option>
										<option value="06" <?php if($mes_end == "06"){ $mesName = "Junio"; ?>selected="selected"<?php } ?> >Junio</option>
										<option value="07" <?php if($mes_end == "07"){ $mesName = "Julio"; ?>selected="selected"<?php } ?> >Julio</option>
										<option value="08" <?php if($mes_end == "08"){ $mesName = "Agosto"; ?>selected="selected"<?php } ?> >Agosto</option>
										<option value="09" <?php if($mes_end == "09"){ $mesName = "Septiembre"; ?>selected="selected"<?php } ?> >Septiembre</option>
										<option value="10" <?php if($mes_end == "10"){ $mesName = "Octubre"; ?>selected="selected"<?php } ?> >Octubre</option>
										<option value="11" <?php if($mes_end == "11"){ $mesName = "Noviembre"; ?>selected="selected"<?php } ?> >Noviembre</option>
										<option value="12" <?php if($mes_end == "12"){ $mesName = "Diciembre"; ?>selected="selected"<?php } ?> >Diciembre</option>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Procesar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Nuevo ROW-->
		<div class="row row-app">
		</div>
		<!-- // END Nuevo ROW-->
		<div class="separator bottom"></div>
		<div class="separator bottom"></div>
	</div>
<!-- // END contenido filtros -->
<!-- Head Widgets -->
	<div class="row border-none row-merge margin-none">
		<div class="col-md-3">
			<div class="padding-bottom-none-phone">
				<!-- Stats Widget -->
				<a href="#" class="widget-stats widget-stats-default widget-stats-4">
					<span class="txt">Score PAC</span>
					<span class="count text-inverse"><?php echo($DatosPAC['pac']); ?></span>
					<span class="glyphicons cup"><i></i></span>
					<div class="clearfix"></div>
					<i class="icon-play-circle"></i>
				</a>
				<!-- // Stats Widget END -->
			</div>
		</div>
		<div class="col-md-3">
			<div class=" padding-bottom-none-phone">
				<!-- Stats Widget -->
				<a href="#" class="widget-stats widget-stats-info widget-stats-2">
					<span class="count"><?php echo($DatosPAC['cat']); ?></span>
					<span class="txt">Mi Categoría</span>
					<span style="display:flex; font-size: 14px;font-weight: bold;"> +<?php echo($DatosPAC['ptos']); ?> Puntos</span>
				</a>
				<!-- // Stats Widget END -->
			</div>
		</div>
		<div class="col-md-3">
			<!-- Stats Widget -->
			<a href="#" class="widget-stats widget-stats-primary widget-stats-5">
				<span class="glyphicons briefcase"><i></i></span>
				<span class="txt">Posición en <?php echo($DatosPAC['region']); ?><span class="text-inverse"><?php echo($DatosPAC['pos_region']); ?> de <?php echo($CantidadRegion['cantidad']); ?></span></span>
				<div class="clearfix"></div>
			</a>
			<!-- // Stats Widget END -->
		</div>
		<div class="col-md-3">
			<!-- Stats Widget -->
			<a href="#" class="widget-stats widget-stats-success widget-stats-5">
				<span class="glyphicons briefcase"><i></i></span>
				<span class="txt">Posición en País<span class="text-inverse"><?php echo($DatosPAC['pos_pais']); ?> de 28</span></span>
				<div class="clearfix"></div>
			</a>
			<!-- // Stats Widget END -->
		</div>
	</div>
<!-- Head Widgets -->
<p class="separator text-center"></p>
<!--<p class="text-center text-success" style="font-size:17px;"><strong>Nivel de Certificación</sotrong></p>
	<div class="row border-none row-merge margin-none">
		<div class="col-md-12">
			<div id="placeholder-AvConcesionario" class="charges-placeholder" style="width: 98%; height: 350px; font-size: 14px; line-height: 1.2em;"></div>
		</div>
	</div>
	<p class="separator text-center"></p>
	<p class="text-center text-success" style="font-size:17px;"><strong>Indicadores</sotrong></p>
	<div class="row border-none row-merge margin-none">
		<div class="col-md-4">
			<span class="text-center">&nbsp; <i class='fa fa-fw icon-pie-graph'></i> Indicador de Ocupación</span>
			<div id="donut-ocupacion" style="width:100%;height:250px;"></div>
		</div>
		<div class="col-md-4">
			<span class="text-center">&nbsp; <i class='fa fa-fw icon-pie-graph'></i> Indicador de Avance Tipo Curso</span>
			<div id="donut-tipocurso" style="width:100%;height:250px;"></div>
		</div>
		<div class="col-md-4">
			<span class="center"> &nbsp;<i class='fa fa-fw icon-pie-graph'></i> Indicador de Cursos Programado Vs Ejecutado</span>
			<div id="donut-cursoprog" style="width:100%;height:250px;"></div>
		</div>
</div>-->

<div class="well">
	<table class="table table-invoice">
		<tbody>
			<tr>
				<td style="width: 30%;">
					<p class="lead">Cupos Vs Inscritos Vs Asistencia</p>
					<div class="row row-merge">
						<div class="col-md-4 bg-gray">
							<div class="innerAll inner-2x text-center">
								<div class="sparkline" sparkHeight="65" data-colors="#5cc7dd,#609450,#cacaca"><?php echo($CantidadCupos['cupos']); ?>,<?php echo($CantidadIncritos['inscritos']); ?>,<?php echo($CantidadAsistencia['cantidad']); ?>,</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="innerAll">
								<ul class="list-unstyled">
									<li class="innerAll half"><i class="fa fa-fw fa-square text-info"></i> <span class="strong"><?php echo($CantidadCupos['cupos']); ?></span> Cupos</li>
									<li class="innerAll half"><i class="fa fa-fw fa-square text-success"></i> <span class="strong"><?php echo($CantidadIncritos['inscritos']); ?></span> Inscritos</li>
									<li class="innerAll half"><i class="fa fa-fw fa-square text-muted"></i> <span class="strong"><?php echo($CantidadAsistencia['cantidad']); ?></span> Asistencia</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="padding-bottom-none-phone">
							<!-- Stats Widget -->
							<a href="#" class="widget-stats widget-stats-primary widget-stats-4">
								<span class="txt">Cantidad de Horas</span>
								<span class="count text-inverse"><?php echo($HorasAsistencia['tiempo']); ?></span>
								<span class="glyphicons stopwatch"><i></i></span>
								<div class="clearfix"></div>
								<i class="icon-play-circle"></i>
							</a>
							<!-- // Stats Widget END -->
						</div>
					</div>

					<div class="col-md-12">
						<div class="padding-bottom-none-phone">
							<!-- Stats Widget -->
							<a href="#" class="widget-stats widget-stats-info widget-stats-4">
								<span class="txt">Cantidad de Sesiones</span>
								<span class="count text-inverse"><?php echo($CantidadSesiones['sesiones']); ?></span>
								<span class="glyphicons calendar"><i></i></span>
								<div class="clearfix"></div>
								<i class="icon-play-circle"></i>
							</a>
							<!-- // Stats Widget END -->
						</div>
					</div>

					<div class="col-md-12">
						<div class="padding-bottom-none-phone">
							<!-- Stats Widget -->
							<a href="#" class="widget-stats widget-stats-default widget-stats-4">
								<span class="txt">Personal sin Cursos</span>
								<span class="count text-inverse"><?php echo($GenteSinCursos['cantidad']); ?></span>
								<span class="glyphicons group"><i></i></span>
								<div class="clearfix"></div>
								<i class="icon-play-circle"></i>
							</a>
							<!-- // Stats Widget END -->
						</div>
					</div>
				</td>
				<td class="right">
					<p class="lead">Avance hasta <?php echo($mesName); ?> / <?php echo($year_end); ?></p>
					<!-- // Table -->
					<table class="table table-condensed table-vertical-center table-thead-simple">
						<thead>
							<tr>
								<th class="center">Trayectoria</th>
								<th class="center"># Cursos</th>
								<th class="center"># Usuarios</th>
								<th class="center">Promedio</th>
								<th class="center">% Avance</th>
								<th class="center">Bronce</th>
								<th class="center">Plata</th>
								<th class="center">Oro</th>
							</tr>
						</thead>
						<tbody id="cuerpo_tabla">
							<?php foreach ($CargosDatosListos as $key => $Data_InfoCargosAna) { 
								//print_r($Data_InfoCargosAna);
								$PorAvanceBronce = "0";
								$PorAvancePlata = "0";
								$PorAvanceOro = "0";
								$PorAvanceGral = "0";
								$promedio = "0";
								if(isset($Data_InfoCargosAna['CantCursosPasoGral'])){
									$PorAvanceGral = number_format($Data_InfoCargosAna['CantCursosPasoGral']/$Data_InfoCargosAna['CantCursosXPersonas']*100,2);
									foreach ($Data_InfoCargosAna['CantCursosPasoXNivel'] as $key => $Datos_Nivel) {
										if($Datos_Nivel['level_id']=="1"){
											$PorAvanceOro = number_format($Datos_Nivel['CantidadCursosNivel']/$Data_InfoCargosAna['CantCursosXPersonas']*100,2);
										}elseif($Datos_Nivel['level_id']=="2"){
											$PorAvancePlata = number_format($Datos_Nivel['CantidadCursosNivel']/$Data_InfoCargosAna['CantCursosXPersonas']*100,2);
										}else{
											$PorAvanceBronce = number_format($Datos_Nivel['CantidadCursosNivel']/$Data_InfoCargosAna['CantCursosXPersonas']*100,2);
										}
									}
								}
								if(isset($Data_InfoCargosAna['Promedio'])){
									$promedio = number_format($Data_InfoCargosAna['Promedio'],2);
								}
								?>
								<tr class="selectable">
									<td class="left" style="padding: 2px; font-size: 80%;"><?php echo(strtoupper($Data_InfoCargosAna['charge'])); ?></td>
									<td class="center important" style="padding: 2px; font-size: 80%;"><?php if(isset($Data_InfoCargosAna['CantCursosVer'])){ echo($Data_InfoCargosAna['CantCursosVer']); }else{ echo("0"); } ?></td>
									<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Data_InfoCargosAna['cantidadPersonas']); ?></td>
									<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($promedio); ?></td>
									<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($PorAvanceGral); ?> %</td>
									<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-danger"><?php echo($PorAvanceBronce); ?> %</span></td>
									<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-default"><?php echo($PorAvancePlata); ?> %</span></td>
									<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary"><?php echo($PorAvanceOro); ?> %</span></td>
								</tr>
							<?php } ?>
						</tbody>
					</table><br>
						<div class="row border-none row-merge margin-none">
							<div class="col-md-6">
								<span class="text-center">&nbsp; <i class='fa fa-fw icon-pie-graph'></i> Horas Tipo de Formación</span>
								<div id="donut-horas" style="width:100%;height:250px;"></div>
							</div>
							<div class="col-md-6">
								<span class="text-center">&nbsp; <i class='fa fa-fw icon-pie-graph'></i> Sesiones Tipo de Formación</span>
								<div id="donut-sesiones" style="width:100%;height:250px;"></div>
							</div>
						</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>










<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_dashboard.js"></script>
</body>
</html>