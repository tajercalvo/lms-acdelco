<?php 
include('src/seguridad.php');
if(!isset($_GET['id'])){
	$_GET['id'] = $_SESSION['idUsuario'];
}
$idUsuario = $_GET['id'];
include('controllers/descargar_trayectoria.php');
?>
<?php
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=TrayectoriaUsuario_".$datosUsuario['identification'].".xls");
?>
<table style="width: 100%;" border="1">
	<tr>
		<td colspan="3" align="center" style="background-color: #0047A8;color: white;"><strong><?php echo utf8_decode($datosUsuario['first_name'].' '.$datosUsuario['last_name']); ?></strong><br><br></td>
	<tr>
	<tr>
		<td align="center">SALIDAS EN EL A&Ntilde;O <?php echo($anoCurso); ?>:</td>
		<td align="left"><strong><?php echo($Salidas); ?></strong></td>
		<td style="width: 50%;" valign="top" align="center" rowspan="3">
			<strong>TRAYECTORIAS</strong><br>
			<?php foreach ($charge_user as $key => $Data_Charges) { ?>
				<?php echo($Data_Charges['charge']); ?><br>
			<?php } ?>
		</td>
	</tr>
		<td align="center">HORAS EN EL A&Ntilde;O <?php echo($anoCurso); ?>:</td>
		<td align="left"><strong><?php echo($Horas); ?></strong></td>
	</tr>
	<tr>
		<td style="width: 50%;" valign="top" align="center" colspan="2">
			<strong>PERFIL</strong><br>
			<?php foreach ($rol_user as $key => $Data_Roles) { ?>
				<?php echo($Data_Roles['rol']); ?><br>
			<?php } ?>
		</td>
	<tr>
</table>
<br><br>
<table style="width: 100%;" border="1">
	<!-- Table heading -->
	<thead>
		<tr>
			<th style="width: 10%;background-color: #0047A8;color: white;" rowspan="2"><strong>C&Oacute;DIGO</strong></th>
			<th rowspan="2" style="background-color: #0047A8;color: white;"><strong>CURSO</strong></th>
			<th rowspan="2" style="background-color: #0047A8;color: white;"><strong>TIPO</strong></th>
			<?php foreach ($charge_user as $key => $Data_Charges) { ?>
				<th colspan="4" style="background-color: #0047A8;color: white;"><strong><?php echo(utf8_decode($Data_Charges['charge'])); ?></strong></th>
			<?php } ?>
		</tr>
		<tr>
			<?php foreach ($charge_user as $key => $Data_Charges) { ?>
				<th style="background-color: #0047A8;color: white;"><strong>Trayectoria</strong></th>
				<th style="background-color: #0047A8;color: white;"><strong>Aprobado</strong></th>
				<th style="background-color: #0047A8;color: white;"><strong>Resultado</strong></th>
				<th style="background-color: #0047A8;color: white;"><strong>Nivel</strong></th>
			<?php } ?>
		</tr>
	</thead>
	<!-- // Table heading END -->
	<!-- Table body -->
	<tbody>
		<?php foreach ($cursos_usuario as $key => $data_course) { ?>
			<tr>
				<td style="width: 10%;"><?php echo utf8_decode($data_course['newcode']); ?></td>
				<td><?php echo utf8_decode($data_course['course']); ?></td>
				<td><?php echo $data_course['type']; ?></td>
				<?php foreach ($charge_user as $key => $Data_Charges) { 
						$Cargo_usr = $Data_Charges['charge_id'];
						$Curso_usr = $data_course['course_id'];
						$Necesario = "";
						$Aprobado = "";
						$Resultado = "";
						$Nivel = "";
						foreach ($cargos_cursos as $key => $Recr_CarCur) {
							if( $Recr_CarCur['charge_id']==$Cargo_usr && $Recr_CarCur['course_id']==$Curso_usr ){
								$Necesario = "SI";
								if($Recr_CarCur['level_id']==1){
									$Nivel = "Especialista";
								}elseif ($Recr_CarCur['level_id']==2) {
									$Nivel = "Experto";
								}elseif ($Recr_CarCur['level_id']==3) {
									$Nivel = "Técnico";
								}elseif ($Recr_CarCur['level_id']==4) {
									$Nivel = "Principiante";
								}else{
									$Nivel = "Comun";
								}
							}
						}
						foreach ($resultados_usuario as $key => $Recr_ResCarCur) {
							if( $Recr_ResCarCur['charge_id']==$Cargo_usr && $Recr_ResCarCur['course_id']==$Curso_usr ){
								if(Round($Recr_ResCarCur['score'],0)>79){
									$Aprobado = "SI";
								}else{
									$Aprobado = "NO";
								}
								$Resultado = Round($Recr_ResCarCur['score'],0);
							}
						}
					?>
					<th><?php echo $Necesario; ?></th>
					<th><?php echo $Aprobado; ?></th>
					<th><?php echo $Resultado; ?></th>
					<th><?php echo $Nivel; ?></th>
				<?php } ?>
			</tr>
		<?php } ?>

			<tr>
				<td colspan="3" style="background-color: #0047A8;color: white;"><strong>RESUMEN DE AVANCE<strong></td>
				<?php foreach ($charge_user as $key => $Data_Charges) { 
						$CantidadNecesarios = 0; 
						$CantidadPaso = 0;
						$CantidadNotas = 0;
						$SumatoriaNotas = 0;
						?>
					<?php foreach ($cursos_usuario as $key => $data_course) { 
						$Resultado = 0;
						
						$Cargo_usr = $Data_Charges['charge_id'];
						$Curso_usr = $data_course['course_id'];
						$Necesario = "";
						$Aprobado = "";
						$Resultado = "";
						$Nivel = "";
						foreach ($cargos_cursos as $key => $Recr_CarCur) {
							if( $Recr_CarCur['charge_id']==$Cargo_usr && $Recr_CarCur['course_id']==$Curso_usr ){
								$Necesario = "SI";
								$CantidadNecesarios++;
							}
						}
						foreach ($resultados_usuario as $key => $Recr_ResCarCur) {
							if( $Recr_ResCarCur['charge_id']==$Cargo_usr && $Recr_ResCarCur['course_id']==$Curso_usr ){
								if(Round($Recr_ResCarCur['score'],0)>79){
									$Aprobado = "SI";
									$CantidadPaso++;
								}else{
									$Aprobado = "NO";
								}
								$CantidadNotas++;
								$Resultado = Round($Recr_ResCarCur['score'],0);
								$SumatoriaNotas = $SumatoriaNotas + $Resultado;
							}
						}


					} ?>
					<td style="background-color: #0047A8;color: white;"><strong><?php echo $CantidadNecesarios; ?></strong></td>
					<td style="background-color: #0047A8;color: white;"><strong><?php echo $CantidadPaso; ?></strong></td>
					<td style="background-color: #0047A8;color: white;"><strong>Avance <?php if($CantidadNecesarios>0) { echo(number_format(($CantidadPaso/$CantidadNecesarios)*100,2)); } echo("0"); ?> %</strong></td>
					<td style="background-color: #0047A8;color: white;"><strong>Promedio <?php if($CantidadNotas>0){ echo(number_format($SumatoriaNotas/$CantidadNotas,0)); }else{ echo("0"); } ?></strong></td>
				<?php } ?>
			</tr>
	</tbody>
	<!-- Table body -->
</table>