<?php include('src/seguridad.php'); ?>
<?php include('controllers/programaciones.php');
$location = 'education';
$qtip = 'qtip';
$script_select2v4 = 'select2v4';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons calendar"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/programaciones.php">Configuración Programación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Programación </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if (isset($_GET['back']) && $_GET['back'] == "inicio") { ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare"><i></i>Regresar</a>
											<h5>
											<?php } else { ?>
												<h5><a href="programaciones.php" class="glyphicons no-js unshare"><i></i>Regresar</a>
													<h5>
													<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
							<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">
							<form id="formulario_data" method="post" autocomplete="off">
								<?php if (isset($registroConfiguracion['start_date'])) { ?>
									<div>
										<input type="hidden" name="start_date_day_c" value="<?php echo (substr($registroConfiguracion['start_date'], 0, 10)); ?>">
										<input type="hidden" name="start_date_time_c" value="<?php echo (substr($registroConfiguracion['start_date'], 11, 8)); ?>">
										<input type="hidden" name="end_date_day_c" value="<?php echo (substr($registroConfiguracion['end_date'], 0, 10)); ?>">
										<input type="hidden" name="end_date_time_c" value="<?php echo (substr($registroConfiguracion['end_date'], 11, 8)); ?>">
										<input type="hidden" name="max_inscription_date_c" value="<?php echo (substr($registroConfiguracion['max_inscription_date'], 0, 10)); ?>">
										<input type="hidden" name="living_id_c" value="<?php echo ($registroConfiguracion['living_id']); ?>">
										<input type="hidden" name="min_size_c" value="<?php echo ($registroConfiguracion['min_size']); ?>">
										<input type="hidden" name="max_size_c" value="<?php echo ($registroConfiguracion['max_size']); ?>">
										<input type="hidden" name="waiting_size_c" value="<?php echo ($registroConfiguracion['waiting_size']); ?>">
										<input type="hidden" name="teacher_id_c" value="<?php echo ($registroConfiguracion['teacher_id']); ?>">
									</div>
								<?php } ?>
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="course_id" style="padding-top:8px;">Curso:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="course_id" name="course_id" onchange="consulta_Modulos();">
													<?php
													$course_id = 0;
													foreach ($listadosCursos as $key => $Data_listados) {
														if ($course_id == 0) {
															$course_id = $Data_listados['course_id'];
														}
													?>
														<option value="<?php echo ($Data_listados['course_id']); ?>" <?php if (isset($registroConfiguracion['course_id']) && $registroConfiguracion['course_id'] == $Data_listados['course_id']) { ?>selected="selected" <?php } ?>><?php echo ($Data_listados['newcode'] . ' : ' . $Data_listados['course']); ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="module_id" style="padding-top:8px;">Módulo:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="module_id" name="module_id" onchange="Consulta_DataCambio('Curso');">
													<?php
													$course_id = 0;
													foreach ($modulos as $key => $val) { ?>
														<option value="<?php echo ($val['module_id']); ?>" <?php echo $registroConfiguracion['module_id'] == $val['module_id'] ? 'selected' : ''; ?>><?php echo $val['module']; ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
										<!-- // Column END -->
										<input type="hidden" id="Cambio_Prog" name="Cambio_Prog">
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicia:</label>
											<div class="col-md-7 input-group date">
												<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo substr($registroConfiguracion['start_date'], 0, 10); ?>" <?php } ?> onchange="CambioProg('Fecha Inicia');" />
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="start_date_time" style="padding-top:8px;">Hora Inicio:</label>
											<div class="col-md-7 input-group bootstrap-timepicker">
												<input id="start_date_time" name="start_date_time" type="text" class="form-control" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo substr($registroConfiguracion['start_date'], 11, 6); ?>" <?php } ?> onchange="CambioProg('Hora Inicio');" />
												<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="end_date_day" style="padding-top:8px;">Fecha Finaliza:</label>
											<div class="col-md-7 input-group date">
												<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo substr($registroConfiguracion['end_date'], 0, 10); ?>" <?php } ?> onchange="CambioProg('Fecha Termina');" />
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="end_date_time" style="padding-top:8px;">Hora Fin:</label>
											<div class="col-md-7 input-group bootstrap-timepicker">
												<input id="end_date_time" name="end_date_time" type="text" class="form-control" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo substr($registroConfiguracion['end_date'], 11, 6); ?>" <?php } ?> onchange="CambioProg('Hora Final');" />
												<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="max_inscription_date" style="padding-top:8px;">Fecha Máxima inscripción:</label>
											<div class="col-md-7 input-group date">
												<input class="form-control" type="text" id="max_inscription_date" name="max_inscription_date" placeholder="Finaliza el" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo substr($registroConfiguracion['max_inscription_date'], 0, 10); ?>" <?php } ?> />
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="living_id" style="padding-top:8px;">Lugar:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="living_id" name="living_id" onchange="Consulta_DataCambio('Lugar');">
													<?php
													$living_id = 0;
													foreach ($listadosSalones as $key => $Data_listados) {
														if ($living_id == 0) {
															if (isset($listadosSalones['living_id'])) {
																$living_id = $listadosSalones['living_id'];
															}
														}
													?>
														<option value="<?php echo ($Data_listados['living_id']); ?>" <?php if (isset($registroConfiguracion['living_id']) && $registroConfiguracion['living_id'] == $Data_listados['living_id']) { ?>selected="selected" <?php } ?>><?php echo ($Data_listados['living'] . ' : ' . $Data_listados['address']); ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="max_size" style="padding-top:8px;">Participantes Máx:</label>
											<div class="col-md-7">
												<input class="form-control" id="max_size" name="max_size" type="text" placeholder="Cantidad máxima de participantes" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo $registroConfiguracion['max_size']; ?>" <?php } else { ?> value="0" <?php } ?> onchange="Consulta_CambioCupos('Cupos');" />
												<input type="hidden" value="0" name="city_id" id="city_id" />
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="min_size" style="padding-top:8px;">Participantes Mín:</label>
											<div class="col-md-7">
												<input class="form-control" id="min_size" name="min_size" type="text" placeholder="Cantidad mínima de participantes" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo $registroConfiguracion['min_size']; ?>" <?php } else { ?> value="0" <?php } ?> onchange="Consulta_CambioCupos('Cupos');" />
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="waiting_size" style="padding-top:8px;">Participantes Esp:</label>
											<div class="col-md-7"><input class="form-control" id="waiting_size" name="waiting_size" type="text" placeholder="Participantes en espera" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo $registroConfiguracion['waiting_size']; ?>" <?php } ?> /></div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<div class="col-md-2"></div>
										<div class="col-md-10">
											<div class="checkbox">
												<label class="checkbox-custom">
													<input type="checkbox" name="notificar_email" id="notificar_email">
													<i class="fa fa-fw fa-square-o checked"></i> Notificar por e-Mail?
												</label>
											</div>
										</div>
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="teacher_id" style="padding-top:8px;">Profesor:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="teacher_id" name="teacher_id" onchange="CambioProg('Profesor');">
													<?php foreach ($listadosProfesores as $key => $Data_listados) { ?>
														<option value="<?php echo ($Data_listados['user_id']); ?>" <?php if (isset($registroConfiguracion['teacher_id']) && $registroConfiguracion['teacher_id'] == $Data_listados['user_id']) { ?>selected="selected" <?php } ?>><?php echo ($Data_listados['first_name'] . ' ' . $Data_listados['last_name']); ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar')) { ?>
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
												<div class="col-md-7">
													<select style="width: 100%;" id="status_id" name="status_id">
														<option value="1" <?php if ($registroConfiguracion['status_id'] == "1") { ?>selected="selected" <?php } ?>>Activo</option>
														<option value="2" <?php if ($registroConfiguracion['status_id'] == "2") { ?>selected="selected" <?php } ?>>Inactivo</option>
													</select>
												</div>
											</div>
											<!-- // Group END -->
										<?php } ?>
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-10">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="teacher_id_aux" style="padding-top:8px;">Profesores auxiliares:</label>
											<div class="col-md-10">
												<select multiple id="teacher_id_aux" name="teacher_id_aux[]" class="js-data-example-ajax" style="width: 100%;">
													<?php foreach ($listadosProfesoresAux as $key => $Data_listados) { ?>
														<option value="<?php echo ($Data_listados['user_id']); ?>" selected="selected"><?php echo ($Data_listados['first_name'] . ' ' . $Data_listados['last_name']); ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
									<div class="ajax-loading hide" id="loading_programacion">
										<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
									</div>
									<!-- Column -->
									<div class="col-md-12" id="detProgramacion"></div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Form actions -->
								<div class="form-actions">
									<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'nuevo')) { ?>
										<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
										<input type="hidden" id="opcn" name="opcn" value="crear">
									<?php } elseif (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar')) { ?>
										<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
										<input type="hidden" id="opcn" name="opcn" value="editar">
										<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
									<?php } ?>
									<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
								</div>
								<!-- // Form actions END -->
							</form>
						</div>
					</div>
					<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/programaciones.js?version=<?php echo md5(time()); ?>"></script>
</body>

</html>