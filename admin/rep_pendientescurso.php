<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_pendientescurso.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons adjust_alt"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_pendientescurso.php">Reporte de Pendientes Vs Curso</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Pendientes Vs Curso </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de los usuarios pendientes por cada curso (NOTA: Recuerde que para que salga información pendiente, el curso debe estar incluído dentro de una trayectoria académica).</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<h5><a href="rep_pendientescurso_excel.php?course_id=<?php echo($_POST['course_id']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<form action="rep_pendientescurso.php" method="post">
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="course_id" style="padding-top:8px;">Curso:</label>
					<div class="col-md-10">
						<select style="width: 100%;" id="course_id" name="course_id">
							<?php foreach ($datosCursos as $key => $Data_Cursos) { ?>
								<option value="<?php echo($Data_Cursos['course_id']); ?>" <?php if(isset($_POST['course_id']) && $_POST['course_id']==$Data_Cursos['course_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Cursos['type'].': '.$Data_Cursos['newcode'].' '.$Data_Cursos['course']); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información: </h4>  Usuarios: <strong class='text-primary'><?php echo($cantidad_datos); ?></strong>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<!-- Table elements-->
		<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
			<!-- Table heading -->
			<thead>
				<tr>
					<th data-hide="phone,tablet">CÓDIGO</th>
	                <th data-hide="phone,tablet">CURSO</th>
	                <th data-hide="phone,tablet">TIPO</th>
	                <th data-hide="phone,tablet">CONCESIONARIO</th>
	                <th data-hide="phone,tablet">ALUMNO</th>
	                <th data-hide="phone,tablet">IDENTIFICACIÓN</th>
	                <th data-hide="phone,tablet">CARGOS</th>
				</tr>
			</thead>
			<!-- // Table heading END -->
			<tbody>
				<?php 
				if($cantidad_datos > 0){ 
						foreach ($datosCursos_all as $iID => $data) { 
							?>
					<tr>
						<td><?php echo($data['newcode']); ?></td>
						<td><?php echo($data['course']); ?></td>
						<td><?php echo($data['type']); ?></td>
						<td><?php echo($data['dealer']); ?><br><?php echo($data['headquarter']); ?></td>
						<td><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
						<td><?php echo($data['identification']); ?></td>
						<td>
							<? foreach ($data['cargos'] as $iID => $data_c) { 
								echo "<i class='fa fa-fw icon-identification'></i>".$data_c['charge'].'<br>';
							} ?>
						</td>
					</tr>
				<?php } } ?>
			</tbody>
		</table>
		<!-- // Table elements END -->
		<!-- Total elements-->
		<div class="form-inline separator bottom small">
			Total de usuarios: <strong class='text-primary'><?php echo($cantidad_datos); ?></strong>
		</div>
		<!-- // Total elements END -->
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_pendientescurso.js"></script>
</body>
</html>