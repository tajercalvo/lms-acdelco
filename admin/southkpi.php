<?php include('src/seguridad.php'); ?>
<?php include('controllers/sentinel_indicadores.php');
$location = 'reporting';
$locData = true;
//$Asist = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons charts"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/southkpi.php">South America KPIS</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Indicadores Regionales - KPI´s </h2>
							<br><br><!--<div class="clearfix"> &nbsp;[<?php echo $fecha_inicial.' - '.$fecha_final; ?>] <?php echo $diferencia_mes; ?> Meses </div>-->
							<button type="button" data-toggle="print" class="btn btn-default print hidden-print"><i class="fa fa-fw fa-print"></i> Imprimir</button>
						</div>
					<!-- // END heading -->
<!-- contenido filtros -->
	<div class="widget widget-heading-simple widget-body-gray hidden-print">
		<div class="widget-body">
			<div class="row">
				<div class="row">
					<form action="southkpi.php" method="post">
						<div class="col-md-10">
							<!-- Group -->
							<div class="form-group">
								<div class="col-md-4">
									<label class="control-label" for="dealer_id">Año:</label>
									<select style="width: 100%;" id="ano_id_end" name="ano_id_end" >
										<?php for ($y=date("Y");$y>=2017;$y--){ ?>
											<option value="<?php echo $y; ?>" <?php if($y==$year_end){ ?>selected="selected"<?php } ?>><?php echo $y; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-8">
									La información disponible a continuación se encuentra desplegada por años, el sistema tomará mes a mes los datos procesados y los presentará conforme avance la disponibilidad de información. El año por defecto es el que se encuentra en curso, para observar otro año que contenga información disponible, basta con seleccionarlo en el desplegable y dar clic en el botón Consultar.
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<div class="col-md-2 text-center">
							<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Nuevo ROW-->
		<div class="row row-app">
		</div>
		<!-- // END Nuevo ROW-->
		<div class="separator bottom"></div>
		<div class="separator bottom"></div>
	</div>
<!-- // END contenido filtros -->

<!-- PRIMER INDICADOR COMPLETO -->
	<div class="well">
		<table class="table table-invoice">
			<tbody>
				<tr>
					<td style="width: 15%;">
						<p class="lead">KPI´s por Mes</p>
							<h4>A tener en cuenta:</h4>
							<address class="margin-none" style="text-align: justify;">
								<strong>Año: </strong><?php if(isset($_POST['ano_id_end'])){ echo($_POST['ano_id_end']); }else{ echo(date("Y")); } ?>
							</address>
					</td>
					<td class="right">
						<p class="lead">SALES AND AFTERSALES TRAINING</p>
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple">
							<thead>
								<tr>
									<th class="center" style="width: 20%;">KPI</th>
									<th class="center">JUL-17</th>
									<th class="center">AGO-17</th>
									<th class="center">SEP-17</th>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># students</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">1220</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">906</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">840</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># F2F training available</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">33</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">42</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">40</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># WBTs available</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">62</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">62</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">62</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># classes</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">33</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">42</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">40</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># participants (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">352</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">758</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">709</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% participants vs objective (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># completed WBTs</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">320</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">527</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">588</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% WBTs completed vs objective</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Satisfation index (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">85%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">97%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">96%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Satisfation index (WBT)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">90%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">94%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">96%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% Approved (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">82%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">96%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">95%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% Approved (WBT)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">98%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">98%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">99%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Average - Final Test Grade (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Average - Final Test Grade (WBT)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Evolution Index (F2F) p.p.</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Evolution Index (WBT) p.p.</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
							</tbody>
						</table>
						<!-- // Table END -->
					</td>
				</tr>
				<tr>
					<td style="width: 15%;"></td>
					<td class="right">
						<p class="lead">SALES TRAINING</p>
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple">
							<thead>
								<tr>
									<th class="center" style="width: 20%;">KPI</th>
									<th class="center">JUL-17</th>
									<th class="center">AGO-17</th>
									<th class="center">SEP-17</th>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># students</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">793</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">740</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">562</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># F2F training available</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">17</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">41</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">30</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># WBTs available</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">25</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">25</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">25</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># classes</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">17</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">41</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">30</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># participants (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">144</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">712</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">542</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% participants vs objective (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;"># completed WBTs</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">210</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">369</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">446</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% WBTs completed vs objective</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Satisfation index (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">85%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">96%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">96%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Satisfation index (WBT)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">90%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">94%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">96%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% Approved (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">83%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">93%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">94%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">% Approved (WBT)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">99%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">99%</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">99%</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Average - Final Test Grade (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Average - Final Test Grade (WBT)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Evolution Index (F2F)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
								<tr>
									<td class="left important" style="padding: 2px; font-size: 80%;">Evolution Index (WBT)</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
								</tr>
							</tbody>
						</table>
						<!-- // Table END -->
					</td>
				</tr>
				<tr>
					<td style="width: 15%;"></td>
					<td class="right">
						<p class="lead">AFTERSALES TRAINING</p>
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple">
							<thead>
								<tr>
									<th class="center" style="width: 20%;">KPI</th>
									<th class="center">JUL-17</th>
									<th class="center">AGO-17</th>
									<th class="center">SEP-17</th>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;"># students</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">436</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">57</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">175</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;"># F2F training available</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">33</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">30</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">39</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;"># WBTs available</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">30</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">30</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">30</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;"># classes</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">33</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">30</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">39</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;"># participants (F2F)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">214</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">49</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">167</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">% participants vs objective (F2F)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;"># completed WBTs</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">275</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">439</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">406</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">% WBTs completed vs objective</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">Satisfation index (F2F)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">85%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">96%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">95%</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">Satisfation index (WBT)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">90%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">93%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">94%</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">% Approved (F2F)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">82%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">98%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">95%</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">% Approved (WBT)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">97%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">98%</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">99%</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">Average - Final Test Grade (F2F)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">Average - Final Test Grade (WBT)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">Evolution Index (F2F)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									</tr>
									<tr>
										<td class="left important" style="padding: 2px; font-size: 80%;">Evolution Index (WBT)</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
										<td class="center important" style="padding: 2px; font-size: 80%;">0</td>
									</tr>
							</tbody>
						</table>
						<!-- // Table END -->
					</td>
				</tr>
			</tbody>
		</table>
	</div>
<!-- PRIMER INDICADOR COMPLETO-->



<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/sentinel_indicadores.js"></script>
</body>
</html>
