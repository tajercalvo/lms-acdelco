<?php
include('src/seguridad.php');
include_once('../config/database.php');
include_once('../config/config.php');
if(isset($_GET['fecha'])){
	$fechaInicial = $_GET['fecha'];
}else{
	$month = date('m');
    $year = date('Y');
	$fechaInicial = $year."-".$month."-01";
}
$query = "SELECT d.dealer_id, d.dealer, h.headquarter, a.area, z.zone, u.identification, u.first_name, u.last_name, c.charge, u.date_startgm, u.status_id as estadoestu, u.date_session, b.*
FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, 
(SELECT user_id, charge_id, count(*) FROM ludus_charges_users GROUP BY user_id) cu, ludus_charges c,
(SELECT m.user_id, c.newcode, c.course, mo.duration_time, c.type, s.start_date, s.end_date, l.living, u.first_name as first_prof, 
u.last_name as last_prof, u.identification as iden_prof, i.status_id as asistencia, m.score, m.score_waybill, m.score_review, m.score_evaluation, sp.specialty, 
su.supplier, MONTH(s.start_date) as mes, s.schedule_id, m.module_result_usr_id
FROM ludus_invitation i, ludus_schedule s, ludus_modules_results_usr m, ludus_courses c, ludus_users u, ludus_modules mo, 
ludus_livings l, ludus_specialties sp, ludus_supplier su
WHERE i.schedule_id = s.schedule_id AND i.invitation_id = m.invitation_id AND s.course_id = c.course_id AND 
s.teacher_id = u.user_id AND c.course_id = mo.course_id AND s.living_id = l.living_id
AND c.specialty_id = sp.specialty_id AND c.supplier_id = su.supplier_id AND s.start_date > '$fechaInicial') b
WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND 
a.zone_id = z.zone_id AND u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND u.user_id = b.user_id";
//echo($query);
$DataBase_Acciones = new Database();
$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
$cantidad_datos = count($Rows_config);
/*header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_Felipe.csv');*/
?>
<table border="1">
	<tr>
		<td>CONCESIONARIO</td>
		<td>SEDE</td>
		<td>CIUDAD</td>
		<td>ZONA</td>
		<td>IDENTIFICACIÓN</td>
		<td>ALUMNO</td>
		<td>CARGO</td>
		<td>CÓDIGO</td>
		<td>CURSO</td>
		<td>HORAS CURSO</td>
		<td>DIAS CURSO</td>
		<td>TIPO DE CURSO</td>
		<td>FECHA</td>
		<td>FINAL</td>
		<td>LUGAR</td>
		<td>PROFESOR</td>
		<td>ASISTENCIA</td>
		<td>PUNTAJE</td>
		<td>SATISFACCIÓN</td>
		<td>HOJARUTA</td>
		<td>EVALUACION</td>
		<td>RESULTADO</td>
		<td>MINIMOS</td>
		<td>MAXIMOS</td>
		<td>INSCRITOS</td>
		<td>EXCUSA</td>
		<td>COBRO SI/NO</td>
		<td>ESPECIALIDAD</td>
		<td>TARIFA</td>
		<td>PROVEEDOR</td>
		<td>TOTAL</td>
		<td>MES</td>
		<td>PESO % ESTUDIANTE</td>
		<td>Satisfacción general con GMAcademy</td>
		<td>Recomendaría al Instructor / Facilitador</td>
		<td>Contenido del curso</td>
		<td>Logística: Aula y/o Alimentación</td>
		<td>Material de apoyo</td>
		<td>Aplicación del curso en su Ámbito laboral</td>
		<td>¿La metodología fue la adecuada?</td>
		<td>¿Disfrutó el curso?</td>
		<td>¿Recomienda este curso a sus colegas?</td>
		<td>¿Qué sugerencias o recomendación frente al curso tomado considera importante para generar valor agregado?</td>
		<td>Fecha de Ingreso a la marca</td>
		<td>Fecha de Retiro</td>
		<td>Estado Alumno</td>
	</tr>
	<?php if($cantidad_datos > 0){ 
        foreach ($Rows_config as $iID => $data) { 
        	/*Confirma si hay maximos y mínimos*/
        	$d = $data['dealer_id'];
        	$s = $data['schedule_id'];
        	$m = $data['module_result_usr_id'];
        	$query_new = "SELECT * FROM ludus_dealer_schedule WHERE schedule_id = $s AND dealer_id = $d ";
        	$Rows_MAX = $DataBase_Acciones->SQL_SelectRows($query_new);
			$cantidad_MAX = count($Rows_MAX);
        	/*Confirma si hay maximos y mínimos*/
        	/*Confirma si hay encuesta de satisfacción*/
        	$query_enc = "SELECT q.question, d.result
        	FROM ludus_reviews_answer r, ludus_reviews_answer_detail d, ludus_questions q
        	WHERE r.review_id = 2 AND r.module_result_usr_id = $m AND r.reviews_answer_id = d.reviews_answer_id
        	AND d.question_id = q.question_id
        	ORDER BY d.question_id";
        	$Rows_ENC = $DataBase_Acciones->SQL_SelectMultipleRows($query_enc);
			$cantidad_ENC = count($Rows_ENC);
        	/*Confirma si hay encuesta de satisfacción*/
        	?>
	<tr>
		<td><?php echo($data['dealer']); ?></td>
		<td><?php echo($data['headquarter']); ?></td>
		<td><?php echo($data['area']); ?></td>
		<td><?php echo($data['zone']); ?></td>
		<td><?php echo($data['identification']); ?></td>
		<td><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
		<td><?php echo($data['charge']); ?></td>
		<td><?php echo($data['newcode']); ?></td>
		<td><?php echo($data['course']); ?></td>
		<td><?php echo($data['duration_time']); ?></td>
		<td><?php echo($data['duration_time']/8); ?></td>
		<td><?php echo($data['type']); ?></td>
		<td><?php echo($data['start_date']); ?></td>
		<td><?php echo($data['end_date']); ?></td>
		<td><?php echo($data['living']); ?></td>
		<td><?php echo($data['first_prof'].' '.$data['last_prof']); ?></td>
		<td><?php if($data['asistencia']=="1"){ echo("Invitado"); }elseif($data['asistencia']=="2"){ echo("Asistió"); }else{ echo("No Asistió"); }?></td>
		<td><?php echo($data['score']); ?></td>
		<td><?php echo($data['score_review']); ?></td>
		<td><?php echo($data['score_waybill']); ?></td>
		<td><?php echo($data['score_evaluation']); ?></td>
		<td><?php if($data['score']>79){ echo("Superado"); }else{ echo("Fallido"); } ?></td>
		<td><?php if($cantidad_MAX>0){ echo($Rows_MAX['min_size']); } ?></td>
		<td><?php if($cantidad_MAX>0){ echo($Rows_MAX['max_size']); } ?></td>
		<td><?php if($cantidad_MAX>0){ echo($Rows_MAX['inscriptions']); } ?></td>
		<td></td>
		<td></td>
		<td><?php echo($data['specialty']); ?></td>
		<td></td>
		<td><?php echo($data['supplier']); ?></td>
		<td></td>
		<td><?php echo($data['mes']); ?></td>
		<td></td>
		<?php if($cantidad_ENC>0){ 
				foreach ($Rows_ENC as $iIDD => $dataENC) { ?>
			<td><?php echo($dataENC['result']);?></td>
		<?php } }else{ ?>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
		<?php }?>

		<td><?php echo($data['date_startgm']); ?></td>
		<td><?php if($data['estadoestu']=="2"){ echo($data['date_session']); } ?></td>
		<td><?php if($data['estadoestu']=="1"){ echo("Activo"); }else{ echo("Inactivo"); } ?></td>
	</tr>
	<?php } } ?>
</table>
<?php


    /*echo("CONCESIONARIO;SEDE;NOMBRES;APELLIDOS;IDENTIFICACION;MODULO;CODIGO;CURSO;CODIGO;PUNTAJE\n");
    if($cantidad_datos > 0){ 
        foreach ($Rows_config as $iID => $data) {
            echo(utf8_decode($data['dealer'].';'.$data['headquarter'].';'.$data['first_name'].';'.$data['last_name'].';'.$data['identification'].';'.$data['module'].';'.$data['newcode'].';'.$data['course'].';'.$data['newcode'].';'.$data['score'])."\n");
        }
    }*/

/*
SELECT d.dealer, h.headquarter, a.area, z.zone, u.identification, u.first_name, u.last_name, c.charge FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, (SELECT user_id, charge_id, count(*) FROM ludus_charges_users GROUP BY user_id) cu, ludus_charges c WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND u.user_id = cu.user_id AND cu.charge_id = c.charge_id

SELECT m.user_id, c.newcode, c.course, mo.duration_time, c.type, s.start_date, s.end_date, l.living, u.first_name, u.last_name, u.identification, i.status_id, m.score, m.score_waybill, m.score_review, m.score_evaluation, sp.specialty, su.supplier
FROM ludus_invitation i, ludus_schedule s, ludus_modules_results_usr m, ludus_courses c, ludus_users u, ludus_modules mo, ludus_livings l, ludus_specialties sp, ludus_supplier su
WHERE i.schedule_id = s.schedule_id AND i.invitation_id = m.invitation_id AND s.course_id = c.course_id AND s.teacher_id = u.user_id AND c.course_id = mo.course_id AND s.living_id = l.living_id
AND c.specialty_id = sp.specialty_id AND c.supplier_id = su.supplier_id
*/