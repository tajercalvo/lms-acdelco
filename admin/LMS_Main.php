<?php 
include('controllers/LMS.php');
@session_start();
?>
	<!--
		****************************************************************************************
		*
		* Confidencial: Propiedad de Ludus Learning Systems
		*
		*  (C) 2015 Todos los derechos reservados
		*  Ultima Revisión Sept 2015 DL
		*  Description: Procesa los llamados de scorm cuando se hace envío y recepción de datos
		*
		****************************************************************************************
	-->
<?php 
/* Variables de control para el sistema LMS en valores de ida y vuelta */
	$_cmicorelessonlocation = "";
	$_cmicorelessonstatus = "not attempted";
	$_cmicoreentry = "";
	$_cmicorescoreraw = "";
	$_cmicoretotaltime = "0000:00:00.00";
	$_cmicoreexit = "";
	$_cmicoresessiontime = "";
	$_cmisuspenddata = "";
	$_cmilaunchdata = "";
	$_cmicorescoremax = "";
	$_cmicorescoremin = "";
	$_cmicomments = "";
	$_cmistudentdatamasteryscore = "";
	$_cmistudentdatamaxtimeallowed = "";
	$_cmistudentpreferenceaudio = "0";
	$_cmistudentpreferencespeed = "0";
	$_cmistudentpreferencetext = "0";

	foreach ($LMS_datos as $iID => $data_LMSBase){
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicorelessonlocation') ){ $_cmicorelessonlocation = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicorelessonstatus') ){ $_cmicorelessonstatus = $data_LMSBase['value']; $_Var_CtrlAttempt = $data_LMSBase['attempt']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicoreentry') ){ $_cmicoreentry = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicorescoreraw') ){ $_cmicorescoreraw = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicoretotaltime') ){ $_cmicoretotaltime = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicoreexit') ){ $_cmicoreexit = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicoresessiontime') ){ $_cmicoresessiontime = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmisuspenddata') ){ $_cmisuspenddata = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmilaunchdata') ){ $_cmilaunchdata = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicorescoremax') ){ $_cmicorescoremax = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicorescoremin') ){ $_cmicorescoremin = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmicomments') ){ $_cmicomments = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmistudentdatamasteryscore') ){ $_cmistudentdatamasteryscore = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmistudentdatamaxtimeallowed') ){ $_cmistudentdatamaxtimeallowed = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmistudentpreferenceaudio') ){ $_cmistudentpreferenceaudio = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmistudentpreferencespeed') ){ $_cmistudentpreferencespeed = $data_LMSBase['value']; }
		if(isset($data_LMSBase['element']) && ($data_LMSBase['element']=='cmistudentpreferencetext') ){ $_cmistudentpreferencetext = $data_LMSBase['value']; }
	}
/* Variables de control para el sistema LMS en valores de ida y vuelta */
?>
<html>
	<head><meta charset="gb18030">
		<title>LMS Frame</title>
		<script language="Javascript">
			// Error Codes
			var _NoError = 0;
			var _GeneralException = 101;
			var _InvalidArgumentError = 201;
			var _ElementCannotHaveChildren = 202;
			var _ElementIsNotAnArray = 203;
			var _NotInitialized = 301;
			var _NotImplementedError = 401;
			var _InvalidSetValue = 402;
			var _ElementIsReadOnly = 403;
			var _ElementIsWriteOnly = 404;
			var _IncorrectDataType = 405;
			var _isInitialized = false;
			// ***** Bookmark Reseting Requirement *****
			// If this variable is true, the value for cmi.core.lesson_location and
			// cmi.suspend_data will be reset to empty string when a SCO is exited 
			// with a status of "completed" or "passed".
			var _bookmarkResetActivated = false;  // Set to false in Test Suite
			var LMS_Last_Action = "";
			var LMS_Last_Error = _NotInitialized;
			var LMS_Last_Debug = "";
			var aLMSQueue = new Array();
			var iTicks = 0;
			var iCommitCountdown = 0;
			var timerstate = 0;
			var timer_off = 0;
			var timer_doCommit = 1;
			var debugHandle;
			// var confirmHandle;
			var bDebug = "";
			/*******************************************************************************
			** Function: apiObject()
			**
			** Description:
			** Constructor for API object.
			*******************************************************************************/
			function apiObject() {
			  this.LMSInitialize = LMSInitialize;
			  this.LMSFinish = LMSFinish;
			  this.LMSGetValue = LMSGetValue;
			  this.LMSSetValue = LMSSetValue;
			  this.LMSCommit = LMSCommit;
			  this.LMSGetLastError = LMSGetLastError;
			  this.LMSGetErrorString = LMSGetErrorString;
			  this.LMSGetDiagnostic = LMSGetDiagnostic;
			}
			/*******************************************************************************
			** Function: loadAction()
			**
			** Description:
			** Instantiate the API object and load the course content.
			*******************************************************************************/
			function loadAction() {
			  top.API = new apiObject();
			  parent.Content_Scorm.location.href = document.forms[0].sScoUrl.value;
			  bDebug = document.forms[0].bDebug.value;
			  window.setTimeout("tickTock()", 1000);
			  
			  if (bDebug == "true") {  // display LMS calls in popup window
			    var winURL = "debug.html";
			    var winFeatures = "resizable=yes,width=725,height=500,top=160,left=120";
			    debugHandle = window.open(winURL, "debug", winFeatures);
			  }
			  window.focus();
			  
			}
			/*******************************************************************************
			** Function: unloadAction()
			**
			** Description:
			** Close the debug window if open.
			*******************************************************************************/
			function unloadAction() {
			  // This function no longer has anything to do.  GDK 9/22/2008
			  
			  //if (debugHandle) {
			  //  debugHandle.close();
			  //}
			  
			  //if (confirmHandle){
			  //  confirmHandle.close();
			  //}
			  
			  parent.opener.location.reload();  // Reload the course outline page.
			}
			/*******************************************************************************
			** Function: submitAPIExitForm()
			**
			** Description:
			** Pass the form values to the form APIExitForm in OutlineFrame.asp and submit
			** the form.  Submitting the form from CourseOutline.asp ensures that the API 
			** requests will be processed even if called during a page unload event.
			*******************************************************************************/
			function submitAPIExitForm() {
			  if (!parent.opener.document.APIExitForm) {
			    parent.window.close();
			  }
			  else {
			    parent.opener.document.APIExitForm.bDebug.value = document.forms[0].bDebug.value
			    parent.opener.document.APIExitForm.iMapID.value = document.forms[0].iMapID.value
			    parent.opener.document.APIExitForm.iServerID.value = document.forms[0].iServerID.value
			    parent.opener.document.APIExitForm.iProjectID.value = document.forms[0].iProjectID.value
			    parent.opener.document.APIExitForm.iVendorID.value = document.forms[0].iVendorID.value
			    parent.opener.document.APIExitForm.iCourseID.value = document.forms[0].iCourseID.value
			    parent.opener.document.APIExitForm.iScorm_CourseID.value = document.forms[0].iScorm_CourseID.value
			    parent.opener.document.APIExitForm.iBlockID.value = document.forms[0].iBlockID.value
			    parent.opener.document.APIExitForm.iScoID.value = document.forms[0].iScoID.value
			    parent.opener.document.APIExitForm.iPersonID.value = document.forms[0].iPersonID.value
			    parent.opener.document.APIExitForm.sScormShortName.value = document.forms[0].sScormShortName.value
			    parent.opener.document.APIExitForm.sLMSShortName.value = document.forms[0].sLMSShortName.value
			  
			    parent.opener.document.APIExitForm.submitQueueNames.value = document.forms[0].submitQueueNames.value;
			    parent.opener.document.APIExitForm.submitQueueElements.value = document.forms[0].submitQueueElements.value;
			    parent.opener.document.APIExitForm.submitQueueValues.value = document.forms[0].submitQueueValues.value;
			  
			    parent.opener.document.APIExitForm.submit();
			    
			    jsPause(500);
			  }
			}
			/*******************************************************************************
			** Function: LMSInitialize()
			**
			** Description:
			** Initialize communication between the LMS and the course.  Must be called
			** just once before any other LMS functions are called.
			*******************************************************************************/
			function LMSInitialize(sParameter) {
			  LMS_Last_Action = "LMSInitialize";
			  LMS_Last_Error = _NoError;
			  var value = "";

			  if (sParameter.len > 0) {
			    LMS_Last_Error = _InvalidArgumentError;
			  }

			  if (_isInitialized == true) {
			    LMS_Last_Error = _GeneralException;
			    value = "false";
			  }
			  else {
			    addQueueItem("LMSInitialize", "", "");
			    _isInitialized = true;
			    value = "true";
			  }
			  
			  LMS_Debug("LMSInitialize()");
			  return value;
			}
			/*******************************************************************************
			** Function: LMSFinish()
			**
			** Description:
			** End communication between the LMS and the course.  Submit queued items to 
			** APIComm.asp.  The launch window will then then be closed.
			*******************************************************************************/
			function LMSFinish(sParameter) {
			  LMS_Last_Action = "LMSFinish";
			  LMS_Last_Error = _NoError;
			  var value = "";
			  var sSessionTime = "";
			  var sTotalTime = "";
			  
			  if (sParameter.len > 0) {
			    LMS_Last_Error = _InvalidArgumentError;
			  }
			  
			  if (_isInitialized == false) {
			    LMS_Last_Error = _NotInitialized;
			    LMS_Debug("LMSFinish()");
			    value = "false";
			  }
			  else {
			  
			    // If true, clear the bookmark and suspend_data if the status is "completed" or "passed"
			    if (_bookmarkResetActivated) {
			      var sStatus = document.forms[0].cmicorelessonstatus.value;
			      if ((sStatus == "completed") || (sStatus == "passed")) {
			        addQueueItem("LMSSetValue", "cmi.core.lesson_location", "");
			        addQueueItem("LMSSetValue", "cmi.suspend_data", "");
			      }
			    }
			  
			    timerstate = timer_off;  // stop any pending commit
			    LMS_Debug("LMSFinish()");
			    addQueueItem("LMSFinish", "", "");
			    submitQueue();
			    _isInitialized = false;
			    value = "true";
			    
			    if (document.forms[0].bDebug.value == "true") {
			    
			      // Confirm window will no longer be launched here.  
			      // Causes too many problems, and is unnecessary.  GDK 9/22/2008
			      //// The following condition fixes a bug where opening a confirm window will prevent
			      //// the values from being stored if LMSFinish was called on an unload event.
			      //if (window.screenTop < 10000) {
			      //  var winURL = "confirm.html";
			      //  var winFeatures = "resizable=yes,width=100,height=75,top=100,left=100";
			      //  confirmHandle = window.open(winURL, "confirm", winFeatures);
			      //}
			      //else {
			      //  submitAPIExitForm();
			      //  parent.window.close();
			      //}
			      
			      // Confirm window will now be spawned from the debug window
			      if (debugHandle) {
			        debugHandle.spawnConfirmWindow();
			      }
			      submitAPIExitForm();
			      parent.window.close();
			      
			    }
			    else {
			      submitAPIExitForm();
			      parent.window.close();
			    }
			  }
			  
			  return value; 
			}
			/*******************************************************************************
			** Function: LMSGetValue()
			**
			** Description:
			** Retrieve the value for a given element from the LMS.  These values are
			** pre-fetched and stored in hidden form fields.
			*******************************************************************************/
			function LMSGetValue(sElement) {
			  LMS_Last_Action = "LMSGetValue"
			  LMS_Last_Error = _NoError;
			  var value = "";
			  
			  if (_isInitialized == false) {
			    LMS_Last_Error = _NotInitialized;
			  }
			  else {
			    value = getItem(sElement);
			  }
			  
			  LMS_Debug("LMSGetValue(\"" + sElement + "\")");
			  return value;
			}
			/*******************************************************************************
			** Function: LMSSetValue()
			**
			** Description:
			** Set the value for a given element on the LMS.  These values are temporarily 
			** stored in hidden form fields, but are written to the database when the queue 
			** is submitted upon LMSFinish or LMSCommit being called.
			*******************************************************************************/
			function LMSSetValue(sElement, sValue) {
			  LMS_Last_Action = "LMSSetValue"
			  LMS_Last_Error = _NoError;
			  var value = "";
			  
			  if (_isInitialized == false) {
			    LMS_Last_Error = _NotInitialized;
			    value = "false";
			  }
			  else if (checkDataType(sElement, sValue) == false) {
			    LMS_Last_Error = _IncorrectDataType;
			    value = "false";
			  }
			  else {
			    value = setItem(sElement, sValue);
			    if (value == "true") {
			      if (sElement == "cmi.comments") {  // special cases
			        sValue = document.forms[0].cmicomments.value;
			      }
			      else if (sElement == "cmi.core.lesson_status") {
			        sValue = document.forms[0].cmicorelessonstatus.value;
			      }
			      addQueueItem("LMSSetValue", sElement, sValue);
			    }
			  }
			  
			  LMS_Debug("LMSSetValue(\"" + sElement + "\",\"" + sValue + "\")");
			  return value;  
			}
			/*******************************************************************************
			** Function: LMSCommit()
			**
			** Description:
			** Submit queued items to APIComm.asp for processing into the database.  A
			** flag is set to keep the window open, and the queue is cleared.  Commits
			** are scheduled a few seconds after their request to prevent a flurry of 
			** commit calls in a row.  If more than one commit call is requested during
			** this time, only the latest commit call will execute.
			*******************************************************************************/
			function LMSCommit(sParameter) {
			  LMS_Last_Action = "LMSCommit"
			  LMS_LastError = _NoError;
			  var value = "";
			  
			  if(sParameter.len > 0) {
			    LMS_Last_Error = _InvalidArgumentError;
			  }
			  
			  if (_isInitialized == false) {
			    LMS_Last_Error = _NotInitialized;
			    LMS_Debug("LMSCommit()");
			    value = "false";
			  }
			  else {    
			    timerstate = timer_off;	  // stop any pending commit
			    value = "true";
			    submitQueue();
			    iCommitCountdown = 2  	  // 5 second countdown
			    timerstate = timer_doCommit;  // schedule the commit
			  }
			  
			  return value;
			}
			/*******************************************************************************
			** Function: LMSGetLastError()
			**
			** Description:
			** Return the current error code.
			*******************************************************************************/
			function LMSGetLastError() {
			  var value = LMS_Last_Error;
			  
			  LMS_Debug("LMSGetLastError()");
			  
			  return value;
			}
			/*******************************************************************************
			** Function: LMSGetErrorString()
			**
			** Description:
			** Return a string corresponding to the given error code.
			*******************************************************************************/
			function LMSGetErrorString(sErrorCode) {
			  var sError = sErrorCode;
			  var sValue = "";
			  if (sError == "") sError = LMS_Last_Error;
			  
			  switch(sError) {
			    case "0":
			      sValue = "No error"
			      break
			    case "101":
			      sValue = "General exception"
			      break
			    case "201":
			      sValue = "Invalid argument error"
			      break
			    case "202":
			      sValue = "Element cannot have children"
			      break
			    case "203":
			      sValue = "Element not an array - cannot have count"
			      break
			    case "301":
			      sValue = "Not initialized"
			      break
			    case "401":
			      sValue = "Not implemented error"
			      break
			    case "402":
			      sValue = "Invalid set value, element is a keyword"
			      break
			    case "403":
			      sValue = "Element is read only"
			      break
			    case "404":
			      sValue = "Element is write only"
			      break
			    case "405":
			      sValue = "Incorrect data type"
			      break
			    default:
			      sValue = "Unknown error code"
			      break      
			  }
			  
			  LMS_Debug("LMSGetErrorString(\"" + sError + "\")");
			  return sValue
			}
			/*******************************************************************************
			** Function: LMSGetDiagnostic()
			**
			** Description:
			**
			*******************************************************************************/
			function LMSGetDiagnostic(sParameter) {
			  var value = "Emulator has no diagnostics";
			  
			  LMS_Debug("LMSGetDiagnostic(\"" + sParameter + "\")");
			  
			  return value;
			}
			/*******************************************************************************
			** Function: tickTock()
			**
			** Description:
			** Maintain a timer, used in scheduling commit actions.
			*******************************************************************************/
			function tickTock() {
			  window.setTimeout("tickTock()", 1000);
			  iTicks = iTicks + 1;
			  
			  if (timerstate == timer_doCommit) {
			    doCommit();
			  }
			}
			/*******************************************************************************
			** Function: convertSeconds()
			**
			** Description:
			** Convert a positive float to a string in the time format 
			** HHHH:MM:SS.SS
			*******************************************************************************/
			function convertSeconds(iCount) {
			  var strHours;
			  var strMinutes;
			  var strSeconds;
			  var strMiliSeconds;
			  var iHours = Math.floor(iCount / 3600);
			  var iMinutes = Math.floor((iCount - (iHours * 3600)) / 60);
			  var iSeconds = Math.floor(iCount - (iHours * 3600) - (iMinutes * 60));
			  var iMiliSeconds = Math.floor(iCount * 100)
			  strMiliSeconds = iMiliSeconds.toString();
			  strMiliSeconds = strMiliSeconds.substr(strMiliSeconds.length - 2, 2)
			  if (iHours < 10) strHours = "000" + iHours.toString();
			  else if (iHours < 100) strHours = "00" + iHours.toString();
			  else if (iHours < 1000) strHours = "0" + iHours.toString();
			  else strHours = iHours.toString();
			  if (iMinutes < 10) strMinutes = "0" + iMinutes.toString();
			  else strMinutes = iMinutes.toString();
			  if (iSeconds < 1) strSeconds = "00";
			  else if (iSeconds < 10) strSeconds = "0" + iSeconds.toString();
			  else strSeconds = iSeconds.toString();
			  var strTime = strHours + ":" + strMinutes + ":" + strSeconds + "." + strMiliSeconds;
			  return strTime;
			}
			/*******************************************************************************
			** Function: revertSeconds()
			**
			** Description:
			** Convert a time formatted string back to a positive float
			*******************************************************************************/
			function revertSeconds(sCount) {
			  var iTime = 0;
			  var countArray = sCount.split(":");
			  var iMultiplier = 1;
			  for (i=countArray.length-1; i>-1; i--) {
			    iTime += parseFloat(countArray[i]) * iMultiplier;
			    iMultiplier = iMultiplier * 60;
			  }
			  return iTime;
			}
			/*******************************************************************************
			** Function: toFloat()
			**
			** Description:
			** Convert a string to a floating point value
			*******************************************************************************/
			function toFloat(sString) {
			  if (sString.length < 1) return 0;
			  else if (isNaN(parseFloat(sString))) return 0;
			  else return parseFloat(sString);
			}
			/*******************************************************************************
			** Function: jsPause()
			**
			** Description:
			** Force a javascript pause for a given number of milliseconds
			*******************************************************************************/
			function jsPause(millis) {
			  date = new Date();
			  var curDate = null;

			  do { var curDate = new Date(); } 
			  while(curDate-date < millis);
			}
			/*******************************************************************************
			** Function: doCommit()
			**
			** Description:
			** Carry out the commit action.
			*******************************************************************************/
			function doCommit() {
			    iCommitCountdown = iCommitCountdown - 1;
			    if (iCommitCountdown < 1) {
			      timerstate = timer_off;
			      document.forms[0].sCommitFlag.value = "true";
			      document.forms[0].submit();
			      document.forms[0].sCommitFlag.value = "false";
			      
			      // If the queue is cleared here, any LMSSetValues that get called while
			      // the commit was pending will be erased from the queue.  I can think
			      // of no reason to do that. GDK 10/16/2006
			      // aLMSQueue.length = 0;
			    }
			}
			/*******************************************************************************
			** Function: checkDataType()
			**
			** Description:
			** Determine if the correct data type is used for a given element and value
			*******************************************************************************/
			function checkDataType(sElement, sValue) {
			  var bResult = true;
			  
			  switch(sElement) {
			    case "cmi.core.lesson_location":
			      bResult = checkString255(sValue)
			      break
			    case "cmi.core.lesson_status":
			      bResult = checkLessonStatusVocabulary(sValue)
			      break
			    case "cmi.core.exit":
			      bResult = checkExitVocabulary(sValue)
			      break
			    case "cmi.core.session_time":
			      bResult = checkTimeFormat(sValue)
			      break
			    case "cmi.core.score.raw":
			      bResult = checkDecimalWithRange(sValue, 0, 100)
			      break
			    case "cmi.core.score.max":
			      bResult = checkDecimalWithRange(sValue, 0, 100)
			      break
			    case "cmi.core.score.min":
			      bResult = checkDecimalWithRange(sValue, 0, 100)
			      break
			    case "cmi.suspend_data":
			      bResult = checkString4096(sValue)
			      break
			    case "cmi.comments":
			      bResult = checkString4096(sValue)
			      break
			    case "cmi.student_preference.audio":
			      bResult = checkIntegerWithRange(sValue, -1, 100)
			      break
			    case "cmi.student_preference.language":
			      bResult = checkString255(sValue)
			      break
			    case "cmi.student_preference.speed":
			      bResult = checkIntegerWithRange(sValue, -100, 100)
			      break
			    case "cmi.student_preference.text":
			      bResult = checkIntegerWithRange(sValue, -1, 1)
			      break
			  }
			  return bResult;
			}
			/*******************************************************************************
			** Function: checkString255()
			**
			** Description:
			** Check if a string is no more than 255 characters in length
			*******************************************************************************/
			function checkString255(sValue) {
			  if (sValue.length > -1 && sValue.length < 256) {
			    return true;
			  }
			  else {
			    return false;
			  }
			}
			/*******************************************************************************
			** Function: checkString4096()
			**
			** Description:
			** Check if a string is no more than 4096 characters in length
			*******************************************************************************/
			function checkString4096(sValue) {
			  if (sValue.length > -1 && sValue.length < 4096) {
			    return true;
			  }
			  else {
			    return false;
			  }
			}
			/*******************************************************************************
			** Function: checkDecimalWithRange()
			**
			** Description:
			** Check if a string contains a decimal number expression within the given 
			** range.
			*******************************************************************************/
			function checkDecimalWithRange(sValue, fMin, fMax) {
			  var bResult;
			  var fValue;
			  if (sValue == "") {
			    bResult = false
			  }
			  else {
			    regex = /^[-+]?\d*\.?(\d+)?$/
			    bResult = regex.test(sValue);
			  }
			  if (bResult) {
			    fValue = parseFloat(sValue);
			    if ((fValue < fMin) || (fValue > fMax)) bResult = false;
			  }
			  
			  return bResult;
			}
			/*******************************************************************************
			** Function: checkIntegerWithRange()
			**
			** Description:
			** Check if a string contains an integer number expression within the given
			** range.
			*******************************************************************************/
			function checkIntegerWithRange(sValue, iMin, iMax) {
			  var bResult;
			  var iValue;
			  bResult = true;

			  regex = /^[-+]?\d+$/
			  bResult = regex.test(sValue);
			  if (bResult) {
			    iValue = parseInt(sValue);
			    if ((iValue < iMin) || (iValue > iMax)) bResult = false;
			  }
			  
			  return bResult;
			}
			/*******************************************************************************
			** Function: checkTimeFormat()
			**
			** Description:
			** Check if a string contains a time expression formatted as HHHH:MM:SS.SS
			*******************************************************************************/
			function checkTimeFormat(sValue) {
			  var bResult;

			  if (sValue == "") {
			    bResult = false;
			  }
			  else {
			    regex = /^\d{1,4}\:\d{1,2}\:\d{1,2}\.?(\d+)?$/
			    bResult = regex.test(sValue);
			  }

			  return bResult;
			}
			/*******************************************************************************
			** Function: checkLessonStatusVocabulary()
			**
			** Description:
			** Check for legal values for the element cmi.core.lesson_status
			*******************************************************************************/
			function checkLessonStatusVocabulary(sValue) {
			  switch(sValue) {
			    case "passed":
			      return true
			      break
			    case "completed":
			      return true
			      break
			    case "failed":
			      return true
			      break
			    case "incomplete":
			      return true
			      break
			    case "browsed":
			      return true
			      break
			    case "not attempted":
			      return true
			      break
			  }
			  return false;
			}
			/*******************************************************************************
			** Function: checkExitVocabulary()
			**
			** Description:
			** Check for legal values for the element cmi.core.exit
			*******************************************************************************/
			function checkExitVocabulary(sValue) {
			  switch(sValue) {
			    case "time-out":
			      return true
			      break
			    case "suspend":
			      return true
			      break
			    case "logout":
			      return true
			      break
			    case "":
			      return true
			      break
			  }
			  return false;
			}
			/*******************************************************************************
			** Function: getItem()
			**
			** Description:
			** Return the value of an element temporarily stored in the form.
			*******************************************************************************/
			function getItem(sName) {
			  var sValue = "";
			  switch(sName) {
			  
			    // Mandatory SCORM Data Model Elements
			    case "cmi.core._children":
			      sValue = document.forms[0].cmicorechildren.value
			      break
			    case "cmi.core.student_id":
			      sValue = document.forms[0].cmicorestudentid.value
			      break
			    case "cmi.core.student_name":
			      sValue = document.forms[0].cmicorestudentname.value
			      break
			    case "cmi.core.student_image":
			      sValue = document.forms[0].cmicorestudentimg.value
			      break
			    case "cmi.core.lesson_location":
			      sValue = document.forms[0].cmicorelessonlocation.value
			      break
			    case "cmi.core.credit":
			      sValue = document.forms[0].cmicorecredit.value
			      break
			    case "cmi.core.lesson_status":
			      sValue = document.forms[0].cmicorelessonstatus.value
			      break
			    case "cmi.core.entry":
			      sValue = document.forms[0].cmicoreentry.value
			      break
			    case "cmi.core.score._children":
			      sValue = document.forms[0].cmicorescorechildren.value
			      break
			    case "cmi.core.score.raw":
			      sValue = document.forms[0].cmicorescoreraw.value
			      break
			    case "cmi.core.total_time":
			      sValue = document.forms[0].cmicoretotaltime.value
			      break
			    case "cmi.core.exit":
			      LMS_Last_Error = _ElementIsWriteOnly
			      break
			    case "cmi.core.session_time":
			      LMS_Last_Error = _ElementIsWriteOnly
			      break
			    case "cmi.suspend_data":
			      sValue = document.forms[0].cmisuspenddata.value
			      break
			    case "cmi.launch_data":
			      sValue = document.forms[0].cmilaunchdata.value
			      break

			    // Optional SCORM Data Model Elements
			    case "cmi.core.score.max":
			      sValue = document.forms[0].cmicorescoremax.value
			      break
			    case "cmi.core.score.min":
			      sValue = document.forms[0].cmicorescoremin.value
			      break
			    case "cmi.core.lesson_mode":
			      sValue = document.forms[0].cmicorelessonmode.value
			      break
			    case "cmi.comments":
			      sValue = document.forms[0].cmicomments.value
			      break
			    case "cmi.comments_from_lms":
			      sValue = document.forms[0].cmicommentsfromlms.value
			      break
			    case "cmi.student_data._children":
			      sValue = document.forms[0].cmistudentdatachildren.value
			      break
			    case "cmi.student_data.mastery_score":
			      sValue = document.forms[0].cmistudentdatamasteryscore.value
			      break
			    case "cmi.student_data.max_time_allowed":
			      sValue = document.forms[0].cmistudentdatamaxtimeallowed.value
			      break
			    case "cmi.student_data.time_limit_action":
			      sValue = document.forms[0].cmistudentdatatimelimitaction.value
			      break
			    case "cmi.student_preference._children":
			      sValue = document.forms[0].cmistudentpreferencechildren.value
			      break
			    case "cmi.student_preference.audio":
			      sValue = document.forms[0].cmistudentpreferenceaudio.value
			      break
			    case "cmi.student_preference.language":
			      sValue = document.forms[0].cmistudentpreferencelanguage.value
			      break
			    case "cmi.student_preference.speed":
			      sValue = document.forms[0].cmistudentpreferencespeed.value
			      break
			    case "cmi.student_preference.text":
			      sValue = document.forms[0].cmistudentpreferencetext.value
			      break

			    default:
			      LMS_Last_Error = _InvalidArgumentError
			  }
			  
			  return sValue;
			}
			/*******************************************************************************
			** Function: setItem()
			**
			** Description:
			** Set the value of an element temporarily stored in the form.
			*******************************************************************************/
			function setItem(sName, sValue) {
			  var value = "true";
			  switch(sName) {
			  
			    // Mandatory SCORM Data Model Elements
			    case "cmi.core._children":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.core.student_id":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.core.student_name":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.core.lesson_location":
			      document.forms[0].cmicorelessonlocation.value = sValue
			      break
			    case "cmi.core.credit":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.core.lesson_status":
			      // In accordance with SCORM 1.2 specs, if a mastery score is set, and the status is not "incomplete",
			      // the LMS may change the status to passed or failed based on the student's score.
			      if ((sValue != "incomplete") && (sValue != "not attempted")) {
			        sValue = getCorrectedLessonStatus(sValue)
			      }
			      document.forms[0].cmicorelessonstatus.value = sValue
			      break
			    case "cmi.core.entry":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.core.score._children":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.core.score.raw":
			      document.forms[0].cmicorescoreraw.value = sValue
			      break
			    case "cmi.core.total_time":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.core.exit":
			      document.forms[0].cmicoreexit.value = sValue
			      break
			    case "cmi.core.session_time":
			      document.forms[0].cmicoresessiontime.value = sValue
			      break
			    case "cmi.suspend_data":
			      document.forms[0].cmisuspenddata.value = sValue
			      break
			    case "cmi.launch_data":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break

			    // Optional SCORM Data Model Elements
			    case "cmi.core.score.max":
			      document.forms[0].cmicorescoremax.value = sValue
			      break
			    case "cmi.core.score.min":
			      document.forms[0].cmicorescoremin.value = sValue
			      break
			    case "cmi.core.lesson_mode":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.comments":
			      // This data model element accumulates the comments set by the SCO, as opposed to overwriting previously set values.
			      if (!checkString4096(document.forms[0].cmicomments.value + sValue)) {
			        LMS_Last_Error = _IncorrectDataType;
			        value = "false"
			      }
			      else {
			        document.forms[0].cmicomments.value = document.forms[0].cmicomments.value + sValue
			      }
			      break
			    case "cmi.comments_from_lms":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.student_data._children":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.student_data.mastery_score":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.student_data.max_time_allowed":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.student_data.time_limit_action":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.student_preference._children":
			      LMS_Last_Error = _ElementIsReadOnly
			      value = "false"
			      break
			    case "cmi.student_preference.audio":
			      document.forms[0].cmistudentpreferenceaudio.value = sValue
			      break
			    case "cmi.student_preference.language":
			      document.forms[0].cmistudentpreferencelanguage.value = sValue
			      break
			    case "cmi.student_preference.speed":
			      document.forms[0].cmistudentpreferencespeed.value = sValue
			      break
			    case "cmi.student_preference.text":
			      document.forms[0].cmistudentpreferencetext.value = sValue
			      break

			    default:
			      LMS_Last_Error = _InvalidArgumentError
			      value = "false"
			  }
			  
			  return value;
			}
			/*******************************************************************************
			** Function: getCorrectedLessonStatus()
			**
			** Description: If a mastery score is set in the manifest, ensure the lesson 
			** status is consistent with the student's raw score.
			** 
			*******************************************************************************/
			function getCorrectedLessonStatus(sStatus) {
			  var sCredit = document.forms[0].cmicorecredit.value;
			  var fMasteryScore = toFloat(document.forms[0].cmistudentdatamasteryscore.value);
			  var fScoreRaw = toFloat(document.forms[0].cmicorescoreraw.value);
			  
			  if ((sCredit == "credit") && (fMasteryScore > 0)) {
			    if (fScoreRaw < fMasteryScore) sStatus = "failed";
			    else sStatus = "passed";
			  }
			  
			  return sStatus;
			}
			/*******************************************************************************
			** Function: queueItemLookup()
			**
			** Description:
			** Look up an item on the queue and return its position.
			*******************************************************************************/
			function queueItemLookup(sName, sElement) {
			  for (i = 0; i < aLMSQueue.length; i++) {
			    if (aLMSQueue[i].sName == sName && aLMSQueue[i].sElement == sElement) {
			      return i;
			    }
			  }
			  return null;
			}
			/*******************************************************************************
			** Function: addQueueItem()
			**
			** Description:
			** Update an item on the queue if an existing item has the same name and 
			** element.  Otherwise, add a new item to the queue.
			*******************************************************************************/
			function addQueueItem(sName, sElement, sValue) {
				//* New code does a delete and add.  This ensures database inserts done in correct order.
				  var bResult = deleteQueueItem(sName, sElement);
				  aLMSQueue[aLMSQueue.length] = new aQueueItemConstruct(sName, sElement, sValue);

				//* Old code did a find and replace.
				//  var iItem = queueItemLookup(sName, sElement);
				//  if (iItem != null) {
				//    aLMSQueue[iItem].sName = sName;
				//    aLMSQueue[iItem].sElement = sElement;
				//   aLMSQueue[iItem].sValue = sValue;
				//  }
				//  else {
				//    aLMSQueue[aLMSQueue.length] = new aQueueItemConstruct(sName, sElement, sValue);
				//  }
			}
			/*******************************************************************************
			** Function: deleteQueueItem()
			**
			** Description:
			** Delete an item from the queue if an existing item has the same name and 
			** element.  Return true if successful.
			*******************************************************************************/
			function deleteQueueItem(sName, sElement) {
			  var iItem = queueItemLookup(sName, sElement);
			  if (iItem != null) {
			    aLMSQueue.splice(iItem, 1);
			    return true;
			  }
			  else {
			    return false;
			  }
			}
			/*******************************************************************************
			** Function: aQueueItemConstruct()
			**
			** Description:
			** Constructor for queue item object.
			*******************************************************************************/
			function aQueueItemConstruct(sName, sElement, sValue) {
			  this.sName = sName;
			  this.sElement = sElement;
			  this.sValue = sValue;
			}
			/*******************************************************************************
			** Function: LMS_Debug()
			**
			** Description:
			** Create a debug message based on the last LMS function called.
			*******************************************************************************/
			function LMS_Debug(str) {
			  LMS_Last_Debug = str;
			  if (LMS_Last_Error != _NoError) {
			    LMS_Last_Debug += " -- Error Code " + LMS_Last_Error;
			  }
			  else {
			    LMS_Last_Debug += " -- No Error";
			  }
			  if (bDebug == "true") {
			    document.forms[0].sDebugText.value = document.forms[0].sDebugText.value + LMS_Last_Debug + "\r\r";
			    if (debugHandle) {
			      debugHandle.document.forms[0].results.value = document.forms[0].sDebugText.value;
			    }
			  }
			}
			/*******************************************************************************
			** Function: submitQueue()
			**
			** Description:
			** Submit queue data to the form for processing.
			*******************************************************************************/
			function submitQueue() {
			  var sQueueNameList = "";
			  var sQueueElementList= "";
			  var sQueueValueList = "";
			  var sDelim = "";
			  
			  for(i = 0; i < aLMSQueue.length; i++) {
			    if (i!=0) {
			      sDelim = "<lms:delim />";
			    }
			    sQueueNameList += sDelim + "" + aLMSQueue[i].sName;
			    sQueueElementList += sDelim + "" + aLMSQueue[i].sElement;
			    sQueueValueList += sDelim + "" + aLMSQueue[i].sValue;
			  }

			  document.forms[0].submitQueueNames.value = sQueueNameList;
			  document.forms[0].submitQueueElements.value = sQueueElementList;
			  document.forms[0].submitQueueValues.value = sQueueValueList;
			}
		</script>
	</head>
	<body onload="loadAction()" onunload="unloadAction()" cz-shortcut-listen="true">
		<form method="POST" id="APIForm" name="APIForm" action="APICommunication.php" target="API_Comunications">
			<!-- ReadOnly parameters-->
				<!-- Mandatory SCORM Data Model Elements -->
				<input type="hidden" name="cmicorechildren" value="student_id,student_name,lesson_location,credit,lesson_status,entry,score,total_time,lesson_mode,exit,session_time">
				<input type="hidden" name="cmicorestudentid" value="<?php echo($_SESSION['idUsuario']); ?>">
				<input type="hidden" name="cmicorestudentname" value="<?php echo($_SESSION['NameUsuario']); ?>">
				<input type="hidden" name="cmicorestudentimg" value="<?php echo($_SESSION['imagen']); ?>">
				<input type="hidden" name="cmicorecredit" value="credit">
				<input type="hidden" name="cmicorescorechildren" value="raw,max,min">
				<input type="hidden" name="cmicore_Attempt" value="<?php echo($_Var_CtrlAttempt); ?>">
				<!-- Optional SCORM Data Model Elements -->
				<!-- cmi.objectives, and cmi.interactions not supported -->
				<input type="hidden" name="cmicorelessonmode" value="normal"> 
				<input type="hidden" name="cmicommentsfromlms" value="SCORM 1.2 LMS - (C) 2015 Ludus Learning Systems">
				<input type="hidden" name="cmistudentdatachildren" value="mastery_score,max_time_allowed,time_limit_action">
				<input type="hidden" name="cmistudentdatatimelimitaction" value="continue,no message">
				<input type="hidden" name="cmistudentpreferencechildren" value="audio,language,speed,text">
				<input type="hidden" name="cmistudentpreferencelanguage" value="es">
				<!-- LMS Values -->
				<input type="hidden" name="iPersonID" value="<?php echo($_SESSION['idUsuario']); ?>">
				<input type="hidden" name="sScormShortName" value="Scorm1_2">
				<input type="hidden" name="sLMSShortName" value="LMS">
				<input type="hidden" name="iCourseID" value="<?php echo($_GET['ScormId']); ?>">
				<input type="hidden" name="iScorm_CourseID" value="<?php echo($_GET['ScormId']); ?>">
				<input type="hidden" name="iScoID" value="<?php echo($_GET['ScoId']); ?>">
				<input type="hidden" name="iBlockID" value="0">
				<input type="hidden" name="iMapID" value="0">
				<input type="hidden" name="iServerID" value="0">
				<input type="hidden" name="iProjectID" value="0">
				<input type="hidden" name="iVendorID" value="0">
				<input type="hidden" name="bDebug" value="">
				<input type="hidden" name="sDebugText" value="">
				<input type="hidden" name="sScoUrl" value="../Courses/<?php echo($_GET['URLCourse']); ?>">
				<!--<input type="hidden" name="sScoUrl" value="http://localhost/ludus/Courses/<?php echo($_GET['URLCourse']); ?>">-->
				<input type="hidden" name="sCommitFlag" value="true">
				<textarea cols="80" rows="2" name="submitQueueNames"></textarea><br>
				<textarea cols="80" rows="2" name="submitQueueElements"></textarea><br>
				<textarea cols="80" rows="2" name="submitQueueValues"></textarea><br>
			<!-- ReadOnly parameters-->
			<!-- Read/Write parameters-->
				<!-- Mandatory SCORM Data Model Elements -->
				<input type="hidden" name="cmicorelessonlocation" value="<?php echo($_cmicorelessonlocation); ?>">
				<input type="hidden" name="cmicorelessonstatus" value="<?php echo($_cmicorelessonstatus); ?>">
				<input type="hidden" name="cmicoreentry" value="<?php echo($_cmicoreentry); ?>">
				<input type="hidden" name="cmicorescoreraw" value="<?php echo($_cmicorescoreraw); ?>">
				<input type="hidden" name="cmicoretotaltime" value="<?php echo($_cmicoretotaltime); ?>">
				<input type="hidden" name="cmicoreexit" value="<?php echo($_cmicoreexit); ?>"><!--write only-->
				<input type="hidden" name="cmicoresessiontime" value="<?php echo($_cmicoresessiontime); ?>"><!--write only-->
				<!--<input type="hidden" name="cmisuspenddata" value="0,0|0,0,0|0,0,0,0,0,0,0,0,0|0,0,0,0,0|0,0,0,0,0,0,0|0,0,0,0,0|0,0,0,0|0,0,0,0,0,0,0|0*0,0|0,0,0|0,0,0,0,0,0,0,0,0|0,0,0,0,0|0,0,0,0,0,0,0|0,0,0,0,0|0,0,0,0|0,0,0,0,0,0,0|0">-->
				<input type="hidden" name="cmisuspenddata" value="<?php echo($_cmisuspenddata); ?>">
				<input type="hidden" name="cmilaunchdata" value="<?php echo($_cmilaunchdata); ?>">
				<!-- Optional SCORM Data Model Elements -->
				<!-- cmi.objectives, and cmi.interactions not supported -->
				<input type="hidden" name="cmicorescoremax" value="<?php echo($_cmicorescoremax); ?>">
				<input type="hidden" name="cmicorescoremin" value="<?php echo($_cmicorescoremin); ?>">
				<input type="hidden" name="cmicomments" value="<?php echo($_cmicomments); ?>">
				<input type="hidden" name="cmistudentdatamasteryscore" value="<?php echo($_cmistudentdatamasteryscore); ?>">
				<input type="hidden" name="cmistudentdatamaxtimeallowed" value="<?php echo($_cmistudentdatamaxtimeallowed); ?>">
				<input type="hidden" name="cmistudentpreferenceaudio" value="<?php echo($_cmistudentpreferenceaudio); ?>">
				<input type="hidden" name="cmistudentpreferencespeed" value="<?php echo($_cmistudentpreferencespeed); ?>">
				<input type="hidden" name="cmistudentpreferencetext" value="<?php echo($_cmistudentpreferencetext); ?>">
			<!-- Read/Write parameters-->
		</form>
	</body>
</html>
<!--<script src="../assets/jquery-1.11.3.min.js?v=v2.3.0"></script>-->
<script language="Javascript">
/*$(document).ready(function(){
	console.log("----------------------------------------------"+$('#iScorm_CourseID').val()+"----------------------------------------------");
});
$( "#APIForm" ).submit(function( event ) {
    console.log("Dio click en el envio de datos");
    event.preventDefault();
});*/
</script>

