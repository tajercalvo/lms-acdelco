<?php include('src/seguridad.php');
	if(!isset($_SESSION['idUsuario'])){
		header("location: login");
	}
?>
<?php
$detail_msg = "SI";
$location = 'mensajes';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		<?php include('src/menu.php'); ?>
		<!-- Content -->
<div id="content">
		<?php include('src/top_nav_bar.php'); ?>
		<!-- Camino de Hormigas -->
		<ul class="breadcrumb">
			<li>Estás aquí</li>
			<li><a href="index.php" class="glyphicons message_flag"><i></i> LUDUS LMS</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
			<li><a href="../admin/">Mensajes</a></li>
		</ul>
		<!-- Camino de Hormigas -->
		<!-- Mensajes -->
		<div class="widget widget-messages widget-heading-simple widget-body-white">
			<div class="widget-body padding-none">

				<div class="row row-merge borders">
					<div class="ajax-loading hide" id="Loading_Msg_Usr">
						<i class="fa fa-refresh text-primary-light fa fa-spin fa fa-4x"></i>
					</div>
					<div class="col-md-3 listWrapper" id="ListadoRemitentes" style="min-height: 765px !important;">
						<div class="innerAll">
							<div class="input-group">
						    	<input class="form-control" id="busqueda_pal" name="busqueda_pal" type="text" placeholder="Buscar Remitente .." value="" />
						    	<span class="input-group-addon">
						    		<i class="fa fa-search"></i>
						    	</span>
							</div>
						</div>
						<span class="results">Mensajes: <?php echo $cantidadTotal; ?> | Remitentes: <?php echo $cantidadRemitentes; ?> | Destinatarios: <?php echo $cantidadDestinatarios; ?><i class="fa fa-circle-arrow-down"></i></span>
						<ul class="list unstyled">
							<?php foreach ($listadoPersonas as $iID => $dataPersona) { ?>
								<li class="UsrMsg<?php if($dataPersona['user_id']==$idPer_sel){ ?> active<?php } ?>" data-id="<?php echo $dataPersona['user_id']; ?>" data-name="<?php echo $dataPersona['first_name'].' '.$dataPersona['last_name']; ?>">
									<div class="media innerAll">
										<div class="media-object pull-left thumb hidden-phone"><img alt="Image" src="../assets/images/usuarios/<?php echo $dataPersona['image']; ?>" style="width: 50px; height: 50px;" /></div>
										<div class="media-body">
											<span class="usr_list strong"><?php echo $dataPersona['first_name'].' '.$dataPersona['last_name']; ?></span>
											<small>Ultimo mensaje enviado el:</small><br/>
											<small class="text-italic text-primary-light"><i class="fa fa-calendar fa fa-fixed-width"></i> <?php echo $dataPersona['date_creation']; ?></small>
										</div>
									</div>
								</li>
							<?php } ?>
							<li>
								<div class="media innerAll">
									<div class="media-object pull-left thumb hidden-phone"><img alt="Image" src="../assets/images/new_messages.png" style="width: 50px; height: 50px;" /></div>
									<div class="media-body">
										<span class="strong">
											<select style="width: 100%;" id="user_msg_id" name="user_msg_id">
											<?php foreach ($listadosUsuarios as $key => $Data_Usr) { ?>
												<option value="<?php echo($Data_Usr['user_id']); ?>" ><?php echo($Data_Usr['first_name'].' '.$Data_Usr['last_name']); ?></option>
											<?php } ?>
											</select>
										</span>
										<small class="MsgNvo">Escribir un mensaje nuevo</small><br/>
										<small class="text-italic text-primary-light MsgNvo"><i class="fa fa-calendar fa fa-fixed-width"></i> Ahora</small>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-9 detailsWrapper">
						<div class="innerAll">
							<form role="form" id="formulario_data" name="formulario_data">
								<input type="hidden" name="user_newMsg" id="user_newMsg" value="<?php echo ($idPer_sel); ?>" />
								<input type="hidden" name="opcn" id="opcn" value="NewMsg" />
								<div class="form-group">
									<textarea name="MensajeTxt" id="MensajeTxt" placeholder="Escriba el mensaje .. " class="form-control" rows="2"></textarea>
								</div>
								<div class="form-group">
									<div class="text-right">
										<button type="button" id="BtnMsgEnviar" class="btn btn-primary" onclick="Act_FormularioData();">Enviar <i class="fa-fw fa fa-envelope"></i></button>
									</div>
								</div>
							</form>
						</div>
						<div class="ajax-loading hide" id="Loading_Msg">
							<i class="fa fa-refresh text-primary-light fa fa-spin fa fa-4x"></i>
						</div>
						<div id="SeccionMensajes" style="min-height: 615px;height: 615px;overflow-y: scroll;">
							<span href="" class="load"><i class="fa fa-calendar innerR third"></i>Mensajes / Notificaciones <?php if(isset($name_per)){ echo('Con: '.$name_per); }?></span>
							<input type="hidden" name="name_user_newMsg" id="name_user_newMsg" value="<?php echo ($name_per); ?>" />
							<?php foreach ($mensajes_Persona as $iID => $dataMessage_detail) { ?>
							<div class="row row-merge borders border-top">
								<div class="col-md-12">
									<div class="innerAll">
										<div class="media">
											<a href="" class="thumb pull-left visible-md visible-lg"><img src="../assets/images/usuarios/<?php echo $dataMessage_detail['cr_im']; ?>" alt="Image" class="media-object" style="width: 50px; height: 50px;" /></a>
											<div class="media-body">
												<small class="pull-right text-primary-light"><?php if($dataMessage_detail['type']="1"){ ?><i class="fa fa-comment fa fa-fixed-width"></i><?php }else{?><i class="fa fa-bell fa fa-fixed-width"></i><?php } ?> <i class="fa fa-calendar fa fa-fixed-width"></i> <?php echo $dataMessage_detail['date_creation']; ?></small>
												<strong class="text-primary"><?php echo $dataMessage_detail['cr_fn'].' '.$dataMessage_detail['cr_ln']; ?></strong><br/>
												<small><?php echo  html_entity_decode($dataMessage_detail['message'], ENT_QUOTES); ?></small>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="row row-merge borders border-top">
						</div>

					</div>
				</div>

			</div>
		</div>
		<!-- // Mensajes -->
</div>
		</div>
		<!-- // Content END -->
				</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/mensajes.js"></script>
</body>
</html>
