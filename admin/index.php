<?php include_once('src/seguridad.php'); ?>
<?php
if (!isset($_GET['id'])) {
	if (isset($_SESSION['idUsuario'])) {
		$_GET['id'] = $_SESSION['idUsuario'];
	}
}
include('controllers/index.php');
$location = 'home';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<style>
		#banner-home {
			display: inline-flex !important;
		}
	</style>
</head>

<body class="document-body ">
	<?php if ($var_streaming > 0 || $var_transmision > 0) { ?>
		<!-- Modal -->
		<div class="modal fade " id="modal-streaming" style="z-index:20000">
			<div class="modal-dialog " style="width:930px;top:-25px;">
				<div class="modal-content">
					<!-- Modal heading -->
					<div class="modal-header">
						<button type="button" class="close" style="top:0px;" data-dismiss="modal" aria-hidden="true" onclick="stop_vid();">&times;</button>
						<h3 class="modal-title" style="height:25px;font-size:12px;line-height:22px;">Streaming Nacional Autocat</h3><?php if ($var_transmision > 0) { ?><h3 class="modal-title text-primary" style="height:25px;font-size:12px;line-height:22px;" id="ContadorVisitantes">Visitantes: 0</h3><?php } ?>
					</div>
					<!--// Modal heading END -->
					<!-- Modal body -->
					<div class="modal-body center">
						<?php if ($var_transmision > 0) { ?>
							<!--<div class="center" id="camera-publisher">
							<img id="imagenPreview" src="../assets/images/stream_GM.jpg" style="width: 600px; ">
						</div>-->
						<?php } elseif ($var_streaming > 0) { ?>
							<!--<div class="center" id="camera-subscriber">
							<img id="imagenPreview" src="../assets/images/stream_GM.jpg" style="width: 600px; ">
						</div>
						<div class="text-danger center" style="font-size:10px">Si presenta alguna dificultad con su conexión a internet; o el audio, o el video pierden calidad de forma inesperada: revise su conexión a internet e intente nuevamente cargando la página. Muchas gracias.</div>-->
						<?php } ?>
						<video id="vid1" src="../assets/vct/ST_02032017.mp4" poster="../assets/images/stream_GM.jpg" class="video-js vjs-default-skin" controls preload="auto" width="800" height="435"></video>
					</div>
					<!-- // Modal body END -->
				</div>
			</div>
		</div>
		<!-- // Modal END -->
	<?php } ?>
	<?php if ($var_transmision > 0) { ?>
		<input type="hidden" name="emisor_id" id="emisor_id" value="1">
	<?php } ?>
	<?php if (is_array($listado_BannerMod)) { ?>
		<!-- Modal -->
		<div class="modal fade " id="modal-lanzamiento" style="z-index:20000">
			<div class="modal-dialog " style="width:930px;top:-25px;">
				<div class="modal-content">
					<!-- Modal heading -->
					<div class="modal-header">
						<button type="button" class="close" style="top:0px;" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 class="modal-title" style="height:25px;font-size:12px;line-height:22px;"><?php echo ($listado_BannerMod['banner_name']); ?></h3>
					</div>
					<!--// Modal heading END -->
					<!-- Modal body -->
					<div class="modal-body">
						<?php
						$archivo = explode(".", $listado_BannerMod['file_name']);
						$extension = end($archivo);
						if ($extension == "mp4") { ?>
							<a href="<?php echo ($listado_BannerMod['banner_url']); ?>">
								<video width="100%" controls autoplay>
									<source src="../assets/gallery/banners/<?php echo ($listado_BannerMod['file_name']); ?>" type="video/mp4">
									Tu navegador no soporta este componente de video.
								</video>
								<!-- <img class="img-responsive" id="imgModal_Lanzamiento"  title="<?php echo ($listado_BannerMod['banner_name']); ?>" class="img-responsive" onclick="clickBanner(<?php echo ($listado_BannerMod['banner_id']); ?>)"/> -->
							</a>
						<?php } else { ?>
							<a href="<?php echo ($listado_BannerMod['banner_url']); ?>">
								<img class="img-responsive" id="imgModal_Lanzamiento" src="../assets/gallery/banners/<?php echo ($listado_BannerMod['file_name']); ?>" title="<?php echo ($listado_BannerMod['banner_name']); ?>" class="img-responsive" onclick="clickBanner(<?php echo ($listado_BannerMod['banner_id']); ?>)" />
							</a>
						<?php } ?>

					</div>
					<!-- // Modal body END -->
				</div>
			</div>
		</div>
		<!-- // Modal END -->
	<?php } ?>


	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>



				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Dashboard Estudiante</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- Camino de Hormigas -->
					<!-- Saludo estudiante -->
					<?php include_once('index/info.php'); ?>
					<!-- Saludo estudiante -->
				</div>
			</div>
			<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	</div>
	<!--<div id="float-whatsapp" style="position:fixed;bottom:60px;right:20px;"> <a href="https://wa.me/573182529023?text=Hola%21+Estoy+en+el+lms_ACDelco+y+necesito+ayuda.+Muchas+Gracias" target="_blank"> <img src="https://autotrain.com.co/wp-content/themes/sydney-child/img/whatsapp-icon.svg" width="50" height="50"> </a></div>
	-->
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="//static.opentok.com/v2/js/opentok.js"></script>
	<script src="js/index.js?varRand=<?php echo (rand(10, 999999)); ?>"></script>
</body>

</html>