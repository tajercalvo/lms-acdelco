<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_metricas_lideres.php');
$location = 'reporting';
$locData = true;
//$Asist = true;
$DashBoard = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper" style="visibility: hidden;">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons charts"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_metricas_lideres.php">Métricas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Informe de Métricas</h2>
							<br><br>
						</div>
					<!-- // END heading -->
<!-- contenido filtros -->
	<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; "> </h5><br>
			</div>
			<div class="col-md-2">
				<?php if(isset($_POST['start_date_day'])){ ?>
					<!-- <h5><a href="rep_trayectorias_concesionarios_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5> -->
				<?php } ?>
			
			</div>
		</div>
		<div class="row">
			<form method="post" id="formulario_usuarios">
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Hasta..."<?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..."<?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
					<!-- <h1 class="btn btn-success" id="btn_consultar"><i class="fa fa-check-circle"></i> Consultar</h1> -->
					<button type="submit" class="btn btn-success" id="btn_consultar"><i class="fa fa-check-circle"></i> Consultar</button>
					<div id="mensaje_descargando"> </div>
					<!-- <button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button> -->
				</div>
			</form>
		</div>
	</div>
</div>
<!-- // END contenido filtros -->

<div id="mensaje_si_vacio"></div>
<div class="well" id="well_tablas" style="visibility: hidden;">

				<div class="row border-top border-bottom innerLR">
				<div class="col-md-6">
					<div class="widget widget-heading-simple widget-body-white widget-flat">
					    <div class="widget-body ">
					        	<i class="fa fa-bar-chart-o fa-3x text-primary innerB half"> Interacciones por sección</i>
					        	
							<table class="table" id="Interacciones_todos">
							    <thead>
							        <tr>
							            <th>SECCIÓN</th>
							            <th>VISITAS</th>
							        </tr>
							    </thead>
							    <tbody>
							        
							    </tbody>
							</table>

					    </div>
					</div>
				</div>
			
					<div class="col-md-6">
					<div class="widget widget-heading-simple widget-body-white widget-flat">
					    <div class="widget-body ">
					        	<i class="fa fa-bar-chart-o fa-3x text-primary innerB half"> Usuarios únicos por sección</i>
					        	
							<table class="table" id="Interacciones_unicos">
							    <thead>
							        <tr>
							            <th>SECCIÓN</th>
							            <th>USUARIOS ÚNICOS</th>
							        </tr>
							    </thead>
							    <tbody>
							       
							    </tbody>
							</table>

					    </div>
					</div>
				</div>
			</div>

<!-- End well -->
</div>
<!-- Fin tabla  -->
<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div> 
				</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_metricas_lideres.js"></script>
</body>
</html>