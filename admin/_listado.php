<?php
    include_once('../config/config.php');
    include_once('../config/database.php');
    include_once('controllers/email.php');

    $consulta = "SELECT u.user_id, u.first_name, u.last_name, u.email, u.status_id, u.confirmed
        FROM ludus_users u WHERE u.status_id = 1 AND u.email != '' AND u.confirmed = 0";
    // $consulta = "SELECT u.user_id, u.first_name, u.last_name, u.email, u.status_id, u.confirmed
    //      FROM ludus_users u WHERE u.status_id = 1 AND u.email = 'a.vega@luduscolombia.com.co'";
    $DataBase_Acciones = new Database();
    $Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $consulta );



    if ( isset( $_GET['opcn'] ) ) {

        switch ( $_GET['opcn'] ) {
            case 'enviar':
                $mail = new EmailLudus();
                $gmacademy = md5("gmacademy");
                $ludus = md5("luduscolombia");


                foreach ($Rows_config as $key => $value) {
                    if( strpos( $value['email'], "@" ) !== false && $value['confirmed'] == 0 ){
                        $link = "<a href='https://www.gmacademy.co/admin/activar.php?a=$gmacademy&opcn=a&op=$ludus&usi={$value['user_id']}'>Activar correo aquí</a>";
                        $mensaje = "Hola {$value['first_name']} {$value['last_name']}.<br>Te invitamos a que actives tu correo registrado en la plataforma gmacademy por medio del siguiente link: $link.";
                        $resultado = $mail->EnviaEmail("Activación correo gmacademy", $value['email'], $mensaje, "");
                    }
                }
            break;
        }
    }


    $Total = "SELECT u.user_id, u.first_name, u.last_name, u.email, u.status_id, u.confirmed
        FROM ludus_users u WHERE u.status_id = 1 AND u.email != ''";
    $lista_total = $DataBase_Acciones->SQL_SelectMultipleRows( $Total );

    $con_activos = "SELECT COUNT( u.user_id ) AS numero FROM ludus_users u WHERE u.confirmed = 1 AND u.status_id = 1 AND u.email != ''";
    $activados = $DataBase_Acciones->SQL_SelectRows( $con_activos );

    $con_inactivos = "SELECT COUNT( u.user_id ) AS numero FROM ludus_users u WHERE u.confirmed = 0 AND u.status_id = 1 AND u.email != ''";
    $pendientes = $DataBase_Acciones->SQL_SelectRows( $con_inactivos );

    $numero = count( $Rows_config );
    unset( $DataBase_Acciones );
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Correos</title>
</head>
<body>
    <h2>usuarios <small><?php echo( $numero ); ?></small></h2>
    <br>
    <h3>activados <small><?php echo( $activados['numero'] ); ?></small></h3>
    <br>
    <h3>pendientes <small><?php echo( $pendientes['numero'] ); ?></small></h3>
    <hr>
    <form action="" method="get">
        <button type="submit" name="opcn" value="enviar">Enviar correo activacion</button>
    </form>
    <hr>
    <table>
        <tr>
            <th>user_id</th>
            <th>first_name</th>
            <th>last_name</th>
            <th>email</th>
            <th>status_id</th>
            <th>confirmed</th>
        </tr>
        <?php foreach ($lista_total as $key => $value): ?>
            <tr>
                <td><?php echo( $value['user_id'] ); ?></td>
                <td><?php echo( $value['first_name'] ); ?></td>
                <td><?php echo( $value['last_name'] ); ?></td>
                <td><?php echo( $value['email'] ); ?></td>
                <td><?php echo( $value['status_id'] ); ?></td>
                <td><?php echo( $value['confirmed'] ); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
