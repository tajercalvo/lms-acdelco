<?php include('src/seguridad.php'); ?>
<?php include('controllers/virtualclass.php'); ?>
<!DOCTYPE html>
<html>

<head><meta charset="gb18030">
  

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VCT - AutoTrain</title>
  <link rel="icon" type="image/png" href="https://www.acdelco.com.co/lms/template/login/images/acdelcomini.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/templateVct/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/templateVct/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="../assets/templateVct/bower_components/Ionicons/css/ionicons.min.css"> -->
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="../assets/templateVct/bower_components/jvectormap/jquery-jvectormap.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/templateVct/dist/css/AdminLTE.min.css">
  <!-- Material Design -->
  <link rel="stylesheet" href="../assets/templateVct/dist/css/bootstrap-material-design.min.css">
  <link rel="stylesheet" href="../assets/templateVct/dist/css/ripples.min.css">
  <link rel="stylesheet" href="../assets/templateVct/dist/css/MaterialAdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <!-- <link rel="stylesheet" href="../assets/templateVct/dist/css/skins/all-md-skins.min.css"> -->
  <link rel="stylesheet" href="../assets/templateVct/dist/css/mi.css?version=<?php echo md5(time()); ?>">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
  #screen-preview .OT_subscriber {
    width: 100% !important;
  }
  
  @media screen and(min-width: 1900px) {
    #screen-preview .OT_subscriber {
      width: 100% !important;
    }
  }

  #finalizar:hover {
    color: #dd4b39 !important;
  }

  .quitar_alumno:hover{
    transform: scale(1.5);
  }
</style>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

      <!-- Logo -->
    <a href="https://acdelco.com.co/lms/admin/" class="logo" style="background: #0041d8">
      <img style="width: 100%;" src="https://acdelco.com.co/wp-content/uploads/2017/09/logo.png" alt="">
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background: -webkit-linear-gradient(left, #0041d8, #0a41d3, #6B0E1E);">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <a href="index.php">
              <i class="fa fa-mail-reply-all"></i>
            </a>
          </li>
            <?php if (($DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario'])) { ?>
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-danger" id="cuantosLevantaron"></span>
                  <div class="ripple-container"></div>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Usuarios en espera</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu" id="listaManoLevantada">
                    </ul>
                  </li>
                  <li class="footer"><a id="quitarNotificaciones" href="#">Quitar todas</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <!-- <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger" id="cuantosLevantaron"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" id="levantaronMano">Notificaciones</li>
              <li>
                <ul class="menu" id="listaManoLevantada">
                  
                </ul>
              </li>
              <li class="footer"><a id="quitarNotificaciones" href="#">Quitar todas</a></li>
            </ul>
          </li> -->
              <!-- Tasks: style can be found in dropdown.less -->
            <?php } ?>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img id="mi-foto" src="../assets/images/usuarios/<?php echo $_SESSION['imagen'] ?>" class="user-image" alt="img">
                <span id="mi-nombre" class="hidden-xs"><?php echo $_SESSION['NameUsuario'] ?></span>
              </a>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
              <a style="border-left: 1px solid rgb(255,255,255, 0.1);" href="#" data-toggle="control-sidebar"><i class="fa fa-bars"></i></a>
            </li>
          </ul>
        </div>

      </nav>
    </header>

    <!-- Content Wrapper. Contains page content div perdido-->
    <div class="content-wrapper" style="margin-left: 0px;">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo ($registroConfiguracionDetail['course']); ?>
          <small> <i class="fa fa-file-powerpoint-o"></i> Presentación # <strong>1</strong>
            <?php if (($DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario'])) { ?>
              <span id="finalizar" class="btn btn-xs label-danger">Finalizar curso</span>
            <?php } ?>
          </small>
        </h1>
        <ol class="breadcrumb">
          <li><a style="color: #dd4b39;" href="#">
              <!-- <i class="fa fa-play-circle-o"></i> -->
              <img src="../assets/fileVCT/icon-contectadocon.png" style="width: 25px;">
              Conectado con </a></li>
          <li class="active"><strong style="color: #dd4b39;"><?php echo $DetallesProgramacion['first_name'] . ' ' . $DetallesProgramacion['last_name']; ?></strong></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content" style="padding-bottom: 0px;">
        <!-- Main row -->
        <div class="row">
          <!-- col chat -->
          <div class="col-md-3">
            <div id="camaras" style="text-align: right;">
              <ul style="margin: 0px; padding:0px;">
                <?php if (($DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario'])) { ?>
                  <!-- <li>
                <div class="contenedor">
                  <div id="alumno"></div>
                </div>
                <div style="text-align: center;">
                </div>
              </li> -->
                  <li>
                    <div class="contenedor">
                      <div id="profesor"></div>
                      <img id="img-profesor" style="width: 150px;" src="../assets/fileVCT/default-profile.png">
                    </div>
                  </li>
                <?php } else { ?>
                  <li>
                    <div class="contenedor">
                      <div id="valida_alumno" style="display: block"></div>
                      <div id="profesor" style="display: block"></div>
                    </div>
                    <div style="text-align: center;">
                      <!-- <span onclick="recibe();" style="cursor:pointer;"><i class="text-success fa fa-play" aria-hidden="true"></i></span> -->
                    </div>
                  </li>
                  <?php if (isset($_GET['alumnoTransmisor'])) { ?>
                    <!-- <li>
                    <div class="contenedor">
                      <div id="profesor"></div>
                      <img id="img-profesor" style="width: 100%" src="../assets/fileVCT/default-profile.png">
                    </div>
                    <div style="text-align: center;">
                    </div>
                  </li> -->
                <?php
                  } //end alumnoTransmisor
                } // ende else
                ?>
              </ul>
            </div>
            <!-- DIRECT CHAT -->
            <div style="border-top: 3px solid #E4142A;" class="box box-warning direct-chat direct-chat-warning">
              <div class="box-header with-border" style="padding: 4px;">
                <h3 class="box-title">Chat</h3>
                <small class="text-danger" id="mensajes_nuevos"></small>
                <div class="box-tools pull-right">
                  <?php if ($DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario']) { ?>
                    <span onclick="compartirPantalla();" style="cursor:pointer;"><i class="text-success fa fa-object-ungroup"></i></span>
                    <span onclick="iniciar();" id="iniciar" style="cursor:pointer;"><i class="text-success fa fa-video-camera"></i></span>

                  <?php } else { ?>
                    <?php if (isset($_GET['alumnoTransmisor'])) { ?>
                      <span id="finAlumnoTransmisor"><i class="text-danger fa fa-stop"></i></span>
                      <!-- <span onclick="iniciarAlumno();" id="iniciar" style="cursor:pointer;"><i class="text-success fa fa-video-camera"></i></span> -->
                    <?php } ?>
                    <span id="levantarMano"><i class="text-danger fa fa-hand-paper-o"></i></span>
                  <?php } ?>
                  <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts"data-widget="chat-pane-toggle">
                      <i class="fa fa-comments"></i></button>
                </button> -->
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages" id="ventana-chat">
                  <span style="display: none" id="msjId" data-msjId=""><?php echo count($comentarios) > 0 ? $comentarios[(count($comentarios) - 1)]['comments_id'] : 0; ?></span>
                  <?php foreach ($comentarios as $key => $value) { ?>
                    <!-- Message. Default to the left -->
                    <div class="direct-chat-msg <?php echo $value['chat'] ?> ">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-<?php echo $value['chat'] ?>"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></span>
                        <span class="direct-chat-timestamp pull-<?php echo $value['tiempo'] ?>"><?php echo $value['fecha'] . ' ' . $value['hora'];
                                                                                                echo $value['tiempo'] == 'left' ? ' <i class="fa fa-check text-info"></i>' : ''; ?></span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="../assets/images/usuarios/<?php echo $value['image'] ?>" alt="img">
                      <!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        <?php echo $value['comment_text']; ?>
                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->
                  <?php } ?>

                  <!-- Message to the right -->
                  <!-- <div class="direct-chat-msg right">
                  <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name pull-right">Sarah Bullock</span>
                    <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                  </div>
                  <img class="direct-chat-img" src="../assets/templateVct/dist/img/user3-128x128.jpg" alt="message img">
                  <div class="direct-chat-text">
                    You better believe it!
                  </div>
                </div> -->
                  <!-- /.direct-chat-msg -->

                </div>
                <!--/.direct-chat-messages-->
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <form id="frm_chat">
                  <div class="input-group">
                    <input type="text" id="mi-msj" name="mi-msj" placeholder="Escribe tu mensaje" class="form-control" autocomplete="off">
                    <span style="padding: 0px;" class="input-group-btn">
                      <button type="submit" style="padding: 10px; padding-bottom: 0px; border-radius: 5px;" type="button" class="btn btn-warning btn-flat"><i class="fa fa-send-o"></i></button>
                    </span>
                    <div style="display: none" id="instructor"><?php if ($DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario']) {
                                                                  echo 'si';
                                                                } else {
                                                                  echo 'no';
                                                                } ?></div>
                    </pre>
                    <audio id="sonido_chat" src="../assets/Biblioteca/sonido_chat.mp3"></audio>
                    <audio id="sonido_mensajes_nuevos" src="../assets/Biblioteca/sonido_chat.mp3"></audio>
                  </div>
                </form>
              </div>
              <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
          </div>
          <!--/col chat -->
          <!-- Left col -->
          <div class="col-md-9">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
              <!-- <div class="box-header with-border">
              <h3 class="box-title">Diapositiva 1</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div> -->
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="row">
                  <div class="col-md-12">
                    <div id="screen-preview"></div>
                    <div id="diapositiva_actual" style="box-shadow: 0 1px 6px 0 rgba(0,0,0,0.12), 0 1px 6px 0 rgba(0,0,0,0.12);">
                      <img style="width: 100%; max-height: 721px;" src="../assets/guiaAulaVirtual.jpg" alt="">
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
            <!-- /.row -->
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <input type="hidden" id="sessionid" name="sessionid" value="<?php echo $DetallesProgramacion['session_id']?>">
    <input type="hidden" id="token" name="token" value="<?php echo $DetallesProgramacion['token']?>">
    <footer class="main-footer" style="margin-left: 0px;">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
    <strong><a style="color: black;" href="https://acdelco.com.co" target="_blank">Training  Academy ACDelco &copy; 2020</a></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs" style="background: #6B0E1E;">
      <li style="border-right: 0.5px solid #721;"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-users"></i></a></li>
        <?php if ($DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario']) {
          $display = 'block';
        } else {
          $display = 'none';
        } ?>
        <li><a style="display: <?php echo $display ?>" href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-file-powerpoint-o"></i></a></li>
      </ul>
      <!-- Tab panes -->
      <div id="content-dia-user" class="tab-content" style="overflow-y: auto;">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
          <span class="control-sidebar-heading"><strong id="cant_conectados"> <?php echo count($registrados) ?> </strong> Usuarios Conectados</span>
          <ul class="control-sidebar-menu" id="usuarios_conectados">
            <?php foreach ($registrados as $key => $value) { ?>
              <li>
                <a href="javascript:void(0)">
                  <img class="direct-chat-img" src="../assets/images/usuarios/<?php echo $value['image'] ?>" alt="img">
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></h4>
                    <p><i class="fa fa-circle text-green"></i> <?php echo $value['headquarter'] ?></p>
                  </div>
                </a>
              </li>
            <?php } ?>
          </ul>
          <!-- /.control-sidebar-menu -->
        </div>
        <!-- /.tab-pane -->

        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
          <h3 class="control-sidebar-heading">Diapositivas</h3>
          <!-- diapositivas -->
          <ul class="products-list product-list-in-box">
            <?php foreach ($diapositivas as $key => $value) { ?>
              <li class="item mini-diap" id="diapo_<?php echo $value['coursefile_id'] ?>" data-diapositiva="<?php echo $value['coursefile_id'] ?>" data-image="<?php echo $value['image'] ?>" data-archivo="<?php echo $value['archivo'] ?>" data-ruta="../assets/fileVCT/VCT_<?php echo $value['course_id'] . '/' ?>" data-question_id="<?php echo $value['question_id'] ?>">
                <div class="product-img">
                  <img src="<?php echo $value['imagen'] ?>" alt="img">
                </div>
                <div class="product-info">
                  <span class="product-title"><?php echo $value['file'] ?> -
                    <?php if (isset($value['preg'])) { ?>
                      <a class="ver" href="<?php echo $value['preg'] . '&schedule=' . $_GET['schedule'] ?>" target="_blank">Ver</a>
                    <?php } else { ?>
                      <a class="ver" href="../assets/fileVCT/VCT_<?php echo $value['course_id'] ?>/<?php echo $value['archivo'] ?>" target="_blank">Ver</a>
                    <?php } ?>
                  </span>
                  <span class="product-description"><?php echo $value['name'] ?></span>
                </div>
              </li>
            <?php } ?>

          </ul>
          <!-- /diapositivas -->
        </div>
        <!-- /.tab-pane -->
      </div>
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="../assets/templateVct/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="../assets/templateVct/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Material Design -->
  <script src="../assets/templateVct/dist/js/material.min.js"></script>
  <script src="../assets/templateVct/dist/js/ripples.min.js"></script>
  <script>
    $.material.init();
  </script>
  <script src="../assets/templateVct/dist/js/adminlte.min.js"></script>
  <script src="//static.opentok.com/v2/js/opentok.js"></script>
  <script src="js/virtualclass.js?version=<?php echo md5(time()); ?>"></script>
</body>

</html>