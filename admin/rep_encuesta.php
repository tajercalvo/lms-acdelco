<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_encuesta.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<div id="cargando_pagina"> <span class="label label-success">Cargando elementos, por favor espere...</span><img style="width: 15px; " src="../assets/loading-course.gif"> </div>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left" style="visibility: hidden;" id="mibody">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_encuesta.php">Reporte de Encuestas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Encuestas</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de las encuestas y el rango de fechas.</h5></br>
								</div>
								<div class="col-md-2">

									<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion" enctype="multipart/form-data">
										<h5 id="descargar"><a href="" class="glyphicons cloud-upload" onclick="return false;"><i></i>Descargar</a><br></h5>
										<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Reporte encuentas" />
										<input type="hidden" id="datos_tabla" name="datos_tabla" />
									</form>
									<div id="mensaje_descargando" ></div>
								</div>

							</div>
							<div class="separator bottom"></div>
							<div class="row">
								<form action="rep_encuesta.php" method="post">
								<div class="col-md-5">
									<!-- Group -->
									<div class="form-group">
										<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
										<div class="col-md-7 input-group date">
									    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
									    	<span class="input-group-addon">
									    		<i class="fa fa-th"></i>
									    	</span>

										</div>
									</div>
									<!-- // Group END -->
								</div>
								<div class="col-md-5">
									<!-- Group -->
									<div class="form-group">
										<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Final:</label>
										<div class="col-md-7 input-group date">
									    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
									    	<span class="input-group-addon">
									    		<i class="fa fa-th"></i>
									    	</span>
										</div>
									</div>
									<!-- // Group END -->
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
								</div>
								<div class="col-md-10">
									<!-- Group -->
									<div class="form-group">
										<label class="col-md-2 control-label" for="review_id" style="padding-top:8px;">Evaluación:</label>
											<div class="col-md-10 input-group date">
												<select style="width: 100%;" id="review_id" name="review_id">
													<?php foreach ($reviews_active as $key => $Data_ActiveReview) {
														?>
														<option value="<?php echo $Data_ActiveReview['review_id']; ?>" <?php if(isset($_POST['review_id']) && ($_POST['review_id']==$Data_ActiveReview['review_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $Data_ActiveReview['review']; ?></option>
													<?php } ?>
									        	</select>
									        </div>
									</div>
									<!-- // Group END -->
								</div>
								<div class="col-md-2">
								</div>
								</form>
							</div>
						</div>
					</div>
					<?php if( (isset($_SESSION['max_rol']) && $_SESSION['max_rol']>4) && isset($_POST['review_id']) && ($_POST['review_id']=="2") ){
						$var_1 = 0;
						$var_2 = 0;
						$var_3 = 0;
						$var_4 = 0;
						$var_5 = 0;
						$var_6 = 0;
						$var_7 = 0;
						$var_8 = 0;
						$var_9 = 0;
						$var_10 = 0;
						?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td style="width: 50%;">
											<span class="text-center">&nbsp; <i class='fa fa-fw fa-tasks'></i> Información</span><br><br>
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">Preguntas</th>
														<th class="center" colspan="<?php echo(count($PreguntasOpcionesEncuestas[0]['Resp'])); ?>">Respuestas / Resultados</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
												<?php
												foreach ($PreguntasOpcionesEncuestas as $iID => $PregData) { ?>
													<tr class="selectable">
														<td style="padding: 2px; font-size: 80%;" align="left"><?php echo($PregData['question']); ?></td>
														<?php foreach ($PregData['Resp'] as $iID => $RespData) {
															if($RespData['answer']=="1"){
																if(isset($RespData['CantResp'])){
																	$var_1 = $var_1 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="2"){
																if(isset($RespData['CantResp'])){
																	$var_2 = $var_2 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="3"){
																if(isset($RespData['CantResp'])){
																	$var_3 = $var_3 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="4"){
																if(isset($RespData['CantResp'])){
																	$var_4 = $var_4 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="5"){
																if(isset($RespData['CantResp'])){
																	$var_5 = $var_5 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="6"){
																if(isset($RespData['CantResp'])){
																	$var_6 = $var_6 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="7"){
																if(isset($RespData['CantResp'])){
																	$var_7 = $var_7 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="8"){
																if(isset($RespData['CantResp'])){
																	$var_8 = $var_8 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="9"){
																if(isset($RespData['CantResp'])){
																	$var_9 = $var_9 + $RespData['CantResp'][0]['Cantidad'];
																}
															}elseif($RespData['answer']=="10"){
																if(isset($RespData['CantResp'])){
																	$var_10 = $var_10 + $RespData['CantResp'][0]['Cantidad'];
																}
															}
															?>
															<td class="center important" style="padding: 2px; font-size: 80%;">
																<strong class="text-success"><?php echo($RespData['answer']); ?></strong><br/>
																<span class="text-danger"><?php if(isset($RespData['CantResp'])){ echo($RespData['CantResp'][0]['Cantidad']); }else{ echo("0"); } ?></span><br/>
																<!--<input class="" type="hidden" name="" value="<?php echo($RespData['answer']); ?>" />-->
															</td>
														<?php } ?>
													</tr>
													<tr class="selectable">
														<td></td>
													</tr>
												<?php } ?>
													<tr class="selectable">
														<td style="padding: 2px; font-size: 80%;" align="left">Total</td>
														<td><strong class="text-inverse"><?php echo($var_1); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_2); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_3); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_4); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_5); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_6); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_7); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_8); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_9); ?></strong></td>
														<td><strong class="text-inverse"><?php echo($var_10); ?></strong></td>
														<input type="hidden" name="TotValues" value="TotValues" id="TotValues" dat-1="<?php echo($var_1); ?>" dat-2="<?php echo($var_2); ?>" dat-3="<?php echo($var_3); ?>" dat-4="<?php echo($var_4); ?>" dat-5="<?php echo($var_5); ?>" dat-6="<?php echo($var_6); ?>" dat-7="<?php echo($var_7); ?>" dat-8="<?php echo($var_8); ?>" dat-9="<?php echo($var_9); ?>" dat-10="<?php echo($var_10); ?>" />
													</tr>
												</tbody>
											</table>
										</td>
										<td style="width: 50%;">
											<span class="text-center">&nbsp; <i class='fa fa-fw fa-bar-chart-o'></i> Comportamiento General</span>
											<div id="GrafGral" class="flotchart-holder" style="height: 300px;"></div>
											<!--<span class="text-center">&nbsp; <i class='fa fa-fw fa-bar-chart-o'></i> Comportamiento Detallado</span>
											<div id="GrafDetalle" class="flotchart-holder" style="height: 300px;"></div>-->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>

					<?php
				if($cantidad_datos > 0){
						foreach ($datosCursos_all as $iID => $data) { ?>
							<div class="well">
								<table class="table table-invoice">
									<tbody>
										<tr>
											<td style="width: 30%;">
												<p class="lead">Evaluado</p>
												<h4>COD: <?php echo($data['code']); ?><br/><?php echo($data['review']); ?></h4>
												<address class="margin-none">
													<strong><?php echo($data['identification']); ?> | <?php echo($data['first_name'].' '.$data['last_name']); ?></strong><br/>
													<strong>Concesionario: </strong><?php echo($data['dealer']); ?><br/>
													<strong>Sede: </strong><?php echo($data['headquarter']); ?><br/>
													<!--<strong>Zona: </strong><?php //echo($data['zone']); ?><br/>-->
													<strong>Fecha: </strong><?php echo($data['date_creation']); ?><br/><br/>
													<!--<strong>Tiempo: </strong><?php echo($data['time_response']); ?><br/><br/>-->
													<?php if(isset($data['course'])){ ?>
														<strong>Código: </strong><?php echo($data['code_curso']); ?><br/>
														<strong>Curso: </strong><?php echo($data['course']); ?>
													<?php } ?>
													<?php if( isset($data['teacher_name']) && isset($data['teacher_last']) ){ ?>
														<br/><strong>Instructor: </strong><?php echo($data['teacher_name']." ".$data['teacher_last']); ?>
													<?php } ?>
													<?php if(isset($data['start_date']) ){ ?>
														<br/><strong>Fecha Curso: </strong><?php echo($data['start_date']); ?>
													<?php }?>
													<?php if(isset($data['review_id']) && ($data['review_id']==2) ){ ?>
														<br/><strong>Puntos Obtenidos: </strong><?php echo($data['score']); ?>
													<?php }?>
												</address>
											</td>
											<td class="right">
												<p class="lead">Respuestas</p>
												<!-- // Table -->
												<table class="table table-condensed table-vertical-center table-thead-simple">
													<thead>
														<tr>
															<th class="center">Pregunta</th>
															<th class="center">Respuesta</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">
														<?php
														if(isset($data['respuestas'])){
															$var_Ctrl = 0;
															foreach ($data['respuestas'] as $iID => $dataRespuestas) {
														?>
															<!-- Item -->
															<tr class="selectable">
																<td style="padding: 2px; font-size: 80%;" align="left"><?php echo($dataRespuestas['code']); ?> <?php echo($dataRespuestas['question']); ?></td>
																<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($dataRespuestas['answer']); ?></td>
															</tr>
															<!-- // Item END -->
														<?php
															$puntaje = 0;
																}
														}
														?>
														<?php
														if(isset($data['respuestasAbiertas'])){
															foreach ($data['respuestasAbiertas'] as $iID => $dataRespuestas) {  ?>
															<!-- Item -->
															<tr class="selectable">
																<td style="padding: 2px; font-size: 80%;" align="left"><?php echo($dataRespuestas['code']); ?> <?php echo($dataRespuestas['question']); ?></td>
																<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($dataRespuestas['result']); ?></td>
															</tr>
															<!-- // Item END --><?php
														}
													} ?>
													</tbody>
												</table>
												<!-- // Table END -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php } } ?>

					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_evaluacion.js"></script>
</body>
</html>
