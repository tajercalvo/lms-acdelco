<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}
}
include('controllers/dashboard_new.php');
$location = 'home';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<style media="screen">
		.widget-stats-gray{
			background: #1552c1 !important;
		}
	</style>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb loader_s">
					<li>Estás aquí</li>
					<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Dashboard</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<!-- <h2 class="margin-none pull-left">Dashboard </h2> -->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<div class="widget" data-toggle="collapse-widget" >
						<div class="widget-head"><h4 class="heading">Filtros</h4></div>
						<div class="widget-body">
							<form action="" id="form_dashboard" name="form_dashboard" method="post">
								<div class="row">
									<!-- Group -->
									<!-- ==================================================== -->
									<!-- lista de quarters/meses -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="start_date_day" style="padding-top:8px;">Fecha:</label>
											<div class="col-md-7 input-group date">
												<select multiple="multiple" style="width: 100%;" id="fecha" name="fecha[]" >
													<option value="0">Todos</option>
													<option value="1">Enero</option>
													<option value="2">Febrero</option>
													<option value="3">Marzo</option>
													<option value="4">Abril</option>
													<option value="5">Mayo</option>
													<option value="6">Junio</option>
													<option value="7">Julio</option>
													<option value="8">Agosto</option>
													<option value="9">Septiembre</option>
													<option value="10">Octubre</option>
													<option value="11">Noviembre</option>
													<option value="12">Diciembre</option>
												</select>
											</div>
										</div>
									</div>
									<!-- ==================================================== -->
									<!-- fin lista de quarters/meses -->
									<!-- ==================================================== -->
									<!-- // Group END -->
									<!-- ==================================================== -->
									<!-- select para seleccionar las zonas -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<div class="form-group" <?php if( isset($zona_sel) ){ echo 'style="display: none;"'; } ?>>
											<label class="col-md-4 control-label" for="zonas" style="padding-top:8px;">Zonas:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="zonas" name="zonas[]" >
													<optgroup label="Zonas">
														<?php foreach ($zonas as $key => $vZona): ?>
															<option value="<?php echo $vZona['zone_id'] ?>" <?php if( isset($zona_sel) && $zona_sel['zone_id'] == $vZona['zone_id'] ){ echo "selected"; } ?>><?php echo $vZona['zone'] ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<!-- ==================================================== -->
									<!-- END select para seleccionar las zonas -->
									<!-- ==================================================== -->
									<!-- // Group END -->
									<!-- ==================================================== -->
									<!-- select para seleccionar los concesionarios -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<div class="form-group" <?php if( isset($zona_sel) ){ echo 'style="display: none;"'; } ?>>
											<label class="col-md-4 control-label" for="concesionarios" style="padding-top:8px;">Concesionarios:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="concesionarios" name="concesionarios[]" >
													<optgroup label="Concesionarios" id="dataConcs">
														<?php if ( isset($zona_sel['zone_id']) ): ?>
															<?php foreach ($concesionarios as $key => $vCns): ?>
																<option value="<?php echo $vCns['dealer_id'] ?>" <?php echo $vCns['dealer_id'] == $_SESSION['dealer_id'] ? "selected" : "" ?>><?php echo $vCns['dealer'] ?></option>
															<?php endforeach; ?>
														<?php endif; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<!-- ==================================================== -->
									<!-- END select para seleccionar los concesionarios -->
									<!-- ==================================================== -->
								</div>
								<div class="row">
									<div class="col-md-2">
										<button type="submit" class="btn btn-success" id="consultaAnalytics"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
									<div class="col-md-2">
										<h5 style="display:none"><a href="" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									</div>
								</div>
							</form>
						</div>
					</div>

					<!-- Menu y titulo -->
					<div class="innerAll">
						<h1 class="margin-none pull-left">Dashboard &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h1>
						<div class="clearfix"></div>
					</div>
					<!-- fin menu y titulo -->

					<div class="well">

						<!-- Row 1.2 -->
						<!-- Historico PAC -->
						<div class="row">
							<div class="col-md-12">
								<div class="box box-danger">
									<div class="box-header with-border">
										<h3 class="box-title">Resultados de gestión GMAcademy <small>Muestra el cumplimiento mensual y trimestral del resultado de gestión</small></h3>
									</div>
									<br>
									<!-- ==================================================== -->
									<!-- Historico body -->
									<div class="box-body">
										<div class="row">
											<div class="col-md-12">

												<!-- ==================================================== -->
												<!-- Quarters body -->
												<!-- ==================================================== -->
												<div class="row">
													<div class="col-md-3 border-right innerAll">
														<div class=" padding-bottom-none-phone">
															<!-- Stats Widget -->
															<a href="" class="widget-stats widget-stats-default widget-stats-4">
																<span class="txt">Cumplimiento</span>
																<span class="count" style="font-size: 40px" id="q1">0</span>
																<span style="float: right;position: relative;top: auto;right: auto;font-size: 30px;font-weight: 700;padding: 0 0 3px;display: block;line-height: 55px;"> Q1</span>
															</a>
															<!-- // Stats Widget END -->
														</div>
													</div>
													<div class="col-md-3 border-right innerAll">
														<div class=" padding-bottom-none-phone">
															<!-- Stats Widget -->
															<a href="" class="widget-stats widget-stats-primary widget-stats-4">
																<span class="txt">Cumplimiento</span>
																<span class="count" style="font-size: 40px" id="q2">0</span>
																<span style="float: right;position: relative;top: auto;right: auto;font-size: 30px;font-weight: 700;padding: 0 0 3px;display: block;line-height: 55px;"> Q2</span>
															</a>
															<!-- // Stats Widget END -->
														</div>
													</div>
													<div class="col-md-3 border-right innerAll">
														<div class=" padding-bottom-none-phone">
															<!-- Stats Widget -->
															<a href="" class="widget-stats widget-stats-gray widget-stats-4">
																<span class="txt" style="color: white;">Cumplimiento</span>
																<span class="count" style="font-size: 40px;color: white;" id="q3">0</span>
																<span style="float: right;position: relative;top: auto;right: auto;font-size: 30px;font-weight: 700;padding: 0 0 3px;display: block;line-height: 55px;color: white;"> Q3</span>
															</a>
															<!-- // Stats Widget END -->
														</div>
													</div>
													<div class="col-md-3 border-right innerAll">
														<div class="padding-bottom-none-phone">
															<!-- Stats Widget -->
															<a href="" class="widget-stats widget-stats-inverse widget-stats-4">
																<span class="txt">Cumplimiento</span>
																<span class="count" style="font-size: 40px" id="q4">0</span>
																<span style="float: right;position: relative;top: auto;right: auto;font-size: 30px;font-weight: 700;padding: 0 0 3px;display: block;line-height: 55px;"> Q4</span>
															</a>
															<!-- // Stats Widget END -->
														</div>
													</div>
												</div>
												<!-- ==================================================== -->
												<!-- END Quarters body -->
												<!-- ==================================================== -->
												<br>
												<!-- ==================================================== -->
												<!-- Meses body -->
												<!-- ==================================================== -->
												<div class="row">
													<div class="col-md-12 text-center" style="padding: 5px 0">
														<table class="table table-bordered table-primary" style="width: 100%">
															<thead>
																<tr>
																	<th class="text-center">/</th>
																	<th><h4 class="text-center">Ene</h4></th>
																	<th><h4 class="text-center">Feb</h4></th>
																	<th><h4 class="text-center">Mar</h4></th>
																	<th><h4 class="text-center">Abr</h4></th>
																	<th><h4 class="text-center">May</h4></th>
																	<th><h4 class="text-center">Jun</h4></th>
																	<th><h4 class="text-center">Jul</h4></th>
																	<th><h4 class="text-center">Ago</h4></th>
																	<th><h4 class="text-center">Sep</h4></th>
																	<th><h4 class="text-center">Oct</h4></th>
																	<th><h4 class="text-center">Nov</h4></th>
																	<th><h4 class="text-center">Dic</h4></th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>%</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_1"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_2"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_3"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_4"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_5"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_6"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_7"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_8"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_9"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_10"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_11"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cump_mes_12"></p>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>Cupos</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_1"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_2"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_3"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_4"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_5"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_6"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_7"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_8"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_9"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_10"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_11"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="cupos_mes_12"></p>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>Asistentes</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_1"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_2"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_3"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_4"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_5"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_6"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_7"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_8"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_9"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_10"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_11"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="asistentes_mes_12"></p>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td>Excusas</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_1"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_2"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_3"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_4"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_5"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_6"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_7"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_8"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_9"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_10"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_11"></p>
																		</div>
																	</td>
																	<td>
																		<div class="text-center">
																			<p class="lead strong margin-bottom-none" id="excusas_mes_12"></p>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>

													</div>
												</div>
												<!-- ==================================================== -->
												<!-- END Meses body -->
												<!-- ==================================================== -->
											</div>
										</div>
									</div>
									<!-- Historico body -->
									<!-- ==================================================== -->
								</div>
							</div>

						</div>
						<!-- END Historico PAC -->

						<br>
						<!-- Row 2 -->
						<!-- Población -->
						<div class="row">
							<div class="col-md-12">
								<!-- ==================================================== -->
								<!-- box Poblacion -->
								<div class="box box-danger">
									<div class="box-header with-border">
										<h3 class="box-title">Población <small>Resumen de los usuarios, y la cantidad de usuarios por trayectorias</small></h3>
									</div>
									<br>
									<!-- ==================================================== -->
									<!-- Poblacion body -->
									<div class="box-body">
										<div class="row">
											<!-- ==================================================== -->
											<!-- total de usuarios por Zonas/Concesionario -->
											<!-- ==================================================== -->
											<div class="col-md-4">
												<div class="row">
													<!-- ==================================================== -->
													<!-- total de usuarios activos en la plataforma segun zona/concesionario -->
													<!-- ==================================================== -->
													<div class="col-md-12">
														<div class="padding-bottom-none-phone">
															<!-- Stats Widget -->
															<a href="#" class="widget-stats widget-stats-primary widget-stats-4">
																<span class="txt">Total de Usuarios</span>
																<span class="count" id="total_usuarios"></span>
																<span class="glyphicons group"><i></i></span>
																<div class="clearfix"></div>
																<i class="icon-play-circle"></i>
															</a>
															<!-- // Stats Widget END -->
														</div>
													</div>
													<!-- ==================================================== -->
													<!-- END total de usuarios activos en la plataforma segun zona/concesionario -->
													<!-- ==================================================== -->
													<!-- // -->
													<!-- ==================================================== -->
													<!-- total de usuarios con trayectorias, se repiten si tienen mas de una trayectoria -->
													<!-- ==================================================== -->
													<div class="col-md-12">
														<div class="padding-bottom-none-phone">
															<!-- Stats Widget -->
															<a href="#" class="widget-stats widget-stats-inverse widget-stats-4">
																<span class="txt">Total de Usuarios con trayectoria</span>
																<span class="count text-primary" id="total_trayectorias_usr"></span>
																<span class="glyphicons group"><i></i></span>
																<div class="clearfix"></div>
																<i class="icon-play-circle"></i>
															</a>
															<!-- // Stats Widget END -->
														</div>
													</div>
													<!-- ==================================================== -->
													<!-- END total de usuarios con trayectorias, se repiten si tienen mas de una trayectoria -->
													<!-- ==================================================== -->
													<!-- // -->
													<!-- ==================================================== -->
													<!-- total de usuarios con trayectorias -->
													<!-- ==================================================== -->
													<!-- <div class="col-md-12">
														<div class="padding-bottom-none-phone">
															<a href="#" class="widget-stats widget-stats-default widget-stats-4">
																<span class="txt">Usuarios con trayectorias</span>
																<span class="count text-primary" id="total_usuarios_trayectorias"></span>
																<span class="glyphicons group"><i></i></span>
																<div class="clearfix"></div>
																<i class="icon-play-circle"></i>
															</a>
														</div>
													</div> -->
													<!-- ==================================================== -->
													<!-- END total de usuarios con trayectorias -->
													<!-- ==================================================== -->
													<!-- // -->
												</div>
											</div>
											<!-- ==================================================== -->
											<!-- END total de usuarios por Zonas/Concesionario -->
											<!-- ==================================================== -->
											<!-- // Group END -->
											<!-- ==================================================== -->
											<!-- Tablas para mostrar los usuarios por trayectorias -->
											<!-- ==================================================== -->
											<div class="col-md-4">
												<table class="table">
													<tbody id="tabla1">
													</tbody>
												</table>
											</div>
											<div class="col-md-4">
												<table class="table">
													<tbody id="tabla2">
													</tbody>
												</table>
											</div>
											<!-- ==================================================== -->
											<!-- END Tablas para mostrar los usuarios por trayectorias -->
											<!-- ==================================================== -->
										</div>
									</div>
									<!-- END Poblacion body -->
									<!-- ==================================================== -->
								</div>
								<!-- END box Poblacion -->
								<!-- ==================================================== -->

							</div>
						</div>
						<!-- END Población -->

						<!-- <br> -->
						<!-- Row 3 -->
						<!-- Estudiantes -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="box box-danger">
										<div class="box-header with-border">
											<h3 class="box-title">Estudiantes <small>Total de estudiantes vs participaciones</small></h3>
										</div>
										<div class="box-body">
										</div>
									</div>
								</div>
							</div>
						</div> -->
						<!-- END Estudiantes -->

						<br>
						<!-- Row 4 -->
						<!-- Participación -->
						<div class="row">
							<div class="col-md-12">
								<div class="box box-danger">
									<div class="box-header with-border">
										<h3 class="box-title">Participación <small>Muestra la cantidad de cupos y su asistencia consolidado o por especialidad</small></h3>
									</div>
									<br>
									<div class="box-body">
										<div class="row">
											<div class="col-md-5">
												<canvas id="gfFunnelParticipacion" height="250"></canvas>
											</div>
											<div class="col-md-1"></div>
											<div class="col-md-6">
												<div class="row">
													<div class="col-md-6">
														<h4>Producto</h4>
														<canvas id="gfFunnelParticipacion_Producto" height="200"></canvas>
													</div>
													<div class="col-md-6">
														<h4>Habilidades Técnicas</h4>
														<canvas id="gfFunnelParticipacion_HTecticas" height="200"></canvas>
													</div>
													<div class="col-md-6">
														<h4>Habilidades Blandas</h4>
														<canvas id="gfFunnelParticipacion_HBlandas" height="200"></canvas>
													</div>
													<div class="col-md-6">
														<h4>Procesos de marca</h4>
														<canvas id="gfFunnelParticipacion_Marca" height="200"></canvas>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						<!-- END Participación -->

						<br>
						<!-- Row 5 -->
						<!-- Horas de capacitación -->
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="box box-danger">
										<div class="box-header with-border">
											<h3 class="box-title">Horas de capacitación <small>Promedio de horas hombre en capacitación</small></h3>
										</div>
										<!-- ======================================================= -->
										<!-- Filtros Horas de capacitación -->
										<!-- ======================================================= -->
										<div class="box-body">
											<br>
											<!-- Row -->
											<div class="row">
												<!-- ======================================================= -->
												<!-- Capacitación: General -->
												<!-- ======================================================= -->
												<div class="col-md-6">
													<div class="col-md-2"></div>
													<div class="col-md-8">
														<div class="padding-bottom-none-phone">
															<!-- Stats Widget -->
															<a href="#" class="widget-stats widget-stats-primary widget-stats-4">
																<span class="txt">Promedio Horas Total</span>
																<span class="count" id="p_hor"></span>
																<span class="glyphicons user"><i></i></span>
																<div class="clearfix"></div>
																<i class="icon-play-circle"></i>
															</a>
															<!-- // Stats Widget END -->
														</div>
														<table class="table table-striped">
															<tr>
																<th>Asistentes: <span id="h_lab" class="text-primary"></span></th>
																<th>Horas Capacitación: <span id="h_cap" class="text-primary"></span> </th>
															</tr>
														</table>
													</div>
													<div class="col-md-2"></div>
												</div>
												<!-- ======================================================= -->
												<!-- END Capacitación: General -->
												<!-- ======================================================= -->

												<!-- ======================================================= -->
												<!-- Capacitación: Tablas -->
												<!-- ======================================================= -->
												<div class="col-md-6">
													<div class="row">
														<div class="col-md-12">
															<div class="row">
																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->
																<div class="col-md-6">
																	<div class="col-md-12">
																		<div class="padding-bottom-none-phone">
																			<!-- Stats Widget -->
																			<a href="#" class="widget-stats widget-stats-default widget-stats-4">
																				<span class="txt">Promedio Horas Hab. Tecnicas</span>
																				<span class="count" id="p_t_hor"></span>
																				<span class="glyphicons user"><i></i></span>
																				<div class="clearfix"></div>
																				<i class="icon-play-circle"></i>
																			</a>
																			<!-- // Stats Widget END -->
																		</div>
																		<table class="table table-striped">
																			<tr>
																				<th>Asistentes: <span id="h_t_lab" class="text-primary"></span></th>
																				<th>Horas Capacitación: <span id="h_t_cap" class="text-primary"></span> </th>
																			</tr>
																		</table>
																	</div>
																</div>
																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->

																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->
																<div class="col-md-6">
																	<div class="col-md-12">
																		<div class="padding-bottom-none-phone">
																			<!-- Stats Widget -->
																			<a href="#" class="widget-stats widget-stats-inverse widget-stats-4">
																				<span class="txt">Promedio Horas Hab. Blandas</span>
																				<span class="count" id="p_b_hor"></span>
																				<span class="glyphicons user"><i></i></span>
																				<div class="clearfix"></div>
																				<i class="icon-play-circle"></i>
																			</a>
																			<!-- // Stats Widget END -->
																		</div>
																		<table class="table table-striped">
																			<tr>
																				<th>Asistentes: <span id="h_b_lab" class="text-primary"></span></th>
																				<th>Horas Capacitación: <span id="h_b_cap" class="text-primary"></span> </th>
																			</tr>
																		</table>
																	</div>
																</div>
																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->
																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->
																<div class="col-md-6">
																	<div class="col-md-12">
																		<div class="padding-bottom-none-phone">
																			<!-- Stats Widget -->
																			<a href="#" class="widget-stats widget-stats-inverse widget-stats-4">
																				<span class="txt">Promedio Horas Producto</span>
																				<span class="count" id="p_p_hor"></span>
																				<span class="glyphicons user"><i></i></span>
																				<div class="clearfix"></div>
																				<i class="icon-play-circle"></i>
																			</a>
																			<!-- // Stats Widget END -->
																		</div>
																		<table class="table table-striped">
																			<tr>
																				<th>Asistentes: <span id="h_p_lab" class="text-primary"></span></th>
																				<th>Horas Capacitación: <span id="h_p_cap" class="text-primary"></span> </th>
																			</tr>
																		</table>
																	</div>
																</div>
																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->

																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->
																<div class="col-md-6">
																	<div class="col-md-12">
																		<div class="padding-bottom-none-phone">
																			<!-- Stats Widget -->
																			<a href="#" class="widget-stats widget-stats-default widget-stats-4">
																				<span class="txt">Promedio Horas Marca</span>
																				<span class="count" id="p_m_hor"></span>
																				<span class="glyphicons user"><i></i></span>
																				<div class="clearfix"></div>
																				<i class="icon-play-circle"></i>
																			</a>
																			<!-- // Stats Widget END -->
																		</div>
																		<table class="table table-striped">
																			<tr>
																				<th>Asistentes: <span id="h_m_lab" class="text-primary"></span></th>
																				<th>Horas Capacitación: <span id="h_m_cap" class="text-primary"></span> </th>
																			</tr>
																		</table>
																	</div>
																</div>
																<!-- ======================================================= -->
																<!-- Capacitación: Tablas -->
																<!-- ======================================================= -->
															</div>
														</div>
													</div>
												</div>
												<!-- ======================================================= -->
												<!-- END Capacitación: General -->
												<!-- ======================================================= -->
											</div>
											<!-- END Row -->
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- END Horas de capacitación -->

						<br>
						<!-- Row 6 -->
						<!-- Notas por Zona -->
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="box box-danger">
										<div class="box-header with-border">
											<h3 class="box-title">Notas por Zona <small>Comparativo de notas en la zona, indicando promedio general y promedio de zona</small></h3>
										</div>
										<!-- ======================================================= -->
										<!-- Filtros Notas por Zona -->
										<!-- ======================================================= -->
										<div class="box-body">
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<!-- ==================================================== -->
													<!-- Formulario metodologia  -->
													<!-- ==================================================== -->
													<form id="form_hab_ccs">
														<div class="btn-group" data-toggle="buttons">
														  	<label class="btn btn-primary active resetear">
														    	<input class="hab_ccs" type="radio" name="options_hab" value="all" checked="checked"> Todos
														  	</label>
														  	<label class="btn btn-primary">
														    	<input class="hab_ccs" type="radio" name="options_hab" value="1">HABILIDADES TÉCNICAS
														  	</label>
														  	<label class="btn btn-primary">
														    	<input class="hab_ccs" type="radio" name="options_hab" value="2">HABILIDADES BLANDAS
														  	</label>
														  	<label class="btn btn-primary">
														    	<input class="hab_ccs" type="radio" name="options_hab" value="3">PRODUCTO
														  	</label>
														  	<label class="btn btn-primary">
														    	<input class="hab_ccs" type="radio" name="options_hab" value="4">PROCESOS DE MARCA
														  	</label>
														</div>
														<button type="submit" name="button" style="display:none" id="btn_hab_ccs"></button>
													</form>
													<!-- ==================================================== -->
													<!-- END Formulario metodologia  -->
													<!-- ==================================================== -->

													<!-- ==================================================== -->
													<!-- Grafica  -->
													<!-- ==================================================== -->
													<canvas id="promNotasCcs" style="height:250px !important"></canvas>
													<!-- ==================================================== -->
													<!-- END Grafica  -->
													<!-- ==================================================== -->
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
										<!-- ======================================================= -->
										<!-- END Filtros Notas por Zona -->
										<!-- ======================================================= -->
									</div>
								</div>
							</div>
						</div>
						<!-- END Notas por Zona -->

						<br>
						<!-- Row 7 -->
						<!-- IFE -->
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="box box-danger">
										<div class="box-header with-border">
											<h3 class="box-title">IFE - Índice de Felicidad del Estudiante <small>Valor promotor neto de las encuestas de satisfacción</small></h3>
										</div>
										<!-- ======================================================= -->
										<!-- Filtros IFE -->
										<!-- ======================================================= -->
										<div class="box-body">
											<form id="form_ife">
												<div class="btn-group" data-toggle="buttons">
												  	<label class="btn btn-primary active resetear">
												    	<input class="h_ife" type="radio" name="options_ife" value="todos" checked="checked"> Todos
												  	</label>
												  	<label class="btn btn-primary">
												    	<input class="h_ife" type="radio" name="options_ife" value="VCT">VCT
												  	</label>
												  	<label class="btn btn-primary">
												    	<input class="h_ife" type="radio" name="options_ife" value="IBT">IBT
												  	</label>
												  	<!-- <label class="btn btn-primary">
												    	<input class="h_ife" type="radio" name="options_ife" value="WBT">WBT
												  	</label> -->
												  	<label class="btn btn-primary">
												    	<input class="h_ife" type="radio" name="options_ife" value="OJT">OJT
												  	</label>
												</div>
												<button type="submit" name="button" style="display:none" id="btn_ife"></button>
											</form>
										</div>
										<!-- ======================================================= -->
										<!-- END Filtros IFE -->
										<!-- ======================================================= -->
									</div>
								</div>
								<div class="row">
									<!-- ==================================================== -->
									<!-- Satisfacción General con GMAcademy  -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<table class="table">
											<thead>
												<tr>
													<th>Satisfacción General con GMAcademy</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<div class="text-center innerAll inner-2x">
															<p class="margin-none">
																<img src="../assets/logo.png" style="width: 110px; height:100px; margin-bottom: 15px" alt="">
															</p>
															<p class="lead margin-none strong" id="satisfaccion_gral">0</p>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!-- ==================================================== -->
									<!-- END Satisfacción General con GMAcademy  -->
									<!-- ==================================================== -->

									<!-- ==================================================== -->
									<!-- Instructor / Facilitador  -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<table class="table">
											<thead>
												<tr>
													<th>Instructor / Facilitador</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<div class="text-center innerAll inner-2x">
															<p class="margin-none">
																<img src="../assets/iconodashboard-04.png" style="width: 100px; margin-bottom: 15px" alt="">
															</p>
															<p class="lead margin-none strong" id="instruc_facilitador">0</p>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!-- ==================================================== -->
									<!-- END Instructor / Facilitador  -->
									<!-- ==================================================== -->

									<!-- ==================================================== -->
									<!-- Contenido del curso  -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<table class="table">
											<thead>
												<tr>
													<th>Contenido del curso</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<div class="text-center innerAll inner-2x">
															<p class="margin-none">
																<img src="../assets/iconodashboard-02.png" style="width: 100px; margin-bottom: 15px" alt="">
															</p>
															<p class="lead margin-none strong" id="contenido_curso">0</p>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<!-- ==================================================== -->
									<!-- END Contenido del curso  -->
									<!-- ==================================================== -->
								</div>
							</div>
						</div>
						<!-- END IFE -->

						<br>
						<div class="row">
							<!-- ==================================================== -->
							<!-- Ranking Concesionarios -->
							<!-- ==================================================== -->
							<div class="col-md-6">
								<div class="box box-danger">
									<div class="box-header with-border">
										<h3 class="box-title">Ranking Estudiantes <small>indica los mejores agentes de acuerdo a sus cursos aprobados</small> </h3>
									</div>
									<div class="box-body">
										<table class="footable table table-striped table-primary">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th class="text-center">ALUMNO</th>
													<th class="text-center">CURSOS APROBADOS / PROMEDIO</th>
												</tr>
											</thead>
											<tbody id="rankingEstudiantesC">

											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- ==================================================== -->
							<!-- END Ranking Concesionarios -->
							<!-- ==================================================== -->

							<!-- ==================================================== -->
							<!-- Ranking promedio Nota -->
							<!-- ==================================================== -->
							<div class="col-md-6">
								<div class="box box-danger">
									<div class="box-header with-border">
										<h3 class="box-title">Ranking promedio Nota <small>lista los mejores promedios de notas</small> </h3>
									</div>
									<div class="box-body">
										<table class="footable table table-striped table-primary">
											<thead>
												<tr>
													<th class="text-center">#</th>
													<th class="text-center">ESTUDIANTE</th>
													<th class="text-center">PROMEDIO NOTA / TIEMPO / CURSOS</th>
												</tr>
											</thead>
											<tbody id="rankingAlumn">

											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- ==================================================== -->
							<!-- END Ranking promedio Nota -->
							<!-- ==================================================== -->
						</div>

					</div>
				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<!-- <script src="../assets/components/plugins/chartjs-funnel/dist/chart.funnel.min.js"></script>
	<script src="../assets/components/plugins/chartjs-funnel/dist/chart.funnel.bundled.min.js"></script> -->
	<script src="js/dashboard_new.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
