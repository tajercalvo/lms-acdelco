<?php include('src/seguridad.php'); ?>
<?php
$source = "vid";
include('controllers/argumentarios.php');
$location = 'tutor';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/argumentario.css" media="screen" title="argumentario">
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons shopping_cart"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/argumentarios.php">Argumentario Comercial</a></li>
				</ul>
				<!-- inner -->
				<!-- -->
				<!-- El container cambia si es galeria o menu-->
				<div class="<?php if(isset($_GET['id'])){echo('container');}else{echo('innerLR');}?>">
					<!-- heading -->
					<div class="innerB">
						<div class="row">
							<h2 class="margin-none pull-left text-primary col-md-4">
							<?php if(isset($_GET['id'])){ ?>
							<strong  style="font-size: 25px;"><?php echo($Argumentario_datos['argument']); ?></strong>
							<br>
							<a href="#" data-toggle="modal" onclick="LikeGalery(<?php echo($Argumentario_datos['argument_id']); ?>); return false;"><i class="fa fa-fw fa-thumbs-o-up"></i> Me gusta</a>
							<span class="text-small margin-none">
						    <?php if($_SESSION['max_rol']>=6){ ?>
						    <a href="#ModalEditar" data-toggle="modal"><i class="fa fa-fw fa-pencil"></i> Editar </a>
						    <?php } ?>
						  </span>
							<br>
							<small><?php echo($Argumentario_datos['description']); ?></small>
							<!-- likes del modelo -->
							<!-- <i class="fa fa-fw fa-thumbs-o-up"></i> <h2 style="font-weight:normal;color:#0058a3 !important" id="CantLikes<?php echo($Argumentario_datos['argument_id']); ?>"><?php echo(number_format($Argumentario_datos['likes'],0)); ?></h2> -->
							<?php } ?>
							</h2>
							<?php if(isset($_GET['id'])){ ?>
							<div class="col-md-4">
								<audio controls="controls">
						        <source src="../assets/gallery/source/<?php echo($Argumentario_datos['file_mp3']); ?>" type="audio/mpeg">
						        Your browser does not support the audio tag.
						    </audio>
							</div>
							<?php } ?>
							<div class="btn-group pull-right">
								<?php if(isset($_GET['id'])){ ?>
								<a href="#" class="glyphicons no-js unshare" onclick="javascript:history.go(-1)"><i></i>Regresar</a>
								<?php }else{ ?>
								<?php if($_SESSION['max_rol']>=6){ ?>
								<a href="#ModalCrearNueva" data-toggle="modal" class="glyphicons no-js circle_plus" ><i></i>Agregar Argumentario</a>
								<?php } ?>
								<?php } ?>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<?php if(!isset($_GET['id'])){ ?>
					<!-- contenido interno margin-none border-none padding-none -->
					<!-- Tabs -->
					<div class="relativeWrap" >
						<div class="wizard">
							<div class="widget widget-tabs widget-tabs-double widget-tabs-vertical row margin-none widget-tabs-gray">

								<!-- Widget heading -->
								<div class="widget-head col-md-3">
									<ul id="listaMenu">
										<li class="active" style="height: 25px;"><a href="#tab1" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Mini</span></a></li>
										<li style="height: 25px;"><a href="#tab2" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Small</span></a></li>
										<li style="height: 25px;"><a href="#tab3" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Medium</span></a></li>
										<li style="height: 25px;"><a href="#tab7" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Camionetas</span></a></li>
										<li style="height: 25px;"><a href="#tab8" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Pick Up</span></a></li>
										<li style="height: 25px;"><a href="#tab9" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Premium</span></a></li>
										<li style="height: 25px;"><a href="#tab4" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Taxis</span></a></li>
										<li style="height: 25px;"><a href="#tab5" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Vanes</span></a></li>
										<li style="height: 25px;"><a href="#tab10" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Buses</span></a></li>
										<li style="height: 25px;"><a href="#tab11" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Camiones</span></a></li>
										<li style="height: 25px;"><a href="#tab6" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Tecnología</span></a></li>
										<li style="height: 25px;"><a href="#tab12" style="height: 25px;" data-toggle="tab"><span class="text-uppercase text-center">Acdelco</span></a></li>
									</ul>
								</div>
								<!-- // Widget heading END -->
								<div class="widget-body col-md-9">
									<div class="tab-content">
										<!-- Step 1 -->
										<div class="tab-pane active" id="tab1">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Mini"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 1 END -->
										<!-- Step 2 -->
										<div class="tab-pane" id="tab2">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Small"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 2 END -->
										<!-- Step 3 -->
										<div class="tab-pane" id="tab3">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Medium"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 3 END -->
										<!-- Step 4 -->
										<div class="tab-pane" id="tab4">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Taxis"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 4 END -->
										<!-- Step 5 -->
										<div class="tab-pane" id="tab5">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Vanes"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 5 END -->
										<!-- Step 6 -->
										<div class="tab-pane" id="tab6">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Tecnolog&iacute;a"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 6 END -->
										<!-- Step 7 -->
										<div class="tab-pane" id="tab7">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Camionetas"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 7 END -->
										<!-- Step 8 -->
										<div class="tab-pane" id="tab8">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Pick Up"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 8 END -->
										<!-- Step 9 -->
										<div class="tab-pane" id="tab9">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Premium"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 9 END -->
										<!-- Step 10 -->
										<div class="tab-pane" id="tab10">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Buses"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>
										<!-- // Step 10 END -->
										<!-- Step 11 -->
										<div class="tab-pane" id="tab11">
											<div class="row">
												<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Camiones"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				}
																			}
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php } } ?>
											</div>
										</div>

											<!-- Step 12 -->
								<div class="tab-pane" id="tab12">
											<div class="row">
												<?php  foreach ($Argumentarios_datos as $key => $Data_Gal) {
												if($Data_Gal['argument_cat']=="Acedelco"){ ?>
												<div class="col-md-6">
													<div class="box-generic border-none sombra" style="border: 2px solid gold;padding: 0;">
														<div class="row border-none">
															<p class="text-small margin-none col-md-12 text-primary text-right">Actualización: <?php  $date = date_create($Data_Gal['date_edition']); echo(date_format($date,'d-m-Y')); ?></p>
														</div>

														<div class="relativeWrap overflow-hidden" style="height:350px">
															<a href="argumentarios.php?id=<?php echo $Data_Gal['argument_id'].'&galeria='.$Data_Gal['argument_cat_id'] ; ?>" class="pull-left">
																<img src="../assets/gallery/source/<?php  echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
															</a>
															<!--<div class="fixed-bottom bg-inverse-faded">-->
															<div class="fixed-bottom" style="background: #1552c1 !important;">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong class="text-center text-uppercase">
																		<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																		<i class="fa fa-fw fa-rocket"></i> <?php echo($Data_Gal['argument']); ?></a>
																		</strong>
																		<div class="row border-none">
																			<?php
																			$titulo_otr = "";
																			if(isset($Data_Gal['likes_data'])){
																				 foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																					$titulo_otr .= "<i class='fa fa-group text-danger'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																				 }
																			 }
																			?>
																			<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="col-md-4 text-default text-right text-medium" style="float: right;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo(number_format($Data_Gal['likes'],0)); ?></a>

																		</div>
																		<br>
																		<div class="" style="color: white;">
																			<?php echo($Data_Gal['description']); ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>

													</div>
												</div>
												<?php  } } ?>
											</div>
										</div>
											<!-- // Step 10 END -->
									</div>

								</div>
							</div>
						</div>
						<!-- -->
						<!-- -->
					</div>
					<!-- // Tabs END -->
					<!-- -->
					<!-- -->
					<!-- -->
					<?php }else{ ?>
					<!-- -->
					<!-- -->
					<!-- Blueimp Gallery -->
					<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
						<div class="slides"></div>
						<h3 class="title">Titulo de la fografía</h3>
						<a class="prev no-ajaxify">‹</a>
						<a class="next no-ajaxify">›</a>
						<a class="close no-ajaxify">×</a>
						<a class="play-pause no-ajaxify"></a>
						<ol class="indicator"></ol>
				</div><!-- -->
				<!-- // Pagina completa  -->
				<div class="innerL row row-merge">
					<!-- cabecera del argumentario-->
					<div class="row innerL innerTB">
						<div class="col-md-12">
							<!-- About -->
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div id="caracte" class="widget-body border-none margin-none padding-none">
									<h3 class="heading">Características: </h3>
									<p style="text-align: justify;"><?php echo($Argumentario_datos['characteristics']); ?></p>
								</div>
							</div>
							<!-- // About END -->
						</div>
					</div>
					<!-- fin cabecera argumentario-->
					<!-- Carrousel -->
					<div class="row center innerL innerTB">
						<!-- carrousel de imagenes-->
						<div class="row padding">
							<div class="col-md-12">
								<div id="banner-home" class="owl-carousel owl-theme">
									<!-- item -->
									<?php if($Argumentario_datos['img_top1'] != "" && $Argumentario_datos['img_top1'] != "default2.png"){ ?>
										<div class="item">
											<div class="" style="width: 100%;max-height: 550px;">
												<div class="" style="width: 60%;display: inline-block;">
													<img src="../assets/gallery/source/<?php echo($Argumentario_datos['img_top1']); ?>" alt="Banner 1" class="img-responsive padding-none border-none" style="width: 100%;min-height:350px;"/>
												</div>
											</div>
										</div>
									<?php } ?>
									<!-- Fin item -->
									<!-- item -->
									<?php if($Argumentario_datos['img_top2'] != "" && $Argumentario_datos['img_top2'] != "default2.png"){ ?>
										<div class="item">
											<div class="" style="width: 100%;max-height: 550px;">
												<div class="" style="width: 60%;display: inline-block;">
													<img src="../assets/gallery/source/<?php echo($Argumentario_datos['img_top2']); ?>" alt="Banner 2" class="img-responsive padding-none border-none" style="width: 100%;min-height:350px;"/>
												</div>
											</div>
										</div>
									<?php } ?>
									<!-- Fin item -->
									<!-- item -->
									<?php if($Argumentario_datos['img_top3'] != "" && $Argumentario_datos['img_top3'] != "default2.png"){ ?>
										<div class="item">
											<div class="" style="width: 100%;max-height: 550px;">
												<div class="" style="width: 60%;display: inline-block;">
													<img src="../assets/gallery/source/<?php echo($Argumentario_datos['img_top3']); ?>" alt="Banner 3" class="img-responsive padding-none border-none" style="width: 100%;min-height:350px;"/>
												</div>
											</div>
										</div>
									<?php } ?>
									<!-- Fin item -->
								</div>
							</div>
						</div>
						<!-- fin carrousel de imagenes-->
					</div>
					<!-- Fin Carrousel -->
					<!-- resto del argumentario-->
					<div class="row innerL innerTB">
						<div class="col-md-12 containerBg innerTB sombra">
							<div class="wizard">
								<div class="widget widget-tabs widget-tabs-double widget-tabs-gray">
									<!-- Widget heading -->
									<div class="widget-head" style="height: 25px;">
										<ul id="listaMenu" style="width: 100%;height: 25px;">
											<li <?php if(!isset($_GET['aprobar'])){echo 'class="active"';} ?>  style="width: 18%;height: 25px;"  ><a href="#tab1-2" data-toggle="tab"><span align="center" class="strong text-uppercase">Argumentos comerciales</span></a></li>
											<li style="width: 15%;height: 25px;"  ><a href="#tab2-2" data-toggle="tab"><span align="center" class="strong text-uppercase"><?php if(isset($_GET['galeria']) and $_GET['galeria'] == 13 ){echo "Portafolio de producto";}else{echo "Versiones";} ?></span></a></li>
											<li style="width: 15%;height: 25px;"  ><a href="#tab3-2" data-toggle="tab"><span align="center" class="strong text-uppercase">Destacado</span></a></li>
											<li style="width: 15%;height: 25px;"  ><a href="#tab4-2" data-toggle="tab"><span align="center" class="strong text-uppercase">Accesorios</span></a></li>
											<li style="width: 15%;height: 25px;"  ><a href="#tab5-2" data-toggle="tab"><span align="center" class="strong text-uppercase">Material de apoyo </span></a></li>
											<li style="width: 15%;height: 25px;"  ><a href="#tab6-2" data-toggle="tab"><span align="center" class="strong text-uppercase">BUENAS PRÁCTICAS </span></a></li>
										</ul>

									</div>
									<!-- // Widget heading END -->
									<div class="widget-body">
										<div class="tab-content">
											<!-- Step 1 -->
											<div class="tab-pane active" id="tab1-2">
												<div class="row">
													<div class="widget-body">
						                <p style="text-align: justify;"><?php echo($Argumentario_datos['commercial']); ?></p>
						              </div>
												</div>
											</div>
											<!-- // Step 1 END -->
											<!-- Step 2 -->
											<div class="tab-pane" id="tab2-2">
												<div class="row">
													<div class="widget-body" style="text-align: justify;padding-left: 30px;">
											      <?php echo($Argumentario_datos['versions']); ?>
											    </div>
												</div>
											</div>
											<!-- // Step 2 END -->
											<!-- Step 3 -->
											<div class="tab-pane" id="tab3-2">
												<div class="row">
													<div class="col-md-12">
														<img src="../assets/gallery/source/<?php echo($Argumentario_datos['prominent']); ?>" alt="Destacado" style="width: 100%;"/>
													</div>
												</div>
											</div>
											<!-- // Step 3 END -->
											<!-- Step 4 -->
											<div class="tab-pane" id="tab4-2">
												<div class="widget-body">
					                <ul>
					                	<?php
														if(is_array($accesorios) && count($accesorios) > 0){
														for ($i=1; $i < count($accesorios) ; $i++) { ?>
															<li><?php echo($accesorios[$i]); ?></li>
														<?php }
														}else{
															echo($Argumentario_datos['accesories']);
														}
														?>
					                </ul>
					              </div>
											</div>
											<!-- // Step 4 END -->
											<div class="tab-pane" id="tab5-2">
												<div class="widget-body">
													<div class="row">
														<?php foreach($archivos as $file){ ?>
					                  <!-- Gallery Layout -->
														<div class="col-md-3">
																<div style="width: 250px;">
																	<a href="../assets/gallery/MaterialApoyo/<?php echo $file['file_name']; ?>" target="_blank" >
																		<img src="../assets/gallery/source/<?php echo $file['img_file']; ?>" alt="imagen <?php echo $file['img_file']; ?>" style="width:90%;"/>
																		<br>
																		<span class="btn btn-inverse btn-sm" style="width: 90%;">
																			<?php echo $file['title']; ?>
																		</span>
																	</a>
																</div>
														</div>
					                  <!-- // Gallery Layout END -->
					                  <?php }//fin foreach ?>
													</div>

											<?php if (isset($_GET['id'])) {?>
													<div class="separator bottom"></div>
													<div class="row">
													<?php foreach ($relacionadadConBiblioteca as $key => $value) {?>
														<div class="col-md-3">
															<img align="auto" src="../assets/Biblioteca/m/<?php echo $value['image']?>" style="width: 640px;height: auto;" alt="" class="img-responsive padding-none border-none">
															<audio src="../assets/Biblioteca/<?php echo $value['file']?>" preload="none" controls="" style = "width: 100%" ></audio>
														</div>
														<?php } ?>
													</div>
														<?php }?>

												</div>
											</div>
											<!-- // Step 6-2 END -->
											<div class="tab-pane" id="tab6-2">
												<div class="widget-body">
													<div class="row">

														<div style="width: 1050px; height: 300px; overflow-y: scroll;" class="col-md-12">
																<div class="box-generic">
				<div class="timeline-top-info content-filled border-bottom">
					<i class="fa fa-comment-o"></i> <a href="#">Buenas prácticas</a>
				</div>
				<?php foreach ($Buentaspracticas as $key => $value) { ?>
			       <div id="elim<?php echo $value['id_ludus_argument_good_practices'];?>" class="media innerAll margin-none">
			        <a class="pull-left" href="#"><img src="../assets/images/usuarios/<?php echo $value['image'];?>" alt="photo" class="media-object" width="35"></a>
			        <div class="media-body"> <strong> <?php echo $value['dealer']; ?></strong>
			          	<a href="<?php if($_SESSION['max_rol']>5){?>ver_perfil.php?back=usuarios&id=<?php echo $value['user_id'].'"target="_blank"';}else{echo '#"';}?> class="strong"><?php echo $value['first_name'].' '.$value['last_name']; ?></a> <?php echo $value['good_practices']; ?>
			          <?php if($value['status_id']==2){ ?><td class="relativeWrap"><span id="aprob<?php echo $value['id_ludus_argument_good_practices'];?>" class="label label-success">  <a href="#" onclick="aprobar(<?php echo $value['id_ludus_argument_good_practices']; ?>); return false;" data-toggle="modal"  style="color: white;" ><i class="fa fa-check-circle"></i> Aprobar</a><?php } ?></span></td> <?php if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){ ?><td class="relativeWrap"><span class="label label-danger">  <a href="#" onclick="eliminar(<?php echo $value['id_ludus_argument_good_practices']; ?>); return false;" data-toggle="modal"  style="color: white;" ><i class="fa fa-trash-o"></i> Eliminar</a><?php ?></span></td> <?php } ?>
		     			<div class="timeline-bottom">
							<i class="fa fa-clock-o"></i> <?php echo $value['date_creation']; ?>
						</div>
			        </div>
			        <div class="timeline-top-info content-filled border-bottom"></div>
			    </div>
			    <?php } ?>
				<!-- <div class="innerAll">
				<input type="hidden" id="argument_id" value="<?php // echo $_GET['id']; ?>">
					<textarea id="good_practices" rows="4" cols="50" placeholder="Escribe aqui" style=" resize: none; height: 150PX;"> </textarea><br><br>
					<button id="enviar" class="btn btn-success"><i class="fa fa-comment-o"></i> Enviar</button>

				</div> -->
					</div>

						</div>

													</div>

														<div class="innerAll">
				<input type="hidden" id="argument_id" value="<?php echo $_GET['id']; ?>">
					<textarea id="good_practices" rows="4" cols="50" placeholder="Escribe aqui" style=" resize: none; height: 150PX;"> </textarea><br><br>
					<button id="enviar" class="btn btn-success"><i class="fa fa-comment-o"></i> Enviar</button>

				</div>

												</div>

											</div>

										<!-- Fin Step 6-2 -->
										</div>
										<!-- // Wizard pagination controls END -->

									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Fin resto del argumentario -->
					<!-- Documentos anexos-->
					<div class="row innerL innerTB">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12">
									<a href="../assets/gallery/source/<?php echo($Argumentario_datos['file_pdf']); ?>" target="_blank" class="btn btn-sm boton-azul"><i class="fa fa-download fa-fw"></i>Descargar Ficha Técnica</a>
								</div>
							</div>
							<div class="row innerTB">
								<div class="col-md-12">
									<h3 class="heading">Competencias: </h3>
									<p style="text-align: justify;">
	                  <?php echo($Argumentario_datos['competition']); ?>
	                </p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12">
									<h3 class="heading">Galería de imágenes: </h3>
									<ul class="row list-unstyled">
										<?php foreach ($Galeria_datos as $key => $Data_Gal) { ?>
										<li class="col-md-3">
											<a class="thumb no-ajaxify" href="fotografias_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>&back=<?php echo($_GET['id']); ?>">
												<img src="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>" title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" alt="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" class="img-responsive" />
											</a>
										</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<div class="row innerTB">
								<div class="col-md-12">
									<h3 class="heading">Galería de videos: </h3>
									<ul class="row list-unstyled">
										<?php foreach ($Galeria_datosv as $key => $Data_Gal) { ?>
										<li class="col-md-3">
											<a class="thumb no-ajaxify" href="videos_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>&back=<?php echo($_GET['id']); ?>">
												<img src="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>" title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" alt="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" class="img-responsive" />
											</a>
										</li>
										<?php } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- Fin Documentos anexos -->
				</div>
				<?php } ?>
				<div class="separator bottom"></div>
				<div class="separator bottom"></div>
				<!-- // END contenido interno -->
			</div>
			<!-- // END inner -->
		</div>
		<!-- // END Contenido proyectos -->
	</div>
	<?php if(!isset($_GET['id'])){ ?>
	<!-- Modal -->
	<form action="argumentarios.php" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
		<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Crear Nuevo Argumentario Comercial</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row"><!-- Campos de Modelo y Segmento-->
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Modelo: </label>
										<input type="text" id="argument" name="argument" class="form-control col-md-8" placeholder="Modelo del vehículo" />
									</div>
									<div class="col-md-5">
										<label class="control-label">Segmento: </label>
										<select style="width: 100%;" id="argument_cat_id" name="argument_cat_id" >
											<?php foreach ($Argumentarios_Catdatos as $key => $Data_Cat) { ?>
											<option value="<?php echo($Data_Cat['argument_cat_id']); ?>" ><?php echo($Data_Cat['argument_cat']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
										<input type="hidden" id="opcn" value="crear" name="opcn">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo para una breve descripción o un slogan -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Descripción:</label>
										<textarea class="form-control" name="description_principal" id="description_principal" rows="2" cols="20" placeholder="Ingrese una breve descripciòn"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo ficha tecnica-->
									<div class="col-md-1"></div>
									<div class="col-md-10">
										<div class="form-group">
											<label for="ficha_tec">Ficha Tecnica: </label>
											<input type="file" name="file_pdf" id="file_pdf">
										</div>
									</div>
									<div class="col-md-1"></div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Select galeria Imagenes -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Galería Imágenes: </label>
										<select style="width: 100%;" id="media_id" name="media_id" >
											<option value="56">Sin Galería</option>
											<?php foreach ($Argumentarios_Meddatos as $key => $Data_Med) { ?>
											<option value="<?php echo($Data_Med['media_id']); ?>" ><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Select galeria Videos -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Galería Videos: </label>
										<select style="width: 100%;" id="media_idv" name="media_idv" >
											<option value="56">Sin Galería</option>
											<?php foreach ($Argumentarios_MeddatosV as $key => $Data_Med) { ?>
											<option value="<?php echo($Data_Med['media_id']); ?>" ><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Caracteristicas -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Características: </label>
										<textarea id="characteristics" name="characteristics" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Argumentos comerciales -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Arg. Comerciales: </label>
										<textarea id="commercial" name="commercial" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Competencia -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Competencia: </label>
										<textarea id="competition" name="competition" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Accesorios -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Accesorios: </label>
										<textarea id="accesories" name="accesories" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Versiones -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Versiones: </label>
										<textarea id="versions" name="versions" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Carga las imagenes para el banner de cada modelo -->
									<div class="col-md-1"></div>
									<div class="col-md-10">
										<label class="control-label">Imágenes Banner:</label>
										<div class="row">
											<div class="col-md-3">
												<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
													<span class="btn btn-default btn-file">
														<span class="fileupload-new">Imágen 1</span>
														<span class="fileupload-exists">Cambiar Imagen 1</span>
														<input type="file" class="margin-none" id="img_top1" name="img_top1" />
													</span>
													<span class="fileupload-preview"></span>
													<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
												</div>
											</div>
											<div class="col-md-3">
												<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
													<span class="btn btn-default btn-file">
														<span class="fileupload-new">Imágen 2</span>
														<span class="fileupload-exists">Cambiar Imagen 2</span>
														<input type="file" class="margin-none" id="img_top2" name="img_top2" />
													</span>
													<span class="fileupload-preview"></span>
													<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
												</div>
											</div>
											<div class="col-md-3">
												<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
													<span class="btn btn-default btn-file">
														<span class="fileupload-new">Imágen 3</span>
														<span class="fileupload-exists">Cambiar Imagen 3</span>
														<input type="file" class="margin-none" id="img_top3" name="img_top3" />
													</span>
													<span class="fileupload-preview"></span>
													<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Carga la imagen principal del modelo -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Imágen principal:</label>
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
												<span class="fileupload-exists">Cambiar Imagen</span>
												<input type="file" class="margin-none" id="image_new" name="image_new" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Carga la imagen para la seccion de destacado -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Imágen Destacado:</label>
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Imágen</span>
												<span class="fileupload-exists">Cambiar Imagen</span>
												<input type="file" class="margin-none" id="prominent" name="prominent" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Carga hasta 3 archivos y sus imagenes correspondientes como material de apoyo-->
									<div class="col-md-1">
									</div>
									<label class="control-label">Material de Apoyo:</label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<input type="text" id="text_f1" name="text_f1" class="form-control col-md-8" placeholder="Titulo primer archivo" />
												</div>
												<div class="col-md-1"></div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file">
															<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
															<span class="fileupload-exists">Cambiar Archivo</span>
															<input type="file" class="margin-none" id="pdf_new" name="pdf_new"/>
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
												</div>
												<div class="col-md-5">
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file">
															<span class="fileupload-new">Imágen Archivo</span>
															<span class="fileupload-exists">Cambiar Imágen</span>
															<input type="file" class="margin-none" id="img_pdf_new" name="img_pdf_new"/>
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<label class="control-label">Material de Apoyo 2:</label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<input type="text" id="text_f2" name="text_f2" class="form-control col-md-8" placeholder="Titulo segundo archivo" />
												</div>
												<div class="col-md-1"></div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file">
															<span class="fileupload-new">Seleccionar Archivo 2 (PDF)</span>
															<span class="fileupload-exists">Cambiar Archivo 2</span>
															<input type="file" class="margin-none" id="pdf_new2" name="pdf_new2"/>
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
												</div>
												<div class="col-md-5">
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file">
															<span class="fileupload-new">Imágen Archivo</span>
															<span class="fileupload-exists">Cambiar Imágen</span>
															<input type="file" class="margin-none" id="img_pdf_new2" name="img_pdf_new2"/>
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<label class="control-label">Material de Apoyo 3:</label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<input type="text" id="text_f3" name="text_f3" class="form-control col-md-8" placeholder="Titulo tercer archivo" />
												</div>
												<div class="col-md-1"></div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file">
															<span class="fileupload-new">Seleccionar Archivo 3 (PDF)</span>
															<span class="fileupload-exists">Cambiar Archivo 3</span>
															<input type="file" class="margin-none" id="pdf_new3" name="pdf_new3"/>
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
												</div>
												<div class="col-md-5">
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file">
															<span class="fileupload-new">Imágen Archivo</span>
															<span class="fileupload-exists">Cambiar Imágen</span>
															<input type="file" class="margin-none" id="img_pdf_new3" name="img_pdf_new3"/>
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
												</div>
												<div class="col-md-1"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row"><!-- Campo Audio mp3 -->
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Audio:</label>
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo Cuña (MP3)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="mp3_file" name="mp3_file" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- /.modal -->
	<?php }else{ ?>
	<!-- Modal -->
	<form action="argumentarios.php?id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
	  <div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	          <h4 class="modal-title">Editar el Argumentario Comercial</h4>
	        </div>
	        <div class="modal-body">
	          <div class="widget widget-heading-simple widget-body-gray">
	            <div class="widget-body">
	              <!-- Row -->
	              <div class="row"><!-- Campos de Modelo y Segmento-->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-5">
	                  <label class="control-label">Modelo: </label>
	                  <input type="text" id="argument" name="argument" class="form-control col-md-8" placeholder="Modelo del vehículo" value="<?php echo($Argumentario_datos['argument']); ?>"/>
	                </div>
	                <div class="col-md-5">
	                  <label class="control-label">Segmento: </label>
	                  <select style="width: 100%;" id="argument_cat_id" name="argument_cat_id" >
	                    <?php foreach ($Argumentarios_Catdatos as $key => $Data_Cat) { ?>
	                    <option value="<?php echo($Data_Cat['argument_cat_id']); ?>" <?php if($Argumentario_datos['argument_cat_id']==$Data_Cat['argument_cat_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Cat['argument_cat']); ?></option>
	                    <?php } ?>
	                  </select>
	                </div>
	                <div class="col-md-1">
	                  <input type="hidden" id="argument_id" value="<?php echo $_GET['id']; ?>" name="argument_id">
	                  <input type="hidden" id="opcn" value="editar" name="opcn">
	                  <input type="hidden" name="ant_new_nameMp3" id="ant_new_nameMp3" value="<?php echo($Argumentario_datos['file_mp3']); ?>">
										<input type="hidden" name="ant_image_new" id="ant_image_new" value="<?php echo($Argumentario_datos['image']); ?>">
										<input type="hidden" name="ant_file_pdf" id="ant_file_pdf" value="<?php echo($Argumentario_datos['file_pdf']); ?>">
										<input type="hidden" name="ant_img_top1" id="ant_img_top1" value="<?php echo($Argumentario_datos['img_top1']); ?>">
										<input type="hidden" name="ant_img_top2" id="ant_img_top2" value="<?php echo($Argumentario_datos['img_top2']); ?>">
										<input type="hidden" name="ant_img_top3" id="ant_img_top3" value="<?php echo($Argumentario_datos['img_top3']); ?>">
										<input type="hidden" name="ant_prominent" id="ant_prominent" value="<?php echo($Argumentario_datos['prominent']); ?>">
										<?php
										$cont = 0;
										foreach ($archivos as $key => $value) {
											$cont++;
											?>
											<input type="hidden" name="ant_pdf<?php echo($cont); ?>" id="ant_pdf<?php echo($cont); ?>" value="<?php echo($value['argument_file_id']); ?>">
										<?php } ?>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo para una breve descripción o un slogan -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Descripción:</label>
	                  <textarea class="form-control" name="description_principal" id="description_principal" rows="2" cols="20" placeholder="Ingrese una breve descripciòn"><?php echo($Argumentario_datos['description']); ?></textarea>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo ficha tecnica-->
	                <div class="col-md-1"></div>
	                <div class="col-md-10">
	                  <div class="form-group">
	                    <label for="ficha_tec">Ficha Tecnica: </label>
	                    <input type="file" name="file_pdf" id="file_pdf" <?php if(isset($Argumentario_datos['file_pdf']) && $Argumentario_datos['file_pdf'] != ""){ echo("value='".$Argumentario_datos['file_pdf']."'");} ?> >
	                  </div>
	                </div>
	                <div class="col-md-1"></div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Select galeria Imagenes -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Galería Imágenes: </label>
	                  <select style="width: 100%;" id="media_id" name="media_id" >
	                    <?php foreach ($Argumentarios_Meddatos as $key => $Data_Med) { ?>
	                    <option value="<?php echo($Data_Med['media_id']); ?>" <?php if($Argumentario_datos['media_id']==$Data_Med['media_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Med['media']); ?></option>
	                    <?php } ?>
	                  </select>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Select galeria Videos -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Galería Videos: </label>
	                  <select style="width: 100%;" id="media_idv" name="media_idv" >
	                    <?php foreach ($Argumentarios_MeddatosV as $key => $Data_Med) { ?>
	                    <option value="<?php echo($Data_Med['media_id']); ?>" <?php if($Argumentario_datos['media_idv']==$Data_Med['media_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Med['media']); ?></option>
	                    <?php } ?>
	                  </select>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row">
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-5">
	                  <label class="control-label">Estado: </label>
	                  <select style="width: 100%;" id="estado" name="estado">
	                    <option value="1" <?php if($Argumentario_datos['status_id']=="1"){ ?>selected="selected"<?php } ?>>Activo</option>
	                    <option value="2" <?php if($Argumentario_datos['status_id']=="2"){ ?>selected="selected"<?php } ?>>Inactivo</option>
	                  </select>
	                </div>
	                <div class="col-md-1">

	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Caracteristicas -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Características: </label>
	                  <textarea id="characteristics" name="characteristics" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['characteristics']); ?></textarea>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Argumentos comerciales -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Arg. Comerciales: </label>
	                  <textarea id="commercial" name="commercial" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['commercial']); ?></textarea>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Competencia -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Competencia: </label>
	                  <textarea id="competition" name="competition" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['competition']); ?></textarea>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Accesorios -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Accesorios: </label>
	                  <textarea id="accesories" name="accesories" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['accesories']); ?></textarea>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Versiones -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Versiones: </label>
	                  <textarea id="versions" name="versions" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['versions']); ?></textarea>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Carga las imagenes para el banner de cada modelo -->
	                <div class="col-md-1"></div>
	                <div class="col-md-10">
	                  <label class="control-label">Imágenes Banner:</label>
	                  <div class="row">
	                    <div class="col-md-3">
	                      <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                        <span class="btn btn-default btn-file">
														<?php if($Argumentario_datos['img_top1'] != "" && $Argumentario_datos['img_top1'] != "default2.png"){ ?>
																<label for="img_top1">
																	<img src="../assets/gallery/source/<?php echo($Argumentario_datos['img_top1']); ?>" style="width: 100%;"/>
																</label>
																<input type="file" class="margin-none" id="img_top1" name="img_top1" />
														<?php }else{ ?>
															<span class="fileupload-new">Imágen 1</span>
		                          <span class="fileupload-exists">Cambiar Imagen 1</span>
		                          <input type="file" class="margin-none" id="img_top1" name="img_top1" />
														<?php } ?>
	                        </span>
	                        <span class="fileupload-preview"></span>
	                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                      </div>
	                    </div>
	                    <div class="col-md-3">
	                      <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                        <span class="btn btn-default btn-file">
														<?php if($Argumentario_datos['img_top2'] != "" && $Argumentario_datos['img_top2'] != "default2.png"){ ?>
																<label for="img_top2">
																	<img src="../assets/gallery/source/<?php echo($Argumentario_datos['img_top2']); ?>" style="width: 100%;"/>
																</label>
																<input type="file" class="margin-none" id="img_top2" name="img_top2" />
														<?php }else{ ?>
															<span class="fileupload-new">Imágen 2</span>
		                          <span class="fileupload-exists">Cambiar Imagen 2</span>
		                          <input type="file" class="margin-none" id="img_top2" name="img_top2" />
														<?php } ?>
	                        </span>
	                        <span class="fileupload-preview"></span>
	                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                      </div>
	                    </div>
	                    <div class="col-md-3">
	                      <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                        <span class="btn btn-default btn-file">
														<?php if($Argumentario_datos['img_top3'] != "" && $Argumentario_datos['img_top3'] != "default2.png"){ ?>
																<label for="img_top3">
																	<img src="../assets/gallery/source/<?php echo($Argumentario_datos['img_top3']); ?>" style="width: 100%;"/>
																</label>
																<input type="file" class="margin-none" id="img_top3" name="img_top3" />
														<?php }else{ ?>
															<span class="fileupload-new">Imágen 3</span>
		                          <span class="fileupload-exists">Cambiar Imagen 3</span>
		                          <input type="file" class="margin-none" id="img_top3" name="img_top3" />
														<?php } ?>
	                        </span>
	                        <span class="fileupload-preview"></span>
	                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                      </div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Carga la imagen principal del modelo -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Imágen principal:</label>
	                  <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                    <span class="btn btn-default btn-file">
	                      <?php if($Argumentario_datos['image'] != "" && $Argumentario_datos['image'] != "default2.png"){ ?>
													<label for="image_new">
														<img src="../assets/gallery/source/<?php echo($Argumentario_datos['image']); ?>" style="width: 100%;"/>
													</label>
		                      <input type="file" class="margin-none" id="image_new" name="image_new" />
												<?php }else{ ?>
													<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
		                      <span class="fileupload-exists">Cambiar Imagen</span>
		                      <input type="file" class="margin-none" id="image_new" name="image_new" />
												<?php } ?>
	                    </span>
	                    <span class="fileupload-preview"></span>
	                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                  </div>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Carga la imagen para la seccion de destacado -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Imágen Destacado:</label>
	                  <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                    <span class="btn btn-default btn-file">
	                      <?php if($Argumentario_datos['prominent'] != "" && $Argumentario_datos['prominent'] != "default2.png"){ ?>
													<label for="prominent">
														<img src="../assets/gallery/source/<?php echo($Argumentario_datos['prominent']); ?>" style="width: 100%;"/>
													</label>
													<input type="file" class="margin-none" id="prominent" name="prominent" />
												<?php }else{ ?>
													<span class="fileupload-new">Seleccionar Imágen</span>
		                      <span class="fileupload-exists">Cambiar Imagen</span>
		                      <input type="file" class="margin-none" id="prominent" name="prominent" />
												<?php } ?>
	                    </span>
	                    <span class="fileupload-preview"></span>
	                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                  </div>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Carga hasta 3 archivos y sus imagenes correspondientes como material de apoyo-->
	                <div class="col-md-1">
	                </div>
	                <label class="control-label">Material de Apoyo:</label>
	                <div class="row">
	                  <div class="col-md-12">
	                    <div class="row">
	                      <div class="col-md-1"></div>
	                      <div class="col-md-10">
	                        <input type="text" id="text_f1" name="text_f1" class="form-control col-md-8" placeholder="Titulo primer archivo" <?php if(isset($archivos[0]['title']) && $archivos[0]['title'] != ""){ echo("value='".$archivos[0]['title']."'"); } ?>/>
	                      </div>
	                      <div class="col-md-1"></div>
	                    </div>
	                    <br>
	                    <div class="row">
	                      <div class="col-md-1"></div>
	                      <div class="col-md-5">
	                        <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                          <span class="btn btn-default btn-file">
	                            <span class="fileupload-new">Seleccionar Archivo (PDF)</span>
	                            <span class="fileupload-exists">Cambiar Archivo</span>
	                            <input type="file" class="margin-none" id="pdf_new" name="pdf_new" accept="application/pdf"/>
	                          </span>
	                          <span class="fileupload-preview"></span>
	                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                        </div>
	                      </div>
	                      <div class="col-md-5">
	                        <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                          <span class="btn btn-default btn-file">
	                            <?php if(isset($archivos[0]['img_file']) && $archivos[0]['img_file'] != "" && $archivos[0]['img_file'] != "default2.png"){ ?>
																<label for="img_pdf_new">
																	<img src="../assets/gallery/source/<?php echo($archivos[0]['img_file']); ?>" style="width: 100%;max-width: 100px;"/>
																</label>
																<input type="file" class="margin-none" id="img_pdf_new" name="img_pdf_new"/>
															<?php }else{ ?>
																<span class="fileupload-new">Imágen Archivo</span>
		                            <span class="fileupload-exists">Cambiar Imágen</span>
		                            <input type="file" class="margin-none" id="img_pdf_new" name="img_pdf_new" accept="image/png, image/jpeg"/>
															<?php } ?>
	                          </span>
	                          <span class="fileupload-preview"></span>
	                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                        </div>
	                      </div>
	                      <div class="col-md-1"></div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row">
	                <div class="col-md-1">
	                </div>
	                <label class="control-label">Material de Apoyo 2:</label>
	                <div class="row">
	                  <div class="col-md-12">
	                    <div class="row">
	                      <div class="col-md-1"></div>
	                      <div class="col-md-10">
	                        <input type="text" id="text_f2" name="text_f2" class="form-control col-md-8" placeholder="Titulo segundo archivo" <?php if(isset($archivos[1]['title']) && $archivos[1]['title'] != ""){ echo("value='".$archivos[1]['title']."'"); } ?>/>
	                      </div>
	                      <div class="col-md-1"></div>
	                    </div>
	                    <br>
	                    <div class="row">
	                      <div class="col-md-1"></div>
	                      <div class="col-md-5">
	                        <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                          <span class="btn btn-default btn-file">
	                            <span class="fileupload-new">Seleccionar Archivo 2 (PDF)</span>
	                            <span class="fileupload-exists">Cambiar Archivo 2</span>
	                            <input type="file" class="margin-none" id="pdf_new2" name="pdf_new2" accept="application/pdf/>
	                          </span>
	                          <span class="fileupload-preview"></span>
	                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                        </div>
	                      </div>
	                      <div class="col-md-5">
	                        <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                          <span class="btn btn-default btn-file">
															<?php if(isset($archivos[1]['img_file']) && $archivos[1]['img_file'] != "" && $archivos[1]['img_file'] != "default2.png"){ ?>
																<label for="img_pdf_new2">
																	<img src="../assets/gallery/source/<?php echo($archivos[1]['img_file']); ?>" style="width: 100%;max-width: 100px;"/>
																</label>
																<input type="file" class="margin-none" id="img_pdf_new2" name="img_pdf_new2"/>
															<?php }else{ ?>
																<span class="fileupload-new">Imágen Archivo</span>
		                            <span class="fileupload-exists">Cambiar Imágen</span>
		                            <input type="file" class="margin-none" id="img_pdf_new2" name="img_pdf_new2" accept="image/png, image/jpeg"/>
															<?php } ?>
	                          </span>
	                          <span class="fileupload-preview"></span>
	                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                        </div>
	                      </div>
	                      <div class="col-md-1"></div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row">
	                <div class="col-md-1">
	                </div>
	                <label class="control-label">Material de Apoyo 3:</label>
	                <div class="row">
	                  <div class="col-md-12">
	                    <div class="row">
	                      <div class="col-md-1"></div>
	                      <div class="col-md-10">
	                        <input type="text" id="text_f3" name="text_f3" class="form-control col-md-8" placeholder="Titulo tercer archivo" <?php if(isset($archivos[3]['title']) && $archivos[3]['title'] != ""){ echo("value='".$archivos[3]['title']."'"); } ?>/>
	                      </div>
	                      <div class="col-md-1"></div>
	                    </div>
	                    <br>
	                    <div class="row">
	                      <div class="col-md-1"></div>
	                      <div class="col-md-5">
	                        <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                          <span class="btn btn-default btn-file">
	                            <span class="fileupload-new">Seleccionar Archivo 3 (PDF)</span>
	                            <span class="fileupload-exists">Cambiar Archivo 3</span>
	                            <input type="file" class="margin-none" id="pdf_new3" name="pdf_new3" accept="application/pdf/>
	                          </span>
	                          <span class="fileupload-preview"></span>
	                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                        </div>
	                      </div>
	                      <div class="col-md-5">
	                        <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                          <span class="btn btn-default btn-file">
															<?php if(isset($archivos[2]['img_file']) && $archivos[2]['img_file'] != "" && $archivos[2]['img_file'] != "default2.png"){ ?>
																<label for="img_pdf_new3">
																	<img src="../assets/gallery/source/<?php echo($archivos[2]['img_file']); ?>" style="width: 100%;max-width: 100px;"/>
																</label>
																<input type="file" class="margin-none" id="img_pdf_new3" name="img_pdf_new3"/>
															<?php }else{ ?>
																<span class="fileupload-new">Imágen Archivo</span>
		                            <span class="fileupload-exists">Cambiar Imágen</span>
		                            <input type="file" class="margin-none" id="img_pdf_new3" name="img_pdf_new3" accept="image/png, image/jpeg"/>
															<?php } ?>
	                          </span>
	                          <span class="fileupload-preview"></span>
	                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                        </div>
	                      </div>
	                      <div class="col-md-1"></div>
	                    </div>
	                  </div>
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Audio mp3 -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                  <label class="control-label">Audio:</label>
	                  <div class="fileupload fileupload-new margin-none" data-provides="fileupload">
	                    <span class="btn btn-default btn-file">
	                      <span class="fileupload-new">Seleccionar Archivo Cuña (MP3)</span>
	                      <span class="fileupload-exists">Cambiar Archivo</span>
	                      <input type="file" class="margin-none" id="mp3_file" name="mp3_file" />
	                    </span>
	                    <span class="fileupload-preview"></span>
	                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
	                  </div>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	               <div class="clearfix"><br></div>
	              <!-- Row -->
	              <div class="row"><!-- Campo Audio mp3 -->
	                <div class="col-md-1">
	                </div>
	                <div class="col-md-10">
	                 <h5 class="text-uppercase strong text-primary"><i class="fa fa-book "></i> Relacion con la Bibilioteca </h5>
						<select multiple="multiple" style="width: 100%;" id="librarylibrary_id" name="librarylibrary_id[]">
								<?php foreach ($biblioteca as $key => $Data_library_id) { $selected = ''; ?>
										<?php foreach ($relacionadadConBiblioteca as $key => $value) {
											if ($Data_library_id ['library_id'] == $value['library_id']) {
												$selected = 'selected';
											}
										} ?>
									<option value="<?php echo $Data_library_id['library_id']; ?>" <?php echo $selected ?>><?php echo $Data_library_id['name']; ?></option>
								<?php } ?>
						</select>
	                </div>
	                <div class="col-md-1">
	                </div>
	              </div>
	              <!-- Row END-->
	              <div class="clearfix"><br></div>
	            </div>
	          </div>
	        </div>
	        <div class="modal-footer">
	          <button type="submit" id="BtnCreacion" class="btn btn-primary">Editar</button>
	        </div>
	      </div>
	    </div>
	  </div>
	</form>
	<!-- /.modal -->
	<!--
		Modal para cargar los archivos adjuntos correspondientes para
		Material de apoyo
	-->
	<form action="argumentarios.php?id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_MaterialApoyo"><br><br>
		<div class="modal fade" id="ModalApoyo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Cargar Material de apoyo</h4>
					</div>
					<div class="modal-body">

						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Titulo del archivo</label>
										<input type="text" id="tittle_file" name="tittle_file" class="form-control col-md-8" placeholder="Titulo del material" value="<?php //echo libro cargado; ?>" />
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_adjunto" name="pdf_adjunto" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row -->
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="opcn" value="pdfApoyo">
						<button type="submit" id="BtnApoyo" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- Fon Modal Material de apoyo-->

	<!--
		Modal para cargar las preguntas frecuentes
	-->
	<form action="argumentarios.php?id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_CreateFaq"><br><br>
		<div class="modal fade" id="ModalFaq" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Cargar Pregunta Frecuente</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Pregunta</label>
										<input type="text" id="faq" name="faq" class="form-control col-md-12" placeholder="Titulo del material" value="<?php //echo libro cargado; ?>" />
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">respuesta</label>
										<textarea placeholder="Escriba aquí la respuesta" class="form-control col-md-12" id="faq_answer" name="faq_answer" ></textarea>
									</div>
								</div>
								<!-- Row END-->
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="opcn" value="faqCreate">
						<button type="submit" id="BtnFaq" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- Fin Modal Preguntas frecuentes-->

	<?php } ?>
	<div class="clearfix"></div>
	<!-- // Sidebar menu & content wrapper END -->
	<?php include('src/footer.php'); ?>
</div>
<!-- // Main Container Fluid END -->
<?php include('src/global.php'); ?>
<script src="js/argumentarios.js"></script>
</body>
</html>
