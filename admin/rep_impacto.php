<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_impacto.php');
$location = 'reporting';
$locData = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons cars"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_impacto.php">Impacto en el negocio</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">indicadores de impacto en el negocio </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<!-- contenido filtros -->
	<div class="widget widget-heading-simple widget-body-gray hidden-print">
		<div class="widget-body">
		<form action="rep_impacto.php" method="post">
			<div class="row">
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<div class="col-md-4">
							<label class="control-label" for="dealer_id">Año:</label>
							<select style="width: 100%;" id="ano_id_end" name="ano_id_end" >
								<?php for ($y=date("Y");$y>2010;$y--){ ?>
									<option value="<?php echo $y; ?>" <?php if($y==$year_end){ ?>selected="selected"<?php } ?>><?php echo $y; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-4">
							<label class="control-label" for="zone_id">Zona:</label>
							<select style="width: 100%;" id="zone_id" name="zone_id">
									<option value="0">Todas</option>
								<?php foreach ($Zonas as $key => $data) { ?>
									<option value="<?php echo($data['zone_id']); ?>" <?php if( (isset($_POST['zone_id']) && $_POST['zone_id']==$data['zone_id']) ){ ?>selected="selected"<?php }?> ><?php echo($data['zone']); ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-4">
							<label class="control-label" for="dealer_id">Concesionario:</label>
							<select style="width: 100%;" id="dealer_id" name="dealer_id">
									<option value="0">Todos</option>
								<?php foreach ($Concesionarios as $key => $data) { ?>
									<option value="<?php echo($data['dealer_id']); ?>" <?php if( (isset($_POST['dealer_id']) && $_POST['dealer_id']==$data['dealer_id']) ){ ?>selected="selected"<?php }?> ><?php echo($data['dealer']); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2 text-center">
					<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10">
					<div class="form-group">
						<div class="col-md-6">
							<label class="control-label" for="course_id">Cursos:</label>
							<select multiple="multiple" style="width: 100%;" id="course_id" name="course_id[]">
									<option value="0">Todos</option>
								<?php foreach ($Cursos as $key => $data) { 
									$selected = "";
									foreach ($_POST['course_id'] as $iID => $data_c) {
										if($data_c == $data["course_id"]){
											$selected = "selected";
										}
									}
									?>
									<option value="<?php echo($data['course_id']); ?>" <?php echo($selected); ?> ><?php echo($data['newcode'].':'.$data['course']); ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-6">
							<label class="control-label" for="model">Modelo:</label>
							<select multiple="multiple" style="width: 100%;" id="model" name="model[]">
									<option value="0">Todos</option>
								<?php foreach ($Modelos as $key => $data) { 
									$selected = "";
									foreach ($_POST['model'] as $iID => $model_selec) {
										if($model_selec == $data['model']){
											$selected = "selected";
										}
									}
									?>
									<option value="<?php echo($data['model']); ?>" <?php echo($selected); ?> ><?php echo($data['model']); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
		<!-- Nuevo ROW-->
		<div class="row row-app">
		</div>
		<!-- // END Nuevo ROW-->
		<div class="separator bottom"></div>
		<div class="separator bottom"></div>
	</div>
<!-- // END contenido filtros -->
<!-- PRIMER INDICADOR COMPLETO -->
	<div class="well">
		<table class="table table-invoice">
			<tbody>
				<tr>
					<td style="width: 50%;">
						<p class="lead">ASESOR DE VEHÍCULOS NUEVOS</p>
							<h4>A tener en cuenta:</h4>
							<address class="margin-none" style="text-align: justify;">
								<strong>Periodo: </strong><?php echo($fec_finalQ); ?><br/><br/>
							</address><br>
							<p class="lead text-info"><b>Ventas Mensuales X Modelo</b></p>
							<!-- // Table -->
							<table class="table table-condensed table-vertical-center table-thead-simple">
								<thead>
									<tr>
										<th class="center">MODELO</th>
										<th class="center">Ene</th>
										<th class="center">Feb</th>
										<th class="center">Mar</th>
										<th class="center">Abr</th>
										<th class="center">May</th>
										<th class="center">Jun</th>
										<th class="center">Jul</th>
										<th class="center">Ago</th>
										<th class="center">Sep</th>
										<th class="center">Oct</th>
										<th class="center">Nov</th>
										<th class="center">Dic</th>
										<th class="center">TOTAL</th>
									</tr>
								</thead>
								<tbody id="cuerpo_tabla">
									<?php if($cantidad_datos > 0){ 
										$ene = 0; $ene_tot = 0;
										$feb = 0; $feb_tot = 0;
										$mar = 0; $mar_tot = 0;
										$abr = 0; $abr_tot = 0;
										$may = 0; $may_tot = 0;
										$jun = 0; $jun_tot = 0;
										$jul = 0; $jul_tot = 0;
										$ago = 0; $ago_tot = 0;
										$sep = 0; $sep_tot = 0;
										$oct = 0; $oct_tot = 0;
										$nov = 0; $nov_tot = 0;
										$dic = 0; $dic_tot = 0;
										foreach ($DatosCant_VentasModelo as $iID => $data_proc) {
											$ene = 0;
											$feb = 0;
											$mar = 0;
											$abr = 0;
											$may = 0;
											$jun = 0;
											$jul = 0;
											$ago = 0;
											$sep = 0;
											$oct = 0;
											$nov = 0;
											$dic = 0;
											if(isset($data_proc['Datos'])){
												foreach ($data_proc['Datos'] as $key => $value) {
													if($value['mes']==1){
														$ene = $value['cant'];
														$ene_tot = $ene_tot + $ene;
														//echo($ene.'::'.$ene_tot.'-');
													}else if($value['mes']==2){
														$feb = $value['cant'];
														$feb_tot = $feb_tot + $feb;
													}else if($value['mes']==3){
														$mar = $value['cant'];
														$mar_tot = $mar_tot + $mar;
													}else if($value['mes']==4){
														$abr = $value['cant'];
														$abr_tot = $abr_tot + $abr;
													}else if($value['mes']==5){
														$may = $value['cant'];
														$may_tot = $may_tot + $may;
													}else if($value['mes']==6){
														$jun = $value['cant'];
														$jun_tot = $jun_tot + $jun;
													}else if($value['mes']==7){
														$jul = $value['cant'];
														$jul_tot = $jul_tot + $jul;
													}else if($value['mes']==8){
														$ago = $value['cant'];
														$ago_tot = $ago_tot + $ago;
													}else if($value['mes']==9){
														$sep = $value['cant'];
														$sep_tot = $sep_tot + $sep;
													}else if($value['mes']==10){
														$oct = $value['cant'];
														$oct_tot = $oct_tot + $oct;
													}else if($value['mes']==11){
														$nov = $value['cant'];
														$nov_tot = $nov_tot + $nov;
													}else if($value['mes']==12){
														$dic = $value['cant'];
														$dic_tot = $dic_tot + $dic;
													}
												}
											}
											?>
											<!-- Item -->
											<tr class="selectable">
											<input type="hidden" class="Graf1" id="Graf1" name="Graf1" value="<?php echo($data_proc['model']); ?>" dat-1="<?php echo($ene);?>" dat-2="<?php echo($feb);?>" dat-3="<?php echo($mar);?>" dat-4="<?php echo($abr);?>" dat-5="<?php echo($may);?>" dat-6="<?php echo($jun);?>" dat-7="<?php echo($jul);?>" dat-8="<?php echo($ago);?>" dat-9="<?php echo($sep);?>" dat-10="<?php echo($oct);?>" dat-11="<?php echo($nov);?>" dat-12="<?php echo($dic);?>" />
												<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-info"><?php echo($data_proc['model']); ?></span></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ene);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($feb);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($mar);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($abr);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($may);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jun);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jul);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ago);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($sep);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($oct);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($nov);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($dic);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ene+$feb+$mar+$abr+$may+$jun+$jul+$ago+$sep+$oct+$nov+$dic);?></td>
											</tr>
											<!-- // Item END -->
									<?php } }?>
											<tr class="selectable">
												<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-info">TOTAL GENERAL</span></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ene_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($feb_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($mar_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($abr_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($may_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jun_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jul_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ago_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($sep_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($oct_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($nov_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($dic_tot); ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ene_tot+$feb_tot+$mar_tot+$abr_tot+$may_tot+$jun_tot+$jul_tot+$ago_tot+$sep_tot+$oct_tot+$nov_tot+$dic_tot);?></b></td>
												
											</tr>
								</tbody>
							</table>
							<div id="char_1" class="flotchart-holder" style="height: 300px;"></div>
							<!-- // Table END -->
							<p class="lead text-primary"><b>Cursos Mensuales X Promedio</b></p>
							<!-- // Table -->
							<table class="table table-condensed table-vertical-center table-thead-simple">
								<thead>
									<tr>
										<th class="center">CURSO</th>
										<th class="center">Ene</th>
										<th class="center">Feb</th>
										<th class="center">Mar</th>
										<th class="center">Abr</th>
										<th class="center">May</th>
										<th class="center">Jun</th>
										<th class="center">Jul</th>
										<th class="center">Ago</th>
										<th class="center">Sep</th>
										<th class="center">Oct</th>
										<th class="center">Nov</th>
										<th class="center">Dic</th>
										<th class="center">AVG Total</th>
									</tr>
								</thead>
								<tbody id="cuerpo_tabla">
									<?php if($cantidad_datos > 0){ 
										$cant_Prov = 0;
										$ene = 0; $ene_tot = 0; $tot_gral_1 = 0;
										$feb = 0; $feb_tot = 0; $tot_gral_2 = 0;
										$mar = 0; $mar_tot = 0; $tot_gral_3 = 0;
										$abr = 0; $abr_tot = 0; $tot_gral_4 = 0;
										$may = 0; $may_tot = 0; $tot_gral_5 = 0;
										$jun = 0; $jun_tot = 0; $tot_gral_6 = 0;
										$jul = 0; $jul_tot = 0; $tot_gral_7 = 0;
										$ago = 0; $ago_tot = 0; $tot_gral_8 = 0;
										$sep = 0; $sep_tot = 0; $tot_gral_9 = 0;
										$oct = 0; $oct_tot = 0; $tot_gral_10 = 0;
										$nov = 0; $nov_tot = 0; $tot_gral_11 = 0;
										$dic = 0; $dic_tot = 0; $tot_gral_12 = 0;
										foreach ($DatosAvg_CursosPromedio as $iID => $data_proc) {
											$cant_Prov++;
											$ene = 0;
											$feb = 0;
											$mar = 0;
											$abr = 0;
											$may = 0;
											$jun = 0;
											$jul = 0;
											$ago = 0;
											$sep = 0;
											$oct = 0;
											$nov = 0;
											$dic = 0;
											if(isset($data_proc['Datos'])){
												$num_mes = 0;
												foreach ($data_proc['Datos'] as $key => $value) {
													if($value['mes']==1){
														$ene = $value['cant'];
														$ene_tot = $ene_tot + $ene; $num_mes++; $tot_gral_1++;
													}else if($value['mes']==2){
														$feb = $value['cant'];
														$feb_tot = $feb_tot + $feb; $num_mes++; $tot_gral_2++;
													}else if($value['mes']==3){
														$mar = $value['cant'];
														$mar_tot = $mar_tot + $mar; $num_mes++; $tot_gral_3++;
													}else if($value['mes']==4){
														$abr = $value['cant'];
														$abr_tot = $abr_tot + $abr; $num_mes++; $tot_gral_4++;
													}else if($value['mes']==5){
														$may = $value['cant'];
														$may_tot = $may_tot + $may; $num_mes++; $tot_gral_5++;
													}else if($value['mes']==6){
														$jun = $value['cant'];
														$jun_tot = $jun_tot + $jun; $num_mes++; $tot_gral_6++;
													}else if($value['mes']==7){
														$jul = $value['cant'];
														$jul_tot = $jul_tot + $jul; $num_mes++; $tot_gral_7++;
													}else if($value['mes']==8){
														$ago = $value['cant'];
														$ago_tot = $ago_tot + $ago; $num_mes++; $tot_gral_8++;
													}else if($value['mes']==9){
														$sep = $value['cant'];
														$sep_tot = $sep_tot + $sep; $num_mes++; $tot_gral_9++;
													}else if($value['mes']==10){
														$oct = $value['cant'];
														$oct_tot = $oct_tot + $oct; $num_mes++; $tot_gral_10++;
													}else if($value['mes']==11){
														$nov = $value['cant'];
														$nov_tot = $nov_tot + $nov; $num_mes++; $tot_gral_11++;
													}else if($value['mes']==12){
														$dic = $value['cant'];
														$dic_tot = $dic_tot + $dic; $num_mes++; $tot_gral_12++;
													}
												}
											}
											?>
											<!-- Item -->
											<tr class="selectable">
											<input type="hidden" class="Graf2" id="Graf2" name="Graf2" value="<?php echo($data_proc['newcode']); ?>" dat-1="<?php echo($ene);?>" dat-2="<?php echo($feb);?>" dat-3="<?php echo($mar);?>" dat-4="<?php echo($abr);?>" dat-5="<?php echo($may);?>" dat-6="<?php echo($jun);?>" dat-7="<?php echo($jul);?>" dat-8="<?php echo($ago);?>" dat-9="<?php echo($sep);?>" dat-10="<?php echo($oct);?>" dat-11="<?php echo($nov);?>" dat-12="<?php echo($dic);?>" />
												<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-primary"><?php echo($data_proc['newcode'].':'.$data_proc['course']); ?></span></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($ene,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($feb,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($mar,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($abr,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($may,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($jun,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($jul,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($ago,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($sep,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($oct,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($nov,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format($dic,3);?></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo number_format(($ene+$feb+$mar+$abr+$may+$jun+$jul+$ago+$sep+$oct+$nov+$dic)/$num_mes,3);?></td>
											</tr>
											<!-- // Item END -->
									<?php } }?>
											<tr class="selectable">
												<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-primary">PROM GENERAL</span></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($ene_tot>0 && $tot_gral_1>0){ echo number_format($ene_tot/$tot_gral_1,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($feb_tot>0 && $tot_gral_2>0){ echo number_format($feb_tot/$tot_gral_2,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($mar_tot>0 && $tot_gral_3>0){ echo number_format($mar_tot/$tot_gral_3,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($abr_tot>0 && $tot_gral_4>0){ echo number_format($abr_tot/$tot_gral_4,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($may_tot>0 && $tot_gral_5>0){ echo number_format($may_tot/$tot_gral_5,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($jun_tot>0 && $tot_gral_6>0){ echo number_format($jun_tot/$tot_gral_6,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($jul_tot>0 && $tot_gral_7>0){ echo number_format($jul_tot/$tot_gral_7,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($ago_tot>0 && $tot_gral_8>0){ echo number_format($ago_tot/$tot_gral_8,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($sep_tot>0 && $tot_gral_9>0){ echo number_format($sep_tot/$tot_gral_9,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($oct_tot>0 && $tot_gral_10>0){ echo number_format($oct_tot/$tot_gral_10,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($nov_tot>0 && $tot_gral_11>0){ echo number_format($nov_tot/$tot_gral_11,3); }else{ echo("0"); } ?></b></td>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php if($dic_tot>0 && $tot_gral_12>0){ echo number_format($dic_tot/$tot_gral_12,3); }else{ echo("0"); } ?></b></td>
												<?php 
													$resultadoGral = 0;
													$CtrlProm = 0;
													if($ene_tot>0 && $tot_gral_1>0){
														$resultadoGral = $resultadoGral + $ene_tot/$tot_gral_1; $CtrlProm++;
													}
													if($feb_tot>0 && $tot_gral_2>0){
														$resultadoGral = $resultadoGral + $feb_tot/$tot_gral_2; $CtrlProm++;
													}
													if($mar_tot>0 && $tot_gral_3>0){
														$resultadoGral = $resultadoGral + $mar_tot/$tot_gral_3; $CtrlProm++;
													}
													if($abr_tot>0 && $tot_gral_4>0){
														$resultadoGral = $resultadoGral + $abr_tot/$tot_gral_4; $CtrlProm++;
													}
													if($may_tot>0 && $tot_gral_5>0){
														$resultadoGral = $resultadoGral + $may_tot/$tot_gral_5; $CtrlProm++;
													}
													if($jun_tot>0 && $tot_gral_6>0){
														$resultadoGral = $resultadoGral + $jun_tot/$tot_gral_6; $CtrlProm++;
													}
													if($jul_tot>0 && $tot_gral_7>0){
														$resultadoGral = $resultadoGral + $jul_tot/$tot_gral_7; $CtrlProm++;
													}
													if($ago_tot>0 && $tot_gral_8>0){
														$resultadoGral = $resultadoGral + $ago_tot/$tot_gral_8; $CtrlProm++;
													}
													if($sep_tot>0 && $tot_gral_9>0){
														$resultadoGral = $resultadoGral + $sep_tot/$tot_gral_9; $CtrlProm++;
													}
													if($oct_tot>0 && $tot_gral_10>0){
														$resultadoGral = $resultadoGral + $oct_tot/$tot_gral_10; $CtrlProm++;
													}
													if($nov_tot>0 && $tot_gral_11>0){
														$resultadoGral = $resultadoGral + $nov_tot/$tot_gral_11; $CtrlProm++;
													}
													if($dic_tot>0 && $tot_gral_12>0){
														$resultadoGral = $resultadoGral + $dic_tot/$tot_gral_12; $CtrlProm++;
													}

												?>
												<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo number_format($resultadoGral/$CtrlProm,3); ?></b></td>
											</tr>
								</tbody>
							</table>
							<div id="char_2" class="flotchart-holder" style="height: 300px;"></div>
							<!-- // Table END -->
					</td>
					<td class="right">
						<p class="lead text-primary"><b>Segmentación por Edades</b></p>
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple">
							<thead>
								<tr>
									<th class="center">RANGO</th>
									<th class="center">Ene</th>
									<th class="center">Feb</th>
									<th class="center">Mar</th>
									<th class="center">Abr</th>
									<th class="center">May</th>
									<th class="center">Jun</th>
									<th class="center">Jul</th>
									<th class="center">Ago</th>
									<th class="center">Sep</th>
									<th class="center">Oct</th>
									<th class="center">Nov</th>
									<th class="center">Dic</th>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
								<?php if($cantidad_datos > 0){ 
									$ene = 0; $ene_tot = 0;
									$feb = 0; $feb_tot = 0;
									$mar = 0; $mar_tot = 0;
									$abr = 0; $abr_tot = 0;
									$may = 0; $may_tot = 0;
									$jun = 0; $jun_tot = 0;
									$jul = 0; $jul_tot = 0;
									$ago = 0; $ago_tot = 0;
									$sep = 0; $sep_tot = 0;
									$oct = 0; $oct_tot = 0;
									$nov = 0; $nov_tot = 0;
									$dic = 0; $dic_tot = 0;
									foreach ($DatosCant_RangosEdad as $iID => $data_proc) {
										$ene = 0;
										$feb = 0;
										$mar = 0;
										$abr = 0;
										$may = 0;
										$jun = 0;
										$jul = 0;
										$ago = 0;
										$sep = 0;
										$oct = 0;
										$nov = 0;
										$dic = 0;
										if(isset($data_proc['Datos'])){
											foreach ($data_proc['Datos'] as $key => $value) {
												if($value['mes']==1){
													$ene = $value['cant'];
													$ene_tot = $ene_tot + $ene;
													//echo($ene.'::'.$ene_tot.'-');
												}else if($value['mes']==2){
													$feb = $value['cant'];
													$feb_tot = $feb_tot + $feb;
												}else if($value['mes']==3){
													$mar = $value['cant'];
													$mar_tot = $mar_tot + $mar;
												}else if($value['mes']==4){
													$abr = $value['cant'];
													$abr_tot = $abr_tot + $abr;
												}else if($value['mes']==5){
													$may = $value['cant'];
													$may_tot = $may_tot + $may;
												}else if($value['mes']==6){
													$jun = $value['cant'];
													$jun_tot = $jun_tot + $jun;
												}else if($value['mes']==7){
													$jul = $value['cant'];
													$jul_tot = $jul_tot + $jul;
												}else if($value['mes']==8){
													$ago = $value['cant'];
													$ago_tot = $ago_tot + $ago;
												}else if($value['mes']==9){
													$sep = $value['cant'];
													$sep_tot = $sep_tot + $sep;
												}else if($value['mes']==10){
													$oct = $value['cant'];
													$oct_tot = $oct_tot + $oct;
												}else if($value['mes']==11){
													$nov = $value['cant'];
													$nov_tot = $nov_tot + $nov;
												}else if($value['mes']==12){
													$dic = $value['cant'];
													$dic_tot = $dic_tot + $dic;
												}
											}
										}
										$rango_lb = "";
										if($data_proc['rango']==1){
											$rango_lb = "15 - 25";
										}else if($data_proc['rango']==2){
											$rango_lb = "26 - 35";
										}else if($data_proc['rango']==3){
											$rango_lb = "36 - 45";
										}else if($data_proc['rango']==4){
											$rango_lb = "46 - 55";
										}else if($data_proc['rango']==5){
											$rango_lb = "56 - 65";
										}else if($data_proc['rango']==6){
											$rango_lb = "MAYOR DE 65";
										}
										?>
										<!-- Item -->
										<tr class="selectable">
										<input type="hidden" class="Graf3" id="Graf3" name="Graf3" value="<?php echo($rango_lb); ?>" dat-1="<?php echo($ene);?>" dat-2="<?php echo($feb);?>" dat-3="<?php echo($mar);?>" dat-4="<?php echo($abr);?>" dat-5="<?php echo($may);?>" dat-6="<?php echo($jun);?>" dat-7="<?php echo($jul);?>" dat-8="<?php echo($ago);?>" dat-9="<?php echo($sep);?>" dat-10="<?php echo($oct);?>" dat-11="<?php echo($nov);?>" dat-12="<?php echo($dic);?>" />
											<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-primary"><?php echo($rango_lb); ?></span></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ene);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($feb);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($mar);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($abr);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($may);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jun);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jul);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ago);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($sep);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($oct);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($nov);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($dic);?></td>
										</tr>
										<!-- // Item END -->
								<?php } }?>
										<tr class="selectable">
											<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-primary">TOTAL GENERAL</span></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ene_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($feb_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($mar_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($abr_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($may_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jun_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jul_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ago_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($sep_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($oct_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($nov_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($dic_tot); ?></b></td>
											
										</tr>
							</tbody>
						</table>
						<div id="char_3" class="flotchart-holder" style="height: 300px;"></div>
						<!-- // Table END -->
						<p class="lead text-info"><b>Segmentación por Género</b></p>
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple">
							<thead>
								<tr>
									<th class="center">GENERO</th>
									<th class="center">Ene</th>
									<th class="center">Feb</th>
									<th class="center">Mar</th>
									<th class="center">Abr</th>
									<th class="center">May</th>
									<th class="center">Jun</th>
									<th class="center">Jul</th>
									<th class="center">Ago</th>
									<th class="center">Sep</th>
									<th class="center">Oct</th>
									<th class="center">Nov</th>
									<th class="center">Dic</th>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
								<?php if($cantidad_datos > 0){ 
									$ene = 0; $ene_tot = 0;
									$feb = 0; $feb_tot = 0;
									$mar = 0; $mar_tot = 0;
									$abr = 0; $abr_tot = 0;
									$may = 0; $may_tot = 0;
									$jun = 0; $jun_tot = 0;
									$jul = 0; $jul_tot = 0;
									$ago = 0; $ago_tot = 0;
									$sep = 0; $sep_tot = 0;
									$oct = 0; $oct_tot = 0;
									$nov = 0; $nov_tot = 0;
									$dic = 0; $dic_tot = 0;
									foreach ($DatosCant_Genero as $iID => $data_proc) {
										$ene = 0;
										$feb = 0;
										$mar = 0;
										$abr = 0;
										$may = 0;
										$jun = 0;
										$jul = 0;
										$ago = 0;
										$sep = 0;
										$oct = 0;
										$nov = 0;
										$dic = 0;
										foreach ($data_proc['Datos'] as $key => $value) {
											if($value['mes']==1){
												$ene = $value['cant'];
												$ene_tot = $ene_tot + $ene;
											}else if($value['mes']==2){
												$feb = $value['cant'];
												$feb_tot = $feb_tot + $feb;
											}else if($value['mes']==3){
												$mar = $value['cant'];
												$mar_tot = $mar_tot + $mar;
											}else if($value['mes']==4){
												$abr = $value['cant'];
												$abr_tot = $abr_tot + $abr;
											}else if($value['mes']==5){
												$may = $value['cant'];
												$may_tot = $may_tot + $may;
											}else if($value['mes']==6){
												$jun = $value['cant'];
												$jun_tot = $jun_tot + $jun;
											}else if($value['mes']==7){
												$jul = $value['cant'];
												$jul_tot = $jul_tot + $jul;
											}else if($value['mes']==8){
												$ago = $value['cant'];
												$ago_tot = $ago_tot + $ago;
											}else if($value['mes']==9){
												$sep = $value['cant'];
												$sep_tot = $sep_tot + $sep;
											}else if($value['mes']==10){
												$oct = $value['cant'];
												$oct_tot = $oct_tot + $oct;
											}else if($value['mes']==11){
												$nov = $value['cant'];
												$nov_tot = $nov_tot + $nov;
											}else if($value['mes']==12){
												$dic = $value['cant'];
												$dic_tot = $dic_tot + $dic;
											}
										}
										?>
										<!-- Item -->
										<tr class="selectable">
										<input type="hidden" class="Graf4" id="Graf4" name="Graf4" value="<?php if($data_proc['gender']==1){ echo("Masculino"); }elseif($data_proc['gender']==2){ echo("Femenino"); }else{ echo("Por Definir"); } ?>" dat-1="<?php echo($ene);?>" dat-2="<?php echo($feb);?>" dat-3="<?php echo($mar);?>" dat-4="<?php echo($abr);?>" dat-5="<?php echo($may);?>" dat-6="<?php echo($jun);?>" dat-7="<?php echo($jul);?>" dat-8="<?php echo($ago);?>" dat-9="<?php echo($sep);?>" dat-10="<?php echo($oct);?>" dat-11="<?php echo($nov);?>" dat-12="<?php echo($dic);?>" />
											<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-info"><?php if($data_proc['gender']==1){ echo("Masculino"); }elseif($data_proc['gender']==2){ echo("Femenino"); }else{ echo("Por Definir"); } ?></span></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ene);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($feb);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($mar);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($abr);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($may);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jun);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jul);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ago);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($sep);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($oct);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($nov);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($dic);?></td>
										</tr>
										<!-- // Item END -->
								<?php } }?>
										<tr class="selectable">
											<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-info">TOTAL GENERAL</span></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ene_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($feb_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($mar_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($abr_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($may_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jun_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jul_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ago_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($sep_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($oct_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($nov_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($dic_tot); ?></b></td>
											
										</tr>
							</tbody>
						</table>
						<div id="char_4" class="flotchart-holder" style="height: 300px;"></div>
						<!-- // Table END -->
						<p class="lead text-primary"><b>Asesores de Vehículos Nuevos</b></p>
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple">
							<thead>
								<tr>
									<th class="center">TRAYECTORIA</th>
									<th class="center">Ene</th>
									<th class="center">Feb</th>
									<th class="center">Mar</th>
									<th class="center">Abr</th>
									<th class="center">May</th>
									<th class="center">Jun</th>
									<th class="center">Jul</th>
									<th class="center">Ago</th>
									<th class="center">Sep</th>
									<th class="center">Oct</th>
									<th class="center">Nov</th>
									<th class="center">Dic</th>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
								<?php if($cantidad_datos > 0){ 
									$ene = 0; $ene_tot = 0;
									$feb = 0; $feb_tot = 0;
									$mar = 0; $mar_tot = 0;
									$abr = 0; $abr_tot = 0;
									$may = 0; $may_tot = 0;
									$jun = 0; $jun_tot = 0;
									$jul = 0; $jul_tot = 0;
									$ago = 0; $ago_tot = 0;
									$sep = 0; $sep_tot = 0;
									$oct = 0; $oct_tot = 0;
									$nov = 0; $nov_tot = 0;
									$dic = 0; $dic_tot = 0;
									foreach ($DatosCant_Trayectoria as $iID => $data_proc) {
										$ene = 0;
										$feb = 0;
										$mar = 0;
										$abr = 0;
										$may = 0;
										$jun = 0;
										$jul = 0;
										$ago = 0;
										$sep = 0;
										$oct = 0;
										$nov = 0;
										$dic = 0;
										foreach ($data_proc['Datos'] as $key => $value) {
											if($value['mes']==1){
												$ene = $value['cant'];
												$ene_tot = $ene_tot + $ene;
											}else if($value['mes']==2){
												$feb = $value['cant'];
												$feb_tot = $feb_tot + $feb;
											}else if($value['mes']==3){
												$mar = $value['cant'];
												$mar_tot = $mar_tot + $mar;
											}else if($value['mes']==4){
												$abr = $value['cant'];
												$abr_tot = $abr_tot + $abr;
											}else if($value['mes']==5){
												$may = $value['cant'];
												$may_tot = $may_tot + $may;
											}else if($value['mes']==6){
												$jun = $value['cant'];
												$jun_tot = $jun_tot + $jun;
											}else if($value['mes']==7){
												$jul = $value['cant'];
												$jul_tot = $jul_tot + $jul;
											}else if($value['mes']==8){
												$ago = $value['cant'];
												$ago_tot = $ago_tot + $ago;
											}else if($value['mes']==9){
												$sep = $value['cant'];
												$sep_tot = $sep_tot + $sep;
											}else if($value['mes']==10){
												$oct = $value['cant'];
												$oct_tot = $oct_tot + $oct;
											}else if($value['mes']==11){
												$nov = $value['cant'];
												$nov_tot = $nov_tot + $nov;
											}else if($value['mes']==12){
												$dic = $value['cant'];
												$dic_tot = $dic_tot + $dic;
											}
										}
										?>
										<!-- Item -->
										<tr class="selectable">
										<input type="hidden" class="Graf5" id="Graf5" name="Graf5" value="<?php echo($data_proc['charge']); ?>" dat-1="<?php echo($ene);?>" dat-2="<?php echo($feb);?>" dat-3="<?php echo($mar);?>" dat-4="<?php echo($abr);?>" dat-5="<?php echo($may);?>" dat-6="<?php echo($jun);?>" dat-7="<?php echo($jul);?>" dat-8="<?php echo($ago);?>" dat-9="<?php echo($sep);?>" dat-10="<?php echo($oct);?>" dat-11="<?php echo($nov);?>" dat-12="<?php echo($dic);?>" />
											<td class="left" style="padding: 2px; font-size: 80%;"><span class="label label-primary"><?php echo($data_proc['charge']); ?></span></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ene);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($feb);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($mar);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($abr);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($may);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jun);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($jul);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($ago);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($sep);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($oct);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($nov);?></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($dic);?></td>
										</tr>
										<!-- // Item END -->
								<?php } }?>
										<!--<tr class="selectable">
											<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary">TOTAL GENERAL</span></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ene_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($feb_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($mar_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($abr_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($may_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jun_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($jul_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($ago_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($sep_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($oct_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($nov_tot); ?></b></td>
											<td class="center important" style="padding: 2px; font-size: 80%;"><b><?php echo($dic_tot); ?></b></td>
										</tr>-->
							</tbody>
						</table>
						<div id="char_5" class="flotchart-holder" style="height: 300px;"></div>
						<!-- // Table END -->
					</td>
				</tr>
			</tbody>
		</table>
	</div>
<!-- PRIMER INDICADOR COMPLETO-->
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_impacto.js"></script>
</body>
</html>