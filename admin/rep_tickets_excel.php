<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_tickets.php');
}
//echo('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_tickets.xls");
//header ("Content-Transfer-Encoding: binary");

if(isset($_GET['start_date_day'])){ ?>
    <table>
        <thead>
            <tr>
                <td>ID TICKET</td>
                <td>CONCESIONARIO</td>
                <td>NOMBRE</td>
                <td>ESTADO</td>
                <td>DESCRIPCION</td>
                <td>PRIORIDAD</td>
                <td>TIPO</td>
                <td>FECHA CREACION</td>
                <td>FECHA RESPUESTA</td>
                <td>ENCARGADO</td>
                <td>TIEMPO RESPUESTA</td>
                <td># RESPUESTAS</td>
                <td>RESPUESTAS</td>
            </tr>
        </thead>
        <tbody>
        <?php if ($cantidad_datos > 0) {
            foreach ($datos_tickets as $iID => $data) {?>
            <tr>
                <td><?php echo( $data['ticket_id'] ); ?></td>
                <td><?php echo( $data['dealer'] ); ?></td>
                <td><?php echo( $data['first_name']." ".$data['last_name'] ); ?></td>
                <td>
                    <?php
                    switch ($data['status_id']) {
                        case 0: echo( "Cerrado" ); break;
                        case 1: echo( "Pendiente" ); break;
                        case 3: echo( "Reabierto" ); break;
                    }//fin switch
                    ?>
                </td>
                <td>
                    <?php echo( strip_tags(utf8_decode($data['description']), '<strong>') ); ?>
                </td>
                <td>
                <?php
                switch ($data['priority']) {
                    case 1: echo( "Alta" ); break;
                    case 2: echo( "Media" ); break;
                    case 3: echo( "Baja" ); break;
                }//fin switch
                ?>
                </td>
                <td><?php echo( $data['key_description'] ); ?></td>
                <td><?php echo( $data['date_creation'] ); ?></td>
                <td>
                <?php  if (isset($data['date_respuesta'])) {
                    echo( $data['date_respuesta'] );
                } ?>
                </td>
                <td>
                <?php if( isset($data['first_s_name']) && isset( $data['last_s_name'] ) ) {
                    echo ( $data['first_s_name']." ".$data['last_s_name'] );
                } ?>
                </td>
                <td>
                <?php if( isset($data['date_respuesta']) ){
                    $datetime1 = date_create($data['date_creation']);
                    $datetime2 = date_create($data['date_respuesta']);
                    $interval = date_diff($datetime1, $datetime2);
                    echo(utf8_decode($interval->format('%R%a días')));
                } ?>
                </td>
                <td><?php if( isset( $data['num_respuestas'] ) ){echo( $data['num_respuestas'] );}else{ echo( 0 ); } ?></td>
                <td><?php echo( strip_tags(utf8_decode($data['respuestas']), '<strong>') ); ?></td>
            </tr>
        <?php } //fin foreach
        }//fin if ?>
        </tbody>
    </table>
<?php } ?>
