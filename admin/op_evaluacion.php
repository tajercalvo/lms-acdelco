<?php include('src/seguridad.php'); ?>
<?php include('controllers/evaluaciones.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons check"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/evaluaciones.php">Configurar Evaluaciones</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Evaluaciones </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
										<h5><a href="evaluaciones.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">
							<!-- div.row formulario evaluacion -->
							<div class="row">
								<div class="col-md-12">
									<div class="widget">
										<div class="widget-head">
											<h4 class="heading">Datos Evaluación</h4>
										</div>
										<div class="widget-body">
											<form id="formulario_data" method="post" autocomplete="off">
												<!-- Row -->
													<div class="row innerLR">
														<!-- Column -->
														<div class="col-md-5">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-4 control-label" for="code" style="padding-top:8px;">Código:</label>
																<div class="col-md-8"><input class="form-control" id="code" name="code" type="text" placeholder="Código de la evaluación" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['code']; ?>"<?php } ?> /></div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-5">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-5 control-label" for="type" style="padding-top:8px;">Tipo de Evaluación:</label>
																<div class="col-md-7">
																	<select style="width: 100%;" id="type" name="type" onchange="CambiaTipo();">
																		<option value="1" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="1"){ ?>selected="selected"<?php } ?>>Evaluación Abierta</option>
																		<option value="3" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="3"){ ?>selected="selected"<?php } ?>>Evaluación Curso</option>
																	</select>
																</div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-2">
														</div>
														<!-- // Column END -->
													</div>
												<!-- // Row END -->
												<div class="separator"></div>
													<!-- Row -->
													<div class="row innerLR">
														<!-- Column -->
														<div class="col-md-10">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-2 control-label" for="review" style="padding-top:8px;">Evaluación:</label>
																<div class="col-md-10"><input class="form-control" id="review" name="review" type="text" placeholder="Nombre de la evaluación" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['review']; ?>"<?php } ?> /></div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-2">
														</div>
														<!-- // Column END -->
													</div>
													<!-- // Row END -->
													<div class="separator"></div>
													<!-- Row -->
													<div class="row innerLR">
														<!-- Column -->
														<div class="col-md-10">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-2 control-label" for="review" style="padding-top:8px;">Descripción:</label>
																<div class="col-md-10">
																<textarea id="description" name="description" class="wysihtml5 col-md-12 form-control" rows="5"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['description']; } ?></textarea>
																</div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-2">
														</div>
														<!-- // Column END -->
													</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
													<div class="row innerLR" id="VistaCursos">
														<!-- Column -->
														<div class="col-md-10">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-2 control-label" for="cursos" style="padding-top:8px;">Cursos:</label>
																<div class="col-md-10">
																	<select multiple="multiple" style="width: 100%;" id="cursos" name="cursos[]" >
																		<?php foreach ($listadosCursos as $key => $Data_Course) {
																			$selected = "";
																			if(isset($registroConfiguracion['cursos'])){
																				foreach ($registroConfiguracion['cursos'] as $key => $Data_CourseUser) {
																					if($Data_CourseUser['course_id'] == $Data_Course['course_id']){
																						$selected = "selected";
																					}
																				}
																			}
																			?>
																			<option value="<?php echo $Data_Course['course_id']; ?>" <?php echo $selected; ?>><?php echo $Data_Course['newcode'].' | '.$Data_Course['course']; ?></option>
																		<?php }?>
														        	</select>
																</div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-2">
														</div>
														<!-- // Column END -->
													</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
													<div class="row innerLR" id="VistaCargos">
														<!-- Column -->
														<div class="col-md-10">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-2 control-label" for="cargos" style="padding-top:8px;">Cargos:</label>
																<div class="col-md-10">
																	<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]" >
																		<?php foreach ($listadosCargos as $key => $Data_Cargo) {
																			$selected = "";
																			if(isset($registroConfiguracion['cargos'])){
																				foreach ($registroConfiguracion['cargos'] as $key => $Data_ChargeUser) {
																					if($Data_ChargeUser['charge_id'] == $Data_Cargo['charge_id']){
																						$selected = "selected";
																					}
																				}
																			}
																			?>
																			<option value="<?php echo $Data_Cargo['charge_id']; ?>" <?php echo $selected; ?>><?php echo $Data_Cargo['charge']; ?></option>
																		<?php }?>
														        	</select>
																</div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-2">
														</div>
														<!-- // Column END -->
													</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
													<div class="row innerLR">
														<!-- Column -->
														<div class="col-md-5">
															<label class="col-md-6 control-label" for="time" style="padding-top:8px;">Tiempo de Evaluación (mín):</label>
																<div class="col-md-6"><input class="form-control" id="time" name="time" type="text" placeholder="Tiempo de la evaluación" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['time']; ?>"<?php } ?> /></div>
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-5">
															<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-5 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
																<div class="col-md-7">
																	<select style="width: 100%;" id="status_id" name="status_id">
																		<option value="1" <?php if($registroConfiguracion['status_id']=="1"){ ?>selected="selected"<?php } ?>>Activo</option>
																		<option value="2" <?php if($registroConfiguracion['status_id']=="2"){ ?>selected="selected"<?php } ?>>Inactivo</option>
																	</select>
																</div>
															</div>
															<!-- // Group END -->
															<?php } ?>
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-2">
														</div>
														<!-- // Column END -->
													</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
												<div class="row innerLR">
													<!-- Column -->
													<div class="col-md-5">
														<label class="col-md-6 control-label" for="cant_question" style="padding-top:8px;">Cantidad de Preguntas:</label>
															<div class="col-md-6"><input class="form-control" id="cant_question" name="cant_question" type="text" placeholder="Preguntas que lanza la evaluación" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['cant_question']; ?>"<?php } ?> /></div>
													</div>
													<!-- // Column END -->
													<!-- Column -->
													<div class="col-md-5">
													</div>
													<!-- // Column END -->
													<!-- Column -->
													<div class="col-md-2">
													</div>
													<!-- // Column END -->
												</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Form actions -->
												<div class="form-actions">
													<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
														<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
														<input type="hidden" id="opcn" name="opcn" value="crear">
													<?php }elseif(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
														<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
														<input type="hidden" id="opcn" name="opcn" value="editar">
														<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
													<?php } ?>
													<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
												</div>
												<!-- // Form actions END -->
											</form>
										</div>
									</div>
								</div>
							</div>
							<!-- fin div.row formulario evaluacion -->
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
							<br>
							<!-- div.row lista de agrupaciones creadas -->
							<div class="row">
								<div class="col-md-6">
									<div class="widget">
										<!-- Widget heading -->
										<div class="widget-head">
											<h4 class="heading">Agrupación de preguntas</h4>
										</div>
										<!-- // Widget heading END -->
										<div class="widget-body innerAll inner-2x">
											<!-- Table -->
											<table class="table table-bordered table-primary" id="tb_agrupacion">
												<!-- Table heading -->
												<thead>
													<tr>
														<th class="center">No.</th>
														<th>Agrupación</th>
														<th>preguntas</th>
														<th>opciones</th>
													</tr>
												</thead>
												<!-- // Table heading END -->
												<!-- Table body -->
												<tbody class="lista_agrupaciones">

												</tbody>
												<!-- // Table body END -->
											</table>
											<!-- // Table END -->

										</div>
									</div>
								</div>
							</div>
							<!-- Fin div.row lista de agrupaciones creadas -->
							<br>
							<!-- div.row lista de preguntas -->
							<div class="row">
								<div class="col-md-12">
									<div class="widget">
										<div class="widget-head">
											<h4 class="heading">Preguntas Evaluaciones</h4>
										</div>
										<div class="widget-body">
											<form id="agregarPregunta">
												<div class="separator"></div>
												<!-- Row -->
												<div class="row innerLR">
													<!-- Column -->
													<div class="col-md-12">
														<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
															<!-- Table heading -->
															<thead>
																<tr>
																	<th>PREGUNTA</th>
																	<th>RESPONSABLE</th>
																	<th style="width: 5%;">PESO</th>
																	<th style="width: 5%;">AGRUPACION</th>
																	<th style="width: 5%;">ACT/DES</th>
																</tr>
															</thead>
															<!-- // Table heading END -->
															<tbody id="list_preguntasSel">
																<!-- Lista de preguntas seleccionadas -->
															</tbody>
														</table>
													</div>
													<!-- // Column END -->
												</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
												<div class="row innerLR">
													<!-- Column -->
													<div class="col-md-12">
														<!-- Group -->
														<div class="form-group">
															<label class="col-md-1 control-label" for="question_id" style="padding-top:8px;">Pregunta:</label>
															<div class="col-md-11">
																<select style="width: 100%;" id="question_id" name="question_id">

																</select>
															</div>
														</div>
														<!-- // Group END -->
													</div>
													<!-- // Column END -->
												</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
												<div class="row innerLR">
													<!-- Column -->
													<div class="col-md-5">
														<!-- Group -->
														<div class="form-group">
															<label class="col-md-4 control-label" for="value" style="padding-top:8px;">Peso Pregunta:</label>
															<div class="col-md-8"><input class="form-control" id="value" name="value" type="text" placeholder="Peso de la pregunta Ej 10"/></div>
														</div>
														<!-- // Group END -->
													</div>
													<!-- // Column END -->
													<!-- Column -->
													<div class="col-md-5">
														<div class="form-group">
															<label class="col-md-4 control-label" for="select_agrup" style="padding-top:8px;">Agrupación:</label>
															<div class="col-md-8">
																<select style="width: 100%;" id="select_agrup" name="agrup">
																</select>
															</div>
														</div>
													</div>
													<!-- // Column END -->
												</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
												<div class="row innerLR">
													<div class="col-md-3">
														<!-- Form actions -->
														<div class="form-actions">
															<input type="hidden" id="review_id" name="review_id" value="<?php echo $_GET['id']; ?>">
															<button type="submit" class="btn btn-success" id="btn_agregarPregunta"><i class="fa fa-check-circle"></i> Agregar Pregunta</button>
														</div>
														<!-- // Form actions END -->
													</div>
												</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<div class="separator"></div>
												<div class="separator"></div>
											</form>
										</div>
									</div>
								</div>
							</div>
							<!-- Fin div.row lista de preguntas -->
							<?php } ?>
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/evaluaciones.js"></script>
</body>
</html>
