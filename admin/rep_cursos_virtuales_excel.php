<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_cursos_virtuales.php');
}
//echo('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Cursos_Virtuales.xls");
header ("Content-Transfer-Encoding: binary");
if(isset($_GET['start_date_day'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th>CONCESIONARIO</th>
                <th>CODIGO</th>
                <th>ESPECIALIDAD</th>
                <th>CURSO</th>
                <th>SEDE</th>
                <th>CIUDAD</th>
                <th>ZONA</th>
                <th>ALUMNO</th>
                <th>IDENTIFICACION</th>
                <th>INTENTO</th>
                <th>#INTENTOS</th>
                <th>FECHA</th>
                <th>PUNTAJE</th>
                <th>APROBACION</th>
                <th>DATOS</th>
                <th>AVANCE</th>
                <th>PUNTAJE-HISTORICO-APROBADO</th>
                <th>FECHA</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php
                foreach ($datosCursos_all as $iID => $data) {
                    $Var_Superado = 0;
                    $Var_Fecha = "En Proceso";
                    $Var_Res = "No Aprobado";
                    $_varEstilo = "label-danger";
                    if( isset($data['respuestas']) ){
                        $Var_Superado = $data['respuestas'][0]['puntaje'];
                        $Var_Fecha = $data['respuestas'][0]['date_creation'];
                        if($Var_Superado>79){
                            $Var_Res = "Aprobado";
                            $_varEstilo = "label-success";
                        }
                    }
                    $pase_vr = 0;
                    if(isset($data['pase'])){
                        $pase_vr = $data['pase'];
                    }
                    $cuantos_vr = 0;
                    if(isset($data['cuantos'])){
                        $cuantos_vr = $data['cuantos'];
                    }
                     ?>
                        <!-- Item -->
                        <tr class="selectable">
                            <td><?php echo(utf8_decode($data['dealer'])); ?></td>
                            <td><?php echo(utf8_decode($data['newcode'])); ?></td>
                            <td><?php echo(utf8_decode($data['specialty'])); ?></td>
                            <td><?php echo(utf8_decode($data['course'])); ?></td>
                            <td><?php echo(utf8_decode($data['headquarter'])); ?></td>
                            <td><?php echo(utf8_decode($data['area'])); ?></td>
                            <td><?php echo(utf8_decode($data['zone'])); ?></td>
                            <td><?php echo(utf8_decode($data['first_name'].' '.$data['last_name'])); ?></td>
                            <td><?php echo($data['identification']); ?></td>
                            <td><?php echo($data['attempt']); ?></td>
                            <td><?php echo($data['cantAttempt']); ?></td>
                            <td><?php echo($Var_Fecha); ?></td>
                            <td><?php echo($Var_Superado); ?></td>
                            <td><?php echo($Var_Res); ?></td>
                            <td>[ <?php echo($pase_vr); ?> | <?php echo($cuantos_vr); ?> ]</td>
                            <td><?if($Var_Res=='Aprobado'){ ?>100<?php }else if($pase_vr>0){ echo(number_format($pase_vr/$cuantos_vr,2)*100); }else{ echo("0"); }?> %</td>
                            <?php if(isset($data['histo'])){?>
                              <td><?php echo($data['histo']['score']); ?></td>
                              <td><?php echo($data['histo']['fecpaso']); ?></td>
                            <?php }else{?>
                              <td></td>
                              <td></td>
                            <?php }?>
                        </tr>
                        <!-- // Item END -->
                    <?php
                    }
                    ?>
        </tbody>
    </table>
<?php } ?>
