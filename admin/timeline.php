<?php include('src/seguridad.php'); ?>
<?php include('controllers/timeline.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>

	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons git_merge"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/timeline.php">TimeLines</a></li>
				</ul>
				<!-- Inicio del cuerpo de la pagina -->
			<h1>Timelines <span>Usuario</span></h1>
			<!-- Inicio Form para buscar timeline de usuario por fecha-->
			<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body">
					<div class="row">
						<form action="timeline.php" method="post" onsubmit="return validacion()">
							<div class="col-md-3">
								<!-- Group -->
								<div class="form-group">
									<label class="col-md-3 control-label" for="start_date_day" style="padding-top:8px;">Desde: </label>
									<div class="col-md-6 input-group date">
								    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Desde..." value="<?php if(isset($_POST['start_date_day'])) { ?><?php echo $_POST['start_date_day']; ?><?php }else if(isset($_GET['start_date_day'])){ echo($_GET['start_date_day']); }?>"/>
								    	<span class="input-group-addon">
								    		<i class="fa fa-th"></i>
								    	</span>
									</div>
								</div>
								<!-- // Group END -->
							</div>
							<div class="col-md-3">
								<!-- Group -->
								<div class="form-group">
									<label class="col-md-3 control-label" for="end_date_day" style="padding-top:8px;">Hasta: </label>
									<div class="col-md-6 input-group date">
								    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..." value="<?php if(isset($_POST['end_date_day'])) { ?><?php echo $_POST['end_date_day']; ?><?php }else if(isset($_GET['end_date_day'])){ echo($_GET['end_date_day']); }?>"/>
								    	<span class="input-group-addon">
								    		<i class="fa fa-th"></i>
								    	</span>
									</div>
								</div>
								<!-- // Group END -->
							</div>
								<div class="col-md-3">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-4 control-label" for="identificacion" style="padding-top:8px;">Identificación</label>
												<div class="col-md-8 input-group">
													<input class="form-control" type="text" id="identificacion" name="identificacion" value="<?php if(isset($_POST['identificacion'])) { ?><?php echo $_POST['identificacion']; ?><?php }else if(isset($_GET['identificacion'])){ echo($_GET['identificacion']); }?>">
												</div>
											</div>
											<!-- // Group END -->
										</div>
							<div class="col-md-2">
							<input type="hidden" name="opcn" value = "buscar">
								<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
								<div class="checkbox" style="position: relative;left: -235px;">
									<label class="checkbox-custom">
										<input type="checkbox" name="checkbox" value="si">
										<i class="fa fa-fw fa-square-o"></i> Incluir navegación
									</label>
								</div>
							</div>

						</form>

					</div>

				</div>
			</div>
			<?php
			 //foreach ($infor as $key => $datos) {
   // echo $datos['fecha'].' '.$datos['evento'].' '.$datos['name'].' '.$datos['descripcion'].'<br/>';} ?>
			<!-- Fin Form para buscar timeline de usuario por fecha-->
			<!-- Inicio cuadro timeline -->
			<?php if(isset($infor) && count($infor)>0){ ?>
	<div class="widget widget-inverse">
		<div class="widget-head"><h4 class="heading"><i class="fa fa-sitemap fa-fw"></i> Secuencia Timeline</h4></div>
			<div class="widget-body padding-none">
				<div class="relativeWrap overflow-hidden">
					<div class="row row-merge layout-timeline layout-timeline-mirror">
						<div class="col-sm-6"></div>
							<div class="col-sm-6">
								<div class="innerAll">
									<ul class="timeline">
										<li class="active">
											<div class="separator bottom">
												<span class="date box-generic">Inicio</span>
												<span class="type glyphicons user">Perfil <i></i></span>
											</div>
											<?php if(isset($_POST['opcn'])||isset($_GET['opcn'])){ ?>
											<div class="widget widget-heading-simple widget-body-white margin-none">
												<div class="widget-body">
													<div class="media">
														<div class="media-object pull-left thumb">  <?php if ($usuario!='') { ?><img class="pull-left" src="../assets/images/usuarios/<?php echo $usuario['image']; ?>" style="width: 35px;" alt="" /></div>
														<div class="media-body"> <?php } ?>

															<a class="author">
															 	<?php if ($usuario=='') {echo "Usuario no existe, verifique el documento de identidad";} echo $usuario['first_name'].' '.$usuario['last_name'] ; ?></a><br/>
															<span class="muted"><?php echo $usuario['email']; ?></span>
														</div>
													</div>
													<div class="alert alert-gray">
														<p class="glyphicons circle_info margin-none"><i></i> Última navegación <strong><?php echo($uconexion ['ultimavez']).'<br>'; if (isset($mensaje)) {echo($mensaje);}?></strong></p>
													</div>
												</div>
											</div>
										</li>
											<?php $i=1; foreach ($infor as $key => $datos) { $i++?>
										<li class="active">
											<span class="type glyphicons clock"><?php if ($i%2!=0){ echo "- ";} ?>Fecha<?php if ($i%2==0){ echo " -";} ?> <i></i><span class="time" style="width: 130px;"><?php echo $datos ['fecha']; ?></span></span>
											<div class="widget widget-heading-simple widget-body-white margin-none">
												<div class="widget-body">
													<p class="<?php
													 if ($datos['evento']=='cargo'){ echo "glyphicons briefcase";}
													 if ($datos['evento']=='excusa'){ echo "glyphicons hospital_h";}
													 if ($datos['evento']=='foro'){ echo "glyphicons check";}
													 if ($datos['evento']=='invitacion') {echo "glyphicons link";}
													 if ($datos['evento']=='like') {echo "glyphicons thumbs_up";}
													 if ($datos['evento']=='visto'){ echo "glyphicons eye_open";}
													 if ($datos['evento']=='navegacion') {echo "glyphicons link";}
													 if ($datos['evento']=='answer') {echo "glyphicons check";}
													 if (is_numeric($datos['evento'])) {echo "glyphicons list";}
													 if ($datos['evento']=='Hruta') {echo "glyphicons bookt";}
													 ?>"><i></i> <strong><?php
													if ($datos['evento']=='cargo') { echo "Se le Asignó el cargo de = ".'</strong>'.$datos ['name'].'</p>';}
													if ($datos['evento']=='excusa') { echo "Presentó excusa para el curso de = ".'</strong>'.$datos ['descripcion'].'<br> '.'<a href="../assets/excusas/'.$datos['name'].'"'.'download="excusa"">Descargar excusa </a>';}
													if ($datos['evento']=='foro') { echo "Participo en el foro = ".'</strong>'.$datos ['name'].'.'.'<br>Cantidad de veces que participo en este foro = '; echo $datos ['descripcion'];echo ' <br> Última participación ';echo $datos ['fecha'];}
													if ($datos['evento']=='invitacion') { echo "Se le invitó al curso de = ".'</strong>'.$datos ['name'].'<br>La invitación está '; if($datos ['descripcion']==1){echo " abierta";}else echo "Cerrada";}
													if ($datos['evento']=='like') { echo "Like al archivo de la biblioteca = ".'</strong>'.$datos ['name'];}
													if ($datos['evento']=='visto') { echo "Miró archivo de la biblioteca = ".'</strong>'.$datos ['name'];}
													if ($datos['evento']=='navegacion') { echo "Navegó por = ".$datos ['name'].'<br/> </strong> Un total de '.$datos ['descripcion'].' segundos'.'</p>';}
													if ($datos['evento']=='answer') { echo "Respondió = ".'</strong>'.$datos ['name'].'<br/><span class="text-info"><b>Nota: </b>'.$datos ['score'].' - <b>Tiempo:</b> '.$datos ['time_r'].'<span> <br/><br/><a href="timeline.php?reviews_answer_id='.$datos['idEvento'].'&identificacion='.$id.'&start_date_day='.$start_date_day.'&end_date_day='.$end_date_day.'&opcn=buscar" onclick="return confirmar()" class="btn btn-danger"> REINICIAR </a></p>';}
													if (is_numeric($datos['evento'])) { echo "Se le Calificó el curso  = ".'</strong>'.$datos ['descripcion'].'<br>'.'<span class="text-info"><b>Puntaje = </b>'.$datos ['name'].'<span>';}
													if ($datos['evento']=='Hruta') { echo "Llenó preguntas de profundización = ".'</strong>'.$datos ['name'].' <br> con un puntaje = '.$datos['descripcion'];}
													?>
													</strong> </p>
												</div>
											</div>
										</li>
										<?php }}?>
											<li class="active">
											<div class="separator bottom">
												<span class="date box-generic">Fin</span>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<!-- Fin cuadro timeline -->
				<!-- FIN del cuerpo de la pagina -->
		</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/timeline.js"></script>
</body>
</html>
