<?php
Class RepCursosVirtuales {
	// ==============================================================
	// Andres Vega
	// 01/12/2016
	// Funcion que retorna el listado de notas por cursos virtuales segun el filtro
	// ==============================================================
	public static function consultaDatosAll( $p ){
		include_once('../config/init_db.php');
		@session_start();

		$dealer_id = $_SESSION['dealer_id'];
		$filtro = "";
		$filtro_2 = "";
		$course_id = "";

		//si no es un Admin/SuperAdmin se consulta solo de su mismo concesionario
		if(isset($_SESSION['max_rol']) && $_SESSION['max_rol'] < 5 ){
			$filtro = " AND d.dealer_id = $dealer_id";
		}elseif($p['dealer_id'] != ""){
			$filtro = " AND d.dealer_id IN ( {$p['dealer_id']} )";
		}

		if(isset($_SESSION['max_rol']) && $_SESSION['max_rol'] < 5 ){
			$filtro_2 = " AND d.dealer_id = $dealer_id";
		}elseif($p['dealer_id'] != ""){
			$filtro_2 = " AND d.dealer_id IN ({$p['dealer_id']})";
		}

		//Filtra por el curso seleccionado
		if( isset($p['course_id']) && $p['course_id'] > 0 ){
			$course_id = " AND co.course_id = {$p['course_id']}";
		}

		$query_sql = "SELECT d.dealer, h.headquarter, a.area, z.zone, u.first_name, u.last_name, u.identification, m.module, co.newcode, co.course, sp.specialty, t.user_id, s.module_id, MAX(t.attempt) as attempt, COUNT(distinct t.attempt) as cantAttempt
				FROM ludus_scorm_track t, ludus_scorm_sco c, ludus_scorm s, ludus_modules m, ludus_courses co, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, ludus_specialties sp
				WHERE t.scorm_sco_id = c.scorm_sco_id AND c.scorm_id = s.scorm_id
				AND t.datetime BETWEEN '{$p['start_date_day']}' AND '{$p['end_date_day']}'
				AND s.module_id = m.module_id
				AND m.course_id = co.course_id
				AND t.user_id = u.user_id
				AND u.headquarter_id = h.headquarter_id
				AND h.dealer_id = d.dealer_id
				AND h.area_id = a.area_id
				AND a.zone_id = z.zone_id
				AND co.specialty_id = sp.specialty_id
				$course_id
				$filtro
				GROUP BY d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, m.module, co.course, t.user_id, s.module_id
				ORDER BY co.course, d.dealer, h.headquarter, u.first_name, u.last_name";
		// echo($query_sql."<br>");
		$Rows_config = DB::query($query_sql);

		$query_res = "SELECT o.user_id, o.module_id, p.puntaje, o.date_creation
						FROM ludus_modules_results_usr o, (SELECT r.user_id, m.module_id, MAX(r.score) as puntaje
							FROM ludus_modules_results_usr r, ludus_modules m, ludus_users u, ludus_headquarters h, ludus_dealers d
							WHERE r.module_id = m.module_id
							AND m.type = 'WBT'
							AND r.date_creation BETWEEN '{$p['start_date_day']}' AND '{$p['end_date_day']}'
							AND r.user_id = u.user_id
							AND u.headquarter_id = h.headquarter_id
							AND h.dealer_id = d.dealer_id
							$filtro_2
							GROUP BY r.user_id, r.module_id ) p
							WHERE o.user_id = p.user_id
							AND o.date_creation BETWEEN '{$p['start_date_day']}' AND '{$p['end_date_day']}'
							AND o.module_id = p.module_id
							AND o.score = p.puntaje
							GROUP BY o.user_id, o.module_id";
		// echo($query_res);
		$Rows_res = DB::query($query_res);

		$query_histo = "SELECT r.user_id, m.module_id, MAX(r.score) as score, r.date_creation as fecpaso
							FROM ludus_modules_results_usr r, ludus_modules m
							WHERE m.type = 'WBT' AND m.module_id = r.module_id AND r.date_creation < '{$p['start_date_day']} 00:00:00' AND r.approval = 'SI'
							GROUP BY r.user_id, m.module_id";
		$Rows_histo = DB::query($query_histo);

		$query_cuantos = "SELECT c.module_id, s.scorm_id, count(s.scorm_sco_id) as cuantos
						FROM ludus_scorm_sco s, ludus_scorm c
						WHERE s.scormtype = 'sco' AND s.scorm_id = c.scorm_id GROUP BY c.module_id, s.scorm_id ORDER BY `s`.`scorm_id` ASC";
		$Rows_cuantos = DB::query($query_cuantos);

		$query_pase = "SELECT t.user_id, c.module_id, count(distinct t.scorm_sco_id) as pase
			FROM ludus_scorm_track t, ludus_scorm_sco s, ludus_scorm c
			WHERE t.element = 'cmicorelessonstatus'
			AND t.value = 'completed'
			AND t.scorm_sco_id = s.scorm_sco_id
			AND s.scormtype = 'sco'
			AND s.scorm_id = c.scorm_id
			GROUP BY t.user_id, c.module_id";

		$Rows_pase = DB::query($query_pase);
		$Rows_res = DB::query($query_res);

		$cuenta = count($Rows_config);
		$respuestas = count($Rows_res);
		$cuantos = count($Rows_cuantos);
		$pase = count($Rows_pase);

		for($i=0;$i< $cuenta ;$i++) {
			$var_user_id = $Rows_config[$i]['user_id'];
			$var_module_id = $Rows_config[$i]['module_id'];

			for($y=0;$y< $respuestas ;$y++) {
				if($var_user_id == $Rows_res[$y]['user_id'] && $var_module_id == $Rows_res[$y]['module_id'] ){
					$Rows_config[$i]['respuestas'][] = $Rows_res[$y];
				}
			}//fin for respuestas

			for( $z=0; $z< $cuantos; $z++ ) {
				if($var_module_id == $Rows_cuantos[$z]['module_id'] ){
					$Rows_config[$i]['cuantos'] = $Rows_cuantos[$z]['cuantos'];
				}
			}//fin for cuentos

			for($x=0;$x<$pase;$x++) {
				if($var_user_id == $Rows_pase[$x]['user_id'] && $var_module_id == $Rows_pase[$x]['module_id'] ){
					$Rows_config[$i]['pase'] = $Rows_pase[$x]['pase'];
				}
			}//fin for pase

			for($w=0;$w<count($Rows_histo);$w++) {
				if($var_user_id == $Rows_histo[$w]['user_id'] && $var_module_id == $Rows_histo[$w]['module_id'] ){
					$Rows_config[$i]['histo'] = $Rows_histo[$w];
				}
			}//fin historico

		}//fin for registros

		return $Rows_config;
	}//end function consultaDatosAll
	// ==============================================================
	// Andres Vega
	// 01/12/2016
	// Funcion que lista los concesionarios y sus correspondientes id
	// ==============================================================
	public static function consultaConcesionarios(){
		include_once('../config/init_db.php');
		$query_sql = "SELECT d.dealer_id, d.dealer FROM ludus_dealers d WHERE d.status_id = 1";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}//end function consultaConcesionarios
	// ==============================================================
	// Andres Vega
	// 2018-02-07
	// Funcion que lista los cursos virtuales para realizar un filtro
	// ==============================================================
	public static function consultaCursosVirtuales(){
		include_once('../config/init_db.php');
		$query_sql = "SELECT c.course_id, c.course FROM ludus_courses c WHERE c.type = 'WBT' AND c.status_id = 1";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}//end function consultaConcesionarios

}
