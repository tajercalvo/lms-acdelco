<?php
Class Calificaciones {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
						FROM ludus_courses c
						WHERE c.status_id = 1 AND c.type NOT IN ('WBT') ORDER BY c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad($course_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.course, s.start_date, l.living, l.address, u.first_name, u.last_name, count(*) as cantidad
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i
			WHERE c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.schedule_id = i.schedule_id ";
			if($course_id!="0"){
				$query_sql .= " AND c.course_id = '$course_id' ";
			}
			$query_sql .= "GROUP BY c.course, s.start_date, l.living, l.address, u.first_name, u.last_name";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatosAll($course_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.course, s.start_date, l.living, l.address, u.first_name, u.last_name, count(*) as cantidad
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i
			WHERE c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.schedule_id = i.schedule_id ";
			if($course_id!="0"){
				$query_sql .= " AND c.course_id = '$course_id' ";
			}
			$query_sql .= "GROUP BY c.course, s.start_date, l.living, l.address, u.first_name, u.last_name";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
