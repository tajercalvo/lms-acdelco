<?php

class RepFacturacion{
    public static function consultaDatosCursos( $p, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, s.living_id, s.min_size, s.max_size, s.status_id, s.course_id, s.teacher_id, s.inscriptions, 
                c.course, c.type, m.duration_time, sp.specialty_id, sp.specialty, 0 AS Asistentes, 0 AS Invitados, 0 AS Capacitacion
            FROM ludus_schedule s, ludus_courses c, ludus_modules m, ludus_specialties sp
            WHERE s.course_id = c.course_id
            AND c.course_id = m.course_id
            AND c.specialty_id = sp.specialty_id
            AND s.status_id = 1
            AND s.start_date BETWEEN '{$p['start_date_day']} 00:00:00' AND '{$p['end_date_day']} 23:59:59'";
            // echo( $query_sql );
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
    }
    
    public static function consultaDetalle( $p, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT d.dealer_id, d.dealer, h.headquarter, a.area, z.zone, u.identification, u.first_name, u.last_name, c.charge, u.date_startgm, u.status_id as estadoestu, u.date_session, b.*
            FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, 
            (SELECT user_id, charge_id, count(*) FROM ludus_charges_users GROUP BY user_id) cu, ludus_charges c,
            (SELECT m.user_id, c.newcode, c.course, mo.duration_time, c.type, s.start_date, s.end_date, l.living, u.first_name as first_prof, 
            u.last_name as last_prof, u.identification as iden_prof, i.status_id as asistencia, m.score, m.score_waybill, m.score_review, m.score_evaluation, sp.specialty, 
            su.supplier, MONTH(s.start_date) as mes, s.schedule_id, m.module_result_usr_id
            FROM ludus_invitation i, ludus_schedule s, ludus_modules_results_usr m, ludus_courses c, ludus_users u, ludus_modules mo, 
            ludus_livings l, ludus_specialties sp, ludus_supplier su
            WHERE i.schedule_id = s.schedule_id AND i.invitation_id = m.invitation_id AND s.course_id = c.course_id AND 
            s.teacher_id = u.user_id AND c.course_id = mo.course_id AND s.living_id = l.living_id
            AND c.specialty_id = sp.specialty_id AND c.supplier_id = su.supplier_id AND s.start_date BETWEEN '{$p['start_date_day']} 00:00:00' AND '{$p['end_date_day']} 23:59:59') b
            WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND 
            a.zone_id = z.zone_id AND u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND u.user_id = b.user_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}