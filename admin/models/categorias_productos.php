<?php
Class Especialidades {

	function consulta_categorias($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.argument_cat_id, c.argument_cat, c.status_id, c.date_creation, c.date_edition, c.color,
							u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
						FROM ludus_argument_cat c, ludus_users u, ludus_users s, ludus_status e
						WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id ";
			if($sWhere!=''){
				$query_sql .= " AND ( c.argument_cat LIKE '%$sWhere%' or c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%'  )";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crear_categoria($nombre, $estado, $color){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){echo 'su sesión a expirado'; return false;}
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_argument_cat
							(
							argument_cat,
							status_id,
							creator,
							date_creation,
							color,
							editor,
							date_edition)
							VALUES
							(
							'$nombre',
							$estado,
							$idQuien,
							NOW(),
							'$color',
							$idQuien,
							'');";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}

	function Actualizar_categoria($id,$nombre,$status){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){ echo 'su sesión a expirado'; return false;}
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		echo $updateSQL_ER = "UPDATE ludus_argument_cat
					SET
					argument_cat = '$nombre',
					status_id = $status,
					date_edition = now(),
					editor = $idQuien
					WHERE argument_cat_id = $id;";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
}
