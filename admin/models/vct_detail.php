<?php
Class CursosVCT {
	/*
	Funcion que consulta los participantes inscritos en un curso de VCT
	@idVCT = es el id del curso del que se quiere obtener registros
	*/
	function consultaRegistros($idVCT,$schedule_id,$prof=""){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT i.inscription_id,i.course_id,i.status_id, u.user_id, u.last_name, u.first_name, u.image, h.headquarter, d.dealer
			FROM ludus_courses c, ludus_headquarters h, ludus_users u, ludus_inscriptions i, ludus_dealers d
			WHERE c.course_id = i.course_id
			AND u.headquarter_id = h.headquarter_id
			AND i.user_id = u.user_id AND i.schedule_id = '$schedule_id'
      		AND d.dealer_id = h.dealer_id
			AND c.course_id = '$idVCT'";//LIMIT 0,20
		//echo($query_sql);
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion consultaRegistros
	/*
	Funcion que consulta los participantes inscritos en un curso de VCT recientemente para actualizar con ajax
	@idVCT = es el id del curso del que se quiere obtener registros
	*/
	function consultaNuevoRegistros($idVCT,$lastId){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT i.inscription_id,i.course_id,i.status_id, u.user_id, u.last_name, u.first_name, u.image, h.headquarter, d.dealer
			FROM ludus_courses c, ludus_headquarters h, ludus_users u, ludus_inscriptions i, ludus_dealers d
			WHERE c.course_id = i.course_id
			AND u.headquarter_id = h.headquarter_id
			AND i.user_id = u.user_id
      		AND d.dealer_id = h.dealer_id
			AND c.course_id = '$idVCT'
			AND i.inscription_id > $lastId";//LIMIT 0,20
		//echo($query_sql);
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion consultaRegistros
	/*
	funcion para consultar la informacion general del curso
	@idVCT = es el id del curso del que se quiere obtener registros
	*/
	function consultaRegistroDetail($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, s.supplier
			FROM ludus_courses c, ludus_supplier s
			WHERE c.course_id = '$idRegistro' AND c.supplier_id = s.supplier_id ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion consultaRegistroDetail
	/*
	consulta las inscripciones hechas en un VCT
	@idCourse = es el id del registro que valida si esta inscrito o no a un curso
	*/
	function consultaInscripcion($idCourse,$schedule_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT inscription_id FROM ludus_inscriptions WHERE user_id = '$idQuien' AND course_id = '$idCourse' AND schedule_id = '$schedule_id' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectCantidad($query_sql);
		unset($DataBase_Class);
		if($Rows_config>0){
			return true;
		}else{
			return false;
		}
	}//fin funcion consultaInscripcion
	function DetProgramacion($schedule_id, $prof = "" ){
		@session_start();
		include_once( $prof.'../config/database.php');
		include_once( $prof.'../config/config.php');
		$query_sql = "SELECT s.*, u.first_name, u.last_name, u.identification, u.image
					FROM ludus_schedule s, ludus_users u
					WHERE s.teacher_id = u.user_id AND s.schedule_id = '$schedule_id' ";//LIMIT 0,20
					//echo($query_sql);
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	/*
	Trae un array con los chats hechos en un VCT
	@idVCT = es el id del curso del que se quiere obtener registros
	*/
	public function consultaComentarios($idVCT){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, u.user_id, u.first_name, u.last_name, u.image
			FROM ludus_users u, ludus_comments c
			WHERE u.user_id = c.user_id
			AND c.schedule_id = $idVCT";
		//echo($query_sql);
		$dataBase_Class = new Database();
		$Rows_config = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($dataBase_Class);
		return $Rows_config;
	}//fin funcion consultaComentarios
	/*
	Trae un array con los ultimos chats hechos en un VCT para refrescar el chat
	@idVCT = es el id del curso del que se quiere obtener registros
	*/
	//consulta comentarios cada cuando se da clic en al pestña, telecomunicaciones o cuando se da clic en actualizar chat
	public function consultaUltimosComentarios($idVCT,$lastId){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, u.user_id, u.first_name, u.last_name, u.image
			FROM ludus_users u, ludus_comments c
			WHERE u.user_id = c.user_id
			AND c.schedule_id = $idVCT
			order by comments_id desc
			limit 20;";
			// AND c.comments_id > $lastId";
		//echo($query_sql);

		$dataBase_Class = new Database();
		$Rows_config = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		//
		if (count($Rows_config)>0) {
			# code...
		
				foreach ($Rows_config as $key => $datos) {
		   		 $aux[$key] = $datos['date_comment']; // Recorriendo el array y ordenandolo por fechas
				}
				array_multisort($aux, SORT_ASC, $Rows_config); // Ordenando las fechas del aray en orden de menor a mayor
				// 
			}
		unset($dataBase_Class);
		return $Rows_config;
	}//fin funcion consultaUltimosComentarios
	/*
	Agrega un registro a la tabla ludus_comments
	Esta funcion es llamada desde ajax
	@idVCT = es el id del VCT en el cual se realizo el comentario
	*/	
	//consulta comentarios cada 5 segundos, segun la funcion automatica de ajax en el js
		public function consultaUltimosComentarios2($idVCT, $lastId){
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, u.user_id, u.first_name, u.last_name, u.image
			FROM ludus_users u, ludus_comments c
			WHERE u.user_id = c.user_id
			AND c.schedule_id = $idVCT
			AND u.user_id <> $idQuien
			AND comments_id > $lastId
			order by comments_id desc
			limit 10;";
			// AND c.comments_id > $lastId";
		//echo($query_sql);

		$dataBase_Class = new Database();
		$Rows_config = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		//
		if (count($Rows_config)>0) {
			foreach ($Rows_config as $key => $datos) {
		   		 $aux[$key] = $datos['date_comment']; // Recorriendo el array y ordenandolo por fechas
				}
				array_multisort($aux, SORT_ASC, $Rows_config); // Ordenando las fechas del aray en orden de menor a mayor
		}
		
		// 

		unset($dataBase_Class);
		return $Rows_config;
	}//fin funcion consultaUltimosComentarios2, consulta los comentarios finales menos los del mismo usuario

	public function consultaDatos_usuarios(){
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.* FROM ludus_users u WHERE u.user_id = $idQuien;";
		$dataBase_Class = new Database();
		$datos_usuario = $dataBase_Class->SQL_SelectRows($query_sql);
		unset($dataBase_Class);
		unset($datos_usuario['password']);
		//print_r($datos_usuario);
		return $datos_usuario;
	}//fin funcion consultaUltimosComentarios2, consulta los comentarios finales menos los del mismo usuario

	public function insertarComentario($idVCT,$texto, $idMsjUsuario){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$timeComment = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$query_sql = "INSERT INTO ludus_comments (user_id,schedule_id,date_comment,comment_text, id_comments_users)
			VALUES ($idQuien,$idVCT,'$timeComment','$texto', '$idMsjUsuario')";
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_Insert($query_sql);
		// 
		if ($resultado>= 0) {
			$query_sql = "SELECT date_comment, id_comments_users FROM  ludus_comments where comments_id = $resultado;";
			$resultado = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		}
		// 
		unset($dataBase_Class);
		return $resultado;
	}//fin insertarComentario

	public function Cant_msj_enviados($idVCT){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$timeComment = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$query_sql = "SELECT * FROM ludus_comments 
			WHERE schedule_id = 3924
			and user_id = 16384;";
		//echo($query_sql);
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($dataBase_Class);
		return $resultado;
	}//fin funcion consultarProgramaciones

	
	/*
	Consulta las programaciones hechas sobre un curso VCT
	*/
	public function consultarProgramaciones($idVCT){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$timeComment = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$query_sql = "SELECT * FROM ludus_schedule s
		WHERE s.course_id = $idVCT
		AND s.start_date > '$timeComment'
		ORDER BY s.start_date";
		//echo($query_sql);
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($dataBase_Class);
		return $resultado;
	}//fin funcion consultarProgramaciones
	/*
	Genera una nueva inscripcion a un curso virtual, cuando intenta ingresar a un VCT en curso
	*/
	public function nuevaInscripcion($schedule){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$timeComment = date("Y-m-d")." ".(date("H")).":".date("i:s");
		/*
		date_send =
		text =
		result_preview
		data_result
		creator
		*/
		$query_sql = "INSERT INTO ludus_invitation (schedule_id,user_id,status_id) VALUES ($schedule,$idQuien,1)";
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_Insert($query_sql);
		unset($dataBase_Class);
		return $resultado;
	}//fin funcion nuevaInscripcion
	/*
	Funcion que genera la inscripcion a un curso determinado
	@idCourse = es el id que tiene el curso al cual se quiere inscribir
	*/
	public function inscribirseCurso( $idCourse, $schedule_id, $prof="" ){
		@session_start();
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$insertSQL_EE = "INSERT INTO ludus_inscriptions (user_id,course_id,status_id,date_creation,schedule_id) VALUES ('$idQuien','$idCourse',1,NOW(),$schedule_id) ";//LIMIT 0,20
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_Insert($insertSQL_EE);
		unset($dataBase_Acciones);
		return $resultado;
	}//fin funcion inscribirseCurso
	/*
	Consulta los horarios que esten para un shedule
	*/
	public function horarios($idschedule){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_schedule WHERE schedule_id = $idschedule";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion horarios
	/*
	*Andres Vega
	*06/03/2017
	*Funcion que consulta los archivos que se mostraran en un curso VCT (videos)
	*/
	public function consultaVCTFiles( $schedule_id ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_vct_files WHERE schedule_id = $schedule_id AND status_id = 1";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	//================================================================================================
	//Valida que el usuario ya este inscrito en un schedule especifico
	//================================================================================================
	public function yaInscrito( $user, $schedule_id, $prof = "" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT COUNT(user_id) as cantidad FROM ludus_inscriptions
			WHERE schedule_id = $schedule_id AND user_id = $user AND status_id = 1";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config['cantidad'];
	}//fin funcion yaInscrito
	//================================================================================================
	//valida que un usuario este invitado a un schedule
	//================================================================================================
	public function invitado( $user, $schedule_id, $prof = "" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT COUNT(user_id) as cantidad FROM ludus_invitation
			WHERE schedule_id = $schedule_id AND user_id = $user";//status_id = 1 LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config['cantidad'];
	}//fin function invitado


}//fin clase CursosVCT
