<?php
Class RepEvaluaciones {
	function consultaDatosAll($review_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		//$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, r.review, r.code, a.score, a.available_score
					FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r
					WHERE a.review_id = '$review_id' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' 
					AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id";
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&& $_SESSION['max_rol'] <= 4 ){
			$query_sql .= " AND d.dealer_id = $dealer_id";
		}
		$query_sql .= " ORDER BY d.dealer, h.headquarter, u.first_name, u.last_name";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		/*$query_res = "SELECT a.reviews_answer_id, ad.result, q.question, q.code, ans.answer, ans.result as res_ans
					FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans
					WHERE a.review_id = '$review_id' AND a.reviews_answer_id = ad.reviews_answer_id AND 
					a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' 
					AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id";
		$Rows_res = $DataBase_Acciones->SQL_SelectMultipleRows($query_res);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_reviews_answer_id = $Rows_config[$i]['reviews_answer_id'];

			for($y=0;$y<count($Rows_res);$y++) {
				if($var_reviews_answer_id == $Rows_res[$y]['reviews_answer_id']){
					$Rows_config[$i]['respuestas'][] = $Rows_res[$y];
				}
			}

		}*/

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function EvaluacionesEncuestas($tipo){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_evaluaciones = "SELECT * FROM ludus_reviews WHERE type IN ($tipo) ";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_evaluaciones);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
