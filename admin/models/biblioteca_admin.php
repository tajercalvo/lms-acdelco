<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
Class Libraries{

	function CrearLibraries($file, $name, $image, $status_id, $description, $segment_id){
		include_once( '../../config/init_db.php' );
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$views = 0;
		$likes = 0;
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$insertSQL_EE = "INSERT INTO ludus_libraries (file, name, image, status_id, description, detalle, specialty_id, views, likes, date_creation, user_id)
		VALUES ('$file', '$name', '$image', $status_id, '$description', '$description', '$segment_id', $views, $likes, '$NOW_data', $idQuien)";
		$Rows_config = DB::query( $insertSQL_EE );

		$res = array();
		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = 'Archivo creado correctamente, compartido a: ';
			$res['type'] = 'success';
			$res['library_id'] = DB::insertId();
		}else{
			$res['error'] = true;
			$res['msj'] = 'No se pudo crear el archivo';
			$res['type']= 'error';
		}
		return $res;
	}


	function ActualizarLibraries($library_id, $file, $image,  $name, $status_id, $description, $segment_id){
		include_once( '../../config/init_db.php' );
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		//Eliminando registros, para insertar los nuevos
		$updateSQL_DEL1 = "DELETE FROM ludus_library_charge where library_id = $library_id;";
		$Rows_config = DB::query( $updateSQL_DEL1 );

		$updateSQL_DEL3 = "DELETE FROM ludus_library_dealer where library_id = $library_id;";
		$Rows_config = DB::query( $updateSQL_DEL3 );

		$updateSQL_DEL5 = "DELETE FROM ludus_library_rol where library_id = $library_id;";
		$Rows_config = DB::query( $updateSQL_DEL5 );

		$file = $file == "" ? "" : "file = '$file', " ;
		$image = $image == "" ? "" : "image = '$image', " ;
		
		$updateSQL_ER = "UPDATE ludus_libraries
								SET
								name = '$name',
								$file
								$image
								status_id		= $status_id,
								description		='$description',
								detalle			='$description',
								specialty_id	= $segment_id,
								date_creation	= '$NOW_data',
								user_id			= $idQuien
								WHERE library_id= $library_id";
		$Rows_config = DB::query( $updateSQL_ER );

		$res = array();
		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = 'Archivo actualizado correctamente, compartido a: ';
			$res['type'] = 'success';
			// $res['library_id'] = DB::insertId();
		}else{
			$res['error'] = true;
			$res['msj'] = 'No se pudo actualizar el archivo';
			$res['type']= 'error';
		}
		return $res;
	}

	function consulta_cargos_usuarios( $ajax ){
		@session_start();
		include_once( $ajax.'../config/database.php');
		include_once( $ajax.'../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$DataBase_Acciones = new Database();
		$id_usuario = $_SESSION['idUsuario'];

		$query_sql = "SELECT * FROM ludus_charges_users where user_id = $id_usuario;";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	// Eliminar libreria
	function eliminarLibraries($id){
		include_once( '../../config/init_db.php' );
		$query_sql = "DELETE FROM ludus_libraries WHERE library_id=$id;";
		$Rows_config = DB::query($query_sql);

		$res = array();
		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = 'Archivo eliminado correctamete';
			$res['type'] = 'success';

		}else{
			$res['error'] = true;
			$res['msj'] = 'No se pudo eliminar el archivo';
			$res['type']= 'error';

		}
		return $res;
	}

	// consultar todos los archivos sin excepcion
	function consultaLibraries( $ajax = '../'){
		include_once( $ajax.'../config/init_db.php' );
		DB::$encoding = 'utf8'; // defaults to latin1 if omitted
		@session_start();

		$query_sql = "SELECT l.*, u.image as imguser, u.first_name, u.last_name, s.specialty, est.status
		FROM ludus_libraries l, ludus_users u, ludus_specialties s, ludus_status est
		WHERE l.user_id = u.user_id
        and s.specialty_id = l.specialty_id
        and l.status_id = est.status_id
        ORDER BY l.date_creation DESC";
		$Rows_config = DB::query($query_sql);

		foreach ( $Rows_config as $key => $value ) {
				if( $value['status_id'] == 1){
					$Rows_config[$key]['estado'] = '<span class="label label-success">Activo</span>';
				}else{
					$Rows_config[$key]['estado'] = '<span class="label label-danger">Inactivo</span>';
				}

			}
			//print_r($Rows_config);
		return $Rows_config;
	}

	// consultar todos los archivos sin excepcion
	function getSpecialities()
	{
		include_once( '../config/init_db.php' );
		DB::$encoding = 'utf8'; // defaults to latin1 if omitted
		@session_start();

		$query_sql = "SELECT * FROM ludus_specialties where status_id = 1";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}
	// consultar un archivo por id
	function consultar_libreria( $library_id ){
		include_once( '../../config/init_db.php' );
		DB::$encoding = 'utf8'; // defaults to latin1 if omitted

		$res = array();
		$query_sql = "SELECT l.*, u.image as imguser, u.first_name, u.last_name
						FROM ludus_libraries l, ludus_users u
						WHERE l.user_id = u.user_id and library_id=$library_id";
		$Rows_config = DB::query($query_sql);
		$res['libreria'] = $Rows_config[0];

		$query_cha = "SELECT * FROM ludus_library_charge where library_id = $library_id";
		$Rows_cha = DB::query($query_cha);
		$res['cargos'] = $Rows_cha;


		$query_des = "SELECT * FROM ludus_library_dealer where library_id = $library_id";
		$Rows_con = DB::query($query_des);
		$res['concesionarios'] = $Rows_con;

		$query_rol = "SELECT * FROM ludus_library_rol where library_id = $library_id";
		$Rows_rol = DB::query($query_rol);
		$res['roles'] = $Rows_rol;
		return $res;
	}

	// consultar todos los  tipos de archivos sin excepcion, de la tabla ludus_library_types
	function consultaTypeLibraries(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT library_segment_id, segment_library FROM ludus_library_segment;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaCargos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_charges where status_id=1 ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

		function consultaCargosid($library_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT llc.*, lc.charge
		FROM ludus_library_charge llc, ludus_charges lc
		WHERE llc.library_id = $library_id and lc.charge_id = llc.charge_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaCursosid($library_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT llc.*, lc.course
		FROM ludus_library_course llc, ludus_courses lc
		WHERE llc.library_id = $library_id and lc.course_id = llc.course_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}


	function consultaDealers(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_dealers where status_id=1 ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDealersid($library_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = " SELECT lld.*, ld.dealer
		FROM ludus_library_dealer lld, ludus_dealers ld
		WHERE lld.library_id = $library_id and ld.dealer_id = lld.dealer_id ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}


	function consultaRoles(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_roles where status_id=1 ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaRolesid($library_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT llr.*, lr.rol
		FROM ludus_library_rol llr, ludus_roles lr
		WHERE llr.library_id = $library_id and lr.rol_id = llr.rol_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearCargos($library_id, $Charges_sel ){
		@session_start();
		include_once( '../../config/init_db.php' );

		if ( $Charges_sel == 'null' ) {
			$select_id = "SELECT charge_id FROM ludus_charges;";
			$Charges_sel = DB::query($select_id);
		}else{
			$Charges_sel = explode(',',$Charges_sel);

			$select = array();
			foreach ($Charges_sel as $key => $value) {
				$select[]['charge_id']= $value;
			}
			$Charges_sel = $select;
		}

		$contador = 0;
		foreach ($Charges_sel as $key => $value) {
			$charge_id = $value['charge_id'];
			$insertSQL_EE = "INSERT INTO ludus_library_charge (library_id, charge_id) VALUES ($library_id, $charge_id);";
			$Rows_config = DB::query($insertSQL_EE);
			if($Rows_config){
				$contador++;
			}
		}

		$res['cargos'] = $contador.' perfiles de '.count($Charges_sel).' seleccionados';
		return $res;
	}

	function ActualizarCargos($library_charge_id, $library_id, $charge_id){
	@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "UPDATE ludus_library_charge SET library_id=$library_id, charge_id=$charge_id WHERE library_charge_id=$library_charge_id;";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}

	function CrearCursos($library_id, $course_id){
	@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_library_course (library_id, course_id) VALUES ($library_id, $course_id);";
		$resultado = $DataBase_Log->SQL_Update($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
		function ActualizarCursos($library_id, $course_id){
	@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_library_course (library_id, course_id) VALUES ($library_id, $course_id);";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}


	function CrearConcesionario($library_id, $dealer_Sel){
		include_once( '../../config/init_db.php' );

		if ( $dealer_Sel == 'null' ) {
			$select_id = "SELECT dealer_id from ludus_dealers";
			$dealer_Sel = DB::query($select_id);
		}else{
			$dealer_Sel = explode(',',$dealer_Sel);

			$select = array();
			foreach ($dealer_Sel as $key => $value) {
				$select[]['dealer_id']= $value;
			}
			$dealer_Sel = $select;
		}

		$contador = 0;
		foreach ($dealer_Sel as $key => $value) {
			$dealer_id = $value['dealer_id'];
			$insertSQL_EE = "INSERT INTO ludus_library_dealer (library_id, dealer_id) VALUES ($library_id, $dealer_id);";
			$Rows_config = DB::query($insertSQL_EE);
			if($Rows_config){
				$contador++;
			}
		}

		$res['concesionarios'] = $contador.' empresas de '.count($dealer_Sel).' seleccionados';
		return $res;
	}

	function CrearRoles($library_id, $rol_Sel){
		include_once( '../../config/init_db.php' );

		if ( $rol_Sel == 'null' ) {
			$select_id = "SELECT rol_id FROM ludus_roles;";
			$rol_Sel = DB::query($select_id);
		}else{
			$rol_Sel = explode(',',$rol_Sel);

			$select = array();
			foreach ($rol_Sel as $key => $value) {
				$select[]['rol_id']= $value;
			}
			$rol_Sel = $select;
		}

		$contador = 0;
		foreach ($rol_Sel as $key => $value) {
			$rol_id = $value['rol_id'];
			$insertSQL_EE = "INSERT INTO ludus_library_rol (library_id, rol_id) VALUES ($library_id, $rol_id);";
			$Rows_config = DB::query($insertSQL_EE);
			if($Rows_config){
				$contador++;
			}
		}

		$res['roles'] = $contador.' trayectorias de '.count($rol_Sel).' seleccionados';
		return $res;

	}

function CrearLibrerialibreria($library_dad, $library_child){
	@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_library_library (library_dad, library_child) VALUES ($library_dad, $library_child);";
		$resultado = $DataBase_Log->SQL_Update($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}


function CrearLike($library_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (isset($_SESSION['idUsuario'])) {
			$idQuien = $_SESSION['idUsuario'];
			$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
			$DataBase_Log = new Database();
			$insertSQL_ER = "INSERT INTO ludus_library_likes (user_id,date_edition,library_id) VALUES ('$idQuien','$NOW_data','$library_id')";
			$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
			if($resultado_IN>0){
				$updateSQL_ER = "UPDATE ludus_libraries
							SET likes = likes+1
							WHERE library_id = '$library_id'";
				$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
				unset($DataBase_Log);
				return $resultado;
			}
		} else{
			return 'sin_session';// simular un error en la peticion php
		}//END if
	}

// Quienes han dado like
function consulta_quien_like($library_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT last_name, first_name FROM ludus_users u, ludus_library_likes lll where
		lll.user_id = u.user_id
		and lll.library_id = $library_id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CreaView($library_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_library_views (library_id,user_id,date_edition) VALUES ('$library_id','$idQuien','$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Insert($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE ludus_libraries
						SET views = views+1
						WHERE library_id = '$library_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
			unset($DataBase_Log);
		}
		unset($DataBase_Log);
		return $resultado_IN;
	}



		function consultaLibrariesCourse(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT DISTINCT ll.*
		FROM ludus_libraries ll, ludus_library_course llc, ludus_modules_results_usr lmru, ludus_modules lm
		where ll.library_id = llc.library_id and llc.course_id = lm.course_id  and  lmru.module_id = lm.module_id and lmru.user_id = $idQuien";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

		function consultaLibrariesSerch($valor){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT * FROM ludus_libraries WHERE name LIKE '%$valor%' or description LIKE '%$valor%' ;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function quien_lo_vio(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "select l.library_id, l.views, u.first_name, u.last_name, u.identification, max(lv.date_edition) ultimo_vez_visto, l.name, l.description, count(*) veces_visto, h.headquarter, d.dealer
			from ludus_users u, ludus_libraries l, ludus_library_views lv, ludus_headquarters h, ludus_dealers d
			where u.user_id = lv.user_id
			and l.library_id = lv.library_id
			and u.headquarter_id = h.headquarter_id
			and h.dealer_id = d.dealer_id
			group by l.library_id, u.first_name, u.last_name, u.identification
			order by lv.date_edition asc;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function quien_lo_vio_ajax($id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (isset($_SESSION['idUsuario'])) {
				// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "select l.library_id, u.first_name, u.last_name, u.image, u.user_id, u.identification, max(lv.date_edition) ultimo_vez_visto, l.name, l.description, count(*) veces_visto, h.headquarter, d.dealer
			from ludus_users u, ludus_libraries l, ludus_library_views lv, ludus_headquarters h, ludus_dealers d
			where u.user_id = lv.user_id
			and l.library_id = lv.library_id
			and u.headquarter_id = h.headquarter_id
			and h.dealer_id = d.dealer_id
			and lv.library_id = $id
			group by l.library_id, u.first_name, u.last_name, u.identification
			order by lv.date_edition asc";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
			# code...
		}else{
			return 'sin_session';
		}

	}

	function misguardados(){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		if (isset($_SESSION['idUsuario'])) {
				// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
				$query_sql = "select l.*, s.date_edition as fechaguardado
								from
								ludus_libraries l, ludus_library_saved s
								where l.library_id = s.library_id
								and s.user_id = $idQuien;";
				$DataBase_Acciones = new Database();
				$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
				unset($DataBase_Acciones);
				return $Rows_config;
					# code...
		}else{
			return 'sin_session';
		}

	}

	function misCompartidos(){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		if (isset($_SESSION['idUsuario'])) {
				// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
				$query_sql = "select l.*, s.date_edition as fechaguardado, CONCAT(u.first_name,' ' ,u.last_name) as quien
								from
								ludus_libraries l, ludus_library_shared s, ludus_users u
								where l.library_id = s.library_id
                                and u.user_id = s.user_id
								and s.user_id_shared = $idQuien;";
				$DataBase_Acciones = new Database();
				$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
				unset($DataBase_Acciones);
				return $Rows_config;
					# code...
		}else{
			return 'sin_session';
		}

	}



	function quien_dio_like(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "select l.library_id, u.first_name, u.last_name, u.identification, ll.date_edition
				from ludus_users u, ludus_libraries l, ludus_library_likes ll
				where u.user_id = ll.user_id
				and l.library_id = ll.library_id
				group by l.library_id, u.first_name, u.last_name, u.identification";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	// Funciones para la nueva biblioteca

	function consultaProducto($ajax, $segmento){
		@session_start();
		if ($ajax == 0) {
			$ajax ='../';
		}else{
			$ajax ='';
		}
		if ($segmento == 0) {

		}
		include_once($ajax.'../config/database.php');
		include_once($ajax.'../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		// $query_sql = "SELECT * FROM ludus_libraries where segment_id = 1 order by RAND();";
		$query_sql = "SELECT * FROM ludus_libraries ";
		if ($segmento > 0) {
			$query_sql .= " where segment_id = $segmento ";
		}
		$query_sql .= "  order by RAND();";
		// echo "$query_sql";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaProductoVideos($segmento){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		// $query_sql = "SELECT * FROM ludus_libraries where segment_id = 1 and library_type_id = 2;";
		$query_sql = "SELECT * FROM ludus_libraries ";
		if ($segmento > 0) {
			$query_sql .= " where segment_id = $segmento ";
		}
		$query_sql .= " and library_type_id = 2;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaProductoAudiolibros($segmento){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		// $query_sql = "SELECT * FROM ludus_libraries where segment_id = 1 and library_type_id = 1 and file like '%mp3%';";
		$query_sql = "SELECT * FROM ludus_libraries ";
		if ($segmento > 0) {
			$query_sql .= " where segment_id = $segmento ";
		}
		$query_sql .= " and file like '%mp3%';";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaProductoArchivos($segmento){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		// $query_sql = "SELECT * FROM ludus_libraries where segment_id = 1 and library_type_id = 3 ;";
		$query_sql = "SELECT * FROM ludus_libraries ";
		if ($segmento > 0) {
			$query_sql .= " where segment_id = $segmento ";
		}
		$query_sql .= " and library_type_id = 3;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaTodo(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT * FROM ludus_libraries where segment_id = 1 and file like '%mp3%' order by RAND();";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

		function consultaLoultimo($ajax){
		@session_start();
		$agregar = $ajax == 0? '': '../';
		include_once($agregar.'../config/database.php');
		include_once($agregar.'../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT * FROM ludus_libraries order by date_creation desc";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaRecomendados($ajax){
		$agregar = $ajax == 0? '': '../';
		if ($this->validarsession()){
			@session_start();
		$cargo = $_SESSION['charge_id'];
		include_once($agregar.'../config/database.php');
		include_once($agregar.'../config/config.php');
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT ll.* FROM ludus_libraries ll, ludus_library_charge lc where lc.library_id = ll.library_id and lc.charge_id = $cargo order by rand()";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}else{

		return "sessionOff";
	}

	}

	function consultaMasvistos(){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_libraries order by views desc";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaMasvistosInicial(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_libraries order by rand() desc limit 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function validarsession(){
		@session_start();
		if (isset($_SESSION['idUsuario'])) {
			return true;
		}else{
			return false;
		}
	}

	function crearComentarios($libraryid, $comments, $id_comentario_usuario){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$comments = str_replace("'", "\'", $comments);
		$comments = str_replace('"', '\"', $comments);
		$insertSQL_EE = "INSERT INTO ludus_library_comments (library_id, user_id, date_edition, comments) VALUES ('$libraryid', '$idQuien', '$NOW_data ', '$comments');";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		if ($resultado>0) {
			$libraryid=["id"=>"$resultado", "fecha"=>"$NOW_data", "id_comentario_usuario"=> "$id_comentario_usuario"];
		}
		return $libraryid;
	}

	function guardarLibreria($libraryid){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$query_sql = "select * from ludus_library_saved where user_id = $idQuien and library_id = $libraryid";
		$Rows_config = $DataBase_Log->SQL_SelectRows($query_sql);

		if(count($Rows_config) == 0){
			$insertSQL_EE = "INSERT INTO ludus_library_saved
							(
							user_id,
							date_edition,
							library_id)
							VALUES
							(
							$idQuien,
							'$NOW_data',
							$libraryid);
							";
			$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
			$respuesta=["resultado"=>"SI"];
			unset($DataBase_Log);
		}else{
			$respuesta=["resultado"=>"NO"];
		}

		return $respuesta;
	}

	function compartirCon($usuario, $libraryid){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$query_sql = "SELECT * FROM ludus_library_shared where user_id = $idQuien and user_id_shared = $usuario and library_id = $libraryid;";
		$Rows_config = $DataBase_Log->SQL_SelectRows($query_sql);

		if(count($Rows_config) == 0){
			$insertSQL_EE = "INSERT INTO ludus_library_shared
							(
							user_id,
							date_edition,
							user_id_shared,
							library_id)
							VALUES
							(
							$idQuien,
							'$NOW_data',
							$usuario,
							$libraryid);
							";
			$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
			$respuesta=["resultado"=>"SI"];
			unset($DataBase_Log);
		}else{
			$respuesta=["resultado"=>"NO"];
		}

		return $respuesta;
	}

	function consultaComentarios($libraryid, $ajax){
		@session_start();
		if ($ajax == 0) {
			$ajax ='';
		}else{
			$ajax ='../';
		}
		include_once($ajax.'../config/database.php');
		include_once($ajax.'../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT llc.*, lu.first_name, lu.last_name, lu.user_id, lu.image, ld.dealer
				FROM ludus_library_comments llc, ludus_users lu, ludus_headquarters lh, ludus_dealers ld
				where lu.user_id = llc.user_id
				and lu.headquarter_id = lh.headquarter_id
				and lh.dealer_id = ld.dealer_id ";
				if ($ajax == '../') {
					$query_sql .="and library_id = $libraryid;";
				}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function eliminartaComentarios($libraryid, $ajax){
		@session_start();
		if ($ajax == 0) {
			$ajax ='';
		}else{
			$ajax ='../';
		}
		include_once($ajax.'../config/database.php');
		include_once($ajax.'../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "DELETE FROM ludus_library_comments WHERE library_comments_id='$libraryid';";
		// echo "$query_sql";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_Update($query_sql);
		unset($DataBase_Acciones);
		if ($Rows_config>0) {
			$Rows_config =["filasafectadas"=>"$Rows_config"];
		}else{
			$Rows_config =["filasafectadas"=>"error"];
		}
		return $Rows_config;
	}

	function consultainstantanea($consultainstantanea){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_libraries where name like '%$consultainstantanea%' or file like '%$consultainstantanea%';";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaUsuarios($buscar){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		$dealer_id = $_SESSION['dealer_id'];
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT u.user_id id, CONCAT(u.first_name, ' ', u.last_name) as text
		FROM ludus_users u, ludus_headquarters h, ludus_dealers d
        where u.headquarter_id = h.headquarter_id
        and h.dealer_id =  d.dealer_id
        and d.dealer_id = $dealer_id
        and u.first_name like '%$buscar%'
        and u.user_id <> $idQuien
		order by u.first_name
		asc limit 5;";
		// echo "$query_sql";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function compartidasConmigo(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$dealer_id = $_SESSION['dealer_id'];
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_library_shared where user_id_shared = $idQuien;";
		// echo "$query_sql";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$Rows_config = count($Rows_config);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function guardadas(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$dealer_id = $_SESSION['dealer_id'];
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_library_saved where user_id = $idQuien;";
		// echo "$query_sql";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$Rows_config = count($Rows_config);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function play_video( $library_id ){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "UPDATE ludus_library_views set play = 'Si' WHERE  library_id = $library_id and user_id = $idQuien;";
		$Rows_config = $DataBase_Acciones->SQL_Update($query_sql);
		$res = array();
		if( count($Rows_config) > 0 ){
			$res['error'] = false;
			$res['msj'] = 'play';
		}else{
			$res['error'] = true;
		}
		unset($DataBase_Acciones);
		return $res;
	}

	function finis_video( $library_id ){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "UPDATE ludus_library_views set finish = 'Si' WHERE  library_id = $library_id and user_id = $idQuien;";
		$Rows_config = $DataBase_Acciones->SQL_Update($query_sql);
		$res = array();
		if( count($Rows_config) > 0 ){
			$res['error'] = false;
			$res['msj'] = 'finish';
		}else{
			$res['error'] = true;
		}
		unset($DataBase_Acciones);
		return $res;
	}

	public function consultar_segment( $segment ){
		include_once( '../../config/init_db.php' );
		$query = "SELECT * FROM ludus_libraries where status_id = 1 and segment_id = $segment limit 16";
		$Rows = DB::query($query);
		return $Rows;
	}
}
