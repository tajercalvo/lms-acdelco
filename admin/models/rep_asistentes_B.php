<?php
class Asistentesbroad{
	

		public static function consultar_Broad(){
	        
			include_once('../../config/init_db.php');
			@session_start();
			// $rol_id    = $_SESSION['id_rol'][0]['rol_id'];
			// $dealer_id = $_SESSION['dealer_id'];
			 //$id_usuario = $_SESSION['charge_id'];
			// print_r($id_usuario);
			// return;
			//$query     = "SELECT conference_id, conference FROM ludus_conferences order by conference_id desc";
			$query ="SELECT schedule_id conference_id, schedule conference FROM ludus_schedule_av ORDER BY conference_id DESC";
			$resultado = DB::query($query);
			     
			   return $resultado;
		}

		public static function consultar_asistentes($conference_id){
			include_once('../../config/init_db.php');
		//	DB::$encoding = 'utf8';
			@session_start();
			//print_r($_SESSION);
			//return;
			$rol_id    = $_SESSION['id_rol'][0]['rol_id']; //rol super usuario
			$charge_id = $_SESSION['charge_id'];  //cargo
			$dealer_id = $_SESSION['dealer_id']; //empresa o concesionario
			//si es super usuario, reporta todas las empresas
		
			if ($rol_id == 1) {
				
				$query = "SELECT u.identification, u.last_name nombres, u.first_name apellidos, h.headquarter sede, d.dealer concesionario, GROUP_CONCAT( distinct c.charge separator ', ') trayectorias, r.rol perfil
							FROM ludus_users u 
							INNER JOIN ludus_headquarters h on h.headquarter_id = u.headquarter_id
							INNER JOIN ludus_dealers d on d.dealer_id = h.dealer_id
							INNER JOIN ludus_charges_users cu on u.user_id = cu.user_id
							INNER JOIN ludus_charges c on c.charge_id = cu.charge_id
							INNER JOIN ludus_roles_users ru on ru.user_id = u.user_id
							INNER JOIN ludus_roles r on r.rol_id = ru.rol_id
							INNER JOIN ludus_asistentes_av av on av.user_id = u.user_id 
							AND av.schedule_id_av = $conference_id GROUP BY u.user_id";
			    $resultado = DB::query($query);
			    //si es lider gm, crea un reporte de acuerdo a su empresa perteneciente
			}else{
				
				$resultado['mensaje']= 'Usted no tiene el perfil para hacer esta consulta';
			}
			
			return $resultado;
	     }

	     public static function mensaje_chat($conference_id){
	     	include_once('../../config/init_db.php');
	     	@session_start();
			$user_id = $_SESSION['idUsuario'];
			//print_r($_SESSION);
			//return;
	   
			$query = "SELECT c.comment_text Comentario, u.user_id_av, u.usuario Usuario, u.imagen, h.headquarter Empresa, c.schedule_id Conferencia,c.date_comment date_creation,

				CASE
					WHEN u.user_id = $user_id  THEN 'right'
					ELSE 'left' 
					END as chat,
				CASE
					WHEN u.user_id != $user_id  THEN 'right' 
					ELSE 'left'
					END as tiempo
					FROM ludus_asistentes_av u, ludus_comments_av c
		            LEFT JOIN ludus_users us on us.user_id = c.user_id
		            LEFT JOIN ludus_headquarters h on h.headquarter_id=us.headquarter_id
					WHERE u.user_id_av = c.user_id AND c.schedule_id = $conference_id and u.schedule_id_av = $conference_id order by c.comments_id asc";
			$result = DB::query($query);
			return $result;
	     }

}

// $consultar = new Asistentesbroad();
// $consultar -> consultabrod();
  	// $query="SELECT cr.conference_reply Comentario, CONCAT(u.first_name, ' ', u.last_name) Usuario,h.headquarter Empresa ,cr.date_creation,con.conference Conferencia 
			    //  	FROM ludus_conferences_reply cr 
			    //  	INNER JOIN ludus_users u on u.user_id = cr.user_id 
			    //  	INNER JOIN ludus_conferences con on con.conference_id = cr.conference_id 
			    //  	INNER JOIN ludus_headquarters h on h.headquarter_id=u.headquarter_id
			    //  	WHERE cr.conference_id = $conference_id";
?>