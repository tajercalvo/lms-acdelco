<?php
Class Notas {
	function consultaDatosNota($sWhere,$sOrder,$sLimit){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT mr.module_result_usr_id, mr.score, mr.score_review, mr.score_waybill, mr.user_id, mr.module_id, m.code, m.newcode, mr.approval, mr.date_creation, m.module, u.first_name, u.last_name, u.identification, u1.first_name as name_crea, u1.last_name as ape_crea
					FROM ludus_modules_results_usr mr, ludus_modules m, ludus_users u, ludus_users u1
					WHERE mr.module_id = m.module_id AND mr.user_id = u.user_id AND m.status_id = 1 AND u.status_id = 1 AND mr.creator = u1.user_id ";
			if($sWhere!=''){
				$query_sql .= " AND ( mr.score LIKE '%$sWhere%' OR mr.approval LIKE '%$sWhere%' OR mr.date_creation LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR m.module LIKE '%$sWhere%' OR m.newcode LIKE '%$sWhere%' OR m.code LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%') ";
			}
			if(isset($_SESSION['module_id_not_flr'])&&($_SESSION['module_id_not_flr']!="0")){
				$module_id = $_SESSION['module_id_not_flr'];
				$query_sql .= " AND mr.module_id = '$module_id' ";
			}
			if(isset($_SESSION['user_id_not_flr'])&&($_SESSION['user_id_not_flr']!="0")){
				$user_id = $_SESSION['user_id_not_flr'];
				$query_sql .= " AND u.user_id = '$user_id' ";
			}
			if(isset($_SESSION['score_not_flr'])&&($_SESSION['score_not_flr']!="")){
				$score_not = $_SESSION['score_not_flr'];
				$query_sql .= " AND mr.score = '$score_not' ";
			}
			if(isset($_SESSION['approval_not_flr'])&&($_SESSION['approval_not_flr']!="0")){
				$approval = $_SESSION['approval_not_flr'];
				$query_sql .= " AND mr.approval = '$approval' ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
			//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT mr.module_result_usr_id, mr.score, mr.score_review, mr.score_waybill, mr.user_id, mr.module_id, m.code, m.newcode, mr.approval, mr.date_creation, m.module, u.first_name, u.last_name, u.identification, u1.first_name as name_crea, u1.last_name as ape_crea
					FROM ludus_modules_results_usr mr, ludus_modules m, ludus_users u, ludus_users u1
					WHERE mr.module_id = m.module_id AND mr.user_id = u.user_id AND m.status_id = 1 AND u.status_id = 1 AND mr.creator = u1.user_id ";
			if($sWhere!=''){
				$query_sql .= " AND ( mr.score LIKE '%$sWhere%' OR mr.approval LIKE '%$sWhere%' OR mr.date_creation LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR m.module LIKE '%$sWhere%' OR m.newcode LIKE '%$sWhere%' OR m.code LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%') ";
			}
			if(isset($_SESSION['module_id_not_flr'])&&($_SESSION['module_id_not_flr']!="0")){
				$module_id = $_SESSION['module_id_not_flr'];
				$query_sql .= " AND mr.module_id = '$module_id' ";
			}
			if(isset($_SESSION['user_id_not_flr'])&&($_SESSION['user_id_not_flr']!="0")){
				$user_id = $_SESSION['user_id_not_flr'];
				$query_sql .= " AND u.user_id = '$user_id' ";
			}
			if(isset($_SESSION['score_not_flr'])&&($_SESSION['score_not_flr']!="")){
				$score_not = $_SESSION['score_not_flr'];
				$query_sql .= " AND mr.score = '$score_not' ";
			}
			if(isset($_SESSION['approval_not_flr'])&&($_SESSION['approval_not_flr']!="0")){
				$approval = $_SESSION['approval_not_flr'];
				$query_sql .= " AND mr.approval = '$approval' ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT mr.module_result_usr_id, mr.score, mr.score_review, mr.score_waybill, mr.user_id, mr.module_id, m.code, m.newcode, mr.approval, mr.date_creation, m.module, u.first_name, u.last_name, u.identification, u1.first_name as name_crea, u1.last_name as ape_crea
					FROM ludus_modules_results_usr mr, ludus_modules m, ludus_users u, ludus_users u1
					WHERE mr.module_id = m.module_id AND mr.user_id = u.user_id AND m.status_id = 1 AND u.status_id = 1 AND mr.creator = u1.user_id ";//LIMIT 0,20
		if(isset($_SESSION['module_id_not_flr'])&&($_SESSION['module_id_not_flr']!="0")){
			$module_id = $_SESSION['module_id_not_flr'];
			$query_sql .= " AND mr.module_id = '$module_id' ";
		}
		if(isset($_SESSION['user_id_not_flr'])&&($_SESSION['user_id_not_flr']!="0")){
			$user_id = $_SESSION['user_id_not_flr'];
			$query_sql .= " AND u.user_id = '$user_id' ";
		}
		if(isset($_SESSION['score_not_flr'])&&($_SESSION['score_not_flr']!="")){
			$score_not = $_SESSION['score_not_flr'];
			$query_sql .= " AND mr.score = '$score_not' ";
		}
		if(isset($_SESSION['approval_not_flr'])&&($_SESSION['approval_not_flr']!="0")){
			$approval = $_SESSION['approval_not_flr'];
			$query_sql .= " AND mr.approval = '$approval' ";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearNotas($score,$user_id,$module_id,$approval){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración cargos: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_modules_results_usr` (score,user_id,module_id,approval,date_creation,status_id,creator,invitation_id) VALUES ('$score','$user_id','$module_id','$approval','$NOW_data',1,'$idQuien',0)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarNotas($id,$score,$user_id,$module_id,$approval){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_notas.php?opcn=ver&id=$id&back=inicio>$id</a>] a cargos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_modules_results_usr` SET module_id = '$module_id', user_id = '$user_id', score = '$score', approval = '$approval',date_creation = '$NOW_data', creator = '$idQuien' WHERE module_result_usr_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_modules_results_usr c
			WHERE c.module_result_usr_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.module_id, c.module, c.code, u.newcode
						FROM ludus_modules c, ludus_courses u WHERE c.status_id = 1 AND c.course_id = u.course_id
						ORDER BY c.module";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaUsuarios($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.user_id, c.first_name, c.last_name, c.identification
						FROM ludus_users c WHERE c.status_id = 1
						ORDER BY c.first_name, c.last_name";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
}
