<?php
Class ReporteAsistencia {

	public function getCursos(){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM ludus_courses;";
		$Rows_config = DB::query($query);
		return $Rows_config;
	}

	public static function getReporte( $p ){
		extract($p);
		$course_id = $course_id == '' ? '' : " and sch.course_id = $course_id ";
		include_once('../config/init_db.php');
		$query = "SELECT inv.invitation_id, co.newcode, co.course, co.type, h.headquarter, a.area, z.zone, u.first_name, u.last_name, u.identification, sch.schedule_id, t.first_name nom_instructor, t.last_name ape_instructor, sch.inscriptions, sch.start_date, sch.end_date, li.living, inv.status_id, more.score, d.dealer, h.headquarter, inv.status_id
					FROM ludus_invitation inv
						INNER JOIN ludus_schedule sch
							on sch.schedule_id = inv.schedule_id
						INNER JOIN ludus_livings li
							on li.living_id = sch.living_id
						INNER JOIN ludus_modules mo
							on sch.module_id = mo.module_id
						INNER JOIN ludus_courses co
							on mo.course_id = co.course_id
						INNER JOIN ludus_users u
							on u.user_id = inv.user_id
						INNER JOIN ludus_headquarters h
							on h.headquarter_id = inv.headquarter_id
						INNER JOIN ludus_dealers d
							on h.dealer_id = d.dealer_id
						INNER JOIN ludus_areas a
							on h.area_id = a.area_id
						INNER JOIN ludus_zone z
							on a.zone_id = z.zone_id
						LEFT JOIN ludus_modules_results_usr more
							on more.invitation_id = inv.invitation_id
						INNER JOIN ludus_users t
							on t.user_id = sch.teacher_id
						where sch.start_date between '$start_date' and '$end_date'
								$course_id
						group by inv.invitation_id";
		$Rows_config = DB::query($query);
		return $Rows_config;
	}

        
}
