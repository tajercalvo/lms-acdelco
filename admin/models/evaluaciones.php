<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
Class Evaluaciones {
	function consultaDatosEvaluaciones($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (1,3) ";
			if($sWhere!=''){
				$query_sql .= " AND (c.review LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' ) ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (1,3) ";
			if($sWhere!=''){
				$query_sql .= " AND (c.review LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' ) ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (1,3)
			ORDER BY c.review ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearEvaluaciones($review,$code,$type,$description,$time,$cant_question){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$DataBase_Log->mysqli->set_charset("utf8mb4");
		/*$insertSQL_EE = "INSERT INTO erum_logacceso (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración Evaluaciones: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO ludus_reviews (review,status_id,editor,date_edition,code,type,description,time,cant_question) VALUES ('$review','1','$idQuien','$NOW_data','$code','$type','$description','$time','$cant_question')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarEvaluaciones($id,$review,$status,$code,$type,$description,$time,$cant_question){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		/*$insertSQL_EE = "INSERT INTO erum_logacceso (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_evaluacions.php?opcn=ver&id=$id&back=inicio>$id</a>] a Evaluaciones: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE ludus_reviews SET review = '$review', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data',code = '$code',type = '$type',description = '$description',time = '$time',cant_question='$cant_question' WHERE review_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function CrearRelacion($review_id,$Elementos,$tipo){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_reviews_".$tipo."s (review_id,".$tipo."_id) VALUES";
		$ctrl = 0;
		foreach ($Elementos as $iID => $elements_select) {
			if($ctrl==0){
				$insertSQL_EE .= " ('$review_id','$elements_select') ";
			}else{
				$insertSQL_EE .= " ,('$review_id','$elements_select') ";
			}
			$ctrl++;
		}
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function BorraEvaluacion_Rel($idRegistro){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO erum_logacceso (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_evaluacions.php?opcn=ver&id=$id&back=inicio>$id</a>] a Evaluaciones: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "DELETE FROM ludus_reviews_courses WHERE review_id = '$idRegistro'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		$updateSQL_ER = "DELETE FROM ludus_reviews_charges WHERE review_id = '$idRegistro'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_reviews c
			WHERE c.review_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		if($Rows_config['type']=="1"){
			$query_sql = "SELECT c.charge_id
						FROM ludus_reviews_charges c
						WHERE c.review_id = $idRegistro
						ORDER BY c.charge_id";
			$DataBase_Class = new Database();
			$Rows_config_Extr = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
			$Rows_config['cargos'] = $Rows_config_Extr;
		}else{
			$query_sql = "SELECT c.course_id
						FROM ludus_reviews_courses c
						WHERE c.review_id = $idRegistro
						ORDER BY c.course_id";
			$DataBase_Class = new Database();
			$Rows_config_Extr = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
			$Rows_config['cursos'] = $Rows_config_Extr;
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.course_id, c.course, c.newcode
						FROM ludus_courses c
						ORDER BY c.course";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaCargos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.charge_id, c.charge
						FROM ludus_charges c WHERE c.status_id = 1
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaPreguntasSel($idRegistro, $prof="../" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT rq.*, q.question, q.type, u.first_name, u.last_name, q.code
						FROM ludus_reviews_questions rq, ludus_questions q, ludus_users u
						WHERE rq.review_id = '$idRegistro' AND rq.question_id = q.question_id AND rq.editor = u.user_id
						ORDER BY rq.date_edition ASC";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		$query_CantRespuestas = "SELECT ra.review_id, ad.question_id, count(*)
						FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad
						WHERE ra.review_id = '$idRegistro' AND ad.reviews_answer_id = ra.reviews_answer_id
						GROUP BY ra.review_id, ad.question_id";
		$Rows_RespCant = $DataBase_Class->SQL_SelectMultipleRows($query_CantRespuestas);

		for( $i=0; $i<count($Rows_config); $i++ ) {
			$review_id = $Rows_config[$i]['review_id'];
			$question_id = $Rows_config[$i]['question_id'];
			$Rows_config[$i]['YaRespondida'] = "NO";
			$Rows_config[$i]['question'] = utf8_encode( $Rows_config[$i]['question'] );
			for($x=0;$x<count($Rows_RespCant);$x++) {
				if( $review_id == $Rows_RespCant[$x]['review_id'] && $question_id == $Rows_RespCant[$x]['question_id'] ){
					$Rows_config[$i]['YaRespondida'] = "SI";
				}
			}
		}

		unset($DataBase_Class);
		return $Rows_config;
	}

	function consultaPreguntas($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT p.question, p.question_id, p.type, p.code
						FROM ludus_questions p
						WHERE p.status_id = 1
						ORDER BY p.code";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function AgrPregunta($question_id,$value,$review_id, $agrup){
		@session_start();
		include('../config/database.php');
		include('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_reviews_questions (review_id,question_id,value,editor,date_edition,status_id,agrup) VALUES ('$review_id','$question_id','$value','$idQuien','$NOW_data','1','$agrup')";
		// echo( $insertSQL_EE );
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActPregunta($reviews_questions_id,$status_id){
		@session_start();
		include('../config/database.php');
		include('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_reviews_questions SET status_id = '$status_id' WHERE reviews_questions_id = '$reviews_questions_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		unset($DataBase_Log);
		return $resultado;
	}//fin funcion ActPregunta

	/*
		funcion para actualizar, e ingresar la nueva pregunta dentro de la Evaluaciones
		@ $questions_id:	es el id de la anterior pregunta
		@ $question_sel:	es el id de la nueva pregunta
		@ $value:					es el valor asignado para la calificacion de la pregunta
		@ $review_id:			es el id de la evaluacion correspondiente
	*/
	function ActPreguntaNueva($questions_id,$question_sel,$value,$review_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		//$idQuien = $_SESSION['idUsuario'];
		$CantPreg = 0;
		$resultado = 0;

		//SQL para consultar
		$query_sql = "SELECT ra.*
						FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad
						WHERE ra.review_id = $review_id AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = $questions_id ";
		$consultaPreguntas= $DataBase_Log->SQL_SelectMultipleRows($query_sql);

			for($x=0;$x<count($consultaPreguntas);$x++) {
				if( $review_id == $consultaPreguntas[$x]['review_id'] && $question_sel == $consultaPreguntas[$x]['question_id'] ){
					$CantPreg++;
				}
			}


		if($CantPreg==0){
			$updateSQL_ER = "UPDATE ludus_reviews_questions set question_id = '$question_sel', value= '$value'  where review_id = '$review_id' and question_id= '$questions_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		}
		unset($DataBase_Log);
		return $resultado;
	}//fin funcion ActPreguntaNueva

	//==========================================================================
	// Crea la agrupacion correspondiente de las preguntas para cada evaluacion
	//==========================================================================
	public static function crearAgrupacion( $p, $prof = "../" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Log = new Database();
		$DataBase_Log->mysqli->set_charset("utf8mb4");
		$agrup = trim( $p['agrup'] );
		$insertSQL_EE = "INSERT INTO ludus_reviews_dist( review_id, agrup, obligatorias, creator)
			VALUES ( {$p['review_id']},'$agrup',{$p['obligatorias']},{$p['user_id']})";
		$resultado = $DataBase_Log->SQL_Insert( $insertSQL_EE );
		unset($DataBase_Log);
		return $resultado;
	}//fin crearAgrupacion

	//==========================================================================
	// Consulta las agrupaciones relacionadas con un review_id
	//==========================================================================
	public static function consultaAgrupacion( $review_id, $prof = "../" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Log = new Database();
		$query_sql = "SELECT * FROM ludus_reviews_dist where review_id = $review_id";
		$resultado = $DataBase_Log->SQL_SelectMultipleRows( $query_sql );
		unset($DataBase_Log);
		return $resultado;
	}//fin consultaAgrupacion

	//==========================================================================
	// Consulta las agrupaciones relacionadas con un review_id
	//==========================================================================
	public static function editarAgrupacion( $p, $prof = "../" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Log = new Database();
		$agrup = trim( $p['agrup'] );
		$query_sql = "UPDATE ludus_reviews_dist SET agrup = '$agrup', obligatorias = {$p['obligatorias']} WHERE review_dist_id = {$p['review_dist_id']}";
		$resultado = $DataBase_Log->SQL_Update( $query_sql );
		unset($DataBase_Log);
		return $resultado;
	}//fin consultaAgrupacion

	//==========================================================================
	// Elimina una agrupacion
	//==========================================================================
	public static function eliminarAgrupacion( $review_dist_id, $prof = "../" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Log = new Database();
		$query_sql = "DELETE FROM ludus_reviews_dist WHERE review_dist_id = $review_dist_id";
		$resultado = $DataBase_Log->SQL_Update( $query_sql );
		unset($DataBase_Log);
		return $resultado;
	}//fin eliminarAgrupacion

	//==========================================================================
	// Modifica el estado de las preguntas
	//==========================================================================
	public static function estadoPregunta( $p, $prof = "../" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Log = new Database();
		$query_sql = "UPDATE ludus_reviews_questions SET status_id = {$p['status_id']} WHERE reviews_questions_id = {$p['reviews_questions_id']}";
		$resultado = $DataBase_Log->SQL_Update( $query_sql );
		unset($DataBase_Log);
		return $resultado;
	}//fin estadoPregunta

	//==========================================================================
	// valida si una pregunta ya se encuentra vinculada a una evaluación
	//==========================================================================
	public static function consultaPregunta( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		$query_sql = "SELECT * FROM ludus_reviews_questions WHERE question_id = %i AND review_id = %i ";
		$resultado = DB::queryFirstRow( $query_sql, $p['question_id'], $p['review_id'] );
		return $resultado;
	}//fin 	public static function consultaPregunta( $p, $prof = "../" ){


	//==========================================================================
	// Consulta los datos correspondientes a una pregunta
	//==========================================================================
	public static function getPregunta( $reviews_questions_id, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		$query_sql = "SELECT rq.*, q.question, q.type, u.first_name, u.last_name, q.code, 'NO' AS YaRespondida
						FROM ludus_reviews_questions rq, ludus_questions q, ludus_users u
						WHERE rq.question_id = q.question_id AND rq.editor = u.user_id AND rq.reviews_questions_id = %i";
		$resultado = DB::queryFirstRow( $query_sql, $reviews_questions_id );
		return $resultado;
	}//fin getPregunta

	//==========================================================================
	// Agrega una nueva pregunta para una evaluación
	//==========================================================================
	public static function agregarPregunta( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		DB::insert( 'ludus_reviews_questions', [
			'review_id' => $p['review_id'],
			'question_id' => $p['question_id'],
			'value' => $p['value'],
			'editor' => $p['user_id'],
			'date_edition' => DB::sqleval('NOW()'),
			'status_id' => 1,
			'agrup' => $p['agrup'],
			] );
		return DB::insertId();
	}//fin agregarPregunta

	//==========================================================================
	// Modifica una pregunta especifica
	//==========================================================================
	public static function editarPreguntaSel( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		DB::update( 'ludus_reviews_questions', [
			'question_id' => $p['question_id'],
			'value' => $p['value'],
			'editor' => $p['user_id'],
			'date_edition' => DB::sqleval('NOW()'),
			'agrup' => $p['agrup'],
		], 'reviews_questions_id = %i', $p['reviews_questions_id'] );
		return DB::affectedRows();
	}//fin editarPreguntaSel


}//fin clase Evaluaciones
