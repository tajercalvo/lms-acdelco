<?php
Class EncuestaGerentes {
	function ConsultaEncuesta(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT *
					FROM ludus_encuesta_ger WHERE user_id = '$idQuien' ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CreaEncuesta($AVN,$ACC,$CID,$ARA,$TIG,$TID){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_encuesta_ger` (user_id,AVN,ACC,CID,ARA,TIG,TID,date_creation) VALUES 
		('$idQuien','$AVN','$ACC','$CID','$ARA','$TIG','$TID','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function CreaDetalle($trayectoria,$blanda1,$blanda2,$blanda3,$tecnica1,$tecnica2,$tecnica3){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_encuesta_ger_det` (trayectoria,blanda1,blanda2,blanda3,tecnica1,tecnica2,tecnica3,user_id,date_creation) VALUES 
		('$trayectoria','$blanda1','$blanda2','$blanda3','$tecnica1','$tecnica2','$tecnica3','$idQuien','$NOW_data')";
		//echo($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}

	function consultaRespuestas($answer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT q.question_id, q.question, q.type_response
						FROM ludus_reviews_questions rq, ludus_questions q
						WHERE rq.review_id = 2 AND rq.question_id = q.question_id AND q.status_id = 1
						ORDER BY reviews_questions_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$query_answers = "SELECT a.* FROM ludus_answers a WHERE a.question_id IN (SELECT q.question_id
						FROM ludus_reviews_questions rq, ludus_questions q
						WHERE rq.review_id = 2 AND rq.question_id = q.question_id AND q.status_id = 1
						ORDER BY reviews_questions_id) ORDER BY a.question_id, a.answer_id";
		$Rows_Answers = $DataBase_Class->SQL_SelectMultipleRows($query_answers);
		for($i=0;$i<count($Rows_config);$i++) {
			$question_id = $Rows_config[$i]['question_id'];
				for($y=0;$y<count($Rows_Answers);$y++) {
					if( $question_id == $Rows_Answers[$y]['question_id'] ){
						$Rows_config[$i]['AnswersData'][] = $Rows_Answers[$y];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearRespuesta($reviews_answer_id,$question_id,$answer_id,$result,$tiempo){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_reviews_answer_detail` (reviews_answer_id,question_id,answer_id,result,date_creation) VALUES 
		('$reviews_answer_id','$question_id','$answer_id','$result','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		/*if($resultado>0){
			if($result=="SI"){
				$updateSQL_ER = "UPDATE `ludus_reviews_answer` SET score = score+1, time_response = '$tiempo' WHERE reviews_answer_id = '$reviews_answer_id'";
				$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);
			}else{
				$updateSQL_ER = "UPDATE `ludus_reviews_answer` SET time_response = '$tiempo' WHERE reviews_answer_id = '$reviews_answer_id'";
				$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);
			}
		}*/
		unset($DataBase_Log);
		return $resultado;
	}
	function CrearRespEncuesta($review_id,$available_score,$result_score){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$module_result_usr_id = $_SESSION['_EncSat_ResultId'];
		$insertSQL_EE = "INSERT INTO `ludus_reviews_answer` (review_id,user_id,available_score,score,date_creation,time_response,module_result_usr_id) VALUES 
		('$review_id','$idQuien','$available_score','$result_score','$NOW_data','00:00','$module_result_usr_id')";
		//echo($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		if($resultado>0){
			$consultaPuntajeObtenido = "SELECT score FROM ludus_modules_results_usr WHERE module_result_usr_id = '$module_result_usr_id' ";
			$Rows_config = $DataBase_Log->SQL_SelectRows($consultaPuntajeObtenido);
			$scoreAct = $Rows_config['score'];
			if($scoreAct>79){
				$updateSQL_ER = "UPDATE ludus_modules_results_usr SET score = score+".$result_score.", score_review = '$result_score' WHERE module_result_usr_id = '$module_result_usr_id' ";
				$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);
			}
			$updateAppAll = "UPDATE ludus_modules_results_usr SET approval = 'SI' WHERE score>79";
			$resultado_AP = $DataBase_Log->SQL_Update($updateAppAll);
		}
		unset($DataBase_Log);
		return $resultado;
	}
}
