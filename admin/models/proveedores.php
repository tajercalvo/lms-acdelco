<?php
Class Cargos {
	function consultaDatoscargos($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.supplier_id, c.supplier, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, c.phone, c.address, c.location
			FROM ludus_supplier c, ludus_users u, ludus_users s, ludus_status e
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.supplier LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.address LIKE '%$sWhere%' OR c.phone LIKE '%$sWhere%' OR c.location LIKE '%$sWhere%' ) ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.supplier_id, c.supplier, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, c.phone, c.address, c.location
			FROM ludus_supplier c, ludus_users u, ludus_users s, ludus_status e
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.supplier LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.address LIKE '%$sWhere%' OR c.phone LIKE '%$sWhere%' OR c.location LIKE '%$sWhere%' ) ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.supplier_id, c.supplier, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, c.phone, c.address, c.location
			FROM ludus_supplier c, ludus_users u, ludus_users s, ludus_status e
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id 
			ORDER BY supplier ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearcargos($nombre,$address,$phone,$location){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración cargos: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_supplier` (supplier,status_id,creator,editor,date_creation,date_edition,address,phone,location) VALUES ('$nombre','1','$idQuien','$idQuien','$NOW_data','$NOW_data','$address','$phone','$location')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarcargos($id,$nombre,$status,$address,$phone,$location){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_proveedores.php?opcn=ver&id=$id&back=inicio>$id</a>] a cargos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_supplier` SET supplier = '$nombre', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data',address = '$address',phone = '$phone',location = '$location' WHERE supplier_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_supplier c
			WHERE c.supplier_id = '$idRegistro' ";//LIMIT 0,20
			echo $query_sql;
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
