<?php
Class evaluarlosR {
	function consultaEvaluacion($idCurso){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT r.*, s.cantidad, c.course
					FROM ludus_reviews_r r, ludus_courses c, (SELECT review_id, count(*) as cantidad FROM `ludus_reviews_questions_r` WHERE status_id = 1 GROUP BY review_id) s
					WHERE r.course_id = c.course_id AND r.course_id = '$idCurso' AND r.review_id = s.review_id ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaRespuesta($answer_id){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_answers_r
					WHERE answer_id = '$answer_id' ";
		// echo( $query_sql."<br>" );
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaResultadoYa($reviews_answer_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT *
					FROM ludus_reviews_answer_r
					WHERE reviews_answer_id = '$reviews_answer_id' AND user_id = '$idQuien' ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaEvaluacionYa($idRegistro,$module_result_usr_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT r.*
					FROM ludus_reviews_answer_r r
					WHERE r.review_id = '$idRegistro' AND r.user_id = '$idQuien' AND module_result_usr_id = '$module_result_usr_id' ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearRespuesta($reviews_answer_id,$question_id,$answer_id,$result,$tiempo,$review_id,$module_result_usr_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_reviews_answer_detail_r` (reviews_answer_id,question_id,answer_id,result,date_creation) VALUES
		('$reviews_answer_id','$question_id','$answer_id','$result','$NOW_data')";
		// echo( "$insertSQL_EE<br>" );
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		if($resultado>0){
			if($result=="SI"){
				$Select_PuntajePregunta = "SELECT value FROM ludus_reviews_questions_r WHERE review_id = $review_id AND question_id = $question_id";
				$row_consulta = $DataBase_Log->SQL_SelectRows($Select_PuntajePregunta);
				$ValorRespuesta = $row_consulta['value'];

				$time = explode(":", $tiempo);
			    $suma = intval( $time[0] ) * 60 + intval( $time[1] );
				$updateSQL_ER = "UPDATE `ludus_reviews_answer_r` SET score = score+1, time_response = '$tiempo', time_seconds = $suma, module_result_usr_id = $module_result_usr_id WHERE reviews_answer_id = '$reviews_answer_id'";
				$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);

				if(isset($module_result_usr_id)){
					//echo("::".$_SESSION['_EvalCour_ResultId']."::");
					// $module_result_usr_id = $_SESSION['_EvalCour_ResultId'];
					$updateScore = "UPDATE ludus_modules_results_usr SET score=score+$ValorRespuesta, score_waybill=score_waybill+$ValorRespuesta WHERE module_result_usr_id = '$module_result_usr_id' ";
					$resultado_US = $DataBase_Log->SQL_Update($updateScore);
					$updateAppAll = "UPDATE ludus_modules_results_usr SET approval = 'SI' WHERE score>79";
					$resultado_AP = $DataBase_Log->SQL_Update($updateAppAll);
				}
			}else{
				$time = explode(":", $tiempo);
			    $suma = intval( $time[0] ) * 60 + intval( $time[1] );
				$updateSQL_ER = "UPDATE `ludus_reviews_answer_r` SET time_response, time_seconds = '$tiempo', time_seconds = $suma WHERE reviews_answer_id = '$reviews_answer_id'";
				$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);
			}
		}
		unset($DataBase_Log);
		return $resultado;
	}
	function CrearRespEvaluacion($review_id,$available_score){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		if(isset($_SESSION['_EvalCour_ResultId'])){
			$module_result_usr_id = $_SESSION['_EvalCour_ResultId'];
		}else{
			$module_result_usr_id = 0;
		}
		$DataBase_Log = new Database();
		$data = [];
		$insertSQL_EE = "INSERT INTO `ludus_reviews_answer_r` (review_id,user_id,available_score,score,date_creation,time_response, time_seconds,module_result_usr_id) VALUES
		('$review_id','$idQuien','$available_score',0,'$NOW_data','00:00', 0,'$module_result_usr_id')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		// $data['reviews_answer_id'] = $resultado;

		$QueryConsultaPreguntas = "SELECT rq.agrup, rq.question_id, rq.value, q.question, q.source
						FROM ludus_reviews_questions_r rq, ludus_questions_r q
						WHERE rq.review_id = '$review_id' AND rq.status_id = '1' AND
						rq.question_id = q.question_id
						ORDER BY rand() LIMIT 0,5" ;
		$Rows_listos = $DataBase_Log->SQL_SelectMultipleRows($QueryConsultaPreguntas);
		foreach ($Rows_listos as $key => $value) {
			$Rows_config[] = $value;
		}
		// $Rows_config = $DataBase_Log->SQL_SelectMultipleRows($QueryConsultaPreguntas);

		$query_answers = "SELECT a.*
							FROM ludus_reviews_questions_r rq, ludus_questions_r q, ludus_answers_r a
							WHERE rq.review_id = '$review_id' AND rq.status_id = '1' AND
							rq.question_id = q.question_id AND q.question_id = a.question_id
							ORDER BY q.question_id, rand()";
		$Rows_Answers = $DataBase_Log->SQL_SelectMultipleRows($query_answers);

		for($i=0;$i<count($Rows_config);$i++) {
			$question_id = $Rows_config[$i]['question_id'];
			//Cargos
				for($y=0;$y<count($Rows_Answers);$y++) {
					if( $question_id == $Rows_Answers[$y]['question_id'] ){
						$Rows_config[$i]['AnswersData'][] = $Rows_Answers[$y];
					}
				}
			//Cargos
		}
		$_SESSION['EvaluacionCompleta'] = $Rows_config;
		//print_r($Rows_config);
		unset($DataBase_Log);
		return $resultado;
	}
	function RecordEvaluacion($review_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$idQuien = $_SESSION['idUsuario'];
		$query_res = "SELECT a.reviews_answer_id, ad.result, q.question, q.code, ans.answer, ans.result as res_ans, rq.value as valor
					FROM ludus_reviews_answer_r a, ludus_reviews_answer_detail_r ad, ludus_questions_r q, ludus_answers_r ans, ludus_reviews_questions_r rq
					WHERE a.review_id = '$review_id' AND a.user_id = '$idQuien' AND a.reviews_answer_id = ad.reviews_answer_id
					AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id AND rq.review_id = '$review_id' AND ad.question_id = rq.question_id ";
		$Rows_res = $DataBase_Log->SQL_SelectMultipleRows($query_res);
		unset($DataBase_Log);
		return $Rows_res;
	}
	public static function refuerzo(){
		@session_start();
		include_once('../config/init_db.php');
		$idQuien = $_SESSION['idUsuario'];
		$fecha = date_create(date("Y-m-d"));
		$fecha = date_add($fecha, date_interval_create_from_date_string('-10 days'));
		$fecha = date_format($fecha, 'Y-m-d');
		$usuario = $_SESSION['max_rol'] < 5 ? " AND mu.user_id = $idQuien " : "";
		$query_res = "SELECT mu.*, u.first_name, u.last_name, u.identification, c.course_id, c.course, c.type, s.start_date
			FROM ludus_modules_results_usr mu, ludus_users u, ludus_modules m, ludus_courses c, ludus_invitation i, ludus_schedule s
			WHERE mu.user_id = u.user_id
			AND mu.invitation_id = i.invitation_id
			AND i.schedule_id = s.schedule_id
			AND mu.module_id = m.module_id
			AND m.course_id = c.course_id
			AND mu.date_creation >= '$fecha 00:00:00'
			AND mu.score_waybill = 0
			AND mu.score_evaluation >= 80
			AND mu.user_id = mu.editor_ev
			$usuario";
		// echo($query_res);
		// die();
		$Rows_res = DB::query($query_res);

		return $Rows_res;
	}
}
