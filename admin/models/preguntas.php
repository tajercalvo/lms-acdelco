<?php
Class Preguntas {
	function consultaDatosPreguntas($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_questions c, ludus_users s, ludus_status e
			WHERE c.editor = s.user_id AND c.status_id = e.status_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.question LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.type LIKE '%$sWhere%') ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_questions c, ludus_users s, ludus_status e
			WHERE c.editor = s.user_id AND c.status_id = e.status_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.question LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.type LIKE '%$sWhere%') ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_questions c, ludus_users s, ludus_status e
			WHERE c.editor = s.user_id AND c.status_id = e.status_id
			ORDER BY c.type ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearPreguntas($question,$code,$type,$type_response,$answer1,$answer2,$answer3,$answer4,$answer5,$answer6,$answer7,$answer8,$answer9,$answer10,$RespSel,$imagen,$Resp_new_name1,$Resp_new_name2,$Resp_new_name3,$Resp_new_name4,$Resp_new_name5,$Resp_new_name6,$Resp_new_name7,$Resp_new_name8,$Resp_new_name9,$Resp_new_name10){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración Preguntas: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_questions` (question,code,type,type_response,editor,date_edition,status_id,source) VALUES ('$question','$code','$type','$type_response','$idQuien','$NOW_data','1','$imagen')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		if($type_response=="1"){
			/*Almacena las respuestas*/
				if($answer1!=""){
					if($RespSel =="1"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer1','$Resp_new_name1','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer1','$Resp_new_name1','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer2!=""){
					if($RespSel =="2"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer2','$Resp_new_name2','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer2','$Resp_new_name2','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer3!=""){
					if($RespSel =="3"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer3','$Resp_new_name3','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer3','$Resp_new_name3','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer4!=""){
					if($RespSel =="4"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer4','$Resp_new_name4','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer4','$Resp_new_name4','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer5!=""){
					if($RespSel =="5"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer5','$Resp_new_name5','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer5','$Resp_new_name5','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer6!=""){
					if($RespSel =="6"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer6','$Resp_new_name6','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer6','$Resp_new_name6','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer7!=""){
					if($RespSel =="7"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer7','$Resp_new_name7','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer7','$Resp_new_name7','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer8!=""){
					if($RespSel =="8"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer8','$Resp_new_name8','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer8','$Resp_new_name8','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer9!=""){
					if($RespSel =="9"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer9','$Resp_new_name9','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer9','$Resp_new_name9','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer10!=""){
					if($RespSel =="10"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer10','$Resp_new_name10','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$resultado','$answer10','$Resp_new_name10','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
			/*Almacena las respuestas*/
		}
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarPreguntas($id,$status,$question,$code,$type,$type_response,$answer1,$answer2,$answer3,$answer4,$answer5,$answer6,$answer7,$answer8,$answer9,$answer10,$RespSel,$imagen,$Resp_new_name1,$Resp_new_name2,$Resp_new_name3,$Resp_new_name4,$Resp_new_name5,$Resp_new_name6,$Resp_new_name7,$Resp_new_name8,$Resp_new_name9,$Resp_new_name10){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_preguntas.php?opcn=ver&id=$id&back=inicio>$id</a>] a Preguntas: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_questions` SET question = '$question', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data',code = '$code',type = '$type',type_response = '$type_response',source='$imagen' WHERE question_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		/*Elimina las respuestas anteriores*/
		$updateSQL_ER = "DELETE FROM ludus_answers WHERE question_id = '$id'";
		$resultado_D = $DataBase_Log->SQL_Update($updateSQL_ER);
		/*Elimina las respuestas anteriores*/
		if($type_response=="1"){
			/*Almacena las respuestas*/
				if($answer1!=""){
					if($RespSel =="1"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer1','$Resp_new_name1','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer1','$Resp_new_name1','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer2!=""){
					if($RespSel =="2"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer2','$Resp_new_name2','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer2','$Resp_new_name2','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer3!=""){
					if($RespSel =="3"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer3','$Resp_new_name3','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer3','$Resp_new_name3','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer4!=""){
					if($RespSel =="4"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer4','$Resp_new_name4','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer4','$Resp_new_name4','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer5!=""){
					if($RespSel =="5"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer5','$Resp_new_name5','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer5','$Resp_new_name5','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer6!=""){
					if($RespSel =="6"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer6','$Resp_new_name6','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer6','$Resp_new_name6','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer7!=""){
					if($RespSel =="7"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer7','$Resp_new_name7','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer7','$Resp_new_name7','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer8!=""){
					if($RespSel =="8"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer8','$Resp_new_name8','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer8','$Resp_new_name8','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer9!=""){
					if($RespSel =="9"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer9','$Resp_new_name9','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer9','$Resp_new_name9','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
				if($answer10!=""){
					if($RespSel =="10"){
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer10','$Resp_new_name10','SI')";
					}else{
						$insertSQL_EER = "INSERT INTO `ludus_answers` (question_id,answer,source,result) VALUES ('$id','$answer10','$Resp_new_name10','NO')";
					}
					$resultado_A = $DataBase_Log->SQL_Insert($insertSQL_EER);
				}
			/*Almacena las respuestas*/
		}
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_questions c
			WHERE c.question_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaRespuestas($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_answers c
			WHERE c.question_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	public static function respuestasPregunta( $question_id, $prof="../" ) {
		include_once($prof.'../config/init_db.php');
		$query_sql = "SELECT COUNT( rad.reviews_answer_detail_id ) AS numero FROM ludus_reviews_answer_detail rad WHERE rad.question_id = $question_id";
		$resultSet = DB::queryFirstRow( $query_sql );
		return $resultSet['numero'];
	}

}
