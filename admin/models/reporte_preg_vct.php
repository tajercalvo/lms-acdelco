<?php
Class ReportePreguntas {

	public function getSesiones(){
		include_once('../config/init_db.php');
		$query = "SELECT s.schedule_id, s.start_date, s.module_id, u.first_name, u.last_name,m.module
						from ludus_schedule s
								inner join ludus_users u
									on u.user_id = s.teacher_id
								inner join ludus_modules m
									on m.module_id = s.module_id";
		$Rows_config = DB::query($query);
		return $Rows_config;
	}

	public static function getReporte( $p ){
		extract($p);
		@session_start();
		include_once('../config/init_db.php');
		
		$query = "SELECT u.first_name, u.last_name, cf.name pregunta, answer, u.user_id, u.identification
					FROM ludus_respuestas_usuarios_cursos ru
						inner join ludus_invitation a
							on ru.user_id = a.user_id
							and a.schedule_id = ru.schedule_id
						inner join ludus_courses_files cf
							on cf.question_id = ru.question_id
						inner join ludus_respuestas_preguntas_cursos pc
							on pc.id = ru.answer_id
							left join ludus_users u
							on u.user_id = a.user_id
						where ru.schedule_id = $schedule_id
						order by pregunta;";
		$res = DB::query($query);
		return $res;
	}
        
}
