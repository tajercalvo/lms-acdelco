<?php

class Analytics{

    function getConcesionarios( $zonas , $prof = "" ){
        include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
        $zona = $zonas == "" ? "" : "AND z.zone_id IN ( $zonas ) ";
        $query_sql = "SELECT DISTINCT d.dealer_id, d.dealer
            FROM ludus_dealers d, ludus_headquarters h, ludus_areas a, ludus_zone z
            WHERE d.dealer_id = h.dealer_id
            AND d.status_id = 1
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            $zona ";
        $DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
        unset( $DataBase_Acciones );
        return $Rows_config;
    }//fin funcion getConcesionarios

    function getZonas($prof = "" ){
        include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
        $query_sql = "SELECT z.zone_id, z.zone FROM ludus_zone z WHERE z.status_id = 1";
        $DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
        unset( $DataBase_Acciones );
        return $Rows_config;
    }//fin funcion getZonas

    function getCargos($prof = "" ){
        include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
        $query_sql = "SELECT c.charge_id, c.charge FROM ludus_charges c WHERE c.status_id = 1";
        $DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
        unset( $DataBase_Acciones );
        return $Rows_config;
    }//fin funcion getCargos

    function getTipoUsuarios($prof = "" ){
        include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
        $query_sql = "SELECT t.type_user_id, t.type_user FROM ludus_type_user t";
        $DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
        unset( $DataBase_Acciones );
        return $Rows_config;
    }//fin funcion getCargos

    function getSedes( $concesionarios ,$prof = "" ){
        include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
        $query_sql = "SELECT h.headquarter_id, h.headquarter FROM ludus_headquarters h
            WHERE h.status_id = 1
            AND h.dealer_id IN ( $concesionarios ) ";
        $DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
        unset( $DataBase_Acciones );
        return $Rows_config;
    }//fin funcion getCargos

    function consultaUsuariosCcs( $p ){
		include_once($p['prof'].'../config/database.php');
		include_once($p['prof'].'../config/config.php');
        $filtros = "";
        // filtro de zonas/concesionarios/sedes
        if( $p['sedes'] != "" ){
            $filtros .= " AND h.headquarter_id IN ( {$p['sedes']} ) ";
        }elseif( $p['concesionarios'] != "" ){
            $filtros .= " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }elseif( $p['zonas'] != "" ){
            $filtros .= " AND z.zone_id IN ( {$p['zonas']} ) ";
        }
        //filtro de cargos
        if( $p['cargos'] != "" ){
            $filtros .= " AND c.charge_id IN ( {$p['cargos']} ) ";
        }
        //filtro de estado de usuarios
        if( $p['es_activo'] == "SI" && $p['es_inactivo'] == "NO" ){
            $filtros .= " AND u.status_id = 1 ";
        }elseif( $p['es_activo'] == "NO" && $p['es_inactivo'] == "SI" ){
            $filtros .= " AND u.status_id = 2 ";
        }
        //filtro de genero de usuarios
        if( $p['masculino'] == "SI" && $p['femenino'] == "NO" ){
            $filtros .= " AND u.gender = 1 ";
        }elseif( $p['femenino'] == "SI" && $p['masculino'] == "NO" ){
            $filtros .= " AND u.gender = 2 ";
        }
        //filtro de tipo de usuario
        if( $p['tipo_usuario'] > 0 ){
            $filtros .= " AND u.type_user_id = {$p['tipo_usuario']} ";
        }
        //filtro de tipo de usuario
        if( $p['contratacion'] != 'todos' ){
            $filtros .= " AND u.linking = '{$p['contratacion']}' ";
        }

		$query_sql = "SELECT z.zone, d.dealer, h.headquarter, n.section, count(n.navigation_id) as cantidadVisitas, sum(n.time_navigation) as tiempoTotal, avg(n.time_navigation) as tiempoPromedio, count(distinct n.user_id) as cantidadUsuarios
			FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges c, ludus_charges_users cu
			WHERE n.user_id = u.user_id
			AND u.user_id = cu.user_id
			AND cu.charge_id = c.charge_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
            $filtros
			AND n.date_navigation BETWEEN '{$p['start_date_day']} 00:00:00' AND '{$p['end_date_day']} 23:59:59'
            GROUP BY z.zone, d.dealer, h.headquarter, n.section";
            // echo( $query_sql );
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$cuenta = count( $Rows_config );

		// print_r( $Rows_config );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin consultaUsuariosCcs

}

?>
