<?php
Class RepCalificacion {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDatosAll($start_date,$end_date){
		include_once('../config/init_db.php');
		//@session_start();
		//$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT s.start_date, s.end_date, s.min_size, s.max_size, l.living, l.address, c.newcode, c.course, p.supplier, u.identification, u.first_name, u.last_name, m.identification, m.first_name, m.last_name,m.fecCalif, DATEDIFF(m.fecCalif,s.start_date) as DiasCalif, MONTH(s.start_date) AS montCourse, MONTH(m.fecCalif) as montCalif
			FROM ludus_schedule s, ludus_livings l, ludus_courses c, ludus_supplier p, ludus_users u,
			(
				SELECT i.schedule_id, u.identification, u.first_name, u.last_name, MAX(i.date_result) as fecCalif
			    FROM ludus_invitation i, ludus_users u
			    WHERE i.user_id = u.user_id
			    GROUP BY i.schedule_id, u.identification, u.first_name, u.last_name
			) m
			WHERE s.living_id = l.living_id
			AND s.course_id = c.course_id
			AND c.supplier_id = p.supplier_id
			AND s.teacher_id = u.user_id
			AND s.schedule_id = m.schedule_id
			AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
			ORDER BY s.start_date";

		// echo $query_sql;
		// die();

		$Rows_config = DB::query( $query_sql );
		return $Rows_config;
	}
}
