<?php

class ResultadoGestion {

    public static function getCupos( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');

        $query_cupos = "SELECT d.dealer_id, d.dealer, YEAR(s.start_date) AS anio, MONTH(s.start_date) AS mes, d.category, SUM( ds.max_size ) AS cupos
            FROM ludus_dealer_schedule ds, ludus_schedule s, ludus_dealers d
            WHERE ds.schedule_id = s.schedule_id
            AND ds.dealer_id = d.dealer_id
            AND s.status_id = 1
            AND d.category = 'CHEVROLET'
            AND d.dealer_id > 1
            AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
            GROUP BY d.dealer";

        $cupos = DB::query( $query_cupos );

        return $cupos;
    }// fin getCupos

    public static function getExcusas( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');

        $query_excusas = "SELECT h.dealer_id, e.schedule_id, e.status_id
            FROM ludus_excuses e, ludus_schedule s, ludus_users u, ludus_headquarters h
            WHERE e.schedule_id = s.schedule_id
            AND e.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND s.status_id = 1
            AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
            AND e.status_id = 1";

        $excusas = DB::query( $query_excusas );

        return $excusas;
    }// fin getExcusas

    public static function getExcusasCupos( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');

        $query_excusas = "SELECT ed.*
            FROM ludus_excuses_dealers ed, ludus_schedule s
            WHERE ed.schedule_id = s.schedule_id
            AND ed.status_id = 1
            AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'";

        $excusas = DB::query( $query_excusas );

        return $excusas;
    }// fin getExcusas

    public static function getUsuarios( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $query_users = "SELECT h.dealer_id, i.*, s.start_date
            FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s
            WHERE i.user_id = u.user_id
            AND i.headquarter_id = h.headquarter_id
            AND i.schedule_id = s.schedule_id
            AND s.status_id = 1
            AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'";

        $users = DB::query( $query_users );

        return $users;
    }// fin getUsuarios


    // ========================================================
    // Muestra el concesionario del usuario segun la fecha del curso
    // ========================================================
    public static function ccsHistoricoUsuario( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');

        $query_sql = "SELECT i.*, h.headquarter, d.dealer_id, d.dealer
            FROM ludus_headquarter_history i, ludus_headquarters h, ludus_dealers d, (
                SELECT MAX(date_creation) as date_creation, user_id FROM ludus_headquarter_history
                WHERE date_creation < '{$p['start_date']}' AND user_id = {$p['user_id']} GROUP BY user_id
            ) as hy
            WHERE i.date_creation = hy.date_creation
            AND i.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND i.user_id = hy.user_id";
        // echo $query_sql;
        $resultSet = DB::queryFirstRow( $query_sql );

        return $resultSet;
    }// fin ccsHistoricoUsuario


}
