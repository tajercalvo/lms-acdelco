<?php 

class foro{
     
		public static function consultar_perfiles(){
		 	include_once('../../config/init_db.php');

		 	$query = "SELECT rol_id perfil_id, rol perfil FROM ludus_roles WHERE status_id = 1 ORDER BY rol_id DESC";
		 	$result = DB::query($query);

		 	return $result;

		}

		public static function consultar_trayectorias(){
		 	include_once('../../config/init_db.php');

		    $query="SELECT charge_id, charge FROM ludus_charges WHERE status_id = 1 ORDER BY charge_id DESC";
		    $result = DB::query($query);
		    
		   
		   foreach ($result as $key => $value) {
		   		
		   		if ( $value['charge_id'] == 1) {
		   			unset ($result[$key]);

		   		}if ($value['charge_id'] == 5) {
		   			unset ($result[$key]);
		   		}

		     }

          $result = array_values($result);

		   // print_r($result);
		 	return $result;
		}


		public static function crear_hilo($p){
			//extract($p);
			include_once('../../config/init_db.php');
			session_start();
 			$user_id = $_SESSION['idUsuario'];
 			// echo "<pre>";
 			// print_r(count($p['id_perfiles']));
 			// 	return;

 				$p['id_perfiles'] = explode(',', $p['id_perfiles']);
 				$p['id_trayectorias'] = explode(',', $p['id_trayectorias']);


		$query = "INSERT INTO ludus_foro
                      (foro_name, 
                      creador_id, 
                      date_creation 
                      )
                      VALUES
                      (
                      '{$p['titulo']}',
                      $user_id,
                       NOW()
                      )";
                      $resultSet = DB::query( $query );

                     if ($resultSet) {
                       	
                       	$id_foro = DB::insertId();
					
							for ($i=0; $i < count($p['id_perfiles'])  ; $i++) { 

									
								   DB::query("INSERT INTO ludus_foro_perfil 
								            					  (
								            					  foro_id,
								            					   perfil_id
								            					  )
								            					  VALUES(
								            					  $id_foro,
								            					  '{$p['id_perfiles'][$i]}'
								            					   )");
							}

							for ($i=0; $i < count($p['id_trayectorias']); $i++) { 


								 DB::query("INSERT INTO ludus_foro_trayectoria 
            					  (
            					   foro_id,
            					   trayectoria_id
            					  )
            					  VALUES
            					  (
            					   $id_foro,
            					   '{$p['id_trayectorias'][$i]}'
            					   )");
							}

                     }
                  return $resultSet;
           

		}

		public function consultarCargos(){
			include_once( '../../config/init_db.php' );
			@session_start();
			$usuario_id = $_SESSION['idUsuario'];

			$query = "SELECT charge_id FROM ludus_charges_users WHERE user_id = $usuario_id";
			$Rows = DB::query($query);
			return $Rows;
		}


		public static function consultar_hilo($trayectoria){
			  include_once('../../config/init_db.php');
			  @session_start();
			  $num_rol;
			  $num_chargues;
			  
			  	foreach ($_SESSION['id_rol'] as $key => $value) {
				 	$num_rol[] = $value['rol_id'];
			  	
				}
				foreach ($trayectoria as $key2 => $value2) {
					# cargos...
					foreach ($value2 as $key => $value) {
						$num_chargues[]=$value;
					}
					
				}

				$id_roles = implode(",",$num_rol);//perfiles
				$id_chargues = implode(",",$num_chargues);//cargos
				//print_r($id_chargues);
	 			//die();

			    $query="SELECT lf.*, fp.perfil_id, ft.trayectoria_id, lu.first_name, lu.last_name, lu.image FROM ludus_foro lf
								INNER JOIN ludus_foro_perfil fp on fp.foro_id = lf.foro_id
								INNER JOIN ludus_foro_trayectoria ft on ft.foro_id = lf.foro_id
								LEFT JOIN ludus_users lu on lu.user_id = lf.creador_id
								WHERE fp.perfil_id IN (".$id_roles.") AND
								ft.trayectoria_id IN (".$id_chargues.")
								GROUP BY lf.foro_id ORDER BY foro_id DESC ";

			 	 $result = DB::query($query);
				 return $result;
		}

		//--------------- Chat del foro  ---------------------

		public static function consultar_chatforo($foro_id){
		
			include_once('../../config/init_db.php');

			// print_r(trim($foro_id));
			// return;
			
			$query = "SELECT fc.*, concat(lu.first_name,' ',lu.last_name) name_user, lf.foro_name, lu.image 
						FROM ludus_foro_chat fc
							INNER JOIN ludus_users lu ON lu.user_id = fc.user_id
									INNER JOIN ludus_foro lf ON lf.foro_id = fc.foro_id
											WHERE fc.foro_id = $foro_id";
			 $result = DB::query($query );
			 
		  	//print_r($result);	
			 return $result;


		}

		public static function enviar_message($p){
			extract($p);
			include_once('../../config/init_db.php');
			@session_start();
			$idUsuario=$_SESSION['idUsuario'];
			// print_r($message);
			// die();
		
			  $query = "INSERT INTO ludus_foro_chat 
								    (
								        foro_id,
										mensaje,
										user_id,
										date_cration
								        )
								         VALUES(
								        $id_foro,
								        '$message',
								        $idUsuario,
								        NOW()
								     )";
			    $result = DB::query($query);
			    
			    $id_forochat = DB::insertId();
			    
			    if ($result) {
			    		$id_forochat = DB::insertId();

			    		 $query2 = "SELECT fc.*, concat(lu.first_name,' ',lu.last_name) name_user, lf.foro_name, lu.image, lf.foro_name  
									FROM ludus_foro_chat fc
										INNER JOIN ludus_users lu ON lu.user_id = fc.user_id
												INNER JOIN ludus_foro lf ON lf.foro_id = fc.foro_id
														WHERE fc.forochat_id = $id_forochat";
			 			
			 
			    }

			    $result2 = DB::query($query2);
			    return $result2;
			 
		}

		public static function consultar_mensajes($foro_id){
			include_once('../../config/init_db.php');

			$query = "SELECT fc.*, concat(lu.first_name,' ',lu.last_name) name_user, lf.foro_name, lu.image, lf.foro_name  
									FROM ludus_foro_chat fc
										INNER JOIN ludus_users lu ON lu.user_id = fc.user_id
												INNER JOIN ludus_foro lf ON lf.foro_id = fc.foro_id
												 WHERE fc.foro_id = $foro_id
                                                ORDER BY forochat_id DESC LIMIT 1";

			$result = DB::query($query);
			return $result;
		}

		public static function consult_hilo_forid($foro_id){
			include_once('../../config/init_db.php');

			$query = "SELECT lf.*, fp.perfil_id, ft.trayectoria_id, lu.first_name, lu.last_name, lu.image FROM ludus_foro lf
								INNER JOIN ludus_foro_perfil fp on fp.foro_id = lf.foro_id
								INNER JOIN ludus_foro_trayectoria ft on ft.foro_id = lf.foro_id
								LEFT JOIN ludus_users lu on lu.user_id = lf.creador_id
                                WHERE lf.foro_id = $foro_id
                                ";
                $result = DB::query($query);
                return $result;
		}

		public static function editar_Hilo($p){
			extract($p);
			include_once('../../config/init_db.php');
			@session_start();
			$user_id=$_SESSION['idUsuario'];

			    $p['id_perfiles'] = explode(',', $perfiledit);
 				$p['id_trayectorias'] = explode(',', $trayectoriaedit);

 				$query = "UPDATE ludus_foro 
						SET foro_name = '$tituloedit', editor= $user_id, date_creation =  NOW()
						WHERE foro_id = $foro_id";
                  $resultSet = DB::query( $query );

 			  $Dele_perfil = "DELETE FROM ludus_foro_perfil WHERE foro_id = $foro_id";
 			  				$resultSet = DB::query( $Dele_perfil );

 			  $Dele_trayectoria = "DELETE FROM ludus_foro_trayectoria WHERE foro_id =  $foro_id";
 			 			 $resultSet = DB::query( $Dele_trayectoria );

			 
                     if ($resultSet) {
					
							for ($i=0; $i < count($p['id_perfiles'])  ; $i++) { 

									
								    DB::query("INSERT INTO ludus_foro_perfil 
								            					  (
								            					  foro_id,
								            					   perfil_id
								            					  )
								            					  VALUES(
								            					  '$foro_id',
								            					  '{$p['id_perfiles'][$i]}'
								            					   );");
							}

							for ($i=0; $i < count($p['id_trayectorias']); $i++) { 


								 DB::query("INSERT INTO ludus_foro_trayectoria 
            					  (
            					   foro_id,
            					   trayectoria_id
            					  )
            					  VALUES
            					  (
            					   '$foro_id',
            					   '{$p['id_trayectorias'][$i]}'
            					   );");
							}

                     }						
					
						
                  return $resultSet;

		}

		// public static function consultar_Usuarios(){
		// 	include_once('../../config/init_db.php');

		// 	$query = "SELECT COUNT( DISTINCT(user_id)) user_id FROM ludus_foro_chat";
		// 	$result = DB::query($query);
		// 	return $result;
		// }

}//Fin class

// $obj= new foro();
// $result = $obj->enviar_message();
// print_r($result);