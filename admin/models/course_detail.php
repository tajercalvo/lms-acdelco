<?php
Class Cursos {
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_courses c
			WHERE c.course_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaRegistroDetail($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*
			FROM ludus_courses c
			WHERE c.course_id = '$idRegistro'";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaProveedores($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.supplier_id, c.supplier
						FROM ludus_supplier c
						WHERE c.status_id = 1
						ORDER BY c.supplier";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaRegistroDetailModulos($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*, c.newcode as codigo_curso
		FROM ludus_modules m, ludus_courses c
		WHERE m.course_id = c.course_id
		AND m.course_id = '$idRegistro'
		AND m.status_id = 1";//LIMIT 0,20
		//echo($query_sql);
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function ConsultaArchivos($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*
			FROM ludus_course_files m
			WHERE m.course_id = '$idRegistro' AND m.status_id = 1 ORDER BY date_creation DESC";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaAsignaturas($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.subject_id, c.subject
						FROM ludus_subject c
						WHERE c.status_id = 1
						ORDER BY c.subject";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function ConsultaInscripcion($idCourse){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT inscription_id FROM ludus_inscriptions WHERE user_id = '$idQuien' AND course_id = '$idCourse' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		if($Rows_config>0){
			return true;
		}else{
			return false;
		}
	}
	function InscribirseCurso($idCourse){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$insertSQL_EE = "INSERT INTO ludus_inscriptions (user_id,course_id,status_id,date_creation) VALUES ('$idQuien','$idCourse',1,NOW()) ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$resultado = $DataBase_Class->SQL_Insert($insertSQL_EE);
		unset($DataBase_Acciones);
		return $resultado;
	}
	function actualizaFile($course_id,$name,$new_name1){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$insertSQL_EE = "INSERT INTO ludus_course_files (course_id,name,file,type,user_id,date_creation,status_id) VALUES ( '$course_id','$name','$new_name1','pdf','$idQuien',NOW(),1 ) ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$resultado = $DataBase_Class->SQL_Insert($insertSQL_EE);
		unset($DataBase_Acciones);
		return $resultado;
	}
	function BorrarArchivo($nomArchivo){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_course_files`
						SET status_id = '0',
							user_id = '$idQuien',
							date_creation = NOW()
						WHERE file = '$nomArchivo' ";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaPasoCurso($idCourse,$idUsuario){
		@session_start();
		if($idUsuario!=0){
			$idQuien = $idUsuario;
		}else{
			if(!isset($_SESSION['idUsuario'])){
				$idQuien = 0;
			}else{
				$idQuien = $_SESSION['idUsuario'];
			}
		}
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT r.*
					FROM ludus_modules_results_usr r, ludus_modules m
					WHERE m.course_id = '$idCourse' AND m.module_id = r.module_id AND
					r.score > 79 AND r.user_id = $idQuien";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function ConsultaScos($idCourse){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT m.module_id, m.course_id, m.code, m.module, s.md5hash, s.scorm_id, ss.scorm_sco_id, ss.parent, ss.identifier, ss.launch, ss.title, m.newcode
					FROM ludus_modules m, ludus_scorm s, ludus_scorm_sco ss
					WHERE m.course_id = '$idCourse' AND m.module_id = s.module_id AND s.scorm_id = ss.scorm_id AND ss.scormtype = 'sco'
					ORDER BY ss.sortorder ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		$query_attempt = "SELECT s.scorm_sco_id, max(s.attempt) as cantidad
						FROM ludus_scorm_track s
						WHERE s.user_id = '$idQuien'
						GROUP BY s.scorm_sco_id";
		$Rows_Attempts = $DataBase_Class->SQL_SelectMultipleRows($query_attempt);

		$query_notes = "SELECT s.scorm_sco_id, max( CAST( value AS UNSIGNED ) ) as maxNote
						FROM ludus_scorm_track s
						WHERE s.user_id = '$idQuien' AND element = 'cmicorescoreraw'
						GROUP BY s.scorm_sco_id";
		$Rows_NOTES = $DataBase_Class->SQL_SelectMultipleRows($query_notes);

		$query_status = "SELECT s.scorm_sco_id, element, value
						FROM ludus_scorm_track s
						WHERE s.user_id = '$idQuien' AND element = 'cmicorelessonstatus'
						ORDER BY timemodified DESC ";
		$Rows_STATUS = $DataBase_Class->SQL_SelectMultipleRows($query_status);

		for($i=0;$i<count($Rows_config);$i++) {
			$scorm_sco_id = $Rows_config[$i]['scorm_sco_id'];
				for($y=0;$y<count($Rows_Attempts);$y++) {
					if( $scorm_sco_id == $Rows_Attempts[$y]['scorm_sco_id'] ){
						$Rows_config[$i]['cantidadAttempts'] = $Rows_Attempts[$y]['cantidad'];
					}
				}
				for($y=0;$y<count($Rows_NOTES);$y++) {
					if( $scorm_sco_id == $Rows_NOTES[$y]['scorm_sco_id'] ){
						$Rows_config[$i]['maxNota'] = $Rows_NOTES[$y]['maxNote'];
					}
				}
				for($y=0;$y<count($Rows_STATUS);$y++) {
					if( $scorm_sco_id == $Rows_STATUS[$y]['scorm_sco_id'] ){
						if(!isset($Rows_config[$i]['Status'])){
							$Rows_config[$i]['Status'] = $Rows_STATUS[$y]['value'];
						}else{
							if($Rows_config[$i]['Status']!='completed' && $Rows_config[$i]['Status']!='passed'){
								$Rows_config[$i]['Status'] = $Rows_STATUS[$y]['value'];
							}
						}
					}
				}
			if(!isset($Rows_config[$i]['cantidadAttempts'])){
				$Rows_config[$i]['cantidadAttempts'] = '0';
			}
			if(!isset($Rows_config[$i]['maxNota'])){
				$Rows_config[$i]['maxNota'] = '0';
			}
			if(!isset($Rows_config[$i]['Status'])){
				$Rows_config[$i]['Status'] = 'Sin Iniciar';
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	/*
	Andres Vega
	06/12/2016
	Funcion que trae 2 preguntas de seguridad para realizar la inscripcion de algun curso virtual
	*/
	public function consultaPreguntasSeguridad($id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT sa.*, sq.security_question
			FROM ludus_security_answer sa, ludus_security_question sq
			WHERE sa.security_question_id = sq.security_question_id
			AND sa.user_id = $id
			ORDER BY RAND()
			LIMIT 0,2";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaPreguntasSeguridad
	/*
	Andres Vega
	06/12/2016
	Funcion que consulta el registro de la respuesta para el id especifico
	$id = security_answer_id que identifica en la tabla
	*/
	public function consultaRespuestaSeguridad($id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT sa.*
			FROM ludus_security_answer sa
			WHERE sa.security_answer_id = $id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaRespuestaSeguridad

}//fin clase
