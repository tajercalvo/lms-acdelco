<?php
Class encuestarlos {
	function consultaEncuesta($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT r.*, s.cantidad
					FROM ludus_reviews r, (SELECT review_id, count(*) as cantidad FROM `ludus_reviews_questions` WHERE status_id = 1 GROUP BY review_id) s
					WHERE r.review_id = '$idRegistro' AND r.review_id = s.review_id ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaRespuestas($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT q.question_id, q.question, q.type_response
						FROM ludus_reviews_questions rq, ludus_questions q
						WHERE rq.review_id = '$idRegistro' AND rq.question_id = q.question_id AND q.status_id = 1
						ORDER BY reviews_questions_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$query_answers = "SELECT a.* FROM ludus_answers a WHERE a.question_id IN (SELECT q.question_id
						FROM ludus_reviews_questions rq, ludus_questions q
						WHERE rq.review_id = '$idRegistro' AND rq.question_id = q.question_id AND q.status_id = 1
						ORDER BY reviews_questions_id) ORDER BY a.question_id, a.answer_id";
		$Rows_Answers = $DataBase_Class->SQL_SelectMultipleRows($query_answers);
		for($i=0;$i<count($Rows_config);$i++) {
			$question_id = $Rows_config[$i]['question_id'];
				for($y=0;$y<count($Rows_Answers);$y++) {
					if( $question_id == $Rows_Answers[$y]['question_id'] ){
						$Rows_config[$i]['AnswersData'][] = $Rows_Answers[$y];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearRespuesta($reviews_answer_id,$question_id,$answer_id,$result,$tiempo){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_reviews_answer_detail` (reviews_answer_id,question_id,answer_id,result,date_creation) VALUES
		('$reviews_answer_id','$question_id','$answer_id','$result','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function CrearRespEncuesta($review_id,$available_score,$result_score){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$module_result_usr_id = $_SESSION['_EncSat_ResultId'];
		$insertSQL_EE = "INSERT INTO `ludus_reviews_answer` (review_id,user_id,available_score,score,date_creation,time_response,module_result_usr_id) VALUES
		('$review_id','$idQuien','$available_score','$result_score','$NOW_data','00:00','$module_result_usr_id')";
		//echo($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		if($resultado>0){
			$consultaPuntajeObtenido = "SELECT score FROM ludus_modules_results_usr WHERE module_result_usr_id = '$module_result_usr_id' ";
			$Rows_config = $DataBase_Log->SQL_SelectRows($consultaPuntajeObtenido);
			$scoreAct = $Rows_config['score'];
			if($scoreAct>79){
				//$updateSQL_ER = "UPDATE ludus_modules_results_usr SET score = score+".$result_score.", score_review = '$result_score' WHERE module_result_usr_id = '$module_result_usr_id' ";
				//$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);
			}
			//$updateAppAll = "UPDATE ludus_modules_results_usr SET approval = 'SI' WHERE score>79";
			//$resultado_AP = $DataBase_Log->SQL_Update($updateAppAll);
		}
		unset($DataBase_Log);
		return $resultado;
	}
}
