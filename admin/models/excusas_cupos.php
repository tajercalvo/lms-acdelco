<?php
Class Excusas {

	public static function consultaDatosNota( $sWhere, $sOrder, $sLimit, $fecha, $anio, $dealer ){
		@session_start();
		include_once('../../config/init_db.php');
		if( $anio == "0" ){
			$anio = "";
		}
		$filtro = "";
		$f_mes = "";
		$f_dealer = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = " AND d.dealer_id = ".$_SESSION['dealer_id'];
		}
		if( $fecha > 0 ){
			$f_mes = " AND MONTH(de.date_creation) = $fecha ";
		}
		if( $dealer > 0 ){
			$f_dealer = " AND d.dealer_id = $dealer";
		}
		$query_sql = "SELECT s.start_date, d.dealer, c.code, c.newcode, c.course, t.first_name as first_creator, t.last_name as last_creator, de.excuse_dealer_id, de.file, de.date_creation, de.status_id
			FROM ludus_excuses_dealers de, ludus_schedule s, ludus_courses c, ludus_users t, ludus_dealers d
			WHERE de.schedule_id = s.schedule_id
			AND s.course_id = c.course_id
			AND de.creator_id = t.user_id
			AND de.dealer_id = d.dealer_id
			$f_dealer
			AND de.date_creation BETWEEN '$anio-01-01 00:00:00' AND '$anio-12-31 23:59:59'
			$f_mes
			$filtro";

			if($sWhere!=''){
				$query_sql .= " AND ( OR t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR t.identification LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR s.start_date LIKE '%$sWhere%') ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		// echo $query_sql;
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public static function consultaCantidadFull( $sWhere, $sOrder, $sLimit, $fecha, $anio, $dealer ){
		@session_start();
		include_once('../../config/init_db.php');
		if( $anio == "0" ){
			$anio = "";
		}
		$filtro = "";
		$f_mes = "";
		$f_dealer = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = " AND d.dealer_id = ".$_SESSION['dealer_id'];
		}
		if( $fecha > 0 ){
			$f_mes = " AND MONTH(de.date_creation) = $fecha ";
		}
		if( $dealer > 0 ){
			$f_dealer = " AND d.dealer_id = $dealer";
		}
		$query_sql = "SELECT s.start_date, d.dealer, c.code, c.newcode, c.course, t.first_name as first_creator, t.last_name as last_creator, de.excuse_dealer_id, de.file, de.date_creation, de.status_id
			FROM ludus_excuses_dealers de, ludus_schedule s, ludus_courses c, ludus_users t, ludus_dealers d
			WHERE de.schedule_id = s.schedule_id
			AND s.course_id = c.course_id
			AND de.creator_id = t.user_id
			AND de.dealer_id = d.dealer_id
			$f_dealer
			AND de.date_creation BETWEEN '$anio-01-01 00:00:00' AND '$anio-12-31 23:59:59'
			$f_mes
			$filtro";

			if($sWhere!=''){
				$query_sql .= " AND ( OR t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR t.identification LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR s.start_date LIKE '%$sWhere%') ";
			}
			// echo( $query_sql );
		$cuenta = DB::query($query_sql);
		$Rows_config = count($cuenta);
		return $Rows_config;
	}

	public static function consultaCantidad( $prof = '../' ){
		@session_start();
		include_once($prof.'../config/init_db.php');
		$anio = date('Y');
		$filtro = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = "WHERE d.dealer_id = ".$_SESSION['dealer_id'];
		}
		$query_sql = "SELECT * FROM ludus_excuses_dealers ".$filtro;
		// echo( $query_sql );
		$Rows_config = DB::query($query_sql);
		$cuenta = count($Rows_config);
		return $cuenta;
	}

	public static function CrearExcusa( $p, $prof = "../" ){
		include_once( $prof.'../config/init_db.php');

		$aplica = isset($p['aplica_para']) ? $p['aplica_para'] : "";

		DB::insert( 'ludus_excuses_dealers', [
			"dealer_id" 		=> $p['dealer_id'],
			"schedule_id" 		=> $p['schedule_id'],
			"num_invitation" 	=> $p['num_invitation'],
			"type" 				=> $p['type'],
			"aplica_para" 		=> $aplica,
			"file" 				=> $p['file'],
			"description" 		=> $p['description'],
			"date_creation" 	=> DB::sqleval( 'NOW()' ),
			"creator_id" 		=> $p['creator_id'],
			"status_id" 		=> $p['status_id']
		] );

		$resultado = DB::insertId();
		return $resultado;
	}
	//=====================================================================
	//Retorna un listado de 15 sesiones de un curso
	//=====================================================================
	public static function consultaSchedule( $p , $prof = "../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$filtro = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = "AND ds.dealer_id = {$_SESSION['dealer_id']}";
		}
		if( $p['searchTerm'] != "" ){
			$filtro_texto = $p['searchTerm'];
			$texto = "AND ( s.start_date LIKE '%$filtro_texto%' OR c.course LIKE '%$filtro_texto%' OR c.newcode LIKE '%$filtro_texto%' ) ";
		}
		$query_sql = "SELECT DISTINCT s.schedule_id, s.start_date, c.course, c.newcode
				FROM ludus_dealer_schedule ds, ludus_schedule s, ludus_courses c
				WHERE ds.schedule_id = s.schedule_id
				AND s.course_id = c.course_id
				AND s.status_id = 1
				AND ds.max_size > 0
				$filtro
				$texto
				LIMIT 20";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}//fin consultaUsuarios
	//=====================================================================
	//Retorna el usuario al cual ya se le genero una excusa
	//=====================================================================
	public static function consultaUsuarioExcusa($user_id, $prof = "../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$query_sql = "SELECT c.user_id, c.first_name, c.last_name, c.identification
						FROM ludus_users c
						WHERE c.user_id = $user_id";
		$Rows_config = DB::queryFirstRow($query_sql);
		return $Rows_config;
	}// fin consultaUsuarioExcusa
	//=====================================================================
	//Retorna los datos de la programacion para un usuario
	//=====================================================================
	public static function GetProgramacion($user_id, $prof="../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$dia = date('d');
		$mes = intval(date('m'));
		$anio = date('Y');
		$filtro = "";
		$mes_ant = 0;
		
		// if( intval($dia) <= 5 || $_SESSION['max_rol'] >= 5 ){
		// 	$mes_ant = $mes == 1 ? 12 : $mes - 1;
		// }

		$filtro = "AND MONTH( s.start_date ) IN ( $mes_ant, $mes ) ";

		$query_sql = "SELECT i.invitation_id, s.schedule_id, s.start_date, c.course, c.newcode, c.code
						FROM ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE i.user_id = $user_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND i.invitation_id NOT IN ( SELECT e.invitation_id FROM ludus_excuses e WHERE e.status_id = 1 AND e.user_id = $user_id )
						AND YEAR( s.start_date ) = $anio
						ORDER BY s.start_date DESC";

		$query_mru = DB::query("SELECT mru.module_result_usr_id, mru.invitation_id FROM ludus_modules_results_usr mru WHERE mru.user_id = $user_id");

		// echo( $query_sql );
		$Rows_config = DB::query($query_sql);
		$cuenta = count( $Rows_config );
		for ($i=0; $i < $cuenta ; $i++) {
			$Rows_config[$i]['module_result_usr_id'] = 0;
			foreach ($query_mru as $key => $value) {
				if( $Rows_config[$i]['invitation_id'] == $value['invitation_id'] ){
					$Rows_config[$i]['module_result_usr_id'] = $value['module_result_usr_id'];
				}
			}
		}

		return $Rows_config;
	}// fin GetProgramacion
	//=====================================================================
	//Retorna la excusa seleccionada
	//=====================================================================
	public static function getExcusa( $excuse_dealer_id, $prof = "" ){
		include_once($prof.'../config/init_db.php');
		$query = "SELECT e.*, s.start_date, c.newcode, c.course
			FROM ludus_excuses_dealers e, ludus_schedule s, ludus_courses c
			WHERE e.schedule_id = s.schedule_id
			AND s.course_id = c.course_id
			AND e.excuse_dealer_id = $excuse_dealer_id";
		$excusa = DB::queryFirstRow( $query );
		return $excusa;
	}//fin getExcusa
	//=====================================================================
	//Actualiza la excusa con la informacion enviada
	//=====================================================================
	public static function actualizarExcusa( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$description = "";
		$idQuien = $_SESSION['idUsuario'];

		if( isset( $p['excusa_text_r'] ) && $p['excusa_text_r'] != "" ){
			$description = $p['description']."\n".$_SESSION['NameUsuario'].": ".$p['excusa_text_r'];
		}else{
			$description = $p['description'];
		}

		$aplica = isset($p['aplica_para']) ? $p['aplica_para'] : "";

		$datos = [
			"date_edition" 	=> DB::sqleval('NOW()'),
			"status_id" 	=> $p['status_id'],
			"dealer_id" 	=> $p['dealer_id'],
			"num_invitation"=> $p['num_invitation'],
			"schedule_id" 	=> $p['schedule_id'],
			"aplica_para" 	=> $aplica,
			"type" 			=> $p['type'],
			"description" 	=> $description,
			"editor_id" 	=> $idQuien
		];
		if( isset($p['file']) && $p['file'] != "" ){
			$datos['file'] = $p['file'];
		}

		$datos['aplica_para'] = isset($p['aplica_para']) ? $p['aplica_para'] : "";

		// print_r( $datos );
		DB::update('ludus_excuses_dealers', $datos, "excuse_dealer_id = %i", $p['excuse_dealer_id'] );
		$excusa = DB::affectedRows();
		return $excusa;
	}//fin getExcusa
	//=====================================================================
	//Consulta el listado de cursos según filtro
	//=====================================================================
	public static function getCursos( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		$query_sql = "SELECT c.course_id, c.course, c.newcode  FROM ludus_courses c WHERE c.course LIKE '%{$p['searchTerm']}%' OR c.newcode = '%{$p['searchTerm']}%' LIMIT 0, 10 ";
		$aux = DB::query( $query_sql );
		$cursos = [];

		foreach ($aux as $key => $a) {
			$cursos[] = [ "id" => $a['course_id'], "text" => $a['course']." | ".$a['newcode'] ];
		}

		return $cursos;

	} //fin getCursos
	//=====================================================================
	//Consulta el listado de cursos según filtro
	//=====================================================================
	public static function getSesiones( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		$query_sql = "SELECT ds.dealer_schedule_id, d.dealer, s.schedule_id, s.start_date, c.course
			FROM ludus_dealers d, ludus_dealer_schedule ds, ludus_schedule s, ludus_courses c
			WHERE ds.schedule_id = s.schedule_id
			AND s.course_id = c.course_id
			AND ds.dealer_id = d.dealer_id
			AND s.course_id = {$p['course_id']}";
		$aux = DB::query( $query_sql );

		return $aux;

	} //fin getCursos
	//=====================================================================
	//Consulta el listado de cursos según filtro
	//=====================================================================
	public static function getScheduleDealers( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$filtro = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = " AND ds.dealer_id = ".$_SESSION['dealer_id'];
		}
		$query_sql = "SELECT d.dealer_id, d.dealer FROM ludus_dealers d, ludus_dealer_schedule ds
			WHERE d.dealer_id = ds.dealer_id
			AND ds.schedule_id = {$p['schedule_id']}
			AND ds.max_size > 0
			$filtro";
		$resultSet = DB::query( $query_sql );

		return $resultSet;

	} //fin getScheduleDealers

}
