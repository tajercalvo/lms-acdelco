<?php
Class GestionHojas {
	function consultaDatoshojas($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT u.first_name, u.last_name, u.identification, t.first_name as first_prof, t.last_name as last_prof, c.code, c.newcode, c.course, h.waybill_id, h.date_creation, h.score, h.status_id
						FROM ludus_waybills h, ludus_users u, ludus_users t, ludus_modules_results_usr r, ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE h.user_id = u.user_id
						AND h.teacher_id = t.user_id
						AND h.module_result_usr_id = r.module_result_usr_id
						AND r.invitation_id = i.invitation_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND r.score > 79
						AND h.status_id = 3
						";
			if($sWhere!=''){
				$query_sql .= " AND (t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR h.date_creation LIKE '%$sWhere%' OR h.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR h.score LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' )";
			}
			$idQuien = $_SESSION['idUsuario'];
			if( isset($_SESSION['max_rol']) && ($_SESSION['max_rol']>=3 && $_SESSION['max_rol']<=5) ){
				$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id= $dealer_id) ";
			}
			if( isset($_SESSION['max_rol']) && ($_SESSION['max_rol']>=1 && $_SESSION['max_rol']<3) ){
				$query_sql .= " AND u.user_id =$idQuien	";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT u.first_name, u.last_name, u.identification, t.first_name as first_prof, t.last_name as last_prof, c.code, c.newcode, c.course, h.waybill_id, h.date_creation, h.score, h.status_id
						FROM ludus_waybills h, ludus_users u, ludus_users t, ludus_modules_results_usr r, ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE h.user_id = u.user_id
						AND h.teacher_id = t.user_id
						AND h.module_result_usr_id = r.module_result_usr_id
						AND r.invitation_id = i.invitation_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND r.score > 79
						AND h.status_id = 3
						";
		if($sWhere!=''){
			$query_sql .= " AND (t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR h.date_creation LIKE '%$sWhere%' OR h.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR h.score LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' )";
		}
		$idQuien = $_SESSION['idUsuario'];
		if( isset($_SESSION['max_rol']) && ($_SESSION['max_rol']>=3 && $_SESSION['max_rol']<=5) ){
			$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id= $dealer_id) ";
		}
		if( isset($_SESSION['max_rol']) && ($_SESSION['max_rol']>=1 && $_SESSION['max_rol']<3) ){
			$query_sql .= " AND u.user_id =$idQuien	";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT u.first_name, u.last_name, u.identification, t.first_name as first_prof, t.last_name as last_prof, c.code, c.newcode, c.course, h.waybill_id, h.date_creation, h.score, h.status_id
						FROM ludus_waybills h, ludus_users u, ludus_users t, ludus_modules_results_usr r, ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE h.user_id = u.user_id
						AND h.teacher_id = t.user_id
						AND h.module_result_usr_id = r.module_result_usr_id
						AND r.invitation_id = i.invitation_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND r.score > 79
						AND h.status_id = 3
						";
			$idQuien = $_SESSION['idUsuario'];
			if( isset($_SESSION['max_rol']) && ($_SESSION['max_rol']>=3 && $_SESSION['max_rol']<=5) ){
				$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id= $dealer_id) ";
			}
			if( isset($_SESSION['max_rol']) && ($_SESSION['max_rol']>=1 && $_SESSION['max_rol']<3) ){
				$query_sql .= " AND u.user_id =$idQuien	";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}


	function Actualizarhojas($id,$description,$target,$action,$fec_fulfillment){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_waybills`
		SET description = '$description', status_id = '2', date_edition = '$NOW_data',target = '$target' , action = '$action', fec_fulfillment = '$fec_fulfillment'
		WHERE waybill_id = '$id'";
		//echo($updateSQL_ER);
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}//fin funcion Actualizarhojas

	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, DATEDIFF(c.date_edition,c.date_creation) as difDias, u.first_name, u.last_name, u.image, s.first_name as first_prof, s.last_name as last_prof, s.image as img_prof
			FROM ludus_waybills c, ludus_users u, ludus_users s
			WHERE c.waybill_id = '$idRegistro' AND c.user_id = u.user_id AND c.teacher_id = s.user_id";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaRegistro
	/*
	Andres Vega
	11/08/2016
	funcion para consultar los FeedBack hechos en una hoja de ruta especifica
	*/
	function consultaFeedback($hoja){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql= "SELECT wf.*, u.first_name, u.last_name, s.first_name as first_w, s.last_name as last_w, u.image, s.image as img_prof
		FROM ludus_users u, ludus_waybills_feedback wf, ludus_waybills w, ludus_users s
		WHERE u.user_id = wf.user_id AND wf.waybill_id = $hoja AND wf.waybill_id = w.waybill_id AND w.user_id = s.user_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaFeedback
	/*
	Andres Vega
	11/08/2016
	funcion que almacena los nuevos Feedback realizados por el coordinador
	*/
	function insertarFeedback($idHoja,$feedback){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$query_sql= "INSERT INTO ludus_waybills_feedback (waybill_id,feedback,feedback_date,user_id)
		VALUES($idHoja,'$feedback','$NOW_data',{$_SESSION['idUsuario']})";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_Insert($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion insertarFeedback
	/*
	Andres Vega
	11/08/2016
	funcion que almacena los nuevos Feedback realizados por el coordinador
	*/
	function insertarRespuestaFeedback($feedback,$idFeedback){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$query_sql= "UPDATE ludus_waybills_feedback
		SET feedback_r = '$feedback', feedback_rdate = '$NOW_data'
		WHERE waybills_feedback_id = $idFeedback";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_Update($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion insertarRespuestaFeedback
}
