<?php

Class RepAsesoresClass {
	function consultaModelosArgumentario( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$DataBase_Acciones = new Database();
		$query_sql = "SELECT a.argument_id, a.argument, a.image, a.likes, c.argument_cat, c.argument_cat_id
						FROM ludus_argument a, ludus_argument_cat c
						WHERE a.status_id = 1
						AND a.argument_cat_id = c.argument_cat_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_CantVisitas = "SELECT DISTINCT n.element_id, count(n.user_id) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('argumentarios.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
			// echo( $query_CantVisitas );
		$Rows_CantVisitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantVisitas);

		$query_TiempoTotal = "SELECT DISTINCT n.element_id, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('argumentarios.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
			// echo( $query_TiempoTotal );
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.element_id, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('argumentarios.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
			// echo( $query_TiempoPromedio );
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.element_id, count(distinct n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('argumentarios.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
			// echo( $query_CantidadUsr );
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$element_id = $Rows_config[$i]['argument_id'];
				for($y=0;$y<count($Rows_CantVisitas);$y++) {
					if( $element_id == $Rows_CantVisitas[$y]['element_id'] ){
						$Rows_config[$i]['cantidadVisitas'] = $Rows_CantVisitas[$y]['cantidadVisitas'];
					}
				}
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $element_id == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $element_id == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $element_id == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaUsuarios( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";
		$DataBase_Acciones = new Database();

		$query_sql = "SELECT DISTINCT n.section, count(n.user_id) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.section IN ( {$p['secciones']} )
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.section";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_TiempoTotal = "SELECT DISTINCT n.section, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.section IN ({$p['secciones']})
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.section";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.section, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.section IN ({$p['secciones']})
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.section";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.section, count(DISTINCT n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.section IN ({$p['secciones']})
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.section";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$section = $Rows_config[$i]['section'];
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $section == $Rows_TiempoTotal[$y]['section'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $section == $Rows_TiempoPromedio[$y]['section'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $section == $Rows_CantidadUsr[$y]['section'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaNovedades( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$query_sql = "SELECT n.* FROM ludus_news n WHERE n.type = 2 ";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_CantVisitas = "SELECT DISTINCT n.element_id, count(n.user_id) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('novedades.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantVisitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantVisitas);

		$query_TiempoTotal = "SELECT DISTINCT n.element_id, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('novedades.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.element_id, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('novedades.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.element_id, count(DISTINCT n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('novedades.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$element_id = $Rows_config[$i]['news_id'];
				for($y=0;$y<count($Rows_CantVisitas);$y++) {
					if( $element_id == $Rows_CantVisitas[$y]['element_id'] ){
						$Rows_config[$i]['cantidadVisitas'] = $Rows_CantVisitas[$y]['cantidadVisitas'];
					}
				}
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $element_id == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $element_id == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $element_id == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaNoticias( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$query_sql = "SELECT n.* FROM ludus_news n WHERE n.type = 1 ";

		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$query_CantVisitas = "SELECT DISTINCT n.element_id, count( n.user_id ) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('noticias.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantVisitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantVisitas);

		$query_TiempoTotal = "SELECT DISTINCT n.element_id, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('noticias.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' GROUP BY n.element_id";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.element_id, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('noticias.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' GROUP BY n.element_id";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.element_id, count(distinct n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('noticias.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' GROUP BY n.element_id";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$element_id = $Rows_config[$i]['news_id'];
				for($y=0;$y<count($Rows_CantVisitas);$y++) {
					if( $element_id == $Rows_CantVisitas[$y]['element_id'] ){
						$Rows_config[$i]['cantidadVisitas'] = $Rows_CantVisitas[$y]['cantidadVisitas'];
					}
				}
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $element_id == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $element_id == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $element_id == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaForos( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT n.* FROM ludus_forums n ";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$query_CantVisitas = "SELECT DISTINCT n.element_id, count( n.user_id ) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
		WHERE n.user_id = u.user_id AND  n.element_id > 1 AND n.section IN ('foros_tutor.php')
		AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantVisitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantVisitas);

		$query_TiempoTotal = "SELECT DISTINCT n.element_id, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
		WHERE n.user_id = u.user_id AND  n.element_id > 1 AND n.section IN ('foros_tutor.php')
		AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.element_id, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
		WHERE n.user_id = u.user_id AND  n.element_id > 1 AND n.section IN ('foros_tutor.php')
		AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.element_id, count(distinct n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
		WHERE n.user_id = u.user_id AND  n.element_id > 1 AND n.section IN ('foros_tutor.php')
		AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);


		$query_CantidadLikes = "SELECT r.forum_id, count(*) Likes
						FROM ludus_forums_reply_likes l, ludus_forums_reply r
						WHERE r.forum_reply_id = l.forum_reply_id AND l.date_edition BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
						GROUP BY r.forum_id";
		$Rows_CantidadLikes = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadLikes);


		for($i=0;$i<count($Rows_config);$i++) {
			$element_id = $Rows_config[$i]['forum_id'];
				for($y=0;$y<count($Rows_CantVisitas);$y++) {
					if( $element_id == $Rows_CantVisitas[$y]['element_id'] ){
						$Rows_config[$i]['cantidadVisitas'] = $Rows_CantVisitas[$y]['cantidadVisitas'];
					}
				}
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $element_id == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $element_id == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $element_id == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
				for($y=0;$y<count($Rows_CantidadLikes);$y++) {
					if( $element_id == $Rows_CantidadLikes[$y]['forum_id'] ){
						$Rows_config[$i]['likes'] = $Rows_CantidadLikes[$y]['Likes'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaEventos( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT n.* FROM ludus_events n ORDER BY date_event";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$query_CantVisitas = "SELECT DISTINCT n.element_id, count(n.user_id) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('eventos.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantVisitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantVisitas);

		$query_TiempoTotal = "SELECT DISTINCT n.element_id, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('eventos.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.element_id, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('eventos.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.element_id, count(distinct n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('eventos.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$element_id = $Rows_config[$i]['event_id'];
				for($y=0;$y<count($Rows_CantVisitas);$y++) {
					if( $element_id == $Rows_CantVisitas[$y]['element_id'] ){
						$Rows_config[$i]['cantidadVisitas'] = $Rows_CantVisitas[$y]['cantidadVisitas'];
					}
				}
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $element_id == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $element_id == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $element_id == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaGaleriasF( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT n.* FROM ludus_media n WHERE type = 1";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$query_CantVisitas = "SELECT DISTINCT n.element_id, count( n.user_id ) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('fotografias_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantVisitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantVisitas);

		$query_TiempoTotal = "SELECT DISTINCT n.element_id, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('fotografias_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.element_id, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('fotografias_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.element_id, count(distinct n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('fotografias_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$element_id = $Rows_config[$i]['media_id'];
				for($y=0;$y<count($Rows_CantVisitas);$y++) {
					if( $element_id == $Rows_CantVisitas[$y]['element_id'] ){
						$Rows_config[$i]['cantidadVisitas'] = $Rows_CantVisitas[$y]['cantidadVisitas'];
					}
				}
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $element_id == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $element_id == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $element_id == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaGaleriasV( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT n.* FROM ludus_media n WHERE type = 2";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$query_CantVisitas = "SELECT DISTINCT n.element_id, count( n.user_id ) as cantidadVisitas FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('videos_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantVisitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantVisitas);

		$query_TiempoTotal = "SELECT DISTINCT n.element_id, sum(n.time_navigation) as tiempoTotal FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('videos_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT DISTINCT n.element_id, avg(n.time_navigation) as tiempoPromedio FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('videos_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT DISTINCT n.element_id, count(distinct n.user_id) as cantidadUsuarios FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id AND n.element_id > 0 AND n.section IN ('videos_detail.php')
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' $trayectoria GROUP BY n.element_id";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$element_id = $Rows_config[$i]['media_id'];
				for($y=0;$y<count($Rows_CantVisitas);$y++) {
					if( $element_id == $Rows_CantVisitas[$y]['element_id'] ){
						$Rows_config[$i]['cantidadVisitas'] = $Rows_CantVisitas[$y]['cantidadVisitas'];
					}
				}
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $element_id == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $element_id == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $element_id == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDetGaleriasF( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT me.media,md.media_detail,me.status_id,md.likes,md.source FROM ludus_media_detail md, ludus_media me
		WHERE me.type=1 AND me.media_id = md.media_id AND md.likes > 0 ORDER BY md.likes DESC LIMIT 0,12";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDetGaleriasV( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT me.media,md.media_detail,me.status_id,md.likes,md.source FROM ludus_media_detail md, ludus_media me
		WHERE me.type=2 AND me.media_id = md.media_id AND md.likes > 0 ORDER BY md.likes DESC LIMIT 0,12";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaConcesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT dealer FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaZonas(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT z.zone FROM ludus_zone z";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaZonasCcs(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT DISTINCT z.zone, d.dealer
			FROM ludus_zone z, ludus_areas a, ludus_headquarters h, ludus_dealers d
			WHERE z.zone_id = a.zone_id
			AND a.area_id = h.area_id
			AND h.dealer_id = d.dealer_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaZonasSecciones( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT DISTINCT z.zone, n.section, 0 AS cantidadVisitas, 0 AS tiempoTotal, 0 AS tiempoPromedio, 0 AS cantidadUsuarios
			FROM ludus_navigation n, ludus_zone z
			WHERE n.section IN ( {$p['secciones']} )
			ORDER BY z.zone";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaUsuariosCcs( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND c.charge_id IN ( {$p['trayectoria']} ) ";

		@session_start();
		$query_sql = "SELECT d.dealer, n.section, count(n.section) as cantidadVisitas
			FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users c
			WHERE n.user_id = u.user_id
			AND c.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND n.section IN ( {$p['secciones']} )
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			GROUP BY d.dealer, n.section";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_TiempoTotal = "SELECT d.dealer, n.section, sum(n.time_navigation) as tiempoTotal
			FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users c
			WHERE n.user_id = u.user_id
			AND c.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND n.section IN ({$p['secciones']})
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			GROUP BY d.dealer, n.section";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT d.dealer, n.section, avg(n.time_navigation) as tiempoPromedio
			FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users c
			WHERE n.user_id = u.user_id
			AND c.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND n.section IN ({$p['secciones']})
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			GROUP BY d.dealer, n.section";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT d.dealer, n.section, count(distinct n.user_id) as cantidadUsuarios
			FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users c
			WHERE n.user_id = u.user_id
			AND c.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND n.section IN ({$p['secciones']})
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			GROUP BY d.dealer, n.section";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for( $i=0; $i< count( $Rows_config ); $i++ ) {

			$concs = $Rows_config[$i]['dealer'];
			$section = $Rows_config[$i]['section'];

				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $section == $Rows_TiempoTotal[$y]['section'] && $concs == $Rows_TiempoTotal[$y]['dealer']){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $section == $Rows_TiempoPromedio[$y]['section']  && $concs == $Rows_TiempoPromedio[$y]['dealer']){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'] ;
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $section == $Rows_CantidadUsr[$y]['section']  && $concs == $Rows_CantidadUsr[$y]['dealer']){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		// print_r( $Rows_config );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin consultaUsuariosCcs

	function consultaCategoriasArg(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT a.argument_id, a.argument,a.image, c.argument_cat
			FROM ludus_argument a, ludus_argument_cat c
			WHERE a.argument_cat_id = c.argument_cat_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaListaCategorias(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT c.argument_cat, 0 AS cantidadVisitas, 0 AS tiempoTotal, 0 AS tiempoPromedio, 0 AS cantidadUsuarios
			FROM ludus_argument_cat c WHERE c.status_id = 1";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDetalleArgumentario( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$query_sql = "SELECT n.element_id, COUNT( n.navigation_id ) AS cantidadVisitas
			FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id
			AND n.section = 'argumentarios.php'
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			GROUP BY n.element_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_TiempoTotal = "SELECT n.element_id, sum(n.time_navigation) as tiempoTotal
		    FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id
			AND n.section = 'argumentarios.php'
		    AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
		    GROUP BY n.element_id";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);

		$query_TiempoPromedio = "SELECT n.element_id, avg(n.time_navigation) as tiempoPromedio
		    FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id
			AND n.section = 'argumentarios.php'
		    AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
		    GROUP BY n.element_id";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT n.element_id, count(distinct n.user_id) as cantidadUsuarios
		    FROM ludus_navigation n, ludus_charges_users u
			WHERE n.user_id = u.user_id
			AND n.section = 'argumentarios.php'
		    AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
		    GROUP BY n.element_id";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		$num_RowConfig = count($Rows_config);
		$num_RowsTiempoTotal = count($Rows_TiempoTotal);
		$num_RowsTiempoPromedio = count($Rows_TiempoPromedio);
		$num_RowsCantidadUsr = count($Rows_CantidadUsr);

		for( $i=0; $i< $num_RowConfig ; $i++ ) {
			$argument = $Rows_config[$i]['element_id'];
				for($y=0;$y<$num_RowsTiempoTotal;$y++) {
					if( $argument == $Rows_TiempoTotal[$y]['element_id'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<$num_RowsTiempoPromedio;$y++) {
					if( $argument == $Rows_TiempoPromedio[$y]['element_id'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<$num_RowsCantidadUsr;$y++) {
					if( $argument == $Rows_CantidadUsr[$y]['element_id'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function desc_consultaUsuariosCcs( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND cu.charge_id IN ( {$p['trayectoria']} ) ";

		$query_sql = "SELECT DISTINCT z.zone, d.dealer, h.headquarter, u.last_name, u.first_name, u.identification,c.charge, n.section, n.date_navigation, n.element_id, n.time_navigation, n.name_section
			FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges c, ludus_charges_users cu
			WHERE n.user_id = u.user_id
			AND u.user_id = cu.user_id
			AND cu.charge_id = c.charge_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			AND n.section IN ({$p['secciones']})
			$trayectoria
			AND n.date_navigation BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_cat = "SELECT a.argument_id, a.argument, c.argument_cat
			FROM ludus_argument a, ludus_argument_cat c
			WHERE a.argument_cat_id = c.argument_cat_id";
		$categorias = $DataBase_Acciones->SQL_SelectMultipleRows( $query_cat );

		$cuenta = count( $Rows_config );

		for ($i=0; $i < $cuenta ; $i++) {
			$Rows_config[$i]['argument'] = "";
			$Rows_config[$i]['argument_cat'] = "";
			if( $Rows_config[$i]['section'] == 'argumentarios.php' ){

				if( $Rows_config[$i]['element_id'] == 0 ){

					$Rows_config[$i]['argument'] = "Página Principal";
					$Rows_config[$i]['argument_cat'] = "Página Principal";

				}else{

					foreach ($categorias as $key => $cat) {
						if( $Rows_config[$i]['element_id'] == $cat['argument_id'] ){

							$Rows_config[$i]['argument'] = $cat['argument'];
							$Rows_config[$i]['argument_cat'] = $cat['argument_cat'];

						}
					}

				}

			}//fin if = argumentarios.php
		}

		// print_r( $Rows_config );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin consultaUsuariosCcs

	function getCargos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT c.charge_id, c.charge FROM ludus_charges c WHERE c.status_id = 1";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//============================================================================================
	//Consulta la informacion de los audios
	//============================================================================================
	function consultaAudios( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND u.charge_id IN ( {$p['trayectoria']} ) ";

		$DataBase_Acciones = new Database();
		$query_sql = "SELECT l.library_id, l.file, l.name, l.image, l.status_id,
				( SELECT COUNT( v.user_id )
					FROM ludus_library_views v, ludus_charges_users u
					WHERE v.library_id = l.library_id
					AND v.user_id = u.user_id
					$trayectoria
					AND v.date_edition BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' ) AS views,
				( SELECT COUNT( ll.user_id )
					FROM ludus_library_likes ll, ludus_charges_users u
					WHERE ll.library_id = l.library_id
					AND ll.user_id = u.user_id
					$trayectoria
					AND ll.date_edition BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59' ) AS n_likes
			FROM ludus_libraries l
			where l.file like '%.mp3%'";
		// echo("<br>$query_sql<br>");
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query_sql );
		unset( $DataBase_Acciones );
		return $Rows_config;
	}
	//============================================================================================
	//Consulta las vistas hechas por los usuarios a los audios
	//============================================================================================
	function consultaAudiosViews( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND cu.charge_id IN ( {$p['trayectoria']} ) ";

		$DataBase_Acciones = new Database();
		$query_sql = "SELECT ll.name, ll.image, ll.library_id, ll.file, lll.date_edition, lu.first_name, lu.last_name
			FROM ludus_libraries ll, ludus_library_views lll, ludus_users lu, ludus_charges_users cu
			where lll.library_id = ll.library_id
			and lu.user_id = lll.user_id
			AND lu.user_id = cu.user_id
			AND lll.date_edition BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			AND ll.file like '%.mp3%'";
		// echo("<br>$query_sql<br>");
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query_sql );
		unset( $DataBase_Acciones );
		return $Rows_config;
	}
	//============================================================================================
	//Consulta los likes hechos por los usuarios
	//============================================================================================
	function consultaAudiosLikes( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND cu.charge_id IN ( {$p['trayectoria']} ) ";

		$DataBase_Acciones = new Database();
		$query_sql = "SELECT ll.name, ll.image, ll.library_id, ll.file, lll.date_edition, lu.first_name, lu.last_name
			FROM ludus_libraries ll, ludus_library_likes lll, ludus_users lu, ludus_charges_users cu
			where lll.library_id = ll.library_id
			and lu.user_id = lll.user_id
			AND lu.user_id = cu.user_id
			AND lll.date_edition BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			AND ll.file LIKE '%.mp3%'";
		// echo("<br>$query_sql<br>");
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query_sql );
		unset( $DataBase_Acciones );
		return $Rows_config;
	}
	//============================================================================================
	//Consulta la informacion de los audios
	//============================================================================================
	function consultaDescAudios( $p ){
		include_once('../config/database.php');
		include_once('../config/config.php');

		$trayectoria = ( !is_array( $p['trayectoria'] ) && $p['trayectoria'] == '0') ? "" : "AND cu.charge_id IN ( {$p['trayectoria']} ) ";
		$query_sql = "SELECT h.headquarter, d.dealer, z.zone, lu.first_name, lu.last_name, lu.identification, c.charge, ll.name, ll.image, ll.library_id, ll.file, lll.date_edition
			FROM ludus_libraries ll, ludus_library_views lll, ludus_users lu, ludus_charges_users cu, ludus_charges c, ludus_dealers d, ludus_headquarters h, ludus_zone z, ludus_areas a
			WHERE lll.library_id = ll.library_id
			AND lu.user_id = lll.user_id
			AND lu.user_id = cu.user_id
			AND cu.charge_id = c.charge_id
			AND lu.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			AND lll.date_edition BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$trayectoria
			AND ll.file like '%.mp3%'";
		$DataBase_Acciones = new Database();

		// echo("<br>$query_sql<br>");
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query_sql );
		unset( $DataBase_Acciones );
		return $Rows_config;
	}

}
