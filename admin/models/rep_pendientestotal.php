<?php
Class RepPendientesCursos {
	function consultaDatosAll(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT c.newcode, c.type, c.course, count(u.user_id) as cant_usr
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d
					WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.status_id = 1 
					AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score > 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1)
					GROUP BY c.newcode, c.type, c.course
					ORDER BY cant_usr DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
