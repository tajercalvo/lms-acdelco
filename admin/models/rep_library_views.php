<?php


class Asistentesviews{


		public static function consultar_Views(){

			include_once('../../config/init_db.php');

			$query = "SELECT library_id, name FROM ludus_libraries order by library_id desc";
			$resultado = DB::query($query);
			return $resultado;
		}

		public static function consultar_user($library_id){
			include_once('../../config/init_db.php');

			$query = "SELECT
						    u.first_name nombre,
						    u.last_name apellido,
						    u.identification identificacion,
						    d.dealer concesionario,
						    h.headquarter sede,
						    z.zone zona,
						    lv.play,
						    lv.finish
						FROM
						    ludus_users u
						        INNER JOIN
						    ludus_headquarters h ON h.headquarter_id = u.headquarter_id
						        INNER JOIN
						    ludus_dealers d ON d.dealer_id = h.dealer_id
						        INNER JOIN
						    ludus_areas a ON a.area_id = h.area_id
						        INNER JOIN
						    ludus_zone z ON z.zone_id = a.zone_id
						        INNER JOIN
						    ludus_library_views lv ON lv.user_id = u.user_id
						        AND lv.library_id = $library_id
						GROUP BY u.user_id ";
			$resultado = DB::query($query);
			return $resultado;

	     }


}

// $consulta = new Asistentesviews();
// $consulta -> consultar_views();






