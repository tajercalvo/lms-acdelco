<?php
Class CursosVCT {
	//====================================================
	// Retorna los datos correspondiente al curso actual
	//====================================================
	public static function getProgramacion( $p, $prof = "" ){
		include_once( $prof.'../config/init_db.php');
		@session_start();
		$query_sql = "SELECT s.*, u.first_name, u.last_name, u.identification, u.image
					FROM ludus_schedule s, ludus_users u
					WHERE s.teacher_id = u.user_id AND s.schedule_id = %i ";
		$Rows_config = DB::queryFirstRow($query_sql, $p['schedule_id']);
		return $Rows_config;
	}//fin getDatosCurso
	//====================================================
	// Consulta si ya se esta inscrito en un curso
	//====================================================
	public static function consultaInscripcion($p, $prof=''){
		include_once( $prof.'../config/init_db.php');
		@session_start();
		$idQuien = 0;
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT inscription_id FROM ludus_inscriptions WHERE user_id = %i AND course_id = %i AND schedule_id = %i";
		$Rows_config = DB::queryFirstRow($query_sql, $idQuien, $p['course_id'], $p['schedule_id'] );
		if( isset( $Rows_config['inscription_id'] ) ){
			return true;
		}else{
			return false;
		}
	}//fin funcion consultaInscripcion
	//====================================================
	// funcion para consultar la informacion general del curso
	//====================================================
	public static function getDetalleCurso($p, $prof=""){
		include_once( $prof.'../config/init_db.php');
		$query_sql = "SELECT c.*, s.supplier
			FROM ludus_courses c, ludus_supplier s
			WHERE c.course_id = %i AND c.supplier_id = s.supplier_id ";
		$Rows_config = DB::queryFirstRow($query_sql, $p['course_id']);
		return $Rows_config;
	}//fin funcion getDetalleCurso
	//====================================================
	// Funcion que consulta los archivos que se mostraran en un curso VCT (videos)
	//====================================================
	public static function consultaVCTFiles( $p, $prof="" ){
		include_once( $prof.'../config/init_db.php');
		$query_sql = "SELECT * FROM ludus_vct_files WHERE schedule_id = %i AND status_id = 1";
		$Rows_config = DB::query($query_sql, $p['schedule_id'] );
		return $Rows_config;
	}//fin consultaVCTFiles


}
