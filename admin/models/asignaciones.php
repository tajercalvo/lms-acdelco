<?php
class Asignaciones
{
	// ========================================================
	//
	// ========================================================
	public static function consultaAsignacion($schedule_id, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, m.module_id, m.module, c.course, l.living, l.address, s.status_id, c.newcode, d.area, s.max_size, s.inscriptions, s.max_inscription_date, c.newcode, c.type, s.image_ics, u.first_name, u.last_name, s.finalizado
						FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_areas d, ludus_users u, ludus_modules m
						WHERE s.course_id = c.course_id
						AND s.living_id = l.living_id
						AND s.schedule_id = '$schedule_id'
						AND l.city_id = d.area_id
						AND s.teacher_id = u.user_id
						AND c.course_id = m.course_id
						AND m.module_id = s.module_id";
		$Rows_config = DB::queryFirstRow($query_sql);
		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaCargosAsign($schedule_id)
	{
		include_once('../config/init_db.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT u.charge
				FROM ludus_schedule s, ludus_charges_courses c, ludus_charges u
				WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND u.status_id = 1";

		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaDatosUsuario_sche($idUsuario)
	{
		include_once('../../config/init_db.php');
		$query_sql = "SELECT u.*, c.headquarter, a.area
						FROM ludus_users u, ludus_headquarters c, ludus_areas a
						WHERE u.user_id = '$idUsuario' AND u.headquarter_id = c.headquarter_id AND c.area_id = a.area_id";

		$row_consulta = DB::queryFirstRow($query_sql);

		return $row_consulta;
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaAsignaciones($start, $end)
	{
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$dealer_id = $_SESSION['dealer_id'];

		// print_r( $_SESSION );

		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 4) {
			$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.course, l.living, l.address, s.status_id, c.newcode, d.area, u.last_name, u.first_name, s.inscriptions, s.max_size,
				s.max_inscription_date, u1.first_name as fn_editor, u1.last_name as ln_editor, u1.email, c.newcode, m.module, m.code
					FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_areas d, ludus_users u, ludus_users u1, ludus_modules m, (SELECT cc.course_id, count(*) as cantidad
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_headquarters h
					WHERE cc.charge_id = cu.charge_id AND
					cu.user_id = u.user_id AND
					u.headquarter_id = h.headquarter_id AND
					h.dealer_id > 0
					GROUP BY cc.course_id) cu
					WHERE s.course_id = c.course_id AND s.living_id = l.living_id AND s.status_id = 1 AND l.city_id = d.area_id AND s.teacher_id = u.user_id
					AND s.editor = u1.user_id
					AND c.course_id = cu.course_id AND s.module_id = m.module_id AND cu.cantidad > 0  AND s.start_date BETWEEN '$start' AND '$end' ";
		} else {
			$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.course, l.living, l.address, s.status_id, c.newcode, d.area, u.last_name, u.first_name, s.inscriptions,
			s.max_size, s.max_inscription_date, u1.first_name as fn_editor, u1.last_name as ln_editor, u1.email, c.newcode, m.module, m.code
					FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_areas d, ludus_users u, ludus_users u1, ludus_modules m, (SELECT cc.course_id, count(*) as cantidad
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_headquarters h
					WHERE cc.charge_id = cu.charge_id AND
					cu.user_id = u.user_id AND
					u.headquarter_id = h.headquarter_id AND
					h.dealer_id = '$dealer_id'
					GROUP BY cc.course_id) cu
					WHERE s.course_id = c.course_id AND s.living_id = l.living_id AND s.status_id = 1 AND l.city_id = d.area_id AND s.teacher_id = u.user_id
					AND s.editor = u1.user_id
					AND c.course_id = cu.course_id AND s.module_id = m.module_id AND cu.cantidad > 0  AND s.start_date BETWEEN '$start' AND '$end' ";
		}
		$query_ValInvitations = "SELECT s.schedule_id, s.max_size as cantidad
								FROM ludus_dealer_schedule s
								WHERE s.dealer_id  = $dealer_id ";

		$Rows_config = DB::query($query_sql);
		$Rows_Cupos = DB::query($query_ValInvitations);
		for ($i = 0; $i < count($Rows_config); $i++) {
			$schedule_id = $Rows_config[$i]['schedule_id'];
			//Cargos
			for ($y = 0; $y < count($Rows_Cupos); $y++) {
				if ($schedule_id == $Rows_Cupos[$y]['schedule_id']) {
					if ($Rows_Cupos[$y]['cantidad'] && $Rows_Cupos[$y]['cantidad'] > 0) {
						$Rows_config[$i]['cupos'] = "SI";
					}
				}
			}
			//Cargos
		}

		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaAsignacionesIns($start, $end)
	{
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.course, l.living, l.address, s.status_id, c.newcode, d.area, u.last_name, u.first_name, s.inscriptions, s.max_size, s.max_inscription_date, u1.first_name as fn_editor, u1.last_name as ln_editor, u1.email, c.newcode, c.type, mo.module
						FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_areas d, ludus_users u, ludus_users u1, ludus_modules mo
						WHERE s.course_id = c.course_id
						AND s.living_id = l.living_id
						AND s.module_id = mo.module_id
						AND s.teacher_id = '$idQuien'
						AND s.status_id = 1
						AND l.city_id = d.area_id
						AND s.teacher_id = u.user_id
						AND s.editor = u1.user_id
						AND s.start_date BETWEEN '$start' AND '$end' ";

		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaConcesionarios($schedule_id)
	{
		include_once('../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT DISTINCT d.dealer, d.dealer_id
			FROM ludus_schedule s, ludus_charges_courses c, ludus_charges_users u, ludus_users r, ludus_headquarters h, ludus_areas a, ludus_dealers d
			WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND
			u.status_id = 1 AND u.user_id = r.user_id AND r.headquarter_id = h.headquarter_id AND h.dealer_id > 0 AND h.area_id = a.area_id AND h.dealer_id = d.dealer_id
			ORDER BY d.dealer";

		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}
	public static function consultaSchedule($schedule_id, $dealer_id, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql_s = "SELECT * FROM ludus_dealer_schedule s WHERE s.schedule_id = '$schedule_id' AND s.dealer_id = '$dealer_id' ";
		// echo $query_sql_s;
		$Rows_config_s = DB::queryFirstRow($query_sql_s);

		return $Rows_config_s;
	}

	public static function scheduleFInalizado($schedule_id)
	{
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql_s = "SELECT finalizado FROM ludus_schedule  WHERE schedule_id = $schedule_id ";
		$Rows_config_s = DB::queryFirstRow($query_sql_s);
		return $Rows_config_s['finalizado'];
	}

	public static function usuariosSeleccionados($usuarios)
	{
		include_once('../../config/init_db.php');
		@session_start();
		$query_sql_s = "SELECT user_id FROM ludus_users  WHERE identification in ( $usuarios ) ";
		$Rows_config = DB::query($query_sql_s);
		return $Rows_config;
	}


	public static function cantidadUsuariosAsig($schedule_id, $dealer_id_adm)
	{
		include_once('../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$dealer_id = $_SESSION['dealer_id'];
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 4) {
			if ($dealer_id_adm == "") {
				$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
					FROM ludus_schedule s, ludus_charges_courses c, ludus_charges_users u, ludus_users r, ludus_headquarters h, ludus_areas a
					WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND
					u.status_id = 1 AND u.user_id = r.user_id AND r.headquarter_id = h.headquarter_id AND h.dealer_id > 0 AND h.area_id = a.area_id
					ORDER BY a.area, r.first_name, r.last_name";
			} else {
				$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
					FROM ludus_schedule s, ludus_charges_courses c, ludus_charges_users u, ludus_users r, ludus_headquarters h, ludus_areas a
					WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND
					u.status_id = 1 AND u.user_id = r.user_id AND r.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id_adm' AND h.area_id = a.area_id
					ORDER BY a.area, r.first_name, r.last_name";
			}
		} else {
			$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
				FROM ludus_schedule s, ludus_charges_courses c, ludus_charges_users u, ludus_users r, ludus_headquarters h, ludus_areas a
				WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND
				u.status_id = 1 AND u.user_id = r.user_id AND r.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND h.area_id = a.area_id
				ORDER BY a.area, r.first_name, r.last_name";
		}

		$Rows_config = DB::query($query_sql);

		return count($Rows_config);
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaUsuariosAsig($schedule_id, $dealer_id_adm)
	{
		include_once('../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$dealer_id = $_SESSION['dealer_id'];
		$usuarios = Asignaciones::consultaAsignados($schedule_id, $dealer_id_adm);
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 4) {
			if ($dealer_id_adm == "") {
				$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
					FROM ludus_schedule s, ludus_charges_courses c, ludus_charges_users u, ludus_users r, ludus_headquarters h, ludus_areas a
					WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND u.status_id = 1 AND r.status_id = 1 AND u.user_id = r.user_id
					AND r.headquarter_id = h.headquarter_id AND h.dealer_id > 0 AND h.area_id = a.area_id AND r.user_id NOT IN ($usuarios)
					ORDER BY a.area, r.first_name, r.last_name";
			} else {
				$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
				FROM ludus_schedule s, ludus_charges_courses c, ludus_charges_users u, ludus_users r, ludus_headquarters h, ludus_areas a
				WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND
				u.status_id = 1 AND r.status_id = 1 AND u.user_id = r.user_id AND r.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id_adm' AND h.area_id = a.area_id AND r.user_id NOT IN ($usuarios)
				ORDER BY a.area, r.first_name, r.last_name";
			}
		} else {
			$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
				FROM ludus_schedule s, ludus_charges_courses c, ludus_charges_users u, ludus_users r, ludus_headquarters h, ludus_areas a
				WHERE s.schedule_id = '$schedule_id' AND s.course_id = c.course_id AND c.status_id = 1 AND c.charge_id = u.charge_id AND
				u.status_id = 1 AND r.status_id = 1 AND u.user_id = r.user_id AND r.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND h.area_id = a.area_id AND r.user_id NOT IN ($usuarios)
				ORDER BY a.area, r.first_name, r.last_name";
		}
		//echo $query_sql;

		$Rows_config = DB::query($query_sql);

		$query_Cargo = "SELECT c.charge, u.user_id FROM ludus_charges_users u, ludus_charges c WHERE u.charge_id = c.charge_id AND u.status_id = 1";
		$Rows_Charges = DB::query($query_Cargo);

		$query_YaenInvitacion = "SELECT * FROM ludus_invitation WHERE schedule_id = $schedule_id ";
		$Rows_Invit = DB::query($query_YaenInvitacion);

		$query_Notas = "SELECT r.score, r.approval, r.date_creation, r.user_id
						FROM ludus_schedule s, ludus_modules_results_usr r, ludus_modules m
						WHERE s.schedule_id = '$schedule_id' AND s.course_id = m.course_id AND m.module_id = r.module_id ORDER BY r.score DESC";
		$Rows_Notas = DB::query($query_Notas);

		//Consulta detalle programación
		$query_sql_s = "SELECT s.start_date FROM ludus_schedule s WHERE s.schedule_id = '$schedule_id'";
		$Rows_config_s = DB::queryFirstRow($query_sql_s);
		$Start_Date_var = $Rows_config_s['start_date'];
		$month = substr($Start_Date_var, 5, 2);
		$year = substr($Start_Date_var, 0, 4);
		$fec_ini_comp = $year . '-' . $month . '-01';
		$fec_fin_comp = $year . '-' . $month . '-31';
		$query_Otras = "SELECT i.user_id, i.status_id, i.date_send, c.newcode, c.course, s.start_date, l.living, c.newcode
						FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_livings l
						WHERE i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND s.living_id = l.living_id AND (s.start_date between '$fec_ini_comp' AND '$fec_fin_comp')
						ORDER BY i.user_id, s.start_date";

		$Rows_Otras = DB::query($query_Otras);
		//Consulta detalle programación

		for ($i = 0; $i < count($Rows_config); $i++) {
			$var_User = $Rows_config[$i]['user_id'];
			//Cargos del usuario
			for ($y = 0; $y < count($Rows_Charges); $y++) {
				if ($var_User == $Rows_Charges[$y]['user_id']) {
					$Rows_config[$i]['cargos'][] = $Rows_Charges[$y]['charge'];
				}
			}
			//Cargos del usuario
			//Notas del usuario
			for ($x = 0; $x < count($Rows_Notas); $x++) {
				if ($var_User == $Rows_Notas[$x]['user_id']) {
					$Rows_config[$i]['notas'][] = $Rows_Notas[$x];
				}
			}
			//Notas del usuario
			//Otras Asignaciones en el mes del usuario
			for ($v = 0; $v < count($Rows_Otras); $v++) {
				if ($var_User == $Rows_Otras[$v]['user_id']) {
					$Rows_config[$i]['otras'][] = $Rows_Otras[$v];
				}
			}
			//Otras Asignaciones en el mes del usuario
			$Inv_data = 0;
			//Invitaciones del usuario
			for ($z = 0; $z < count($Rows_Invit); $z++) {
				if ($var_User == $Rows_Invit[$z]['user_id']) {
					$Inv_data = $Rows_Invit[$z]['invitation_id'];
				}
			}
			//Invitaciones del usuario
			$Rows_config[$i]['YaenInvitacion'] = $Inv_data;
		}

		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function BorraSchedule($schedule_id, $dealer_id, $dataSeleccionados, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		$resultado = 0;
		$list_users = "";
		if ($dataSeleccionados != "") {
			$list_users = " AND u.user_id NOT IN ($dataSeleccionados)";
		}
		$usuarios = DB::query("SELECT * FROM ludus_invitation WHERE schedule_id = $schedule_id AND user_id IN (SELECT u.user_id
						FROM ludus_users u, ludus_headquarters h
						WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id )");

		if (count($usuarios) > 0) {
			$where = "schedule_id = $schedule_id AND user_id IN (SELECT u.user_id
							FROM ludus_users u, ludus_headquarters h
							WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id $list_users )";

			DB::delete("ludus_invitation", $where);
			$affected = DB::affectedRows();
			if ($affected > 0) {
				DB::update('ludus_schedule', [
					"inscriptions" => DB::sqleval("inscriptions - $affected")
				], "schedule_id = '$schedule_id'");

				DB::update('ludus_dealer_schedule', [
					'inscriptions' => DB::sqleval("inscriptions - $affected")
				], "schedule_id = $schedule_id AND dealer_id = $dealer_id");
			}
		}
		return $resultado;
	}
	// ========================================================
	//
	// ========================================================
	public static function CrearInvitation($user_id, $schedule_id, $dealer_id, $headquarter_id, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		@session_start();
		$resultado = 0;
		$idQuien = $_SESSION['idUsuario'];
		if ($user_id >= 0 && $user_id != "") {
			$consulta = "SELECT COUNT(i.user_id) AS user FROM ludus_invitation i, ludus_users u, ludus_headquarters h WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND i.user_id = $user_id AND i.schedule_id = $schedule_id AND h.dealer_id = $dealer_id";
			$existe = DB::queryFirstRow($consulta);
			if ($existe['user'] == 0) {

				$query = "INSERT INTO ludus_invitation
									(
									schedule_id,
									user_id,
									headquarter_id,
									date_send,
									result_preview,
									status_id,
									creator
									)
									VALUES
									(
									$schedule_id,
									$user_id,
									$headquarter_id,
									now(),
									2,
									1,
									$idQuien
									);";

				$Rows_config = DB::query($query);
				$resultado = DB::insertId();
				if ($resultado > 0) {
					DB::update('ludus_schedule', [
						'inscriptions' => DB::sqleval("inscriptions + 1")
					], "schedule_id = $schedule_id");
					DB::update('ludus_dealer_schedule', [
						'inscriptions' => DB::sqleval("inscriptions + 1")
					], "schedule_id = $schedule_id AND dealer_id = $dealer_id");
				}
			}
		}

		return $resultado;
	}
	// ========================================================
	//
	// ========================================================
	public static function ActualizaSchedule($schedule_id, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		DB::update(
			'ludus_schedule',
			['inscriptions' => DB::sqleval("(SELECT COUNT( i.user_id )
				FROM ludus_invitation i
				WHERE i.schedule_id = $schedule_id)")],
			"schedule_id = $schedule_id"
		);

		$resultado = DB::affectedRows();
		return $resultado;
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaMisAsignaciones($start, $end)
	{
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT
											s.schedule_id,s.start_date,s.end_date,c.course,c.course_id,l.living,l.address,s.status_id,c.newcode,d.area,u.last_name,u.first_name,s.inscriptions,s.max_size,s.max_inscription_date,c.newcode,c.type,m.module,m.module_id,m.code, '' as activity_schedule_id
									FROM
											ludus_schedule s
									INNER JOIN ludus_courses c ON
											s.course_id = c.course_id
									INNER JOIN ludus_livings l ON
											s.living_id = l.living_id
									INNER JOIN ludus_areas d ON
											l.city_id = d.area_id
									INNER JOIN ludus_users u ON
											s.teacher_id = u.user_id
									INNER JOIN ludus_invitation i ON
											i.schedule_id = s.schedule_id
									INNER JOIN ludus_modules m ON
											s.module_id = m.module_id
									WHERE
											s.status_id = 1 AND c.type IN('ILT', 'OJT', 'VCT', 'MFR') AND i.user_id = '$idQuien' AND s.start_date BETWEEN '$start' AND '$end'
									GROUP BY s.schedule_id 
									";
		$query_sql .= "UNION
										SELECT
												s.schedule_id,s.start_date,s.end_date,c.course,c.course_id,l.living,l.address,s.status_id,c.newcode,d.area,u.last_name,u.first_name,s.inscriptions,s.max_size,s.max_inscription_date,c.newcode,c.type,m.module,m.module_id,m.code, '' as activity_schedule_id 
										FROM
												ludus_schedule s
										INNER JOIN ludus_courses c ON
												s.course_id = c.course_id
										INNER JOIN ludus_livings l ON
												s.living_id = l.living_id
										INNER JOIN ludus_areas d ON
												l.city_id = d.area_id
										INNER JOIN ludus_users u ON
												s.teacher_id = u.user_id
										INNER JOIN ludus_modules m ON
												s.module_id = m.module_id
										WHERE
												s.teacher_id = '$idQuien' AND s.status_id = 1 AND c.type IN('VCT') AND s.start_date BETWEEN '$start' AND '$end' GROUP BY s.schedule_id
												";

		$Rows_config = DB::query($query_sql);
		$actividades = [];
		if (!empty($Rows_config)) {
			foreach ($Rows_config as $key => $value) {
				if ($value["type"] == "VCT") {
					$query = "SELECT
										s.schedule_id,
										las.desde as start_date,
										las.hasta as end_date,
										c.course,
										c.course_id,
										l.living,
										l.address,
										s.status_id,
										c.newcode,
										d.area,
										u.last_name,
										u.first_name,
										s.inscriptions,
										s.max_size,
										s.max_inscription_date,
										c.newcode,
										c.type,
										m.module,
										m.module_id,
										m.code,
										las.activity_schedule_id,
										lma.tipo,
										lma.review_id 
								FROM
										ludus_schedule s
								INNER JOIN ludus_courses c ON
										s.course_id = c.course_id
								INNER JOIN ludus_livings l ON
										s.living_id = l.living_id
								INNER JOIN ludus_areas d ON
										l.city_id = d.area_id
								INNER JOIN ludus_users u ON
										s.teacher_id = u.user_id
								INNER JOIN ludus_invitation i ON
										i.schedule_id = s.schedule_id
								INNER JOIN ludus_modules m ON
										s.module_id = m.module_id
								INNER JOIN ludus_activities_schedule las ON
										las.schedule_id = s.schedule_id
								INNER JOIN ludus_modules_activities lma ON 
										lma.activity_id = las.activity_id 
								WHERE
										s.status_id = 1 AND i.user_id = $idQuien AND s.schedule_id = '{$value["schedule_id"]}'";

					$response = DB::query($query);
					if (!empty($response)) {
						$actividades = array_merge($actividades, $response);
					}
				}
			}
		}

		if (!empty($actividades)) {
			$Rows_config = array_merge($Rows_config, $actividades);
		}
		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function ConsultaCirculares($start, $end, $cargos_id)
	{
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT m.*, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
						FROM ludus_newsletters m, ludus_users s, ludus_newsletters_charges c
						WHERE m.status_id = 1 AND m.user_id = s.user_id AND m.newsletter_id = c.newsletter_id
						AND m.date_edition BETWEEN '$start' AND '$end'
						AND c.charge_id IN ($cargos_id) AND c.newsletter_id NOT IN (SELECT newsletter_id FROM ludus_newsletters_visites WHERE user_id = $idQuien)
						ORDER BY m.date_edition DESC";

		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function ConsultaCircularesJSL($cargos_id)
	{
		include_once('../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT m.*, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
						FROM ludus_newsletters m, ludus_users s, ludus_newsletters_charges c
						WHERE m.status_id = 1 AND m.user_id = s.user_id AND m.newsletter_id = c.newsletter_id
						AND c.charge_id IN ($cargos_id) AND c.newsletter_id NOT IN (SELECT newsletter_id FROM ludus_newsletters_visites WHERE user_id = $idQuien)
						ORDER BY m.date_edition DESC";

		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	// ========================================================
	//
	// ========================================================
	public static function consultaUsuariosNoAsig($schedule_id, $dealer_id_adm, $filtro, $usuarios, $prof = "")
	{
		@session_start();
		include_once($prof . '../config/init_db.php');
		$idQuien = $_SESSION['idUsuario'];
		$dealer_id = (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 4 && $dealer_id_adm != "") ? $dealer_id_adm : $_SESSION['dealer_id'];

		$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
			FROM ludus_users r, ludus_headquarters h, ludus_areas a
			WHERE r.status_id = 1
			AND r.headquarter_id = h.headquarter_id
			AND h.dealer_id = '$dealer_id'
			AND h.area_id = a.area_id
			AND ( r.first_name like '%$filtro%' OR r.last_name like '%$filtro%' OR r.identification like '%$filtro%' )
			AND r.user_id NOT IN ( $usuarios )
			ORDER BY a.area, r.first_name, r.last_name";
		if ($filtro == "") {
			$query_sql .= " LIMIT 0,30";
		}
		//echo $query_sql;

		$Rows_config = DB::query($query_sql);

		$query_Cargo = "SELECT c.charge, u.user_id
					FROM ludus_charges_users u, ludus_charges c, ludus_users r, ludus_headquarters h
					WHERE u.charge_id = c.charge_id
					AND u.status_id = 1
					AND u.user_id = r.user_id
					AND r.status_id = 1
					AND r.headquarter_id = h.headquarter_id
					AND h.dealer_id = $dealer_id";
		$Rows_Charges = DB::query($query_Cargo);

		$query_YaenInvitacion = "SELECT * FROM ludus_invitation WHERE schedule_id = $schedule_id ";
		$Rows_Invit = DB::query($query_YaenInvitacion);

		$query_Notas = "SELECT r.score, r.approval, r.date_creation, r.user_id
						FROM ludus_schedule s, ludus_modules_results_usr r, ludus_modules m
						WHERE s.schedule_id = '$schedule_id' AND s.course_id = m.course_id AND m.module_id = r.module_id ORDER BY r.score DESC";
		$Rows_Notas = DB::query($query_Notas);

		//Consulta detalle programación
		$query_sql_s = "SELECT s.start_date FROM ludus_schedule s WHERE s.schedule_id = '$schedule_id'";
		$Rows_config_s = DB::queryFirstRow($query_sql_s);

		$Start_Date_var = $Rows_config_s['start_date'];
		$month = substr($Start_Date_var, 5, 2);
		$year = substr($Start_Date_var, 0, 4);
		$fec_ini_comp = $year . '-' . $month . '-01';
		$fec_fin_comp = $year . '-' . $month . '-31';
		$query_Otras = "SELECT i.user_id, i.status_id, i.date_send, c.newcode, c.course, s.start_date, l.living, c.newcode
						FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_livings l
						WHERE i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND s.living_id = l.living_id AND (s.start_date between '$fec_ini_comp' AND '$fec_fin_comp')
						ORDER BY i.user_id, s.start_date";
		$Rows_Otras = DB::query($query_Otras);
		//Consulta detalle programación

		for ($i = 0; $i < count($Rows_config); $i++) {
			$var_User = $Rows_config[$i]['user_id'];
			//Cargos del usuario
			for ($y = 0; $y < count($Rows_Charges); $y++) {
				if ($var_User == $Rows_Charges[$y]['user_id']) {
					$Rows_config[$i]['cargos'][] = $Rows_Charges[$y]['charge'];
				}
			}
			//Cargos del usuario
			//Notas del usuario
			for ($x = 0; $x < count($Rows_Notas); $x++) {
				if ($var_User == $Rows_Notas[$x]['user_id']) {
					$Rows_config[$i]['notas'][] = $Rows_Notas[$x];
				}
			}
			//Notas del usuario
			//Otras Asignaciones en el mes del usuario
			for ($v = 0; $v < count($Rows_Otras); $v++) {
				if ($var_User == $Rows_Otras[$v]['user_id']) {
					$Rows_config[$i]['otras'][] = $Rows_Otras[$v];
				}
			}
			//Otras Asignaciones en el mes del usuario
			$Inv_data = 0;
			//Invitaciones del usuario
			for ($z = 0; $z < count($Rows_Invit); $z++) {
				if ($var_User == $Rows_Invit[$z]['user_id']) {
					$Inv_data = $Rows_Invit[$z]['invitation_id'];
				}
			}
			//Invitaciones del usuario
			$Rows_config[$i]['YaenInvitacion'] = $Inv_data;
		}

		return $Rows_config;
	} //fin consultaUsuariosNoAsig
	/*
	Andres Vega
	2017-02-09
	Funcion que trae un array de correos
	*/
	public static function traerCorreos($user_id, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		// $query_sql = "SELECT u.email FROM ludus_users u, ludus_invitation i WHERE u.user_id = i.user_id AND i.schedule_id = $schedule AND u.status_id = 1 ";
		$query_sql = "SELECT u.email FROM ludus_users u WHERE u.user_id = $user_id AND u.status_id = 1 ";
		// echo( $query_sql );

		$Rows_config = DB::queryFirstRow($query_sql);

		return $Rows_config;
	}
	/*
	Andres Vega
	07/03/2017
	Funcion que retorna un texto de usuarios, que estan en el schedule y en el concesionario, separados por comas
	*/
	public static function consultaAsignados($schedule_id, $dealer_id_adm, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$usuarios = "0";
		$dealer_id = ($dealer_id_adm == "") ? $_SESSION['dealer_id'] : $dealer_id_adm;
		$query_sql = "SELECT i.user_id
			FROM ludus_invitation i, ludus_users u, ludus_headquarters h
			WHERE u.user_id = i.user_id
			AND u.headquarter_id = h.headquarter_id
			AND i.schedule_id = $schedule_id
			AND h.dealer_id = $dealer_id";

		$Rows_config = DB::query($query_sql);

		foreach ($Rows_config as $value => $usuario) {
			$usuarios .= "," . $usuario['user_id'];
		}

		return $usuarios;
	} //fin consultaAsignados
	/*
	*Andres Vega
	*07/03/2017
	*Funcion que trae los datos de los usuarios que ya estan registrados en el curso
	*/
	public static function listaUsuariosRegistrados($schedule_id, $dealer_id_adm, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$usuarios = Asignaciones::consultaAsignados($schedule_id, $dealer_id_adm);
		$dealer_id = ($dealer_id_adm == "") ? $_SESSION['dealer_id'] : $dealer_id_adm;
		$query_sql = "SELECT DISTINCT r.user_id, r.last_name, r.first_name, r.identification, r.image, h.headquarter, h. bac, h.address1, a.area
						FROM ludus_users r, ludus_headquarters h, ludus_areas a
						WHERE r.status_id = 1
						AND r.headquarter_id = h.headquarter_id
						AND h.area_id = a.area_id
						AND r.user_id IN ( $usuarios )
						ORDER BY a.area, r.first_name, r.last_name";
		$Rows_config = DB::query($query_sql);

		$query_Cargo = "SELECT c.charge, u.user_id FROM ludus_charges_users u, ludus_charges c WHERE u.charge_id = c.charge_id AND u.status_id = 1 AND u.user_id IN ($usuarios)";
		$Rows_Charges = DB::query($query_Cargo);

		$query_YaenInvitacion = "SELECT * FROM ludus_invitation WHERE schedule_id = $schedule_id ";
		$Rows_Invit = DB::query($query_YaenInvitacion);

		$query_Notas = "SELECT r.score, r.approval, r.date_creation, r.user_id
						FROM ludus_schedule s, ludus_modules_results_usr r, ludus_modules m
						WHERE s.schedule_id = '$schedule_id' AND s.course_id = m.course_id AND m.module_id = r.module_id AND r.user_id IN ( $usuarios ) ORDER BY r.score DESC";
		$Rows_Notas = DB::query($query_Notas);

		//Consulta detalle programación
		$query_sql_s = "SELECT s.start_date FROM ludus_schedule s WHERE s.schedule_id = '$schedule_id'";
		$Rows_config_s = DB::queryFirstRow($query_sql_s);

		$Start_Date_var = $Rows_config_s['start_date'];
		$month = substr($Start_Date_var, 5, 2);
		$year = substr($Start_Date_var, 0, 4);
		$fec_ini_comp = $year . '-' . $month . '-01';
		$fec_fin_comp = $year . '-' . $month . '-31';
		$query_Otras = "SELECT i.user_id, i.status_id, i.date_send, c.newcode, c.course, s.start_date, l.living, c.newcode
						FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_livings l
						WHERE i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND s.living_id = l.living_id AND (s.start_date between '$fec_ini_comp' AND '$fec_fin_comp')
						AND i.user_id IN ( $usuarios )
						ORDER BY i.user_id, s.start_date";
		$Rows_Otras = DB::query($query_Otras);
		//Consulta detalle programación

		for ($i = 0; $i < count($Rows_config); $i++) {
			$var_User = $Rows_config[$i]['user_id'];
			//Cargos del usuario
			for ($y = 0; $y < count($Rows_Charges); $y++) {
				if ($var_User == $Rows_Charges[$y]['user_id']) {
					$Rows_config[$i]['cargos'][] = $Rows_Charges[$y]['charge'];
				}
			}
			//Cargos del usuario
			//Notas del usuario
			for ($x = 0; $x < count($Rows_Notas); $x++) {
				if ($var_User == $Rows_Notas[$x]['user_id']) {
					$Rows_config[$i]['notas'][] = $Rows_Notas[$x];
				}
			}
			//Notas del usuario
			//Otras Asignaciones en el mes del usuario
			for ($v = 0; $v < count($Rows_Otras); $v++) {
				if ($var_User == $Rows_Otras[$v]['user_id']) {
					$Rows_config[$i]['otras'][] = $Rows_Otras[$v];
				}
			}
			//Otras Asignaciones en el mes del usuario
			$Inv_data = 0;
			//Invitaciones del usuario
			for ($z = 0; $z < count($Rows_Invit); $z++) {
				if ($var_User == $Rows_Invit[$z]['user_id']) {
					$Inv_data = $Rows_Invit[$z]['invitation_id'];
				}
			}
			//Invitaciones del usuario
			$Rows_config[$i]['YaenInvitacion'] = $Inv_data;
		}

		return $Rows_config;
	} //fin listaUsuariosRegistrados

	public static function finalizar_curso_MFR($schedule_id)
	{
		include_once('../../config/init_db.php');
		DB::$error_handler = false;
		DB::$throw_exception_on_error = true;
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$data = [];
		try {
			$query_sch = "SELECT * from ludus_schedule where schedule_id = $schedule_id;";
			$Rows_sch = DB::queryFirstRow($query_sch);
			$course_id = $Rows_sch['course_id']; // Id del curso
			$module_id = $Rows_sch['module_id']; // Id del modulo

			$query_aux = "SELECT * from ludus_invitation where schedule_id = $schedule_id -- and status_id = 1;";
			$Rows_inv = DB::query($query_aux);

			if (empty($Rows_inv)) {
				$data["Error"] = false;
				$data["Msj"] = "No hay usuarios invitados en la asignación";
				return $data;
			}

			foreach ($Rows_inv as $key => $value) {
				$user_id = $value['user_id'];
				$invitation_id = $value['invitation_id'];
				$verificaIns = DB::query("SELECT * FROM ludus_inscriptions WHERE user_id = $user_id AND module_id = $module_id AND course_id = $course_id AND schedule_id = $schedule_id");
				if (empty($verificaIns)) {  // Crear inscripción
					DB::query("INSERT INTO ludus_inscriptions(
											user_id,
											course_id,
											module_id,
											status_id,
											date_creation,
											schedule_id
										)
										VALUES(
											$user_id,
											$course_id,
											$module_id,
											1,
											NOW(), 
											$schedule_id
										)");
				}
				// asocial el id de la invitación
				$queryVerResult = "SELECT module_result_usr_id FROM ludus_modules_results_usr WHERE user_id = $user_id AND module_id = $module_id";
				$veriResult = DB::query($queryVerResult);
				if (empty($veriResult)) {
					// DB::query("DELETE FROM ludus_modules_results_usr WHERE module_result_usr_id IN ($queryVerResult)"); // Eliminar nota del usuario relacionada con el modulo
					DB::query("INSERT INTO ludus_modules_results_usr(
											score,
											user_id,
											module_id,
											approval,
											FILE,
											date_creation,
											status_id,
											creator,
											invitation_id
										)
										VALUES(
											0,
											$user_id,
											$module_id,
											'NO',
											'',
											NOW(), 
											1, 
											$idQuien, 
											$invitation_id
										)");
				}
			}

			DB::query("UPDATE ludus_invitation set status_id = 2, date_result = now(), editor = $idQuien where schedule_id = $schedule_id"); // Actualizar la asistencia de los usuario
			DB::query("UPDATE ludus_schedule SET finalizado = '1' WHERE schedule_id = $schedule_id"); // Cambiar el estado de la programación a finalizada
			$data['Error'] = false;
			$data['Msj'] = 'Curso cerrado correctamente';
		} catch (MeekroDBException $e) {
			$data['Error'] = true;
			$data['Msj'] = 'Error al cerrar el curso.';
			$data['MeekroDBException'] = $e->getMessage();
			$data['SQLQuery'] = $e->getQuery();
		}

		DB::$error_handler = 'meekrodb_error_handler';
		DB::$throw_exception_on_error = false;
		DB::disconnect();
		return $data;
	}
}
