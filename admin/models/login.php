<?php
include('../config/config.php');
@session_start();
if( isset($_SESSION['id_evaluacion']) && $_SESSION['id_evaluacion'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: evaluar.php");
}else if( isset($_SESSION['_EvalCour_ResultId']) && $_SESSION['_EvalCour_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: evaluar.php");
}else if( isset($_SESSION['_EncSat_ResultId']) && $_SESSION['_EncSat_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuestar.php");
}else if( isset($_SESSION['_EnCarg_ReviewId']) && $_SESSION['_EnCarg_ReviewId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuestar_Cargo.php");
}else if( isset($_SESSION['evl_obligatoria']) && $_SESSION['evl_obligatoria'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuesta_gerentes.php");
}elseif(isset($_SESSION['NameUsuario'])){
	if( isset($_SESSION['url_solicitada'] ) and !strpos($_SESSION['url_solicitada'], 'login') ){
		header("location: ".$_SESSION['url_solicitada']);
	}else{
		header("location: ../admin/");
	}
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<?php include('src/tittle.php'); ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../template/login/images/faviconAutotrain.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../template/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../template/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../template/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../template/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../template/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../template/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../template/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../template/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../template/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="../template/login/css/main.css">
<!--===============================================================================================-->
</head>
<style type="text/css">
	/*responsive*/
	@media only screen 
        and (min-device-width : 768px) 
        and (max-device-width : 1024px)  {
        .wrap-login100 {
            margin-left: 0px;
          }
      }

    @media only screen 
      and (min-device-width : 375px) 
      and (max-device-width : 667px) { 
        .wrap-login100 {
            margin-left: 0px;
        }
    }

    @media only screen 
    and (min-device-width : 414px) 
    and (max-device-width : 736px){ 
        .wrap-login100 {
            margin-left: 0px;
        }
    }

    @media only screen 
    and (min-device-width : 320px) 
    and (max-device-width : 568px)
    { 
        .wrap-login100 {
            margin-left: 0px;
        }
    }

    select:focus, input:focus{
    	outline: none;
	}
</style>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('../template/login/images/bg-01.jpg');">
			<div class="wrap-login100 p-b-100">
				<span class="login100-form-title p-b-41">
					<img src="../template/login/images/Logo.png" style="width: 100%;background: white;border-radius: 15px;">
				</span>
				<form action="controllers/usuarios.php" method="post" name="login" id="login" class="login100-form validate-form p-b-33 p-t-5">

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" id="username" placeholder="Usuario" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" id="password" placeholder="Contraseña" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>
					<br><input type="hidden" name="opcn" id="opcn" value="login">
					<!-- <div class="wrap-input100 validate-input" data-validate="Enter password">
						<select class="input100" style="background-color: transparent; border: 0px solid;">
							<option value="">Pais</option>
							<option value="colombia">Chile</option>
							<option value="colombia">Colombia</option>
							<option value="colombia">Paraguay</option>
							<option value="colombia">Uruguay</option>
							<option value="colombia">Peru</option>
							<option value="colombia">Bolivia</option>
						</select>
					</div> -->
					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Ingresar
						</button>
					</div>
					<br>
					<div style="text-align-last: center;" ><a id="btn-registrarme" style="color: white;" href="">Ingresar</a></div>
				</form>

				<form style="display: none;" method="post" name="frm-registrarme" id="frm-registrarme" class="login100-form validate-form p-b-33 p-t-5">

					<div class="wrap-input100">
						<input class="input100" type="text" name="identification" id="identification" placeholder="Ideitificacion" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100">
						<input class="input100" type="text" name="first_name" id="first_name" placeholder="Nombres" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100">
						<input class="input100" type="text" name="last_name" id="last_name" placeholder="Apellidos" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100">
						<input class="input100" type="text" name="email" id="email" placeholder="Correo" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100">
						<input class="input100" type="text" name="mobile_phone" id="mobile_phone" placeholder="Teléfono" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100">
						<input class="input100" type="date" name="date_birthday" id="date_birthday" placeholder="Fecha de nacimiento" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100">
						<select class="input100" style="background-color: transparent; border: 0px solid;">
							<option value="">Genero</option>
							<option value="colombia">Masculino</option>
							<option value="colombia">Femenino</option>
						</select>
					</div>
					<br><input type="hidden" name="opcn" value="registrarme">
					<!-- <div class="wrap-input100 validate-input" data-validate="Enter password">
						<select class="input100" style="background-color: transparent; border: 0px solid;">
							<option value="">Pais</option>
							<option value="colombia">Chile</option>
							<option value="colombia">Colombia</option>
							<option value="colombia">Paraguay</option>
							<option value="colombia">Uruguay</option>
							<option value="colombia">Peru</option>
							<option value="colombia">Bolivia</option>
						</select>
					</div> -->
					<div class="container-login100-form-btn m-t-32">
						<button id="registrarme" class="login100-form-btn">
							Registrarme
						</button>
					</div>
					<br>
					<div style="text-align-last: center;" ><a id="btn-ingresar" style="color: white;" href="">Ingresar</a></div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="../template/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../template/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../template/login/vendor/bootstrap/js/popper.js"></script>
	<script src="../template/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../template/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../template/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="../template/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../template/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../template/login/js/main.js"></script>
	<script>
		$('#btn-registrarme').click(function (e) { 
			e.preventDefault();
			$('#login').css('display', 'none');
			$('#frm-registrarme').fadeIn();
		});

		$('#btn-ingresar').click(function (e) { 
			e.preventDefault();
			$('#frm-registrarme').css('display', 'none');
			$('#login').fadeIn();
		});

		$('#registrarme').click(function (e) { 
			e.preventDefault();
			var data = new FormData();
          	var other_data = $('#frm-registrarme').serializeArray();
          	$.each(other_data,function(key,input){
              data.append(input.name,input.value);
          	});
		  
		
		$.ajax({
			url: 'controllers/usuarios.php',
			type: 'POST',
			dataType: 'JSON',
			data:         data,
            cache:        false,
            contentType:  false,
            processData:  false,
			data: data,
		})
		.done(function(data) {
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
			
			
		});
		
	</script>
</body>
<footer style="background-color: #1F2838; height: 7%; padding: 1%;">
	<img src="../template/login/images/chile.png" style="width: 20px; margin: 3px;">
	<img src="../template/login/images/colombia.png" style="width: 20px; margin: 3px;">
	<img src="../template/login/images/paraguay.png" style="width: 20px; margin: 3px;">
	<img src="../template/login/images/uruguay.png" style="width: 20px; margin: 3px;">
	<img src="../template/login/images/peru.png" style="width: 20px; margin: 3px;">
	<img src="../template/login/images/bolivia.png" style="width: 20px; margin: 3px;">
	<a href="https://luduscolombia.com.co/" target="_blank"><img src="../template/login/images/logoLudus.png" style="width:30px; margin: 3px; float: right;"></a>
	<a href="https://autotrain.com.co/" target="_blank"><img src="../template/login/images/LogoAutoTrain.png" style="width: 150px; margin: 3px; float: right;"></a>
</footer>
</html>