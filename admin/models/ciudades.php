<?php
Class Cargos {
	function consultaDatoscargos($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT a.area_id, a.area, a.status_id, a.zone_id, z.zone, s.status, a.status_id
		FROM ludus_areas a
			INNER JOIN ludus_zone z
				on a.zone_id = z.zone_id
			INNER JOIN ludus_status s
				on a.status_id = s.status_id";
			if($sWhere!=''){
				$query_sql .= " WHERE (a.area LIKE '%$sWhere%' OR s.status LIKE '%$sWhere%' OR z.zone LIKE '%$sWhere%' )";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT a.area_id, a.area, a.status_id, a.zone_id, z.zone, s.status, a.status_id
		FROM ludus_areas a
			INNER JOIN ludus_zone z
				on a.zone_id = z.zone_id
			INNER JOIN ludus_status s
				on a.status_id = s.status_id";
			if($sWhere!=''){
				$query_sql .= " WHERE (a.area LIKE '%$sWhere%' OR s.status LIKE '%$sWhere%' OR z.zone LIKE '%$sWhere%' )";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT a.area_id, a.area, a.status_id, a.zone_id, z.zone, s.status, a.status_id
		FROM ludus_areas a
			INNER JOIN ludus_zone z
				on a.zone_id = z.zone_id
			INNER JOIN ludus_status s
				on a.status_id = s.status_id
			WHERE  a.status_id = 1 ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearcargos($nombre, $zone_id){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_areas
								(
								area,
								zone_id,
								date_creation,
								status_id
								)
								VALUES
								(
								'$nombre',
								'$zone_id',
								now(),
								1
								);";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarcargos($id,$nombre, $zone, $status){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_areas SET area = '$nombre', status_id = '$status', zone_id = $zone WHERE area_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_areas a
			WHERE a.area_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function getDepartamentos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_zone;";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
