<?php
Class RepEvaluaciones {
	function consultaDatosAll($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		//$dealer_id = $_SESSION['dealer_id'];
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, r.review, r.code, a.score, a.available_score
					FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r
					WHERE r.type IN (1,3) AND u.user_id = '$idQuien' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id
					AND r.status_id = 1 AND r.date_edition < DATE(DATE_SUB(NOW(),INTERVAL 7 DAY))
					ORDER BY r.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_res = "SELECT a.reviews_answer_id, ad.result, q.question, q.code, ans.answer, ans.result as res_ans, q.question_id
					FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans
					WHERE a.user_id = '$idQuien' AND a.reviews_answer_id = ad.reviews_answer_id AND
					a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id";
		$Rows_res = $DataBase_Acciones->SQL_SelectMultipleRows($query_res);

		$query_correctas = "SELECT * FROM ludus_answers WHERE result = 'SI'";
		$Rows_correctas = $DataBase_Acciones->SQL_SelectMultipleRows($query_correctas);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_reviews_answer_id = $Rows_config[$i]['reviews_answer_id'];
			for($y=0;$y<count($Rows_res);$y++) {
				if($var_reviews_answer_id == $Rows_res[$y]['reviews_answer_id']){
					$question_id = $Rows_res[$y]['question_id'];
					for($z=0;$z<count($Rows_correctas);$z++){
						if($question_id==$Rows_correctas[$z]['question_id']){
							$Rows_res[$y]['correcta'] = $Rows_correctas[$z]['answer'];
						}
					}
					$Rows_config[$i]['respuestas'][] = $Rows_res[$y];
				}
			}

		}
		/*$invitations = "1";
		for($y=0;$y<count($Rows_detail);$y++) {
			$invitations .= ",".$Rows_detail[$y]['invitation_id'];
		}
		$schedules = "0";
		for($y=0;$y<count($Rows_config);$y++) {
			$schedules .= ",".$Rows_config[$y]['schedule_id'];
		}

		$query_nota = "SELECT i.invitation_id, i.approval, i.score
						FROM `ludus_modules_results_usr` i
						WHERE i.status_id = 1 AND i.invitation_id IN ($invitations)";
						//echo $query_nota;
		$Rows_notas = $DataBase_Acciones->SQL_SelectMultipleRows($query_nota);

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_asis = "SELECT s.schedule_id, d.dealer, s.min_size, s.max_size, s.inscriptions, d.dealer_id
			FROM ludus_dealer_schedule s, ludus_dealers d
			WHERE s.schedule_id IN ($schedules) AND s.dealer_id = d.dealer_id
			AND s.max_size > 0";
		}else{
			$query_asis = "SELECT s.schedule_id, d.dealer, s.min_size, s.max_size, s.inscriptions, d.dealer_id
			FROM ludus_dealer_schedule s, ludus_dealers d
			WHERE s.schedule_id IN ($schedules) AND s.dealer_id = d.dealer_id AND d.dealer_id = '$dealer_id'
			AND s.max_size > 0";
		}
		$Rows_asistencia = $DataBase_Acciones->SQL_SelectMultipleRows($query_asis);*/


		//print_r($Rows_config);
		/*for($i=0;$i<count($Rows_config);$i++) {
			$var_schedule_id = $Rows_config[$i]['schedule_id'];
			for($y=0;$y<count($Rows_detail);$y++) {
				if($var_schedule_id == $Rows_detail[$y]['schedule_id']){
					//print_r($Rows_config[$i]['resultados']);
					$var_Invitation = $Rows_detail[$y]['invitation_id'];
					for($z=0;$z<count($Rows_notas);$z++) {
						if($var_Invitation == $Rows_notas[$z]['invitation_id']){
							//print_r($Rows_notas[$y]);
							$Rows_detail[$y]['resultados'][] = $Rows_notas[$z];
							//print_r($Rows_config[$i]['resultados']);
						}
					}
					//print_r($Rows_detail[$y]);
					$Rows_config[$i]['detail'][] = $Rows_detail[$y];
				}
			}
			for($y=0;$y<count($Rows_asistencia);$y++) {
				if($var_schedule_id == $Rows_asistencia[$y]['schedule_id']){
					$Rows_config[$i]['asistencia'][] = $Rows_asistencia[$y];
				}
			}
		}*/

		/*for($i=0;$i<count($Rows_config);$i++) {
			$query_Resultado = "SELECT i.approval, i.score
						FROM `ludus_modules_results_usr` i
						WHERE i.invitation_id = ".$Rows_config[$i]['invitation_id']." AND i.status_id = 1 ";
			$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
			$Rows_config[$i]['resultados'] = $Rows_cant;
		}*/

		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
