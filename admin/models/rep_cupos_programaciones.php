<?php
Class RepCalificacion {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDatosAll($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		//@session_start();
		//$dealer_id = $_SESSION['dealer_id'];
		$query_sql ="
		SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, a.dealer, d.min_size AS puntoMin_size, d.max_size as puntoMax_size, d.inscriptions, c.newcode, c.course, c.type, m.module, m.duration_time, u.supplier
		FROM ludus_schedule s,ludus_dealer_schedule d, ludus_dealers a, ludus_courses c, ludus_modules m, ludus_supplier u
		WHERE s.schedule_id = d.schedule_id AND d.dealer_id = a.dealer_id AND s.course_id = c.course_id AND c.course_id= m.course_id AND c.supplier_id = u.supplier_id AND d.min_size > 0 AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
		ORDER BY s.start_date";
		//echo $query_sql;

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
