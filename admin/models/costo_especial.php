<?php
Class Costo_Especial {

	//====================================================================================
	//Consulta las sesiones especificas
	//====================================================================================
	public static function consulta_Schedule( $p, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT s.schedule_id, s.start_date, c.course, c.type, c.newcode, u.first_name, u.last_name, l.living, ct.city
			FROM ludus_schedule s, ludus_users u, ludus_livings l, ludus_courses c, ludus_cities ct
			WHERE s.teacher_id = u.user_id AND s.course_id = c.course_id AND s.living_id = l.living_id AND l.city_id = ct.city_id AND s.start_date BETWEEN '{$p['fecha_inicio']} 00:00:00' AND '{$p['fecha_fin']} 23:59:59' AND (c.course LIKE '%{$p['searchTerm']}%' OR c.type LIKE '%{$p['searchTerm']}%')
			ORDER BY s.start_date DESC";
		//print_r($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta fecha de la sesion especifica
	//====================================================================================
	public static function consulta_Fecha_Schedule( $schedule_id, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = " SELECT * FROM ludus_schedule WHERE schedule_id = '$schedule_id' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta concesionarios por sesion
	//====================================================================================
	public static function consulta_Dealer( $schedule_id, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT s.schedule_id, s.start_date, d.dealer_id, d.dealer, ds.inscriptions 
			FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s 
			WHERE s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id AND s.status_id = 1 AND d.status_id = 1 AND ds.inscriptions > 0 AND s.schedule_id = '$schedule_id' ";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta concesionarios por fecha
	//====================================================================================
	public static function consulta_All_Dealer( $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = " SELECT DISTINCT d.dealer_id, d.dealer 
			FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s, ludus_courses c, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM ludus_dealer_schedule GROUP BY schedule_id) as i 
			WHERE s.course_id = c.course_id AND c.type IN ('IBT','VCT','OJT') AND s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id AND s.status_id = 1 AND d.status_id = 1 AND d.category = 'CHEVROLET' AND s.schedule_id = i.schedule_id 
			ORDER BY d.dealer ";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta los extra costos
	//====================================================================================
	public static function consulta_Costo( $sWhere, $sOrder, $sLimit, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = " SELECT * FROM (SELECT ec.id_extra_cost, s.schedule_id, d.dealer_id, d.dealer, s.start_date, s.end_date, c.course, c.newcode, l.living, h.headquarter, u.first_name, u.last_name, ec.type, ec.cost_total, ec.date_cost, ec.status_id, ec.date_edition FROM ludus_extra_cost ec, ludus_schedule s, ludus_dealers d, ludus_users u, ludus_livings l, ludus_courses c, ludus_headquarters h WHERE ec.type = 1 AND ec.schedule_id = s.schedule_id AND s.course_id = c.course_id AND s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND ec.dealer_id = d.dealer_id AND ec.user_id = u.user_id UNION SELECT ec.id_extra_cost,'', d.dealer_id, d.dealer, '', '', '', '', '', '', u.first_name, u.last_name, ec.type, ec.cost_total, ec.date_cost, ec.status_id, ec.date_edition FROM ludus_extra_cost ec, ludus_dealers d, ludus_users u WHERE ec.type = 2 AND ec.dealer_id = d.dealer_id AND ec.user_id = u.user_id) AS c ";
		if($sWhere!=''){
			$query_sql .= " WHERE (c.dealer LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' OR c.schedule_id LIKE '%$sWhere%' OR c.headquarter LIKE '%$sWhere%' OR c.first_name LIKE '%$sWhere%' OR c.last_name LIKE '%$sWhere%' OR c.date_cost LIKE '%$sWhere%' )";
		}
		$query_sql .= $sOrder;
		$query_sql .= ' '.$sLimit;
		//echo $query_sql;
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	public static function consulta_Costo_CantidadFull($sWhere,$sOrder,$sLimit, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = " SELECT * FROM (SELECT ec.id_extra_cost, s.schedule_id, d.dealer_id, d.dealer, s.start_date, s.end_date, c.course, c.newcode, l.living, h.headquarter, u.first_name, u.last_name, ec.type, ec.cost_total, ec.date_cost, ec.status_id, ec.date_edition FROM ludus_extra_cost ec, ludus_schedule s, ludus_dealers d, ludus_users u, ludus_livings l, ludus_courses c, ludus_headquarters h WHERE ec.type = 1 AND ec.schedule_id = s.schedule_id AND s.course_id = c.course_id AND s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND ec.dealer_id = d.dealer_id AND ec.user_id = u.user_id UNION SELECT ec.id_extra_cost,'', d.dealer_id, d.dealer, '', '', '', '', '', '', u.first_name, u.last_name, ec.type, ec.cost_total, ec.date_cost, ec.status_id, ec.date_edition FROM ludus_extra_cost ec, ludus_dealers d, ludus_users u WHERE ec.type = 2 AND ec.dealer_id = d.dealer_id AND ec.user_id = u.user_id) AS c ";
		if($sWhere!=''){
			$query_sql .= " WHERE (c.dealer LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' OR c.schedule_id LIKE '%$sWhere%' OR c.headquarter LIKE '%$sWhere%' OR c.first_name LIKE '%$sWhere%' OR c.last_name LIKE '%$sWhere%' OR c.date_cost LIKE '%$sWhere%' )";
		}
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Crear el nuevo costo
	//====================================================================================
	public static function crear_Costo( $p, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();

		$insertSQL_EE = "INSERT INTO `ludus_extra_cost`
		(schedule_id,dealer_id,type,cost_total,date_cost,status_id,date_edition,user_id)
		VALUES ({$p['schedule_id']},{$p['dealer_id']},{$p['tipo']},{$p['tarifa']},'{$p['start_date_day']}','1','$NOW_data','$idQuien')";
		//echo $insertSQL_EE;
		$resultado = $DataBase_Log->SQL_Update($insertSQL_EE);

		unset($DataBase_Log);
		return $resultado;
	}

	//====================================================================================
	//Editar el extra costo
	//====================================================================================
	public static function actualizar_Costo( $p, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();

		$insertSQL_EE = "UPDATE ludus_extra_cost 
			SET schedule_id = {$p['schedule_id']}, 
				dealer_id = {$p['dealer_id']}, 
				type = {$p['tipo']}, 
				cost_total = {$p['tarifa']}, 
				date_cost = '{$p['start_date_day']}', 
				date_edition = '$NOW_data', 
				user_id = '$idQuien'
			WHERE id_extra_cost = {$p['id_cost']} ";
		//echo $insertSQL_EE;
		$resultado = $DataBase_Log->SQL_Update($insertSQL_EE);

		unset($DataBase_Log);
		return $resultado;
	}

	//====================================================================================
	//Consulta los extra costos
	//====================================================================================
	public static function consulta_Costo_Especial( $id, $prof ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = " SELECT * FROM (SELECT ec.id_extra_cost, s.schedule_id, d.dealer_id, d.dealer, s.start_date, s.end_date, c.course, c.newcode, l.living, h.headquarter, u.first_name, u.last_name, ec.type, ec.cost_total, ec.date_cost, ec.status_id, ec.date_edition FROM ludus_extra_cost ec, ludus_schedule s, ludus_dealers d, ludus_users u, ludus_livings l, ludus_courses c, ludus_headquarters h WHERE ec.type = 1 AND ec.schedule_id = s.schedule_id AND s.course_id = c.course_id AND s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND ec.dealer_id = d.dealer_id AND ec.user_id = u.user_id UNION SELECT ec.id_extra_cost,'', d.dealer_id, d.dealer, '', '', '', '', '', '', u.first_name, u.last_name, ec.type, ec.cost_total, ec.date_cost, ec.status_id, ec.date_edition FROM ludus_extra_cost ec, ludus_dealers d, ludus_users u WHERE ec.type = 2 AND ec.dealer_id = d.dealer_id AND ec.user_id = u.user_id) AS c WHERE c.id_extra_cost = '$id' ";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

}
