<?php  

class encuesta{

	public static function insertar($p){
		// extract($p);
		include('../../config/init_db.php');
		@session_start();

		$Nombre = ($_SESSION['NameUsuario']);
		// return;
	

	     $query = "INSERT INTO encuesta_covid19(name_user, nombreycontacto, mot_salida, opc_otros, lugar_a_trabajar, md_transporte, check1, check2, check3, check4, check5, check6, check7, check8, check9, check10, check11, check12, check13) 
			VALUES (
			 '$Nombre',
			'{$p['nombreycontacto']}',
			'{$p['mot_salida']}',
			'{$p['opc_otros']}',
			'{$p['lugar_a_trabajar']}',
			'{$p['md_transporte']}',
			'{$p['check1']}',
			'{$p['check2']}',
			'{$p['check3']}',
			'{$p['check4']}',
			'{$p['check5']}',
			'{$p['check6']}',
			'{$p['check7']}',
			'{$p['check8']}',
			'{$p['check9']}',
			'{$p['check10']}',
			'{$p['check11']}',
			'{$p['check12']}',
			'{$p['check13']}'
			)";

	     $resultSet = DB::query( $query );

          $respuesta = array();
         if ($resultSet) {
              $respuesta['error'] = false;
              $respuesta['mensaje'] = "Encuesta guardad con éxito.";
              encuesta::enviar_Email($Nombre);
          } else {
              $respuesta['error'] = true;
              $respuesta['mensaje'] = "La encuesta no ha sido guardada.";
          }  
              return $respuesta;

	}

	public static function consultar_Encuesta(){
		include('../../config/init_db.php');

		$query = "SELECT * FROM encuesta_covid19";
		$resp = DB::query($query);

		return $resp;
	}

	public static function graficos(){
		include('../../config/init_db.php');
		
		$result['Motivo_salida']=DB::query("SELECT 'Otros' as label, COUNT(*) as value FROM encuesta_covid19 							WHERE mot_salida = 'Otros'
										UNION 
										SELECT 'Trabajo Cat' as label, COUNT(*) as value FROM encuesta_covid19 WHERE mot_salida = 'Trabajo Cat'
										UNION
										SELECT 'Trabajo Taller de Servicio' as label, COUNT(*) as value FROM encuesta_covid19 WHERE mot_salida = 'Trabajo Taller de Servicio'
										UNION
										SELECT 'Trabajo Concesionario' as label, COUNT(*) as value FROM encuesta_covid19 WHERE mot_salida = 'Trabajo Concesionario'
										UNION
										SELECT 'Live Store' as label, COUNT(*) as value FROM encuesta_covid19 WHERE mot_salida = 'Live Store'
										UNION
										SELECT 'Trabajo Publicidad' as label, COUNT(*) as value FROM encuesta_covid19 WHERE mot_salida = 'Trabajo Publicidad'
										UNION
										SELECT 'Trabajo en sede AutoTrain' as label, COUNT(*) as value FROM encuesta_covid19 WHERE mot_salida = 'Trabajo en sede AutoTrain'

										");
		$result['md_transporte']= DB::query("SELECT 'Carro propio' as label, COUNT(*) as value FROM encuesta_covid19 WHERE md_transporte = 'Carro propio' UNION SELECT 'Moto' as label, COUNT(*) as value FROM encuesta_covid19 WHERE md_transporte = 'Moto' UNION SELECT 'Bicicleta' as label, COUNT(*) as value FROM encuesta_covid19 WHERE md_transporte = 'Bicicleta' UNION SELECT 'Servicio_Publico' as label, COUNT(*) as value FROM encuesta_covid19 WHERE md_transporte = 'Servicio_Publico' UNION SELECT 'A pie' as label, COUNT(*) as value FROM encuesta_covid19 WHERE md_transporte = 'A pie'");
		
		 $result['check1']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check1 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check1 = 'NO'");
		 $result['check2']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check2 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check2 = 'NO'");
		 $result['check3']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check3 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check3 = 'NO'");
		 $result['check4']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check4 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check4 = 'NO'");
		 $result['check5']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check5= 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check5 = 'NO'");
		 $result['check6']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check6 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check6 = 'NO'");
		 $result['check7']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check7 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check7 = 'NO'");
		 $result['check8']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check8 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check8 = 'NO'");
		 $result['check9']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check9 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check9 = 'NO'");
		 $result['check10']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check10 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check10 = 'NO'");
		 $result['check11']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check11 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check11 = 'NO'");
		 $result['check12']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check12 = 'SI' UNION SELECT 'NO' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check12 = 'NO'");
		 $result['check13']= DB::query("SELECT 'SI' as label, COUNT(*) as value FROM encuesta_covid19 WHERE check13 = 'SI'");


		return $result;

	}

	 public static function enviar_Email($Nombre){
	 			
				$to = "sgsst@autotrain.com";
				$subject = "Encuesta Sintomatologia COVID AutoTrain";
				$headers = "MIME-Version: 1.0" . "\r\n"; 
				$headers = "From: colombia@autotrain.com" . "\r\n" . "Content-type:text/html;charset=UTF-8";
				$nombre = strtolower($Nombre);
				$message = "
				<html>
				<head>
					<meta charset='utf-8'>
					<meta http-equiv='X-UA-Compatible' content='IE=edge'>
					<title>AutoTrain</title>
				</head>
				<body>
				 <div id = 'contenido' style='color:black'>
					<div id='imagen de cabecera' style='text-align: center;'>
						<img src='https://autotrainacademy.com/assets/images/distinciones/Logo_AutoTrain_Academy_32.png' alt=''>
					</div>
						<div id='contenedor' style='text-align: center;'>
							
							<h2>Bienvenido/a al mejor espacio de apendizaje de AutoTrain</h2>
							<p style='text-transform:capitalize;font-size:16px'>El usuario $nombre, ha contestado la encuesta COVID-19</p>
						</div>
				    </div>
					    <footer>
						    <div style='text-align: center;'>
						    	<p>Por favor no responder a este correo.</p>
								    <a href='https://autotrainacademy.com/' target='_blank'> Para m&aacute;s informaci&oacute;n ingresa a nuestra plataforma AutoTrain, Click Aqui!</a>
						    </div>
						</footer>
				</body>
				</html> ";
				 //print_r($message);
				   mail($to, $subject, $message, $headers);
        }

}

