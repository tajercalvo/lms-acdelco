<?php
/* namespace Models\Usuarios; */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Usuarios
{
	function login($username, $password)
	{
		

		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT u.*, h.dealer_id, d.dealer
		FROM ludus_users u, ludus_headquarters h, ludus_dealers d
		WHERE u.identification = '" . $username . "' AND u.password = AES_ENCRYPT('" . $password . "','" . $var_key_bd . "')
		AND u.status_id = '1' AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id";
		
		

		$DataBase_Class = new Database();
		$row_consulta = $DataBase_Class->SQL_SelectRows($query_sql);
		
		$totalRows_consulta = $DataBase_Class->SQL_SelectCantidad($query_sql);

		if ($totalRows_consulta > 0) {
			//session_set_cookie_params('3600');
			$_SESSION['NameUsuario'] 	= strtoupper($row_consulta['first_name'] . " " . $row_consulta['last_name']);
			$_SESSION['idUsuario'] 		= $row_consulta['user_id'];
			/*Consulta los roles del usuario*/
			$query_permisos = "SELECT ru.*, ro.level as nivel FROM ludus_roles_users ru, ludus_roles ro WHERE ru.user_id = " . $row_consulta['user_id'] . " AND ru.status_id = 1 AND ru.rol_id = ro.rol_id AND ro.status_id = 1";
			$query_maxrol = "SELECT MAX(ro.level) as nivel FROM ludus_roles_users ru, ludus_roles ro WHERE ru.user_id = " . $row_consulta['user_id'] . " AND ru.status_id = 1 AND ru.rol_id = ro.rol_id AND ro.status_id = 1";
			$query_sql .= " ORDER BY u.apellidos, u.nombres";
			$Rows_roles = $DataBase_Class->SQL_SelectMultipleRows($query_permisos);
			$Rows_max_rol = $DataBase_Class->SQL_SelectRows($query_maxrol);
			$_SESSION['id_rol'] 		= $Rows_roles;
			$_SESSION['max_rol']		= $Rows_max_rol['nivel'];
			$_SESSION['identification']		= $row_consulta['identification'];
			$_SESSION['gender'] = $row_consulta['gender'];
			/*Consulta los roles del usuario*/
			$_SESSION['imagen'] 		= $row_consulta['image'];
			$_SESSION['eMailUsuario'] 	= $row_consulta['email'];
			$_SESSION['dealer_id'] 		= $row_consulta['dealer_id'];
			$_SESSION['dealer'] 		= $row_consulta['dealer'];
			$_SESSION['pais'] 			= 'cl';
			$_SESSION["error"] = "";
			//agregar el id del cargo a la variable de _SESSION
			$query_sql = "SELECT charge_id FROM ludus_charges_users where user_id = " . $row_consulta['user_id'];
			$charge_id = $DataBase_Class->SQL_SelectRows($query_sql);
			$_SESSION['charge_id'] = $charge_id['charge_id'];
			//agregar el id del cargo a la variable de _SESSION

			/*Ingresa datos en log de acceso*/
			$idQuien = $_SESSION['idUsuario'];
			$insertSQL_EE = "INSERT INTO ludus_log (user_id,element_id,type,message,date_creation) VALUES ('$idQuien','$idQuien','1','Ingresó al Sistema','$NOW_data')";

			$resultado = $DataBase_Class->SQL_Insert($insertSQL_EE);
			/*Ingresa datos en log de acceso*/
		} else {
			// echo($totalRows_consulta); 
			// print_r($_SESSION["error"]);
		return;
			$_SESSION["error"] = "Usuario " . $username . " no existe, debe registrarse.";
			// die();
		}
		unset($DataBase_Class);
		return $totalRows_consulta;
	}
	function registraLogSalida($idQuien, $mensaje)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$insertSQL_EE = "INSERT INTO ludus_log (user_id,element_id,type,message,date_creation) VALUES ('$idQuien','$idQuien','1','$mensaje','$NOW_data')";
		$DataBase_Log = new Database();
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
	}
	function ConsultaEncuestaObl_Ger()
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Class = new Database();
		/*Consulta si tiene evaluaciones o encuestas pendientes por responder*/
		unset($_SESSION['evl_obligatoria']);
		if ($idQuien == 11021) {
			$query_evaluacion = "SELECT COUNT(*) as cantidad FROM ludus_charges_users c WHERE c.user_id = '$idQuien' AND c.user_id NOT IN (SELECT user_id FROM ludus_encuesta_ger_det)";
			$Row_id_evaluacion = $DataBase_Class->SQL_SelectRows($query_evaluacion);
			if ($Row_id_evaluacion['cantidad'] > 0) {
				$_SESSION['evl_obligatoria'] = $Row_id_evaluacion['cantidad'];
			}
		}
		/*Consulta si tiene evaluaciones o encuestas pendientes por responder*/
		unset($DataBase_Class);
	}
	function ConsultaEvaluacionCargo()
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Class = new Database();
		/*Consulta si tiene evaluaciones o encuestas pendientes por responder*/
		$query_evaluacion = "SELECT DISTINCT rc.review_id, r.review
				FROM ludus_charges_users cu, ludus_reviews_charges rc, ludus_reviews r
				WHERE cu.user_id = '$idQuien' AND cu.charge_id = rc.charge_id AND rc.review_id = r.review_id AND r.status_id = 1 AND r.type = 1
				AND rc.review_id NOT IN (SELECT review_id FROM ludus_reviews_answer WHERE user_id = '$idQuien')
				LIMIT 0,1";
		$Row_id_evaluacion = 0;
		$Row_id_evaluacion = $DataBase_Class->SQL_SelectRows($query_evaluacion);
		$_SESSION['id_evaluacion'] = $Row_id_evaluacion['review_id'];
		$_SESSION['name_evaluacion'] = $Row_id_evaluacion['review'];
		/*Consulta si tiene evaluaciones o encuestas pendientes por responder*/
		unset($DataBase_Class);
	}
	function ConsultaEvaluacionCurso()
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		include_once('evaluar.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Class = new Database();
		$evNoTime = evaluarlos::evaluacionesNoTime($idQuien);
		//$fecha = date('Y-m-j');
		/*$nuevafecha_8 = strtotime('-10 day',strtotime($fecha));
		$nuevafecha_7 = strtotime('-7 day',strtotime($fecha));
		$nuevafecha_8 = date('Y-m-j',$nuevafecha_8)." 00:00:00";
		$nuevafecha_7 = date('Y-m-j',$nuevafecha_7)." 23:59:59";*/
		$fecha = date('Y-m-j H:i:s');
		$nuevafecha_C = strtotime('-45 day', strtotime($fecha));
		$nuevafecha_8 = strtotime('-14 day', strtotime($fecha)); // 12 días originalmente
		$nuevafecha_7 = strtotime('-8 day', strtotime($fecha));
		/*$nuevafecha_8 = date('Y-m-j H:i:s',$nuevafecha_8);
	    $nuevafecha_7 = date('Y-m-j H:i:s',$nuevafecha_7);*/
		$nuevafecha_C = date('Y-m-j', $nuevafecha_C) . ' 00:00:01';
		$nuevafecha_8 = date('Y-m-j', $nuevafecha_8) . ' 00:00:01';
		$nuevafecha_7 = date('Y-m-j', $nuevafecha_7) . ' 23:59:59';
		/*Consulta si tiene evaluaciones o encuestas pendientes por responder*/
		if (isset($evNoTime['review_notime_id'])) {
			$query_evaluacion = "SELECT rc.review_id, co.course_id, mo.module_id, rw.review, rw.code, mrw.module_result_usr_id, mrw.score, mrw.date_creation
						FROM ludus_reviews_courses rc, ludus_courses co, ludus_modules mo, ludus_reviews rw, ludus_modules_results_usr mrw
						WHERE rw.status_id = 1 AND rc.course_id = co.course_id AND co.course_id = mo.course_id AND mo.module_id IN
						(SELECT mru.module_id
							FROM ludus_modules_results_usr mru, ludus_invitation inv, ludus_schedule sc
							WHERE mru.user_id = '$idQuien' AND mru.invitation_id = inv.invitation_id AND inv.status_id = '2'
							AND inv.schedule_id = sc.schedule_id AND sc.end_date BETWEEN '$nuevafecha_C' AND '$nuevafecha_7'
							AND mru.module_result_usr_id
							NOT IN (SELECT ra.module_result_usr_id FROM ludus_reviews_answer ra WHERE ra.user_id = '$idQuien' AND ra.review_id NOT IN (180,62,63,64,65) ))
						AND rw.status_id = 1 AND rc.review_id = rw.review_id AND rw.type = '3' AND mrw.user_id = '$idQuien' AND mrw.module_id = mo.module_id
						ORDER BY mrw.module_result_usr_id DESC
						LIMIT 0,1";
		} else {
			$query_evaluacion = "SELECT rc.review_id, co.course_id, mo.module_id, rw.review, rw.code, mrw.module_result_usr_id, mrw.score, mrw.date_creation
						FROM ludus_reviews_courses rc, ludus_courses co, ludus_modules mo, ludus_reviews rw, ludus_modules_results_usr mrw
						WHERE rw.status_id = 1 AND rc.course_id = co.course_id AND co.course_id = mo.course_id AND mo.module_id IN
						(SELECT mru.module_id
							FROM ludus_modules_results_usr mru, ludus_invitation inv, ludus_schedule sc
							WHERE mru.user_id = '$idQuien' AND mru.invitation_id = inv.invitation_id AND inv.status_id = '2'
							AND inv.schedule_id = sc.schedule_id AND sc.end_date BETWEEN '$nuevafecha_8' AND '$nuevafecha_7'
							AND mru.module_result_usr_id
							NOT IN (SELECT ra.module_result_usr_id FROM ludus_reviews_answer ra WHERE ra.user_id = '$idQuien' AND ra.review_id NOT IN (180,62,63,64,65)))
						AND rw.status_id = 1 AND rc.review_id = rw.review_id AND rw.type = '3' AND mrw.user_id = '$idQuien' AND mrw.module_id = mo.module_id
						ORDER BY mrw.module_result_usr_id DESC
						LIMIT 0,1";
		}
		// echo($query_evaluacion);
		$Row_id_evaluacion = 0;
		$Row_id_evaluacion = $DataBase_Class->SQL_SelectRows($query_evaluacion);
		$_SESSION['_EvalCour_id_evaluacion'] = $Row_id_evaluacion['review_id'];
		$_SESSION['_EvalCour_ResultId'] = $Row_id_evaluacion['module_result_usr_id'];
		$_SESSION['_EvalCour_Name'] = $Row_id_evaluacion['review'];
		$_SESSION['_EvalCour_Date'] = $Row_id_evaluacion['date_creation'];
		$_SESSION['_EvalCour_Score'] = $Row_id_evaluacion['score'];
		/*Consulta si tiene evaluaciones o encuestas pendientes por responder*/
		unset($DataBase_Class);
	}
	function ConsultaEncuestasSatIBTVCT()
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Class = new Database();
		//$date_ctrl = date('Y-m-d', strtotime('-8 day', strtotime(date("Ymd")))); mru.date_creation > '$date_ctrl 00:00:00' AND
		/*Consulta si tiene encuesta por cargo para responder*/
		$query_encSatistaccion = "SELECT mru.module_result_usr_id, m.module, m.type, mru.date_creation as date_nota, mru.score, mru.approval, u.first_name, u.last_name, u.identification, s.start_date
				FROM ludus_modules_results_usr mru, ludus_modules m, ludus_invitation i, ludus_schedule s, ludus_users u
				WHERE mru.user_id = '$idQuien' AND mru.score >= 0 AND mru.date_creation > '2018-01-01' AND mru.module_id = m.module_id AND m.type IN  ('IBT','VCT','OJT')
				AND mru.module_result_usr_id NOT IN (SELECT module_result_usr_id FROM ludus_reviews_answer WHERE review_id IN (180,62,63,64,65) AND user_id = '$idQuien' AND module_result_usr_id > 0)
				AND mru.invitation_id = i.invitation_id AND i.schedule_id = s.schedule_id AND s.teacher_id = u.user_id
				ORDER BY mru.date_creation ASC LIMIT 0,1";
		//echo($query_encSatistaccion);
		$Row_Encuesta = $DataBase_Class->SQL_SelectRows($query_encSatistaccion);
		if (isset($Row_Encuesta['module_result_usr_id']) && $Row_Encuesta['module_result_usr_id'] > 0) {
			$_SESSION['_EncSat_ResultId'] = $Row_Encuesta['module_result_usr_id'];
			$_SESSION['_EncSat_Name'] = $Row_Encuesta['module'];
			$_SESSION['_EncSat_Date'] = $Row_Encuesta['date_nota'];
			$_SESSION['_EncSat_Score'] = $Row_Encuesta['score'];
			$_SESSION['_EncSat_Teach'] = $Row_Encuesta['first_name'] . ' ' . $Row_Encuesta['last_name'];
			$_SESSION['_EncSat_FecSt'] = $Row_Encuesta['start_date'];
			$_SESSION['_EncSat_Type'] = $Row_Encuesta['type'];
		}
		/*Consulta si tiene encuesta por cargo para responder*/
		unset($DataBase_Log);
	}
	function ConsultaEncuestasSatWBT()
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Class = new Database();
		//$date_ctrl = date('Y-m-d', strtotime('-8 day', strtotime(date("Ymd")))); mru.date_creation > '$date_ctrl 00:00:00' AND
		/*Consulta si tiene encuesta por cargo para responder*/
		$query_encSatistaccion = "SELECT mru.module_result_usr_id, m.module, m.type, mru.date_creation as date_nota, mru.score, mru.approval
				FROM ludus_modules_results_usr mru, ludus_modules m
				WHERE mru.user_id = '$idQuien' AND mru.score >= 0 AND mru.date_creation > '2018-01-01' AND mru.module_id = m.module_id AND m.type IN  ('WBT')
				AND mru.module_result_usr_id NOT IN (SELECT module_result_usr_id FROM ludus_reviews_answer WHERE review_id IN (180,62,63,64,65) AND user_id = '$idQuien' AND module_result_usr_id > 0)
				ORDER BY mru.date_creation ASC LIMIT 0,1";
		$Row_Encuesta = $DataBase_Class->SQL_SelectRows($query_encSatistaccion);
		// if(isset($Row_Encuesta['module_result_usr_id']) && $Row_Encuesta['module_result_usr_id']>0){
		// 	$_SESSION['_EncSat_ResultId'] = $Row_Encuesta['module_result_usr_id'];
		// 	$_SESSION['_EncSat_Name'] = $Row_Encuesta['module'];
		// 	$_SESSION['_EncSat_Date'] = $Row_Encuesta['date_nota'];
		// 	$_SESSION['_EncSat_Score'] = $Row_Encuesta['score'];
		// 	$_SESSION['_EncSat_Teach'] = 'Capacitación autodirigida';
		// 	$_SESSION['_EncSat_FecSt'] = $Row_Encuesta['score'];
		// 	$_SESSION['_EncSat_Type'] = $Row_Encuesta['type'];
		// }
		// echo 'wbt';
		// echo '<pre>';
		//             print_r($_SESSION);
		//             echo '</pre>';
		//             die();
		/*Consulta si tiene encuesta por cargo para responder*/
		unset($DataBase_Log);
	}
	function ConsultaEncuestasUsrPorCargo()
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Class = new Database();
		/*Consulta si tiene encuesta por cargo para responder*/
		$query_encSatistaccion = "SELECT r.review, r.review_id, r.date_edition, r.description
						FROM ludus_reviews r, ludus_reviews_charges rc, ludus_charges_users cu
						WHERE r.type = '2' AND r.status_id = 1 AND r.review_id = rc.review_id AND rc.charge_id = cu.charge_id AND cu.user_id = '$idQuien'
						AND r.review_id NOT IN (SELECT review_id FROM ludus_reviews_answer WHERE user_id = '$idQuien' )";
		$Row_Encuesta = $DataBase_Class->SQL_SelectRows($query_encSatistaccion);
		$_SESSION['_EnCarg_ReviewId'] = $Row_Encuesta['review_id'];
		$_SESSION['_EnCarg_Name'] = $Row_Encuesta['review'];
		$_SESSION['_EnCarg_Date'] = $Row_Encuesta['date_edition'];
		$_SESSION['_EnCarg_Desc'] = $Row_Encuesta['description'];
		/*Consulta si tiene encuesta por cargo para responder*/
		unset($DataBase_Log);
	}
	function consultaDatosUsuario($idUsuario, $prof = "")
	{
		include_once($prof . '../config/database.php');
		include_once($prof . '../config/config.php');
		$query_sql = "SELECT u.*, headquarter, type_user
						FROM ludus_users u, ludus_headquarters c, ludus_type_user t
						WHERE u.user_id = '$idUsuario' AND u.headquarter_id = c.headquarter_id AND u.type_user_id = t.type_user_id";
		$DataBase_Class = new Database();
		$row_consulta = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $row_consulta;
	}
	function LastNav($idUsuario)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT MAX(date_navigation) as maxnav FROM ludus_navigation WHERE user_id = '$idUsuario' ";
		//echo($query_sql);
		$DataBase_Class = new Database();
		$row_consulta = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $row_consulta;
	}
	function consultaUsuarioSel($identificacion)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT u.*, headquarter
						FROM ludus_users u, ludus_headquarters c
						WHERE u.identification = '$identificacion' AND u.headquarter_id = c.headquarter_id";
		$DataBase_Class = new Database();
		$row_consulta = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $row_consulta;
	}
	function consultaDatosUsuario_By($campo, $valor)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT u.*
						FROM ludus_users u
						WHERE u." . $campo . " = '$valor'";
		$DataBase_Class = new Database();
		$row_consulta = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $row_consulta;
	}
	function consultaDatosCargosJS($idUsuario)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT u.charge_id,
		                     c.charge,
							 cc.category
						FROM ludus_charges_users u,
						     ludus_charges c,
							 ludus_charges_categories cc
						WHERE c.category_id = cc.category_id AND
						      u.user_id = '$idUsuario' AND
							  u.charge_id = c.charge_id AND
							  u.status_id = 1 AND
							  c.status_id = 1
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaDatosCargosJSL($idUsuario)
	{
		include_once('config/database.php');
		include_once('config/config.php');
		$query_sql = "SELECT u.charge_id, c.charge, cc.category
						FROM ludus_charges_users u, ludus_charges c, ludus_charges_categories cc
						WHERE c.category_id = cc.category_id AND u.user_id = '$idUsuario' AND u.charge_id = c.charge_id AND u.status_id = 1 AND c.status_id = 1
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaDatosCargos($idUsuario, $prof = "")
	{
		include_once($prof . '../config/database.php');
		include_once($prof . '../config/config.php');
		$query_sql = "SELECT u.charge_id,
		                     c.charge,
							 cc.category
						FROM ludus_charges_users u,
						ludus_charges c,
						ludus_charges_categories cc
						WHERE c.category_id = cc.category_id AND
						      u.user_id = '$idUsuario' AND
							  u.charge_id = c.charge_id AND
							  u.status_id = 1 AND 
							  c.status_id = 1 AND
							  u.charge_id != 5
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaTrayectorias($prof = "")
	{
		include_once($prof . '../config/database.php');
		include_once($prof . '../config/config.php');
		$query_sql = "SELECT charge_id,charge 
		                     FROM ludus_charges 
							 WHERE status_id = 1
						     ORDER BY charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function ListarTrayectorias()
	{
		include_once("../../config/init_db.php");
		DB::$encoding = 'utf8';
		$resultado = DB::query("SELECT charge_id,charge 
								FROM ludus_charges 
								WHERE status_id = 1
								ORDER BY charge");

		return $resultado;
	}
	function consultaDatosRoles($idUsuario)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.rol_id, c.rol
						FROM ludus_roles_users u, ludus_roles c
						WHERE u.user_id = '$idUsuario' AND u.rol_id = c.rol_id AND u.status_id = 1 AND c.status_id = 1
						ORDER BY c.rol";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaAllDatosCargos($prof, $status_id)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		if (!empty($status_id)) {
			$status = "c.status_id = $status_id";
		} else {
			$status = "c.status_id > 0";
		}
		$query_sql = "SELECT c.charge_id, c.charge
						FROM ludus_charges c
						WHERE $status
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaAllDatosRoles($prof, $status_id)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		if ($status_id != '') {
			$status = "c.status_id = '$status_id'";
		} else {
			$status = "c.status_id > 0";
		}
		$query_sql = "SELECT c.rol_id, c.rol
						FROM ludus_roles c
						WHERE $status
						ORDER BY c.rol";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaAllDatosConcesionarios($prof)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.headquarter_id, c.headquarter, c.address1, c.dealer_id
						FROM ludus_headquarters c
						WHERE c.status_id >= 1 ";
		@session_start();
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "3") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "4") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		$query_sql .= "ORDER BY c.headquarter ASC";
		//echo $query_sql;
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaAllDatosConcesionariosAll($prof)
	{
		@session_start();
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.dealer_id, c.dealer
						FROM ludus_dealers c
						WHERE c.status_id = 1 ";
		if (isset($_SESSION['max_rol']) && ($_SESSION['max_rol'] == "3" || $_SESSION['max_rol'] == "4")) {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		$query_sql .= "ORDER BY c.dealer";
		//echo $query_sql;
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function cambiaContrasena($idUsuario, $contrasena)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($var_key_bd)) {
			$var_key_bd	= 'LdLmsDoorGM_2015*';
			$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		}
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_log (user_id,element_id,type,message,date_creation) VALUES ('$idUsuario','$idUsuario','1','Cambio su contraseña','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		$updateSQL_ER = "UPDATE ludus_users SET password = AES_ENCRYPT('" . $contrasena . "','" . $var_key_bd . "') WHERE user_id = '$idUsuario'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function actualizaImagen($idUsuario, $imagen)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_log (user_id,element_id,type,message,date_creation) VALUES ('$idQuien','$idQuien','1','Se cambio la imagen del usuario [<a href=ver_perfil.php?id=$idUsuario&back=inicio>$idUsuario</a>]','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		$updateSQL_ER = "UPDATE ludus_users SET image = '$imagen' WHERE user_id = '$idUsuario'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function ProcesoMasivo($action, $identification_list)
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		if ($action == "Inactivar") {
			$updateSQL_ER = "UPDATE ludus_users SET status_id = '2', editor='$idQuien', date_edition=NOW() WHERE identification IN ($identification_list) ";
			// $updateSQL_ER = "UPDATE ludus_users SET status_id = '2', headquarter_id = '114', editor='$idQuien', date_edition=NOW() WHERE identification IN ($identification_list) ";
		} else {
			$updateSQL_ER = "UPDATE ludus_users SET status_id = '1', editor='$idQuien', date_edition=NOW() WHERE identification  IN ($identification_list) ";
		}

		// echo( $updateSQL_ER );
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		$insertSQL_EE = "INSERT INTO ludus_log (user_id,element_id,type,message,date_creation) VALUES ('$idQuien','$idQuien','1','Realizo un proceso masivo de $action, sobre los usuarios: $identification_list',NOW())";
		// echo("<br>$insertSQL_EE");
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
	function consultaCantidad($prof)
	{
		@session_start();
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT u.*, c.headquarter
						FROM ludus_users u, ludus_headquarters c
						WHERE u.headquarter_id = c.headquarter_id "; //LIMIT 0,20
		if (isset($_SESSION['charge_id_usr_flr']) && ($_SESSION['charge_id_usr_flr'] != "0")) {
			$charge_id = $_SESSION['charge_id_usr_flr'];
			$query_sql .= " AND u.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id = $charge_id) ";
		}
		if (isset($_SESSION['rol_id_usr_flr']) && ($_SESSION['rol_id_usr_flr'] != "0")) {
			$rol_id = $_SESSION['rol_id_usr_flr'];
			$query_sql .= " AND u.user_id  IN (SELECT user_id FROM ludus_roles_users WHERE rol_id = $rol_id) ";
		}
		if (isset($_SESSION['status_id_usr_flr']) && ($_SESSION['status_id_usr_flr'] != "0")) {
			$status_id = $_SESSION['status_id_usr_flr'];
			$query_sql .= " AND u.status_id ='$status_id' ";
		}
		if (isset($_SESSION['headquarter_id_usr_flr']) && ($_SESSION['headquarter_id_usr_flr'] != "0")) {
			$headquarter_id = $_SESSION['headquarter_id_usr_flr'];
			$query_sql .= " AND u.headquarter_id = $headquarter_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "3") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "4") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		if (isset($_SESSION['type_user_id_usr_flr']) && ($_SESSION['type_user_id_usr_flr'] != "0")) {
			$type_user_id = $_SESSION['type_user_id_usr_flr'];
			$query_sql .= " AND u.type_user_id = $type_user_id ";
		}
		if (isset($_SESSION['dealer_id_usr_flr']) && ($_SESSION['dealer_id_usr_flr'] != "0")) {
			$dealer_id = $_SESSION['dealer_id_usr_flr'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['no_time_ev_usr_flr']) && ($_SESSION['no_time_ev_usr_flr'] != "0")) {
			$query_sql .= " AND u.user_id IN ( SELECT rn.user_id FROM ludus_reviews_notime rn WHERE rn.status_id = 1 ) ";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadIndex()
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.*, headquarter
						FROM ludus_users u, ludus_headquarters c
						WHERE u.headquarter_id = c.headquarter_id "; //LIMIT 0,20
		@session_start();
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "3") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "4") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatosUsuariosIndex()
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.*, c.headquarter, s.status
						FROM ludus_users u, ludus_headquarters c, ludus_status s
						WHERE u.headquarter_id = c.headquarter_id AND u.status_id = s.status_id
						";
		@session_start();
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "3") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "4") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		$query_sql .= " ORDER BY u.image DESC LIMIT 0,5";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatosUsuarios($sWhere, $sOrder, $sLimit)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT u.*, c.headquarter, s.status, d.dealer
						FROM ludus_users u, ludus_headquarters c, ludus_status s, ludus_dealers d
						WHERE u.headquarter_id = c.headquarter_id AND u.status_id = s.status_id AND c.dealer_id = d.dealer_id ";
		session_start();
		if (isset($_SESSION['charge_id_usr_flr']) && ($_SESSION['charge_id_usr_flr'] != "0")) {
			$charge_id = $_SESSION['charge_id_usr_flr'];
			$query_sql .= " AND u.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id = $charge_id) ";
		}
		if (isset($_SESSION['rol_id_usr_flr']) && ($_SESSION['rol_id_usr_flr'] != "0")) {
			$rol_id = $_SESSION['rol_id_usr_flr'];
			$query_sql .= " AND u.user_id  IN (SELECT user_id FROM ludus_roles_users WHERE rol_id = $rol_id) ";
		}
		if (isset($_SESSION['status_id_usr_flr']) && ($_SESSION['status_id_usr_flr'] != "0")) {
			$status_id = $_SESSION['status_id_usr_flr'];
			$query_sql .= " AND u.status_id ='$status_id' ";
		}
		if (isset($_SESSION['headquarter_id_usr_flr']) && ($_SESSION['headquarter_id_usr_flr'] != "0")) {
			$headquarter_id = $_SESSION['headquarter_id_usr_flr'];
			$query_sql .= " AND u.headquarter_id = $headquarter_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "3") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "4") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		if (isset($_SESSION['type_user_id_usr_flr']) && ($_SESSION['type_user_id_usr_flr'] != "0")) {
			$type_user_id = $_SESSION['type_user_id_usr_flr'];
			$query_sql .= " AND u.type_user_id = $type_user_id ";
		}
		if (isset($_SESSION['dealer_id_usr_flr']) && ($_SESSION['dealer_id_usr_flr'] != "0")) {
			$dealer_id = $_SESSION['dealer_id_usr_flr'];
			$query_sql .= " AND d.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['no_time_ev_usr_flr']) && ($_SESSION['no_time_ev_usr_flr'] != "0")) {
			$query_sql .= " AND u.user_id IN ( SELECT rn.user_id FROM ludus_reviews_notime rn WHERE rn.status_id = 1 ) ";
		}
		if ($sWhere != '') {
			$query_sql .= " AND ";
			$ary = explode("+", $sWhere);
			$ctrl_val = 0;
			foreach ($ary as $key => $val) {
				if ($ctrl_val == 0) {
					$query_sql .= " (u.first_name LIKE '%$val%' OR u.last_name LIKE '%$val%' OR
							u.identification LIKE '%$val%' OR u.email LIKE '%$val%' OR
							u.phone LIKE '%$val%' OR
							s.status LIKE '%$val%' OR c.headquarter LIKE '%$val%') ";
				} else {
					$query_sql .= " AND (u.first_name LIKE '%$val%' OR u.last_name LIKE '%$val%' OR
							u.identification LIKE '%$val%' OR u.email LIKE '%$val%' OR
							u.phone LIKE '%$val%' OR
							s.status LIKE '%$val%' OR c.headquarter LIKE '%$val%') ";
				}
				$ctrl_val++;
			}
		}
		$query_sql .= $sOrder;
		$query_sql .= ' ' . $sLimit;
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		for ($i = 0; $i < count($Rows_config); $i++) {
			$query_Cargo = "SELECT c.charge
						FROM ludus_charges_users u, ludus_charges c
						WHERE u.user_id = " . $Rows_config[$i]['user_id'] . " AND u.charge_id = c.charge_id";
			$Rows_Charges = $DataBase_Acciones->SQL_SelectMultipleRows($query_Cargo);
			$Rows_config[$i]['cargos'] = $Rows_Charges;
		}

		// caracteres a buscar
		//$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");

		$no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "Ñ", "À");

		// caracteres de remplazo
		$permitidas = array(
			"a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A",
			"E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E"
		);

		foreach ($Rows_config as $key => $value) {

			// buscamos en cada posición del array y creamos uno nuevo con los valores remplazados
			$nombre = utf8_encode($value['first_name']);
			$apellido = utf8_encode($value['last_name']);
			$email = utf8_encode($value['email']);
			$user_id = $value['user_id'];

			$nombre_nuevo = str_replace($no_permitidas, $permitidas, $nombre, $cantidad_de_coincidencias_nombres);
			if ($cantidad_de_coincidencias_nombres > 0) {

				//Actualizando el nuevo nombre
				$Qyery_update = "UPDATE ludus_users SET first_name='$nombre' WHERE user_id= '$user_id';";
				$resultado = $DataBase_Acciones->SQL_Update($Qyery_update);
			}

			$nombre_nuevo = str_replace($no_permitidas, $permitidas, $apellido, $cantidad_de_coincidencias_apellido);

			if ($cantidad_de_coincidencias_apellido > 0) {

				//Actualizando el nuevo apellido
				$Qyery_update = "UPDATE ludus_users SET last_name='$apellido' WHERE user_id= '$user_id';";
				$resultado = $DataBase_Acciones->SQL_Update($Qyery_update);
			}

			$nombre_nuevo = str_replace($no_permitidas, $permitidas, $email, $cantidad_de_coincidencias_email);

			if ($cantidad_de_coincidencias_email > 0) {

				//Actualizando el nuevo email
				$Qyery_update = "UPDATE ludus_users SET email='$email' WHERE user_id= '$user_id';";
				$resultado = $DataBase_Acciones->SQL_Update($Qyery_update);
			}
		} // fin foreach

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFilter($prof, $sWhere, $sOrder, $sLimit)
	{
		@session_start();
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT u.*, c.headquarter, s.status, d.dealer
						FROM ludus_users u, ludus_headquarters c, ludus_status s, ludus_dealers d
						WHERE u.headquarter_id = c.headquarter_id AND u.status_id = s.status_id AND c.dealer_id = d.dealer_id ";
		if (isset($_SESSION['charge_id_usr_flr']) && ($_SESSION['charge_id_usr_flr'] != "0")) {
			$charge_id = $_SESSION['charge_id_usr_flr'];
			$query_sql .= " AND u.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id = $charge_id) ";
		}
		if (isset($_SESSION['rol_id_usr_flr']) && ($_SESSION['rol_id_usr_flr'] != "0")) {
			$rol_id = $_SESSION['rol_id_usr_flr'];
			$query_sql .= " AND u.user_id  IN (SELECT user_id FROM ludus_roles_users WHERE rol_id = $rol_id) ";
		}
		if (isset($_SESSION['status_id_usr_flr']) && ($_SESSION['status_id_usr_flr'] != "0")) {
			$status_id = $_SESSION['status_id_usr_flr'];
			$query_sql .= " AND u.status_id ='$status_id' ";
		}
		if (isset($_SESSION['headquarter_id_usr_flr']) && ($_SESSION['headquarter_id_usr_flr'] != "0")) {
			$headquarter_id = $_SESSION['headquarter_id_usr_flr'];
			$query_sql .= " AND u.headquarter_id = $headquarter_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "3") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "4") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		if (isset($_SESSION['no_time_ev_usr_flr']) && ($_SESSION['no_time_ev_usr_flr'] != "0")) {
			$filter = $_SESSION['no_time_ev_usr_flr'];
			$query_sql .= " AND u.user_id IN ( SELECT rn.user_id FROM ludus_reviews_notime rn WHERE rn.status_id = 1 ) ";
		}
		if ($sWhere != '') {
			$query_sql .= " AND (u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR
				u.identification LIKE '%$sWhere%' OR u.email LIKE '%$sWhere%' OR
				u.phone LIKE '%$sWhere%' OR
				s.status LIKE '%$sWhere%' OR c.headquarter LIKE '%$sWhere%') ";
		}
		if (isset($_SESSION['type_user_id_usr_flr']) && ($_SESSION['type_user_id_usr_flr'] != "0")) {
			$type_user_id = $_SESSION['type_user_id_usr_flr'];
			$query_sql .= " AND u.type_user_id = $type_user_id ";
		}
		if (isset($_SESSION['dealer_id_usr_flr']) && ($_SESSION['dealer_id_usr_flr'] != "0")) {
			$dealer_id = $_SESSION['dealer_id_usr_flr'];
			$query_sql .= " AND d.dealer_id = $dealer_id ";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function BorraRoles($user_id)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$updateSQL_ER = "DELETE FROM ludus_roles_users WHERE user_id = '$user_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function BorraCargos($user_id)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$updateSQL_ER = "DELETE FROM ludus_charges_users WHERE user_id = '$user_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function CrearRoles($user_id, $rol_id)
	{
		// include_once('../../config/database.php');
		// include_once('../../config/config.php');
		include_once('../../config/init_db.php');
		// $DataBase_Log = new Database();
		// $insertSQL_EE = "INSERT INTO ludus_roles_users VALUES ($rol_id,$user_id,NOW(),1)";
		$resultado = DB::insert('ludus_roles_users', [
			"rol_id" => $rol_id,
			"user_id" => $user_id,
			"date_creation" => DB::sqleval('NOW()'),
			"status_id" => 1
		]);
		return DB::insertId();
	}
	//========================================================================
	//Elimina la zona si fue asignado como coordinador y ya no tiene el cargo
	//========================================================================
	function resetZonas($user_id)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$resultado = 0;
		$cuenta = "SELECT COUNT(user_id) AS cuenta FROM ludus_roles_users
			WHERE user_id = $user_id AND rol_id = 3";

		$res_cuenta = $DataBase_Log->SQL_SelectRows($cuenta);
		if ($res_cuenta['cuenta'] == 0) {
			$insertSQL_EE = "UPDATE ludus_users SET zone_id = 0 WHERE user_id = $user_id";
			$resultado = $DataBase_Log->SQL_Update($insertSQL_EE);
		}
		unset($DataBase_Log);
		return $resultado;
	}
	function CrearCargos($user_id, $charge_id)
	{
		// include_once('../../config/database.php');
		// include_once('../../config/config.php');
		include_once('../../config/init_db.php');
		// $DataBase_Log = new Database();
		// $insertSQL_EE = "INSERT INTO ludus_charges_users VALUES ($charge_id,$user_id,NOW(),1)";
		DB::insert('ludus_charges_users', [
			"charge_id" => $charge_id,
			"user_id" => $user_id,
			"date_creation" => DB::sqleval('NOW()'),
			"status_id" => 1
		]);
		return DB::insertId();
	}
	//========================================================================
	//Modifica todos los datos de un usuario especifico
	//========================================================================
	function actualizaUsuario_all($first_name, $last_name, $date_birthday, $user_id, $email, $phone, $headquarter_id, $status_id, $restablece, $identification, $gender, $education, $pais)
	{
		@session_start();
		// include_once('../../config/database.php');
		// include_once('../../config/config.php');
		include_once('../../config/init_db.php');
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		$var_key_bd = "LdLmsDoorGM_2015*";
		$idQuien = $_SESSION['idUsuario'];
		$zone = "";
		// $DataBase_Log = new Database();

		$datos_u = DB::queryFirstRow("SELECT * FROM ludus_users WHERE user_id = $user_id");
		//si cambia de headquarter inserta un nuevo registro en ludus_headquarter_history
		if (($datos_u['headquarter_id'] != $headquarter_id) || ($datos_u['status_id'] != $status_id)) {
			DB::update('ludus_headquarter_history', [
				"status_id" => 2,
				"date_inactive" => DB::sqleval("NOW()"),
				"editor" => $idQuien,
				"date_edition" => DB::sqleval("NOW()")
			], " user_id = %i AND status_id = 1", $user_id);

			if ($status_id == 1) {
				DB::insert('ludus_headquarter_history', [
					"headquarter_id" => $headquarter_id,
					"user_id" => $user_id,
					"date_creation" => DB::sqleval('NOW()'),
					"status_id" => 1
				]);
			}
		} //fin validacion nuvo headquarter

		if ($restablece == "SI") {
			DB::update('ludus_users', [
				"password" => DB::sqleval("AES_ENCRYPT(identification,'" . $var_key_bd . "')")
			], "user_id = $user_id");
		}
		DB::update("ludus_users", [
			'first_name' 		=> $first_name,
			'last_name'			=> $last_name,
			'identification' 	=> $identification,
			'date_birthday' 	=> $date_birthday,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'headquarter_id' 	=> $headquarter_id,
			'date_session' 		=> $NOW_data,
			'gender' 			=> $gender,
			'editor' 			=> $idQuien,
			'date_edition' 		=> DB::sqleval("NOW()"),
			'education' 		=> $education,
			'pais'				=> $pais,
			'status_id' 		=> $status_id
		], "user_id = $user_id");
		return DB::affectedRows();
	} //fin actualizaUsuario_all
	//========================================================================
	//Crea un nuevo usuario
	//========================================================================
	function creaUsuario_all($first_name, $last_name, $date_birthday, $identification, $email, $phone, $headquarter_id, $gender, $education)
	{
		@session_start();
		// include_once('../../config/database.php');
		// include_once('../../config/config.php');
		include_once('../../config/init_db.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		// $DataBase_Log = new Database();
		$var_key_bd = "LdLmsDoorGM_2015*";
		DB::insert("ludus_users", [
			'first_name' 		=> $first_name,
			'last_name'			=> $last_name,
			'identification' 	=> $identification,
			'email' 			=> $email,
			"password" 			=> DB::sqleval("AES_ENCRYPT(identification,'" . $var_key_bd . "')"),
			'phone' 			=> $phone,
			'image'				=> "default.png",
			'headquarter_id' 	=> $headquarter_id,
			'date_creation' 	=> DB::sqleval("NOW()"),
			'date_session' 		=> DB::sqleval("NOW()"),
			'date_startgm' 		=> DB::sqleval("NOW()"),
			'gender' 			=> $gender,
			'editor' 			=> $idQuien,
			'date_edition' 		=> DB::sqleval("NOW()"),
			'education' 		=> $education,
			'status_id' 		=> 1
		]);
		$user_id = DB::insertId();
		DB::insert('ludus_headquarter_history', [
			"headquarter_id" => $headquarter_id,
			"user_id" => $user_id,
			"date_creation" => DB::sqleval('NOW()'),
			"status_id" => 1
		]);

		return $user_id;
	} //fin creaUsuario_all

	function consultaCantidadTomados()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT m.module_id, m.module, m.type, m.minimum_obtain, u.score, u.approval, u.date_creation, m.newcode, m.course_id
					FROM ludus_modules m, ludus_modules_results_usr u
					WHERE m.module_id = u.module_id AND u.user_id = '$idQuien' AND u.status_id = 1"; //LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaTomados()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}

		$query_sql = "SELECT DISTINCT inv.invitation_id, c.newcode, c.newcode, c.course, c.type, c.course_id, c.image, s.start_date date_creation, MAX(n.date_creation), AVG(n.score) score
							FROM ludus_courses c, ludus_modules m, ludus_invitation inv, ludus_schedule s, (SELECT MAX(score) as score, date_creation, module_id, invitation_id
							FROM ludus_modules_results_usr WHERE user_id = $idQuien GROUP BY module_id) AS n
							WHERE n.module_id = m.module_id AND m.course_id = c.course_id
							and inv.invitation_id = n.invitation_id
							and inv.schedule_id = s.schedule_id
							GROUP BY c.course, c.type, c.newcode
							ORDER BY date_creation DESC";
							// echo'<pre>';
							// print_r($query_sql);
							// echo'</pre>';
							// die();
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CA_Inscrito()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Autodirigidos' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.type = 'WBT' AND c.course_id NOT IN (SELECT c.course_id
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu
					WHERE cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id ) AND
					c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id NOT IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CT_Inscrito()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Mi Trayectoria' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id AND c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id NOT IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CA_Finalizado()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Autodirigidos' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.type = 'WBT' AND c.course_id NOT IN (SELECT c.course_id
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu
					WHERE cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id ) AND
					c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CT_Finalizado()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Mi Trayectoria' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id AND c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CA_Disponible()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Autodirigidos' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.type = 'WBT' AND c.course_id NOT IN (SELECT c.course_id
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu
					WHERE cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id ) AND
					c.course_id NOT IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien')
					AND c.status_id = 1 ORDER BY c.date_edition";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CT_Disponible()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Mi Trayectoria' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id AND c.course_id NOT IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien')
					AND c.status_id = 1 ORDER BY c.date_edition";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_CT_Disponible2()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT  c.newcode,cu.course_id,cu.user_id, c.course,'Mi Trayectoria' as disposicion, c.image, s.specialty_id, s.specialty,c.status_id FROM ludus_courses_users cu
						INNER JOIN ludus_courses c on c.course_id = cu.course_id
						INNER JOIN ludus_specialties s on s.specialty_id = c.specialty_id
						WHERE cu.user_id = '$idQuien' AND c.status_id = 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	// ========================================================
	// Consulta los usuarios para descargar en formato excel
	// ========================================================
	public static function consultaDatosUsuarios_Down()
	{
		include_once('../config/init_db.php');
		@session_start();

		$query_sql = "SELECT u.*, c.headquarter, s.status, d.dealer, z.zone, t.type_user, TIMESTAMPDIFF(YEAR, date_birthday, CURDATE()) as edad
						FROM ludus_users u, ludus_headquarters c, ludus_status s, ludus_dealers d, ludus_type_user t, ludus_areas a, ludus_zone z
						WHERE u.headquarter_id = c.headquarter_id AND u.status_id = s.status_id AND c.dealer_id = d.dealer_id AND u.type_user_id = t.type_user_id AND c.area_id = a.area_id AND a.zone_id = z.zone_id";
		if (isset($_SESSION['charge_id_usr_flr']) && ($_SESSION['charge_id_usr_flr'] != "0")) {
			$charge_id = $_SESSION['charge_id_usr_flr'];
			$query_sql .= " AND u.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id = $charge_id) ";
		}
		if (isset($_SESSION['rol_id_usr_flr']) && ($_SESSION['rol_id_usr_flr'] != "0")) {
			$rol_id = $_SESSION['rol_id_usr_flr'];
			$query_sql .= " AND u.user_id  IN (SELECT user_id FROM ludus_roles_users WHERE rol_id = $rol_id) ";
		}
		if (isset($_SESSION['status_id_usr_flr']) && ($_SESSION['status_id_usr_flr'] != "0")) {
			$status_id = $_SESSION['status_id_usr_flr'];
			$query_sql .= " AND u.status_id ='$status_id' ";
		}
		if (isset($_SESSION['headquarter_id_usr_flr']) && ($_SESSION['headquarter_id_usr_flr'] != "0")) {
			$headquarter_id = $_SESSION['headquarter_id_usr_flr'];
			$query_sql .= " AND u.headquarter_id = $headquarter_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "3") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['type_user_id_usr_flr']) && ($_SESSION['type_user_id_usr_flr'] != "0")) {
			$type_user_id = $_SESSION['type_user_id_usr_flr'];
			$query_sql .= " AND u.type_user_id = $type_user_id ";
		}
		if (isset($_SESSION['dealer_id_usr_flr']) && ($_SESSION['dealer_id_usr_flr'] != "0")) {
			$dealer_id = $_SESSION['dealer_id_usr_flr'];
			$query_sql .= " AND d.dealer_id = $dealer_id ";
		}
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] == "4") {
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		$query_sql .= " ORDER BY u.first_name, u.last_name ASC";
		// echo($query_sql);

		$Rows_config = DB::query($query_sql);
		for ($i = 0; $i < count($Rows_config); $i++) {
			$query_Cargo = "SELECT c.charge, cc.category
						FROM ludus_charges_users u, ludus_charges c, ludus_charges_categories cc
						WHERE u.user_id = " . $Rows_config[$i]['user_id'] . " AND u.charge_id = c.charge_id AND c.category_id = cc.category_id";
			$Rows_Charges = DB::query($query_Cargo);
			$Rows_config[$i]['cargos'] = $Rows_Charges;
		}

		return $Rows_config;
	}
	function consulta_CoursesUsuario($idUsuario)
	{
		include_once('../config/init_db.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.type, c.course_id, c.image
					FROM ludus_charges_users cu, ludus_charges_courses cc, ludus_courses c
					WHERE cu.user_id = $idUsuario AND cu.charge_id = cc.charge_id AND cc.course_id = c.course_id AND cc.status_id = 1
					AND c.status_id = 1
					ORDER BY c.course ";

		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}
	function consulta_CoursesCharges($idUsuario)
	{
		include_once('../config/init_db.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT cc.course_id, cc.charge_id, cu.user_id, cc.level_id
				FROM ludus_charges_courses cc, ludus_charges_users cu
				WHERE cu.user_id = $idUsuario AND cu.charge_id = cc.charge_id AND cc.status_id = 1";

		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}
	function consulta_CoursesChargesResult($idUsuario)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT cc.course_id, cc.charge_id, cc.level_id, re.score
				FROM ludus_charges_users cu, ludus_charges_courses cc, ludus_courses c, (SELECT m_r.course_id, AVG(tm.score) as score
				FROM ludus_modules m_r, (SELECT module_id, MAX(score) as score
						FROM ludus_modules_results_usr
						WHERE user_id = '$idUsuario'
						GROUP BY module_id) as tm
				WHERE m_r.module_id = tm.module_id
				GROUP BY m_r.course_id) re
				WHERE cu.user_id = '$idUsuario' AND cu.charge_id = cc.charge_id AND cc.course_id = c.course_id AND c.course_id = re.course_id
				ORDER BY cc.course_id, cc.charge_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_Publicidad($idPublicidad)
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT *
					FROM ludus_release m
					WHERE m.pub_release = '$idPublicidad' AND m.user_id = '$idQuien' "; //LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function crea_Publicidad($idPublicidad)
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_release (user_id,pub_release,date_creation) VALUES ($idQuien,$idPublicidad,NOW())";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
	function crea_Navegacion($section, $element_id, $table_ref, $time_navigation)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$section = str_replace('http://www.gmacademy.co./admin/', '', $section);
		$section = str_replace('http://www.gmacademy.co/', '', $section);
		$section = str_replace('http://www.gmacademy.co', '', $section);

		$section = str_replace('https://www.gmacademy.co./admin/', '', $section);
		$section = str_replace('https://www.gmacademy.co/', '', $section);
		$section = str_replace('https://www.gmacademy.co', '', $section);

		$section = str_replace('http://gmacademy.luduscolombia.com.co./admin/', '', $section);
		$section = str_replace('http://luduscolombia.com.co/gmacademy/admin/', '', $section);
		$section = str_replace('http://www.gmacademy.luduscolombia.com.co./admin/', '', $section);
		$section = str_replace('https://gmacademy.luduscolombia.com.co./gmacademy/admin/', '', $section);
		$section = str_replace('https://luduscolombia.com.co/gmacademy/admin/', '', $section);
		$section = str_replace('https://www.gmacademy.luduscolombia.com.co/gmacademy/admin/', '', $section);
		if ($idQuien > 0) {
			$DataBase_Log = new Database();
			$insertSQL_EE = "INSERT INTO ludus_navigation (user_id,section,date_navigation,element_id,table_ref,time_navigation)
							VALUES
							($idQuien,'$section',NOW(),'$element_id','$table_ref','$time_navigation')";
			$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
			return $resultado;
			unset($DataBase_Log);
		}
	}
	function CantidadSalidas($idUsuario)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Class = new Database();
		$anoCurso = date('Y');
		/*Consulta si tiene encuesta de satisfacción pendiente por responder*/
		$query_encSatistaccion = "SELECT sum(m.duration_time) as tiempo
				FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_modules m
				WHERE i.status_id = 2 AND i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND c.course_id = m.course_id
				AND s.start_date like '$anoCurso-%'
				AND i.user_id = '$idUsuario' ";
		//echo($query_encSatistaccion);
		$Row_Encuesta = $DataBase_Class->SQL_SelectRows($query_encSatistaccion);
		/*Consulta si tiene encuesta de satisfacción pendiente por responder*/
		unset($DataBase_Log);
		return $Row_Encuesta;
	}
	function actualizaUsuario_par($first_name, $last_name, $date_birthday, $user_id, $email, $phone, $date_startgm, $gender, $education, $pais)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');

		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_users SET
								date_birthday = '$date_birthday',
								email = '" . strtolower($email) . "',
								first_name = '" . ucwords(strtolower($first_name)) . "',
								last_name = '" . ucwords(strtolower($last_name)) . "',
								phone = '$phone',
								date_startgm = '$date_startgm',
								date_session = '$NOW_data',
								education    = '$education',
								pais   		 = '$pais',
								gender = '$gender' WHERE user_id = '$user_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	/*Funciones angular*/
	function consulta_CA_Inscrito_S($specialty, $contenido_data)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Autodirigidos' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND s.specialty_id = $specialty AND c.course like '%$contenido_data%' AND c.type = 'WBT' AND c.course_id NOT IN (SELECT c.course_id
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu
					WHERE cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id ) AND
					c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id NOT IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CT_Inscrito_S($specialty, $contenido_data)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Mi Trayectoria' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND s.specialty_id = $specialty AND c.course like '%$contenido_data%' AND cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id AND c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id NOT IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CA_Finalizado_S($specialty, $contenido_data)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Autodirigidos' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.specialty_id = $specialty AND c.course like '%$contenido_data%' AND c.type = 'WBT' AND c.course_id NOT IN (SELECT c.course_id
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu
					WHERE cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id ) AND
					c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CT_Finalizado_S($specialty, $contenido_data)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Mi Trayectoria' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.specialty_id = $specialty AND c.course like '%$contenido_data%' AND cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id AND c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CA_Disponible_S($specialty, $contenido_data)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Autodirigidos' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.specialty_id = $specialty AND c.course like '%$contenido_data%' AND c.type = 'WBT' AND c.course_id NOT IN (SELECT c.course_id
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu
					WHERE cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id ) AND
					c.course_id NOT IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien')
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consulta_CT_Disponible_S($specialty, $contenido_data)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			$idQuien = 0;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, 'Mi Trayectoria' as disposicion, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.specialty_id = $specialty AND c.course like '%$contenido_data%' AND cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id AND c.course_id NOT IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien')
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	} //fin metodo consulta_CT_Disponible_S
	/*
	Andres Vega
	09/08/2016
	Metodo para consultar los tipos de usuarios existentes
	*/
	function tipoUsuarios()
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_type_user";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	} //fin metodo tipoUsuarios
	/*
	Andres Vega
	01/11/2016
	Metodo para consultar un usuario en especifico
	*/
	function consultaUsuario($identification)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_users WHERE identification = '$identification'";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	} //fin metodo tipoUsuarios
	/*
	Andres Vega
	01/11/2016
	Metodo para consultar un usuario en especifico
	*/
	function consultaTipoUsuario($tipo)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_charges WHERE charge = '$tipo'";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	} //fin metodo consultaTipoUsuario
	/*
	Andres Vega
	01/11/2016
	Metodo para consultar un usuario en especifico
	*/
	function cargarRegistro($query)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_Insert($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	} //fin metodo cargarRegistro
	/*
	Andres Vega
	01/11/2016
	Metodo para consultar un usuario en especifico
	*/
	function actualizarRegistro($query)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_Update($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	} //fin metodo cargarRegistro
	//=====================================================================
	// actualiza ludus_headquarter_history si se realiza un cambio en la sede del usuario
	//=====================================================================
	public static function updateHeadquarterHistory($headquarter_id, $documento)
	{
		include_once('../config/init_db.php');
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		$user = DB::queryFirstRow("SELECT * FROM ludus_users WHERE identification = %i", $documento);
		if ($user['headquarter_id'] != $headquarter_id || $user['status_id'] == 2) {
			DB::update('ludus_headquarter_history', [
				"status_id" => 2,
				"date_inactive" => DB::sqleval("NOW()"),
				"editor" => $user_id,
				"date_edition" => DB::sqleval("NOW()")
			], " user_id = %i AND status_id = 1", $user['user_id']);

			DB::insert('ludus_headquarter_history', [
				"headquarter_id" => $headquarter_id,
				"user_id" => $user['user_id'],
				"date_creation" => DB::sqleval('NOW()'),
				"status_id" => 1
			]);
		}
	} //fin updateHeadquarterHistory
	//=====================================================================
	// inserta en ludus_headquarter_history un nuevo registro
	//=====================================================================
	public static function insertHeadquarterHistory($headquarter_id, $user_id)
	{
		include_once('../config/init_db.php');
		DB::insert('ludus_headquarter_history', [
			"headquarter_id" => $headquarter_id,
			"user_id" => $user_id,
			"date_creation" => DB::sqleval('NOW()'),
			"status_id" => 1
		]);
	} //fin updateHeadquarterHistory
	/*
	Andres Vega
	25/11/2016
	Funcion para consultar el id de un concesionario
	*/
	function consultarSede($sede)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT h.headquarter_id, h.dealer_id FROM ludus_headquarters h WHERE h.headquarter = '$sede' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	} //funcion consultarSede
	//============================================================================
	//Andres Vega
	//26 mayo 2017
	//consulta la lista de zonas
	//============================================================================
	function getZonas()
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT z.zone_id, z.zone FROM ludus_zone z";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//============================================================================
	//Andres Vega
	//7 Junio 2017
	//consulta un correo en especifico
	//============================================================================
	function getCorreo($email, $prof = "")
	{
		include_once($prof . '../config/database.php');
		include_once($prof . '../config/config.php');

		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$query_sql = "SELECT u.user_id, u.first_name, u.last_name, u.email, u.identification FROM ludus_users u WHERE u.email = '$email' AND u.status_id = 1";
			$DataBase_Acciones = new Database();
			$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
			unset($DataBase_Acciones);
		} else {
			$Rows_config = [
				"error" => true, "mensaje" => "No es un correo valido"
			];
		}

		return $Rows_config;
	} //fin getCorreo

	/*Diego L 26/02/2017
	Funcion para registrar visitantes banner streaming*/
	function registraStreaming()
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		$insertSQL_EE = "INSERT INTO ludus_jaimegil (user_id,date_time) VALUES ('$idQuien','$NOW_data')";
		$DataBase_Log = new Database();
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
	}
	function consultaConectados()
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT count(DISTINCT user_id) as cant FROM ludus_jaimegil ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	/*Fin funciones streaming*/
	//============================================================================
	//Consulta los usuarios filtrados por headquarter
	//============================================================================
	public static function usuariosBy($headquarter_id, $campo, $prof = "../")
	{
		include_once($prof . "../config/init_db.php");
		$resultSet = DB::query("SELECT * FROM ludus_users WHERE $campo = %i", $headquarter_id);
		return $resultSet;
	}
	//============================================================================
	//Consulta los usuarios filtrados por headquarter
	//============================================================================
	public static function inactivarHeadquarter($headquarter_id, $identifications, $prof = "../")
	{
		include_once($prof . "../config/init_db.php");
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		DB::update("ludus_headquarter_history", [
			"status_id" => 2,
			"date_inactive" => DB::sqleval('NOW()'),
			"editor" => $user_id,
			"date_edition" => DB::sqleval('NOW()')
		], "headquarter_id = %i AND status_id = 1 AND user_id IN ( SELECT user_id FROM ludus_users WHERE identification IN ( $identifications ) )", $headquarter_id);
		return DB::affectedRows();
	} //fin inactivarHeadquarter
	//============================================================================
	//Consulta los usuarios filtrados por headquarter
	//============================================================================
	public static function activarHeadquarter($p, $prof = "../")
	{
		include_once($prof . "../config/init_db.php");
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		DB::insert("ludus_headquarter_history", [
			"headquarter_id" => $p['headquarter_id'],
			"user_id" => $p['user_id'],
			"status_id" => 1,
			"date_creation" => DB::sqleval('NOW()'),
			"editor" => $user_id,
			"date_edition" => DB::sqleval('NOW()')
		]);
		return DB::insertId();
	} //fin activarHeadquarter
	//============================================================================
	//Consulta los usuarios filtrados por headquarter
	//============================================================================
	public static function query($query, $prof = "../")
	{
		include_once($prof . "../config/init_db.php");
		return DB::query($query);
	} //fin query
	//============================================================================
	// consulta los concesionarios para un coordinador
	//============================================================================
	public static function getCoordinadoresCcs($user_id, $prof = "../")
	{
		include_once($prof . "../config/init_db.php");
		$query = "SELECT dc.* FROM ludus_dealers_coordinators dc WHERE dc.user_id = $user_id";
		return DB::query($query);
	} //fin getCoordinadoresCcs
	//============================================================================
	// Elimina y vuelte a cargar los concesionarios a los coordinadores
	//============================================================================
	public static function eliminaCoordinadoresCcs($user_id, $prof = "../")
	{
		include_once($prof . "../config/init_db.php");
		DB::delete('ludus_dealers_coordinators', "user_id = $user_id");
		return DB::affectedRows();
	} //fin eliminaCoordinadoresCcs
	//============================================================================
	// Crea un nuevo concesionario para un coordinador de zona
	//============================================================================
	public static function actualizarCoordinadoresCcs($p, $prof = "../")
	{
		include_once($prof . "../config/init_db.php");
		DB::insert('ludus_dealers_coordinators', [
			"user_id" => $p['user_id'],
			"dealer_id" => $p['dealer_id'],
			"status_id" => 1,
			"date_creation" => DB::sqleval('NOW()')
		]);
		return DB::insertId();
	} //fin actualizarCoordinadoresCcs


	public static function registrarme($p)
	{
		include_once("../../config/init_db.php");
		$identification = $p['identification'];
		$query_select = "SELECT * from ludus_users where identification = $identification;";

		$Rows_u = DB::query($query_select);
		$res = array();
		if (count($Rows_u) > 0) {
			$res['msj'] = 'Usuario ya existe';
			$res['error'] = false;
		} else {

			$p['treatment_policy'] = $p['treatment_policy'] == 'on' ? 'Si' : 'No';
			$password = $p['identification'];
			 $var_key_bd = isset($datos['key'] ) ? $datos['key'] : "LdLmsDoorGM_2015*";
			$password = "AES_ENCRYPT('" . $password . "','" . $var_key_bd . "')";
			$query = "INSERT INTO ludus_users
						(
						last_name,
						first_name,
						image,
						identification,
						password,
						email,
						phone,
						date_creation,
						headquarter_id,
						status_id,
						gender,
						zone_id,
						pais,
						treatment_policy
			
						)
						VALUES
						(
						'" . ucwords(strtolower($p['last_name'])) . "',
						'" . ucwords(strtolower($p['first_name'])) . "',
						'default.png',
						'{$p['identification']}',
						$password,
						'" . strtolower($p['email']) . "',
						'{$p['mobile_phone']}',
						now(),
						2,
						1,
						'{$p['gender']}',
						2,
						'{$p['pais']}',
						'{$p['treatment_policy']}'
						);";

				
					

			$Rows_config = DB::query($query);
			$user_id = DB::insertId($query);

			$query_rol = "INSERT INTO ludus_roles_users
						(rol_id,
						user_id,
						date_creation,
						status_id)
						VALUES
						(6,
						$user_id,
						now(),
						1);";
			$Rows_config = DB::query($query_rol);


			for ($i = 0, $size = count($p['trayectoria']); $i < $size; ++$i) {
				$query_rol = "INSERT INTO ludus_charges_users
							(charge_id,
							user_id,
							date_creation,
							status_id)
							VALUES
							('{$p['trayectoria'][$i]}',
							$user_id,
							now(),
							1);";
				$Rows_config = DB::query($query_rol);
			}


			// insertamos los cursos WBT al usuario creado dependiendo de la trayectoria
			$trayectoriaSelect = implode(",", $p['trayectoria']);
			DB::query("INSERT INTO ludus_courses_users(user_id,
									course_id,
									date_creation,
									creator,
									editor,
									date_edition,
									status_id) 
									SELECT 
									    $user_id,
										lcc.course_id,
										now(),
										$user_id,
										$user_id,
										now(),
										1
										FROM ludus_charges_courses lcc
										INNER JOIN ludus_courses c
										ON lcc.course_id = c.course_id 
										WHERE lcc.charge_id IN($trayectoriaSelect) AND c.type = 'WBT'");



			if ($Rows_config) {

				$nombre = utf8_decode(ucwords(strtolower($p['first_name'])));
				$apellidos = utf8_decode(ucwords(strtolower($p['last_name'])));
				$identificacion = utf8_decode(ucwords(strtolower($p['identification'])));
				$email = utf8_decode(ucwords(strtolower($p['email'])));

				ini_set('display_errors', 1);
				error_reporting(E_ALL);

				if ($p['gender'] == 1) {
					$gender = 'o';
				} else {
					$gender = 'a';
				}
				$destinatario = $email;
				$asunto = "Nuevo usuario registrado";
				$cuerpo = '<table>
							<thead>
								<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/head_email.png"></td>
							</thead>
							<tbody>
								<tr>
									<td style="text-align: center;" >
										<h3>Bienvenido a un mundo de aprendizaje dise&ntilde;ado especialmente para ti:</h3>
										<p><strong>Usuario </strong>' . $identificacion . '</p>
										<p><strong>Contrase&ntilde;a: </strong>' . $identificacion . '</p>
										<p><strong>Recuerda cambiar tu contrase&ntilde;a, Muchas Gracias</strong></p>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;"><p style="color: #0731a7; text-transform: uppercase; font-size: 20px; font-weight: 700;">Cursos Disponibles</p></td>
								</tr>
                                <tr style="text-align: center;">
	                             	<td style="text-align: center;">
	                             		<div style="float: left; width: 390px;">
	                             			<img style="width: 90%;" src="https://www.acdelco.com.co/lms/assets/images/Mail_cursos_02.jpg">
	                             		</div>
	                             	    <div style="float: left; width: 390px;">
	                             	    	<img style="width: 90%;" src="https://www.acdelco.com.co/lms/assets/images/Mail_cursos_03.jpg">
	                             	    </div>
	                             	</td>
                                </tr>
								<tr><td></td></tr>

							</tbody>
							<tfoot>
								<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/footer_email.png"></td>
							</tfoot>
						</table>';

				//para el envío en formato HTML 
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

				//dirección del remitente 
				$headers .= "From: ACDelco LMS <trainingacademy@acdelco.com.co>\r\n";

				//dirección de respuesta, si queremos que sea distinta que la del remitente 
				//$headers .= "Reply-To: respuesta@autotrain.com\r\n"; 

				//ruta del mensaje desde origen a destino 
				$headers .= "Return-path: trainingacademy@acdelco.com.co\r\n";

				//direcciones que recibián copia 
				$headers .= "Cc: trainingacademy@acdelco.com.co\r\n";

				//direcciones que recibirán copia oculta 
				//$headers .= "Bcc: pepe@pepe.com,juan@juan.com\r\n"; 

				//TODO: DESCOMENTAR

				/* if (mail($destinatario, $asunto, $cuerpo, $headers)) {
					$res['email'] = 'Email enviado correctamente';
				} else {
					$res['email'] = 'Email no enviado';
				} */
				//echo "The email message was sent.";

				$res['msj'] = 'Se ha registrado exitosamente, estamos validando sus datos. Su usuario y contraseña llegaran al correo registrado para que pueda ingresar. Muchas gracias';
				$res['error'] = false;
			} else {
				$res['error'] = true;
			}
		}
		return $res;
	}



	function agregarTrayectoria($p)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_charges_users(charge_id, user_id, date_creation, status_id) VALUES ('{$p['charge_id']}', '{$p['user_id']}', now(), '1')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
}//fin clase Usuarios
