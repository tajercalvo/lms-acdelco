<?php
Class RepNavegacionClass {

	function consultaNavegacion($start_date, $end_date, $dealer){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];

		if ($_SESSION['max_rol']<=4) {

			if ($dealer== '') {

				$Rows_config = array();
				return $Rows_config;

			}else{
				$query_sql = "SELECT n.section, count(*) as cantidadVisitas, count(distinct n.user_id) as cantidadUsuarios ,avg(time_navigation) as tiempoPromedio, sum(time_navigation) as tiempoTotal 
					FROM ludus_navigation n, ludus_users u, ludus_headquarters h
					WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND n.user_id = u.user_id
					AND u.headquarter_id = h.headquarter_id
					AND h.dealer_id = $dealer_id
					GROUP BY n.section ORDER BY n.section";
			}

			

		}else{
			
				if ($dealer!= '') {
			
					$query_sql = "SELECT n.section, count(*) as cantidadVisitas, count(distinct n.user_id) as cantidadUsuarios ,avg(time_navigation) as tiempoPromedio, sum(time_navigation) as tiempoTotal 
					FROM ludus_navigation n, ludus_users u, ludus_headquarters h
					WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND n.user_id = u.user_id
					AND u.headquarter_id = h.headquarter_id
					AND h.dealer_id = $dealer
					GROUP BY n.section ORDER BY n.section";

				}else{

					$query_sql = "SELECT n.section, count(*) as cantidadVisitas, count(distinct n.user_id) as cantidadUsuarios ,avg(time_navigation) as tiempoPromedio, sum(time_navigation) as tiempoTotal 
					FROM ludus_navigation n, ludus_users u, ludus_headquarters h
					WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND n.user_id = u.user_id
					AND u.headquarter_id = h.headquarter_id
					GROUP BY n.section ORDER BY n.section";

				}
		}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		//print_r($Rows_config);

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function VisitantesUnicos($start_date, $end_date, $dealer){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];

		if ($_SESSION['max_rol']<=4) {

			if ($dealer == '') {

				$Rows_config = array();
				return $Rows_config;
				

			}else{
					$query_sql = "SELECT count(distinct n.user_id) as cantidad 
					FROM ludus_navigation n, ludus_users u, ludus_headquarters h
					WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND 
					n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id 
					AND h.dealer_id = $dealer_id";
			}

		}else{
			
			if ($dealer == '') {

						$query_sql = "SELECT count(distinct n.user_id) as cantidad 
						FROM ludus_navigation n, ludus_users u, ludus_headquarters h
						WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND 
						n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id ";
				
				}else{

						$query_sql = "SELECT count(distinct n.user_id) as cantidad 
						FROM ludus_navigation n, ludus_users u, ludus_headquarters h
						WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND 
						n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id 
						AND h.dealer_id = $dealer";
				}
		}

		
		
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function DetalleVisitas($start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		

		if(isset($_SESSION['max_rol'])&& $_SESSION['max_rol'] <= 4 ){
			$dealer_id = $_SESSION['dealer_id'];
			$query = " SELECT u.user_id, u.first_name, u.last_name, u.identification, u.phone, u.status_id, h.headquarter, d.dealer, u.email, n.section, n.date_navigation, n.time_navigation
				FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
				WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.dealer_id = $dealer_id ORDER BY d.dealer,h.headquarter,u.first_name, u.last_name,n.section";
			 
		}else{	

				$Rows_config = array();
				$dealer = $this->consulta_concesioanrios();

				foreach ($dealer as $key => $value) {

					$query = " SELECT u.user_id, u.first_name, u.last_name, u.identification, u.phone, u.status_id, h.headquarter, d.dealer, u.email, n.section, n.date_navigation, n.time_navigation
								FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
								WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id 
								AND h.dealer_id = ".$value['dealer_id'].
								" ORDER BY d.dealer,h.headquarter,u.first_name, u.last_name,n.section";

					$Rows_config2= $DataBase_Acciones->SQL_SelectMultipleRows($query);

					$Rows_config = array_merge($Rows_config2, $Rows_config); // Unificando los array

					// foreach ($Rows_config2 as $key => $value) {
					// 	$Rows_config[] = $value;
					// }


				}

				

				// echo "<pre>";
				// print_r($Rows_config);
				// echo "</pre>";
				
		}

		
		


		$query_Cargos = "SELECT c.charge as cargo, cu.user_id FROM ludus_charges_users cu, ludus_charges c WHERE cu.charge_id = c.charge_id AND c.status_id = 1 ORDER BY cu.user_id, c.charge";
		$Rows_cargos = $DataBase_Acciones->SQL_SelectMultipleRows($query_Cargos);

		foreach ($Rows_config as $key => $value) {
			$user_id = $value['user_id'];
			//Cargos
				foreach ($Rows_cargos as $key2 => $value2) {
					if( $user_id == $value2['user_id'] ){
						$Rows_config[$key]['charges'][] = $Rows_cargos[$key2];
					}
				}
			//Cargos
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}


	//USUARIOS EN TRAYECTORIAS
	function consultaUsuarios($start_date, $end_date, $dealer ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();

		$dealer_id = $_SESSION['dealer_id'];
		if ($_SESSION['max_rol']<=4) {

				if ($dealer== '') {

					$Rows_config = array();
					return $Rows_config;

				}else{
				
				$query_sql = "SELECT DISTINCT lu.* 
					FROM ludus_users lu, ludus_charges_users lcu, ludus_navigation n, ludus_headquarters h
					where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and 
					lu.headquarter_id = h.headquarter_id AND 
					lu.status_id = 1 and lcu.charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) and lu.user_id = lcu.user_id and lu.user_id = n.user_id
					AND h.dealer_id = $dealer_id";
				}

		}else{
		
					if($dealer == ''){

							$query_sql = "SELECT DISTINCT lu.user_id  
							FROM ludus_users lu, ludus_charges_users lcu, ludus_navigation n, ludus_headquarters h
							where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and 
							lu.headquarter_id = h.headquarter_id AND 
							lu.status_id = 1 and lcu.charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) and lu.user_id = lcu.user_id and lu.user_id = n.user_id ";
					}else{

							$query_sql = "SELECT DISTINCT lu.* 
							FROM ludus_users lu, ludus_charges_users lcu, ludus_navigation n, ludus_headquarters h
							where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and 
							lu.headquarter_id = h.headquarter_id AND 
							lu.status_id = 1 and lcu.charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) and lu.user_id = lcu.user_id and lu.user_id = n.user_id
							AND h.dealer_id = $dealer";
					}
		}


		
		
		// $query_sql = "SELECT DISTINCT lu.* 
		// FROM ludus_users lu, ludus_charges_users lcu 
		// where lu.status_id = 1 and lcu.charge_id not in (125,123) and lu.user_id = lcu.user_id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultatiempoUsuarios($start_date, $end_date, $dealer){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];

		if ($_SESSION['max_rol']<=4) {
			
			$query_sql = "SELECT sum(ln.time_navigation) as tiempoTotal, h.dealer_id
									FROM ludus_navigation ln, ludus_users lu, ludus_headquarters h
									where date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and 
									lu.headquarter_id = h.headquarter_id AND 
									ln.user_id = lu.user_id and 
									lu.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) )
									AND h.dealer_id = $dealer_id";

		}else{
			
					if($dealer == ''){

									$query_sql = "SELECT sum(ln.time_navigation) as tiempoTotal, h.dealer_id
									FROM ludus_navigation ln, ludus_users lu, ludus_headquarters h
									where date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and 
									lu.headquarter_id = h.headquarter_id AND 
									ln.user_id = lu.user_id and 
									lu.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) )
									group by h.dealer_id ";


					}else{

						$query_sql = "SELECT sum(ln.time_navigation) as tiempoTotal, h.dealer_id
									FROM ludus_navigation ln, ludus_users lu, ludus_headquarters h
									where date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and 
									lu.headquarter_id = h.headquarter_id AND 
									ln.user_id = lu.user_id and 
									lu.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) )
									AND h.dealer_id = $dealer";

					}
		}


	
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//Todos los Usuarios del mismo concesionario activos
	function consulta_Usuarios_iteracion($start_date, $end_date, $dealer){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];

		if ($_SESSION['max_rol']<=4) {
			

			if ($dealer == '') {
				
				$Rows_config = array();
				return $Rows_config;

			}else{
					$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer
				from ludus_users u, ludus_headquarters h, ludus_dealers d,

				(select count( distinct u.user_id) as iteracion, d.dealer_id
								from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
								where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
								and n.user_id = u.user_id
								and u.headquarter_id = h.headquarter_id
								and h.dealer_id = d.dealer_id
								and u.status_id = 1
								and d.dealer_id = $dealer_id
								group by d.dealer
								order by dealer asc) as todos

				where u.headquarter_id = h.headquarter_id
				and h.dealer_id = d.dealer_id
				and todos.dealer_id = d.dealer_id
				and u.status_id = 1
				and d.dealer_id = $dealer_id
				group by d.dealer
				order by dealer asc";

			}

		}else{
			
						if ($dealer == '') {



					
						$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer_id
						from ludus_users u, ludus_headquarters h, ludus_dealers d,

						(select count( distinct u.user_id) as iteracion, d.dealer_id
										from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
										where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
										and n.user_id = u.user_id
										and u.headquarter_id = h.headquarter_id
										and h.dealer_id = d.dealer_id
										and u.status_id = 1
										group by d.dealer
										order by dealer asc) as todos

						where u.headquarter_id = h.headquarter_id
						and h.dealer_id = d.dealer_id
						and todos.dealer_id = d.dealer_id
						and u.status_id = 1
						group by d.dealer
						order by dealer asc";
					

				}else{

					
					$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer
						from ludus_users u, ludus_headquarters h, ludus_dealers d,

						(select count( distinct u.user_id) as iteracion, d.dealer_id
										from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
										where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
										and n.user_id = u.user_id
										and u.headquarter_id = h.headquarter_id
										and h.dealer_id = d.dealer_id
										and u.status_id = 1
										and d.dealer_id = $dealer
										group by d.dealer
										order by dealer asc) as todos

						where u.headquarter_id = h.headquarter_id
						and h.dealer_id = d.dealer_id
						and todos.dealer_id = d.dealer_id
						and u.status_id = 1
						and d.dealer_id = $dealer
						group by d.dealer
						order by dealer asc";
				
				}

		}
	
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

		//Todos los Usuarios en trayectorias del mismo concesionario activos
	function consulta_GMAcademy_iteracion($start_date, $end_date, $dealer, $trayectoria){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];


		if ($trayectoria=='') {
			$trayectory = '23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99';
		}else{
			$trayectory = $trayectoria;
		}

		$dealer_id = $_SESSION['dealer_id'];
		if ($_SESSION['max_rol']<=4) {

				if ($dealer == '') {

					$Rows_config = array();
					return $Rows_config;

				}else{

					$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer_id
					from ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu,

					(select count( distinct u.user_id) as iteracion, d.dealer_id
									from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu
									where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
									and lcu.charge_id in ($trayectory)
									and u.user_id = lcu.user_id
									and n.user_id = u.user_id
									and u.headquarter_id = h.headquarter_id
									and h.dealer_id = d.dealer_id
									and d.dealer_id = $dealer_id
									and u.status_id = 1
									group by d.dealer
									order by dealer asc) as todos

					where u.headquarter_id = h.headquarter_id
					and lcu.charge_id in ($trayectory)
					and u.user_id = lcu.user_id
					and h.dealer_id = d.dealer_id
					and todos.dealer_id = d.dealer_id
					and u.status_id = 1
					and d.dealer_id = $dealer_id
					group by d.dealer
					order by dealer asc;";

				}
			
		}else{

					if ($dealer == '') { 
						$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer_id
							from ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu,

							(select count( distinct u.user_id) as iteracion, d.dealer_id
											from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu
											where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
											and lcu.charge_id  in ($trayectory)
											and u.user_id = lcu.user_id
											and n.user_id = u.user_id
											and u.headquarter_id = h.headquarter_id
											and h.dealer_id = d.dealer_id
											and u.status_id = 1
											group by d.dealer
											order by dealer asc) as todos

							where u.headquarter_id = h.headquarter_id
							and lcu.charge_id in ($trayectory)
							and u.user_id = lcu.user_id
							and h.dealer_id = d.dealer_id
							and todos.dealer_id = d.dealer_id
							and u.status_id = 1
							group by d.dealer
							order by dealer asc";
								
					}else{

											$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer_id
						from ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu,

						(select count( distinct u.user_id) as iteracion, d.dealer_id
										from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu
										where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
										and lcu.charge_id in ($trayectory)
										and u.user_id = lcu.user_id
										and n.user_id = u.user_id
										and u.headquarter_id = h.headquarter_id
										and h.dealer_id = d.dealer_id
										and d.dealer_id = $dealer
										and u.status_id = 1
										group by d.dealer
										order by dealer asc) as todos

						where u.headquarter_id = h.headquarter_id
						and lcu.charge_id in ($trayectory)
						and u.user_id = lcu.user_id
						and h.dealer_id = d.dealer_id
						and todos.dealer_id = d.dealer_id
						and u.status_id = 1
						and d.dealer_id = $dealer
						group by d.dealer
						order by dealer asc;";

					}
		}

		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_concesioanrios(){

		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if ($_SESSION['max_rol']<=4) {
			$query_sql = "SELECT * FROM ludus_dealers where dealer_id = $dealer_id ";
		}else{
			$query_sql = "SELECT * FROM ludus_dealers order by dealer;";
		}
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_Trayectorias(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT * FROM ludus_charges
		where charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99)
		order by charge;";
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	
}
