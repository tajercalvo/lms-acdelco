<?php
Class Casos {
	//===============================================================
	// Retorna los datos de los sentinel segun los filtros
	//===============================================================
	function consultaDatosCasos($p,$sOrder,$sLimit){
		include_once('../../config/init_db.php');
		$sWhere = $p['sSearch'];
		$query_sql = "SELECT mo.sentinel_model,se.sentinel_service,sy.sentinel_system,u.first_name,u.last_name,u.identification,s.status,d.dealer,h.headquarter,a.area,ca.*
			FROM ludus_sentinel_case ca, ludus_sentinel_model mo, ludus_sentinel_service se, ludus_sentinel_system sy, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_dealers d, ludus_status s
			WHERE ca.sentinel_model_id = mo.sentinel_model_id AND ca.sentinel_service_id = se.sentinel_service_id AND ca.sentinel_system_id = sy.sentinel_system_id AND ca.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.area_id = a.area_id AND h.dealer_id = d.dealer_id AND ca.status_id = s.status_id";
			if($sWhere!=''){
				$query_sql .= " AND (ca.case_cat LIKE '%$sWhere%' OR ca.km_mot LIKE '%$sWhere%' OR mo.sentinel_model LIKE '%$sWhere%' OR se.sentinel_service LIKE '%$sWhere%' OR sy.sentinel_system LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%' OR s.status LIKE '%$sWhere%' OR ca.date_sentinel LIKE '%$sWhere%' OR ca.nro_ot LIKE '%$sWhere%' OR ca.date_ot LIKE '%$sWhere%' OR ca.vin LIKE '%$sWhere%' OR ca.nserie LIKE '%$sWhere%' OR ca.fail_code LIKE '%$sWhere%' OR ca.customer_complaint LIKE '%$sWhere%' OR ca.diagnostic LIKE '%$sWhere%' OR ca.cause_failure LIKE '%$sWhere%' OR ca.correction LIKE '%$sWhere%' OR ca.total LIKE '%$sWhere%' OR d.dealer LIKE '%$sWhere%' OR h.headquarter LIKE '%$sWhere%' OR a.area LIKE '%$sWhere%')";
			}
			@session_start();

			if(isset($p['dealer_id_s']) && $p['dealer_id_s']>0 ){
				$query_sql .= " AND h.dealer_id = ".$p['dealer_id_s']." ";
			}
			if(isset($p['sentinel_model_id_s']) && $p['sentinel_model_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_model_id = ".$p['sentinel_model_id_s']." ";
			}
			if(isset($p['status_id_s']) && $p['status_id_s']>0 ){
				$query_sql .= " AND ca.status_id = ".$p['status_id_s']." ";
			}
			if(isset($p['sentinel_service_id_s']) && $p['sentinel_service_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_service_id = ".$p['sentinel_service_id_s']." ";
			}
			if(isset($p['sentinel_system_id_s']) && $p['sentinel_system_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_system_id = ".$p['sentinel_system_id_s']." ";
			}
			if(isset($p['date_start_s']) && $p['date_start_s'] != '' ){
				$query_sql .= " AND ca.date_sentinel >= '".$p['date_start_s']."' ";
			}
			if(isset($p['date_end_s']) && $p['date_end_s'] != '' ){
				$query_sql .= " AND ca.date_sentinel <= '".$p['date_end_s']."' ";
			}
			if(isset($p['date_escalado_start']) && $p['date_escalado_start'] != '' ){
				$query_sql .= " AND ca.fec_qualityTot >= '".$p['date_escalado_start']."' ";
			}
			if(isset($p['date_escalado_end']) && $p['date_escalado_end'] != '' ){
				$query_sql .= " AND ca.fec_qualityTot <= '".$p['date_escalado_end']."' ";
			}
			if(isset($p['sub_status_id_s'])&&$p['sub_status_id_s']>0){
				$query_sql .= " AND ca.sentinel_case_id IN (SELECT g.sentinel_case_id
						FROM ludus_sentinel_gestion g, (SELECT sentinel_case_id, max(date_gestion) as date_gestion
						FROM ludus_sentinel_gestion
						WHERE sub_Status > 0
						GROUP BY sentinel_case_id) o, ludus_substatus s
						WHERE g.sentinel_case_id = o.sentinel_case_id AND g.date_gestion = o.date_gestion AND g.sub_status = s.substatus_id AND s.substatus_id = ".$p['sub_status_id_s'].")";
			}


			if($_SESSION['SentinelData']=='Colab'){
				$query_sql .= " AND (d.dealer_id = ".$_SESSION['dealer_id']." OR ca.status_id=7) ";
			}
			if($_SESSION['SentinelData']=='Garantias'){
				$query_sql .= " AND ca.use_parts = 'SI' ";
			}
			if($_SESSION['SentinelData']=='Calidad'){
				$query_sql .= " AND ca.status_id = 7 ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		// $DataBase_Acciones = new Database();
		// $Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$Rows_config = DB::query($query_sql);

		$Query_Substatus = "SELECT g.sentinel_case_id, g.sub_status, g.date_gestion, s.substatus, DATEDIFF(CURDATE(),g.date_gestion) as diasDif
						FROM ludus_sentinel_gestion g, (SELECT sentinel_case_id, max(date_gestion) as date_gestion
						FROM ludus_sentinel_gestion
						WHERE sub_Status > 0
						GROUP BY sentinel_case_id) o, ludus_substatus s
						WHERE g.sentinel_case_id = o.sentinel_case_id AND g.date_gestion = o.date_gestion AND g.sub_status = s.substatus_id";
		$Rows_SubStatus = DB::query($Query_Substatus);

		$Query_TodosStatus = "SELECT * FROM ludus_sentinel_gestion ORDER BY date_gestion DESC";
		$Rows_TodosStatus = DB::query($Query_TodosStatus);

		for($i=0;$i<count($Rows_config);$i++) {
			$sentinel_case_id = $Rows_config[$i]['sentinel_case_id'];
			for($x=0;$x<count($Rows_SubStatus);$x++) {
				if( $sentinel_case_id == $Rows_SubStatus[$x]['sentinel_case_id'] ){
					$Rows_config[$i]['SubStatus'][] = $Rows_SubStatus[$x];
				}
			}

			for($z=0;$z<count($Rows_TodosStatus);$z++) {
				if( $sentinel_case_id == $Rows_TodosStatus[$z]['sentinel_case_id'] ){
					$Rows_config[$i]['StatusComplete'][] = $Rows_TodosStatus[$z];
				}
			}
		}

		// unset($DataBase_Acciones);
		return $Rows_config;
	}
	//===============================================================
	// Retorna la sabana de datos para descargar de los casos sentinel
	//===============================================================
	function consultaDatosCasos_para_descarga(){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');

		if (isset($_SESSION['idUsuario'])) {
		$query_sql = "SELECT  ca.*, d.dealer, mo.sentinel_model, ca.customer_complaint, ca.date_creation, s.status
			FROM ludus_sentinel_case ca, ludus_sentinel_model mo, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_status s
			WHERE ca.sentinel_model_id = mo.sentinel_model_id
			AND ca.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND ca.status_id = s.status_id
			order by ca.date_creation desc";
			@session_start();
			if($_SESSION['SentinelData']=='Colab'){
				$query_sql .= " AND (d.dealer_id = ".$_SESSION['dealer_id']." OR ca.status_id=7) ";
			}
			if($_SESSION['SentinelData']=='Garantias'){
				$query_sql .= " AND ca.use_parts = 'SI' ";
			}
			if($_SESSION['SentinelData']=='Calidad'){
				$query_sql .= " AND ca.status_id = 7 ";
			}
			# code...
		}else{
			return 'sin_session';
		}


		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_substatus = "SELECT g.sentinel_case_id, g.sub_status, g.date_gestion, s.substatus, DATEDIFF(CURDATE(),g.date_gestion) as diasDif
						FROM ludus_sentinel_gestion g, (SELECT sentinel_case_id, max(date_gestion) as date_gestion
						FROM ludus_sentinel_gestion
						WHERE sub_Status > 0
						GROUP BY sentinel_case_id) o, ludus_substatus s
						WHERE g.sentinel_case_id = o.sentinel_case_id AND g.date_gestion = o.date_gestion AND g.sub_status = s.substatus_id";
		$Rows_substatus = $DataBase_Acciones->SQL_SelectMultipleRows($query_substatus);

		/*$Query_Substatus = "SELECT g.sentinel_case_id, g.sub_status, g.date_gestion, s.substatus, DATEDIFF(CURDATE(),g.date_gestion) as diasDif
						FROM ludus_sentinel_gestion g, (SELECT sentinel_case_id, max(date_gestion) as date_gestion
						FROM ludus_sentinel_gestion
						WHERE sub_Status > 0
						GROUP BY sentinel_case_id) o, ludus_substatus s
						WHERE g.sentinel_case_id = o.sentinel_case_id AND g.date_gestion = o.date_gestion AND g.sub_status = s.substatus_id";
		$Rows_SubStatus = DB::query($Query_Substatus);*/

		// print_r($Rows_substatus);
		// print_r($Rows_config);

		foreach ($Rows_substatus as $key => $value) {

			foreach ($Rows_config as $key2 => $value2) {
				if ($value['sentinel_case_id'] == $value2['sentinel_case_id']) {
					$Rows_config[$key2]['sub_estado'] = $value['substatus'];
				}
			}

		}

		/*$Query_Substatus = "SELECT g.sentinel_case_id, g.sub_status, g.date_gestion, s.substatus
						FROM ludus_sentinel_gestion g, (SELECT sentinel_case_id, max(date_gestion) as date_gestion
						FROM ludus_sentinel_gestion
						WHERE sub_Status > 0
						GROUP BY sentinel_case_id) o, ludus_substatus s
						WHERE g.sentinel_case_id = o.sentinel_case_id AND g.date_gestion = o.date_gestion AND g.sub_status = s.substatus_id";
		$Rows_SubStatus = $DataBase_Acciones->SQL_SelectMultipleRows($Query_Substatus);

		for($i=0;$i<count($Rows_config);$i++) {
			$sentinel_case_id = $Rows_config[$i]['sentinel_case_id'];
			for($x=0;$x<count($Rows_SubStatus);$x++) {
				if( $sentinel_case_id == $Rows_SubStatus[$x]['sentinel_case_id'] ){
					$Rows_config[$i]['SubStatus'][] = $Rows_SubStatus[$x];
				}
			}
		}*/

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//===============================================================
	// Consulta la cantidad de registros obtenidos segun la busqueda realizada
	//===============================================================
	function consultaCantidadFull($p,$sOrder,$sLimit){
		include_once('../../config/init_db.php');
		$sWhere = $p['sSearch'];
		$query_sql = "SELECT mo.sentinel_model,se.sentinel_service,sy.sentinel_system,u.first_name,u.last_name,u.identification,s.status,d.dealer,h.headquarter,a.area,ca.*
			FROM ludus_sentinel_case ca, ludus_sentinel_model mo, ludus_sentinel_service se, ludus_sentinel_system sy, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_dealers d, ludus_status s
			WHERE ca.sentinel_model_id = mo.sentinel_model_id AND ca.sentinel_service_id = se.sentinel_service_id AND ca.sentinel_system_id = sy.sentinel_system_id AND ca.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.area_id = a.area_id AND h.dealer_id = d.dealer_id AND ca.status_id = s.status_id";
			if($sWhere!=''){
				$query_sql .= " AND (ca.case_cat LIKE '%$sWhere%' OR ca.km_mot LIKE '%$sWhere%' OR mo.sentinel_model LIKE '%$sWhere%' OR se.sentinel_service LIKE '%$sWhere%' OR sy.sentinel_system LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%' OR s.status LIKE '%$sWhere%' OR ca.date_sentinel LIKE '%$sWhere%' OR ca.nro_ot LIKE '%$sWhere%' OR ca.date_ot LIKE '%$sWhere%' OR ca.vin LIKE '%$sWhere%' OR ca.nserie LIKE '%$sWhere%' OR ca.fail_code LIKE '%$sWhere%' OR ca.customer_complaint LIKE '%$sWhere%' OR ca.diagnostic LIKE '%$sWhere%' OR ca.cause_failure LIKE '%$sWhere%' OR ca.correction LIKE '%$sWhere%' OR ca.total LIKE '%$sWhere%' OR d.dealer LIKE '%$sWhere%' OR h.headquarter LIKE '%$sWhere%' OR a.area LIKE '%$sWhere%')";
			}
			@session_start();
			if(isset($p['dealer_id_s']) && $p['dealer_id_s']>0 ){
				$query_sql .= " AND h.dealer_id = ".$p['dealer_id_s']." ";
			}
			if(isset($p['sentinel_model_id_s']) && $p['sentinel_model_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_model_id = ".$p['sentinel_model_id_s']." ";
			}
			if(isset($p['status_id_s']) && $p['status_id_s']>0 ){
				$query_sql .= " AND ca.status_id = ".$p['status_id_s']." ";
			}
			if(isset($p['sentinel_service_id_s']) && $p['sentinel_service_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_service_id = ".$p['sentinel_service_id_s']." ";
			}
			if(isset($p['sentinel_system_id_s']) && $p['sentinel_system_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_system_id = ".$p['sentinel_system_id_s']." ";
			}
			if(isset($p['date_start_s']) && $p['date_start_s'] != '' ){
				$query_sql .= " AND ca.date_sentinel >= '".$p['date_start_s']."' ";
			}
			if(isset($p['date_end_s']) && $p['date_end_s'] != '' ){
				$query_sql .= " AND ca.date_sentinel <= '".$p['date_end_s']."' ";
			}
			if(isset($p['sub_status_id_s'])&&$p['sub_status_id_s']>0){
				$query_sql .= " AND ca.sentinel_case_id IN (SELECT g.sentinel_case_id
						FROM ludus_sentinel_gestion g, (SELECT sentinel_case_id, max(date_gestion) as date_gestion
						FROM ludus_sentinel_gestion
						WHERE sub_Status > 0
						GROUP BY sentinel_case_id) o, ludus_substatus s
						WHERE g.sentinel_case_id = o.sentinel_case_id AND g.date_gestion = o.date_gestion AND g.sub_status = s.substatus_id AND s.substatus_id = ".$p['sub_status_id_s'].")";
			}
			// if($_SESSION['SentinelData']=='Colab'){
			// 	$query_sql .= " AND d.dealer_id = ".$_SESSION['dealer_id'];
			// }
			if($_SESSION['SentinelData']=='Colab'){
				$query_sql .= " AND (d.dealer_id = ".$_SESSION['dealer_id']." OR ca.status_id=7) ";
			}
			if($_SESSION['SentinelData']=='Garantias'){
				$query_sql .= " AND ca.use_parts = 'SI' ";
			}
			if($_SESSION['SentinelData']=='Calidad'){
				$query_sql .= " AND ca.status_id = 7 ";
			}
			// echo("$query_sql");
		// $DataBase_Acciones = new Database();
		$Rows_config = count(DB::query($query_sql));
		// unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
			$query_sql = "SELECT mo.sentinel_model,se.sentinel_service,sy.sentinel_system,u.first_name,u.last_name,u.identification,s.status,d.dealer,h.headquarter,a.area,ca.*
			FROM ludus_sentinel_case ca, ludus_sentinel_model mo, ludus_sentinel_service se, ludus_sentinel_system sy, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_dealers d, ludus_status s
			WHERE ca.sentinel_model_id = mo.sentinel_model_id AND ca.sentinel_service_id = se.sentinel_service_id AND ca.sentinel_system_id = sy.sentinel_system_id AND ca.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.area_id = a.area_id AND h.dealer_id = d.dealer_id AND ca.status_id = s.status_id";//LIMIT 0,20
			@session_start();
			if(isset($_SESSION['dealer_id_s']) && $_SESSION['dealer_id_s']>0 ){
				$query_sql .= " AND h.dealer_id = ".$_SESSION['dealer_id_s']." ";
			}
			if(isset($_SESSION['sentinel_model_id_s']) && $_SESSION['sentinel_model_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_model_id = ".$_SESSION['sentinel_model_id_s']." ";
			}
			if(isset($_SESSION['status_id_s']) && $_SESSION['status_id_s']>0 ){
				$query_sql .= " AND ca.status_id = ".$_SESSION['status_id_s']." ";
			}
			if(isset($_SESSION['sentinel_service_id_s']) && $_SESSION['sentinel_service_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_service_id = ".$_SESSION['sentinel_service_id_s']." ";
			}
			if(isset($_SESSION['sentinel_system_id_s']) && $_SESSION['sentinel_system_id_s']>0 ){
				$query_sql .= " AND ca.sentinel_system_id = ".$_SESSION['sentinel_system_id_s']." ";
			}
			if(isset($_SESSION['date_start_s']) && $_SESSION['date_start_s'] != '' ){
				$query_sql .= " AND ca.date_sentinel >= '".$_SESSION['date_start_s']."' ";
			}
			if(isset($_SESSION['date_end_s']) && $_SESSION['date_end_s'] != '' ){
				$query_sql .= " AND ca.date_sentinel <= '".$_SESSION['date_end_s']."' ";
			}
			if(isset($_SESSION['sub_status_id_s'])&&$_SESSION['sub_status_id_s']>0){
				$query_sql .= " AND ca.sentinel_case_id IN (SELECT g.sentinel_case_id
						FROM ludus_sentinel_gestion g, (SELECT sentinel_case_id, max(date_gestion) as date_gestion
						FROM ludus_sentinel_gestion
						WHERE sub_Status > 0
						GROUP BY sentinel_case_id) o, ludus_substatus s
						WHERE g.sentinel_case_id = o.sentinel_case_id AND g.date_gestion = o.date_gestion AND g.sub_status = s.substatus_id AND s.substatus_id = ".$_SESSION['sub_status_id_s'].")";
			}
			// if($_SESSION['SentinelData']=='Colab'){
			// 	$query_sql .= " AND d.dealer_id = ".$_SESSION['dealer_id'];
			// }
			if($_SESSION['SentinelData']=='Colab'){
				$query_sql .= " AND (d.dealer_id = ".$_SESSION['dealer_id']." OR ca.status_id=7) ";
			}
			if($_SESSION['SentinelData']=='Garantias'){
				$query_sql .= " AND ca.use_parts = 'SI' ";
			}
			if($_SESSION['SentinelData']=='Calidad'){
				$query_sql .= " AND ca.status_id = 7 ";
			}
			//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearCasos($date_sentinel,$nro_ot,$date_ot,$photos,$video,$history,$sentinel_model_id,$vin,$sentinel_application_id,$km,$garranty_start,$sentinel_service_id,$transmission,$configuration,$sentinel_system_id,$nserie,$sentinel_check_id,$fail_code,$customer_complaint,$diagnostic,$cause_failure,$correction,$use_parts,$parts_description,$km_mot,$cost_workforce,$cost_parts,$cost_tot,$total,$model_year,$car_version,$cat_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_sentinel_case (date_sentinel,nro_ot,date_ot,photos,video,history,sentinel_model_id,vin,sentinel_application_id,km,garranty_start,sentinel_service_id,transmission,configuration,sentinel_system_id,nserie,sentinel_check_id,fail_code,customer_complaint,diagnostic,cause_failure,correction,use_parts,parts_description,km_mot,cost_workforce,cost_parts,cost_tot,total,user_id,date_creation,status_id,file,date_edition,user_ed,model_year,car_version,case_cat) VALUES ('$date_sentinel','$nro_ot','$date_ot','$photos','$video','$history','$sentinel_model_id','$vin','$sentinel_application_id','$km','$garranty_start','$sentinel_service_id','$transmission','$configuration','$sentinel_system_id','$nserie','$sentinel_check_id','$fail_code','$customer_complaint','$diagnostic','$cause_failure','$correction','$use_parts','$parts_description','$km_mot','$cost_workforce','$cost_parts','$cost_tot','$total','$idQuien',NOW(),5,'default.jpg',NOW(),'$idQuien','$model_year','$car_version','$cat_id')";
		//echo ($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarCasos($date_sentinel,$nro_ot,$date_ot,$photos,$video,$history,$sentinel_model_id,$vin,$sentinel_application_id,$km,$garranty_start,$sentinel_service_id,$transmission,$configuration,$sentinel_system_id,$nserie,$sentinel_check_id,$fail_code,$customer_complaint,$diagnostic,$cause_failure,$correction,$use_parts,$parts_description,$km_mot,$cost_workforce,$cost_parts,$cost_tot,$total,$id,$status_id,$model_year,$car_version,$date_requ_cdg,$date_start_cdg,$ok_first,$solution_contribute,$parts_cdg, $fec_analysis_part, $fec_retirement_part, $fec_destruction_part,$cat_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$difDias = "";
		if($date_start_cdg!=''){
			$datetime1 = date_create($date_requ_cdg);
			$datetime2 = date_create($date_start_cdg);
			$interval = date_diff($datetime1, $datetime2);
			$difDias = $interval->format('%a');
		}
		$updateSQL_ER = "UPDATE ludus_sentinel_case SET date_sentinel = '$date_sentinel',
						nro_ot = '$nro_ot',
						date_ot = '$date_ot',
						photos = '$photos',
						video = '$video',
						history = '$history',
						sentinel_model_id = '$sentinel_model_id',
						vin = '$vin',
						sentinel_application_id = '$sentinel_application_id',
						km = '$km',
						garranty_start = '$garranty_start',
						sentinel_service_id = '$sentinel_service_id',
						transmission = '$transmission',
						configuration = '$configuration',
						sentinel_system_id = '$sentinel_system_id',
						nserie = '$nserie',
						sentinel_check_id = '$sentinel_check_id',
						fail_code = '$fail_code',
						customer_complaint = '$customer_complaint',
						diagnostic = '$diagnostic',
						cause_failure = '$cause_failure',
						correction = '$correction',
						use_parts = '$use_parts',
						model_year = '$model_year',
						parts_description = '$parts_description',
						km_mot = '$km_mot',
						cost_workforce = '$cost_workforce',
						cost_parts = '$cost_parts',
						cost_tot = '$cost_tot',
						total = '$total',
						date_edition = NOW(),
						status_id = '$status_id',
						car_version = '$car_version',
						date_requ_cdg = '$date_requ_cdg',
						date_start_cdg = '$date_start_cdg',
						ok_first = '$ok_first',
						solution_contribute = '$solution_contribute',
						day_diference_cdg = '$difDias',
						parts_cdg = '$parts_cdg',
						fec_analysis_part = '$fec_analysis_part',
						fec_retirement_part = '$fec_retirement_part',
						fec_destruction_part = '$fec_destruction_part',
						case_cat = '$cat_id',
						user_ed = '$idQuien' WHERE sentinel_case_id = '$id'";
						//echo $updateSQL_ER;
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}

	//Consulta la fecha en que se recibíola parte
	function consultaFechaParte($idRegistro, $date_start_cdg){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "SELECT date_start_cdg FROM ludus_sentinel_case where sentinel_case_id = $idRegistro;";
		$resultado = $DataBase_Log->SQL_SelectRows($updateSQL_ER);
		// echo "fecha enviada".$date_start_cdg;
		// echo "fecha consultada".$resultado['date_start_cdg'];
		if ($resultado['date_start_cdg'] == $date_start_cdg ) {
			$resultado = 'NO';
		}else{
			$resultado = 'SI';
		}
		return $resultado;
	}

	function BorraOT_Ant($idRegistro){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		// $updateSQL_ER = "DELETE FROM  ludus_sentinel_vin_ot WHERE sentinel_case_id = '$idRegistro'";
		// $resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		// return $resultado;
		return 1;
	}
	function CrearVinOT($idRegistro,$vin,$km,$date_ot,$comments){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_sentinel_vin_ot (sentinel_case_id,vin,km,date_ot,comments,date_creation,user_id) VALUES ('$idRegistro','$vin','$km','$date_ot','$comments',NOW(),'$idQuien')";
		//echo ($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}

	function actualizarComentarios($comments, $id_comments){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "UPDATE ludus_sentinel_vin_ot SET comments='$comments' WHERE sentinel_vin_ot_id='$id_comments';";
		$resultado = $DataBase_Log->SQL_Update($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}

	function CrearVinOT_O($idRegistro,$vin,$km,$date_ot,$comments){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_sentinel_vin_ot (sentinel_case_id,vin,km,date_ot,comments,date_creation,user_id) VALUES ('$idRegistro','$vin','$km','$date_ot','$comments',NOW(),'$idQuien')";
		//echo ($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function CrearVinQA($idRegistro,$vin,$date_ot,$action){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_sentinel_vin_qa (sentinel_gestion_id,vin,date,action,date_creation,user_id) VALUES ('$idRegistro','$vin','$date_ot','$action',NOW(),'$idQuien')";
		//echo ($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, s.status
			FROM ludus_sentinel_case c, ludus_status s
			WHERE c.sentinel_case_id = '$idRegistro' AND c.status_id = s.status_id";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function ConsultaVinOT($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT o.*, u.first_name, u.last_name, u.identification, h.headquarter, d.dealer, o.date_creation
			FROM ludus_sentinel_vin_ot o, ludus_users u, ludus_headquarters h, ludus_dealers d
			WHERE sentinel_case_id = $idRegistro AND o.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaBiblioteca($ref,$stats){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if($stats=="status"){
			$query_sql = "SELECT *
			FROM ludus_sentinel_".$ref." WHERE status_id = 1 ORDER BY sentinel_".$ref;//LIMIT 0,20
		}else{
			$query_sql = "SELECT *
			FROM ludus_sentinel_".$ref." ORDER BY sentinel_".$ref;//LIMIT 0,20
		}

		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaEstados(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_status WHERE status_id > 3 AND status_id < 10 ORDER BY status";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaSubEstados(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_substatus WHERE substatus_id < 6 ORDER BY substatus";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function actualizaImagen($sentinel_case_id,$imagen){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_sentinel_case SET file = '$imagen' WHERE sentinel_case_id = '$sentinel_case_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		//echo $updateSQL_ER;
		return $resultado;
		unset($DataBase_Log);
	}
	function CrearArchivoJS($sentinel_case_id, $file, $type){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_sentinel_files (file,sentinel_case_id,type) VALUES ('$file','$sentinel_case_id','$type')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function CrearArchivo($sentinel_case_id, $file, $type){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_sentinel_files (file,sentinel_case_id,type) VALUES ('$file','$sentinel_case_id','$type')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function BorrarArchivo($sentinel_file_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "DELETE FROM ludus_sentinel_files WHERE sentinel_file_id = '$sentinel_file_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function ArchivosCargados($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_sentinel_files c
			WHERE c.sentinel_case_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function CrearGestion($sentinel_case_id, $gestion, $status_act, $status_id_new, $date_request, $date_delivered, $date_scrap, $subEstado, $nomArchivo){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_sentinel_gestion (sentinel_case_id,status_id_old,status_id_new,user_id,date_gestion,gestion,sub_status,file_gestion) VALUES ('$sentinel_case_id','$status_act','$status_id_new',$idQuien,NOW(),'$gestion','$subEstado','$nomArchivo')";
		if($date_request!=""&&$date_delivered!=""){
			$insertSQL_EE = "INSERT INTO ludus_sentinel_gestion (sentinel_case_id,status_id_old,status_id_new,user_id,date_gestion,gestion,date_request,date_delivered) VALUES ('$sentinel_case_id','$status_act','$status_id_new',$idQuien,NOW(),'$gestion','$date_request','$date_delivered')";
		}elseif($date_request!=""){
			$insertSQL_EE = "INSERT INTO ludus_sentinel_gestion (sentinel_case_id,status_id_old,status_id_new,user_id,date_gestion,gestion,date_request) VALUES ('$sentinel_case_id','$status_act','$status_id_new',$idQuien,NOW(),'$gestion','$date_request')";
		}elseif($date_delivered!=""){
			$insertSQL_EE = "INSERT INTO ludus_sentinel_gestion (sentinel_case_id,status_id_old,status_id_new,user_id,date_gestion,gestion,date_delivered) VALUES ('$sentinel_case_id','$status_act','$status_id_new',$idQuien,NOW(),'$gestion','$date_delivered')";
		}elseif($date_scrap!=""){
			$insertSQL_EE = "INSERT INTO ludus_sentinel_gestion (sentinel_case_id,status_id_old,status_id_new,user_id,date_gestion,gestion,date_scrap) VALUES ('$sentinel_case_id','$status_act','$status_id_new',$idQuien,NOW(),'$gestion','$date_scrap')";
		}
		//echo($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		$update = "UPDATE ludus_sentinel_case SET status_id = $status_id_new WHERE sentinel_case_id = $sentinel_case_id ";
		$resultado_upd = $DataBase_Log->SQL_Insert($update);
		unset($DataBase_Log);
		return $resultado;
	}
	function GestionesCargadas($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, s.status as status_old, l.status as status_new, u.first_name, u.last_name, u.identification
			FROM ludus_sentinel_gestion c, ludus_status s, ludus_status l, ludus_users u
			WHERE c.sentinel_case_id = '$idRegistro' AND
			c.status_id_old = s.status_id AND
			c.status_id_new = l.status_id AND
			c.user_id = u.user_id
			ORDER BY c.date_gestion
			";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$QueryDet = "SELECT * FROM ludus_sentinel_vin_qa ORDER BY sentinel_gestion_id";
		$Rows_Detail = $DataBase_Class->SQL_SelectMultipleRows($QueryDet);

		$QuerySubS = "SELECT * FROM ludus_substatus ORDER BY substatus_id";
		$Rows_SubStatus = $DataBase_Class->SQL_SelectMultipleRows($QuerySubS);

		for($i=0;$i<count($Rows_config);$i++) {
			$sentinel_gestion_id = $Rows_config[$i]['sentinel_gestion_id'];
			//Cargos
				for($y=0;$y<count($Rows_Detail);$y++) {
					if( $sentinel_gestion_id == $Rows_Detail[$y]['sentinel_gestion_id'] ){
						$Rows_config[$i]['Detail'][] = $Rows_Detail[$y];
					}
				}
			//Cargos
			if($Rows_config[$i]['sub_status']>0){
				for($z=0;$z<count($Rows_SubStatus);$z++) {
					if( $Rows_config[$i]['sub_status'] == $Rows_SubStatus[$z]['substatus_id'] ){
						$Rows_config[$i]['SubStatus'] = $Rows_SubStatus[$z]['substatus'];
					}
				}
			}
		}
		unset($DataBase_Class);
		return $Rows_config;
	}
	function GetEmailAdminSentinel($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT u.email
					FROM ludus_roles_users r, ludus_users u
					WHERE r.rol_id = 9 AND r.user_id = u.user_id AND u.status_id = 1 AND u.email like '%@%'";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function GetEmailCalidadSentinel($prof,$IdCaso){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT u.email
						FROM ludus_sentinel_case s, ludus_sentinel_model_users m, ludus_users u
						WHERE s.sentinel_case_id = $IdCaso AND (s.sentinel_model_id = m.sentinel_model_id OR m.sentinel_model_id = 48 ) AND m.user_id = u.user_id AND m.status_id = 1 AND u.status_id = 1 AND u.email like '%@%'";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function GetDetailsCompleteSentinel($prof,$IdCaso){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}

		$query_sql = "SELECT s.sentinel_case_id, s.customer_complaint, m.sentinel_model, s.garranty_start, s.diagnostic, s.cause_failure, s.correction
						FROM ludus_sentinel_case s, ludus_sentinel_model m
						WHERE s.sentinel_case_id = $IdCaso AND s.sentinel_model_id = m.sentinel_model_id";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCircular(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_newsletters c
			WHERE c.type = '2' and c.status_id = 1 ORDER BY date_edition DESC";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaConcesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_dealers
			WHERE status_id = 1 ORDER BY dealer";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
