<?php
Class Multimedias_Detail {
	function consultaGalerias($media_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_media_detail m, ludus_users u
						WHERE m.media_id = '$media_id' AND m.editor = u.user_id ORDER BY date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_media_detail m, ludus_users u
						WHERE m.media_id = '$media_id' AND m.editor = u.user_id AND m.status_id = 1 ORDER BY date_edition DESC";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_Likes = "SELECT cu.*, u.first_name, u.last_name
						FROM ludus_media_likes cu, ludus_users u
						WHERE cu.user_id = u.user_id
						ORDER BY cu.date_edition ";
		$Rows_Likes = $DataBase_Acciones->SQL_SelectMultipleRows($query_Likes);

		$query_Views = "SELECT cu.*, u.first_name, u.last_name
						FROM ludus_media_views cu, ludus_users u
						WHERE cu.user_id = u.user_id
						ORDER BY cu.date_edition ";
		$Rows_Views = $DataBase_Acciones->SQL_SelectMultipleRows($query_Views);

		for($i=0;$i<count($Rows_config);$i++) {
			$media_detail_id = $Rows_config[$i]['media_detail_id'];
			//Cargos
				for($y=0;$y<count($Rows_Likes);$y++) {
					if( $media_detail_id == $Rows_Likes[$y]['media_detail_id'] ){
						$Rows_config[$i]['likes_data'][] = $Rows_Likes[$y];
					}
				}
				for($y=0;$y<count($Rows_Views);$y++) {
					if( $media_detail_id == $Rows_Views[$y]['media_detail_id'] ){
						$Rows_config[$i]['views_data'][] = $Rows_Views[$y];
					}
				}
			//Cargos
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($media_id){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_media_detail m, ludus_users u
						WHERE m.media_id = '$media_id' AND m.editor = u.user_id ORDER BY date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_media_detail m, ludus_users u
						WHERE m.media_id = '$media_id' AND m.editor = u.user_id AND m.status_id = 1 ORDER BY date_edition DESC";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad($media_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_media_detail m, ludus_users u
						WHERE m.media_id = '$media_id' AND m.editor = u.user_id ORDER BY date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_media_detail m, ludus_users u
						WHERE m.media_id = '$media_id' AND m.editor = u.user_id AND m.status_id = 1 ORDER BY date_edition DESC";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearMultimedias($media,$description,$id_gal,$image,$video){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$insertSQL_EE = "INSERT INTO `ludus_media_detail`
		(media_id,media_detail,description,video,date_edition,editor,status_id,source,likes)
		VALUES ($id_gal,'$media','$description','$video','$NOW_data','$idQuien','1','$image',0)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarMultimedias($id,$media,$description,$video,$image,$estado){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_media_detail`
						SET media_detail = '$media',
							description = '$description',
							editor = '$idQuien',
							date_edition = '$NOW_data',
							video = '$video',
							source = '$image',
							status_id = '$estado'
						WHERE media_detail_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_modules c
			WHERE c.module_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatoGaleria($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_media c
			WHERE c.media_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearLike($media_detail_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO `ludus_media_likes` (media_detail_id,user_id,date_edition) VALUES ('$media_detail_id','$idQuien','$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Insert($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE `ludus_media_detail`
						SET likes = likes+1
						WHERE media_detail_id = '$media_detail_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}


	//Cantidad de veces que se vio un video
	function CrearMediaView($media_detail_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_media_views (media_views_id,media_detail_id,user_id,date_edition) VALUES (null,'$media_detail_id','$idQuien','$NOW_data')";
		//echo($insertSQL_ER);
		$resultado_IN = $DataBase_Log->SQL_Insert($insertSQL_ER);

		// Actualizar las veces que se vio un video en la tabla ludus_media_detail
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE `ludus_media_detail`
						SET views  = views +1
						WHERE media_detail_id = '$media_detail_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}

}
