<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
Class Cargos {
	
	function RegistrarNota($Nota,$Modulo_id){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$approval = 'NO';
		if($Nota>=80){
			$approval = 'SI';
		}else{
			$approval = 'NO';
		}
		$insertSQL_EE = "INSERT INTO ludus_modules_results_usr (score,user_id,module_id,approval,file,date_creation,status_id,creator,invitation_id)
		VALUES ('$Nota','$idQuien','$Modulo_id','$approval','','$NOW_data','1','$idQuien',0)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}

	function crearCurso($course, $description,$prerequisites,$type,$specialty_id,$target){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_courses (course,status_id,creator,editor,date_creation,date_edition,description,prerequisites,type,image,specialty_id,target)
									VALUES
									('$course','2','$idQuien','$idQuien','$NOW_data','$NOW_data','$description','$prerequisites','$type','default.png','$specialty_id','$target')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);

		$newcode = $type . $resultado;
		if($resultado){
			$updateSQL_ER = "UPDATE ludus_courses SET newcode='$newcode' WHERE course_id='$resultado' ";
			$resultado_update = $DataBase_Log->SQL_Update($updateSQL_ER);
		}

		return $resultado;
	}

	public static function progress_bar($course_id){
		include_once('../../config/init_db.php');

		$sql_progress_bar = "SELECT max(course_id), 'Una trayectoria asociada' as caso, CASE
									WHEN course_id IS NULL THEN 'NO'
									ELSE 'SI'
									END AS tiene FROM ludus_charges_courses where course_id = $course_id
									union
								SELECT  max(course_id), 'Un módulo creado' as caso, CASE
									WHEN course_id IS NULL THEN 'NO'
									ELSE 'SI'
									END AS tiene  FROM ludus_modules where course_id = $course_id;";
		// 	union
		// SELECT max(course_id), 'Preguntas de profundización' as caso, CASE
		// 	WHEN course_id IS NULL THEN 'NO'
		// 	ELSE 'SI'
		// 	END AS tiene FROM ludus_reviews_r where course_id = $course_id
		// 	union
		// SELECT max(course_id), 'Una evaluación' as caso, CASE
		// 	WHEN course_id IS NULL THEN 'NO'
		// 	ELSE 'SI'
		// 	END AS tiene  FROM ludus_reviews_courses where course_id = $course_id
		// 	union
		// SELECT max(course_id), 'Material de apoyo' as caso, CASE
		// 	WHEN course_id IS NULL THEN 'NO'
		// 	ELSE 'SI'
		// 	END AS tiene FROM ludus_course_files where course_id = $course_id
		$Rows_progress_bar = DB::query($sql_progress_bar);
		// print_r( $Rows_progress_bar );
		// die();
		$progreso = 0;
		$no_tiene = [];
		foreach ($Rows_progress_bar as $key => $value) {
			if($value['tiene'] == 'SI'){
				$progreso += 1;
			}else{
				$no_tiene[] = $value['caso'];
			}
		}

		// *! se modifica de 5 a 3 como número top de validaciones
		if($progreso == 2){
			$datos_listos = ['progreso' => $progreso];
		}else{
			$datos_listos = ['progreso' => $progreso, 'datos'=> $no_tiene];
		}

		return $datos_listos;

	}

	function Actualizarcargos($id,$course,$status_id,$description,$prerequisites,$type,$specialty_id,$target){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_courses SET course = '$course', status_id = '$status_id', editor = '$idQuien', date_edition = NOW(), description = '$description', prerequisites = '$prerequisites', type = '$type', specialty_id = '$specialty_id', target = '$target' WHERE course_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		$DataBase_Log = new Database();
		$sql = "SELECT newcode FROM ludus_courses where course_id = $id;";
		$course_id = $DataBase_Log->SQL_SelectRows($sql);

		$res = array();
		if($resultado){
			$res['resultado'] = 'SI';
			$res['id'] = $course_id['newcode'];
		}else{
			$res['resultado'] = 'NO';
			$res['id'] = '';
		}
		
		

		return $res;
	}

	//Elimina todos los archvos asociados a un curso
	function eliminar_archivos_vct($course_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$sql = "DELETE FROM ludus_courses_files WHERE course_id = '$course_id' and archivo <> '.preg';";
		$resultado = $DataBase_Log->SQL_Update($sql);
		return $resultado;
	}

	//Elimina un archivo de un curso
	function eliminar_archivo_vct($coursefile_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$sql = "DELETE FROM ludus_courses_files WHERE coursefile_id = $coursefile_id;";
		$resultado = $DataBase_Log->SQL_Update($sql);
		return $resultado;
	}

	function eliminar_respuesta_pregunta_vct( $coursefile_id ){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		
		$sql = "SELECT question_id from ludus_courses_files WHERE coursefile_id = $coursefile_id";
		$res = $DataBase_Log->SQL_SelectRows($sql);
		$question_id = $res['question_id'];

		$sql = "DELETE from ludus_respuestas_preguntas_cursos WHERE question_id = $question_id";
		$resultado = $DataBase_Log->SQL_Update($sql);
		return $resultado;
	}

	function consultar_imagen_vct($coursefile_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$sql = "SELECT CASE
						WHEN image = archivo THEN 'iguales'
						ELSE 'diferentes'
						END
						AS valida
					FROM ludus_courses_files
					where coursefile_id = $coursefile_id;";
		$resultado = $DataBase_Log->SQL_SelectRows($sql);

		return $resultado;
	}

	function Crear_archivos_vct($file, $name, $course_id, $name_file, $question_id = 0){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return false;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		if (strpos($name, '.mp4') || strpos($name, '.mov') || strpos($name, '.avi') || strpos($name, '.mkv') || strpos($name, '.flv') ) {
			$image = 'default_video.png';
			$archivo = $name;
		}else if (strpos($name, '.pdf')){
			$image = 'default_pdf.png';
			$archivo = $name;
		}else if (strpos($name, '.preg')){
			$image = 'default_pregunta.png';
			$archivo = substr($name, -5); // extraemos la extención .preg
			$question_id = $name_file;
			$name_file = substr($name, 0, -5); // quitamos la extención .preg
			//$archivo = $name;
		}else{
			$image = $name;
			$archivo = $name;
		}

		if($name_file == ''){
			$name_file = substr($name, 19); // quitamos la fecha y dejamos solamente el nombre de la diapositiva
		}
		$updateSQL_ER = "INSERT INTO ludus_courses_files
								(
								file,
								name,
								image,
								archivo,
								date_creation,
								course_id,
								creador_id,
								status_id,
								question_id)
								VALUES
								(
								'$file',
								'$name_file',
								'$image',
								'$archivo',
								NOW(),
								$course_id,
								$idQuien,
								2,
								$question_id);";
		$resultado = $DataBase_Log->SQL_Insert($updateSQL_ER);
		unset($DataBase_Log);
		return $resultado;
	}

	function actualizar_archivos_vct($file, $name, $image, $archivo, $course_id, $coursefile_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien 		= $_SESSION['idUsuario'];
		$DataBase_Log 	= new Database();
		$idQuien 		= $_SESSION['idUsuario'];
		$image 		= $image == ''? '' :" image 	= '$image', ";
		$archivo 		= $archivo == ''? '' :" archivo 	= '$archivo', ";
		$updateSQL_ER 	= "UPDATE ludus_courses_files
									SET
									file 			= '$file',
									name 			= '$name',
									$image
									$archivo
									date_creation 	=  concat(CURDATE(),' ' ,CURTIME()),
									course_id 		= '$course_id',
									creador_id 		= '$idQuien'
									WHERE coursefile_id = '$coursefile_id';";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		unset($DataBase_Log);
		return $resultado;
	}

	function get_archivos_vct($course_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$sql_select_archivos = "SELECT * FROM ludus_courses_files where course_id = '{$course_id}' order by file asc";
		$datos_archivos = $DataBase_Log->SQL_SelectMultipleRows($sql_select_archivos);
		$sql_select_preguntas = "SELECT id, question_id, course_id, answer, result FROM ludus_respuestas_preguntas_cursos where course_id = '{$course_id}'";
		$datos_preguntas = $DataBase_Log->SQL_SelectMultipleRows($sql_select_preguntas);
		foreach ($datos_archivos as $key => $value) {
			$value['question_id'];
			foreach ($datos_preguntas as $key2 => $value2) {
				if($value['question_id'] == $value2['question_id']){
					$datos_archivos[$key]['respuestas'][] = $value2;
				}
			}
		}
		// print_r($datos_archivos);
		// return;
		unset($DataBase_Log);
		return $datos_archivos;
	}

	function get_max_id_question($course_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return false;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		$sql_select = "SELECT max(question_id) as question_id from ludus_courses_files;";
		$datos = $DataBase_Log->SQL_SelectRows($sql_select);
		unset($DataBase_Log);
		return $datos;
	}

	function crear_pregunta($course_id, $question){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return false;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		$sql_select = "INSERT INTO ludus_preguntas_cursos
							(
							course_id,
							question,
							date_creation,
							creator,
							status_id)
							VALUES
							(
							$course_id,
							'$question',
							NOW(),
							$idQuien,
							1);";
		$datos = $DataBase_Log->SQL_Insert($sql_select);
		unset($DataBase_Log);
		return $datos;
	}

	function crear_respuestas($course_id, $question_id, $answer, $result){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return false;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		$sql_select = "INSERT INTO ludus_respuestas_preguntas_cursos
										(
										question_id,
										course_id,
										answer,
										result,
										date_creation,
										creator,
										status_id)
										VALUES
										(
										$question_id,
										$course_id,
										'$answer',
										'$result',
										NOW(),
										$idQuien,
										1);";
		$datos = $DataBase_Log->SQL_Insert($sql_select);
		unset($DataBase_Log);
		return $datos;
	}

	// ELiminar las las respuestas
	function eliminar_respuestas($question_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return false;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		$sql_delete = "DELETE FROM ludus_respuestas_preguntas_cursos WHERE question_id = $question_id;";
		$datos = $DataBase_Log->SQL_Update($sql_delete);
		unset($DataBase_Log);
		return $datos;
	}

	// Actualiza preguntas y respuetsas
	function update_pregunta($question_id, $name){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$sql_delete = "UPDATE ludus_courses_files set name = '$name'  where question_id = $question_id;";
		$datos = $DataBase_Log->SQL_Update($sql_delete);
		unset($DataBase_Log);
		return $datos;
	}

	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_courses c
			WHERE c.course_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function actualizaImagen($course_id,$imagen){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_courses SET image = '$imagen', editor = '$idQuien', date_edition = '$NOW_data' WHERE course_id = '$course_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function consultaEspecialidades($prof){
		
		include_once('../config/database.php');
		include_once('../config/config.php');
		
		$query_sql = "SELECT c.*
						FROM ludus_specialties c
						WHERE c.status_id = 1
						ORDER BY c.specialty";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}

	// function consultaRegistroDetailModulos($idRegistro){
	// 	include_once('../config/database.php');
	// 	include_once('../config/config.php');
	// 	$query_sql = "SELECT m.*
	// 		FROM ludus_modules m
	// 		WHERE m.course_id = '$idRegistro' AND m.status_id = 1 ";//LIMIT 0,20
	// 	$DataBase_Class = new Database();
	// 	$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
	// 	unset($DataBase_Acciones);
	// 	return $Rows_config;
	// }
	// function consultaPasoCurso($idCourse,$idUsuario){
	// 	@session_start();
	// 	if($idUsuario!=0){
	// 		$idQuien = $idUsuario;
	// 	}else{
	// 		$idQuien = $_SESSION['idUsuario'];
	// 	}
	// 	include_once('../config/database.php');
	// 	include_once('../config/config.php');
	// 	$query_sql = "SELECT r.*
	// 				FROM ludus_modules_results_usr r, ludus_modules m
	// 				WHERE m.course_id = '$idCourse' AND m.module_id = r.module_id AND
	// 				r.score > 79 AND r.user_id = $idQuien";//LIMIT 0,20
	// 	$DataBase_Acciones = new Database();
	// 	$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
	// 	unset($DataBase_Acciones);
	// 	return $Rows_config;
	// }


	
}
