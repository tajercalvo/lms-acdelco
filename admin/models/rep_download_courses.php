<?php


class Asistentesviews{


		public static function consultar_Views(){

			include_once('../../config/init_db.php');

			$query = "SELECT course, course_id FROM ludus_courses where type  like '%ibt%' or type like '%vct%';";
			$resultado = DB::query($query);
			return $resultado;
		}

		public static function consultar_user( $course_id ){
			include_once('../../config/init_db.php');

			$query = "SELECT u.first_name nombre,
						    u.last_name apellido,
						    u.identification identificacion,
						    d.dealer concesionario,
						    h.headquarter sede,
						    z.zone zona,
                            el.name_file,
                            max(el.date_creation) date_creation,
                            cf.name,
                            count(*) descargas
                            FROM ludus_download_element el
							INNER JOIN  ludus_course_files cf
									on el.id_element = cf.course_file_id and cf.course_id = $course_id
							INNER JOIN
                            ludus_users u on u.user_id = el.creator
                            INNER JOIN
						    ludus_headquarters h ON h.headquarter_id = u.headquarter_id
						        INNER JOIN
						    ludus_dealers d ON d.dealer_id = h.dealer_id
						        INNER JOIN
						    ludus_areas a ON a.area_id = h.area_id
						        INNER JOIN
						    ludus_zone z ON z.zone_id = a.zone_id
                            group by u.user_id, el.id_element
                            order by el.date_creation desc";
			$resultado = DB::query($query);
			return $resultado;

	     }


}

// $consulta = new Asistentesviews();
// $consulta -> consultar_views();






