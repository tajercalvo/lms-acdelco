<?php
Class Circulares {
	function consultaCirculares($cargos_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
						FROM ludus_newsletters m, ludus_users s
						WHERE m.user_id = s.user_id AND m.type = 2
						ORDER BY m.date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
						FROM ludus_newsletters m, ludus_users s, ludus_newsletters_charges c
						WHERE m.status_id = 1 AND m.user_id = s.user_id AND m.newsletter_id = c.newsletter_id AND c.charge_id IN ($cargos_id) AND m.type = 2
						ORDER BY m.date_edition DESC";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearCircular($title,$newsletter,$archivo){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$insertSQL_EE = "INSERT INTO `ludus_newsletters` 
		(title,newsletter,date_edition,user_id,status_id,visits,file_data,type) 
		VALUES ('$title','$newsletter','$NOW_data','$idQuien','1',0,'$archivo','2')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarCircular($id,$title,$newsletter,$estado,$archivo){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_newsletters` 
						SET title = '$title', 
							newsletter = '$newsletter', 
							user_id = '$idQuien', 
							date_edition = '$NOW_data',
							file_data = '$archivo',
							type = '2',
							status_id = '$estado'
						WHERE newsletter_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}

	//Funcion para inactivar una circular
	function inactivarCircular($id){
		// @session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// $NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$statusIdCircular = "SELECT status_id FROM ludus_newsletters where newsletter_id = $id;";
		$estadoActual = $DataBase_Log->SQL_SelectRows($statusIdCircular);

		if ($estadoActual['status_id'] == 1) {
			$estado = 2;
		}else{
			$estado = 1;
		}
		
		$updateSQL_ER = "UPDATE ludus_newsletters SET status_id = '$estado' WHERE newsletter_id = '$id';";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		$SQLestadoFinal = "SELECT status_id FROM ludus_newsletters where newsletter_id = $id;";
		$estadoActual = $DataBase_Log->SQL_SelectRows($SQLestadoFinal);
		
		if ($resultado>0) {
			$resultado = [
				    "resultado" => "SI",
				    "estadoFinal" => $estadoActual['status_id'],
				];
		}else{
			$resultado = [
				    "resultado" => "NO",
				];
		}
		return $resultado;
	}
	
	function consultaCircular($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_newsletters c
			WHERE c.newsletter_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		$query_Charges = "SELECT charge_id
						FROM ludus_newsletters_charges
						WHERE newsletter_id = '$idRegistro'
						ORDER BY charge_id ";
		$Rows_Charges = $DataBase_Class->SQL_SelectMultipleRows($query_Charges);
		$Rows_config['charges'][] = $Rows_Charges;
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearVisitado($newsletter_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO `ludus_newsletters_visites` (newsletter_id,user_id,date_edition) VALUES ('$newsletter_id','$idQuien','$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE `ludus_newsletters` 
						SET visits = visits+1 
						WHERE newsletter_id = '$newsletter_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		}
		return $resultado_IN;
	}
	function BorraCargos($newsletter_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$updateSQL_ER = "DELETE FROM ludus_newsletters_charges WHERE newsletter_id = '$newsletter_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function CrearCargos($newsletter_id,$charge_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_newsletters_charges VALUES ($newsletter_id,$charge_id)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
}
