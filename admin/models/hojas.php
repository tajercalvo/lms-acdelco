<?php
Class Hojas {
	function consultaDatoshojas($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$query_sql = "SELECT u.first_name, u.last_name, u.identification, t.first_name as first_prof, t.last_name as last_prof, c.code, c.newcode, c.course, h.waybill_id, h.date_creation, h.score, h.status_id
						FROM ludus_waybills h, ludus_users u, ludus_users t, ludus_modules_results_usr r, ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE h.user_id = u.user_id
						AND h.teacher_id = t.user_id
						AND h.module_result_usr_id = r.module_result_usr_id
						AND r.invitation_id = i.invitation_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND r.score > 79 ";
			if($sWhere!=''){
				$query_sql .= " AND (t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR h.date_creation LIKE '%$sWhere%' OR h.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR h.score LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' )";
			}
			$idQuien = $_SESSION['idUsuario'];
			if( isset($_SESSION['max_rol']) && $_SESSION['max_rol']=="2" ){
				$query_sql .= " AND t.user_id = '$idQuien' AND h.status_id = 2 ";
			}
			if( isset($_SESSION['max_rol']) && ($_SESSION['max_rol']!="2") && ($_SESSION['max_rol']>=1 && $_SESSION['max_rol']<=5) ){
				$query_sql .= " AND u.user_id = '$idQuien' ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT u.first_name, u.last_name, u.identification, t.first_name as first_prof, t.last_name as last_prof, c.code, c.newcode, c.course, h.waybill_id, h.date_creation, h.score, h.status_id
						FROM ludus_waybills h, ludus_users u, ludus_users t, ludus_modules_results_usr r, ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE h.user_id = u.user_id
						AND h.teacher_id = t.user_id
						AND h.module_result_usr_id = r.module_result_usr_id
						AND r.invitation_id = i.invitation_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND r.score > 79 ";
		if($sWhere!=''){
			$query_sql .= " AND (t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR h.date_creation LIKE '%$sWhere%' OR h.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR h.score LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' )";
		}
		$idQuien = $_SESSION['idUsuario'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="2"){
			$query_sql .= " AND t.user_id = '$idQuien' ";
		}
		if(isset($_SESSION['max_rol'])&& ($_SESSION['max_rol']>=1 && $_SESSION['max_rol']<=5) ){
			$query_sql .= " AND u.user_id = '$idQuien' ";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.first_name, u.last_name, u.identification, t.first_name as first_prof, t.last_name as last_prof, c.code, c.newcode, c.course, h.waybill_id, h.date_creation, h.score, h.status_id
						FROM ludus_waybills h, ludus_users u, ludus_users t, ludus_modules_results_usr r, ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE h.user_id = u.user_id
						AND h.teacher_id = t.user_id
						AND h.module_result_usr_id = r.module_result_usr_id
						AND r.invitation_id = i.invitation_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND r.score > 79 ";
			$idQuien = $_SESSION['idUsuario'];
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="2"){
				$query_sql .= " AND t.user_id = '$idQuien' ";
			}
			if(isset($_SESSION['max_rol'])&& ($_SESSION['max_rol']>=1 && $_SESSION['max_rol']<=5) ){
				$query_sql .= " AND u.user_id = '$idQuien' ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearhojas($nombre){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración hojas: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_subject` (subject,status_id,creator,editor,date_creation,date_edition) VALUES ('$nombre','1','$idQuien','$idQuien','$NOW_data','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarhojas($id,$description,$target,$action,$fec_fulfillment){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_waybills`
		SET description = '$description', status_id = '2', date_edition = '$NOW_data',target = '$target' , action = '$action', fec_fulfillment = '$fec_fulfillment'
		WHERE waybill_id = '$id'";
		//echo($updateSQL_ER);
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}//fin funcion Actualizarhojas
	function ActualizarhojasF($id,$estado,$feedback,$score,$module_result_usr_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_waybills`
		SET score = '$score', status_id = '$estado', feedback = '$feedback', user_feedback = '$idQuien', date_feedback = '$NOW_data'
		WHERE waybill_id = '$id'";
		//echo($updateSQL_ER);
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		$updateSQL_Cal = "UPDATE ludus_modules_results_usr SET score_waybill = '$score', score = score + $score WHERE module_result_usr_id = $module_result_usr_id";
		$resultado_cal = $DataBase_Log->SQL_Update($updateSQL_Cal);
		//echo($updateSQL_Cal);

		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, DATEDIFF(c.date_edition,c.date_creation) as difDias, u.first_name, u.last_name, u.image, s.first_name as first_prof, s.last_name as last_prof, s.image as img_prof
			FROM ludus_waybills c, ludus_users u, ludus_users s
			WHERE c.waybill_id = '$idRegistro' AND c.user_id = u.user_id AND c.teacher_id = s.user_id";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
