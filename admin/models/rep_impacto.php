<?php
Class Datos {
	function CantUsuarios_Genero($ano_inicial,$zona,$concesionario){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT gender
		FROM ludus_users
		WHERE status_id = 1
		ORDER BY gender DESC";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "
				SELECT l.gender, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-02-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-03-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-04-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-05-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-06-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-07-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-08-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-09-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-10-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-11-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-12-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation <= '$ano_inicial-12-31'
				GROUP BY l.gender";
				//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$gender = $Rows_config[$i]['gender'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $gender == $Rows_cant[$x]['gender'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantUsuarios_Trayectoria($ano_inicial,$zona,$concesionario){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT c.charge
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-02-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				ORDER BY c.charge";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "
				SELECT c.charge, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-02-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-03-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-04-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-05-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-06-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-07-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-08-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-09-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-10-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-11-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation < '$ano_inicial-12-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND cu.charge_id = 23 AND l.date_creation <= '$ano_inicial-12-31'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge";
				//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$charge = $Rows_config[$i]['charge'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $charge == $Rows_cant[$x]['charge'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantUsuarios_RangosEdad($ano_inicial,$zona,$concesionario){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "
			SELECT 1 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND u.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 0 AND 25 UNION 
			SELECT 2 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND u.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 26 AND 35 UNION 
			SELECT 3 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND u.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 36 AND 45 UNION 
			SELECT 4 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND u.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 46 AND 55 UNION 
			SELECT 5 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND u.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 56 AND 65 UNION 
			SELECT 6 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND u.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) > 65";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos_1 = "SELECT 1 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25";
		$Rows_cant_1 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_1);

		$queryDatos_2 = "SELECT 2 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35";
		$Rows_cant_2 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_2);

		$queryDatos_3 = "SELECT 3 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45";
		$Rows_cant_3 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_3);

		$queryDatos_4 = "SELECT 4 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55";
		$Rows_cant_4 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_4);

		$queryDatos_5 = "SELECT 5 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65";
		$Rows_cant_5 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_5);

		$queryDatos_6 = "SELECT 6 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23) AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65";
		$Rows_cant_6 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_6);

		for($i=0;$i<count($Rows_config);$i++) {
			$rango = $Rows_config[$i]['rango'];

			for($x=0;$x<count($Rows_cant_1);$x++) {
				if( $rango == $Rows_cant_1[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_1[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_2);$x++) {
				if( $rango == $Rows_cant_2[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_2[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_3);$x++) {
				if( $rango == $Rows_cant_3[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_3[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_4);$x++) {
				if( $rango == $Rows_cant_4[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_4[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_5);$x++) {
				if( $rango == $Rows_cant_5[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_5[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_6);$x++) {
				if( $rango == $Rows_cant_6[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_6[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantVentas_Modelo($fec_inicial,$fec_final,$modelos,$zona,$concesionario){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT model, count(*) 
					FROM `ludus_sales` 
					WHERE date_sale BETWEEN '$fec_inicial' AND '$fec_final'";
					if($modelos!="''"){
						$query_hab .= " AND model IN (".$modelos.")";
					}
		$query_hab .= " GROUP BY model 
					ORDER BY count(*) DESC";
					//echo($query_hab);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		$queryDatos = "SELECT model, MONTH(date_sale) as mes, count(*) as cant
					FROM `ludus_sales` 
					WHERE date_sale BETWEEN '$fec_inicial' AND '$fec_final' 
					GROUP BY model, MONTH(date_sale)
					ORDER BY count(*) DESC, MONTH(date_sale)";
					//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);
		for($i=0;$i<count($Rows_config);$i++) {
			$model = $Rows_config[$i]['model'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $model == $Rows_cant[$x]['model'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CursosPromedio($fec_inicial,$fec_final,$course_sel,$zona,$concesionario){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT c.newcode, c.course
			FROM ludus_charges_courses cc, ludus_courses c, ludus_users u,
			(SELECT r.user_id, c.newcode, c.course, c.course_id, max(r.score) as score, r.date_creation
			FROM ludus_modules_results_usr r, ludus_modules m, ludus_courses c
			WHERE r.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23)
			AND r.module_id = m.module_id AND m.course_id = c.course_id
			AND r.date_creation BETWEEN '$fec_inicial' AND '$fec_final'
			GROUP BY r.user_id, c.newcode, c.course, c.course_id) m
			WHERE cc.charge_id = 23 AND c.specialty_id IN (3,4) AND cc.course_id = c.course_id AND c.course_id = m.course_id AND m.user_id = u.user_id AND u.status_id = 1";
			if($course_sel!="0"){
				$query_hab .= " AND c.course_id IN ($course_sel)";
			}
			$query_hab .= " ORDER BY c.newcode, c.course";
			//echo($query_hab);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT c.newcode, c.course, MONTH(m.date_creation) as mes, AVG(m.score) as cant
			FROM ludus_charges_courses cc, ludus_courses c, ludus_users u,
			(SELECT r.user_id, c.newcode, c.course, c.course_id, max(r.score) as score, r.date_creation
			FROM ludus_modules_results_usr r, ludus_modules m, ludus_courses c
			WHERE r.user_id IN (SELECT user_id FROM `ludus_charges_users` WHERE charge_id = 23)
			AND r.module_id = m.module_id AND m.course_id = c.course_id
			AND r.date_creation BETWEEN '$fec_inicial' AND '$fec_final'
			GROUP BY r.user_id, c.newcode, c.course, c.course_id) m
			WHERE cc.charge_id = 23 AND c.specialty_id IN (3,4) AND cc.course_id = c.course_id AND c.course_id = m.course_id AND m.user_id = u.user_id AND u.status_id = 1";
			if($course_sel!="0"){
				$queryDatos .= " AND c.course_id IN ($course_sel)";
			}
			$queryDatos .= " GROUP BY c.newcode, c.course, MONTH(m.date_creation)
			ORDER BY AVG(m.score) DESC";
		//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$newcode = $Rows_config[$i]['newcode'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $newcode == $Rows_cant[$x]['newcode'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function Modelos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT model FROM `ludus_sales` GROUP BY model ORDER BY model";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function Cursos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT * FROM `ludus_courses` WHERE status_id = 1 AND specialty_id IN (3,4) ORDER BY course";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function Concesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT * FROM `ludus_dealers` WHERE status_id = 1 ORDER BY dealer";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function Zonas(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT * FROM `ludus_zone` WHERE status_id = 1 ORDER BY zone";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
