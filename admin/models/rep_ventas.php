<?php
Class RepVentas {
	//====================================================================================
	//Consulta los concesionarios
	//====================================================================================
	function Concesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT * FROM `ludus_dealers` WHERE status_id = 1 ORDER BY dealer";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion Concesionarios

	//====================================================================================
	//Consulta las Zonas disponibles
	//====================================================================================
	function Zonas( $prof="" ){
		include_once( $prof.'../config/database.php' );
		include_once( $prof.'../config/config.php' );
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT * FROM ludus_zone WHERE status_id = 1 ORDER BY zone";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion Zona

	//====================================================================================
	//Consulta las sedes de un concesionario especifico
	//====================================================================================
	function sede( $dealer, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT h.headquarter_id, h.headquarter, h.address1 FROM ludus_headquarters h WHERE h.status_id = 1 AND h.dealer_id = $dealer";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion Sede

	//====================================================================================
	//genera un string de dealer_id separados por ,
	//====================================================================================
	function stringConcesionarios( $zona, $prof="" ){
		include_once( $prof.'../config/database.php' );
		include_once( $prof.'../config/config.php' );
		@session_start();
		$concesionario = "0";
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT d.dealer_id
			FROM ludus_dealers d, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE d.dealer_id = h.dealer_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			AND z.zone_id IN ( $zona )";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		foreach ($Rows_config as $key => $value ) {
			$concesionario .= ",".$value['dealer_id'];
		}
		unset($DataBase_Acciones);
		return $concesionario;
	}//fin funcion stringConcesionarios

	//====================================================================================
	//Consulta datos tabla pais, facturacion y matriculas
	//====================================================================================
	function consultaPais( $p ){
		$arr = [];
		$facturacion = $this->totalFacturacion( $p['start_date'], $p['end_date'], $p['prof'] );
		$matriculas =$this->totalMatriculas( $p['start_date'], $p['end_date'], $p['prof'] );
		$arr[] = ["texto" => "facturacion", "cantidad" => $facturacion['cantidad'] ];
		$arr[] = ["texto" => "matriculas", "cantidad" => $matriculas['cantidad'] ];
		return $arr;

	}//fin consultaPais

	//====================================================================================
	//Consulta numero de registros en facturacion entre dos fechas
	//====================================================================================
	function totalFacturacion( $start_date, $end_date, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT COUNT( b.billing_id ) AS cantidad
			FROM ludus_billing b WHERE b.Fch_Fact BETWEEN '$start_date' AND '$end_date'";
		$Rows_config = $DataBase_Acciones->SQL_SelectRows( $query_hab );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numFacturacion

	//====================================================================================
	//Consulta numero de registros en matriculas entre dos fechas
	//====================================================================================
	function totalMatriculas( $start_date, $end_date, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT COUNT( e.enrollment_id ) AS cantidad
			FROM ludus_enrollment e WHERE e.Fch_Fact BETWEEN '$start_date' AND '$end_date'";
		$Rows_config = $DataBase_Acciones->SQL_SelectRows( $query_hab );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numMatriculas

	//====================================================================================
	//Consulta las zonas, areas, sedes y concesionarios
	//====================================================================================
	function ubicacion( $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$DataBase_Acciones = new Database();
		$query = "SELECT z.zone_id, z.zone, a.area_id, a.area,h.headquarter_id, h.headquarter, d.dealer_id, d.dealer
			FROM ludus_zone z, ludus_areas a, ludus_headquarters h, ludus_dealers d
			WHERE z.zone_id = a.zone_id
			AND a.area_id = h.area_id
			AND h.dealer_id = d.dealer_id
			AND d.status_id = 1 AND h.status_id = 1 AND a.status_id = 1 AND z.status_id = 1";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion ubicacion

	//====================================================================================
	//Consulta el numero de facturas entre dos fechas
	//====================================================================================
	function numFacturasZona( $p ){
		include_once($p['prof'].'../config/database.php');
		include_once($p['prof'].'../config/config.php');
		$and = "";
		$campo_filtro = "z.zone";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" ){
						$and = ( $p['zona'] == "" || $p['zona'] == 0 ) ? "" : " AND z.zone_id IN ( {$p['zona']} ) ";
					}
				break;
			}
		}
		$DataBase_Acciones = new Database();
		$query = "SELECT $campo_filtro, COUNT( DISTINCT b.billing_id ) as cantidad
			FROM ludus_billing b, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE b.dealer_id = h.dealer_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			$and
			AND b.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
			GROUP BY $campo_filtro";
		// echo("<br>$query</br>");
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numFacturas

	//====================================================================================
	//Consulta el numero de facturas entre dos fechas
	//====================================================================================
	function numMatriculasZona( $p ){
		include_once($p['prof'].'../config/database.php');
		include_once($p['prof'].'../config/config.php');
		$and = "";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" ){
						$and = ( $p['zona'] == "" || $p['zona'] == 0 ) ? "" : " AND z.zone_id IN ( {$p['zona']} ) ";
					}
				break;
			}
		}
		$DataBase_Acciones = new Database();
		$query = "SELECT z.zone, COUNT( e.user_id ) AS cantidad
			FROM ludus_enrollment e, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE e.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			$and
			AND e.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
			GROUP BY z.zone_id";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numMatriculasZona

	//====================================================================================
	//Consulta el numero de matriculas por concesionario entre dos fechas
	//====================================================================================
	function numMatriculasConcs( $p ){
		include_once( $p['prof'].'../config/database.php' );
		include_once( $p['prof'].'../config/config.php' );
		$and = "";
		$texto_campo = "d.dealer";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" && $p['zona'] != "0" && $p['zona'] != "null" ){
						$aux = $this->stringConcesionarios( $p['zona'], $p['prof'] );
						$and .= " AND d.dealer_id IN ( $aux ) ";
					}
				break;
				case 'concesionario':
					if( $p['concesionario'] != "" && $p['concesionario'] != "0" && $p['concesionario'] != "null" ){
						$and .= " AND d.dealer_id IN ( {$p['concesionario']} ) ";
					}
				break;
				case 'usuario':
					if( $p['usuario'] != "" && $p['usuario'] != "0" && $p['usuario'] != "null" ){
						$and .= " AND u.identification IN ( {$p['usuario']} ) ";
					}
				break;
				case 'sede':
					if( $p['sede'] != "" && $p['sede'] != "0" && $p['sede'] != "null" ){
						$and .= " AND h.headquarter_id IN ( {$p['sede']} ) ";
					}
					$texto_campo = "h.headquarter";
				break;
			}
		}
		$DataBase_Acciones = new Database();
		$query = "SELECT $texto_campo, COUNT(e.user_id) AS cantidad
			FROM ludus_enrollment e, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
			WHERE e.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
            AND h.dealer_id = d.dealer_id
			$and
			AND e.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
            GROUP BY $texto_campo
            ORDER BY cantidad DESC
            LIMIT 0,10";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numMatriculasConcs

	//====================================================================================
	//Consulta el numero de facturas entre dos fechas, por concesionarios
	//====================================================================================
	function numFacturasConcs( $p ){
		include_once( $p['prof'].'../config/database.php' );
		include_once( $p['prof'].'../config/config.php' );
		// var_dump( $p );
		$and = "";
		$texto_campo = "d.dealer";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" && $p['zona'] != "0" && $p['zona'] != "null" ){
						$aux = $this->stringConcesionarios( $p['zona'], $p['prof'] );
						$and .= " AND d.dealer_id IN ( $aux ) ";
					}
				break;
				case 'concesionario':
					if( $p['concesionario'] != "" && $p['concesionario'] != "0" && $p['concesionario'] != "null" ){
						$and .= " AND d.dealer_id IN ( {$p['concesionario']} ) ";
					}
				break;
			}
		}
		$DataBase_Acciones = new Database();
		$query = "SELECT $texto_campo, COUNT( b.dealer_id ) AS cantidad
			FROM ludus_billing b, ludus_dealers d
			WHERE b.dealer_id = d.dealer_id
			$and
			AND b.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
			GROUP BY $texto_campo
			ORDER BY cantidad DESC
			LIMIT 0,10";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numFacturasConcs

	//====================================================================================
	//Consulta el numero de facturas entre dos fechas, por modelos
	//====================================================================================
	function numFacturasModelos( $p ){
		include_once( $p['prof'].'../config/database.php' );
		include_once( $p['prof'].'../config/config.php' );
		$and = "";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" && $p['zona'] != "0" && $p['zona'] != "null" ){
						$aux = $this->stringConcesionarios( $p['zona'], $p['prof'] );
						$and .= " AND b.dealer_id IN ( $aux ) ";
					}
				break;
				case 'sede':
					if( $p['sede'] != "" && $p['sede'] != "0" && $p['sede'] != "null" ){
						$and .= " AND h.headquarter_id IN ( {$p['sede']} ) ";
					}
				break;
			}
		}
		$DataBase_Acciones = new Database();
		$query = "SELECT b.Desc_KMAT, COUNT( b.dealer_id ) AS cantidad
			FROM ludus_billing b
			WHERE b.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
			$and
			GROUP BY b.Desc_KMAT
			ORDER BY cantidad DESC
			LIMIT 0,10";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numFacturasModelos

	//====================================================================================
	//Consulta el numero de matriculas por modelos entre dos fechas
	//====================================================================================
	function numMatriculasModelos( $p ){
		include_once($p['prof'].'../config/database.php');
		include_once($p['prof'].'../config/config.php');
		$and = "";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" ){
						$and = ( $p['zona'] == "" || $p['zona'] == 0 ) ? "" : " AND z.zone_id IN ( {$p['zona']} ) ";
					}
				break;
				case 'usuario':
					if( $p['usuario'] != "" ){
						$and = ( $p['usuario'] == "" || $p['usuario'] == 0 ) ? "" : " AND u.identification IN ( {$p['usuario']} ) ";
					}
				break;
				case 'sede':
					if( $p['sede'] != "" ){
						$and = ( $p['sede'] == "" || $p['sede'] == 0 ) ? "" : " AND u.headquarter_id IN ( {$p['sede']} ) ";
					}
				break;
			}
		}
		$DataBase_Acciones = new Database();
		$query = "SELECT e.Desc_KMAT, COUNT(e.user_id) AS cantidad
			FROM ludus_enrollment e, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE e.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
			$and
            AND e.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
            GROUP BY e.Desc_KMAT
            ORDER BY cantidad DESC
            LIMIT 0,10";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion numMatriculasModelos

	//====================================================================================
	//Consulta el numero de matriculas por género entre dos fechas
	//====================================================================================
	function numGenero( $p ){
		include_once($p['prof'].'../config/database.php');
		include_once($p['prof'].'../config/config.php');
		$and = "";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" ){
						$and = ( $p['zona'] == "" || $p['zona'] == 0 ) ? "" : " AND z.zone_id IN ( {$p['zona']} ) ";
					}
				break;
				case 'usuario':
					if( $p['usuario'] != "" ){
						$and = ( $p['usuario'] == "" || $p['usuario'] == 0 ) ? "" : " AND u.identification IN ( {$p['usuario']} ) ";
					}
				break;
			}
		}
		$genero = [];
		$DataBase_Acciones = new Database();
		$query = "SELECT u.gender, COUNT( e.enrollment_id ) cantidad
			FROM ludus_users u, ludus_enrollment e, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE e.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			$and
			AND e.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
			GROUP BY u.gender";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		foreach ($Rows_config as $key => $value) {
			if( $value['gender'] == 0 ){
				$genero[] = ["gender" => "Sin definir", "cantidad" => $value['cantidad']];
			}
			if( $value['gender'] == 1 ){
				$genero[] = ["gender" => "Masculino", "cantidad" => $value['cantidad']];
			}
			if( $value['gender'] == 2 ){
				$genero[] = ["gender" => "Femenino", "cantidad" => $value['cantidad']];
			}
		}
		unset($DataBase_Acciones);
		return $genero;
	}//fin funcion numGenero

	//====================================================================================
	//Consulta el numero de matriculas por género entre dos fechas
	//====================================================================================
	function numXAgentes( $p ){
		include_once($p['prof'].'../config/database.php');
		include_once($p['prof'].'../config/config.php');
		$and = "";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" ){
						$and = ( $p['zona'] == "" || $p['zona'] == 0 ) ? "" : " AND z.zone_id IN ( {$p['zona']} ) ";
					}
				break;
				case 'usuario':
					if( $p['usuario'] != "" ){
						$and = ( $p['usuario'] == "" || $p['usuario'] == 0 ) ? "" : " AND u.identification IN ( {$p['usuario']} ) ";
					}
				break;
			}
		}
		$usuarios = [];
		$DataBase_Acciones = new Database();
		$query = "SELECT u.user_id, u.first_name, u.last_name, COUNT( e.user_id ) cantidad
			FROM ludus_users u, ludus_enrollment e, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE e.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			$and
			AND e.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
			GROUP BY u.user_id
			ORDER BY cantidad DESC
			LIMIT 0, 10";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		foreach ($Rows_config as $key => $value) {
			$usuarios[] = ['nombre' => $value['first_name']." ".$value['last_name'], "cantidad" => $value['cantidad'] ];
		}
		unset($DataBase_Acciones);
		return $usuarios;
	}//fin funcion numXAgentes

	//====================================================================================
	//Consulta el numero de matriculas en rango de edades entre dos fechas
	//====================================================================================
	function numEdad( $p ){
		include_once($p['prof'].'../config/database.php');
		include_once($p['prof'].'../config/config.php');
		$and = "";
		if( isset($p['opcn']) ){
			switch ($p['opcn']) {
				case 'zona':
					if( $p['zona'] != "" ){
						$and = ( $p['zona'] == "" || $p['zona'] == 0 ) ? "" : " AND z.zone_id IN ( {$p['zona']} ) ";
					}
				break;
				case 'usuario':
					if( $p['usuario'] != "" ){
						$and = ( $p['usuario'] == "" || $p['usuario'] == 0 ) ? "" : " AND u.identification IN ( {$p['usuario']} ) ";
					}
				break;
			}
		}
		$edades = [
			["texto" => "15 - 25", "cantidad" => 0],
			["texto" => "26 - 35", "cantidad" => 0],
			["texto" => "36 - 45", "cantidad" => 0],
			["texto" => "46 - 55", "cantidad" => 0],
			["texto" => "56 - 65", "cantidad" => 0],
			["texto" => "Mayores de 65", "cantidad" => 0]
		];
		$DataBase_Acciones = new Database();
		$query = "SELECT u.user_id, ( YEAR(CURDATE()) - YEAR(u.date_birthday) ) edad
			FROM ludus_users u, ludus_enrollment e, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE e.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
			$and
			AND e.Fch_Fact BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
			ORDER BY edad DESC";
		// echo($query);
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query );
		foreach ($Rows_config as $key => $value ) {
			if( $value['edad'] > 15 && $value['edad'] <= 25 ){
				$edades[0]["cantidad"]++;
			}elseif( $value['edad'] > 25 && $value['edad'] <= 35 ){
				$edades[1]["cantidad"]++;
			}elseif( $value['edad'] > 35 && $value['edad'] <= 45 ){
				$edades[2]["cantidad"]++;
			}elseif( $value['edad'] > 45 && $value['edad'] <= 55 ){
				$edades[3]["cantidad"]++;
			}elseif( $value['edad'] > 55 && $value['edad'] <= 65 ){
				$edades[4]["cantidad"]++;
			}elseif( $value['edad'] > 65 ){
				$edades[5]["cantidad"]++;
			}
		}
		unset($DataBase_Acciones);
		return $edades;
	}//fin funcion numEdad
}
