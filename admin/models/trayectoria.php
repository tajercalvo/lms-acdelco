<?php
Class Trayectorias {
	function consultaTrayectorias($idCharge_Tray,$idUser_Tray){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT DISTINCT
											c.newcode,
											c.course,
											c.type,
											re.score,
											cc.level_id,
											c.course_id,
											re.date_creation,
											n.level
									FROM
											ludus_charges_courses cc,
											ludus_courses c,
											ludus_level n,
											(
											SELECT
													m_r.course_id,
													tm.date_creation,
													AVG(tm.score) AS score
											FROM
													ludus_modules m_r,
													(
													SELECT
															module_id,
															date_creation,
															MAX(score) AS score
													FROM
															ludus_modules_results_usr
													WHERE
															user_id = '$idUser_Tray'
													GROUP BY
															module_id
											) AS tm
									WHERE
											m_r.module_id = tm.module_id
									GROUP BY
											m_r.course_id
									) re
									WHERE
											cc.charge_id = '$idCharge_Tray' AND cc.course_id = c.course_id AND c.course_id = re.course_id AND n.level_id = cc.level_id -- AND cc.level_id != 5
									ORDER BY
											cc.level_id ASC,
											c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaTrayectorias_cant($idCharge_Tray,$idUser_Tray){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT cc.level_id, count(*) as cantidad
						FROM ludus_charges_courses cc, ludus_courses c, (SELECT m_r.course_id, AVG(tm.score) as score
						FROM ludus_modules m_r, (SELECT module_id, MAX(score) as score 
								FROM ludus_modules_results_usr 
								WHERE user_id = '$idUser_Tray' 
								GROUP BY module_id) as tm
						WHERE m_r.module_id = tm.module_id
						GROUP BY m_r.course_id) re
						WHERE cc.charge_id = '$idCharge_Tray' AND cc.course_id = c.course_id AND c.course_id = re.course_id
						GROUP BY cc.level_id ASC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaTrayectoria_res($idCharge_Tray){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT cc.level_id, count(*) as cantidad
						FROM ludus_charges_courses cc, ludus_courses c
						WHERE cc.charge_id = '$idCharge_Tray' AND cc.course_id = c.course_id
						GROUP BY cc.level_id DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaAutodirigidos($idCharges_Auto,$idUser_Tray){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT distinct c.newcode,c.course,c.type,re.score, c.course_id, re.date_creation
						FROM ludus_courses c, (SELECT m_r.course_id, tm.date_creation, AVG(tm.score) as score
						FROM ludus_modules m_r, (SELECT module_id, date_creation, MAX(score) as score 
								FROM ludus_modules_results_usr 
								WHERE user_id = '$idUser_Tray' 
								GROUP BY module_id) as tm
						WHERE m_r.module_id = tm.module_id
						GROUP BY m_r.course_id) re
						WHERE c.course_id = re.course_id AND c.type = 'WBT' AND c.course_id NOT IN (SELECT cc.course_id FROM ludus_charges_courses cc WHERE cc.charge_id IN ('$idCharges_Auto'))
						ORDER BY c.course, c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaSinTrayectoria($idCharges_Auto,$idUser_Tray){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT distinct c.newcode,c.course,c.type,re.score, c.course_id, re.date_creation
						FROM ludus_courses c, (SELECT m_r.course_id, tm.date_creation, AVG(tm.score) as score
						FROM ludus_modules m_r, (SELECT module_id, date_creation, MAX(score) as score 
								FROM ludus_modules_results_usr 
								WHERE user_id = '$idUser_Tray' 
								GROUP BY module_id) as tm
						WHERE m_r.module_id = tm.module_id
						GROUP BY m_r.course_id) re
						WHERE c.course_id = re.course_id AND c.type <> 'WBT' AND c.course_id NOT IN (SELECT cc.course_id FROM ludus_charges_courses cc WHERE cc.charge_id IN ('$idCharges_Auto'))
						ORDER BY c.course, c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}