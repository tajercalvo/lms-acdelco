<?php
Class RepEncuestas {

	//Principal que se muestra en la tabla
	function consultaDatosAll($specialty_id, $supplier_id, $zone_id, $dealer_id, $start_date, $end_date, $review_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		
		if($zone_id==0 && $dealer_id==0){

			$SubQuery_usr = "SELECT u.user_id
				FROM ludus_users u, ludus_headquarters h
				WHERE u.headquarter_id = h.headquarter_id";

		}else if($zone_id > 0){

			$SubQuery_usr = "SELECT u.user_id
				FROM ludus_users u, ludus_headquarters h, ludus_areas a
				WHERE u.headquarter_id = h.headquarter_id AND h.area_id = a.area_id AND a.zone_id = $zone_id";

		}else if($dealer_id > 0){

			$SubQuery_usr = "SELECT u.user_id
				FROM ludus_users u, ludus_headquarters h
				WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id";

		}

		if($specialty_id==0 && $supplier_id==0){

			$SubQuery_prov = "SELECT m.module_result_usr_id FROM ludus_modules_results_usr m WHERE m.invitation_id > 0";

		}else if($specialty_id > 0){

			$SubQuery_prov = "SELECT m.module_result_usr_id 
							FROM ludus_modules_results_usr m, ludus_modules o, ludus_courses c
							WHERE m.invitation_id > 0 AND m.module_id = o.module_id AND o.course_id = c.course_id AND c.specialty_id = $specialty_id
							";

		}else if($supplier_id > 0){

			$SubQuery_prov = "SELECT m.module_result_usr_id 
							FROM ludus_modules_results_usr m, ludus_modules o, ludus_courses c
							WHERE m.invitation_id > 0 AND m.module_id = o.module_id AND o.course_id = c.course_id AND c.supplier_id = $supplier_id
							";
		}
		

		$DataBase_Acciones = new Database();

		if ($review_id == 0 ) {
			$review_id = "62, 63, 64";
		}

		$queryPreg = "SELECT distinct q.question, q.question_id
					FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers a
					WHERE ra.module_result_usr_id >0 AND ra.review_id  in($review_id) AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = q.question_id AND ad.answer_id = a.answer_id AND ra.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND ra.user_id IN ($SubQuery_usr) 
					AND ra.module_result_usr_id IN ($SubQuery_prov)
					GROUP BY q.question, q.question_id
					ORDER BY q.question, q.question_id ";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryPreg);

		$query_sql = "SELECT YEAR( ra.date_creation ) AS ANO, MONTH( ra.date_creation ) AS MES, q.question, q.question_id, a.answer, count(*) AS cantidad
					FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers a
					WHERE ra.module_result_usr_id >0 AND ra.review_id  in($review_id) AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = q.question_id AND ad.answer_id = a.answer_id AND ra.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND ra.user_id IN ($SubQuery_usr) 
					AND ra.module_result_usr_id IN ($SubQuery_prov)
					GROUP BY YEAR( ra.date_creation ) , MONTH( ra.date_creation ), q.question, q.question_id, a.answer
					ORDER BY q.question, a.answer, YEAR( ra.date_creation ), MONTH( ra.date_creation ) ";
		$Rows_Resp = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_periodos = "SELECT YEAR( ra.date_creation ) AS ANO, MONTH( ra.date_creation ) AS MES
					FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers a
					WHERE ra.module_result_usr_id >0 AND ra.review_id  in($review_id) AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = q.question_id AND ad.answer_id = a.answer_id AND ra.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' 
					AND ra.user_id IN ($SubQuery_usr) 
					AND ra.module_result_usr_id IN ($SubQuery_prov)
					GROUP BY YEAR( ra.date_creation ) , MONTH( ra.date_creation )
					ORDER BY YEAR( ra.date_creation ), MONTH( ra.date_creation ) ";
		$Rows_Peri = $DataBase_Acciones->SQL_SelectMultipleRows($query_periodos);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_question_id = $Rows_config[$i]['question_id'];
			$Rows_config[$i]['Perio'] = $Rows_Peri;
			for($y=0;$y<count($Rows_Resp);$y++) {
				if( $var_question_id == $Rows_Resp[$y]['question_id'] ){
					$Rows_config[$i]['Resp'][] = $Rows_Resp[$y];
				}
			}
		}
		
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//si
	function consultaZonas(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		/*if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			
		}else{
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 AND dealer_id = $dealer_id ORDER BY dealer";
		}*/
		$query_sql = "SELECT * FROM ludus_zone WHERE status_id = 1 ORDER BY zone";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//si
	function consultaEspecialidades(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT * FROM ludus_specialties WHERE status_id = 1 ORDER BY specialty";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//si
	function consultaProveedores(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT * FROM ludus_supplier WHERE status_id = 1 ORDER BY supplier";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//si
	function consultaEncuenstas(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT * FROM ludus_reviews where review_id in (62, 63, 64);";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaConcesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		}else{
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 AND dealer_id = $dealer_id ORDER BY dealer";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

		/*function EvaluacionesEncuestas($tipo){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_evaluaciones = "SELECT * FROM ludus_reviews WHERE type IN ($tipo) ";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_evaluaciones);
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/

	/*	function ConsultaPreguntas($start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT q.code, q.question, q.question_id
					FROM ludus_reviews_questions rq, ludus_questions q
					where rq.review_id = ".$review_id." AND rq.question_id = q.question_id AND q.status_id = 1 AND q.type = 2";

					echo "$query_sql";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		echo "<pre>";
		print_r(expression);
		echo "</pre>";

		$query_Resp = "SELECT q.code, q.question, a.answer, q.question_id, a.answer_id
					FROM ludus_reviews_questions rq, ludus_questions q, ludus_answers a 
					where rq.review_id = ".$review_id." AND rq.question_id = q.question_id AND q.status_id = 1 AND q.type = 2 AND q.question_id = a.question_id";
		$Rows_Resp = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resp);

		$query_Resu = "SELECT ans.answer_id, count(*) as Cantidad FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans WHERE a.review_id = '$review_id' AND a.reviews_answer_id = ad.reviews_answer_id AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id group by ans.answer_id";
		$Rows_Resu = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resu);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_question_id = $Rows_config[$i]['question_id'];
			for($y=0;$y<count($Rows_Resp);$y++) {
				if( $var_question_id == $Rows_Resp[$y]['question_id'] ){
					$var_answer_id = $Rows_Resp[$y]['answer_id'];
					for($z=0;$z<count($Rows_Resu);$z++) {
						if($var_answer_id == $Rows_Resu[$z]['answer_id']){
							$Rows_Resp[$y]['CantResp'][] = $Rows_Resu[$z];
						}
					}
					$Rows_config[$i]['Resp'][] = $Rows_Resp[$y];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/
	

}
