<?php
Class Comparativo {

	/*
	Andres Vega
	29/11/2016
	Funcion que trae el total de cada curso por concesionario y/o zona
	*/
	function InformacionCursos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$query_sql_CXC = "SELECT z.zone, d.dealer, count(distinct s.course_id) as CantidadCursos
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s,ludus_areas a, ludus_zone z
						WHERE d.dealer_id = h.dealer_id
						AND d.dealer_id NOT IN (1,30,34,36)
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
			            AND h.area_id = a.area_id
			            AND a.zone_id = z.zone_id
						GROUP BY z.zone, d.dealer";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CXC);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function InformacionCargosDatos($fechaInicial,$fechaFinal){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		/*Trae la cantidad de personas por cargo de un concesionario que deberian ver los cursos*/
		$query_sql = "SELECT z.zone, d.dealer, c.charge_id, c.charge, count(*) as cantidadPersonas
		FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_areas a, ludus_zone z
		WHERE d.dealer_id = h.dealer_id
		AND d.dealer_id NOT IN (1,30,34,36)
		AND h.headquarter_id = u.headquarter_id
		AND u.user_id = cu.user_id
		AND u.status_id = 1
		AND cu.charge_id = c.charge_id
		AND c.charge_id IN (23,35,137,32,31,34,135,141,105,89,94,93,68,97,98,128,134,100,49,99)
		AND h.area_id = a.area_id
		AND a.zone_id = z.zone_id
		GROUP BY z.zone, d.dealer, c.charge
		ORDER BY z.zone, d.dealer, c.charge";
		//echo $query_sql." fecha inicial ".$fechaInicial."fecha final ".$fechaFinal;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		/*Trae la cantidad de cursos por cargo de un concesionario de forma general*/
		$query_sql_CXC = "SELECT z.zone, d.dealer, c.charge_id, c.charge, count(distinct s.course_id) as CantidadCursos
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s,ludus_areas a, ludus_zone z
						WHERE d.dealer_id = h.dealer_id
						AND d.dealer_id NOT IN (1,30,34,36)
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
						GROUP BY z.zone, d.dealer, c.charge_id, c.charge";
		$Rows_CXC = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CXC);

		/*Trae la cantidad de cursos asignados a las personas en la trayectoria general*/
		$query_sql_CAXT = "SELECT z.zone, d.dealer, c.charge_id, c.charge, count(*) as CantidadCursosPersonas
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s, ludus_areas a, ludus_zone z
						WHERE d.dealer_id = h.dealer_id
						AND d.dealer_id NOT IN (1,30,34,36)
						AND h.headquarter_id = u.headquarter_id
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
			            AND h.area_id = a.area_id
			            AND a.zone_id = z.zone_id
						GROUP BY z.zone, d.dealer, c.charge_id, c.charge";
		$Rows_CAXT = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CAXT);

		/*Trae la cantidad de cursos que pasaron las personas en la trayectoria por cada nivel en la trayectoria*/
		$query_sql_CAXTNG = "SELECT z.zone, d.dealer,c.charge_id, c.charge, count(*) as CantidadCursosPaso
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s, ludus_areas a, ludus_zone z, (SELECT distinct c.course_id, mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.date_creation BETWEEN '$fechaInicial' AND '$fechaFinal' AND mr.approval = 'SI' AND mr.module_id = m.module_id AND m.course_id = c.course_id) cp
						WHERE d.dealer_id = h.dealer_id
						AND d.dealer_id NOT IN (1,30,34,36)
						AND h.headquarter_id = u.headquarter_id
						AND u.user_id = cu.user_id
            			AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						AND u.user_id = cp.user_id
			            AND s.course_id = cp.course_id
			            AND h.area_id = a.area_id
			            AND a.zone_id = z.zone_id
						GROUP BY z.zone, d.dealer,c.charge_id, c.charge";
		$Rows_CAXTNG = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CAXTNG);


		/*Trae la cantidad de cursos que pasaron las personas en la trayectoria por cada nivel en la trayectoria*/
		/*
		$query_sql_CAXTN = "SELECT z.zone, d.dealer,c.charge_id, c.charge, cc.level_id, count(*) as CantidadCursosNivel
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s, ludus_areas a, ludus_zone z, (SELECT distinct c.course_id, mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.date_creation BETWEEN '2016-10-01' AND '2016-10-31' AND mr.approval = 'SI' AND mr.module_id = m.module_id AND m.course_id = c.course_id) cp
						WHERE d.dealer_id = h.dealer_id
						AND u.date_creation BETWEEN '2016-10-01' AND '2016-10-31'
						AND h.headquarter_id = u.headquarter_id
						AND u.user_id = cu.user_id
            AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						AND u.user_id = cp.user_id
            AND s.course_id = cp.course_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
						GROUP BY z.zone, d.dealer,c.charge_id, c.charge, cc.level_id";
		$Rows_CAXTN = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CAXTN);
		*/

		$query_sql_PROM = "SELECT z.zone, d.dealer,c.charge_id, c.charge, AVG(cp.promedio) as PromedioXCargo
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s, ludus_areas a, ludus_zone z, (SELECT c.course_id, mr.user_id, AVG(mr.score) as promedio FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.date_creation BETWEEN '$fechaInicial' AND '$fechaFinal' AND mr.module_id = m.module_id and m.course_id = c.course_id GROUP BY c.course_id, mr.user_id) cp
						WHERE d.dealer_id = h.dealer_id
						AND d.dealer_id NOT IN (1,30,34,36)
						AND h.headquarter_id = u.headquarter_id
						AND u.user_id = cu.user_id
            			AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						AND u.user_id = cp.user_id
            AND s.course_id = cp.course_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
						GROUP BY z.zone, d.dealer,c.charge_id, c.charge";
		$Rows_PROM = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_PROM);


		for($i=0;$i<count($Rows_config);$i++) {
			$charge_id = $Rows_config[$i]['charge_id'];
			$dealer = $Rows_config[$i]['dealer'];
			$zone = $Rows_config[$i]['zone'];
			$Rows_config[$i]['_CantidadCursos'] = 0;

				for($x=0;$x<count($Rows_CXC);$x++) {
					if( $charge_id == $Rows_CXC[$x]['charge_id'] && $dealer == $Rows_CXC[$x]['dealer'] && $zone == $Rows_CXC[$x]['zone']){
						$Rows_config[$i]['_CantidadCursos'] += $Rows_CXC[$x]['CantidadCursos'];
					}
				}

				for($x=0;$x<count($Rows_CAXT);$x++) {
					if( $charge_id == $Rows_CAXT[$x]['charge_id'] && $dealer == $Rows_CAXT[$x]['dealer'] && $zone == $Rows_CAXT[$x]['zone']){
						$Rows_config[$i]['_CantidadCursosPersonas'] = $Rows_CAXT[$x]['CantidadCursosPersonas'];
					}
				}

				for($x=0;$x<count($Rows_CAXTNG);$x++) {
					if( $charge_id == $Rows_CAXTNG[$x]['charge_id'] && $dealer == $Rows_CAXTNG[$x]['dealer'] && $zone == $Rows_CAXTNG[$x]['zone']){
						$Rows_config[$i]['_CantidadCursosPaso'] = $Rows_CAXTNG[$x]['CantidadCursosPaso'];
					}
				}

				/*
				Informacion cantidad de cursos por nivel
				for($x=0;$x<count($Rows_CAXTN);$x++) {
					if( $charge_id == $Rows_CAXTN[$x]['charge_id'] && $dealer == $Rows_CAXTN[$x]['dealer'] && $zone == $Rows_CAXTN[$x]['zone']){
						$Rows_config[$i]['CantCursosPasoXNivel'][] = $Rows_CAXTN[$x];
					}
				}*/

				for($x=0;$x<count($Rows_PROM);$x++) {
					if( $charge_id == $Rows_PROM[$x]['charge_id'] && $dealer == $Rows_PROM[$x]['dealer'] && $zone == $Rows_PROM[$x]['zone']){
						$Rows_config[$i]['_PromedioXCargo'] = $Rows_PROM[$x]['PromedioXCargo'];
					}
				}
		}
		//print_r($Rows_config);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion InformacionCargosDatos

}
