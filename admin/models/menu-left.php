<?php  

class MenuLeft{
	public static function mdlMenuLeft(){
		include_once('../config/init_db.php');
		DB::$encoding = 'utf8';

		$idQuien = $_SESSION['idUsuario'];
		$roles = 0;
		foreach ($_SESSION['id_rol'] as $key => $value) {
			$roles .= ','.$value['rol_id'];
		}

		$menu = DB::query("SELECT
							    m.*
							FROM
							    ludus_menu m
							INNER JOIN ludus_roles_menus rm ON
							    rm.menu_id = m.menu_id
							WHERE
							    m.estado_id = 1 
							    AND rm.rol_id IN($roles) 
							    AND m.nivel = 1
							GROUP BY
							    m.menu_id
							ORDER BY
							    m.orden");


		$nivel2 = DB::query("SELECT
							    m.*
							FROM
							    ludus_menu m
							INNER JOIN ludus_roles_menus rm ON
							    rm.menu_id = m.menu_id
							WHERE
							    m.estado_id = 1 
							    AND rm.rol_id IN($roles) 
							    AND m.nivel = 2
							GROUP BY
							    m.menu_id
							ORDER BY
							    m.orden");

		$nivel3 = DB::query("SELECT
							    m.*
							FROM
							    ludus_menu m
							INNER JOIN ludus_roles_menus rm ON
							    rm.menu_id = m.menu_id
							WHERE
							    m.estado_id = 1 
							    AND rm.rol_id IN($roles) 
							    AND m.nivel = 3
							GROUP BY
							    m.menu_id
							ORDER BY
							    m.orden");

		foreach ($nivel2 as $key => $value) {
			foreach ($nivel3 as $key2 => $value2) {
				if ($nivel2[$key]['menu_id'] == $nivel3[$key2]['padre_id']) {
					$nivel2[$key]['sub'][] = $value2;
				}
			}
		}

		foreach ($menu as $key => $value) {
			foreach ($nivel2 as $key2 => $value2) {
				if ($menu[$key]['menu_id'] == $nivel2[$key2]['padre_id']) {
					$menu[$key]['sub'][] = $value2;
				}
			}
		}

		
		return $menu;
	}
}