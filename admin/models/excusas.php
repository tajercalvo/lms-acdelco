<?php
Class Notas {
	function consultaDatosNota($sWhere,$sOrder,$sLimit, $fecha, $anio, $dealer ){
		@session_start();
		include_once('../../config/init_db.php');
		if( $anio == "0" ){
			$anio = "";
		}
		$filtro = "";
		$f_mes = "";
		$f_dealer = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = " AND d.dealer_id = ".$_SESSION['dealer_id'];
		}
		if( $fecha > 0 ){
			$f_mes = " AND MONTH(e.date_creation) = $fecha ";
		}
		if( $dealer > 0 ){
			$f_dealer = " AND d.dealer_id = $dealer";
		}
		$query_sql = "SELECT s.start_date, d.dealer, c.code, c.newcode, c.course, u.first_name, u.last_name, u.identification, t.first_name as first_creator, t.last_name as last_creator, e.excuse_id, e.file, e.date_creation, e.status_id
			FROM ludus_excuses e, ludus_users u, ludus_schedule s, ludus_courses c, ludus_users t, ludus_headquarters h, ludus_dealers d
			WHERE e.schedule_id = s.schedule_id
			AND e.user_id = u.user_id
			AND s.course_id = c.course_id
			AND e.creator = t.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			$f_dealer
			AND e.date_creation BETWEEN '$anio-01-01 00:00:00' AND '$anio-12-31 23:59:59'
			$f_mes
			$filtro";

			if($sWhere!=''){
				$query_sql .= " AND ( u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%' OR t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR t.identification LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR s.start_date LIKE '%$sWhere%') ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		// echo $query_sql;
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit, $fecha, $anio, $dealer ){
		@session_start();
		include_once('../../config/init_db.php');
		if( $anio == "0" ){
			$anio = "";
		}
		$filtro = "";
		$f_mes = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = " AND d.dealer_id = ".$_SESSION['dealer_id'];
		}
		if( $fecha > 0 ){
			$f_mes = " AND MONTH(e.date_creation) = $fecha ";
		}
		$query_sql = "SELECT s.start_date, c.code, c.newcode, c.course, u.first_name, u.last_name, u.identification, t.first_name as first_creator, t.last_name as last_creator, e.excuse_id, e.file, e.date_creation
			FROM ludus_excuses e, ludus_users u, ludus_schedule s, ludus_courses c, ludus_users t, ludus_headquarters h, ludus_dealers d
			WHERE e.schedule_id = s.schedule_id
			AND e.user_id = u.user_id
			AND s.course_id = c.course_id
			AND e.creator = t.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND e.date_creation BETWEEN '$anio-01-01 00:00:00' AND '$anio-12-31 23:59:59'
			$f_mes
			$filtro";
			if($sWhere!=''){
				$query_sql .= " AND ( u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%' OR t.first_name LIKE '%$sWhere%' OR t.last_name LIKE '%$sWhere%' OR t.identification LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.course LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR s.start_date LIKE '%$sWhere%') ";
			}
			// echo( $query_sql );
		$cuenta = DB::query($query_sql);
		$Rows_config = count($cuenta);
		return $Rows_config;
	}
	function consultaCantidad(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$anio = date('Y');
		$filtro = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = " AND d.dealer_id = ".$_SESSION['dealer_id'];
		}
		$query_sql = "SELECT s.start_date, c.code, c.newcode, c.course, u.first_name, u.last_name, u.identification, t.first_name as first_creator, t.last_name as last_creator, e.excuse_id, e.file, e.date_creation
			FROM ludus_excuses e, ludus_users u, ludus_schedule s, ludus_courses c, ludus_users t, ludus_headquarters h, ludus_dealers d
			WHERE e.schedule_id = s.schedule_id
			AND e.user_id = u.user_id
			AND s.course_id = c.course_id
			AND e.creator = t.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id";
		// echo( $query_sql );
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearExcusa($schedule_id,$invitation_id,$module_result_usr_id,$user_id,$new_name1,$type, $description){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_excuses` (user_id,invitation_id,schedule_id,module_result_usr_id,file,type,description,creator,date_creation, status_id, aplica_para) VALUES ('$user_id','$invitation_id','$schedule_id','$module_result_usr_id','$new_name1','$type', '$description','$idQuien','$NOW_data', 2, '')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		$insertSQL_ALL = "INSERT INTO ludus_invitation_excuse SELECT * FROM ludus_invitation WHERE invitation_id = $invitation_id";
		$resultado_COPY = $DataBase_Log->SQL_Insert($insertSQL_ALL);
		unset($DataBase_Log);
		return $resultado;
	}
	//=====================================================================
	//Retorna un listado de 15 usuarios
	//=====================================================================
	public static function consultaUsuarios($texto, $prof = "../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$filtro = "";
		if( $_SESSION['max_rol'] == 3 ){
			$filtro = "AND d.dealer_id = {$_SESSION['dealer_id']}";
		}
		if( $texto != "" ){
			$texto = "AND ( c.first_name LIKE '%$texto%' OR c.last_name LIKE '%$texto%' OR c.identification LIKE '%$texto%' ) ";
		}
		$query_sql = "SELECT c.user_id, c.first_name, c.last_name, c.identification
						FROM ludus_users c, ludus_headquarters h, ludus_dealers d
						WHERE c.headquarter_id = h.headquarter_id
						AND h.dealer_id = d.dealer_id
						$filtro
						$texto
						ORDER BY c.first_name, c.last_name
						LIMIT 15";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}//fin consultaUsuarios
	//=====================================================================
	//Retorna el usuario al cual ya se le genero una excusa
	//=====================================================================
	public static function consultaUsuarioExcusa($user_id, $prof = "../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$query_sql = "SELECT c.user_id, c.first_name, c.last_name, c.identification
						FROM ludus_users c
						WHERE c.user_id = $user_id";
		$Rows_config = DB::queryFirstRow($query_sql);
		return $Rows_config;
	}// fin consultaUsuarioExcusa
	//=====================================================================
	//Retorna los datos de la programacion para un usuario
	//=====================================================================
	public static function GetProgramacion($user_id, $prof="../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		// $dia = date('d');
		$mes = intval(date('m'));
		$anio = date('Y');
		$filtro = "";
		// $mes_ant = 0;
		// if( intval($dia) <= 5 || $_SESSION['max_rol'] >= 5 ){
		// 	$mes_ant = $mes == 1 ? 12 : $mes - 1;
		// IN ( $mes_ant, $mes )
		// }

		if ( $_SESSION['max_rol'] < 5 ) {
			$filtro = "AND MONTH( s.start_date ) IN ( $mes ) ";
		}

		$query_sql = "SELECT i.invitation_id, s.schedule_id, s.start_date, c.course, c.newcode, c.code
						FROM ludus_invitation i, ludus_schedule s, ludus_courses c
						WHERE i.user_id = $user_id
						AND i.schedule_id = s.schedule_id
						AND s.course_id = c.course_id
						AND i.invitation_id NOT IN ( SELECT e.invitation_id FROM ludus_excuses e WHERE e.status_id = 1 AND e.user_id = $user_id )
						$filtro
						AND YEAR( s.start_date ) = $anio
						ORDER BY s.start_date DESC";

		$query_mru = DB::query("SELECT mru.module_result_usr_id, mru.invitation_id FROM ludus_modules_results_usr mru WHERE mru.user_id = $user_id");

		// echo( $query_sql );
		$Rows_config = DB::query($query_sql);
		$cuenta = count( $Rows_config );
		for ($i=0; $i < $cuenta ; $i++) {
			$Rows_config[$i]['module_result_usr_id'] = 0;
			foreach ($query_mru as $key => $value) {
				if( $Rows_config[$i]['invitation_id'] == $value['invitation_id'] ){
					$Rows_config[$i]['module_result_usr_id'] = $value['module_result_usr_id'];
				}
			}
		}

		return $Rows_config;
	}// fin GetProgramacion
	//=====================================================================
	//Retorna la excusa seleccionada
	//=====================================================================
	public static function getExcusa( $excuse_id, $prof = "" ){
		include_once($prof.'../config/init_db.php');
		$query = "SELECT e.* FROM ludus_excuses e WHERE e.excuse_id = $excuse_id";
		$excusa = DB::queryFirstRow( $query );
		return $excusa;
	}//fin getExcusa
	//=====================================================================
	//Actualiza la excusa con la informacion enviada
	//=====================================================================
	public static function actualizarExcusa( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$description = "";
		$idQuien = $_SESSION['idUsuario'];

		if( isset( $p['excusa_text_r'] ) && $p['excusa_text_r'] != "" ){
			$description = $p['description']."\n".$_SESSION['NameUsuario'].": ".$p['excusa_text_r'];
		}else{
			$description = $p['description'];
		}
		$datos = [
			"user_id" 		=> $p['user_id'],
			"date_edition" 	=> DB::sqleval('NOW()'),
			"status_id" 	=> $p['status_id'],
			"type" 			=> $p['type'],
			"description" 	=> $description,
			"editor" 		=> $idQuien
		];
		if( isset($p['file']) && $p['file'] != "" ){
			$datos['file'] = $p['file'];
		}
		if( isset( $p['schedule_id'] ) && $p['schedule_id'] != "" ){
			$us = explode(':', $_POST['schedule_id']);
			$datos['invitation_id'] = $us[0];
			$datos['schedule_id'] = $us[1];
			$datos['aplica_para'] = $p['aplica_para'];
			$datos['module_result_usr_id'] = $us[2];
		}
		// print_r( $datos );
		DB::update('ludus_excuses', $datos, "excuse_id = %i", $p['excuse_id'] );
		$excusa = DB::affectedRows();
		return $excusa;
	}//fin getExcusa
	//=====================================================================
	//Actualiza la excusa con la informacion enviada
	//=====================================================================
	public static function eliminarRegistroExcusa( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		$excusa = 0;
		$query = "SELECT module_result_usr_id, score, score_review, score_waybill, user_id, module_id, approval, file, date_creation, status_id, creator, invitation_id, editor_ev FROM ludus_modules_results_usr WHERE module_result_usr_id = ".$p['module_result_usr_id'];
		$module_result_usr = DB::queryFirstRow( $query );
		if( isset( $module_result_usr ) && isset( $module_result_usr['module_result_usr_id'] ) ){
			DB::insertUpdate('ludus_modules_results_usr_excuse', $module_result_usr );
		}
		// DB::delete('ludus_invitation', "invitation_id = %i", $p['invitation_id'] );
		//DB::delete('ludus_modules_results_usr', "module_result_usr_id = %i", $p['module_result_usr_id'] );
		$excusa = DB::affectedRows();
		return $excusa;
	}//fin getExcusa

}
