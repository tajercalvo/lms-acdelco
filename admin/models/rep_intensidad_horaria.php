<?php
Class RepIntensidadHoraria {


	function consultaIntensidadHoraria($start_date, $end_date, $condicion){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT   d.dealer as concecionairo, h.headquarter sede, count(*) asistentes, sum( EXTRACT(hour FROM TIMEDIFF(s.end_date, s.start_date))) AS Horas_de_imparticon, c.course, c.course_id, h.headquarter_id, d.dealer_id, s.schedule_id, invitados.cantidad as invitados
						FROM ludus_inscriptions i, ludus_schedule s, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_invitation inv, ludus_courses c, (SELECT count(*) as cantidad, i.schedule_id, u.headquarter_id
						FROM ludus_invitation i, ludus_users u
						where i.user_id = u.user_id
						group by i.schedule_id, u.headquarter_id) as invitados
						where s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
						and u.user_id = i.user_id
						and h.headquarter_id = u.headquarter_id
						and h.dealer_id = d.dealer_id
						and s.schedule_id = i.schedule_id
						and u.user_id = inv.user_id
						and inv.schedule_id = s.schedule_id
						and c.course_id = s.course_id
						and invitados.schedule_id = s.schedule_id
						and invitados.headquarter_id = h.headquarter_id ";
						if($condicion != ''){
							$query_sql .= "and d.dealer_id = '$condicion'";
						}
		$query_sql .= " group by  h.headquarter_id;";
		$datos = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $datos;
	}

	function consulta_zona_Ajax($zona_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "	SELECT distinct d.dealer_id, d.dealer
						FROM ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
						where h.area_id = a.area_id
						and z.zone_id = a.zone_id
						and d.dealer_id = h.dealer_id
						and z.zone_id = $zona_id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_concesionario_Ajax($dealer_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		//$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT h.headquarter_id, h.headquarter, a.area, a.area_id, z.zone, z.zone_id, d.dealer, d.dealer_id
							FROM ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
							where h.area_id = a.area_id
							and z.zone_id = a.zone_id
							and d.dealer_id = h.dealer_id
							and d.dealer_id = $dealer_id;";
							//echo "$query_sql";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_visitantes_unicos1_Ajax($start_date, $end_date, $condicion, $cargo){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');

		// echo "$cargo";

		if ($cargo=='') {

				$query_visitantes_unicos = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(distinct n.user_id) as visitantes_unicos
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
	                        and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							$condicion
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation)
							ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";

				$query_visitas = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(n.navigation_id) as visitas
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							$condicion
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation)
							ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";

			}else{

				$query_visitantes_unicos = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(distinct n.user_id) as visitantes_unicos
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges_users cu, ludus_charges c
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							and u.user_id = cu.user_id
							and cu.charge_id = c.charge_id
							$condicion
							and c.charge_id = $cargo
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation) ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";

							// echo "$query_visitantes_unicos";
				$query_visitas = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(n.navigation_id) as visitas
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges_users cu
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							and u.user_id = cu.user_id
                            $condicion
							and cu.charge_id = $cargo
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation)
							ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";
							// echo "$query_visitas";

					}

		$DataBase_Acciones = new Database();
		$visitantes_unicos = $DataBase_Acciones->SQL_SelectMultipleRows($query_visitantes_unicos);


		$visitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_visitas);

		foreach ($visitas as $key => $value) {
			$visitantes_unicos[$key]['visitas']=$value['visitas'];

		}



		foreach ($visitantes_unicos as $key => $value) {
			$ano = $value['ano'];
			$mes = $value['mes'];
			$query_cedulas = "SELECT count(distinct u.user_id) as cant, '1' as mes
					FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges_users cu, ludus_charges c
					WHERE u.status_id = 1 AND u.date_creation < '$ano-$mes-01'
					and u.headquarter_id = h.headquarter_id
					and h.dealer_id = d.dealer_id
					and n.user_id = u.user_id
					and h.area_id = a.area_id
					and z.zone_id = a.zone_id
					and u.user_id = cu.user_id
					and cu.charge_id = c.charge_id
					$condicion";
					if ($cargo!="") {
						$query_cedulas.=" and cu.charge_id = $cargo";
					}
					$cantidad_cedulas = $DataBase_Acciones->SQL_SelectMultipleRows($query_cedulas);
					// print_r($cantidad_cedulas);
					$visitantes_unicos[$key]['cant_cedulas']= $cantidad_cedulas[0]['cant'];
		}

		unset($DataBase_Acciones);
		return $visitantes_unicos;
	}

	function consulta_sedes(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_headquarters order by headquarter;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_zona(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_zone;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_concesioanrios(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT * FROM ludus_dealers order by dealer;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
