<?php
class Cargos
{
	function consultaDatossedes($sWhere, $sOrder, $sLimit)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, z.course,  d.first_name as first_prof, d.last_name as last_prof, l.living, z.newcode, m.duration_time, m.module, z.type
			FROM ludus_schedule c, ludus_users s, ludus_status e, ludus_courses z, ludus_users d, ludus_livings l, ludus_modules m
			WHERE c.editor = s.user_id AND c.status_id = e.status_id
			-- AND c.course_id = z.course_id
			AND c.teacher_id = d.user_id AND l.living_id = c.living_id AND z.course_id = m.course_id AND m.module_id=c.module_id ";
		//Aplica los filtros
		if (isset($_SESSION['course_id_search_prfl']) && ($_SESSION['course_id_search_prfl'] != "0")) {
			$course_id_search = $_SESSION['course_id_search_prfl'];
			$query_sql .= " AND z.course_id = '$course_id_search' ";
		}
		if (isset($_SESSION['teacher_id_search_prfl']) && ($_SESSION['teacher_id_search_prfl'] != "0")) {
			$teacher_id_search = $_SESSION['teacher_id_search_prfl'];
			$query_sql .= " AND d.user_id = '$teacher_id_search' ";
		}
		if (isset($_SESSION['status_id_search_prfl']) && ($_SESSION['status_id_search_prfl'] != "0")) {
			$status_id_search = $_SESSION['status_id_search_prfl'];
			$query_sql .= " AND c.status_id = '$status_id_search' ";
		}
		if (isset($_SESSION['living_id_search_prfl']) && ($_SESSION['living_id_search_prfl'] != "0")) {
			$living_id_search = $_SESSION['living_id_search_prfl'];
			$query_sql .= " AND l.living_id = '$living_id_search' ";
		}
		if (isset($_SESSION['start_date_search_prfl']) && ($_SESSION['start_date_search_prfl'] != "0")) {
			$start_date_search = $_SESSION['start_date_search_prfl'];
			$query_sql .= " AND c.start_date >=  '$start_date_search' ";
		}
		if (isset($_SESSION['end_date_search_prfl']) && ($_SESSION['end_date_search_prfl'] != "0")) {
			$end_date_search = $_SESSION['end_date_search_prfl'];
			$query_sql .= " AND c.start_date <=  '$end_date_search' ";
		}
		//Aplica los filtros
		if ($sWhere != '') {
			$query_sql .= " AND (c.schedule_id LIKE '%$sWhere%' or c.start_date LIKE '%$sWhere%' OR c.end_date LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR z.newcode LIKE '%$sWhere%' OR c.max_inscription_date LIKE '%$sWhere%'  OR c.min_size LIKE '%$sWhere%' OR c.max_size LIKE '%$sWhere%' OR c.waiting_size LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR d.first_name LIKE '%$sWhere%' OR d.last_name LIKE '%$sWhere%' OR l.living LIKE '%$sWhere%' ) ";
		}
		$query_sql .= $sOrder;
		$query_sql .= ' ' . $sLimit;
		$DataBase_Acciones = new Database();
		//echo $query_sql;
		//die();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	public static function DatosCompletosProgramacion($idRegistro)
	{
		include_once('../../config/init_db.php');
		$query_sql = "SELECT c.*,m.type as tipemodule, s.first_name as nom_edita, s.last_name as ape_edita, z.course, z.type,
							d.first_name as first_prof, d.last_name as last_prof, l.living, l.address, s.email, d.email as email_prof, z.newcode,
							m.module, m.newcode as codemod
					FROM ludus_schedule c, ludus_users s, ludus_status e, ludus_courses z, ludus_users d, ludus_livings l, ludus_modules m
					WHERE c.editor = s.user_id
					AND c.status_id = e.status_id
					-- AND c.course_id = z.course_id
					AND m.module_id=c.module_id
					AND c.teacher_id = d.user_id
					AND l.living_id = c.living_id
					AND c.schedule_id = '$idRegistro' ";

		$Rows_config = DB::queryFirstRow($query_sql);
		return $Rows_config;
	}
	function DatosCompletosInvolucrados_Jefes($idRegistro)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT DISTINCT u.email
					FROM ludus_dealer_schedule s, ludus_headquarters h, ludus_users u, ludus_roles_users r
					WHERE s.schedule_id = '$idRegistro' AND s.max_size > 0 AND s.dealer_id = h.dealer_id AND h.headquarter_id = u.headquarter_id AND
					u.user_id = r.user_id AND r.rol_id = 4 ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$emails = "";
		for ($i = 0; $i < count($Rows_config); $i++) {
			$var_User = $Rows_config[$i]['email'];
			$emails .= $var_User . ",";
		}
		unset($DataBase_Acciones);
		return $emails;
	}
	function DatosCompletosInvolucrados($idRegistro)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT DISTINCT u.email
					FROM ludus_invitation i, ludus_users u
					WHERE i.schedule_id = '$idRegistro' AND i.user_id = u.user_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere, $sOrder, $sLimit)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, z.course,  d.first_name as first_prof, d.last_name as last_prof, l.living, z.newcode
			FROM ludus_schedule c, ludus_users s, ludus_status e, ludus_courses z, ludus_users d, ludus_livings l
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id AND c.teacher_id = d.user_id AND l.living_id = c.living_id ";
		//Aplica los filtros
		if (isset($_SESSION['course_id_search_prfl']) && ($_SESSION['course_id_search_prfl'] != "0")) {
			$course_id_search = $_SESSION['course_id_search_prfl'];
			$query_sql .= " AND z.course_id = '$course_id_search' ";
		}
		if (isset($_SESSION['teacher_id_search_prfl']) && ($_SESSION['teacher_id_search_prfl'] != "0")) {
			$teacher_id_search = $_SESSION['teacher_id_search_prfl'];
			$query_sql .= " AND d.user_id = '$teacher_id_search' ";
		}
		if (isset($_SESSION['status_id_search_prfl']) && ($_SESSION['status_id_search_prfl'] != "0")) {
			$status_id_search = $_SESSION['status_id_search_prfl'];
			$query_sql .= " AND c.status_id = '$status_id_search' ";
		}
		if (isset($_SESSION['living_id_search_prfl']) && ($_SESSION['living_id_search_prfl'] != "0")) {
			$living_id_search = $_SESSION['living_id_search_prfl'];
			$query_sql .= " AND l.living_id = '$living_id_search' ";
		}
		if (isset($_SESSION['start_date_search_prfl']) && ($_SESSION['start_date_search_prfl'] != "0")) {
			$start_date_search = $_SESSION['start_date_search_prfl'];
			$query_sql .= " AND c.start_date >=  '$start_date_search' ";
		}
		if (isset($_SESSION['end_date_search_prfl']) && ($_SESSION['end_date_search_prfl'] != "0")) {
			$end_date_search = $_SESSION['end_date_search_prfl'];
			$query_sql .= " AND c.start_date <=  '$end_date_search' ";
		}
		//Aplica los filtros
		if ($sWhere != '') {
			$query_sql .= " AND (c.start_date LIKE '%$sWhere%' OR c.end_date LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR z.newcode LIKE '%$sWhere%' OR c.max_inscription_date LIKE '%$sWhere%'  OR c.min_size LIKE '%$sWhere%' OR c.max_size LIKE '%$sWhere%' OR c.waiting_size LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR d.first_name LIKE '%$sWhere%' OR d.last_name LIKE '%$sWhere%' OR l.living LIKE '%$sWhere%' ) ";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad()
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, z.course,  d.first_name as first_prof, d.last_name as last_prof, l.living, z.newcode
			FROM ludus_schedule c, ludus_users s, ludus_status e, ludus_courses z, ludus_users d, ludus_livings l
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id AND c.teacher_id = d.user_id AND l.living_id = c.living_id "; //LIMIT 0,20
		//Aplica los filtros
		if (isset($_SESSION['course_id_search_prfl']) && ($_SESSION['course_id_search_prfl'] != "0")) {
			$course_id_search = $_SESSION['course_id_search_prfl'];
			$query_sql .= " AND z.course_id = '$course_id_search' ";
		}
		if (isset($_SESSION['teacher_id_search_prfl']) && ($_SESSION['teacher_id_search_prfl'] != "0")) {
			$teacher_id_search = $_SESSION['teacher_id_search_prfl'];
			$query_sql .= " AND d.user_id = '$teacher_id_search' ";
		}
		if (isset($_SESSION['status_id_search_prfl']) && ($_SESSION['status_id_search_prfl'] != "0")) {
			$status_id_search = $_SESSION['status_id_search_prfl'];
			$query_sql .= " AND c.status_id = '$status_id_search' ";
		}
		if (isset($_SESSION['living_id_search_prfl']) && ($_SESSION['living_id_search_prfl'] != "0")) {
			$living_id_search = $_SESSION['living_id_search_prfl'];
			$query_sql .= " AND l.living_id = '$living_id_search' ";
		}
		if (isset($_SESSION['start_date_search_prfl']) && ($_SESSION['start_date_search_prfl'] != "0")) {
			$start_date_search = $_SESSION['start_date_search_prfl'];
			$query_sql .= " AND c.start_date >=  '$start_date_search' ";
		}
		if (isset($_SESSION['end_date_search_prfl']) && ($_SESSION['end_date_search_prfl'] != "0")) {
			$end_date_search = $_SESSION['end_date_search_prfl'];
			$query_sql .= " AND c.start_date <=  '$end_date_search' ";
		}
		//Aplica los filtros
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function BorraSchedule_ind($schedule_id, $dealer_id, $dataSeleccionados)
	{
		include_once('../../config/database.php');
		include('../../config/config.php');
		$resultado = 0;
		if ($dataSeleccionados == "0,") {
			$dataSeleccionados = "0,0";
		}
		$DataBase_Log = new Database();
		$updateSQL_ER = "DELETE FROM ludus_dealer_schedule WHERE schedule_id = '$schedule_id' AND dealer_id NOT IN ($dataSeleccionados) )";
		$resultado_br = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function insertSchedule($start_date_day, $start_date_time, $end_date_day, $end_date_time, $max_inscription_date, $living_id, $min_size, $max_size, $waiting_size, $course_id, $teacher_id, $module_id, $teacher_id_aux)
	{
		@session_start();
		include_once('../../config/database.php');
		include('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$review_date_day = (strtotime($end_date_day . "+ 7 days"));
		$review_date_day =  date("Y-m-d", $review_date_day) . " 00:00:01";

		$DataBase_Log = new Database();

		$insertSQL_EE = "INSERT INTO ludus_schedule (status_id,creator,editor,date_creation,date_edition,start_date,end_date,max_inscription_date,	review_date,living_id,min_size,max_size,waiting_size,course_id,teacher_id, module_id, finalizado) VALUES ('1','$idQuien','$idQuien','$NOW_data','$NOW_data','$start_date_day $start_date_time', '$end_date_day $end_date_time','$max_inscription_date','$review_date_day','$living_id','$min_size','$max_size','$waiting_size','$course_id','$teacher_id', '$module_id', 0 )";
		//echo($insertSQL_EE);
		$schedule_id = $DataBase_Log->SQL_Insert($insertSQL_EE);
		foreach ($teacher_id_aux as $key => $teacher_id_av) {
			$query_aux = "INSERT INTO ludus_teachers_aux
								(teacher_id,
								schedule_id
								)
								VALUES
								(
								$teacher_id_av,
								$schedule_id);";
			$DataBase_Log->SQL_Update($query_aux);
		}
		unset($DataBase_Log);
		return $schedule_id;
	}
	function Crear_InvDealer($dealer_id, $schedule_id, $min_size, $max_size)
	{
		@session_start();
		include_once('../../config/database.php');
		include('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$resultado = 0;
		/*$insertSQL_EE = "INSERT INTO erum_logacceso (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración cargos: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		/*if($max_size<=0){
			$deleteSQL_EE = "DELETE FROM ludus_dealer_schedule WHERE dealer_id = '$dealer_id' AND schedule_id = '$schedule_id'";
			$resultado = $DataBase_Log->SQL_Update($deleteSQL_EE);
			//echo($deleteSQL_EE.'<br>');
		}*/
		$query = "SELECT * FROM ludus_dealer_schedule WHERE dealer_id = $dealer_id AND schedule_id = $schedule_id AND status_id = 1";
		//echo($query);
		$resultSet = $DataBase_Log->SQL_SelectRows($query);

		if (!isset($resultSet['dealer_id'])) {
			$insertSQL_EE = "INSERT INTO ludus_dealer_schedule (dealer_id,schedule_id,min_size,max_size,inscriptions,date_creation,date_edition,creator,editor,status_id)
				VALUES ($dealer_id,$schedule_id,$min_size,$max_size,0,'$NOW_data','$NOW_data','$idQuien','$idQuien',1)";
			// echo($insertSQL_EE.'<br>');
			$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		}
		unset($DataBase_Log);
		return $resultado;
	}
	function Editar_InvDealer($dealer_id, $schedule_id, $min_size, $max_size)
	{
		@session_start();
		include_once('../../config/database.php');
		include('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*if($max_size<=0){
			$deleteSQL_EE = "DELETE FROM ludus_dealer_schedule WHERE dealer_id = '$dealer_id' AND schedule_id = '$schedule_id'";
			$resultado = $DataBase_Log->SQL_Update($deleteSQL_EE);
			//echo($deleteSQL_EE.'<br>');
		}*/
		$updateSQL_ER = "UPDATE ludus_dealer_schedule SET min_size='$min_size',max_size='$max_size',date_edition='$NOW_data',editor='$idQuien' WHERE dealer_id = '$dealer_id' AND schedule_id = '$schedule_id' ";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		//echo($updateSQL_ER.'<br>');
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarcargos($id, $status_id, $start_date_day, $start_date_time, $end_date_day, $end_date_time, $max_inscription_date, $living_id, $min_size, $max_size, $waiting_size, $course_id, $teacher_id, $module_id)
	{
		@session_start();
		include_once('../../config/database.php');
		include('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$review_date_day = (strtotime($end_date_day . "+ 7 days"));
		$review_date_day =  date("Y-m-d", $review_date_day) . " 00:00:01";

		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_schedule SET start_date = '$start_date_day $start_date_time', status_id = '$status_id', editor = '$idQuien', date_edition = '$NOW_data', end_date = '$end_date_day $end_date_time', max_inscription_date = '$max_inscription_date', review_date = '$review_date_day', living_id = '$living_id', min_size = '$min_size', max_size = '$max_size', waiting_size = '$waiting_size', course_id = '$course_id', teacher_id = '$teacher_id', module_id = $module_id WHERE schedule_id = '$id'";

		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}

	function ActualizarIntructoresAux($p)
	{
		include_once('../../config/database.php');
		include('../../config/config.php');

		$DataBase_Log = new Database();

		$query_del = "DELETE from ludus_teachers_aux where schedule_id = {$p['idElemento']}";
		$resultado = $DataBase_Log->SQL_Update($query_del);

		if (isset($p['teacher_id_aux'])) {
			foreach ($p['teacher_id_aux'] as $key => $teacher_id_av) {
				$query_aux = "INSERT INTO ludus_teachers_aux
										(teacher_id,
										schedule_id
										)
										VALUES
										(
										$teacher_id_av,
										{$p['idElemento']});";
				$resultado = $DataBase_Log->SQL_Update($query_aux);
			}
		}

		return $resultado;
	}

	function consultaRegistro($idRegistro)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_schedule c
			WHERE c.schedule_id = '$idRegistro' "; //LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCupos($prof, $living_id)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT l.vacant, l.city_id
			FROM ludus_livings l
			WHERE l.living_id = '$living_id' "; //LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.course_id, c.course, c.newcode
						FROM ludus_courses c
						WHERE c.status_id = 1 
						ORDER BY c.course_id DESC";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaProfesores($prof)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.user_id, c.first_name, c.last_name
						FROM ludus_users c, ludus_roles_users u
						WHERE c.status_id = 1 AND c.user_id = u.user_id AND u.status_id = 1 AND u.rol_id = 7
						ORDER BY c.first_name";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaProfesoresAux($schedule_id)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');

		$query_sql = "SELECT * FROM ludus_teachers_aux ta
						inner join ludus_users u
							on ta.teacher_id = u.user_id
						where ta.schedule_id = $schedule_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaSalones($prof)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.living_id, c.living, c.city_id, c.address
						FROM ludus_livings c
						WHERE c.status_id = 1
						ORDER BY c.living";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaDisponibles_lugar($prof, $living_id, $course_id, $city_id, $Schedule_id_sel, $module_id)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.newcode, c.type, c.course, d.dealer, d.dealer_id, a.area, count(u.user_id) as cant_usr
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d, ludus_areas a
					WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND cc.course_id = '$course_id'
					AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score > 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1 AND c.course_id = '$course_id' AND mr.module_id = '$module_id')
					AND h.area_id = '$city_id' AND h.area_id = a.area_id AND u.status_id = 1 AND cc.status_id = 1 AND c.status_id = 1
					GROUP BY c.newcode, c.type, c.course, d.dealer, d.dealer_id, a.area
					ORDER BY cant_usr DESC";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		$query_usuarios = "SELECT distinct d.dealer_id, u.user_id, u.first_name, u.last_name, u.identification, d.dealer, h.headquarter, u.identification
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d
					WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND cc.course_id = '$course_id'
					AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score > 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1 AND c.course_id = '$course_id' AND mr.module_id = '$module_id')
					AND h.area_id = '$city_id' AND u.status_id = 1 AND cc.status_id = 1 AND c.status_id = 1
					ORDER BY d.dealer, h.headquarter";
		$Rows_usuarios = $DataBase_Class->SQL_SelectMultipleRows($query_usuarios);

		$query_YaProg = "SELECT dealer_id, min_size, max_size FROM ludus_dealer_schedule WHERE schedule_id = $Schedule_id_sel";
		$Rows_programaciones = $DataBase_Class->SQL_SelectMultipleRows($query_YaProg);

		for ($i = 0; $i < count($Rows_config); $i++) {
			$var_User = $Rows_config[$i]['dealer_id'];
			//Cargos del usuario
			for ($y = 0; $y < count($Rows_usuarios); $y++) {
				if ($var_User == $Rows_usuarios[$y]['dealer_id']) {
					$Rows_config[$i]['personas'][] = $Rows_usuarios[$y];
				}
			}
			//Cargos del usuario
			//Programacion Anterior
			for ($y = 0; $y < count($Rows_programaciones); $y++) {
				if ($var_User == $Rows_programaciones[$y]['dealer_id']) {
					$Rows_config[$i]['minsize_ya'] = $Rows_programaciones[$y]['min_size'];
					$Rows_config[$i]['maxsize_ya'] = $Rows_programaciones[$y]['max_size'];
				}
			}
			//Programacion Anterior
		}
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaDisponibles_nolugar($prof, $living_id, $course_id, $city_id, $Schedule_id_sel, $module_id)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.type, c.newcode, c.course, d.dealer, d.dealer_id, count(u.user_id) as cant_usr
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d
					WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND cc.course_id = '$course_id'
					AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score > 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1 AND c.course_id = '$course_id' AND mr.module_id = '$module_id')
					AND h.area_id <> '$city_id' AND u.status_id = 1 AND cc.status_id = 1 AND c.status_id = 1
					GROUP BY c.type, c.newcode, c.course, d.dealer, d.dealer_id
					ORDER BY dealer";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		$query_ciud = "SELECT DISTINCT d.dealer_id, a.area
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d, ludus_areas a
					WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND cc.course_id = '$course_id'
					AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score > 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1 AND c.course_id = '$course_id' AND mr.module_id = '$module_id')
					AND h.area_id <> '$city_id' AND h.area_id = a.area_id AND u.status_id = 1 AND cc.status_id = 1 AND c.status_id = 1
					ORDER BY d.dealer_id, a.area ";
		$DataBase_Class = new Database();
		$Rows_ciudades = $DataBase_Class->SQL_SelectMultipleRows($query_ciud);

		$query_usuarios = "SELECT distinct d.dealer_id, u.user_id, u.first_name, u.last_name, u.identification, d.dealer, h.headquarter, u.identification, a.area
					FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d, ludus_areas a
					WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND cc.course_id = '$course_id'
					AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score > 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1 AND c.course_id = '$course_id' AND mr.module_id = '$module_id')
					AND h.area_id <> '$city_id' AND h.area_id = a.area_id AND u.status_id = 1 AND cc.status_id = 1 AND c.status_id = 1
					ORDER BY d.dealer, h.headquarter";
		$Rows_usuarios = $DataBase_Class->SQL_SelectMultipleRows($query_usuarios);

		$query_YaProg = "SELECT dealer_id, min_size, max_size FROM ludus_dealer_schedule WHERE schedule_id = $Schedule_id_sel";
		$Rows_programaciones = $DataBase_Class->SQL_SelectMultipleRows($query_YaProg);

		for ($i = 0; $i < count($Rows_config); $i++) {
			$var_User = $Rows_config[$i]['dealer_id'];
			//Personas
			for ($y = 0; $y < count($Rows_usuarios); $y++) {
				if ($var_User == $Rows_usuarios[$y]['dealer_id']) {
					$Rows_config[$i]['personas'][] = $Rows_usuarios[$y];
				}
			}
			//Personas
			//Ciudades del dealer
			for ($y = 0; $y < count($Rows_ciudades); $y++) {
				if ($var_User == $Rows_ciudades[$y]['dealer_id']) {
					$Rows_config[$i]['ciudades'][] = $Rows_ciudades[$y];
				}
			}
			//Ciudades del dealer
			//Programacion Anterior
			for ($y = 0; $y < count($Rows_programaciones); $y++) {
				if ($var_User == $Rows_programaciones[$y]['dealer_id']) {
					$Rows_config[$i]['minsize_ya'] = $Rows_programaciones[$y]['min_size'];
					$Rows_config[$i]['maxsize_ya'] = $Rows_programaciones[$y]['max_size'];
				}
			}
			//Programacion Anterior
		}
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaCreada($prof, $Schedule_id_sel)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT * FROM ludus_dealer_schedule s WHERE s.schedule_id = $Schedule_id_sel ORDER BY dealer_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function ConsultaNotificaciones($FechaConsulta)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, l.living, l.address, concat(t.first_name,' ',t.last_name) as profesor, concat(c.newcode,' : ',c.course) as curso, d.dealer,
			d.dealer_id, u.user_id, concat(u.first_name,' ',u.last_name) as coordinador, u.email
						FROM ludus_schedule s, ludus_livings l, ludus_users t, ludus_courses c, ludus_dealer_schedule ds, ludus_dealers d, ludus_roles_users ru, ludus_users u, ludus_headquarters h
						WHERE s.start_date BETWEEN '$FechaConsulta 00:00:00' AND '$FechaConsulta 23:59:59'
						AND s.status_id = 1
						AND s.living_id = l.living_id
						AND s.teacher_id = t.user_id
						AND s.course_id = c.course_id
						AND s.schedule_id = ds.schedule_id
						AND ds.min_size > 0 AND ds.max_size > 0 AND ds.inscriptions > 0 AND ds.status_id = 1
						AND ds.dealer_id = d.dealer_id
						AND ru.rol_id = 4 AND ru.user_id = u.user_id
						AND u.headquarter_id = h.headquarter_id
						AND h.dealer_id = d.dealer_id
						AND u.email IS NOT NULL";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function ConsultaQuienesTienen($FechaConsulta)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, l.living, l.address, concat(t.first_name,' ',t.last_name) as profesor,
					concat(c.newcode,' : ',c.course) as curso, concat(u.first_name,' ',u.last_name) as alumno, u.email
					FROM ludus_schedule s, ludus_livings l, ludus_users t, ludus_courses c, ludus_users u, ludus_invitation i
					WHERE s.start_date BETWEEN '$FechaConsulta 00:00:00' AND '$FechaConsulta 23:59:59'
					AND s.status_id = 1
					AND s.living_id = l.living_id
					AND s.teacher_id = t.user_id
					AND s.course_id = c.course_id
					AND s.schedule_id = i.schedule_id
					AND i.user_id = u.user_id
					AND u.email IS NOT NULL AND u.email like '%@%' AND u.email like '%.%'";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaTodaProgramacion()
	{
		include_once('../config/init_db.php');
		@session_start();
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, z.course,  d.first_name as first_prof, d.last_name as last_prof, l.living, z.newcode, '' AS trayectorias, sp.specialty, m.duration_time
			FROM ludus_schedule c, ludus_users s, ludus_status e, ludus_courses z, ludus_users d, ludus_livings l, ludus_specialties sp, ludus_modules m
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id AND c.teacher_id = d.user_id AND l.living_id = c.living_id AND z.specialty_id = sp.specialty_id AND z.course_id = m.course_id ";
		//Aplica los filtros
		if (isset($_SESSION['course_id_search_prfl']) && ($_SESSION['course_id_search_prfl'] != "0")) {
			$course_id_search = $_SESSION['course_id_search_prfl'];
			$query_sql .= " AND z.course_id = '$course_id_search' ";
		}
		if (isset($_SESSION['teacher_id_search_prfl']) && ($_SESSION['teacher_id_search_prfl'] != "0")) {
			$teacher_id_search = $_SESSION['teacher_id_search_prfl'];
			$query_sql .= " AND d.user_id = '$teacher_id_search' ";
		}
		if (isset($_SESSION['status_id_search_prfl']) && ($_SESSION['status_id_search_prfl'] != "0")) {
			$status_id_search = $_SESSION['status_id_search_prfl'];
			$query_sql .= " AND c.status_id = '$status_id_search' ";
		}
		if (isset($_SESSION['living_id_search_prfl']) && ($_SESSION['living_id_search_prfl'] != "0")) {
			$living_id_search = $_SESSION['living_id_search_prfl'];
			$query_sql .= " AND l.living_id = '$living_id_search' ";
		}
		if (isset($_SESSION['start_date_search_prfl']) && ($_SESSION['start_date_search_prfl'] != "0")) {
			$start_date_search = $_SESSION['start_date_search_prfl'];
			$query_sql .= " AND c.start_date >=  '$start_date_search' ";
		}
		if (isset($_SESSION['end_date_search_prfl']) && ($_SESSION['end_date_search_prfl'] != "0")) {
			$end_date_search = $_SESSION['end_date_search_prfl'];
			$query_sql .= " AND c.start_date <=  '$end_date_search' ";
		}
		//Aplica los filtros
		$query_sql .= " ORDER BY c.start_date ASC";
		$Rows_config = DB::query($query_sql);

		$cursos = DB::query('SELECT cc.course_id, c.charge FROM ludus_charges_courses cc, ludus_charges c WHERE cc.charge_id = c.charge_id');

		$cuenta_r = count($Rows_config);
		for ($i = 0; $i < $cuenta_r; $i++) {
			foreach ($cursos as $key => $c) {
				if ($Rows_config[$i]['course_id'] == $c['course_id']) {
					$Rows_config[$i]['trayectorias'] .= " - " . $c['charge'];
				}
			}
		}

		return $Rows_config;
	}
	//==============================================================================
	//Consulta los datos del schedule
	//==============================================================================
	function consultaAsignacion($schedule_id, $prof = "")
	{
		include_once($prof . '../config/database.php');
		include_once($prof . '../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.course, l.living, l.address, s.status_id, d.area, s.max_size, s.inscriptions, s.max_inscription_date, c.newcode, c.type, s.image_ics
						FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_areas d
						WHERE s.course_id = c.course_id AND s.living_id = l.living_id AND s.schedule_id = '$schedule_id' AND l.city_id = d.area_id ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//==============================================================================
	//guarda el nombre del archivo en el schedule
	//==============================================================================
	function guardarICS($schedule, $archivo, $prof = "")
	{
		include_once($prof . '../config/database.php');
		include_once($prof . '../config/config.php');
		$query = "UPDATE ludus_schedule SET image_ics = '$archivo' WHERE schedule_id = $schedule";
		//echo( $query );
		$DataBase_Log = new Database();
		$resultado = $DataBase_Log->SQL_Update($query);
		return $resultado;
	}
	//==============================================================================
	//Consulta el nombre del salon seleccinado
	//==============================================================================
	function consultaSalon($living_id, $prof = "")
	{
		include_once($prof . '../config/database.php');
		include_once($prof . '../config/config.php');
		$query_sql = "SELECT l.*
						FROM ludus_livings l
						WHERE l.living_id = $living_id ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//==============================================================================
	//Consulta el nombre del salon seleccinado
	//==============================================================================
	public static function consultaInstructos($user_id, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		$query_sql = "SELECT u.user_id, u.email, u.first_name, u.last_name, u.identification
						FROM ludus_users u
						WHERE u.user_id = $user_id ";
		$Rows_config = DB::queryFirstRow($query_sql);
		return $Rows_config;
	}

	public function getModulesCourse($schedule_id, $prof = "")
	{
		include_once($prof . '../config/init_db.php');
		$query_sql = "SELECT mo.module_id, mo.module from ludus_modules mo
						INNER JOIN ludus_courses co
							on mo.course_id = co.course_id
						INNER JOIN ludus_schedule sch
							on co.course_id = sch.course_id
							and sch.schedule_id = $schedule_id";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public function getModules($course_id, $schedule_id)
	{
		include_once('../../config/init_db.php');

		if ($schedule_id == '') {
			$query_sql = "SELECT mo.module_id, mo.module from ludus_modules mo
						INNER JOIN ludus_courses co
							on mo.course_id = co.course_id
							and co.course_id = $course_id";
			$Rows_config = DB::query($query_sql);
			$Rows_sch['module_id'] = 0;
		} else {
			$query_mo = "SELECT module_id FROM ludus_schedule where schedule_id= $schedule_id;";
			$Rows_sch = DB::queryFirstRow($query_mo);

			$query_sql = "SELECT mo.module_id, mo.module from ludus_modules mo
						INNER JOIN ludus_courses co
							on mo.course_id = co.course_id
							and co.course_id = $course_id";
			$Rows_config = DB::query($query_sql);
		}

		foreach ($Rows_config as $key => $value) {
			if ($value['module_id'] == $Rows_sch['module_id']) {
				$Rows_config[$key]['selected'] = "selected";
			} else {
				$Rows_config[$key]['selected'] = "";
			}
		}

		return $Rows_config;
	}

	public function getModulesActivities($module_id, $schedule_id)
	{
		include_once('../../config/init_db.php');
		/* 	$query_sql = "SELECT acsc.activity_schedule_id, moac.module_id, moac.activity_id, moac.activity, acsc.desde, acsc.hasta
							FROM ludus_modules_activities moac
								LEFT JOIN ludus_activities_schedule acsc
									ON moac.activity_id = acsc.activity_id
										and  acsc.schedule_id = $schedule_id
								where moac.module_id = $module_id
								order by orden asc;"; */

		$query_sql = "SELECT
										acsc.activity_schedule_id,
										acsc.schedule_id,
										moac.module_id,
										moac.activity_id,
										moac.activity,
										acsc.desde,
										acsc.hasta
										from
											ludus_modules_activities moac
										left join ludus_activities_schedule acsc
																		on
											moac.activity_id = acsc.activity_id
											and acsc.schedule_id = $schedule_id
										where
											moac.module_id = $module_id
										order by
											orden asc
					
					
					";

$Rows_config = DB::query($query_sql);
	
		// var_dump($Rows_config); die();

		foreach ($Rows_config as $key => $value) {

			if (is_null($value['activity_schedule_id'])) {
				$Rows_config[$key]['activity_schedule_id'] = '';
			}

			$Rows_config[$key]['schedule_id'] = $schedule_id;

			if (is_null($value['desde'])) {
				$Rows_config[$key]['desde'] = '';
			} else {
				$Rows_config[$key]['desde'] = substr($value['desde'], 0, -9);
			}

			if (is_null($value['hasta'])) {
				$Rows_config[$key]['hasta'] = '';
			} else {
				$Rows_config[$key]['hasta'] = substr($value['hasta'], 0, -9);
			}
		}

		return $Rows_config;
	
	}


	public function programar_actividades($p)
	{
		include_once('../../config/init_db.php');
		$date_edition 	= date("Y-m-d") . " " . date("H") . ":" . date("i:s");
		@session_start();
		$editor = $_SESSION['idUsuario'];

		$json = array();
		$json['msj'] = 'No se realizó ningún cambio';
		$json['error'] = true;

		foreach ($p as $key => $value) {
			extract($value);

			if ($activity_schedule_id == '') {
				$query__insert = "INSERT INTO ludus_activities_schedule
						(
						activity_id,
						schedule_id,
						desde,
						hasta,
						editor,
						date_edition
						)
						VALUES
						(
						'$activity_id',
						'$schedule_id',
						'$desde 00:00:00',
						'$hasta 23:59:59',
						'$editor',
						'$date_edition'
						);";
				$res = DB::query($query__insert);
				if ($res) {
					$json['msj'] = 'Actividad creada';
				}
			} else {
				$query_update = "UPDATE ludus_activities_schedule
							SET
							desde 					= '$desde 00-00-00',
							hasta 					= '$hasta 23:59:59',
							editor 					= '$editor',
							date_edition 			= '$date_edition'
							WHERE activity_schedule_id = $activity_schedule_id;";
				$res = DB::query($query_update);
				if ($res) {
					$json['msj'] = 'Actividad actualizada';
				}
			}
		}

		if ($res) {
			$json['error'] = false;
		}
		// print_r($json);
		// die();
		return $json;
	}

	public function getToken($schedule_id)
	{
		include_once('../../config/init_db.php');
		$query_update = "SELECT session_id, token from ludus_schedule where schedule_id = $schedule_id;";
		$res = DB::query($query_update);
		return $res[0];
	}

	public function asignar_token($p)
	{
		include_once('../../config/init_db.php');
		extract($p);
		$query_update = "UPDATE ludus_schedule SET session_id = '$session_id', token = '$token' WHERE schedule_id = '$schedule_id';";
		$res = DB::query($query_update);

		if ($res) {
			$json['error'] = false;
			$json['msj'] = "Token establecido correctamente";
			$json['type'] = "success";
		} else {
			$json['error'] = true;
			$json['msj'] = "No se pudo establecer el token";
			$json['type'] = "error";
		}
		return $json;
	}
}
