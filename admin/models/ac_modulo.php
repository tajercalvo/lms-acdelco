<?php

class Ac_modulo
{

	public function getActividades($module_id)
	{
		include_once('../config/init_db.php');
		//DB::$encoding = 'utf8';
		@session_start();

		$query_mo = "SELECT * FROM ludus_modules where module_id = $module_id ";
		$modulo = DB::queryFirstRow($query_mo);

		$query_eval = "SELECT review_id, review FROM ludus_reviews where status_id = 1;";
		$evaluaciones = DB::query($query_eval);

		$query_act = "SELECT ac.*, mo.module, u.first_name, u.last_name, re.review, st.status
		FROM ludus_modules_activities ac
			INNER JOIN ludus_modules mo
				   on ac.module_id = mo.module_id
			INNER JOIN ludus_status st
				on ac.status_id = st.status_id
			INNER JOIN ludus_users u
				on ac.editor = u.user_id
			LEFT JOIN ludus_reviews re
				on ac.review_id = re.review_id
			where ac.module_id = $module_id
			ORDER BY ac.orden desc";
		$actividades = DB::query($query_act);

		$datos = array();
		$datos['mod'] = $modulo;
		$datos['act'] = $actividades;
		$datos['evaluaciones'] = $evaluaciones;
		return $datos;
	}


	public function valida_actividades($p)
	{
		extract($p);
		include_once('../../config/init_db.php');
		$query_valida = "SELECT * FROM ludus_modules_activities where module_id = $module_id and activity = '$activity';";
		$res_valida = DB::query($query_valida);
		$json = array();

		if (count($res_valida) > 0) {
			$json['error'] = true;
			$json['msj']   = 'El nombre de la actividad ya existe para este módulo';
		} else {
			$json['error'] = false;
		}
		return $json;
	}
	public function crearActividades($p)
	{
		extract($p);
		@session_start();
		$editor = $_SESSION['idUsuario'];
		include_once('../../config/init_db.php');
		$date_edition 	= date("Y-m-d") . " " . date("H") . ":" . date("i:s");

		$query_insert_enc = "INSERT INTO ludus_modules_activities
											(
											module_id,
											activity,
											orden,
											tipo,
											resultado,
											porcentaje,
											adjuntos,
											status_id,
											date_edition,
											editor,
											instrucciones,
											adjunto,
											link_video,
											review_id
											)
											VALUES
											(
											 $module_id,
											'$activity',
											'$orden',
											'$tipo',
											'$resultado',
											'$porcentaje',
											'$adjuntos',
											1,
											'$date_edition',
											'$editor',
											'$instrucciones',
											'$adjunto',
											'$video',
											'$review_id'
											);
											";
		// echo $query_insert_enc;
		$result = DB::query($query_insert_enc);
		$json = array();
		if ($result) {
			$json['error'] = false;
			$json['msj']   = 'Actividad creada correctamente';
		} else {
			$json['error'] = true;
			$json['msj']   = 'Error al crear la actividad';
		}

		return $json;
	}

	public function borrar($p)
	{
		extract($p);
		include_once('../../config/init_db.php');

		$query_valida = "SELECT * FROM ludus_activities_schedule actes
								inner join ludus_activities_user actusr
									on actes.activity_schedule_id = actusr.activity_schedule_id
									where actes.activity_id = $activity_id;";
		$res = DB::query($query_valida);

		$json = array();
		if (count($res) > 0) {
			$json['error'] = true;
			$json['msj']   = 'No se puede borrar la actividad porque algunos usuarios han respondido';
		} else {

			$query = "DELETE FROM ludus_activities_schedule WHERE activity_id = '$activity_id';";
			$result = DB::query($query);

			$query = "DELETE FROM ludus_modules_activities WHERE activity_id = '$activity_id';";
			$result = DB::query($query);

			if ($result) {
				$json['error'] = false;
				$json['msj']   = 'Actividad borrada correctamente';
			} else {
				$json['error'] = false;
			}
		}

		return $json;
	}


	public function getActividad($p)
	{
		extract($p);
		include_once('../../config/init_db.php');

		$query_valida = "SELECT adjunto FROM ludus_modules_activities where  activity_id = $activity_id;";
		$res = DB::queryFirstRow($query_valida);
		if (isset($res) && $res != '') {
			$adjunto = $res['adjunto'];
		} else {
			$adjunto = '';
		}
		return $adjunto;
	}
}
