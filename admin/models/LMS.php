<?php
Class LMSs {
	function consultaDatosLMS($scorm_sco_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * 
						FROM ludus_scorm_track st 
						WHERE st.user_id = '$idQuien' AND st.scorm_sco_id = '$scorm_sco_id' AND 
						st.attempt IN (SELECT MAX(attempt) 
										FROM ludus_scorm_track 
										WHERE user_id = '$idQuien' AND scorm_sco_id = '$scorm_sco_id' AND 
										element = 'cmicorelessonstatus' AND value = 'incomplete' )";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaMaxIntLMS($scorm_sco_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT max(st.attempt) as attempt FROM ludus_scorm_track st WHERE st.user_id = '$idQuien' AND st.scorm_sco_id = '$scorm_sco_id' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		$var_Return = 1;
		if(isset($Rows_config['attempt'])){
			if($Rows_config['attempt']>0){
				$var_Return = $Rows_config['attempt']+1;
			}
		}
		return $var_Return;
	}
	function InsertaValue($scorm_sco_id,$element,$value,$intento){
		echo('::'.$scorm_sco_id.'::'.$element.'::'.$value);
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$user_id = $_SESSION['idUsuario'];
			$idQuien = $user_id;
			$scormTrack = 0;
			$timemodified = date("Ymd")."".date("H");
			/*Verifica si hay un valor para este elemento en un intento que aún no se ha terminado del mismo campo para actualizarlo de lo contrario lo crea*/
			$query_sql = "SELECT scorm_track_id, attempt 
							FROM ludus_scorm_track st 
							WHERE st.user_id = '$idQuien' AND 
							st.scorm_sco_id = '$scorm_sco_id' AND 
							st.element = '$element' AND 
							st.attempt = '$intento' ";
			$DataBase_Acciones = new Database();
			$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
			if(isset($Rows_config['scorm_track_id'])){
				$scormTrack = $Rows_config['scorm_track_id'];
				$updateSQL_ER = "UPDATE `ludus_scorm_track` 
							SET value = '$value', 
								timemodified = '$timemodified'
							WHERE scorm_track_id = '$scormTrack' AND element = '$element' ";
				$resultado = $DataBase_Acciones->SQL_Update($updateSQL_ER);
			}else{
				$insertSQL_EE = "INSERT INTO `ludus_scorm_track` 
								(scorm_sco_id,attempt,user_id,element,value,timemodified) 
								VALUES ('$scorm_sco_id','$intento','$user_id','$element','$value','$timemodified')";
				$resultado = $DataBase_Acciones->SQL_Insert($insertSQL_EE);
			}
			/*Verifica si hay un valor para este elemento en un intento que aún no se ha terminado del mismo campo para actualizarlo de lo contrario lo crea*/
			unset($DataBase_Acciones);
		}
	}
	function CreaNotaLudus(){
	}
}
