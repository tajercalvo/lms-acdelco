<?php

class RepIntensidadH{
    //===================================================================
    // Retorna el listado de los concecionarios activos
    //===================================================================
    public static function getConcesionarios( $zonas , $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $zona = $zonas == "" ? "" : "AND z.zone_id IN ( $zonas ) ";
        $query_sql = "SELECT DISTINCT d.dealer_id, d.dealer
            FROM ludus_dealers d, ludus_headquarters h, ludus_areas a, ludus_zone z
            WHERE d.dealer_id = h.dealer_id
            AND d.status_id = 1
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            AND d.category = 'CHEVROLET'
            $zona ";
            // echo($query_sql);
		$resultSet = DB::query($query_sql);
        return $resultSet;
    }//fin funcion getConcesionarios
    //===================================================================
    // Retorna el listado de los cursos discriminados por dealers
    //===================================================================
    public static function getCursos( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');

        $filtro = "";
        if( isset( $p['dealer_id'] ) && $p['dealer_id'] != "" && $p['dealer_id'] != "0" ){
            $filtro = " AND d.dealer_id IN ( {$p['dealer_id']} )";
        }

        $query_sql = "SELECT s.start_date, s.schedule_id, s.teacher_id, u.first_name, u.last_name, d.dealer_id, d.dealer, ds.dealer_schedule_id, ds.min_size,
            ds.max_size, ds.total_size, c.course_id, c.course, c.type AS course_type, c.specialty_id, sp.specialty, c.newcode, m.type AS module_type, m.duration_time
            FROM ludus_schedule s, ludus_dealer_schedule ds, ludus_courses c, ludus_dealers d, ludus_modules m, ludus_users u, ludus_specialties sp
            WHERE s.schedule_id = ds.schedule_id
            AND ds.dealer_id = d.dealer_id
            AND s.course_id = c.course_id
            AND c.course_id = m.course_id
            AND s.teacher_id = u.user_id
            AND c.specialty_id = sp.specialty_id
            AND ds.max_size > 0
            AND s.status_id = 1
            $filtro
            AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'";
            // echo($query_sql);
		$resultSet = DB::query($query_sql);
        return $resultSet;
    }//fin funcion getCursos
    //===================================================================
    // Retorna el listado de los inscritos en cada curso por dealers
    //===================================================================
    public static function getInscritos( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');

        $filtro = "";
        if( isset( $p['dealer_id'] ) && $p['dealer_id'] != "" && $p['dealer_id'] != "0" ){
            $filtro = " AND d.dealer_id IN ( {$p['dealer_id']} )";
        }

        $query_sql = "SELECT i.schedule_id, h.dealer_id, COUNT( i.invitation_id ) AS inscritos
            FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s, ludus_dealers d
            WHERE i.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND i.schedule_id = s.schedule_id
            AND s.status_id = 1
            AND i.status_id = 2
            $filtro
            AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
            GROUP BY i.schedule_id, h.dealer_id";
            // echo($query_sql);
		$resultSet = DB::query($query_sql);
        return $resultSet;
    }//fin funcion getCursos
    //===================================================================
    // Retorna el listado de los inscritos en cada curso por dealers
    //===================================================================
    public static function sabanaDatos( $cursos, $inscritos, $wbt ){
        $num = count( $cursos );
        for ($i=0; $i < $num ; $i++) {
            $cursos[$i]['inscritos'] = 0;
            $cursos[$i]['horas_cap'] = 0;
            foreach ( $inscritos as $key => $ins ) {
                if( $cursos[$i]['schedule_id'] == $ins['schedule_id'] && $cursos[$i]['dealer_id'] == $ins['dealer_id'] ){
                    $cursos[$i]['inscritos'] += intval( $ins['inscritos'] );
                    $cursos[$i]['horas_cap'] += ( intval( $ins['inscritos'] ) * intval( $cursos[$i]['duration_time'] ) );
                }
            }
        }

        foreach ($wbt as $key => $w) {
            $aux = [];
            $aux['start_date'] = $w['start_date'];
            $aux['schedule_id'] = $w['schedule_id'];
            $aux['teacher_id'] = 0;
            $aux['first_name'] = $w['first_name'];
            $aux['last_name'] = $w['last_name'];
            $aux['dealer_id'] = $w['dealer_id'];
            $aux['dealer'] = $w['dealer'];
            $aux['dealer_schedule_id'] = 0;
            $aux['min_size'] = 0;
            $aux['max_size'] = 0;
            $aux['total_size'] = 0;
            $aux['course_id'] = $w['course_id'];
            $aux['course'] = $w['course'];
            $aux['course_type'] = $w['course_type'];
            $aux['specialty_id'] = $w['specialty_id'];
            $aux['specialty'] = $w['specialty'];
            $aux['newcode'] = $w['newcode'];
            $aux['module_type'] = $w['module_type'];
            $aux['duration_time'] = $w['duration_time'];
            $aux['inscritos'] = 1;
            $aux['horas_cap'] = 2;
            $cursos[] = $aux;
        }
        return $cursos;
    }//fin funcion getCursos
    //===================================================================
    // Obtengo la base de datos de cursos WBT
    //===================================================================
    public static function consultaWBT( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();

		$filtro = "";
		$filtro_2 = "";

        if( isset( $p['dealer_id'] ) && $p['dealer_id'] != "" && $p['dealer_id'] != "0" ){
            $filtro = " AND d.dealer_id IN ( {$p['dealer_id']} )";
            $filtro2 = " AND d.dealer_id IN ( {$p['dealer_id']} )";
        }

		$query_sql = "SELECT d.dealer_id, 0 AS schedule_id, co.type AS course_type, d.dealer, h.headquarter, a.area, z.zone, u.first_name, u.last_name, u.identification, m.module, co.newcode, co.course,
                t.datetime AS start_date, m.duration_time, m.type AS module_type, co.course_id, sp.specialty_id, sp.specialty, t.user_id, s.module_id, MAX(t.attempt) as attempt,
                COUNT(distinct t.attempt) as cantAttempt
				FROM ludus_scorm_track t, ludus_scorm_sco c, ludus_scorm s, ludus_modules m, ludus_courses co, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, ludus_specialties sp
				WHERE t.scorm_sco_id = c.scorm_sco_id AND c.scorm_id = s.scorm_id
				AND t.datetime BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
				AND s.module_id = m.module_id
				AND m.course_id = co.course_id
				AND t.user_id = u.user_id
				AND u.headquarter_id = h.headquarter_id
				AND h.dealer_id = d.dealer_id
				AND h.area_id = a.area_id
				AND a.zone_id = z.zone_id
				AND co.specialty_id = sp.specialty_id
				$filtro
				GROUP BY d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, m.module, co.course, t.user_id, s.module_id
				ORDER BY co.course, d.dealer, h.headquarter, u.first_name, u.last_name";

		$Rows_config = DB::query($query_sql);

		$query_res = "SELECT o.user_id, o.module_id, p.puntaje, o.date_creation
						FROM ludus_modules_results_usr o, (SELECT r.user_id, m.module_id, MAX(r.score) as puntaje
							FROM ludus_modules_results_usr r, ludus_modules m, ludus_users u, ludus_headquarters h, ludus_dealers d
							WHERE r.module_id = m.module_id
							AND m.type = 'WBT'
							AND r.date_creation BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
							AND r.user_id = u.user_id
							AND u.headquarter_id = h.headquarter_id
							AND h.dealer_id = d.dealer_id
							$filtro_2
							GROUP BY r.user_id, r.module_id ) p
							WHERE o.user_id = p.user_id
							AND o.date_creation BETWEEN '{$p['start_date']}' AND '{$p['end_date']}'
							AND o.module_id = p.module_id
							AND o.score = p.puntaje
							GROUP BY o.user_id, o.module_id";

		$Rows_res = DB::query($query_res);

		$query_histo = "SELECT r.user_id, m.module_id, MAX(r.score) as score, r.date_creation as fecpaso
							FROM ludus_modules_results_usr r, ludus_modules m
							WHERE m.type = 'WBT' AND m.module_id = r.module_id AND r.date_creation < '{$p['start_date']} 00:00:00' AND r.approval = 'SI'
							GROUP BY r.user_id, m.module_id";
		$Rows_histo = DB::query($query_histo);

		$query_cuantos = "SELECT c.module_id, s.scorm_id, count(s.scorm_sco_id) as cuantos
						FROM ludus_scorm_sco s, ludus_scorm c
						WHERE s.scormtype = 'sco' AND s.scorm_id = c.scorm_id GROUP BY c.module_id, s.scorm_id ORDER BY `s`.`scorm_id` ASC";
		$Rows_cuantos = DB::query($query_cuantos);

		$query_pase = "SELECT t.user_id, c.module_id, count(distinct t.scorm_sco_id) as pase
			FROM ludus_scorm_track t, ludus_scorm_sco s, ludus_scorm c
			WHERE t.element = 'cmicorelessonstatus'
			AND t.value = 'completed'
			AND t.scorm_sco_id = s.scorm_sco_id
			AND s.scormtype = 'sco'
			AND s.scorm_id = c.scorm_id
			GROUP BY t.user_id, c.module_id";

		$Rows_pase = DB::query($query_pase);
		$Rows_res = DB::query($query_res);

		$cuenta = count($Rows_config);
		$respuestas = count($Rows_res);
		$cuantos = count($Rows_cuantos);
		$pase = count($Rows_pase);

		for($i=0;$i< $cuenta ;$i++) {
			$var_user_id = $Rows_config[$i]['user_id'];
			$var_module_id = $Rows_config[$i]['module_id'];

			for($y=0;$y< $respuestas ;$y++) {
				if($var_user_id == $Rows_res[$y]['user_id'] && $var_module_id == $Rows_res[$y]['module_id'] ){
					$Rows_config[$i]['respuestas'][] = $Rows_res[$y];
				}
			}//fin for respuestas

			for( $z=0; $z< $cuantos; $z++ ) {
				if($var_module_id == $Rows_cuantos[$z]['module_id'] ){
					$Rows_config[$i]['cuantos'] = $Rows_cuantos[$z]['cuantos'];
				}
			}//fin for cuentos

			for($x=0;$x<$pase;$x++) {
				if($var_user_id == $Rows_pase[$x]['user_id'] && $var_module_id == $Rows_pase[$x]['module_id'] ){
					$Rows_config[$i]['pase'] = $Rows_pase[$x]['pase'];
				}
			}//fin for pase

			for($w=0;$w<count($Rows_histo);$w++) {
				if($var_user_id == $Rows_histo[$w]['user_id'] && $var_module_id == $Rows_histo[$w]['module_id'] ){
					$Rows_config[$i]['histo'] = $Rows_histo[$w];
				}
			}//fin historico

		}//fin for registros

		return $Rows_config;
	}//end function consultaDatosAll

}//fin clase RepIntensidadH
