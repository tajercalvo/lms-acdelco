<?php
Class RepAcumulado {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	
	function consultaDatosAll($dealer_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		//@session_start();
		//$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT c.course_id, c.newcode, c.course,  AVG(m_r.score) as avg_score
					FROM ludus_courses c, ludus_charges_courses cc, ( SELECT u.user_id, m.course_id, avg(mr.score) as score
					FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
					AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND mr.module_id = m.module_id
					AND h.dealer_id = '$dealer_id'
					GROUP BY u.user_id, m.course_id ) AS m_r
					WHERE m_r.course_id = c.course_id AND cc.course_id = c.course_id
					GROUP BY c.course";
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT c.course_id, c.newcode, c.course,  AVG(m_r.score) as avg_score
					FROM ludus_courses c, ludus_charges_courses cc, ( SELECT u.user_id, m.course_id, avg(mr.score) as score
					FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
					AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND mr.module_id = m.module_id
					GROUP BY u.user_id, m.course_id ) AS m_r
					WHERE m_r.course_id = c.course_id AND cc.course_id = c.course_id
					GROUP BY c.course";//LIMIT 0,20
		}else{
			$query_sql = "SELECT c.course_id, c.newcode, c.course,  AVG(m_r.score) as avg_score
					FROM ludus_courses c, ludus_charges_courses cc, ( SELECT u.user_id, m.course_id, avg(mr.score) as score
					FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
					AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND mr.module_id = m.module_id
					AND h.dealer_id = '$dealer_id'
					GROUP BY u.user_id, m.course_id ) AS m_r
					WHERE m_r.course_id = c.course_id AND cc.course_id = c.course_id
					GROUP BY c.course";//LIMIT 0,20
		}

		//echo $query_sql;
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		//verifica los cargos 
		$query_Resultado = "SELECT cc.course_id, c.charge
			FROM ludus_charges_courses cc, ludus_charges c
			WHERE c.status_id = 1 AND cc.charge_id = c.charge_id";
		$Rows_Charges = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
		//verifica los cargos

		//verifica cuantos usuarios lo vieron
		$query_Resultado = "SELECT c.course_id, count(u.user_id) as usr_pres
							FROM ludus_courses c, ludus_users u, ( SELECT u.user_id, m.course_id, avg(mr.score) as score
							FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
							WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
							AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND mr.module_id = m.module_id
							GROUP BY u.user_id, m.course_id ) AS m_r
							WHERE m_r.course_id = c.course_id AND m_r.user_id = u.user_id
							GROUP BY c.course_id";
		$Rows_usuarios = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
		//verifica cuantos usuarios lo vieron

		//verifica maxima nota
		$query_Resultado = "SELECT c.course_id, MAX(m_r.score) as avg_score
							FROM ludus_courses c, ludus_charges_courses cc, ( SELECT u.user_id, m.course_id, avg(mr.score) as score
							FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
							WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
							AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND mr.module_id = m.module_id
							GROUP BY u.user_id, m.course_id ) AS m_r
							WHERE m_r.course_id = c.course_id AND cc.course_id = c.course_id
							GROUP BY c.course_id";
		$Rows_maximos = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
		//verifica maxima nota

		//verifica minima nota
		$query_Resultado = "SELECT c.course_id, MIN(m_r.score) as avg_score
							FROM ludus_courses c, ludus_charges_courses cc, ( SELECT u.user_id, m.course_id, avg(mr.score) as score
							FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
							WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
							AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND mr.module_id = m.module_id
							GROUP BY u.user_id, m.course_id ) AS m_r
							WHERE m_r.course_id = c.course_id AND cc.course_id = c.course_id
							GROUP BY c.course_id";
		$Rows_minimos = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
		//verifica minima nota

		//verifica las notas de cada curso para el histórico
		$query_Resultado ="SELECT c.newcode, c.course_id, u.first_name, u.last_name, u.identification, m_r.score
							FROM ludus_courses c, ludus_users u, ( SELECT u.user_id, m.course_id, avg(mr.score) as score
							FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
							WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
							AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND mr.module_id = m.module_id
							GROUP BY u.user_id, m.course_id ) AS m_r
							WHERE m_r.course_id = c.course_id AND m_r.user_id = u.user_id";
		$Rows_notas = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
		//verifica las notas de cada curso para el histórico

		//verifica la programación
		$query_Resultado = "SELECT d.dealer_id, s.course_id, SUM(d.min_size) as min_size, SUM(d.max_size) as max_size, SUM(d.inscriptions) as inscriptions
							FROM ludus_schedule s, ludus_dealer_schedule d
							WHERE s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							AND s.schedule_id = d.schedule_id
							AND d.dealer_id = '$dealer_id'
							AND d.min_size > 0
							GROUP BY d.dealer_id, s.course_id";
		$Rows_programacion = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
		//verifica la programación

		//verifica asistencia
		$query_Resultado = "SELECT s.course_id, h.dealer_id, count(d.user_id) as cantidad
							FROM ludus_schedule s, ludus_invitation d, ludus_users u, ludus_headquarters h
							WHERE s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							AND s.schedule_id = d.schedule_id
							AND d.status_id = 2
							AND d.user_id = u.user_id
							AND u.headquarter_id = h.headquarter_id
							AND h.dealer_id = '$dealer_id'
							GROUP BY s.course_id, h.dealer_id";
		$Rows_asistencia = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
		//verifica asistencia

		for($i=0;$i<count($Rows_config);$i++) {
			$var_course_id = $Rows_config[$i]['course_id'];
			//Cargos del usuario
				for($y=0;$y<count($Rows_Charges);$y++) {
					if($var_course_id == $Rows_Charges[$y]['course_id']){
						$Rows_config[$i]['cargos'][] = $Rows_Charges[$y];
					}
				}
			//Cargos del usuario

			//Cargos del usuario
				for($n=0;$n<count($Rows_notas);$n++) {
					if($var_course_id == $Rows_notas[$n]['course_id']){
						$Rows_config[$i]['notas'][] = $Rows_notas[$n];
					}
				}
			//Cargos del usuario

			//Verifica cuantos usuarios lo vieron
				for($x=0;$x<count($Rows_usuarios);$x++) {
					if($var_course_id == $Rows_usuarios[$x]['course_id']){
						$Rows_config[$i]['usr_pres'] = number_format($Rows_usuarios[$x]['usr_pres'],0);
					}
				}
			//Verifica cuantos usuarios lo vieron

			//Verifica maxima nota
				for($z=0;$z<count($Rows_maximos);$z++) {
					if($var_course_id == $Rows_maximos[$z]['course_id']){
						$Rows_config[$i]['max_score'] = number_format($Rows_maximos[$z]['avg_score'],0);
					}
				}
			//Verifica maxima nota

			//Verifica minima nota
				for($w=0;$w<count($Rows_minimos);$w++) {
					if($var_course_id == $Rows_minimos[$w]['course_id']){
						$Rows_config[$i]['min_score'] = number_format($Rows_minimos[$w]['avg_score'],0);
					}
				}
			//Verifica minima nota

			//verifica la programación
				for($w=0;$w<count($Rows_programacion);$w++) {
					if($var_course_id == $Rows_programacion[$w]['course_id']){
						$Rows_config[$i]['min_size'] = $Rows_programacion[$w]['min_size'];
						$Rows_config[$i]['max_size'] = $Rows_programacion[$w]['max_size'];
						$Rows_config[$i]['inscriptions'] = $Rows_programacion[$w]['inscriptions'];
					}
				}
			//verifica la programación

			//verifica asistencia
				for($w=0;$w<count($Rows_asistencia);$w++) {
					if($var_course_id == $Rows_asistencia[$w]['course_id']){
						$Rows_config[$i]['cantidad'] = $Rows_asistencia[$w]['cantidad'];
					}
				}
			//verifica asistencia
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
