<?php
Class Cargos {

	function consultaDatossedes($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		session_start();
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, p.specialty
			FROM ludus_courses c, ludus_users s, ludus_status e, ludus_specialties p
			WHERE c.editor = s.user_id
			AND c.status_id = e.status_id
			AND c.specialty_id = p.specialty_id ";

			if($_SESSION['charge_id'] == 128){
				$query_sql .= " AND c.status_id = 1";
			}

			if($sWhere!=''){
				$query_sql .= " AND (c.course LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.newcode like '%$sWhere%' OR c.description LIKE '%$sWhere%' OR c.prerequisites LIKE '%$sWhere%' OR c.type LIKE '%$sWhere%' OR p.specialty LIKE '%$sWhere%') ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
 
	function consultaRegistroDetailModulos($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		    $query_sql = "SELECT m.*
			FROM ludus_modules m
			WHERE m.course_id = '$idRegistro' AND m.status_id = 1 ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaRegistroDetail($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*,c.* FROM ludus_modules m 
					  INNER JOIN ludus_courses c on c.course_id = m.course_id 
					  		WHERE m.course_id= $idRegistro and m.status_id = 1";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	/*function progress_bar($course_id){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		$sql_progress_bar = "SELECT module, 'Información de Modulo' as tiene FROM ludus_modules where course_id = $course_id
								union
								SELECT review, 'Preguntas de profundizacion' as tiene FROM ludus_reviews_r where course_id = $course_id
								union
								SELECT course_id, 'Material de apoyo' as tiene FROM ludus_courses_files where course_id = $course_id
								union
                                SELECT distinct(course_id), 'Evaluación' as tiene FROM ludus_reviews_courses where course_id = $course_id
                                union
                                SELECT max(course_id), 'Está atado a trayectoria' as tiene FROM ludus_charges_courses where course_id = $course_id;";
		$Rows_progress_bar = $DataBase_Acciones->SQL_SelectMultipleRows($sql_progress_bar);
		unset($DataBase_Acciones);
		return $Rows_progress_bar;
	}*/

	// ========================================================
	// retorna el estado de la validación de los componentes de cada curso
	// ========================================================
	public static function progress_bar($course_id, $prof = "../"){
		include_once($prof.'../config/init_db.php');
		$sql_progress_bar = "SELECT max(course_id) AS aux, 'Atado a trayectoria' as caso, CASE
									WHEN course_id IS NULL THEN 'NO'
									ELSE 'SI'
									END AS tiene FROM ludus_charges_courses where course_id = $course_id
									union
								SELECT  max(course_id) AS aux, 'Información de modulo' as caso, CASE
									WHEN course_id IS NULL THEN 'NO'
									ELSE 'SI'
									END AS tiene  FROM ludus_modules where course_id = $course_id
									union
								SELECT max(course_id) AS aux, 'Preguntas de profundización' as caso, CASE
									WHEN course_id IS NULL THEN 'NO'
									ELSE 'SI'
									END AS tiene FROM ludus_reviews_r where course_id = $course_id
									union
								SELECT max(course_id) AS aux, 'Evaluación' as caso, CASE
									WHEN course_id IS NULL THEN 'NO'
									ELSE 'SI'
									END AS tiene  FROM ludus_reviews_courses where course_id = $course_id
									union
								SELECT max(course_id) AS aux, 'Material de apoyo' as caso, CASE
									WHEN course_id IS NULL THEN 'NO'
									ELSE 'SI'
									END AS tiene FROM ludus_course_files where course_id = $course_id
									union
									SELECT COUNT( rq.reviews_questions_id ) AS aux, 'Preguntas de profundizacion' AS caso,
									CASE
										WHEN r.course_id IS NULL THEN 'NO'
									    WHEN COUNT( rq.reviews_questions_id ) < 15 THEN 'NO'
									    ELSE 'SI'
									END AS tiene
									FROM ludus_reviews_questions_r rq, ludus_reviews_r r
									where rq.review_id = r.review_id AND r.course_id = $course_id;";
		// echo $sql_progress_bar."<br>"
		$Rows_progress_bar = DB::query($sql_progress_bar);
		return $Rows_progress_bar;
	}// fin progress_bar

	// ========================================================
	// consulta los cursos que se dictaran a futuro
	// ========================================================
	public static function getCursosIncompletos( $fecha, $prof = "../" ) {
		include_once($prof.'../config/init_db.php');

		$query_sql = "SELECT DISTINCT s.course_id, c.course, c.newcode
			FROM ludus_schedule s, ludus_courses c
			WHERE s.course_id = c.course_id
			AND s.start_date BETWEEN '$fecha 00:00:00' AND '$fecha 23.59:59'";

		$cursos = DB::query( $query_sql );

		return $cursos;
	}// fin getCursosIncompletos

	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, p.specialty
			FROM ludus_courses c, ludus_users s, ludus_status e, ludus_specialties p
			WHERE c.editor = s.user_id AND c.status_id = e.status_id";
			if($sWhere!=''){
				$query_sql .= " AND (c.course LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.newcode like '%$sWhere%' OR c.description LIKE '%$sWhere%' OR c.prerequisites LIKE '%$sWhere%' OR c.type LIKE '%$sWhere%' OR p.specialty LIKE '%$sWhere%') ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, p.specialty
			FROM ludus_courses c, ludus_users s, ludus_status e, ludus_specialties p
			WHERE c.editor = s.user_id AND c.status_id = e.status_id
			AND c.specialty_id = p.specialty_id
			ORDER BY course ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function RegistrarNota($Nota,$Modulo_id){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$approval = 'NO';
		if($Nota>=80){
			$approval = 'SI';
		}else{
			$approval = 'NO';
		}
		$insertSQL_EE = "INSERT INTO `ludus_modules_results_usr` (score,user_id,module_id,approval,file,date_creation,status_id,creator,invitation_id)
		VALUES ('$Nota','$idQuien','$Modulo_id','$approval','','$NOW_data','1','$idQuien',0)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Crearcargos($course,$code, $description,$prerequisites,$type,$specialty_id,$target){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración cargos: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_courses` (course,status_id,creator,editor,date_creation,date_edition,code,description,prerequisites,type,image,specialty_id,target) VALUES ('$course','2','$idQuien','$idQuien','$NOW_data','$NOW_data','$code','$description','$prerequisites','$type','default.png','$specialty_id','$target')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);

		if($resultado>9999){
			$newcode = $type.$resultado;
		}elseif($resultado>999){
			$newcode = $type.'0'.$resultado;
		}else{
			$newcode = $type.'00'.$resultado;
		}
		$updateSQL_ER = "UPDATE ludus_courses SET code='$newcode', newcode='$newcode' WHERE course_id='$resultado' ";
		$resultado_update = $DataBase_Log->SQL_Update($updateSQL_ER);

		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarcargos($id,$course,$status_id,$description,$prerequisites,$type,$specialty_id,$target){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_cursos.php?opcn=ver&id=$id&back=inicio>$id</a>] a cargos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_courses` SET course = '$course', status_id = '$status_id', editor = '$idQuien', date_edition = '$NOW_data', description = '$description', prerequisites = '$prerequisites', type = '$type', specialty_id = '$specialty_id', target = '$target' WHERE course_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_courses c
			WHERE c.course_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	
	function actualizaImagen($course_id,$imagen){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_courses` SET image = '$imagen', editor = '$idQuien', date_edition = '$NOW_data' WHERE course_id = '$course_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function consultaEspecialidades($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.*
						FROM ludus_specialties c
						WHERE c.status_id = 1
						ORDER BY c.specialty";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
		
	function consultaPasoCurso($idCourse,$idUsuario){
		@session_start();
		if($idUsuario!=0){
			$idQuien = $idUsuario;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT r.*
					FROM ludus_modules_results_usr r, ludus_modules m
					WHERE m.course_id = '$idCourse' AND m.module_id = r.module_id AND
					r.approval = 'SI' AND r.user_id = $idQuien";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//===================================================================
	//Consulta el listado de valores por cada curso
	//===================================================================
	public static function listaParametros( $prof = "" ){
		include_once($prof.'../config/init_db.php');
		$query = "SELECT * FROM ludus_parameters where parameter_id > 2";
		$resultSet = DB::query( $query );
		return $resultSet;
	}
	//===================================================================
	//Actualiza los datos de los cursos/sedes con los parametros de cobros
	//===================================================================
	public static function guardarCambiosParametros( $p, $prof = "" ){
		include_once($prof.'../config/init_db.php');
		$parameter_id = intval($p['parameter_id']);
		$rowsAffected = 0;
		$query = "SELECT * FROM ludus_parameters where parameter_id = $parameter_id";
		$resultSet = DB::queryFirstRow( $query );
		$query_IBT = "SELECT m.course_id FROM ludus_modules m WHERE m.type = %s AND m.duration_time = %i";
		switch ($parameter_id) {
			//IBT 16 horas
			case 3:
				$res_IBT = DB::query( $query_IBT, 'IBT', 16 );
				foreach ($res_IBT as $key => $v ) {
					DB::update( 'ludus_courses', [ "rate_course" => $resultSet['value'] ], 'course_id = %i', $v['course_id'] );
					$rowsAffected += DB::affectedRows() > 0 ? 1 : 0;
				}
			break;
			//IBT 8 horas
			case 4:
				$res_IBT = DB::query( $query_IBT, 'IBT', 8 );
				foreach ($res_IBT as $key => $v ) {
					DB::update( 'ludus_courses', [ "rate_course" => $resultSet['value'] ], 'course_id = %i', $v['course_id'] );
					$rowsAffected += DB::affectedRows() > 0 ? 1 : 0;
				}
			break;
			//IBT 4 horas
			case 5:
				$res_IBT = DB::query( $query_IBT, 'IBT', 4 );
				foreach ($res_IBT as $key => $v ) {
					DB::update( 'ludus_courses', [ "rate_course" => $resultSet['value'] ], 'course_id = %i', $v['course_id'] );
					$rowsAffected += DB::affectedRows() > 0 ? 1 : 0;
				}
			break;
			//IBT 2 horas
			case 6:
				$res_IBT = DB::query( $query_IBT, 'IBT', 2 );
				foreach ($res_IBT as $key => $v ) {
					DB::update( 'ludus_courses', [ "rate_course" => $resultSet['value'] ], 'course_id = %i', $v['course_id'] );
					$rowsAffected += DB::affectedRows() > 0 ? 1 : 0;
				}
			break;
			//OJT 2 horas
			case 7:
				$res_IBT = DB::query( $query_IBT, 'OJT', 2 );
				foreach ($res_IBT as $key => $v ) {
					DB::update( 'ludus_courses', [ "rate_course" => $resultSet['value'] ], 'course_id = %i', $v['course_id'] );
					$rowsAffected += DB::affectedRows() > 0 ? 1 : 0;
				}
			break;
			//VCT 2 horas
			case 8:
				$res_IBT = DB::query( $query_IBT, 'VCT', 2 );
				foreach ($res_IBT as $key => $v ) {
					DB::update( 'ludus_courses', [ "rate_course" => $resultSet['value'] ], 'course_id = %i', $v['course_id'] );
					$rowsAffected += DB::affectedRows() > 0 ? 1 : 0;
				}
			break;
			//VCT 2 horas
			case 9:
				DB::update( 'ludus_headquarters', [ "rate_course" => $resultSet['value'] ], 'rate_course > 0' );
				$rowsAffected += DB::affectedRows() > 0 ? 1 : 0;
			break;
		}//fin switch
		return $rowsAffected;
	}


	//QUien descargo el material de apoyo
	public static function descargo_material( $p ){
			include_once('../../config/init_db.php');
			session_start();

			$res = array();
			if( !isset( $_SESSION['idUsuario'] ) ){
				$res['error'] = true;
				$res['msj'] = 'Por favor inicie sesión para poder descargar este archivo';
				return $res;
			}
			$quien = $_SESSION['idUsuario'];
			$query = "INSERT INTO ludus_download_element
							(
							id_element,
							type,
							name_file,
							creator,
							date_creation)
							VALUES
							(
							'{$p['course_file_id']}',
							'Material de apoyo curso',
							'{$p['name_file']}',
							 $quien,
							 now()
							);";
			$resultado = DB::query($query);


			if( $resultado ){
				$res['error'] = false;
				$res['msj'] = 'descargado';
			}else{
				$res['error'] = true;
				$res['msj'] = 'No descargado';
			}

			return $res;
		}

}
