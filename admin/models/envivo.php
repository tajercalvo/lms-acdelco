<?php
Class Multimedias_Detail {

		function consultaTransmision(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if($_SESSION['max_rol']>=6){
			$query_sql = "SELECT * FROM ludus_streaming
		order by date_creation desc, date_edition desc";
		}else{
			$query_sql = "SELECT * FROM ludus_streaming
			where status_id = 1
		order by date_creation desc, date_edition desc";
		}
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		// $query_Likes = "SELECT cu.*, u.first_name, u.last_name
		// 				FROM ludus_media_likes cu, ludus_users u
		// 				WHERE cu.user_id = u.user_id
		// 				ORDER BY cu.date_edition ";
		// $Rows_Likes = $DataBase_Acciones->SQL_SelectMultipleRows($query_Likes);

		// $query_Views = "SELECT cu.*, u.first_name, u.last_name
		// 				FROM ludus_media_views cu, ludus_users u
		// 				WHERE cu.user_id = u.user_id
		// 				ORDER BY cu.date_edition ";
		// $Rows_Views = $DataBase_Acciones->SQL_SelectMultipleRows($query_Views);

		// for($i=0;$i<count($Rows_config);$i++) {
		// 	$media_detail_id = $Rows_config[$i]['media_detail_id'];
		// 	//Cargos
		// 		for($y=0;$y<count($Rows_Likes);$y++) {
		// 			if( $media_detail_id == $Rows_Likes[$y]['media_detail_id'] ){
		// 				$Rows_config[$i]['likes_data'][] = $Rows_Likes[$y];
		// 			}
		// 		}
		// 		for($y=0;$y<count($Rows_Views);$y++) {
		// 			if( $media_detail_id == $Rows_Views[$y]['media_detail_id'] ){
		// 				$Rows_config[$i]['views_data'][] = $Rows_Views[$y];
		// 			}
		// 		}
			//Cargos
		 // }

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearTransmision($tittle, $description, $url, $image, $estado){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$insertSQL_EE = "INSERT INTO ludus_streaming (tittle, description, views, likes, creator, editor, date_creation, date_edition, url_streaming, image, status_id)
		VALUES ('$tittle','$description',0,0,$idQuien,'','$NOW_data','','$url','$image', $estado);";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarMultimedias($streaming_id, $media, $description, $url_streaming, $image, $status){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_streaming
		SET tittle = '$media',
		description = '$description',
		`creator` = $idQuien,
		editor = '163840',
		date_edition ='$NOW_data ',
		url_streaming = '$url_streaming',
		`status_id`='$status'
		WHERE `streaming_id` = $streaming_id;";
		//echo "$updateSQL_ER";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}


	function CrearLike($media_detail_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_streaming_likes (user_id, date_edition, streaming_id) VALUES ('$idQuien', '$NOW_data', '$media_detail_id')";
		// echo $insertSQL_ER;
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE ludus_streaming
						SET likes = likes+1
						WHERE streaming_id = '$media_detail_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}


	//Cantidad de veces que se vio un video
	function CrearMediaView($media_detail_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_streaming_views
			(
			streaming_id,
			user_id,
			date_edition)
			VALUES
			($media_detail_id,
			'$idQuien',
			'$NOW_data');";
		//echo($insertSQL_ER);
		$resultado_IN = $DataBase_Log->SQL_Insert($insertSQL_ER);

		// Actualizar las veces que se vio un video en la tabla ludus_media_detail
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE ludus_streaming
						SET views  = views +1
						WHERE streaming_id = '$media_detail_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}

}
