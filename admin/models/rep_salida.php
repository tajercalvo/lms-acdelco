<?php
Class RepSalida {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		}else{
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 AND dealer_id = $dealer_id ORDER BY dealer";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	
	function consultaDatosAll($dealer_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, sum(m.duration_time) as tiempo
				FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_modules m, ludus_users u, ludus_headquarters h, 
				ludus_dealers d
				WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND i.status_id = 2 AND i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND c.course_id = m.course_id
				AND d.dealer_id = $dealer_id
				AND s.start_date BETWEEN '$start_date' AND '$end_date'
				GROUP BY d.dealer, h.headquarter, u.first_name, u.last_name,u.identification ";
		
		$query_charges = "SELECT u.identification, c.charge 
		FROM ludus_users u, ludus_charges_users cu, ludus_charges c, ludus_headquarters h, ludus_dealers d
		WHERE u.status_id = 1 AND u.user_id = cu.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND
		d.dealer_id = $dealer_id AND cu.charge_id = c.charge_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$Rows_Charges = $DataBase_Acciones->SQL_SelectMultipleRows($query_charges);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_identification = $Rows_config[$i]['identification'];
			for($l=0;$l<count($Rows_Charges);$l++){
				if($var_identification==$Rows_Charges[$l]['identification']){
					$Rows_config[$i]['charges'][] = $Rows_Charges[$l];
				}
			}
		}


		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
