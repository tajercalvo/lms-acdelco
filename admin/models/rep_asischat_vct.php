<?php
class chatvct{
	

		public static function consultar_schedule(){
	        
			include_once('../../config/init_db.php');
			@session_start();
			// $rol_id    = $_SESSION['id_rol'][0]['rol_id'];
			// $dealer_id = $_SESSION['dealer_id'];
			 //$id_usuario = $_SESSION['charge_id'];
			// print_r($id_usuario);
			// return;
			$query     = "SELECT schedule_id, c.course FROM ludus_schedule s
							INNER JOIN ludus_courses c on c.course_id = s.course_id 
							ORDER BY `s`.`schedule_id`  DESC";
			$resultado = DB::query($query);
			     
			   return $resultado;
		}

		public static function consultar_asistentesvct($schedule_id){
			include_once('../../config/init_db.php');
		//	DB::$encoding = 'utf8';
			@session_start();
			//print_r($_SESSION);
			//return;
			$rol_id    = $_SESSION['id_rol'][0]['rol_id']; //rol super usuario
			$charge_id = $_SESSION['charge_id'];  //cargo
			$dealer_id = $_SESSION['dealer_id']; //empresa o concesionario
			//si es super usuario, reporta todas las empresas
		
			if ($rol_id == 1) {
				
				$query = "SELECT u.identification identification, u.last_name apellidos, u.first_name nombres, u.image, i.course_id,i.status_id, h.headquarter sede, d.dealer concesionario, GROUP_CONCAT( distinct ch.charge separator ', ') trayectorias, r.rol perfil
							FROM ludus_courses c, ludus_headquarters h, ludus_users u, ludus_inscriptions i, ludus_dealers d, ludus_charges_users cu, ludus_charges ch, ludus_roles_users ru, ludus_roles r
							WHERE c.course_id = i.course_id
							AND u.headquarter_id = h.headquarter_id
                            AND ch.charge_id = cu.charge_id
                            AND u.user_id = cu.user_id
                            AND ru.user_id = u.user_id
							AND r.rol_id = ru.rol_id
							AND i.user_id = u.user_id AND i.schedule_id = '$schedule_id'
							AND d.dealer_id = h.dealer_id
                            GROUP by u.user_id";

			    $resultado = DB::query($query);

			    $numero_part = count($resultado);

			    foreach ($resultado as $key => $value) {
			    	
			    	$resultado[$key]['xnum_part']=$numero_part;
			    }


			    //si es lider gm, crea un reporte de acuerdo a su empresa perteneciente
			}else{
				
				$resultado['mensaje']= 'Usted no tiene el perfil para hacer esta consulta';
			}
			
			return $resultado;
	     }

	     public static function mensaje_chat($schedule_id){
	     	include_once('../../config/init_db.php');
	     	$query="SELECT cm.comment_text Comentario, CONCAT(u.first_name, ' ', u.last_name) Usuario,h.headquarter Empresa ,cm.date_comment date_creation, c.course Conferencia 
			     	FROM ludus_comments cm
			     	INNER JOIN ludus_users u on u.user_id = cm.user_id 
                    INNER JOIN ludus_schedule s on s.schedule_id = cm.schedule_id
                    INNER JOIN ludus_courses c on c.course_id = s.course_id
			     	INNER JOIN ludus_headquarters h on h.headquarter_id=u.headquarter_id
			     	WHERE cm.schedule_id = $schedule_id";
			$result = DB::query($query);

			$numero_chat = count($result);
			foreach ($result as $key => $value) {
				$result[$key]['cantidad'] = $numero_chat;
			}
			

			return $result;
	     }

}

// $consultar = new Asistentesbroad();
// $consultar -> consultabrod();
?>