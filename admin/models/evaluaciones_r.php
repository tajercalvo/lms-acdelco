<?php
Class Evaluaciones {
	function consultaDatosEvaluaciones($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews_r c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (1,3) ";
			if($sWhere!=''){
				$query_sql .= " AND (c.review LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' ) ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews_r c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (1,3) ";
			if($sWhere!=''){
				$query_sql .= " AND (c.review LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' ) ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews_r c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (1,3)
			ORDER BY c.review ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearEvaluaciones($course_id,$type,$time,$cant_question){
		@session_start();
		include_once('../../config/init_db.php');
		$idQuien = $_SESSION['idUsuario'];
		$curso = DB::queryFirstRow( "SELECT * FROM ludus_courses c WHERE c.course_id = $course_id" );
		switch ( $type ) {
			case '1': $ttyp = "VCT";  break;
			case '2': $ttyp = "OJT"; break;
			case '3': $ttyp = "IBT"; break;
		}

		DB::insert( 'ludus_reviews_r', [
				"review"=> $curso['course'],
				"status_id"=> 1,
				"editor"=> $idQuien,
				"date_edition"=> DB::sqleval('NOW()'),
				"code"=> "EV_C_".$ttyp.$course_id,
				"course_id"=> $course_id,
				"type"=> $type,
				"description"=> '',
				"time"=> $time,
				"cant_question"=> $cant_question
			] );

		return DB::insertId();
	}
	function ActualizarEvaluaciones($id,$status,$course_id,$type,$time,$cant_question){
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];

		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$curso = DB::queryFirstRow( "SELECT * FROM ludus_courses c WHERE c.course_id = $course_id" );
		switch ( $type ) {
			case '1': $ttyp = "VCT";  break;
			case '2': $ttyp = "OJT"; break;
			case '3': $ttyp = "IBT"; break;
		}

		DB::update( 'ludus_reviews_r', [
			"review" => $curso['course'],
			"status_id" => $status,
			"editor" => $idQuien,
			"date_edition" => DB::sqleval('NOW()'),
			"code" => "EV_C_".$ttyp.$course_id,
			"course_id" => $course_id,
			"type" => $type,
			"description" => '',
			"time" => $time,
			"cant_question" => $cant_question ], "review_id = $id");
		// echo( $updateSQL_ER );
		$resultado = DB::affectedRows();
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_reviews_r c
			WHERE c.review_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		$query_sql = "SELECT c.course_id
					FROM ludus_reviews_courses c
					WHERE c.review_id = $idRegistro
					ORDER BY c.course_id";
		$DataBase_Class = new Database();
		$Rows_config_Extr = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$Rows_config['cursos'] = $Rows_config_Extr;
		// print_r( $Rows_config );
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.course_id, c.course, c.code, c.newcode
						FROM ludus_courses c
						ORDER BY c.course";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	// function consultaCargos($prof){
	// 	if($prof=="js"){
	// 		include_once('../../config/database.php');
	// 		include_once('../../config/config.php');
	// 	}else{
	// 		include_once('../config/database.php');
	// 		include_once('../config/config.php');
	// 	}
	// 	$query_sql = "SELECT c.charge_id, c.charge
	// 					FROM ludus_charges c WHERE c.status_id = 1
	// 					ORDER BY c.charge";
	// 	$DataBase_Class = new Database();
	// 	$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
	// 	unset($DataBase_Class);
	// 	return $Rows_config;
	// }
	function consultaPreguntasSel($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT rq.*, q.question, q.type, u.first_name, u.last_name, q.code
						FROM ludus_reviews_questions_r rq, ludus_questions_r q, ludus_users u
						WHERE rq.review_id = '$idRegistro' AND rq.question_id = q.question_id AND rq.editor = u.user_id
						ORDER BY rq.date_edition ASC";
		// echo( $query_sql );
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		$query_CantRespuestas = "SELECT ra.review_id, ad.question_id, count(*)
						FROM ludus_reviews_answer_r ra, ludus_reviews_answer_detail_r ad
						WHERE ra.review_id = '$idRegistro' AND ad.reviews_answer_id = ra.reviews_answer_id
						GROUP BY ra.review_id, ad.question_id";
		$Rows_RespCant = $DataBase_Class->SQL_SelectMultipleRows($query_CantRespuestas);

		for($i=0;$i<count($Rows_config);$i++) {
			$review_id = $Rows_config[$i]['review_id'];
			$question_id = $Rows_config[$i]['question_id'];
			for($x=0;$x<count($Rows_RespCant);$x++) {
				if( $review_id == $Rows_RespCant[$x]['review_id'] && $question_id == $Rows_RespCant[$x]['question_id'] ){
					$Rows_config[$i]['YaRespondida'] = "SI";
				}else{
					$Rows_config[$i]['YaRespondida'] = "NO";
				}
			}
		}

		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaPreguntas($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT p.question, p.question_id, p.type, p.code
						FROM ludus_questions_r p
						WHERE p.status_id = 1
						ORDER BY p.code";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function AgrPregunta($question_id,$value,$review_id){
		@session_start();
		include('../config/database.php');
		include('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_reviews_questions_r` (review_id,question_id,value,editor,date_edition,status_id) VALUES ('$review_id','$question_id','$value','$idQuien','$NOW_data','1')";
		// echo($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActPregunta($reviews_questions_id,$status_id){
		@session_start();
		include('../config/database.php');
		include('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_reviews_questions_r` SET status_id = '$status_id' WHERE reviews_questions_id = '$reviews_questions_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		unset($DataBase_Log);
		return $resultado;
	}//fin funcion ActPregunta

	/*
		funcion para actualizar, e ingresar la nueva pregunta dentro de la Evaluaciones
		@ $questions_id:	es el id de la anterior pregunta
		@ $question_sel:	es el id de la nueva pregunta
		@ $value:					es el valor asignado para la calificacion de la pregunta
		@ $review_id:			es el id de la evaluacion correspondiente
	*/
	function ActPreguntaNueva($questions_id,$question_sel,$value,$review_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		//$idQuien = $_SESSION['idUsuario'];
		$CantPreg = 0;
		$resultado = 0;

		//SQL para consultar
		$query_sql = "SELECT ra.*
						FROM ludus_reviews_answer_r ra, ludus_reviews_answer_detail_r ad
						WHERE ra.review_id = $review_id AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = $questions_id ";
		// echo( $query_sql );
		$consultaPreguntas= $DataBase_Log->SQL_SelectMultipleRows($query_sql);

			for($x=0;$x<count($consultaPreguntas);$x++) {
				if( $review_id == $consultaPreguntas[$x]['review_id'] && $question_sel == $consultaPreguntas[$x]['question_id'] ){
					$CantPreg++;
				}
			}


		if($CantPreg==0){
			$updateSQL_ER = "UPDATE ludus_reviews_questions_r set question_id = '$question_sel', value= '$value'  where review_id = '$review_id' and question_id= '$questions_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		}
		unset($DataBase_Log);
		return $resultado;
	}//fin funcion ActPreguntaNueva


}//fin clase Evaluaciones
