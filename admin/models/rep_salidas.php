<?php
Class RepSalida {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		}else{
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 AND dealer_id = $dealer_id ORDER BY dealer";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	
	function consultaDatosAll($dealer_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			if($dealer_id=="0"){
				$query_sql = "SELECT z.zone, d.dealer, ca.charge, sum(m.duration_time) as tiempo, count(distinct u.user_id) as cantidad 
					FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_modules m, ludus_users u, ludus_headquarters h, 
					ludus_dealers d, ludus_areas a, ludus_zone z, ludus_charges_users cu, ludus_charges ca 
					WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND 
					a.zone_id = z.zone_id AND i.status_id = 2 AND i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND 
					c.course_id = m.course_id AND u.user_id = cu.user_id AND cu.charge_id = ca.charge_id AND u.status_id = 1 
					AND s.start_date BETWEEN '$start_date' AND '$end_date' 
					GROUP BY z.zone, d.dealer, ca.charge";
			}else{
				$query_sql = "SELECT z.zone, d.dealer, ca.charge, sum(m.duration_time) as tiempo, count(distinct u.user_id) as cantidad 
					FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_modules m, ludus_users u, ludus_headquarters h, 
					ludus_dealers d, ludus_areas a, ludus_zone z, ludus_charges_users cu, ludus_charges ca 
					WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND 
					a.zone_id = z.zone_id AND i.status_id = 2 AND i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND 
					c.course_id = m.course_id AND u.user_id = cu.user_id AND cu.charge_id = ca.charge_id AND u.status_id = 1 AND d.dealer_id = $dealer_id
					AND s.start_date BETWEEN '$start_date' AND '$end_date'
					GROUP BY z.zone, d.dealer, ca.charge";
			}
		}else{
			$query_sql = "SELECT z.zone, d.dealer, ca.charge, sum(m.duration_time) as tiempo, count(distinct u.user_id) as cantidad 
					FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_modules m, ludus_users u, ludus_headquarters h, 
					ludus_dealers d, ludus_areas a, ludus_zone z, ludus_charges_users cu, ludus_charges ca 
					WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND 
					a.zone_id = z.zone_id AND i.status_id = 2 AND i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND 
					c.course_id = m.course_id AND u.user_id = cu.user_id AND cu.charge_id = ca.charge_id AND u.status_id = 1 AND d.dealer_id = $dealer_id
					AND s.start_date BETWEEN '$start_date' AND '$end_date'
					GROUP BY z.zone, d.dealer, ca.charge";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
