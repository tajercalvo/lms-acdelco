<?php
Class Libraries{

	function CrearLibraries($file, $name, $image, $status_id, $description, $segment_id,$Cvct_id){
		include_once( '../../config/init_db.php' );
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$views = 0;
		$likes = 0;
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$insertSQL_EE = "INSERT INTO ludus_library_videos (file, name, image, status_id, description, detalle, specialty_id, views, likes,schedule_id,date_creation, user_id)
		VALUES ('$file', '$name', '$image', $status_id, '$description', '$description', '$segment_id', $views, $likes, $Cvct_id,'$NOW_data', $idQuien)";
		$Rows_config = DB::query( $insertSQL_EE );

		$res = array();
		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = 'Archivo creado correctamente, compartido a: ';
			$res['type'] = 'success';
			$res['library_id'] = DB::insertId();
		}else{
			$res['error'] = true;
			$res['msj'] = 'No se pudo crear el archivo';
			$res['type']= 'error';
		}
		return $res;
	}


	function ActualizarLibraries($library_id, $file, $image,  $name, $status_id, $description, $segment_id,$Cvct_id){
		include_once( '../../config/init_db.php' );
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		//Eliminando registros, para insertar los nuevos
		// $updateSQL_DEL1 = "DELETE FROM ludus_library_charge where library_id = $library_id;";
		// $Rows_config = DB::query( $updateSQL_DEL1 );

		// $updateSQL_DEL3 = "DELETE FROM ludus_library_dealer where library_id = $library_id;";
		// $Rows_config = DB::query( $updateSQL_DEL3 );

		// $updateSQL_DEL5 = "DELETE FROM ludus_library_rol where library_id = $library_id;";
		// $Rows_config = DB::query( $updateSQL_DEL5 );

		$file = $file == "" ? "" : "file = '$file', " ;
		$image = $image == "" ? "" : "image = '$image', " ;

		$updateSQL_ER = "UPDATE ludus_library_videos
								SET
								name = '$name',
								$file
								$image
								status_id		= $status_id,
								description		='$description',
								detalle			='$description',
								schedule_id		='$Cvct_id',
								specialty_id	= $segment_id,
								date_creation	= '$NOW_data',
								user_id			= $idQuien
								WHERE library_id= $library_id";
		$Rows_config = DB::query( $updateSQL_ER );

		$res = array();
		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = 'Archivo actualizado correctamente, compartido a: ';
			$res['type'] = 'success';
			$res['library_id'] = DB::insertId();
		}else{
			$res['error'] = true;
			$res['msj'] = 'No se pudo actualizar el archivo';
			$res['type']= 'error';
		}
		return $res;
	}

	function consulta_cargos_usuarios( $ajax ){
		@session_start();
		include_once( $ajax.'../config/database.php');
		include_once( $ajax.'../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$DataBase_Acciones = new Database();
		$id_usuario = $_SESSION['idUsuario'];

		$query_sql = "SELECT * FROM ludus_charges_users where user_id = $id_usuario;";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	// Eliminar libreria
	function eliminarLibraries($id){
		include_once( '../../config/init_db.php' );
		$query_sql = "DELETE FROM ludus_library_videos WHERE library_id=$id;";
		$Rows_config = DB::query($query_sql);

		$res = array();
		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = 'Archivo eliminado correctamete';
			$res['type'] = 'success';

		}else{
			$res['error'] = true;
			$res['msj'] = 'No se pudo eliminar el archivo';
			$res['type']= 'error';

		}
		return $res;
	}

	// consultar todos los archivos sin excepcion
	function consultaLibraries( $ajax = '../'){
		include_once( $ajax.'../config/init_db.php' );
		//DB::$encoding = 'utf8'; // defaults to latin1 if omitted
		@session_start();

		$query_sql = "SELECT l.*, u.image as imguser, u.first_name, u.last_name, s.specialty, est.status
		FROM ludus_library_videos l, ludus_users u, ludus_specialties s, ludus_status est
		WHERE l.user_id = u.user_id
        and s.specialty_id = l.specialty_id
        and l.status_id = est.status_id
        ORDER BY l.date_creation DESC";
		$Rows_config = DB::query($query_sql);

		foreach ( $Rows_config as $key => $value ) {
				if( $value['status_id'] == 1){
					$Rows_config[$key]['estado'] = '<span class="label label-success">Activo</span>';
				}else{
					$Rows_config[$key]['estado'] = '<span class="label label-danger">Inactivo</span>';
				}

			}
			//print_r($Rows_config);
		return $Rows_config;
	}

	// consultar todos los archivos sin excepcion
	function getSpecialities()
	{
		include_once( '../config/init_db.php' );
		DB::$encoding = 'utf8'; // defaults to latin1 if omitted
		@session_start();

		$query_sql = "SELECT * FROM ludus_specialties where status_id = 1";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}
	// consultar un archivo por id
	function consultar_libreria( $library_id ){
		include_once( '../../config/init_db.php' );
		DB::$encoding = 'utf8'; // defaults to latin1 if omitted

		$res = array();
		$query_sql = "SELECT l.*, u.image as imguser, u.first_name, u.last_name
						FROM ludus_library_videos l, ludus_users u
						WHERE l.user_id = u.user_id and library_id=$library_id";
		$Rows_config = DB::query($query_sql);
		$res['libreria'] = $Rows_config[0];

		$query_cha = "SELECT * FROM ludus_library_charge where library_id = $library_id";
		$Rows_cha = DB::query($query_cha);
		$res['cargos'] = $Rows_cha;


		$query_des = "SELECT * FROM ludus_library_dealer where library_id = $library_id";
		$Rows_con = DB::query($query_des);
		$res['concesionarios'] = $Rows_con;

		$query_rol = "SELECT * FROM ludus_library_rol where library_id = $library_id";
		$Rows_rol = DB::query($query_rol);
		$res['roles'] = $Rows_rol;
		return $res;
	}





function CrearLike($library_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (isset($_SESSION['idUsuario'])) {
			$idQuien = $_SESSION['idUsuario'];
			$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
			$DataBase_Log = new Database();
			$insertSQL_ER = "INSERT INTO ludus_library_likes (user_id,date_edition,library_id) VALUES ('$idQuien','$NOW_data','$library_id')";
			$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
			if($resultado_IN>0){
				$updateSQL_ER = "UPDATE ludus_library_videos
							SET likes = likes+1
							WHERE library_id = '$library_id'";
				$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
				unset($DataBase_Log);
				return $resultado;
			}
		} else{
			return 'sin_session';// simular un error en la peticion php
		}//END if
	}

// Quienes han dado like
function consulta_quien_like($library_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT last_name, first_name FROM ludus_users u, ludus_library_likes lll where
		lll.user_id = u.user_id
		and lll.library_id = $library_id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CreaView($library_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_library_views (library_id,user_id,date_edition) VALUES ('$library_id','$idQuien','$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Insert($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE ludus_library_videos
						SET views = views+1
						WHERE library_id = '$library_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
			unset($DataBase_Log);
		}
		unset($DataBase_Log);
		return $resultado_IN;
	}


		function consultaLibrariesSerch($valor){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT * FROM ludus_library_videos WHERE name LIKE '%$valor%' or description LIKE '%$valor%' ;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function quien_lo_vio(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "select l.library_id, l.views, u.first_name, u.last_name, u.identification, max(lv.date_edition) ultimo_vez_visto, l.name, l.description, count(*) veces_visto, h.headquarter, d.dealer
			from ludus_users u, ludus_library_videos l, ludus_library_views lv, ludus_headquarters h, ludus_dealers d
			where u.user_id = lv.user_id
			and l.library_id = lv.library_id
			and u.headquarter_id = h.headquarter_id
			and h.dealer_id = d.dealer_id
			group by l.library_id, u.first_name, u.last_name, u.identification
			order by lv.date_edition asc;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function quien_lo_vio_ajax($id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (isset($_SESSION['idUsuario'])) {
				// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "select l.library_id, u.first_name, u.last_name, u.image, u.user_id, u.identification, max(lv.date_edition) ultimo_vez_visto, l.name, l.description, count(*) veces_visto, h.headquarter, d.dealer
			from ludus_users u, ludus_library_videos l, ludus_library_views lv, ludus_headquarters h, ludus_dealers d
			where u.user_id = lv.user_id
			and l.library_id = lv.library_id
			and u.headquarter_id = h.headquarter_id
			and h.dealer_id = d.dealer_id
			and lv.library_id = $id
			group by l.library_id, u.first_name, u.last_name, u.identification
			order by lv.date_edition asc";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
			# code...
		}else{
			return 'sin_session';
		}

	}

	function misguardados(){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		if (isset($_SESSION['idUsuario'])) {
				// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
				$query_sql = "select l.*, s.date_edition as fechaguardado
								from
								ludus_library_videos l, ludus_library_saved s
								where l.library_id = s.library_id
								and s.user_id = $idQuien;";
				$DataBase_Acciones = new Database();
				$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
				unset($DataBase_Acciones);
				return $Rows_config;
					# code...
		}else{
			return 'sin_session';
		}

	}

	function misCompartidos(){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		if (isset($_SESSION['idUsuario'])) {
				// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
				$query_sql = "select l.*, s.date_edition as fechaguardado, CONCAT(u.first_name,' ' ,u.last_name) as quien
								from
								ludus_library_videos l, ludus_library_shared s, ludus_users u
								where l.library_id = s.library_id
                                and u.user_id = s.user_id
								and s.user_id_shared = $idQuien;";
				$DataBase_Acciones = new Database();
				$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
				unset($DataBase_Acciones);
				return $Rows_config;
					# code...
		}else{
			return 'sin_session';
		}

	}



	function quien_dio_like(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "select l.library_id, u.first_name, u.last_name, u.identification, ll.date_edition
				from ludus_users u, ludus_library_videos l, ludus_library_likes ll
				where u.user_id = ll.user_id
				and l.library_id = ll.library_id
				group by l.library_id, u.first_name, u.last_name, u.identification";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	// Funciones para la nueva biblioteca

		function consultaLoultimo($ajax){
		@session_start();
		$agregar = $ajax == 0? '': '../';
		include_once($agregar.'../config/database.php');
		include_once($agregar.'../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT * FROM ludus_library_videos order by date_creation desc";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaRecomendados($ajax){
		$agregar = $ajax == 0? '': '../';
		if ($this->validarsession()){
			@session_start();
		$cargo = $_SESSION['charge_id'];
		include_once($agregar.'../config/database.php');
		include_once($agregar.'../config/config.php');
		// se utiliza en la consulta, la palabra reservada DISTINCT para no traer registros duplicados
		$query_sql = "SELECT ll.* FROM ludus_library_videos ll, ludus_library_charge lc where lc.library_id = ll.library_id and lc.charge_id = $cargo order by rand()";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}else{

		return "sessionOff";
	}

	}

	function consultaMasvistos(){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_library_videos order by views desc";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaMasvistosInicial(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_library_videos order by rand() desc limit 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function validarsession(){
		@session_start();
		if (isset($_SESSION['idUsuario'])) {
			return true;
		}else{
			return false;
		}
	}



	function consultainstantanea($consultainstantanea){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_library_videos where name like '%$consultainstantanea%' or file like '%$consultainstantanea%';";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaUsuarios($buscar){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		$dealer_id = $_SESSION['dealer_id'];
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT u.user_id id, CONCAT(u.first_name, ' ', u.last_name) as text
		FROM ludus_users u, ludus_headquarters h, ludus_dealers d
        where u.headquarter_id = h.headquarter_id
        and h.dealer_id =  d.dealer_id
        and d.dealer_id = $dealer_id
        and u.first_name like '%$buscar%'
        and u.user_id <> $idQuien
		order by u.first_name
		asc limit 5;";
		// echo "$query_sql";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}


	 // public static function consultar_Usuarios($vct_id){
	 // 	include_once('../../config/init_db.php');
	 // 	$result = [];
	 // 	//quienes fueron asiganados al vct en estado 2 asistieron, estado 3 no asistieron
	 // 	$query = "SELECT i.schedule_id, i.user_id, CONCAT(u.first_name ,' ',u.last_name) nombre, i.status_id  		FROM ludus_invitation i 
		// 			INNER JOIN ludus_users u ON u.user_id = i.user_id
		// 			WHERE i.schedule_id = $vct_id";
		// $result['invitados'] = DB::query($query);

		// $query2 = "SELECT us.user_id, CONCAT(us.first_name ,' ',us.last_name) nombre,us.identification, c.charge, ch.charge_id 
		// 					FROM ludus_charges_users ch
		// 					INNER JOIN ludus_users us on us.user_id   = ch.user_id
		// 					INNER JOIN ludus_charges c on c.charge_id = ch.charge_id
		// 					WHERE ch.charge_id = 6
		// 				    group by us.user_id";
		// $result['all_user'] = DB::query($query2);

		// //print_r($result['all_user']);
		// 	foreach ($result['all_user'] as $key => $value) {
		// 			// print_r($value['user_id']);
		// 			$all_user_id = $value['user_id'];
		// 			foreach ($result['invitados'] as $key2 => $value2) {
					
		// 		             $invitados_id = $value2['user_id'];

		// 		           //  print_r($invitados_id);
		// 				if ($all_user_id == $invitados_id) {
		// 					 unset($result['all_user'][$key]);
							 
		// 				}
						
		// 			}
		// 	}
		// 	$result['all_user'] = array_values($result['all_user']);
		// 	$result['invitados'] = array_values($result['all_user']);
		// 	//print_r($result);
		// return $result;
	 // }

// 	 	$query = "SELECT 'invitados', i.schedule_id, i.user_id, CONCAT(u.first_name ,' ',u.last_name) nombre, i.status_id, '' identification, '' charge, '' charge_id,uv.checked FROM ludus_invitation i 
// 					INNER JOIN ludus_users u ON u.user_id = i.user_id
//                     left outer JOIN ludus_library_user_videos uv on uv.user_id = i.user_id
// 					WHERE i.schedule_id = $vct_id
// 					group by i.user_id
// UNION
  
// SELECT 'all_user', '' schedule_id, us.user_id, CONCAT(us.first_name ,' ',us.last_name) nombre,'' status_id, us.identification, c.charge, ch.charge_id, uv.checked
// 				FROM ludus_charges_users ch 
// 				INNER JOIN ludus_users us on us.user_id = ch.user_id 
// 				INNER JOIN ludus_charges c on c.charge_id = ch.charge_id 
// 				LEFT JOIN ludus_library_user_videos uv on uv.user_id = us.user_id
// 				WHERE ch.charge_id = 6 AND uv.user_id 
// 				NOT IN (SELECT user_id FROM ludus_invitation WHERE schedule_id = $vct_id)
// 				GROUP by us.user_id";

	 public static function consultar_Usuarios($vct_id){
	 	include_once('../../config/init_db.php');
	 	$result = [];

	 	$query = "SELECT 'all_user',  us.user_id, CONCAT(us.first_name ,' ',us.last_name) nombre, us.identification,uv.schedule_id, uv.checked, uv.date_creation
					FROM ludus_users us
					INNER JOIN ludus_charges_users cu on cu.user_id = us.user_id
					INNER JOIN ludus_charges ch on ch.charge_id = cu.charge_id
					left JOIN ludus_library_user_videos uv on uv.user_id = us.user_id
					AND uv.schedule_id = $vct_id
					WHERE cu.charge_id = 6 AND cu.status_id = 1";
	 	$result = DB::query($query);

		 $ahora= new DateTime(date("Y-m-d"));

		 foreach ($result as $key => $value) {
			$fecha2 = new DateTime($value['date_creation']);
			//print_r($value['date_creation']);
			if( is_null( $value['date_creation'] ) ){
				$result[$key]['dias'] = "Nunca asignado";
				$result[$key]['date_creation'] = "";
			}else{
				$diff = $ahora->diff($fecha2);
				$result[$key]['dias'] = 'Hace: '.$diff->days." Días";
			}
			
			 
		 }
		return $result;
	 }
	//obtienes todos los cursos vct
	function get_Vct(){
		include_once( '../config/init_db.php' );
		//DB::$encoding = 'utf8'; // defaults to latin1 if omitted
		@session_start();

		$query_sql = "SELECT DISTINCT (i.schedule_id), c.course FROM ludus_invitation i
						INNER JOIN ludus_schedule s ON s.schedule_id = i.schedule_id
						INNER JOIN ludus_courses c ON c.course_id = s.course_id
						ORDER BY i.schedule_id desc";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public static function asignar_video($p){
	 		extract($p);
	 		include_once('../../config/init_db.php');
			@session_start();
			//$idQuien = $_SESSION['idUsuario'];
			// print_r($_SESSION) ;
			// return;
	 	   // echo($user_id ." ". $opcion ." ". $schedule_id);


	 	    if ($opcion =='true') {

	 	    	$user_idres = DB::queryFirstRow("SELECT user_id FROM ludus_library_user_videos WHERE schedule_id = $schedule_id AND user_id = $user_id");

		 	      if (empty($user_idres)) {
		 	    	  $query = DB::query("INSERT INTO ludus_library_user_videos(schedule_id, user_id, checked, date_creation, creator_id) 
		 	    	  		 	    	  			VALUES (
		 	    	  		 	    	  					$schedule_id,
		 	    	  		 	    	  					$user_id,
		 	    	  		 	    	  					'checked',
		 	    	  		 	    	  					NOW(),
		 	    	  		 	    	  					30
		 	    	  		 	    	  					)");
		 	    	}else{
		 	    		$query =DB::query("UPDATE ludus_library_user_videos SET 
										 schedule_id=$schedule_id,
										 user_id=$user_id,
										 checked='checked',
										 editor_id=30,
										 date_edition=NOW()
										  WHERE user_id = $user_id and schedule_id = $schedule_id");
		 	    	}

	 	    }else{
	 	    	$query =DB::query("UPDATE ludus_library_user_videos SET schedule_id = $schedule_id,user_id = $user_id, checked = '', editor_id = 30, date_edition = NOW() WHERE user_id = $user_id and schedule_id = $schedule_id");
	 	    }
	 	   
	 	    return $query;
	 	   
	 	    
	}

	public static function busqueda($p){
		    extract($p);
	 		include_once('../../config/init_db.php');
	 		//print_r($p['datos']);
	 		$Datos = $p['datos'];
	 		// print_r($Datos);
	 		// var_dump($Datos);
	 		// return;
	 		if (!empty($Datos)) {
	 			$query = DB::query("SELECT us.user_id, CONCAT(us.first_name ,' ',us.last_name) nombre, us.identification,uv.schedule_id, uv.checked 
					FROM ludus_users us
					left JOIN ludus_library_user_videos uv on uv.user_id = us.user_id
					AND uv.schedule_id = $vct_id
					WHERE  us.first_name LIKE '%$Datos%' OR us.last_name LIKE '%$Datos%';");

		 			if (empty($query)) {
		 				# code...
		 				$query = DB::query("SELECT us.user_id, CONCAT(us.first_name ,' ',us.last_name) nombre, us.identification,uv.schedule_id, uv.checked 
						FROM ludus_users us
						left JOIN ludus_library_user_videos uv on uv.user_id = us.user_id
						AND uv.schedule_id = $vct_id
						WHERE  us.identification IN ($Datos)");
		 			}
	 		}else{
	 			$query = DB::query("SELECT 'all_user',  us.user_id, CONCAT(us.first_name ,' ',us.last_name) nombre, us.identification,uv.schedule_id, uv.checked 
					FROM ludus_users us
					INNER JOIN ludus_charges_users cu on cu.user_id = us.user_id
					INNER JOIN ludus_charges ch on ch.charge_id = cu.charge_id
					left JOIN ludus_library_user_videos uv on uv.user_id = us.user_id
					AND uv.schedule_id = $vct_id
					WHERE cu.charge_id = 6 AND cu.status_id = 1");
	 		}
	 		
	 	// 	$query = "SELECT us.user_id, CONCAT(us.first_name ,' ',us.last_name) nombre, us.identification,uv.schedule_id, uv.checked 
			// 		FROM ludus_users us
			// 		INNER JOIN ludus_library_user_videos uv on uv.user_id = us.user_id AND uv.schedule_id = $vct_id
			// 		WHERE  us.first_name LIKE '%{$p['datos']}%' OR us.last_name LIKE '%{$p['datos']}%'";
			// $result = DB::query($query);

			return $query;

	}

}//fin class
