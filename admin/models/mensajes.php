<?php
Class Mensajes {
	function consultaCantidadMensajes($prof,$tipo){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		
		$query_sql = "SELECT m.message_id, m.message, m.type, u1.user_id as creator_id, u1.first_name as first_creator, u1.last_name as last_creator, u2.user_id as reader_id, u2.first_name as first_reader, u2.last_name as last_reader, m.date_creation
			FROM ludus_messages m, ludus_users u1, ludus_users u2
			WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND m.type = '$tipo' AND m.reader_id = '$idQuien' AND m.status_id = 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaMensajes($prof,$tipo){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$query_sql = "SELECT m.message_id, m.message, m.type, u1.user_id as creator_id, u1.first_name as first_creator, u1.last_name as last_creator, u2.user_id as reader_id, u2.first_name as first_reader, u2.last_name as last_reader,m.date_creation
			FROM ludus_messages m, ludus_users u1, ludus_users u2
			WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND m.type = '$tipo' AND m.reader_id = '$idQuien' AND m.status_id = 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadTotal($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$query_sql = "SELECT m.message_id
			FROM ludus_messages m, ludus_users u1, ludus_users u2
			WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND m.reader_id = '$idQuien' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadRemitentes($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$query_sql = "SELECT distinct u1.user_id
			FROM ludus_messages m, ludus_users u1, ludus_users u2
			WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND m.reader_id = '$idQuien' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadDestinatarios($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$query_sql = "SELECT distinct u2.user_id
			FROM ludus_messages m, ludus_users u1, ludus_users u2
			WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND m.creator_id = '$idQuien' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaUsrAct($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
			$dealer_id = $_SESSION['dealer_id'];
		}else{
			$idQuien = "0";
			$dealer_id = 0;
		}
		$query_sql = "SELECT c.user_id, c.first_name, c.last_name
						FROM ludus_users c
						WHERE c.status_id = 1 AND user_id <> '$idQuien' AND c.user_id IN (11678,8048,7194,1,7187)
						UNION ";
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>=5){
			$query_sql .= "SELECT c.user_id, c.first_name, c.last_name
						FROM ludus_users c WHERE c.status_id = 1 AND c.user_id NOT IN ($idQuien,11678,8048,7194,1,7187)
						ORDER BY first_name ";
		}else{
			$query_sql .="SELECT c.user_id, c.first_name, c.last_name
						FROM ludus_users c, ludus_headquarters h
						WHERE c.status_id = 1 AND c.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id
						ORDER BY first_name ";
		}
		
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaPersonas ($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$query_sql = "SELECT user_id, first_name, last_name, image, date_creation FROM 
		(SELECT u1.user_id as user_id, u1.first_name as first_name, u1.last_name as last_name, u1.image as image, max(m.date_creation) as date_creation 
			FROM ludus_messages m, ludus_users u1, ludus_users u2 WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND m.reader_id = '$idQuien' 
			GROUP BY u1.user_id, u1.first_name, u1.last_name, u1.image 
		UNION 
		SELECT u2.user_id as user_id, u2.first_name as first_name, u2.last_name as last_name, u2.image as image, max(m.date_creation) as date_creation 
			FROM ludus_messages m, ludus_users u1, ludus_users u2 WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND m.creator_id = '$idQuien' 
			GROUP BY u2.user_id, u2.first_name, u2.last_name, u2.image) as tab 
		GROUP BY user_id, first_name, last_name, image 
		ORDER BY date_creation DESC, first_name, last_name";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function ConsultaMensajesDetail($prof,$idPersona){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$query_sql = "SELECT u1.user_id as cr_id, u1.first_name as cr_fn, u1.last_name as cr_ln, u1.image as cr_im, m.date_creation, m.type, m.message, u2.user_id as lc_id, u2.first_name as lc_fn, u2.last_name as lc_ln, u2.image lc_im
					FROM ludus_messages m, ludus_users u1, ludus_users u2
					WHERE m.creator_id = u1.user_id AND m.reader_id = u2.user_id AND ((m.creator_id = $idQuien AND m.reader_id = $idPersona) OR (m.creator_id = $idPersona AND m.reader_id = $idQuien))
					ORDER BY m.date_creation DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function crearMensaje($prof,$idPersona,$Mensaje){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_messages (message,type,creator_id,reader_id,date_creation,status_id) VALUES ('$Mensaje',1,$idQuien,$idPersona,NOW(),1)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
	function LecturaMensajes($prof,$idPersona){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		@session_start();
		if(isset($_SESSION['idUsuario'])){
			$idQuien = $_SESSION['idUsuario'];
		}else{
			$idQuien = "0";
		}
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_messages` SET status_id = '2', date_reader = NOW() WHERE creator_id = '$idPersona'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function ConsultaCirculares($start,$end,$cargos_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT m.*, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
						FROM ludus_newsletters m, ludus_users s, ludus_newsletters_charges c
						WHERE m.status_id = 1 AND m.user_id = s.user_id AND m.newsletter_id = c.newsletter_id 
						AND m.date_edition BETWEEN '$start' AND '$end'
						AND c.charge_id IN ($cargos_id) AND c.newsletter_id NOT IN (SELECT newsletter_id FROM ludus_newsletters_visites WHERE user_id = $idQuien)
						ORDER BY m.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function ConsultaCircularesJSL($cargos_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * from ludus_newsletters n";
	    $query_sql = "SELECT m.*, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
	    				FROM ludus_newsletters m, ludus_users s, ludus_newsletters_charges c
	    				WHERE m.status_id = 1 AND m.user_id = s.user_id AND m.newsletter_id = c.newsletter_id 
	    				AND c.charge_id IN ($cargos_id) AND c.newsletter_id NOT IN (SELECT newsletter_id FROM ludus_newsletters_visites WHERE user_id = $idQuien)
	    				ORDER BY m.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaTrayectoriaUsuario (){
		include_once("../config/init_db.php");
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
        $resultado = DB::queryFirstColumn("SELECT charge_id
		                            FROM ludus_charges_users
									WHERE user_id = $idQuien");
		$trayectoria = DB::query("SELECT charge_id,charge 
											FROM ludus_charges 
											WHERE status_id = 1
											ORDER BY charge");
		if(in_array("5",$resultado) && count($resultado) == 1) {
			return $trayectoria;
		}
        return "";
	}
	function trayectoriaUsuarios ($p){
		include_once("../../config/init_db.php");
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		foreach ($p as $key => $value) {
        $insert = DB::query("INSERT INTO ludus_charges_users(
																charge_id,
																user_id,
																date_creation,
																status_id
															)
															VALUES(
																$value,
																$idQuien,
																NOW(),
																'1'
															)");
		}
		return "";
	}
}


