<?php
Class InfSalida {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		}else{
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 AND dealer_id = $dealer_id ORDER BY dealer";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaMdetalle($tabla){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT * FROM $tabla WHERE status_id = 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatosAll($dealer_id,$start_date,$end_date,$specialty_id,$charge_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$WhereCharge = "charge_id > 0";
		if($charge_id>0){
			$WhereCharge = "charge_id = ".$charge_id;
		}
		$query_sql = "SELECT d.dealer_id, d.dealer,u.first_name,u.last_name,u.identification, h.headquarter_id, h.headquarter, sp.specialty_id, sp.specialty, SUM(m.duration_time) AS suma, SUM(m.duration_time)/8 AS salidas
		FROM ludus_schedule s, ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_courses c, ludus_modules m, ludus_specialties sp,
		(SELECT distinct user_id FROM ludus_charges_users where $WhereCharge) ca
		WHERE s.schedule_id = i.schedule_id
			AND i.status_id = 2
			AND i.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND s.course_id = c.course_id
			AND c.course_id = m.course_id
			AND c.specialty_id = sp.specialty_id
			AND u.user_id = ca.user_id
			AND s.start_date BETWEEN '$start_date' AND '$end_date' ";
		if($specialty_id>0){
			$query_sql .= " AND sp.specialty_id = ".$specialty_id;
		}
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			if($dealer_id>0){
				$query_sql .= " AND d.dealer_id=".$dealer_id;
			}
		}else{
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND d.dealer_id=".$dealer_id;
		}
		$query_sql .= " GROUP BY d.dealer_id, d.dealer,u.first_name,u.last_name,u.identification, h.headquarter_id, h.headquarter, sp.specialty_id, sp.specialty";
		//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
