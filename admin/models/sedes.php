<?php
Class Cargos {
	function consultaDatossedes($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,  z.area, d.dealer
			FROM ludus_headquarters c, ludus_users s, ludus_status e, ludus_areas z, ludus_dealers d
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.area_id = z.area_id AND c.dealer_id = d.dealer_id ";
			@session_start();
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="3"){
				$dealer_id_V = $_SESSION['dealer_id'];
				$query_sql .= " AND c.dealer_id = $dealer_id_V ";
			}
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="4"){
				$dealer_id = $_SESSION['dealer_id'];
				$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
			}
			if($sWhere!=''){
				$query_sql .= " AND (c.headquarter LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.area LIKE '%$sWhere%' OR c.address1 LIKE '%$sWhere%'  OR c.address2 LIKE '%$sWhere%' OR c.email1 LIKE '%$sWhere%' OR c.email2 LIKE '%$sWhere%' OR c.phone1 LIKE '%$sWhere%' OR c.phone2 LIKE '%$sWhere%' OR c.postal_code LIKE '%$sWhere%' OR c.bac LIKE '%$sWhere%' OR d.dealer LIKE '%$sWhere%') ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
// 		echo $query_sql;
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
//      print_r($Rows_config);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*, z.area, d.dealer
			FROM ludus_headquarters c, ludus_users s, ludus_status e, ludus_areas z, ludus_dealers d
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.area_id = z.area_id AND c.dealer_id = d.dealer_id ";
			@session_start();
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="3"){
				$dealer_id = $_SESSION['dealer_id'];
				$query_sql .= " AND c.dealer_id = $dealer_id ";
			}
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="4"){
				$dealer_id = $_SESSION['dealer_id'];
				$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
			}
			if($sWhere!=''){
				$query_sql .= " AND (c.headquarter LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.area LIKE '%$sWhere%' OR c.address1 LIKE '%$sWhere%'  OR c.address2 LIKE '%$sWhere%' OR c.email1 LIKE '%$sWhere%' OR c.email2 LIKE '%$sWhere%' OR c.phone1 LIKE '%$sWhere%' OR c.phone2 LIKE '%$sWhere%' OR c.postal_code LIKE '%$sWhere%' OR c.bac LIKE '%$sWhere%' OR d.dealer LIKE '%$sWhere%') ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*, s.first_name as nom_edita, s.last_name as ape_edita, z.area, d.dealer
			FROM ludus_headquarters c, ludus_users s, ludus_status e, ludus_areas z, ludus_dealers d
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.area_id = z.area_id AND c.dealer_id = d.dealer_id
			";//LIMIT 0,20
			@session_start();
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="3"){
				$dealer_id = $_SESSION['dealer_id'];
				$query_sql .= " AND c.dealer_id = $dealer_id ";
			}
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="4"){
				$dealer_id = $_SESSION['dealer_id'];
				$query_sql .= " AND c.headquarter_id IN (SELECT DISTINCT a.headquarter_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
			}
			$query_sql .= "ORDER BY headquarter";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearSede( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		session_start();
		$p['creator'] = $_SESSION['idUsuario'];
		$p['editor'] = $_SESSION['idUsuario'];
		$p['status_id'] = 1;
		$p['date_creation'] = DB::sqleval('NOW()');
		DB::insert( 'ludus_headquarters', $p );
		return DB::insertId();
	}
	function ActualizarSede( $p, $headquarter_id ){
		include_once('../../config/init_db.php');
		session_start();
		$p['editor'] = $_SESSION['idUsuario'];
		$p['date_edition'] = DB::sqleval('NOW()');
		DB::update( 'ludus_headquarters', $p, "headquarter_id = $headquarter_id" );
		return DB::affectedRows();
	}
	function consultaRegistro( $idRegistro ){
		include_once('../config/init_db.php');
		$query_sql = "SELECT *
			FROM ludus_headquarters c
			WHERE c.headquarter_id = $idRegistro ";//LIMIT 0,20

		$Rows_config = DB::queryFirstRow($query_sql);
		return $Rows_config;
	}
	function consultaCiudades($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.area_id, c.area
						FROM ludus_areas c
						WHERE c.status_id = 1
						ORDER BY c.area";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaConcesionarios($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.dealer_id, c.dealer
						FROM ludus_dealers c
						WHERE c.status_id = 1";
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="4"){
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id IN (SELECT DISTINCT a.dealer_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		$query_sql .= " ORDER BY c.dealer";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
}
