<?php
Class RepParticipacion {
	//========================================================================
	//Genera las consultas y unifica los valores de Cursos - Cargos e Inscritos
	//========================================================================
	function consultaDatosAll( $start_date, $end_date, $course_id ){
		$cursos 	= $this->consultaCursosDictados( $start_date, $end_date, $course_id );
		$cargos 	= $this->cargosCursos();
		$inscritos 	= $this->numeroInscritos( $start_date, $end_date );
		$num = count( $cursos );
		//inserto los cargos de los cursos
		for ($i=0; $i < $num ; $i++) {
			foreach ($cargos as $key => $value) {
				if( $cursos[$i]['course_id'] == $value['course_id'] ){
					$cargosCursos[] = $value['charge'];
				}
			}
			if( isset( $cargosCursos ) && count( $cargosCursos ) > 0 ){
				$cursos[$i]['trayectoria'] = $cargosCursos;
				unset( $cargosCursos );
			}
		}//fin for
		//inserto los inscritos por cada curso
		for ($i=0; $i < $num ; $i++) {
			foreach ($inscritos as $key => $valueI) {
				if( $cursos[$i]['schedule_id'] == $valueI['schedule_id'] ){
					$cursos[$i]['inscritos'] = $valueI['inscritos'];
				}
			}
		}
		return $cursos;
	}//fin funcion consultaDatosAll
	//========================================================================
	//Consulta los cursos dictados entre las dos fechas
	//========================================================================
	function consultaCursosDictados( $start_date, $end_date, $course_id ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.course_id, c.newcode, c.course, m.duration_time, sp.specialty, s.schedule_id, s.teacher_id, u.first_name, u.last_name, s.start_date, s.end_date, s.living_id, l.living, s.min_size, s.max_size, s.inscriptions AS inscritos, s.status_id
			FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_users u, ludus_specialties sp, ludus_modules m
			WHERE s.course_id = c.course_id
			AND s.living_id = l.living_id
			AND s.teacher_id = u.user_id
			AND c.specialty_id = sp.specialty_id
			AND c.course_id = m.course_id
			AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
		$query_sql .= $course_id > 0 ? " AND c.course_id = $course_id " : "";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaDatosAll
	//========================================================================
	//Consulta el numero de inscritos por cada schedule registrado
	//========================================================================
	function numeroInscritos( $start_date, $end_date ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = " SELECT i.schedule_id, COUNT( i.user_id ) AS inscritos
			FROM ludus_inscriptions i, ludus_schedule s
			WHERE i.schedule_id = s.schedule_id
			AND  s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
			GROUP BY schedule_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows( $query_sql );
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//========================================================================
	//Consulta los cargos por cursos
	//========================================================================
	function cargosCursos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.course_id, g.charge FROM ludus_charges_courses cc, ludus_courses c, ludus_charges g
			WHERE cc.course_id = c.course_id
			AND cc.charge_id = g.charge_id
			AND g.status_id = 1
			AND cc.status_id = 1
			AND c.status_id = 1
			ORDER BY c.course_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//========================================================================
	//Consulta los cursos disponibles
	//========================================================================
	public function consultaCursos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT c.course_id, c.course FROM ludus_courses c WHERE c.status_id = 1";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaCursos

}//fin clase
