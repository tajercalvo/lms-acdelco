<?php
Class Libraries{

	// Consultar todos los cursos a los que está incrito, $speciality_id determina la especialidad a consultar
	function consulta_CA_Inscrito($specialty_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return 'Su sesión a caducado, por favor inicie nuevamente haciendo clic  <a href="index.php" target="_blank">aquí</a>';
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$specialty_id = $specialty_id == "" ? "" : " AND s.specialty_id = $specialty_id " ;// modificar el query dependiendo de l acondición

		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, c.description, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.type = 'WBT' AND
					c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id NOT IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 {$specialty_id}
					ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$mis_trayectorias = $this->mis_trayectorias();
		foreach ($Rows_config as $key => $value) {
			 $Rows_config[$key]['disposicion'] = 'Autodirigidos';
			 $Rows_config[$key]['label'] = 'danger';
			foreach ($mis_trayectorias as $key2 => $value2) {
				if($value['course_id'] == $value2['course_id']){
					$Rows_config[$key]['disposicion'] = 'Mi trayectoria';
					$Rows_config[$key]['label'] = 'success';
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//Cursos que ha finalizado y quiere repasar
	function consulta_CA_Finalizado_S($specialty_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return 'Su sesión a caducado, por favor inicie nuevamente haciendo clic  <a href="index.php" target="_blank">aquí</a>';
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$specialty_id = $specialty_id == "" ? "" : " AND s.specialty_id = $specialty_id " ;// modificar el query dependiendo de l acondición
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, c.description, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id {$specialty_id} AND c.type = 'WBT' AND c.course_id NOT IN (SELECT c.course_id
					FROM ludus_courses c, ludus_charges_courses cc, ludus_charges_users cu
					WHERE cu.user_id = '$idQuien' AND cu.charge_id = cc.charge_id AND c.type = 'WBT' AND
					c.course_id = cc.course_id ) AND
					c.course_id IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien') AND
					c.course_id IN (SELECT m.course_id
								FROM ludus_modules_results_usr r, ludus_modules m
								WHERE r.user_id = '$idQuien' AND r.approval = 'SI' AND r.module_id = m.module_id)
					AND c.status_id = 1 ORDER BY c.date_edition DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$mis_trayectorias = $this->mis_trayectorias();
		foreach ($Rows_config as $key => $value) {
			 $Rows_config[$key]['disposicion'] = 'Autodirigidos';
			 $Rows_config[$key]['label'] = 'danger';
			foreach ($mis_trayectorias as $key2 => $value2) {
				if($value['course_id'] == $value2['course_id']){
					$Rows_config[$key]['disposicion'] = 'Mi trayectoria';
					$Rows_config[$key]['label'] = 'success';
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//Cursos que tiene disponibles para inscripción
	function consulta_CA_Disponible($specialty_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return 'Su sesión a caducado, por favor inicie nuevamente haciendo clic  <a href="index.php" target="_blank">aquí</a>';
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$specialty_id = $specialty_id == "" ? "" : " AND s.specialty_id = $specialty_id " ;// modificar el query dependiendo de l acondición
		$query_sql = "SELECT DISTINCT c.newcode, c.course, c.course_id, c.image, c.description, s.specialty_id, s.specialty
					FROM ludus_courses c, ludus_specialties s
					WHERE c.specialty_id = s.specialty_id AND c.type = 'WBT' AND
					c.course_id NOT IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien')
					AND c.status_id = 1 {$specialty_id} ORDER BY c.date_edition";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$mis_trayectorias = $this->mis_trayectorias();
		foreach ($Rows_config as $key => $value) {
			 $Rows_config[$key]['disposicion'] = 'Autodirigidos';
			 $Rows_config[$key]['label'] = 'danger';// agregamos la clase danger para pintar la etiqueta de color rojo
			foreach ($mis_trayectorias as $key2 => $value2) {
				if($value['course_id'] == $value2['course_id']){
					$Rows_config[$key]['disposicion'] = 'Mi trayectoria';
					$Rows_config[$key]['label'] = 'success';// agregamos la clase danger para pintar la etiqueta de color verde
				}
				$aux[$key] = $Rows_config[$key]['disposicion'];// acumulamos la variable auxiliar para luego ordenar alfabeticamente y poner los de 'Mi trayectoria primero'
			}// end foreach $key2
		}// end foreach $key
		//print_r($aux);
		if(isset($aux)){
			array_multisort($aux, SORT_DESC, $Rows_config);// ordenamos el array alfabeticamente de forma descendiente
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//trayectorias asociada al usuario
	function mis_trayectorias(){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){ // validamos la sesión, si o esta activa enviamos este mensaje en una notificacion
			return 'Su sesión a caducado, por favor inicie nuevamente haciendo clic  <a href="login.php" target="_blank">aquí</a>';
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT distinct(course_id) FROM ludus_charges_courses where charge_id in (SELECT charge_id FROM ludus_charges_users where user_id = $idQuien) order by course_id asc;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//Cursos que está buscando
	function buscar_curso($buscar){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			return 'Su sesión a caducado, por favor inicie nuevamente haciendo clic  <a href="index.php" target="_blank">aquí</a>';
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT *
						FROM ludus_courses
						WHERE type = 'WBT'
						AND status_id = 1
						AND course_id NOT IN (SELECT course_id FROM ludus_inscriptions WHERE user_id = '$idQuien')
						AND course LIKE '%$buscar%';";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$mis_trayectorias = $this->mis_trayectorias();
		foreach ($Rows_config as $key => $value) {
			 $Rows_config[$key]['disposicion'] = 'Autodirigidos';
			 $Rows_config[$key]['label'] = 'danger';
			foreach ($mis_trayectorias as $key2 => $value2) {
				if($value['course_id'] == $value2['course_id']){
					$Rows_config[$key]['disposicion'] = 'Mi trayectoria';
					$Rows_config[$key]['label'] = 'success';
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

}
