<?php
Class RepEncuestas {
	function ConsultaPreguntas($review_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT q.code, q.question, q.question_id
					FROM ludus_reviews_questions rq, ludus_questions q
					where rq.review_id = ".$review_id." AND rq.question_id = q.question_id AND q.status_id = 1 AND q.type = 2";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_Resp = "SELECT q.code, q.question, a.answer, q.question_id, a.answer_id
					FROM ludus_reviews_questions rq, ludus_questions q, ludus_answers a
					where rq.review_id = ".$review_id." AND rq.question_id = q.question_id AND q.status_id = 1 AND q.type = 2 AND q.question_id = a.question_id";
		$Rows_Resp = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resp);

		$query_Resu = "SELECT ans.answer_id, count(*) as Cantidad FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans WHERE a.review_id = '$review_id' AND a.reviews_answer_id = ad.reviews_answer_id
			AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id group by ans.answer_id";
		$Rows_Resu = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resu);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_question_id = $Rows_config[$i]['question_id'];
			for($y=0;$y<count($Rows_Resp);$y++) {
				if( $var_question_id == $Rows_Resp[$y]['question_id'] ){
					$var_answer_id = $Rows_Resp[$y]['answer_id'];
					for($z=0;$z<count($Rows_Resu);$z++) {
						if($var_answer_id == $Rows_Resu[$z]['answer_id']){
							$Rows_Resp[$y]['CantResp'][] = $Rows_Resu[$z];
						}
					}
					$Rows_config[$i]['Resp'][] = $Rows_Resp[$y];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDatosAll($review_id, $type, $start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if( $type == 4 && $review_id != 64  ){
			$query_sql = "SELECT us.first_name AS teacher_name, us.last_name AS teacher_last, s.start_date, a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, z.zone,
				r.review, r.review_id, r.code, a.score, a.available_score, md.module, cou.course, cou.newcode as code_curso
				FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r, ludus_modules_results_usr mu, ludus_modules md, ludus_courses cou, ludus_invitation i, ludus_schedule s, ludus_users us, ludus_zone z, ludus_areas ar
				WHERE a.review_id = '$review_id' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
				AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id
				AND a.module_result_usr_id = mu.module_result_usr_id AND mu.module_id = md.module_id AND md.course_id = cou.course_id
				AND mu.invitation_id = i.invitation_id
				AND i.schedule_id = s.schedule_id
				AND s.teacher_id = us.user_id
				AND h.area_id = ar.area_id
				AND z.zone_id = ar.zone_id ";
				if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']<=4){
			$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id)";
			}
		}else if( $review_id == 64 ){
			$query_sql = "SELECT a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, r.review, r.review_id, r.code, a.score, a.available_score, md.module, cou.course, cou.newcode as code_curso, z.zone
				FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r, ludus_modules_results_usr mu, ludus_modules md, ludus_courses cou, ludus_zone z, ludus_areas ar
				WHERE a.review_id = '$review_id' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
				AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id
				AND a.module_result_usr_id = mu.module_result_usr_id AND mu.module_id = md.module_id AND md.course_id = cou.course_id AND h.area_id = ar.area_id AND z.zone_id = ar.zone_id";
				if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']<=4){
			$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id)";
			}
		}else{
			$query_sql = "SELECT a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, r.review, r.review_id, r.code, a.score, a.available_score, z.zone
					FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r, ludus_zone z, ludus_areas ar
					WHERE a.review_id = '$review_id' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id AND h.area_id = ar.area_id AND z.zone_id = ar.zone_id";
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']<=4){
				$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id)";
			}
		}

		//echo( $query_sql )

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_res = "SELECT a.reviews_answer_id, ad.result, q.question, q.code, ans.answer_id, ans.answer, ans.result as res_ans
					FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans
					WHERE a.review_id = '$review_id' AND a.reviews_answer_id = ad.reviews_answer_id AND
					a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id";
		$Rows_res = $DataBase_Acciones->SQL_SelectMultipleRows($query_res);

		$query_resAb = "SELECT a.reviews_answer_id, ad.result, q.question, q.code FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q WHERE a.review_id = '$review_id'
			AND a.reviews_answer_id = ad.reviews_answer_id AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ad.question_id = q.question_id AND ad.answer_id = 0";
		$Rows_resAb = $DataBase_Acciones->SQL_SelectMultipleRows($query_resAb);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_reviews_answer_id = $Rows_config[$i]['reviews_answer_id'];

			for($y=0;$y<count($Rows_res);$y++) {
				if($var_reviews_answer_id == $Rows_res[$y]['reviews_answer_id']){
					$Rows_config[$i]['respuestas'][] = $Rows_res[$y];
				}
			}
			for($y=0;$y<count($Rows_resAb);$y++) {
				if($var_reviews_answer_id == $Rows_resAb[$y]['reviews_answer_id']){
					$Rows_config[$i]['respuestasAbiertas'][] = $Rows_resAb[$y];
				}
			}

		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDescarga($review_id, $type, $start_date,$end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if($type == 4 && $review_id != 64 ){

			$query_sql = "SELECT us.first_name AS teacher_name, us.last_name AS teacher_last, s.start_date, a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name,
				u.identification, r.review, r.review_id, r.code, a.score, a.available_score, md.module, cou.course, cou.newcode as code_curso
					FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r, ludus_modules_results_usr mu, ludus_modules md, ludus_courses cou, ludus_invitation i, ludus_schedule s, ludus_users us
					WHERE a.review_id = '$review_id' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND a.user_id = u.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id
					AND a.module_result_usr_id = mu.module_result_usr_id AND mu.module_id = md.module_id AND md.course_id = cou.course_id
					AND mu.invitation_id = i.invitation_id
					AND i.schedule_id = s.schedule_id
					AND s.teacher_id = us.user_id ";
					if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']<=4){
			$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id)";
			}
		} if( $review_id == 64 ){
			$query_sql = "SELECT a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, r.review, r.review_id, r.code, a.score, a.available_score, md.module, cou.course, cou.newcode as code_curso
				FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r, ludus_modules_results_usr mu, ludus_modules md, ludus_courses cou
				WHERE a.review_id = '$review_id' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
				AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id
				AND a.module_result_usr_id = mu.module_result_usr_id AND mu.module_id = md.module_id AND md.course_id = cou.course_id";
				if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']<=4){
			$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id)";
			}
		}else{

			$query_sql = "SELECT a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, r.review, r.review_id, r.code, a.score, a.available_score
					FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r
					WHERE a.review_id = '$review_id' AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND a.review_id = r.review_id";
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']<=4){
				$query_sql .= " AND u.user_id IN (SELECT u.user_id FROM ludus_users u, ludus_headquarters h WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id)";
			}
		}

		//echo "$query_sql"; return;

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_res = "SELECT a.reviews_answer_id, ad.result, q.question, q.code, ans.answer_id, ans.answer, ans.result as res_ans
					FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans
					WHERE a.review_id = '$review_id' AND a.reviews_answer_id = ad.reviews_answer_id AND
					a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id";
		$Rows_res = $DataBase_Acciones->SQL_SelectMultipleRows($query_res);

		$query_resAb = "SELECT a.reviews_answer_id, ad.result, q.question, q.code FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q WHERE a.review_id = '$review_id'
			AND a.reviews_answer_id = ad.reviews_answer_id AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ad.question_id = q.question_id AND ad.answer_id = 0";
		$Rows_resAb = $DataBase_Acciones->SQL_SelectMultipleRows($query_resAb);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_reviews_answer_id = $Rows_config[$i]['reviews_answer_id'];

			for($y=0;$y<count($Rows_res);$y++) {
				if($var_reviews_answer_id == $Rows_res[$y]['reviews_answer_id']){
					$Rows_config[$i]['respuestas'][] = $Rows_res[$y];
				}
			}
			for($y=0;$y<count($Rows_resAb);$y++) {
				if($var_reviews_answer_id == $Rows_resAb[$y]['reviews_answer_id']){
					$Rows_config[$i]['respuestasAbiertas'][] = $Rows_resAb[$y];
				}
			}

		}
		unset($DataBase_Acciones);

		return $Rows_config;
	}

	//========================================================================
	// Retorna el listado de encuestas registradas
	//========================================================================
	public static function EvaluacionesEncuestas($tipo){
		include_once('../config/init_db.php');
		$query_evaluaciones = "SELECT * FROM ludus_reviews WHERE type IN ($tipo) ";
		$Rows_config = DB::query($query_evaluaciones);
		return $Rows_config;
	}

	//========================================================================
	//Consulta los datos de una evaluación o encuesta especifica
	//========================================================================
	public static function getEncuesta( $review_id ){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM ludus_reviews WHERE review_id = $review_id";
		$Rows_config = DB::queryFirstRow($query);
		return $Rows_config;
	}
}
