<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
Class Libraries{

	// consultar todos los archivos sin excepcion
	function consultaLibraries(){
		include_once( '../config/init_db.php' );
		DB::$encoding = 'utf8'; // defaults to latin1 if omitted
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$cargo = $this->consulta_cargos_usuarios();
		$cargo_id = $cargo[0]['charge_id'];

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
				$query_sql = "SELECT l.*, u.image as imguser, u.first_name, u.last_name, s.segment_library
				FROM ludus_libraries l, ludus_users u, ludus_library_segment s
				WHERE l.user_id = u.user_id
                and s.library_segment_id = l.segment_id
                ORDER BY l.date_creation DESC";
			}else{

				$query_sql = "SELECT  distinct l.library_id, l.*, u.image as imguser, u.first_name, u.last_name
				FROM ludus_libraries l, ludus_users u, ludus_library_dealer ld, ludus_library_charge lc
				WHERE l.user_id = u.user_id
				and ld.library_id = l.library_id
				and ld.dealer_id = $dealer_id
                and lc.library_id = l.º
                and lc.charge_id = $cargo_id
				ORDER BY l.date_creation DESC;";
			}
		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}
	
function CrearLike($library_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (isset($_SESSION['idUsuario'])) {
			$idQuien = $_SESSION['idUsuario'];
			$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
			$DataBase_Log = new Database();
			$insertSQL_ER = "INSERT INTO ludus_library_likes (user_id,date_edition,library_id) VALUES ('$idQuien','$NOW_data','$library_id')";
			$resultado_IN = $DataBase_Log->SQL_Insert($insertSQL_ER);
			if($resultado_IN>0){
				$updateSQL_ER = "UPDATE ludus_libraries
							SET likes = likes+1
							WHERE library_id = '$library_id'";
				$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
				$updateSQL_ER = "SELECT * FROM ludus_library_likes where library_id = '$library_id'";
				$resultado = $DataBase_Log->SQL_SelectMultipleRows($updateSQL_ER);
				unset($DataBase_Log);
				return count($resultado);
			}
		} else{
			return 'sin_session';// simular un error en la peticion php
		}//END if
	}

	function crearView($library_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (isset($_SESSION['idUsuario'])) {
			$idQuien = $_SESSION['idUsuario'];
			$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
			$DataBase_Log = new Database();
			$insertSQL_ER = "INSERT INTO ludus_library_views
								(
								library_id,
								user_id,
								date_edition,
								play,
								finish)
								VALUES
								(
								$library_id,
								$idQuien,
								'$NOW_data',
								0,
								0
								);";
			$resultado_IN = $DataBase_Log->SQL_Insert($insertSQL_ER);
			
			$vistos = 0;
			if($resultado_IN>0){
				$updateSQL_ER = "SELECT * FROM ludus_library_views where library_id = '$library_id'";
				$vistos = $DataBase_Log->SQL_SelectMultipleRows($updateSQL_ER);
				$vistos = count($vistos);

				$updateSQL_ER = "UPDATE ludus_libraries
							SET views = $vistos
							WHERE library_id = '$library_id'";
				$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
				
				unset($DataBase_Log);
				return $vistos;
			}
		} else{
			return 'sin_session';// simular un error en la peticion php
		}//END if
	}

	public function consultar_segment( $segment ){
		include_once( '../../config/init_db.php' );
		$query = "SELECT * FROM ludus_libraries where status_id = 1 and segment_id = $segment limit 16";
		$Rows = DB::query($query);
		return $Rows;
	}

	public function consultarCargos(){
		include_once( '../config/init_db.php' );
		@session_start();
		$usuario_id = $_SESSION['idUsuario'];

		$query = "SELECT charge_id FROM ludus_charges_users WHERE user_id = $usuario_id";
		$Rows = DB::query($query);
		return $Rows;
	}


	public function getLibrary($specialty_id,$perfiles){
		include_once( '../config/init_db.php' );
		@session_start();
			$dealer_id = $_SESSION['dealer_id'];
			$charge_id = $_SESSION['charge_id'];
			$num = array();
			 $id_perfiles = [];
		
			foreach ($_SESSION['id_rol'] as $key => $value) {
				 	$num[] = $value['rol_id'];
			}
		
		    foreach($perfiles as $key2 => $value2) {
				 	foreach ($value2 as $key => $value) {
				 		$id_perfiles[]= $value;
				 	}
				
		    }
		    $roles = implode(",",$num);
			$perfiles_id = implode(",",$id_perfiles);
			// print_r($idperfiles);
			// return;
	    $query ="SELECT ll.*, ld.dealer_id, lc.charge_id, lr.rol_id FROM ludus_libraries ll
				INNER JOIN ludus_library_dealer ld on ld.library_id =ll.library_id
				INNER JOIN ludus_library_charge lc on lc.library_id =ll.library_id
				INNER JOIN ludus_library_rol lr  on lr.library_id =ll.library_id
				where ll.status_id = 1 AND
				ll.specialty_id = $specialty_id AND 
				ld.dealer_id in ($dealer_id) AND 
				lc.charge_id IN(".$perfiles_id.") AND 
				lr.rol_id IN(".$roles.")
				GROUP by library_id";
		$Rows = DB::query($query);
		return $Rows;
	}

	function consultar_biblioteca(){
		include_once('../../config/init_db.php');
		 $query = "SELECT * FROM ludus_libraries where status_id = 1;";
	
		 $Rows = DB::query($query);
		  return $Rows;
	}

	function buscar($txtbuscar){
		// print_r($txtbuscar);
		// die();
		include_once( '../config/init_db.php' );
	    $query = "SELECT * FROM ludus_libraries WHERE name like '%$txtbuscar%';";
		$respuesta  = DB::query($query);
		// print_r($respuesta);
		// die();
		return $respuesta;
	}

}
