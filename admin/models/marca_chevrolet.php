<?php
Class MarcaChevrolet {
	function consultaGaleria($tipo){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query = "SELECT m.media, m.media_id, m.image as source, m.description
		FROM ludus_marca_media c, ludus_media m
		WHERE c.media_id = m.media_id AND m.status_id = 1 AND m.type = '$tipo'";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaTodas(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query = "SELECT *
		FROM ludus_media m
		WHERE m.status_id = 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function BorraGalerias(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$updateSQL_ER = "DELETE FROM `ludus_marca_media` ";
		//echo($updateSQL_ER);
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function CrearGalerias($gal_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$idQuien = $_SESSION['idUsuario'];
		$insertSQL_EE = "INSERT INTO `ludus_marca_media` VALUES (null,$gal_id,$idQuien,NOW())";
		//echo($insertSQL_EE);
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
	function CrearApoyo($title,$file_name,$url){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$idQuien = $_SESSION['idUsuario'];
		$insertSQL_EE = "INSERT INTO `ludus_marca_file` VALUES (null,'$title','$file_name',$idQuien,NOW(),'$url')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
	function consultaApoyo(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query = "SELECT m.*, u.first_name
		FROM ludus_marca_file m, ludus_users u
		WHERE m.user_id = u.user_id
		ORDER BY m.date_creation DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function BorrarMaterial($idEliminar){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$idQuien = $_SESSION['idUsuario'];
		$insertSQL_EE = "DELETE FROM `ludus_marca_file` WHERE marca_file_id = $idEliminar ";
		$resultado = $DataBase_Log->SQL_Update($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}

	function CrearLike(){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_marca_likes(marca_likes_id, user_id, date_edition) VALUES (null,$idQuien,'$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		unset($DataBase_Log);
		return $resultado_IN;
	}




	function consultaLike(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query = "select count(*) as cantidad FROM ludus_marca_likes";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearView(){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_marca_views (marca_views_id, user_id, date_edition) VALUES (null, $idQuien, '$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		unset($DataBase_Log);
		return $resultado_IN;
	}

	function ConsultaView(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query = "select count(*) as cantidad FROM ludus_marca_views";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}


}
