<?php
Class CalificacionesActividades {
	
	function getCoursesTeacher($p){
		extract($p);
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
        //print_r($_SESSION);
		$rol = false;
		foreach ($_SESSION['id_rol'] as $key => $value) {
			if( $value['rol_id'] == 1 ){
				$rol = true;
				break;
			}	
		}
        
        //var_dump($rol);
		$query_aux = $rol ? '' : " and sch.teacher_id = $idQuien " ;
		$query = "SELECT mo.module_id, mo.module, co.course_id, co.course, sch.schedule_id, sch.start_date, sch.end_date, sch.status_id, sch.teacher_id
									from ludus_modules mo
									INNER join ludus_schedule sch
										on mo.module_id = sch.module_id
									INNER join ludus_courses co
										on mo.course_id = co.course_id
									WHERE 
									sch.start_date BETWEEN '$fecha_inicial 00:00:00' and '$fecha_final 23:59:59'
									$query_aux	 
									GROUP BY sch.schedule_id";
									
		$Rows_config = DB::query($query);
		return $Rows_config;
	}// fin CrearCalificacion

	function getAcitivitieUser($p){
		extract($p);
		include_once('../../config/init_db.php');
		DB::$encoding = 'utf8';
		@session_start();
		$query_ModulesCourse = "SELECT actus.*, u.identification, u.first_name, u.last_name, moac.activity, moac.activity_id, actus.teacher_id, moac.tipo, moac.porcentaje FROM
									ludus_activities_schedule acsch
										inner join ludus_activities_user actus
											on acsch.activity_schedule_id = actus.activity_schedule_id
										inner join ludus_users u
											on actus.user_id = u.user_id
										inner join ludus_modules_activities moac
											on moac.activity_id = acsch.activity_id
									where schedule_id = $schedule_id and moac.tipo <> 'Evaluacion';";
		$Rows_config = DB::query($query_ModulesCourse);
		return $Rows_config;
	}// fin CrearCalificacion


	function calificar($p){
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".date("H").":".date("i:s");
		//print_r($p);
		$registros = 0;
		$Rows_config = false;
		foreach ($p as $key => $value) {

			$superAdmin = false;
			$calificado = false;
			if( $value['teacher_id'] <> 0 ){
				//if ( $_SESSION['max_rol'] == 6) {
				if ( true) {
					$superAdmin = true;
				}else{
					continue;
				}
			}else{
				$calificado = true;
			}
			

			$registros++;
			$query_ModulesCourse = "UPDATE ludus_activities_user
							SET
							teacher_id 			= $idQuien,
							coments_teacher 	= '{$value['coments_teacher']}',
							date_teacher 		= '$NOW_data',
							resultado 			= {$value['resultado']}
							WHERE activities_user_id = {$value['activities_user_id']};";
			$Rows_config = DB::query($query_ModulesCourse);

			$clase = "SELECT schedule_id from ludus_activities_schedule where activity_schedule_id = {$value['activity_schedule_id']};";
			$res_clase = DB::queryFirstRow($clase);

			$schedule_id = $res_clase['schedule_id'];
			$query_act_usr = "SELECT act.activity_id, actus.resultado, moac.porcentaje,
											sum( ROUND( (moac.porcentaje * actus.resultado / 100), 0 ) ) puntaje_real
										from ludus_activities_schedule act
											left join ludus_activities_user actus
												on act.activity_schedule_id = actus.activity_schedule_id
													and actus.user_id = {$value['user_id']}
											inner join ludus_modules_activities moac
												on act.activity_id = moac.activity_id
										where act.schedule_id = $schedule_id;";
			$res_final = DB::queryFirstRow($query_act_usr);

			//print_r( $res_final );
			
			$score_final = $res_final['puntaje_real'];
			$module_result_usr_id = $res_final;
			$update_nota = "UPDATE ludus_modules_results_usr set score = $score_final where module_result_usr_id = {$value['module_result_usr_id']};";
			$Rows_config = DB::query($update_nota );
		}

		$res = array();
		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = "Se calificaron $registros registros";
		}else{
			$res['error'] = true;
			$res['msj'] = "No se realizaron cambios";
		}
		return $res;
	}// fin CrearCalificacion
}
