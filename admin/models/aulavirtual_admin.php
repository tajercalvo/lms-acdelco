<?php
class Foros
{
	//========================================================================
	// Consulta los foros que se pueden ver, segun rol o cargo
	//========================================================================
	public static function consultaForos($p, $prof = "../")
	{
		include_once($prof . '../config/init_db.php');
		$query_sql = "SELECT sch.*, sch.schedule_id conference_id, sch.schedule conference, u.user_id creator, u.image image_usr, u.first_name, u.last_name FROM ludus_schedule_av sch
			INNER JOIN ludus_users u
				on sch.teacher_id = u.user_id
				ORDER by sch.schedule_id DESC";
		$Rows_config = DB::query($query_sql);
		//print_r($Rows_config);
		return $Rows_config;
	} //fin consultaForos

	//========================================================================
	// Elimina las imagenes de un foro
	//========================================================================
	public static function eliminarFoto($p, $prof = "")
	{
		include_once($prof . "../config/init_db.php");

		DB::delete('ludus_conferences_files', " conference_id = %i", $p['conference_id']);

		return DB::affectedRows();
	} //fin eliminarFoto
	//========================================================================
	// Inserta una nueva foto asignada a un foro
	//========================================================================
	public static function nuevaFoto($p, $prof = "")
	{
		include_once($prof . "../config/init_db.php");
		$idx = $p['idx'] == 1 ? 1 : 0;
		DB::insert('ludus_conferences_files', [
			"conference_id" => $p['conference_id'],
			"imagen" => $p['imagen'],
			"idx" => $idx,
			"status_id" => 1
		]);

		return DB::insertId();
	} //fin nuevaFoto
	//========================================================================
	// Retorna las imagenes correspondientes a un foro
	//========================================================================
	public static function getFotos($p, $prof = "")
	{
		include_once($prof . "../config/init_db.php");
		$query = "SELECT cf.* FROM ludus_conferences_files cf WHERE cf.conference_id = %i";
		$resultSet = DB::query($query, $p['conference_id']);

		return $resultSet;
	} //fin getFotos

	//Consualar estado del foro
	public static function get_estado_foro($p)
	{
		include_once("../../config/init_db.php");
		$query = "SELECT status_id FROM ludus_conferences WHERE conference_id = '{$p['conference_id']}'";
		$resultSet = DB::query($query);

		return $resultSet[0]['status_id'];
	} //fin estado

	//========================================================================
	// Coloca las imagenes en estado 0 para seleccionar la imagen
	//========================================================================
	public static function unselectImagen($p, $prof = "")
	{
		include_once($prof . "../config/init_db.php");
		DB::update('ludus_conferences_files', [
			"idx" => 0
		], 'conference_id = %i', $p['conference_id']);
		return DB::affectedRows();
	} //fin unselectImagen
	//========================================================================
	// Coloca las imagenes en estado 0 para seleccionar la imagen
	//========================================================================
	public static function seleccionarImagen($p, $prof = "")
	{
		include_once($prof . "../config/init_db.php");
		DB::update('ludus_conferences_files', [
			"idx" => 1
		], 'conference_file_id = %i', $p['conference_file_id']);
		return DB::affectedRows();
	} //fin unselectImagen

	public static function CrearForo($p, $prof = "../")
	{
		include_once($prof . '../config/init_db.php');
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		$query = "INSERT INTO ludus_schedule_av
								(
								schedule,
								start_date,
								end_date,
								living_id,
								min_size,
								max_size,
								waiting_size,
								status_id,
								date_creation,
								course_id,
								module_id,
								teacher_id,
								date_edition,
								creator,
								editor,
								inscriptions,
								image_ics,
								session_id,
								token,
								type,
								privada)
								VALUES
								(
								'{$p['conference']}',
								now(),
								now(),

								'1',
								'1',
								'1',
								'1',
								'1',
								now(),
								'1',
								'1',
								'{$p['teacher_id']}',
								now(),
								'$user_id',
								'$user_id',
								'0',
								'image_ics',
								'{$p['session_id']}',
								'{$p['token']}',
								'0',
								'{$p['privada']}'
								);";
		$result = DB::query($query);
		$schedule_id_av = DB::insertId();

		// $query_del = "DELETE from ludus_teachers_aux_av where schedule_id_av = $schedule_id_av";
		// DB::query($query_del);
		
		if ( isset( $p['auxiliares'] ) ) {
			foreach ( $p['auxiliares'] as $key => $teacher_id_av ) {
				$query_aux = "INSERT INTO ludus_teachers_aux_av
							(teacher_id_av,
							schedule_id_av
							)
							VALUES
							(
							$teacher_id_av,
							$schedule_id_av);
							";
				$result = DB::query($query_aux);
			}
		}
		
		if ( isset( $p['cargos'] ) ) {
			foreach ( $p['cargos'] as $key => $charge_id ) {
				$query_aux = "INSERT INTO ludus_aulas_charges
						   (charge_id,
						   conference_id
						   )
						   VALUES
						   (
						   $charge_id,
						   $schedule_id_av);";
			   $result = DB::query($query_aux);
		   }
		}

		return $result;
	}
	//=======================================================================
	//Modificado 2017-07-05 Andres Vega
	//Ingreso validacion para comprobar si la persona ya se habia registrado
	//=======================================================================
	function AsistenciaForo($Foro_id)
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$resultado = 0;
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");

		$consulta = "SELECT COUNT( user_id ) AS asistio FROM ludus_conference_assitants WHERE user_id = $idQuien AND conference_id = $Foro_id ";
		$asistencia = $DataBase_Log->SQL_SelectRows($consulta);

		if ($asistencia['asistio'] == 0) {
			$insertSQL_EE = "INSERT INTO ludus_conference_assitants VALUES ('$Foro_id','$idQuien','$NOW_data')";
			$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		}
		//echo($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ConsultaAsistentes($Foro_id)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT distinct user_id
					FROM ludus_conference_assitants m
					WHERE m.conference_id = '$Foro_id' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_sum = "SELECT SUM( room_asis ) AS suma FROM ludus_conference_assitants_room WHERE conference_id = $Foro_id";
		$sumaEnSala = $DataBase_Acciones->SQL_SelectRows($query_sum);
		unset($DataBase_Acciones);
		return count($Rows_config) + $sumaEnSala['suma'];
	}
	//=======================================================================
	//Andres Vega 2017-07-05
	//=======================================================================
	function AgregarAsistentes($Foro_id, $valor)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");

		$query_sql = "SELECT COUNT( r.user_id ) AS numero
			FROM ludus_conference_assitants_room r
			WHERE r.conference_id = $Foro_id AND r.user_id = $idQuien";
		$DataBase_Acciones = new Database();
		$cuenta = $DataBase_Acciones->SQL_SelectRows($query_sql);

		if ($cuenta['numero'] == 0) {
			$query = "INSERT INTO ludus_conference_assitants_room ( conference_id, user_id, date_creation, room_asis )
			VALUES( $Foro_id, $idQuien, '$NOW_data',$valor )";
			// echo( $query );
			$Rows_config = $DataBase_Acciones->SQL_Update($query);
		} else if ($cuenta['numero'] > 0) {
			$query = "UPDATE ludus_conference_assitants_room SET room_asis = $valor WHERE user_id = $idQuien AND conference_id = $Foro_id";
			// echo( $query );
			$Rows_config = $DataBase_Acciones->SQL_Update($query);
		} else {
			$Rows_config = [];
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	} //fin funcion AgregarAsistentes
	public static function ActualizarForo($p, $prof = "../")
	{	

		include_once($prof . '../config/init_db.php');
		@session_start();
		$editor = $_SESSION['idUsuario'];
		$query = "UPDATE ludus_schedule_av
					SET
					schedule 		= '{$p['conference']}',
					teacher_id 		= '{$p['teacher_id']}',
					editor 			=  $editor,
					session_id 		= '{$p['session_id']}',
					token 			= '{$p['token']}',
					privada 		= '{$p['privada']}'
					WHERE schedule_id = {$p['conference_id']};";
		DB::query($query);

		$query_del = "DELETE from ludus_teachers_aux_av where schedule_id_av = {$p['conference_id']}";
		$result = DB::query($query_del);

		if ( isset($p['auxiliares']) ) {
			foreach ($p['auxiliares'] as $key => $teacher_id_av) {
				$query_aux = "INSERT INTO ludus_teachers_aux_av
									(teacher_id_av,
									schedule_id_av
									)
									VALUES
									(
									$teacher_id_av,
									{$p['conference_id']});";
				$result = DB::query($query_aux);
			}	
		}
		
		$query_del = "DELETE from ludus_aulas_charges where conference_id = {$p['conference_id']}";
		$result = DB::query($query_del);

		if ( isset($p['cargos']) ) {
		foreach ( $p['cargos'] as $key => $charge_id ) {
			$query_aux = "INSERT INTO ludus_aulas_charges
						(charge_id,
						conference_id
						)
						VALUES
						(
						$charge_id,
						{$p['conference_id']});";
			$result = DB::query($query_aux);
			}
		}

		return $result;
	}
	function consultaForo($idRegistro)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_conferences c
			WHERE c.conference_id = '$idRegistro' "; //LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		$query_Charges = "SELECT charge_id
						FROM ludus_conference_charges
						WHERE conference_id = '$idRegistro'
						ORDER BY charge_id ";
		$Rows_Charges = $DataBase_Class->SQL_SelectMultipleRows($query_Charges);
		$Rows_config['charges'][] = $Rows_Charges;
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatoGaleria($idRegistro)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_media c
			WHERE c.media_id = '$idRegistro' "; //LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaMed()
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_media m
					WHERE m.status_id = 1 AND m.type = 1
					ORDER BY m.media ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaGaleria($idRegistro)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*
			FROM ludus_argument a, ludus_media_detail m
			WHERE a.argument_id = '$idRegistro' AND a.media_id = m.media_id "; //LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaReply($idRegistro)
	{
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 5) {
			$query_sql = "SELECT a.*, m.first_name, m.last_name, m.image, m.status_id as status_usr
			FROM ludus_conferences_reply a, ludus_users m
			WHERE a.conference_id = '$idRegistro' AND a.user_id = m.user_id
			ORDER BY a.date_creation ASC"; //LIMIT 0,20
		} else {
			$query_sql = "SELECT a.*, m.first_name, m.last_name, m.image, m.status_id as status_usr
			FROM ludus_conferences_reply a, ludus_users m
			WHERE a.conference_id = '$idRegistro' AND a.user_id = m.user_id AND (a.status_id = 1 OR (a.status_id = 0 AND a.user_id = $idQuien) )
			ORDER BY a.date_creation ASC"; //LIMIT 0,20
		}

		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		/*$query_Likes = "SELECT cu.*, u.first_name, u.last_name
						FROM ludus_conferences_reply_likes cu, ludus_users u
						WHERE cu.user_id = u.user_id
						ORDER BY cu.date_edition ASC";
		$Rows_Likes = $DataBase_Class->SQL_SelectMultipleRows($query_Likes);

		for($i=0;$i<count($Rows_config);$i++) {
			$conference_reply_id = $Rows_config[$i]['conference_reply_id'];
			//Cargos
				for($y=0;$y<count($Rows_Likes);$y++) {
					if( $conference_reply_id == $Rows_Likes[$y]['conference_reply_id'] ){
						$Rows_config[$i]['likes_data'][] = $Rows_Likes[$y];
					}
				}
			//Cargos
		}*/
		unset($DataBase_Class);
		return $Rows_config;
	}
	function CrearReply($conference_id, $replaycomment)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_conferences_reply (conference_id,user_id,date_creation,conference_reply,status_id,likes) VALUES ('$conference_id','$idQuien','$NOW_data','$replaycomment',1,0)";
		//echo $insertSQL_ER;
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if ($resultado_IN > 0) {
			$updateSQL_ER = "UPDATE ludus_conferences
						SET num_posts = num_posts+1, last_post_user_id = '$idQuien'
						WHERE conference_id = '$conference_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		}
		return $resultado_IN;
	}
	function ConsultaMensajes($conference_id, $MaxIdReply)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*, u.first_name, u.last_name
			FROM ludus_conferences_reply c, ludus_users u
			WHERE c.conference_id = '$conference_id' AND c.conference_reply_id > $MaxIdReply AND c.user_id = u.user_id "; //LIMIT 0,20
		//echo($query_sql);
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function StatusPost($conference_reply_id, $status_id)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_conferences_reply
					SET status_id = $status_id
					WHERE conference_reply_id = '$conference_reply_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}

	public static function buscar_moderador($p)
	{
		$buscar =  isset($p['term']) ? $p['term']  : '';;
		include_once('../../config/init_db.php');
		$query = "SELECT user_id id, concat(first_name, ' ' ,last_name) 'text'
											FROM ludus_users
												where concat(first_name,' ',last_name) like '%$buscar%' or first_name like '%$buscar%' or  last_name like '%$buscar%' or identification like '%$buscar%'
												and status_id = 1 order by first_name asc limit 10 ";
		$datos = DB::query($query);
		return $datos;
	}

	public static function buscar_cargo($p)
	{
		$buscar =  isset($p['term']) ? $p['term']  : '';;
		include_once('../../config/init_db.php');
		$query = "SELECT charge_id id, charge 'text'
											FROM ludus_charges
												where charge like '%$buscar%'
												order by charge asc  ";
		$datos = DB::query($query);
		return $datos;
	}

	public static function buscar_cargos_conferencias($p)
	{
		include_once('../../config/init_db.php');
		extract($p);
		$resul['teacher_id'] = 
		$query = "SELECT u.user_id, concat(u.first_name, ' ' ,u.last_name) nombre  FROM ludus_schedule_av s
								inner join ludus_users u
									on s.teacher_id = u.user_id
								where s.schedule_id = $id_broadcast;";
		$teacher_id = DB::query($query);

		$query_aux = "SELECT u.user_id, concat(u.first_name, ' ' ,u.last_name) nombre from ludus_teachers_aux_av t
								inner join ludus_users u
									on t.teacher_id_av = u.user_id
						where t.schedule_id_av = $id_broadcast;";
		$teacher_aux = DB::query($query_aux);

		$query_aux = "SELECT cha.charge_id, cha.charge cargo FROM ludus_aulas_charges acha
							INNER JOIN	ludus_charges cha
							on cha.charge_id = acha.charge_id
						where acha.conference_id = $id_broadcast;";
		$cargos = DB::query($query_aux);

		$resul['teacher_aux'] = $teacher_aux;
		$resul['teacher_id'] = $teacher_id;
		$resul['cargos'] = $cargos;
		return $resul;
	}

	//Finalizar trasmision
	public static function finalizar_trasmision($p)
	{
		include_once('../../config/init_db.php');
		$query = "UPDATE ludus_conferences SET status_id = '9' WHERE conference_id = {$p['conference_id']};";

		$result = DB::query($query);

		if ($result) {

			$response['error'] = false;
			$response['msj']   = 'Trasmisión finalizada';
		} else {
			$response['error'] = true;
			$response['msj']   = 'NO se pudo finalizar la trasmisión';
		}
		return $response;
	}


	public static function Crear_archivos_vct($file, $name, $course_id, $name_file, $question_id = 0)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if (!isset($_SESSION['idUsuario'])) {
			return false;
		} else {
			$idQuien = $_SESSION['idUsuario'];
		}
		$DataBase_Log = new Database();
		if (strpos($name, '.mp4') || strpos($name, '.mov') || strpos($name, '.avi') || strpos($name, '.mkv') || strpos($name, '.flv')) {
			$image = 'default_video.png';
			$archivo = $name;
		} else if (strpos($name, '.pdf')) {
			$image = 'default_pdf.png';
			$archivo = $name;
		} else if (strpos($name, '.preg')) {
			$image = 'default_pregunta.png';
			$archivo = substr($name, -5); // extraemos la extención .preg
			$question_id = $name_file;
			$name_file = substr($name, 0, -5); // quitamos la extención .preg
			//$archivo = $name;
		} else {
			$image = $name;
			$archivo = $name;
		}

		if ($name_file == '') {
			$name_file = substr($name, 32); // quitamos la fecha y dejamos solamente el nombre de la diapositiva
		}

		$updateSQL_ER = "INSERT INTO ludus_courses_files_av
								(
								file,
								name,
								image,
								archivo,
								date_creation,
								course_id,
								creador_id,
								status_id,
								question_id)
								VALUES
								(
								'$file',
								'$name_file',
								'$image',
								'$archivo',
								NOW(),
								$course_id,
								$idQuien,
								2,
								$question_id);";
		$resultado = $DataBase_Log->SQL_Insert($updateSQL_ER);
		unset($DataBase_Log);
		return $resultado;
	}

	public static function borrar_archivos_vct($course_id)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$query_del = "DELETE FROM ludus_courses_files_av WHERE course_id = $course_id;";
		$resultado = $DataBase_Log->SQL_Update($query_del);
	}

	public static function get_preguntas($course_id)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$query_del = "SELECT * FROM ludus_courses_files_av WHERE course_id = $course_id AND archivo = '.preg';";
		$res = $DataBase_Log->SQL_SelectMultipleRows($query_del);;
		return $res;
	}

	public static function get_respuestas($question_id)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$query_del = "SELECT * FROM ludus_respuestas_preguntas_cursos_av WHERE question_id = $question_id";
		$res = $DataBase_Log->SQL_SelectMultipleRows($query_del);;
		return $res;
	}

	public static function crear_pregunta($p)
	{
		extract($p);
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		$DataBase_Log = new Database();

		$query = "SELECT max(question_id) as question_id FROM ludus_courses_files_av;";
		$res = $DataBase_Log->SQL_SelectRows($query);
		$res['question_id'];
		$question_id =  $res['question_id'] + 1;

		$query_ins = "INSERT INTO ludus_courses_files_av
					(
					file,
					name,
					image,
					archivo,
					date_creation,
					course_id,
					creador_id,
					status_id,
					question_id
					)VALUES(
					$orden,
					'$pregunta',
					'default_pregunta.png',
					'.preg',
					now(),
					$conference_id,
					$idQuien,
					2,
					$question_id);";
		$res = $DataBase_Log->SQL_Insert($query_ins);

		$json = array();
		if ($res) {
			$json['error'] = false;
			$json['conference_id'] = $conference_id;
			$json['msj'] = 'Pregunta creada correctamente';
		} else {
			$json['error'] = true;
			$json['msj'] = 'No se pudo crear la pregunta';
		}
		// $query_del = "INSERT INTO ludus_respuestas_preguntas_cursos_av
		// 						(
		// 						question_id,
		// 						course_id,
		// 						answer,
		// 						result,
		// 						date_creation,
		// 						creator,
		// 						status_id
		// 						)
		// 						VALUES
		// 						(
		// 						question_id,
		// 						course_id,
		// 						answer,
		// 						result,
		// 						date_creation,
		// 						creator,
		// 						status_id
		// 						);
		// 						";
		// $res = $DataBase_Log->SQL_SelectMultipleRows($query_del);;
		return $json;
	}

	public static function crear_respuesta($p)
	{
		extract($p);
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		$DataBase_Log = new Database();

		$query_ins = "INSERT INTO ludus_respuestas_preguntas_cursos_av
								(
								question_id,
								course_id,
								answer,
								result,
								date_creation,
								creator,
								status_id
								)
								VALUES
								(
								$question_id,
								$conference_id,
								'$respuesta',
								'si',
								now(),
								$idQuien,
								1
								);
								";
		$res = $DataBase_Log->SQL_Insert($query_ins);

		$json = array();
		if ($res) {
			$json['error'] = false;
			$json['question_id'] = $question_id;
			$json['msj'] = 'Respuesta creada correctamente';
		} else {
			$json['error'] = true;
			$json['msj'] = 'No se pudo crear la respuesta';
		}

		// $res = $DataBase_Log->SQL_SelectMultipleRows($query_del);;
		return $json;
	}

	public static function eliminar_pregunta($p)
	{
		extract($p);
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
		$DataBase_Log = new Database();

		$query_del = "DELETE FROM ludus_courses_files_av WHERE coursefile_id = '$coursefile_id';";
		$res = $DataBase_Log->SQL_Update($query_del);

		$query_del2 = "DELETE FROM ludus_respuestas_preguntas_cursos_av WHERE question_id = $question_id;";
		$res = $DataBase_Log->SQL_Update($query_del2);


		$json = array();
		if ($res) {
			$json['error'] = false;
			$json['course_id'] = $course_id;
			$json['msj'] = 'pregunta eliminada correctamente';
		} else {
			$json['error'] = true;
			$json['msj'] = 'No se pudo eliminar la pregunta';
		}

		// $res = $DataBase_Log->SQL_SelectMultipleRows($query_del);;
		return $json;
	}

	public static function descargar($p)
	{
		extract($p);
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();

		$query = "SELECT usuario, name pregunta, answer, a.user_id, a.user_id_av, u.identification FROM ludus_respuestas_usuarios_cursos_av ru
						inner join ludus_asistentes_av a
							on ru.user_id = a.user_id_av
								and a.schedule_id_av = ru.schedule_id
						inner join ludus_courses_files_av cf
								on cf.question_id = ru.question_id
						inner join ludus_respuestas_preguntas_cursos_av pc
								on pc.id = ru.answer_id
						left join ludus_users u
							on u.user_id = a.user_id
							where ru.schedule_id = $schedule_id
							order by pregunta;";
		$res = $DataBase_Log->SQL_SelectMultipleRows($query);

		return $res;
	}
}
