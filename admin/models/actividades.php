<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Actividades
{

	function getActivities($ajax = '')
	{
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		include_once("$ajax../config/database.php");
		include_once("$ajax../config/config.php");
		$DataBase_Class = new Database();
		$query_sql = "SELECT aces.activity_schedule_id, moac.activity, moac.activity_id, moac.tipo, moac.adjunto, moac.porcentaje, moac.review_id, aces.*, mo.module, co.course, actus.resultado, actus.archivo, actus.teacher_id, actus.comentarios, actus.coments_teacher, sch.teacher_id profesor
						FROM ludus_activities_schedule aces
								INNER JOIN ludus_modules_activities moac
									ON aces.activity_id = moac.activity_id
								INNER JOIN ludus_invitation ins
									on aces.schedule_id = ins.schedule_id
								INNER JOIN ludus_schedule sch
									on aces.schedule_id = sch.schedule_id
								INNER JOIN ludus_modules mo
									on sch.module_id = mo.module_id
								INNER JOIN ludus_courses co
									on co.course_id = mo.course_id
								LEFT JOIN ludus_activities_user actus
									ON actus.activity_schedule_id = aces.activity_schedule_id
									and actus.user_id = $user_id
								where ins.user_id = $user_id
								order by aces.desde desc ;";
		$actividades = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$ahora = date("Y-m-d") . " " . date("H") . ":" . date("i:s");

		foreach ($actividades as $key => $value) {
			//$actividades[$key]['resultado'] = is_null($value['resultado']) ? 0 : $value['resultado'] ;
			$link = '';
			if ($ahora > $value['hasta']) {

				if (is_null($value['resultado'])) {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'NO';
					$actividades[$key]['color'] = 'red';
				} else {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'SI';
					$actividades[$key]['color'] = 'green';
					$actividades[$key]['resultado'] = $value['teacher_id'] == 0 ? 'Pendiente' : $value['resultado'];
				}
			} else {
				if ($value['teacher_id'] == 0) {
					if ($value['tipo'] == 'Taller' || $value['tipo'] == 'Practica' || $value['tipo'] == 'Calificacion') {
						$link = 'op_actividades.php?id='  . $value['activity_schedule_id'] . '&activity_id=' . $value['activity_id'];
					}
					if ($value['tipo'] == 'Evaluacion') {
						$link = 'evaluar.php?review_id=' . $value['review_id'] . '&activity_schedule_id=' . $value['activity_schedule_id'];
					}

					$actividades[$key]['link'] = $link;
					$actividades[$key]['respondio'] = 'NO';
					$actividades[$key]['color'] = 'green';
				} else {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'SI';
					$actividades[$key]['color'] = 'green';
					$actividades[$key]['resultado'] = $value['teacher_id'] == 0 ? 'Pendiente' : $value['resultado'];
				}
			}
		}
		unset($DataBase_Class);
		return $actividades;
	}

	function getActivities_courses($course_id, $ajax = '')
	{
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		include_once("$ajax../config/database.php");
		include_once("$ajax../config/config.php");
		$DataBase_Class = new Database();
		$query_sql = "SELECT aces.activity_schedule_id, moac.activity, moac.activity_id, moac.tipo, moac.adjunto, moac.porcentaje, moac.review_id, aces.*, mo.module, co.course, actus.resultado, actus.archivo, actus.teacher_id, actus.comentarios, actus.coments_teacher, sch.teacher_id profesor
						FROM ludus_activities_schedule aces
								INNER JOIN ludus_modules_activities moac
									ON aces.activity_id = moac.activity_id
								INNER JOIN ludus_invitation ins
									on aces.schedule_id = ins.schedule_id
								INNER JOIN ludus_schedule sch
									on aces.schedule_id = sch.schedule_id
								INNER JOIN ludus_modules mo
									on sch.module_id = mo.module_id
								INNER JOIN ludus_courses co
									on co.course_id = mo.course_id
								LEFT JOIN ludus_activities_user actus
									ON actus.activity_schedule_id = aces.activity_schedule_id
									and actus.user_id = $user_id
								where ins.user_id = $user_id
									and co.course_id = $course_id
								order by aces.desde desc ;";
		$actividades = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$ahora = date("Y-m-d") . " " . date("H") . ":" . date("i:s");

		foreach ($actividades as $key => $value) {
			//$actividades[$key]['resultado'] = is_null($value['resultado']) ? 0 : $value['resultado'] ;
			$link = '';
			if ($ahora > $value['hasta']) {

				if (is_null($value['resultado'])) {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'NO';
					$actividades[$key]['color'] = 'red';
				} else {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'SI';
					$actividades[$key]['color'] = 'green';
					$actividades[$key]['resultado'] = $value['teacher_id'] == 0 ? 'Pendiente' : $value['resultado'];
				}
			} else {
				if ($value['teacher_id'] == 0) {
					if ($value['tipo'] == 'Taller' || $value['tipo'] == 'Practica' || $value['tipo'] == 'Calificacion') {
						$link = 'op_actividades.php?id='  . $value['activity_schedule_id'] . '&activity_id=' . $value['activity_id'];
					}
					if ($value['tipo'] == 'Evaluacion') {
						$link = 'evaluar.php?review_id=' . $value['review_id'] . '&activity_schedule_id=' . $value['activity_schedule_id'];
					}

					$actividades[$key]['link'] = $link;
					$actividades[$key]['respondio'] = 'NO';
					$actividades[$key]['color'] = 'green';
				} else {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'SI';
					$actividades[$key]['color'] = 'green';
					$actividades[$key]['resultado'] = $value['teacher_id'] == 0 ? 'Pendiente' : $value['resultado'];
				}
			}
		}
		unset($DataBase_Class);
		return $actividades;
	}

	function get_courses($ajax = '')
	{
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		include_once("$ajax../config/database.php");
		include_once("$ajax../config/config.php");
		$DataBase_Class = new Database();
		$query_sql = "SELECT co.course, co.course_id, mo.module, inv.schedule_id, sch.start_date
						from ludus_modules_results_usr more
								INNER join ludus_modules mo
									on more.module_id = mo.module_id
								INNER JOIN ludus_courses co
									on mo.course_id = co.course_id
								INNER join ludus_invitation inv
									on more.invitation_id = inv.invitation_id
								inner join ludus_schedule sch
									on inv.schedule_id = sch.schedule_id
							where more.user_id = $user_id";
		$cursos = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $cursos;
	}

	public function mdlCursos()
	{
		include("../../config/init_db.php");
		@session_start();
		$user_id = $_SESSION['idUsuario'];

		$query = "SELECT co.course_id, co.course FROM ludus_activities_schedule aces
						INNER JOIN ludus_modules_activities moac
								ON aces.activity_id = moac.activity_id
						INNER JOIN ludus_invitation ins
								on aces.schedule_id = ins.schedule_id
						INNER JOIN ludus_schedule sch
								on aces.schedule_id = sch.schedule_id
						INNER JOIN ludus_modules mo
								on sch.module_id = mo.module_id
						INNER JOIN ludus_courses co
								on co.course_id = mo.course_id
						LEFT JOIN ludus_activities_user actus
								ON actus.activity_schedule_id = aces.activity_schedule_id
								and actus.user_id = $user_id
						where ins.user_id = $user_id
						GROUP BY co.course_id
						order by aces.desde desc, mo.module_id ASC, moac.orden ASC;";
		return DB::query($query);
	}

	public static function mdlModulos($curso)
	{
		include("../../config/init_db.php");
		@session_start();
		$user_id = $_SESSION['idUsuario'];

		$query = "SELECT mo.module_id, mo.module FROM ludus_activities_schedule aces
						INNER JOIN ludus_modules_activities moac
								ON aces.activity_id = moac.activity_id
						INNER JOIN ludus_invitation ins
								on aces.schedule_id = ins.schedule_id
						INNER JOIN ludus_schedule sch
								on aces.schedule_id = sch.schedule_id
						INNER JOIN ludus_modules mo
								on sch.module_id = mo.module_id
						INNER JOIN ludus_courses co
								on co.course_id = mo.course_id
						LEFT JOIN ludus_activities_user actus
								ON actus.activity_schedule_id = aces.activity_schedule_id
								and actus.user_id = $user_id
						where ins.user_id = $user_id AND co.course_id = $curso
            GROUP BY mo.module_id
						order by aces.desde desc, mo.module_id ASC, moac.orden ASC;";
		return DB::query($query);
	}

	function mdlActividades( $ajax = '',$curso, $modulo)
	{
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		include_once("$ajax../config/database.php");
		include_once("$ajax../config/config.php");
		$DataBase_Class = new Database();
		$query_sql = "SELECT aces.activity_schedule_id, moac.activity, moac.activity_id, moac.tipo, moac.adjunto, moac.porcentaje, moac.review_id, aces.*, mo.module, co.course, actus.resultado, actus.archivo, actus.teacher_id, actus.comentarios, actus.coments_teacher, sch.teacher_id profesor
						FROM ludus_activities_schedule aces
								INNER JOIN ludus_modules_activities moac
									ON aces.activity_id = moac.activity_id
								INNER JOIN ludus_invitation ins
									on aces.schedule_id = ins.schedule_id
								INNER JOIN ludus_schedule sch
									on aces.schedule_id = sch.schedule_id
								INNER JOIN ludus_modules mo
									on sch.module_id = mo.module_id
								INNER JOIN ludus_courses co
									on co.course_id = mo.course_id
								LEFT JOIN ludus_activities_user actus
									ON actus.activity_schedule_id = aces.activity_schedule_id
									and actus.user_id = $user_id
								where ins.user_id = $user_id AND co.course_id = $curso and mo.module_id = $modulo
								order by aces.desde desc, mo.module_id ASC, moac.orden ASC ;";
		$actividades = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		$ahora = date("Y-m-d") . " " . date("H") . ":" . date("i:s");

		foreach ($actividades as $key => $value) {
			//$actividades[$key]['resultado'] = is_null($value['resultado']) ? 0 : $value['resultado'] ;
			$link = '';
			if ($ahora > $value['hasta']) {
				if (is_null($value['resultado'])) {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'NO';
					$actividades[$key]['color'] = 'red';
				} else {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'SI';
					$actividades[$key]['color'] = 'green';
					$actividades[$key]['resultado'] = $value['teacher_id'] == 0 ? 'Pendiente' : $value['resultado'];
				}
			} else {
				if ($value['teacher_id'] == 0) {

					if ($value['tipo'] == 'Taller' || $value['tipo'] == 'Practica' || $value['tipo'] == 'Calificacion') {
						$link = 'op_actividades.php?id='  . $value['activity_schedule_id'] . '&activity_id=' . $value['activity_id'];
					}
					if ($value['tipo'] == 'Evaluacion') {
						$link = 'evaluar.php?review_id=' . $value['review_id'] . '&activity_schedule_id=' . $value['activity_schedule_id'];
					}
					$actividades[$key]['link'] = $link;
					$actividades[$key]['respondio'] = 'NO';
					$actividades[$key]['color'] = 'green';
				} else {
					$actividades[$key]['boton_responder'] = '';
					$actividades[$key]['respondio'] = 'SI';
					$actividades[$key]['color'] = 'green';
					$actividades[$key]['resultado'] = $value['teacher_id'] == 0 ? 'Pendiente' : $value['resultado'];
				}
			}
		}
		// echo "<pre>";
		// print_r($actividades);
		// return;
		unset($DataBase_Class);
		return $actividades;
	}
}
