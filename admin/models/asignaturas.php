<?php
Class asignaturas {
	function consultaDatosasignaturas($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.subject_id, c.subject, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_subject c, ludus_users u, ludus_users s, ludus_status e
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id";
			if($sWhere!=''){
				$query_sql .= " AND (c.subject LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' )";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.subject_id, c.subject, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_subject c, ludus_users u, ludus_users s, ludus_status e
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id";
			if($sWhere!=''){
				$query_sql .= " AND (c.subject LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' )";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.subject_id, c.subject, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_subject c, ludus_users u, ludus_users s
			WHERE c.creator = u.user_id AND c.editor = s.user_id
			ORDER BY subject ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearasignaturas($nombre){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración asignaturas: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_subject` (subject,status_id,creator,editor,date_creation,date_edition) VALUES ('$nombre','1','$idQuien','$idQuien','$NOW_data','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarasignaturas($id,$nombre,$status){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_asignaturas.php?opcn=ver&id=$id&back=inicio>$id</a>] a asignaturas: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_subject` SET subject = '$nombre', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data' WHERE subject_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_subject c
			WHERE c.subject_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
