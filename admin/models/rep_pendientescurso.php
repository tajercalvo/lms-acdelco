<?php
Class RepPendientesCursos {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT DISTINCT c.course_id, c.course, c.newcode, c.type
FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c
WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id 
AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score >= 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1)
ORDER BY c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatosAll($course_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT DISTINCT c.newcode, c.type, c.course, u.user_id, u.first_name, u.last_name, u.identification, d.dealer, h.headquarter
FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d
WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND cc.course_id = '$course_id' 
AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score >= 80 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1 AND c.course_id = '$course_id')
ORDER BY d.dealer, u.first_name, u.last_name, c.course";
//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_Resultado = "SELECT cu.user_id, c.charge, c.charge_id
						FROM ludus_charges_users cu, ludus_charges c
						WHERE cu.charge_id = c.charge_id ";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);

		for($i=0;$i<count($Rows_config);$i++) {
			$user_id = $Rows_config[$i]['user_id'];
			//Cargos
				for($y=0;$y<count($Rows_cant);$y++) {
					if( $user_id == $Rows_cant[$y]['user_id'] ){
						$Rows_config[$i]['cargos'][] = $Rows_cant[$y];
					}
				}
			//Cargos
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
