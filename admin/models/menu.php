<?php

Class Menu{

	public static function mdlMenu(){
		include_once('../config/init_db.php');
		DB::$encoding = 'utf8';
		// session_start();
		$idQuien = $_SESSION['idUsuario'];
		$roles = 0;
		foreach ($_SESSION['id_rol'] as $key => $value) {
			$roles .= ','.$value['rol_id'];
		}
		// echo "$roles";
		// die();
		//$menuSql = "SELECT * FROM ludus_menu WHERE nivel = 1 AND estado_id = 1 ORDER BY orden";
		$menuSql = "SELECT m.* FROM ludus_roles_menus rm
						inner join ludus_menu m
							on rm.menu_id = m.menu_id
								and rm.rol_id in ($roles)
								and m.nivel = 1
								and m.estado_id = 1
						group by menu_id
		                order by m.orden;";
		                // die();
		$menuPrincipal = DB::query($menuSql);

		//$subSql = "SELECT * FROM ludus_menu WHERE nivel = 2 AND estado_id = 1 ORDER BY orden";
		$subSql = "SELECT m.* FROM ludus_roles_menus rm
						inner join ludus_menu m
							on rm.menu_id = m.menu_id
								and rm.rol_id in ($roles)
								and m.nivel = 2
								and m.estado_id = 1
						group by menu_id
		                order by m.orden;";
		$submenu = DB::query($subSql);

		foreach ($menuPrincipal as $key => $value) {
			$menuId = $menuPrincipal[$key]['menu_id'];
			$menuPrincipal[$key]['subMenu']= array();
			foreach ($submenu as $key2 => $value2) {
				$subId = $submenu[$key2]['padre_id'];
				if ($menuId == $subId) {
					$menuPrincipal[$key]['subMenu'][] = $value2;
				}
			}
		}

		//$subSql2 = "SELECT * FROM ludus_menu WHERE nivel = 3 AND estado_id = 1 ORDER BY orden";
		$subSql2 = "SELECT m.* FROM ludus_roles_menus rm
						inner join ludus_menu m
							on rm.menu_id = m.menu_id
								and rm.rol_id in ($roles)
								and m.nivel = 3
								and m.estado_id = 1
						group by menu_id
		                order by m.orden;";
		$submenu2 = DB::query($subSql2);

		if (!empty($submenu2)) {
			foreach ($menuPrincipal as $key => $value) {
				if (!empty($menuPrincipal[$key]['subMenu'])) {
					foreach ($menuPrincipal[$key]['subMenu'] as $key2 => $value2) {
							$menuPrincipal[$key]['subMenu'][$key2]['subMenu'] = array();
						$subMenuId = $menuPrincipal[$key]['subMenu'][$key2]['menu_id'];
						foreach ($submenu2 as $key3 => $value3) {
							$subId = $submenu2[$key3]['padre_id'];
							if ($subMenuId == $subId) {
								$menuPrincipal[$key]['subMenu'][$key2]['subMenu'][]=$value3;
							}
						}
					}
				}
			}
		}

		return $menuPrincipal;
	}

	public static function getMenu(){
		include_once('../../config/init_db.php');
		DB::$encoding = 'utf8';

		$query = "SELECT p.menu_id, p.menu padre,  'NO' as hijo, s.status  FROM
						ludus_menu p inner join ludus_status s on s.status_id = p.estado_id and padre_id = 0
					union
					SELECT h.menu_id, p.menu padre, h.menu hijo, s.status  FROM
						ludus_menu p
							inner join ludus_menu h
								on p.menu_id = h.padre_id
							inner join ludus_status s on s.status_id = h.estado_id;";
		$result = DB::query($query);

		foreach ($result as $key => $value) {
			$result[$key]['estado'] = $value['status'] == 'Activo' ? 'checked' : '';
		}
		return $result;
	}

	public static function act_menu($estado_id, $menu_id){
		include_once('../../config/init_db.php');
		DB::$encoding = 'utf8';

		$query = "UPDATE ludus_menu SET estado_id = '$estado_id' WHERE menu_id = '$menu_id';";
		$result = DB::query($query);

		$res = array();
		if( $result ){
			$res['error'] 	= false;
			$res['msj'] 	= 'Menú actualizado correctamente';
		}else{
			$res['error'] 	= true;
			$res['msj'] 	= 'Error, Intente nuevamente';
		}
		return $res;
	}

	public static function get_permisos( $menu_id ){
		include_once('../../config/init_db.php');
		DB::$encoding = 'utf8';

		$query = "SELECT r.rol_id, r.rol, rm.menu_id from ludus_roles r
					left join ludus_roles_menus rm
						on r.rol_id = rm.rol_id
							and rm.menu_id = $menu_id
                        group by r.rol_id;";
		$result = DB::query($query);

		foreach ($result as $key => $value) {
			$result[$key]['estado'] = $value['menu_id'] == null ? '' : 'checked';
		}

		return $result;
	}

	public static function act_perm( $estado_id, $menu_id, $rol_id ){
		include_once('../../config/init_db.php'	);
		// DB::$encoding = 'utf8';

		if( $estado_id == 1 ){
			$query = "INSERT INTO ludus_roles_menus (rol_id, menu_id, status_id) VALUES ( $rol_id, $menu_id, $estado_id);";
		}else{
			$query = "DELETE FROM ludus_roles_menus WHERE rol_id = '$rol_id' and menu_id = '$menu_id';";
		}

		echo "$query";
		$result = DB::query($query);
		$res = array();
		if( $result ){
			$res['error'] 	= false;
			$res['msj'] 	= 'Menú actualizado correctamente';
		}else{
			$res['error'] 	= true;
			$res['msj'] 	= 'Error, Intente nuevamente';
		}
		return $res;
	}

}

