<?php

Class Encuestas_pendientes{

	public static function getEncuentas_pendientes(){
		include_once('../config/init_db.php');
		//DB::$encoding = 'utf8';
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$roles = 0;
		$NOW_data 	= date("Y-m-d");
		
		$menuSql = "SELECT en.encuesta_id, en.encuesta, en.fecha_inicio, en.fecha_fin, en.despliegue_mensual, en.status_id FROM ludus_encuestas en
							where en.status_id = 1
									and en.fecha_inicio <= '$NOW_data'
									and en.fecha_fin >= '$NOW_data'
									or (en.status_id = 1 and en.despliegue_mensual = 'SI');";
		$encuesta = DB::queryFirstRow($menuSql);
		
		if( is_null($encuesta) ){
			return false;
		}


		//Primer d铆a del mes
		$month = date('m');
      	$year = date('Y');
		$primer_dia_mes = date('Y-m-d', mktime(0,0,0, $month, 1, $year));

		$encuesta_id = isset($encuesta['encuesta_id']) ? $encuesta['encuesta_id'] : 0 ;

		$sel_enc_charges = "SELECT * from ludus_encuestas_charges encha
												inner join ludus_charges_users chaus
													on encha.charge_id = chaus.charge_id
											where encuesta_id = $encuesta_id and chaus.user_id  = $idQuien;";
		$res_char = DB::queryFirstRow($sel_enc_charges);

		$sel_enc_courses 	= "SELECT * from ludus_encuestas_courses enco
									inner join ludus_schedule sch
										on enco.course_id = sch.course_id
									inner join ludus_invitation inv
										on inv.schedule_id = sch.schedule_id
									where enco.encuesta_id = $encuesta_id and user_id = $idQuien;";
		$res_courses = DB::queryFirstRow($sel_enc_courses);

		$sel_enc_dealers 	= "SELECT * from ludus_encuestas_dealers ende
											inner join ludus_dealers d
												on ende.dealer_id = d.dealer_id
											inner join ludus_headquarters h
												on d.dealer_id = h.dealer_id
											inner join ludus_users u
												on u.headquarter_id = h.headquarter_id
										where encuesta_id = $encuesta_id and u.user_id = $idQuien;";
		$res_dealers = DB::queryFirstRow($sel_enc_dealers);
		
        if( is_null( $res_char ) and is_null( $res_courses ) and is_null( $res_dealers )){ return false; }
        

		$fecha_mes = (isset($encuesta['despliegue_mensual']) && $encuesta['despliegue_mensual'] == 'SI' )? " and creacion >= '$primer_dia_mes 00:00:00' " : '' ;
		$query_respuesta_usuario = "SELECT * from ludus_encuestas_respuestas_usuarios
										where encuesta_id = $encuesta_id
											and user_id = $idQuien
											$fecha_mes;";

		$respuesta_usuario = DB::queryFirstRow($query_respuesta_usuario);
		// echo '<pre>';
		// var_dump($respuesta_usuario);
		// die();
		if( is_null($respuesta_usuario)){ // no ha respondido la encuesta
			$queyr_preguta = "SELECT * FROM ludus_encuestas_preguntas where encuesta_id = $encuesta_id ;";
			$encuesta['preguntas'] = DB::query($queyr_preguta);
		}else{
			return false;
		}
		// echo '<pre>';
		// var_dump($encuesta);
		// die();
		return $encuesta;
	}

	public function getEncuestas(){
		include_once('../config/init_db.php');
		//DB::$encoding = 'utf8';
		//@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$roles = 0;
		$query_enc = "SELECT en.encuesta_id, en.encuesta, en.fecha_inicio, en.fecha_fin, en.despliegue_mensual, en.creacion, en.status_id, u.first_name, u.last_name  from ludus_encuestas en
						INNER JOIN ludus_users u
						on en.creador = u.user_id";
		$encuesta = DB::query($query_enc);

		//Encuesta por empresas
		$query_enc_dea = "SELECT de.dealer, de.dealer_id, ende.encuesta_id from ludus_encuestas_dealers ende
							INNER JOIN ludus_dealers de
							on ende.dealer_id = de.dealer_id";
		$encuestas_dealers = DB::query($query_enc_dea);

		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['empresas'] = array();
			foreach ($encuestas_dealers as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['empresas'][] = $value2;
				}
			}
		}
		//Fin encuesta por empresas

		//Encuesta por cargos
		$query_enc_cha = "SELECT encha.encuesta_id, cha.charge_id, cha.charge FROM ludus_encuestas_charges encha
							inner join ludus_charges cha
								on encha.charge_id = cha.charge_id;";
		$encuestas_charges = DB::query($query_enc_cha);

		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['cargos'] = array();
			foreach ($encuestas_charges as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['cargos'][] = $value2;
				}
			}
		}
		//Fin encuesta por cargos

		//Encuesta por cursos
		$query_enc_cha = "SELECT enco.encuesta_id, co.course_id, co.course FROM ludus_encuestas_courses enco
									inner join ludus_courses co
										on enco.course_id = co.course_id; ";
		$encuestas_charges = DB::query($query_enc_cha);

		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['cursos'] = array();
			foreach ($encuestas_charges as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['cursos'][] = $value2;
				}
			}
		}
		//Fin encuesta por cursos

		$query_enc_pre = "SELECT * from ludus_encuestas_preguntas;";
		$encuestas_preguntas = DB::query($query_enc_pre);

		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['preguntas'] = array();
			foreach ($encuestas_preguntas as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['preguntas'][] = $value2;
				}
			}
		}

		return $encuesta;
	}

	public function consultaDealers(){
		include_once('../config/init_db.php');
		$query_sql = "SELECT * FROM ludus_dealers where status_id = 1;";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public function consultaCharges(){
		include_once('../config/init_db.php');
		$query_sql = "SELECT * FROM ludus_charges where status_id = 1;";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public function consultaCourses(){
		include_once('../config/init_db.php');
		$query_sql = "SELECT * FROM ludus_courses;";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public function getEncuesta( $encuesta_id ){
		include_once('../../config/init_db.php');
		$query_enc = "SELECT en.encuesta_id, en.encuesta, en.fecha_inicio, en.fecha_fin, en.despliegue_mensual, en.status_id, en.creacion, u.first_name, u.last_name  from ludus_encuestas en
						INNER JOIN ludus_users u
						on en.creador = u.user_id
						and en.encuesta_id = $encuesta_id";
		$encuesta = DB::query($query_enc);

		$query_enc_dea = "SELECT de.dealer, de.dealer_id, ende.encuesta_id from ludus_encuestas_dealers ende
							INNER JOIN ludus_dealers de
							on ende.dealer_id = de.dealer_id";
		$encuestas_dealers = DB::query($query_enc_dea);

		//encuesta por empresas
		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['empresas'] = array();
			foreach ($encuestas_dealers as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['empresas'][] = $value2;
				}
			}
		}
		// fin encuesta por encuesta

		//Encuesta por cargos
		$query_enc_cha = "SELECT encha.encuesta_id, cha.charge_id, cha.charge FROM ludus_encuestas_charges encha
							inner join ludus_charges cha
								on encha.charge_id = cha.charge_id;";
		$encuestas_charges = DB::query($query_enc_cha);

		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['cargos'] = array();
			foreach ($encuestas_charges as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['cargos'][] = $value2;
				}
			}
		}
		//Fin encuesta por cargos

		//Encuesta por cursos
		$query_enc_cha = "SELECT enco.encuesta_id, co.course_id, co.course FROM ludus_encuestas_courses enco
									inner join ludus_courses co
										on enco.course_id = co.course_id; ";
		$encuestas_charges = DB::query($query_enc_cha);

		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['cursos'] = array();
			foreach ($encuestas_charges as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['cursos'][] = $value2;
				}
			}
		}
		//Fin encuesta por cursos

		$query_enc_pre = "SELECT * from ludus_encuestas_preguntas;";
		$encuestas_preguntas = DB::query($query_enc_pre);

		foreach ($encuesta as $key => $value) {
			$encuesta[$key]['preguntas'] = array();
			foreach ($encuestas_preguntas as $key2 => $value2) {
				if($value['encuesta_id'] == $value2['encuesta_id']){
					$encuesta[$key]['preguntas'][] = $value2;
				}
			}
		}

		return $encuesta[0];
	}

	public function crearEncuesta( $p ){
		// print_r($p);
		// die();
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		include_once('../../config/init_db.php');
		$fecha_inicio = $p['despliegue_mensual'] == 'SI'? '00-00-00' : $p['fecha_inicio'];
		$fecha_fin = $p['despliegue_mensual'] == 'SI' ? '00-00-00' : $p['fecha_fin'];
		$estado = $p['estado'];
		$despliegue_mensual = $p['despliegue_mensual'];
		$encuesta = $p['encuesta'];
		// print_r($p);
		// die();
		$NOW_data 	= date("Y-m-d")." ".date("H").":".date("i:s");
		$query_insert_enc = "INSERT INTO ludus_encuestas
						(
						encuesta,
						creador,
						creacion,
						fecha_inicio,
						despliegue_mensual,
						status_id,
						fecha_fin
						)
						VALUES
						(
						'$encuesta',
						$idQuien,
						'$NOW_data',
						'$fecha_inicio',
						'$despliegue_mensual',
						'$estado',
						'$fecha_fin'
						);";
		$result_enc = DB::query($query_insert_enc);
		$encuesta_id = DB::insertId();

		$creacion 	= date("Y-m-d")." ".date("H").":".date("i:s");

		if( $result_enc ){

			//crear encuesta por empresas
			foreach ($p['dealer_id'] as $key => $value) {
				$query_insert_dea = "INSERT INTO ludus_encuestas_dealers
										(
										encuesta_id,
										dealer_id)
										VALUES
										(
										$encuesta_id,
										$value);";
				$result_dea = DB::query($query_insert_dea);
			}
			//Fin crear encuesta por empresas

			//Crear encuesta por cargos
			foreach ($p['charge_id'] as $key => $charge_id) {
				$query_insert_cha = "INSERT INTO ludus_encuestas_charges
											(
											encuesta_id,
											charge_id,
											creador,
											creacion,
											estatus_id)
											VALUES
											(
											$encuesta_id,
											$charge_id,
											$idQuien,
											'$creacion',
											1);";
				DB::query($query_insert_cha);
			}
			//Fin encuesta por cargos

			//Crear encuesta por cursos
			foreach ($p['course_id'] as $key => $course_id) {
				$query_insert_co = "INSERT INTO ludus_encuestas_courses
												(
												encuesta_id,
												course_id,
												creador,
												creacion,
												estatus_id)
												VALUES
												(
												$encuesta_id,
												$course_id,
												$idQuien,
												'$creacion',
												1);
												;";
				DB::query($query_insert_co);
			}
			//Fin encuesta por cursos

			foreach ($p['array_preguntas'] as $key => $value) {
				$query_insert_pre = "INSERT INTO ludus_encuestas_preguntas
											(
											encuesta_id,
											encuesta_pregunta,
											tipo_respuetsa
											)
											VALUES
											(
											$encuesta_id,
											'{$value['encuesta_pregunta']}',
											1
											);";
				$result_pre = DB::query($query_insert_pre);
			}
		}

		$res = array();
		if( $result_enc && $result_dea && $result_pre){
			$res['error'] 	= false;
			$res['msj'] 	= 'Encuesta guardada correctamente';
		}else{
			$res['error'] 	= true;
			$res['msj'] 	= 'Algo sali贸 mal, intenta nuevamente';
		}

		return $res;
	}

	public function editarEncuesta( $p ){
		// print_r($p);
		// die();
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		include_once('../../config/init_db.php');
		$fecha_inicio = $p['despliegue_mensual'] == 'SI'? '00-00-00' : $p['fecha_inicio'];
		$fecha_fin = $p['despliegue_mensual'] == 'SI' ? '00-00-00' : $p['fecha_fin'];
		$estado = $p['estado'];
		$despliegue_mensual = $p['despliegue_mensual'];
		$encuesta = $p['encuesta'];

		// borrar encuesta de las empreas
		$encuesta_id = $p['encuesta_id'];
		$truncate_dealers = "DELETE FROM ludus_encuestas_dealers WHERE encuesta_id = $encuesta_id;";
		DB::query($truncate_dealers);

		// borrar encuesta de los cursos
		$encuesta_id = $p['encuesta_id'];
		$truncate_dealers = "DELETE FROM ludus_encuestas_courses WHERE encuesta_id = $encuesta_id;";
		DB::query($truncate_dealers);

		// borrar encuesta de los cargos
		$encuesta_id = $p['encuesta_id'];
		$truncate_dealers = "DELETE FROM ludus_encuestas_charges WHERE encuesta_id = $encuesta_id;";
		DB::query($truncate_dealers);
		
		
		$truncate_preg = "DELETE FROM ludus_encuestas_preguntas WHERE encuesta_id = $encuesta_id;";
		$res_trun_preg = DB::query($truncate_preg);
		// print_r($p);
		// die();

		$NOW_data 	= date("Y-m-d")." ".date("H").":".date("i:s");
		$query_insert_enc = "UPDATE ludus_encuestas
								SET
								encuesta 			= '$encuesta',
								creador 			=  $idQuien,
								creacion 			= '$NOW_data',
								fecha_inicio 		= '$fecha_inicio',
								despliegue_mensual 	= '$despliegue_mensual',
								status_id 			= '$estado',
								fecha_fin 			= '$fecha_fin'
								WHERE encuesta_id 	= $encuesta_id	;";
		$result_enc = DB::query($query_insert_enc);

		$creacion 	= date("Y-m-d")." ".date("H").":".date("i:s");
		if( $result_enc ){
            
            if( is_array($p['dealer_id']) ){
    			foreach ($p['dealer_id'] as $key => $dealer_id) {
    				$query_insert_dea = "INSERT INTO ludus_encuestas_dealers
    										(
    										encuesta_id,
    										dealer_id)
    										VALUES
    										(
    										$encuesta_id,
    										$dealer_id);";
    				DB::query($query_insert_dea);
    			}
            }
            
            if( is_array($p['charge_id']) ){
                foreach ($p['charge_id'] as $key => $charge_id) {
				$query_insert_cha = "INSERT INTO ludus_encuestas_charges
											(
											encuesta_id,
											charge_id,
											creador,
											creacion,
											estatus_id)
											VALUES
											(
											$encuesta_id,
											$charge_id,
											$idQuien,
											'$creacion',
											1);";
				    DB::query($query_insert_cha);
			    }
            }
			
            if( is_array($p['course_id']) ){
                foreach ($p['course_id'] as $key => $course_id) {
				$query_insert_co = "INSERT INTO ludus_encuestas_courses
												(
												encuesta_id,
												course_id,
												creador,
												creacion,
												estatus_id)
												VALUES
												(
												$encuesta_id,
												$course_id,
												$idQuien,
												'$creacion',
												1);
												;";
				    DB::query($query_insert_co);
			    }   
            }
            
			foreach ($p['array_preguntas'] as $key => $value) {
				$query_insert_dea = "INSERT INTO ludus_encuestas_preguntas
											(
											encuesta_id,
											encuesta_pregunta,
											tipo_respuetsa
											)
											VALUES
											(
											$encuesta_id,
											'{$value['encuesta_pregunta']}',
											1
											);";
				$result_pre = DB::query($query_insert_dea);
			}
		}

		$res = array();
		if( $result_pre ){
			$res['error'] 	= false;
			$res['msj'] 	= 'Encuesta guardada correctamente';
		}else{
			$res['error'] 	= true;
			$res['msj'] 	= 'Algo sali贸 mal, intenta nuevamente';
		}

		return $res;
	}

	public function guardarEncuesta( $p )
	{	
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];

		$query_aux = "SELECT headquarter_id FROM ludus_users where user_id = $idQuien;";
		$res_aux = DB::queryFirstRow($query_aux);
		$headquarter_id = $res_aux['headquarter_id'];
		
		
		$query_aux_d = "SELECT dealer_id FROM ludus_headquarters WHERE headquarter_id = $headquarter_id";
		$res_aux_d = DB::queryFirstRow($query_aux_d);
		$dealer_id = $res_aux_d['dealer_id'];
		
		$encuesta_id = $p['encuesta_id'];
		$NOW_data 	= date("Y-m-d")." ".date("H").":".date("i:s");
		foreach ($p['datos'] as $key => $value) {
			$query = "INSERT INTO ludus_encuestas_respuestas_usuarios
					(
					encuesta_id,
					encuesta_pregunta_id,
					puntaje,
					dealer_id,
					headquarter_id,
					user_id,
					creacion)
					VALUES
					(
					$encuesta_id,
					{$value['encuesta_pregunta_id']},
					'{$value['puntaje']}',
					$dealer_id,
					$headquarter_id,
					$idQuien,
					'$NOW_data'
					);";
					$result = DB::query($query);
		}

		$res = array();
		if ($result) {
			$res['error'] = false;
			$res['msj'] = 'Encuesta guardada correctamente';
		}else{
			$res['error'] = true;
			$res['msj'] = 'No se pudo guardar la encuesta, intenta nuevamente';
		}
		return $res;

	}
	
}
