<?php
Class RepSinCalificar {
	public static function consultaCantidad(){
		include_once('../config/init_db.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$fec_dia = date("Y-m-d");
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.newcode, c.course, u.first_name, u.last_name, u.identification, i.schedule_id, COUNT( i.invitation_id ) AS numero
			FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_users u, ludus_users us
			WHERE s.schedule_id = i.schedule_id
			AND s.course_id = c.course_id
			AND s.teacher_id = u.user_id
			AND i.user_id = us.user_id
			AND us.headquarter_id != 114
			AND s.start_date BETWEEN '2018-01-01 00:00:00' AND '$fec_dia 23:59:59'
			AND s.status_id = 1
			AND i.status_id = 1
			GROUP BY i.schedule_id HAVING numero > 0
			ORDER BY s.start_date";
					//echo $query_sql;

		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}
	public static function consultaDatosAll(){
		include_once('../config/init_db.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$fec_dia = date("Y-m-d");
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.newcode, c.course, u.first_name, u.last_name, u.identification, i.schedule_id, COUNT( i.invitation_id ) AS numero
			FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_users u, ludus_users us
			WHERE s.schedule_id = i.schedule_id
			AND s.course_id = c.course_id
			AND s.teacher_id = u.user_id
			AND i.user_id = us.user_id
			AND us.headquarter_id != 114
			AND s.start_date BETWEEN '2018-01-01 00:00:00' AND '$fec_dia 23:59:59'
			AND s.status_id = 1
			AND i.status_id = 1
			GROUP BY i.schedule_id HAVING numero > 0
			ORDER BY s.start_date";
					//echo $query_sql;

		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}
}
