<?php
class RepAsistencias {

	public static function consultaDatos( $prof="../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT * FROM ludus_reviews WHERE type IN (1,3)";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public static function consultaDatosAll($review_id, $prof = "../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$query_sql = "SELECT r.review, u.first_name, u.last_name, u.identification, h.headquarter, d.dealer, a.*
			FROM ludus_reviews_answer a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r
			WHERE a.review_id = '$review_id' AND a.review_id = r.review_id
			AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id
			AND a.score = 0 AND a.time_response = '00:00' AND a.reviews_answer_id NOT IN (SELECT reviews_answer_id FROM ludus_reviews_answer_detail)";

		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public static function eliminar_Respuesta( $reviews_answer_id, $prof="../"){
		include_once( $prof.'../config/init_db.php' );
		@session_start();
		$idQuien 	= $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$insert 	= 0;
		$reviews_answer = DB::queryFirstRow( "SELECT * FROM ludus_reviews_answer WHERE reviews_answer_id = $reviews_answer_id" );
		$reviews_answer['eraser_id'] = $idQuien;
		$reviews_answer['date_eraser'] = $NOW_data;
		DB::delete( 'ludus_reviews_answer', "reviews_answer_id = $reviews_answer_id" );
		if ( DB::affectedRows() ) {
			DB::insert( "ludus_reviews_answer_eraser", $reviews_answer );
			$insert = DB::affectedRows();
		}// fin if

		return $insert;
	}// fin eliminar_Respuesta

	public static function consultaDatosAllR($review_id, $prof = "../"){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$query_sql = "SELECT r.review, u.first_name, u.last_name, u.identification, h.headquarter, d.dealer, a.*
			FROM ludus_reviews_answer_r a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews r
			WHERE a.review_id = '$review_id' AND a.review_id = r.review_id
			AND a.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id
			AND a.score = 0 AND a.time_response = '00:00' AND a.reviews_answer_id NOT IN ( SELECT reviews_answer_id FROM ludus_reviews_answer_detail_r )";

		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	public static function eliminar_RespuestaR( $reviews_answer_id, $prof="../"){
		include_once( $prof.'../config/init_db.php' );
		@session_start();
		DB::delete( 'ludus_reviews_answer_r', "reviews_answer_id = $reviews_answer_id" );

		return DB::affectedRows();
	}// fin eliminar_Respuesta

	//=====================================================================
	// Consulta los datos de evaluaciones de refuerzo
	//=====================================================================
	public static function consultaDatosR( $prof="../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT * FROM ludus_reviews_r WHERE type IN (1,3)";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;

	}

}
