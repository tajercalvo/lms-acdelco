<?php
Class Mejor {

	// interacciones por sección
	function interacciones_por_sección($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();

		$Query = "SELECT section, count(*) as cantidad,
					CASE
					WHEN section = 'clublideres.php' THEN 'Infografía'
				    WHEN section = 'extracto_lideres.php' THEN 'Mi Kilometraje'
				    WHEN section = 'catalogo_lideres.php' THEN 'Catálogo'
				    ELSE ''
					END
					AS modulo
					FROM ludus_navigation
					WHERE date_navigation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'   
				    and section in ('clublideres.php','extracto_lideres.php','catalogo_lideres.php','rep_metricas_lideres.php')
					GROUP BY section;";
		$Rows = $DataBase_Acciones->SQL_SelectMultipleRows($Query);

		unset($DataBase_Acciones);
		return $Rows;

	}

	//usuarios únicos por sección
	function usuarios_únicos_por_sección($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();


		$Query ="SELECT section, count(distinct user_id) as usuarios,
					CASE
					WHEN section = 'clublideres.php' THEN 'Infografía'
				    WHEN section = 'extracto_lideres.php' THEN 'Mi Kilometraje'
				    WHEN section = 'catalogo_lideres.php' THEN 'Catálogo'
				    ELSE ''
					END
					AS modulo
					FROM ludus_navigation
					WHERE date_navigation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'   
				    and section in ('clublideres.php','extracto_lideres.php','catalogo_lideres.php','rep_metricas_lideres.php')
					GROUP BY section;";
			
		$Rows = $DataBase_Acciones->SQL_SelectMultipleRows($Query);

		unset($DataBase_Acciones);
		return $Rows;
	}

}

