<?php
Class Invitaciones {

	//====================================================================================
	//Consulta todos los cursos especificos
	//====================================================================================
	function consulta_All_Cursos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.course_id, c.newcode, c.type, c.course, s.first_name as nom_edita, s.last_name as ape_edita
						FROM ludus_courses c, ludus_users s, ludus_status e
						WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.status_id = 1 ";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta los cursos especificos
	//====================================================================================
	function consulta_Cursos( $sWhere, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT c.course_id, c.newcode, c.type, c.course, s.first_name as nom_edita, s.last_name as ape_edita
						FROM ludus_courses c, ludus_users s, ludus_status e
						WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.status_id = 1
						AND (c.course LIKE '%$sWhere%' OR c.type LIKE '%$sWhere%') LIMIT 30";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta las sesiones por cada curso
	//====================================================================================
	function consulta_Schedule( $curso, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT s.schedule_id, s.start_date, u.first_name, u.last_name, l.living
						FROM ludus_schedule s, ludus_users u, ludus_livings l
						WHERE s.teacher_id = u.user_id AND s.living_id = l.living_id AND s.course_id = $curso ORDER BY s.start_date DESC";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta los usuarios invitados para cada sesion por curso
	//====================================================================================
	public static function consulta_Invitacion_Schedule($schedule_id, $prof="../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$dealer_id = 0;
		}else{
			$dealer_id = $_SESSION['dealer_id'];
		}

		if ($dealer_id == "0") {
			$query_sql = "SELECT i.*, u.first_name, u.last_name, u.identification, u.image, h.headquarter_id, h.dealer_id, d.dealer, s.course_id
							FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_schedule s
							WHERE i.user_id = u.user_id
							AND u.headquarter_id = h.headquarter_id
							AND h.dealer_id = d.dealer_id
							AND i.schedule_id = s.schedule_id
							AND i.schedule_id = $schedule_id";
		} else {
			$query_sql = "SELECT i.*, u.first_name, u.last_name, u.identification, u.image, h.headquarter_id, h.dealer_id, d.dealer, s.course_id
							FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_schedule s
							WHERE i.user_id = u.user_id
							AND u.headquarter_id = h.headquarter_id
							AND h.dealer_id = d.dealer_id
							AND i.schedule_id = s.schedule_id
							AND i.schedule_id = $schedule_id
							AND d.dealer_id = $dealer_id" ;
		}
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta las sesiones por cada curso despues de la fecha actual
	//====================================================================================
	public static function consulta_New_Schedule( $curso_id, $start_date, $prof="../" ){
		include_once($prof.'../config/init_db.php');
		$query_sql = "SELECT s.schedule_id, s.start_date, u.first_name, u.last_name, l.living
			FROM ludus_schedule s, ludus_users u, ludus_livings l
			WHERE s.teacher_id = u.user_id
		    AND s.living_id = l.living_id
		    AND s.course_id = $curso_id
		    AND s.start_date > '$start_date'
		    ORDER BY s.start_date DESC";
		//echo $query_sql;
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta la existencia de una invitacion en la nueva sesion
	//====================================================================================
	function consulta_Existencia_Invitacion( $user_id, $new_schedule_id, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT * FROM ludus_invitation WHERE user_id='$user_id' AND schedule_id = '$new_schedule_id' ";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		$num_rows = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $num_rows;
	}

	//====================================================================================
	//Edita las sesion para cada usuario en la invitacion
	//====================================================================================
	function Edicion_Schedule_User_Invitacion( $new_schedule_id, $schedule_id, $user_id, $invitation_id, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');

		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_invitation SET schedule_id='$new_schedule_id', status_id='1' WHERE invitation_id='$invitation_id' AND user_id='$user_id' AND schedule_id='$schedule_id' ";
		$resultado_update = $DataBase_Log->SQL_Update($updateSQL_ER);

		if ( $resultado_update > 0) {
			$query_sql = "SELECT * FROM ludus_modules_results_usr
							WHERE invitation_id = '$invitation_id' AND user_id = '$user_id' AND status_id IN (2,3)";
			$Rows_config = $DataBase_Log->SQL_SelectRows($query_sql);
			$num_rows = $DataBase_Log->SQL_SelectCantidad($query_sql);
			//Se elimina los registros con estado 2 o 3 de ludus_modules_results_usr si existen
			if ( $num_rows > 0) {
				$module_result_usr_id = $Rows_config['module_result_usr_id'];
				$updateSQL_ER = "DELETE FROM ludus_modules_results_usr WHERE module_result_usr_id='$module_result_usr_id' ";
				$resultado_update = $DataBase_Log->SQL_Update($updateSQL_ER);
			}
		}

		unset($DataBase_Log);
		return $resultado_update;
	}

	//====================================================================================
	//Consulta y Elimina la inscripcion por cada usuario a Curso y Sesion
	//====================================================================================
	function borrar_User_Inscriptions( $schedule_id, $user_id, $course_id, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');

		$query_sql = "SELECT * FROM ludus_inscriptions
						WHERE schedule_id = '$schedule_id' AND user_id = '$user_id' AND course_id = '$course_id'
						ORDER BY date_creation DESC";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		$num_rows = $DataBase_Acciones->SQL_SelectCantidad($query_sql);

		if( $num_rows>0 ){
			$inscription_id = $Rows_config['inscription_id'];
			$updateSQL_ER = "DELETE FROM ludus_inscriptions WHERE inscription_id='$inscription_id' ";
			//$updateSQL_ER = "UPDATE ludus_inscriptions SET status_id='12' WHERE inscription_id='$inscription_id' ";
			$resultado_update = $DataBase_Acciones->SQL_Update($updateSQL_ER);

			unset($DataBase_Acciones);
			return $resultado_update;
		} else{
			unset($DataBase_Acciones);
			return 2; //No existe inscripcion
		}

	}

	//====================================================================================
	//Edita el cupo de la sesion para cada concesionario
	//====================================================================================
	function edita_Dealer_Schedule( $new_schedule_id, $schedule_id, $dealer_id, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');

		//Consulta de cupo para la sesion origen
		$query_sql = "SELECT * FROM ludus_dealer_schedule WHERE schedule_id = $schedule_id AND dealer_id = $dealer_id";
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_schedule = $DataBase_Acciones->SQL_SelectRows($query_sql);
		$num_rows = $DataBase_Acciones->SQL_SelectCantidad($query_sql);

		if( $num_rows > 0 ){

			$min_size = $Rows_schedule['min_size'];
			$max_size = $Rows_schedule['max_size'];
			$inscriptions = $Rows_schedule['inscriptions'];
			$total_size = $Rows_schedule['total_size'];
			$dealer_schedule_id = $Rows_schedule['dealer_schedule_id'];

			//Consulta de cupo para la sesion nueva
			$query_sql = "SELECT * FROM ludus_dealer_schedule WHERE schedule_id = $new_schedule_id AND dealer_id = $dealer_id";
			//echo $query_sql;
			$Rows_schedule_new = $DataBase_Acciones->SQL_SelectRows($query_sql);
			$num_rows_new = $DataBase_Acciones->SQL_SelectCantidad($query_sql);

			if( $num_rows_new > 0 ){
				//Si el cupo no es igual o menor a cero se resta
				if( $min_size > 0 && $max_size > 0 && $inscriptions > 0 ){
				//if( $inscriptions > 0 && $total_size > 0){
					$updateSQL_ER = "UPDATE ludus_dealer_schedule SET min_size=min_size-1, max_size=max_size-1, inscriptions=inscriptions-1 WHERE dealer_schedule_id=$dealer_schedule_id";
					//$updateSQL_ER = "UPDATE ludus_dealer_schedule SET inscriptions=inscriptions-1, total_size=total_size-1 WHERE dealer_schedule_id=$dealer_schedule_id";
					$resultado_schedule = $DataBase_Acciones->SQL_Update($updateSQL_ER);
					//Si la sesion origen se edita exitosamente, se agrega el cupo a la nueva sesion
					if ( $resultado_schedule > 0) {

						$dealer_schedule_id_new = $Rows_schedule_new['dealer_schedule_id'];
						$updateSQL_ER = "UPDATE ludus_dealer_schedule SET min_size=min_size+1,max_size=max_size+1,inscriptions=inscriptions+1 WHERE dealer_schedule_id=$dealer_schedule_id_new";
						//$updateSQL_ER = "UPDATE ludus_dealer_schedule SET inscriptions=inscriptions+1, total_size=total_size+1 WHERE dealer_schedule_id=$dealer_schedule_id_new";
						$resultado_schedule_new = $DataBase_Acciones->SQL_Update($updateSQL_ER);
						//Si la nueva sesion se edita existosamente, se regresa TRUE
						if ( $resultado_schedule_new > 0) {
							unset($DataBase_Acciones);
							return $resultado_schedule_new;
						} else {
							unset($DataBase_Acciones);
							return 0;
						}

					} else {
						unset($DataBase_Acciones);
						return 0;
					}

				} else {
					unset($DataBase_Acciones);
					return 0;
				}


			} else {
				unset($DataBase_Acciones);
				return $num_rows_new;
			}


		} else {
			unset($DataBase_Acciones);
			return $num_rows;
		}

	}

	public static function consultaSchedule( $schedule_id, $prof="../" ) {
		include_once($prof.'../config/init_db.php');

		$query_sql = "SELECT * FROM ludus_schedule s WHERE s.schedule_id = $schedule_id";
		$resultSet = DB::queryFirstRow( $query_sql );

		return $resultSet;
	}


}
