<?php

class outside_user{
	

 public static function consultar_outside_user(){
	include_once('../../config/init_db.php');
	@session_start();
		
	$resultado = DB::query("SELECT u.user_id,
									CONCAT(u.first_name, ' ', u.last_name) AS nombre,
									u.identification,
									u.email,
									cu.charge_id,
									cu.charge,
									courser.courser
									FROM ludus_users u
									INNER JOIN (SELECT GROUP_CONCAT(lcu.charge_id) as charge_id,
														GROUP_CONCAT(c.charge SEPARATOR '<br>') as charge,
														lcu.user_id
														FROM
														ludus_charges_users lcu
														INNER JOIN ludus_charges c
														ON lcu.charge_id = c.charge_id
													WHERE lcu.status_id = 1 AND
														  c.status_id = 1
															GROUP BY lcu.user_id ) as cu
									ON u.user_id = cu.user_id
									INNER JOIN (SELECT GROUP_CONCAT(lcu.course_id) as course_id,
														GROUP_CONCAT(lc.course SEPARATOR ' - ') as courser,
														lcu.user_id
														FROM ludus_courses_users lcu
														INNER JOIN ludus_courses lc
														ON lcu.course_id = lc.course_id
														WHERE lc.status_id = 1
															GROUP BY lcu.user_id) as courser
									ON u.user_id = courser.user_id
									WHERE status_id = 1 AND cu.charge_id != 5");
	    return $resultado;
	}

	 public static function consultarCurso_id($user_id){
			include_once('../../config/init_db.php');
			@session_start();
			//DB::$encoding = 'utf8';
			$totalCursos = DB::query("SELECT c.course_id,
												c.course,
												cu.charge_id
												FROM ludus_courses c
												INNER JOIN ludus_charges_courses cu
												ON c.course_id = cu.course_id
												WHERE c.type = 'WBT' AND
													cu.charge_id IN(SELECT charge_id FROM ludus_charges_users WHERE user_id = $user_id)
													GROUP BY cu.course_id");
		    $cursosUsuario =  DB::query("SELECT lcu.course_id,
												c.course
												FROM ludus_courses_users lcu
												INNER JOIN ludus_courses c
												ON lcu.course_id = c.course_id
												WHERE user_id = $user_id");

	        $courses_adq['cursosTotal'] = $totalCursos;
	        $courses_adq['cursosUsuario'] = $cursosUsuario;
	      
		    return $courses_adq;

	    }


	    public static function consultar_allCursos(){

		include_once('../../config/init_db.php');
					@session_start();

				    $query = "SELECT course_id, course FROM ludus_courses WHERE type = 'wbt' and status_id = 1";

				    $courses_adq  = DB::query($query);

				    return $courses_adq;


	    }

	    public static function editar_asignacion($p){

		include_once('../../config/init_db.php');
					@session_start();
			//DB::$encoding = 'utf8';
			
			$idUsuario = $_SESSION['idUsuario'];

					// print_r($p['info'][0]['value']);
					// return;
			    $Eliminar = DB::query("DELETE FROM ludus_courses_users WHERE user_id = '{$p['id']}'");

			    if ($Eliminar) {

			    		foreach ($p['info'] as $key => $v) {
			    			
			    		$query = "INSERT INTO ludus_courses_users
                            (user_id, 
                            course_id, 
                            date_creation, 
                            creator, 
                            editor, 
                            date_edition, 
                            status_id)
                            VALUES
                            (
                            '{$p['id']}',
                            '{$p['info'][$key]['value']}',
                             NOW(),
                            $idUsuario,
                            $idUsuario,
                            NOW(),
                            1)";
                            $resultSet = DB::query( $query );

			    		}

      
			    }
			   if ($resultSet) {
			        $respuesta['error'] = false;
			        $respuesta['msj'] = "Actualizado con éxito.";
			   }else {
			        $respuesta['error'] = true;
			        $respuesta['msj'] = "La asignacion no ha sido guardado con éxito.";
			    }
			        return $respuesta;


	    }

}//fin class


// $consultar = new Asistentesbroad();
// $consultar -> consultabrod();