<?php
Class RepPrestamos {
	function consultaDatosAll($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT d.dealer, h.headquarter, l.living, l.address, c.city, s.living_id, count(*) as cantidad 
					FROM ludus_schedule s, ludus_livings l, ludus_cities c, ludus_headquarters h, ludus_dealers d 
					WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND 
					s.living_id = l.living_id AND l.city_id = c.city_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id
					GROUP BY d.dealer, h.headquarter, l.living, l.address, c.city, s.living_id 
					ORDER BY count(*) DESC";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$query_sql_det = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, s.inscriptions, c.newcode, c.course, u.first_name, u.last_name, s.living_id
					FROM ludus_schedule s, ludus_courses c, ludus_users u 
					WHERE s.status_id = 1 AND s.course_id = c.course_id AND s.teacher_id = u.user_id AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
		$Rows_detail = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_det);
		for($i=0;$i<count($Rows_config);$i++) {
			$var_living_id = $Rows_config[$i]['living_id'];
			for($y=0;$y<count($Rows_detail);$y++) {
				if($var_living_id == $Rows_detail[$y]['living_id']){
					$Rows_config[$i]['detail'][] = $Rows_detail[$y];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
