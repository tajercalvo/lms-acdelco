<?php
Class RepAsistenciasTotales {
	function consultaDatosAll($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT s.schedule_id, c.newcode, c.course, u.first_name, u.last_name, s.start_date, s.end_date, l.living, d.dealer,
			ds.min_size, ds.max_size, ds.inscriptions, d.dealer_id
					FROM ludus_schedule s, ludus_courses c, ludus_users u, ludus_livings l, ludus_dealer_schedule ds,
					ludus_dealers d
					WHERE s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND s.course_id = c.course_id AND
					s.teacher_id = u.user_id AND s.living_id = l.living_id AND s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id
					AND ds.min_size > 0 AND s.status_id = 1
					ORDER BY s.start_date";//LIMIT 0,20
		}else{
			$query_sql = "SELECT s.schedule_id, c.newcode, c.course, u.first_name, u.last_name, s.start_date, s.end_date, l.living, d.dealer,
			ds.min_size, ds.max_size, ds.inscriptions, d.dealer_id
					FROM ludus_schedule s, ludus_courses c, ludus_users u, ludus_livings l, ludus_dealer_schedule ds,
					ludus_dealers d
					WHERE s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND s.course_id = c.course_id AND
					s.teacher_id = u.user_id AND s.living_id = l.living_id AND s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id
					d.dealer_id = '$dealer_id' AND ds.min_size > 0 AND s.status_id = 1
					ORDER BY s.start_date";//LIMIT 0,20
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_SiAsistio="SELECT h.dealer_id, i.schedule_id, count(*) as asistencias
											FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s
											WHERE i.status_id IN (2) AND i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
											AND i.schedule_id = s.schedule_id AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
											GROUP BY h.dealer_id, i.schedule_id";
		$Rows_Siconfig = $DataBase_Acciones->SQL_SelectMultipleRows($query_SiAsistio);

		$query_NoAsistio="SELECT h.dealer_id, i.schedule_id, count(*) as noasistencias
											FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s
											WHERE i.status_id IN (3) AND i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
											AND i.schedule_id = s.schedule_id AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
											GROUP BY h.dealer_id, i.schedule_id";
		$Rows_Noconfig = $DataBase_Acciones->SQL_SelectMultipleRows($query_NoAsistio);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_dealer_id = $Rows_config[$i]['dealer_id'];
			$var_schedule_id = $Rows_config[$i]['schedule_id'];
			for($y=0;$y<count($Rows_Siconfig);$y++) {
				if($var_dealer_id == $Rows_Siconfig[$y]['dealer_id'] && $var_schedule_id == $Rows_Siconfig[$y]['schedule_id'] ){
					$Rows_config[$i]['Siconfig'][] = $Rows_Siconfig[$y];
				}
			}
			for($z=0;$z<count($Rows_Noconfig);$z++) {
				if($var_dealer_id == $Rows_Noconfig[$z]['dealer_id'] && $var_schedule_id == $Rows_Noconfig[$z]['schedule_id'] ){
					$Rows_config[$i]['Noconfig'][] = $Rows_Noconfig[$z];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
