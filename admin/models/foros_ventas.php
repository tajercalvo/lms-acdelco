<?php
Class Foros {
	//========================================================================
	// Consulta los foros que se pueden ver, segun rol o cargo
	//========================================================================
	public static function consultaForos($p, $prof = "../"){
		include_once($prof.'../config/init_db.php');
		$filtro = "";
		$fecha = "";
		if( $p['texto'] != "" ){
			$filtro = "AND ( m.conference LIKE '%{$p['texto']}%' OR u.first_name LIKE '%{$p['texto']}%' OR u.last_name LIKE '%{$p['texto']}%' ) ";
		}
		if( $p['fecha'] != "" ){
			$fecha = "AND YEAR( m.date_creation ) = {$p['fecha']}";
		}
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>=5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
						FROM ludus_conferences m, ludus_users u, ludus_users s
						WHERE m.creator = u.user_id
						$fecha
						AND m.last_post_user_id = s.user_id
						$filtro
						ORDER BY m.date_creation DESC, m.conference_id,m.category, m.position
						LIMIT 0,10";
		}else{
			$query_sql = "SELECT DISTINCT m.*, u.first_name, u.last_name, u.image as image_usr, u.email, s.first_name as first_post, s.last_name as last_post, s.image as img_post
						FROM ludus_conferences m, ludus_users u, ludus_users s, ludus_conference_charges c
						WHERE m.status_id IN( 1, 9 )
						AND m.creator = u.user_id
						AND m.last_post_user_id = s.user_id
						AND m.conference_id = c.conference_id
						$filtro
						AND c.charge_id IN ({$p['charge_id']})
						$fecha
						ORDER BY m.date_creation DESC, m.conference_id,m.category, m.position
						LIMIT 0,10";
		}
		// echo $query_sql;
		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}//fin consultaForos

	//========================================================================
	// Elimina las imagenes de un foro
	//========================================================================
	public static function eliminarFoto( $p, $prof="" ){
		include_once( $prof."../config/init_db.php" );

		DB::delete('ludus_conferences_files'," conference_id = %i", $p['conference_id']);

		return DB::affectedRows();
	}//fin eliminarFoto
	//========================================================================
	// Inserta una nueva foto asignada a un foro
	//========================================================================
	public static function nuevaFoto( $p, $prof="" ){
		include_once( $prof."../config/init_db.php" );
		$idx = $p['idx'] == 1 ? 1 : 0;
		DB::insert('ludus_conferences_files',[
			"conference_id" => $p['conference_id'],
			"imagen" => $p['imagen'],
			"idx" => $idx,
			"status_id" => 1
		]);

		return DB::insertId();
	}//fin nuevaFoto
	//========================================================================
	// Retorna las imagenes correspondientes a un foro
	//========================================================================
	public static function getFotos( $p, $prof="" ){
		include_once( $prof."../config/init_db.php" );
		$query = "SELECT cf.* FROM ludus_conferences_files cf WHERE cf.conference_id = %i";
		$resultSet = DB::query( $query, $p['conference_id'] );

		return $resultSet;
	}//fin getFotos

	//Consualar estado del foro
	public static function get_estado_foro( $p ){
		include_once( "../../config/init_db.php" );
		$query = "SELECT status_id FROM ludus_conferences WHERE conference_id = '{$p['conference_id']}'";
		$resultSet = DB::query( $query );

		return $resultSet[0]['status_id'];
	}//fin estado

	//========================================================================
	// Coloca las imagenes en estado 0 para seleccionar la imagen
	//========================================================================
	public static function unselectImagen( $p, $prof="" ){
		include_once( $prof."../config/init_db.php" );
		DB::update('ludus_conferences_files',[
			"idx" => 0
		], 'conference_id = %i', $p['conference_id'] );
		return DB::affectedRows();
	}//fin unselectImagen
	//========================================================================
	// Coloca las imagenes en estado 0 para seleccionar la imagen
	//========================================================================
	public static function seleccionarImagen( $p, $prof="" ){
		include_once( $prof."../config/init_db.php" );
		DB::update('ludus_conferences_files',[
			"idx" => 1
		], 'conference_file_id = %i', $p['conference_file_id'] );
		return DB::affectedRows();
	}//fin unselectImagen

	public static function CrearForo($p, $prof="../"){
		include_once($prof.'../config/init_db.php');

		$query = "INSERT INTO ludus_conferences
														(
														conference,
														conference_description,
														category,
														date_creation,
														creator,
														status_id,
														num_posts,
														last_post_user_id,
														position
														)
														VALUES
														(
														'{$p['conference']}',
														'{$p['conference_description']}',
														'{$p['category']}',
														 NOW(),
														 {$p['id_usuario']},
														1,
														0,
														{$p['id_usuario']},
														'{$p['position']}'
														);
														";
		DB::query( $query );

		// DB::insert('ludus_conferences', [
		// 	'conference' 							=>$p['conference'],
		// 	'conference_description' 	=>$p['conference_description'],
		// 	'category' 								=>$p['category'],
		// 	'date_creation' 					=> DB::sqleval('NOW()'),
		// 	'creator' 								=>$p['user_id'],
		// 	'status_id' 							=> 1,
		// 	'num_posts' 							=> 0,
		// 	'last_post_user_id' 			=> $p['user_id'],
		// 	'position' 								=>$p['position']
		// ]);
		return DB::insertId();
	}
	//=======================================================================
	//Modificado 2017-07-05 Andres Vega
	//Ingreso validacion para comprobar si la persona ya se habia registrado
	//=======================================================================
	function AsistenciaForo($Foro_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$resultado = 0;
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$consulta = "SELECT COUNT( user_id ) AS asistio FROM ludus_conference_assitants WHERE user_id = $idQuien AND conference_id = $Foro_id ";
		$asistencia = $DataBase_Log->SQL_SelectRows( $consulta );

		if( $asistencia['asistio'] == 0 ){
			$insertSQL_EE = "INSERT INTO ludus_conference_assitants VALUES ('$Foro_id','$idQuien','$NOW_data')";
			$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		}
		//echo($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ConsultaAsistentes($Foro_id){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT distinct user_id
					FROM ludus_conference_assitants m
					WHERE m.conference_id = '$Foro_id' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_sum = "SELECT SUM( room_asis ) AS suma FROM ludus_conference_assitants_room WHERE conference_id = $Foro_id";
		$sumaEnSala = $DataBase_Acciones->SQL_SelectRows( $query_sum );
		unset($DataBase_Acciones);
		return count($Rows_config) + $sumaEnSala['suma'];
	}
	//=======================================================================
	//Andres Vega 2017-07-05
	//=======================================================================
	function AgregarAsistentes( $Foro_id, $valor ){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$query_sql = "SELECT COUNT( r.user_id ) AS numero
			FROM ludus_conference_assitants_room r
			WHERE r.conference_id = $Foro_id AND r.user_id = $idQuien";
		$DataBase_Acciones = new Database();
		$cuenta = $DataBase_Acciones->SQL_SelectRows( $query_sql );

		if( $cuenta['numero'] == 0 ){
			$query = "INSERT INTO ludus_conference_assitants_room ( conference_id, user_id, date_creation, room_asis )
			VALUES( $Foro_id, $idQuien, '$NOW_data',$valor )";
			// echo( $query );
			$Rows_config = $DataBase_Acciones->SQL_Update( $query );
		}else if( $cuenta['numero'] > 0 ){
			$query = "UPDATE ludus_conference_assitants_room SET room_asis = $valor WHERE user_id = $idQuien AND conference_id = $Foro_id";
			// echo( $query );
			$Rows_config = $DataBase_Acciones->SQL_Update( $query );
		}else{
			$Rows_config = [];
		}

		unset( $DataBase_Acciones );
		return $Rows_config;
	}//fin funcion AgregarAsistentes
	public static function ActualizarForo($p, $prof="../"){
		include_once($prof.'../config/init_db.php');
		$p['estado'] = isset( $p['estado'] ) ? $p['estado'] : 1;
		DB::update('ludus_conferences',[
			'conference' => $p['conference'],
			'conference_description' => $p['conference_description'],
			'creator' => $p['id_usuario'],
			'date_creation' => DB::sqleval('NOW()'),
			'category' => $p['category'],
			'position' => $p['position'],
			'status_id' => $p['estado'],
			'token' => $p['token'],
			'session_id' => $p['session_id'],
		],"conference_id = {$p['conference_id']}" );
		return DB::affectedRows();
	}
	function consultaForo($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_conferences c
			WHERE c.conference_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		$query_Charges = "SELECT charge_id
						FROM ludus_conference_charges
						WHERE conference_id = '$idRegistro'
						ORDER BY charge_id ";
		$Rows_Charges = $DataBase_Class->SQL_SelectMultipleRows($query_Charges);
		$Rows_config['charges'][] = $Rows_Charges;
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatoGaleria($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_media c
			WHERE c.media_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaMed(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_media m
					WHERE m.status_id = 1 AND m.type = 1
					ORDER BY m.media ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaGaleria($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*
			FROM ludus_argument a, ludus_media_detail m
			WHERE a.argument_id = '$idRegistro' AND a.media_id = m.media_id ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaReply($idRegistro){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT a.*, m.first_name, m.last_name, m.image, m.status_id as status_usr
			FROM ludus_conferences_reply a, ludus_users m
			WHERE a.conference_id = '$idRegistro' AND a.user_id = m.user_id
			ORDER BY a.date_creation ASC";//LIMIT 0,20
		}else{
			$query_sql = "SELECT a.*, m.first_name, m.last_name, m.image, m.status_id as status_usr
			FROM ludus_conferences_reply a, ludus_users m
			WHERE a.conference_id = '$idRegistro' AND a.user_id = m.user_id AND (a.status_id = 1 OR (a.status_id = 0 AND a.user_id = $idQuien) )
			ORDER BY a.date_creation ASC";//LIMIT 0,20
		}

		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		/*$query_Likes = "SELECT cu.*, u.first_name, u.last_name
						FROM ludus_conferences_reply_likes cu, ludus_users u
						WHERE cu.user_id = u.user_id
						ORDER BY cu.date_edition ASC";
		$Rows_Likes = $DataBase_Class->SQL_SelectMultipleRows($query_Likes);

		for($i=0;$i<count($Rows_config);$i++) {
			$conference_reply_id = $Rows_config[$i]['conference_reply_id'];
			//Cargos
				for($y=0;$y<count($Rows_Likes);$y++) {
					if( $conference_reply_id == $Rows_Likes[$y]['conference_reply_id'] ){
						$Rows_config[$i]['likes_data'][] = $Rows_Likes[$y];
					}
				}
			//Cargos
		}*/
		unset($DataBase_Class);
		return $Rows_config;
	}
	function CrearReply($conference_id,$replaycomment){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_conferences_reply (conference_id,user_id,date_creation,conference_reply,status_id,likes) VALUES ('$conference_id','$idQuien','$NOW_data','$replaycomment',1,0)";
		//echo $insertSQL_ER;
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE ludus_conferences
						SET num_posts = num_posts+1, last_post_user_id = '$idQuien'
						WHERE conference_id = '$conference_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}
	function ConsultaMensajes($conference_id,$MaxIdReply){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*, u.first_name, u.last_name
			FROM ludus_conferences_reply c, ludus_users u
			WHERE c.conference_id = '$conference_id' AND c.conference_reply_id > $MaxIdReply AND c.user_id = u.user_id ";//LIMIT 0,20
			//echo($query_sql);
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function StatusPost($conference_reply_id,$status_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_conferences_reply
					SET status_id = $status_id
					WHERE conference_reply_id = '$conference_reply_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	public static function BorraCargos($conference_id, $prof="../"){
		include_once($prof.'../config/init_db.php');
		DB::delete('ludus_conference_charges', "conference_id = $conference_id" );
		return DB::affectedRows();
	}
	public static function CrearCargos($conference_id,$charge_id, $prof="../"){
		include_once($prof.'../config/init_db.php');
		DB::insert('ludus_conference_charges',[
			"conference_id" => $conference_id,
			"charge_id" => $charge_id
		]);
		return DB::insertId();
	}
	public static function getCargos( $prof="../" ){
		include_once($prof.'../config/init_db.php');
		$query = "SELECT c.charge_id, c.charge FROM ludus_charges c WHERE c.status_id = 1";
		$cargos = DB::query( $query );
		return $cargos;
	}

	public static function buscar_moderador( $p ){
		$buscar = $p['buscar'];
		include_once( '../../config/init_db.php' );
		$query = "SELECT user_id, first_name, last_name
											FROM ludus_users
												where concat(first_name,' ',last_name) like '%$buscar%' or first_name like '%$buscar%' or  last_name like '%$buscar%' or identification like '%$buscar%'
												and status_id = 1 order by first_name asc limit 10 ";
		$cargos = DB::query( $query );
		$datos = array();
		foreach ($cargos as $key => $value) {
			$datos[$key]['first_name'] = $value['first_name'].' '.$value['last_name'];
			$datos[$key]['user_id'] = $value['user_id'];

		}
		return $datos;
	}

	public static function buscar_cargos_conferencias( $p ){
		include_once( '../../config/init_db.php' );
		$query_cc = "SELECT cc.conference_id, ch.charge_id,  ch.charge FROM ludus_conference_charges cc
								inner join   ludus_charges ch
										on cc.charge_id = ch.charge_id
                                        where cc.conference_id = {$p['id_broadcast']};";
		$cargos_cc = DB::query( $query_cc );

		$query = "SELECT * FROM ludus_charges where status_id = 1;";
		$cargos = DB::query( $query );
		$datos = array();
		foreach ($cargos as $key => $value) {
			$charge_id = $value['charge_id'];
			$datos[$key] = $value;
			$selected = '';
			foreach ($cargos_cc as $key2 => $value2) {
				$datos[$key]['selected'] = $selected;
				if ( $charge_id == $value2['charge_id']  ) {
					$selected = 'selected';
					$datos[$key]['selected'] = $selected;
					break;
				}

			}

		}
		return $datos;
	}

	//Finalizar trasmision
	public static function finalizar_trasmision( $p ){
		include_once( '../../config/init_db.php' );
		$query = "UPDATE ludus_conferences SET status_id = '9' WHERE conference_id = {$p['conference_id']};";

		$result = DB::query( $query );

		if( $result ){

			$response['error'] = false;
			$response['msj']   = 'Trasmisión finalizada';
		}else{
			$response['error'] = true;
			$response['msj']   = 'NO se pudo finalizar la trasmisión';
		}
		return $response;
	}

}
