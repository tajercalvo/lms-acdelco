<?php
Class Cargos {
function consultaDatoscargos($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		include_once('../../config/init_db.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita, z.course, z.newcode as newcodec
			FROM ludus_modules c, ludus_users s, ludus_status e, ludus_courses z
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.module LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.type LIKE '%$sWhere%' OR c.duration_time LIKE '%$sWhere%' OR c.minimum_obtain LIKE '%$sWhere%' OR z.newcode LIKE '%$sWhere%' ) ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
			$Rows_config = DB::query($query_sql);
		// $DataBase_Acciones = new Database();
		// $Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		// unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita, z.course, z.newcode as newcodec
			FROM ludus_modules c, ludus_users s, ludus_status e, ludus_courses z
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.module LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR c.newcode LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' OR c.type LIKE '%$sWhere%' OR c.duration_time LIKE '%$sWhere%' OR c.minimum_obtain LIKE '%$sWhere%') ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita, z.course
			FROM ludus_modules c, ludus_users s, ludus_status e, ludus_courses z
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id 
			ORDER BY c.module ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearcargos($module,$code,$course_id,$minimum_obtain,$type,$duration_time){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		
		$insertSQL_EE = "INSERT INTO ludus_modules (module,status_id,creator,editor,date_creation,date_edition,code,course_id,minimum_obtain,type,duration_time,priority,minimum_type,newcode) VALUES ('$module','1','$idQuien','$idQuien','$NOW_data','$NOW_data','','$course_id','$minimum_obtain','$type','$duration_time',1,1,'')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		
		if($resultado>9999){
			$newcode = $type.$resultado;
		}elseif($resultado>999){
			$newcode = $type.'0'.$resultado;
		}else{
			$newcode = $type.'00'.$resultado;
		}
		$updateSQL_ER = "UPDATE ludus_modules SET code='$newcode', newcode='$newcode' WHERE module_id='$resultado' ";
		$resultado_update = $DataBase_Log->SQL_Update($updateSQL_ER);

		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarcargos($id,$module,$status,$code,$course_id,$minimum_obtain,$type,$duration_time){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO erum_logacceso (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_modulos.php?opcn=ver&id=$id&back=inicio>$id</a>] a cargos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE ludus_modules SET module = '$module', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data',course_id = '$course_id',minimum_obtain = '$minimum_obtain',type = '$type',duration_time = '$duration_time' WHERE module_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_modules c
			WHERE c.module_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.course_id, c.course, c.newcode
						FROM ludus_courses c
						ORDER BY c.course_id DESC";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
}
