<?php
Class evaluarlos {
	function consultaEvaluacion($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT r.*, s.cantidad
					FROM ludus_reviews r, (SELECT review_id, count(*) as cantidad FROM ludus_reviews_questions WHERE status_id = 1 GROUP BY review_id) s
					WHERE r.review_id = '$idRegistro' AND r.review_id = s.review_id ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaRespuesta($answer_id){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_answers
					WHERE answer_id = '$answer_id' ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaResultadoYa($reviews_answer_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT *
					FROM ludus_reviews_answer
					WHERE reviews_answer_id = '$reviews_answer_id' AND user_id = '$idQuien' ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaEvaluacionYa($idRegistro){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT r.*
					FROM ludus_reviews_answer r
					WHERE r.review_id = '$idRegistro' AND r.user_id = '$idQuien' AND r.module_result_usr_id NOT IN (SELECT r.module_result_usr_id FROM ludus_modules_results_usr m, ludus_reviews_answer r WHERE m.user_id = '$idQuien' AND m.user_id = r.user_id AND r.review_id = '$idRegistro' AND r.module_result_usr_id = m.module_result_usr_id)";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearRespuesta($reviews_answer_id,$question_id,$answer_id,$result,$tiempo,$review_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		date_default_timezone_set('America/Bogota');
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();

		$insertSQL_EE = "INSERT INTO ludus_reviews_answer_detail (reviews_answer_id,question_id,answer_id,result,date_creation) VALUES
		('$reviews_answer_id','$question_id','$answer_id','$result','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		if($resultado>0){
			if($result=="SI"){
				$Select_PuntajePregunta = "SELECT value FROM ludus_reviews_questions WHERE review_id = $review_id AND question_id = $question_id";
				$row_consulta = $DataBase_Log->SQL_SelectRows($Select_PuntajePregunta);
				$ValorRespuesta = $row_consulta['value'];
				$time = explode(":", $tiempo);
			    $suma = intval( $time[0] ) * 60 + intval( $time[1] );
				$updateSQL_ER = "UPDATE ludus_reviews_answer SET score = score+1, time_response = '$tiempo', time_seconds = $suma WHERE reviews_answer_id = '$reviews_answer_id'";
				$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);

				if(isset($_SESSION['_EvalCour_ResultId'])){
					//echo("::".$_SESSION['_EvalCour_ResultId']."::");
					$module_result_usr_id = $_SESSION['_EvalCour_ResultId'];
					//cierra por parte del usuario la evaluacion sin tiempo, si la tiene activada
					$evNoTime = evaluarlos::cerrarEvNoTime( $idQuien, $module_result_usr_id );
					$updateScore = "UPDATE ludus_modules_results_usr SET score=score+$ValorRespuesta, score_evaluation=score_evaluation+$ValorRespuesta, date_creation=NOW(), editor_ev=$idQuien WHERE module_result_usr_id = '$module_result_usr_id' ";
					$resultado_US = $DataBase_Log->SQL_Update($updateScore);
					$updateAppAll = "UPDATE ludus_modules_results_usr SET approval = 'SI' WHERE score>79";
					$resultado_AP = $DataBase_Log->SQL_Update($updateAppAll);
				}
			}else{
				$time = explode(":", $tiempo);
			    $suma = intval( $time[0] ) * 60 + intval( $time[1] );
				$updateSQL_ER = "UPDATE ludus_reviews_answer SET time_response = '$tiempo', time_seconds = $suma WHERE reviews_answer_id = '$reviews_answer_id'";
				$resultado_up = $DataBase_Log->SQL_Update($updateSQL_ER);
			}
		}
		unset($DataBase_Log);
		return $resultado;
	}
	function CrearRespEvaluacion($review_id, $module_result_usr_id ){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".date("H").":".date("i:s");

		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_reviews_answer (review_id,user_id,available_score,score,date_creation,time_response, time_seconds,module_result_usr_id) VALUES
		('$review_id','$idQuien','100',0,'$NOW_data','00:00', 0,'$module_result_usr_id')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);

		$query_agrupaciones = "SELECT obligatorias, agrup FROM ludus_reviews_dist WHERE review_id = '$review_id' ";
		$Rows_agrup = $DataBase_Log->SQL_SelectMultipleRows($query_agrupaciones);
		$Rows_config = array();

		foreach ($Rows_agrup as $key => $v) {
			$QueryConsultaPreguntas = "SELECT rq.agrup, rq.question_id, rq.value, q.question, q.source
							FROM ludus_reviews_questions rq, ludus_questions q
							WHERE rq.review_id = $review_id AND rq.status_id = '1' AND
							rq.question_id = q.question_id AND rq.agrup = '{$v['agrup']}'
							ORDER BY rand() LIMIT 0, {$v['obligatorias']}";
			// echo $QueryConsultaPreguntas."<br>";
			$Rows_listos = $DataBase_Log->SQL_SelectMultipleRows($QueryConsultaPreguntas);
			foreach ($Rows_listos as $key => $value) {
				$Rows_config[] = $value;
			}
		}//foreach

		$query_answers = "SELECT a.*
							FROM ludus_reviews_questions rq, ludus_questions q, ludus_answers a
							WHERE rq.review_id = '$review_id' AND rq.status_id = '1' AND
							rq.question_id = q.question_id AND q.question_id = a.question_id
							ORDER BY q.question_id, rand()";
		$Rows_Answers = $DataBase_Log->SQL_SelectMultipleRows($query_answers);

		for($i=0;$i<count($Rows_config);$i++) {
			$question_id = $Rows_config[$i]['question_id'];
			//Cargos
				for($y=0;$y<count($Rows_Answers);$y++) {
					if( $question_id == $Rows_Answers[$y]['question_id'] ){
						$Rows_config[$i]['AnswersData'][] = $Rows_Answers[$y];
					}
				}
			//Cargos
		}
		$_SESSION['EvaluacionCompleta'] = $Rows_config;
		//print_r($Rows_config);
		unset($DataBase_Log);
		return $resultado;
	}
	function RecordEvaluacion($review_id,$reviews_answer_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$idQuien = $_SESSION['idUsuario'];
		$query_res = "SELECT a.reviews_answer_id, ad.result, q.question, q.code, ans.answer, ans.result as res_ans, rq.value as valor
					FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans, ludus_reviews_questions rq
					WHERE a.review_id = '$review_id' AND a.user_id = '$idQuien' AND a.reviews_answer_id = ad.reviews_answer_id
					AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id AND rq.review_id = '$review_id' AND ad.question_id = rq.question_id AND ad.reviews_answer_id = '$reviews_answer_id' AND ad.reviews_answer_id = a.reviews_answer_id ";
		$Rows_res = $DataBase_Log->SQL_SelectMultipleRows($query_res);
		unset($DataBase_Log);
		return $Rows_res;
	}
	//=========================================================================
	// Consulta las evaluaciones que tenga pendientes por realizar sin limite de tiempo para desplegar
	//=========================================================================
	public static function evaluacionesNoTime( $user_id, $prof="../" ){
		include_once($prof.'../config/init_db.php');
		$query_pendientes = "SELECT * FROM ludus_reviews_notime WHERE user_id = %i AND status_id = 1";
		$resultSet = DB::queryFirstRow( $query_pendientes, $user_id );
		return $resultSet;
	}
	//=========================================================================
	// Activa la opcion para realizar una evaluacion despues del tiempo establecido
	//=========================================================================
	public static function activarEvNoTime( $p, $prof="../" ){
		include_once($prof.'../config/init_db.php');
		$query_pendientes = "SELECT * FROM ludus_reviews_notime WHERE user_id = %i AND status_id = 1";
		$result = DB::queryFirstRow( $query_pendientes, $p['user_id'] );

		if(  isset( $result['review_notime_id'] ) ){
			$resultSet = -1;
		}else{
			DB::insert( 'ludus_reviews_notime',[
					"user_id" => $p['user_id'],
					"module_result_usr_id" => 0,
					"creator_id" => $p['creator_id'],
					"date_creation" => DB::sqleval( "NOW()" ),
					"status_id" => 1
				] );
			$resultSet = DB::insertId();
		}

		return $resultSet;
	}//fin function activarEvNoTime
	//=========================================================================
	// Activa la opcion para realizar una evaluacion despues del tiempo establecido
	//=========================================================================
	public static function inactivarEvNoTime( $p, $prof="../" ){
		include_once($prof.'../config/init_db.php');
		DB::update( 'ludus_reviews_notime', [
				"status_id" => 2,
				"editor_id" => $p['editor_id'],
				"date_edition" => DB::sqleval( "NOW()" )
			], 'review_notime_id = %i', $p['review_notime_id'] );
		$resultSet = DB::affectedRows();
		return $resultSet;
	}//fin function inactivarEvNoTime
	//=========================================================================
	// Cierra la opcion de evaluacion luego de responder
	//=========================================================================
	public static function cerrarEvNoTime( $user_id, $module_result_usr_id, $prof="../" ){
		include_once($prof.'../config/init_db.php');
		$resultSet = 0;
		$query = DB::queryFirstRow( "SELECT * FROM ludus_reviews_notime WHERE user_id = %i AND status_id = 1", $user_id );

		if( isset( $query['review_notime_id'] ) ){
			DB::update( 'ludus_reviews_notime', [
					"status_id" => 2,
					"editor_id" => $user_id,
					"module_result_usr_id" => $module_result_usr_id,
					"date_edition" => DB::sqleval( "NOW()" )
				], 'review_notime_id = %i', $query['review_notime_id'] );
			$resultSet = DB::affectedRows();
		}
		return $resultSet;
	}//fin function inactivarEvNoTime


	public function actualizarNota( $activity_schedule_id, $reviews_answer_id ){
		// include_once('../../config/database.php');
		// include_once('../../config/config.php');
		include_once('../../config/init_db.php');
		//$resultSet = DB::queryFirstRow( $query_pendientes, $user_id );
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$DataBase_Class = new Database();

		$query_por = "SELECT porcentaje, sch.teacher_id FROM ludus_modules_activities moac
								inner join ludus_activities_schedule aces
									on moac.activity_id = aces.activity_id
								inner join ludus_schedule sch
									on sch.schedule_id = aces.schedule_id
								where activity_schedule_id = $activity_schedule_id";
		$actividad = DB::queryFirstRow( $query_por );
		//$actividad = $DataBase_Class->SQL_SelectRows($query_por);

		$query_eval = "SELECT module_result_usr_id, review_id from ludus_reviews_answer where reviews_answer_id = $reviews_answer_id;";
		$evaluacion = DB::query( $query_eval );
		//$evaluacion = $DataBase_Class->SQL_SelectMultipleRows($query_eval);

		$PorTotal = count($evaluacion);


		$review_id = $evaluacion[0]['review_id'];		
		$res_evaluacion = $this->RecordEvaluacion($review_id,$reviews_answer_id);

		$score = 0;
		foreach ($res_evaluacion as $key => $value) {
			if ($value['result'] == "SI") {
				$score = $score + $value['valor'];
			}
		}
	
		$module_result_usr_id = $evaluacion[0]['module_result_usr_id'];

		$teacher_id = $actividad['teacher_id'];
		$query_ins  = "INSERT INTO ludus_activities_user
							(
							user_id,
							activity_schedule_id,
							resultado,
							comentarios,
							archivo,
							date_edition,
							reviews_answer_id,
							module_result_usr_id,
							teacher_id,
							coments_teacher,
							date_teacher)
							VALUES
							(
							$idQuien,
							$activity_schedule_id,
							$score,
							'',
							'',
							'$NOW_data',
							$reviews_answer_id,
							$module_result_usr_id,
							$teacher_id,
							'',
							'$NOW_data');";
		$Rows_config = DB::query( $query_ins );
		//$Rows_config = $DataBase_Class->SQL_Update($query_ins);

		/*16 noviembre 2020 Juan Carlos Villar, Nuevo metodo de calificación*/
		// Traemos las actividades asociadas a la clase,  schedule_id
		$clase = "SELECT schedule_id from ludus_activities_schedule where activity_schedule_id = $activity_schedule_id;";
		$res_clase = DB::queryFirstRow( $clase );
		//$res_clase = $DataBase_Class->SQL_SelectRows($clase);

		$schedule_id = $res_clase['schedule_id'];
		$query_act_usr = "SELECT act.activity_id, actus.resultado, moac.porcentaje,
										sum( ROUND( (moac.porcentaje * actus.resultado / 100), 0 ) ) puntaje_real
									from ludus_activities_schedule act
										left join ludus_activities_user actus
											on act.activity_schedule_id = actus.activity_schedule_id
												and actus.user_id = $idQuien
										inner join ludus_modules_activities moac
											on act.activity_id = moac.activity_id
									where act.schedule_id = $schedule_id;";
		$res_final = DB::queryFirstRow( $query_act_usr );
		//$res_final = $DataBase_Class->SQL_SelectRows($query_act_usr);
		
		$score_final = $res_final['puntaje_real'];
		$update_nota = "UPDATE ludus_modules_results_usr set score = $score_final where module_result_usr_id = $module_result_usr_id;";
		$Rows_config = DB::query( $update_nota );
		//$Rows_config = $DataBase_Class->SQL_Update($update_nota );

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function getActividad($activity_schedule_id){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];

		$query = "SELECT more.* from ludus_activities_schedule act
						inner join ludus_invitation inv
							on inv.schedule_id = act.schedule_id
						inner join ludus_modules_results_usr more
							on more.invitation_id = inv.invitation_id
					where act.activity_schedule_id = $activity_schedule_id and more.user_id = $idQuien;";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	
}
