<?php
Class RepAsistencias {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT DISTINCT c.course, c.course_id, s.start_date, s.schedule_id, i.city
						FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_cities i
						WHERE c.status_id = 1 AND c.type NOT IN ('WBT') AND c.course_id = s.course_id
						AND s.schedule_id IN (SELECT schedule_id FROM `ludus_invitation`)
						AND s.living_id = l.living_id AND l.city_id = i.city_id AND s.status_id = 1 
						ORDER BY s.start_date DESC, c.course";
		}else{
			$query_sql = "SELECT DISTINCT c.course, c.course_id, s.start_date, s.schedule_id, i.city
						FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_cities i
						WHERE c.status_id = 1 AND c.type NOT IN ('WBT') AND c.course_id = s.course_id
						AND s.schedule_id IN (SELECT i.schedule_id
										FROM ludus_users u, ludus_headquarters h, ludus_invitation i
										WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $dealer_id AND u.user_id = i.user_id)
						AND s.living_id = l.living_id AND l.city_id = i.city_id AND s.status_id = 1 
						ORDER BY s.start_date DESC, c.course";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad($schedule_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, u1.identification
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h
			WHERE s.schedule_id = '$schedule_id' AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id
			ORDER BY c.course, s.start_date ";//LIMIT 0,20
		}else{
			$query_sql = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, u1.identification
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h
			WHERE s.schedule_id = '$schedule_id' AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id AND h.dealer_id = '$dealer_id' 
			ORDER BY c.course, s.start_date ";//LIMIT 0,20
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatosAll($schedule_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, i.invitation_id, u1.identification
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h
			WHERE s.schedule_id = '$schedule_id' AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id
			ORDER BY c.course, s.start_date ";//LIMIT 0,20
		}else{
			$query_sql = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, i.invitation_id, u1.identification
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h
			WHERE s.schedule_id = '$schedule_id' AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id AND h.dealer_id = '$dealer_id' 
			ORDER BY c.course, s.start_date ";//LIMIT 0,20
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		for($i=0;$i<count($Rows_config);$i++) {
			$query_Resultado = "SELECT i.approval, i.score
						FROM `ludus_modules_results_usr` i
						WHERE i.invitation_id = ".$Rows_config[$i]['invitation_id']." AND i.status_id = 1 ";
			$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
			$Rows_config[$i]['resultados'] = $Rows_cant;
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
