<?php

class Dashboard{
    //===================================================================
    // Retorna el listado de los concecionarios activos
    //===================================================================
    public static function getConcesionarios( $zonas , $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $zona = $zonas == "" ? "" : "AND z.zone_id IN ( $zonas ) ";
        $query_sql = "SELECT DISTINCT d.dealer_id, d.dealer
            FROM ludus_dealers d, ludus_headquarters h, ludus_areas a, ludus_zone z
            WHERE d.dealer_id = h.dealer_id
            AND d.status_id = 1
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            AND h.status_id = 1
            AND d.category = 'CHEVROLET'
            $zona ";
            // echo($query_sql);
		$Rows_config = DB::query($query_sql);
        return $Rows_config;
    }//fin funcion getConcesionarios

    //===================================================================
    // Retorna el listado de zonas activas
    //===================================================================
    public static function getZonas($prof = "" ){
        include_once($prof.'../config/init_db.php');
        $query_sql = "SELECT z.zone_id, z.zone FROM ludus_zone z WHERE z.status_id = 1";
		$Rows_config = DB::query($query_sql);
        return $Rows_config;
    }//fin funcion getZonas

    //===================================================================
    // Retorna el listado de trayectorias con el numero de usuarios correspondientes
    //===================================================================
    public static function getTrayectorias( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $filtro = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }else if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }

        $query_sql = "SELECT d.dealer_id, c.category_id, cc.category, cu.charge_id, c.charge, c.group_id, COUNT( cu.user_id ) AS usuarios
            FROM ludus_charges_users cu, ludus_users u, ludus_charges c, ludus_headquarters h, ludus_dealers d, ludus_charges_categories cc
            WHERE cu.user_id = u.user_id
            AND cu.charge_id = c.charge_id
            AND c.category_id = cc.category_id
            AND u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            $filtro
            AND c.status_id = 1
            AND c.group_id = 'CHEVROLET'
            AND d.category = 'CHEVROLET'
            AND u.status_id = 1
            GROUP BY cc.category, cu.charge_id
            ORDER BY c.category_id DESC";

        $Rows_config = DB::query( $query_sql );

        return $Rows_config;
    }//

    //===================================================================
    // Categorias de los cargos
    //===================================================================
    public static function listaCategorias( $prof = "../" ){
        include_once($prof.'../config/init_db.php');

        $query_sql = "SELECT * FROM ludus_charges_categories";

        $Rows_config = DB::query( $query_sql );

        return $Rows_config;
    }//

    //===================================================================
    // Retorna el numero de usuarios registrados en la plataforma segun el filtro
    //===================================================================
    public static function getTotalUsuarios( $p, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $filtro = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }else if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }

        $query_sql = "SELECT COUNT( u.user_id ) AS usuarios
            FROM ludus_charges c, ludus_charges_users cu, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
            WHERE c.charge_id = cu.charge_id
            AND cu.status_id = 1
            AND cu.user_id = u.user_id
            AND u.status_id = 1
            AND c.status_id = 1
            AND u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            AND d.category = 'CHEVROLET'
            $filtro";
        $Rows_config = DB::queryFirstRow( $query_sql );
        return $Rows_config['usuarios'];
    }//fin function getTotalUsuarios

    //===================================================================
    // Retorna la sabana de datos de los cupos por concesionarios y sesiones
    //===================================================================
    public static function getCuposTotales( $p, $tiempo, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');
        // $anio = 2017;
        $filtro = "";
        $fecha = "";
        if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }
        if( $tiempo == "mensual" ){
            $fecha = " AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        }
        $query_sql = "SELECT ds.dealer_id, d.dealer, ds.schedule_id, c.specialty_id, sp.specialty, ds.max_size, ds.inscriptions, MONTH(s.start_date) AS mes, 0 AS promedio
            FROM ludus_dealer_schedule ds, ludus_schedule s, ludus_dealers d, ludus_courses c, ludus_specialties sp
            WHERE ds.schedule_id = s.schedule_id
            AND s.course_id = c.course_id
            AND c.specialty_id = sp.specialty_id
            AND ds.dealer_id = d.dealer_id
            AND s.status_id = 1
            AND ds.max_size > 0
            $fecha
            AND d.category = 'CHEVROLET'
            $filtro
            AND YEAR( s.start_date ) = $anio";
            // echo( $query_sql );
		$Rows_config = DB::query($query_sql);
        return $Rows_config;
    }//fin funcion getCuposTotales

    //===================================================================
    // Retorna la sabana de datos de las notas por concesionarios y sesiones
    //===================================================================
    public static function getNotas( $p, $tiempo, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');
        // $anio = 2017;
        $filtro = "";
        $fecha = "";
        if( $p['concesionarios'] != "" ){
            $filtro = " AND vn.dealer_id IN ( {$p['concesionarios']} ) ";
        }
        if( $tiempo == "mensual" ){
            $fecha = " AND MONTH( vn.start_date ) IN ( {$p['fecha']} )";
        }
        $query_sql = "SELECT vn.dealer_id, vn.user_id, vn.schedule_id, vn.specialty_id, vn.specialty, vn.type, MONTH( vn.start_date ) AS mes, vn.score
            FROM view_notes vn, ludus_schedule s
            WHERE  vn.schedule_id = s.schedule_id
            AND s.status_id = 1
            $fecha
            AND YEAR( vn.start_date ) = $anio
            $filtro";
            // echo( $query_sql );
		$Rows_config = DB::query($query_sql);
        return $Rows_config;
    }//fin funcion getNotas

    //===================================================================
    // Retorna la sabana de datos de las excusas por concesioanrios y sesiones
    //===================================================================
    public static function getExcusas( $p, $tiempo, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');
        // $anio = 2017;
        $filtro = "";
        $filtro_ext = "";
        $fecha = "";
        if( $p['concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['concesionarios']} ) ";
            $filtro_ext = " AND ed.dealer_id IN ( {$p['concesionarios']} ) ";
        }
        if( $tiempo == "mensual" ){
            $fecha = " AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        }
        $query_sql = "SELECT s.schedule_id, h.dealer_id, MONTH(s.start_date) AS mes, COUNT( excuse_id ) AS excusas
                FROM ludus_excuses ex, ludus_schedule s, ludus_users u, ludus_headquarters h
                WHERE ex.schedule_id = s.schedule_id
                AND ex.user_id = u.user_id
                AND u.headquarter_id = h.headquarter_id
                AND s.status_id = 1
                AND ex.status_id = 1
                $filtro
                AND YEAR( s.start_date ) = $anio
                $fecha
                GROUP BY s.schedule_id, h.dealer_id, mes";
            // echo( $query_sql );
		$Rows_config = DB::query($query_sql);
        $num_excusas = count( $Rows_config );
        // ========================================================
        // excusas de cupos sin asignar
        // ========================================================
        $query_sqlExc = "SELECT ed.*
            FROM ludus_excuses_dealers ed, ludus_schedule s
            WHERE ed.schedule_id = s.schedule_id
            AND ed.status_id = 1
            $filtro_ext
            AND YEAR( s.start_date ) = $anio
            $fecha";
        $excusas_cupos = DB::query( $query_sqlExc );

        // ========================================================
        // Cruza las excusas a invitados con las excusas a cupos
        // ========================================================
        for ($i=0; $i < $num_excusas; $i++) {
            $Rows_config[$i]['excusas'] = intval( $Rows_config[$i]['excusas'] );
            foreach ( $excusas_cupos as $key => $e ) {
                if ( $e['dealer_id'] == $Rows_config[$i]['dealer_id'] && $e['schedule_id'] == $Rows_config[$i]['schedule_id'] ) {
                    $Rows_config[$i]['excusas'] += intval( $Rows_config[$i]['num_invitation'] );
                }
            }//fin foreach
        }//fin for

        return $Rows_config;
    }//fin funcion getExcusas

    //===================================================================
    // Retorna la sabana de datos de las invitaciones por concesionarios y sesiones
    //===================================================================
    public static function getInvitaciones( $p, $tiempo, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');
        // $anio = 2017;
        $filtro = "";
        $fecha = "";
        if( $p['concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['concesionarios']} ) ";
        }
        if( $tiempo == "mensual" ){
            $fecha = " AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        }
        $query_sql = "SELECT h.dealer_id, s.schedule_id, COUNT(i.invitation_id) AS invitaciones
            FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_users u, ludus_headquarters h
            WHERE i.schedule_id = s.schedule_id
            AND i.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND s.course_id = c.course_id
            $filtro
            AND YEAR( s.start_date ) = $anio
            $fecha
            GROUP BY h.dealer_id, s.schedule_id";
            // echo( $query_sql );
		$Rows_config = DB::query($query_sql);
        return $Rows_config;
    }//fin funcion getInvitaciones

    //===================================================================
    // Retorna la sabana de datos de las inscripciones realizadas por concesionarios y sesiones
    //===================================================================
    public static function getInscripciones( $p, $tiempo, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');
        // $anio = 2017;
        $filtro = "";
        $fecha = "";
        if( $p['concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['concesionarios']} ) ";
        }
        if( $tiempo == "mensual" ){
            $fecha = " AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        }
        $query_sql = "SELECT h.dealer_id, s.schedule_id, i.user_id, i.status_id, m.duration_time, c.specialty_id
            FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_users u, ludus_headquarters h, ludus_modules m
            WHERE i.schedule_id = s.schedule_id
            AND i.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND s.course_id = c.course_id
            AND c.course_id = m.course_id
            $filtro
            AND YEAR( s.start_date ) = $anio
            $fecha";
            // echo( $query_sql );
		$Rows_config = DB::query($query_sql);
        return $Rows_config;
    }//fin funcion getInscripciones

    //===================================================================
    // Retorna los datos para el funnel de participacion
    //===================================================================
    public static function generarSabanaDatos( $cupos, $invitados, $inscritos, $notas, $excusas ){
        $cuenta = count( $cupos );
        for ( $i=0 ; $i < $cuenta ; $i++ ) {
            $cupos[$i]['invitados'] = 0;
            $cupos[$i]['inscritos'] = 0;
            $cupos[$i]['aprobo'] = 0;
            $cupos[$i]['reprobo'] = 0;
            $cupos[$i]['excusas'] = 0;
            //recorre las invitaciones para agruparlas frente a sus cupos respectivos
            foreach ( $invitados as $key => $iv ) {
                if( $iv['dealer_id'] == $cupos[$i]['dealer_id'] && $iv['schedule_id'] == $cupos[$i]['schedule_id'] ){
                    $cupos[$i]['invitados'] += $iv['invitaciones'];
                }
            }//fin foreach invitados

            //recorre las inscripciones para agruparlas frente a sus cupos respectivos
            foreach ( $inscritos as $key2 => $is ){
                if( $is['dealer_id'] == $cupos[$i]['dealer_id'] && $is['schedule_id'] == $cupos[$i]['schedule_id'] ){
                    if( $is['status_id'] == 2 ){
                        $cupos[$i]['inscritos'] ++;
                    }
                }
            } //fin foreach inscritos

            //recorre las notas para agruparlas a sus concesionarios y sesiones correspondientes
            foreach ( $notas as $key2 => $n ){
                if( $n['dealer_id'] == $cupos[$i]['dealer_id'] && $n['schedule_id'] == $cupos[$i]['schedule_id'] ){
                    $score = intval( $n['score'] );
                    $cupos[$i]['promedio'] += $score;
                    if( $score >= 80 ){
                        $cupos[$i]['aprobo'] ++;
                    }else{
                        $cupos[$i]['reprobo'] ++;
                    }
                }
            }//fin foreach notas

            //recorre las excusas
            foreach ( $excusas as $key2 => $e ){
                if( $e['dealer_id'] == $cupos[$i]['dealer_id'] && $e['schedule_id'] == $cupos[$i]['schedule_id'] ){
                    $cupos[$i]['excusas'] = intval( $e['excusas'] );
                    // $cupos[$i]['max_size'] = intval($cupos[$i]['max_size']) - intval( $e['excusas'] );
                    // $cupos[$i]['invitados'] = intval($cupos[$i]['invitados']) - intval( $e['excusas'] );
                }
            }//fin foreach excusas

        }//fin for cupos

        return $cupos;
    }//fin getFunnelParticipacion

    //===================================================================
    // Obtiene el promedio de notas del año, segmentado por mes
    //===================================================================
    public static function getQuarters( $invitaciones ){
        $cuenta_inv = count( $invitaciones );
        $resultSet = [
            [ "mes" =>1, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],[ "mes" =>2, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0],[ "mes" =>3, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],
            [ "mes" =>4, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],[ "mes" =>5, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0],[ "mes" =>6, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],
            [ "mes" =>7, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],[ "mes" =>8, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0],[ "mes" =>9, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],
            [ "mes" =>10, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],[ "mes" =>11, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0],[ "mes" =>12, "notas"=>0, "cupos"=>0, "inscritos"=>0, "excusas"=>0 ],
        ];

        for ($j=0; $j < 12; $j++) {
            $promedio = 0;
            $cupos = 0;
            $inscritos = 0;
            $excusas = 0;
            $mes = $j + 1;
            foreach ($invitaciones as $key => $i) {
                if( $i['mes'] == $mes ){
                    $promedio += intval( $i['promedio']);
                    $cupos += intval( $i['max_size']);
                    $inscritos += intval( $i['inscritos']);
                    $excusas += intval( $i['excusas']);
                }
            }
            $resultSet[$j]['notas'] = $promedio;
            $resultSet[$j]['cupos'] = $cupos;
            $resultSet[$j]['inscritos'] = $inscritos;
            $resultSet[$j]['excusas'] = $excusas;
        }

        return $resultSet;

	} //fin getQuarters

    //===================================================================
    // Obtiene el ranking de los estudiantes por promedio
    //===================================================================
    public static function getRankingAlumnos( $p, $tiempo, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date( "Y" );
        $filtro = "";
        $fecha = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }elseif( $p['concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['concesionarios']} ) ";
        }
        if( $tiempo == "mensual" ){
            $fecha = " AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        }

        $query_sql = "SELECT mru.user_id, u.first_name, u.last_name, d.dealer, AVG( mru.score ) AS promedio, AVG( ra.time_seconds ) AS tiempo, COUNT( mru.module_result_usr_id ) AS cursos
            FROM ludus_modules_results_usr mru, ludus_reviews_answer ra, ludus_invitation i, ludus_schedule s, ludus_users u, ludus_headquarters h, ludus_dealers d
            WHERE ra.module_result_usr_id = mru.module_result_usr_id
            AND mru.invitation_id = i.invitation_id
            AND i.schedule_id = s.schedule_id
            AND mru.user_id = u.user_id
            AND i.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            $filtro
            AND YEAR( s.start_date ) = $anio
            $fecha
            GROUP BY mru.user_id
            ORDER BY promedio DESC, tiempo ASC
            LIMIT 10";
        $Rows_config = DB::query($query_sql);

        return $Rows_config;
    }//fin getRankingAlumnos

    //===================================================================
    // Obtiene el ranking de los estudiantes por cantidad de cursos aprobados
    //===================================================================
    public static function estudiantesCursos( $p, $tiempo, $prof = "../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date( "Y" );
        $filtro = "";
        $fecha = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }elseif( $p['concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['concesionarios']} ) ";
        }
        if( $tiempo == "mensual" ){
            $fecha = " AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        }

        $query_sql = "SELECT mru.user_id, u.first_name, u.last_name, d.dealer, AVG( mru.score ) AS promedio, count( mru.user_id ) AS cursos
            FROM ludus_modules_results_usr mru, ludus_invitation i, ludus_schedule s, ludus_users u, ludus_headquarters h, ludus_dealers d
            WHERE mru.invitation_id = i.invitation_id
            AND i.schedule_id = s.schedule_id
            AND mru.user_id = u.user_id
            AND i.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND YEAR( s.start_date ) = $anio
            $fecha
            AND mru.approval = 'SI'
            $filtro
            GROUP BY mru.user_id
            ORDER BY cursos DESC, promedio DESC
            LIMIT 10";
            // echo( $query_sql );
        $Rows_config = DB::query($query_sql);

        return $Rows_config;
    }//fin estudiantesCursos







    //===================================================================
    // Obtiene la tabla de promedios de los concecionarios para el ranking
    //===================================================================
    public static function getTablaGruposProm( $data, $concesionarios ){
        $ccs =  $concesionarios;
        $cuenta = count( $ccs );
        for ( $i=0; $i < $cuenta ; $i++ ) {
            $ccs[$i]['notas'] = 0;
            $ccs[$i]['cupos'] = 0;
            foreach ( $data as $key => $d ) {
                if( $ccs[$i]['dealer_id'] == $d['dealer_id'] ){
                    $ccs[$i]['notas'] += intval( $d['promedio'] ) ;
                    $ccs[$i]['cupos'] += intval( $d['max_size'] ) ;
                }
            }
        }
        return $ccs;
    } //fin getTablaGruposProm

    //===================================================================
    // Obtiene el promedio de notas de una zona/concesionario en un intervalo de tiempo
    //===================================================================
    public static function getDatosNotas( $data ){
        $pac = 0;
        $notas = 0;
        $cupos = 0;
        foreach ($data as $key => $d) {
            $notas += $d['promedio'];
            $cupos += $d['max_size'];
        }
        $pac = $notas / $cupos;
        return $pac;
    }//fin getDatosNotas

    //===================================================================
    // Obtiene el promedio de notas de una zona/concesionario/sede en un intervalo de tiempo
    //===================================================================
    public static function getCcsPromedio( $data, $concesionarios ){
        $ccs = [];
        $resultSet = [];
        $hab_1 = ["dealer"=>'',"dealer_id"=>0, "notas"=>0, "cupos"=>0, "promedio"=>0, "specialty"=>"","specialty_id"=>1];
        $hab_2 = ["dealer"=>'',"dealer_id"=>0, "notas"=>0, "cupos"=>0, "promedio"=>0, "specialty"=>"","specialty_id"=>2];
        $hab_3 = ["dealer"=>'',"dealer_id"=>0, "notas"=>0, "cupos"=>0, "promedio"=>0, "specialty"=>"","specialty_id"=>3];
        $hab_4 = ["dealer"=>'',"dealer_id"=>0, "notas"=>0, "cupos"=>0, "promedio"=>0, "specialty"=>"","specialty_id"=>4];

        foreach ($concesionarios as $key2 => $c) {
            $hab_1['dealer_id'] = $c[ 'dealer_id' ];
            $hab_2['dealer_id'] = $c[ 'dealer_id' ];
            $hab_3['dealer_id'] = $c[ 'dealer_id' ];
            $hab_4['dealer_id'] = $c[ 'dealer_id' ];
            $ccs[] = $hab_1;
            $ccs[] = $hab_2;
            $ccs[] = $hab_3;
            $ccs[] = $hab_4;
        }//foreach $concesionarios

        $cuenta = count( $ccs );

        for ($i=0; $i < $cuenta ; $i++) {
            foreach ($data as $key => $d) {
                if( $ccs[$i]['dealer_id'] == $d['dealer_id'] && isset( $d['specialty_id'] ) && $ccs[$i]['specialty_id'] == $d['specialty_id'] ){
                    $ccs[$i]['dealer'] = $d['dealer'];
                    $ccs[$i]['specialty'] = $d['specialty'];
                    $ccs[$i]['notas'] += intval($d['promedio']);
                    $ccs[$i]['cupos'] += intval($d['max_size']);
                }
            }//foreach $data
            if( $ccs[$i]['cupos'] > 0 ){
                $ccs[$i]['promedio'] = $ccs[$i]['notas'] / $ccs[$i]['cupos'];
            }
        }

        foreach ($ccs as $key => $cs) {
            if( $cs['promedio'] > 0 ){
                $resultSet[] = $cs;
            }
        }

        return $resultSet;
    }//fin getCcsPromedio

    //===================================================================
    // Retorna el promedio general de los concesionarios
    //===================================================================
    public static function promedioGeneral( $data ){
        $notas = 0;
        $cupos = 0;
        $promedio = 0;
        foreach ($data as $key => $d) {
            $notas += intval( $d['promedio'] );
            $cupos += intval( $d['max_size'] );
        }
        if( $cupos > 0 ){
            $promedio = $notas / $cupos;
        }
        return $promedio;
	} //fin promedioGeneral

    //===================================================================
    // Obtiene el numero de horas por habilidad
    //===================================================================
    public static function getHorasHabilidad( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
        $filtro = "";
        $anio = date( "Y" );
        // $anio = 2017;
        $fecha = $p['fecha'] == "0" ? "" : " AND MONTH(s.start_date) IN ( {$p['fecha']} ) " ;
        if( $p['concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['concesionarios']} ) ";
        }

		$query_sql = "SELECT COUNT(i.user_id) AS cuenta, s.schedule_id, m.duration_time, c.type, c.specialty_id, sp.specialty
            FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_modules m, ludus_specialties sp,
            	ludus_users u, ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
            WHERE i.schedule_id = s.schedule_id
            AND s.course_id = c.course_id
            AND c.course_id = m.course_id
            AND i.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND d.category = 'CHEVROLET'
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            $filtro
            AND c.specialty_id = sp.specialty_id
            AND YEAR(s.start_date) = $anio
            $fecha
            GROUP BY s.schedule_id";
            // echo( $query_sql );
		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}//fin getHorasHabilidad

    //===================================================================
    // Obtiene el numero de horas por habilidad
    //===================================================================
    public static function getCursos( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
        $filtro = "";
        $anio = date( "Y" );
        // $anio = 2017;
        $fecha = $p['fecha'] == "0" ? "" : " AND MONTH(s.start_date) IN ( {$p['fecha']} ) " ;
        if( $p['zonas'] != "" ){
            $filtro = " AND z.zone_id IN ( {$p['zonas']} ) ";
        }elseif( $p['concesionarios'] != "" ){
            $filtro = " AND h.dealer_id IN ( {$p['concesionarios']} ) ";
        }

		$query_sql = "SELECT COUNT(s.schedule_id) AS cursos, c.course_id, c.course, c.type, c.specialty_id, sp.specialty
        FROM ludus_schedule s, ludus_courses c, ludus_specialties sp,
        	( SELECT DISTINCT ds.schedule_id
        		FROM ludus_dealer_schedule ds, ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
        	 	WHERE ds.dealer_id = h.dealer_id
                AND h.dealer_id = d.dealer_id
                AND d.category = 'CHEVROLET'
        		AND h.area_id = a.area_id
        		AND a.zone_id = z.zone_id
        		$filtro ) AS dst
        WHERE s.course_id = c.course_id
        AND c.specialty_id = sp.specialty_id
        AND s.status_id = 1
        AND s.schedule_id = dst.schedule_id
        AND YEAR(s.start_date) = $anio
        $fecha
        GROUP BY c.course_id, c.type, c.specialty_id";
            // echo( $query_sql );
		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	}//fin getCursos

    //===================================================================
    // Obtiene la cantidad de cupos e inscritos por cada concesionario
    //===================================================================
    public static function getCuposInsc( $data, $concesionarios ){
        $ccs = [];
        $resultSet = [];
        $hab_1 = ["dealer"=>'',"dealer_id"=>0, "asistentes"=>0, "cupos"=>0, "specialty_id"=>1];
        $hab_2 = ["dealer"=>'',"dealer_id"=>0, "asistentes"=>0, "cupos"=>0, "specialty_id"=>2];
        $hab_3 = ["dealer"=>'',"dealer_id"=>0, "asistentes"=>0, "cupos"=>0, "specialty_id"=>3];
        $hab_4 = ["dealer"=>'',"dealer_id"=>0, "asistentes"=>0, "cupos"=>0, "specialty_id"=>4];

        foreach ($concesionarios as $key2 => $c) {
            $hab_1['dealer_id'] = $c[ 'dealer_id' ];
            $hab_2['dealer_id'] = $c[ 'dealer_id' ];
            $hab_3['dealer_id'] = $c[ 'dealer_id' ];
            $hab_4['dealer_id'] = $c[ 'dealer_id' ];
            $ccs[] = $hab_1;
            $ccs[] = $hab_2;
            $ccs[] = $hab_3;
            $ccs[] = $hab_4;
        }//foreach $concesionarios

        $cuenta = count( $ccs );

        for ($i=0; $i < $cuenta ; $i++) {
            foreach ($data as $key => $d) {
                if( $ccs[$i]['dealer_id'] == $d['dealer_id'] && isset( $d['specialty_id'] ) && $ccs[$i]['specialty_id'] == $d['specialty_id'] ){
                    $ccs[$i]['dealer'] = $d['dealer'];
                    $ccs[$i]['specialty'] = $d['specialty'];
                    $ccs[$i]['asistentes'] += intval($d['inscriptions']);
                    $ccs[$i]['cupos'] += intval($d['max_size']);
                }
            }//foreach $data
        }

        foreach ($ccs as $key => $cs) {
            if( $cs['cupos'] > 0 ){
                $resultSet[] = $cs;
            }
        }

        return $resultSet;
	}//fin getCuposInsc
    //===================================================================
    // Obtiene el ranking de los concesionarios por su promedio de notas
    //===================================================================
    public static function getRankingCcs( $data, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
        $query_sql = "SELECT DISTINCT h.dealer_id, z.zone_id
            FROM ludus_headquarters h, ludus_areas a, ludus_zone z
            WHERE h.area_id = a.area_id
            AND a.zone_id = z.zone_id";
        $zonas_cs = DB::query( $query_sql );
        $cuenta_cs = count( $zonas_cs );
        $zonas = DB::query( "SELECT zone_id, zone FROM ludus_zone" );
        $cuenta = count( $zonas );

        for ($i=0; $i < $cuenta_cs ; $i++) {
            $zonas_cs[$i]['notas'] = 0;
            $zonas_cs[$i]['cupos'] = 0;
            foreach ($data as $key => $d) {
                if( $zonas_cs[$i]['dealer_id'] == $d['dealer_id'] ){
                    $zonas_cs[$i]['notas'] += intval( $d['promedio'] );
                    $zonas_cs[$i]['cupos'] += intval( $d['max_size'] );
                }
            }
        }//FIN for

        for ($j=0; $j < $cuenta; $j++) {
            $zonas[$j]['notas'] = 0;
            $zonas[$j]['cupos'] = 0;
            $zonas[$j]['promedio'] = 0;
            foreach ($zonas_cs as $key => $zc) {
                if( $zonas[$j]['zone_id'] == $zc['zone_id'] ){
                    $zonas[$j]['notas'] += intval( $zc['notas'] );
                    $zonas[$j]['cupos'] += intval( $zc['cupos'] );
                }
            }
            if( $zonas[$j]['cupos'] > 0 ){
                $zonas[$j]['promedio'] = $zonas[$j]['notas'] / $zonas[$j]['cupos'];
            }
        }

		return $zonas;
	}//fin getRankingCcs

    //===================================================================
    // Obtiene el promedio de notas del año
    //===================================================================
    public static function getDatosEncuestas( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
        $filtro = "";
        $filtro_s = "";
        $datos = [];
        $anio = date( "Y" );
        // $anio = 2017;
        $fecha = $p['fecha'] == "0" ? "" : " AND MONTH(sc.start_date) IN ( {$p['fecha']} ) " ;
        if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
            $filtro_s = " AND he.dealer_id IN ( {$p['concesionarios']} ) ";
        }elseif( $p['zonas'] != "" ){
            $filtro = " AND z.zone_id IN ( {$p['zonas']} ) ";
            $filtro_s = " AND zo.zone_id IN ( {$p['zonas']} ) ";
        }
		$query_sql = "SELECT ra.user_id, ra.review_id, mru.module_result_usr_id, i.schedule_id, c.type, rad.*
            FROM ludus_reviews_answer_detail rad, ludus_reviews_answer ra, ludus_modules_results_usr mru, ludus_invitation i,
            	ludus_schedule s, ludus_courses c, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d,
            (
               	SELECT DISTINCT ds.schedule_id
                FROM ludus_dealer_schedule ds, ludus_schedule sc, ludus_headquarters he, ludus_areas ar, ludus_zone zo, ludus_dealers de
                WHERE ds.schedule_id = sc.schedule_id
                AND ds.dealer_id = he.dealer_id
                AND he.dealer_id = de.dealer_id
                AND he.area_id = ar.area_id
                AND ar.zone_id = zo.zone_id
                AND de.category = 'CHEVROLET'
                $filtro_s
                AND sc.status_id = 1 AND ds.status_id = 1 AND he.status_id = 1 AND ar.status_id AND zo.status_id = 1 AND de.status_id = 1
                $fecha
                AND YEAR(sc.start_date) = $anio
            ) AS nds
            WHERE rad.reviews_answer_id = ra.reviews_answer_id
            AND ra.module_result_usr_id = mru.module_result_usr_id
            AND mru.invitation_id = i.invitation_id
            AND i.schedule_id = s.schedule_id
            AND s.course_id = c.course_id
            AND ra.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            AND d.category = 'CHEVROLET'
            AND u.status_id = 1 AND h.status_id = 1 AND a.status_id = 1 AND z.status_id = 1 AND d.status_id = 1
            $filtro
            AND s.schedule_id = nds.schedule_id
            AND rad.question_id IN ( 35, 36, 37 )";
            // echo("<br>$query_sql<br>");
		$Rows_config = DB::query($query_sql);

		return $Rows_config;
	} //fin getDatosEncuestas
    //===================================================================
    // Obtiene el promedio de notas del año
    //===================================================================
    public static function encuestas( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
        $reviews = [
            "pregunta_35" => ["max"=>0,"min"=>0],
            "pregunta_36" => ["max"=>0,"min"=>0],
            "pregunta_37" => ["max"=>0,"min"=>0],
            "encuestas" => 0
        ];
        $tipos = ["IBT","VCT","WBT","OJT"];
        $cursos = [
            "IBT" => $reviews,
            "VCT" => $reviews,
            "WBT" => $reviews,
            "OJT" => $reviews
        ];
        $encuestas = Dashboard::getDatosEncuestas( $p );
        $num_encuestas = Dashboard::getNumDatosEncuestas( $p );

        //recorre cada uno de los tipos de cursos que hay
        foreach ($tipos as $key => $t) {
            //recorre la base de las notas
            foreach ($encuestas as $key2 => $e) {
                if( $e['type'] == $t ){
                    if( intval( $e['result'] ) >= 1 && intval( $e['result'] ) <= 6 ){
                        $cursos[$t][ 'pregunta_'.$e['question_id'] ]['min'] += intval( $e['result'] );
                    }elseif( intval( $e['result'] ) >= 9 ){
                        $cursos[$t][ 'pregunta_'.$e['question_id'] ]['max'] += intval( $e['result'] );
                    }

                }//fin validacion el tipo de curso

            }//fin foreach $encuestas

            //valida si hay encuestas de ese tipo en especifico
            if( isset($num_encuestas[ $t ]) ){
                $cursos[$t]['encuestas'] += $num_encuestas[ $t ];
            }

        }//fin foreach $tipos

		return $cursos;
	} //fin getDatosEncuestas

    //===================================================================
    // Obtiene el número de encuestas realizadas entre las fechas y la zona seleccionada
    //===================================================================
    public static function getNumDatosEncuestas( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
        $filtro = "";
        $filtro_s = "";
        $cuenta_reg = [];
        $anio = date( "Y" );
        // $anio = 2017;
        $fecha = $p['fecha'] == "0" ? "" : " AND MONTH(sc.start_date) IN ( {$p['fecha']} ) " ;
        if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
            $filtro_s = " AND he.dealer_id IN ( {$p['concesionarios']} ) ";
        }elseif( $p['zonas'] != "" ){
            $filtro = " AND z.zone_id IN ( {$p['zonas']} ) ";
            $filtro_s = " AND zo.zone_id IN ( {$p['zonas']} ) ";
        }
		$query_sql = "SELECT c.type, COUNT(DISTINCT ra.reviews_answer_id ) AS encuestas
            FROM ludus_reviews_answer_detail rad, ludus_reviews_answer ra, ludus_modules_results_usr mru, ludus_invitation i,
            	ludus_schedule s, ludus_courses c, ludus_users u, ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d,
            (
               	SELECT DISTINCT ds.schedule_id
                FROM ludus_dealer_schedule ds, ludus_schedule sc, ludus_headquarters he, ludus_areas ar, ludus_zone zo, ludus_dealers de
                WHERE ds.schedule_id = sc.schedule_id
                AND ds.dealer_id = he.dealer_id
                AND he.dealer_id = de.dealer_id
                AND he.area_id = ar.area_id
                AND ar.zone_id = zo.zone_id
                AND de.category = 'CHEVROLET'
                $filtro_s
                AND sc.status_id = 1 AND ds.status_id = 1 AND he.status_id = 1 AND ar.status_id AND zo.status_id = 1 AND de.status_id = 1
                $fecha
                AND YEAR(sc.start_date) = $anio
            ) AS nds
            WHERE rad.reviews_answer_id = ra.reviews_answer_id
            AND ra.module_result_usr_id = mru.module_result_usr_id
            AND mru.invitation_id = i.invitation_id
            AND i.schedule_id = s.schedule_id
            AND s.course_id = c.course_id
            AND ra.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            AND d.category = 'CHEVROLET'
            AND u.status_id = 1 AND h.status_id = 1 AND a.status_id = 1 AND z.status_id = 1 AND d.status_id = 1
            $filtro
            AND s.schedule_id = nds.schedule_id
            AND rad.question_id IN ( 35, 36, 37 )
            GROUP BY c.type";
            // echo("<br>$query_sql<br>");
		$Rows_config = DB::query($query_sql);

        foreach ($Rows_config as $key => $v) {
            $cuenta_reg[ $v['type'] ] = $v['encuestas'];
        }

		return $cuenta_reg;
	} //fin getNumDatosEncuestas


    // ========================================================
    // ========================================================
    // Nuevas funciones
    // ========================================================
    // ========================================================




    // ========================================================
    // Consulta los usuarios inscritos a cursos segun los meses seleccionados
    // ========================================================
    public static function getUsuariosInscritos( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');

        $anio = date('Y');
        $filtro = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }else if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }else{
            $filtro = "";
        }

        $query_sql = "SELECT i.*, u.first_name, u.last_name, u.identification, s.start_date, s.end_date, s.course_id, '' AS excusa, h.headquarter_id, h.headquarter, d.dealer_id, mru.score
            FROM ludus_schedule s, ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_modules_results_usr mru
            WHERE s.schedule_id = i.schedule_id
            AND i.user_id = u.user_id
            AND i.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND i.invitation_id = mru.invitation_id
            AND s.status_id = 1
            $filtro
            AND YEAR( s.start_date ) = $anio
            AND MONTH(s.start_date) IN ( {$p['fecha']} )
            UNION
            SELECT i.*, u.first_name, u.last_name, u.identification, s.start_date, s.end_date, s.course_id, '' AS excusa, h.headquarter_id, h.headquarter, d.dealer_id, 0 AS score
                        FROM ludus_schedule s, ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d
                        WHERE s.schedule_id = i.schedule_id
                        AND i.user_id = u.user_id
                        AND i.headquarter_id = h.headquarter_id
                        AND h.dealer_id = d.dealer_id
                        AND s.status_id = 1
                        $filtro
                        AND i.invitation_id NOT IN ( SELECT mru.invitation_id FROM ludus_modules_results_usr mru )
                        AND YEAR( s.start_date ) = $anio
                        AND MONTH(s.start_date) IN ( {$p['fecha']} )";

        $resultSet = DB::query( $query_sql );

        return $resultSet;
    }// fin getUsuariosInscritos

    // ========================================================
    // Consulta los usuarios inscritos a cursos segun los meses seleccionados
    // ========================================================
    public static function getNumeroUsuariosInscritos( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');

        $anio = date('Y');
        $filtro = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }else if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }else{
            $filtro = "";
        }

        $query_sql = "SELECT COUNT( u.user_id ) AS usuarios FROM ludus_users u,
            ( SELECT DISTINCT u.identification
                FROM ludus_schedule s, ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_modules_results_usr mru
                WHERE s.schedule_id = i.schedule_id
                AND i.user_id = u.user_id
                AND i.headquarter_id = h.headquarter_id
                AND h.dealer_id = d.dealer_id
                AND d.category = 'CHEVROLET'
                AND i.invitation_id = mru.invitation_id
                AND s.status_id = 1
                $filtro
                AND YEAR( s.start_date ) = $anio
                AND MONTH(s.start_date) IN ( {$p['fecha']} )
                UNION
                SELECT DISTINCT u.identification
                            FROM ludus_schedule s, ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d
                            WHERE s.schedule_id = i.schedule_id
                            AND i.user_id = u.user_id
                            AND i.headquarter_id = h.headquarter_id
                            AND h.dealer_id = d.dealer_id
                            AND d.category = 'CHEVROLET'
                            AND s.status_id = 1
                            $filtro
                            AND i.invitation_id NOT IN ( SELECT mru.invitation_id FROM ludus_modules_results_usr mru )
                            AND YEAR( s.start_date ) = $anio
                            AND MONTH(s.start_date) IN ( {$p['fecha']} )
             )  AS us
             WHERE u.identification = us.identification";

        $resultSet = DB::queryFirstRow( $query_sql );
        return $resultSet['usuarios'];
    }// fin getUsuariosInscritos

    // ========================================================
    // Muestra el concesionario del usuario segun la fecha del curso
    // ========================================================
    public static function ccsHistoricoUsuario( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');

        $query_sql = "SELECT i.*, h.headquarter, d.dealer_id
            FROM ludus_headquarter_history i, ludus_headquarters h, ludus_dealers d, (
                SELECT MAX(date_creation) as date_creation, user_id FROM ludus_headquarter_history
                WHERE date_creation < '{$p['start_date']}' AND user_id = {$p['user_id']} GROUP BY user_id
            ) as hy
            WHERE i.date_creation = hy.date_creation
            AND i.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND i.user_id = hy.user_id";
        // echo $query_sql;
        $resultSet = DB::queryFirstRow( $query_sql );

        return $resultSet;
    }// fin ccsHistoricoUsuario

    // ========================================================
    // Consulta los cupos de cada sesion segun el concesionario
    // ========================================================
    public static function getSchedules( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');

        $anio = date('Y');
        $filtro = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND ds.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }else if( $p['concesionarios'] != "" ){
            $filtro = " AND ds.dealer_id IN ( {$p['concesionarios']} ) ";
        }else{
            $filtro = "";
        }

        $query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, s.course_id, c.course, c.type, c.specialty_id, sp.specialty, MONTH( s.start_date ) AS mes,
                ds.dealer_schedule_id, ds.dealer_id, d.dealer, ds.max_size, m.duration_time
            FROM ludus_schedule s, ludus_dealer_schedule ds, ludus_courses c, ludus_specialties sp, ludus_dealers d, ludus_modules m
            WHERE s.schedule_id = ds.schedule_id
            AND s.course_id = c.course_id
            AND ds.dealer_id = d.dealer_id
            AND c.specialty_id = sp.specialty_id
            AND c.course_id = m.course_id
            AND d.category = 'CHEVROLET'
            AND ds.max_size > 0
            AND s.status_id = 1
            AND ds.status_id = 1
            AND YEAR( s.start_date ) = $anio
            $filtro
            AND MONTH(s.start_date) IN ( {$p['fecha']} )";

        $resultSet = DB::query( $query_sql );

        return $resultSet;
    }// fin getSchedules

    // ========================================================
    // Muestra el concesionario del usuario segun la fecha del curso
    // ========================================================
    public static function excusaUsuario( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');

        $query_sql = "SELECT e.* FROM ludus_excuses e WHERE e.status_id = 1 AND e.invitation_id = {$p['invitation_id']}";
        // echo $query_sql;
        $resultSet = DB::queryFirstRow( $query_sql );

        return $resultSet;
    }// fin ccsHistoricoUsuario

    // ========================================================
    // Muestra el concesionario del usuario segun la fecha del curso
    // ========================================================
    public static function excusasAgrupadas( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');

        $query_sql = "SELECT e.schedule_id, e.user_id
            FROM ludus_excuses e, ludus_schedule s
            WHERE e.schedule_id = s.schedule_id
            AND e.status_id = 1
            AND YEAR( s.start_date ) = $anio
            AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        // echo $query_sql;
        $resultSet = DB::query( $query_sql );

        return $resultSet;
    }// fin excusasAgrupadas

    // ========================================================
    // Muestra el concesionario del usuario segun la fecha del curso
    // ========================================================
    public static function excusasCuposAgrupadas( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');

        $query_sql = "SELECT e.dealer_id, e.schedule_id, e.num_invitation
            FROM ludus_excuses_dealers e, ludus_schedule s
            WHERE e.schedule_id = s.schedule_id
            AND e.status_id = 1
            AND YEAR( s.start_date ) = $anio
            AND MONTH( s.start_date ) IN ( {$p['fecha']} )";
        // echo $query_sql;
        $resultSet = DB::query( $query_sql );

        return $resultSet;
    }// fin excusasAgrupadas
    // ========================================================
    //
    // ========================================================
    public static function sabanaDatos( $p, $prof="../" ){
        $schedules = Dashboard::getSchedules( $p, $prof );
        $usuarios = Dashboard::getUsuariosInscritos( $p, $prof );
        $excusas = Dashboard::excusasAgrupadas( $p, $prof );
        $excusasCupos = Dashboard::excusasCuposAgrupadas( $p, $prof );
        $l_encuestas = Dashboard::getEncuestas( $p, $prof );
        $l_number_encuestas = Dashboard::getCantidadEncuestas( $p, $prof );

        $num_schedules = count( $schedules );
        $num_usuarios = count( $usuarios );
        $invitaciones = [];
        $_enc = [];

        // ========================================================
        // Agrupa en un objeto las excusas
        // ========================================================
        foreach ( $excusas as $key => $v ) {
            $_l = $v['schedule_id'].'_'.$v['user_id'];
            $exc_agrup[ $_l ] = 'Excusa';
        }

        foreach ( $excusasCupos as $key => $v ) {
            $_l = $v['schedule_id'].'_'.$v['dealer_id'];
            $exc_cupos_agrup[ $_l ] = intval($v['num_invitation']);
        }

        // ========================================================
        // Asigna el concesionario del usuario segun la fecha del curso
        // ========================================================
        for ($i=0; $i < $num_usuarios ; $i++) {
            $_label = $usuarios[$i]['schedule_id'].'_'.$usuarios[$i]['user_id'];

            if ( isset( $exc_agrup[$_label] ) ) {
                $usuarios[$i]['excusa'] = 'Excusa';
            }
        }// fin for excusas/dealers usuarios

        // ========================================================
        // Agrupo los usuarios en su respectivo curso
        // ========================================================
        for ($i=0; $i < $num_schedules ; $i++) {

            $schedule_id = $schedules[$i]['schedule_id'];
            $dealer_id = $schedules[$i]['dealer_id'];
            $schedules[$i]['inscritos'] = 0;
            $schedules[$i]['excusas'] = 0;
            $schedules[$i]['excusas_cupos'] = 0;
            $schedules[$i]['asistentes'] = 0;
            $schedules[$i]['aprobo'] = 0;
            $schedules[$i]['reprobo'] = 0;
            $schedules[$i]['promedio'] = 0;
            $schedules[$i]['usuarios'] = [];
            $prom_aux = 0;
            // Le asigna el numero de excusas para cupos segun sesion/concesionario
            $_label = $schedules[$i]['schedule_id'].'_'.$schedules[$i]['dealer_id'];
            if ( isset( $exc_cupos_agrup[$_label] ) ) {
                $schedules[$i]['excusas_cupos'] = $exc_cupos_agrup[$_label];
            }

            // Valida y asigna los usuarios de cada sesion
            foreach ( $usuarios as $key => $v ) {
                if ( $v['schedule_id'] == $schedule_id && $v['dealer_id'] == $dealer_id ) {
                    $invitaciones[] = $v['invitation_id'];
                    $schedules[$i]['inscritos']++;
                    $schedules[$i]['promedio'] += intval($v['score']);
                    if ( intval( $v['score'] ) >= 80 ) {
                        $schedules[$i]['aprobo'] ++;
                    } else {
                        $schedules[$i]['reprobo'] ++;
                    }

                    // cuenta las excusas
                    if ( $v['excusa'] == "Excusa" ) {
                        $schedules[$i]['excusas']++;
                    }
                    $schedules[$i]['usuarios'][] = $v;

                    // valida la asistencia
                    if ( $v['status_id'] == 2 ) {
                        $schedules[$i]['asistentes']++;
                    }
                }
            }// asigna los usuarios en su schedule correspondiente


        }// fin for schedules

        // ========================================================
        // Genero el array de las encuestas que esten en los ccs seleccionados
        // ========================================================
        // foreach ( $l_encuestas as $key => $e ) {
        //
        //     foreach ( $invitaciones as $key => $value ) {
        //         if ( $e['invitation_id'] == $value ) {
        //             $_enc[] = $e;
        //             break;
        //         }
        //     }
        // }

        $encuestas = Dashboard::listaEncuestas( $l_encuestas, $l_number_encuestas, $prof );

        return [ "schedules" => $schedules, "encuestas" => $encuestas ];

    }

    //===================================================================
    // Obtiene el promedio de notas del año, segmentado por mes
    //===================================================================
    public static function quartersDashboard( $d_schedules ){
        $cuenta_inv = count( $d_schedules );
        $resultSet = [
            [ "mes" =>1, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],[ "mes" =>2, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0],[ "mes" =>3, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],
            [ "mes" =>4, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],[ "mes" =>5, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0],[ "mes" =>6, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],
            [ "mes" =>7, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],[ "mes" =>8, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0],[ "mes" =>9, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],
            [ "mes" =>10, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],[ "mes" =>11, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0],[ "mes" =>12, "notas"=>0, "cupos"=>0, "asistentes"=>0, "excusas"=>0 ],
        ];

        for ($j=0; $j < 12; $j++) {
            $promedio = 0;
            $cupos = 0;
            $asistentes = 0;
            $excusas = 0;
            $mes = $j + 1;
            foreach ($d_schedules as $key => $i) {
                if( $i['mes'] == $mes ){
                    $cupos += intval( $i['max_size']);
                    $asistentes += intval( $i['asistentes']);
                    $excusas += intval( $i['excusas']) + intval( $i['excusas_cupos']);
                    $promedio = 0;
                    // foreach ($i['usuarios'] as $key => $u ) {
                    //     $promedio += intval( $u['score'] );
                    // }
                }
            }
            $resultSet[$j]['notas'] = $promedio;
            $resultSet[$j]['cupos'] = $cupos;
            $resultSet[$j]['asistentes'] = $asistentes;
            $resultSet[$j]['excusas'] = $excusas;
        }

        return $resultSet;

	} //fin quartersDashboard

    public static function getEncuestas( $p, $prof="../" ) {
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');

        $filtro = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }else if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }else{
            $filtro = "";
        }

        $query_sql = "SELECT c.type, rad.question_id, q.question, a.answer, COUNT( a.answer_id ) AS ife
            FROM ludus_invitation i, ludus_schedule s, ludus_modules_results_usr mru, ludus_reviews_answer ra,
            	ludus_dealers d, ludus_headquarters h, ludus_reviews_answer_detail rad, ludus_questions q, ludus_answers a, ludus_courses c
            WHERE i.schedule_id = s.schedule_id
            AND i.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND i.invitation_id = mru.invitation_id
            AND mru.module_result_usr_id = ra.module_result_usr_id
            AND ra.reviews_answer_id = rad.reviews_answer_id
            AND rad.question_id = q.question_id
            AND rad.answer_id = a.answer_id
            AND s.course_id = c.course_id
            AND s.status_id = 1
            $filtro
            AND ra.review_id IN ( 62,63,64 )
            AND rad.question_id IN ( 35, 36, 37, 1507, 1508, 1509, 1518, 1519, 1520 )
            AND MONTH( s.start_date ) IN ( {$p['fecha']} ) AND YEAR( s.start_date ) = $anio
            GROUP BY c.type, rad.question_id, a.answer_id
            ORDER BY c.type, a.answer";
        // AND rad.question_id IN ( 1530, 1532 ) == WBT

        $resultSet = DB::query( $query_sql );

        return $resultSet;

    }

    public static function getCantidadEncuestas( $p, $prof="../" ) {
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');

        $filtro = "";
        if( $p['filtro_concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['filtro_concesionarios']} ) ";
        }else if( $p['concesionarios'] != "" ){
            $filtro = " AND d.dealer_id IN ( {$p['concesionarios']} ) ";
        }else{
            $filtro = "";
        }

        $query_sql = "SELECT c.type, COUNT( i.invitation_id ) AS usuario
        FROM ludus_invitation i, ludus_schedule s, ludus_modules_results_usr mru, ludus_reviews_answer ra,
        	ludus_dealers d, ludus_headquarters h, ludus_courses c
        WHERE i.schedule_id = s.schedule_id
        AND i.headquarter_id = h.headquarter_id
        AND h.dealer_id = d.dealer_id
        AND i.invitation_id = mru.invitation_id
        AND mru.module_result_usr_id = ra.module_result_usr_id
        AND s.course_id = c.course_id
        AND s.status_id = 1
        $filtro
        AND ra.review_id IN ( 62,63,64 )
        AND MONTH( s.start_date ) IN ( {$p['fecha']} ) AND YEAR( s.start_date ) = $anio
        GROUP BY c.type";
        // AND rad.question_id IN ( 1530, 1532 ) == WBT

        $resultSet = DB::query( $query_sql );

        $types = [];
        foreach ($resultSet as $key => $value) {
            $types[ $value['type'] ] = $value['usuario'];
        }

        return $types;

    }

    //===================================================================
    // Obtiene el promedio de notas del año
    //===================================================================
    public static function listaEncuestas( $encuestas, $cant_encuestas, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
        $reviews = [
            "pregunta_35" => ["max"=>0,"min"=>0],
            "pregunta_36" => ["max"=>0,"min"=>0],
            "pregunta_37" => ["max"=>0,"min"=>0],
            "encuestas" => 0
        ];
        $tipos = ["IBT","VCT","WBT","OJT"];
        $cursos = [
            "IBT" => $reviews,
            "VCT" => $reviews,
            // "WBT" => $reviews,
            "OJT" => $reviews
        ];

        $cursos['IBT']['encuestas'] = isset( $cant_encuestas['IBT'] ) ? intval($cant_encuestas['IBT']) : 0;
        $cursos['VCT']['encuestas'] = isset( $cant_encuestas['VCT'] ) ? intval($cant_encuestas['VCT']) : 0;
        $cursos['OJT']['encuestas'] = isset( $cant_encuestas['OJT'] ) ? intval($cant_encuestas['OJT']) : 0;
        // $cursos['WBT']['encuestas'] = isset( $cant_encuestas['WBT'] ) ? intval($cant_encuestas['WBT']) : 0;

        //recorre cada uno de los tipos de cursos que hay
        foreach ($tipos as $t) {
            //recorre la base de las notas
            foreach ($encuestas as $key2 => $e) {
                if( $e['type'] == $t ){
                    switch ( $e['question_id'] ) {
                        case '35': $indice = "pregunta_35"; break;
                        case '36': $indice = "pregunta_36"; break;
                        case '37': $indice = "pregunta_37"; break;
                        case '1507': $indice = "pregunta_35"; break;
                        case '1508': $indice = "pregunta_36"; break;
                        case '1509': $indice = "pregunta_37"; break;
                        case '1518': $indice = "pregunta_35"; break;
                        case '1519': $indice = "pregunta_36"; break;
                        case '1520': $indice = "pregunta_37"; break;
                    }
                    if( intval( $e['answer'] ) >= 1 && intval( $e['answer'] ) <= 6 ){
                        $cursos[$t][ $indice ]['min'] += intval($e['ife']);
                        // $cursos[$t]['encuestas'] ++;

                    }elseif( intval( $e['answer'] ) >= 9 ){
                        $cursos[$t][ $indice ]['max'] += intval($e['ife']);
                        // $cursos[$t]['encuestas'] ++;

                    }

                }//fin validacion el tipo de curso

            }//fin foreach $encuestas

        }//fin foreach $tipos

		return $cursos;
	} //fin listaEncuestas


    // ========================================================
    // COnsulta la zona de un concesionario
    // ========================================================
    public static function getZonaByCcs( $dealer_id, $prof="../" ){
        include_once($prof.'../config/init_db.php');
        $query_sql = "SELECT DISTINCT z.*
            FROM ludus_zone z, ludus_areas a, ludus_headquarters h
            WHERE z.zone_id = a.zone_id
            AND a.area_id = h.area_id
            AND h.dealer_id = $dealer_id";
        $resultSet = DB::queryFirstRow( $query_sql );
        return $resultSet;
    }


}//fin clase Dashboard
