<?php
Class Datos {
	function consultaConcesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE dealer_id = 0 ORDER BY dealer";
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol'] > 3){
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		}elseif(isset($_SESSION['max_rol'])&&$_SESSION['max_rol'] == 3){
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 AND dealer_id = '$dealer_id' ORDER BY dealer";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function InformacionCargosDatos($fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		/*Trae la cantidad de personas por cargo de un concesionario que deberian ver los cursos*/
		$query_sql = "SELECT c.charge_id, c.charge, count(*) as cantidadPersonas
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c
						WHERE d.dealer_id = $_dealer_id
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id 
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id NOT IN (123,125,126,127,128,130)
						GROUP BY c.charge_id, c.charge";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		/*Trae la cantidad de cursos por cargo de un concesionario de forma general*/
		$query_sql_CXC = "SELECT c.charge_id, c.charge, count(distinct s.course_id) as CantidadCursos
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s
						WHERE d.dealer_id = $_dealer_id
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id 
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						GROUP BY c.charge_id, c.charge";
		$Rows_CXC = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CXC);

		/*Trae la cantidad de cursos asignados a las personas en la trayectoria general*/
		$query_sql_CAXT = "SELECT c.charge_id, c.charge, count(*) as CantidadCursosPersonas
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s
						WHERE d.dealer_id = $_dealer_id
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id 
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						GROUP BY c.charge_id, c.charge";
		$Rows_CAXT = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CAXT);

		/*Trae la cantidad de cursos que pasaron las personas en la trayectoria por cada nivel en la trayectoria*/
		$query_sql_CAXTNG = "SELECT c.charge_id, c.charge, count(*) as CantidadCursosPaso
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s, (SELECT distinct c.course_id, mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.date_creation < '$fechaCtrl' AND mr.approval = 'SI' AND mr.module_id = m.module_id AND m.course_id = c.course_id) cp
						WHERE d.dealer_id = $_dealer_id
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id 
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						AND u.user_id = cp.user_id AND s.course_id = cp.course_id
						GROUP BY c.charge_id, c.charge";
		$Rows_CAXTNG = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CAXTNG);


		/*Trae la cantidad de cursos que pasaron las personas en la trayectoria por cada nivel en la trayectoria*/
		$query_sql_CAXTN = "SELECT c.charge_id, c.charge, cc.level_id, count(*) as CantidadCursosNivel
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s, (SELECT distinct c.course_id, mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.date_creation < '$fechaCtrl' AND mr.approval = 'SI' AND mr.module_id = m.module_id AND m.course_id = c.course_id) cp
						WHERE d.dealer_id = $_dealer_id
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id 
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						AND u.user_id = cp.user_id AND s.course_id = cp.course_id
						GROUP BY c.charge_id, c.charge, cc.level_id";
		$Rows_CAXTN = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_CAXTN);

		$query_sql_PROM = "SELECT c.charge_id, c.charge, AVG(cp.promedio) as PromedioXCargo
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_charges_courses cc, ludus_courses s, (SELECT c.course_id, mr.user_id, AVG(mr.score) as promedio FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.date_creation < '$fechaCtrl' AND mr.module_id = m.module_id AND m.course_id = c.course_id GROUP BY c.course_id, mr.user_id) cp
						WHERE d.dealer_id = $_dealer_id
						AND d.dealer_id = h.dealer_id
						AND h.headquarter_id = u.headquarter_id 
						AND u.user_id = cu.user_id AND u.status_id = 1
						AND cu.charge_id = c.charge_id
						AND c.charge_id = cc.charge_id
						AND cc.course_id = s.course_id
						AND u.user_id = cp.user_id AND s.course_id = cp.course_id
						GROUP BY c.charge_id, c.charge";
		$Rows_PROM = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_PROM);


		for($i=0;$i<count($Rows_config);$i++) {
			$charge_id = $Rows_config[$i]['charge_id'];
			
				for($x=0;$x<count($Rows_CXC);$x++) {
					if( $charge_id == $Rows_CXC[$x]['charge_id'] ){
						$Rows_config[$i]['CantCursosVer'] = $Rows_CXC[$x]['CantidadCursos'];
					}
				}

				for($x=0;$x<count($Rows_CAXT);$x++) {
					if( $charge_id == $Rows_CAXT[$x]['charge_id'] ){
						$Rows_config[$i]['CantCursosXPersonas'] = $Rows_CAXT[$x]['CantidadCursosPersonas'];
					}
				}

				for($x=0;$x<count($Rows_CAXTNG);$x++) {
					if( $charge_id == $Rows_CAXTNG[$x]['charge_id'] ){
						$Rows_config[$i]['CantCursosPasoGral'] = $Rows_CAXTNG[$x]['CantidadCursosPaso'];
					}
				}

				for($x=0;$x<count($Rows_CAXTN);$x++) {
					if( $charge_id == $Rows_CAXTN[$x]['charge_id'] ){
						$Rows_config[$i]['CantCursosPasoXNivel'][] = $Rows_CAXTN[$x];
					}
				}

				for($x=0;$x<count($Rows_PROM);$x++) {
					if( $charge_id == $Rows_PROM[$x]['charge_id'] ){
						$Rows_config[$i]['Promedio'] = $Rows_PROM[$x]['PromedioXCargo'];
					}
				}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function InformacionDealers(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_dealers m
					WHERE m.status_id = 1
					ORDER BY m.dealer ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantCupos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT SUM(ds.min_size) as cupos
						FROM ludus_dealer_schedule ds, ludus_schedule s
						WHERE ds.schedule_id = s.schedule_id AND
						(s.start_date BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND ds.dealer_id = $_dealer_id AND ds.status_id = 1  AND s.status_id = 1 ";//LIMIT 0,20
						//echo $query_sql;
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantInscritos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT SUM(ds.inscriptions) as inscritos
						FROM ludus_dealer_schedule ds, ludus_schedule s
						WHERE ds.schedule_id = s.schedule_id AND 
						(s.start_date BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND ds.dealer_id = $_dealer_id AND ds.status_id = 1 ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantAsistencia($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT count(*) as cantidad 
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s
						WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND 
						i.schedule_id = s.schedule_id AND
						(s.start_date BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND 
						h.dealer_id = $_dealer_id AND
						i.status_id = 2 AND i.schedule_id IN (SELECT schedule_id FROM ludus_dealer_schedule WHERE dealer_id = $_dealer_id AND status_id = 1 ) ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantHorasAsistencia($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT SUM(m.duration_time) as tiempo
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s, ludus_courses c, ludus_modules m
						WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND 
						h.dealer_id = $_dealer_id AND i.schedule_id = s.schedule_id AND s.course_id = c.course_id AND c.course_id = m.course_id AND
						(s.start_date BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND i.status_id = 2 AND i.schedule_id IN (SELECT schedule_id FROM ludus_dealer_schedule WHERE dealer_id = $_dealer_id AND status_id = 1 ) ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantSesiones($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT count(distinct i.schedule_id) as sesiones
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s
						WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND i.schedule_id = s.schedule_id AND 
						(s.start_date BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND 
						h.dealer_id = $_dealer_id AND
						(i.date_result BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND i.status_id = 2 AND i.schedule_id IN (SELECT schedule_id FROM ludus_dealer_schedule WHERE dealer_id = $_dealer_id AND status_id = 1 ) ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function GetCantHorasAsistenciaProveedor($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT su.supplier, SUM(m.duration_time) as tiempo
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s, ludus_courses c, ludus_modules m, ludus_supplier su
						WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND 
						h.dealer_id = $_dealer_id AND i.schedule_id = s.schedule_id AND s.course_id = c.course_id 
						AND c.supplier_id = su.supplier_id
						AND c.course_id = m.course_id AND
						(s.start_date BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND i.status_id = 2 AND i.schedule_id IN (SELECT schedule_id FROM ludus_dealer_schedule WHERE dealer_id = $_dealer_id AND status_id = 1 ) 
						GROUP BY su.supplier";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantSesionesProveedor($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT su.supplier, count(distinct i.schedule_id) as sesiones
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_schedule s, ludus_courses c, ludus_supplier su 
						WHERE i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND i.schedule_id = s.schedule_id 
						AND s.course_id = c.course_id AND c.supplier_id = su.supplier_id 
						AND 
						(s.start_date BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND 
						h.dealer_id = $_dealer_id AND
						(i.date_result BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') AND i.status_id = 2 AND i.schedule_id IN (SELECT schedule_id FROM ludus_dealer_schedule WHERE dealer_id = $_dealer_id AND status_id = 1 ) 
						GROUP BY su.supplier";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}


	function GetGenteNoCursos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT count(distinct u.user_id) as cantidad
						FROM ludus_users u, ludus_headquarters h
						WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = $_dealer_id
						AND u.user_id NOT IN (SELECT user_id FROM ludus_modules_results_usr WHERE (date_creation BETWEEN '$fechaCtrl_Ini' AND '$fechaCtrl') < '$fechaCtrl' AND status_id = 1) ";//LIMIT 0,2
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetPAC($AnoData,$MesData,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT *
						FROM ludus_pac
						WHERE dealer_id = $_dealer_id
						AND mes = $MesData
						AND ano = $AnoData ";//LIMIT 0,20
						//echo $query_sql;
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantRegion($region){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$_dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT count(distinct dealer_id) as cantidad
					FROM `ludus_pac`
					WHERE region = '$region' ";//LIMIT 0,20
						//echo $query_sql;
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function GetCantVoluntarios($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT count(distinct u.user_id) as cantidad
						FROM ludus_charges_users c, ludus_users u, ludus_headquarters h
						WHERE c.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND u.status_id = 1
						AND h.dealer_id = $_dealer_id AND u.date_creation < '$fechaCtrl' AND c.charge_id IN (123,125) AND u.user_id NOT IN (SELECT u.user_id
						FROM ludus_charges_users c, ludus_users u, ludus_headquarters h
						WHERE c.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND u.status_id = 1
						AND h.dealer_id = $_dealer_id AND u.date_creation < '$fechaCtrl' AND c.charge_id NOT IN (123,125))";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetCantTotalPeople($fechaCtrl_Ini,$fechaCtrl,$_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT count(distinct u.user_id) as cantidad
						FROM ludus_users u, ludus_headquarters h
						WHERE u.headquarter_id = h.headquarter_id AND u.status_id = 1
						AND h.dealer_id = $_dealer_id AND u.date_creation < '$fechaCtrl' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function GetPAC_Evolutivo($_dealer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT ano, mes, pac, pos_region, pos_pais FROM `ludus_pac` where dealer_id = $_dealer_id ORDER BY ano, mes";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
