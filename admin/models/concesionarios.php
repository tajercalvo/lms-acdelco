<?php
Class Cargos {
	function consultaDatoscargos($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.dealer_id, c.dealer, c.sentinel, c.category, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_dealers c, ludus_users u, ludus_users s, ludus_status e
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id";
			@session_start();
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="4"){
				$dealer_id = $_SESSION['dealer_id'];
				$query_sql .= " AND c.dealer_id IN (SELECT DISTINCT a.dealer_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
			}
			if($sWhere!=''){
				$query_sql .= " AND (c.dealer LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.sentinel LIKE '%$sWhere%' )";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.dealer_id, c.dealer, c.sentinel, c.category, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_dealers c, ludus_users u, ludus_users s, ludus_status e
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id";
			@session_start();
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="4"){
				$dealer_id = $_SESSION['dealer_id'];
				$query_sql .= " AND c.dealer_id IN (SELECT DISTINCT a.dealer_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
			}
			if($sWhere!=''){
				$query_sql .= " AND (c.dealer LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.sentinel LIKE '%$sWhere%' )";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.dealer_id, c.dealer, c.sentinel, c.category, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_dealers c, ludus_users u, ludus_users s
			WHERE c.creator = u.user_id AND c.editor = s.user_id ";//LIMIT 0,20
		@session_start();
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']=="4"){
			$dealer_id = $_SESSION['dealer_id'];
			$query_sql .= " AND c.dealer_id IN (SELECT DISTINCT a.dealer_id FROM ludus_headquarters h, ludus_headquarters a, ludus_areas d, ludus_areas b WHERE h.dealer_id = $dealer_id AND h.area_id = d.area_id AND d.zone_id = b.zone_id AND b.area_id = a.area_id) ";
		}
		$query_sql.="ORDER BY dealer";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearcargos($nombre,$categoria){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$query_validar = "SELECT * FROM ludus_dealers where dealer = '$nombre'";
		$Rows_validar = $DataBase_Log->SQL_SelectMultipleRows($query_validar);
		if( count($Rows_validar) > 0 ){ return -1;}
		$insertSQL_EE = "INSERT INTO ludus_dealers (dealer,status_id,creator,editor,date_creation,date_edition,sentinel,category) VALUES ('$nombre','1','$idQuien','$idQuien','$NOW_data','$NOW_data','1','$categoria')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarcargos($id,$nombre,$status,$categoria){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO erum_logacceso (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_concesionarios.php?opcn=ver&id=$id&back=inicio>$id</a>] a cargos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE ludus_dealers SET dealer = '$nombre', category = '$categoria', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data' WHERE dealer_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_dealers c
			WHERE c.dealer_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
