<?php
Class Trayectoria {
	/*
	Andres Vega
	25/10/2016
	Funcion que trae la informacion del cargo que se pasa como parametro
	*/
	function consultaCargo($charge){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query = "SELECT * FROM ludus_charges WHERE charge_id = $charge";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaCargo
	/*
	Andres Vega
	25/10/2016
	Funcion que traen un array de las trayectorias y sus respectivos niveles
	*/
	function consultaTrayectorias($charge){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query = "SELECT cc.charge_course_id, cu.course_id, cu.newcode, cu.course, cc.level_id
		FROM ludus_charges_courses cc, ludus_courses cu
		WHERE cu.course_id = cc.course_id
		AND cc.charge_id = $charge
		AND cu.status_id = 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaCargo
	/*
	Funcion para traer las trayectorias especificas, seleccionadas en la clausula WHERE IN
	*/
	function consultaTrayectorias_Act(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query = "SELECT * FROM ludus_charges WHERE charge_id IN (49,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137) ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	/*
	*/
	public function consultaTrayectoriasUrs(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		$query = "SELECT * FROM ludus_charges_users WHERE user_id = $user_id AND status_id = 1";
		//echo($query);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin consultaTrayectoriasUrs
	/*
	*/
	public function consultaCursosAprobados($charge_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		$query_sql = "SELECT MAX(ru.score), ru.user_id, ru.module_id, cc.charge_id, m.course_id
			FROM ludus_modules_results_usr ru, ludus_modules m, ludus_charges_courses cc
		    WHERE ru.module_id = m.module_id
		    AND m.course_id = cc.course_id
		    AND ru.score >= 80
		    AND ru.user_id = $user_id
		    AND cc.charge_id = $charge_id
		    GROUP BY m.course_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaCursosAprobados
}//fin clase Trayectoria
