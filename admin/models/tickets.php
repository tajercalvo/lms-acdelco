<?php

class Tickets{
    //==========================================================================
    //Crea un nuevo ticket
    //==========================================================================
    public function crearTicket( $prioridad, $texto, $key_id , $imagen ){
        include_once('../../config/database.php');
		include_once('../../config/config.php');
        @session_start();
        $user = $_SESSION['idUsuario'];
        $now = date("Y-m-d")." ".(date("H")).":".date("i:s");
        $sql_query = "INSERT INTO ludus_tickets (user_id,date_creation, status_id, description, priority, key_id,image)
        VALUES ($user, '$now', 1, '$texto', $prioridad, $key_id, '$imagen')";
        //echo($sql_query);
        $database = new Database();
        $resultSet = $database->SQL_Insert( $sql_query );
        unset($database);
        return $resultSet;
    }//fin funcion crearTicket
    //==========================================================================
    //Consulta los datos del usuario para responder y enviar un correo
    //==========================================================================
    public function ticketSeleccionado($ticket){
        include_once('../../config/database.php');
		include_once('../../config/config.php');
        $sql_query = "SELECT t.*, u.first_name, u.last_name, u.email, k.key_id, k.key_description FROM ludus_tickets t, ludus_tickets_key k,ludus_users u
            WHERE ticket_id = $ticket
            AND t.user_id = u.user_id
            AND t.key_id = k.key_id";
        //echo($sql_query);
        $database = new Database();
        $resultSet = $database->SQL_SelectRows($sql_query);
        unset($database);
        return $resultSet;
    }//fin funcion ticketSeleccionado
    //==========================================================================
    //Consulta los tickets segun la lista pasada como parametro
    //==========================================================================
    public function consultaTickets( $lista ){
        include_once('../../config/database.php');
    	include_once('../../config/config.php');

        $sql_query = "SELECT t.*, u.first_name, u.last_name, u.image AS perfil, u.email, k.key_id, k.key_description
            FROM ludus_tickets t, ludus_tickets_key k, ludus_users u
            WHERE u.user_id = t.user_id
            AND t.key_id = k.key_id
            AND t.ticket_id IN ( $lista )
            ORDER BY t.ticket_id DESC";
        //echo( $sql_query );
        $database = new Database();
        $resultSet = $database->SQL_SelectMultipleRows($sql_query);
        unset($database);
        return $resultSet;
    }//fin funcion consultaTickets
    //==========================================================================
    //Consulta las respuestas hechas
    //==========================================================================
    function consultaRespuestas( $lista ){
        include_once('../../config/database.php');
    	include_once('../../config/config.php');
        $sql_query = "SELECT r.*, u.first_name, u.last_name
            FROM ludus_tickets_response r, ludus_users u
            WHERE r.user_id = u.user_id
            AND r.ticket_id IN ( $lista )";
        //echo( $sql_query );
        $database = new Database();
        $resultSet = $database->SQL_SelectMultipleRows($sql_query);
        unset($database);
        return $resultSet;
    }//fin funcion consultaRespuestas
    //==========================================================================
    //Retorna un String de los tickets_id separado por ,
    //==========================================================================
    function listaTickets( $pag = 1, $grup, $user = "" ) {
        include_once('../../config/database.php');
    	include_once('../../config/config.php');
        $lista = "0";
        if( $user == "" ){
            switch($grup){
                case 'Bandeja Entrada'  : $grupos = ""; break;
                case 'Pendientes'       : $grupos = " WHERE t.status_id = 1"; break;
                case 'Cerrados'         : $grupos = " WHERE t.status_id = 0"; break;
                case 'Reabiertos'       : $grupos = " WHERE t.status_id = 3"; break;
            }
        }else{
            $grupos = " WHERE t.user_id = $user";
        }
        $pagina = $pag < 1 ? 1 : $pag ;
        $inicio = ( $pagina - 1 ) * 15;
        $sql_query = "SELECT t.ticket_id
            FROM ludus_tickets t
            $grupos
            ORDER BY t.ticket_id DESC
            LIMIT $inicio, 15";
        //echo( $sql_query );
        $database = new Database();
        $resultSet = $database->SQL_SelectMultipleRows($sql_query);
        unset($database);
        foreach ($resultSet as $key => $value) {
            $lista .= "," . $value['ticket_id'];
        }
        return $lista;
    } //fin funcion listaTickets
    //==========================================================================
    //Retorna un String de los tickets_id separado por ,
    //==========================================================================
    function traerFiltroTickets( $nombre, $fecha = "" ) {
        include_once('../../config/database.php');
    	include_once('../../config/config.php');
        $fecha_f = " AND t.date_creation BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59' ";
        $ticket = intval( $nombre );
        //filtro
        if( $fecha == "" ){
            $sql_query_f = "AND ( u.first_name LIKE '%$nombre%'
                    OR u.last_name LIKE '%$nombre%'
                    OR t.ticket_id = $ticket )";
        }else{
            $sql_query_f = " AND t.date_creation BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59' ";
        }

        $sql_query = "SELECT t.*, u.first_name, u.last_name, u.image AS perfil, u.email, k.key_id, k.key_description
            FROM ludus_tickets t, ludus_tickets_key k, ludus_users u
            WHERE u.user_id = t.user_id
            AND t.key_id = k.key_id
            $sql_query_f
            ORDER BY t.ticket_id DESC
            LIMIT 0, 15";
        // echo( $sql_query );
        $database = new Database();
        $resultSet = $database->SQL_SelectMultipleRows($sql_query);
        unset($database);
        return $resultSet;
    } //fin funcion listaTickets
    //=============================================================================
    //Unifica las respuestas de los tickets con sus tickets correspondientes
    //=============================================================================
    function traerListaTickets( $pag = 1, $grup, $user = "" ){
        //retorna lista de tickets ( 0, 1, 12 )
        $lista = $this->listaTickets( $pag, $grup, $user );
        //retorna un array[] con los tickets
        $tickets = $this->consultaTickets( $lista );
        //retorna un array[] con las respuestas de los tickets
        $res_tickets = $this->consultaRespuestas( $lista );
        for($i = 0; $i < count( $tickets ) ; $i ++ ) {
            foreach ($res_tickets as $key => $respuesta ) {
                if( $tickets[$i]['ticket_id'] == $respuesta['ticket_id'] ){
                    $tickets[$i]['respuestas'][] = $respuesta;
                }
            }
        }
        return $tickets;
    }
    //=============================================================================
    //Trae los tickets creados para validar en excel
    //=============================================================================
    function descargaTickets( $estado ){
        include_once('../../config/database.php');
    	include_once('../../config/config.php');
        $condicion = "";
        switch( $estado ){
            case '1': $condicion .= " AND ( t.status_id = 1 OR t.status_id = 3 )"; break;
            case '2': $condicion .= " AND t.status_id = 0"; break;
        }
        $query_sql = "SELECT u.first_name, u.last_name, d.dealer, t.*, k.key_description
            FROM ludus_users u, ludus_tickets t, ludus_tickets_key k, ludus_headquarters h, ludus_dealers d
            WHERE t.user_id = u.user_id
            AND u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND t.key_id = k.key_id";
        $query_sql .= $condicion;
        $database = new Database();
        $resultSet = $database->SQL_SelectMultipleRows($query_sql);
        return $resultSet;
    }//fin funcion descargaTickets
    //==========================================================================
    //Cuenta la cantidad de tickets según el estado
    //==========================================================================
    public function cuentaTickets($grup = "todos"){
        include_once('../../config/database.php');
        include_once('../../config/config.php');
        $grupos = "";
        switch($grup){
            case 'cerrados'     : $grupos = " WHERE status_id = 0"; break;
            case 'pendientes'   : $grupos = " WHERE status_id = 1"; break;
            case 'reabiertos'   : $grupos = " WHERE status_id = 3"; break;

        }
        $sql_query = "SELECT COUNT(*) AS tickets FROM ludus_tickets";
        $sql_query .= $grupos;
        //echo("cuentaTickets ".$sql_query."<br>");
        $database = new Database();
        $resultSet = $database->SQL_SelectRows($sql_query);
        //print_r($resultSet);
        unset($database);
        return $resultSet;
    }//fin funcion cuentaTickets
    //==========================================================================
    //Cambia el estado de los tickets para que sean vistos o nuevos
    //==========================================================================
    public function visto($id, $checked){
        include_once('../../config/database.php');
		include_once('../../config/config.php');
        $sql_query = "UPDATE ludus_tickets SET checked = $checked WHERE ticket_id = $id";
        //echo($sql_query);
        $database = new Database();
        $resultSet = $database->SQL_Update($sql_query);
        unset($database);
        return $resultSet;
    }//fin funcion visto
    //==========================================================================
    //Cambia un ticket al estado cerrado = 0
    //==========================================================================
    public function cerrarTicket( $id, $texto, $imagen ){
        include_once( '../../config/database.php' );
        include_once( '../../config/config.php' );
        @session_start();
        $user = $_SESSION['idUsuario'];
        $now = date("Y-m-d")." ".(date("H")).":".date("i:s");
        $sql_insert = "INSERT INTO ludus_tickets_response (ticket_id,description,user_id,status_id,date_creation,image_answer)
            VALUES ($id,'$texto',$user,0,'$now','$imagen')";
        //echo($sql_insert."<br>");
        $database = new Database();
        $resultSet = $database->SQL_Insert( $sql_insert );
        unset($database);
        return $resultSet;
    }//fin cerrarTicket
    //==========================================================================
    //Cambia el estado de un ticket
    //==========================================================================
    public function actualizarEstado( $status_id, $ticket_id ){
        include_once( '../../config/database.php' );
        include_once( '../../config/config.php' );
        $sql_insert = "UPDATE ludus_tickets SET status_id = $status_id WHERE ticket_id = $ticket_id";
        $sql_insert_2 = "UPDATE ludus_tickets_response SET status_id = $status_id WHERE ticket_id = $ticket_id";
        //echo($sql_insert."<br>");
        $database = new Database();
        $resultSet = $database->SQL_Update($sql_insert);
        $resultSet2 = $database->SQL_Update($sql_insert_2);
        unset($database);
        return $resultSet;
    }//fin actualizarEstado
    //==========================================================================
    //Genera una lista de tipos de casos
    //==========================================================================
    public function tiposCasos(){
        include_once( '../config/database.php' );
        include_once( '../config/config.php' );
        $sql_query = "SELECT * FROM ludus_tickets_key";
        $database = new Database();
        $resultSet = $database->SQL_SelectMultipleRows($sql_query);
        unset($database);
        return $resultSet;
    }//fin funcion tiposCasos
    //==========================================================================
    //Crea notificaciones que se enviaran a cada uno de los usuarios
    //==========================================================================
    public function crearMensaje( $Mensaje, $idPersona, $level = 0 ){
        if( $level == 0 ){
            include_once( '../config/database.php' );
    		include_once( '../config/config.php' );
        }else{
            include_once( '../../config/database.php' );
    		include_once( '../../config/config.php' );
        }
        $user = $_SESSION['idUsuario'];
        $now = date("Y-m-d")." ".(date("H")).":".date("i:s");
        $sql_insert = "INSERT INTO ludus_messages (message,type,creator_id,reader_id,date_creation,status_id)
            VALUES ('$Mensaje',1,$user,$idPersona,'$now',1)";
        //echo($sql_insert);
        $database = new Database();
        $resultSet = $database->SQL_Insert( utf8_encode( $sql_insert ) );
        unset($database);
        return $resultSet;
    }//fin funcion crearMensaje
    //==========================================================================
    //Crea y envia un correo con los tickets
    //==========================================================================
    public function enviarCorreoTicket($titulo,$para,$mensaje_cuerpo){
		$mensaje   = '<html>
						<head>
							<title>Email Ludus Colombia - GMAcademy</title>
							<style type="text/css">
								@font-face {
									font-family: "LouisRegular";
									src: url("https://gmacademy.co/assets/fonts/LouisRegular.ttf");
								}
								body {
									background-color: #E9E7E9;
									font-family: LouisRegular;
								}
								.divPrincipal {
									background-image: url("https://gmacademy.co/assets/templateEmail/mailing.jpg");
									width: 770px;
									height:1030px;
								}
								.tituloComunicaciones{
									text-align: center;
									font-size: 30px;
									font-weight: bold;
									color: #013C9E;
									background-color: #E9E7E9;
									width: 482px;
								}
								.Amarillo{
									color: #0058a3;
								}
								.Negro{
									color: black;
									font-size: 12px;
									text-align: justify;
									vertical-align: top;
								}
								.NegroPequena{
									color: black;
									font-size: 9px;
									text-align: justify;
									vertical-align: top;
								}
							</style>
							<meta charset="utf-8">
						</head>
						<body style="background-color: #E9E7E9;">
							<div class="divPrincipal" style="background-image: url(https://gmacademy.co/assets/templateEmail/mailing.jpg);width: 770px;height:1030px;">
								<table>
									<tr>
										<td>
											<table>
												<tr>
													<td style="width: 620px; height: 125px;"></td>
													<td><img src="https://gmacademy.co/assets/templateEmail/logo.png" style="width: 120px;"></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style="height: 43px;"></td>
									</tr>
									<tr>
										<td>
											<table width="103%">
												<tr>
													<td style="height: 50px; width: 142px;"></td>
													<td class="tituloComunicaciones" style="text-align: center;font-size: 30px;font-weight: bold;color: #013C9E;background-color: #E9E7E9;width: 478px;">GM<span class="Amarillo" style="color: #0058a3;">Academy </span></td>
													<td style="width: 163px;"></td>
												</tr>
												<tr>
													<td style="height: 45px; width: 142px;"></td>
													<td class="tituloComunicaciones" style="text-align: center;font-size: 30px;font-weight: bold;color: #013C9E;background-color: #E9E7E9;width: 478px;">Comunicaciones Oficiales</td>
													<td style="width: 163px;"></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style="height: 135px;"></td>
									</tr>
									<tr>
										<td>
											<table width="103%">
												<tr>
													<td style="height: 82px; width: 55px;"></td>
													<td class="Amarillo" style="color: #0058a3;">'.($titulo).'</td>
													<td style="width: 90px;"></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style="height: 20px;"></td>
									</tr>
									<tr>
										<td>
											<table width="103%">
												<tr>
													<td style="height: 350px; width: 55px;"></td>
													<td class="Negro" style="color: black;font-size: 12px;text-align: justify;vertical-align: top;">'.($mensaje_cuerpo).'</td>
													<td style="width: 90px;"></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td style="height: 45px;"></td>
									</tr>
									<tr>
										<td>
											<table width="100%">
												<tr>
													<td style="height: 50px; width: 12px;"></td>
													<td class="NegroPequena" style="color: black;font-size: 9px;text-align: justify;vertical-align: top;"><em>Este correo es de tipo informativo y por lo tanto, le pedimos no responda a este mensaje. <b>Aviso legal:</b> El contenido de este mensaje y los archivos adjuntos son confidenciales y de uso exclusivo de GMAcademy. Si lo ha recibido por error, infórmelo y elimínelo de su correo. Las opciones, información, conclusiones y cualquier otro tipo de datos contenido en este correo electrónico, no relacionados con la actividad de GMAcademy se entenderán como personales y de ninguna manera son avaladas por la Academia. Se encuentran dirigidos solo al uso del destinatario al cual van enviados. La reproducción, lectura y/o copia se encuentra prohibidas a cualquier persona diferente a éste y puede ser ilegal.</em></td>
													<td style="width: 105px;"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</body>
					</html>';
		// Para enviar un correo HTML, debe establecerse la cabecera Content-type
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$cabeceras .= 'Reply-To: noreply@luduscolombia.com.co' . "\r\n";
		$cabeceras .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
		// Cabeceras adicionales
		$cabeceras .= 'To: ' . $para . "\r\n";
		$cabeceras .= 'From: Soporte Ludus <suport.gmacademy@luduscolombia.com.co>' . "\r\n";
		/*$cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";*/
		//$cabeceras .= 'Bcc: ' . $destinos . "\r\n";
		$resultado = mail('gmacademy@luduscolombia.com.co,'.$para, $titulo, $mensaje, $cabeceras);
		return $resultado;
	}//fin enviarCorreoTicket
    //==========================================================================
    //Consulta la informacion para los tickets
    //==========================================================================
    public function ticketsCreados(){
        include_once('../../config/database.php');
		include_once('../../config/config.php');
        @session_start();
        $tickets;
        $database = new Database();
        $sql_query = "SELECT * FROM ludus_tickets WHERE user_id = ".$_SESSION['idUsuario'];
        $resultSet = $database->SQL_SelectMultipleRows( $sql_query );
        foreach( $resultSet AS $key => $ticket ){

        }
        $sql_2 = "SELECT * FROM ludus_tickets_response WHERE ticket_id IN (  )";
        unset( $database );
        return $resultSet;
    }//fin ticketsCreados
}//fin clase Tickets

?>
