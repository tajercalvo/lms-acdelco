<?php
Class RepHojas {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	/*
	*/
	function consultaDatosAll($dealer_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($dealer_id) && $dealer_id == 'todos'){
			$query_sql = "SELECT d.dealer, h.headquarter, w.date_edition as date_creation, w.description, w.score, w.feedback, w.fec_fulfillment, w.target, w.action,
					u.first_name, u.last_name, u.identification, u1.first_name as first_prof, u1.last_name as last_prof, s.start_date, s.end_date,
					l.living, m.score as notaTotal, m.date_creation as fec_calificacion, c.course, DATEDIFF(w.date_edition,m.date_creation) as difCalfDias, c.newcode
					FROM ludus_waybills w, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_modules_results_usr m, ludus_schedule s,
					ludus_courses c, ludus_invitation i, ludus_users u1, ludus_livings l
					WHERE w.description IS NOT NULL AND w.date_edition BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND w.user_id = u.user_id
					AND w.teacher_id = u1.user_id
					AND u.headquarter_id = h.headquarter_id
					AND h.dealer_id = d.dealer_id
					AND w.module_result_usr_id = m.module_result_usr_id
					AND m.invitation_id = i.invitation_id
					AND i.schedule_id = s.schedule_id
					AND s.course_id = c.course_id
					AND s.living_id = l.living_id";
		}else{
			$query_sql = "SELECT d.dealer, h.headquarter, w.date_edition as date_creation, w.description, w.score, w.feedback, w.fec_fulfillment, w.target, w.action,
					u.first_name, u.last_name, u.identification, u1.first_name as first_prof, u1.last_name as last_prof, s.start_date, s.end_date,
					l.living, m.score as notaTotal, m.date_creation as fec_calificacion, c.course, DATEDIFF(w.date_edition,m.date_creation) as difCalfDias, c.newcode
					FROM ludus_waybills w, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_modules_results_usr m, ludus_schedule s,
					ludus_courses c, ludus_invitation i, ludus_users u1, ludus_livings l
					WHERE w.description IS NOT NULL AND w.date_edition BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					AND w.user_id = u.user_id
					AND w.teacher_id = u1.user_id
					AND u.headquarter_id = h.headquarter_id
					AND h.dealer_id = d.dealer_id
					AND w.module_result_usr_id = m.module_result_usr_id
					AND m.invitation_id = i.invitation_id
					AND i.schedule_id = s.schedule_id
					AND s.course_id = c.course_id
					AND s.living_id = l.living_id
					AND d.dealer_id = $dealer_id";
		}
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaDatosAll
}
