<?php
Class Argumentarios {
	function consultaArgumentarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email, c.argument_cat, c.color
						FROM ludus_argument m, ludus_argument_cat c, ludus_users u
						WHERE m.editor = u.user_id AND m.argument_cat_id = c.argument_cat_id
						ORDER BY m.argument_cat_id";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email, c.argument_cat, c.color
						FROM ludus_argument m, ludus_argument_cat c, ludus_users u
						WHERE m.status_id = 1 AND m.editor = u.user_id AND m.argument_cat_id = c.argument_cat_id
						ORDER BY m.argument_cat_id";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_Likes = "SELECT cu.*, u.first_name, u.last_name
						FROM ludus_argument_likes cu, ludus_users u
						WHERE cu.user_id = u.user_id
						ORDER BY cu.date_edition ";
		$Rows_Likes = $DataBase_Acciones->SQL_SelectMultipleRows($query_Likes);

		for($i=0;$i<count($Rows_config);$i++) {
			$argument_id = $Rows_config[$i]['argument_id'];
			//Cargos
				for($y=0;$y<count($Rows_Likes);$y++) {
					if( $argument_id == $Rows_Likes[$y]['argument_id'] ){
						$Rows_config[$i]['likes_data'][] = $Rows_Likes[$y];
					}
				}
			//Cargos
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCatArgumentario(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_argument_cat m
					WHERE m.status_id = 1
					ORDER BY m.argument_cat ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaMedArgumentario($tipo){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_media m
					WHERE m.status_id = 1 AND m.type = $tipo
					ORDER BY m.media ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearArgumentario($argument,$argument_cat_id,$characteristics,$competition,$accesories,$media_id,$image,$pdf,$commercial,$versions,$media_idv,$pdf2,$pdf3,$mp3,$description,$img_top1,$img_top2,$img_top3,$prominent){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$insertSQL_EE = "INSERT INTO `ludus_argument`
		(argument,argument_cat_id,date_edition,editor,status_id,image,likes,comments,characteristics,competition,accesories,media_id,file_pdf,commercial,versions,media_idv,file_pdf2,file_pdf3,file_mp3,description,img_top1,img_top2,img_top3,prominent)
		VALUES ('$argument','$argument_cat_id','$NOW_data','$idQuien','1','$image',0,0,'$characteristics','$competition','$accesories','$media_id','$pdf','$commercial','$versions','$media_idv','$pdf2','$pdf3','$mp3',
		'$description','$img_top1','$img_top2','$img_top3','$prominent')";
		//echo $insertSQL_EE;
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarArgumentario($id,$argument,$argument_cat_id,$characteristics,$competition,$accesories,$media_id,$image,$pdf,$commercial,$versions,$media_idv,$pdf2,$pdf3,$mp3,$description,$img_top1,$img_top2,$img_top3,$prominent,$estado){

		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_argument`
						SET argument = '$argument',
							argument_cat_id = '$argument_cat_id',
							editor = '$idQuien',
							date_edition = '$NOW_data',";
		if($image == '' || $image == 'default2.png'){$updateSQL_ER .= ''; }else{ $updateSQL_ER .= "image = '$image',";}
		$updateSQL_ER .= "status_id = '$estado',
							characteristics = '$characteristics',
							competition = '$competition',
							accesories = '$accesories',
							media_id = '$media_id',";
		if($pdf == '' || $pdf == 'default2.png'){$updateSQL_ER .= ''; }else{ $updateSQL_ER .= "file_pdf = '$pdf',";}
		$updateSQL_ER .= "commercial = '$commercial',
							versions = '$versions',";
		if($mp3 == '' || $mp3 == 'default.mp3'){$updateSQL_ER .= ''; }else{ $updateSQL_ER .= "file_mp3 = '$mp3',";}
		$updateSQL_ER .= "media_idv = '$media_idv',
							description = '$description'";
		if($img_top1 == '' || $img_top1 == 'default2.png'){$updateSQL_ER .= ''; }else{ $updateSQL_ER .= ",img_top1 = '$img_top1'";}
		if($img_top2 == '' || $img_top2 == 'default2.png'){$updateSQL_ER .= ''; }else{ $updateSQL_ER .= ",img_top2 = '$img_top2'";}
		if($img_top3 == '' || $img_top3 == 'default2.png'){$updateSQL_ER .= ''; }else{ $updateSQL_ER .= ",img_top3 = '$img_top3'";}
		if($prominent == '' || $prominent == 'default2.png'){$updateSQL_ER .= ''; }else{ $updateSQL_ER .= ",prominent = '$prominent'";}
		$updateSQL_ER .= " WHERE argument_id = $id";
		//echo($updateSQL_ER);
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaArgumentario($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT a.*, c.argument_cat, c.color
			FROM ludus_argument a, ludus_argument_cat c
			WHERE a.argument_id = '$idRegistro' AND a.argument_cat_id = c.argument_cat_id";//LIMIT 0,20
		$DataBase_Class = new Database();
		//echo($query_sql);
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaGaleria($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*
			FROM ludus_argument a, ludus_media_detail m
			WHERE a.argument_id = '$idRegistro'
			AND a.media_id = m.media_id
			AND m.status_id = 1
			LIMIT 0,4";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaGaleriaV($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*
			FROM ludus_argument a, ludus_media_detail m
			WHERE a.argument_id = '$idRegistro' AND a.media_idv = m.media_id LIMIT 0,4";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearLike($argument_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO `ludus_argument_likes` (argument_id,user_id,date_edition) VALUES ('$argument_id','$idQuien','$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE `ludus_argument`
						SET likes = likes+1
						WHERE argument_id = '$argument_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}

	/*
	Juan Carlos Villar 16/02/2017
	
	*/

	function actualizar_good_Practices($argument_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "UPDATE ludus_argument_good_practices SET `status_id`='1', `date_creation`='$NOW_data', `user_id_aprovation`='$idQuien' WHERE `id_ludus_argument_good_practices`='$argument_id';";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		return $resultado_IN;
	}

	function eliminar_good_Practices($argument_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_ER = "DELETE FROM ludus_argument_good_practices WHERE `id_ludus_argument_good_practices`='$argument_id';";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		return $resultado_IN;
	}

	

	


	function good_Practices($argument_id, $good_practices){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO ludus_argument_good_practices (`argument_id`, `good_practices`, `status_id`, `date_creation`, `user_id_creator`) VALUES ('$argument_id', '$good_practices', '2', '$NOW_data', '$idQuien');";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		
		return $resultado_IN;
	}

		function ludus_argument_good_practices($argument_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
		$query = "SELECT u.first_name, u.last_name, u.user_id, u.image, agp.*, d.dealer
			from ludus_users u, ludus_argument_good_practices agp, ludus_argument a, ludus_headquarters h, ludus_dealers d
			where u.user_id = agp.user_id_creator
			and agp.argument_id = a.argument_id
			and h.headquarter_id = u.headquarter_id
			and h.dealer_id = d.dealer_id
			and agp.argument_id = $argument_id
			order by date_creation asc";

		}else{
			$query = "SELECT u.first_name, u.last_name, u.user_id, u.image, agp.*, d.dealer
			from ludus_users u, ludus_argument_good_practices agp, ludus_argument a, ludus_headquarters h, ludus_dealers d
			where u.user_id = agp.user_id_creator
			and agp.argument_id = a.argument_id
			and h.headquarter_id = u.headquarter_id
			and h.dealer_id = d.dealer_id
			and agp.argument_id = $argument_id
			and agp.status_id = 1
			order by date_creation asc";
		}
		$resultado= $DataBase_Log->SQL_SelectMultipleRows($query);
		return $resultado;
	}

	//Fin Juan Carlos Villar

	/*
	Andres Vega 02/08/2016
	Funcion que valida y guarda el titulo y un archivo seleccionado como material de apoyo
	@argument_id		: argumento al cual esta asociado el archivo
	@title				: nombre del archivo en la vista
	@file_name			: nombre del archivo guardado
	*/
	function guardarMaterialApoyo($argument_id,$title,$file_name,$img_file){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		//id del usuario que guardo el archivo
		$idQuien= $_SESSION['idUsuario'];
		//fecha-hora del momento en el que se guardo el archivo
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$resultado=1;
		$repetidos=0;
		$repetidos= $DataBase_Log->SQL_SelectRows("SELECT title from ludus_argument_file where title = '$title' AND argument_id = $argument_id");
		if(count($repetidos)>0){
			$resultado=0;
		}else{
			$query_insert = "INSERT INTO ludus_argument_file (argument_id,title, file_name, img_file,user_id,date_creation) VALUES ($argument_id,'$title','$file_name','$img_file',$idQuien,'$NOW_data')";
			$resultado = $DataBase_Log->SQL_Insert($query_insert);
		}
		return $resultado;
	}//fin metodo guardarMaterialApoyo
	/*
	Andres Vega 20/10/2016
	Funcion que actualiza un adjunto para una galeria en especifico
	$file_id		: id del archivo a actualizar
	$title				: nombre del archivo en la vista
	$file_name			: nombre del archivo guardado
	$img_file : Imagen asociada a cada adjunto
	*/
	function actualizarMaterialApoyo($file_id,$title,$file_name,$img_file){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		//id del usuario que guardo el archivo
		$idQuien= $_SESSION['idUsuario'];
		//fecha-hora del momento en el que se guardo el archivo
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$query_insert = "UPDATE ludus_argument_file SET ";
			if($title != ""){$query_insert .= "title = '$title'";}
			if($file_name != ""){$query_insert .= ",file_name = '$file_name'";}
			if($img_file != ""){$query_insert .= ",img_file = '$img_file'";}
			$query_insert .= ",user_id = $idQuien,
			date_creation = '$NOW_data'
		WHERE argument_file_id = '$file_id'";
		echo($query_insert);
		$resultado = $DataBase_Log->SQL_Insert($query_insert);
		return $resultado;
	}//fin metodo actualizarMaterialApoyo

	function guardarFaq($argument_id,$faq,$faq_answer){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		//id del usuario que guardo el archivo
		$idQuien= $_SESSION['idUsuario'];
		//fecha-hora del momento en el que se guardo el archivo
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$query_insert = "INSERT INTO ludus_argument_faq (argument_id,faq,faq_answer,user_id,date_creation,status_id) VALUES ($argument_id,'$faq','$faq_answer',$idQuien,'$NOW_data','1')";
		$resultado = $DataBase_Log->SQL_Insert($query_insert);
		return $resultado;
	}

	function consultaMaterialApoyo($argument_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$query = "SELECT ar.* FROM ludus_argument_file ar WHERE ar.argument_id = $argument_id";
		$resultado= $DataBase_Log->SQL_SelectMultipleRows($query);
		return $resultado;
	}

	function consultaFaqs($argument_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$query = "SELECT ar.* FROM ludus_argument_faq ar WHERE ar.argument_id = $argument_id";
		$resultado= $DataBase_Log->SQL_SelectMultipleRows($query);
		return $resultado;
	}
	/*
	*/
	public function archivoEspecifico($id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$repetidos= $DataBase_Log->SQL_SelectRows("SELECT * from ludus_argument_file where argument_file_id = $id");
		return $repetidos;
	}

		// consultar archivos de la biblioteca
	function consultaLibraries(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_libraries where file like '%.mp3%';";						
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function relacionadadConBiblioteca($id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
						FROM ludus_libraries l, ludus_library_argument la
						where l.library_id = la.library_id 
						and la.argument_id = $id;";
						
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function crear_relacion_libreria($library_id, $argument_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');

		$query_sql = "INSERT INTO ludus_library_argument (library_id, argument_id) VALUES ('$library_id', '$argument_id');";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_Insert($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	// Eliminar todas las relaciones del argumentario con la biblioteca para despues actualizar las relaciones
	function eliminar_relacion_libreria($argument_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sqlDELETE = "DELETE FROM ludus_library_argument WHERE argument_id = $argument_id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_Update($query_sqlDELETE);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	

}
