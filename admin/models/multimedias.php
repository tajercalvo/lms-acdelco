<?php
Class Multimedias {
	function consultaGalerias($type_gal){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if($type_gal=="vid"){
			$type_var = "2";
		}else{
			$type_var = "1";
		}
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email 
						FROM ludus_media m, ludus_users u
						WHERE m.media_id <> 56 AND type = '$type_var' AND m.editor = u.user_id ORDER BY date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email 
						FROM ludus_media m, ludus_users u
						WHERE m.media_id <> 56 AND type = '$type_var' AND m.editor = u.user_id AND m.status_id = 1 ORDER BY date_edition DESC";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		if($type_gal=="vid"){
			$type_var = "2";
		}else{
			$type_var = "1";
		}
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email 
						FROM ludus_media m, ludus_users u
						WHERE m.media_id <> 56 AND type = '$type_var' AND m.editor = u.user_id ORDER BY date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email 
						FROM ludus_media m, ludus_users u
						WHERE m.media_id <> 56 AND type = '$type_var' AND m.editor = u.user_id AND m.status_id = 1 ORDER BY date_edition DESC";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if($type_gal=="vid"){
			$type_var = "2";
		}else{
			$type_var = "1";
		}
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email 
						FROM ludus_media m, ludus_users u
						WHERE m.media_id <> 56 AND type = '$type_var' AND m.editor = u.user_id ORDER BY date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email 
						FROM ludus_media m, ludus_users u
						WHERE m.media_id <> 56 AND type = '$type_var' AND m.editor = u.user_id AND m.status_id = 1 ORDER BY date_edition DESC";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearMultimedias($media,$description,$type_gal,$image){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración Multimedias: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_media` 
		(media,description,type,date_edition,editor,status_id,image) 
		VALUES ('$media','$description','$type_gal','$NOW_data','$idQuien','1','$image')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarMultimedias($id,$media,$description,$type_gal,$image,$estado){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_media` 
						SET media = '$media', 
							description = '$description', 
							editor = '$idQuien', 
							date_edition = '$NOW_data',
							type = '$type_gal',
							image = '$image',
							status_id = '$estado'
						WHERE media_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_modules c
			WHERE c.module_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.course_id, c.course
						FROM ludus_courses c
						ORDER BY c.course";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
}
