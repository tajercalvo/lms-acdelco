<?php
Class Mejor {

	function Asesor_Comercial_mejor_pais($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();

		// Ventas
		$ventas = "SELECT user_id, count(*) ventas FROM ludus_sales 
					where date_sale BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					group by user_id
					order by ventas desc;";
		$Rows_ventas = $DataBase_Acciones->SQL_SelectMultipleRows($ventas);

		// Notas
		$mejor_pais = "SELECT u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso, AVG(no.score) * COUNT(*) as academico
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (23, 35)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY cantidad_curso DESC, promedio DESC";		
        $Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_pais);

        $Rows_config2 = array();	
         $i = 0; // iniciando posicion del array final

         foreach ($Rows_ventas as $key => $valor) {

			foreach ($Rows_config as $key2 => $value2) {

				if ($valor['user_id'] == $value2['user_id'] ) { // Si el usuario tiene nota y venta

					if ($i == 0) {


						$venta_max = $valor['ventas']; // Capturamos el maximo de ventas que hizo el primer usuario, para tener la base
					}

					$academico_real = ($value2['academico'] / $Rows_config[0]['academico']*100); // capturamos el promedio, multiplicaco por la cantidad de cursos
					$ventas_porciento = ($valor['ventas'] / $venta_max * 100);
					
					$Rows_config2[$i] = $value2;
					$Rows_config2[$i]['academico_real'] = $academico_real;
					$Rows_config2[$i]['academico_mas_alto_como_referencia'] = $Rows_config[0]['academico'];
					$Rows_config2[$i]['ventas'] = $valor['ventas'];
					$Rows_config2[$i]['ventas_mas_alta_como_referencia'] = $venta_max;
					$Rows_config2[$i]['ventas_porciento'] = $ventas_porciento;
					$Rows_config2[$i]['mejores'] = round(($academico_real + $ventas_porciento) / 2, 2);

					$i=$i+1;
				}
			}

		}

		if (count($Rows_config2)>0) {
			// print_r($Rows_config2);
			foreach ($Rows_config2 as $key => $datos) {
	   		 $aux[$key] = $datos['mejores']; // Recorriendo el array caprurando los mejores
			}
			array_multisort($aux, SORT_DESC, $Rows_config2); // Ordenando los mejores del aray en orden de mayor a menor
		}
		 
		unset($DataBase_Acciones);
		return $Rows_config2;

	}


	function Asesor_Comercial_mejor_zona($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();

		$Rows_config = array();
		
		// Ventas
		$ventas = "SELECT user_id, count(*) ventas FROM ludus_sales 
					where date_sale BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					group by user_id
					order by ventas desc;";
		$Rows_ventas = $DataBase_Acciones->SQL_SelectMultipleRows($ventas);

		// Notas
		$mejor_zona = "SELECT z.zone_id, z.zone, u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso, AVG(no.score) * COUNT(*) as academico
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, ludus_areas a, ludus_zone z, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (23, 35)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					AND	a.area_id = h.area_id
                    AND a.zone_id = z.zone_id
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY cantidad_curso DESC, promedio DESC";
					// echo "$mejor_zona";		
        $Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_zona);

        // echo "$Rows_config";
        $Rows_config2 = array();	
         $i = 0; // iniciando posicion del array final

         foreach ($Rows_ventas as $key => $valor) {

			foreach ($Rows_config as $key2 => $value2) {

				if ($valor['user_id'] == $value2['user_id'] ) { // Si el usuario tiene nota y venta

					if ($i == 0) {

						$venta_max = $valor['ventas']; // Capturamos el maximo de ventas que hizo el primer usuario, para tener la base
					}

					$academico_real = ($value2['academico'] / $Rows_config[0]['academico']*100); // capturamos el promedio, multiplicaco por la cantidad de cursos
					$ventas_porciento = ($valor['ventas'] / $venta_max * 100);
					
					$Rows_config2[$i] = $value2;
					$Rows_config2[$i]['academico_real'] = $academico_real;
					$Rows_config2[$i]['academico_mas_alto_como_referencia'] = $Rows_config[0]['academico'];
					$Rows_config2[$i]['ventas'] = $valor['ventas'];
					$Rows_config2[$i]['ventas_mas_alta_como_referencia'] = $venta_max;
					$Rows_config2[$i]['ventas_porciento'] = $ventas_porciento;
					$Rows_config2[$i]['mejores'] = round(($academico_real + $ventas_porciento) / 2, 2);

					$i=$i+1;
				}
			}

		}

		if (count($Rows_config2)>0) {
			// print_r($Rows_config2);
			foreach ($Rows_config2 as $key => $datos) {
	   		 $aux[$key] = $datos['mejores']; // Recorriendo el array y ordenandolo por fechas
			}
			array_multisort($aux, SORT_DESC, $Rows_config2); // Ordenando las fechas del aray en orden de menor a mayor
		}
		//

		//zona
		$zone = "SELECT * FROM ludus_zone;";
		$Rows_zone = $DataBase_Acciones->SQL_SelectMultipleRows($zone);

        foreach ($Rows_zone as $key => $value) {

			foreach ($Rows_config2 as $key2 => $value2) {
				
				if ($value['zone_id'] == $value2['zone_id'] ) {
					$Rows_config3[] = $value2;
					// unset($Rows_zone[$key]);
					// unset($Datos[$key2]);
					break;
				}
			}
		}

		if (isset($Rows_config3)) {
			
			if ( count($Rows_config3) > 0) {
				// print_r($Rows_config2);
				foreach ($Rows_config3 as $key => $datos) {
		   		 $aux2[$key] = $datos['mejores']; // Recorriendo el array y ordenandolo por mejores
				}
				array_multisort($aux2, SORT_DESC, $Rows_config3); // Ordenando los mejores del aray en orden de mayor a menor
			}
		}else{
			$Rows_config3 = array();
			
		}


		unset($DataBase_Acciones);
		return $Rows_config3;
	}

	function Asesor_Comercial_mejor_concesionario($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();

		$Rows_config = array();
		
		// Ventas
		$ventas = "SELECT user_id, count(*) ventas FROM ludus_sales 
					where date_sale BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					group by user_id
					order by ventas desc;";
		$Rows_ventas = $DataBase_Acciones->SQL_SelectMultipleRows($ventas);

		// Notas
		$mejor_pais = "SELECT u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso, AVG(no.score) * COUNT(*) as academico
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (23, 35)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY cantidad_curso DESC, promedio DESC";		
        $Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_pais);
		
        // echo "$Rows_config";
        $Rows_config2 = array();	
         $i = 0; // iniciando posicion del array final

         foreach ($Rows_ventas as $key => $valor) {

			foreach ($Rows_config as $key2 => $value2) {

				if ($valor['user_id'] == $value2['user_id'] ) { // Si el usuario tiene nota y venta

					if ($i == 0) {

						$venta_max = $valor['ventas']; // Capturamos el maximo de ventas que hizo el primer usuario, para tener la base
					}

					$academico_real = ($value2['academico'] / $Rows_config[0]['academico']*100); // capturamos el promedio, multiplicaco por la cantidad de cursos
					$ventas_porciento = ($valor['ventas'] / $venta_max * 100);
					
					$Rows_config2[$i] = $value2;
					$Rows_config2[$i]['academico_real'] = $academico_real;
					$Rows_config2[$i]['academico_mas_alto_como_referencia'] = $Rows_config[0]['academico'];
					$Rows_config2[$i]['ventas'] = $valor['ventas'];
					$Rows_config2[$i]['ventas_mas_alta_como_referencia'] = $venta_max;
					$Rows_config2[$i]['ventas_porciento'] = $ventas_porciento;
					$Rows_config2[$i]['mejores'] = round(($academico_real + $ventas_porciento) / 2, 2);

					$i=$i+1;
				}
			}

		}

		if (count($Rows_config2)>0) {
			// print_r($Rows_config2);
			foreach ($Rows_config2 as $key => $datos) {
	   		 $aux[$key] = $datos['mejores']; // Recorriendo el array y ordenandolo por fechas
			}
			array_multisort($aux, SORT_DESC, $Rows_config2); // Ordenando las fechas del aray en orden de menor a mayor
		}
		//

		//Inicio capturando los mejores por concesionario

		//Obteniendo los concesionario
		$dealer = "SELECT dealer FROM ludus_dealers";
		$Rows_dealer = $DataBase_Acciones->SQL_SelectMultipleRows($dealer);

		// Recorrindo los concesionarios
		foreach ($Rows_dealer as $key => $value) {

			foreach ($Rows_config2 as $key2 => $value2) {

				//Si coinciden los concesionario
				if ($value['dealer'] == $value2['dealer'] ) {
					$Rows_config3[] = $value2;
					break;
				}
			}
		}
		//Fin capturando los mejores de la zona

		//Inicio ordenamiento de array final por puntaje

		if (isset($Rows_config3)) {
				if (count($Rows_config3) > 0) {
					// print_r($Rows_config2);
					foreach ($Rows_config3 as $key => $datos) {
			   		 $aux2[$key] = $datos['mejores']; // Recorriendo el array y ordenandolo por mejores
					}
					array_multisort($aux2, SORT_DESC, $Rows_config3); // Ordenando los mejores del aray en orden de mayor a menor
				}
				//Fin ordenamiento de array final por puntaje
		}else{
			$Rows_config3 = array();
		}

		unset($DataBase_Acciones);
		return $Rows_config3;
	}

	function Asesor_Tecnico_mejor_pais($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();

		$mejor_pais = "SELECT u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso, AVG(no.score) * COUNT(*) as academico
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (137, 31, 32)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY cantidad_curso DESC, promedio DESC
                    limit 3";

                    //echo "$mejor_pais";
        $Rows_configDetail = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_pais);
		unset($DataBase_Acciones);
		return $Rows_configDetail;
	}

	function Asesor_Tecnico_mejor_zona($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		$zone = "SELECT * FROM ludus_zone;";
		$Rows_zone = $DataBase_Acciones->SQL_SelectMultipleRows($zone);

		$mejor_zona = "SELECT z.zone_id, z.zone, u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, AVG(no.score) as promedio, COUNT(*) as cantidad_curso, AVG(no.score) * COUNT(*) as academico
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, ludus_areas a, ludus_zone z,
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (137, 31, 32)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
                    AND	a.area_id = h.area_id
                    AND a.zone_id = z.zone_id
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, z.zone
					ORDER BY cantidad_curso DESC, promedio DESC;";
					//echo "$mejor_zona";
        $Datos = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_zona);

        foreach ($Rows_zone as $key => $value) {

			foreach ($Datos as $key2 => $value2) {
				
				if ($value['zone_id'] == $value2['zone_id'] ) {
					$Rows_config[] = $value2;
					unset($Rows_zone[$key]);
					unset($Datos[$key2]);
					break;
				}
			}
		}

		if (isset($Rows_config)) {
				if (count($Rows_config) > 0) {
					// print_r($Rows_config2);
					foreach ($Rows_config as $key => $datos) {
			   		 $aux2[$key] = $datos['academico']; // Recorriendo el array y ordenandolo por mejores
					}
					array_multisort($aux2, SORT_DESC, $Rows_config); // Ordenando los mejores del aray en orden de mayor a menor
				}
				//Fin ordenamiento de array final por puntaje
		}else{
			$Rows_config = array();
		}


		unset($DataBase_Acciones);
		return $Rows_config;
	}


	function Asesor_Tecnico_mejor_concesionario($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		$dealer = "SELECT dealer FROM ludus_dealers";
		$Rows_dealer = $DataBase_Acciones->SQL_SelectMultipleRows($dealer);

		$mejor_concesionario = "SELECT u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso, AVG(no.score) * COUNT(*) as academico
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (137, 31, 32)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY cantidad_curso DESC, promedio DESC;";
		$Datos = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_concesionario);

		foreach ($Rows_dealer as $key => $value) {

			foreach ($Datos as $key2 => $value2) {
				
				if ($value['dealer'] == $value2['dealer'] ) {
					$Rows_config[] = $value2;
					unset($Rows_dealer[$key]);
					unset($Datos[$key2]);
					break;
				}
			}
		}

		if (isset($Rows_config)) {
				if (count($Rows_config) > 0) {
					// print_r($Rows_config2);
					foreach ($Rows_config as $key => $datos) {
			   		 $aux2[$key] = $datos['academico']; // Recorriendo el array y ordenandolo por mejores
					}
					array_multisort($aux2, SORT_DESC, $Rows_config); // Ordenando los mejores del aray en orden de mayor a menor
				}
				//Fin ordenamiento de array final por puntaje
		}else{
			$Rows_config = array();
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	/*function Asesor_Comercial_mejor_concesionario($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();

		$Rows_config = array();

		$dealer = "SELECT dealer FROM ludus_dealers";
		$Rows_dealer = $DataBase_Acciones->SQL_SelectMultipleRows($dealer);

		// Ventas
		$ventas = "SELECT user_id, count(*) ventas FROM ludus_sales 
					where date_sale BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					group by user_id;";
		$Rows_ventas = $DataBase_Acciones->SQL_SelectMultipleRows($ventas);
		// echo "<pre>";
		// print_r($Rows_ventas);
		// echo "</pre>";
		

		$mejor_concesionario = "SELECT u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (23, 35)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY d.dealer, cantidad_curso DESC, promedio DESC";
		$Datos = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_concesionario);

		foreach ($Rows_dealer as $key => $value) {

			foreach ($Datos as $key2 => $value2) {

				if ($value['dealer'] == $value2['dealer'] ) {
					$Rows_config[] = $value2;
					unset($Rows_dealer[$key]);
					unset($Datos[$key2]);
					break;
				}
			}
		}

		foreach ($Rows_config as $key => $valor) {

			foreach ($Rows_ventas as $key2 => $value2) {

				if ($valor['user_id'] == $value2['user_id'] ) {

					$prom_cursos_y_ventas = round(($valor['promedio'] + $valor['cantidad_curso'] + $value2['ventas'])/3, 1);
					$Rows_config[$key]['ventas'] = $value2['ventas'];
					$Rows_config[$key]['prom_cursos_y_ventas'] = $prom_cursos_y_ventas;
					unset($Rows_ventas[$key]);
					break;
				}
			}

		}

		// echo "<pre>";
		// print_r($Rows_ventas);
		// echo "</pre>";

		unset($Rows_dealer);
		unset($Datos);
		unset($Rows_ventas);
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/


	/*	function Asesor_Comercial_mejor_zona($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();

		$Rows_config = array();

		$zone = "SELECT * FROM ludus_zone;";
		$Rows_zone = $DataBase_Acciones->SQL_SelectMultipleRows($zone);

		$mejor_zona = "SELECT z.zone_id, z.zone, u.user_id, u.image, u.identification, u.first_name, u.last_name, h.headquarter, AVG(no.score) as promedio, COUNT(*) as cantidad_curso
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, ludus_areas a, ludus_zone z,
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (23, 35)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
                    AND	a.area_id = h.area_id
                    AND a.zone_id = z.zone_id
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, z.zone
					ORDER BY d.dealer, cantidad_curso DESC, promedio DESC";
					 // echo "$mejor_zona";
        $Datos = $DataBase_Acciones->SQL_SelectMultipleRows($mejor_zona);

        foreach ($Rows_zone as $key => $value) {

			foreach ($Datos as $key2 => $value2) {
				
				if ($value['zone_id'] == $value2['zone_id'] ) {
					$Rows_config[] = $value2;
					// unset($Rows_zone[$key]);
					// unset($Datos[$key2]);
					break;
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}*/


/*		function AsesorComercial($start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $query_ppal = "SELECT c.charge_id, c.charge, count(*) as cantidad 
		// 			FROM ludus_charges c, ludus_charges_courses cc
		// 			WHERE c.charge_id IN (95,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137) 
		// 			AND c.charge_id = cc.charge_id
		// 			GROUP BY c.charge_id, c.charge
		// 			ORDER BY c.charge";
		$query_ppal = "SELECT dealer
		FROM ludus_dealers";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_ppal);

		$query = "SELECT u.user_id, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (95,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY d.dealer, cantidad_curso DESC, promedio DESC ";
		$Rows_configDetail = $DataBase_Acciones->SQL_SelectMultipleRows($query);

		for($i=0;$i<count($Rows_config);$i++) {
			$charge_id = $Rows_config[$i]['dealer'];
			//Cargos
				for($y=0;$y<count($Rows_configDetail);$y++) {
					if( $charge_id == $Rows_configDetail[$y]['dealer'] ){
						$Rows_config[$i]['Details'][] = $Rows_configDetail[$y];
					}
				}
			//Cargos
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/

}

