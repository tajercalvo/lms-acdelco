<?php
Class RepNavegacionClass {

	function consulta_zona_Ajax($zona_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "	SELECT distinct d.dealer_id, d.dealer
						FROM ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
						where h.area_id = a.area_id
						and z.zone_id = a.zone_id
						and d.dealer_id = h.dealer_id
						and z.zone_id = $zona_id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_concesionario_Ajax($dealer_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		//$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT h.headquarter_id, h.headquarter, a.area, a.area_id, z.zone, z.zone_id, d.dealer, d.dealer_id
							FROM ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
							where h.area_id = a.area_id
							and z.zone_id = a.zone_id
							and d.dealer_id = h.dealer_id
							and d.dealer_id = $dealer_id;";
							//echo "$query_sql";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_visitantes_unicos1_Ajax($start_date, $end_date, $condicion, $cargo){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');

		// echo "$cargo";

		if ($cargo=='') {

				$query_visitantes_unicos = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(distinct n.user_id) as visitantes_unicos
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
	                        and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							$condicion
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation)
							ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";

				$query_visitas = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(n.navigation_id) as visitas
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							$condicion
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation)
							ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";

			}else{

				$query_visitantes_unicos = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(distinct n.user_id) as visitantes_unicos
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges_users cu, ludus_charges c
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							and u.user_id = cu.user_id
							and cu.charge_id = c.charge_id
							$condicion
							and c.charge_id = $cargo
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation) ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";

							// echo "$query_visitantes_unicos";
				$query_visitas = "SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(n.navigation_id) as visitas
							FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges_users cu
							WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
							and u.headquarter_id = h.headquarter_id
							and h.dealer_id = d.dealer_id
							and n.user_id = u.user_id
							and h.area_id = a.area_id
							and z.zone_id = a.zone_id
							and u.user_id = cu.user_id
                            $condicion
							and cu.charge_id = $cargo
							GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation)
							ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";
							// echo "$query_visitas";

					}

		$DataBase_Acciones = new Database();
		$visitantes_unicos = $DataBase_Acciones->SQL_SelectMultipleRows($query_visitantes_unicos);


		$visitas = $DataBase_Acciones->SQL_SelectMultipleRows($query_visitas);

		foreach ($visitas as $key => $value) {
			$visitantes_unicos[$key]['visitas']=$value['visitas'];

		}



		foreach ($visitantes_unicos as $key => $value) {
			$ano = $value['ano'];
			$mes = $value['mes'];
			$query_cedulas = "SELECT count(distinct u.user_id) as cant, '1' as mes
					FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_zone z, ludus_areas a, ludus_charges_users cu, ludus_charges c
					WHERE u.status_id = 1 AND u.date_creation < '$ano-$mes-01'
					and u.headquarter_id = h.headquarter_id
					and h.dealer_id = d.dealer_id
					and n.user_id = u.user_id
					and h.area_id = a.area_id
					and z.zone_id = a.zone_id
					and u.user_id = cu.user_id
					and cu.charge_id = c.charge_id
					$condicion";
					if ($cargo!="") {
						$query_cedulas.=" and cu.charge_id = $cargo";
					}
					$cantidad_cedulas = $DataBase_Acciones->SQL_SelectMultipleRows($query_cedulas);
					// print_r($cantidad_cedulas);
					$visitantes_unicos[$key]['cant_cedulas']= $cantidad_cedulas[0]['cant'];
		}

		unset($DataBase_Acciones);
		return $visitantes_unicos;
	}

	function consulta_sedes(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_headquarters order by headquarter;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consulta_zona(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_zone;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}




	function consulta_Trayectorias(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT * FROM ludus_charges
		where charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99)
		order by charge;";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

		function consulta_concesioanrios(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT * FROM ludus_dealers order by dealer;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	/*function consultaNavegacion($start_date, $end_date, $dealer){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();

		if ($dealer!= '') {


		$query_sql = "SELECT n.section, count(*) as cantidadVisitas, count(distinct n.user_id) as cantidadUsuarios ,avg(time_navigation) as tiempoPromedio, sum(time_navigation) as tiempoTotal
		FROM ludus_navigation n, ludus_users u, ludus_headquarters h
		WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
		AND n.user_id = u.user_id
		AND u.headquarter_id = h.headquarter_id
		AND h.dealer_id = $dealer
		GROUP BY n.section ORDER BY n.section";
		}else{


		$query_sql = "SELECT n.section, count(*) as cantidadVisitas, count(distinct n.user_id) as cantidadUsuarios ,avg(time_navigation) as tiempoPromedio, sum(time_navigation) as tiempoTotal
		FROM ludus_navigation n, ludus_users u, ludus_headquarters h
		WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
		AND n.user_id = u.user_id
		AND u.headquarter_id = h.headquarter_id
		GROUP BY n.section ORDER BY n.section";

		}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);


		// Lugar y cantidad de visitas
		$query_sql = "SELECT n.section, count(*) as cantidadVisitas
		FROM ludus_navigation n, ludus_users u, ludus_headquarters h
		WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND
		n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
		";
		if(isset($_SESSION['max_rol'])&& $_SESSION['max_rol'] <= 4 ){
			$query_sql .= " AND h.dealer_id = $dealer_id";
		}
		$query_sql .= " GROUP BY n.section ORDER BY n.section";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		// Tiempo total por sesion
		$query_TiempoTotal = "SELECT section, sum(time_navigation) as tiempoTotal
		FROM ludus_navigation n, ludus_users u, ludus_headquarters h
		WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND
		n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
		";
		if(isset($_SESSION['max_rol'])&& $_SESSION['max_rol'] <= 4 ){
			$query_TiempoTotal .= " AND h.dealer_id = $dealer_id";
		}
		$query_TiempoTotal .= " GROUP BY n.section";
		$Rows_TiempoTotal = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoTotal);


		//
		$query_TiempoPromedio = "SELECT section, avg(time_navigation) as tiempoPromedio
		FROM ludus_navigation n, ludus_users u, ludus_headquarters h
		WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND
		n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
		";
		if(isset($_SESSION['max_rol'])&& $_SESSION['max_rol'] <= 4 ){
			$query_TiempoPromedio .= " AND h.dealer_id = $dealer_id";
		}
		$query_TiempoPromedio .= " GROUP BY n.section";
		$Rows_TiempoPromedio = $DataBase_Acciones->SQL_SelectMultipleRows($query_TiempoPromedio);

		$query_CantidadUsr = "SELECT n.section, count(distinct n.user_id) as cantidadUsuarios
		FROM ludus_navigation n, ludus_users u, ludus_headquarters h
		WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND
		n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
		";
		if(isset($_SESSION['max_rol'])&& $_SESSION['max_rol'] <= 4 ){
			$query_CantidadUsr .= " AND h.dealer_id = $dealer_id";
		}
		$query_CantidadUsr .= " GROUP BY n.section";
		$Rows_CantidadUsr = $DataBase_Acciones->SQL_SelectMultipleRows($query_CantidadUsr);

		for($i=0;$i<count($Rows_config);$i++) {
			$section = $Rows_config[$i]['section'];
				for($y=0;$y<count($Rows_TiempoTotal);$y++) {
					if( $section == $Rows_TiempoTotal[$y]['section'] ){
						$Rows_config[$i]['tiempoTotal'] = $Rows_TiempoTotal[$y]['tiempoTotal'];
					}
				}
				for($y=0;$y<count($Rows_TiempoPromedio);$y++) {
					if( $section == $Rows_TiempoPromedio[$y]['section'] ){
						$Rows_config[$i]['tiempoPromedio'] = $Rows_TiempoPromedio[$y]['tiempoPromedio'];
					}
				}
				for($y=0;$y<count($Rows_CantidadUsr);$y++) {
					if( $section == $Rows_CantidadUsr[$y]['section'] ){
						$Rows_config[$i]['cantidadUsuarios'] = $Rows_CantidadUsr[$y]['cantidadUsuarios'];
					}
				}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/

	// function VisitantesUnicos($start_date, $end_date, $dealer){
	// 	include_once('../config/database.php');
	// 	include_once('../config/config.php');
	// 	@session_start();

	// 	if ($dealer == '') {

	// 			$query_sql = "SELECT count(distinct n.user_id) as cantidad
	// 			FROM ludus_navigation n, ludus_users u, ludus_headquarters h
	// 			WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND
	// 			n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id ";

	// 	}else{

	// 			$query_sql = "SELECT count(distinct n.user_id) as cantidad
	// 			FROM ludus_navigation n, ludus_users u, ludus_headquarters h
	// 			WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND
	// 			n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
	// 			AND h.dealer_id = $dealer";
	// 	}

	// 	$DataBase_Acciones = new Database();
	// 	$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
	// 	unset($DataBase_Acciones);
	// 	return $Rows_config;
	// }

	// function DetalleVisitas($start_date, $end_date){
	// 	include_once('../config/database.php');
	// 	include_once('../config/config.php');
	// 	@session_start();
	// 	$dealer_id = $_SESSION['dealer_id'];

	// 	$query = "SELECT u.user_id, u.first_name, u.last_name, u.identification, u.phone, u.status_id, h.headquarter, d.dealer, u.email, n.section, n.date_navigation, n.time_navigation
	// 			FROM ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
	// 			WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND n.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id
	// 			";
	// 	if(isset($_SESSION['max_rol'])&& $_SESSION['max_rol'] <= 4 ){
	// 		$query .= " AND h.dealer_id = $dealer_id";
	// 	}
	// 	$query .= " ORDER BY d.dealer,h.headquarter,u.first_name, u.last_name,n.section";
	// 	$DataBase_Acciones = new Database();
	// 	$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);

	// 	$query_Cargos = "SELECT c.charge as cargo, cu.user_id FROM ludus_charges_users cu, ludus_charges c WHERE cu.charge_id = c.charge_id AND c.status_id = 1 ORDER BY cu.user_id, c.charge";
	// 	$Rows_cargos = $DataBase_Acciones->SQL_SelectMultipleRows($query_Cargos);

	// 	foreach ($Rows_config as $key => $value) {
	// 		$user_id = $value['user_id'];
	// 		//Cargos
	// 			foreach ($Rows_cargos as $key2 => $value2) {
	// 				if( $user_id == $value2['user_id'] ){
	// 					$Rows_config[$key]['charges'][] = $Rows_cargos[$key2];
	// 				}
	// 			}
	// 		//Cargos
	// 	}

	// 	/*for($i=0;$i<count($Rows_config);$i++) {
	// 		$user_id = $Rows_config[$i]['user_id'];
	// 		//Cargos
	// 			for($y=0;$y<count($Rows_cargos);$y++) {
	// 				if( $user_id == $Rows_cargos[$y]['user_id'] ){
	// 					$Rows_config[$i]['charges'][] = $Rows_cargos[$y];
	// 				}
	// 			}
	// 		//Cargos
	// 	}*/

	// 	unset($DataBase_Acciones);
	// 	return $Rows_config;
	// }


	//USUARIOS EN TRAYECTORIAS
/*	function consultaUsuarios($start_date, $end_date, $dealer ){
		include_once('../config/database.php');
		include_once('../config/config.php');

		if($dealer == ''){

				$query_sql = "SELECT DISTINCT lu.user_id
				FROM ludus_users lu, ludus_charges_users lcu, ludus_navigation n, ludus_headquarters h
				where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and
				lu.headquarter_id = h.headquarter_id AND
				lu.status_id = 1 and lcu.charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) and lu.user_id = lcu.user_id and lu.user_id = n.user_id ";
		}else{

				$query_sql = "SELECT DISTINCT lu.*
				FROM ludus_users lu, ludus_charges_users lcu, ludus_navigation n, ludus_headquarters h
				where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and
				lu.headquarter_id = h.headquarter_id AND
				lu.status_id = 1 and lcu.charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) and lu.user_id = lcu.user_id and lu.user_id = n.user_id
				AND h.dealer_id = $dealer";
		}

		// $query_sql = "SELECT DISTINCT lu.*
		// FROM ludus_users lu, ludus_charges_users lcu
		// where lu.status_id = 1 and lcu.charge_id not in (125,123) and lu.user_id = lcu.user_id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/

	/*function consultatiempoUsuarios($start_date, $end_date, $dealer){
		include_once('../config/database.php');
		include_once('../config/config.php');

		if($dealer == ''){

						$query_sql = "SELECT sum(ln.time_navigation) as tiempoTotal, h.dealer_id
						FROM ludus_navigation ln, ludus_users lu, ludus_headquarters h
						where date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and
						lu.headquarter_id = h.headquarter_id AND
						ln.user_id = lu.user_id and
						lu.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) )
						group by h.dealer_id ";


		}else{

			$query_sql = "SELECT sum(ln.time_navigation) as tiempoTotal, h.dealer_id
						FROM ludus_navigation ln, ludus_users lu, ludus_headquarters h
						where date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' and
						lu.headquarter_id = h.headquarter_id AND
						ln.user_id = lu.user_id and
						lu.user_id IN (SELECT user_id FROM ludus_charges_users WHERE charge_id in (23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99) )
						AND h.dealer_id = $dealer";

		}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//Todos los Usuarios del mismo concesionario activos
	function consulta_Usuarios_iteracion($start_date, $end_date, $dealer){
		include_once('../config/database.php');
		include_once('../config/config.php');

		if ($dealer == '') {

				$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer_id
				from ludus_users u, ludus_headquarters h, ludus_dealers d,

				(select count( distinct u.user_id) as iteracion, d.dealer_id
								from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
								where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
								and n.user_id = u.user_id
								and u.headquarter_id = h.headquarter_id
								and h.dealer_id = d.dealer_id
								and u.status_id = 1
								group by d.dealer
								order by dealer asc) as todos

				where u.headquarter_id = h.headquarter_id
				and h.dealer_id = d.dealer_id
				and todos.dealer_id = d.dealer_id
				and u.status_id = 1
				group by d.dealer
				order by dealer asc";


		}else{


			$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer
				from ludus_users u, ludus_headquarters h, ludus_dealers d,

				(select count( distinct u.user_id) as iteracion, d.dealer_id
								from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d
								where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
								and n.user_id = u.user_id
								and u.headquarter_id = h.headquarter_id
								and h.dealer_id = d.dealer_id
								and u.status_id = 1
								and d.dealer_id = $dealer
								group by d.dealer
								order by dealer asc) as todos

				where u.headquarter_id = h.headquarter_id
				and h.dealer_id = d.dealer_id
				and todos.dealer_id = d.dealer_id
				and u.status_id = 1
				and d.dealer_id = $dealer
				group by d.dealer
				order by dealer asc";

		}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/

		//Todos los Usuarios en trayectorias del mismo concesionario activos
	/*function consulta_GMAcademy_iteracion($start_date, $end_date, $dealer, $trayectoria){
		include_once('../config/database.php');
		include_once('../config/config.php');

		if ($trayectoria=='') {
			$trayectory = '23, 35, 137, 135, 32, 31, 36, 128, 141, 105, 68, 134, 93, 94, 89, 97, 98, 100, 49, 99';
		}else{
			$trayectory = $trayectoria;
		}

		if ($dealer == '') {
			$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer_id
				from ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu,

				(select count( distinct u.user_id) as iteracion, d.dealer_id
								from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu
								where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
								and lcu.charge_id  in ($trayectory)
								and u.user_id = lcu.user_id
								and n.user_id = u.user_id
								and u.headquarter_id = h.headquarter_id
								and h.dealer_id = d.dealer_id
								and u.status_id = 1
								group by d.dealer
								order by dealer asc) as todos

				where u.headquarter_id = h.headquarter_id
				and lcu.charge_id in ($trayectory)
				and u.user_id = lcu.user_id
				and h.dealer_id = d.dealer_id
				and todos.dealer_id = d.dealer_id
				and u.status_id = 1
				group by d.dealer
				order by dealer asc";

								}else{

									$query_sql = "select count( distinct u.user_id) as todos, d.dealer, todos.iteracion as con_iteracion, d.dealer_id
				from ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu,

				(select count( distinct u.user_id) as iteracion, d.dealer_id
								from ludus_navigation n, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu
								where n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
								and lcu.charge_id in ($trayectory)
								and u.user_id = lcu.user_id
								and n.user_id = u.user_id
								and u.headquarter_id = h.headquarter_id
								and h.dealer_id = d.dealer_id
								and d.dealer_id = $dealer
								and u.status_id = 1
								group by d.dealer
								order by dealer asc) as todos

				where u.headquarter_id = h.headquarter_id
				and lcu.charge_id in ($trayectory)
				and u.user_id = lcu.user_id
				and h.dealer_id = d.dealer_id
				and todos.dealer_id = d.dealer_id
				and u.status_id = 1
				and d.dealer_id = $dealer
				group by d.dealer
				order by dealer asc;";

								}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/




	// function consulta_visitas1_Ajax($start_date, $end_date){
	// 	@session_start();
	// 	include_once('../../config/database.php');
	// 	include_once('../../config/config.php');
	// 	//$dealer_id = $_SESSION['dealer_id'];
	// 	$query_sql = "	SELECT YEAR(n.date_navigation) as ano, MONTH(n.date_navigation) as mes, count(n.navigation_id) as visitas
	// 						FROM ludus_navigation n
	// 						WHERE n.date_navigation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
	// 						GROUP BY YEAR(n.date_navigation), MONTH(n.date_navigation)
	// 						ORDER BY YEAR(n.date_navigation), MONTH(n.date_navigation);";
	// 	$DataBase_Acciones = new Database();
	// 	$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
	// 	unset($DataBase_Acciones);
	// 	return $Rows_config;
	// }





}
