<?php
Class RepGlobales {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT charge, charge_id FROM ludus_charges WHERE status_id = 1 ORDER BY charge";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	
	function consultaDatosAll($charge_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT c.course_id, c.newcode, c.course,  AVG(m_r.score) as avg_score
					FROM ludus_courses c, ludus_modules m, ludus_charges_courses cc, ( SELECT u.user_id, mr.module_id, max(mr.score) as score
					FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr
					WHERE u.headquarter_id = h.headquarter_id AND u.status_id = 1 AND u.user_id = mr.user_id
					AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					GROUP BY u.user_id, mr.module_id ) AS m_r
					WHERE m_r.module_id = m.module_id AND m.course_id = c.course_id AND cc.course_id = c.course_id
					GROUP BY c.course";//LIMIT 0,20
		}else{
			$query_sql = "SELECT c.course_id, c.newcode, c.course,  AVG(m_r.score) as avg_score
					FROM ludus_courses c, ludus_modules m, ludus_charges_courses cc, ( SELECT u.user_id, mr.module_id, max(mr.score) as score
					FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
					AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
					GROUP BY u.user_id, mr.module_id ) AS m_r
					WHERE m_r.module_id = m.module_id AND m.course_id = c.course_id AND cc.course_id = c.course_id
					GROUP BY c.course";//LIMIT 0,20
		}
		//echo $query_sql;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		for($i=0;$i<count($Rows_config);$i++) {
			$query_Resultado = "SELECT cc.charge_id, c.charge
						FROM ludus_charges_courses cc, ludus_charges c
						WHERE cc.course_id = ".$Rows_config[$i]['course_id']." AND c.status_id = 1 AND cc.charge_id = c.charge_id";
			$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
			$Rows_config[$i]['cargos'] = $Rows_cant;
			//Verifica cuantos usuarios deberían haberlo visto
			//Verifica cuantos usuarios deberían haberlo visto

			//Verifica cuantos usuarios lo vieron
			$query_Resultado = "SELECT count(u.user_id) as usr_pres
						FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr, ludus_modules m
						WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '2' AND u.status_id = 1 AND u.user_id = mr.user_id
						AND mr.date_creation BETWEEN '2014-01-01 00:00:00' AND '2015-04-30 23:59:59' AND mr.module_id = m.module_id AND m.course_id = ".$Rows_config[$i]['course_id'];
			$Rows_cant = $DataBase_Acciones->SQL_SelectRows($query_Resultado);
			$Rows_config[$i]['usr_pres'] = $Rows_cant['usr_pres'];
			//Verifica cuantos usuarios lo vieron

			//Verifica maxima nota
			$query_Resultado = "SELECT c.newcode, c.course, max(m_r.score) as max_score
						FROM ludus_courses c, ludus_modules m, ( SELECT u.user_id, mr.module_id, max(mr.score) as score
						FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr
						WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
						AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
						GROUP BY u.user_id, mr.module_id ) AS m_r
						WHERE c.course_id = ".$Rows_config[$i]['course_id']." AND m_r.module_id = m.module_id AND m.course_id = c.course_id
						GROUP BY c.newcode, c.course";
			$Rows_cant = $DataBase_Acciones->SQL_SelectRows($query_Resultado);
			$Rows_config[$i]['max_score'] = $Rows_cant['max_score'];
			//Verifica maxima nota

			//Verifica minima nota
			$query_Resultado = "SELECT c.newcode, c.course, min(m_r.score) as min_score
						FROM ludus_courses c, ludus_modules m, ( SELECT u.user_id, mr.module_id, max(mr.score) as score
						FROM ludus_users u, ludus_headquarters h, ludus_modules_results_usr mr
						WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND u.status_id = 1 AND u.user_id = mr.user_id
						AND mr.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
						GROUP BY u.user_id, mr.module_id ) AS m_r
						WHERE c.course_id = ".$Rows_config[$i]['course_id']." AND m_r.module_id = m.module_id AND m.course_id = c.course_id
						GROUP BY c.newcode, c.course";
			$Rows_cant = $DataBase_Acciones->SQL_SelectRows($query_Resultado);
			$Rows_config[$i]['min_score'] = $Rows_cant['min_score'];
			//Verifica minima nota
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
