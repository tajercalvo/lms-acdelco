<?php
class ReiniciarActividades
{

	function getCoursesTeacher($p)
	{
		extract($p);
		include_once('../../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		//print_r($_SESSION);
		$rol = false;
		foreach ($_SESSION['id_rol'] as $key => $value) {
			if ($value['rol_id'] == 1) {
				$rol = true;
				break;
			}
		}

		//var_dump($rol);
		$query_aux = $rol ? '' : " and sch.teacher_id = $idQuien ";
		$query = "SELECT mo.module_id, mo.module, co.course_id, co.course, sch.schedule_id, sch.start_date, sch.end_date, sch.status_id, sch.teacher_id
									from ludus_modules mo
									INNER join ludus_schedule sch
										on mo.module_id = sch.module_id
									INNER join ludus_courses co
										on mo.course_id = co.course_id
									WHERE 
									sch.start_date BETWEEN '$fecha_inicial 00:00:00' and '$fecha_final 23:59:59'
									$query_aux	 
									GROUP BY sch.schedule_id";

		$Rows_config = DB::query($query);
		return $Rows_config;
	} // fin CrearCalificacion

	function getAcitivitieUser($p)
	{
		extract($p);
		include_once('../../config/init_db.php');
		@session_start();
		$query_ModulesCourse = "SELECT u.user_id, u.identification, u.first_name, u.last_name, rean.date_creation, actus.*
									from  ludus_activities_user actus
												inner join ludus_users u
													on u.user_id = actus.user_id
												inner join ludus_activities_schedule act
													on act.activity_schedule_id = actus.activity_schedule_id
												inner join ludus_reviews_answer rean
													on rean.reviews_answer_id = actus.reviews_answer_id
										where act.schedule_id = $schedule_id;";
		$Rows_config = DB::query($query_ModulesCourse);
		return $Rows_config;
	} // fin CrearCalificacion


	function reiniciar_evaluacion($activities_user_id)
	{
		include_once('../../config/init_db.php');
		@session_start();

		$query_sel = "SELECT reviews_answer_id from ludus_activities_user where activities_user_id = $activities_user_id ;";
		$res = DB::queryFirstRow($query_sel);
		$reviews_answer_id = $res['reviews_answer_id'];

		//1) se borra la actividad realizada por el usuario
		$query_ModulesCourse = "DELETE from ludus_activities_user where activities_user_id = $activities_user_id;";
		$Rows_config = DB::query($query_ModulesCourse);

		// 2) se borra el detalle de las respuestas del usuario
		$query_del_re_de = "DELETE from  ludus_reviews_answer_detail where reviews_answer_id = $reviews_answer_id;";
		DB::query($query_del_re_de);

		// 3)  se borra la evaluacion
		$query_del_re = "DELETE from ludus_reviews_answer where reviews_answer_id = $reviews_answer_id";
		DB::query($query_del_re);

		$json = array();
		if ($Rows_config) {
			$json['error'] = false;
			$json['type'] = "success";
			$json['msj'] = 'Evaluacion reiniciada correctamente';
		} else {
			$json['error'] = true;
			$json['type'] = "error";
			$json['msj'] = "No se pudo reiniciar la evaluación";
		}
		return $json;
	} // fin CrearCalificacion
}
