<?php
Class RepEncuestas {
	function ConsultaPreguntas($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT q.code, q.question, q.question_id
					FROM ludus_reviews_questions rq, ludus_questions q
					where rq.review_id = ".$review_id." AND rq.question_id = q.question_id AND q.status_id = 1 AND q.type = 2";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_Resp = "SELECT q.code, q.question, a.answer, q.question_id, a.answer_id
					FROM ludus_reviews_questions rq, ludus_questions q, ludus_answers a 
					where rq.review_id = ".$review_id." AND rq.question_id = q.question_id AND q.status_id = 1 AND q.type = 2 AND q.question_id = a.question_id";
		$Rows_Resp = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resp);

		$query_Resu = "SELECT ans.answer_id, count(*) as Cantidad FROM ludus_reviews_answer a, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers ans WHERE a.review_id = '$review_id' AND a.reviews_answer_id = ad.reviews_answer_id AND a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ad.question_id = q.question_id AND ad.answer_id = ans.answer_id group by ans.answer_id";
		$Rows_Resu = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resu);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_question_id = $Rows_config[$i]['question_id'];
			for($y=0;$y<count($Rows_Resp);$y++) {
				if( $var_question_id == $Rows_Resp[$y]['question_id'] ){
					$var_answer_id = $Rows_Resp[$y]['answer_id'];
					for($z=0;$z<count($Rows_Resu);$z++) {
						if($var_answer_id == $Rows_Resu[$z]['answer_id']){
							$Rows_Resp[$y]['CantResp'][] = $Rows_Resu[$z];
						}
					}
					$Rows_config[$i]['Resp'][] = $Rows_Resp[$y];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatosAll($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryPreg = "SELECT distinct q.question, q.question_id
					FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers a
					WHERE ra.review_id = 2 AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = q.question_id AND ad.answer_id = a.answer_id AND ra.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' 
					GROUP BY q.question, q.question_id
					ORDER BY q.question, q.question_id ";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryPreg);


		$query_sql = "SELECT YEAR( ra.date_creation ) AS ANO, MONTH( ra.date_creation ) AS MES, q.question, q.question_id, a.answer, count(*) AS cantidad
					FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers a
					WHERE ra.review_id = 2 AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = q.question_id AND ad.answer_id = a.answer_id AND ra.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' 
					GROUP BY YEAR( ra.date_creation ) , MONTH( ra.date_creation ), q.question, q.question_id, a.answer
					ORDER BY q.question, a.answer, YEAR( ra.date_creation ), MONTH( ra.date_creation ) ";
		$Rows_Resp = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_periodos = "SELECT YEAR( ra.date_creation ) AS ANO, MONTH( ra.date_creation ) AS MES
					FROM ludus_reviews_answer ra, ludus_reviews_answer_detail ad, ludus_questions q, ludus_answers a
					WHERE ra.review_id = 2 AND ra.reviews_answer_id = ad.reviews_answer_id AND ad.question_id = q.question_id AND ad.answer_id = a.answer_id AND ra.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' 
					GROUP BY YEAR( ra.date_creation ) , MONTH( ra.date_creation )
					ORDER BY YEAR( ra.date_creation ), MONTH( ra.date_creation ) ";
		$Rows_Peri = $DataBase_Acciones->SQL_SelectMultipleRows($query_periodos);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_question_id = $Rows_config[$i]['question_id'];
			$Rows_config[$i]['Perio'] = $Rows_Peri;
			for($y=0;$y<count($Rows_Resp);$y++) {
				if( $var_question_id == $Rows_Resp[$y]['question_id'] ){
					$Rows_config[$i]['Resp'][] = $Rows_Resp[$y];
				}
			}
		}
		
		
		
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function EvaluacionesEncuestas($tipo){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_evaluaciones = "SELECT * FROM ludus_reviews WHERE type IN ($tipo) ";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_evaluaciones);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
