<?php
Class Cargos {
	function consultaDatoscargos($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.charge_id, c.charge, c.status_id, c.date_creation, c.group_id, c.date_edition, u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, cc.category, sc.school
					FROM ludus_charges c
                    	INNER JOIN ludus_users u
                    		ON c.creator = u.user_id
                        INNER JOIN ludus_users s
                        	ON c.editor = s.user_id
                        INNER JOIN ludus_status e
                        	ON c.status_id = e.status_id
                       INNER JOIN ludus_charges_categories cc
                       		ON c.category_id = cc.category_id
                       INNER JOIN ludus_school sc
                      		ON c.school_id = sc.school_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.charge LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' )";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.charge_id, c.charge, c.status_id, c.date_creation, c.group_id, c.date_edition, u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, cc.category, sc.school
					FROM ludus_charges c
                    	INNER JOIN ludus_users u
                    		ON c.creator = u.user_id
                        INNER JOIN ludus_users s
                        	ON c.editor = s.user_id
                        INNER JOIN ludus_status e
                        	ON c.status_id = e.status_id
                       INNER JOIN ludus_charges_categories cc
                       		ON c.category_id = cc.category_id
                       INNER JOIN ludus_school sc
                      		ON c.school_id = sc.school_id ";
			if($sWhere!=''){
				$query_sql .= " AND (c.charge LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' )";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.charge_id, c.charge, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_charges c, ludus_users u, ludus_users s
			WHERE c.creator = u.user_id AND c.editor = s.user_id
			ORDER BY charge ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearcargos($nombre){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_charges
						(charge,status_id,creator,editor,date_creation,date_edition,category_id,group_id,school_id)
						VALUES
						('$nombre','1','$idQuien','$idQuien','$NOW_data','$NOW_data',1,'1', 1)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarcargos($id,$nombre,$status){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_cargos.php?opcn=ver&id=$id&back=inicio>$id</a>] a cargos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_charges` SET charge = '$nombre', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data', category_id = 1, group_id = '1', school_id = 1 WHERE charge_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_charges c
			WHERE c.charge_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaCategorias(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_charges_categories c";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin consultaCategorias

	function consultaEscuelas(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM `ludus_school` where status_id = 1";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

}
