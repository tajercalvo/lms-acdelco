<?php
Class Asistencia {

	public static function getChat(){
		include_once( '../../config/init_db.php' );
		@session_start();
		$query_sql = "SELECT ch.*, u.first_name, u.last_name, u.image FROM ludus_chat_asistencia_tecnica ch INNER join ludus_users u on ch.creator = u.user_id and ch.ticket = 0;";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}//fin getDatosCurso

	public static function insertarChat($p){
		include_once( '../../config/init_db.php' );
		@session_start();
		$quien = $_SESSION['idUsuario'];
		$message = $p['message'];
		$query_sql = "INSERT INTO ludus_chat_asistencia_tecnica
									(
									message,
									creator,
									creation
									)
									VALUES
									(
									'$message',
									$quien,
									NOW()
									);
		";
		$Rows_config = DB::query($query_sql);

		if( $Rows_config ){
			$res['error'] = false;
			$res['msj'] = 'si';
		}else{
			$res['error'] = true;
			$res['msj'] = 'no';
		}
		return $res;
	}//fin getDatosCurso

	public static function cerrar_ticket($p){
		include_once( '../../config/init_db.php' );
		@session_start();
		$query_sql = "SELECT max( ticket ) ultimo from ludus_chat_asistencia_tecnica";
		$Rows_config = DB::query($query_sql);

		if( count($Rows_config) > 0){
			$ticket = $Rows_config[0]['ultimo'] + 1;
			$query_sql_ticket = "UPDATE ludus_chat_asistencia_tecnica set ticket = $ticket where ticket = 0";
			$Rows_config_ticket = DB::query($query_sql_ticket);

			if( $Rows_config_ticket ){
				$res['error'] = false;
				$res['msj'] = 'Chat actualizado';
			}else{
				$res['error'] = true;
				$res['msj'] = 'No se pudo actualizar el chat';
			}

		}else{
			$res['error'] = true;
			$res['msj'] = 'No existe chat';
		}

		return $res;
	}//fin getDatosCurso


}
