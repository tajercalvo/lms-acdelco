<?php

declare(strict_types=1);
class Jobuser
{
    public static function addUser(): array
    {
        include_once('../../config/init_db.php');
        DB::$error_handler = false;
        DB::$throw_exception_on_error = true;
        try {
            $userApp = DB::query("SELECT
                                        identificacion AS identification,
                                        apellidos AS last_name,
                                        nombres AS first_name,
                                        email,
                                        telefono AS mobile_phone,
                                        NOW() AS date_birthday,
                                        genero AS gender, 
                                        trayectoria, 
                                        'on' AS treatment_policy,
                                        'Colombia' as pais
                                    FROM
                                        user_app
                                    WHERE
                                        identificacion NOT IN(
                                        SELECT
                                            identification
                                        FROM
                                            ludus_users
                                    )");
        } catch (MeekroDBException $e) {
            echo "Error: " . $e->getMessage() . "<br>\n"; // something about duplicate keys
            echo "SQL Query: " . $e->getQuery() . "<br>\n"; // INSERT INTO accounts...
        }
        DB::$error_handler = 'meekrodb_error_handler';
        DB::$throw_exception_on_error = false;
        DB::disconnect();
        return $userApp;
    }
}
