<?php
Class Salones {
	function consultaDatosAll($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.living_id, c.living, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, z.area, c.address, c.vacant
			FROM ludus_livings c, ludus_users u, ludus_users s, ludus_status e, ludus_areas z
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id AND c.city_id = z.area_id";
			if($sWhere!=''){
				$query_sql .= " AND (c.living LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.area LIKE '%$sWhere%' OR c.address LIKE '%$sWhere%' OR c.vacant LIKE '%$sWhere%') ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
			//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.living_id, c.living, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, z.area, c.address, c.vacant
			FROM ludus_livings c, ludus_users u, ludus_users s, ludus_status e, ludus_areas z
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id AND c.city_id = z.area_id";
			if($sWhere!=''){
				$query_sql .= " AND (c.living LIKE '%$sWhere%' OR e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.area LIKE '%$sWhere%' OR c.address LIKE '%$sWhere%' OR c.vacant LIKE '%$sWhere%') ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.living_id, c.living, c.status_id, c.date_creation, c.date_edition,
		u.first_name, u.last_name, s.first_name as nom_edita, s.last_name as ape_edita, z.area, c.address, c.vacant
			FROM ludus_livings c, ludus_users u, ludus_users s, ludus_status e, ludus_areas z
			WHERE c.creator = u.user_id AND c.editor = s.user_id AND c.status_id = e.status_id AND c.city_id = z.area_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearSalones($nombre,$address,$vacant,$city_id){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_livings (living,status_id,creator,editor,date_creation,date_edition,address,vacant,city_id,headquarter_id) VALUES ('$nombre','1','$idQuien','$idQuien','$NOW_data','$NOW_data','$address','$vacant','$city_id','1')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarSalones($id,$nombre,$status,$address,$vacant,$city_id){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_livings SET living = '$nombre', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data',address = '$address',vacant = '$vacant',city_id = '$city_id' WHERE living_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_livings c
			WHERE c.living_id = '$idRegistro' ";//LIMIT 0,20
			//echo $query_sql;
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCiudades($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT concat(a.area, ' - ', z.zone) city, a.area_id city_id
						FROM ludus_areas a
							inner join ludus_zone z
								on a.zone_id = z.zone_id
								and a.status_id = 1
						ORDER BY a.area";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaConcesionarios($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.*
						FROM ludus_headquarters c
						WHERE c.status_id = 1
						ORDER BY c.headquarter";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
}
