<?php
Class Timeline {

	function Tl_User($id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT first_name,last_name, image, email 
		FROM ludus_users
		 where identification = $id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function ultimanavegacion($id_user){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql= "SELECT MAX(ln.date_navigation) AS ultimavez
						FROM ludus_navigation ln, ludus_users lu
						WHERE ln.user_id = lu.user_id
                        and lu.identification = $id_user";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	

	function Tl_User_Charge($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT 'cargo' as evento, lcu.date_creation as fecha, lc.charge as name, ' ' as descripcion
		FROM ludus_users lu, ludus_charges_users lcu, ludus_charges lc
		where lcu.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
		and lc.charge_id = lcu.charge_id 
 		and lcu.user_id = lu.user_id and lu.identification = $id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_excuses($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT 'excusa' as evento, ls.start_date as fecha, le.file as name, lc.course as descripcion
		FROM ludus_excuses le, ludus_schedule ls, ludus_users lu, ludus_courses lc
		where ls.start_date between '$start_date 00:00:00' and '$end_date 23:59:59'
		and le.user_id = lu.user_id
		and lc.course_id = ls.course_id
		and le.schedule_id = ls.schedule_id
		and lu.identification = $id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_forum($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT  'foro' as evento, max(lfr.date_creation) as fecha,  lf.forum as name, COUNT(lfr.forum_reply_id) as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM  ludus_forums lf, ludus_forums_reply lfr, ludus_users lu
        WHERE  lf.forum_id = lfr.forum_id and lfr.user_id = lu.user_id and lfr.forum_id = 2 and lu.identification = $id and
		lfr.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
        union
		SELECT  'foro' as evento, max(lfr.date_creation) as fecha, lf.forum as name, COUNT(lfr.forum_reply_id) as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM  ludus_forums lf, ludus_forums_reply lfr, ludus_users lu
        WHERE  lf.forum_id = lfr.forum_id and lfr.user_id = lu.user_id and lfr.forum_id = 3 and lu.identification = $id and
		lfr.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
        union
		SELECT  'foro' as evento, max(lfr.date_creation) as fecha, lf.forum as name, COUNT(lfr.forum_reply_id) as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM  ludus_forums lf, ludus_forums_reply lfr, ludus_users lu
        WHERE  lf.forum_id = lfr.forum_id and lfr.user_id = lu.user_id and lfr.forum_id = 4 and lu.identification = $id and
		lfr.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
        union
		SELECT  'foro' as evento, max(lfr.date_creation) as fecha, lf.forum as name, COUNT(lfr.forum_reply_id) as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM  ludus_forums lf, ludus_forums_reply lfr, ludus_users lu
        WHERE  lf.forum_id = lfr.forum_id and lfr.user_id = lu.user_id and lfr.forum_id = 5 and lu.identification = $id and
		lfr.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
        union
		SELECT  'foro' as evento, max(lfr.date_creation) as fecha, lf.forum as name, COUNT(lfr.forum_reply_id) as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM  ludus_forums lf, ludus_forums_reply lfr, ludus_users lu
        WHERE  lf.forum_id = lfr.forum_id and lfr.user_id = lu.user_id and lfr.forum_id = 6 and lu.identification = $id and
		lfr.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
        union
		SELECT  'foro' as evento, max(lfr.date_creation) as fecha, lf.forum as name, COUNT(lfr.forum_reply_id) as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM  ludus_forums lf, ludus_forums_reply lfr, ludus_users lu
        WHERE  lf.forum_id = lfr.forum_id and lfr.user_id = lu.user_id and lfr.forum_id = 7 and lu.identification = $id and
		lfr.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_invitation($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT 'invitacion' as evento, li.date_send as fecha, lc.course as name, li.status_id as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM ludus_invitation li, ludus_schedule ls, ludus_courses lc, ludus_users lu
		where li.date_send between '$start_date 00:00:00' and '$end_date 23:59:59'
		and lc.course_id = ls.course_id
		and ls.schedule_id = li.schedule_id
		and li.user_id = lu.user_id
		and lu.identification = $id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_library_likes($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT 'like' as evento, lll.date_edition as fecha, ll.name as name, ' ' as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM ludus_libraries ll, ludus_library_likes lll, ludus_users lu
		where lll.date_edition between '$start_date 00:00:00' and '$end_date 23:59:59' 
		and ll.library_id  = lll.library_id 
		and lll.user_id = lu.user_id 
		and lu.identification = $id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_library_views($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT distinct 'visto' as evento, llv.date_edition as fecha, ll.name as name, ' ' as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM ludus_libraries ll, ludus_library_views llv, ludus_users lu
		where llv.date_edition between '$start_date 00:00:00' and '$end_date 23:59:59'
		and ll.library_id  = llv.library_id 
		and llv.user_id = lu.user_id 
		and lu.identification = $id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_navigation($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query = "SELECT 'navegacion' as evento, ln.date_navigation as fecha, ln.section as name, ln.time_navigation as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM ludus_navigation ln, ludus_users lu
		where ln.date_navigation between '$start_date 00:00:00' and '$end_date 23:59:59'
		and  ln.user_id = lu.user_id 
		and lu.identification = $id ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_answer($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query = "SELECT 'answer' as evento, lr.date_edition as fecha, lr.review as name, ' ' as descripcion, lra.score as score, lra.time_response as time_r, lra.reviews_answer_id as idEvento
		FROM ludus_reviews lr,ludus_reviews_answer lra, ludus_users lu
		where lr.date_edition between '$start_date 00:00:00' and '$end_date 23:59:59'
		and lr.review_id = lra.review_id
		and lra.user_id = lu.user_id
		and lu.identification = $id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_modules_results($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query = "SELECT lmru.module_result_usr_id as evento, lmru.date_creation as fecha, lmru.score as name, lm.module as descripcion, '' as score, '' as time_r, '' as idEvento
		from ludus_modules lm,ludus_modules_results_usr lmru, ludus_users lu
		where lmru.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
		and lm.module_id = lmru.module_id 
		and lmru.user_id = lu.user_id
		and lu.identification = $id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Tl_User_waybills($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query = "SELECT 'Hruta' as evento, lw.date_creation as fecha,  lm.module as name,lw.score as descripcion, '' as score, '' as time_r, '' as idEvento
		FROM ludus_waybills lw, ludus_users lu, ludus_modules lm, ludus_modules_results_usr lmru
		where lw.date_creation between '$start_date 00:00:00' and '$end_date 23:59:59'
        and lm.module_id = lmru.module_id
		and lmru.module_result_usr_id = lw.module_result_usr_id
		and lw.user_id = lu.user_id
		and lu.identification = $id;";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function BorrarEvaluacion($reviews_answer_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$QueryABorrar = "SELECT * FROM ludus_reviews_answer WHERE reviews_answer_id = '$reviews_answer_id'";
		//echo($QueryABorrar);
		$DataBase_Acciones = new Database();
		$Row_config = $DataBase_Acciones->SQL_SelectRows($QueryABorrar);
		if(isset($Row_config['review_id'])){
			$review_id = $Row_config['review_id'];
			$user_id = $Row_config['user_id'];
			$available_score = $Row_config['available_score'];
			$score = $Row_config['score'];
			$date_creation = $Row_config['date_creation'];
			$time_response = $Row_config['time_response'];
			$module_result_usr_id = $Row_config['module_result_usr_id'];
			$date_eraser = date("Y-m-d")." ".(date("H")).":".date("i:s");
			$InsertBorrar = "INSERT INTO ludus_reviews_answer_eraser VALUES ($reviews_answer_id,$review_id,$user_id,$available_score,$score,'$date_creation','$time_response',$module_result_usr_id,$idQuien,'$date_eraser');";
			$resultado = $DataBase_Acciones->SQL_Insert($InsertBorrar);
			$DeleteDetail = "DELETE FROM ludus_reviews_answer_detail WHERE reviews_answer_id = $reviews_answer_id ";
			$resultado = $DataBase_Acciones->SQL_Insert($DeleteDetail);
			$DeleteReview = "DELETE FROM ludus_reviews_answer WHERE reviews_answer_id = $reviews_answer_id ";
			$resultado = $DataBase_Acciones->SQL_Insert($DeleteReview);
		}
	}
	
}

/*function Tl_User_All($id, $start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query = "(SELECT 'cargo' as evento, lcu.date_creation as fecha, lc.charge as name, ' ' as descripcion
		FROM ludus_users lu, ludus_charges_users lcu, ludus_charges lc
		where lcu.date_creation between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
		and lc.charge_id = lcu.charge_id 
 		and lcu.user_id = lu.user_id and lu.identification = 1082889369)
        union
        (SELECT 'excusa' as evento, ls.start_date as fecha, le.file, lc.course as descripcion
		FROM ludus_excuses le, ludus_schedule ls, ludus_users lu, ludus_courses lc
		where ls.start_date between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
		and le.user_id = lu.user_id
		and lc.course_id = ls.course_id
		and le.schedule_id = ls.schedule_id
		and lu.identification = 1082889369)
        union
        SELECT 'excusa' as evento, li.date_send, lc.course, li.status_id,   li.creator
		FROM ludus_invitation li, ludus_schedule ls, ludus_courses lc, ludus_users lu
		where li.date_send between '2015-01-20 04:45:58' and '2016-03-20 04:45:58'
		and lc.course_id = ls.course_id
		and ls.schedule_id = li.schedule_id
		and li.user_id = lu.user_id
		and lu.identification = 98567449
        union
        (SELECT 'like' as evento, lll.date_edition as fecha, ll.name, ' ' as descripcion
		FROM ludus_libraries ll, ludus_library_likes lll, ludus_users lu
		where lll.date_edition between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
		and ll.library_id  = lll.library_id 
		and lll.user_id = lu.user_id 
		and lu.identification = 1082889369)
        union
       (SELECT distinct 'visto' as evento, llv.date_edition as fecha, ll.name, ' ' as descripcion
		FROM ludus_libraries ll, ludus_library_views llv, ludus_users lu
		where llv.date_edition between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
		and ll.library_id  = llv.library_id 
		and llv.user_id = lu.user_id 
		and lu.identification = 1082889369)
        union
        (SELECT 'navegacion' as evento, ln.date_navigation as fecha, ln.section, ln.time_navigation as descripcion
		FROM ludus_navigation ln, ludus_users lu
		where ln.date_navigation between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
		and  ln.user_id = lu.user_id 
		and lu.identification = 1082889369)
		union
        (SELECT 'answer' as evento, lr.date_edition as fecha, lr.review, ' ' as descripcion
		FROM ludus_reviews lr,ludus_reviews_answer lra, ludus_users lu
		where lr.date_edition between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
		and lr.review_id = lra.review_id
		and lra.user_id = lu.user_id
		and lu.identification = 1082889369)
        union
        (SELECT lmru.module_result_usr_id, lmru.date_creation as fecha, lmru.score, lm.module as descripcion
		from ludus_modules lm,ludus_modules_results_usr lmru, ludus_users lu
		where lmru.date_creation between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
		and lmru.module_id = lm.module_id 
		and lmru.user_id = lu.user_id
		and lu.identification = 1082889369)
        union
        (SELECT 'Hruta' as evento, lw.date_creation as fecha,  lm.module,' ' as descripcion
		FROM ludus_waybills lw, ludus_users lu, ludus_modules lm, ludus_modules_results_usr lmru
		where lw.date_creation between '2016-07-13  00:00:00' and '2016-11-13  23:59:59'
        and lm.module_id = lmru.module_id
		and lmru.module_result_usr_id = lw.module_result_usr_id
		and lw.user_id = lu.user_id
		and lu.identification = 1082889369)
		ORDER BY fecha desc;
        ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query);
		unset($DataBase_Acciones);
		return $Rows_config;
	}*/

        