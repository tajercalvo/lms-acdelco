<?php
Class RepDisponibilidadHistorica {
	function consultaDatosAll($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT ds.dealer_schedule_id, s.schedule_id, c.newcode, c.course_id, c.course, u.first_name, u.last_name, s.start_date, s.end_date, l.living, d.dealer_id, zona.zone, d.dealer,
			ds.min_size, ds.max_size, ds.inscriptions, asis.asistencias as asistance
					FROM ludus_schedule s, ludus_courses c, ludus_users u, ludus_livings l, ludus_dealer_schedule ds,
					ludus_dealers d, 
					( SELECT DISTINCT h.dealer_id, z.zone FROM ludus_headquarters h, ludus_areas a, ludus_zone z WHERE h.area_id = a.area_id AND a.zone_id = z.zone_id ) AS zona,
					( SELECT h.dealer_id, i.schedule_id, count(*) as asistencias FROM ludus_invitation i, ludus_users u, ludus_headquarters h WHERE i.status_id = 2 AND i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id GROUP BY h.dealer_id, i.schedule_id ) asis
					WHERE s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND s.course_id = c.course_id AND
					s.teacher_id = u.user_id AND s.living_id = l.living_id AND s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id
					AND s.schedule_id = asis.schedule_id AND d.dealer_id = asis.dealer_id AND d.dealer_id = zona.dealer_id
					ORDER BY s.start_date";//LIMIT 0,20
		}else{
			$query_sql = "SELECT ds.dealer_schedule_id, s.schedule_id, c.newcode, c.course_id, c.course, u.first_name, u.last_name, s.start_date, s.end_date, l.living, d.dealer_id, d.dealer,
			ds.min_size, ds.max_size, ds.inscriptions, asis.asistencias as asistance
					FROM ludus_schedule s, ludus_courses c, ludus_users u, ludus_livings l, ludus_dealer_schedule ds,
					ludus_dealers d, (SELECT h.dealer_id, i.schedule_id, count(*) as asistencias FROM ludus_invitation i, ludus_users u, ludus_headquarters h WHERE i.status_id = 2 AND i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id GROUP BY h.dealer_id, i.schedule_id) asis
					WHERE s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND s.course_id = c.course_id AND
					s.teacher_id = u.user_id AND s.living_id = l.living_id AND s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id
					AND s.schedule_id = asis.schedule_id AND d.dealer_id = asis.dealer_id AND d.dealer_id = '$dealer_id'
					ORDER BY s.start_date";//LIMIT 0,20
		}
		// echo( "<br>$query_sql<br>" );
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function getAsistencia($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT h.dealer_id, i.*, s.start_date, m.module_id, u.first_name, u.last_name, u.identification
						FROM ludus_invitation i, ludus_schedule s, ludus_modules m, ludus_users u, ludus_headquarters h
						WHERE i.schedule_id = s.schedule_id
						AND i.user_id = u.user_id
						AND u.headquarter_id = h.headquarter_id
						AND s.course_id = m.course_id
						AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
		// echo( "<br>$query_sql<br>" );
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function getDisponibilidad( $dealer_id, $course_id, $start_date ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_sql = "SELECT u.user_id, u.first_name, u.last_name, u.identification, c.charge, h.dealer_id
			FROM ludus_headquarters h, ludus_users u, ludus_charges_users cu, ludus_charges_courses cc, ludus_modules m, ludus_charges c
			WHERE h.dealer_id = $dealer_id
			AND h.headquarter_id = u.headquarter_id
			AND u.user_id = cu.user_id
			AND cu.charge_id = cc.charge_id
			AND cc.course_id = $course_id
			AND cc.course_id = m.course_id
			AND cu.charge_id = c.charge_id
			AND u.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m WHERE m.module_id = mr.module_id AND m.course_id = $course_id AND mr.date_creation < '$start_date')";
		// echo( "<br>".$query_sql."<br>" );
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
