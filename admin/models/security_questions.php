<?php
Class Securities {
	function consultaQuestions($from, $to){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_security_question WHERE security_question_id BETWEEN '$from' AND '$to' ORDER BY rand() ";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaActualQuestions(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT * FROM ludus_security_answer WHERE user_id = $idQuien ORDER BY security_question_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearQuestion($question,$answer){

		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$insertSQL_EE = "INSERT INTO `ludus_security_answer`
		(security_question_id,user_id,answer,creator,date_creation)
		VALUES ('$question','$idQuien','$answer','$idQuien','$NOW_data');";

		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}


	// Inicio de funcion para actualizar preguntas de seguridad, Recibe tres parametros del contorlador security_questions
	// por Juan Carlos Villar

	function ActualizarQuestion($question_id,$answer,$security_answer_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$query_sql = "	UPDATE ludus_security_answer
		SET `security_question_id`='$question_id', `user_id`='$idQuien', `answer`='$answer', `creator`='$idQuien', `date_creation`='$NOW_data'
		WHERE `security_answer_id`='$security_answer_id';";
		$resultado = $DataBase_Log->SQL_Update($query_sql);
		unset($DataBase_Log);
		return $resultado;
	}



	function consultaForos($cargos_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email, s.first_name as first_post, s.last_name as last_post, s.image as img_post, s.user_id as user_post_id
						FROM ludus_forums m, ludus_users u, ludus_users s
						WHERE m.creator = u.user_id AND m.last_post_user_id = s.user_id
						ORDER BY m.category, m.position";
		}else{
			$query_sql = "SELECT distinct m.*, u.first_name, u.last_name, u.image as image_usr, u.email, s.first_name as first_post, s.last_name as last_post, s.image as img_post
						FROM ludus_forums m, ludus_users u, ludus_users s, ludus_forum_charges c
						WHERE m.status_id = 1 AND m.creator = u.user_id AND m.last_post_user_id = s.user_id AND m.forum_id = c.forum_id AND c.charge_id IN ($cargos_id)
						ORDER BY m.category, m.position";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function ActualizarForo($id,$forum,$forum_description,$category,$estado,$position){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_forums`
						SET forum = '$forum',
							forum_description = '$forum_description',
							creator = '$idQuien',
							date_creation = '$NOW_data',
							category = '$category',
							position = '$position',
							status_id = '$estado'
						WHERE forum_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaForo($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_forums c
			WHERE c.forum_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		$query_Charges = "SELECT charge_id
						FROM ludus_forum_charges
						WHERE forum_id = '$idRegistro'
						ORDER BY charge_id ";
		$Rows_Charges = $DataBase_Class->SQL_SelectMultipleRows($query_Charges);
		$Rows_config['charges'][] = $Rows_Charges;
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatoGaleria($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_media c
			WHERE c.media_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearLike($forum_reply_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO `ludus_forums_reply_likes` (forum_reply_id,user_id,date_edition) VALUES ('$forum_reply_id','$idQuien','$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE `ludus_forums_reply`
						SET likes = likes+1
						WHERE forum_reply_id = '$forum_reply_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}
	function consultaMed(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_media m
					WHERE m.status_id = 1 AND m.type = 1
					ORDER BY m.media ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaGaleria($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT m.*
			FROM ludus_argument a, ludus_media_detail m
			WHERE a.argument_id = '$idRegistro' AND a.media_id = m.media_id ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaReply($idRegistro){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			$query_sql = "SELECT a.*, m.first_name, m.last_name, m.image, m.status_id as status_usr
			FROM ludus_forums_reply a, ludus_users m
			WHERE a.forum_id = '$idRegistro' AND a.user_id = m.user_id
			ORDER BY a.date_creation DESC";//LIMIT 0,20
		}else{
			$query_sql = "SELECT a.*, m.first_name, m.last_name, m.image, m.status_id as status_usr
			FROM ludus_forums_reply a, ludus_users m
			WHERE a.forum_id = '$idRegistro' AND a.user_id = m.user_id AND (a.status_id = 1 OR (a.status_id = 0 AND a.user_id = $idQuien) )
			ORDER BY a.date_creation DESC";//LIMIT 0,20
		}

		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		$query_Likes = "SELECT cu.*, u.first_name, u.last_name
						FROM ludus_forums_reply_likes cu, ludus_users u
						WHERE cu.user_id = u.user_id
						ORDER BY cu.date_edition DESC";
		$Rows_Likes = $DataBase_Class->SQL_SelectMultipleRows($query_Likes);

		for($i=0;$i<count($Rows_config);$i++) {
			$forum_reply_id = $Rows_config[$i]['forum_reply_id'];
			//Cargos
				for($y=0;$y<count($Rows_Likes);$y++) {
					if( $forum_reply_id == $Rows_Likes[$y]['forum_reply_id'] ){
						$Rows_config[$i]['likes_data'][] = $Rows_Likes[$y];
					}
				}
			//Cargos
		}
		unset($DataBase_Class);
		return $Rows_config;
	}
	function CrearReply($forum_id,$replaycomment){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO `ludus_forums_reply` (forum_id,user_id,date_creation,forum_reply,status_id,likes,editor,date_edition) VALUES ('$forum_id','$idQuien','$NOW_data','$replaycomment',0,0,'$idQuien','$NOW_data')";
		//echo $insertSQL_ER;
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE `ludus_forums`
						SET num_posts = num_posts+1, last_post_user_id = '$idQuien'
						WHERE forum_id = '$forum_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}
	function StatusPost($forum_reply_id,$status_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_forums_reply`
					SET status_id = $status_id
					WHERE forum_reply_id = '$forum_reply_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function BorraCargos($forum_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$updateSQL_ER = "DELETE FROM ludus_forum_charges WHERE forum_id = '$forum_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
		unset($DataBase_Log);
	}
	function CrearCargos($forum_id,$charge_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_forum_charges VALUES ($forum_id,$charge_id)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		return $resultado;
		unset($DataBase_Log);
	}
}
