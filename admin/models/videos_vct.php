<?php 


 class vct_Videos{
	
	function consulta_vct_Videos(){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}

		$super_admin = false;
		foreach ($_SESSION['id_rol'] as $key => $value) {
			if( $value['rol_id'] == 1 || $value['rol_id'] == 2){
				$super_admin = true;
				break;
			}
		}

		$fecha_actual = date("d-m-Y");
		$dos_meses = date("Y-m-d",strtotime($fecha_actual."-60 days"));
		$quer_aux = $super_admin ? "WHERE uv.checked = 'checked' " : " WHERE uv.user_id = $idQuien and uv.date_creation > '$dos_meses 00:00:00' " ;

		$query_sql = "SELECT lv.*, uv.checked FROM ludus_library_videos lv 
						INNER JOIN ludus_library_user_videos uv ON uv.schedule_id = lv.schedule_id
						$quer_aux 
						group by lv.library_id";

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDatosCargos($idUsuario){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.charge_id, c.charge, cc.category
						FROM ludus_charges_users u, ludus_charges c, ludus_charges_categories cc
						WHERE c.category_id = cc.category_id AND u.user_id = '$idUsuario' AND u.charge_id = c.charge_id AND u.status_id = 1 AND c.status_id = 1
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
}