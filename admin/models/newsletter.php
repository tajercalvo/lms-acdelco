<?php
class Newsletter{

  /*
  Andres Vega
  08/10/2016
  Funcion que retorna los correos enviados por un usuario
  */
  public function cargarCorreos(){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $user = $_SESSION['idUsuario'];
    $sql_select = "SELECT e.*,u.first_name,u.last_name, u.image AS img_pro
    FROM ludus_emails e, ludus_users u
    WHERE e.user_id = u.user_id
    AND e.user_id = $user";
    //echo($sql_select);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_select);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion cargarCorreos
  /*
	Andres Vega
	24/08/2016
	funcion para consultar los concesionarios activos
	*/
	public function consultaPersonalUnico($usuarios){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.user_id,u.first_name,u.last_name,u.email
    FROM ludus_users u
    WHERE u.status_id =1
    AND u.user_id IN ($usuarios)";
		$db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($query_sql);
		unset($db_acciones);
		return $Rows_config;
	}//fin consultaConcesionarios
  /*
	Andres Vega
	24/08/2016
	funcion para consultar los concesionarios activos
	*/
	public function consultaPersonal(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.user_id,u.first_name,u.last_name,u.email
    FROM ludus_users u
    WHERE u.status_id =1
    AND u.email not in ('','NO TIENE')";
		$db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($query_sql);
		unset($db_acciones);
		return $Rows_config;
	}//fin consultaConcesionarios
  /*
	Andres Vega
	24/08/2016
	funcion para consultar los concesionarios activos
	*/
	public function consultaConcesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_dealers WHERE status_id = 1";
		$db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($query_sql);
		unset($db_acciones);
		return $Rows_config;
	}//fin consultaConcesionarios
  /*
	Andres Vega
	08/10/2016
	funcion para consultar los cargos activos
	*/
	public function consultaCargos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_charges WHERE status_id = 1";
		$db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($query_sql);
		unset($db_acciones);
		return $Rows_config;
	}//fin consultaConcesionarios
  /*
	Andres Vega
	08/10/2016
	funcion para consultar los roles activos
	*/
	public function consultaRoles(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_roles WHERE status_id = 1";
		$db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($query_sql);
		unset($db_acciones);
		return $Rows_config;
	}//fin consultaConcesionarios
  /*
  Andres Vega
  09/10/2016
  Funcion para almacenar en la base de datos la informacion de cada correo enviado por cada usuario
  */
  public function guardarCorreo($persona,$concesionario,$cargo,$genero,$rol,$tipo_destinatario,$asunto,$archivo,$imagen,$texto){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $user = $_SESSION['idUsuario'];
    $NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
    $query_sql = "INSERT INTO ludus_emails (user_id,reciever,dealer,charge,gender,rol,type_reciever,affair,file,image,date_creation,email_text)
    VALUES ($user,'$persona','$concesionario','$cargo',$genero,'$rol','$tipo_destinatario','$asunto','$archivo','$imagen','$NOW_data','$texto')";
    //echo($query_sql);
		$db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_Insert($query_sql);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion guardarCorreo
  /*
  10/10/2016
  funcion que lista los correos de los concesionarios que se seleccionaron
  @cencesionarios = Son los id de los concesionarios separados por comas (,)
  */
  public function emailPersonas($concesionarios){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $sql_query = "SELECT u.email,u.user_id
      FROM ludus_users u
      WHERE u.user_id IN ($concesionarios)";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion emailConcesionarios
  /*
  10/10/2016
  funcion que lista los correos de los concesionarios que se seleccionaron
  @cencesionarios = Son los id de los concesionarios separados por comas (,)
  */
  public function email($concesionarios,$gender,$rol,$charge){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $sql_query = "SELECT u.email, u.user_id, h.dealer_id, u.gender, c.charge_id, r.rol_id
        FROM ludus_headquarters h, ludus_users u, ludus_charges_users c, ludus_roles_users r
        WHERE h.headquarter_id = u.headquarter_id
        AND u.user_id = c.user_id
        AND u.user_id = r.user_id";
    $sql_query .= $charge != "" ? " AND c.charge_id IN ($charge)" : "";
    $sql_query .= $concesionarios != "" ? " AND h.dealer_id IN ($concesionarios)" : "";
    $sql_query .= $gender != "" ? " AND u.gender = $gender" : "";
    $sql_query .= $rol != "" ? " AND r.rol_id = 1" : "";
    $sql_query .= " AND u.status_id = 1
        AND u.email NOT IN ('','NO TIENE')";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion emailConcesionarios
  /*
  10/10/2016
  funcion que lista los correos de los concesionarios que se seleccionaron
  @cencesionarios = Son los id de los concesionarios separados por comas (,)
  */
  public function emailConcesionarios($concesionarios){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $sql_query = "SELECT u.email,u.user_id, h.headquarter_id, h.headquarter,h.dealer_id
      FROM ludus_headquarters h, ludus_users u
      WHERE h.headquarter_id = u.headquarter_id
      AND h.dealer_id in ($concesionarios)
      AND u.email NOT IN ('','NO TIENE')";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion emailConcesionarios
  /*
  Andres Vega
  11/10/2016
  Funcion que obtiene los correos de los cargos seleccionados segun los cargos
  */
  public function emailCargos($cargos){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $sql_query="SELECT u.email,c.charge_id
    FROM ludus_users u, ludus_charges_users c
    WHERE c.user_id = u.user_id
    AND u.status_id = 1
    AND c.charge_id IN ($cargos)
    AND u.email not in ('','NO TIENE')";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion emailCargos
  /*
  Andres Vega
  11/10/2016
  Funcion que obtiene los correos de los cargos seleccionados segun el genero
  */
  public function emailGenero($genero){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $sql_query="SELECT u.email,u.gender
    FROM ludus_users u
    WHERE u.gender = $genero
    AND u.email not in ('','NO TIENE')";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion emailCargos
  /*
  Andres Vega
  11/10/2016
  Funcion que obtiene los correos de los cargos seleccionados
  */
  public function emailRol($Rol){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $sql_query="SELECT u.email,r.rol_id
    FROM ludus_users u, ludus_roles_users r
    WHERE r.user_id = u.user_id
    AND u.status_id = 1
    AND r.rol_id IN ($Rol)
    AND u.email not in ('','NO TIENE')";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion emailCargos
  /*
  */
  public function emailPrueba($texto){
    include_once('../../config/database.php');
		include_once('../../config/config.php');
    $sql_query="SELECT u.user_id,u.first_name,u.last_name
    FROM ludus_users u
    WHERE u.status_id = 1
    AND u.first_name LIKE %$texto%
    LIMIT 0,40";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectMultipleRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion emailCargos
  /*
  Andres Vega
  11/10/2016
  Funcion que retorna el registro del correo seleccionado
  */
  public function registroSeleccionado($id){
    include_once('../config/database.php');
		include_once('../config/config.php');
    $sql_query = "SELECT * FROM ludus_emails WHERE email_id = $id";
    //echo("query ".$sql_query);
    $db_acciones = new Database();
		$Rows_config = $db_acciones->SQL_SelectRows($sql_query);
		unset($db_acciones);
		return $Rows_config;
  }//fin funcion registroSeleccionado
  /*
  10/10/2016
  Funcion que genera y envia el correo
  */
  function redactarCorreo($titulo,$para,$mensaje_cuerpo,$destinos,$imagen,$archivo){
		//$para      = 'nobody@example.com';
		//$titulo    = 'El título';
		$var_img = "https://www.gmacademy.co/assets/emails/";
    $mensaje = '<div id="mail-body" style="margin: 20px auto;padding:5px;width: 80%;max-width: 900px;justify-content: center;border: solid 1px silver;border-radius: 3px;">
        <div style="margin:0 auto; padding:5px 10px; display: block;flex-wrap; wrap">
          <div style="float: left;margin: 5px;width: 25%;min-width: 220px;">
          <label for="imagen-mail">
            <div id="contentImagen">
              <img src="'.$var_img.$imagen.'" alt="'.$imagen.'" style="width: 100%">
            </div>
          </label>
          <input type="file" name="imagen-mail" id="imagen-mail" style="display: none;">
          </div>
          <div class="" name="textoEmail" id="textoEmail" style="border: solid 1px #eee;margin: 5px;width: 70%;min-height: 250px;" row="15" placeholder="Ingrese su comentario">'.$mensaje_cuerpo.'</div>';
    if($archivo != ""){
      $mensaje.= '<div class="">
        <strong>Adjunto: </strong>
        <a href="'.$var_img.$archivo.'" target="_blank">'.$archivo.'</a>
      </div>';
    }
      $mensaje .= '</div></div>';
		// Para enviar un correo HTML, debe establecerse la cabecera Content-type
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		$cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$cabeceras .= 'Reply-To: noreply@gmacademy.co' . "\r\n";
		$cabeceras .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
		// Cabeceras adicionales
		$cabeceras .= 'To: ' . $para . "\r\n";
		$cabeceras .= 'From: Comunicaciones Ludus <comunicaciones@gmacademy.co>' . "\r\n";
		/*$cabeceras .= 'Cc: birthdayarchive@example.com' . "\r\n";*/
		$cabeceras .= 'Bcc: ' . $destinos . "\r\n";
		$resultado = mail($para, $titulo, $mensaje, $cabeceras);

		return $resultado;
	}
}//fin clase Newsletter
?>
