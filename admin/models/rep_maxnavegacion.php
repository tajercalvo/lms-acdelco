<?php
Class RepMaxNavegacion {
	// Juan Carlos Villar - Funcion para consultar todos los usuarios de ludus que esten activos, con su ultima seccion
	function consultaDatosAll($start_date_day,$end_date_day,$identificacion){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$query_sql = "SELECT u.first_name, u.last_name, u.identification, d.dealer, h.headquarter, MAX(n.date_navigation)
						FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_navigation n
						WHERE u.status_id = 1 AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = n.user_id ";

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']<=4){
			$query_sql .= " AND d.dealer_id = '$dealer_id' ";
		}
		
		if($start_date_day!='' && $end_date_day!=''){
			$query_sql .= " AND n.date_navigation BETWEEN '$start_date_day 00:00:00' AND '$end_date_day 23:59:59' ";
		}elseif($start_date_day!=''){
			$query_sql .= " AND n.date_navigation > '$start_date_day 00:00:00' ";
		}elseif($end_date_day!=''){
			$query_sql .= " AND n.date_navigation < '$end_date_day 23:59:59' ";
		}

		if($identificacion!=''){
			$query_sql .= " AND u.identification = '$identificacion' ";
		}

		$query_sql .= " GROUP BY u.first_name, u.last_name, u.identification, d.dealer, h.headquarter 
						ORDER BY d.dealer, h.headquarter, u.first_name";
						//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
