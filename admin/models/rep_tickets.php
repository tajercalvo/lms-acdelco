<?php
Class RepTickets {
	function consultaDatosAll( $start_date, $end_date, $opcion="xls" ){
		include_once('../config/database.php');
		include_once('../config/config.php');
		//se hace la consulta de los tickets
		$query_sql = "SELECT t.ticket_id,t.user_id, u.first_name, u.last_name, t.status_id, t.description, t.priority, k.key_description, t.date_creation, d.dealer
			FROM ludus_tickets t, ludus_tickets_key k,ludus_users u, ludus_headquarters h, ludus_dealers d
			WHERE t.key_id = k.key_id
			AND t.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND t.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		//se realiza la consulta de las respuestas
		$query_sql_ans = "SELECT r.ticket_id, r.user_id, r.date_creation, r.description, MAX(r.date_creation) as date_respuesta , us.first_name, us.last_name
			FROM ludus_tickets_response r, ludus_users us
			WHERE r.user_id = us.user_id
			AND r.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
			GROUP BY r.date_creation
			ORDER BY r.ticket_id";
		$Rows_answer = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_ans);
		for ($i=0; $i < count($Rows_config); $i++) {
			$respuesta = "";
			$contador = 0;
			foreach ($Rows_answer as $key => $value) {
				$clase = "";
				if( $Rows_config[$i]['ticket_id'] == $value['ticket_id'] ){
					$contador += 1;
					if( $opcion == "html" ){
						$clase = ( $Rows_config[$i]['user_id'] == $value['user_id'] ) ? "class='text-primary'" : "class='text-warning'";
						$respuesta .= "<strong $clase>".$value['date_creation']."</strong> ".$value['description']."<br>";
					}else{
						$respuesta .= "<strong>".$value['date_creation']."</strong> ".$value['description']."  |  ";
					}
					$Rows_config[$i]['num_respuestas'] = $contador;
					$Rows_config[$i]['date_respuesta'] = $value['date_respuesta'];
					$Rows_config[$i]['first_s_name'] = $value['first_name'];
					$Rows_config[$i]['last_s_name'] = $value['last_name'];
				}
				$Rows_config[$i]['respuestas'] = $respuesta;
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
