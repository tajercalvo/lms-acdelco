<?php
Class Parametros {
	function consultaDatosparametros($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT p.*, u.first_name, u.last_name 
						FROM ludus_parameters p, ludus_users u 
						WHERE p.user_id = u.user_id ";
			if($sWhere!=''){
				$query_sql .= " AND (p.parameter LIKE '%$sWhere%' OR p.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' )";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT p.*, u.first_name, u.last_name 
						FROM ludus_parameters p, ludus_users u 
						WHERE p.user_id = u.user_id 
						ORDER BY parameter_id ";
			if($sWhere!=''){
				$query_sql .= " AND (p.parameter LIKE '%$sWhere%' OR p.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' )";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT p.*, u.first_name, u.last_name 
						FROM ludus_parameters p, ludus_users u 
						WHERE p.user_id = u.user_id 
						ORDER BY parameter_id ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearparametros($nombre, $valor){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$insertSQL_EE = "INSERT INTO ludus_parameters (parameter,value,user_id,date_edition) VALUES ('$nombre','$valor','$idQuien','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function Actualizarparametros($id,$nombre,$valor){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$updateSQL_ER = "UPDATE ludus_parameters SET parameter = '$nombre', value = '$valor', user_id = '$idQuien', date_edition = '$NOW_data' WHERE parameter_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * 
						FROM ludus_parameters p 
						WHERE p.parameter_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
