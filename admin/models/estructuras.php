<?php
class Cargos
{
	function consultaDatoscargos($sWhere, $sOrder, $sLimit)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita, z.course,z.newcode, x.charge, v.level, v.image
			FROM ludus_charges_courses c, ludus_users s, ludus_status e, ludus_courses z, ludus_charges x, ludus_level v
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id 
			AND c.charge_id = x.charge_id AND c.level_id = v.level_id ";
		if ($sWhere != '') {
			$query_sql .= " AND (e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.newcode LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR x.charge LIKE '%$sWhere%' OR v.level LIKE '%$sWhere%' ) ";
		}
		$query_sql .= $sOrder;
		$query_sql .= ' ' . $sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere, $sOrder, $sLimit)
	{
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita, z.course,z.newcode, x.charge, v.level, v.image
			FROM ludus_charges_courses c, ludus_users s, ludus_status e, ludus_courses z, ludus_charges x, ludus_level v
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id 
			AND c.charge_id = x.charge_id AND c.level_id = v.level_id ";
		if ($sWhere != '') {
			$query_sql .= " AND (e.status LIKE '%$sWhere%' OR c.date_creation LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR z.newcode LIKE '%$sWhere%' OR z.course LIKE '%$sWhere%' OR x.charge LIKE '%$sWhere%' OR v.level LIKE '%$sWhere%' ) ";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad()
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita, z.course,z.newcode, x.charge, v.level, v.image
			FROM ludus_charges_courses c, ludus_users s, ludus_status e, ludus_courses z, ludus_charges x, ludus_level v
			WHERE c.editor = s.user_id AND c.status_id = e.status_id AND c.course_id = z.course_id 
			AND c.charge_id = x.charge_id AND c.level_id = v.level_id 
			ORDER BY v.level, x.charge "; //LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Crearcargos($charge_id, $course_id, $level_id)
	{
		$data = [];
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		try {
			$DataBase_Log = new Database();
			$resultado = 0;
			foreach ($charge_id as $key => $charge) {
				$querySel = "SELECT * FROM ludus_charges_courses WHERE course_id = $course_id and charge_id = $charge;";
				$res = $DataBase_Log->SQL_SelectMultipleRows($querySel);
				if (empty($res)) {
					$insertSQL_EE = "INSERT INTO ludus_charges_courses (course_id,status_id,creator,editor,date_creation,date_edition,charge_id,level_id) VALUES ('$course_id','1','$idQuien','$idQuien','$NOW_data','$NOW_data','$charge','$level_id')";
					$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
					$data['resultado'] = 'SI';
					$data['msg'] = 'Curso Agregado a la trayectoria Exitosamente';
					$data['error'] = false;
				} else {
					$data['resultado'] = 'NO';
					$data['msg'] = 'El curso ya esta Asignado a la Trayectoria..';
					$data['error'] = true;
				}
			}
			unset($DataBase_Log);
		} catch (Exception $th) {
			$data['resultado'] = 'NO';
			$data['msg'] = 'Ocurrio en Error Comunicarse con Soporte..';
			$data['error'] = true;
		}

		return $data;
	}
	function Actualizarcargos($id, $status, $charge_id, $course_id, $level_id)
	{
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_estructuras.php?opcn=ver&id=$id&back=inicio>$id</a>] a cargos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_charges_courses` SET course_id = '$course_id', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data',charge_id = '$charge_id',level_id = '$level_id' WHERE charge_course_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_charges_courses c
			WHERE c.charge_course_id = '$idRegistro' "; //LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.course_id, c.course, c.newcode
						FROM ludus_courses c
						ORDER BY c.course_id DESC";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaCargos($prof)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.charge_id, c.charge
						FROM ludus_charges c
						WHERE status_id = 1
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaNiveles($prof)
	{
		if ($prof == "js") {
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		} else {
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.level_id, c.level
						FROM ludus_level c
						ORDER BY c.level";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
}
