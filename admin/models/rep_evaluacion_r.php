<?php
Class RepEvaluaciones {

	public static function consultaDatosAll($review_id,$start_date,$end_date){
		include_once('../config/init_db.php');
		@session_start();

		$query_sql = "SELECT a.reviews_answer_id, a.date_creation, a.time_response, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, r.review, r.code, a.score, a.available_score
			FROM ludus_reviews_answer_r a, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews_r r, ludus_reviews_courses c, ludus_reviews re
			WHERE a.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
			AND a.user_id = u.user_id
			AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND a.review_id = r.review_id
			AND r.course_id = c.course_id
			AND c.review_id = re.review_id
			AND r.review_id = $review_id ";

		$Rows_config = DB::query($query_sql);

		$query_res = "SELECT ra.reviews_answer_id, ad.result, q.question, q.code, ans.answer, ans.result as res_ans, rq.agrup
			FROM ludus_reviews_answer_r ra, ludus_reviews_answer_detail_r ad, ludus_questions_r q, ludus_answers_r ans,
			ludus_reviews_questions_r rq, ludus_reviews_r r, ludus_reviews_courses c
			WHERE ra.review_id = r.review_id
			AND r.course_id = c.course_id
			AND r.review_id = $review_id
			AND ra.reviews_answer_id = ad.reviews_answer_id
			AND ra.date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
			AND ad.question_id = q.question_id
			AND ad.answer_id = ans.answer_id
			AND ra.review_id = rq.review_id
			AND q.question_id = rq.question_id";
		$Rows_res = DB::query($query_res);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_reviews_answer_id = $Rows_config[$i]['reviews_answer_id'];

			for($y=0;$y<count($Rows_res);$y++) {
				if($var_reviews_answer_id == $Rows_res[$y]['reviews_answer_id']){
					$Rows_config[$i]['respuestas'][] = $Rows_res[$y];
				}
			}

		}

		return $Rows_config;
	}

	public static function EvaluacionesEncuestas($tipo){
		include_once('../config/init_db.php');
		@session_start();
		$query_evaluaciones = "SELECT * FROM ludus_reviews_r WHERE type IN ($tipo) ";
		$Rows_config = DB::query($query_evaluaciones);
		return $Rows_config;
	}
}
