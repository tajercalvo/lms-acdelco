<?php
Class Banners {
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_banners";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	/*
	Andres Vega
	19/08/2016
	Funcion para consultar el numero de registros de banners almacenados
	*/
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT b.banner_id,t.banners_type,b.banner_name,b.file_name,b.status_id,u.first_name,u.last_name,b.start_date,b.end_date,b.banner_url,b.charge_id,b.create_date,b.dealers_id,b.gender
			FROM ludus_users u, ludus_banners_type t, ludus_banners b
			WHERE t.banner_type_id = b.banner_type_id AND u.user_id = b.user_id
			AND b.banner_id = '$idRegistro'";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin metodo consultaRegistro
	/*
	Andres Vega
	18/08/2016
	funcion para insertar los datos de un registro sobre un banner
	*/
	function CrearBanner($tipoBanner,$bannerName,$fileName,$status,$startDate,$endDate,$url,$charge,$dealers,$gender){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_banners
		(banner_type_id,banner_name,file_name,status_id,user_id,start_date,end_date,banner_url,charge_id,create_date,dealers_id,gender)
		VALUES ($tipoBanner,'$bannerName','$fileName',$status,$idQuien,'$startDate','$endDate','$url','$charge','$NOW_data','$dealers',$gender)";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	/*
	Andres Vega
	18/08/2016
	funcion que actualiza un registro con la nueva informacion del banner
	*/
	function ActualizarBanner($id,$tipoBanner,$bannerName,$fileName,$status,$startDate,$endDate,$url,$charge,$dealers,$gender){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE ludus_banners
		SET banner_type_id = $tipoBanner, banner_name = '$bannerName', file_name = '$fileName',
		status_id = $status,user_id = $idQuien,start_date = '$startDate',end_date = '$endDate',
		banner_url = '$url',charge_id = '$charge',create_date = '$NOW_data', dealers_id = '$dealers',
		gender = $gender
		WHERE banner_id = $id";
		//echo($updateSQL_ER);
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		unset($DataBase_Log);
		return $resultado;
	}
	/*
	Andres Vega
	17/08/2016
	funcion para consultar los datos de la tabla de tipos de banners
	*/
	public function tipoBanner(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_banners_type";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion tipoBanner
	/*
	funcion que trae los datos de los cargos de acuerdo a su estado
	@prof= 				Indica de donde debe realizar la navegacion en el proyecto para importar
	@status_id= 	Es el estado de los cargos que va a consultar
	*/
	function consultaAllDatosCargos($prof,$status_id){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		if($status_id != ''){
			$status = "c.status_id = '$status_id'";
		}else{
			$status = "c.status_id > 0";
		}
		$query_sql = "SELECT c.charge_id, c.charge
						FROM ludus_charges c
						WHERE $status
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}//fin consultaAllDatosCargos
	/*
	Andres Vega
	19/08/2016
	Funcion que trae los datos del registro seleccionado para ser editados
	*/
	/*
	Cristian
	Semana del 24 de septiembre
	Se agrega $sWhere,$sOrder,$sLimit,$prof= ""
	*/
	public function consultaDatosBanners($sWhere,$sOrder,$sLimit,$prof= ""){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT b.banner_id,t.banners_type,b.banner_name,b.file_name,b.status_id,u.first_name,u.last_name,b.start_date,b.end_date,b.banner_url,b.charge_id,b.create_date,b.dealers_id,b.gender,
			(SELECT count(bu.banner_user_id) FROM ludus_banners_users bu WHERE bu.banner_id = b.banner_id ) AS clicks
			FROM ludus_users u, ludus_banners_type t, ludus_banners b
			WHERE t.banner_type_id = b.banner_type_id AND u.user_id = b.user_id";
			if($sWhere!=''){
			  $query_sql .= " AND (t.banners_type LIKE '%$sWhere%' OR b.banner_name LIKE '%$sWhere%' OR b.file_name LIKE '%$sWhere%' OR b.status_id LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR b.start_date LIKE '%$sWhere%' OR b.end_date LIKE '%$sWhere%' OR b.banner_url LIKE '%$sWhere%' OR b.create_date LIKE '%$sWhere%'  )";
			}
			$query_sql .= $sOrder;
	  	$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	/*
	Cristian
	Semana del 24 de septiembre
	Se crea una consulta igual a consultaDatosBanners pero a esta no se le pone la variable $query_sql
	*/
	public function consultaCantidadFull($sWhere,$sOrder,$sLimit,$prof= ""){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT b.banner_id,t.banners_type,b.banner_name,b.file_name,b.status_id,u.first_name,u.last_name,b.start_date,b.end_date,b.banner_url,b.charge_id,b.create_date,b.dealers_id,b.gender,
			(SELECT count(bu.banner_user_id) FROM ludus_banners_users bu WHERE bu.banner_id = b.banner_id ) AS clicks
			FROM ludus_users u, ludus_banners_type t, ludus_banners b
			WHERE t.banner_type_id = b.banner_type_id AND u.user_id = b.user_id";
			if($sWhere!=''){
			  $query_sql .= " AND (t.banners_type LIKE '%$sWhere%' OR b.banner_name LIKE '%$sWhere%' OR b.file_name LIKE '%$sWhere%' OR b.status_id LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR b.start_date LIKE '%$sWhere%' OR b.end_date LIKE '%$sWhere%' OR b.banner_url LIKE '%$sWhere%' OR b.create_date LIKE '%$sWhere%'  )";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaDatosRegistro
	/*
	Andres Vega
	24/08/2016
	funcion para consultar los concesionarios activos
	*/
	public function consultaConcesionarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_dealers";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones); // Destruimos la variable para liberar memoria
		return $Rows_config;
	}//fin consultaConcesionarios
	/*
	Andres Vega
	24/08/2016
	Funcion que trae los banners superiores
	*/
	public function consultaTop($dealer){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];//dealer_id
		$genero = "0";
		if(isset($_SESSION['gender'])){
			$genero = $_SESSION['gender'];
		}
		$NOW_data = date("Y-m-d");
		$cargos = $this->consultaCargos($idQuien);
		$query1 = "SELECT *
		FROM ludus_banners
		WHERE status_id = 1 AND start_date <= '$NOW_data' AND end_date >= '$NOW_data'
			AND banner_type_id = 1
			AND ( gender = $genero OR gender = 0 )
			AND (dealers_id = ''
				OR charge_id = '')";
		$query_sql = $query1." UNION ";//Genera la union de las tablas
		$query_sql .= "SELECT b.*
		FROM ludus_banners b
		WHERE status_id = 1 AND start_date <= '$NOW_data' AND end_date >= '$NOW_data'
			AND banner_type_id = 1
			AND ( gender = $genero OR gender = 0 )
			AND FIND_IN_SET('$dealer',dealers_id)";
		if(count($cargos)>0){
			$query_sql .= " AND ( FIND_IN_SET('{$cargos[0]['charge_id']}',b.charge_id)";
			if(count($cargos)>1){
				for($i=1; $i< count($cargos) ; $i++){
					$query_sql .= " OR FIND_IN_SET('{$cargos[$i]['charge_id']}',b.charge_id)";
				}
			}
			$query_sql .= " )";
		}
		$query_sql .= " ORDER BY RAND() LIMIT 0,10";
		//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaTop
	/*
	Andres Vega
	24/08/2016
	Funcion que trae los banners del calendario
	*/
	public function consultaCal($dealer){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];//dealer_id
		$genero = "0";
		if(isset($_SESSION['gender'])){
			$genero = $_SESSION['gender'];
		}
		$NOW_data = date("Y-m-d");
		$cargos = $this->consultaCargos($idQuien);
		$query1 = "SELECT *
		FROM ludus_banners
		WHERE status_id = 1 AND start_date <= '$NOW_data' AND end_date >= '$NOW_data'
			AND banner_type_id = 2
			AND ( gender = $genero OR gender = 0 )
			AND ( dealers_id = ''
			OR charge_id = '' )  ";
		$query_sql = $query1."UNION ";//Genera la union de las tablas
		$query_sql .= "SELECT b.*
		FROM ludus_banners b
		WHERE status_id = 1 AND start_date <= '$NOW_data' AND end_date >= '$NOW_data'
			AND banner_type_id = 2
			AND ( gender = $genero OR gender = 0 )
			AND FIND_IN_SET('$dealer',dealers_id)";
		if(count($cargos)>0){
			$query_sql .= " AND ( FIND_IN_SET('{$cargos[0]['charge_id']}',b.charge_id)";
			if(count($cargos)>1){
				for($i=1; $i< count($cargos) ; $i++){
					$query_sql .= " OR FIND_IN_SET('{$cargos[$i]['charge_id']}',b.charge_id)";
				}
			}
			$query_sql .= " )";
		}
		$query_sql .= " ORDER BY RAND() LIMIT 0,1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaTop
	/*
	Andres Vega
	24/08/2016
	Funcion que trae los banners bajo la historia
	*/
	public function consultaHis($dealer){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];//dealer_id
		$genero = "0";
		if(isset($_SESSION['gender'])){
			$genero = $_SESSION['gender'];
		}
		$NOW_data = date("Y-m-d");
		$cargos = $this->consultaCargos($idQuien);
		$query1 = "SELECT *
		FROM ludus_banners
		WHERE status_id = 1 AND start_date <= '$NOW_data' AND end_date >= '$NOW_data'
			AND banner_type_id = 3
			AND ( gender = $genero OR gender = 0 )
			AND ( dealers_id = ''
			OR charge_id = '' ) ";
		$query_sql = $query1." UNION ";//Genera la union de las tablas
		$query_sql .= "SELECT b.*
		FROM ludus_banners b
		WHERE status_id = 1 AND start_date <= '$NOW_data' AND end_date >= '$NOW_data'
			AND banner_type_id = 3
			AND ( gender = $genero OR gender = 0 )
			AND FIND_IN_SET('$dealer',dealers_id)";
		if(count($cargos)>0){
			$query_sql .= " AND ( FIND_IN_SET('{$cargos[0]['charge_id']}',b.charge_id)";
			if(count($cargos)>1){
				for($i=1; $i< count($cargos) ; $i++){
					$query_sql .= " OR FIND_IN_SET('{$cargos[$i]['charge_id']}',b.charge_id)";
				}
			}
			$query_sql .= " )";
		}
		$query_sql .= " ORDER BY RAND() LIMIT 0,3";
		//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaHis
	/*
	Andres Vega
	24/08/2016
	Funcion que trae los banners Modales
	*/
	public function consultaMod($dealer){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];//dealer_id
		$genero = "0";
		if(isset($_SESSION['gender'])){
			$genero = $_SESSION['gender'];
		}
		$NOW_data = date("Y-m-d");
		$cargos = $this->consultaCargos($idQuien);
		$query_sql = "SELECT *
		FROM ludus_banners b
		WHERE b.status_id = 1 AND b.start_date <= '$NOW_data' AND b.end_date >= '$NOW_data'
			AND ( b.banner_type_id = 4 OR b.banner_type_id = 5 )
			AND ( b.gender = $genero OR b.gender = 0 )
			AND ( FIND_IN_SET('$dealer',b.dealers_id) OR b.dealers_id = '' )";
		if(count($cargos)>0){
			$query_sql .= " AND ( FIND_IN_SET('{$cargos[0]['charge_id']}',b.charge_id)";
			if(count($cargos)>1){
				for($i=1; $i< count($cargos) ; $i++){
					$query_sql .= " OR FIND_IN_SET('{$cargos[$i]['charge_id']}',b.charge_id)";
				}
			}
			$query_sql .= " OR b.charge_id = '' ) ";
		}
		$query_sql .= " ORDER BY RAND() LIMIT 0,1";
		//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaMod
	/*
	Andres Vega
	26/08/2016
	Funcion para traer los cargos que tiene un usuario dentro del sistema
	*
	@	$id:	Es el id del usuario que es cargado en $_SESSION principalmente
	*/
	private function consultaCargos($id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.charge_id FROM ludus_charges_users c WHERE user_id = $id AND status_id = 1";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion consultaCargos
	/*
	Andres Vega
	23/11/2016
	Funcion que crea un registro en la tabla [ ludus_banners_users ] con el id del usuario y el banner al que genero el click
	@banner_id: Es el parametro que indica el id que se va a almacenar
	*/
	public function guardarClick($banner_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$user_id = $_SESSION['idUsuario'];
		$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Acciones = new Database();
		$query_sql = "INSERT INTO ludus_banners_users (user_id,banner_id,date_creation) VALUES ($user_id,$banner_id,'$NOW_data')";
		//echo($query_sql);
		$Rows_config = $DataBase_Acciones->SQL_Insert($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion guardarClick

}//fin clase Banners
