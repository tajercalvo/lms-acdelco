<?php
Class Aulavirtual {

	function get_diapositivas($schedule_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Class = new Database();
		$query_sql = "SELECT f.*
					FROM ludus_courses_files_av f
						INNER JOIN ludus_schedule_av s
							on f.course_id = s.schedule_id
							and s.schedule_id = $schedule_id
							order by file asc";
		$archivos = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		
		//$query_aux = "SELECT distinct(f.course_id), s.teacher_id as instructor from ludus_courses_files_av f inner join ludus_schedule_av s on f.course_id = s.course_id and s.schedule_id = $schedule_id";
		//$datos = $DataBase_Class->SQL_SelectRows($query_aux);

		foreach ($archivos as $key => $value) {
			$info = new SplFileInfo($value['archivo']);
			$tipoArchivo = $info->getExtension();
			
			if( ($tipoArchivo == 'png') or ($tipoArchivo == 'PNG') or ($tipoArchivo == 'jpg') or ($tipoArchivo == 'JPG') or ($tipoArchivo == 'jpeg') or ($tipoArchivo == 'JPEG') ){
				$archivos[$key]['imagen'] = "../assets/fileVCT_av/VCT_".$value['course_id']."/".$value['image'];
			}elseif( ($tipoArchivo == 'pdf') or ($tipoArchivo == 'mp4') ){
				$archivos[$key]['imagen'] = "../assets/fileVCT_av/".$value['image'];
			}else{
				$archivos[$key]['imagen'] = "../assets/fileVCT_av/".$value['image'];
				$archivos[$key]['preg'] = 'rep_vct_respuestas_av.php?token='.md5(time()).'&question_id='.$value['question_id'];
			}
	
		}
		
		if(count($archivos) > 0){
			$sql_select_preguntas = "SELECT id, question_id, course_id, answer FROM ludus_respuestas_preguntas_cursos_av where course_id = {$archivos[0]['course_id']};";
			$datos_preguntas = $DataBase_Class->SQL_SelectMultipleRows($sql_select_preguntas);
			foreach ($archivos as $key => $value) {
				$value['question_id'];
				foreach ($datos_preguntas as $key2 => $value2) {
					if($value['question_id'] == $value2['question_id']){
						$archivos[$key]['respuestas'][] = $value2;
					}
				}
			}
		}
		unset($DataBase_Class);
		//$datos_listos = ['archivos' => $archivos, 'datos' => $datos];
		// echo('<pre>');
		// print_r($archivos);
		// echo('</pre>');
		// die();
		return $archivos;
	}//fin funcion consultaRegistros

	function cambiardiapositiva($diapostiva, $schedule){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Class = new Database();

		//$query_sql = "SELECT * from ludus_schedule_av where schedule_id = $schedule";
		//$schedule = $DataBase_Class->SQL_SelectRows($query_sql);

		$query_sql = "UPDATE ludus_courses_files_av SET status_id = '2' WHERE course_id = $schedule;";
		$DataBase_Class->SQL_Update($query_sql);

		$query_sql = "UPDATE ludus_courses_files_av SET status_id = '1' WHERE coursefile_id = '$diapostiva';";
		$Rows_config = $DataBase_Class->SQL_Update($query_sql);

		if($Rows_config > 0){
			$datos = ["resultado" => "SI", 'id' => $diapostiva];
		}else{
			$datos = ["resultado" => "NO", 'error' => $diapostiva];
		}
		unset($DataBase_Class);
		return $datos;
	}//fin funcion set_diapositiva_actual()

	function get_diapositiva_actual($post){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$schedule_id = $post['schedule_id'];
		
		$query_sql = "SELECT sd.id ultimadiapositiva, sd.archivo, sd.course_file_id, fi.question_id, fi.name
						from  ludus_seguimiento_diapositivas sd
								inner join ludus_courses_files_av fi
									on sd.course_file_id = fi.coursefile_id
						where sd.schedule_id = $schedule_id
				        order by id desc limit 1;";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);

		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion set_diapositiva_actual()

	public function conectados( $schedule_id, $ajax = '' )
	{
		include_once($ajax.'../config/database.php');
		include_once($ajax.'../config/config.php');
		$DataBase_Class = new Database();
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		$ultima_conexion 	= date("Y-m-d")." ".date("H").":".date("i:s");

		$query_1 = "UPDATE  ludus_asistentes_av set ultima_conexion = '$ultima_conexion' where schedule_id_av = $schedule_id and user_id = $user_id;";
		$Rows_config = $DataBase_Class->SQL_Update($query_1);

		$query_2 = "SELECT * FROM  ludus_asistentes_av where schedule_id_av = $schedule_id and ultima_conexion > DATE_ADD( '$ultima_conexion' , INTERVAL -5 SECOND);";
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_2);

		foreach ($Rows_config as $key => $value) {
			$Rows_config[$key]['usuario'] = ucwords( strtolower($value['usuario']));
		}
		return $Rows_config;
	}
	function registrarUsuario( $schedule_id, $ajax="" ){
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		$imagen = $_SESSION['imagen'];
		$usuario = $_SESSION['NameUsuario'];
		
		include_once($ajax.'../config/database.php');
		include_once($ajax.'../config/config.php');
		$query_sql = "SELECT * FROM  ludus_asistentes_av where schedule_id_av = $schedule_id and user_id = $user_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		
		$query_sql2 = "SELECT * FROM  ludus_asistentes_av where schedule_id_av = $schedule_id";
		$DataBase_Class = new Database();
		$Rows_config2 = $DataBase_Class->SQL_SelectMultipleRows($query_sql2);
		
		$ultima_conexion 	= date("Y-m-d")." ".date("H").":".date("i:s");

		if( count($Rows_config)  == 0){
			$user_id_av = count($Rows_config2) + 1;
			$query_ins = "INSERT INTO ludus_asistentes_av
						(
						user_id_av,
						user_id,
						usuario,
						imagen,
						schedule_id_av,
						ultima_conexion)
						VALUES
						(
						$user_id_av,
						$user_id,
						'$usuario',
						'$imagen',
						$schedule_id,
						'$ultima_conexion');";
			$res = $DataBase_Class->SQL_Insert($query_ins);
			if($res){
				$_SESSION['user_id_av']     = $user_id_av;
				$_SESSION['imagen_av']      = $imagen;
				$_SESSION['NameUsuario_av'] = $usuario;
			}
		}else{
			$_SESSION['user_id_av'] 	= $Rows_config[0]['user_id_av'];
			$_SESSION['imagen_av'] 		= $_SESSION['imagen'];
			$_SESSION['NameUsuario_av'] = $_SESSION['NameUsuario'];
		}

		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion consultaRegistros

	function registrarInvitado( $schedule_id, $usuario,  $ajax="../" ){
		@session_start();
		
		include_once($ajax.'../config/database.php');
		include_once($ajax.'../config/config.php');
		$query_sql = "SELECT * FROM  ludus_asistentes_av where schedule_id_av = $schedule_id";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		
			$user_id_av = count($Rows_config) + 1;
			$query_ins = "INSERT INTO ludus_asistentes_av
						(
						user_id_av,
						user_id,
						usuario,
						imagen,
						schedule_id_av)
						VALUES
						(
						$user_id_av,
						0,
						'$usuario',
						'default.png',
						$schedule_id);";
		$res = $DataBase_Class->SQL_Insert($query_ins);
		if( $res ){
			$_SESSION['user_id_av'] = $user_id_av;
			$_SESSION['idUsuario'] 	= 0;
			$_SESSION['imagen_av'] 	= 'default.png';
			$_SESSION['NameUsuario_av'] = $usuario;
			$_SESSION['time_sesion_av'] = time();
		}
		
		unset($DataBase_Class);
		return $res;
	}//fin funcion consultaRegistros
	
	
	function consultaRegistroDetail($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * FROM ludus_schedule_av where schedule_id = '$idRegistro'  ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}//fin funcion consultaRegistroDetail
	
	function consultaInscripcion($idCourse,$schedule_id){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$query_sql = "SELECT inscription_id FROM ludus_inscriptions WHERE user_id = '$idQuien' AND course_id = '$idCourse' AND schedule_id = '$schedule_id' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectCantidad($query_sql);
		unset($DataBase_Class);
		if($Rows_config>0){
			return true;
		}else{
			return false;
		}
	}

	function DetProgramacion($schedule_id, $prof = "" ){
		include_once( $prof.'../config/init_db.php');
		@session_start();
		$teacher_id_av = $_SESSION['idUsuario'];
		$query_sql = "SELECT s.*, u.first_name, u.last_name, u.identification, u.image
					FROM ludus_schedule_av s, ludus_users u
					WHERE s.teacher_id = u.user_id AND s.schedule_id = $schedule_id ";//LIMIT 0,20
		$Rows_config = DB::queryFirstRow($query_sql);

		// Instructores auxiliares
		$query_aux = "SELECT teacher_id_av from ludus_teachers_aux_av where schedule_id_av = $schedule_id and teacher_id_av = $teacher_id_av";
		$Rows_aux = DB::queryFirstRow($query_aux);
		
		//Cambiamos el instructor principal por uno auxiliar si fuera el caso
		$Rows_config['teacher_id'] = isset($Rows_aux['teacher_id_av']) ? $Rows_aux['teacher_id_av'] : $Rows_config['teacher_id'] ; 
		

		$query_validacion = "SELECT * from ludus_invitation WHERE schedule_id = $schedule_id and status_id <> 1;";
		$validacion = DB::query($query_validacion);
		$Rows_config['curso_cerrado'] = isset( $validacion ) && count( $validacion ) > 0 ? "si" : "no";

		return $Rows_config;
	}
    
    
	public function consultaComentarios($schedule_id){
        @session_start();
		$idQuien = $_SESSION['user_id_av'];
		include_once('../config/database.php');
        include_once('../config/config.php');
        
		$query_sql = "SELECT c.*, u.user_id_av, u.usuario, u.imagen,
		CASE
			WHEN u.user_id = $idQuien THEN 'right'
			ELSE 'left' 
			END as chat,
		CASE
			WHEN u.user_id != $idQuien THEN 'right' 
			ELSE 'left'
			END as tiempo
			FROM ludus_asistentes_av u, ludus_comments_av c
			WHERE u.user_id_av = c.user_id AND c.schedule_id = $schedule_id and u.schedule_id_av = $schedule_id order by c.comments_id asc";
		$dataBase_Class = new Database();
		$Rows_config = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		foreach ($Rows_config as $key => $value) {
			$Rows_config[$key]['fecha'] = substr($value['date_comment'],0, 10);
			$Rows_config[$key]['hora'] = date("g:i a",strtotime(substr($value['date_comment'],-8, 8)));
		}
		unset($dataBase_Class);
		return $Rows_config;
	}
	
	public function nuevomsj($schedule,$msj, $idmsj){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['user_id_av'];
		$timeComment = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$query_sql = "INSERT INTO ludus_comments_av (user_id,schedule_id,date_comment,comment_text)
			VALUES ($idQuien,$schedule,'$timeComment','$msj')";
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_Insert($query_sql);
		
		$res = array();
		if ($resultado> 0) {
			$res['error'] = false;
			$res['time'] = substr($timeComment,0, 10).' '.date("g:i a",strtotime(substr($timeComment,-8, 8)));
			$res['idmsj'] = $idmsj;
		}else{
			$res['error'] = true;
			$res['idmsj'] = $idmsj;
		}
		
		unset($dataBase_Class);
		return $res;
	}
	
	public function msjNuevos($schedule, $msj_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['user_id_av'];
		$query_sql = "SELECT co.*, u.imagen, u.usuario
				FROM ludus_comments_av co
						INNER JOIN ludus_asistentes_av u
							on co.user_id = u.user_id_av
							and co.schedule_id = $schedule
							and u.schedule_id_av = $schedule
							and co.comments_id  > $msj_id
							and co.user_id <> $idQuien
							order by co.comments_id asc";
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_SelectMultipleRows($query_sql);
		$res = array();
		foreach ($resultado as $key => $value) {
			$resultado[$key]['time'] = substr($value['date_comment'],0, 10).' '.date("g:i a",strtotime(substr($value['date_comment'],-8, 8)));
		}
		
		$res['msj'] = $resultado;
		$res['msj_id'] =  count($resultado) > 0 ? $resultado[(count($resultado) - 1)]['comments_id'] : $msj_id;
		unset($dataBase_Class);
		return $res;
	}	

	public function diapositivaActual($schedule){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		
		$query_sql = "SELECT f.*
						FROM ludus_courses_files_av f
							INNER JOIN ludus_schedule_av s
								on f.course_id = s.schedule_id
								and s.schedule_id = '$schedule'
								and f.status_id = 1";
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_SelectRows($query_sql);

		if ( is_null($resultado) ) {
			$resultado = array();
		}else{
			$resultado['ruta'] = '../assets/fileVCT_av/VCT_'.$resultado['course_id'].'/';
		}

		unset($dataBase_Class);
		return $resultado;
	}
	
	public function inscribirseCurso( $module_id, $idCourse, $schedule_id, $prof="" ){
		@session_start();
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		if(!isset($_SESSION['idUsuario'])){
			$idQuien = 0;
		}else{
			$idQuien = $_SESSION['idUsuario'];
		}
		$insertSQL_EE = "INSERT INTO ludus_inscriptions (user_id,course_id,status_id,date_creation,schedule_id, module_id) VALUES ('$idQuien','$idCourse',1,NOW(),$schedule_id, $module_id) ";//LIMIT 0,20
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_Insert($insertSQL_EE);
		unset($dataBase_Acciones);
		return $resultado;
	}
	
	public function yaInscrito( $user, $schedule_id, $prof = "" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT COUNT(user_id) as cantidad FROM ludus_inscriptions
			WHERE schedule_id = $schedule_id AND user_id = $user AND status_id = 1";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config['cantidad'];
	}
	
	public function invitado( $user, $schedule_id, $prof = "" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		$query_sql = "SELECT COUNT(user_id) as cantidad FROM ludus_invitation
			WHERE schedule_id = $schedule_id AND user_id = $user";//status_id = 1 LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config['cantidad'];
	}


	// levantar la mano
	public function levantarMano($p){
		extract($p);
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['user_id_av'];
		$date_creation = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$dataBase_Class = new Database();

		$query_valida = "SELECT * FROM ludus_hand_up_av where schedule_id = $schedule_id and user_id = $idQuien;";
		$resultado = $dataBase_Class->SQL_SelectMultipleRows($query_valida);

		$res = array();
		if( count($resultado) > 0 ){
			if( $resultado[0]['status_id'] == 2 ){
				$res['error'] = true;
				$res['msj'] = 'Se encuentra en espera';
				return $res;
			}else{
				$query_sql = "UPDATE ludus_hand_up_av SET status_id = '2' WHERE user_id = $idQuien;";
				$resultado = $dataBase_Class->SQL_Update($query_sql);
			}
		}else{
			$query_sql = "INSERT INTO ludus_hand_up_av
						(
						user_id,
						schedule_id,
						status_id,
						date_creation)
						VALUES
						(
						'$idQuien',
						'$schedule_id',
						2,
						'$date_creation')";
			$resultado = $dataBase_Class->SQL_Update($query_sql);
		}

		
		
		if ($resultado> 0) {
			$res['error'] = false;
		}else{
			$res['error'] = true;
		}
		
		unset($dataBase_Class);
		return $res;
	}

	public function quienLevanto( $schedule_id){
		include_once( '../../config/database.php' );
		include_once( '../../config/config.php' );
		$query_sql = "SELECT us.user_id_av, us.usuario, hu.status_id, silenciar
						from ludus_hand_up_av hu
								INNER JOIN ludus_asistentes_av us
									on hu.user_id = us.user_id_av
									where hu.schedule_id = $schedule_id and us.schedule_id_av = $schedule_id and hu.status_id in (2, 1);";//status_id = 1 LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}

	public function silenciar( $schedule_id, $user_id){
		include_once( '../../config/database.php' );
		include_once( '../../config/config.php' );
		$DataBase_Class = new Database();
		$query_sql = "SELECT * from ludus_hand_up_av where schedule_id = $schedule_id and user_id = $user_id;";//status_id = 1 LIMIT 0,20
		$hand_inf = $DataBase_Class->SQL_SelectRows($query_sql);

		$silenciar = $hand_inf['silenciar'] == 0 ? 1 : 0;
		$query_up = "UPDATE ludus_hand_up_av SET silenciar = $silenciar where schedule_id = $schedule_id and user_id = $user_id;";//status_id = 1 LIMIT 0,20
		$Rows_config = $DataBase_Class->SQL_Update($query_up);

		$res = array();
		if ($Rows_config) {
			$res['error'] = false;
			$res['msj'] = 'audio actualizado';
		}else{
			$res['error'] = true;
			$res['msj'] = 'audio no actualizado';
		}
		unset($DataBase_Class);
		return $res;
	}

	public function autorizarAlumno( $p){
		extract($p);
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		include_once( '../../config/database.php' );
		include_once( '../../config/config.php' );
		$query_sql = "UPDATE ludus_hand_up_av set status_id = 1 where schedule_id = $schedule_id and user_id = $user_id";//status_id = 1 LIMIT 0,20
		$DataBase_Class = new Database();
		$resultado = $DataBase_Class->SQL_Update($query_sql);
		unset($DataBase_Class);

		if ( $resultado> 0 ) {
			$res['error'] = false;
		}else{
			$res['error'] = true;
		}
		
		return $res;
	}

	public function quitar_alumno( $p){
		extract($p);
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		include_once( '../../config/database.php' );
		include_once( '../../config/config.php' );
		$query_sql = "UPDATE ludus_hand_up_av set status_id = 3 where schedule_id = $schedule_id and user_id = $user_id";//status_id = 1 LIMIT 0,20
		$DataBase_Class = new Database();
		$resultado = $DataBase_Class->SQL_Update($query_sql);
		unset($DataBase_Class);

		if ( $resultado > 0 ) {
			$res['error'] = false;
		}else{
			$res['error'] = true;
		}
		
		return $res;
	}

	public function quitarNotificaciones( $schedule_id){
		include_once( '../../config/database.php' );
		include_once( '../../config/config.php' );
		$query_sql = "UPDATE ludus_hand_up_av set status_id = 3 where schedule_id = $schedule_id";//status_id = 1 LIMIT 0,20
		$DataBase_Class = new Database();
		$resultado = $DataBase_Class->SQL_Update($query_sql);
		unset($DataBase_Class);

		if ( $resultado> 0 ) {
			$res['error'] = false;
		}else{
			$res['error'] = true;
		}
		
		return $res;
	}

	public function validarAutorizacion($schedule_id)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['user_id_av'];
		$dataBase_Class = new Database();

		$query_valida = "SELECT * FROM ludus_hand_up_av where schedule_id = $schedule_id and status_id in (1, 2, 3) and user_id = $idQuien;";
		$resultado = $dataBase_Class->SQL_SelectMultipleRows($query_valida);
		if ( count($resultado) > 0 ) {

			if( $resultado[0]['status_id'] == 1 ){
				$res['status_id'] = $resultado[0]['status_id'];
				$res['error'] = false;
				$res['autorizado'] = 'si';
				$res['silenciar'] = $resultado[0]['silenciar'];
			}elseif( $resultado[0]['status_id'] == 2 ){
				$res['status_id'] = $resultado[0]['status_id'];
				$res['error'] = true;
				$res['autorizado'] = 'Esperando autorizacion';
				$res['silenciar'] = 0;
			}else{
				$res['status_id'] = $resultado[0]['status_id'];
				$res['error'] = true;
				$res['autorizado'] = 'denegado';
				$res['silenciar'] = 0;
			}
			
		}else{
			$res['error'] = true;
			$res['autorizado'] = 'No ha levantado la mano';
			$res['silenciar'] = 0;
		}
		return $res;
	}

	public function cargarPregunta( $question_id )
	{	
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$dataBase_Class = new Database();

		$query_pre = "SELECT * FROM ludus_courses_files_av where question_id = $question_id;";
		$pregunta = $dataBase_Class->SQL_SelectRows($query_pre);

		$query_res = "SELECT * from  ludus_respuestas_preguntas_cursos_av where question_id = $question_id;";
		$respuestas = $dataBase_Class->SQL_SelectMultipleRows($query_res);
		
		$res = '';
		foreach ($respuestas as $key => $value) {
			$res .= '<div class="radio">
						<label>
							<input type="radio" name="answer_id" value="'.$value['id'].'" required><span class="circle"></span><span class="check"></span>
								'.$value['answer'].'
							</label>
					</div>';
		}

		$formulario = '<div style="padding: 7%;">
							<div class="box-header with-border">
							<h3 class="box-title">'.$pregunta['name'].'</h3>
							</div>
							<form id="frm-responder">
							'.$res.'
							 <input type="hidden" name="question_id" value="'.$pregunta['question_id'].'">
							<div class="box-footer">
								<button style="background-color: #4caf50 !important;" id="responder" type="submit" class="btn bg-green">Responder</button>
							</div>
							</form>
					<div>';

		return $formulario;
	}

	public function responderPreguntas( $p){
		extract($p);
		@session_start();
		$idQuien = $_SESSION['user_id_av'];
		$date_creation = date("Y-m-d")." ".(date("H")).":".date("i:s");
		include_once( '../../config/database.php' );
		include_once( '../../config/config.php' );
		$DataBase_Class = new Database();
		

		$query_valida = "SELECT * from ludus_respuestas_usuarios_cursos_av where  question_id = $question_id and user_id = $idQuien and schedule_id = $schedule_id";
		$valida = $DataBase_Class->SQL_SelectMultipleRows($query_valida);

		$res = array();
		if( count($valida) > 0 ){
			$res['error'] = true;
			$res['msj'] = 'Ya respondiste esta pregunta';
			$res['type'] = 'error';
			return $res;
		}
		$query_aproval = "SELECT result from ludus_respuestas_preguntas_cursos_av where id = $answer_id";
		$aproval = $DataBase_Class->SQL_SelectRows($query_aproval);
		$aproval = $aproval['result'];

		$query_sql = "INSERT INTO ludus_respuestas_usuarios_cursos_av
								(
								question_id,
								answer_id,
								user_id,
								course_id,
								schedule_id,
								aproval,
								date_creation,
								status_id
								)
								VALUES
								(
								'$question_id',
								'$answer_id',
								'$idQuien',
								'0',
								'$schedule_id',
								'$aproval',
								'$date_creation',
								'1');";
		
		$resultado = $DataBase_Class->SQL_Update($query_sql);
		unset($DataBase_Class);

		if ( $resultado> 0 ) {
			$res['error'] = false;
			$res['msj'] = 'Recibimos tu respuesta';
			$res['type'] = 'success';
			
		}else{
			$res['error'] = true;
		}
		
		return $res;
	}


	public function finAlumnoTransmisor($schedule_id)
	{
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['user_id_av'];
		$query_sql = "UPDATE ludus_hand_up_av set status_id = 4 where user_id = $idQuien and schedule_id = $schedule_id";
		$dataBase_Class = new Database();
		$resultado = $dataBase_Class->SQL_Update($query_sql);
		if ( $resultado > 0 ) {
			$res['error'] = false;
			$res['msj'] = 'Finalizaste la transmision';
			//$res['url'] = 'virtualclass.php?token=f9a324946c883db2f221d80c457b1144&_valSco=50203f8bc0ac1121b683a40f1c55d7fd&opcn=ver&id=5&schedule='.$schedule_id;
		}else{
			$res['error'] = true;
		}
		return $res;
	}

	function alumnoTransmisor($schedule_id){
		@session_start();
		$user_id = $_SESSION['user_id_av'];
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT * from ludus_hand_up_av where schedule_id = $schedule_id and user_id = $user_id and status_id = 1";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}

	// reporte de usuarios que respondieron las preguntas que aparecen en medio del curso
	function reporte_respuestas($question_id, $schedule_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Class = new Database();

		$usuarios_inscritos = "SELECT ins.usuario, ins.user_id_av, '' as answer, '' as aproval
								FROM ludus_asistentes_av ins
										where ins.schedule_id_av = $schedule_id";
		$inscritos = $DataBase_Class->SQL_SelectMultipleRows($usuarios_inscritos);

		$query_pre = "SELECT * from ludus_courses_files_av where question_id = $question_id";
		$pre = $DataBase_Class->SQL_SelectRows($query_pre);

		$query_res = "SELECT resu.user_id, resu.answer_id, respre.id, respre.answer from ludus_respuestas_usuarios_cursos_av resu
							inner join ludus_respuestas_preguntas_cursos_av respre
								on resu.answer_id = respre.id
						WHERE resu.schedule_id = $schedule_id and resu.question_id = $question_id";
		$res = $DataBase_Class->SQL_SelectMultipleRows($query_res);
		unset($DataBase_Class);

		// $fallaron = 0;
		// $acertaron = 0;
		// foreach ( $res as $key => $value ) {
		// 	if ($value['aproval'] == 'si') {
		// 		$acertaron++;
		// 	}else{
		// 		$fallaron++;
		// 	}
		// }

		foreach ($res as $key1 => $value1) {
			foreach ($inscritos as $key2 => $value2) {
				if( $value1['user_id'] == $value2['user_id_av']){
					$inscritos[$key2]['answer'] = $value1['answer'];
					//$inscritos[$key2]['aproval'] = $value1['aproval'];
				}
			}
		}

		$datos = array();
		$datos['pre'] = $pre;
		$datos['res'] = $inscritos;
		$datos['respondieron'] = count($res);
		// $datos['fallaron'] = $fallaron;
		// $datos['acertaron'] = $acertaron;
		$datos['conectados'] = count($inscritos);

		return $datos;
	}

	// function conectados($schedule_id){
	// 	@session_start();
	// 	$user_id = $_SESSION['idUsuario'];
	// 	include_once('../../config/database.php');
	// 	include_once('../../config/config.php');
	// 	$DataBase_Class = new Database();
	// 	$query_sql = "SELECT i.inscription_id,i.course_id,i.status_id, u.user_id, u.last_name, u.first_name, u.image, h.headquarter, d.dealer
	// 						FROM ludus_courses c, ludus_headquarters h, ludus_users u, ludus_inscriptions i, ludus_dealers d
	// 						WHERE c.course_id = i.course_id
	// 						AND u.headquarter_id = h.headquarter_id
	// 						AND i.user_id = u.user_id AND i.schedule_id = '$schedule_id'
	// 						AND d.dealer_id = h.dealer_id";
	// 	$inscritos = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
	// 	unset($DataBase_Class);
	// 	return $inscritos;
	// }


	//Al finalizar le curso
	public static function actualizarAsistencia( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		@session_start();
		$not = $p['status_id'] == 3 ? "NOT" : "";
		DB::update( 'ludus_invitation', [
			"status_id" => $p['status_id'],
			"date_result" => DB::sqleval('NOW()'),
			"editor" => $_SESSION['idUsuario'],
		], "schedule_id IN ( {$p['schedule_id']} ) AND user_id $not IN (
		SELECT user_id FROM ludus_inscriptions WHERE schedule_id IN ( {$p['schedule_id']} ) )" );
		return DB::affectedRows();
	}
	
	public static function insertarNotaDefecto( $module_result_usr, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		DB::insert('ludus_modules_results_usr', $module_result_usr );
		return DB::insertId();
	}
	
	public static function listadoParticipantes( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		$query = "SELECT 0 AS score, 0 AS score_review, 0 AS score_waybill, 0 AS score_evaluation, i.user_id AS user_id, m.module_id AS module_id, 'NO' AS approval,
			'' AS file, NOW() AS date_creation, 1 AS status_id, s.teacher_id AS creator, i.invitation_id AS invitation_id, 0 AS editor_ev
		    FROM ludus_invitation i, ludus_schedule s, ludus_courses c, ludus_modules m
		    WHERE i.schedule_id IN ( {$p['schedule_id']} )
		    AND i.schedule_id = s.schedule_id
		    AND s.course_id = c.course_id
		    AND c.course_id = m.course_id
		    AND i.invitation_id NOT IN (SELECT invitation_id FROM ludus_modules_results_usr )";
		$registros = DB::query( $query );
		return $registros;
	}
	
	public static function insertarNotaRefuerzo( $waybills, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		DB::insert('ludus_waybills', $waybills );
		return DB::insertId();
	}
	
	public static function listadoParticipantesRefuerzo( $p, $prof = "../" ){
		include_once($prof.'../config/init_db.php');
		$query = "SELECT m.module_result_usr_id AS module_result_usr_id, m.user_id AS user_id, s.teacher_id AS teacher_id, NOW() AS date_creation, NULL AS description, 0 AS score, NULL AS date_edition,
			'' AS feedback, 0 AS user_feedback, NULL AS date_feedback, 1 AS status_id, NULL AS fec_fulfillment, '' AS target, '' AS action
		    FROM ludus_invitation i, ludus_modules_results_usr m, ludus_schedule s
		    WHERE i.invitation_id = m.invitation_id
		    AND i.schedule_id = s.schedule_id
			AND i.schedule_id IN ( {$p['schedule_id']} )
		    AND m.module_result_usr_id NOT IN (SELECT module_result_usr_id FROM ludus_waybills )";
		$registros = DB::query( $query );
		return $registros;
	}

	public  function finalizar_curso( $schedule_id ){
		include_once( '../../config/init_db.php' );
		$query = "UPDATE ludus_schedule_av set status_id = 2 where schedule_id = $schedule_id;";
		$res = DB::query( $query );
		$json = array();
		if( $res ){
			$json['error'] = false;
			$json['msj'] = 'Trasmision finalizada correctamente';
		}else{
			$json['error'] = true;
			$json['msj'] = 'No se pudo finalizar';
		}
		return $json;
	}

	public  function statusSchedule( $schedule_id ){
		include_once( '../config/init_db.php' );
		$query = "SELECT * FROM ludus_schedule_av where schedule_id = $schedule_id;";
		$res = DB::query( $query );
		return $res[0];
	}

	public  function cargos_aula( $schedule_id ){
		include_once( '../config/init_db.php' );
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		$query = "SELECT * from ludus_charges_users chaus
					INNER JOIN ludus_aulas_charges au
						on chaus.charge_id = au.charge_id
					where chaus.user_id = $user_id and au.conference_id = $schedule_id";
		$res = DB::query( $query );
		return $res;
	}
		
	

}//fin clase CursosVCT
