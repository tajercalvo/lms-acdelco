<?php
Class ReporteEncuestas {

	public function getEncuestas(){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM ludus_encuestas;";
		$Rows_config = DB::query($query);
		return $Rows_config;
	}

	public static function getReporte( $p ){
		extract($p);
		include_once('../config/init_db.php');
		$query = "SELECT en.encuesta, enpre.encuesta_pregunta, enres.puntaje, enres.creacion, d.dealer, h.headquarter, u.first_name, u.last_name, u.identification
		FROM ludus_encuestas_respuestas_usuarios enres
				INNER JOIN ludus_encuestas_preguntas enpre
					on enres.encuesta_pregunta_id = enpre.encuesta_pregunta_id
				INNER JOIN ludus_encuestas en
					on enres.encuesta_id = en.encuesta_id
				INNER JOIN ludus_users u
					on u.user_id = enres.user_id
				INNER JOIN ludus_headquarters h
					on h.headquarter_id = enres.headquarter_id
				INNER JOIN ludus_dealers d
					on d.dealer_id = enres.dealer_id
				where en.encuesta_id = $encuesta_id
				order by enres.creacion desc";
		$Rows_config = DB::query($query);
		return $Rows_config;
	}

        
}
