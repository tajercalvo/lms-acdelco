<?php
Class RepLogistica {
	function consultaDatos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>=5){
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 ORDER BY dealer";
		}else{
			$query_sql = "SELECT dealer, dealer_id FROM ludus_dealers WHERE status_id = 1 AND dealer_id = $dealer_id ORDER BY dealer";
		}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaParametros(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		
		$query_sql = "SELECT * FROM ludus_parameters WHERE parameter_id IN (1,2)";
		
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaDatosAll($dealer_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');

		if ($dealer_id == "0") {

			$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, l.living, h.headquarter, d.dealer_id, d.dealer, i.inscriptions, c.course, c.newcode, c.type, c.course_id, IF( (SELECT schedule_id FROM ludus_feeding WHERE schedule_id=s.schedule_id) = s.schedule_id,(SELECT CONCAT(total,'-',status_id) FROM ludus_feeding WHERE schedule_id=s.schedule_id ),'FALSE' ) as feeding FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM `ludus_dealer_schedule` GROUP BY schedule_id) as i WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = i.schedule_id AND s.course_id = c.course_id AND c.type IN ('IBT','VCT') AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND s.status_id = 1";
		} else {
			$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, l.living, h.headquarter, d.dealer_id, d.dealer, i.inscriptions, c.course, c.newcode, c.type, c.course_id, IF( (SELECT schedule_id FROM ludus_feeding WHERE schedule_id=s.schedule_id) = s.schedule_id,(SELECT CONCAT(total,'-',status_id) FROM ludus_feeding WHERE schedule_id=s.schedule_id ),'FALSE' ) as feeding FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM `ludus_dealer_schedule` GROUP BY schedule_id) as i WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = i.schedule_id AND s.course_id = c.course_id AND c.type IN ('IBT','VCT') AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND d.dealer_id = $dealer_id AND s.status_id = 1";
			/*$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, l.living, h.headquarter, d.dealer, i.inscriptions, c.course, c.newcode, c.type, c.course_id, IF( (SELECT schedule_id FROM ludus_feeding WHERE schedule_id=s.schedule_id AND status_id=1) = s.schedule_id,(SELECT total FROM ludus_feeding WHERE schedule_id=s.schedule_id ),'FALSE' ) as feeding FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM `ludus_dealer_schedule` GROUP BY schedule_id) as i WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = i.schedule_id AND s.course_id = c.course_id AND c.type IN ('IBT','VCT') AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND d.dealer_id = $dealer_id AND s.status_id = 1";*/
		}


		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Consulta la duracion horaria de cada curso
	//====================================================================================
	function consultaHoras($schedule_id, $course_id, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');

		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, d.dealer_id, d.dealer, c.course_id, c.course, m.module_id, m.module_id, m.duration_time FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, ludus_modules m  WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.course_id = c.course_id AND c.course_id = m.course_id AND s.schedule_id = $schedule_id AND m.course_id = $course_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Crea la logistica por sesion
	//====================================================================================
	function guardarLogistica($schedule_id, $costos, $files, $domicile, $total, $status, $notes, $cantidad_datos, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		//die();
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$sql = "";
		if ($status == 1) {
			$sql = ",user_appro='$idQuien',date_appro='$NOW_data'" ;
			$notes = "";
		}

		$query_sql = "SELECT * FROM ludus_feeding where schedule_id = $schedule_id ";
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		$num_rows = $DataBase_Acciones->SQL_SelectCantidad($query_sql);


		if( $num_rows>0 ){
			//Se actualiza los Datos Basicos de Alimentacion
			$updateSQL_ER = "UPDATE ludus_feeding SET domicile='$domicile',date_edition='$NOW_data', user_id='$idQuien',status_id='$status',total='$total',notes='$notes' {$sql} WHERE schedule_id='$schedule_id' ";
			$resultado = $DataBase_Acciones->SQL_Update($updateSQL_ER);

			if ( $resultado > 0 ) {

				$feeding_id_edit = $Rows_config['feeding_id'];
				$valor = $archivo = $tipo = $dia = "";

				for($m=1;$m<=$cantidad_datos;$m++) { 

					if ( $costos[$m]!="" && $files[$m]!="" ) { 

						$valor = $costos[$m]; //Costo
						$archivo = $files[$m]; //Factura

						if ($m%2==0){ $tipo = 2; } //Almuerzo
						else { $tipo = 1; } //Refrigerio

						$dia = ceil( $m/2 );//Dia del alimento

						//Se actualiza los Detalles de Alimentacion
						$updateSQL_Detalle_ER = "UPDATE ludus_feeding_files SET cost='$valor', file='$archivo'
							WHERE feeding_id='$feeding_id_edit' AND type_feeding='$tipo' AND day_feeding='$dia' "; 
						$resultado += $DataBase_Acciones->SQL_Update($updateSQL_Detalle_ER);

					}
				}
			}

		} else {
			//Se crea los Datos Basicos de Alimentacion
			$query_dealer = "SELECT s.schedule_id, l.living_id, l.living, h.headquarter_id, h.headquarter, d.dealer_id, d.dealer FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = $schedule_id";
			$Rows_dealer = $DataBase_Acciones->SQL_SelectRows($query_dealer);
			$dealer_id = $Rows_dealer['dealer_id'];

			$insertSQL_EE = "INSERT INTO ludus_feeding (schedule_id,dealer_id,domicile,date_edition, user_id,status_id,notes,total) 
							VALUES ('$schedule_id','$dealer_id','$domicile','$NOW_data','$idQuien','0','','$total')";
			$resultado = $DataBase_Acciones->SQL_Insert($insertSQL_EE);

			if ( $resultado > 0 ) {
				
				$query_feeding = "SELECT MAX(feeding_id) AS id FROM ludus_feeding";
				$Rows_feeding = $DataBase_Acciones->SQL_SelectRows($query_feeding);
				$feeding_id = $Rows_feeding['id'];
				$valor = $archivo = $tipo = $dia = "";

				for($m=1;$m<=$cantidad_datos;$m++) { 

					if ( $costos[$m]!="" && $files[$m]!="" ) {

						$valor = $costos[$m]; //Costo
						$archivo = $files[$m]; //Factura

						if ($m%2==0){ $tipo = 2; } //Almuerzo
						else { $tipo = 1; } //Refrigerio

						$dia = ceil( $m/2 );//Dia del alimento

						//Se crea los Detalles de Alimentacion
						$insertSQL_Detalle_EE = "INSERT INTO ludus_feeding_files (feeding_id,type_feeding,day_feeding,cost,file) 
							VALUES ('$feeding_id','$tipo','$dia','$valor','$archivo')"; 
						$resultado += $DataBase_Acciones->SQL_Insert($insertSQL_Detalle_EE);
					}

				}

			}
			
		}
		
		unset($DataBase_Acciones);
		return $resultado;
	}

	//====================================================================================
	//Consulta la logistica por sesion
	//====================================================================================
	function consultaLogistica($schedule_id, $prof="" ){
		include_once($prof.'../config/database.php');
		include_once($prof.'../config/config.php');

		$query_sql = "SELECT * FROM ludus_feeding WHERE schedule_id= '$schedule_id' ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);

		$feeding_id = $Rows_config['feeding_id'];
		$query_sql_detail = "SELECT * FROM ludus_feeding_files WHERE feeding_id = '$feeding_id' ORDER BY feeding_files_id ";
		$Rows_config_detail = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_detail);

		for($l=0;$l<count($Rows_config_detail);$l++) { 
			$Rows_config['Detalle_fedding'][] = array( 'tipo' => $Rows_config_detail[$l]['type_feeding'], 'dia' => $Rows_config_detail[$l]['day_feeding'], 'costo' => $Rows_config_detail[$l]['cost'], 'file' => $Rows_config_detail[$l]['file'] );
		} 

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//====================================================================================
	//Cambios tabla
	//====================================================================================
	function cambioTabla(){
		include_once('../config/database.php');
		include_once('../config/config.php');

		$query_sql = "SELECT * FROM ludus_feeding ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		for($l=0;$l<count($Rows_config);$l++) { 

			$feeding_id = $Rows_config[$l]['feeding_id'];

			if ($Rows_config[$l]['refreshment_cost_1'] > 0) {
				$Rows_config[$l]['Detalle_fedding'][] = array($feeding_id,1,1,$Rows_config[$l]['refreshment_cost_1'],$Rows_config[$l]['refreshment_file_1'] );
			}
			if ($Rows_config[$l]['lunch_cost_1'] > 0) {
				$Rows_config[$l]['Detalle_fedding'][] = array($feeding_id,2,1,$Rows_config[$l]['lunch_cost_1'],$Rows_config[$l]['lunch_file_1'] );
			}
			if ($Rows_config[$l]['refreshment_cost_2'] > 0) {
				$Rows_config[$l]['Detalle_fedding'][] = array($feeding_id,1,2,$Rows_config[$l]['refreshment_cost_2'],$Rows_config[$l]['refreshment_file_2'] );
			}
			if ($Rows_config[$l]['lunch_cost_2'] > 0) {
				$Rows_config[$l]['Detalle_fedding'][] = array($feeding_id,2,2,$Rows_config[$l]['lunch_cost_2'],$Rows_config[$l]['lunch_file_2'] );
			}

		}

		$resultado = 0;
		for($l=0;$l<count($Rows_config);$l++) { 
			
			if ( isset($Rows_config[$l]['Detalle_fedding']) ) {
				
				for($m=0;$m<count($Rows_config[$l]['Detalle_fedding']);$m++) { 

					$opc1=$Rows_config[$l]['Detalle_fedding'][$m][0];
					$opc2=$Rows_config[$l]['Detalle_fedding'][$m][1];
					$opc3=$Rows_config[$l]['Detalle_fedding'][$m][2];
					$opc4=$Rows_config[$l]['Detalle_fedding'][$m][3];
					$opc5=$Rows_config[$l]['Detalle_fedding'][$m][4];

					$insertSQL_EE = "INSERT INTO ludus_feeding_files (feeding_id,type_feeding,day_feeding,cost,file) 
							VALUES ('$opc1','$opc2','$opc3','$opc4','$opc5')"; 
					$resultado += $DataBase_Acciones->SQL_Insert($insertSQL_EE);

				}

			}
			
		}

		unset($DataBase_Acciones);
		echo "Cambios: ".$resultado;
		die();
	}

}
