<?php
Class Calificaciones {
	function consultaDatos($schedule_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT i.date_send, u.user_id, u.identification, u.first_name, u.last_name, i.invitation_id, i.status_id, h.headquarter, u.email
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h
						WHERE i.schedule_id = '$schedule_id' AND i.user_id = u.user_id AND u.status_id = 1 AND u.headquarter_id = h.headquarter_id
						ORDER BY h.headquarter, u.first_name, u.last_name ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		for($i=0;$i<count($Rows_config);$i++) {
			$usr = $Rows_config[$i]['user_id'];
			$inv = $Rows_config[$i]['invitation_id'];
			$query_CalifAnt = "SELECT ru.score, ru.approval, li.status_id
							FROM ludus_modules_results_usr ru, ludus_invitation li
							WHERE ru.user_id = '$usr' AND ru.invitation_id = li.invitation_id AND
							li.schedule_id = '$schedule_id' AND li.user_id = '$usr' AND li.invitation_id = '$inv' ";
			$Rows_Historico = $DataBase_Acciones->SQL_SelectMultipleRows($query_CalifAnt);
			$Rows_config[$i]['Califica_Historico'] = $Rows_Historico;
			$query_Charges = "SELECT c.charge
							FROM ludus_charges_users u, ludus_charges c
							WHERE u.user_id = '$usr' AND u.charge_id = c.charge_id";
			$Rows_Cargos = $DataBase_Acciones->SQL_SelectMultipleRows($query_Charges);
			$Rows_config[$i]['Cargos'] = $Rows_Cargos;
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad($schedule_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT i.date_send, u.user_id, u.identification, u.first_name, u.last_name, i.invitation_id, i.status_id, h.headquarter
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h
						WHERE i.schedule_id = '$schedule_id' AND i.user_id = u.user_id AND u.status_id = 1 AND u.headquarter_id = h.headquarter_id
						ORDER BY h.headquarter, u.first_name, u.last_name ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaSesiones($fec_ini,$fec_fin){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		echo $query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.course, l.living, l.address, c.newcode, c.type
						FROM ludus_schedule s, ludus_courses c, ludus_livings l
						WHERE s.course_id = c.course_id AND s.teacher_id = '$idQuien' AND s.living_id = l.living_id AND s.start_date BETWEEN '$fec_ini 00:00:00' AND '$fec_fin 23:59:59'
						ORDER BY c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaSesion($schedule_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.course, l.living, l.address, c.course_id, c.newcode, i.area, u.first_name, u.last_name, c.newcode, c.type
						FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_areas i, ludus_users u
						WHERE s.course_id = c.course_id AND s.schedule_id = '$schedule_id' AND s.living_id = l.living_id
						AND l.city_id = i.area_id AND s.teacher_id = u.user_id
						ORDER BY c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearCalificacion($idQuien,$user_id,$course_id,$Inv_id,$Califica,$Asistio){
		include_once('../../config/init_db.php');
		@session_start();
		$approval = 'NO';
		if($Califica >= 80){
			$approval = 'SI';
		}
		$status_id = 1;
		if($Asistio=="SI"){
			$status_id = 2;
		}else{
			$status_id = 3;
		}
		$query_ModulesCourse = "SELECT m.module_id
								FROM ludus_courses c, ludus_modules m
								WHERE c.course_id = '$course_id' AND c.course_id = m.course_id ";

		$Rows_config = DB::query($query_ModulesCourse);
		foreach ($Rows_config as $iID => $Modules) {
			$module_id = $Modules['module_id'];
			/*Borra las calificaciones anteriores de esta invitacion*/

			$resultado_Wall = DB::delete( 'ludus_waybills', "module_result_usr_id IN (SELECT module_result_usr_id FROM ludus_modules_results_usr WHERE user_id = $user_id AND module_id = $module_id AND invitation_id = $Inv_id)" );
			$resultado_Wall = DB::delete( 'ludus_modules_results_usr', "user_id = $user_id AND module_id = $module_id AND invitation_id = $Inv_id" );

			/*Borra las calificaciones anteriores de esta invitacion*/
			$insert_mru = [
				"score" => $Califica,
				"user_id" => $user_id,
				"module_id" => $module_id,
				"approval" => $approval,
				"date_creation" => DB::sqleval('NOW()'),
				"status_id" => 1,
				"creator" => $idQuien,
				"invitation_id" => $Inv_id
			];
			// print_r( $insert_mru );
			// echo "<br>";
			DB::insert( 'ludus_modules_results_usr', $insert_mru );
			$resultado = DB::insertId();
			// print_r( $resultado );
			/*Crea la hoja de ruta*/
			if($Asistio=="SI"){

				$insert_wayb = [
					"module_result_usr_id" => $resultado,
					"user_id" => $user_id,
					"teacher_id" => $idQuien,
					"date_creation" => DB::sqleval('NOW()'),
					"score" => 0,
					"status_id" => 1
				];
				DB::insert( 'ludus_waybills', $insert_wayb );
			}
			/*Crea la hoja de ruta*/

			DB::update( 'ludus_invitation', [
				"status_id" => $status_id,
				"date_result" => DB::sqleval('NOW()'),
				"editor" => $_SESSION['idUsuario']
			], 'invitation_id = '.$Inv_id );

		}

		return 1;
	}// fin CrearCalificacion

	function CrearCalificacionOld($idQuien,$user_id,$course_id,$Inv_id,$Califica,$Asistio){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$approval = 'NO';
		if($Califica >= 80){
			$approval = 'SI';
		}
		$status_id = 1;
		if($Asistio=="SI"){
			$status_id = 2;
		}else{
			$status_id = 3;
		}
		$query_ModulesCourse = "SELECT m.module_id
								FROM ludus_courses c, ludus_modules m
								WHERE c.course_id = '$course_id' AND c.course_id = m.course_id ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_ModulesCourse);
		foreach ($Rows_config as $iID => $Modules) {
			$module_id = $Modules['module_id'];
			/*Borra las calificaciones anteriores de esta invitacion*/
			$queryDelete_Wall = "DELETE FROM ludus_waybills WHERE module_result_usr_id IN (SELECT module_result_usr_id FROM ludus_modules_results_usr WHERE user_id = '$user_id' AND module_id = '$module_id' AND invitation_id = '$Inv_id')";
			$resultado_Wall = $DataBase_Acciones->SQL_Insert($queryDelete_Wall);

			$Query_delete = "DELETE FROM ludus_modules_results_usr WHERE user_id = '$user_id' AND module_id = '$module_id' AND invitation_id = '$Inv_id' ";
			$resultado = $DataBase_Acciones->SQL_Insert($Query_delete);
			/*Borra las calificaciones anteriores de esta invitacion*/
			$Query_insert = "INSERT INTO ludus_modules_results_usr (score,user_id,module_id,approval,date_creation,status_id,creator,invitation_id) VALUES
			('$Califica','$user_id','$module_id','$approval',NOW(),1,'$idQuien','$Inv_id');";
			//echo($Query_insert);
			$resultado = $DataBase_Acciones->SQL_Insert($Query_insert);
			/*Crea la hoja de ruta*/
			if($Asistio=="SI"){
				$Query_insertHojaRuta = "INSERT INTO ludus_waybills (module_result_usr_id,user_id,teacher_id,date_creation,score,status_id) VALUES ('$resultado','$user_id','$idQuien',NOW(),0,1)";
				$resultado_HR = $DataBase_Acciones->SQL_Insert($Query_insertHojaRuta);
			}
			/*Crea la hoja de ruta*/

			$Query_update = "UPDATE ludus_invitation SET status_id = '$status_id', date_result = NOW(), editor = {$_SESSION['idUsuario']}  WHERE invitation_id = '$Inv_id' ";
			$resultado = $DataBase_Acciones->SQL_Insert($Query_update);


		}
		unset($DataBase_Acciones);
		return 1;
	}
	function consultaDatosUsuario($idUsuario){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.*, headquarter
						FROM ludus_users u, ludus_headquarters c
						WHERE u.user_id = '$idUsuario' AND u.headquarter_id = c.headquarter_id";
		$DataBase_Class = new Database();
		$row_consulta = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $row_consulta;
	}
	function consultaDatosUsuarioJs($idUsuario){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT u.*, headquarter
						FROM ludus_users u, ludus_headquarters c
						WHERE u.user_id = '$idUsuario' AND u.headquarter_id = c.headquarter_id";
		$DataBase_Class = new Database();
		$row_consulta = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $row_consulta;
	}
	function consultaUsuariosIns($schedule_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT u.first_name, u.last_name, u.user_id, u.identification, h.headquarter
				FROM ludus_dealer_schedule s, ludus_headquarters h, ludus_users u
				WHERE s.schedule_id = $schedule_id AND s.min_size>0 AND s.max_size>0
				AND s.dealer_id = h.dealer_id AND h.headquarter_id = u.headquarter_id AND u.status_id = 1
				ORDER BY h.headquarter, u.first_name";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	public static function CalificaOJT( $p, $prof = "../" ){
		include_once( $prof.'../config/init_db.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		// $DataBase_Acciones = new Database();
		// $fecha = date('Y-m-d H:m:s');
		$module_id = 0;
		$query_ModulesCourse = "SELECT m.module_id
								FROM ludus_courses c, ludus_modules m
								WHERE c.course_id = {$p['course_idOJT']} AND c.course_id = m.course_id ";

		$usuario = DB::queryFirstRow( "SELECT u.user_id, u.first_name, u.last_name, u.identification, u.headquarter_id FROM ludus_users u WHERE u.user_id = {$p['userOJT_id']}" );

		$Rows_config = DB::query($query_ModulesCourse);

		DB::insert( 'ludus_invitation', [
				'schedule_id' 		=> $p['Schedule_idOJT'],
				'user_id'			=> $p['userOJT_id'],
				'headquarter_id'	=> $usuario['headquarter_id'],
				'date_send'			=> DB::sqleval('NOW()'),
				'text'				=> NULL,
				'result_preview'	=> 2,
				'date_result'		=> DB::sqleval('NOW()'),
				'status_id'			=> 2,
				'creator'			=> $idQuien,
				'editor'			=> $idQuien
			] );
		$resultado_Inv = DB::insertId();

		if($resultado_Inv>0){
			foreach ($Rows_config as $iID => $Modules) {
				$module_id = $Modules['module_id'];
				$approval_Ojt = $p['CalifOJT'] > 79 ? 'SI' : 'NO';

				DB::insert( 'ludus_modules_results_usr', [
						'score' 			=> $p['CalifOJT'],
						'user_id' 			=> $p['userOJT_id'],
						'module_id'			=> $module_id,
						'approval'			=> $approval_Ojt,
						'file'				=> '',
						'date_creation'		=> DB::sqleval('NOW()'),
						'status_id'			=> 1,
						'creator'			=> $idQuien,
						'invitation_id'		=> $resultado_Inv
					] );
				$resultado_nota = DB::insertId();

				if($resultado_nota>0){
					// $Query_insertHojaRuta = "INSERT INTO ludus_waybills (module_result_usr_id,user_id,teacher_id,date_creation,score,status_id) VALUES ('$resultado_nota','$userOJT_id','$idQuien',NOW(),0,1)";
					DB::insert( 'ludus_waybills', [
							'module_result_usr_id' => $resultado_nota,
							'user_id' => $p['userOJT_id'],
							'teacher_id' => $idQuien,
							'date_creation' => DB::sqleval('NOW()'),
							'score' => 0,
							'status_id' => 1
						] );

					// $Query_ins_Obs = "INSERT INTO ludus_modules_results_usr_notes VALUES ($resultado_nota,$ObservOJT,$idQuien,NOW())";
					DB::insert( 'ludus_modules_results_usr_notes', [
							'module_result_usr_id'	=> $resultado_nota,
							'notes'					=> $p['ObservOJT'],
							'user_id'				=> $idQuien,
							'date_creation'			=> DB::sqleval('NOW()')
						] );
				}
			}
		}

		return $resultado_Inv;
	}

	/*-----------------------------------------------
	Juan Carlos Villar
	20 Junio de 2017 Descargar loista de asistencia
	-----------------------------------------------*/

		//si
	function descargarConsultaDatos($schedule_id){
		$ruta = '../../../';
		include_once($ruta.'../config/database.php');
		// include_once('../config/config.php');
		$query_sql = "SELECT i.date_send, u.user_id, u.identification, u.first_name, u.last_name, i.invitation_id, i.status_id, h.headquarter, u.email
						FROM ludus_invitation i, ludus_users u, ludus_headquarters h
						WHERE i.schedule_id = '$schedule_id' AND i.user_id = u.user_id AND u.status_id = 1 AND u.headquarter_id = h.headquarter_id
						ORDER BY h.headquarter, u.first_name, u.last_name ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		for($i=0;$i<count($Rows_config);$i++) {
			$usr = $Rows_config[$i]['user_id'];
			$inv = $Rows_config[$i]['invitation_id'];
			$query_CalifAnt = "SELECT ru.score, ru.approval, li.status_id
							FROM ludus_modules_results_usr ru, ludus_invitation li
							WHERE ru.user_id = '$usr' AND ru.invitation_id = li.invitation_id AND
							li.schedule_id = '$schedule_id' AND li.user_id = '$usr' AND li.invitation_id = '$inv' ";
			$Rows_Historico = $DataBase_Acciones->SQL_SelectMultipleRows($query_CalifAnt);
			$Rows_config[$i]['Califica_Historico'] = $Rows_Historico;
			$query_Charges = "SELECT c.charge
							FROM ludus_charges_users u, ludus_charges c
							WHERE u.user_id = '$usr' AND u.charge_id = c.charge_id";
			$Rows_Cargos = $DataBase_Acciones->SQL_SelectMultipleRows($query_Charges);
			$Rows_config[$i]['Cargos'] = $Rows_Cargos;
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	//si
	function descargarConsultaSesion($schedule_id){
		$ruta = '../../../';
		include_once($ruta.'../config/database.php');
		// include_once('../config/config.php');
		// $idQuien = $_SESSION['idUsuario'];
		$query_sql = "SELECT s.schedule_id, s.start_date, s.end_date, c.course, l.living, l.address, c.course_id, c.code, i.city, u.first_name, u.last_name, c.newcode, c.type
						FROM ludus_schedule s, ludus_courses c, ludus_livings l, ludus_cities i, ludus_users u
						WHERE s.course_id = c.course_id AND s.schedule_id = '$schedule_id' AND s.living_id = l.living_id
						AND l.city_id = i.city_id AND s.teacher_id = u.user_id
						ORDER BY c.course";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearAsistencia($idQuien, $user_id, $course_id, $Inv_id, $Califica, $Asistio) {
		include_once('../../config/init_db.php');
		@session_start();
		DB::$encoding = 'utf8';
		$approval = 'NO';
		$status_id = 1;
		if ($Asistio == "SI") {
			$status_id = 2;
		} else {
			$status_id = 3;
		}
		$query_ModulesCourse = "SELECT m.module_id
								FROM ludus_courses c, ludus_modules m
								WHERE c.course_id = '$course_id' AND c.course_id = m.course_id ";
		
		$Rows_config = DB::query($query_ModulesCourse);
		
		foreach ($Rows_config as $iID => $Modules) {
			$module_id = $Modules['module_id'];
		}
		
	  
		$insert_mru = [
			"score" => $Califica,
			"user_id" => $user_id,
			"module_id" => $module_id,
			"approval" => $approval,
			"date_creation" => DB::sqleval('NOW()'),
			"status_id" => $status_id,
			"creator" => $idQuien,
			"invitation_id" => $Inv_id
		];
		
		DB::insert('ludus_modules_results_usr', $insert_mru);
		$resultado = DB::insertId();
	
		if ($Asistio == "SI") {
			$insert_wayb = [
				"module_result_usr_id" => $resultado,
				"user_id" => $user_id,
				"teacher_id" => $idQuien,
				"date_creation" => DB::sqleval('NOW()'),
				"score" => 0,
				"status_id" => 1
			];
	
			DB::insert('ludus_waybills', $insert_wayb);
		} else{
			
			DB::update('ludus_modules_results_usr', [
				"score" => 0,
				"status_id" => 1
			], "user_id = $user_id AND module_id = $module_id AND invitation_id = $Inv_id");
		}
	
		/* Actualiza la tabla ludus_invitation */
		DB::update('ludus_invitation', [
			"status_id" => $status_id,
			"date_result" => DB::sqleval('NOW()'),
			"editor" => $_SESSION['idUsuario']
		], 'invitation_id = ' . $Inv_id);
	
		return 1;
	}

	function CerrarCurso($schedule_id) {
		include_once('../../config/init_db.php');
		DB::$encoding = 'utf8';
		$terminarTodo = DB::query("UPDATE ludus_schedule SET calificacion_curso = 2 WHERE schedule_id = $schedule_id");
		
	  }

	/*--------------FIN descargar listado--------------*/
}
