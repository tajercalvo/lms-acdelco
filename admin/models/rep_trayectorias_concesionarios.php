<?php
Class Trayectorias {

	function trayectorias_usuarios($start_date, $end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		// $query_ppal = "SELECT c.charge_id, c.charge, count(*) as cantidad 
		// 			FROM ludus_charges c, ludus_charges_courses cc
		// 			WHERE c.charge_id IN (95,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137) 
		// 			AND c.charge_id = cc.charge_id
		// 			GROUP BY c.charge_id, c.charge
		// 			ORDER BY c.charge";
		$query_ppal = "SELECT dealer
		FROM ludus_dealers";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_ppal);

		$query = "SELECT u.user_id, u.identification, u.first_name, u.last_name, h.headquarter, d.dealer, AVG(no.score) as promedio, COUNT(*) as cantidad_curso
					FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges_courses cc, 
					(SELECT mr.user_id, mo.course_id, max(mr.score) as score
					FROM ludus_modules_results_usr mr, ludus_modules mo
					WHERE mr.date_creation BETWEEN '$start_date 00:00:00' and '$end_date 23:59:59'
					AND mr.module_id = mo.module_id
					AND mr.score > 79 AND mr.approval = 'SI'
					GROUP BY mr.user_id, mo.course_id) no
					WHERE u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND u.user_id = cu.user_id 
					AND cu.charge_id = cc.charge_id AND cc.charge_id IN (95,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137)
					AND u.user_id = no.user_id AND cc.course_id = no.course_id
					AND u.status_id = 1
					GROUP BY u.user_id, u.first_name, u.last_name, h.headquarter, d.dealer
					ORDER BY d.dealer, cantidad_curso DESC, promedio DESC ";
		$Rows_configDetail = $DataBase_Acciones->SQL_SelectMultipleRows($query);

		for($i=0;$i<count($Rows_config);$i++) {
			$charge_id = $Rows_config[$i]['dealer'];
			//Cargos
				for($y=0;$y<count($Rows_configDetail);$y++) {
					if( $charge_id == $Rows_configDetail[$y]['dealer'] ){
						$Rows_config[$i]['Details'][] = $Rows_configDetail[$y];
					}
				}
			//Cargos
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

}

