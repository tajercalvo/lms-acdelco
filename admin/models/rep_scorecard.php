<?php
Class Datos {
	function HorasHabilidad($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT t.specialty
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT MONTH(s.start_date) as mes, t.specialty, sum(m.duration_time) as horas
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY MONTH(s.start_date), t.specialty
		ORDER BY t.specialty, MONTH(s.start_date)";
		//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$specialty_id = $Rows_config[$i]['specialty'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $specialty_id == $Rows_cant[$x]['specialty'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function HorasPorHorasCurso($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT m.duration_time
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id
		ORDER BY m.duration_time";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT m.duration_time, MONTH(s.start_date) as mes, t.specialty, sum(m.duration_time) as horas
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY m.duration_time, MONTH(s.start_date), t.specialty
		ORDER BY m.duration_time, t.specialty, MONTH(s.start_date)";
		//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$duration_time = $Rows_config[$i]['duration_time'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $duration_time == $Rows_cant[$x]['duration_time'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function HoraHabilidadTipo($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT t.specialty, c.type
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT MONTH(s.start_date) as mes, c.type, t.specialty, sum(m.duration_time) as horas
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY MONTH(s.start_date), c.type, t.specialty
		ORDER BY c.type, t.specialty, MONTH(s.start_date)";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$specialty_id = $Rows_config[$i]['specialty'];
			$type_id = $Rows_config[$i]['type'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( ($specialty_id == $Rows_cant[$x]['specialty']) && ($type_id == $Rows_cant[$x]['type']) ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function HoraHabilidadTipoProveedor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT t.specialty, c.type, p.supplier
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT MONTH(s.start_date) as mes, p.supplier, c.type, t.specialty, sum(m.duration_time) as horas
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY MONTH(s.start_date), p.supplier, c.type, t.specialty
		ORDER BY p.supplier, c.type, t.specialty, MONTH(s.start_date)";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$specialty_id = $Rows_config[$i]['specialty'];
			$type_id = $Rows_config[$i]['type'];
			$supplier_id = $Rows_config[$i]['supplier'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( ($specialty_id == $Rows_cant[$x]['specialty']) && ($type_id == $Rows_cant[$x]['type']) && ($supplier_id == $Rows_cant[$x]['supplier']) ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function HorasTipo($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryDatos = "SELECT c.type, sum(m.duration_time) as horas
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY c.type
		ORDER BY c.type";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function HorasProveedor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryDatos = "SELECT p.supplier, sum(m.duration_time) as horas
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY p.supplier
		ORDER BY p.supplier";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function HorasTipoProveedor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryDatos = "SELECT p.supplier, c.type, sum(m.duration_time) as horas
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY p.supplier, c.type
		ORDER BY p.supplier, c.type";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CursosHabilidad($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT t.specialty
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT MONTH(s.start_date) as mes, t.specialty, count(distinct s.schedule_id) as cant
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY MONTH(s.start_date), t.specialty
		ORDER BY t.specialty, MONTH(s.start_date)";
		//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$specialty_id = $Rows_config[$i]['specialty'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $specialty_id == $Rows_cant[$x]['specialty'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CursosPorHorasCurso($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT m.duration_time
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id
		ORDER BY m.duration_time";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT m.duration_time, MONTH(s.start_date) as mes, t.specialty, count(distinct s.schedule_id) as cant
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY m.duration_time, MONTH(s.start_date), t.specialty
		ORDER BY m.duration_time, t.specialty, MONTH(s.start_date)";
		//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$duration_time = $Rows_config[$i]['duration_time'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $duration_time == $Rows_cant[$x]['duration_time'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CursoHabilidadTipo($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT t.specialty, c.type
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT MONTH(s.start_date) as mes, c.type, t.specialty, count(distinct s.schedule_id) as cant
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY MONTH(s.start_date), c.type, t.specialty
		ORDER BY c.type, t.specialty, MONTH(s.start_date)";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$specialty_id = $Rows_config[$i]['specialty'];
			$type_id = $Rows_config[$i]['type'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( ($specialty_id == $Rows_cant[$x]['specialty']) && ($type_id == $Rows_cant[$x]['type']) ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CursoHabilidadTipoProveedor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT t.specialty, c.type, p.supplier
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT MONTH(s.start_date) as mes, p.supplier, c.type, t.specialty, count(distinct s.schedule_id) as cant
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY MONTH(s.start_date), p.supplier, c.type, t.specialty
		ORDER BY p.supplier, c.type, t.specialty, MONTH(s.start_date)";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$specialty_id = $Rows_config[$i]['specialty'];
			$type_id = $Rows_config[$i]['type'];
			$supplier_id = $Rows_config[$i]['supplier'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( ($specialty_id == $Rows_cant[$x]['specialty']) && ($type_id == $Rows_cant[$x]['type']) && ($supplier_id == $Rows_cant[$x]['supplier']) ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CursosTipo($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryDatos = "SELECT c.type, count(distinct s.schedule_id) as cant
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY c.type
		ORDER BY c.type";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CursosProveedor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryDatos = "SELECT p.supplier, count(distinct s.schedule_id) as cant
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY p.supplier
		ORDER BY p.supplier";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CursosTipoProveedor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryDatos = "SELECT p.supplier, c.type, count(distinct s.schedule_id) as cant
		FROM ludus_schedule s, ludus_courses c, ludus_supplier p, ludus_modules m, ludus_specialties t 
		WHERE s.status_id = 1 AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final' AND s.start_date < NOW() AND s.course_id = c.course_id 
		AND c.supplier_id = p.supplier_id AND c.course_id = m.module_id AND c.specialty_id = t.specialty_id 
		GROUP BY p.supplier, c.type
		ORDER BY p.supplier, c.type";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CantUsuarios($ano_inicial){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$queryDatos = "SELECT count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				UNION
				SELECT count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantUsuarios_Genero($ano_inicial){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT gender
		FROM ludus_users
		WHERE status_id = 1
		ORDER BY gender DESC";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "
				SELECT l.gender, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				GROUP BY l.gender
				UNION
				SELECT l.gender, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				GROUP BY l.gender";
				//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$gender = $Rows_config[$i]['gender'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $gender == $Rows_cant[$x]['gender'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantUsuarios_Zona($ano_inicial){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT z.zone
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				ORDER BY z.zone";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "
				SELECT z.zone, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone
				UNION
				SELECT z.zone, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l, ludus_headquarters h, ludus_areas a, ludus_zone z
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31' AND l.headquarter_id = h.headquarter_id AND 
				h.area_id = a.area_id AND a.zone_id = z.zone_id
				GROUP BY z.zone";
				//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$zone = $Rows_config[$i]['zone'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $zone == $Rows_cant[$x]['zone'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantUsuarios_Jerarquia($ano_inicial){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT cc.category
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
		ORDER BY cc.category";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "
				SELECT cc.category, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category
				UNION
				SELECT cc.category, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c, ludus_charges_categories cc
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.category_id = cc.category_id
				GROUP BY cc.category";
				//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$category = $Rows_config[$i]['category'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $category == $Rows_cant[$x]['category'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantUsuarios_Trayectoria($ano_inicial){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT c.charge
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				ORDER BY c.charge";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "
				SELECT c.charge, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge
				UNION
				SELECT c.charge, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l, ludus_charges_users cu, ludus_charges c
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND l.user_id = cu.user_id AND cu.charge_id = c.charge_id 
				GROUP BY c.charge";
				//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$charge = $Rows_config[$i]['charge'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $charge == $Rows_cant[$x]['charge'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CantUsuarios_RangosEdad($ano_inicial){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "
			SELECT 1 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 0 AND 25 UNION 
			SELECT 2 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 26 AND 35 UNION 
			SELECT 3 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 36 AND 45 UNION 
			SELECT 4 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 46 AND 55 UNION 
			SELECT 5 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) BETWEEN 56 AND 65 UNION 
			SELECT 6 as rango, count(distinct user_id) FROM ludus_users u WHERE u.status_id = 1 AND TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) > 65";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos_1 = "SELECT 1 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25
				UNION
				SELECT 1 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 0 AND 25";
		$Rows_cant_1 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_1);

		$queryDatos_2 = "SELECT 2 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35
				UNION
				SELECT 2 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 26 AND 35";
		$Rows_cant_2 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_2);

		$queryDatos_3 = "SELECT 3 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45
				UNION
				SELECT 3 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 36 AND 45";
		$Rows_cant_3 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_3);

		$queryDatos_4 = "SELECT 4 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55
				UNION
				SELECT 4 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 46 AND 55";
		$Rows_cant_4 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_4);

		$queryDatos_5 = "SELECT 5 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65
				UNION
				SELECT 5 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) BETWEEN 56 AND 65";
		$Rows_cant_5 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_5);

		$queryDatos_6 = "SELECT 6 as rango, count(distinct l.user_id) as cant, '1' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-02-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '2' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-03-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '3' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-04-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '4' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-05-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '5' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-06-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '6' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-07-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '7' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-08-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '8' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-09-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '9' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-10-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '10' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-11-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '11' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation < '$ano_inicial-12-01'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65
				UNION
				SELECT 6 as rango, count(distinct l.user_id) as cant, '12' as mes
				FROM ludus_users l
				WHERE l.status_id = 1 AND l.date_creation <= '$ano_inicial-12-31'
				AND TIMESTAMPDIFF(YEAR, l.date_birthday, CURDATE()) > 65";
		$Rows_cant_6 = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos_6);

		for($i=0;$i<count($Rows_config);$i++) {
			$rango = $Rows_config[$i]['rango'];

			for($x=0;$x<count($Rows_cant_1);$x++) {
				if( $rango == $Rows_cant_1[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_1[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_2);$x++) {
				if( $rango == $Rows_cant_2[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_2[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_3);$x++) {
				if( $rango == $Rows_cant_3[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_3[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_4);$x++) {
				if( $rango == $Rows_cant_4[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_4[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_5);$x++) {
				if( $rango == $Rows_cant_5[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_5[$x];
				}
			}

			for($x=0;$x<count($Rows_cant_6);$x++) {
				if( $rango == $Rows_cant_6[$x]['rango'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant_6[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CantPrestamos_Sala($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT distinct d.dealer
				FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d
				WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id
				AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
				ORDER BY d.dealer";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);

		$queryDatos = "SELECT MONTH(s.start_date) as mes, d.dealer, count(distinct s.schedule_id) as cant
					FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d
					WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id
					AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
					GROUP BY MONTH(s.start_date), d.dealer
					ORDER BY d.dealer, MONTH(s.start_date)";
					//echo($queryDatos);
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);

		for($i=0;$i<count($Rows_config);$i++) {
			$dealer = $Rows_config[$i]['dealer'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $dealer == $Rows_cant[$x]['dealer'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function PromCalificacion_Proveedor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT p.supplier
						FROM ludus_schedule s, ludus_livings l, ludus_courses c, ludus_supplier p, ludus_users u, 
							(SELECT i.schedule_id, MAX(m.date_creation) as fecCalif 
								FROM ludus_modules_results_usr m, ludus_invitation i, ludus_users u, ludus_schedule s 
								WHERE m.invitation_id = i.invitation_id AND m.creator = u.user_id AND i.schedule_id = s.schedule_id AND
								s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
								GROUP BY i.schedule_id) m 
						WHERE s.living_id = l.living_id AND s.course_id = c.course_id AND c.supplier_id = p.supplier_id AND 
						s.teacher_id = u.user_id AND s.schedule_id = m.schedule_id 
						AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
						ORDER BY p.supplier";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		$queryDatos = "SELECT MONTH(s.start_date) as mes, p.supplier, AVG(DATEDIFF(m.fecCalif,s.start_date)) as cant
						FROM ludus_schedule s, ludus_livings l, ludus_courses c, ludus_supplier p, ludus_users u, 
							(SELECT i.schedule_id, MAX(m.date_creation) as fecCalif 
								FROM ludus_modules_results_usr m, ludus_invitation i, ludus_users u, ludus_schedule s 
								WHERE m.invitation_id = i.invitation_id AND m.creator = u.user_id AND i.schedule_id = s.schedule_id AND
								s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
								GROUP BY i.schedule_id) m 
						WHERE s.living_id = l.living_id AND s.course_id = c.course_id AND c.supplier_id = p.supplier_id AND 
						s.teacher_id = u.user_id AND s.schedule_id = m.schedule_id 
						AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
						GROUP BY MONTH(s.start_date), p.supplier
						ORDER BY p.supplier";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);
		for($i=0;$i<count($Rows_config);$i++) {
			$supplier = $Rows_config[$i]['supplier'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( $supplier == $Rows_cant[$x]['supplier'] ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function PromCalificacion_ProveedorProfesor($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT p.supplier, u.first_name, u.last_name
						FROM ludus_schedule s, ludus_livings l, ludus_courses c, ludus_supplier p, ludus_users u, 
							(SELECT i.schedule_id, MAX(m.date_creation) as fecCalif 
								FROM ludus_modules_results_usr m, ludus_invitation i, ludus_users u, ludus_schedule s 
								WHERE m.invitation_id = i.invitation_id AND m.creator = u.user_id AND i.schedule_id = s.schedule_id AND
								s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
								GROUP BY i.schedule_id) m 
						WHERE s.living_id = l.living_id AND s.course_id = c.course_id AND c.supplier_id = p.supplier_id AND 
						s.teacher_id = u.user_id AND s.schedule_id = m.schedule_id 
						AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
						ORDER BY p.supplier, u.first_name, u.last_name";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		$queryDatos = "SELECT MONTH(s.start_date) as mes, p.supplier, u.first_name, u.last_name, AVG(DATEDIFF(m.fecCalif,s.start_date)) as cant
						FROM ludus_schedule s, ludus_livings l, ludus_courses c, ludus_supplier p, ludus_users u, 
							(SELECT i.schedule_id, MAX(m.date_creation) as fecCalif 
								FROM ludus_modules_results_usr m, ludus_invitation i, ludus_users u, ludus_schedule s 
								WHERE m.invitation_id = i.invitation_id AND m.creator = u.user_id AND i.schedule_id = s.schedule_id AND
								s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
								GROUP BY i.schedule_id) m 
						WHERE s.living_id = l.living_id AND s.course_id = c.course_id AND c.supplier_id = p.supplier_id AND 
						s.teacher_id = u.user_id AND s.schedule_id = m.schedule_id 
						AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
						GROUP BY MONTH(s.start_date), p.supplier, u.first_name, u.last_name
						ORDER BY p.supplier, u.first_name, u.last_name";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);
		for($i=0;$i<count($Rows_config);$i++) {
			$supplier = $Rows_config[$i]['supplier'];
			$first_name = $Rows_config[$i]['first_name'];
			$last_name = $Rows_config[$i]['last_name'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( ($supplier == $Rows_cant[$x]['supplier']) && ($first_name == $Rows_cant[$x]['first_name']) && ($last_name == $Rows_cant[$x]['last_name']) ){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function PromCalificacion_ProveedorTCurso($fec_inicial,$fec_final){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$DataBase_Acciones = new Database();
		$query_hab = "SELECT DISTINCT p.supplier, c.type
						FROM ludus_schedule s, ludus_livings l, ludus_courses c, ludus_supplier p, ludus_users u, 
							(SELECT i.schedule_id, MAX(m.date_creation) as fecCalif 
								FROM ludus_modules_results_usr m, ludus_invitation i, ludus_users u, ludus_schedule s 
								WHERE m.invitation_id = i.invitation_id AND m.creator = u.user_id AND i.schedule_id = s.schedule_id AND
								s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
								GROUP BY i.schedule_id) m 
						WHERE s.living_id = l.living_id AND s.course_id = c.course_id AND c.supplier_id = p.supplier_id AND 
						s.teacher_id = u.user_id AND s.schedule_id = m.schedule_id 
						AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
						ORDER BY p.supplier, c.type";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_hab);
		$queryDatos = "SELECT MONTH(s.start_date) as mes, p.supplier, c.type, AVG(DATEDIFF(m.fecCalif,s.start_date)) as cant
						FROM ludus_schedule s, ludus_livings l, ludus_courses c, ludus_supplier p, ludus_users u, 
							(SELECT i.schedule_id, MAX(m.date_creation) as fecCalif 
								FROM ludus_modules_results_usr m, ludus_invitation i, ludus_users u, ludus_schedule s 
								WHERE m.invitation_id = i.invitation_id AND m.creator = u.user_id AND i.schedule_id = s.schedule_id AND
								s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
								GROUP BY i.schedule_id) m 
						WHERE s.living_id = l.living_id AND s.course_id = c.course_id AND c.supplier_id = p.supplier_id AND 
						s.teacher_id = u.user_id AND s.schedule_id = m.schedule_id 
						AND s.start_date BETWEEN '$fec_inicial' AND '$fec_final'
						GROUP BY MONTH(s.start_date), p.supplier, c.type
						ORDER BY p.supplier, c.type";
		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($queryDatos);
		for($i=0;$i<count($Rows_config);$i++) {
			$supplier = $Rows_config[$i]['supplier'];
			$type = $Rows_config[$i]['type'];
			for($x=0;$x<count($Rows_cant);$x++) {
				if( ($supplier == $Rows_cant[$x]['supplier']) && ($type == $Rows_cant[$x]['type'])){
					$Rows_config[$i]['Datos'][] = $Rows_cant[$x];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}

}
