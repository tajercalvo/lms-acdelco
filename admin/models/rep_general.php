<?php
class RepGeneralesClass
{
	function consultaUsuarios($start_date,$end_date, $cargos)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];

		if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 4) {
			$query_sql = "SELECT u.user_id, u.identification, u.last_name, u.first_name, u.email, u.phone, h.headquarter, d.dealer, a.area, z.zone
						FROM ludus_users u, ludus_charges_users cu, ludus_charges c, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
						WHERE u.status_id = 1 AND u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.status_id = 1 AND c.charge_id NOT IN (126,123,125) AND
						u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id
						AND cu.charge_id IN ($cargos)
						ORDER BY d.dealer, h.headquarter, u.last_name, u.first_name";
		} else {
			$query_sql = "SELECT u.user_id, u.identification, u.last_name, u.first_name, u.email, u.phone, h.headquarter, d.dealer, a.area, z.zone
						FROM ludus_users u, ludus_charges_users cu, ludus_charges c, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
						WHERE u.status_id = 1 AND u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND c.status_id = 1 AND c.charge_id NOT IN (126,123,125) AND
						u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND d.dealer_id = $dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id
						AND cu.charge_id IN ($cargos)
						ORDER BY d.dealer, h.headquarter, u.last_name, u.first_name";
		}


		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$query_Cargo = "SELECT c.charge, c.charge_id, u.user_id
						FROM ludus_charges_users u, ludus_charges c
						WHERE u.charge_id = c.charge_id AND c.status_id = 1 AND c.charge !='Default'";
		$Rows_Cargos = $DataBase_Acciones->SQL_SelectMultipleRows($query_Cargo);
		$query_Notas = "SELECT s.user_id, m.course_id, avg(s.maxscore) as avgscore
						FROM ludus_modules m, (SELECT user_id, module_id, max(score) AS maxscore
						FROM ludus_modules_results_usr
						WHERE date_creation BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
						GROUP BY user_id, module_id) s
						WHERE m.module_id = s.module_id
						GROUP BY s.user_id, m.course_id";
		$Rows_Notas = $DataBase_Acciones->SQL_SelectMultipleRows($query_Notas);
		for ($i = 0; $i < count($Rows_config); $i++) {
			$user_id = $Rows_config[$i]['user_id'];
			//Cargos
			for ($y = 0; $y < count($Rows_Cargos); $y++) {
				if ($user_id == $Rows_Cargos[$y]['user_id']) {
					$Rows_config[$i]['Charges'][] = $Rows_Cargos[$y];
				}
			}
			//Cargos
			//Notas
			for ($y = 0; $y < count($Rows_Notas); $y++) {
				if ($user_id == $Rows_Notas[$y]['user_id']) {
					$Rows_config[$i]['Notas'][] = $Rows_Notas[$y];
				}
			}
			//Notas
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaTrayectorias($cargos)
	{
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$query_trayectorias = "SELECT c.charge, c.charge_id FROM ludus_charges c WHERE c.status_id = 1 AND c.charge !='Default' AND c.charge_id NOT IN (126,123,125) AND c.charge_id IN ($cargos) ORDER BY c.charge";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_trayectorias);
		$query_cursos = "SELECT c.charge, c.charge_id, co.course, co.newcode, l.level, co.type, co.course_id
					FROM ludus_charges c, ludus_charges_courses cc, ludus_courses co, ludus_level l
					WHERE c.status_id = 1
						AND c.charge_id NOT IN (126,123,125)
						AND c.charge_id IN ($cargos)
						AND c.charge_id = cc.charge_id
						AND cc.course_id = co.course_id AND co.status_id = 1
						AND cc.level_id = l.level_id
						AND cc.status_id = 1
					ORDER BY c.charge ASC, l.level_id DESC";
		$Rows_cursos = $DataBase_Acciones->SQL_SelectMultipleRows($query_cursos);
		for ($i = 0; $i < count($Rows_config); $i++) {
			$charge_id = $Rows_config[$i]['charge_id'];
			for ($y = 0; $y < count($Rows_cursos); $y++) {
				if ($charge_id == $Rows_cursos[$y]['charge_id']) {
					$Rows_config[$i]['Cursos'][] = $Rows_cursos[$y];
				}
			}
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
