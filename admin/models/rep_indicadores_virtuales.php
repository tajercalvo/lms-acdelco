<?php
Class RepIndicadoresVirtuales {
	/*
	Andres Vega
	02/12/2016
	Funcion que consolida y crea un solo Array con los datos de las funciones consultaUsuariosYTrayectorias y consultaNotasVirtuales
	- muestra la cantidad de cursos virtuales hechos por cada concesionario
	$start_date = fecha inicial para la validacion de los cursos virtuales
	$end_date 	= Fecha final de la validacion de notas de los cursos virtuales
	*/
	public function consultaCursosRealizados($start_date,$end_date){
		$usuariosYTrayectorias = $this->consultaUsuariosYTrayectorias();
		$notasCursosVirtuales = $this->consultaNotasVirtuales( $start_date, $end_date );
		//recorre los concesionarios iniciales
		for ($i=0; $i < count( $usuariosYTrayectorias ) ; $i++) {
			$contador = 0;
			foreach ($notasCursosVirtuales as $key => $valueCV) {
				if($valueCV['dealer_id'] == $usuariosYTrayectorias[ $i ]['dealer_id']){
					$contador++;
				}
			}//fin foreach
			$usuariosYTrayectorias[ $i ]['cursosVirtuales'] = $contador;
		}//fin for
		return $usuariosYTrayectorias;
	}//fin funcion consultaCursosRealizados
	/*
	Andres Vega
	02/12/2016
	Funcion que consulta la cantidad de usuarios activos y cuantos tienen al menos una trayectoria, por concesionario
	*/
	public function consultaUsuariosYTrayectorias(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT d.dealer_id, d.dealer, COUNT(u.user_id) AS usuarios,
						( SELECT count( distinct lcu.user_id )
						FROM ludus_users lu, ludus_charges_users lcu, ludus_headquarters lh, ludus_dealers ld
						WHERE lu.user_id = lcu.user_id
					    AND lu.status_id = 1
						AND lcu.status_id = 1
						AND lu.headquarter_id = lh.headquarter_id
						AND lh.dealer_id = ld.dealer_id
						AND ld.dealer_id = d.dealer_id
                        AND lcu.charge_id IN (49,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137) ) AS trayectorias
					FROM ludus_dealers d, ludus_headquarters h, ludus_users u
					WHERE d.dealer_id = h.dealer_id
					AND h.headquarter_id = u.headquarter_id
					AND u.status_id = 1
					GROUP BY d.dealer_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//end function consultaUsuariosYTrayectorias
	/*
	Andres Vega
	02/12/2016
	funcion que consulta los datos de las notas de cursos virtuales entre dos fechas
	*/
	public function consultaNotasVirtuales($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql="SELECT d.dealer_id, o.user_id, p.puntaje, o.date_creation
					FROM ludus_modules_results_usr o,ludus_users u, ludus_dealers d, ludus_headquarters h,
						( SELECT r.user_id, m.module_id, MAX(r.score) as puntaje
							FROM ludus_modules_results_usr r, ludus_modules m, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu
							WHERE r.module_id = m.module_id
							AND m.type = 'WBT'
							AND r.date_creation BETWEEN '$start_date' AND '$end_date'
							AND r.user_id = u.user_id
							AND u.status_id = 1
							AND u.headquarter_id = h.headquarter_id
							AND h.dealer_id = d.dealer_id
                         	AND u.user_id = lcu.user_id
                        	AND lcu.charge_id IN (49,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137)
							AND lcu.status_id = 1
                         GROUP BY r.user_id, r.module_id ) p
					WHERE o.user_id = p.user_id
					AND o.module_id = p.module_id
					AND o.score = p.puntaje
				    AND o.user_id = u.user_id
				    AND u.headquarter_id = h.headquarter_id
				    AND h.dealer_id = d.dealer_id
					AND p.puntaje >= 80
					GROUP BY o.user_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin function consultaNotasVirtuales
	/*
	Andres Vega
	05/12/2016
	Funcion que consolida y crea un solo Array con los datos de las funciones consultaUsuariosYTrayectorias y consultaNotasVirtuales
	- muestra la cantidad de cursos virtuales hechos por cada concesionario
	$start_date = fecha inicial para la validacion de los cursos virtuales
	$end_date 	= Fecha final de la validacion de notas de los cursos virtuales
	*/
	public function consultaCursosRealizadosCSV($start_date,$end_date){
		$usuariosYTrayectorias = $this->consultaUsuariosYTrayectoriasCSV();
		$notasCursosVirtuales = $this->consultaNotasVirtualesCSV( $start_date, $end_date );
		//recorre los concesionarios iniciales
		for ($i=0; $i < count( $usuariosYTrayectorias ) ; $i++) {
			$contador = 0;
			foreach ($notasCursosVirtuales as $key => $valueCV) {
				if($valueCV['user_id'] == $usuariosYTrayectorias[ $i ]['user_id']){
					$contador++;
				}
			}//fin foreach
			$usuariosYTrayectorias[ $i ]['cursosVirtuales'] = $contador;
		}//fin for
		return $usuariosYTrayectorias;
	}//fin funcion consultaCursosRealizados
	/*
	Andres Vega
	05/12/2016
	Funcion que consulta la cantidad de usuarios activos y cuantos tienen al menos una trayectoria, por concesionario
	*/
	public function consultaUsuariosYTrayectoriasCSV(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT d.dealer_id, d.dealer, h.headquarter, u.user_id, u.first_name, u.last_name,
								(SELECT COUNT( distinct lcu.user_id) FROM ludus_charges_users lcu
	                             WHERE lcu.user_id = u.user_id
								 AND u.status_id = 1
								 AND lcu.status_id = 1
	                             AND lcu.charge_id IN (49,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137) ) AS trayectorias
							FROM ludus_dealers d, ludus_headquarters h, ludus_users u
							WHERE d.dealer_id = h.dealer_id
							AND h.headquarter_id = u.headquarter_id
                            AND u.status_id = 1
							ORDER BY u.user_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//end function consultaUsuariosYTrayectoriasCSV
	/*
	Andres Vega
	05/12/2016
	funcion que consulta los datos de las notas de cursos virtuales entre dos fechas
	*/
	public function consultaNotasVirtualesCSV($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql="SELECT o.user_id, o.module_id, p.puntaje, o.date_creation
					FROM ludus_modules_results_usr o,ludus_users u,
						( SELECT r.user_id, m.module_id, MAX(r.score) as puntaje
							FROM ludus_modules_results_usr r, ludus_modules m, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users lcu
							WHERE u.status_id = 1
                         	AND r.module_id = m.module_id
							AND m.type = 'WBT'
							AND r.date_creation BETWEEN '$start_date' AND '$end_date'
							AND r.user_id = u.user_id
							AND u.headquarter_id = h.headquarter_id
							AND h.dealer_id = d.dealer_id
							AND u.user_id = lcu.user_id
                        	AND lcu.charge_id IN (49,99,100,68,89,93,94,98,105,128,134,23,31,32,34,35,36,37,137)
							AND lcu.status_id = 1
						GROUP BY r.user_id, r.module_id ) p
					WHERE o.user_id = p.user_id
					AND o.module_id = p.module_id
					AND o.score = p.puntaje
				    AND o.user_id = u.user_id
                    AND p.puntaje >= 80
					GROUP BY o.user_id, o.module_id";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin function consultaNotasVirtualesCSV

}//fin clase
