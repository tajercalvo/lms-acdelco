<?php
Class Costo {
	function consultaDatosAll($new_dealer_id,$start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();

		if ($new_dealer_id == 0) {
			//Si dealer_id no es enviado se trae de la variable Session
			if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
				$dealer_id = 0;
			}else{
				$dealer_id = $_SESSION['dealer_id'];
			}
		} else {
			$dealer_id = $new_dealer_id;
		}

		if ($dealer_id == "0") {
			//consulta los concesionarios mas campos de cobro, descuento y alimentacion
			$query_sql = "SELECT DISTINCT d.dealer_id, d.dealer, d.category, 0 as cobro, 0 as descuento, 0 as alimentacion, 0 as asistentes, 0 as excusas, 0 as ajustes
				FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s, ludus_courses c,
				(SELECT schedule_id, sum(inscriptions) as inscriptions FROM ludus_dealer_schedule GROUP BY schedule_id) as i
				WHERE s.start_date BETWEEN '$start_date' AND '$end_date' AND s.course_id = c.course_id AND c.type IN ('IBT','VCT','OJT')
				AND s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id AND s.status_id = 1 AND d.status_id = 1 
				AND d.category = 'CHEVROLET' AND s.schedule_id = i.schedule_id ORDER BY d.dealer";

				//consulta las sesiones realizadas en cierto tiempo detalladamente con concesionario, curso, duracion y demas
			$query_detail = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, l.living_id, l.living, h.headquarter, d.dealer, d.dealer_id, i.inscriptions, c.course, c.newcode, c.type, c.course_id, c.specialty_id, h.rate_course, m.duration_time, 0 as descuento, 0 as alimentacion, 0 as excusas, ds.dealer_id as origin_dealer, 0 as dealer_inscriptions, ds.max_size as max, ds.min_size as min, ds.inscriptions as inscritos
				FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, ludus_dealer_schedule ds, ludus_modules m, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM ludus_dealer_schedule GROUP BY schedule_id) as i 
				WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = ds.schedule_id AND s.schedule_id = i.schedule_id AND s.course_id = c.course_id AND c.course_id = m.course_id AND c.type IN ('IBT','VCT','OJT') AND s.start_date BETWEEN '$start_date' AND '$end_date' AND s.status_id = 1 AND (ds.inscriptions NOT IN (0) OR ds.max_size NOT IN (0))
                ORDER BY s.schedule_id";
             /**/ 
			$query_inscritos = "SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, s.schedule_id, d.dealer, d.dealer_id, COUNT(d.dealer_id) AS inscritos_old, 0 AS inscritos FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) GROUP BY s.schedule_id, d.dealer_id ORDER BY s.schedule_id";

			$query_inscritos_detail = " SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, u1.user_id, h.headquarter, s.schedule_id, d.dealer, d.dealer_id FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) ORDER BY s.schedule_id, d.dealer_id ";

				//consulta los logisticas de alimentacion junto la sala donde se realizo
			$query_feeding = "SELECT f.*, l.living 
				FROM ludus_feeding f, ludus_schedule s, ludus_livings l, ludus_headquarters h 
				WHERE s.start_date BETWEEN '$start_date' AND '$end_date' AND s.schedule_id = f.schedule_id AND f.status_id = 1 AND s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND f.dealer_id = h.dealer_id";

			$query_Excusas2 = "SELECT u.user_id, u.first_name, u.last_name, u.identification, h.headquarter, e.file, e.schedule_id, d.dealer_id, d.dealer, a.area, z.zone, ds.min_size, ds.max_size, ds.inscriptions, ds.total_size, e.aplica_para
				FROM ludus_excuses e, ludus_users u, ludus_invitation i, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, ludus_dealer_schedule ds
				WHERE e.user_id = u.user_id AND u.user_id = i.user_id AND e.schedule_id = i.schedule_id AND i.headquarter_id = h.headquarter_id  AND d.dealer_id = ds.dealer_id AND e.schedule_id = ds.schedule_id
				AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND e.status_id = 1 AND e.aplica_para IN ('','COBRO') ";

			$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.file, ed.description, ed.type, ed.aplica_para 
				FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds 
				WHERE ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.status_id = 1";

			$query_cost_extra = "SELECT * FROM ludus_extra_cost WHERE type = 1 AND status_id = 1";

		} else {
			$query_sql = "SELECT DISTINCT d.dealer_id, d.dealer, d.category, 0 as cobro, 0 as descuento, 0 as alimentacion, 0 as asistentes, 0 as excusas, 0 as ajustes
				FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s, ludus_courses c,
				(SELECT schedule_id, sum(inscriptions) as inscriptions FROM ludus_dealer_schedule GROUP BY schedule_id) as i
				WHERE s.start_date BETWEEN '$start_date' AND '$end_date' AND s.course_id = c.course_id AND c.type IN ('IBT','VCT','OJT')
				AND s.schedule_id = ds.schedule_id AND ds.dealer_id = d.dealer_id AND s.status_id = 1 AND d.status_id = 1 AND d.dealer_id = $dealer_id
				AND d.category = 'CHEVROLET' AND s.schedule_id = i.schedule_id  ORDER BY d.dealer";

			$query_detail = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, l.living_id, l.living, h.headquarter, d.dealer, d.dealer_id, i.inscriptions, c.course, c.newcode, c.type, c.course_id, c.specialty_id, h.rate_course, m.duration_time, 0 as descuento, 0 as alimentacion, 0 as excusas, ds.dealer_id as origin_dealer, 0 as dealer_inscriptions, ds.max_size as max, ds.min_size as min, ds.inscriptions as inscritos
				FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, ludus_dealer_schedule ds, ludus_modules m, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM ludus_dealer_schedule GROUP BY schedule_id) as i 
				WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = ds.schedule_id AND s.schedule_id = i.schedule_id AND s.course_id = c.course_id AND c.course_id = m.course_id AND c.type IN ('IBT','VCT','OJT') AND s.start_date BETWEEN '$start_date' AND '$end_date' AND ds.dealer_id = $dealer_id AND s.status_id = 1 AND (ds.inscriptions NOT IN (0) OR ds.max_size NOT IN (0))
                ORDER BY s.schedule_id";
			/**/
			$query_inscritos = "SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, s.schedule_id, d.dealer, d.dealer_id, COUNT(d.dealer_id) AS inscritos_old, 0 AS inscritos FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) AND d.dealer_id = $dealer_id GROUP BY s.schedule_id, d.dealer_id ORDER BY s.schedule_id";

			$query_inscritos_detail = " SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, u1.user_id, h.headquarter, s.schedule_id, d.dealer, d.dealer_id FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) AND d.dealer_id = $dealer_id ORDER BY s.schedule_id, d.dealer_id ";

			$query_feeding = "SELECT f.*, l.living
				FROM ludus_feeding f, ludus_schedule s, ludus_livings l, ludus_headquarters h
				WHERE s.start_date BETWEEN '$start_date' AND '$end_date' AND s.schedule_id = f.schedule_id AND f.status_id = 1 AND
				s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND f.dealer_id = h.dealer_id AND f.dealer_id = $dealer_id";

			$query_Excusas2 = "SELECT u.user_id, u.first_name, u.last_name, u.identification, h.headquarter, e.file, e.schedule_id, d.dealer_id, d.dealer, a.area, z.zone, ds.min_size, ds.max_size, ds.inscriptions, ds.total_size, e.aplica_para
				FROM ludus_excuses e, ludus_users u, ludus_invitation i, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, ludus_dealer_schedule ds
				WHERE e.user_id = u.user_id AND u.user_id = i.user_id AND e.schedule_id = i.schedule_id AND i.headquarter_id = h.headquarter_id  AND d.dealer_id = ds.dealer_id AND e.schedule_id = ds.schedule_id
				AND h.dealer_id = '$dealer_id' AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND e.status_id = 1 AND e.aplica_para IN ('','COBRO') ";

			
			$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.file, ed.description, ed.type, ed.aplica_para 
				FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds 
				WHERE ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.dealer_id = '$dealer_id' AND ed.status_id = 1 ";
				
			$query_cost_extra = "SELECT * FROM ludus_extra_cost WHERE type = 1 AND status_id = 1 AND dealer_id = '$dealer_id' ";
		}
		//echo($query_feeding);
		$DataBase_Acciones = new Database();
		$Rows_config 	= $DataBase_Acciones->SQL_SelectMultipleRows($query_sql); //Concesionarios
		$Rows_Detail 	= $DataBase_Acciones->SQL_SelectMultipleRows($query_detail); //Sesiones en cada concesionario
		$Rows_inscritos = $DataBase_Acciones->SQL_SelectMultipleRows($query_inscritos); //Inscritos en cada concesionario y sesion
		$Rows_inscritos_detail = $DataBase_Acciones->SQL_SelectMultipleRows($query_inscritos_detail); //Detalle de Inscritos
		$Rows_Food 		= $DataBase_Acciones->SQL_SelectMultipleRows($query_feeding);
		$Rows_Excusas2 = $DataBase_Acciones->SQL_SelectMultipleRows($query_Excusas2); //Excusas individuales por cada sesion
		$Rows_Excusas_Dealer = $DataBase_Acciones->SQL_SelectMultipleRows($query_Excusa_Dealer); //Excusas Soporte cupos por cada sesion
		$Rows_Cost_Special = $DataBase_Acciones->SQL_SelectMultipleRows($query_cost_extra); //Cobos extras

		//AJUSTE DE CONCESIONARIO A USUARIO DEPENDIENDO DEL HISTORY
		/*for($l=0;$l<count($Rows_inscritos_detail);$l++) { 
			$historico = $this->ccsHistoricoUsuario( $Rows_inscritos_detail[$l]['start_date'], $Rows_inscritos_detail[$l]['user_id'] );
			$Rows_inscritos_detail[$l]['headquarter'] = $historico['headquarter'];
			$Rows_inscritos_detail[$l]['dealer_id'] = $historico['dealer_id'];
			$Rows_inscritos_detail[$l]['dealer'] = $historico['dealer'];
		}*/

		//CONTEO DE USUARIOS POR CADA SESION Y CONCESIONARIO DESPUES DEL HISTORY
		for($p=0;$p<count($Rows_inscritos);$p++) { 
			for($l=0;$l<count($Rows_inscritos_detail);$l++) { 
				if( $Rows_inscritos[$p]['schedule_id'] == $Rows_inscritos_detail[$l]['schedule_id'] ){
					if ( $Rows_inscritos[$p]['dealer_id'] == $Rows_inscritos_detail[$l]['dealer_id'] ) {
						$Rows_inscritos[$p]['inscritos'] += 1;
					}
				}
			}
		}
		// INCLUSION DE LOS CUPOS EN LAS SESIONES Y CONCESIONARIOS
		for($p=0;$p<count($Rows_Detail);$p++) { 
			for($p2=0;$p2<count($Rows_inscritos);$p2++) {
				if ( $Rows_inscritos[$p2]['inscritos'] > 0 ) { //Se ignoran las sesiones sin cupo cambiadas en el history
					if( $Rows_Detail[$p]['schedule_id'] == $Rows_inscritos[$p2]['schedule_id'] ){
						if ( $Rows_Detail[$p]['origin_dealer'] == $Rows_inscritos[$p2]['dealer_id'] ) {
							$Rows_Detail[$p]['dealer_inscriptions'] = $Rows_inscritos[$p2]['inscritos'];
							break;
						}
					}
				}
			}
		}
		/*print_r($Rows_Detail);
		die();*/

		// INCLUSION DE LOS CUPOS NO INSCRITOS Y/O NO INVITADOS
		for($p=0;$p<count($Rows_Detail);$p++) { 

			$no_inscritos = ($Rows_Detail[$p]['max'] - $Rows_Detail[$p]['inscritos']);
			 if( $no_inscritos > 0 ){
			    $Rows_Detail[$p]['dealer_inscriptions'] += $no_inscritos;
			}

			for($m=0;$m<count($Rows_Excusas_Dealer);$m++) {

				if( ($Rows_Detail[$p]['schedule_id'] == $Rows_Excusas_Dealer[$m]['schedule_id']) && ($Rows_Detail[$p]['origin_dealer'] == $Rows_Excusas_Dealer[$m]['dealer_id']) ){ //SE EVALUA SI EL CUPO TIENE EXCUSA SOPORTE CUPO

		            if( $no_inscritos > 0 ){
		                $Rows_Detail[$p]['dealer_inscriptions'] -= $Rows_Excusas_Dealer[$m]['num_invitation'];
		            }

	        	} 
	        }
        }

		/*Establece los detalles*/
		for($i=0;$i<count($Rows_config);$i++) {

			$dealer_id 	= $Rows_config[$i]['dealer_id'];
			//Las sesiones que se dictaron
			for($y=0;$y<count($Rows_Detail);$y++) {

				$schedule_id = $Rows_Detail[$y]['schedule_id'];
				$valor_acuerdo = 0;
				$excusas = 0;
				$excusas_soporte_cupo = 0;
				//Numero de excusas indivuduales por cada sesion en cada concesionario
				for($m=0;$m<count($Rows_Excusas2);$m++) {
					if($schedule_id == $Rows_Excusas2[$m]['schedule_id']){
						if ($Rows_Detail[$y]['origin_dealer'] == $Rows_Excusas2[$m]['dealer_id']) {
							$excusas++;
						}
					}
				}
				//Numero de excusas soporte cupos por cada sesion en cada concesionario
				for($m=0;$m<count($Rows_Excusas_Dealer);$m++) {
					if($schedule_id == $Rows_Excusas_Dealer[$m]['schedule_id']){
						if ($Rows_Detail[$y]['origin_dealer'] == $Rows_Excusas_Dealer[$m]['dealer_id']) {
							$excusas_soporte_cupo = $Rows_Excusas_Dealer[$m]['num_invitation'];
							$excusas += $excusas_soporte_cupo;
							break;
						}
					}
				}

				//Si el concesionario pertenece a la sesion
				//if( $dealer_id == $Rows_Detail[$y]['origin_dealer'] && ($Rows_Detail[$y]['dealer_inscriptions']-$excusas)>0 ){
				if( $dealer_id == $Rows_Detail[$y]['origin_dealer'] && $Rows_Detail[$y]['dealer_inscriptions']>0 ){

					$Rows_Detail[$y]['excusas'] = $excusas;
					//$Rows_Detail[$y]['dealer_inscriptions'] = $Rows_Detail[$y]['dealer_inscriptions'] - $excusas;

					//****DETERMINA EL COSTO DE CADA SESION****
					if ( $Rows_Detail[$y]['rate_course'] == 0 ) { //Costo de la sesion si no existe acuerdo

						$Rows_Detail[$y]['plan_sede'] = 'NO'; //NO EXISTE ACUERDO

						$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); // Valor del cupo

						$Rows_Detail[$y]['rate_course'] = $valor_cupo * ($Rows_Detail[$y]['dealer_inscriptions']);//VALOR DEL CURSO

						//****** SI HAY INSCRITOS EN EL CONCESIONARIO DE LA SALA PRESTANTE SE LE DESCUENTA CUPO *****
						$Rows_Detail[$y]['descuento'] = $this->Descuento_cupo( $Rows_Detail[$y]['dealer_id'], $Rows_Detail[$y]['origin_dealer'], $valor_cupo, $Rows_Detail[$y]['living'], $Rows_Detail[$y]['living_id'], $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); // Valor del descuento
						//$Rows_Detail[$y]['descuento'] = 0;
						
					} else { //Costo de la sesion si existe acuerdo

						$concesionarioX = $Rows_Detail[$y]['dealer_id'];

						//Se define el valor especial de acuerdo por dia
						if ( $Rows_Detail[$y]['duration_time'] > 8 ) {
							$valor_acuerdo = $Rows_Detail[$y]['rate_course'];
							$Rows_Detail[$y]['rate_course'] = $valor_acuerdo*($Rows_Detail[$y]['duration_time'] / 8);
						}


						//**** DETERMINA SI LA SESION PERTENECE A LA PRESTADORA DE LA SALA ***********
						if ( $Rows_Detail[$y]['origin_dealer'] == $concesionarioX ) {

							$Rows_Detail[$y]['plan_sede'] = 'SI'; //SI EXISTE ACUERDO

							//Consultar la asistencia de otros concesionarios en la misma sala
							$concesionarioY = $this->Concesionarios_acuerdos($schedule_id, $Rows_Detail[$y]['origin_dealer'], 0);
							$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); //Valor Cupo para futuros descuentos

							if ( $concesionarioY['concesionario'] == true ) { //Si existe otro concesionario hay que separar costos

								if ( $concesionarioY['acuerdo'] == true ) { //Si el otro concesionario tiene acuerdo
									//Se dividen por mitades los costos
									$Rows_Detail[$y]['rate_course'] = $Rows_Detail[$y]['rate_course']/2; //VALOR DEL CURSO

								}else {//Si no existe acuerdo con el otro concesionario
									// Se le debe restar al costo, el total de cupos de los otros concesionarios
									$valor_acuerdo = $Rows_Detail[$y]['rate_course'];
									$Rows_Detail[$y]['rate_course'] = $valor_acuerdo-($valor_cupo * $concesionarioY['personal']);
								}

							}

							//****** SI HAY INSCRITOS EN EL CONCESIONARIO DE LA SALA PRESTANTE SE LE DESCUENTA CUPO *****
							$Rows_Detail[$y]['descuento'] = $this->Descuento_cupo( $Rows_Detail[$y]['dealer_id'], $Rows_Detail[$y]['origin_dealer'], $valor_cupo, $Rows_Detail[$y]['living'], $Rows_Detail[$y]['living_id'], $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); // Valor del descuento
							//$Rows_Detail[$y]['descuento'] = $Rows_Detail[$y]['rate_course'] / $Rows_Detail[$y]['inscriptions'];

						} else { 

						//**** CUANDO LA SESION NO PERTENECE A LA PRESTADORA DE LA SALA ***********
							$concesionarioY = $this->Concesionarios_acuerdos($schedule_id, $Rows_Detail[$y]['origin_dealer'], 1);
							
							if ( $concesionarioY['concesionario'] == true ) { //Si la sala prestadora tiene cupos

								if ( $concesionarioY['acuerdo'] == true ) { //Si el otro concesionario (externo a sala) tiene acuerdo
									//Se dividen por mitades los costos
									$Rows_Detail[$y]['plan_sede'] = 'SI'; //SI EXISTE ACUERDO
									$Rows_Detail[$y]['rate_course'] = $Rows_Detail[$y]['rate_course']/2;//VALOR DEL CURSO
								}else {
									$Rows_Detail[$y]['plan_sede'] = 'NO'; 
									//Si no existe acuerdo con el otro concesionario
									$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); //Valor Cupo
									// Se le debe restar al costo, el total de cupos de los otros concesionarios
									$Rows_Detail[$y]['rate_course'] = ($valor_cupo * ($Rows_Detail[$y]['dealer_inscriptions']));
								}

							} else { //*** Si la sala prestadora NO tiene cupos ****

								if ( $concesionarioY['acuerdo'] == true ) { //Si el otro concesionario (externo a sala) tiene acuerdo
									//EL concesionario externo asume todo el costo
									$Rows_Detail[$y]['plan_sede'] = 'SI'; //SI EXISTE ACUERDO
									//$Rows_Detail[$y]['rate_course'] = 2399999;
								} else {
									$Rows_Detail[$y]['plan_sede'] = 'NO'; 
									//Si no existe acuerdo con el otro concesionario
									$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); //Valor Cupo
									// Se le debe restar al costo, el total de cupos de los otros concesionarios
									$Rows_Detail[$y]['rate_course'] = ($valor_cupo * ($Rows_Detail[$y]['dealer_inscriptions']));
								}
							}

						}
						
					}

					//****** SI EXISTE UN COSTO ESPECIAL EN LA SESION Y CONCESIONARIO ACTUAL *****
					for($z=0;$z<count($Rows_Cost_Special);$z++) {
						//Si en la sesion existe algun costo especial 
						if( $schedule_id == $Rows_Cost_Special[$z]['schedule_id'] && $dealer_id == $Rows_Cost_Special[$z]['dealer_id'] ){
							$Rows_Detail[$y]['rate_course'] = $Rows_Cost_Special[$z]['cost_total'];
							$Rows_Detail[$y]['plan_sede'] .= " - Cobro Especial";
						}
					}

					//***VALOR DEL CUPO POR PERSONA****
					$Rows_Detail[$y]['valor_cupo'] = $Rows_Detail[$y]['rate_course']/$Rows_Detail[$y]['dealer_inscriptions'];

					//****** SI HAY LEGALIZACION DE ALIMENTACION EN EL CONCESIONARIO DE LA SALA PRESTANTE *****
					for($z=0;$z<count($Rows_Food);$z++) {
						//Si la sesion prestadora de sala es igual a la legalizacion de alimentos
						if( $schedule_id == $Rows_Food[$z]['schedule_id'] && $dealer_id == $Rows_Food[$z]['dealer_id'] ){
							// Si es difrente a una VIRTUAL CLASS
							if ( $Rows_Detail[$y]['living'] != 'VIRTUAL CLASS' && $Rows_Detail[$y]['living_id'] != '372' ) { 
								$Rows_Detail[$y]['alimentacion'] = $Rows_Food[$z]['total'];
							}
						}
					}

					//Se añaden los detalles de los costos y descuentos
					$Rows_config[$i]['Detalle_Sesiones'][] = $Rows_Detail[$y];
				} else {

					if( $dealer_id == $Rows_Detail[$y]['origin_dealer'] ){

						if ( $excusas > 0) {
							$Rows_Detail[$y]['excusas'] = $excusas;
							$Rows_Detail[$y]['dealer_inscriptions'] = $Rows_Detail[$y]['dealer_inscriptions'];
							//$Rows_Detail[$y]['dealer_inscriptions'] = $Rows_Detail[$y]['dealer_inscriptions'] - $excusas;
							$Rows_Detail[$y]['rate_course'] = 0;
							$Rows_Detail[$y]['plan_sede'] = 'Excusa';

							if ( $excusas_soporte_cupo > 0 ) {
								$Rows_Detail[$y]['plan_sede'] = 'Excusa Soporte Cupo';
							}

							$Rows_Detail[$y]['valor_cupo'] = 0;
							//Se añaden los detalles de los costos y descuentos
							$Rows_config[$i]['Detalle_Sesiones'][] = $Rows_Detail[$y];
						}

					}

				}
				
			}

		}
		/*Establece los detalles*/
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function Concesionarios_acuerdos( $schedule_id, $origen_dealer_id, $status ){ 

		include_once('../config/database.php');
		include_once('../config/config.php');

		$query_invitation =  "SELECT i.invitation_id, i.schedule_id, COUNT(i.user_id) as users, h.headquarter_id, h.headquarter, h.rate_course, h.dealer_id, d.dealer, s.course_id FROM ludus_invitation i, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_schedule s WHERE i.user_id = u.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND i.schedule_id = s.schedule_id AND i.schedule_id = $schedule_id GROUP BY h.headquarter_id";

		$query_Excusas3 = "SELECT u.user_id, u.first_name, u.last_name, u.identification, h.headquarter, e.file, e.schedule_id, d.dealer_id, d.dealer, a.area, z.zone, ds.min_size, ds.max_size, ds.inscriptions, ds.total_size, e.aplica_para
				FROM ludus_excuses e, ludus_users u, ludus_invitation i, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, ludus_dealer_schedule ds
				WHERE e.schedule_id IN ($schedule_id) AND e.user_id = u.user_id AND u.user_id = i.user_id AND e.schedule_id = i.schedule_id AND i.headquarter_id = h.headquarter_id AND d.dealer_id = ds.dealer_id AND e.schedule_id = ds.schedule_id
				AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND e.status_id = 1 AND e.aplica_para IN ('','COBRO') ";

		$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.file, ed.description, ed.type, ed.aplica_para 
			FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds 
			WHERE ed.schedule_id IN ($schedule_id) AND ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.status_id = 1  ";

		$DataBase_Log = new Database();
		$Rows_invitations = $DataBase_Log->SQL_SelectMultipleRows($query_invitation);
		//$Rows_invitations_detail = $DataBase_Log->SQL_SelectMultipleRows($query_invitation_detail);
		$Rows_excuses = $DataBase_Log->SQL_SelectMultipleRows($query_Excusas3);
		$Rows_excuses_dealer = $DataBase_Log->SQL_SelectMultipleRows($query_Excusa_Dealer);

		$resultado = array( 'concesionario' => false, 'acuerdo' => false, 'personal' => 0 );

		//asistentes de otros concesionarios en la misma sesion
		for($m=0;$m<count($Rows_invitations);$m++) {
			if ( $Rows_invitations[$m]['users'] > 0) { //Se ignoran las sesiones cambiadas en el history
				if( $origen_dealer_id != $Rows_invitations[$m]['dealer_id'] ){
					$resultado['concesionario'] = true;
					$resultado['personal'] += $Rows_invitations[$m]['users']; //Numero de inscritos de las otras sedes 
				}
			}
		}

		//Numero de excusas individuales por cada sesion en cada concesionario
		for($n=0;$n<count($Rows_excuses);$n++) {
			if ($origen_dealer_id != $Rows_excuses[$n]['dealer_id']) {
				$resultado['personal']--;
			}
		}

		if ( $resultado['personal'] > 0 ) { $resultado['concesionario'] = true; } 
		else {  $resultado['concesionario'] = false; }

		//Calcular si las sedes de los otros concesionarios tienen acuerdo (siempre se consulta con concesionariosY)
		if ( $resultado['concesionario'] == true || $status == 1 ) {
			$no_acuerdos =0;
			$acuerdos = 0;
			for($m=0;$m<count($Rows_invitations);$m++) {

				if ($status == 1 ) { //Si el origen_dealer NO pertenece a la sala

					if( $origen_dealer_id == $Rows_invitations[$m]['dealer_id'] ){ //Si es el concesionario externo
						if( $Rows_invitations[$m]['rate_course'] != 0 ){ //Si tiene acuerdo las sedes
							//$resultado['acuerdo'] = true;
							$acuerdos += 1; 
						}else{
							//$resultado['acuerdo'] = false;
							$no_acuerdos += 1;
						}
					}

				} else { //Si el origen_dealer pertenece a la sala

					if( $origen_dealer_id != $Rows_invitations[$m]['dealer_id'] ){ //Si es el concesionario externo
						if( $Rows_invitations[$m]['rate_course'] != 0 ){ //Si tiene acuerdo las sedes
							//$resultado['acuerdo'] = true;
							$acuerdos += 1;
						}else{
							//$resultado['acuerdo'] = false;
							$no_acuerdos += 1;
						}
					}
				}

			}
			//
			if ( $acuerdos >= $no_acuerdos ) {
				$resultado['acuerdo'] = true;
			}
		}


		unset($DataBase_Log); 
		return $resultado;
	}

	function Descuento_cupo( $dealer_id_sala , $origen_dealer_id, $valor_cupo, $sala, $sala_id, $type, $duration ){ 
		//****** SI HAY INSCRITOS EN EL CONCESIONARIO DE LA SALA PRESTANTE SE LE DESCUENTA CUPO *****
		$descuento = 0;

		if ( $dealer_id_sala == $origen_dealer_id ) {
			if ( $sala != 'VIRTUAL CLASS' && $sala_id != '372' ) { // Si es difrente a una VIRTUAL CLASS
				if ( $duration > 8 ) { // Si el curso es mayor a 8horas, el valor del descuento sera el valor_cupo de 8horas
					$duration = 8;
					$descuento = $this->Valor_cupo( $type, $duration );
				}else{
					$descuento = $valor_cupo;
				}
			}
		}

		return $descuento;

	}

	function Valor_cupo( $type, $duration ){ //Guardar el valor por cada cupo si no existe acuerdo, acuerdo en route_course

		include_once('../config/database.php');
		include_once('../config/config.php');

			//consulta los parametros de cobro
		$query_parameters = "SELECT * FROM ludus_parameters";
		$DataBase_Log = new Database();
		$Rows_Parameter = $DataBase_Log->SQL_SelectMultipleRows($query_parameters);

		$rate_course = 0;
		/*Validar parametros*/
		for($i=0;$i<count($Rows_Parameter);$i++) {
			if ( $type == "IBT" && $duration == "2" && $Rows_Parameter[$i]['parameter_id'] == 6 ) { //ID de parametro IBT 2 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "IBT" && $duration == "4" && $Rows_Parameter[$i]['parameter_id'] == 5 ) { //ID de parametro IBT 4 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "IBT" && $duration == "8" && $Rows_Parameter[$i]['parameter_id'] == 4 ) { //ID de parametro IBT 8 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "IBT" && $duration == "16" && $Rows_Parameter[$i]['parameter_id'] == 3 ) { //ID de parametro IBT 16 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "IBT" && $duration == "24" && $Rows_Parameter[$i]['parameter_id'] == 10 ) { //ID de parametro IBT 24 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "IBT" && $duration == "32" && $Rows_Parameter[$i]['parameter_id'] == 11 ) { //ID de parametro IBT 32 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "IBT" && $duration == "40" && $Rows_Parameter[$i]['parameter_id'] == 12 ) { //ID de parametro IBT 40 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "VCT" && $duration == "2" && $Rows_Parameter[$i]['parameter_id'] == 8 ) { //ID de parametro VCT 2 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
			if ( $type == "OJT" && $duration == "2" && $Rows_Parameter[$i]['parameter_id'] == 7 ) { //ID de parametro OJT 2 horas
				$rate_course = $Rows_Parameter[$i]['value'];
			}
		}

		unset($DataBase_Log); 
		return $rate_course;
	}

	function consulta_especialidades(){ 

		include_once('../config/database.php');
		include_once('../config/config.php');

		$query_sql = "SELECT specialty_id, specialty, status_id, 0 as cobro, 0 as descuento, 0 as alimentacion, 0 as prestamos, 0 as excusas FROM ludus_specialties WHERE status_id = 1";
		$DataBase_Log = new Database();
		$Rows_config = $DataBase_Log->SQL_SelectMultipleRows($query_sql);

		unset($DataBase_Log); 
		return $Rows_config;
	}

	function ccsHistoricoUsuario( $fecha_sesion, $usuario_id ){
        include_once('../config/database.php');
		include_once('../config/config.php');

        $query_sql = "SELECT i.*, h.headquarter_id, h.headquarter, d.dealer_id, d.dealer
            FROM ludus_headquarter_history i, ludus_headquarters h, ludus_dealers d, (
                SELECT MAX(date_creation) as date_creation, user_id FROM ludus_headquarter_history
                WHERE date_creation < '$fecha_sesion' AND user_id = $usuario_id GROUP BY user_id
            ) as hy
            WHERE i.date_creation = hy.date_creation
            AND i.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND i.user_id = hy.user_id";

		$DataBase_Log = new Database();
		$Rows_config = $DataBase_Log->SQL_SelectRows($query_sql);

        return $Rows_config;
    }

	function consultaAjustesMes($new_dealer_id,$start_date,$end_date){ 

		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();

		$sql = "";
		if ($new_dealer_id == 0) {
			//Si dealer_id no es enviado se trae de la variable Session
			if(isset($_SESSION['max_rol']) && $_SESSION['max_rol']>4){
				$sql = "";
			}else{
				$sql = " AND dealer_id = ".$_SESSION['dealer_id'];
			}
		} else {
			$sql = " AND dealer_id = ".$new_dealer_id;
		}

		$query_sql = "SELECT * FROM ludus_extra_cost
			WHERE type = 2 AND status_id = 1 AND date_cost BETWEEN '$start_date' AND '$end_date' {$sql} ";
		$DataBase_Log = new Database();
		$Rows_config = $DataBase_Log->SQL_SelectMultipleRows($query_sql);

		unset($DataBase_Log); 
		return $Rows_config;
	}

	function consulta_Costo_Concesionario_Sesion( $start_date,$end_date ){ // Consulta los costos de cada sesion para cada concesionario
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			//consulta las sesiones realizadas en cierto tiempo detalladamente con concesionario, curso, duracion y demas
			$query_detail = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, l.living_id, l.living, h.headquarter, d.dealer, d.dealer_id, i.inscriptions, c.course, c.newcode, c.type, c.course_id, c.specialty_id, h.rate_course, m.duration_time, ds.dealer_id as origin_dealer, 0 as dealer_inscriptions, ds.max_size as max , ds.min_size as min, ds.inscriptions as inscritos
				FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, ludus_dealer_schedule ds, ludus_modules m, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM ludus_dealer_schedule GROUP BY schedule_id) as i 
				WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = ds.schedule_id AND s.schedule_id = i.schedule_id AND s.course_id = c.course_id AND c.course_id = m.course_id AND c.type IN ('IBT','VCT','OJT') AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND s.status_id = 1 AND (ds.inscriptions NOT IN (0) OR ds.max_size NOT IN (0))
				ORDER BY s.schedule_id";

			$query_inscritos = "SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, s.schedule_id, d.dealer, d.dealer_id, COUNT(d.dealer_id) AS inscritos_old, 0 AS inscritos FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) GROUP BY s.schedule_id, d.dealer_id ORDER BY s.schedule_id";

			$query_inscritos_detail = " SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, u1.user_id, h.headquarter, s.schedule_id, d.dealer, d.dealer_id FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) ORDER BY s.schedule_id, d.dealer_id ";

			$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.file, ed.description, ed.type, ed.aplica_para 
				FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds 
				WHERE ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.status_id = 1 ";

			$query_cost_extra = "SELECT * FROM ludus_extra_cost WHERE type = 1 AND status_id = 1 ";

		} else {
			$query_detail = "SELECT s.schedule_id, s.start_date, s.end_date, s.min_size, s.max_size, l.living_id, l.living, h.headquarter, d.dealer, d.dealer_id, i.inscriptions, c.course, c.newcode, c.type, c.course_id, c.specialty_id, h.rate_course, m.duration_time, ds.dealer_id as origin_dealer, 0 as dealer_inscriptions, ds.max_size as max , ds.min_size as min, ds.inscriptions as inscritos
				FROM ludus_schedule s, ludus_livings l, ludus_headquarters h, ludus_dealers d, ludus_courses c, ludus_dealer_schedule ds, ludus_modules m, (SELECT schedule_id, sum(inscriptions) as inscriptions FROM ludus_dealer_schedule GROUP BY schedule_id) as i 
				WHERE s.living_id = l.living_id AND l.headquarter_id = h.headquarter_id AND h.dealer_id =d.dealer_id AND s.schedule_id = ds.schedule_id AND s.schedule_id = i.schedule_id AND s.course_id = c.course_id AND c.course_id = m.course_id AND c.type IN ('IBT','VCT','OJT') AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ds.dealer_id = $dealer_id AND s.status_id = 1 AND (ds.inscriptions NOT IN (0) OR ds.max_size NOT IN (0))
				ORDER BY s.schedule_id";

			$query_inscritos = "SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, s.schedule_id, d.dealer, d.dealer_id, COUNT(d.dealer_id) AS inscritos_old, 0 AS inscritos FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) AND d.dealer_id = $dealer_id GROUP BY s.schedule_id, d.dealer_id ORDER BY s.schedule_id";

			$query_inscritos_detail = " SELECT c.newcode, c.course, c.type, s.start_date, s.end_date, u1.user_id, h.headquarter, s.schedule_id, d.dealer, d.dealer_id FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1 AND aplica_para IN ('','COBRO') ) AND d.dealer_id = $dealer_id ORDER BY s.schedule_id, d.dealer_id ";

			$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.file, ed.description, ed.type, ed.aplica_para 
				FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds 
				WHERE ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.dealer_id = '$dealer_id' AND ed.status_id = 1 ";

			$query_cost_extra = "SELECT * FROM ludus_extra_cost WHERE type = 1 AND status_id = 1 AND dealer_id = '$dealer_id' ";

		}
		$DataBase_Acciones2 = new Database();
		$Rows_Detail 	= $DataBase_Acciones2->SQL_SelectMultipleRows($query_detail); //Sesiones en cada concesionario
		$Rows_inscritos = $DataBase_Acciones2->SQL_SelectMultipleRows($query_inscritos); //Inscritos en cada concesionario y sesion
		$Rows_inscritos_detail = $DataBase_Acciones2->SQL_SelectMultipleRows($query_inscritos_detail); //Detalle de Inscritos
		$Rows_Excusas_Dealer = $DataBase_Acciones2->SQL_SelectMultipleRows($query_Excusa_Dealer); //Excusas Soporte cupos por cada sesion
		$Rows_Cost_Special = $DataBase_Acciones2->SQL_SelectMultipleRows($query_cost_extra); //Cobos extras


		//AJUSTE DE CONCESIONARIO A USUARIO DEPENDIENDO DEL HISTORY
		/*for($l=0;$l<count($Rows_inscritos_detail);$l++) { 
			$historico = $this->ccsHistoricoUsuario( $Rows_inscritos_detail[$l]['start_date'], $Rows_inscritos_detail[$l]['user_id'] );
			$Rows_inscritos_detail[$l]['headquarter'] = $historico['headquarter'];
			$Rows_inscritos_detail[$l]['dealer_id'] = $historico['dealer_id'];
			$Rows_inscritos_detail[$l]['dealer'] = $historico['dealer'];
		}*/

		//CONTEO DE USUARIOS POR CADA SESION Y CONCESIONARIO DESPUES DEL HISTORY
		for($p=0;$p<count($Rows_inscritos);$p++) { 
			for($l=0;$l<count($Rows_inscritos_detail);$l++) { 
				if( $Rows_inscritos[$p]['schedule_id'] == $Rows_inscritos_detail[$l]['schedule_id'] ){
					if ( $Rows_inscritos[$p]['dealer_id'] == $Rows_inscritos_detail[$l]['dealer_id'] ) {
						$Rows_inscritos[$p]['inscritos'] += 1;
					}
				}
			}
		}
		// INCLUSION DE LOS CUPOS EN LAS SESIONES Y CONCESIONARIOS
		for($p=0;$p<count($Rows_Detail);$p++) { 
			for($p2=0;$p2<count($Rows_inscritos);$p2++) {
				if ( $Rows_inscritos[$p2]['inscritos'] > 0 ) { //Se ignoran las sesiones sin cupo cambiadas en el history
					if( $Rows_Detail[$p]['schedule_id'] == $Rows_inscritos[$p2]['schedule_id'] ){
						if ( $Rows_Detail[$p]['origin_dealer'] == $Rows_inscritos[$p2]['dealer_id'] ) {
							$Rows_Detail[$p]['dealer_inscriptions'] = $Rows_inscritos[$p2]['inscritos'];
							break;
						}
					}
				}
			}
		}

        // INCLUSION DE LOS CUPOS NO INSCRITOS Y/O NO INVITADOS
		for($p=0;$p<count($Rows_Detail);$p++) { 

			$no_inscritos = ($Rows_Detail[$p]['max'] - $Rows_Detail[$p]['inscritos']);
			 if( $no_inscritos > 0 ){
			    $Rows_Detail[$p]['dealer_inscriptions'] += $no_inscritos;
			}

			for($m=0;$m<count($Rows_Excusas_Dealer);$m++) {

				if( ($Rows_Detail[$p]['schedule_id'] == $Rows_Excusas_Dealer[$m]['schedule_id']) && ($Rows_Detail[$p]['origin_dealer'] == $Rows_Excusas_Dealer[$m]['dealer_id']) ){ //SE EVALUA SI EL CUPO TIENE EXCUSA SOPORTE CUPO

		            if( $no_inscritos > 0 ){
		                $Rows_Detail[$p]['dealer_inscriptions'] -= $Rows_Excusas_Dealer[$m]['num_invitation'];
		            }

	        	} 
	        }
        }

		//Las sesiones que se dictaron
		for($y=0;$y<count($Rows_Detail);$y++) {

			$schedule_id = $Rows_Detail[$y]['schedule_id'];
			$valor_acuerdo = 0;

			//Si el concesionario pertenece a la sesion
			if( $Rows_Detail[$y]['dealer_inscriptions']>0 ){

				//****DETERMINA EL COSTO DE CADA SESION****
				if ( $Rows_Detail[$y]['rate_course'] == 0 ) { //Costo de la sesion si no existe acuerdo

					//NO EXISTE ACUERDO
					$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); // Valor del cupo
					$Rows_Detail[$y]['rate_course'] = $valor_cupo * $Rows_Detail[$y]['dealer_inscriptions'];
					
				} else { //Costo de la sesion si existe acuerdo

					$concesionarioX = $Rows_Detail[$y]['dealer_id'];

					//Se define el valor especial de acuerdo por dia
					if ( $Rows_Detail[$y]['duration_time'] > 8 ) {
						$valor_acuerdo = $Rows_Detail[$y]['rate_course'];
						$Rows_Detail[$y]['rate_course'] = $valor_acuerdo*($Rows_Detail[$y]['duration_time'] / 8);
					}

					//**** DETERMINA SI LA SESION PERTENECE A LA PRESTADORA DE LA SALA ***********
					if ( $Rows_Detail[$y]['origin_dealer'] == $concesionarioX ) {

						//SI EXISTE ACUERDO

						//Consultar la asistencia de otros concesionarios en la misma sala
						$concesionarioY = $this->Concesionarios_acuerdos($schedule_id, $Rows_Detail[$y]['origin_dealer'], 0);
						$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); //Valor Cupo para futuros descuentos

						if ( $concesionarioY['concesionario'] == true ) { //Si existe otro concesionario hay que separar costos

							if ( $concesionarioY['acuerdo'] == true ) { //Si el otro concesionario tiene acuerdo
								//Se dividen por mitades los costos
								$Rows_Detail[$y]['rate_course'] = $Rows_Detail[$y]['rate_course']/2;
							}else {//Si no existe acuerdo con el otro concesionario
								// Se le debe restar al costo, el total de cupos de los otros concesionarios
								$valor_acuerdo = $Rows_Detail[$y]['rate_course'];
								$Rows_Detail[$y]['rate_course'] = $valor_acuerdo-($valor_cupo * $concesionarioY['personal']);
							}

						}

					} else { 

					//**** CUANDO LA SESION NO PERTENECE A LA PRESTADORA DE LA SALA ***********
						$concesionarioY = $this->Concesionarios_acuerdos($schedule_id, $Rows_Detail[$y]['origin_dealer'], 1);
						
						if ( $concesionarioY['concesionario'] == true ) { //Si la sala prestadora tiene cupos

							if ( $concesionarioY['acuerdo'] == true ) { //Si el otro concesionario (externo a sala) tiene acuerdo
								//Se dividen por mitades los costos
								//SI EXISTE ACUERDO
								$Rows_Detail[$y]['rate_course'] = $Rows_Detail[$y]['rate_course']/2;
							}else { 
								//Si no existe acuerdo con el otro concesionario
								$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); //Valor Cupo
								// Se le debe restar al costo, el total de cupos de los otros concesionarios
								$Rows_Detail[$y]['rate_course'] = ($valor_cupo * $Rows_Detail[$y]['dealer_inscriptions']);
							}

						} else { //*** Si la sala prestadora NO tiene cupos ****

							if ( $concesionarioY['acuerdo'] == true ) { //Si el otro concesionario (externo a sala) tiene acuerdo
								//EL concesionario externo asume todo el costo
								//SI EXISTE ACUERDO
								//$Rows_Detail[$y]['rate_course'] = 2399999;
							} else {
								//Si no existe acuerdo con el otro concesionario
								$valor_cupo = $this->Valor_cupo( $Rows_Detail[$y]['type'], $Rows_Detail[$y]['duration_time'] ); //Valor Cupo
								// Se le debe restar al costo, el total de cupos de los otros concesionarios
								$Rows_Detail[$y]['rate_course'] = ($valor_cupo * $Rows_Detail[$y]['dealer_inscriptions']);
							}
						}

					}
					
				}

				//****** SI EXISTE UN COSTO ESPECIAL EN LA SESION Y CONCESIONARIO ACTUAL *****
				for($z=0;$z<count($Rows_Cost_Special);$z++) {
					//Si en la sesion existe algun costo especial
					if( $schedule_id == $Rows_Cost_Special[$z]['schedule_id'] && $Rows_Detail[$y]['origin_dealer'] == $Rows_Cost_Special[$z]['dealer_id'] ){
						$Rows_Detail[$y]['rate_course'] = $Rows_Cost_Special[$z]['cost_total'];
					}
				}

			} else {
				//$Rows_Detail[$y]['dealer_inscriptions'] = $Rows_Detail[$y]['dealer_inscriptions'] - $excusas;
				$Rows_Detail[$y]['dealer_inscriptions'] = $Rows_Detail[$y]['dealer_inscriptions'];
				$Rows_Detail[$y]['rate_course'] = 0;
			}
			
		}

		/*Establece los detalles*/
		unset($DataBase_Acciones2);
		return $Rows_Detail;
	}

	function Reporte_consultaDatosAll( $start_date,$end_date ){ // Creacion de reporte

		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){

			$query_sql = "SELECT DISTINCT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, (SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci, ludus_specialties spe, ludus_supplier su
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id
			AND c.course_id = m.course_id AND l.city_id = ci.city_id AND c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id ";
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20

			$query_sql_det = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.user_id, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, i.invitation_id, u1.identification, s.schedule_id, d.dealer, a.area, z.zone, d.dealer_id
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id
			AND i.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id
			AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1)";
			$query_sql_det .= " ORDER BY d.dealer_id, h.headquarter";//LIMIT 0,20

		}else{

			$query_sql = "SELECT DISTINCT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, (SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci, ludus_specialties spe, ludus_supplier su
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id AND h.dealer_id = '$dealer_id'
			AND c.course_id = m.course_id AND l.city_id = ci.city_id AND c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id ";
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20

			$query_sql_det = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.user_id, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, i.invitation_id, u1.identification, s.schedule_id, d.dealer, a.area, z.zone, d.dealer_id
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id
			AND i.headquarter_id = h.headquarter_id AND h.dealer_id = '$dealer_id' AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id
			AND i.invitation_id NOT IN (SELECT invitation_id FROM `ludus_excuses` WHERE status_id=1)";
			$query_sql_det .= " ORDER BY d.dealer_id, h.headquarter";//LIMIT 0,20

		}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$Rows_detail = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_det);

		$Rows_routes = $this->consulta_Costo_Concesionario_Sesion( $start_date,$end_date );



		$query_charges = "SELECT u.identification, c.charge FROM ludus_users u, ludus_charges_users cu, ludus_charges c WHERE u.user_id = cu.user_id AND
					cu.charge_id = c.charge_id";
					//consulta todos los cargos de todos los usuarios  u.status_id = 1 AND
		$Rows_Charges = $DataBase_Acciones->SQL_SelectMultipleRows($query_charges);

		//AJUSTE DE CONCESIONARIO A USUARIO DEPENDIENDO DEL HISTORY
		$invitations = "1";
		for($y=0;$y<count($Rows_detail);$y++) {
			/*$historico = $this->ccsHistoricoUsuario( $Rows_detail[$y]['start_date'], $Rows_detail[$y]['user_id'] );
			$Rows_detail[$y]['headquarter'] = $historico['headquarter'];
			$Rows_detail[$y]['dealer_id'] = $historico['dealer_id'];
			$Rows_detail[$y]['dealer'] = $historico['dealer'];*/
			$invitations .= ",".$Rows_detail[$y]['invitation_id'];
		}

		$schedules = "0";
		for($y=0;$y<count($Rows_config);$y++) {
			$schedules .= ",".$Rows_config[$y]['schedule_id'];
		}

		$query_nota = "SELECT i.invitation_id, i.approval, i.score, i.score_review, i.score_waybill
						FROM `ludus_modules_results_usr` i
						WHERE i.status_id = 1 AND i.invitation_id IN ($invitations)";
						//echo $query_nota;
		$Rows_notas = $DataBase_Acciones->SQL_SelectMultipleRows($query_nota);

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){

			$query_asis = "SELECT s.schedule_id, d.dealer, s.min_size, s.max_size, s.inscriptions, s.total_size, d.dealer_id
			FROM ludus_dealer_schedule s, ludus_dealers d
			WHERE s.schedule_id IN ($schedules) AND s.dealer_id = d.dealer_id
			AND s.max_size > 0";
			$query_Excusas = "SELECT u.user_id, u.first_name, u.last_name, u.identification, h.headquarter, e.file, e.schedule_id, d.dealer, a.area, z.zone, ds.min_size, ds.max_size, ds.inscriptions, ds.total_size, e.aplica_para
				FROM ludus_excuses e, ludus_users u, ludus_invitation i, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, ludus_dealer_schedule ds
				WHERE e.schedule_id IN ($schedules) AND e.user_id = u.user_id AND u.user_id = i.user_id AND e.schedule_id = i.schedule_id AND i.headquarter_id = h.headquarter_id AND d.dealer_id = ds.dealer_id AND e.schedule_id = ds.schedule_id
				AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND e.status_id = 1 AND e.aplica_para IN ('','COBRO')  ";
			$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, h.headquarter_id, a.area, z.zone, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.file, ed.description, ed.type, ed.aplica_para 
				FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds, (SELECT headquarter_id, dealer_id, area_id FROM ludus_headquarters WHERE type_headquarter = 'Principal' AND status_id = 1 GROUP BY dealer_id ORDER BY dealer_id) h, ludus_areas a, ludus_zone z
				WHERE ed.schedule_id IN ($schedules) AND ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND ed.status_id = 1 ";
		}else{

			$query_asis = "SELECT s.schedule_id, d.dealer, s.min_size, s.max_size, s.inscriptions, s.total_size, d.dealer_id
			FROM ludus_dealer_schedule s, ludus_dealers d
			WHERE s.schedule_id IN ($schedules) AND s.dealer_id = d.dealer_id AND d.dealer_id = '$dealer_id'
			AND s.max_size > 0";
			$query_Excusas = "SELECT u.user_id, u.first_name, u.last_name, u.identification, h.headquarter, e.file, e.schedule_id, d.dealer, a.area, z.zone, ds.min_size, ds.max_size, ds.total_size, ds.inscriptions, e.aplica_para
				FROM ludus_excuses e, ludus_users u, ludus_invitation i, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z, ludus_dealer_schedule ds
				WHERE e.schedule_id IN ($schedules) AND e.user_id = u.user_id AND u.user_id = i.user_id AND e.schedule_id = i.schedule_id AND i.headquarter_id = h.headquarter_id AND d.dealer_id = ds.dealer_id AND e.schedule_id = ds.schedule_id
				AND h.dealer_id = '$dealer_id' AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND e.status_id = 1 AND e.aplica_para IN ('','COBRO') ";
			$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, h.headquarter_id, a.area, z.zone, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.file, ed.description, ed.type, ed.aplica_para 
				FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds , (SELECT headquarter_id, dealer_id, area_id FROM ludus_headquarters WHERE type_headquarter = 'Principal' AND status_id = 1 GROUP BY dealer_id ORDER BY dealer_id) h, ludus_areas a, ludus_zone z
				WHERE ed.schedule_id IN ($schedules) AND ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.dealer_id = '$dealer_id' AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND ed.status_id = 1  ";
		}
		$Rows_asistencia = $DataBase_Acciones->SQL_SelectMultipleRows($query_asis);
		$Rows_excusas = $DataBase_Acciones->SQL_SelectMultipleRows($query_Excusas);
		$Rows_excusas_dealer = $DataBase_Acciones->SQL_SelectMultipleRows($query_Excusa_Dealer);


		//print_r($Rows_config);
		for($i=0;$i<count($Rows_config);$i++) {
			$var_schedule_id = $Rows_config[$i]['schedule_id'];
			for($y=0;$y<count($Rows_detail);$y++) {
				if($var_schedule_id == $Rows_detail[$y]['schedule_id']){
					//print_r($Rows_config[$i]['resultados']);
					$var_Invitation = $Rows_detail[$y]['invitation_id'];
					for($z=0;$z<count($Rows_notas);$z++) {
						if($var_Invitation == $Rows_notas[$z]['invitation_id']){
							//print_r($Rows_notas[$y]);
							$Rows_detail[$y]['resultados'][] = $Rows_notas[$z];
							//print_r($Rows_config[$i]['resultados']);
						}
					}
					$var_identification = $Rows_detail[$y]['identification'];

					// Se agrega el break para que solo agregue un cargo
					for($l=0;$l<count($Rows_Charges);$l++){
						if($var_identification==$Rows_Charges[$l]['identification']){
							$Rows_detail[$y]['charges'] = $Rows_Charges[$l]['charge'];
							break;
						}
					}

					// Se agrega el costo por cada persona
					$schedule_personal = $Rows_detail[$y]['schedule_id'];
					$dealer_personal = $Rows_detail[$y]['dealer_id'];
					for($p=0;$p<count($Rows_routes);$p++){
						$Rows_detail[$y]['rate'] = '';
						if($schedule_personal==$Rows_routes[$p]['schedule_id']){ //Si son la misma sesion 
							if ( $dealer_personal==$Rows_routes[$p]['origin_dealer'] ) {  //Si es el mismo concesionario
								if ( $Rows_routes[$p]['dealer_inscriptions'] > 0 ) { // Si existen participantes
									$Rows_detail[$y]['rate'] = $Rows_routes[$p]['rate_course']/$Rows_routes[$p]['dealer_inscriptions'];
								}else {
									$Rows_detail[$y]['rate'] = 0;
								}
								break;
							}
						}
					}

					//print_r($Rows_detail[$y]);
					$Rows_config[$i]['detail'][] = $Rows_detail[$y];
				}
			}
			for($y=0;$y<count($Rows_excusas);$y++) {
				if($var_schedule_id == $Rows_excusas[$y]['schedule_id']){
					$var_identification = $Rows_excusas[$y]['identification'];
					for($l=0;$l<count($Rows_Charges);$l++){
						if($var_identification==$Rows_Charges[$l]['identification']){
							$Rows_excusas[$y]['charges'] = $Rows_Charges[$l]['charge'];
							break;
						}
					}
					$Rows_config[$i]['excusas'][] = $Rows_excusas[$y];
				}
			}
			for($y=0;$y<count($Rows_excusas_dealer);$y++) {
				if($var_schedule_id == $Rows_excusas_dealer[$y]['schedule_id']){
					$Rows_config[$i]['excusas_dealer'][] = $Rows_excusas_dealer[$y];
				}
			}

			for($y=0;$y<count($Rows_asistencia);$y++) {
				if($var_schedule_id == $Rows_asistencia[$y]['schedule_id']){
					$Rows_config[$i]['asistencia'][] = $Rows_asistencia[$y];
				}
			}

			for($y=0;$y<count($Rows_routes);$y++) {
				if($var_schedule_id == $Rows_routes[$y]['schedule_id']){
					if ( $Rows_routes[$y]['max'] > 0 && $Rows_routes[$y]['inscritos'] == 0 ) {
						$Rows_config[$i]['noinvitado'][] = $Rows_routes[$y];
					}
				}
			}
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}//


	//Funcion que obtiene las líneas de aquellos que no invitaron a nadie a la sesión - REPORTE
	public function Reporte_consultaDatosNoInv($start_date,$end_date){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT DISTINCT d.dealer_id, d.dealer, c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, h.headquarter_id, a.area, z.zone, u.first_name as first_prof, u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier, ds.min_size as minimos, ds.max_size as maximos, ds.inscriptions, 0 AS valor, 0 AS cupos, 0 AS excusas
			FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s, ludus_courses c, ludus_livings l, ludus_users u,
			(SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci,
			ludus_specialties spe, ludus_supplier su, (SELECT headquarter_id, dealer_id, area_id FROM ludus_headquarters WHERE type_headquarter = 'Principal' AND status_id = 1 GROUP BY dealer_id ORDER BY dealer_id) h, ludus_areas a, ludus_zone z
			WHERE s.status_id = 1 AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ds.schedule_id = s.schedule_id AND ds.dealer_id = d.dealer_id AND ds.max_size > 0 AND ds.inscriptions = 0 AND s.course_id = c.course_id AND s.living_id = l.living_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND s.teacher_id = u.user_id AND s.course_id = m.course_id AND l.city_id = ci.city_id AND c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id ";
			
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20
		} else {
			$query_sql = "SELECT DISTINCT d.dealer_id, d.dealer, c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, h.headquarter_id, a.area, z.zone, u.first_name as first_prof,
			u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier, ds.min_size as minimos, ds.max_size as maximos, ds.inscriptions, 0 AS valor, 0 AS cupos, 0 AS excusas
			FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s, ludus_courses c, ludus_livings l, ludus_users u,
			(SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci,
			ludus_specialties spe, ludus_supplier su, (SELECT headquarter_id, dealer_id, area_id FROM ludus_headquarters WHERE type_headquarter = 'Principal' AND status_id = 1 GROUP BY dealer_id ORDER BY dealer_id) h, ludus_areas a, ludus_zone z
			WHERE s.status_id = 1 AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ds.schedule_id = s.schedule_id AND ds.dealer_id = d.dealer_id AND ds.max_size > 0 AND ds.inscriptions = 0 AND s.course_id = c.course_id AND
			s.living_id = l.living_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id AND s.teacher_id = u.user_id AND s.course_id = m.course_id AND l.city_id = ci.city_id AND
			c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id AND d.dealer_id = '$dealer_id'";
			
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20
		}
		//echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		//SE RETIRAN DE NO INVITADOS LOS REGISTROS CON EXCUSA POR CUPO
		$schedules = "0";
		for($y=0;$y<count($Rows_config);$y++) {
			$schedules .= ",".$Rows_config[$y]['schedule_id'];
		}
		$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.aplica_para 
			FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds 
			WHERE ed.schedule_id IN ($schedules) AND ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.status_id = 1";

		$Rows_excusas_dealer = $DataBase_Acciones->SQL_SelectMultipleRows($query_Excusa_Dealer);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_schedule_id = $Rows_config[$i]['schedule_id'];
			$var_dealer_id = $Rows_config[$i]['dealer_id'];
			for($y=0;$y<count($Rows_excusas_dealer);$y++) {
				if ( $var_dealer_id == $Rows_excusas_dealer[$y]['dealer_id'] ) {
					if($var_schedule_id == $Rows_excusas_dealer[$y]['schedule_id']){
						$Rows_config[$i]['excusas'] = $Rows_excusas_dealer[$y]['num_invitation'];
						break;
					}
				}
			}
		}
		
		return $Rows_config;
	}

}
