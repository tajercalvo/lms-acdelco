<?php
Class Noticias {
	function consultaNoticias($type){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){
			/*$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_news m, ludus_users u
						WHERE m.editor = u.user_id AND m.type = '$type'
						ORDER BY m.date_edition DESC";*/
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_news m, ludus_users u
						WHERE m.editor = u.user_id
						ORDER BY m.date_edition DESC";
		}else{
			$query_sql = "SELECT m.*, u.first_name, u.last_name, u.image as image_usr, u.email
						FROM ludus_news m, ludus_users u
						WHERE m.status_id = 1 AND m.editor = u.user_id
						ORDER BY m.date_edition DESC";
		}
		// echo( $query_sql );
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		$query_Likes = "SELECT cu.*, u.first_name, u.last_name
						FROM ludus_news_likes cu, ludus_users u
						WHERE cu.user_id = u.user_id
						ORDER BY cu.date_edition ";
		$Rows_Likes = $DataBase_Acciones->SQL_SelectMultipleRows($query_Likes);

		for($i=0;$i<count($Rows_config);$i++) {
			$news_id = $Rows_config[$i]['news_id'];
			//Cargos
				for($y=0;$y<count($Rows_Likes);$y++) {
					if( $news_id == $Rows_Likes[$y]['news_id'] ){
						$Rows_config[$i]['likes_data'][] = $Rows_Likes[$y];
					}
				}
			//Cargos
		}

		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearNoticia($news,$description,$image,$type,$media_id,$video_id,$category, $archivo ){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");

		$insertSQL_EE = "INSERT INTO `ludus_news`
		(news,description,date_edition,editor,status_id,image,likes,comments,type,media_id,video_id,category, file)
		VALUES ('$news','$description','$NOW_data','$idQuien','1','$image',0,0,'$type','$media_id','$video_id','$category', '$archivo')";
		// echo( $insertSQL_EE );
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarNoticia($id,$news,$description,$image,$estado,$media_id,$video_id, $category, $archivo){
		@session_start();
		include_once('../config/database.php');
		include_once('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_news`
						SET news = '$news',
							description = '$description',
							editor = '$idQuien',
							date_edition = '$NOW_data',
							image = '$image',
							media_id = '$media_id',
							video_id = '$video_id',
							status_id = '$estado',
							category = '$category',
							file = '$archivo'
						WHERE news_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaNoticia($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_news c
			WHERE c.news_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaDatoGaleria($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_media c
			WHERE c.media_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearLike($news_id){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DataBase_Log = new Database();
		$insertSQL_ER = "INSERT INTO `ludus_news_likes` (news_id,user_id,date_edition) VALUES ('$news_id','$idQuien','$NOW_data')";
		$resultado_IN = $DataBase_Log->SQL_Update($insertSQL_ER);
		if($resultado_IN>0){
			$updateSQL_ER = "UPDATE `ludus_news`
						SET likes = likes+1
						WHERE news_id = '$news_id'";
			$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);

		}
		return $resultado_IN;
	}
	function consultaMed($type){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
					FROM ludus_media m
					WHERE m.status_id = 1 AND m.type = $type
					ORDER BY m.media ";
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaGaleria($idRegistro,$type){
		include_once('../config/database.php');
		include_once('../config/config.php');
		if($type==1){
			$query_sql = "SELECT m.*
			FROM ludus_news a, ludus_media_detail m
			WHERE a.news_id = '$idRegistro' AND a.media_id = m.media_id ";//LIMIT 0,20
		}else{
			$query_sql = "SELECT m.*
			FROM ludus_news a, ludus_media_detail m
			WHERE a.news_id = '$idRegistro' AND a.video_id = m.media_id ";//LIMIT 0,20
		}
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	//=================================================================================
	// consulta la lista de categorias
	//=================================================================================
	function listaCategorias(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT DISTINCT n.category FROM ludus_news n";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows( $query_sql );
		unset($DataBase_Acciones);
		return $Rows_config;
	}//fin funcion listaCategorias
}
