<?php
Class AsistenciaTotal {
	/*
	Andres Vega
	30/11/2016
	Funcion que lista los cursos y sus ID correspondientes,
	- se implementa en el select2 de cursosF
	*/
	public static function consultaCursos(){
		include_once('../config/init_db.php');
		$query_sql = "SELECT c.course_id, c.course FROM ludus_courses c";
		$Rows_config = DB::query($query_sql);
		return $Rows_config;
	}//fin funcion consultaCursos

	/*
	Diego Lamprea
	12/01/2018
	Funcion que obtiene las líneas de aquellos que no invitaron a nadie a la sesión
	*/
	public function consultaDatosNoInv($start_date,$end_date,$course_id){
		include_once('../config/database.php');
		include_once('../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT DISTINCT d.dealer_id, d.dealer, c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof,
			u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier, ds.min_size as minimos, ds.max_size as maximos, ds.inscriptions, 0 AS excusas
			FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s, ludus_courses c, ludus_livings l, ludus_users u,
			(SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci,
			ludus_specialties spe, ludus_supplier su
			WHERE s.status_id = 1 AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ds.schedule_id = s.schedule_id AND
			ds.dealer_id = d.dealer_id AND ds.max_size > 0 AND ds.inscriptions = 0 AND s.course_id = c.course_id AND
			s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.course_id = m.course_id AND l.city_id = ci.city_id AND
			c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id ";
			if($course_id != '0'){$query_sql .= " AND c.course_id = ".$course_id;}//valida si se selecciono un curso en especifico
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20
		}else{
			$query_sql = "SELECT DISTINCT d.dealer_id, d.dealer, c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof,
			u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier, ds.min_size as minimos, ds.max_size as maximos, ds.inscriptions, 0 AS excusas
			FROM ludus_dealer_schedule ds, ludus_dealers d, ludus_schedule s, ludus_courses c, ludus_livings l, ludus_users u,
			(SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci,
			ludus_specialties spe, ludus_supplier su
			WHERE s.status_id = 1 AND s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59' AND ds.schedule_id = s.schedule_id AND
			ds.dealer_id = d.dealer_id AND ds.max_size > 0 AND ds.inscriptions = 0 AND s.course_id = c.course_id AND
			s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.course_id = m.course_id AND l.city_id = ci.city_id AND
			c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id AND d.dealer_id = '$dealer_id'";
			if($course_id != '0'){$query_sql .= " AND c.course_id = ".$course_id;}//valida si se selecciono un curso en especifico
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20
		}
		echo($query_sql);
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

		//SE RETIRAN DE NO INVITADOS LOS REGISTROS CON EXCUSA POR CUPO
		$schedules = "0";
		for($y=0;$y<count($Rows_config);$y++) {
			$schedules .= ",".$Rows_config[$y]['schedule_id'];
		}
		$query_Excusa_Dealer = "SELECT ed.excuse_dealer_id, ed.dealer_id, d.dealer, ed.schedule_id, ed.num_invitation, ds.min_size, ds.max_size, ds.inscriptions, ed.aplica_para
			FROM ludus_excuses_dealers ed, ludus_dealers d, ludus_dealer_schedule ds
			WHERE ed.schedule_id IN ($schedules) AND ed.dealer_id = d.dealer_id AND ed.dealer_id = ds.dealer_id AND ed.schedule_id = ds.schedule_id AND ed.status_id = 1";
		$Rows_excusas_dealer = $DataBase_Acciones->SQL_SelectMultipleRows($query_Excusa_Dealer);

		for($i=0;$i<count($Rows_config);$i++) {
			$var_schedule_id = $Rows_config[$i]['schedule_id'];
			$var_dealer_id = $Rows_config[$i]['dealer_id'];
			for($y=0;$y<count($Rows_excusas_dealer);$y++) {
				if ( $var_dealer_id == $Rows_excusas_dealer[$y]['dealer_id'] ) {
					if($var_schedule_id == $Rows_excusas_dealer[$y]['schedule_id']){
						$Rows_config[$i]['excusas'] = $Rows_excusas_dealer[$y]['num_invitation'];
						break;
					}
				}
			}
		}

		return $Rows_config;
	}


	// ========================================================
    // Consulta los usuarios inscritos a cursos segun los meses seleccionados
    // ========================================================
    public static function getUsuariosInscritos( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$filtro3 = isset($_SESSION['max_rol'])&&$_SESSION['max_rol'] <= 4 ? " AND h.dealer_id = $dealer_id" : "";

        $query_sql = "SELECT u.first_name, u.last_name, u.identification, i.*, h.dealer_id, h.headquarter, a.area, z.zone, mru.score, mru.score_evaluation, mru.score_waybill, mru.approval, t.charge
			FROM ludus_invitation i, ludus_modules_results_usr mru, ludus_headquarters h, ludus_schedule s, ludus_areas a, ludus_zone z, ludus_users u LEFT JOIN
			(
			    SELECT cu.user_id, cu.charge_id, c.charge, MAX( cu.date_creation ) AS minimo FROM ludus_charges_users cu, ludus_charges c WHERE cu.charge_id = c.charge_id GROUP BY cu.user_id
			) as t ON u.user_id = t.user_id
			WHERE i.headquarter_id = h.headquarter_id
			AND i.schedule_id = s.schedule_id
			AND i.invitation_id = mru.invitation_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			AND i.user_id = u.user_id
			$filtro3
			AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			AND i.status_id > 1
			UNION
			SELECT u.first_name, u.last_name, u.identification, i.*, h.dealer_id, h.headquarter, a.area, z.zone, 0 AS score, 0 AS score_evaluation, 0 AS score_waybill, 'NO' AS approval, t.charge
			FROM ludus_invitation i, ludus_headquarters h, ludus_schedule s, ludus_areas a, ludus_zone z, ludus_users u LEFT JOIN
			(
			    SELECT cu.user_id, cu.charge_id, c.charge, MAX( cu.date_creation ) AS minimo FROM ludus_charges_users cu, ludus_charges c WHERE cu.charge_id = c.charge_id GROUP BY cu.user_id
			) as t ON u.user_id = t.user_id
			WHERE i.headquarter_id = h.headquarter_id
			AND i.schedule_id = s.schedule_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			AND i.user_id = u.user_id
			$filtro3
			AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			AND i.status_id = 1
			UNION
			SELECT u.first_name, u.last_name, u.identification, i.*, h.dealer_id, h.headquarter, a.area, z.zone, mru.score, mru.score_evaluation, mru.score_waybill, mru.approval, t.charge
				FROM ludus_invitation i, ludus_modules_results_usr_excuse mru, ludus_headquarters h, ludus_schedule s, ludus_areas a, ludus_zone z, ludus_excuses e, ludus_users u LEFT JOIN
				(
				    SELECT cu.user_id, cu.charge_id, c.charge, MAX( cu.date_creation ) AS minimo FROM ludus_charges_users cu, ludus_charges c WHERE cu.charge_id = c.charge_id GROUP BY cu.user_id
				) as t ON u.user_id = t.user_id
				WHERE i.headquarter_id = h.headquarter_id
				AND i.schedule_id = s.schedule_id
				AND i.invitation_id = mru.invitation_id
				AND mru.module_result_usr_id = e.module_result_usr_id
				AND h.area_id = a.area_id
				AND a.zone_id = z.zone_id
				AND i.user_id = u.user_id
				$filtro3
				AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
				AND e.status_id = 1
				AND i.status_id > 1";

		// echo $query_sql;
		// die();

        $resultSet = DB::query( $query_sql );


        return $resultSet;
    }// fin getUsuariosInscritos

	// ========================================================
    // Consulta los usuarios invitados a cursos sin asistencia registrada
    // ========================================================
    // public static function getUsuariosInvitados( $p, $prof="../" ){
    //     include_once($prof.'../config/init_db.php');
	//
    //     $query_sql = "SELECT u.first_name, u.last_name, u.identification, i.*, h.dealer_id, h.headquarter, a.area, z.zone, mru.score_evaluation, mru.score_waybill
	// 		FROM ludus_invitation i, ludus_modules_results_usr mru, ludus_headquarters h, ludus_schedule s, ludus_areas a, ludus_zone z, ludus_users u
	// 		WHERE i.headquarter_id = h.headquarter_id
	// 		AND i.schedule_id = s.schedule_id
	// 		AND i.invitation_id = mru.invitation_id
	// 		AND h.area_id = a.area_id
	// 		AND a.zone_id = z.zone_id
	// 		AND i.user_id = u.user_id
	// 		AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'";
	//
    //     $resultSet = DB::query( $query_sql );
	//
    //     return $resultSet;
    // }// fin getUsuariosInscritos

    // ========================================================
    // Consulta los cupos de cada sesion segun el concesionario
    // ========================================================
    public static function getSchedules( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		$filtro3 = isset($_SESSION['max_rol'])&&$_SESSION['max_rol'] <= 4 ? " AND d.dealer_id = $dealer_id" : "";

        $filtro =  isset( $p['course_id'] ) && $p['course_id'] != "0" ? " AND vs.course_id = ".$p['course_id'] : "";
        $filtro2 =  isset($p['filtro']) && $p['filtro'] == "CHEVROLET" ? " AND d.category = 'CHEVROLET' " : "";

        $query_sql = "SELECT vs.*, MONTH(vs.start_date) as mes, s.strategy
			FROM view_sesiones vs, ludus_dealers d, ludus_courses c LEFT JOIN ludus_strategy s ON c.strategy_id = s.strategy_id
			WHERE vs.dealer_id = d.dealer_id
			AND vs.course_id = c.course_id
			$filtro
			AND vs.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'
			$filtro3
			AND vs.max_size > 0
			$filtro2
			ORDER BY vs.start_date";

		// echo $query_sql;
		// die();

        $resultSet = DB::query( $query_sql );

        return $resultSet;
    }// fin getSchedules

    // ========================================================
    // Muestra el concesionario del usuario segun la fecha del curso
    // ========================================================
    public static function excusasUsuario( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');

        $query_sql = "SELECT e.*
			FROM ludus_excuses e, ludus_schedule s
			WHERE e.schedule_id = s.schedule_id
			AND e.status_id = 1
			AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'";
        // echo $query_sql;
        $resultSet = DB::query( $query_sql );

        return $resultSet;
    }// fin ccsHistoricoUsuario

    // ========================================================
    // Muestra el concesionario del usuario segun la fecha del curso
    // ========================================================
    public static function excusasCupos( $p, $prof="../" ){
        include_once($prof.'../config/init_db.php');
        $anio = date('Y');

        $query_sql = "SELECT ed.*
			FROM ludus_excuses_dealers ed, ludus_schedule s
			WHERE ed.schedule_id = s.schedule_id
			AND ed.status_id = 1
			AND s.start_date BETWEEN '{$p['start_date']} 00:00:00' AND '{$p['end_date']} 23:59:59'";
        // echo $query_sql;
        $resultSet = DB::query( $query_sql );

        return $resultSet;
    }// fin excusasAgrupadas

    // ========================================================
    // Retorna todos los registros agrupados
    // ========================================================
    public static function sabanaDatos( $p, $prof="../" ){
        $schedules = AsistenciaTotal::getSchedules( $p, $prof );
        $usuarios = AsistenciaTotal::getUsuariosInscritos( $p, $prof );
        $excusas = AsistenciaTotal::excusasUsuario( $p, $prof );
        $excusasCupos = AsistenciaTotal::excusasCupos( $p, $prof );

        $num_schedules = count( $schedules );
        $num_usuarios = count( $usuarios );
        $num_excusas = count( $excusas );
        $num_excusas_cupos = count( $excusasCupos );

        // ========================================================
        // Asigna la excusa según la fecha del curso
        // ========================================================
        for ($i=0; $i < $num_usuarios ; $i++) {
			$usuarios[$i]['excusa'] = '';
            // Agrega la excusa al usuario correspondiente
            for ($j=0; $j < $num_excusas ; $j++) {
            	if ( $usuarios[$i]['schedule_id'] == $excusas[$j]['schedule_id'] && $usuarios[$i]['user_id'] == $excusas[$j]['user_id'] ) {
					$usuarios[$i]['excusa'] = $excusas[$j]['file'];
					break;
            	}
            }
        }// fin for excusas/dealers usuarios

        // ========================================================
        // Agrupo los usuarios en su respectivo curso
        // ========================================================
        for ($i=0; $i < $num_schedules ; $i++) {

            $schedule_id = $schedules[$i]['schedule_id'];
            $dealer_id = $schedules[$i]['dealer_id'];
            $schedules[$i]['excusas'] = 0;
            $schedules[$i]['excusas_cupos'] = 0;
            $schedules[$i]['asistentes'] = 0;
            $schedules[$i]['file'] = '';
            // $schedules[$i]['reprobo'] = 0;
            // $schedules[$i]['promedio'] = 0;
            $schedules[$i]['inscritos'] = [];
            $prom_aux = 0;


			// Agrega la excusa al usuario correspondiente
            for ($j=0; $j < $num_excusas_cupos ; $j++) {
            	if ( $schedules[$i]['schedule_id'] == $excusasCupos[$j]['schedule_id'] && $schedules[$i]['dealer_id'] == $excusasCupos[$j]['dealer_id'] ) {
					$schedules[$i]['excusas_cupos'] = $excusasCupos[$j]['num_invitation'];
					$schedules[$i]['file'] = $excusasCupos[$j]['file'];
					break;
            	}
            }

            // Valida y asigna los usuarios de cada sesion
            foreach ( $usuarios as $key => $v ) {
                if ( $v['schedule_id'] == $schedule_id && $v['dealer_id'] == $dealer_id ) {

                    // cuenta las excusas
                    if ( $v['excusa'] != "" ) {
                        $schedules[$i]['excusas']++;
                    }
                    $schedules[$i]['inscritos'][] = $v;

                    // valida la asistencia
                    if ( $v['status_id'] == 2 ) {
                        $schedules[$i]['asistentes']++;
                    }
                }
            }// asigna los usuarios en su schedule correspondiente


        }// fin for schedules

        return $schedules;

    }// fin sabanaDatos

	// ========================================================
	// Genera un objeto relacional de los headquarters con dealers, zone, y areas
	// ========================================================
	public static function detalleSedes( $p, $prof = "../" ){
		include_once( $prof.'../config/init_db.php' );

		$headquarters = [];

		$query_sql = "SELECT h.headquarter_id, h.headquarter, d.dealer_id, d.dealer, a.area, z.zone
			FROM ludus_dealers d, ludus_headquarters h, ludus_areas a, ludus_zone z
			WHERE d.dealer_id = h.dealer_id
			AND h.area_id = a.area_id
			AND a.zone_id = z.zone_id
			AND d.category = '{$p['filtro']}'";
		$resultSet = DB::query( $query_sql );

		foreach ( $resultSet as $key => $value) {
			$headquarters[ $value['headquarter_id'] ] = $value;
		}

		return $headquarters;
	} // fin detalleSedes

}//fin clase
