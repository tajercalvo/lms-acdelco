<?php
Class Escalamientos {
	function consultaDatosEscalamientos($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT mu.sentinel_model_users_id, m. sentinel_model_id, m.sentinel_model, u.first_name, u.last_name, u.identification, u.user_id, mu.status_id, mu.date_creation, mu.date_edition
						FROM ludus_sentinel_model_users mu, ludus_users u, ludus_sentinel_model m
						WHERE mu.sentinel_model_id = m.sentinel_model_id AND 
						mu.user_id = u.user_id ";
			if($sWhere!=''){
				$query_sql .= " AND (m.sentinel_model LIKE '%$sWhere%' OR m.date_creation LIKE '%$sWhere%' OR m.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%' )";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT mu.sentinel_model_users_id, m. sentinel_model_id, m.sentinel_model, u.first_name, u.last_name, u.identification, u.user_id, mu.status_id, mu.date_creation, mu.date_edition
						FROM ludus_sentinel_model_users mu, ludus_users u, ludus_sentinel_model m
						WHERE mu.sentinel_model_id = m.sentinel_model_id AND 
						mu.user_id = u.user_id ";
			if($sWhere!=''){
				$query_sql .= " AND (m.sentinel_model LIKE '%$sWhere%' OR m.date_creation LIKE '%$sWhere%' OR m.date_edition LIKE '%$sWhere%' OR u.first_name LIKE '%$sWhere%' OR u.last_name LIKE '%$sWhere%' OR u.identification LIKE '%$sWhere%' )";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT mu.sentinel_model_users_id, m. sentinel_model_id, m.sentinel_model, u.first_name, u.last_name, u.identification, u.user_id, mu.status_id, mu.date_creation, mu.date_edition
						FROM ludus_sentinel_model_users mu, ludus_users u, ludus_sentinel_model m
						WHERE mu.sentinel_model_id = m.sentinel_model_id AND 
						mu.user_id = u.user_id ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function CrearEscalamientos($sentinel_model_id,$user_id){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_sentinel_model_users` (sentinel_model_id,user_id,status_id,creator,editor,date_creation,date_edition) VALUES ('$sentinel_model_id','$user_id','1','$idQuien','$idQuien','$NOW_data','$NOW_data')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarEscalamientos($id,$sentinel_model_id,$user_id,$status){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_escalamientos.php?opcn=ver&id=$id&back=inicio>$id</a>] a Escalamientos: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_sentinel_model_users` SET sentinel_model_id = '$sentinel_model_id', user_id = '$user_id', status_id = '$status', editor = '$idQuien', date_edition = '$NOW_data' WHERE sentinel_model_users_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_sentinel_model_users c
			WHERE c.sentinel_model_users_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function ListadoModelos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_sentinel_model c
			WHERE status_id = 1
			ORDER BY sentinel_model ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function listadosUsuarios(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT user_id, first_name, last_name FROM ludus_users WHERE user_id IN( SELECT user_id FROM `ludus_roles_users` WHERE rol_id = 11) ORDER BY first_name, last_name ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
}
