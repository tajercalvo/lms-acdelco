<?php
Class Encuestas {
	function consultaDatosEncuestas($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (2,4) ";
			if($sWhere!=''){
				$query_sql .= " AND (c.review LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' ) ";
			}
			$query_sql .= $sOrder;
			$query_sql .= ' '.$sLimit;
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidadFull($sWhere,$sOrder,$sLimit){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (2,4) ";
			if($sWhere!=''){
				$query_sql .= " AND (c.review LIKE '%$sWhere%' OR c.date_edition LIKE '%$sWhere%' OR s.first_name LIKE '%$sWhere%' OR s.last_name LIKE '%$sWhere%' OR c.code LIKE '%$sWhere%' ) ";
			}
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCantidad(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT c.*,s.first_name as nom_edita, s.last_name as ape_edita
			FROM ludus_reviews c, ludus_users s
			WHERE c.editor = s.user_id AND type IN (2,4)
			ORDER BY c.review ";//LIMIT 0,20
		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectCantidad($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function CrearEncuestas($review,$code,$type,$description){
		session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha creado la configuración Encuestas: $nombre en el listado: $listado','$NOW_data')";
		$resultado_in = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$insertSQL_EE = "INSERT INTO `ludus_reviews` (review,status_id,editor,date_edition,code,type,description) VALUES ('$review','1','$idQuien','$NOW_data','$code','$type','$description')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActualizarEncuestas($id,$review,$status,$code,$type,$description){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_encuestas.php?opcn=ver&id=$id&back=inicio>$id</a>] a Encuestas: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "UPDATE `ludus_reviews` SET review = '$review', status_id = '$status', editor = '$idQuien', date_edition = NOW(),code = '$code',type = '$type',description = '$description' WHERE review_id = '$id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function CrearRelacion($review_id,$Elementos,$tipo){
		@session_start();
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO ludus_reviews_".$tipo."s (review_id,".$tipo."_id) VALUES";
		$ctrl = 0;
		foreach ($Elementos as $iID => $elements_select) {
			if($ctrl==0){
				$insertSQL_EE .= " ('$review_id','$elements_select') ";
			}else{
				$insertSQL_EE .= " ,('$review_id','$elements_select') ";
			}
			$ctrl++;
		}
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function BorraEvaluacion_Rel($idRegistro){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		/*$insertSQL_EE = "INSERT INTO `erum_logacceso` (idquien,tipo,descripcion,fecha) VALUES ('$idQuien','2','Ha editado la configuración [<a href=op_encuestas.php?opcn=ver&id=$id&back=inicio>$id</a>] a Encuestas: $nombre en el listado: $listado con status: $status','$NOW_data')";
		$resultado_up = $DataBase_Log->SQL_Insert($insertSQL_EE);*/
		$updateSQL_ER = "DELETE FROM `ludus_reviews_courses` WHERE review_id = '$idRegistro'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		$updateSQL_ER = "DELETE FROM `ludus_reviews_charges` WHERE review_id = '$idRegistro'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		return $resultado;
	}
	function consultaRegistro($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT *
			FROM ludus_reviews c
			WHERE c.review_id = '$idRegistro' ";//LIMIT 0,20
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectRows($query_sql);
		if($Rows_config['type']=="4"){
			$query_sql = "SELECT c.course_id
						FROM ludus_reviews_courses c
						WHERE c.review_id = $idRegistro
						ORDER BY c.course_id";
			$DataBase_Class = new Database();
			$Rows_config_Extr = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
			$Rows_config['cursos'] = $Rows_config_Extr;
		}else{
			$query_sql = "SELECT c.charge_id
						FROM ludus_reviews_charges c
						WHERE c.review_id = $idRegistro
						ORDER BY c.charge_id";
			$DataBase_Class = new Database();
			$Rows_config_Extr = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
			$Rows_config['cargos'] = $Rows_config_Extr;
		}
		unset($DataBase_Acciones);
		return $Rows_config;
	}
	function consultaCursos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.course_id, c.course, c.code, c.newcode
						FROM ludus_courses c WHERE c.status_id = 1
						ORDER BY c.course";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaCargos($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT c.charge_id, c.charge
						FROM ludus_charges c WHERE c.status_id = 1
						ORDER BY c.charge";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaPreguntasSel($idRegistro){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$query_sql = "SELECT rq.*, q.question, q.type, u.first_name, u.last_name, q.code
						FROM ludus_reviews_questions rq, ludus_questions q, ludus_users u
						WHERE rq.review_id = '$idRegistro' AND rq.question_id = q.question_id AND rq.editor = u.user_id
						ORDER BY rq.date_edition ASC";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function consultaPreguntas($prof){
		if($prof=="js"){
			include_once('../../config/database.php');
			include_once('../../config/config.php');
		}else{
			include_once('../config/database.php');
			include_once('../config/config.php');
		}
		$query_sql = "SELECT p.question, p.question_id, p.type, p.code
						FROM ludus_questions p
						WHERE p.status_id = 1
						ORDER BY p.code";
		$DataBase_Class = new Database();
		$Rows_config = $DataBase_Class->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Class);
		return $Rows_config;
	}
	function AgrPregunta($question_id,$value,$review_id){
		@session_start();
		include('../config/database.php');
		include('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$insertSQL_EE = "INSERT INTO `ludus_reviews_questions` (review_id,question_id,value,editor,date_edition,status_id) VALUES ('$review_id','$question_id','$value','$idQuien','$NOW_data','1')";
		$resultado = $DataBase_Log->SQL_Insert($insertSQL_EE);
		unset($DataBase_Log);
		return $resultado;
	}
	function ActPregunta($reviews_questions_id,$status_id){
		@session_start();
		include('../config/database.php');
		include('../config/config.php');
		$idQuien = $_SESSION['idUsuario'];
		$DataBase_Log = new Database();
		$updateSQL_ER = "UPDATE `ludus_reviews_questions` SET status_id = '$status_id' WHERE reviews_questions_id = '$reviews_questions_id'";
		$resultado = $DataBase_Log->SQL_Update($updateSQL_ER);
		unset($DataBase_Log);
		return $resultado;
	}
}
