<?php
Class ResActividades {

	function getActividadPendinete($activity_schedule_id, $ajax = '' ){
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		include_once($ajax."../config/database.php");
		include_once($ajax."../config/config.php");
		$DataBase_Class = new Database();
		$query_sql = "SELECT aces.activity_schedule_id, moac.activity, moac.activity_id, moac.tipo, moac.link_video, aces.*, moac.adjuntos, mo.module, co.course, actus.resultado, actus.archivo, actus.comentarios, moac.instrucciones, moac.adjunto, moac.orden, inv.invitation_id,  more.module_result_usr_id, sch.teacher_id
							FROM ludus_activities_schedule aces
									INNER JOIN ludus_modules_activities moac
										ON aces.activity_id = moac.activity_id
									INNER JOIN ludus_schedule sch
										on aces.schedule_id = sch.schedule_id
									INNER JOIN ludus_modules mo
										on sch.module_id = mo.module_id
									INNER JOIN ludus_courses co
										on co.course_id = mo.course_id
									INNER JOIN ludus_invitation inv
										on inv.schedule_id = aces.schedule_id
											and inv.user_id = $user_id
									inner join ludus_modules_results_usr more
										on more.invitation_id = inv.invitation_id
									LEFT JOIN ludus_activities_user actus
										ON actus.activity_schedule_id = aces.activity_schedule_id
										and actus.user_id = $user_id
									where inv.user_id = $user_id
									    and aces.activity_schedule_id = $activity_schedule_id
									    -- and actus.resultado is null
									    and inv.status_id > 0
									    group by aces.activity_schedule_id;";
											
		$actividades = $DataBase_Class->SQL_SelectRows($query_sql);
		unset($DataBase_Class);
		return $actividades;
	}


	function responder_actividad($p)
	{
		extract($p);
		$archivo = isset($archivo) ? $archivo : "";
		@session_start();
		$user_id = $_SESSION['idUsuario'];
		include_once("../../config/database.php");
		include_once("../../config/config.php");
		$DataBase_Class = new Database();
		$ahora = date("Y-m-d") . " " . date("H") . ":" . date("i:s");

		$query_aux = "SELECT * from ludus_activities_user where user_id = $user_id and activity_schedule_id = $activity_schedule_id";
		$res_aux = $DataBase_Class->SQL_SelectMultipleRows($query_aux);

		$data = array();
		if (count($res_aux) == 0) {
			$query_sql = "INSERT INTO ludus_activities_user
							(
							user_id,
							activity_schedule_id,
							resultado,
							comentarios,
							archivo,
							date_edition,
							reviews_answer_id,
							module_result_usr_id,
							teacher_id,
							coments_teacher,
							date_teacher)
							VALUES
							(
							$user_id,
							$activity_schedule_id,
							0,
							'$comentarios',
							'$archivo',
							'$ahora',
							0,
							$module_result_usr_id,
							0,
							'',
							now())";
			$actividad = $DataBase_Class->SQL_Insert($query_sql);

			if ($actividad > 0) {
				$data['error'] = false;
				$data['msj'] = 'Actividad guardada correctamente';
			} else {
				$data['error'] = true;
				$data['msj'] = 'No se pudo guardar la actividad';
			}
		} else {
			if ($res_aux[0]['teacher_id'] > 0) {
				$data['error'] = true;
				$data['msj'] = "está actividad ya se encuentra calificada por el instructor";
			} else {
				$buscarActividad = ("SELECT
														activities_user_id
													FROM
														ludus_activities_user
													WHERE
														user_id = $user_id AND activity_schedule_id = $activity_schedule_id
													ORDER BY
													activities_user_id
													DESC
													LIMIT 1;");
				$response =		$DataBase_Class->SQL_SelectRows($buscarActividad);

				$query_up = "UPDATE ludus_activities_user
									SET comentarios = '$comentarios',
									archivo = '$archivo',
									date_edition = now()
									WHERE activities_user_id='{$response['activities_user_id']}'";
				$res_up = $DataBase_Class->SQL_Update($query_up);
				if ($res_up > 0) {
					$data['error'] = false;
					$data['msj'] = 'Actividad actualizada correctamente';
				} else {
					$data['error'] = true;
					$data['msj'] = 'No se pudo actualizar la actividad';
				}
			}
		}


		unset($DataBase_Class);
		return $data;;
	}	
}
