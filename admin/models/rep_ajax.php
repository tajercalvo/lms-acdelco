<?php
Class RepPendientesCursos {

	function consultaDatosAll($start_date,$end_date,$course_id){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		@session_start();
		$dealer_id = $_SESSION['dealer_id'];
		// Para seleccionar el primer id
		//$query = "SELECT DISTINCT c.course_id FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, (SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci, ludus_specialties spe, ludus_supplier su WHERE (s.start_date BETWEEN '2017-01-01 00:00:00' AND '2017-12-31 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id AND c.course_id = m.course_id AND l.city_id = ci.city_id AND c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id ORDER BY c.course_id asc limit 1";

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_sql = "SELECT DISTINCT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, (SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci, ludus_specialties spe, ludus_supplier su
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id
			AND c.course_id = m.course_id AND l.city_id = ci.city_id AND c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id ";
			if($course_id != '0'){$query_sql .= " AND c.course_id = ".$course_id;}//valida si se selecciono un curso en especifico
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20
			$query_sql_det = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, i.invitation_id, u1.identification, s.schedule_id, d.dealer, a.area, z.zone, d.dealer_id
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id
			AND h.headquarter_id = u1.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id ";
			if($course_id != '0'){$query_sql_det .= " AND c.course_id = ".$course_id;}//valida si se selecciono un curso en especifico
			$query_sql_det .= " ORDER BY h.headquarter";//LIMIT 0,20
		}else{
			$query_sql = "SELECT DISTINCT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, s.schedule_id, m.duration_time, ci.city, spe.specialty, su.supplier
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, (SELECT course_id, sum(duration_time) as duration_time FROM ludus_modules GROUP BY course_id) m, ludus_cities ci, ludus_specialties spe, ludus_supplier su
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id AND h.dealer_id = '$dealer_id'
			AND c.course_id = m.course_id AND l.city_id = ci.city_id AND c.supplier_id = su.supplier_id AND c.specialty_id = spe.specialty_id ";
			if($course_id != '0'){$query_sql .= " AND c.course_id = ".$course_id;}//valida si se selecciono un curso en especifico
			$query_sql .= " ORDER BY s.start_date";//LIMIT 0,20
			$query_sql_det = "SELECT c.image as image_course, c.newcode, c.course, c.type, s.start_date, s.end_date, l.living, l.address, u.first_name as first_prof, u.last_name as last_prof, u1.first_name, u1.last_name, i.date_send, u1.image, h.headquarter, h.bac, h.address1, i.status_id as estado, i.invitation_id, u1.identification, s.schedule_id, d.dealer, a.area, z.zone, d.dealer_id
			FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
			WHERE (s.start_date BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id
			AND h.headquarter_id = u1.headquarter_id AND h.dealer_id = '$dealer_id' AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id";
			if($course_id != '0'){$query_sql_det .= " AND c.course_id = ".$course_id;}//valida si se selecciono un curso en especifico
			$query_sql_det .= " ORDER BY h.headquarter";//LIMIT 0,20
		}

		$DataBase_Acciones = new Database();
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		$Rows_detail = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql_det);

		$query_charges = "SELECT u.identification, c.charge FROM ludus_users u, ludus_charges_users cu, ludus_charges c WHERE u.status_id = 1 AND u.user_id = cu.user_id AND
					cu.charge_id = c.charge_id";
		$Rows_Charges = $DataBase_Acciones->SQL_SelectMultipleRows($query_charges);

		$invitations = "1";
		for($y=0;$y<count($Rows_detail);$y++) {
			$invitations .= ",".$Rows_detail[$y]['invitation_id'];
		}
		$schedules = "0";
		for($y=0;$y<count($Rows_config);$y++) {
			$schedules .= ",".$Rows_config[$y]['schedule_id'];
		}

		$query_nota = "SELECT i.invitation_id, i.approval, i.score, i.score_review, i.score_waybill
						FROM `ludus_modules_results_usr` i
						WHERE i.status_id = 1 AND i.invitation_id IN ($invitations)";
						//echo $query_nota;
		$Rows_notas = $DataBase_Acciones->SQL_SelectMultipleRows($query_nota);

		if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>4){
			$query_asis = "SELECT s.schedule_id, d.dealer, s.min_size, s.max_size, s.inscriptions, d.dealer_id
			FROM ludus_dealer_schedule s, ludus_dealers d
			WHERE s.schedule_id IN ($schedules) AND s.dealer_id = d.dealer_id
			AND s.max_size > 0";
			$query_Excusas = "SELECT u.user_id, u.first_name, u.last_name, u.identification, h.headquarter, e.file, e.schedule_id, d.dealer, a.area, z.zone
				FROM ludus_excuses e, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
				WHERE e.schedule_id IN ($schedules) AND e.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id";
		}else{
			$query_asis = "SELECT s.schedule_id, d.dealer, s.min_size, s.max_size, s.inscriptions, d.dealer_id
			FROM ludus_dealer_schedule s, ludus_dealers d
			WHERE s.schedule_id IN ($schedules) AND s.dealer_id = d.dealer_id AND d.dealer_id = '$dealer_id'
			AND s.max_size > 0";
			$query_Excusas = "SELECT u.user_id, u.first_name, u.last_name, u.identification, h.headquarter, e.file, e.schedule_id, d.dealer, a.area, z.zone
				FROM ludus_excuses e, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z
				WHERE e.schedule_id IN ($schedules) AND e.user_id = u.user_id AND u.headquarter_id = h.headquarter_id
				AND h.dealer_id = '$dealer_id' AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id";
		}
		$Rows_asistencia = $DataBase_Acciones->SQL_SelectMultipleRows($query_asis);
		$Rows_excusas = $DataBase_Acciones->SQL_SelectMultipleRows($query_Excusas);


		//print_r($Rows_config);
		for($i=0;$i<count($Rows_config);$i++) {
			$var_schedule_id = $Rows_config[$i]['schedule_id'];
			for($y=0;$y<count($Rows_detail);$y++) {
				if($var_schedule_id == $Rows_detail[$y]['schedule_id']){
					//print_r($Rows_config[$i]['resultados']);
					$var_Invitation = $Rows_detail[$y]['invitation_id'];
					for($z=0;$z<count($Rows_notas);$z++) {
						if($var_Invitation == $Rows_notas[$z]['invitation_id']){
							//print_r($Rows_notas[$y]);
							$Rows_detail[$y]['resultados'][] = $Rows_notas[$z];
							//print_r($Rows_config[$i]['resultados']);
						}
					}
					$var_identification = $Rows_detail[$y]['identification'];
					for($l=0;$l<count($Rows_Charges);$l++){
						if($var_identification==$Rows_Charges[$l]['identification']){
							$Rows_detail[$y]['charges'][] = $Rows_Charges[$l];
						}
					}
					//print_r($Rows_detail[$y]);
					$Rows_config[$i]['detail'][] = $Rows_detail[$y];
				}
			}
			for($y=0;$y<count($Rows_excusas);$y++) {
				if($var_schedule_id == $Rows_excusas[$y]['schedule_id']){
					$var_identification = $Rows_excusas[$y]['identification'];
					for($l=0;$l<count($Rows_Charges);$l++){
						if($var_identification==$Rows_Charges[$l]['identification']){
							$Rows_excusas[$y]['charges'][] = $Rows_Charges[$l];
						}
					}
					$Rows_config[$i]['excusas'][] = $Rows_excusas[$y];
				}
			}
			for($y=0;$y<count($Rows_asistencia);$y++) {
				if($var_schedule_id == $Rows_asistencia[$y]['schedule_id']){
					$Rows_config[$i]['asistencia'][] = $Rows_asistencia[$y];
				}
			}
		}

		/*for($i=0;$i<count($Rows_config);$i++) {
			$query_Resultado = "SELECT i.approval, i.score
						FROM `ludus_modules_results_usr` i
						WHERE i.invitation_id = ".$Rows_config[$i]['invitation_id']." AND i.status_id = 1 ";
			$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);
			$Rows_config[$i]['resultados'] = $Rows_cant;
		}*/

		unset($DataBase_Acciones);
		return $Rows_config;
	}

	function consultaCursos_en_consultaDatosAll($start_date, $end_date){
		include_once('../../config/database.php');
		include_once('../../config/config.php');
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT distinct c.course_id FROM ludus_courses c, ludus_schedule s, ludus_livings l, ludus_users u, ludus_invitation i, ludus_users u1, ludus_headquarters h, ludus_dealers d, ludus_areas a, ludus_zone z WHERE (s.start_date BETWEEN '$start_date' AND '$end_date') AND c.course_id = s.course_id AND s.living_id = l.living_id AND s.teacher_id = u.user_id AND s.status_id = 1 AND s.schedule_id = i.schedule_id AND i.user_id = u1.user_id AND h.headquarter_id = u1.headquarter_id AND h.dealer_id = d.dealer_id AND h.area_id = a.area_id AND a.zone_id = z.zone_id ORDER BY c.course_id;";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	 function consultaCursos(){
		include_once('../config/database.php');
		include_once('../config/config.php');
		$DataBase_Acciones = new Database();
		$query_sql = "SELECT c.course_id, c.course FROM ludus_courses c";
		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
		unset($DataBase_Acciones);
		return $Rows_config;
	}

	// 	function usuarios(){
	// 	include_once('../../config/database.php');
	// 	include_once('../../config/config.php');
	// 	@session_start();
	// 	$dealer_id = $_SESSION['dealer_id'];
	// 	$query_sql = "SELECT * FROM ludus_users where identification = 1082889369";
	// 	$DataBase_Acciones = new Database();
	// 	$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
	// 	unset($DataBase_Acciones);
	// 	return $Rows_config;
	// }

	// 	function consultaDatos(){
// 		include_once('../config/database.php');
// 		include_once('../config/config.php');
// 		@session_start();
// 		$dealer_id = $_SESSION['dealer_id'];
// 		$query_sql = "SELECT DISTINCT c.course_id, c.course, c.newcode, c.type
// FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c
// WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id 
// AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score >= 75 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1)
// ORDER BY c.course";
// 		$DataBase_Acciones = new Database();
// 		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
// 		unset($DataBase_Acciones);
// 		return $Rows_config;
// 	}
// 	function consultaDatosAll($course_id){
// 		include_once('../../config/database.php');
// 		include_once('../../config/config.php');
// 		@session_start();
// 		$dealer_id = $_SESSION['dealer_id'];
// 		$query_sql = "SELECT DISTINCT c.newcode, c.type, c.course, u.user_id, u.first_name, u.last_name, u.identification, d.dealer, h.headquarter
// FROM ludus_charges_courses cc, ludus_charges_users cu, ludus_users u, ludus_courses c, ludus_headquarters h, ludus_dealers d
// WHERE cc.course_id = c.course_id AND cc.charge_id = cu.charge_id AND cu.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND cc.course_id = '$course_id' 
// AND cu.user_id NOT IN (SELECT mr.user_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c WHERE mr.score >= 75 AND mr.module_id = m.module_id AND m.course_id = c.course_id AND c.status_id = 1 AND c.course_id = '$course_id')
// ORDER BY d.dealer, u.first_name, u.last_name, c.course";
// //echo $query_sql;
// 		$DataBase_Acciones = new Database();
// 		$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);

// 		$query_Resultado = "SELECT cu.user_id, c.charge, c.charge_id
// 						FROM ludus_charges_users cu, ludus_charges c
// 						WHERE cu.charge_id = c.charge_id ";
// 		$Rows_cant = $DataBase_Acciones->SQL_SelectMultipleRows($query_Resultado);

// 		for($i=0;$i<count($Rows_config);$i++) {
// 			$user_id = $Rows_config[$i]['user_id'];
// 			//Cargos
// 				for($y=0;$y<count($Rows_cant);$y++) {
// 					if( $user_id == $Rows_cant[$y]['user_id'] ){
// 						$Rows_config[$i]['cargos'][] = $Rows_cant[$y];
// 					}
// 				}
// 			//Cargos
// 		}
// 		unset($DataBase_Acciones);
// 		return $Rows_config;
// 	}
}
