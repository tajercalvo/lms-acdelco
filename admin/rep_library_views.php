<?php include('src/seguridad.php'); ?>
<?php //include('../controllers/rep_asistencia.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<style type="text/css" media="screen">
	.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
  background-color: #ddd; 

}
a:hover {
    cursor: pointer;}
</style>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Reporte componentes vistos en la bibliotcea</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Biblioteca - Reporte de elementos vistos</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Consulta la asistencia del personal en el broadcast</h5></br>
			</div>
			<div class="col-md-2">
				<form action="ficheroExcel.php" method="post" id="FormularioExportacion">
					<h5 id="descargar" style="display: none;" ><a class="glyphicons cloud-upload"  ><i></i>Descargar</a><br></h5>
					<input type="hidden" id="datos_tabla" name="datos_tabla" />
					<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Reporte_Usuario" />
				</form>
			</div>
		</div>
		<div class="row">
			<form id="frm" >
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="library_id" style="padding-top:8px;">Sesión:</label>
					<div class="col-md-10">
						<select style="width: 100%;" id="library_id" name="library_id"></select>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
				<span id="consultando">

				</span>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información: </h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<!-- Table elements-->
		<table id="asistentes" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable table-hover" >
			<!-- Table heading -->
			<thead>
				<tr>

					<th data-hide="phone,tablet">IDENTIFICACIÓN</th>
					<th data-hide="phone,tablet">NOMBRE Y APELLIDO</th>
					<th data-hide="phone,tablet">CONCESIONARIO</th>
					<th data-hide="phone,tablet">SEDE</th>
					<th data-hide="phone,tablet">ZONA</th>
					<th data-hide="phone,tablet">CLICK EN PLAY</th>
					<th data-hide="phone,tablet">FINALIZÓ VIDEO</th>
				</tr>
			</thead>
			<!-- // Table heading END -->
			<tbody>

					<!-- <tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr> -->

			</tbody>
		</table>
		<!-- // Table elements END -->
		<!-- Total elements-->
		<!-- <div class="form-inline separator bottom small">
			Total de inscritos: <strong class='text-primary'>1</strong> | Invitados: <strong class='text-primary'>2</strong> | Asistieron: <strong class='text-success'>65</strong> | No Asitieron: <strong class='text-danger'>2424 </strong> | Aprobaron: <strong class='text-success'>2424</strong> | No Aprobaron: <strong class='text-danger'>22</strong>
		</div> -->
		<!-- // Total elements END -->
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_library_views.js"></script>
</body>
</html>