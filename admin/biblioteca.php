<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/biblioteca.php');
$location = 'biblioteca';
$script_select2v4 = 'select2v4';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/biblioteca.css?v=<?php echo md5(time())?>" media="screen" title="biblioteca">
</head>

<style type="text/css">
		
	</style>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php" >Biblioteca</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<center><h2 style="color: #0058a3;letter-spacing: 3px;font-weight: bold;">BIBLIOTECA</h2></center>
					<hr style="border: 0.8px solid #0058a3;">
					<audio id="audio">
						<source type="audio/mp3" src="../assets/Biblioteca/sonidolike/sound.mp3">
					</audio>
					<!--CONTENIDO BIBLIOTECA DETALLE-->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">
								<!-- <div class="col-md-1 detailsWrapper"></div> -->
								<div class="col-md-12 detailsWrapper">
									<div class="innerAll">
										<div class="body">
											<div class="row" id="menuPrincipal">
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<?php $_GET['specialty_id'] = isset($_GET['specialty_id']) ? $_GET['specialty_id']: 1 ; ?>
													<a class="<?php if($_GET['specialty_id']==1){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=1">Habilidades Blandas</a>
												</div>
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<a class="<?php if($_GET['specialty_id']==2){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=2">Técnico</a>
												</div>
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<a class="<?php if($_GET['specialty_id']==3){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=3">Ventas</a>
												</div>
												<div class="col-md-2 text-center">
													<a class="<?php if($_GET['specialty_id']==4){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=4">AutoTrain Marca</a>
												</div>
												<div class="col-md-2 text-center">
													<a class="<?php if($_GET['specialty_id']==5){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=5">Manual de Usuario</a>
												</div>
												<div class="col-md-2 text-center" style="border-bottom: 1px solid;">
													<div class="input-group">
														<div class="input-group-btn"><span><i class="fa fa-search"></i></span></div>
														 <form name="formulario"method="get"action="biblioteca.php">

              									
												             <input id="txtSearch" class="form-control" type="text" name="txtbuscar"value="" placeholder="Escribe tu búsqueda" style="border: none;">
              												<input type="submit" style="display: none" />

          											   </form>
													  	
													</div>
												</div>
												<div class="col-sm-1 text-center">
												</div>
											</div>
											<div class="clearfix"><br></div>
											<!-- Inicio cuadro Lo más visto -->
											<div class="row">
												<!--Cuadro configurado-->
												<div class="col-md-12">
														<div id="contenido">
															<ul>
																<?php foreach ($archivos as $key => $value) { ?>
																<?php  if ($value['file']) {
																	$name = $value['file'];
																	 $extraccion = strtolower(substr($name, -5));
																	 $array_ext = explode('.', $extraccion);
																	 //$extencion = '.'.$array_ext[1];
																	 $extencion = isset($array_ext[1]) ? $extencion = '.'.$array_ext[1] : null;
																	// echo $extencion;
																} ?>

																<?php  if ($extencion != '.mp4' and $extencion != '.jpg' and $extencion != '.jpeg') {?>
																	

																	<li>
																		<div class="contenedor">
																		<a onclick="view(<?php echo $value['library_id']?>);" href="../assets/Biblioteca/<?php echo $value['file']?>" target="_blank"><img src="../assets/Biblioteca/m/<?php echo $value['image']?>"></a>
																			<div class="texto-encima"><?php echo $value['name']?></div>
																		</div>
																		<div style="text-align: center;border-top: 1px solid lightgray;color: gray;">
																			<span onclick="like(<?php echo $value['library_id']?>);" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i> <span id="like_<?php echo $value['library_id']?>"><?php echo $value['likes']?></span></span>
																			<span class="views"><i class="fa fa-eye" aria-hidden="true"></i> <span id="views_<?php echo $value['library_id']?>"><?php echo $value['views']?></span></span>
																		</div>

																	</li>

																<?php } else {?>

																	<li>
																      <div class="contenedor">
																		<a onclick="view(<?php echo $value['library_id']?>);" href="../assets/Biblioteca/<?php echo $value['file']?>" target="_blank"><img src="../assets/Biblioteca/m/<?php echo $value['image']?>"></a>
																			<div class="texto-encima"><?php echo $value['name']?></div>
																		</div>
																		<div style="text-align: center;border-top: 1px solid lightgray;color: gray;">
																			<span onclick="like(<?php echo $value['library_id']?>);" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i> <span id="like_<?php echo $value['library_id']?>"><?php echo $value['likes']?></span></span>
																			<span class="views"><i class="fa fa-eye" aria-hidden="true"></i> <span id="views_<?php echo $value['library_id']?>"><?php echo $value['views']?></span></span>
																		</div>
																	</li>
																	<?php }?>
																<?php }?>
															</ul>
														</div>
													</div>
												</div>
											<!-- Fin cuadro lo mas visto -->
											<!-- Fin cuadro lo mas visto -->
												<div class="row">
												<!--Cuadro configurado-->
												<div class="col-md-12">
														<div id="contenido2">
															
															    <!--viene por ajax-->
														     
														</div>
													</div>
												</div>
											<!-- Fin cuadro lo mas visto -->

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--CONTENIDO BIBLIOTECA DETALLE-->

						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- // Sidebar menu & content wrapper END -->
				<?php include('src/footer.php'); ?>
			</div>
			<!-- // Main Container Fluid END -->
			<?php include('src/global.php'); ?>
			<script src="js/biblioteca.js?v=<?php echo md5(time()) ?>"></script>
		</body>
		</html><?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/biblioteca.php');
$location = 'biblioteca';
$script_select2v4 = 'select2v4';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/biblioteca.css?v=<?php echo md5(time())?>" media="screen" title="biblioteca">
</head>

<style type="text/css">
		
	</style>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php" >Biblioteca</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<center><h2 style="color: #0058a3;letter-spacing: 3px;font-weight: bold;">BIBLIOTECA</h2></center>
					<hr style="border: 0.8px solid #0058a3;">
					<audio id="audio">
						<source type="audio/mp3" src="../assets/Biblioteca/sonidolike/sound.mp3">
					</audio>
					<!--CONTENIDO BIBLIOTECA DETALLE-->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">
								<!-- <div class="col-md-1 detailsWrapper"></div> -->
								<div class="col-md-12 detailsWrapper">
									<div class="innerAll">
										<div class="body">
											<div class="row" id="menuPrincipal">
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<?php $_GET['specialty_id'] = isset($_GET['specialty_id']) ? $_GET['specialty_id']: 1 ; ?>
													<a class="<?php if($_GET['specialty_id']==1){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=1">Habilidades Blandas</a>
												</div>
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<a class="<?php if($_GET['specialty_id']==2){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=2">Técnico</a>
												</div>
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<a class="<?php if($_GET['specialty_id']==3){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=3">Ventas</a>
												</div>
												<div class="col-md-2 text-center">
													<a class="<?php if($_GET['specialty_id']==4){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=4">AutoTrain Marca</a>
												</div>
												<div class="col-md-2 text-center">
													<a class="<?php if($_GET['specialty_id']==5){echo "active";} ?> btn btn-colorgm col-md-12" href="biblioteca.php?token=<?php echo md5(time()); ?>&specialty_id=5">Manual de Usuario</a>
												</div>
												<div class="col-md-2 text-center" style="border-bottom: 1px solid;">
													<div class="input-group">
														<div class="input-group-btn"><span><i class="fa fa-search"></i></span></div>
														 <form name="formulario"method="get"action="biblioteca.php">

              									
												             <input id="txtSearch" class="form-control" type="text" name="txtbuscar"value="" placeholder="Escribe tu búsqueda" style="border: none;">
              												<input type="submit" style="display: none" />

          											   </form>
													  	
													</div>
												</div>
												<div class="col-sm-1 text-center">
												</div>
											</div>
											<div class="clearfix"><br></div>
											<!-- Inicio cuadro Lo más visto -->
											<div class="row">
												<!--Cuadro configurado-->
												<div class="col-md-12">
														<div id="contenido">
															<ul>
																<?php foreach ($archivos as $key => $value) { ?>
																<?php  if ($value['file']) {
																	$name = $value['file'];
																	 $extraccion = strtolower(substr($name, -5));
																	 $array_ext = explode('.', $extraccion);
																	 //$extencion = '.'.$array_ext[1];
																	 $extencion = isset($array_ext[1]) ? $extencion = '.'.$array_ext[1] : null;
																	// echo $extencion;
																} ?>

																<?php  if ($extencion != '.mp4' and $extencion != '.jpg' and $extencion != '.jpeg') {?>
																	

																	<li>
																		<div class="contenedor">
																		<a onclick="view(<?php echo $value['library_id']?>);" href="<?php echo $value['file']?>" target="_blank"><img src="../assets/Biblioteca/m/<?php echo $value['image']?>"></a>
																			<div class="texto-encima"><?php echo $value['name']?></div>
																		</div>
																		<div style="text-align: center;border-top: 1px solid lightgray;color: gray;">
																			<span onclick="like(<?php echo $value['library_id']?>);" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i> <span id="like_<?php echo $value['library_id']?>"><?php echo $value['likes']?></span></span>
																			<span class="views"><i class="fa fa-eye" aria-hidden="true"></i> <span id="views_<?php echo $value['library_id']?>"><?php echo $value['views']?></span></span>
																		</div>

																	</li>

																<?php } else {?>

																	<li>
																      <div class="contenedor">
																		<a onclick="view(<?php echo $value['library_id']?>);" href="../assets/Biblioteca/<?php echo $value['file']?>" target="_blank"><img src="../assets/Biblioteca/m/<?php echo $value['image']?>"></a>
																			<div class="texto-encima"><?php echo $value['name']?></div>
																		</div>
																		<div style="text-align: center;border-top: 1px solid lightgray;color: gray;">
																			<span onclick="like(<?php echo $value['library_id']?>);" class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i> <span id="like_<?php echo $value['library_id']?>"><?php echo $value['likes']?></span></span>
																			<span class="views"><i class="fa fa-eye" aria-hidden="true"></i> <span id="views_<?php echo $value['library_id']?>"><?php echo $value['views']?></span></span>
																		</div>
																	</li>
																	<?php }?>
																<?php }?>
															</ul>
														</div>
													</div>
												</div>
											<!-- Fin cuadro lo mas visto -->
											<!-- Fin cuadro lo mas visto -->
												<div class="row">
												<!--Cuadro configurado-->
												<div class="col-md-12">
														<div id="contenido2">
															
															    <!--viene por ajax-->
														     
														</div>
													</div>
												</div>
											<!-- Fin cuadro lo mas visto -->

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--CONTENIDO BIBLIOTECA DETALLE-->

						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- // Sidebar menu & content wrapper END -->
				<?php include('src/footer.php'); ?>
			</div>
			<!-- // Main Container Fluid END -->
			<?php include('src/global.php'); ?>
			<script src="js/biblioteca.js?v=<?php echo md5(time()) ?>"></script>
		</body>
		</html>