<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/marca_chevrolet.php');
$location = 'tutor';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="marca_chevrolet.php" class="glyphicons cars"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Marca - Chevrolet</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- Sección club de líderes -->


					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">
								<div class="col-md-1 detailsWrapper">
								</div>
								<div class="col-md-10 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
									<div class="innerAll">
										<div class="body">
											<div class="row padding">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-1 center">
														</div>
														<div class="col-md-10 center">
															<img align="center" src="../assets/gallery/marca_chevrolet/banner_marca.jpg" style="width:100%;" alt="" class="img-responsive padding-none border-none" />
														</div>
														<div class="col-md-1 center">
														</div>
													</div>
													<div class="separator bottom"></div>
													<div class="row">
														<div class="col-md-1 center">
														</div>
														<div class="col-md-10 center">
															<!--<video id="vid1" src="" class="video-js vjs-default-skin" controls preload="auto" width="100%" height="450">
																<source src="../assets/gallery/marca_chevrolet/marcaChevrolet.mov" type='video/mov'>
															</video>-->
															<div class="col-md-6" style="text-align: left;">
																	<i class="fa fa-fw fa-thumbs-o-up"></i>
																	<span id="CantLikes"><?php echo($Cant_like[0]['cantidad']); ?></span>
																	<i class="fa fa-fw fa-eye"></i>
																	<span id="CantViews"><?php echo($Cant_view[0]['cantidad']); ?></span>

															</div>
															<div class="col-md-6" style="text-align: right;">

															<a href="#" data-toggle="modal" onclick="LikeGalery(); return false;">
															<//?php echo($Data_Gal['media_detail_id']); ?>

																	<i class="fa fa-fw fa-thumbs-o-up"></i> Me gusta
																</a>
															</div>
															<video controls="controls" width="100%" height="600" name="Marca Chevrolet" id="video"  src="../assets/gallery/marca_chevrolet/marcaChevrolet.mov"></video>
															<!--<input type="button" name="name" value="video visto" onclick="ViewGalery(); return false;">-->
														</div>
														<div class="col-md-1 center">
														</div>
												</div>

												<div class="separator bottom"></div>
												<div class="row">
													<div class="col-md-12">
														<!-- About -->
														<div class="widget widget-heading-simple widget-body-white margin-none">
															<div class="widget-head"><h4 class="heading glyphicons picture" style="color:#0058a3 !important"><i></i>Galerías de Imágenes</h4><?php if($_SESSION['max_rol']>=6){ ?><span style='float: right;'><a href="#ModalEditar" data-toggle="modal"><i class="fa fa-fw fa-pencil-square"></i> Editar Galerías </a></span><?php } ?></div>
																<div class="widget-body">
																	<div class="widget-gallery" data-toggle="collapse-widget">
																		<!-- Gallery Layout -->
																		<div class="gallery gallery-2">
																			<ul class="row">
																				<?php foreach ($Galeria_datos as $key => $Data_Gal) { ?>
																					<li class="col-md-2">
																						<a style="height: 101px;max-height: 101px;overflow: hidden;background-color: black;" class="thumb no-ajaxify" href="fotografias_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>&club=marca_chevrolet">
																							<img src="../assets/gallery/thumbs/<?php echo($Data_Gal['source']); ?>" title="<?php echo($Data_Gal['media'].' - '.$Data_Gal['description']); ?>" alt="<?php echo($Data_Gal['media'].' - '.$Data_Gal['description']); ?>" class="img-responsive" />
																						</a>
																					</li>
																					<?php } ?>
																				</ul>
																			</div>
																			<!-- // Gallery Layout END -->
																		</div>
																	</div>
																</div>
																<!-- // About END -->
															</div>
														</div>

														<div class="separator bottom"></div>
														<div class="row">
															<div class="col-md-12">
																<!-- About -->
																<div class="widget widget-heading-simple widget-body-white margin-none">
																	<div class="widget-head"><h4 class="heading glyphicons film" style="color:#0058a3 !important"><i></i>Galería de Videos</h4><?php if($_SESSION['max_rol']>=6){ ?><span style='float: right;'><a href="#ModalEditar" data-toggle="modal"><i class="fa fa-fw fa-pencil-square"></i> Editar Galerías </a></span><?php } ?></div>
																		<div class="widget-body">
																			<div class="widget-gallery" data-toggle="collapse-widget">
																				<!-- Gallery Layout -->
																				<div class="gallery gallery-2">
																					<ul class="row">
																						<?php foreach ($Galeria_datosv as $key => $Data_Gal) { ?>
																							<li class="col-md-2">
																								<a style="height: 101px;max-height: 101px;overflow: hidden;background-color: black;" class="thumb no-ajaxify" href="videos_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>&club=marca_chevrolet">
																									<img src="../assets/gallery/thumbs/<?php echo($Data_Gal['source']); ?>" title="<?php echo($Data_Gal['media'].' - '.$Data_Gal['description']); ?>" alt="<?php echo($Data_Gal['media'].' - '.$Data_Gal['description']); ?>" class="img-responsive" />
																								</a>
																							</li>
																							<?php } ?>
																						</ul>
																					</div>
																					<!-- // Gallery Layout END -->
																				</div>
																			</div>
																		</div>
																		<!-- // About END -->
																	</div>
																</div>
															</div>
														</div>
														<div class="separator bottom"></div>
														<div class="row">
															<div class="col-md-12">
																<div class="widget widget-heading-simple widget-body-white margin-none">
																	<div class="widget-head"><h4 class="heading glyphicons book" style="color:#0058a3; !important"><i></i>Material de Apoyo </h4><?php if($_SESSION['max_rol']>=6){ ?><span style='float: right;'><a href="#ModalApoyo" data-toggle="modal"><i class="fa fa-fw fa-plus-square"></i> Adjuntar Material</a></span><?php } ?></div>
																		<div class="widget-body">
																			<div class="widget-gallery" data-toggle="collapse-widget">
																				<div class="row">
																					<?php foreach($Galeria_pdf as $file){ ?>
																						<?php if($file['url']!=""){ ?>
																							<div class="block block-inline col-md-2">
																								<div class="box-generic">
																									<div class="timeline-top-info" style="text-align: center;">
																										<?php if($_SESSION['max_rol']>=6){ ?>
																											<a href="marca_chevrolet.php?TockenProcess=<?php echo(md5('GMACADEMY')); ?>&opcn=eliminar&id=<?php echo($file['marca_file_id']);?>" class="text-danger">Eliminar</a></br>
																										<?php } ?>
																										<a href="<?php echo $file['url']; ?>" target="_blank" title="<?php echo $file['title']; ?>"><img src="../assets/gallery/marca_chevrolet/url.png" style="width:50px;height:50px;"/></a><br>
																										<i class="fa fa-user"></i> <a href="<?php echo $file['url']; ?>" target="_blank" title="<?php echo $file['title']; ?>"><?php echo($file['first_name']); ?>: <?php echo($file['title']); ?>.</a>
																										<div class="timeline-bottom">
																											<i class="fa fa-clock-o"></i> <?php echo($file['date_creation']); ?>
																										</div>
																									</div>
																									<div class="content-filled"></div>
																								</div>
																							</div>
																						<?php }else{?>
																							<div class="block block-inline col-md-2">
																								<div class="box-generic">
																									<div class="timeline-top-info" style="text-align: center;">
																										<?php if($_SESSION['max_rol']>=6){ ?>
																											<a href="marca_chevrolet.php?TockenProcess=<?php echo(md5('GMACADEMY')); ?>&opcn=eliminar&id=<?php echo($file['marca_file_id']);?>&file=<?php echo $file['file_name']; ?>" class="text-danger">Eliminar</a></br>
																										<?php } ?>
																										<a href="../assets/gallery/marca_chevrolet/<?php echo $file['file_name']; ?>" target="_blank" title="<?php echo $file['title']; ?>"><img src="../assets/gallery/marca_chevrolet/file.png" style="width:50px;height:50px;"/></a><br>
																										<i class="fa fa-user"></i> <a href="../assets/gallery/marca_chevrolet/<?php echo $file['file_name']; ?>" target="_blank" title="<?php echo $file['title']; ?>"><?php echo($file['first_name']); ?>: <?php echo($file['title']); ?>.</a>
																										<div class="timeline-bottom">
																											<i class="fa fa-clock-o"></i> <?php echo($file['date_creation']); ?>
																										</div>
																									</div>
																									<div class="content-filled"></div>
																								</div>
																							</div>
																						<?php }?>
																					<?php }//fin foreach ?>
																				</div>
																			</div>
																		</div>
																	</div>
																	<!-- // Fin Material de apoyo -->
																		<div class="separator bottom"></div>
																		<div class="row">
																			<div class="col-md-3 center"></div>
																			<div class="col-md-6 center">

																				<!-- // Juan Carlos Villar - 17/Ago/2016- Inicio fila redes sociales -->
																				<table align=center>
																					<tr>
																						<td width="40px"><a target="_blank" href="https://www.facebook.com/chevroletcolombia"> <img src="../assets/gallery/marca_chevrolet/facebook.png" style="width:30px" title="Facebook" ></a></td>
																						<td width="40px"><a target="_blank" href="http://www.twitter.com/chevroletco"> <img src="../assets/gallery/marca_chevrolet/twitter.png" style="width:30px" title="Twitter" ></a></td>
																						<td width="40px"><a target="_blank" href="https://plus.google.com/u/0/+chevroletcolombia/posts"> <img src="../assets/gallery/marca_chevrolet/google_plus.png" style="width:30px" title="Google+" ></a></td>
																						<td width="40px"><a target="_blank" href="http://www.youtube.com/chevroletcolombia"> <img src="../assets/gallery/marca_chevrolet/youtube.png" style="width:30px" title="Youtube" ></a></td>
																						<td width="40px"><a target="_blank" href="https://instagram.com/chevroletco"> <img src="../assets/gallery/marca_chevrolet/instagram.png" style="width:30px" title="Instagram" ></a></td>
																						<td width="40px"><a target="_blank" href="http://pinterest.com/chevroletco"> <img src="../assets/gallery/marca_chevrolet/pinterest.png" style="width:30px" title="Pinterest" ></a></td>
																					</tr>
																				</table>

																				<!-- // Juan Carlos Villar - 17/Ago/2016- Fin fila redes sociales -->

																			</a>
																		</div>
																		<div class="col-md-3 center">
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- Aqui va la modal para editar las galerías-->
									<!-- Modal -->
									<form action="marca_chevrolet.php" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
										<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														<h4 class="modal-title">Editar las galerías de la marca Chevrolet</h4>
													</div>
													<div class="modal-body">
														<div class="widget widget-heading-simple widget-body-gray">
															<div class="widget-body">
																<!-- Row -->
																<div class="row">
																	<div class="col-md-1">
																	</div>
																	<div class="col-md-10">
																		<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> Galerías </h5>
																		<select multiple="multiple" style="width: 100%;" id="galerias" name="galerias[]">
																			<?php foreach ($Galerias_disponibles as $key => $Data_Galerias) {
																				$selected = "";
																				if(isset($Galeria_datos) && count($Galeria_datos>0) ){
																					foreach ($Galeria_datos as $key => $Data_GalImagen) {
																						if($Data_Galerias['media_id'] == $Data_GalImagen['media_id']){
																							$selected = "selected";
																						}
																					}
																				}
																				if(isset($Galeria_datosv) && count($Galeria_datosv>0) ){
																					foreach ($Galeria_datosv as $key => $Data_GalVideo) {
																						if($Data_Galerias['media_id'] == $Data_GalVideo['media_id']){
																							$selected = "selected";
																						}
																					}
																				}
																				$tipo = 'Videos';
																				if($Data_Galerias['type']=="1"){
																					$tipo = 'Imágenes';
																				}
																				?>
																				<option value="<?php echo $Data_Galerias['media_id']; ?>" <?php echo $selected; ?>><?php echo $tipo.' | '.$Data_Galerias['media']; ?></option>
																				<?php } ?>
																			</select>
																		</div>
																		<div class="col-md-1">
																		</div>
																	</div>
																	<!-- Row END-->
																	<div class="clearfix"><br></div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<input type="hidden" value="editarGaleria" name="opcn" id="opcn" />
															<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
														</div>
													</div>
												</div>
											</div>
									</form>
<!--
		Modal para cargar los archivos adjuntos correspondientes para
		Material de apoyo
	-->
	<form action="marca_chevrolet.php" enctype="multipart/form-data" method="post" id="form_MaterialApoyo"><br><br>
		<div class="modal fade" id="ModalApoyo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Cargar Material de apoyo</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label" for="tittle_file">Titulo del archivo</label>
										<input type="text" id="tittle_file" name="tittle_file" class="form-control col-md-8" placeholder="Titulo del material" value="<?php //echo libro cargado; ?>" />
									</div>
									<div class="col-md-5">
										<label class="control-label" for="url_mat">URL del material</label>
										<input type="text" id="url_mat" name="url_mat" class="form-control col-md-8" placeholder="URL del material" value="<?php //echo libro cargado; ?>" />
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo (PDF,ZIP,Imágen)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_new" name="pdf_new" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row -->
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="opcn" value="pdfApoyo">
						<button type="submit" id="BtnApoyo" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- Fon Modal Material de apoyo-->
										<!-- Sección club de líderes -->
									</div>
								</div>
								<!-- // Content END -->

							</div>



							<div class="clearfix"></div>
							<!-- // Sidebar menu & content wrapper END -->
							<?php include('src/footer.php'); ?>
						</div>
						<!-- // Main Container Fluid END -->
						<?php include('src/global.php'); ?>
						<script src="js/marca_chevrolet.js"></script>
					</body>
					</html>
