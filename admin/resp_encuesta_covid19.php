<?php include('src/seguridad.php'); ?>
<?php
$location = 'resp_encuesta_covid19';
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<head><meta charset="gb18030">
		<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

</head>

<style type="text/css">
.margendiv{
	max-height: 169px;
	min-height: 127px;
    min-width: 322px;
    max-width: 710px;
    border: 1px solid;
    border-radius: 24px;
    margin: 12px;3
    padding: 24px;
    padding-top: 22px;
}
input:focus {
  border: 1px solid #ab2b3e !important;
  
}
.ptext{
	text-align: initial;
	font-size: 17px;
	font-weight: 400;
	color: #202124;
	font-family: -webkit-body;
}
</style>
<body class="document-body ">

	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted est¨¢ en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asischat_vct.php">Ecuesta Covid19</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
						<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Encuesta Covid19</h2>
						          <div class="clearfix"></div>
					</div>

					<div class="widget widget-heading-simple widget-body-white">
							
							<!-- // Widget heading END -->
						<div class="widget-body">
									<div class="row">
										 <div class="col-md-12" style="text-align: center;">
										 	<img src="https://autotrain.com.co/AG/img/LogoAutoTrain-1.png" alt="AutoTrain" style="width: 40%">
										 </div>									</div>
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h4 class="heading glyphicons list"><i></i> Información: <span id="num_asistentes" style="color: #bd272c;font-weight: revert;"></span> </h4>
										</div>
										<!-- // Widget heading END -->
										<div class="widget-body">

									<!-- grafica dona---->
							              <div class="row" style="margin-bottom: 20px">            
							                 <!-- graficas dona1---->
							                   <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase"><p>Motivo de salida</p></div>
							                            <div id="dona1" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> Trabajo Cat</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #538fc1;font-size: 20px;"></i> Trabajo Taller de Servicio</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #83efef;font-size: 20px;"></i> Trabajo Concesionario</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #f5dd88;font-size: 20px;"></i> Live Store</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #480505;font-size: 20px;"></i>  Trabajo Publicidad</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #a1ca04;font-size: 20px;"></i>  Trabajo en Sede AutoTrain</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> Otros</li>
							                       </div>
							                 <!-- graficas dona2---->
							                 	 <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>Medio de transporte</p></div>
							                            <div id="dona2" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> Carro propio</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> Moto</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #538fc1;font-size: 20px;"></i> Bicicleta</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #83efef;font-size: 20px;"></i> Servicio publico</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #f5dd88;font-size: 20px;"></i> a pie</li>
							                     
							                       </div>

							                 </div>

							                 <!-- grafica dona---->
							                <div class="row" style="margin-bottom: 20px">            
							                 <!-- graficas dona3---->
							                   <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>1.¿Ha sido diagnosticado con enfermedad por COVID-19 en los últimos 15 días?</p></div>
							                            <div id="dona3" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       
							                       </div>
							                 <!-- graficas dona4---->
							                 	 <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>2. ¿En los últimos 7 días ha estado usted en contacto con alguna persona de su entorno familiar o laboral con diagnóstico de COVID-19?</p></div>
							                            <div id="dona4" style="cursor: pointer;"> </div>

							                       </div>
							                    
							                       	<div class="col-md-2" style="margin-top: 123px;">
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       </div>

							                 </div>
							                 <!-- grafica dona 5 y 6---->
							                <div class="row" style="margin-bottom: 20px">            
							                 <!-- graficas dona5---->
							                   <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>3.¿En los últimos 7 días ha estado usted en contacto con alguna persona de su entorno familiar o laboral con fiebre, tos, dificultad respiratoria, diarrea o dolor de estómago?</p></div>
							                            <div id="dona5" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       
							                       </div>
							                 <!-- graficas dona6---->
							                 	 <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>4. ¿Ha tenido fiebre en los últimos 7 días?</p></div>
							                            <div id="dona6" style="cursor: pointer;"> </div>

							                       </div>
							                    
							                       	<div class="col-md-2" style="margin-top: 123px;">
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       </div>

							                 </div>
							                  <!-- grafica dona 7 y 8---->
							                <div class="row" style="margin-bottom: 20px">            
							                 <!-- graficas dona6---->
							                   <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>5. ¿Ha tenido tos en los últimos 7 días? </p></div>
							                            <div id="dona7" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       
							                       </div>
							                 <!-- graficas dona7---->
							                 	 <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>6. ¿Ha tenido dificultad para respirar en los últimos 7 días?</p></div>
							                            <div id="dona8" style="cursor: pointer;"> </div>

							                       </div>
							                    
							                       	<div class="col-md-2" style="margin-top: 123px;">
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       </div>

							                 </div>
							                  <!-- grafica dona 9 y 10---->
							                <div class="row" style="margin-bottom: 20px">            
							                 <!-- graficas dona6---->
							                   <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>7. Ha tenido dolor muscular o fatiga en los últimos 7 días? </p></div>
							                            <div id="dona9" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       
							                       </div>
							                 <!-- graficas dona7---->
							                 	 <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>8. ¿Ha tenido dolor en el pecho en los últimos 7 días?</p></div>
							                            <div id="dona10" style="cursor: pointer;"> </div>

							                       </div>
							                    
							                       	<div class="col-md-2" style="margin-top: 123px;">
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       </div>

							                 </div>

							                  <!-- grafica dona 11 y 12---->
							                <div class="row" style="margin-bottom: 20px">            
							                 <!-- graficas dona6---->
							                   <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>9. ¿Ha tenido dolor de garganta en los últimos 7 días? </p></div>
							                            <div id="dona11" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       
							                       </div>
							                 <!-- graficas dona7---->
							                 	 <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>10. ¿Ha tenido diarrea en los últimos 7 días? </p></div>
							                            <div id="dona12" style="cursor: pointer;"> </div>

							                       </div>
							                    
							                       	<div class="col-md-2" style="margin-top: 123px;">
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       </div>

							                 </div>

							                   <!-- grafica dona 13 y 14---->
							                <div class="row" style="margin-bottom: 20px">            
							                 <!-- graficas dona6---->
							                   <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>11. ¿Ha experimentado pérdida del olfato o el gusto en los últimos 7 días?  </p></div>
							                            <div id="dona13" style="cursor: pointer;"> </div>

							                       </div>
							                       <div class="col-md-2" style="margin-top: 123px;">
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
							                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       
							                       </div>
							                 <!-- graficas dona7---->
							                 	 <div class="col-md-4" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>12. ¿Conozco los riesgos asociados a la exposición al virus del COVID-19 como son; muerte, síndrome de dificultad respiratoria del adulto, coagulopatías, eventos trombóticos severos, alteraciones gastrointestinales, alteraciones neurológicas, y otras, que pueden aumentar la probabilidad de complicar enfermedades pre existentes, así como las probables secuelas derivadas del padecimiento de la enfermedad? </p></div>
							                            <div id="dona14" style="cursor: pointer;"> </div>

							                       </div>
							                    
							                       	<div class="col-md-2" style="margin-top: 123px;">
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color:#42dabe;font-size: 20px ;"></i> SI</li>
								                       	<li style="list-style:none"><i class="fa fa-circle" aria-hidden="true" style="color: #d25f5f;font-size: 20px;"></i> NO</li>
							                       </div>

							                 </div>
							                 <!-- grafica 15 -->
							                      
							                <div class="row" style="margin-bottom: 20px">            
							                   <div class="col-md-1"></div>
							                 <!-- graficas dona15---->
							                 	 <div class="col-md-10" style="background: white">
							                            <div class="tiles-title dona2" style="text-align:center; text-transform: uppercase">
							                            	<p>Certifico que he leído detenidamente el formulario y he sido informado de las medidas generales de bioseguridad para la prevención del Covid 19. Las entiendo y estoy comprometido a cumplirlas y  a reportar algún resultado positivo mío o de mi familia o cualquier situación de importancia para evitar la propagación del virus. </p></div>
							                            <div id="dona15" style="cursor: pointer;"> </div>

							                       </div>
							                    <div class="col-md-1"></div>
							                 </div>


									 <!---- Tabla---->
											<div class="row">
												<div col-md-12>
													<table id="tabla_encuesta" class="table table-hover" >
														<caption>Resultado Encuesta COVID-19</caption>
														<thead>
															<tr>
																<th>ID</th>
																<th>Usuario</th>
																<th>Motivo de la Salida</th>
																<th>Si su respuesta fué otro, especifique</th>
																<th>Indique el lugar donde se desarrollará la actividad ( Dirección )</th>
																<th>Medio de transporte a utilizar*</th>
																<th>Nombre y teléfono de contacto en caso de emergencia</th>
															    <th>1.¿Ha sido diagnosticado con enfermedad por COVID-19 en los últimos 15 días?</th>
																<th>2. ¿En los últimos 7 días ha estado usted en contacto con alguna persona de su entorno familiar o laboral con diagnóstico de COVID-19?</th>
																<th>3.¿En los últimos 7 días ha estado usted en contacto con alguna persona de su entorno familiar o laboral con fiebre, tos, dificultad respiratoria, diarrea o dolor de estómago?</th>
																<th>4. ¿Ha tenido fiebre en los últimos 7 días?</th>
																<th>5. ¿Ha tenido tos en los últimos 7 días?</th>
																<th>6. ¿Ha tenido dificultad para respirar en los últimos 7 días?</th>
																<th>7. Ha tenido dolor muscular o fatiga en los últimos 7 días? </th>
																<th>8. ¿Ha tenido dolor en el pecho en los últimos 7 días? </th>
																<th>9. ¿Ha tenido dolor de garganta en los últimos 7 días?</th>
																<th>10. ¿Ha tenido diarrea en los últimos 7 días?</th>
																<th>11. ¿Ha experimentado pérdida del olfato o el gusto en los últimos 7 días?</th>
																<th>12. ¿Conozco los riesgos asociados a la exposición al virus del COVID-19?</th>
																<th>13.Certifico que he leído detenidamente el formulario y he sido informado de las medidas generales de bioseguridad para la prevención del Covid 19</th> 
																<th>Fecha en que realizó la encuesta</th>
															</tr>
														</thead>
														<tbody></tbody>
													</table>
												</div>
											</div>
											
										</div>
									</div>
						</div><!-- // widget-body-->
					</div><!-- // widget-->

						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
						<!-- // END contenido interno -->
			    </div>
			<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js" type="text/javascript" ></script>

    <script src="js/resp_encuesta_covid19.js"></script>

	</body>
</html>