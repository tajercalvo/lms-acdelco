<?php include('src/seguridad.php'); ?>
<?php include('controllers/vct_detail2.php');
$location = 'home';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>

	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/vct_detail2.css" media="screen" title="biblioteca">
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->

	<audio preload>
		<source src="../assets/Biblioteca/sonido_chat.mp3" type="audio/mpeg" />
		Tu navegador no soporta esta caracteristica
		</audio>
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content" >
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons briefcase"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/cursos.php">Detalle Cursos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<br>
						<h2 class="margin-none pull-left"><i><img src="../assets/images/webcam.png" alt="webcam" width="20px"/></i> Detalle del Curso </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="btn-group pull-right">
							<h5><a href="index.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<!-- <div id="contenido_interno" style="visibility: hidden;"> -->
					<div id="contenido_interno" style="visibility: visible;">
<!-- row -->
	<div class="row ">
		<!-- col -->
		<?php //if ( isset($DetallesProgramacion['end_date']) and $DetallesProgramacion['end_date'] > $ahora ) {
			if ( false ) {?>
			<div class="col-md-8">
				<!-- Media -->
				<div class="widget innerAll">
					<div id="screen-subscriber"></div>
					<?php if($_SESSION['idUsuario']==$DetallesProgramacion['teacher_id']){?>
						<input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo($DetallesProgramacion['teacher_id']); ?>">
						<!-- <button onclick="javascript:screenshare();" disabled id="shareBtn" class="btn btn-success"><i class="fa fa-fw fa-desktop"></i>Compartir</button> -->
						<!--<button onclick="javascript:startArchive();" disabled id="StartBTN" class="btn btn-success"><i class="fa fa-fw fa-save"></i>Guardar</button>
						<button onclick="javascript:stopArchive();" disabled id="StopBTN" class="btn btn-danger"><i class="fa fa-fw fa-stop"></i>Detener</button>-->
					<?php }?>
					<?php if($_SESSION['idUsuario']==$DetallesProgramacion['teacher_id']){?>
						<div id="camera-publisher" style="z-index: 9999; width: 200px; height: 150px; !important"></div>
					<?php }?>
					<div id="camera-subscriber" style="z-index: 9999;"></div>
				</div>
				<!-- End Media -->
				<div class="widget innerAll">
					<div class="owl-carousel owl-theme">
					<?php if( isset( $archivosVCT ) && count( $archivosVCT ) > 0 ){
						foreach( $archivosVCT as $value => $filesvct ){ ?>
						<div>
							<figure>
								<a data-toggle="modal" href="#myModal" onclick="DatosVideo('<?php echo('../assets/gallery/videosVCT/'.$filesvct['file_name']); ?>','<?php echo($filesvct['file_name']); ?>')">
									<img src="../assets/gallery/miniatura.jpg" alt="prueba">
									<figcaption><?php echo($filesvct['file_name']); ?></figcaption>
								</a>
							</figure>
						</div>
						<?php }//fin foreach
					}//fin if isset ?>
					</div>
				</div>
			</div> <!-- Fin div class col md 8 -->
		<?php }else{ ?>
			<div class="col-md-8">
				<div id="diapositiva_actual">
					<!-- ajax -->
				</div>
				<div class="widget innerAll">
					<div id="screen-subscriber"></div>
					<?php if($_SESSION['idUsuario']==$DetallesProgramacion['teacher_id']){?>
						<input type="hidden" name="teacher_id" id="teacher_id" value="<?php echo($DetallesProgramacion['teacher_id']); ?>">
						<button onclick="javascript:screenshare();" id="shareBtn" class="btn btn-success"><i class="fa fa-fw fa-desktop"></i>Compartir</button>
						<!--<button onclick="javascript:startArchive();" disabled id="StartBTN" class="btn btn-success"><i class="fa fa-fw fa-save"></i>Guardar</button>
						<button onclick="javascript:stopArchive();" disabled id="StopBTN" class="btn btn-danger"><i class="fa fa-fw fa-stop"></i>Detener</button>-->
					<?php }?>
					<?php if($_SESSION['idUsuario']==$DetallesProgramacion['teacher_id']){?>
						<div id="camera-publisher" style="z-index: 9999; width: 200px; height: 150px; !important"></div>
					<?php }?>
					<div id="camera-subscriber" style="z-index: 9999;"></div>
				</div>
				<!-- End Media div ue contiene el compartir pantala -->
				<!-- <div class="widget innerAll">
					<div class="owl-carousel owl-theme">
					<?php if( isset( $archivosVCT ) && count( $archivosVCT ) > 0 ){
						foreach( $archivosVCT as $value => $filesvct ){ ?>
						<div>
							<figure>
								<a data-toggle="modal" href="#myModal" onclick="DatosVideo('<?php echo('../assets/gallery/videosVCT/'.$filesvct['file_name']); ?>','<?php echo($filesvct['file_name']); ?>')">
									<img src="../assets/gallery/miniatura.jpg" alt="prueba">
									<figcaption><?php echo($filesvct['file_name']); ?></figcaption>
								</a>
							</figure>
						</div>
						<?php }//fin foreach
					}//fin if isset ?>
					</div>
				</div> -->

			</div>
		<?php } ?>
		<!-- // END col -->

		<!-- col -->
		<!-- tab cursos virtuales -->
		<div class="col-md-4"   id ="tab_cursos">
			<div class="widget padding-none">
				<div class="widget-body padding-none">
					<div class="relativeWrap">
						<div class="widget widget-tabs widget-tabs-simple widget-tabs-gray">

							<!-- Tabs Heading -->
							<div class="widget-head">
								<ul>
									<li class="active"><a class="glyphicons notes_2" href="#infoCurso" data-toggle="tab"><i></i><span>Información</span></a></li>
									<li id="list_chat"><a class="glyphicons conversation" href="#chat" data-toggle="tab"><i></i><span>Comunicación</span></a></li>
									<li><a class="glyphicons group" href="#invitados" data-toggle="tab"><i></i><span>Participantes</span></a></li>
								</ul>
							</div>
							<!-- // Tabs Heading END -->
							<!-- Contenido Tab lateral -->
							<div class="widget-body">
								<div class="tab-content">
									<!-- Tab content -->
									<div id="infoCurso" class="tab-pane active widget-body-regular">
										<h5>Información del curso</h5>
										<div class="row">
											<div class="innerLR text-center">
												<span class="innerAll text-center text-white"><img src="../assets/images/distinciones/<?php echo($registroConfiguracionDetail['image']);?>" style="width: 200px;" alt="<?php echo($registroConfiguracionDetail['course']); ?>"></span>
											</div>
									  </div>
										<br>
									  	<div class="media-body innerLR">
									    	<div class="innerLR">
									    	<input type="hidden" name="sessionId" id="sessionId" value="<?php if(isset($DetallesProgramacion['session_id'])){ echo($DetallesProgramacion['session_id']); }?>">
									    	<input type="hidden" name="token" id="token" value="<?php if(isset($DetallesProgramacion['token'])){ echo($DetallesProgramacion['token']); }?>">
									    		<h2 class="media-heading margin-none text-primary"><?php echo($registroConfiguracionDetail['course']); ?></h2>
													<br>
									    		<h5>Descripción: <span class="text-warning"><?php echo(ucfirst(strtolower($registroConfiguracionDetail['description']))); ?></span></h5>
									    		<h5>Notas: <span class="text-warning"><?php echo(ucfirst(strtolower($registroConfiguracionDetail['prerequisites']))); ?></span></h5>
									    		<!-- <h5>Autor: <span class="text-warning"><?php echo($registroConfiguracionDetail['supplier']); ?></span></h5> -->
													<h5><input type="hidden" id="fecha_hora" value="<?php echo($horarioCurso['start_date']); ?>"></h5>
													<h5>Tiempo para inscribirse: <span id="mostrarTiempo" class="text-warning"></span><h5>
												<h5>Profesor: <span class="text-warning"><?php echo($DetallesProgramacion['first_name'].' '.$DetallesProgramacion['last_name']);?></span></h5>
												<img src="../assets/images/usuarios/<?php echo($DetallesProgramacion['image']);?>" style="width: 100px;" alt="<?php echo($DetallesProgramacion['first_name']); ?>">
									    		<div class="btn-group pull-right" id="EspacioBoton">
													<?php if(($_SESSION['idUsuario'] == $DetallesProgramacion['teacher_id'] || $_SESSION['max_rol'] == 6 ) && $DetallesProgramacion['curso_cerrado'] == 'no'){?>
														<button class="btn btn-success" id="btn_terminar_curso"><i class="fa fa-check-circle"></i> Terminar Curso</button>
													<?php }?>
													<!-- <a href="" onclick="return false;" class="btn btn-success"><i class="fa fa-fw fa-floppy-o"></i> Ya estás inscrito!</a> -->
									    		</div>
									    	</div> <!-- End class="innerLR" -->
									  	</div> <!-- End div class="media-body innerLR" -->
									</div> <!-- end div id="infoCurso" -->
									<!-- // Tab content END -->

									<!-- Tab content -->
									<div id="invitados" class="tab-pane widget-body-regular">
										<h5 class="text-primary">Participantes en el VCT: <?php echo($cantidadRegistrados); ?></h5>
										<div class="row">
											<div class="innerAll">
												<form autocomplete="off" class="form-inline margin-none">
													<div class="input-group input-group-sm">
														<input type="hidden" name="schedule_id" id="schedule_id" value="<?php echo($vct); ?>">
														<input type="text" class="form-control" placeholder="busca un participante.." name="buscarUsuario" id="buscarUsuario"/>
														<input type="hidden" name="idsearch" value="<?php echo($id); ?>" id="idsearch">
														<span class="input-group-btn">
															<button type="button" class="btn btn-primary btn-xs pull-right"><i class="fa fa-search"></i></button>
														</span>
													</div>
												</form>
											</div>
										</div>
										<div class="row">
											<div class="innerAll">
												<div class="col-md-12 listWrapper"  style="overflow-y:scroll;height:400px;width:100%;">
													<div class="row" id="asistentes">

														<?php foreach ($registrados as $key => $value) { ?>
															<div class="col-md-6 padding-none" id="<?php echo($value['inscription_id']); ?>">
																<div class="media innerAll padding-none">
																	<a href="ver_perfil.php?back=usuarios&id=<?php echo($value['user_id']); ?>" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/<?php echo($value['image']); ?>" alt="Image" style="width: 50px;"/></a>
																	<div class="media-body">
																		<span class="strong text-primary"><?php echo(ucwords(strtolower($value['first_name']." ".$value['last_name']))); ?></span><br/>
																		<small class="text-italic text-primary-light"></i><?php echo($value['dealer']); ?></small><br/>
																		<small><?php echo($value['headquarter']); ?></small>
																	</div>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div> <!-- end div class row -->
									</div> <!-- end div id="invitados" -->
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div id="chat" class="tab-pane widget-body-regular padding-none">
										<div class="row">
											<div class="innerAll">

												<div class="col-md-12 detailsWrapper">
												<div id="cargando_mensajes"> </div>

													<ul id="cajaComentarios" class="" style="background:url('../assets/chat.jpg'); overflow-y: scroll;min-height:400px;height:300px;width: 100%; padding: 2%; ">
														<!-- Comentarios-->

														<?php if (isset($comentarios)){
															foreach ($comentarios as $keyCom => $comValue) { ?>
																<li class="row row-merge border-none" id="<?php echo($comValue['comments_id']); ?>">
																	<div class="col-md-12">
																		<div class="innerAll padding-none">
																			<div class="media">
																				<!--<a href="ver_perfil.php?back=usuarios&id=<?php echo($comValue['user_id']); ?>" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/<?php echo($comValue['image']); ?>" alt="Image" style="width: 50px;"/></a>-->
																				<div class="media-body">
																					<small class="pull-right text-primary-light"><i class="fa fa-calendar fa fa-fixed-width"></i> <?php echo($comValue['date_comment']); ?><em></em></small>
																					<strong class="text-primary"><?php echo(ucwords(strtolower($comValue['first_name']." ".$comValue['last_name']))); ?></strong><br/>
																					<small><?php echo($comValue['comment_text']); ?></small>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
															<?php } // Fin foreach
														}// FIN if
														 ?>
													</ul>
														<!-- Fin comentario -->
														<!-- separador de dia -->
														<!--<span href="" class="load"><i class="fa fa-calendar innerR third"></i>Today</span>-->
														<!-- separador de dia -->
													<!-- Campo de texto-->
													<div class="innerAll padding-none border border-top">
														<?php if( ( isset($inscritoCourse) && ($inscritoCourse) ) || $DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario'] || $_SESSION['max_rol']>=5){ ?>
														<form role="form" id="comentario">
															<input type="hidden" name="opcn" value="newComment">
															<input type="hidden" name="idvct" value="<?php echo($_GET['schedule']); ?>" id="idvct">
															<div class="form-group">
																<textarea placeholder="Escribe un mensaje.." class="form-control" rows="2" name="texto_comentario" id="texto_comentario"></textarea>
															</div>
															<div class="form-group">
																<div class="text-right">
																	<!-- <button type="button" class="btn btn-primary" id="botonChat">enviar <i class="fa-fw fa fa-envelope"></i></button> -->
																</div>

																<div class="text-left">

																	<b>IMPORTANTE:</b> Si tienes inconveniente con el video o el audio ten en cuenta lo siguiente:<br>
																	1. Tienes una conexión a internet estable.<br>
																	2. Estas en el tiempo del curso (Ni antes, ni después).<br>
																	3. Navegador Web recomendado: Google Chrome.<br>
																	4. Si presentas problemas o se congela la información, refresca la pantalla con las teclas CTRL+F5 o CTRL+R (Windows), CMD+R (mac).
																</div>
															</div>
														</form>
														<?php }else{?>
															<input type="hidden" name="idvct" value="<?php echo($_GET['schedule']); ?>" id="idvct">
														<?php }?>
													</div>
													<!-- Fin campo de texto -->
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
								</div>
							</div>
							<!-- Fin contenido tab lateral -->
						</div><!-- Fin widget widget-tabs widget-tabs-double-2 widget-tabs-gray -->
					</div><!-- Fin relativeWrap-->
				</div>
				<br><br>
			</div>
		</div>
	</div>
</div>
	<!-- // END row -->
						<div class="separator bottom"></div>
						<!-- <div class="separator bottom"></div> -->

				<div class="row" id="row_lista_diapositivas" style="visibility: hidden;">
					<!-- <div class="row" id="row_lista_diapositivas" style="visibility: visible;"> -->
					<div class="col-md-12" style="height: 200px">
						<div class="scrollmenu" id="lista_diapositivas">
							<!-- COntenido Modificable por Javascript -->
						</div>
					</div>
				</div>
				<div class="row" id="opcn_diapositivas" style="visibility: hidden;">
					<div class="col-md-1">
						<a id="cargar_lista_diapostivas" href=""><span class="fa fa-refresh label label-success editar_archivos"> Actualizar diapositivas</span></a>
						<!-- <a id="cargar_lista_diapostivas" href=""><h3 class="margin-none separator bottom"><i class="fa fa-refresh text-primary icon-fixed-width"></i> Actualizar lista de diapositivas</h3></a> -->
					</div>
					<div class="col-md-1">
						<a id="editar_lista_diapositivas" href="" target="_blank"><span class="fa fa-pencil label label-success editar_archivos"> Editar diapositivas</span></a>
						<!-- <a id="editar_lista_diapositivas" href="" target="_blank"><h3 class="margin-none separator bottom"><i class="fa fa-refresh text-primary icon-fixed-width"></i> Editar lista de diapositivas</h3></a> -->
					</div>
					<div class="col-md-10">

					</div>
				</div>
						<!-- Nuevo ROW-->
						<div class="row row-app"></div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
	</div>
	</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="//static.opentok.com/v2/js/opentok.js"></script>
	<script src="js/vct_detail2.js?varRand=<?php echo(rand(10,999999));?>"></script>
	<script src="js/vct_detail_diapositivas.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
