<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['dealer_id'])){
    include('controllers/rep_acumulado.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_AcumuladoNotas.xls");
if(isset($_GET['dealer_id'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
                <tr>
                    <th data-hide="phone,tablet" style="width: 10%;"> <?php echo utf8_decode("CÓDIGO"); ?></th>
                    <th data-hide="phone,tablet" style="width: 30%;">CURSO</th>
                    <th data-hide="phone,tablet" style="width: 5%;">USUARIOS</th>
                    <th data-hide="phone,tablet" style="width: 30%;">TRAYECTORIAS</th>
                    <th data-hide="phone,tablet" style="width: 5%;">PROMEDIO</th>
                    <th data-hide="phone,tablet" style="width: 5%;">MAXIMA</th>
                    <th data-hide="phone,tablet" style="width: 5%;">MININMA</th>
                </tr>
            </thead>
            <!-- // Table heading END -->
            <tbody>
                <?php 
                if($cantidad_datos > 0){ 
                    foreach ($datosCursos_all as $iID => $data) { 
                        ?>
                    <tr>
                        <td><?php echo(utf8_decode($data['newcode'])); ?></td>
                        <td><?php echo(utf8_decode($data['course'])); ?></td>
                        <td><?php echo($data['usr_pres']); ?></td>
                        <td><?php 
                            foreach ($data['cargos'] as $iID => $data_c) {
                                echo(utf8_decode($data_c['charge']." - "));
                            } 
                         ?></td>
                        <td align="center"><?php echo(number_format($data['avg_score'],0)); ?></td>
                        <td><?php echo($data['max_score']); ?></td>
                        <td><?php echo($data['min_score']); ?></td>
                    </tr>
                <?php } } ?>
            </tbody>
    </table>
<?php } ?>