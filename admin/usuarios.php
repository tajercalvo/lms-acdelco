<?php include('src/seguridad.php'); ?>
<?php include('controllers/usuarios_all.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/usuarios.php">Administración de Usuarios</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Usuarios </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-9">
									<h5 style="text-align: justify; ">A continuación encontrará la información de los usuarios cargados al sistema, cuando desee profundizar en la información de alguno simplemente de click en ver, si tiene los permisos necesarios podrá realizar modificaciones en los registros o completar la información.</h5><br>
									<h5><a href="usuarios_excel.php" class="glyphicons no-js download_alt" ><i></i>Descargar</a><h5>
								</div>
								<div class="col-md-1">
								</div>
								<div class="col-md-2">
									<?php if($_SESSION['max_rol']>=3){ ?><h5><a href="op_usuarios.php?opcn=nuevo&back=usuarios" class="glyphicons no-js circle_plus" ><i></i>Agregar Usuario</a></h5><?php } ?><br>
									<?php if($_SESSION['max_rol']>5){ ?><h5><a href="#ModalActivacion" data-toggle="modal" class="glyphicons no-js electricity" ><i></i>Activación Masiva</a></h5><?php } ?><br>
									<?php
										$coordinador = "";
										foreach ($_SESSION['id_rol'] as $key => $valueRol ) {
											if( $valueRol['rol_id'] == 4  ){
												$coordinador = "si";
												break;
											}
										}
										if($coordinador == "si" || $_SESSION['id_rol'] >= 5){ ?>
											<h5><a href="#ModalCarga" data-toggle="modal" class="glyphicons no-js file_import" ><i></i>Carga Masiva</a></h5>
										<?php } ?>
											<a href="#ModalResultado" data-toggle="modal" class="hidden" name="btn_resultado" id="btn_resultado">Resultado</a>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Administrar Usuarios</h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de registros: <?php echo($cantidad_datos); ?><br>
							</div>
							<!-- // Total elements END -->
							<!-- Table elements-->
							<table id="TableData" class="display table table-responsive swipe-horizontal table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-class="expand">ID</th>
										<th data-class="expand">FOTO</th>
										<th data-hide="phone,tablet">APELLIDOS</th>
										<th data-hide="phone,tablet">NOMBRES</th>
										<th data-hide="phone,tablet">TRAYECTORIA</th>
										<th data-hide="phone,tablet">EMPRESA</th>
										<th data-hide="phone,tablet">COMPAÑIA</th>
										<th data-hide="phone,tablet">SEDE</th>
										<th data-hide="phone,tablet">EMAIL</th>
										<th data-hide="phone,tablet">TELÉFONO</th>
										<th data-hide="phone,tablet">CUMPLEAÑOS</th>
										<th data-hide="phone,tablet">GÉNERO</th>
										<th data-hide="phone,tablet">ESTADO</th>
										<th data-hide="phone,tablet"> - </th>
									</tr>
								</thead>
								<!-- // Table heading END -->
							</table>
							<!-- // Table elements END -->
						</div>
					</div>
					<!-- Nuevo ROW-->
					<div class="row row-app">
						<p class="separator text-center"><i class="icon-filter icon-2x"></i></p>
						<!-- Filters -->
						<form action="usuarios.php" method="post">
							<input type="hidden" id="opcn" name="opcn" value="filtrar">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-3">
											<label class="control-label" for="charge_id">Trayectoria:</label>
												<select style="width: 100%;" id="charge_id" name="charge_id">
													<option value="0">Todas las trayectorias...</option>
													<?php foreach ($datosCargos_ini as $key => $Data_Cargos) { ?>
														<option value="<?php echo($Data_Cargos['charge_id']); ?>" ><?php echo($Data_Cargos['charge']); ?></option>
													<?php } ?>
												</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="rol_id">Perfil:</label>
												<select style="width: 100%;" id="rol_id" name="rol_id">
													<option value="0">Todos los perfiles...</option>
													<?php foreach ($datosRoles_ini as $key => $Data_Roles) { ?>
														<option value="<?php echo($Data_Roles['rol_id']); ?>" ><?php echo($Data_Roles['rol']); ?></option>
													<?php } ?>
												</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="status_id">Estado:</label>
											<select style="width: 100%;" id="status_id" name="status_id">
												<option value="0">Todos...</option>
												<option value="1">Activos</option>
												<option value="2">Inactivos</option>
											</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="headquarter_id">Sede Empresa</label>
												<select style="width: 100%;" id="headquarter_id" name="headquarter_id">
													<option value="0">Todas las empresas...</option>
													<?php foreach ($datosConcesionarios_ini as $key => $Data_Concesionario) { ?>
														<option value="<?php echo($Data_Concesionario['headquarter_id']); ?>" ><?php echo($Data_Concesionario['headquarter']); ?></option>
													<?php } ?>
												</select>
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-3">
											<?php if($_SESSION['max_rol']>5){?>
												<label class="control-label" for="dealer_id">Empresa:</label>
													<select style="width: 100%;" id="dealer_id" name="dealer_id">
														<option value="0">Todas las empresas...</option>
														<?php foreach ($datosConce_Gral as $key => $Data_Cargos) { ?>
															<option value="<?php echo($Data_Cargos['dealer_id']); ?>" <?php if(isset($_SESSION['dealer_id_usr_flr'])&&$_SESSION['dealer_id_usr_flr']==$Data_Cargos['dealer_id']){ ?>selected="selected" <?php } ?> ><?php echo($Data_Cargos['dealer']); ?></option>
														<?php } ?>
													</select>
											<?php } ?>
										</div>

										<div class="col-md-3">
											<label class="control-label" for="no_time_ev">permite evaluación</label>
											<select style="width: 100%;" id="no_time_ev" name="no_time_ev" <?php if($_SESSION['max_rol']<3){ ?>readonly="readonly"<?php } ?> >
												<option value="0">Sin filtro...</option>
												<option value="1" <?php if( isset( $_SESSION['no_time_ev_usr_flr'] ) && $_SESSION['no_time_ev_usr_flr'] == 1 ){ echo "selected"; } ?> >Permiso evaluación</option>
											</select>
										</div>
										
										<div class="col-md-3">
											 <label class="control-label" for="type_user_id">Agrupación:</label>
												<select style="width: 100%;" id="type_user_id" name="type_user_id" <?php if($_SESSION['max_rol']<3){ ?>readonly="readonly"<?php } ?>>
													<option value="0">Todas las agrupaciones...</option>
													<?php foreach ($type_user as $key => $value){ ?>
														<option value="<?php echo($value['type_user_id']); ?>" <?php if(isset($_SESSION['type_user_id_usr_flr'])&&$_SESSION['type_user_id_usr_flr']==$value['type_user_id']){ ?>selected="selected" <?php } ?> > <?php echo($value['type_user']); ?></option>
													<?php } ?>
												</select> 
										</div>
										
										<div class="col-md-3" style="text-align: right;">
											<button type="submit" class="btn btn-primary">Buscar <i class="icon-search"></i></button>
										</div>
									</div>
									<!-- Row END-->
								</div>
							</div>
						</form>
						<!-- // Filters END -->
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>


		<?php if($_SESSION['max_rol']>5){ ?>
			<!-- Modal -->
			<form action="usuarios.php" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
				<div class="modal fade" id="ModalActivacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Activación/Inactivación Masiva de Usuarios</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-5">
												<label class="control-label">Acción: </label>
												<select style="width: 100%;" id="action" name="action" >
													<option value="Inactivar">Inactivar</option>
													<option value="Activar">Activar</option>
												</select>
											</div>
											<div class="col-md-5">
												<input type="hidden" id="opcn" name="opcn" value="ejecutar">
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Listado de identificaciones separadas por coma: </label>
												<textarea id="identification_list" name="identification_list" class="col-md-12 form-control" rows="5"></textarea>
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" id="BtnEjecutar" class="btn btn-primary">Ejecutar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- /.modal -->
		<?php } ?>

		<?php if( $coordinador == "si" || $_SESSION['max_rol'] >= 5 ){ ?>
			<!-- Modal -->
			<form action="usuarios.php" enctype="multipart/form-data" method="post" id="form_CargarCSV"><br><br>
				<div class="modal fade" id="ModalCarga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" id="cerrarForm" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Carga Masiva de Usuarios</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<!-- Row -->
										
										<div class="row">
											<div class="col-md-1">
											</div>
											<!-- <div class="col-md-10">
												<input type="hidden" id="opcn" name="opcn" value="cargar">
												<label class="control-label" for="headquarter_id">Sede Empresa</label>
													<select style="width: 100%;" id="cargar_headquarter" name="cargar_headquarter">
														<?php foreach ($datosConcesionarios_ini as $key => $Data_Concesionario) {
															if($Data_Concesionario['dealer_id'] == $_SESSION['dealer_id']){?>
															<option value="<?php echo($Data_Concesionario['headquarter_id']); ?>" ><?php echo($Data_Concesionario['headquarter']); ?></option>
														<?php }
														}?>
													</select>
											</div> -->
											<div class="col-md-1">
											</div>
										</div>
									
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Adjuntar un archivo csv: </label>
												<input type="file" name="archivo_csv" id="archivo_csv">
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row END-->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<label for="" class="control-label"><a href="usuarios_ajax.php?opcion=plantilla">Descargar plantilla</a></label>
											</div>
											<div class="col-md-1"></div>
										</div>
										<!-- -->
										<div class="clearfix"><br></div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" id="BtnEjecutar" class="btn btn-primary">Ejecutar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- /.modal -->
		<?php } ?>

		<div class="modal fade" id="ModalResultado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" id="cerrarForm" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Resultado carga:</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<!-- Row -->
							<div class="row">
								<div class="col-md-1">
								</div>
								<div class="col-md-10" id="div_resultado">
								</div>
								<div class="col-md-1">
								</div>
							</div>
							<!-- Row END-->
							<div class="clearfix"><br></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="close btn btn-primary" data-dismiss="modal" aria-hidden="true">cerrar</button>
				</div>
			</div>
		</div>
	</div>


		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/usuarios_all.js"></script>
</body>
</html>
