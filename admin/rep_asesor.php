<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_asesor.php');
$location = 'tutor';
$locData = true;
$Asist = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons stats"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Reporte General</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte General </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray" data-toggle="collapse-widget">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Este es el reporte general en donde se encuentra la información de Personas Vs Trayectorías académicas y su avance, es un reporte completo que tiene un nivel de entendimiento elevado y el recurso necesario para procesarlo también es elevado, sea muy consciente de su necesidad</h5></br>
								</div>
								<div class="col-md-2">
									<?php if($cantidad_datos > 0){ ?>
										<h5><i class="fa fa-fw fa-download"></i> <a href="rep_asesor_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>&trayectoria=<?php echo( $_POST['ntrayectoria'] ); ?>&opcn=rep"> Descargar Reporte</a><h5>
										<h5><i class="fa fa-fw fa-headphones"></i> <a href="rep_asesor_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>&trayectoria=<?php echo( $_POST['ntrayectoria'] ); ?>&opcn=audio"> Reporte Audio Libros</a><h5>
									<?php } ?>
								</div>
							</div>
							<form action="rep_asesor.php" method="post">
								<div class="row">
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
											<div class="col-md-6 input-group date">
										    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Hasta..." <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
											<div class="col-md-6 input-group date">
										    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..." <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label for="trayectoria" class="col-md-2 control-label">Trayectoria</label>
											<div class="col-md-8 input-group">
												<select name="trayectoria[]" id="trayectoria" style="width:100%" multiple>
													<optgroup label="Trayectoria">
														<?php foreach ($cargos as $key => $value) {
															$selected = "";
															foreach ($trayectoria_sel as $keyT => $valueT) {
																if( $valueT == $value['charge_id'] ){
																	$selected = "selected";
																	break;
																}
															} ?>
															<option value="<?php echo( $value['charge_id'] ); ?>" <?php echo( $selected ); ?> ><?php echo( $value['charge'] ); ?></option>
														<?php } ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-5">

									</div>
								</div>
							</form>
						</div>
					</div>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td style="width: 30%;">
											<p class="lead">REPORTE DE NAVEGACIÓN</p>
												<h4>Secciones Asesor Estratégico</h4>
												<address class="margin-none" style="text-align: justify;">
													<strong>Periodo: </strong><?php echo($_POST['start_date_day'].' - '.$_POST['end_date_day']); ?><br/><br/>
													En la tabla lateral podrá ver la información de acceso de cada sección en el Asesor Estratégico, la información detallada de las secciones que incluyen elementos administrables están mas abajo.<br/>
												</address>
										</td>
										<td class="right">
											<p class="lead">Navegación</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">SECCIÓN</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_datos > 0){
														foreach ($SeccionesDatos as $iID => $data) {
															$Section = "";
															if($data['section']=="argumentarios.php"){
																$Section = "Productos Chevrolet";
															}elseif($data['section']=="asesor_estrategico.php"){
																$Section = "Asesor Estratégico";
															}elseif($data['section']=="eventos.php"){
																$Section = "Eventos";
															}elseif($data['section']=="foros_tutor.php"){
																$Section = "Foros";
															}elseif($data['section']=="fotografias.php"){
																$Section = "Fotografías";
															}elseif($data['section']=="fotografias_detail.php"){
																$Section = "Foto Detalle";
															}elseif($data['section']=="noticias.php"){
																$Section = "Noticias";
															}elseif($data['section']=="novedades.php"){
																$Section = "Novedades";
															}elseif($data['section']=="videos.php"){
																$Section = "Videos";
															}elseif($data['section']=="videos_detail.php"){
																$Section = "Video Detalle";
															}elseif($data['section']=="marca_chevrolet.php"){
																$Section = "Marca Chevrolet";
															}elseif($data['section']=="club_lideres.php"){
																$Section = "Club Líderes";
															}elseif($data['section']=="ranking_lideres.php"){
																$Section = "Ranking Líderes";
															}elseif($data['section']=="envivo.php"){
																$Section = "Transmisión en Vivo";
															}
															?>
															<!-- Item -->
															<tr class="selectable">
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Section); ?></td>
																<input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($Section); ?>" data-Vis="<?php echo($data['cantidadVisitas']); ?>" data-Usr="<?php echo($data['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($data['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($data['tiempoPromedio']/60,2)); ?>" />
																<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($data['cantidadVisitas']); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($data['cantidadUsuarios']); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($data['tiempoTotal']/60,2)); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($data['tiempoPromedio']/60,2)); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
									<tr>
										<td class="center" colspan="2">
											<p class="lead">Gráficas de Apoyo</p><br/>
											<div class="row">
												<div class="col-md-6">
													<div class="widget widget-heading-simple widget-body-gray">
														<div class="widget-body">
															<div id="charVisitas" class="flotchart-holder" style="height: 300px;"></div>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="widget widget-heading-simple widget-body-gray">
														<div class="widget-body">
															<div id="charTiempos" class="flotchart-holder" style="height: 300px;"></div>
														</div>
													</div>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td style="width: 30%;">
											<p class="lead">REPORTE DE NAVEGACIÓN CONCESIONARIOS</p>
												<h4>Secciones Asesor Estratégico</h4>
												<address class="margin-none" style="text-align: justify;">
													<strong>Periodo: </strong><?php echo($_POST['start_date_day'].' - '.$_POST['end_date_day']); ?><br/><br/>
													En la tabla lateral podrá ver la información de acceso de cada sección en el Asesor Estratégico por cada concesionario, la información detallada de las secciones que incluyen elementos administrables están mas abajo.<br/>
												</address>
										</td>
										<td class="right">
											<p class="lead">Navegación</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">SECCIÓN</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_datos > 0){
														$aux = 0;
														foreach ($concesionarios as $iID => $data) { ?>
															<!-- Item -->
															<?php if( $data['cantidadVisitas'] > 0 ){ ?>
																<?php $clase_dealer = strpos( $data['dealer'], ' ' ) === false ? $data['dealer'] : str_replace(' ', '_', $data['dealer'] ) ; ?>
																<tr class="selectable" onclick=" mostrar( '<?php echo( $clase_dealer ); ?>' ) ">
																	<td class="center" style="padding: 2px; font-size: 80%; cursor: pointer"><strong><?php echo( $data['dealer'] ); ?></strong></td>
																	<!-- <input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($secc_list[ $data['section'] ]); ?>" data-Vis="<?php echo($data['cantidadVisitas']); ?>" data-Usr="<?php echo($data['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($data['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($data['tiempoPromedio']/60,2)); ?>" /> -->
																	<td class="center important" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo($data['cantidadVisitas']); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo($data['cantidadUsuarios']); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo(number_format($data['tiempoTotal']/60,2)); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo(number_format($data['tiempoTotal']/60/$data['cantidadVisitas'],2)); ?></strong></td>
																</tr>
																<!-- // Item END -->

																<?php foreach ($SeccionesDatosCcs as $key => $value): ?>
																	<?php if ( $value['dealer'] == $data['dealer'] ): ?>
																		<tr class="selectable hide <?php echo( $clase_dealer ); ?>">
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo( $secc_list[ $value['section'] ] ); ?></td>
																			<!-- <input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($secc_list[ $value['section'] ]); ?>" data-Vis="<?php echo($value['cantidadVisitas']); ?>" data-Usr="<?php echo($value['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($value['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($value['tiempoPromedio']/60,2)); ?>" /> -->
																			<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($value['cantidadVisitas']); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value['cantidadUsuarios']); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($value['tiempoTotal']/60,2)); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($value['tiempoPromedio']/60,2)); ?></td>
																		</tr>
																	<?php endif; ?>
																<?php endforeach; ?>

															<?php } ?>
													<?php
														$aux++;
													} }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td style="width: 30%;">
											<p class="lead">REPORTE DE NAVEGACIÓN ZONA</p>
											<h4>Secciones Asesor Estratégico</h4>
											<address class="margin-none" style="text-align: justify;">
												<strong>Periodo: </strong><?php echo($_POST['start_date_day'].' - '.$_POST['end_date_day']); ?><br/><br/>
												En la tabla lateral podrá ver la información de acceso de cada sección en el Asesor Estratégico en cada una de las zonas, la información detallada de las secciones que incluyen elementos administrables están mas abajo.<br/>
											</address>
										</td>
										<td class="right">
											<p class="lead">Navegación</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">SECCIÓN</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_datos > 0){
														$aux = 0;
														foreach ($zonas as $iID => $data) { ?>
															<!-- Item -->
															<?php if( $data['cantidadVisitas'] > 0 ){ ?>
																<?php $clase_zone = strpos( $data['zone'], ' ' ) === false ? $data['zone'] : str_replace(' ', '_', $data['zone'] ) ; ?>
																<tr class="selectable" onclick=" mostrar( '<?php echo( $clase_zone ); ?>' ) ">
																	<td class="center" style="padding: 2px; font-size: 80%; cursor: pointer"><strong><?php echo( $data['zone'] ); ?></strong></td>
																	<!-- <input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($secc_list[ $data['section'] ]); ?>" data-Vis="<?php echo($data['cantidadVisitas']); ?>" data-Usr="<?php echo($data['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($data['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($data['tiempoPromedio']/60,2)); ?>" /> -->
																	<td class="center important" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo($data['cantidadVisitas']); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo($data['cantidadUsuarios']); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo(number_format($data['tiempoTotal']/60,2)); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo(number_format($data['tiempoTotal']/60/$data['cantidadVisitas'],2)); ?></strong></td>
																</tr>
																<!-- // Item END -->

																<?php foreach ($desc_zonas as $key => $value): ?>
																	<?php if ( $value['zone'] == $data['zone'] && $value['cantidadVisitas'] > 0 ): ?>
																		<tr class="selectable hide <?php echo( $clase_zone ); ?>">
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo( $secc_list[ $value['section'] ] ); ?></td>
																			<!-- <input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($secc_list[ $value['section'] ]); ?>" data-Vis="<?php echo($value['cantidadVisitas']); ?>" data-Usr="<?php echo($value['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($value['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($value['tiempoPromedio']/60,2)); ?>" /> -->
																			<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($value['cantidadVisitas']); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value['cantidadUsuarios']); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($value['tiempoTotal']/60,2)); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($value['tiempoPromedio']/60,2)); ?></td>
																		</tr>
																	<?php endif; ?>
																<?php endforeach; ?>

															<?php } ?>
													<?php
														$aux++;
													} }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td style="width: 30%;">
											<p class="lead">REPORTE DE NAVEGACIÓN ARGUMENTARIO</p>
											<h4>Secciones Asesor Estratégico</h4>
											<address class="margin-none" style="text-align: justify;">
												<strong>Periodo: </strong><?php echo($_POST['start_date_day'].' - '.$_POST['end_date_day']); ?><br/><br/>
												En la tabla lateral podrá ver la información de acceso de cada sección de producto Chevrolet del Asesor Estratégico, la información detallada de las secciones que incluyen elementos administrables están mas abajo.<br/>
											</address>
										</td>
										<td class="right">
											<p class="lead">Navegación</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">SECCIÓN</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_datos > 0){
														$aux = 0;
														foreach ($categorias as $iID => $data) { ?>
															<!-- Item -->
															<?php if( $data['cantidadVisitas'] > 0 ){ ?>
																<?php $clase_argum = strpos( $data['argument_cat'], ' ' ) === false ? $data['argument_cat'] : str_replace(' ', '_', $data['argument_cat'] ) ; ?>
																<tr class="selectable" onclick=" mostrar( '<?php echo( $clase_argum ); ?>' ) ">
																	<td class="center" style="padding: 2px; font-size: 80%; cursor: pointer"><strong><?php echo( $data['argument_cat'] ); ?></strong></td>
																	<!-- <input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($secc_list[ $data['section'] ]); ?>" data-Vis="<?php echo($data['cantidadVisitas']); ?>" data-Usr="<?php echo($data['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($data['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($data['tiempoPromedio']/60,2)); ?>" /> -->
																	<td class="center important" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo($data['cantidadVisitas']); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo($data['cantidadUsuarios']); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo(number_format($data['tiempoTotal']/60,2)); ?></strong></td>
																	<td class="center" style="padding: 2px; font-size: 80%;"><strong class="text-primary"><?php echo(number_format($data['tiempoTotal']/60/$data['cantidadVisitas'],2)); ?></strong></td>
																</tr>
																<!-- // Item END -->

																<?php foreach ($DetalleArgumentario as $key => $value): ?>
																	<?php if ( $value['argument_cat'] == $data['argument_cat'] && $value['cantidadVisitas'] > 0 ): ?>
																		<tr class="selectable hide <?php echo( $clase_argum ); ?>">
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo( $value['argument'] ); ?></td>
																			<!-- <input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($secc_list[ $value['section'] ]); ?>" data-Vis="<?php echo($value['cantidadVisitas']); ?>" data-Usr="<?php echo($value['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($value['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($value['tiempoPromedio']/60,2)); ?>" /> -->
																			<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($value['cantidadVisitas']); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value['cantidadUsuarios']); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($value['tiempoTotal']/60,2)); ?></td>
																			<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(number_format($value['tiempoPromedio']/60,2)); ?></td>
																		</tr>
																	<?php endif; ?>
																<?php endforeach; ?>

															<?php } ?>
													<?php
														$aux++;
													} }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td style="width: 30%;">
											<p class="lead">DETALLE ARGUMENTARIO</p>
												<h4>Modelos del Argumentario</h4>
												<address class="margin-none" style="text-align: justify;">
													<strong>Periodo: </strong><?php echo($_POST['start_date_day'].' - '.$_POST['end_date_day']); ?><br/><br/>
													Podremos ver en esta parte del informe el detalle de los diferentes modelos del argumentario que han sido visualizados, incluido el promedio de duración, personas que lo visitan y la cantidad de likes que tiene cada uno de ellos. <br/>
												</address>
										</td>
										<td class="right">
											<p class="lead">Navegación</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">CATEGORÍA</th>
														<th class="center">MODELO</th>
														<th class="center">LIKES</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_argumentario > 0){
														foreach ($ArgumentarioDatos as $iID => $data) {
															$_argument_cat = "";
															$_argumentario = "0";
															$_image = "0";
															$_likes = "0";
															if(isset($data['cantidadVisitas'])){
																$_cantidadVisitas = $data['cantidadVisitas'];
															}else{
																$_cantidadVisitas = "0";
															}
															if(isset($data['cantidadUsuarios'])){
																$_cantidadUsuarios = $data['cantidadUsuarios'];
															}else{
																$_cantidadUsuarios = "0";
															}
															if(isset($data['tiempoTotal'])){
																$_tiempoTotal = number_format($data['tiempoTotal']/60,2);
															}else{
																$_tiempoTotal = "0";
															}
															if(isset($data['tiempoPromedio'])){
																$_tiempoPromedio = number_format($data['tiempoPromedio']/60,2);
															}else{
																$_tiempoPromedio = "0";
															}

															$_argument_cat = $data['argument_cat'];
															$_argumentario = substr($data['argument'], 0, 60);
															$_image = $data['image'];
															$_likes = $data['likes'];
															?>
															<!-- Item -->
															<tr class="selectable">
															<input type="hidden" class="Agument_dat" id="Agument_dat" name="Agument_dat" value="<?php echo($_argumentario); ?>" data-likes="<?php echo($_likes); ?>" data-Vis="<?php echo($_cantidadVisitas); ?>" data-Usr="<?php echo($_cantidadUsuarios); ?>" data-TimeT="<?php echo($_tiempoTotal); ?>" data-Prom="<?php echo($_tiempoPromedio); ?>" />
																<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary"><?php echo($_argument_cat); ?></span></td>
																<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($_argumentario); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_likes); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadVisitas); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadUsuarios); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoTotal); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoPromedio); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
									<tr>
										<td class="center" colspan="2">
											<p class="lead">Gráficas de Apoyo</p><br/>
											<div class="row">
												<div class="col-md-11">
													<div class="widget widget-heading-simple widget-body-gray">
														<div class="widget-body">
															<div id="charArgumentarios" class="flotchart-holder" style="height: 400px;"></div>
														</div>
													</div>
												</div>
												<div class="col-md-1">
													<label class="checkbox">
														<input type="checkbox" class="checkbox" value="SI" id="opcn_Visitas" name="opcn_Visitas" checked="checked" onchange="CargarArgumentario();" />
														Visitas
													</label>
													<label class="checkbox">
														<input type="checkbox" class="checkbox" value="SI" id="opcn_Visitantes" name="opcn_Visitantes" checked="checked" onchange="CargarArgumentario();" />
														Visitantes
													</label>
													<label class="checkbox">
														<input type="checkbox" class="checkbox" value="SI" id="opcn_Tiempo" name="opcn_Tiempo" checked="checked" onchange="CargarArgumentario();" />
														Tiempo
													</label>
													<label class="checkbox">
														<input type="checkbox" class="checkbox" value="SI" id="opcn_Promedio" name="opcn_Promedio" checked="checked" onchange="CargarArgumentario();" />
														Promedio
													</label>
													<label class="checkbox">
														<input type="checkbox" class="checkbox" value="SI" id="opcn_Likes" name="opcn_Likes" checked="checked" onchange="CargarArgumentario();" />
														Likes
													</label>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Detalle de Novedades</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">NOVEDAD</th>
														<th class="center">ESTADO</th>
														<th class="center">LIKES</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_novedad > 0){
														foreach ($NovedadesDatos as $iID => $data) {
															$_news = "0";
															$_image = "0";
															$_likes = "0";
															if(isset($data['cantidadVisitas'])){
																$_cantidadVisitas = $data['cantidadVisitas'];
															}else{
																$_cantidadVisitas = "0";
															}
															if(isset($data['cantidadUsuarios'])){
																$_cantidadUsuarios = $data['cantidadUsuarios'];
															}else{
																$_cantidadUsuarios = "0";
															}
															if(isset($data['tiempoTotal'])){
																$_tiempoTotal = number_format($data['tiempoTotal']/60,2);
															}else{
																$_tiempoTotal = "0";
															}
															if(isset($data['tiempoPromedio'])){
																$_tiempoPromedio = number_format($data['tiempoPromedio']/60,2);
															}else{
																$_tiempoPromedio = "0";
															}
															$_news = substr($data['news'], 0, 110);
															$_image = $data['image'];
															$_likes = $data['likes'];
															if($data['status_id']==1){
													    		$_status_id = '<span class="label label-success">Activo</span>';
													    	}elseif($data['status_id']==2){
													    		$_status_id = '<span class="label label-danger">Inactivo</span>';
													    	}else{
													    		$_status_id = '<span class="label label-important">Error</span>';
													    	}
															?>
															<!-- Item -->
															<tr class="selectable">
																<td class="important" style="padding: 2px; font-size: 80%;"><?php echo($_news); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_status_id); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_likes); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadVisitas); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadUsuarios); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoTotal); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoPromedio); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Detalle de Noticias</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">NOTICIA</th>
														<th class="center">ESTADO</th>
														<th class="center">LIKES</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_noticia > 0){
														foreach ($NoticiasDatos as $iID => $data) {
															$_news = "0";
															$_image = "0";
															$_likes = "0";
															if(isset($data['cantidadVisitas'])){
																$_cantidadVisitas = $data['cantidadVisitas'];
															}else{
																$_cantidadVisitas = "0";
															}
															if(isset($data['cantidadUsuarios'])){
																$_cantidadUsuarios = $data['cantidadUsuarios'];
															}else{
																$_cantidadUsuarios = "0";
															}
															if(isset($data['tiempoTotal'])){
																$_tiempoTotal = number_format($data['tiempoTotal']/60,2);
															}else{
																$_tiempoTotal = "0";
															}
															if(isset($data['tiempoPromedio'])){
																$_tiempoPromedio = number_format($data['tiempoPromedio']/60,2);
															}else{
																$_tiempoPromedio = "0";
															}
															$_news = substr($data['news'], 0, 110);
															$_image = $data['image'];
															$_likes = $data['likes'];
															if($data['status_id']==1){
													    		$_status_id = '<span class="label label-success">Activo</span>';
													    	}elseif($data['status_id']==2){
													    		$_status_id = '<span class="label label-danger">Inactivo</span>';
													    	}else{
													    		$_status_id = '<span class="label label-important">Error</span>';
													    	}
															?>
															<!-- Item -->
															<tr class="selectable">
																<td class="important" style="padding: 2px; font-size: 80%;"><?php echo($_news); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_status_id); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_likes); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadVisitas); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadUsuarios); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoTotal); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoPromedio); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Detalle de los Foros</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">FORO</th>
														<th class="center">ESTADO</th>
														<th class="center">POSTS</th>
														<th class="center">LIKES</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_foro > 0){
														foreach ($ForosDatos as $iID => $data) {
															$_forum = "0";
															$_image = "0";
															$_likes = "0";
															if(isset($data['cantidadVisitas'])){
																$_cantidadVisitas = $data['cantidadVisitas'];
															}else{
																$_cantidadVisitas = "0";
															}
															if(isset($data['cantidadUsuarios'])){
																$_cantidadUsuarios = $data['cantidadUsuarios'];
															}else{
																$_cantidadUsuarios = "0";
															}
															if(isset($data['tiempoTotal'])){
																$_tiempoTotal = number_format($data['tiempoTotal']/60,2);
															}else{
																$_tiempoTotal = "0";
															}
															if(isset($data['tiempoPromedio'])){
																$_tiempoPromedio = number_format($data['tiempoPromedio']/60,2);
															}else{
																$_tiempoPromedio = "0";
															}
															$_forum = substr($data['forum'], 0, 110);
															if(isset($data['likes'])){
																$_likes = $data['likes'];
															}else{
																$_likes = "0";
															}

															$_num_posts = $data['num_posts'];
															if($data['status_id']==1){
													    		$_status_id = '<span class="label label-success">Activo</span>';
													    	}elseif($data['status_id']==2){
													    		$_status_id = '<span class="label label-danger">Inactivo</span>';
													    	}else{
													    		$_status_id = '<span class="label label-important">Error</span>';
													    	}
															?>
															<!-- Item -->
															<tr class="selectable">
																<td class="important" style="padding: 2px; font-size: 80%;"><?php echo($_forum); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_status_id); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_num_posts); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_likes); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadVisitas); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadUsuarios); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoTotal); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoPromedio); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Detalle de los Eventos</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">EVENTO</th>
														<th class="center">ESTADO</th>
														<th class="center">FECHA</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_evento > 0){
														foreach ($EventosDatos as $iID => $data) {
															$_forum = "0";
															$_image = "0";
															$_likes = "0";
															if(isset($data['cantidadVisitas'])){
																$_cantidadVisitas = $data['cantidadVisitas'];
															}else{
																$_cantidadVisitas = "0";
															}
															if(isset($data['cantidadUsuarios'])){
																$_cantidadUsuarios = $data['cantidadUsuarios'];
															}else{
																$_cantidadUsuarios = "0";
															}
															if(isset($data['tiempoTotal'])){
																$_tiempoTotal = number_format($data['tiempoTotal']/60,2);
															}else{
																$_tiempoTotal = "0";
															}
															if(isset($data['tiempoPromedio'])){
																$_tiempoPromedio = number_format($data['tiempoPromedio']/60,2);
															}else{
																$_tiempoPromedio = "0";
															}
															$_event = substr($data['event'], 0, 110);
															$_date_event = $data['date_event'];
															if($data['status_id']==1){
													    		$_status_id = '<span class="label label-success">Activo</span>';
													    	}elseif($data['status_id']==2){
													    		$_status_id = '<span class="label label-danger">Inactivo</span>';
													    	}else{
													    		$_status_id = '<span class="label label-important">Error</span>';
													    	}
															?>
															<!-- Item -->
															<tr class="selectable">
																<td class="important" style="padding: 2px; font-size: 80%;"><?php echo($_event); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_status_id); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_date_event); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadVisitas); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadUsuarios); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoTotal); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoPromedio); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Navegación en Galerías de Fotografías</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">GALERIA</th>
														<th class="center">GALERIA</th>
														<th class="center">ESTADO</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_galeriaF > 0){
														foreach ($EventosGaleriasF as $iID => $data) {
															$_media = "0";
															$_image = "0";
															if(isset($data['cantidadVisitas'])){
																$_cantidadVisitas = $data['cantidadVisitas'];
															}else{
																$_cantidadVisitas = "0";
															}
															if(isset($data['cantidadUsuarios'])){
																$_cantidadUsuarios = $data['cantidadUsuarios'];
															}else{
																$_cantidadUsuarios = "0";
															}
															if(isset($data['tiempoTotal'])){
																$_tiempoTotal = number_format($data['tiempoTotal']/60,2);
															}else{
																$_tiempoTotal = "0";
															}
															if(isset($data['tiempoPromedio'])){
																$_tiempoPromedio = number_format($data['tiempoPromedio']/60,2);
															}else{
																$_tiempoPromedio = "0";
															}
															$_media = substr($data['media'], 0, 110);
															$_image = $data['image'];
															if($data['status_id']==1){
													    		$_status_id = '<span class="label label-success">Activo</span>';
													    	}elseif($data['status_id']==2){
													    		$_status_id = '<span class="label label-danger">Inactivo</span>';
													    	}else{
													    		$_status_id = '<span class="label label-important">Error</span>';
													    	}
															?>
															<!-- Item -->
															<tr class="selectable">
																<td class="center" style="padding: 2px; font-size: 80%;">
																	<img src="../assets/gallery/thumbs/<?php echo($_image); ?>" style="width:35px" alt="" class="img-responsive padding-none border-none center" />
																</td>
																<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($_media); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_status_id); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadVisitas); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadUsuarios); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoTotal); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoPromedio); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Navegación en Galerías de Videos</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">GALERIA</th>
														<th class="center">GALERIA</th>
														<th class="center">ESTADO</th>
														<th class="center">VISITAS</th>
														<th class="center">VISITANTES</th>
														<th class="center">TIEMPO TOTAL</th>
														<th class="center">PROMEDIO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if($cantidad_galeriaV > 0){
														foreach ($EventosGaleriasV as $iID => $data) {
															$_media = "0";
															$_image = "0";
															if(isset($data['cantidadVisitas'])){
																$_cantidadVisitas = $data['cantidadVisitas'];
															}else{
																$_cantidadVisitas = "0";
															}
															if(isset($data['cantidadUsuarios'])){
																$_cantidadUsuarios = $data['cantidadUsuarios'];
															}else{
																$_cantidadUsuarios = "0";
															}
															if(isset($data['tiempoTotal'])){
																$_tiempoTotal = number_format($data['tiempoTotal']/60,2);
															}else{
																$_tiempoTotal = "0";
															}
															if(isset($data['tiempoPromedio'])){
																$_tiempoPromedio = number_format($data['tiempoPromedio']/60,2);
															}else{
																$_tiempoPromedio = "0";
															}
															$_media = substr($data['media'], 0, 110);
															$_image = $data['image'];
															if($data['status_id']==1){
													    		$_status_id = '<span class="label label-success">Activo</span>';
													    	}elseif($data['status_id']==2){
													    		$_status_id = '<span class="label label-danger">Inactivo</span>';
													    	}else{
													    		$_status_id = '<span class="label label-important">Error</span>';
													    	}
															?>
															<!-- Item -->
															<tr class="selectable">
																<td class="center" style="padding: 2px; font-size: 80%;">
																	<img src="../assets/gallery/thumbs/<?php echo($_image); ?>" style="width:35px" alt="" class="img-responsive padding-none border-none center" />
																</td>
																<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($_media); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_status_id); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadVisitas); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_cantidadUsuarios); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoTotal); ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($_tiempoPromedio); ?></td>
															</tr>
															<!-- // Item END -->
													<?php } }?>
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Fotografías con Likes</p>
											<div class="row">
												<?php foreach ($DetallesGaleriasF as $iID => $data) { ?>
													<div class="widget-body padding-none col-md-1">
														<a href="Fotografía" onclick="return false"><img src="../assets/gallery/source/<?php echo($data['source']); ?>" style="width:90px" alt="<?php echo($data['media'].' : '.$data['media_detail']); ?>" title="<?php echo($data['media'].' : '.$data['media_detail']); ?>" /></a>
														<div class="description center">
															<i class="fa fa-fw fa-thumbs-o-up"></i>
															<span ><?php echo($data['likes']); ?></span>
														</div>
													</div>
												<?php } ?>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Videos con Likes</p>
											<div class="row">
												<?php foreach ($DetallesGaleriasV as $iID => $data) { ?>
													<div class="widget-body padding-none col-md-1">
														<a href="Video" onclick="return false"><img src="../assets/gallery/source/<?php echo($data['source']); ?>" style="width:90px" alt="<?php echo($data['media'].' : '.$data['media_detail']); ?>" title="<?php echo($data['media'].' : '.$data['media_detail']); ?>" /></a>
														<div class="description center">
															<i class="fa fa-fw fa-thumbs-o-up"></i>
															<span ><?php echo($data['likes']); ?></span>
														</div>
													</div>
												<?php } ?>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<br/>
					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead center">Reproducciones Audio Libros</p>
											<div class="row">
												<?php foreach ($datosAudios as $iID => $data) { ?>
													<div class="widget-body padding-none col-md-2">
														<a href="Fotografía" onclick="return false">
															<img src="../assets/Biblioteca/m/<?php echo($data['image']); ?>" style="width:100%" alt="<?php echo( $data['file'] ); ?>" title="<?php echo($data['name']); ?>" />
														</a>
														<div class="description center">
															<i class="fa fa-fw fa-headphones"></i>
															<span ><?php echo($data['views']); ?></span>
															<i class="fa fa-fw fa-thumbs-o-up"></i>
															<span ><?php echo($data['n_likes']); ?></span>
														</div>
													</div>
												<?php } ?>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_asesor.js"></script>
</body>
</html>
