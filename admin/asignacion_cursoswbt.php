<?php include('src/seguridad.php'); ?>
<?php

$location = 'asignacion_cursoswbt';
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<head><meta charset="gb18030">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>

</head>

<style type="text/css">
		
	</style>
<body class="document-body ">

<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted est¨¢ en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Asignacion de cursos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Asignacion de cursos</h2>
						
						<div class="clearfix"></div>
					</div>
			
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Informaci&oacuten: <span id="num_asistentes" style="color: orange"></span> </h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
						
							<!-- Table elements-->
							<table id="outside_user" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable table-hover" style="width: 100%;" >
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-hide="phone,tablet">IDENTIFICACI&OacuteN</th>
										<th data-hide="phone,tablet">NOMBRE Y APELLIDO</th>
										<th data-hide="phone,tablet">TRAYECTORIA</th>
										<th data-hide="phone,tablet" style="width: 750px">CURSOS</th>
										<th data-hide="phone,tablet">ASIGNAR CURSO</th>
										
									</tr>
								</thead>
								<!-- // Table heading END -->
								<tbody>
										<!-- <tr>
											<td></td>
											<td></td>
										</tr> -->
								</tbody>
							</table>
		<!--------------------------- Esta tabla permite descargar todo los usuarios  -------------------------------------------->
		   <!-- Modal -->
		 
						<div id="agregar_cursos" class="modal fade" role="dialog">
						  <div class="modal-dialog">
							
						   <!-- Modal content-->
						    <div class="modal-content">

						    <form id="edit_cursos" method="post" accept-charset="utf-8">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Asignacion de cursos WBT</h4>
						      </div>
						      <div class="modal-body">
						        <p>En esta ventana se asignan los cursos WBT con la trayectoria de Estudiante Independiente.</p>
						        	<select id="multiple" class="selec" name="cursos" style="width: 100%" multiple>
									
									</select>
						      </div>
						      <div class="modal-footer">
		                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
		                        <input type="submit" class="btn btn-success" value="Guardar Datos" id="Guardar_datos">
		                    </div>
							</form>
						    </div>
						  </div>
						</div>
				    <!-- fin modal-->

			          </div>
		  		 </div>
					
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	  <!--  <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript" ></script> -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js" type="text/javascript" ></script>
	<script src="js/asignacion_cursos_usuarios.js"></script>
  </body>
</html>