<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_evaluacion.php');
}
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Evaluacion.xls");
header ("Content-Transfer-Encoding: binary");
if(isset($_GET['start_date_day'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">CODIGO</th>
                <th data-hide="phone,tablet">EVALUACI&Oacute;N</th>
                <th data-hide="phone,tablet">IDENTIFICACI&Oacute;N</th>
                <th data-hide="phone,tablet">NOMBRE</th>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">SEDE</th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet">TIEMPO</th>
                <th data-hide="phone,tablet">PUNTOS</th>
                <th data-hide="phone,tablet">PUNTAJE</th>
                <th data-hide="phone,tablet">AGRUPACION</th>
                <th data-hide="phone,tablet">PREGUNTA</th>
                <th data-hide="phone,tablet">RESPUESTA</th>
                <th data-hide="phone,tablet">RESULTADO</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php 
                if($cantidad_datos > 0){ 
						foreach ($datosCursos_all as $iID => $data) { 
							$var_Ctrl = 0;
                            if(isset($data['respuestas'])){
							foreach ($data['respuestas'] as $iID => $dataRespuestas) {?>
                    <tr>
                        <td><?php echo(utf8_decode($data['code'])); ?></td>
                        <td><?php echo(utf8_decode($data['review'])); ?></td>
                        <td><?php echo(utf8_decode($data['identification'])); ?></td>
                        <td><?php echo(utf8_decode($data['first_name'].' '.$data['last_name'])); ?></td>
                        <td><?php echo(utf8_decode($data['dealer'])); ?></td>
                        <td><?php echo(utf8_decode($data['headquarter'])); ?></td>
                        <td><?php echo($data['date_creation']); ?></td>
                        <td><?php echo($data['time_response']); ?></td>
                        <td><?php echo($data['score']); ?></td>
                        <td><?php echo(number_format(($data['score']/$data['available_score'])*100,0)); ?></td>
                        <td><?php echo(utf8_decode($dataRespuestas['agrup'])); ?></td>
                        <td><?php echo(utf8_decode($dataRespuestas['question'])); ?></td>
                        <td><?php echo(utf8_decode($dataRespuestas['answer'])); ?></td>
                        <td><?php echo(utf8_decode($dataRespuestas['result'])); ?></td>
                    </tr>
                <?php } } } } ?>
        </tbody>
    </table>
<?php } ?>