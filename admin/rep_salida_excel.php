<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['dealer_id'])){
    include('controllers/rep_salida.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_SalidasXUsuario.xls");
if(isset($_GET['dealer_id'])){
    ?>
    <table>
        <!-- Table heading -->
            <thead>
                <tr>
                    <th data-hide="phone,tablet" style="width: 15%;">CONCESIONARIO</th>
                    <th data-hide="phone,tablet" style="width: 20%;">SEDE</th>
                    <th data-hide="phone,tablet" style="width: 5%;">IDENTIFICACIÓN</th>
                    <th data-hide="phone,tablet" style="width: 20%;">USUARIO</th>
                    <th data-hide="phone,tablet" style="width: 20%;">TRAYECTORIAS</th>
                    <th data-hide="phone,tablet" style="width: 5%;">SALIDAS</th>
                    <th data-hide="phone,tablet" style="width: 5%;">HORAS</th>
                </tr>
            </thead>
            <!-- // Table heading END -->
            <tbody>
                <?php 
                if($cantidad_datos > 0){ 
                    foreach ($datosCursos_all as $iID => $data) { 
                        ?>
                    <tr>
                        <td><?php echo($data['dealer']); ?></td>
                        <td><?php echo($data['headquarter']); ?></td>
                        <td><?php echo($data['identification']); ?></td>
                        <td><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
                        <td align="center">
                            <?php 
                                if(isset($data['charges'])){
                                    foreach ($data['charges'] as $iID => $dataCharges) { 
                                        echo(" - ".$dataCharges['charge']);
                                    }
                                }
                            ?>
                        </td>
                        <td align="center"><?php echo($data['tiempo']/8); ?></td>
                        <td align="center"><?php echo($data['tiempo']); ?></td>
                    </tr>
                <?php } } ?>
            </tbody>
    </table>
<?php } ?>