<?php
include('src/seguridad.php');
$location = 'education';
$locData = true;
include('controllers/programaciones.php');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Programacion.xls");
    ?>
<table>
    <!-- Table heading -->
    <thead>
            <tr>
                <th data-hide="phone,tablet">CODIGO</th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">ESPECIALIDAD</th>
                <th data-hide="phone,tablet">INSTRUCTOR</th>
                <th data-hide="phone,tablet">TRAYECTORIAS</th>
                <th data-class="expand">INICIO <br><i>(YYYY-MM-DD)</i></th>
                <th data-hide="phone,tablet">INSCRIPCI&Oacute;N <br><i>(YYYY-MM-DD)</i></th>
                <th data-hide="phone,tablet">LUGAR</th>
                <th data-hide="phone,tablet">MIN</th>
                <th data-hide="phone,tablet">MAX</th>
                <th data-hide="phone,tablet">INS</th>
                <th data-hide="phone,tablet">TIEMPO</th>
                <th data-hide="phone,tablet">ESTADO</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php
            if($cantidad_datos > 0){
                foreach ($programacionesCompletas as $iID => $data) {
                    if($data['status_id']==1){
                        $val_estado = 'Activo';
                    }elseif($data['status_id']==2){
                        $val_estado = 'Inactivo';
                    }else{
                        $val_estado = 'Error';
                    }
                    ?>
                <tr>
                    <td><?php echo(utf8_decode($data['newcode'])); ?></td>
                    <td><?php echo(utf8_decode($data['course'])); ?></td>
                    <td><?php echo(utf8_decode($data['specialty'])); ?></td>
                    <td><?php echo(utf8_decode($data['first_prof'].' '.$data['last_prof'])); ?></td>
                    <td><?php echo(utf8_decode($data['trayectorias'])); ?></td>
                    <td><?php echo(utf8_decode($data['start_date'])); ?></td>
                    <td><?php echo(utf8_decode(substr($data['max_inscription_date'],0,10))); ?></td>
                    <td><?php echo(utf8_decode($data['living'])); ?></td>
                    <td><?php echo(utf8_decode($data['min_size'])); ?></td>
                    <td><?php echo(utf8_decode($data['max_size'])); ?></td>
                    <td><?php echo(utf8_decode($data['inscriptions'])); ?></td>
                    <td><?php echo(utf8_decode($data['duration_time'])); ?></td>
                    <td><?php echo($val_estado); ?></td>


                </tr>
            <?php } } ?>
        </tbody>
</table>
