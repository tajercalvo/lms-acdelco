
<?php include('src/seguridad.php'); ?>
  <!-- 
  //-----------------------------//
 // Creado por: Fredy Mendoza   //
//-----------------------------//  -->
<?php 
include('controllers/asignacion_cursos_usuarios.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>

<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<style type="text/css" media="screen">
	.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
  background-color: #ddd; 

}
</style>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Asignacion de cursos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Asignacion de cursos</h2>
						
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">asigna curos a usuarios</h5></br>
			</div>
			<div class="col-md-2">
				<form action="ficheroExcel.php" method="post" id="FormularioExportacion">
					<h5 id="descargar" style="display: none;" ><a class="glyphicons cloud-upload"  ><i></i>Descargar</a><br></h5>
					<input type="hidden" id="datos_tabla" name="datos_tabla" />
					<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Reporte_broadcast" />
				</form>
			</div>
		</div>
		<div class="row">
			<form id="frm" method="get" name="consultar_outside_user">
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="conference_id" style="padding-top:8px;">Sesión:</label>
					<div class="col-md-10">
						<select style="width: 100%;" id="conference_id" name="conference_id"></select>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
				<span id="consultando">
					
				</span>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información: <span id="num_asistentes" style="color: orange"></span> </h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
	
		<!-- Table elements-->
		<table id="outside_user" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable table-hover" style="width: 100%;" >
			<!-- Table heading -->
			<thead>
				<tr>
					<th data-hide="phone,tablet">IDENTIFICACIÓN</th>
					<th data-hide="phone,tablet">NOMBRE Y APELLIDO</th>
					<th data-hide="phone,tablet">TRAYECTORIA</th>
					<th data-hide="phone,tablet">CURSOS</th>
					<th data-hide="phone,tablet">ASIGNAR CURSO</th>
					
				</tr>
			</thead>
			<!-- // Table heading END -->
			<tbody>
					<!-- <tr>
						<td></td>
						<td></td>
					</tr> -->
			</tbody>
		</table>
		<!--------------------------- Esta tabla permite descargar todo los usuarios  -------------------------------------------->
		   <!-- Modal -->
		 
				<div id="agregar_cursos" class="modal fade" role="dialog">
				  <div class="modal-dialog">
					
					
					
				    <!-- Modal content-->
				    <div class="modal-content">

				    <form id="edit_cursos" method="post" accept-charset="utf-8">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Modal Header</h4>
				      </div>
				      <div class="modal-body">
				        <p>Some text in the modal.</p>
				        	<select id="multiple" class="selec" name="cursos" style="width: 100%" multiple>
								<!-- <option value="">asignar cursos</option> -->
								<!-- <option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option> -->

			
							</select>
				      </div>
				      <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                        <input type="submit" class="btn btn-success" value="Guardar_datos" id="Guardar_datos">
                    </div>
					</form>
				    </div>
				  </div>
				</div>
		<!-- fin modal-->

			</div>
		</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/asignacion_cursos_usuarios.js"></script>
</body>
</html>