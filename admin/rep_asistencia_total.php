<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_asistencia_total.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia_total.php">Reporte de Asistencia Total</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Asistencia Total </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
					<div class="widget-body">
						<div class="row">
							<div class="col-md-10">
								<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de los inscritos por cada curso en los programadas definidos entre 2 fechas.</h5></br>
							</div>
							<div class="col-md-2">
								<?php if($cantidad_datos > 0): ?>
									<h5><a href="rep_asistencia_total_excel.php?start_date=<?php echo($_POST['start_date']); ?>&end_date=<?php echo($_POST['end_date']); ?>&course_id=<?php echo($_POST['course_id']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
								<?php endif; ?>
							</div>
						</div>
						<div class="row">
							<form action="rep_asistencia_total.php" method="post">
							<div class="col-md-5">
								<!-- Group -->
								<div class="form-group">
									<label class="col-md-5 control-label" for="start_date" style="padding-top:8px;">Fecha Inicial:</label>
									<div class="col-md-7 input-group date">
								    	<input class="form-control" type="text" id="start_date" name="start_date" placeholder="Inicia el" <?php if(isset($_POST['start_date'])) { ?>value="<?php echo $_POST['start_date']; ?>"<?php } ?>/>
								    	<span class="input-group-addon">
								    		<i class="fa fa-th"></i>
								    	</span>
									</div>
								</div>
								<!-- // Group END -->
							</div>
							<div class="col-md-5">
								<!-- Group -->
								<div class="form-group">
									<label class="col-md-5 control-label" for="end_date" style="padding-top:8px;">Fecha Final:</label>
									<div class="col-md-7 input-group date">
								    	<input class="form-control" type="text" id="end_date" name="end_date" placeholder="Finaliza el" <?php if(isset($_POST['end_date'])) { ?>value="<?php echo $_POST['end_date']; ?>"<?php } ?>/>
								    	<span class="input-group-addon">
								    		<i class="fa fa-th"></i>
								    	</span>
									</div>
								</div>
								<!-- // Group END -->
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
							</div>
							<div class="col-md-10">
								<div class="form-group">
									<label class="col-md-5 control-label" for="course_id" style="padding-top:8px;">Curso :</label>
									<select style="width: 100%;" id="course_id" name="course_id">
										<option value="0">Todos los Cursos</option>
										<?php foreach ($listaCursos as $key => $valueCursos) : ?>
											<option value="<?php echo($valueCursos['course_id']); ?>"><?php echo($valueCursos['course']); ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>

				<?php if ( $cantidad_datos > 0 ): ?>
					<div class="well">
						<table class="table table-invoice">
							<?php foreach ( $datosCursos_all as $key => $v ): ?>
								<?php
									$cupos_pendientes = intval($v['max_size']) - count( $v['inscritos'] );
								?>
								<tbody>
									<tr>
										<td style="width: 30%;">
											<p class="lead">Sesión de entrenamiento</p>
											<h4><?= $v['newcode'] ?> | <?= $v['course'] ?></h4>
											<address class="margin-none">
												<strong>Tipo: </strong><?= $v['type'] ?><br/>
												<strong>Cupos: </strong>Asignados: <?= $v['max_size'] ?> | Excusas: <?= $v['excusas_cupos'] ?><br/>
												<strong>Registrados: </strong> Inscritos: <?= count($v['inscritos']) ?> | Excusas: <?= $v['excusas'] ?><br/>
												<strong>Duración: </strong><?= $v['duration_time'] ?><br/>
												<strong>Profesor: </strong><?= $v['first_prof'].' '.$v['last_prof'] ?><br/>
												<strong>Inicio: </strong><?= $v['start_date'] ?><br />
												<strong>Final: </strong><?= $v['end_date'] ?><br/>
												<strong>Lugar: </strong><?= substr($v['living'],0,30) ?><br/>
												<strong>Dirección: </strong><?= substr($v['address'],0,30) ?><br/>
												<strong>Ciudad: </strong><?= $v['city'] ?> | <?= $v['schedule_id'] ?>
											</address>
										</td>
										<td class="right">
											<p class="lead">Asistencia</p>
											<!-- // Table -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">Alumno</th>
														<th class="center">Identificación</th>
														<th class="center">Cargo</th>
														<th class="center">Sede</th>
														<th class="center">Asistencia</th>
														<th class="center" style="width: 1%">NOTA</th>
														<th class="center" style="width: 1%">EV</th>
														<th class="center" style="width: 1%">EP</th>
														<th class="center">Resultado</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php $var_Ctrl = 0; ?>
													<!-- ======================================================= -->
													<!-- Asistentes al curso -->
													<!-- ======================================================= -->
													<?php foreach ( $v['inscritos'] as $keyi => $vins ): ?>
														<?php $var_Ctrl++; ?>
														<!-- Item -->
														<tr class="selectable">
															<td class="text-left" style="padding: 2px; font-size: 80%;">
																<?= substr($vins['first_name'],0,10).' '.substr($vins['last_name'],0,10) ?>
															</td>
															<td class="text-left important" style="padding: 2px; font-size: 80%;"><?= $vins['identification'] ?></td>
															<td class="center important" style="padding: 2px; font-size: 80%;">
																<?= isset($vins['charge']) && $vins['charge'] != null ? $vins['charge'] : "" ?>
															</td>
															<td class="text-left" style="padding: 2px; font-size: 80%;"><?= $vins['headquarter'] ?></td>
															<td class="center" style="padding: 2px; font-size: 80%;">
																<?php
																	switch ( $vins['status_id'] ) {
																		case '1':
																			echo "<span class='text-info'>Invitado</span>";
																		break;
																		case '2':
																			echo "<span class='text-default'>Asistió</span>";
																		break;
																		case '3':
																			if (  $vins['excusa'] != "" ) {
																				echo "<span class='text-warning'>Excusa</span>";
																			}else{
																				echo "<span class='text-danger'>No Asistió</span>";
																			}
																		break;
																		default:
																			echo "<span class='text-default'>-</span>";
																		break;

																	}
																?>
															</td>
															<td class="center" style="padding: 2px; font-size: 80%;"><?= $vins['score'] ?></td>
															<td class="center" style="padding: 2px; font-size: 80%;"><?= $vins['score_evaluation'] ?></td>
															<td class="center" style="padding: 2px; font-size: 80%;"><?= $vins['score_waybill'] ?></td>
															<td class="center" style="padding: 2px; font-size: 80%;">
																<?php if ( $vins['approval'] == "SI" ): ?>
																	<span class="label label-primary">Aprobó</span>
																<?php else: ?>
																	<?php if ( $vins['status_id'] == 1 ): ?>
																		<span class="label label-info">Pendiente</span>
																	<?php else: ?>
																		<span class="label label-danger">Reprobó</span>
																	<?php endif; ?>
																<?php endif; ?>

															</td>
														</tr>
														<!-- // Item END -->
													<?php endforeach; ?>
													<!-- ======================================================= -->
													<!-- END Asistentes al curso -->
													<!-- ======================================================= -->

													<!-- ======================================================= -->
													<!-- Cupos pendientes/excusas -->
													<!-- ======================================================= -->
													<?php if ( $cupos_pendientes > 0 ): ?>
														<?php for( $i = 1; $i <= $cupos_pendientes; $i++ ) { ?>
															<tr class="selectable">
																<td class="text-left" style="padding: 2px; font-size: 80%;">Sin Usuario</td>
																<td class="text-left important" style="padding: 2px; font-size: 80%;">-</td>
																<td class="center important" style="padding: 2px; font-size: 80%;">-</td>
																<td class="text-left" style="padding: 2px; font-size: 80%;">-</td>
																<td class="center" style="padding: 2px; font-size: 80%;">
																	<span class='text-info'>No Asignado</span>
																</td>
																<td class="center" style="padding: 2px; font-size: 80%;">-</td>
																<td class="center" style="padding: 2px; font-size: 80%;">-</td>
																<td class="center" style="padding: 2px; font-size: 80%;">
																	<?php if ( $i <= intval($v['excusas_cupos']) ): ?>
																		<a href="../assets/excusas/<?= $v['file'] ?>" target="_blank" class="label label-warning">Excusa Soporte Cupo</a>
																	<?php else: ?>
																		<span class="label label-default">No asignado</span>
																	<?php endif; ?>
																</td>
															</tr>
														<?php } ?>
													<?php endif; ?>
													<!-- ======================================================= -->
													<!-- END Cupos pendientes/excusas -->
													<!-- ======================================================= -->
												</tbody>
											</table>
											<!-- // Table END -->
										</td>
									</tr>
								</tbody>
							<?php endforeach; ?>
						</table>
					</div>
				<?php endif; ?>



						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
						<!-- // END contenido interno -->
					</div>
					<!-- // END inner -->
				</div>
				<!-- // END Contenido proyectos -->
			</div>
			<div class="clearfix"></div>
			<!-- // Sidebar menu & content wrapper END -->
			<?php include('src/footer.php'); ?>
		</div>
		<!-- // Main Container Fluid END -->
		<?php include('src/global.php'); ?>
		<script src="js/rep_asistencia_total.js?varRand=<?php echo(rand(10,999999));?>"></script>
	</body>
</html>
