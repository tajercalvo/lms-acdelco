<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date'])){
    include('controllers/rep_asistencia_total.php');
}
//echo('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Asistencia_Total.xls");
header ("Content-Transfer-Encoding: binary");
if(isset($_GET['start_date'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">CONCESIONARIO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet"><?= htmlspecialchars( "CÓDIGO" )  ?></th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">CURSO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">SEDE</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">CIUDAD</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">ZONA</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">ALUMNO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet"><?= htmlspecialchars( "IDENTIFICACIÓN" ) ?></th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">CARGO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">SESION</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">FECHA</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">FINAL</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">LUGAR</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">PROFESOR</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">ASISTENCIA</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">PUNTAJE TOTAL</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">EVALUACION</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet"><?= htmlspecialchars( "PROFUNDIZACIÓN" ) ?></th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">RESULTADO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">CUPOS INICIALES</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">CUPOS FINALES</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">INSCRITOS</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">EXCUSA</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">HORAS CURSO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">DIAS CURSO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">ESPECIALIDAD</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">ESTRATEGIA</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">PROVEEDOR</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">TIPO DE CURSO</th>
                <th style="border:1px solid #000000; background-color: #000; color: #ffffff; font-size: 12px;" data-hide="phone,tablet">MES</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php
                if( isset( $cantidad_datos ) && $cantidad_datos > 0){
                        foreach ( $datosCursos_all as $iID => $v ) {
                            $cupos_pendientes = intval($v['max_size']) - count( $v['inscritos'] );
                            $sede_insc = isset($v['inscritos'][0]['headquarter']) ? $v['inscritos'][0]['headquarter'] : '';
                            $zona_insc = isset($v['inscritos'][0]['zone']) ? $v['inscritos'][0]['zone'] : '';
                            // ========================================================
                            // Fin inscritos
                            // ========================================================
                            foreach ( $v['inscritos'] as $keyi => $vins ){ ?>
                                <tr>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['dealer'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['newcode'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= htmlspecialchars($v['course']) ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $vins['headquarter'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['city'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $vins['zone'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;">
                                        <?= $vins['first_name'].' '.$vins['last_name'] ?>
                                    </td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $vins['identification'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;">
                                        <?php
                                            if(isset($vins['charge'])){
                                                echo( $vins['charge'] );
                                            }
                                        ?>
                                    </td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['schedule_id'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['start_date'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['end_date'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['living'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;">
                                        <?= $v['first_prof'].' '.$v['last_prof'] ?>
                                    </td>
                                    <td style="border:1px solid #000000; font-size: 12px;">
                                        <?php
                                            switch ( $vins['status_id'] ) {
                                                case '1':
                                                    echo "Invitado";
                                                break;
                                                case '2':
                                                    echo "Asistió";
                                                break;
                                                case '3':
                                                    echo "No Asistió";
                                                break;
                                                default:
                                                    echo "No Inscrito";
                                                break;

                                            }
                                        ?>
                                    </td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $vins['score'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $vins['score_evaluation'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $vins['score_waybill'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;">
                                        <?php if ( $vins['approval'] == "SI" ): ?>
                                            <span>Aprobó</span>
                                        <?php else: ?>
                                            <?php if ( $vins['status_id'] == 1 ): ?>
                                                <span>Pendiente</span>
                                            <?php else: ?>
                                                <span>Reprobó</span>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                    </td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['min_size'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['max_size'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= count($v['inscritos']) ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $vins['excusa'] != "" ? "Excusa" : '' ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['duration_time'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= round( intval($v['duration_time']) / 8, 1 ) ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['specialty'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= !empty($v['strategy']) ? $v['strategy'] : ""  ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= !empty($v['supplier']) ? $v['supplier'] : ""  ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $v['type'] ?></td>
                                    <td style="border:1px solid #000000; font-size: 12px;"><?= $mes[ $v['mes'] ] ?></td>
                                </tr>
                            <?php
                        } // fin foreach inscritos
                        // ========================================================
                        // Fin inscritos
                        // ========================================================

                        // ========================================================
                        // Excusas
                        // ========================================================
                        if ( $cupos_pendientes > 0 ){

                            for( $i = 1; $i <= $cupos_pendientes; $i++ ) { ?>

                                <tr>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['dealer'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['newcode'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['course'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $sede_insc ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['city'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $zona_insc ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">No registrado</td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">No registrado</td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['schedule_id'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['start_date'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['end_date'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['living'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">
                                        <?= $v['first_prof'].' '.$v['last_prof'] ?>
                                    </td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">No Inscrito</td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">0</td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">0</td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">0</td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">No asignado</td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['min_size'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['max_size'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= count($v['inscritos']) ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;">
                                        <?php if ( $i <= intval($v['excusas_cupos']) ): ?>
                                            Excusa Soporte Cupo
                                        <?php endif; ?>
                                    </td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['duration_time'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= round( intval($v['duration_time']) / 8, 1 ) ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['specialty'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= !empty($v['strategy']) ? $v['strategy'] : ""  ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= !empty($v['supplier']) ? $v['supplier'] : ""  ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $v['type'] ?></td>
                                    <td style="border:1px solid #F70606; font-size: 12px;"><?= $mes[ $v['mes'] ] ?></td>
                                </tr>

                    <?php }// fin for

                    }// fin cuenta excusas
                        // ========================================================
                        // END Excusas
                        // ========================================================

                    } // fin foreach schedules
                } // fin if count ?>
        </tbody>
    </table>
<?php } ?>
