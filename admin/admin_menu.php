<?php include_once('src/seguridad.php'); ?>
<?php
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/admin_menu.css?version<?php echo md5(time()); ?>">
</head>

<style type="text/css" media="screen">
	.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
  background-color: #ddd;

}
</style>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Administrador del menú LMS</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget-body">
							<!-- Table menus-->
							<table id="tabla" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable table-hover ui-sortable" style="width: 100%;">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-hide="phone,tablet">#</th>
										<th data-hide="phone,tablet">MENÚ ID</th>
										<th data-hide="phone,tablet">PADRE</th>
										<th data-hide="phone,tablet">HIJO</th>
										<th data-hide="phone,tablet">PERMISOS</th>
										<th data-hide="phone,tablet">ESTADO</th>
									</tr>
								</thead>
								<!-- // Table heading END -->
								<tbody>

								</tbody>
							</table>
							<span id="volver" style="display: none; color: #0058a3" class="glyphicon glyphicon-arrow-left"></span>
							<span class="titulo_modulo">Permisos para el moduolo </span>
							<span class="titulo_modulo" id="titulo_modulo"></span>
						<span></span>
							<table style="display: none" id="tabla_permisos" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable table-hover ui-sortable" style="width: 100%;">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-hide="phone,tablet">#</th>
										<th data-hide="phone,tablet">ROL</th>
										<th data-hide="phone,tablet">ESTADO</th>
									</tr>
								</thead>
								<!-- // Table heading END -->
								<tbody>

								</tbody>
							</table>
						</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/admin_menu.js?v=<?php echo md5(time()); ?>"></script>
</body>
</html>