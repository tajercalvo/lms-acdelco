<head>
	<meta charset="utf-8">
	<style>
		*,*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:62.5%;-webkit-tap-highlight-color:rgba(0,0,0,0)}body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.428571429;color:#333;background-color:#fff}html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}body{margin:0}table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table>thead>tr>th,.table>tbody>tr>th,.table>tfoot>tr>th,.table>thead>tr>td,.table>tbody>tr>td,.table>tfoot>tr>td{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table>caption+thead>tr:first-child>th,.table>colgroup+thead>tr:first-child>th,.table>thead:first-child>tr:first-child>th,.table>caption+thead>tr:first-child>td,.table>colgroup+thead>tr:first-child>td,.table>thead:first-child>tr:first-child>td{border-top:0}.table>tbody+tbody{border-top:2px solid #ddd}.table
		.table{background-color:#fff}.table-condensed>thead>tr>th,.table-condensed>tbody>tr>th,.table-condensed>tfoot>tr>th,.table-condensed>thead>tr>td,.table-condensed>tbody>tr>td,.table-condensed>tfoot>tr>td{padding:5px}.table-bordered{border:1px
		solid #ddd}.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px
		solid #ddd}.table-bordered>thead>tr>th,.table-bordered>thead>tr>td{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*="col-"]{position:static;display:table-column;float:none}table td[class*="col-"],
		table th[class*="col-"]{display:table-cell;float:none}.table>thead>tr>.active,.table>tbody>tr>.active,.table>tfoot>tr>.active,.table>thead>.active>td,.table>tbody>.active>td,.table>tfoot>.active>td,.table>thead>.active>th,.table>tbody>.active>th,.table>tfoot>.active>th{background-color:#f5f5f5}.table-hover>tbody>tr>.active:hover,.table-hover>tbody>.active:hover>td,.table-hover>tbody>.active:hover>th{background-color:#e8e8e8}.table>thead>tr>.success,.table>tbody>tr>.success,.table>tfoot>tr>.success,.table>thead>.success>td,.table>tbody>.success>td,.table>tfoot>.success>td,.table>thead>.success>th,.table>tbody>.success>th,.table>tfoot>.success>th{background-color:#dff0d8}.table-hover>tbody>tr>.success:hover,.table-hover>tbody>.success:hover>td,.table-hover>tbody>.success:hover>th{background-color:#d0e9c6}.table>thead>tr>.danger,.table>tbody>tr>.danger,.table>tfoot>tr>.danger,.table>thead>.danger>td,.table>tbody>.danger>td,.table>tfoot>.danger>td,.table>thead>.danger>th,.table>tbody>.danger>th,.table>tfoot>.danger>th{background-color:#f2dede}.table-hover>tbody>tr>.danger:hover,.table-hover>tbody>.danger:hover>td,.table-hover>tbody>.danger:hover>th{background-color:#ebcccc}.table>thead>tr>.warning,.table>tbody>tr>.warning,.table>tfoot>tr>.warning,.table>thead>.warning>td,.table>tbody>.warning>td,.table>tfoot>.warning>td,.table>thead>.warning>th,.table>tbody>.warning>th,.table>tfoot>.warning>th{background-color:#fcf8e3}.table-hover>tbody>tr>.warning:hover,.table-hover>tbody>.warning:hover>td,.table-hover>tbody>.warning:hover>th{background-color:#faf2cc}@media (max-width: 767px){.table-responsive{width:100%;margin-bottom:15px;overflow-x:scroll;overflow-y:hidden;border:1px
		solid #ddd;-ms-overflow-style:-ms-autohiding-scrollbar;-webkit-overflow-scrolling:touch}.table-responsive>.table{margin-bottom:0}.table-responsive>.table>thead>tr>th,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tfoot>tr>td{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>thead>tr>th:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child{border-left:0}.table-responsive>.table-bordered>thead>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>td{border-bottom:0}}.well{min-height:20px;padding:19px;margin-bottom:20px;background-color:#f5f5f5;border:1px
		solid #e3e3e3;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)}
	</style>
</head>

<?php include('src/seguridad.php');

	include_once('../config/config.php');
	include_once('../config/database.php');
	include_once('controllers/email.php');

	$DataBase_Acciones = new Database();
	$email = new EmailLudus();

	// $start = isset($_GET['start']) ? $_GET['start'] : '' ;
	// $end = isset($_GET['end']) ? $_GET['end'] : '' ;
	$fecha = date_create(date("Y-m-d"));
	// $fecha = date_create('2000-01-01');
	date_add($fecha, date_interval_create_from_date_string('1 days'));
	// echo date_format($fecha, 'Y-m-d');
	$fecha = date_format($fecha, 'Y-m-d');

	$query_programacion = "SELECT s.schedule_id, ds.dealer_id, c.type, c.course, r.first_name AS teacher_f_name, r.last_name AS teacher_ls_name, s.start_date, s.end_date, ds.min_size, ds.max_size, ds.inscriptions
		FROM ludus_dealer_schedule ds, ludus_schedule s, ludus_courses c, ludus_users r
		WHERE ds.schedule_id = s.schedule_id AND s.course_id = c.course_id AND s.teacher_id = r.user_id AND s.status_id = 1 AND c.status_id = 1
		AND ds.min_size > 0 AND s.start_date BETWEEN '$fecha 00:00:0' AND '$fecha 23:59:59' ORDER BY ds.dealer_id, s.start_date";

	// echo("<br>$query_programacion<br>");

	$prog = $DataBase_Acciones->SQL_SelectMultipleRows( $query_programacion );

	// $query_lideres = "SELECT d.dealer_id, u.first_name, u.last_name, u.email, u.headquarter_id, c.charge, d.dealer
	// 		FROM ludus_users u, ludus_headquarters h, ludus_charges_users cu, ludus_charges c, ludus_dealers d
	// 		WHERE u.headquarter_id = h.headquarter_id AND u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND h.dealer_id = d.dealer_id
	// 		AND c.charge_id = 128 AND u.status_id = 1 AND cu.status_id = 1 AND d.status_id = 1 ORDER BY d.dealer, u.last_name";

	$query_lideres = "SELECT d.dealer_id, u.first_name, u.last_name, u.email, u.headquarter_id, r.rol, d.dealer
			FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_roles_users ru, ludus_roles r
			WHERE u.headquarter_id = h.headquarter_id
            AND h.dealer_id = d.dealer_id
            AND u.user_id = ru.user_id
            AND ru.rol_id = r.rol_id
            AND r.status_id = 1
            AND u.status_id = 1
            AND d.status_id = 1
            AND r.rol_id = 4
            AND u.email != ''
            AND h.headquarter_id > 1
            ORDER BY d.dealer, u.last_name";

	// echo("<br>$query_lideres<br>");
	$lideres = $DataBase_Acciones->SQL_SelectMultipleRows( $query_lideres );

	$query_alumnos = "SELECT i.user_id, i.schedule_id, u.first_name, u.last_name, u.identification, u.phone, u.email, u.status_id, d.dealer_id
			FROM ludus_invitation i, ludus_schedule s, ludus_users u, ludus_headquarters h, ludus_dealers d
			WHERE i.schedule_id = s.schedule_id  AND i.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND s.status_id = 1
			AND s.start_date BETWEEN '$fecha 00:00:0' AND '$fecha 23:59:59'";

	$alumnos = $DataBase_Acciones->SQL_SelectMultipleRows( $query_alumnos );

	foreach ($lideres as $key => $lid) {
		echo("<br>".$lid['email']."<br>");
		$cont_prog = 0;
		$destinos = "";
		$titulo = "Programación $fecha ".$lid['dealer'];
		$para = $lid['email'];
		// $para = "a.vega@luduscolombia.com.co, diegolamprea@gmail.com, zoraya.ordonez@gm.com";
		$mensaje_cuerpo = '<div style="background-color: white;">
								<table class="table">
									<thead>
										<tr>
											<th>Tipo</th>
											<th>Curso</th>
											<th>Instructor</th>
											<th>F. Inicio</th>
											<th>F. Fin</th>
											<th>Cupos</th>
											<th>Inscritos</th>
										</tr>
									</thead>
									<tbody class="prueba">';
		foreach ($prog as $key => $valProg) {
			if ( $lid['dealer_id'] == $valProg['dealer_id'] ){
				$cont_prog ++;
				$mensaje_cuerpo .= "<tr>";
				$mensaje_cuerpo .= "<td>".$valProg['type']."</td>";
				$mensaje_cuerpo .= "<td>".$valProg['course']."</td>";
				$mensaje_cuerpo .= "<td>".$valProg['teacher_f_name']." ".$valProg['teacher_ls_name']."</td>";
				$mensaje_cuerpo .= "<td>".$valProg['start_date']."</td>";
				$mensaje_cuerpo .= "<td>".$valProg['end_date']."</td>";
				$mensaje_cuerpo .= "<td> min ".$valProg['min_size']." - max ".$valProg['max_size']."</td>";
				$mensaje_cuerpo .= "<td>".$valProg['inscriptions']."</td>";
				$mensaje_cuerpo .= "</tr>";

				if( true ){
					$mensaje_cuerpo .= '<tr>
											<td colspan="6">';
					$mensaje_cuerpo .= '<table class="table" style="width:90%;margin: auto;background-color: silver;">
											<tr>
												<th>Documento</th>
												<th>Nombre</th>
												<th>Telefono</th>
												<th>Correo</th>
											</tr>';
					foreach ($alumnos as $key => $valAlm) {
						if( $lid['dealer_id'] == $valAlm['dealer_id'] && $valProg['schedule_id'] == $valAlm['schedule_id'] ){
							$mensaje_cuerpo .= "<tr>";
							$mensaje_cuerpo .= "<td>".$valAlm['identification']."</td>";
							$mensaje_cuerpo .= "<td>".$valAlm['first_name']." ".$valAlm['last_name']."</td>";
							$mensaje_cuerpo .= "<td>".$valAlm['phone']."</td>";
							$mensaje_cuerpo .= "<td>".$valAlm['email']."</td>";
							$mensaje_cuerpo .= "</tr>";
						}
					}//for que recorre los alumnos invitados por schedule
					$mensaje_cuerpo .= "</table>";
					$mensaje_cuerpo .= "</td>
									</tr>";
				}

			}
		}//for que separa y lista las diferentes schedule
		$mensaje_cuerpo .= "</tbody>
						</table>
					</div><br>";
		echo( $mensaje_cuerpo );

		if( $cont_prog > 0 ){
			$resultado = $email->enviaNewEmail($titulo,$para.",zoraya.ordonez@gm.com,zoraya@gmacademy.co",$mensaje_cuerpo,$destinos, "Programaciones GMAcademy <programaciones@gmacademy.co>");
		}

		// break;
	}//for que separa por lider GMAcademy


?>
