<?php
if( isset( $_GET['opcn'] ) && $_GET['opcn'] == 'a' ){
    include_once('../config/config.php');
    include_once('../config/database.php');
    $DataBase_Acciones = new Database();

    $update = "UPDATE ludus_users SET confirmed = 1 WHERE user_id = {$_GET['usi']}";
    $resultado = $DataBase_Acciones->SQL_Update( $update );
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Activacion</title>
</head>
<body>
    <script type="text/javascript">
        alert("Tu correo ha sido confirmado, por favor selecciona aceptar para continuar");
        window.location = "https://www.gmacademy.co";
    </script>
</body>
</html>
