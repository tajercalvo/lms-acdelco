<?php include('src/seguridad.php'); ?>
<?php
$_GET['opcn'] = 'ver';
$_GET['id'] = $_GET['course_id'];
$id_usuario = 0;
if(isset($_GET['user_id'])){
	$id_usuario = $_GET['user_id'];
	include_once('models/usuarios.php');
	$usuario_Class_CS = new Usuarios();
	$datos_Usr = $usuario_Class_CS->consultaDatosUsuario($id_usuario);
	$name_usr = $datos_Usr['first_name'].' '.$datos_Usr['last_name'];
	$identification_usr = $datos_Usr['identification'];
	unset($usuario_Class_CS);
}else{
	$name_usr = $_SESSION['NameUsuario'];
	$identification_usr = $_SESSION['identification'];
}
include('controllers/cursos.php');
/*Confirma que haya pasado el curso*/

// if($confirmaResultado == 0){

// 	header("location: login.php");
// }
$tieneTID = 0;
if(isset($TrayectoriaTID)){
	$tieneTID = $TrayectoriaTID;
}
/*Confirma que haya pasado el curso*/
// 137 - CID CONSULTOR INTEGRAL DIESEL
// 31 - ASESOR TECNICO DIESEL
/*funciones
--Al español fechas.
*/
	$date = $_GET['date'];
	$mes = date('F', strtotime(str_replace('-','/', $date)));
	$ano = date('Y', strtotime(str_replace('-','/', $date)));
	$dia_m = date('D', strtotime(str_replace('-','/', $date)));
	$dia_m = str_replace('Sun', 'Domingo', $dia_m);
	$dia_m = str_replace('Mon', 'Lunes', $dia_m);
	$dia_m = str_replace('Tue', 'Martes', $dia_m);
	$dia_m = str_replace('Wed', 'Mi&eacute;rcoles', $dia_m);
	$dia_m = str_replace('thu', 'Jueves', $dia_m);
	$dia_m = str_replace('Fri', 'Viernes', $dia_m);
	$dia_m = str_replace('Sat', 'S&aacute;bado', $dia_m);

	$mes = str_replace('January', 'Enero', $mes);
	$mes = str_replace('February', 'Febrero', $mes);
	$mes = str_replace('March', 'Marzo', $mes);
	$mes = str_replace('April', 'Abril', $mes);
	$mes = str_replace('May', 'Mayo', $mes);
	$mes = str_replace('June', 'Junio', $mes);
	$mes = str_replace('July', 'Julio', $mes);
	$mes = str_replace('August', 'Agosto', $mes);
	$mes = str_replace('September', 'Septiembre', $mes);
	$mes = str_replace('October', 'Octubre', $mes);
	$mes = str_replace('November', 'Noviembre', $mes);
	$mes = str_replace('December', 'Diciembre', $mes);

	$dia = date('d', strtotime(str_replace('-','/', $date)));
	$miFecha = $dia_m.' '.$dia.' de '.$mes.' de '.$ano;
/*funciones
--Al español fechas.
*/

$duration = 0;
if(isset($registroConfiguracionModulosDetail)){
	 
	foreach ($registroConfiguracionModulosDetail as $iID => $data_modules) {
		$duration = $data_modules['duration_time']+$duration;
	}


	// print_r($registroConfiguracionDetail);return;
	if($registroConfiguracionDetail['type']=="WBT"){
		$tipo = "virtual ";

	}else{
		$tipo = "presencial ";
	}
}else{
	header("location: login.php");
}

?>
<page class="back">
	<style type="text/css">
		@font-face {
			font-family: LouisRegular;
			src: url('../assets/fonts/LouisRegular.ttf') format('truetype');
		}
		@font-face {
			font-family: LouisItalici;
			src: url('../assets/fonts/LouisItalic.ttf') format('truetype');
		}
		@font-face {
			font-family: DurantItalici;
			src: url('../assets/fonts/DurantItalic.ttf') format('truetype');
		}
		.back{
			background-color: #013C9E;
		}
		.cont_principal{
			width: 100%;
			height: 100%;
			background-color: white;
			background-image: url(../assets/images/FondoCertificadoACDelco-01.jpg);
			background: 100% 100%;
			background-repeat: no-repeat;
			
		}
		.cont_inter{
			width: 90%;
			height: 90%;
			/*border: 10px solid #f4bc00;*/
			/*border-bottom: 1px solid rgb(101, 106, 109);
			border-top: 1px solid rgb(101, 106, 109);
			border-right: 1px solid rgb(101, 106, 109);
			border-left: 1px solid rgb(101, 106, 109);*/
		}
		table{
			border-collapse: collapse;
		}
		.imgLogo{
			width: 160px;
		}
		.imgCert{
			width: 260px;
		}
		.certificado{
			text-align: center;
			font-family: LouisRegular;
			font-size: 30px;
			color: #f4bc00;
		}
		.borde{
			border: 10px solid #f4bc00;
		}
		.gmaca{
			text-align: center;
			font-size: 15px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			height: 25px;
		}
		.persona{
			text-align: center;
			font-family: LouisRegular;
			font-size: 45px;
			font-weight: bold;
			color: #013C9E;
			width: 90%;
			margin-left: 5%;
			margin-right: 5%;
			border-bottom: 1px solid rgb(101, 106, 109);
		}
		.cedula{
			text-align: center;
			font-size: 17px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			height: 50px;
		}
		.leyenda{
			text-align: center;
			font-size: 15px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			width: 90%;
			margin-left: 5%;
			margin-right: 5%;
		}
		.imgVamos{
			width: 180px;
		}
		.cargo{
			text-align: center;
			font-size: 10px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
		}
		.linea_sep{
			border-bottom: 1px solid #000000;
			width: 85%;
			margin-left: 10px;
		}
		.TextFirma{
			text-align: center;
			font-family: LouisItalici;
			vertical-align: top;

		}
		.imgFirma{
			width: 160px;
		}
	</style>
	<table align="center" width= "100%;" class="cont_principal" >
		<tr>
			<!-- <td class="" style="border: 10px solid #0058a3"> -->
			<td class="" style="border: 10px solid #0058a3">
				<table align="center" class="cont_inter" style="">
					<tr>
						<td align="center" colspan="5">
							<img src="../assets/images/LogoACDelco-05.png" class="" style="width: 50%">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="5" style="height: 20px;" >
						</td>
					</tr>
					<tr>
						<td align="center" colspan="5" >
							<!-- <img src="../assets/images/certificado_top.png" class="imgCert"> -->
							<p class="" style="text-transform: uppercase; font-size: 30px; color: #666666">certifica que:</p>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="5" style="height: 30px;" >
						</td>
					</tr>
					<tr>
						<td align="center" class="persona" colspan="5" style="color: #000000">
							<?php echo strtoupper(strtolower($name_usr));?>
						</td>
					</tr>
					<tr>
						<td align="center" class="cedula" colspan="5" style="color: #000000">
							<strong>C.C. <?php echo $identification_usr; ?></strong>
						</td>
					</tr>
					<tr>
						<td align="center" class="leyenda" colspan="5">
							Tom&oacute; el curso <?php echo($tipo); ?>correspondiente a <strong>"<?php echo($registroConfiguracionDetail['course']); ?>"</strong>, <?php if($tipo=="presencial"){ ?>con una duraci&oacute;n de <?php echo($duration); ?> horas <?php } ?>el d&iacute;a <?php echo $miFecha; ?>.<br>
							Bogot&aacute;, Colombia.
						</td>
					</tr>
					<tr>
						<td align="right" colspan="5" style="padding-right: 145px; height: 100px; vertical-align: top;" >
							<!-- <img src="../assets/images/vamoscontoda.png" class="imgVamos"> -->
						</td>
					</tr>
<?php if($tieneTID>0){?>
					<tr>
						<td width="33%" align="center" class="TextFirma">
							<img src="../assets/images/FIRMA_ALEXANDERWOLFF-01.png" class="imgFirma" ><br>
							<span class="linea_sep"></span><br>
							Alexander Wolff Delgadillo.<br>
							<!-- <span class="cargo"><br>PRESIDENTE</span> -->
						</td>
						<td width="33%" align="center" class="TextFirma" >
							<img src="../assets/images/firma7.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br><br>
							Maria Fernanda Bonilla.<br>
							<!-- <span class="cargo">Gerente de desarrollo de concesionarios<br>GM Colmotores</span> -->
						</td>
						<td width="33%" align="center" class="TextFirma">
							<img src="../assets/images/Firma_fernando.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Fernando Molina.<br>
							
						</td>
					</tr>
<?php }else{?>
					<tr>
						<td width="33%" align="center" class="TextFirma">
							<img src="../assets/images/FIRMA_ALEXANDERWOLFF-01.png" class="imgFirma" ><br>
							<span class="linea_sep"></span><br>
							Alexander Wolff Delgadillo.<br>
							<!-- <span class="cargo"><br>PRESIDENTE</span> -->
						</td>
						<td width="33%" align="center" class="TextFirma" >
							<img src="../assets/images/firma7.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br><br>
							Maria Fernanda Bonilla.<br>
							<!-- <span class="cargo">Gerente de desarrollo de concesionarios<br>GM Colmotores</span> -->
						</td>
						<td width="33%" align="center" class="TextFirma">
							<img src="../assets/images/Firma_fernando.png" class="imgFirma"><br>
							<!-- <span class="linea_sep"></span><br> -->
							Fernando Molina.<br>
							
						</td>
					</tr>
<?php }?>


					<tr>
						<td align="right" colspan="5" style="height: 25px;" >
							&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

</page>
