<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_general.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons stats"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Reporte General</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte General </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Este es el reporte general en donde se encuentra la información de Personas Vs Trayectorías académicas y su avance, es un reporte completo que tiene un nivel de entendimiento elevado y el recurso necesario para procesarlo también es elevado, sea muy consciente de su necesidad</h5></br>
								</div>
								<div class="col-md-2">
									<?php if ($cantidad_datos > 0) { ?>
										<h5><a href="rep_asistencia_total_excel.php?start_date_day=<?php echo ($_POST['start_date_day']); ?>&end_date_day=<?php echo ($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt"><i></i>Descargar Reporte</a>
											<h5>
											<?php } ?>

								</div>
							</div>
							<div class="row">
								<form action="rep_general_down.php" method="post" target="_blank">
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
											<div class="col-md-8 input-group date">
												<input autocomplete="off" class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Desde..." <?php if (isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>" <?php } ?> required />
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-4 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
											<div class="col-md-8 input-group date">
												<input autocomplete="off" class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..." <?php if (isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>" <?php } ?> required />
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>


										<div class="col-md-5">
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
										</div>
										<div class="col-md-10">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-2 control-label" for="cargos" style="padding-top:8px;">Trayectorias:</label>
												<div class="col-md-10 input-group date">
													<select style="width: 100%;" id="cargos" name="cargos[]">
														<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
														?>
															<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php if (isset($_GET['charge_id']) && ($_GET['charge_id'] == $Data_ActiveCharge['charge_id'])) { ?> selected="selected" <?php } ?>><?php echo $Data_ActiveCharge['charge']; ?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<div class="col-md-2">
										</div>
								</form>
							</div>
						</div>
					</div>
					<div class="row row-app">

					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_general.js"></script>
</body>

</html>