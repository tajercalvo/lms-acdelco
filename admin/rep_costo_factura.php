<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if( isset($_GET['factura']) && isset($_GET['year']) && isset($_GET['month']) ){
    include('controllers/rep_costo.php');
}
//echo('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Factura_Costo.xls");
header ("Content-Transfer-Encoding: binary");
?>

<?php
if( isset($_GET['factura']) && isset($_GET['year']) && isset($_GET['month']) ){

    $mes = "";
    $year = $_GET['year'];

    $meses = array('','ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
    for ($i=0; $i < sizeof($meses); $i++) { 
        if( $i == ltrim($_GET['month'],"0") ){
            $mes = $meses[$i];
        }
    }
    /*print_r($datosEspecialidad[0]);
    print_r($datosEspecialidad[1]);
    print_r($datosEspecialidad[2]);
    print_r($datosEspecialidad[3]);*/
?>
<head>
    <style type="text/css">
        table,thead,tbody,tfoot,tr,th,td,p { font-family:"Calibri"; font-size:x-small }
        a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
        a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
        comment { display:none;  } 
    </style>
</head>
<body>
<p></p>
<table cellspacing="0" border="0">
    <colgroup span="10" width="79"></colgroup>
    <tbody>
    <tr></tr>
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 2px solid #505050; border-left: 2px solid #505050; border-right: 2px solid #505050" colspan="9" align="center" valign="bottom" bgcolor="#D0CECE"><b><font color="black">COBRO DE ENTRENAMIENTO GMACADEMY <?php echo utf8_decode(($mes.' '.$year)); ?></font></b></td>
        </tr>
    <tr>
        <td height="21" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    <tr>
        <td height="21" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 2px solid #505050; border-bottom: 2px solid #505050; border-left: 2px solid #505050; border-right: 2px solid #505050" colspan="5" align="center" valign="bottom" bgcolor="#DEEBF7"><b><font color="black">Periodo facturado: <?php echo utf8_decode(($periodoInicial.' - '.$periodoFinal)); ?> </font></b></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    <tr>
        <td height="21" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
<?php
if(isset($datosCursos) AND (count($datosCursos)>0)){
    foreach ($datosCursos as $key => $datos_Cobros) {
?>
    <tr>
        <td height="21" align="center" valign="middle"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="center" valign="middle"><font color="black"><br></font></td>
        <td style="border-top: 2px solid #505050; border-bottom: 2px solid #505050; border-left: 2px solid #505050; border-right: 2px solid #505050" colspan="2" align="center" valign="middle"><b><font color="black">CONCESIONARIO</font></b></td>
        <td align="center" valign="middle"><b><font color="black"><br></font></b></td>
        <td align="center" valign="middle"><font color="black"><br></font></td>
        <td align="center" valign="middle"><font color="black"><br></font></td>
        <td style="border-top: 2px solid #505050; border-bottom: 2px solid #505050; border-left: 2px solid #505050; border-right: 2px solid #505050" colspan="2" align="center" valign="middle" bgcolor="#DEEBF7"><b><font color="black"> <?php echo utf8_decode(($datos_Cobros['dealer'])); ?> </font></b></td>
        <td style="border-right: 2px solid #505050" align="center" valign="middle"><font color="black"><br></font></td>
    </tr>
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
<?php
        if(isset($datosEspecialidad) AND (count($datosEspecialidad)>0)){
            foreach ($datosEspecialidad as $iID => $data) {
                if( $data['cobro'] != 0 || $data['descuento'] != 0 || $data['alimentacion'] != 0 || $data['prestamos'] != 0 || $data['excusas'] != 0 ) {
?>
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td colspan="4" align="left" valign="bottom"><b><font color="black"><?php echo utf8_decode(($data['specialty'])); ?></font></b></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    <!-- TOTAL DE COSTO DE CURSO -->
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="3" align="left" valign="bottom"><b><font color="black">ENTRENAMIENTO <?php echo utf8_decode(($data['specialty'])); ?></font></b></td>
        <td style="border-top: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="1" align="right" valign="bottom"><font color="#FF0000"><?php echo utf8_decode( $data['excusas']." EXCUSAS" ); ?></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="2" align="center" valign="bottom"><font color="black">$ <?php echo utf8_decode(($data['cobro'])); ?> </font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    <!-- TOTAL DE DESCUENTO PLAN SEDE -->
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-left: 1px solid #505050; " colspan="3" align="left" valign="bottom"><b><font color="black">DESCUENTO PLAN SEDE <?php echo utf8_decode(($data['specialty'])); ?> </font></b></td>
        <!-- <td style="border-top: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td> -->
        <td style="border-top: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="1" align="right" valign="bottom"><font color="#FF0000"><?php echo utf8_decode( $data['prestamos']." PRESTAMOS" ); ?></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="2" align="center" valign="bottom"><font color="black">$ <?php echo utf8_decode(($data['descuento'])); ?> </font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    <!-- TOTAL DE DESCUENTO ALIMENTACION -->
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-bottom: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="4" align="left" valign="bottom"><b><font color="black"><?php echo utf8_decode("DESCUENTO ALIMENTACIÓN ".$data['specialty']); ?></font></b></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-bottom: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="2" align="center" valign="bottom"><font color="black">$ <?php echo utf8_decode(($data['alimentacion'])); ?> </font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
<?php
    } } }
?>
    <!-- AJUSTE DE MES -->
    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-bottom: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="4" align="center" valign="bottom" ><b><font color="black">AJUSTE VALOR DE MES</font></b></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 1px solid #505050; border-bottom: 1px solid #505050; border-left: 1px solid #505050; border-right: 1px solid #505050" colspan="2" align="center" valign="bottom" ><font color="black">$ <?php echo utf8_decode(($datos_Cobros['ajustes'])); ?> </font></b></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>

    <tr>
        <td height="20" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td colspan="7" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>

    <!-- TOTAL DE FACTURA -->
    <tr>
        <td height="21" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 2px solid #505050; border-bottom: 2px solid #505050; border-left: 2px solid #505050; border-right: 2px solid #505050" colspan="4" align="center" valign="bottom" bgcolor="#DEEBF7"><b><font color="black">TOTAL A PAGAR</font></b></td>
        <td align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-top: 2px solid #505050; border-bottom: 2px solid #505050; border-left: 2px solid #505050; border-right: 2px solid #505050" colspan="2" align="center" valign="bottom" bgcolor="#DEEBF7">$ <?php echo utf8_decode( ($datos_Cobros['cobro']-$datos_Cobros['descuento']-$datos_Cobros['alimentacion'])+($datos_Cobros['ajustes']) ); ?> </font></b></td>
        <td style="border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    <tr>
<?php
    } }
?>
        <td height="21" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050; border-left: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
        <td style="border-bottom: 2px solid #505050; border-right: 2px solid #505050" align="left" valign="bottom"><font color="black"><br></font></td>
    </tr>
    </tbody>
</table>
</body>
<?php } ?>
