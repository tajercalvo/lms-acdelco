<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_cursos_virtuales.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons display"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_cursos_virtuales.php">Reporte de Cursos Virtuales</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Cursos Virtuales</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de las evaluaciones y el rango de fechas.</h5></br>
								</div>
								<div class="col-md-2">
									<?php if($cantidad_datos > 0){ ?>
										<h5><a href="rep_cursos_virtuales_excel.php?start_date_day=<?php echo($p['start_date_day']); ?>&end_date_day=<?php echo($p['end_date_day']); ?>&dealer_id=<?php echo($p['dealer_id']); ?>&course_id=<?php echo($p['course_id']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									<?php } ?>
								</div>
							</div>
							<!-- ============================================================= -->
							<!-- Formulario -->
							<!-- ============================================================= -->
							<form action="rep_cursos_virtuales.php" method="post">
								<div class="row">
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
											<div class="col-md-7 input-group date">
												<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final:</label>
											<div class="col-md-7 input-group date">
												<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-2">
								<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
							</div>
								</div>
								<div class="row">
									<!-- ============================================================= -->
									<!-- Cursos virtuales -->
									<!-- ============================================================= -->
									<div class="col-md-5">
										<label for="course_id">Curso: </label>
										<select style="width: 100%;" id="course_id" name="course_id" >
											<option value="0">Todos los cursos</option>
											<?php foreach ($listaCursos as $key => $lc): ?>
												<?php $selected_course = $lc['course_id'] == $course_id ? "selected" : ""; ?>
												<option value="<?php echo $lc['course_id'] ; ?>" <?php echo $selected_course; ?> ><?php echo $lc['course']; ?></option>
											<?php endforeach; //foreach de los cursos ?>
										</select>
									</div>
									<!-- ============================================================= -->
									<!-- END Cursos virtuales -->
									<!-- ============================================================= -->

									<!-- ============================================================= -->
									<!-- Concesionarios -->
									<!-- ============================================================= -->
									<?php if ( isset($_SESSION['max_rol']) && $_SESSION['max_rol'] >= 5 ): ?>
										<div class="col-md-5">
											<label for="dealer_id">Concesionarios: </label>
											<select multiple="multiple" style="width: 100%;" id="dealer_id" name="dealer_id[]" >
												<optgroup label="Concesionarios">
													<?php foreach ($listaConcesionarios as $key => $lccs) {
														$selected = "";
														if( isset($seleccionados) && count($seleccionados)>0 ){
															foreach ($seleccionados as $key => $value){
																if( $value == $lccs['dealer_id'] ){
																	$selected = "selected";
																	break;
																}
															}//fin foreach, recorre los concesionarios seleccionados
														} //fin if isset ?>
														<option value="<?php echo($lccs['dealer_id']); ?>" <?php echo($selected); ?> ><?php echo($lccs['dealer']); ?></option>
													<?php }//fin lista concesionarios ?>
												</optgroup>
											</select>
										</div>
									<?php endif; ?>
									<!-- ============================================================= -->
									<!-- END Concesionarios -->
									<!-- ============================================================= -->
								</div>
							</form>
							<!-- ============================================================= -->
							<!-- END Formulario -->
							<!-- ============================================================= -->
						</div>
					</div>
					<?php if ($cantidad_datos > 0): ?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td class="right">
											<p class="lead">Respuestas</p>
											<!-- ============================================================= -->
											<!-- Tabla -->
											<!-- ============================================================= -->
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center">CODIGO</th>
														<th class="center">CURSO</th>
														<th class="center">CONCESIONARIO</th>
														<th class="center">SEDE</th>
														<th class="center">ALUMNO</th>
														<th class="center">IDENTIFICACIÓN</th>
														<th class="center" title="Intento Actual">INTENTO</th>
														<th class="center" title="Intentos de este periodo">#INTENTOS</th>
														<th class="center">FECHA</th>
														<th class="center">PUNTAJE</th>
														<th class="center">APROBACIÓN</th>
														<th class="center">DATOS</th>
														<th class="center">AVANCE</th>
														<th class="center" title="Supero el curso anteriormente con este puntaje">HISTORICO</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php
													foreach ($datosCursos_all as $iID => $data) {
														$Var_Superado = 0;
														$Var_Fecha = "En Proceso";
														$Var_Res = "No Aprobado";
														$_varEstilo = "label-danger";
														if( isset($data['respuestas']) ){
															$Var_Superado = $data['respuestas'][0]['puntaje'];
															$Var_Fecha = $data['respuestas'][0]['date_creation'];
															if($Var_Superado>79){
																$Var_Res = "Aprobado";
																$_varEstilo = "label-success";
															}
														}
														$pase_vr = 0;
														if(isset($data['pase'])){
															$pase_vr = $data['pase'];
														}
														$cuantos_vr = 0;
														if(isset($data['cuantos'])){
															$cuantos_vr = $data['cuantos'];
														}
														?>
														<!-- Item -->
														<tr class="selectable">
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['newcode']); ?></td>
															<td class="important" style="padding: 2px; font-size: 70%;" align="left"><?php echo($data['course']); ?></td>
															<td class="important" style="padding: 2px; font-size: 70%;" align="left"><?php echo($data['dealer']); ?></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['headquarter']); ?></td>
															<td class="important" style="padding: 2px; font-size: 70%;" align="left"><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['identification']); ?></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['attempt']); ?></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['cantAttempt']); ?></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php echo($_varEstilo); ?>"><?php echo($Var_Fecha); ?></span></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php echo($_varEstilo); ?>"><?php echo($Var_Superado); ?></span></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php echo($_varEstilo); ?>"><?php echo($Var_Res); ?></span></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php echo($_varEstilo); ?>">[ <?php echo($pase_vr); ?> | <?php echo($cuantos_vr); ?> ]</span></td>
															<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?if($Var_Res=='Aprobado'){ ?>100<?php }else if($pase_vr>0){ echo(number_format($pase_vr/$cuantos_vr,2)*100); }else{ echo("0"); }?> %</td>
															<?php if(isset($data['histo'])){?>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label label-success"><?php print_r($data['histo']['score']); ?></span></td>
															<?php }else{?>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"></td>
															<?php }?>
														</tr>
														<!-- // Item END -->
														<?php
													}
													?>
												</tbody>
											</table>
											<!-- ============================================================= -->
											<!-- END Tabla -->
											<!-- ============================================================= -->
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php else: ?>
						<div class="well">
						</div>
					<?php endif; ?>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_cursos_virtuales.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
