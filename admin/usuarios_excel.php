<?php
include('src/seguridad.php');
include_once('models/usuarios.php');
$usuario_Class_d = new Usuarios();
$datosUsuarios_Down = $usuario_Class_d->consultaDatosUsuarios_Down();
/*header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Usuarios_AutoTrain.xls");*/
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Usuarios_ACDelco.csv');
echo("user_id;ID;APELLIDOS;NOMBRES;EMPRESA;ZONA;SEDE;TRAYECTORIA;CATEGORIA;EMAIL;TELEFONO;NACIMIENTO;EDAD;GENERO;ESTADO;FECHA ULTIMA MODIFICACION;INGRESO LUDUS\n");
foreach ($datosUsuarios_Down as $iID => $aInfo_Data) {
                if($aInfo_Data['status_id']==1){
                    $val_estado = 'Activo';
                }else{
                    $val_estado = 'Inactivo';
                }
                $gender = 'No Definido';
                if($aInfo_Data['gender']=='1'){
                    $gender = 'Masculino';
                }else if($aInfo_Data['gender']=='2'){
                    $gender = 'Femenino';
                }

                $cargos_det = "";
                foreach ($aInfo_Data['cargos'] as $iID => $aCarg) {
                    $cargos_det .= $aCarg['charge']." - ";

                }
                $cargos_det = substr($cargos_det, 0, -3);
                $categoria = isset($aInfo_Data['cargos'][0]['category']) ? $aInfo_Data['cargos'][0]['category'] : "" ;
    echo(utf8_decode($aInfo_Data['user_id'].";".$aInfo_Data['identification'].";".$aInfo_Data['last_name'].";".$aInfo_Data['first_name'].";".$aInfo_Data['dealer'].";".$aInfo_Data['zone'].";".$aInfo_Data['headquarter'].";".$cargos_det.";".$categoria.";".str_replace(';', ',', $aInfo_Data['email']).";".$aInfo_Data['phone'].";".$aInfo_Data['date_birthday'].";".$aInfo_Data['edad'].";".$gender.";".$val_estado.";".$aInfo_Data['date_edition'].";".$aInfo_Data['date_creation']."\n"));
}
?>
