<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_sincalificar.php');
$location = 'reporting';
$locData = true;
?>
<?php if (!isset($_POST['vista'])){?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>

				<!-- Inicio contenido para cargar  -->
				<div id="contenido_para_cargar">

				<?php }//Fin !isset ?>

				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Reporte de Asistencia</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Cursos Sin Calificar (Programaciones) </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Aquí encontrará el listado de cursos que no cuentan con calificación por parte del Instructor asignado a la sesión que aquí se describe.</h5></br>
								</div>
								<div class="col-md-2">
									<?php if($cantidad_datos > 0){ ?>
										<h5><a href="rep_sincalificar_excel.php?schedule_id=<?php echo($_POST['schedule_id']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Información: </h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<!-- Table elements-->
							<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-hide="phone,tablet">#PROG</th>
										<th data-hide="phone,tablet">FECHA</th>
										<th data-hide="phone,tablet">C&Oacute;DIGO</th>
										<th data-hide="phone,tablet">CURSO</th>
										<th data-hide="phone,tablet">INSTRUCTOR</th>
										<th data-hide="phone,tablet">IDENTIFICACIÓN</th>
										<th data-hide="phone,tablet">SIN CALIFICAR</th>
										<th data-hide="phone,tablet">CALIFICAR</th>
									</tr>
								</thead>
								<!-- // Table heading END -->
								<tbody>
									<?php
										$cantidad_tot = 0;
									if($cantidad_datos > 0){
											foreach ($datosCursos_all as $iID => $data) {
												$cantidad_tot++;
												?>
										<tr>
											<td><?php echo($data['schedule_id']); ?></td>
											<td><?php echo($data['start_date']); ?></td>
											<td><?php echo($data['newcode']); ?></td>
											<td><?php echo($data['course']); ?></td>
											<td><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
											<td><?php echo($data['identification']); ?></td>
											<td align="center">
												<?= $data['numero']; ?>
											</td>
											<td align="center">
												<a href="calificaciones.php?schedule_id=<?php echo($data['schedule_id']); ?>&fecha_inicial=<?php echo($data['start_date']); ?>&fecha_final=<?php echo($data['end_date']); ?>" class="btn-action glyphicons pen btn-success"><i></i></a>
											</td>
										</tr>
									<?php } } ?>
								</tbody>
							</table>
							<!-- // Table elements END -->
							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de Cursos (Sesiones) sin calificar: <strong class='text-primary'><?php echo($cantidad_tot); ?></strong>
							</div>
							<!-- // Total elements END -->
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>

		<?php if (!isset($_POST['vista'])){?>

		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
</body>
</html>
</div>
<?php }else{}?>
