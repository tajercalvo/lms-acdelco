<?php include('src/seguridad.php'); ?>
<?php
$source = "vid";
//include('controllers/argumentarios.php');
include('controllers/videos_vct.php');
$location = 'virtuales';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>

</head>
<style>
	.modal {
		top: 45px !important;
	}
</style>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons display"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/list_courses.php">Cursos Virtuales</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Videos Cursos Virtuales
							<?php if (isset($_GET['id'])) { ?> - <?php echo ($Argumentario_datos['argument']); ?>
								<i class="fa fa-fw fa-thumbs-o-up"></i>
								<h2 style="font-weight:normal;color:#d65050 !important" id="CantLikes<?php echo ($Argumentario_datos['argument_id']); ?>"><?php echo (number_format($Argumentario_datos['likes'], 0)); ?></h2>
							<?php } ?>
						</h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno margin-none border-none padding-none -->
					<!-- Tabs -->
					<div class="relativeWrap">
						<div class="box-generic">

							<!-- Tabs Heading -->
							<!-- <div class="tabsbar tabsbar-2">
								<ul class="row row-merge">
									<li class="col-md-2 active glyphicons blacksmith"><a href="#tab1" data-toggle="tab"><i></i>H Blandas</a></li>
									<li class="col-md-2 glyphicons kiosk"><a href="#tab2" data-toggle="tab"><i></i> <span>H Técnicas</span></a></li>
									<li class="col-md-2 glyphicons check"><a href="#tab3" data-toggle="tab"><i></i> <span>Producto</span></a></li>
									<li class="col-md-2 glyphicons check"><a href="#tab4" data-toggle="tab"><i></i> <span>Ventas</span></a></li>
								</ul>
							</div> -->
							<!-- // Tabs Heading END -->

							<div class="tab-content">

								<!-- Tab content -->
								<div>
									<div class="widget widget-heading-simple">

										<div class="widget-body padding-none margin-none border-none">
											<div class="innerAll">
												<div>


													<br>

													<br>

													<div class="row" id="DivCursosDisponibles">
														<h4>Disponibles</h4>
														<?php

														$ct_row = 0;
														foreach ($Datos_vct_Videos as $iID => $DatosCursos_Dispo) {
															// echo "<pre>";
															// print_r($DatosCursos_Dispo);
															// echo "</pre>";
															$ct_row++;
															if ($DatosCursos_Dispo['checked'] == 'checked') {
														?>
																<div class="vervideos" onclick='vervideo("<?php echo $DatosCursos_Dispo['file']; ?>")'>
																	<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" data-schedule_id="<?php echo $DatosCursos_Dispo['schedule_id']; ?>" data-Completo="<?php echo strtoupper($DatosCursos_Dispo['description']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Dispo['detalle'], 0, 28)); ?>">
																		<div class="box-generic padding-none margin-none overflow-hidden">
																			<div class="relativeWrap overflow-hidden" style="height:150px">
																				<img src="../assets/Biblioteca_Cursos_VCT/imagenes/<?php echo $DatosCursos_Dispo['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																				<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																					<div class="media margin-none innerAll">
																						<div class="media-body text-white overflow-hidden">
																							<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Dispo['schedule_id']; ?>"><?php echo strtoupper($DatosCursos_Dispo['description']); ?></strong>
																							<p class="text-small margin-none">
																								<!-- <video src="../assets/Biblioteca_Cursos_VCT/<?php echo $DatosCursos_Dispo['file']; ?>" frameborder="0"></video>  -->
																								<!-- <img id="video" ></img>  -->

																							</p>

																						</div>
																					</div>
																				</div>
																			</div>

																		</div>
																	</div>
																</div>

														<?php }
														} ?>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- // Tab content END -->
							</div>


							<button type="button" id="btn_abrir_video" data-toggle="modal" data-target="#abrir_video" class="abrir_video btn btn-warning btn-xs" style="display:none"></button>

							<div class="modal" role="dialog" id="abrir_video">
								<div class="modal-dialog modal-dialog-centered" role="document" style="width: 50%">
									<div class="modal-content" style="top: 16px;background: #590d1a;">
										<div class="modal-body" style="background-image: url('https://autotrain.com.co/instagram/assets/img/fondo.jpg');">
											<h3 class="modal-title" style="text-transform: uppercase; text-align: center;color: #ffffff;">PERMISOS para visualizar videos</h3>

											<div style="text-align: center;">

												<video id="video" controls width="600" height="400"></video>
											</div>
										</div>


										<div class="modal-footer" style="background: #590d1a;">
											<button id="cerrar" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>

							</div>


						</div>
					</div>
					<!-- // Tabs END -->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<!-- 	<script>
		$(document).ready(function() {
			alert('hola')
		});
		function vervideo(){
			alert('si')
		}
  </script> -->
	<script src="js/vct_videos.js"></script>
</body>

</html>