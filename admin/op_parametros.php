<?php include('src/seguridad.php'); ?>
<?php include('controllers/parametros.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons sheriffs_star"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/parametros.php">Administración Parametros</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Administración Parametros </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-white">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
			</div>
			<div class="col-md-2">
				<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
					<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
				<?php }else{ ?>
					<h5><a href="parametros.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
				<?php } ?>
			</div>
		</div>	
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body innerAll inner-2x">
		<form id="formulario_data" method="post" autocomplete="off">
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-3 control-label" for="nombre" style="padding-top:8px;">Parametro:</label>
							<div class="col-md-9"><input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre de la opción" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['parameter']; ?>"<?php } ?> /></div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-4">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-3 control-label" for="valor" style="padding-top:8px;">Valor:</label>
							<div class="col-md-9"><input class="form-control" id="valor" name="valor" type="text" placeholder="Valor de la opción" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['value']; ?>"<?php } ?> /></div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-3">
					</div>
					<!-- // Column END -->
				</div>
			<!-- // Row END -->
			<div class="separator"></div>
					<!-- Form actions -->
					<div class="form-actions">
						<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
							<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
							<input type="hidden" id="opcn" name="opcn" value="crear">
						<?php }elseif(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
							<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
							<input type="hidden" id="opcn" name="opcn" value="editar">
							<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
						<?php } ?>
						<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
					</div>
					<!-- // Form actions END -->
		</form>
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/parametros.js"></script>
</body>
</html>