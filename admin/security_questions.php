<?php include('src/seguridad.php'); ?>
<?php include('controllers/security_questions.php');
$location = 'configuracion';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
//$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons lock"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/security_questions.php">Preguntas de Seguridad</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- contenido interno -->
	<div class="widget widget-heading-simple widget-body-gray">
		<div class="innerAll spacing-x2">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="widget ">
							<form id="formulario_data" method="post" autocomplete="off" action="security_questions.php">

							<!-- Category Heading -->
							<div class="innerAll half bg-gray border-bottom">
								<h4 class=" margin-bottom-none">Mis preguntas de seguridad</h4><br>
								<div class="clearfix">
									https://acdelco.com.co/lms/ es la herramienta de formación académico-practica que ACDelco ha desarrollado con cariño y profesionalismo para que usted pueda generar optimizaciones en los resultados de negocio relacionados con su puesto de trabajo. Con el objetivo de asegurar su proceso de formación agradecemos responder el siguiente banco de preguntas.<br><br>
									Recomendamos elegir cuidadosamente las respuestas, teniendo en cuenta que las preguntas aplicarán para verificar el usuario dentro de ciertos campos en la plataforma.
								</div>
							</div>
							<!-- End Category Heading -->
							<!-- Category Listing -->
							<div class="overflow-hidden">
								<div class="row innerAll half border-bottom">
									<div class="col-sm-9 col-xs-9">
										<ul class="media-list margin-none">

											<li class="media">
											    <!--   <a class="pull-left innerAll half " href="foros_tutor_detalle.php">
											  		<span class="empty-photo"><i class="fa fa-key fa-2x text-muted"></i></span>
											    </a> --> 
											    <div class="media-body">
												    <div class="innerAll half">
											    		<select style="width: 80%;" id="question_id1" name="question_id1">
											    				<option value="0">Seleccione una</option>
												    		<?php foreach( $Questions_datos1 as $key => $Data_Questions ){ ?>
																<option value="<?php echo($Data_Questions['security_question_id']); ?>" <?php if(isset($ActualQuestions[0]['security_question_id']) && $ActualQuestions[0]['security_question_id'] == $Data_Questions['security_question_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Questions['security_question']); ?></option>
															<?php } ?>
														</select>
														<?php if(isset($ActualQuestions[0]['security_question_id'])){ ?>
															<input type="hidden" value="<?php echo($ActualQuestions[0]['security_answer_id']); ?>" name="secAnswer1" id="secAnswer1" />
														<?php } ?>
												  		<div class="clearfix"></div>
												    	<small class="margin-none">
												    		<input style="width: 80%;" type="password" class="form-control" id="answer1" name="answer1" placeholder="Respuesta a la pregunta" autocomplete="off" value="<?php if(isset($ActualQuestions[0]['security_question_id'])){ echo($ActualQuestions[0]['answer']); } ?>" />
												    	</small>
											    	</div>
											    </div>
											</li>

											<li class="media">
											     <!-- <a class="pull-left innerAll half " href="foros_tutor_detalle.php">
											  		<span class="empty-photo"><i class="fa fa-key fa-2x text-muted"></i></span>
											    </a> -->
											    <div class="media-body">
												    <div class="innerAll half">
											    		<select style="width: 80%;" id="question_id2" name="question_id2">
											    				<option value="0">Seleccione una</option>
												    		<?php foreach( $Questions_datos2 as $key => $Data_Questions ){ ?>
																<option value="<?php echo($Data_Questions['security_question_id']); ?>" <?php if(isset($ActualQuestions[1]['security_question_id']) && $ActualQuestions[1]['security_question_id'] == $Data_Questions['security_question_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Questions['security_question']); ?></option>
															<?php } ?>
														</select>
														<?php if(isset($ActualQuestions[0]['security_question_id'])){ ?>
															<input type="hidden" value="<?php echo($ActualQuestions[1]['security_answer_id']); ?>" name="secAnswer2" id="secAnswer2" />
														<?php } ?>
												  		<div class="clearfix"></div>
												    	<small class="margin-none">
												    		<input style="width: 80%;" type="password" class="form-control" id="answer2" name="answer2" placeholder="Respuesta a la pregunta" value="<?php if(isset($ActualQuestions[1]['security_question_id'])){ echo($ActualQuestions[1]['answer']); } ?>" />
												    	</small>
											    	</div>
											    </div>
											</li>

											<li class="media">
											     <!-- <a class="pull-left innerAll half " href="foros_tutor_detalle.php">
											  		<span class="empty-photo"><i class="fa fa-key fa-2x text-muted"></i></span>
											    </a> -->
											    <div class="media-body">
												    <div class="innerAll half">
												    			<option value="0">Seleccione una</option>
											    		<select style="width: 80%;" id="question_id3" name="question_id3">
																<option value="0">Seleccione una</option>
												    		<?php foreach( $Questions_datos3 as $key => $Data_Questions ){ ?>
																<option value="<?php echo($Data_Questions['security_question_id']); ?>" <?php if(isset($ActualQuestions[2]['security_question_id']) && $ActualQuestions[2]['security_question_id'] == $Data_Questions['security_question_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Questions['security_question']); ?></option>
															<?php } ?>
														</select>
														<?php if(isset($ActualQuestions[0]['security_question_id'])){ ?>
															<input type="hidden" value="<?php echo($ActualQuestions[2]['security_answer_id']); ?>" name="secAnswer3" id="secAnswer3" />
														<?php } ?>
												  		<div class="clearfix"></div>
												    	<small class="margin-none">
												    		<input style="width: 80%;" type="password" class="form-control" id="answer3" name="answer3" placeholder="Respuesta a la pregunta" value="<?php if(isset($ActualQuestions[2]['security_question_id'])){ echo($ActualQuestions[2]['answer']); } ?>" />
												    	</small>
											    	</div>
											    </div>
											</li>

											<li class="media">
											     <!-- <a class="pull-left innerAll half " href="foros_tutor_detalle.php">
											  		<span class="empty-photo"><i class="fa fa-key fa-2x text-muted"></i></span>
											    </a> -->
											    <div class="media-body">
												    <div class="innerAll half">
											    		<select style="width: 80%;" id="question_id4" name="question_id4">
											    				<option value="0">Seleccione una</option>
												    		<?php foreach( $Questions_datos4 as $key => $Data_Questions ){ ?>
																<option value="<?php echo($Data_Questions['security_question_id']); ?>" <?php if(isset($ActualQuestions[3]['security_question_id']) && $ActualQuestions[3]['security_question_id'] == $Data_Questions['security_question_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Questions['security_question']); ?></option>
															<?php } ?>
														</select>
														<?php if(isset($ActualQuestions[0]['security_question_id'])){ ?>
															<input type="hidden" value="<?php echo($ActualQuestions[3]['security_answer_id']); ?>" name="secAnswer4" id="secAnswer4" />
														<?php } ?>
												  		<div class="clearfix"></div>
												    	<small class="margin-none">
												    		<input style="width: 80%;" type="password" class="form-control" id="answer4" name="answer4" placeholder="Respuesta a la pregunta" value="<?php if(isset($ActualQuestions[3]['security_question_id'])){ echo($ActualQuestions[3]['answer']); } ?>" />
												    	</small>
											    	</div>
											    </div>
											</li>

											<li class="media">
											     <!-- <a class="pull-left innerAll half " href="foros_tutor_detalle.php">
											  		<span class="empty-photo"><i class="fa fa-key fa-2x text-muted"></i></span>
											    </a> -->
											    <div class="media-body">
												    <div class="innerAll half">
											    		<select style="width: 80%;" id="question_id5" name="question_id5">
											    				<option value="0">Seleccione una</option>
												    		<?php foreach( $Questions_datos5 as $key => $Data_Questions ){ ?>
																<option value="<?php echo($Data_Questions['security_question_id']); ?>" <?php if(isset($ActualQuestions[4]['security_question_id']) && $ActualQuestions[4]['security_question_id'] == $Data_Questions['security_question_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Questions['security_question']); ?></option>
															<?php } ?>
														</select>
														<?php if(isset($ActualQuestions[0]['security_question_id'])){ ?>
															<input type="hidden" value="<?php echo($ActualQuestions[4]['security_answer_id']); ?>" name="secAnswer5" id="secAnswer5" />
														<?php } ?>
												  		<div class="clearfix"></div>
												    	<small class="margin-none">
												    		<input style="width: 80%;" type="password" class="form-control" id="answer5" name="answer5" placeholder="Respuesta a la pregunta" value="<?php if(isset($ActualQuestions[4]['security_question_id'])){ echo($ActualQuestions[4]['answer']); } ?>" />
												    	</small>
											    	</div>
											    </div>
											</li>

										</ul>
									</div>
								</div>
							</div>
							<!-- // END Category Listing -->
							<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-11">
									</div>
									<div class="col-md-1">
										<div class="form-actions">
										<?php if(isset($ActualQuestions[4]['security_question_id'])){ ?>
											<input type="hidden" id="opcn" name="opcn" value="editar">
											<button type="submit" class="btn btn-success" id="guardarEstudio"><i class="fa fa-check-circle"></i> Actualizar</button>
										<?php }else{?>
											<input type="hidden" id="opcn" name="opcn" value="crear">
											<button type="submit" class="btn btn-success" id="guardarEstudio"><i class="fa fa-check-circle"></i> Crear</button>
										<?php } ?>
										</div>
									</div>
								</div>
								<div class="separator bottom"></div>
							</form>
						</div>
						<!-- // END col-separator -->

					</div>
					<!-- // END col -->
				</div>
				<!-- // END row -->
		</div>
	</div>

						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/security_questions.js"></script>
</body>
</html>
