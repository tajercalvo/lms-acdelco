<?php include('src/seguridad.php'); ?>
<?php include('controllers/excusas_cupos.php');
$location = 'education';
$script_select2v4 = "select2";
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons list"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/excusas.php">Configuración Excusas Cupos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Excusas Cupos </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<h5><a href="excusas_cupos.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<div class="relativeWrap" >
							<div class="box-generic widget-tabs-gray">

								<!-- Tabs Heading -->
								<div class="tabsbar">
									<ul>
										<li class="glyphicons folder_open active"><a href="#tab2-3" data-toggle="tab"><i></i> Excusas Cupos </a></li>
									</ul>
								</div>
								<!-- // Tabs Heading END -->

								<div class="tab-content">

									<!-- ====================================================== -->
									<!-- Tab content Excusas -->
									<!-- ====================================================== -->
									<div class="tab-pane active" id="tab1-3">
										<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4><i></i> Agregar Excusa</h4><?php } ?>
										<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4><i></i> Editar Excusa</h4><?php } ?>

										<div class="widget-body innerAll inner-2x">
											<form id="formulario_data" method="post" autocomplete="off">
												<!-- Row -->
													<div class="row innerLR">
														<!-- Column -->
														<div class="col-md-7">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Curso:</label>
																<div class="col-md-10">
																	<select id="schedule_id" name="schedule_id" style="width: 100%;" required/>
																		<?php if ( isset($excusa['schedule_id']) && $_GET['opcn'] == "editar" ): ?>
																			<option value="<?php echo $excusa['schedule_id']; ?>" selected><?php echo $excusa['newcode']." | ".$excusa['course']." | ".$excusa['start_date']; ?></option>

																		<?php endif; ?>
																	</select>
																</div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
														<!-- Column -->
														<div class="col-md-5">
															<!-- Group -->
															<div class="form-group">
																<label class="col-md-3 control-label" for="dealer_id" style="padding-top:8px;">Concesionario:</label>
																<div class="col-md-9">
																	<select style="width: 100%;" id="dealer_id" name="dealer_id" required>
																		<?php if( $_GET['opcn'] == "editar" ): ?>
																			<?php foreach ( $dealers as $key => $d ) : ?>
																				<option value="<?php echo $d['dealer_id']; ?>" <?php echo $d['dealer_id'] == $excusa['dealer_id'] ? "selected" : "" ; ?>><?php echo $d['dealer']; ?></option>
																			<?php endforeach; ?>
																		<?php endif; ?>
																	</select>
																</div>
															</div>
															<!-- // Group END -->
														</div>
														<!-- // Column END -->
													</div>
													<!-- // Row END -->
													<br>
													<div class="row innerLR">
														<div class="col-md-7">
															<div class="form-group">
																<label class="col-md-2 control-label" for="type" style="padding-top:8px;">Tipo:</label>
																<div class="col-md-10">
																	<input type="text" id="type" name="type" value="cupos" class="form-control" readonly>
																	<!-- <select style="width: 100%;" id="type" name="type">
																		<option value=""></option>
																		<option value="cal_domestica" <?php if( isset($excusa) && isset($excusa['type']) && $excusa['type'] == "cal_domestica" ){ echo "selected"; } ?>>Calamidad domestica</option>
																		<option value="inc_medica" <?php if( isset($excusa) && isset($excusa['type']) && $excusa['type'] == "inc_medica" ){ echo "selected"; } ?>>Incapacidad médica</option>
																		<option value="otro" <?php if( isset($excusa) && isset($excusa['type']) && $excusa['type'] == "otro" ){ echo "selected"; } ?>>Otra</option>
																	</select> -->
																</div>
															</div>
														</div>
														<!-- Column -->
														<div class="col-md-5">
															<div class="form-group">
																<label class="col-md-3 control-label" for="num_invitation" style="padding-top:8px;">Número de cupos:</label>
																<div class="col-md-9">
																	<input type="number" min="0" class="form-control" name="num_invitation" id="num_invitation" required value="<?php echo isset($excusa['num_invitation']) ? $excusa['num_invitation']: ''; ?>">
																</div>
															</div>
														</div>
														<!-- // Column END -->
													</div>

													<br>
													<div class="row">
														<div class="col-md-12">
															<div class="form-group" id="espacio_texto">
																<label class="col-md-3 control-label" for="description" style="padding-top:8px;">Observaciones:</label>
																<div class="col-md-9">
																	<textarea class="form-control" name="description" id="description" rows="8" cols="80"><?php if( isset($excusa) && isset($excusa['description']) ){ echo $excusa['description']; } ?></textarea>
																</div>
															</div>
														</div>
													</div>
													<br>
													<!-- Row -->
													<div class="row innerLR">
														<div class="col-md-5">
																<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																  	<span class="btn btn-default btn-file">
																  		<span class="fileupload-new">Seleccionar Excusa (PDF Únicamente)</span>
																  		<span class="fileupload-exists">Cambiar Excusa</span>
																	  	<input type="file" class="margin-none" id="image_new" name="image_new"
																			<?php echo $_GET['opcn'] == "editar" && $excusa['file'] != "" ? '': 'required'; ?>/>
																	</span>
																  	<span class="fileupload-preview"></span>
																  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
																</div>
															</div>
														<!-- Column -->
														<div class="col-md-2">
														</div>
														<!-- // Column END -->
													</div>
													<br>

													<br>
													<div class="row">
														<div class="col-md-1"></div>
														<div class="col-md-10" style="<?php if( !isset($excusa) || !isset($excusa['file']) || $excusa['file'] == "" ){ echo "display:none"; } ?>">
															<iframe src="../assets/excusas_cupos/<?php echo $excusa['file']; ?>" style="width:700px; height:700px;" frameborder="0" id="pdf_view"></iframe>
														</div>
														<div class="col-md-1"></div>
													</div>
													<br>
													<?php if ( $_SESSION['max_rol'] >= 5 ): ?>

														<div class="row">
															<div class="col-md-5">
																<div class="form-group">
																	<label class="col-md-2 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
																	<div class="col-md-10">
																		<select style="width: 100%;" id="status_id" name="status_id">
																			<option value="1" <?php if( isset( $excusa['status_id'] ) && $excusa['status_id'] == 1 ){ echo "selected"; } ?>>Aprobar</option>
																			<option value="2" <?php if( isset( $excusa['status_id'] ) &&  $excusa['status_id'] == 2 ){ echo "selected"; } ?>>Pendiente</option>
																			<option value="3" <?php if( isset( $excusa['status_id'] ) &&  $excusa['status_id'] == 3 ){ echo "selected"; } ?>>Rechazar</option>
																		</select>
																	</div>
																</div>

															</div>
															<!-- EXCUSAS PARA COBOROS O PAC -->
															<div class="col-md-5">
																<div class="form-group">
																	<label class="col-md-2 control-label" for="aplica_para" style="padding-top:8px;">Aplica para:</label>
																	<div class="col-md-10">
																		<input type="text" class="form-control" id="aplica_para" name="aplica_para" value="EXCUSA" readonly>
																		<!-- <select style="width: 100%;" id="aplica_para" name="aplica_para">
																			<option value="" <?php if( isset( $excusa['aplica_para'] ) &&  $excusa['aplica_para'] == "" ){ echo "selected"; } ?>></option>
																			<option value="COBRO" <?php if( isset( $excusa['aplica_para'] ) &&  $excusa['aplica_para'] == "COBRO" ){ echo "selected"; } ?>>COBRO</option>
																			<option value="PAC" <?php if( isset( $excusa['aplica_para'] ) &&  $excusa['aplica_para'] == "PAC" ){ echo "selected"; } ?>>PAC</option>
																		</select> -->
																	</div>
																</div>
															</div>
														</div>
													<?php endif; ?>
													<?php if ( $_SESSION['max_rol'] < 5 ): ?>
													<input type="hidden" id="status_id" name="status_id" value="2">
													<input type="hidden" id="aplica_para" name="aplica_para" value="EXCUSA">
													<?php endif; ?>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Form actions -->
												<div class="form-actions">
													<input type="hidden" id="excuse_dealer_id" name="excuse_dealer_id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ""; ?>">
													<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
														<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
														<input type="hidden" id="opcn" name="opcn" value="crear">
														<button type="reset" name="button" id="btn_reset" style="display:none"></button>
													<?php } ?>
													<?php if ( isset($_GET['opcn']) && ($_GET['opcn']=='editar') ): ?>
														<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
														<input type="hidden" id="opcn" name="opcn" value="editar">
													<?php endif; ?>
												</div>
												<!-- // Form actions END -->
											</form>
										</div>

									</div>
									<!-- ====================================================== -->
									<!-- // END Tab content Excusas -->
									<!-- ====================================================== -->

									<!-- ====================================================== -->
									<!-- Tab content Cupos -->
									<!-- ====================================================== -->
									<?php if ( $_SESSION['max_rol'] >= 5 ): ?>
									<div class="tab-pane" id="tab2-3">
										<h4>Excusas cupos</h4>

										<div class="widget-body innerAll inner-2x">
											<form id="formulario_cupos" method="post" autocomplete="off">
												<!-- Row -->
												<div class="row innerLR">
													<!-- ====================================================== -->
													<!-- Cursos -->
													<!-- ====================================================== -->
													<div class="col-md-6">
														<div class="form-group">
														  <label for="cursos_cupos">Cursos</label>
														  <input type="text" style="width:100%" name="cursos_cupos" id="cursos_cupos">
														</div>
													</div>
													<!-- ====================================================== -->
													<!-- Cursos -->
													<!-- ====================================================== -->

													<!-- ====================================================== -->
													<!-- Sesiones -->
													<!-- ====================================================== -->
													<div class="col-md-6">
														<div class="form-group">
														  <label for="sesiones_cupos">Sesiones</label>
														  <select style="width:100%" name="sesiones_cupos" id="sesiones_cupos">
														  </select>
														</div>
													</div>
													<!-- ====================================================== -->
													<!-- Sesiones -->
													<!-- ====================================================== -->
												</div>
											</form>
										</div>
									</div>
									<?php endif; ?>
									<!-- ====================================================== -->
									<!-- // END Tab content Cupos -->
									<!-- ====================================================== -->

								</div>
							</div>
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->

				<!-- modal -->
				<a href="#modalRechazar" class="btn btn-success btn-xs" style="display: none;" data-toggle="modal" id="btnReabrirExcusax"><i class="fa fa-check"></i></a>
				<div class="modal fade" id="modalRechazar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cerrarBtnRExcusa">&times;</button>
								<h4 class="modal-title">Rechazar Excusa</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<h4 class="text-uppercase strong"><i class="fa fa-ticket text-regular fa-fw"></i> Rechazar Excusa</h4>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<h5 class="text-uppercase strong"><i class="fa fa-ticket text-primary fa-fw"></i> Observaciones </h5>
												<textarea placeholder="Rechazado por: " class="form-control col-md-12 wysihtml5" id="excusa_text_r" name="excusa_text_r" rows="7"></textarea>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" name="" class="btn btn-primary" id="btnRechazarExcusa"> Rechazar </button>
								<button type="reset" name="" id="limpiarReabrir" style="display: none">limpiar</button>
							</div>
						</div>
					</div>
				</div>
				<!-- fin modal -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/excusas_cupos.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
