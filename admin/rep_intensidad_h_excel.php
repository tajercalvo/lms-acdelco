<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
include('controllers/rep_intensidad_h.php');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_Intensidad_horaria.xls");
if( isset( $_datos ) ){ ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">TIPO</th>
                <th data-hide="phone,tablet">CÓDIGO</th>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet">ESPECIALIDAD</th>
                <th data-hide="phone,tablet">DURACIÓN</th>
                <th data-hide="phone,tablet">INSCRITOS</th>
                <th data-hide="phone,tablet">HORAS CAPACITACIÓN</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>

            <?php foreach ($_datos as $key => $data): ?>
                <tr>
                    <td><?php echo(utf8_decode($data['course'])); ?></td>
                    <td><?php echo(utf8_decode($data['course_type'])); ?></td>
                    <td><?php echo(utf8_decode($data['newcode'])); ?></td>
                    <td><?php echo(utf8_decode($data['dealer'])); ?></td>
                    <td><?php echo(utf8_decode($data['start_date'])); ?></td>
                    <td><?php echo(utf8_decode($data['specialty'])); ?></td>
                    <td><?php echo(utf8_decode($data['duration_time'])); ?></td>
                    <td><?php echo(utf8_decode($data['inscritos'])); ?></td>
                    <td><?php echo(utf8_decode($data['horas_cap'])); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php } ?>
