<?php

include_once( '../config/init_db.php' );

$cargos = DB::query( "SELECT c.charge_id, c.charge, c.group_id, 0 AS usuarios FROM ludus_charges c" );
$usuarios = DB::query( "SELECT u.user_id, cu.charge_id, YEAR( u.date_creation ) AS anio_inicio, MONTH( u.date_creation ) AS mes_inicio, YEAR( u.date_inactive ) AS anio_fin, MONTH( u.date_inactive ) AS mes_fin
        FROM ludus_users u, ludus_charges_users cu
        WHERE u.user_id = cu.user_id" );

$mes = [
    "1" => "ENERO", "2" => "FEBRERO", "3" => "MARZO", "4" => "ABRIL", "5" => "MAYO", "6" => "JUNIO",
    "7" => "JULIO", "8" => "AGOSTO", "9" => "SEPTIEMBRE", "10" => "OCTUBRE", "11" => "NOVIEMBRE", "12" => "DICIEMBRE"
];

$fechas = [];

for ($i= 2015; $i <= date('Y') ; $i++) {

    for ($j=1; $j <= 12 ; $j++) {
        $anio = ["anio" => $i, "mes" => $j, "cargos" => $cargos];
        $fechas[] = $anio;
    }
}

$cuenta = count($fechas);
$cargos_num = count($cargos);

foreach ( $usuarios as $key => $u ) {
    for ($i=0; $i < $cuenta ; $i++) {
        $f = $fechas[ $i ];

        // valida si el año de activación es menor al año a ver
        if ( $u['anio_inicio'] <= $f['anio'] ) {

            // si el año de ingreso es menor al año en curso
            if ( $u['anio_inicio'] < $f['anio'] ) {

                // valida si esta activo aún
                if ( $u['anio_fin'] == null ) {

                    // recorro los cargos segun año y mes
                    for ($j=0; $j < $cargos_num ; $j++) {
                        if ( $u['charge_id'] == $f['cargos'][$j]['charge_id'] ) {
                            $fechas[$i]['cargos'][$j]['usuarios'] = intval($fechas[$i]['cargos'][$j]['usuarios']) + 1;
                            break;
                        }
                    }

                }else if( $f['anio'] >= $u['anio_fin'] && $u['mes_fin'] >= $f['mes'] ){
                    // recorro los cargos segun año y mes
                    for ($j=0; $j < $cargos_num ; $j++) {
                        if ( $u['charge_id'] == $f['cargos'][$j]['charge_id'] ) {
                            $fechas[$i]['cargos'][$j]['usuarios'] = intval($fechas[$i]['cargos'][$j]['usuarios']) + 1;
                            break;
                        }
                    }
                }


            }elseif( $u['anio_inicio'] == $f['anio'] && $u['mes_inicio'] <= $f['mes'] ){

                // valida si esta activo aún
                if ( $u['anio_fin'] == null ) {

                    // recorro los cargos segun año y mes
                    for ($j=0; $j < $cargos_num ; $j++) {
                        if ( $u['charge_id'] == $f['cargos'][$j]['charge_id'] ) {
                            $fechas[$i]['cargos'][$j]['usuarios'] = intval($fechas[$i]['cargos'][$j]['usuarios']) + 1;
                            break;
                        }
                    }

                }else if( $f['anio'] <= $u['anio_fin'] && $u['mes_fin'] >= $f['mes'] ){
                    // recorro los cargos segun año y mes
                    for ($j=0; $j < $cargos_num ; $j++) {
                        if ( $u['charge_id'] == $f['cargos'][$j]['charge_id'] ) {
                            $fechas[$i]['cargos'][$j]['usuarios'] = intval($fechas[$i]['cargos'][$j]['usuarios']) + 1;
                            break;
                        }
                    }
                }

            }

        }// fin if validacion anio

    }// fin for
}// fin foreach

// echo json_encode( $fechas );
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_usuarios_historico_Total.xls");
header ("Content-Transfer-Encoding: binary");
?>
<table>
    <thead>
        <tr>
            <th>AÑO</th>
            <th>MES</th>
            <th>CARGO</th>
            <th>CATEGORIA</th>
            <th>USUARIOS</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ( $fechas as $key => $v ): ?>
            <?php foreach ($v['cargos'] as $key => $value): ?>
                <tr>
                    <td><?= $v['anio'] ?></td>
                    <td><?= $mes[$v['mes']] ?></td>
                    <td><?= $value['charge'] ?></td>
                    <td><?= $value['group_id'] ?></td>
                    <td><?= $value['usuarios'] ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
</table>
