<?php include('src/seguridad.php'); ?>
<?php
if (!isset($_GET['id'])) {
	if (isset($_SESSION['idUsuario'])) {
		$_GET['id'] = $_SESSION['idUsuario'];
	} else {
		header("location: login");
	}
}
include('controllers/encuestas_pendientes.php');
$location = 'biblioteca';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head><meta charset="gb18030">
	
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php">Actividades</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<!-- Sección biblioteca -->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">

								<div class="col-md-12 detailsWrapper">
									<div class="innerAll" style="padding: 0px;">
										<div class="body">
											<div class="row padding">
												<div class="col-md-12">
													<!-- // Inicio cuerpo de la pagina -->

													<div class="row">
														<h4 class="margin-none pull-left"> <a href="#Modalprogress_bar" data-toggle="modal" style=" color: black;" id="agregar"><i class="fa fa-plus"></i> Crear Encuesta </a>
														</h4>
													</div>
													<div clas="separator"></div>

													<div class="widget widget-heading-simple widget-body-white">
														<!-- Widget heading -->
														<div class="widget-head">
														</div>
														<div class="widget-head">
															<h4 class="heading glyphicons list"><i>Información:</i> <strong> </strong> Total de registros:<strong class="text-primary"> <?php echo count($encuestas); ?> </strong> </h4>
														</div>
														<!-- // Widget heading END -->
														<style type="text/css" media="screen">
															#tabla>thead>tr>th {
																background: white !important;
																color: #d65050 !important;
																text-align: center;
																border-top: 1px solid #d65050 !important;
																border-bottom: 1px solid #d65050 !important;
																text-transform: uppercase;
															}

															#tabla>tbody>tr>td {
																background: white !important;
																color: black !important;
																text-align: center;
																border: 1px solid lightgray !important;
																font-size: 10px;
															}

															#tabla>tbody>tr>tr:nth-child(odd) {
																background-color: red !important;
															}

															#tabla>tbody>tr>tr:nth-child(even) {
																background-color: green !important;
															}

															#tabla>tbody>tr>tr:hover {
																background-color: yellow;
															}

															#mitabla>tbody>tr>td {
																background: white !important;
																color: black !important;
																text-align: center;
																border: 1px solid lightgray !important;
																font-size: 10px;
															}

															.eliminar_pregunta, .agregar_pregunta {
																cursor: pointer;
															}
														</style>
														<div class="widget-body" style="border: none;">
															<!-- Table elements-->
															<table id="tabla" style="width: 100%;">
																<thead>
																	<tr>
																		<th>
																			<div style="border-right: 1px solid #d65050">ENCUESTA</div>
																		</th>
																		<th>
																			<div style="border-right: 1px solid #d65050">
																				PREGUNTAS
																			</div>
																		</th>
																		<th>
																			<div style="border-right: 1px solid #d65050">
																				EMPRESAS
																			</div>
																		</th>
																		<th>
																			<div style="border-right: 1px solid #d65050">
																				CURSOS
																			</div>
																		</th>
																		<th>
																			<div style="border-right: 1px solid #d65050">
																				TRAYECTORIAS
																			</div>
																		</th>
																		<th>
																			<div style="border-right: 1px solid #d65050">
																				FECHA INICIO - FECHA FIN
																			</div>
																		</th>
																		<th>
																			<div>

																			</div>
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<?php foreach ($encuestas as $key => $value) { ?>
																		<tr>
																			<td><?php echo $value['encuesta_id'] . ' - ' . $value['encuesta'] ?></td>
																			<td>
																				<?php foreach ($value['preguntas'] as $key2 => $value2) {
																					echo $value2['encuesta_pregunta'] . '<br>';
																				} ?>
																			</td>
																			<td>
																				<?php foreach ($value['empresas'] as $key2 => $value2) {
																					echo $value2['dealer'] . '<br>';
																				} ?>
																			</td>
																			<td>
																			<?php foreach ($value['cursos'] as $key2 => $value2) {
																					echo $value2['course'] . '<br>';
																				} ?>
																			</td>
																			<td>
																			<?php foreach ($value['cargos'] as $key2 => $value2) {
																					echo $value2['charge'] . '<br>';
																				} ?>
																			</td>
																			<td>
																				<?php if ($value['despliegue_mensual'] == 'SI'){
																						echo 'Se dezpliega mensualmente';
																				}else{
																					echo 'Fecha inicio: '.$value['fecha_inicio'].' <br>
																					Fecha fin: '.$value['fecha_fin'];
																				}
																				echo '<br>';
																				echo  $value['status_id'] == 1 ? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>';
																				?>
																			</td>
																			<td align="center"> <a style="color: black; text-decoration: underline; font-size: 12px;" href="#Modalprogress_bar" data-toggle="modal">
																					<p data-encuesta_id=<?php echo $value['encuesta_id'] ?> class="editar">EDITAR</p>
																				</a>
																			</td>
																		</tr>
																	<?php } ?>
															</table>
															<!-- // Table elements END -->
															<!-- Total elements-->
															<div class="form-inline separator bottom small">
																<?php if (isset($encuestas)) {
																	echo "Total de registros:";
																} ?><strong class='text-primary'>
																	<?php if (isset($encuestas)) {
																		echo count($encuestas);
																	} ?> </strong>
															</div>
															<!-- // Total elements END -->
														</div>
													</div>

													<!-- Inicio Modal carga de archivos con progressBar  -->
													<form id="frm" enctype="multipart/form-data" method="post">
														<div class="modal fade" id="Modalprogress_bar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															<div class="modal-dialog" style="z-index: 100000;">
																<div class="modal-content">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																		<h4 class="modal-title">Crear encuesta</h4>
																	</div>
																	<div class="modal-body">
																		<div class="widget widget-heading-simple widget-body-gray">
																			<div class="widget-body">
																				<!-- Row  -->
																				<div class="row">
																					<div class="col-md-12">
																						<label class="control-label">Encuesta: </label>
																						<input type="text" id="encuesta" name="encuesta" class="form-control col-md-8" />
																					</div>
																				</div>
																				<div class="clearfix"><br></div>
																				<div class="row">
																					<div class="col-md-11">
																						<label class="control-label">Pregunta: </label>
																						<textarea id="description" name="description" class="form-control col-md-8"></textarea>
																					</div>
																					<div class="col-md-1">
																						<br><br>
																						<span class="agregar_pregunta label label-success"><i class="fa fa-plus"></i></span>
																					</div>
																				</div>
																				<div class="clearfix"><br></div>
																				<div class="row">
																					<div class="col-md-12">
																						<table id="mitabla" style="width: 100%;">
																							<thead>
																								<th>Pregunta</th>
																								<th>Eliminar</th>
																							</thead>
																							<tbody>
																							</tbody>
																						</table>
																					</div>
																				</div>
																				<div class="clearfix"><br></div>
																				<!-- Row  -->
																				<div class="row">
																					<div class="col-md-12">
																						<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Empresas </h5>
																						<select multiple="multiple" style="width: 100%;" id="dealer_id" name="dealer_id[]">
																							<?php foreach ($empresas as $key => $Data_dealers) { ?>
																								<option value="<?php echo $Data_dealers['dealer_id']; ?>"><?php echo $Data_dealers['dealer']; ?></option>
																							<?php } ?>
																						</select>
																					</div>
																				</div>
																				<!-- Row END -->
																				<!-- Row  -->
																				<div class="row">
																					<div class="col-md-12">
																						<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i>Trayectoria </h5>
																						<select multiple="multiple" style="width: 100%;" id="charge_id" name="charge_id[]">
																							<?php foreach ($cargos as $key => $cargo) { ?>
																								<option value="<?php echo $cargo['charge_id']; ?>"><?php echo $cargo['charge']; ?></option>
																							<?php } ?>
																						</select>
																					</div>
																				</div>
																				<!-- Row END -->
																				<!-- Row  -->
																				<div class="row">
																					<div class="col-md-12">
																						<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Cursos </h5>
																						<select multiple="multiple" style="width: 100%;" id="course_id" name="course_id[]">
																							<?php foreach ($cursos as $key => $curso) { ?>
																								<option value="<?php echo $curso['course_id']; ?>"><?php echo $curso['course']; ?></option>
																							<?php } ?>
																						</select>
																					</div>
																				</div>
																				<!-- Row END -->
																				<div class="clearfix"><br></div>
																				<!-- Row  -->
																				<div class="row">
																					<div class="col-md-12">
																						<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Se despliega mensualmente? </h5>
																						<select style="width: 100%;" id="despliegue_mensual" name="despliegue_mensual">
																							<option value=""></option>
																							<option value="SI">SI</option>
																							<option value="NO">NO</option>
																						</select>
																					</div>
																				</div>
																				<!-- Row END -->
																				<div class="clearfix"><br></div>
																				<div class="row" id="fechas" style="display: none;">
																					<div class="col-md-6">
																						<label class="control-label">Fecha inicio: </label>
																						<input class="form-control" id="fecha_inicio" name="fecha_inicio" type="text" autocomplete="off" />
																					</div>
																					<div class="col-md-6">
																						<label class="control-label">Fecha fin: </label>
																						<input type="text" id="fecha_fin" name="fecha_fin" class="form-control col-md-8" autocomplete="off" />
																					</div>
																				</div>
																				<div class="clearfix"><br></div>
																				<!-- Row  -->
																				<div class="row">
																					<div class="col-md-12">
																						<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Estado </h5>
																						<select style="width: 100%;" id="estado" name="estado">
																							<option value=""></option>
																							<option value="1">Activo</option>
																							<option value="2">Inactivo</option>
																						</select>
																					</div>
																				</div>
																				<!-- Row END -->
																				<div class="clearfix"><br></div>
																				<div class="row">
																					<div class="col-md-12">
																						<a id="enviar" class="btn btn-primary pull-right">Enviar</a>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</form>
													<!-- Fin Modal carga de archivos con progressBar  -->
													<!-- // Fin modal editar archi-->
													<!-- inico modal visto por -->

													<div class="modal fade" id="Visto_por" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																	<h4 id="nombre_archivo_visto" class="modal-title"></h4>
																</div>
																<div class="modal-body">
																	<div class="widget widget-heading-simple widget-body-gray">
																		<div class="widget-body">
																			<br>
																			<br>
																			<!-- Row -->
																			<div class="row">

																				<div class="block block-inline">
																					<div class="caret"></div>
																					<div class="box-generic">
																						<div class="timeline-top-info content-filled border-bottom">
																							<i class="fa fa-user"></i> Subido por: <a href="#"><img src="../assets/images/usuarios/GM_LUDUS_20160905095329.jpg" id="imagen" alt="photo" width="20"></a> <a href="#"><label id="nombre_usuario" class="control-label"> </label></a>
																							<i class="fa fa-clock-o"></i> <label id="fecha" class="control-label"> </label>
																						</div>

																						<!-- <section id="miTablaaa"></section> -->

																						<div id="vistos"></div>


																					</div>

																				</div>

																			</div>

																			<!-- Row END-->
																			<div class="clearfix"><br></div>

																			<div class="clearfix"><br></div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<!-- // Fin cuerpo de la pagina -->
													<!-- // Inicio pie de pagina -->
													<div class="separator bottom"></div>
													<div class="row">
														<div class="col-md-3 center"></div>
														<div class="col-md-6 center">
														</div>
														<div class="col-md-3 center">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- // Fin class widget-heading  -->
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/encuesta_admin.js?v=<?php echo md5(time()); ?>"></script>
</body>

</html>