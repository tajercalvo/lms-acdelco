<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['course_id'])){
    include('controllers/rep_inscritos_cargos.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_InscritosCantidad.xls");
if(isset($_GET['course_id'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">SESION</th>
                <th data-hide="phone,tablet">PROFESOR</th>
                <th data-hide="phone,tablet">LUGAR</th>
                <th data-hide="phone,tablet">CANTIDAD</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php if($cantidad_datos > 0){ 
                        foreach ($datosCursos_all as $iID => $data) { ?>
                <tr>
                    <td><?php echo($data['course']); ?></td>
                    <td><?php echo($data['start_date']); ?></td>
                    <td><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
                    <td><?php echo($data['living']); ?></td>
                    <td><?php echo($data['cantidad']); ?></td>
                </tr>
                <?php } } ?>
        </tbody>
    </table>
<?php } ?>