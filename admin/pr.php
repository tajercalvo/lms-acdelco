<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_asistencia.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	
	<!-- Meta -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	
		
	<!-- 
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
	<link rel="stylesheet/less" href="../template/assets/less/admin/module.admin.page.dashboard_analytics.less" />
	-->

		<!--[if lt IE 9]><link rel="stylesheet" href="../template/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.dashboard_analytics.min.css" />
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


	<script src="../template/assets/components/library/jquery/jquery.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/jquery/jquery-migrate.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/modernizr/modernizr.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/less-js/less.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v2.3.0"></script>	<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
	
</head>
<body class="document-body ">
	
		<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		
				<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		<?php include('src/menu.php'); ?>
		<!-- Content -->
		<div id="content">
		<?php include('src/top_nav_bar.php'); ?>



				
<ul class="breadcrumb">
	<li>You are here</li>
	<li><a href="index.html?lang=en" class="glyphicons dashboard"><i></i> FLAT KIT</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Dashboard</li>
				<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Analytics</li>
			</ul>
				
	<div class="innerLR">
	<div class="innerB">				
		<h1 class="margin-none pull-left">Análisis de Datos &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h1>
		<h5>Periodo: 01/01/2015 - 07/07/2015</h5>
		<div class="clearfix"></div>
	</div>
				
	<div class="row">
		<div class="col-md-8">
			<!-- Widget -->
			<div class=" widget widget-body-white ">
				<div class="widget-body padding-none ">
					<div class="row row-merge border-bottom ">
						<div class="col-md-6 ">
							<div class="innerAll inner-2x text-center">
								<h5>Puntaje PAC Zona Bogotá</h5>
								<h4 class="text-medium text-primary text-condensed">100</h4>
							</div>
						</div>
						<div class="col-md-6">
							<div class="innerAll  inner-2x bg-gray text-center">
								<h5>Ranking</h5>
								<h4 class="text-medium text-primary text-condensed">5 de 12</h4>
							</div>
						</div>
					</div>
					<div class="  innerAll  ">
						<!-- Chart with lines and fill with no points -->
					<div id="chart_lines_fill_nopoints_2" class="flotchart-holder"></div>
					</div>
				</div>
			</div>
			<!-- //Widget -->

			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>

			<div class="row">
				<div class="col-md-6">
					<div class="widget innerAll text-center">
						<h4>Asistencia Vs Cupos</h4>
						<p class="innerTB inner-2x text-xlarge text-condensed strong text-primary">60%</p>
						<div class="sparkline " sparkHeight="62">4:6,7:3,5:5,6:4,3:7,7:3,5:5,6:4,2:8,3:7,7:3,5:5,8:2</div>
					</div>
				</div>



				<div class="col-md-6">
					<div class="widget widget-tabs widget-tabs-double-2  widget-tabs-responsive">
						<div class="widget-body">
							<div class="tab-content">
								<!-- Tab content -->
								<div id="tabReports" class="tab-pane active widget-body-regular innerAll inner-2x text-center">
									<div data-percent="85" data-size="95" class="easy-pie inline-block easy-pie-gender primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
										<div class="value text-center">
											<span class="strong"><i class="icon-graph-up-1 fa-3x text-primary"></i></span>
										</div>
									</div>
								</div>
								<!-- // Tab content END -->
								<!-- Tab content -->
								<div id="tabIncome" class="tab-pane widget-body-regular innerAll inner-2x text-center">
									Aun no se encuentra definido
								</div>
								<!-- // Tab content END -->
								<!-- Tab content -->
								<div id="tabAccounts" class="tab-pane widget-body-regular innerAll inner-2x text-center">
									Aun no se encuentra definido
								</div>
								<!-- // Tab content END -->
							</div>
						</div>
						<!-- Tabs Heading -->
						<div class="widget-head border-top-none border-bottom bg-gray">
							<ul>
								<li class="active"><a class="glyphicons notes" href="#tabReports" data-toggle="tab"><i></i><span>Formación</span></a></li>
								<li><a class="glyphicons credit_card" href="#tabIncome" data-toggle="tab"><i></i><span>Cursos</span></a></li>
								<li><a class="glyphicons user" href="#tabAccounts" data-toggle="tab"><i></i><span>Trayectorias</span></a></li>
							</ul>
						</div>
						<!-- // Tabs Heading END -->
					</div>
					<!-- //Widget -->
				</div>
				<!-- //Col -->
			</div>
			<!-- //Row -->
			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
			<!-- Widget	 -->
			<h2 class=" margin-none"><i class="fa fa-fw icon-wallet text-primary"></i> Oferta Académica</h2>
			<div class="widget widget-body-white widget-heading-simple overflow-hidden">
				<div class="widget-head height-auto">
				</div>
				<div class="widget-body innerAll">
					<!-- Horizontal Bars Chart -->
				<div id="chart_horizontal_bars" class="flotchart-holder"></div>




				</div>
			</div>
			<!-- //Widget -->
			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>

			<div class="row">
				<div class="col-md-12">
					<div class="widget">
						<div class="widget-body innerB half">
							<!-- Chart with lines and fill with no points -->
<div id="chart_mixed_1" class="flotchart-holder" style="height: 182px"></div>

						</div>
					</div>
				</div>
			</div>
			<!-- //Row -->

			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>

			<!-- Widget	 -->
			<h2 class="margin-none"><i class="fa fa-fw icon-star text-primary"></i> Trayectorias</h2>
			<div class="widget">
							
			
				<!-- Widget -->
				<div class="widget-body innerAll inner-2x">
					<table class="table table-striped margin-none">
						<thead>
							<tr>
								<th>Trayectoria</th>
								<th class="text-center"># Usuarios</th>
								<th class="text-right" style="width: 100px;">Promedio</th>
							</tr>
						</thead>
						<tbody>
																					<tr>
								<td><strong>1.</strong> JEFE DE TALLER</td>
								<td class="text-center"> 25</td>
								<td class="text-right">
									<div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#e5412d" sparkFillColor="" data-data="[433,247,105,160,355,380,294,150,172,280]">
									</div>
								</td>
							</tr>
																				<tr>
								<td><strong>2.</strong> GERENTE Y JEFE COMERCIAL</td>
								<td class="text-center"> 12</td>
								<td class="text-right">
									<div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#e5412d" sparkFillColor="" data-data="[122,226,424,146,308,472,415,491,343,361]">
									</div>
								</td>
							</tr>
																				<tr>
								<td><strong>3.</strong> GERENTE DE REPUESTOS</td>
								<td class="text-center"> 18</td>
								<td class="text-right">
									<div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#e5412d" sparkFillColor="" data-data="[389,174,165,458,450,223,379,253,453,229]">
									</div>
								</td>
							</tr>
																				<tr>
								<td><strong>4.</strong> GERENTE DIRECTOR DE TALENTO HUMANO</td>
								<td class="text-center"> 12</td>
								<td class="text-right">
									<div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#e5412d" sparkFillColor="" data-data="[317,169,311,185,326,180,109,215,241,406]">
									</div>
								</td>
							</tr>
																				<tr>
								<td><strong>5.</strong> GERENTE ADMINISTRATIVO</td>
								<td class="text-center"> 8</td>
								<td class="text-right">
									<div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#e5412d" sparkFillColor="" data-data="[362,268,222,314,134,325,119,307,285,370]">
									</div>
								</td>
							</tr>
																				<tr>
								<td><strong>6.</strong> GERENTE GENERAL</td>
								<td class="text-center"> 5</td>
								<td class="text-right">
									<div class="sparkline" style="width: 100px;" sparkHeight="20" sparkType="line" sparkWidth="100%" sparkLineWidth="2" sparkLineColor="#e5412d" sparkFillColor="" data-data="[406,273,102,411,296,168,425,226,386,335]">
									</div>
								</td>
							</tr>
												</tbody>
					</table>
				</div>
				<!-- // End Widget Body -->
			</div>
			<!-- // End Widget -->
		</div>
		<!-- //  End Col -->
					
		<div class="col-md-4">
			<!-- Widget -->
			<div class="widget widget-body-gray ">
				<div class="widget-body padding-none ">
					<div class="bg-primary innerAll">	
						<div class="text-large text-white pull-right">75 %</div>
						<h4 class="text-white strong text-medium margin-none">Cumplimiento</h4>
						<h5 class="text-white">Cumplimiento Hoy</h5>
						<div class="separator"></div>
						<div class="progress progress-mini  margin-none">
							<div class="progress-bar progress-bar-info	" style="width: 70%;"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- //End Widget -->

			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>

			<!-- Widget -->
			<div class="widget widget-body-gray ">
				<div class="widget-body padding-none ">
					<div class="bg-success innerAll">	
						<div class="text-large text-white pull-right">100 %</div>
						<h4 class="text-white strong text-medium margin-none">Asistencia</h4>
						<h5 class="text-white">Asistencia Hoy</h5>
						<div class="separator"></div>
						<div class="progress progress-mini  margin-none">
							<div class="progress-bar progress-bar-info	" style="width: 100%;"></div>
						</div>
					</div>
				</div>
			</div>
			<!-- //End Widget -->

			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
			
			<!-- Widget -->
			<div class="widget widget-body-white">
				<div class="widget-body padding-none">
					<div class="row row-merge">
						<div class="col-md-4 bg-gray">
							<div class="innerAll inner-2x text-center">
								<div class="sparkline" sparkHeight="65" data-colors="#cacaca, #5cc7dd,#609450,#cacaca">1,6,10,2</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="innerAll">
								<ul class="list-unstyled">
									<li class="innerAll half"><i class="fa fa-fw fa-square text-info"></i> <span class="strong">128</span> IBT</li>
									<li class="innerAll half"><i class="fa fa-fw fa-square text-success"></i> <span class="strong">205</span> WBT</li>
									<li class="innerAll half"><i class="fa fa-fw fa-square text-muted"></i> <span class="strong">4</span> MXT</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //End Widget -->
			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
		</div>
		<!-- //End Col -->
</div>
















	
	
		
		</div>
		<!-- // Content END -->
		
				</div>
		<div class="clearfix"></div>
		<?php include('src/footer.php'); ?>
		
	</div>
	<!-- // Main Container Fluid END -->
	


	<!-- Global -->
	<script>
	var basePath = '',
		commonPath = '../template/assets/',
		rootPath = '../',
		DEV = false,
		componentsPath = '../template/assets/components/';
	
	var primaryColor = '#e5412d',
		dangerColor = '#b55151',
		infoColor = '#5cc7dd',
		successColor = '#609450',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';
	
	var themerPrimaryColor = primaryColor;
	</script>
	
	<script src="../template/assets/components/library/bootstrap/js/bootstrap.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/breakpoints/breakpoints.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.resize.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/plugins/jquery.flot.tooltip.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotcharts.common.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-line-2.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-bars-horizontal.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-mixed-1.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/easy-pie/assets/lib/js/jquery.easy-pie-chart.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/easy-pie/assets/custom/easy-pie.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/sparkline/jquery.sparkline.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/sparkline/sparkline.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/maps/vector/assets/lib/jquery-jvectormap-1.2.2.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/maps/vector/assets/lib/maps/jquery-jvectormap-world-mill-en.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/maps/vector/assets/custom/maps-vector.world-map-markers.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.move/js/jquery.event.move.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.swipe/js/jquery.event.swipe.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/megamenu.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/core.init.js?v=v2.3.0"></script>	
</body>
</html>