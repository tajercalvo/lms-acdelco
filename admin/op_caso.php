<?php include('src/seguridad.php');
if(!$_SentinelColaborador && !$_SentinelAdmin && !$_SentinelGarantias && !$_SentinelCalidad){
	header("location: ../admin/");
}
?>
<?php include('controllers/casos.php');
$location = 'centinel';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb hidden-print">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons notes_2"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/casos.php">Administración de Casos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Casos </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-white">
	<div class="widget-body">
		<div class="row hidden-print">
			<?php
				$valOpciones = '';
				$valOpciones2 = '';
				$valOpciones3 = '';
				$valOpciones4 = '';
				$valOpciones5 = '';
				//print_r($registroConfiguracion);
				if(isset($_GET['opcn']) && ($_GET['opcn']=='editar'||$_GET['opcn']=='ver'||$_GET['opcn']=='vines'||$_GET['opcn']=='gestion'||$_GET['opcn']=='galeria') ){
					if($_SESSION['SentinelData']=='Admin' || ($_SESSION['SentinelData']=='Colab' && ($registroConfiguracion['status'] == 'En proceso' || $registroConfiguracion['status'] == 'Pendiente x info Concs.') ) ){
				    	$valOpciones = '<a title="Editar Caso" href="op_caso.php?opcn=editar&id='.$_GET['id'].'" class="btn-action glyphicons edit btn-success"><i></i> </a> Editar';
				    }
				    if(isset($_GET['id'])){
				    	$valOpciones2 = '';
				    	if($_SESSION['SentinelData']=='Admin' || ($_SESSION['SentinelData']=='Calidad') || ($_SESSION['SentinelData']=='Colab' && ($registroConfiguracion['status'] == 'En proceso' || $registroConfiguracion['status'] == 'Pendiente x info Concs.') ) ){
				    		$valOpciones2 = '<a title="Subir Archivos" href="op_caso.php?opcn=galeria&id='.$_GET['id'].'" class="btn-action glyphicons inbox_out btn-inverse"><i></i> </a> Archivos';
				    	}
				    	if($_SESSION['SentinelData']=='Admin' || ($_SESSION['SentinelData']=='Garantias' && ($registroConfiguracion['status'] == 'En Seguimiento'||$registroConfiguracion['status'] == 'En proceso'||$registroConfiguracion['status'] == 'Escalado a Calidad') && $registroConfiguracion['use_parts'] == "SI" ) || ($_SESSION['SentinelData']=='Calidad' && $registroConfiguracion['status'] == 'Escalado a Calidad') ){
				    		$valOpciones3 = '<a title="Gestionar Caso" href="op_caso.php?opcn=gestion&id='.$_GET['id'].'" class="btn-action glyphicons wallet btn-warning"><i></i> </a> Gestión';
				    	}
				    	$valOpciones4 = '<a title="Agregar Vin" href="op_caso.php?opcn=vines&id='.$_GET['id'].'" class="btn-action glyphicons cars btn-primary"><i></i> </a> Agregar Vin';

				    	$valOpciones5 = '<a title="Detalle del Caso" href="op_caso.php?opcn=ver&id='.$_GET['id'].'" class="btn-action glyphicons eye_open btn-danger"><i></i> </a> Detalle';
				    }
				} ?>
			<div class="col-md-1">
				<?php echo($valOpciones); ?>
			</div>
			<div class="col-md-1">
				<?php echo($valOpciones2); ?>
			</div>
			<div class="col-md-1">
				<?php echo($valOpciones3); ?>
			</div>
			<div class="col-md-1">
				<?php echo($valOpciones4); ?>
			</div>
			<div class="col-md-4">
				<?php echo($valOpciones5); ?>
			</div>
			<div class="col-md-2">
				<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
					<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
				<?php } ?>
			</div>
			<div class="col-md-2">
				<a href="#" class="glyphicons print" onclick="window.print(); return false;"><i></i> Print</a>
			</div>
		</div>
	</div>
</div>
<?php if( isset($_GET['opcn']) && ($_GET['opcn']=='nuevo'||$_GET['opcn']=='editar'||$_GET['opcn']=='ver') ){ ?>
	<div class="widget widget-heading-simple widget-body-white">
		<!-- Widget heading -->
		<div class="widget-head">
			<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
				<h4 class="heading glyphicons list"><i></i> Agregar ítem</h4>
			<?php } ?>
			<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?>
				<h4 class="heading glyphicons list"><i></i> Editar ítem</h4>
			<?php } ?>
		</div>
		<!-- // Widget heading END -->
		<div class="widget-body innerAll inner-2x">
			<form id="formulario_data" method="post" autocomplete="off">
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-2">
								<!-- // Group END -->
								<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')){ ?>
									<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
									  	<input type="file" class="margin-none" id="archivo_new" name="archivo_new" />
									</div>
						  		<?php } ?>
						</div>
						<!-- // Column END -->

						<!-- Column -->
						<div class="col-md-6">
							<!-- // Group END -->
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
								<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
								  	<input type="file" class="margin-none" id="image_new" name="image_new" />
								  	<br>
								</div>
								<div class="form-group">
							    	<button type="button" class="btn btn-primary" onclick="cargaImagen();"><i class="fa fa-check-circle"></i> Cambiar Fotografía</button>
							  	</div>
					  		<?php } ?>
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-2 center">
							<!-- Group -->
							<div class="form-group">
								<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
									<img id="ImagenReg" src="../assets/images/sentinel/<?php echo $registroConfiguracion['file']; ?>" style="width: 120px;" alt="<?php echo $registroConfiguracion['file']; ?>">
									<input type="hidden" id="imgant" name="imgant" value="<?php echo($registroConfiguracion['file']);?>">
									<div class="ajax-loading hide" id="loading_imagen">
										<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
									</div>
								<?php } ?>
							</div>
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-2">
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br/>
				<!-- Row -->
					<input type="hidden" value="NA" id="km_mot" name="km_mot"/>
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="date_sentinel" style="padding-top:8px;">Fecha Sentinel:</label>
								<div class="col-md-1"></div>
								<div class="col-md-5 input-group date">
							    	<input class="form-control" type="text" id="date_sentinel" name="date_sentinel" placeholder="Fecha del Sentinel" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['date_sentinel']); ?>" <?php } ?> onchange="CambiarFecha(); return false;"/>
							    	<span class="input-group-addon">
							    		<i class="fa fa-th"></i>
							    	</span>
								</div>
								<div class="col-md-1"></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="nro_ot" style="padding-top:8px;"># Orden OT:</label>
								<div class="col-md-7"><input class="form-control" id="nro_ot" name="nro_ot" type="text" placeholder="Número OT del Sentinel" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['nro_ot']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="date_ot" style="padding-top:8px;">Apertura OT:</label>
								<div class="col-md-1"></div>
								<div class="col-md-5 input-group date">
							    	<input class="form-control" type="text" id="date_ot" name="date_ot" placeholder="Apertura OT" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['date_ot']); ?>" <?php } ?>/>
							    	<span class="input-group-addon">
							    		<i class="fa fa-th"></i>
							    	</span>
								</div>
								<div class="col-md-1"></div>

							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" style="padding-top:8px;">Documentos:</label>
								<div class="col-md-2">
									<input type="checkbox" class="checkbox" value="SI" id="photos" name="photos" <?php if(isset($registroConfiguracion['photos'])&&$registroConfiguracion['photos']=="SI"){ ?> checked="checked" <?php } ?>/> Fotos
								</div>
								<div class="col-md-2">
									<input type="checkbox" class="checkbox" value="SI" id="video" name="video" <?php if(isset($registroConfiguracion['video'])&&$registroConfiguracion['video']=="SI"){ ?> checked="checked" <?php } ?>/> Video
								</div>
								<div class="col-md-3">
									<input type="checkbox" class="checkbox" value="SI" id="history" name="history" <?php if(isset($registroConfiguracion['history'])&&$registroConfiguracion['history']=="SI"){ ?> checked="checked" <?php } ?>/> Historial
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="sentinel_model_id" style="padding-top:8px;">Modelo:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="sentinel_model_id" name="sentinel_model_id">
										<?php foreach ($listadoModelos as $key => $Data_listados) { ?>
											<option value="<?php echo($Data_listados['sentinel_model_id']); ?>" <?php if(isset($registroConfiguracion['sentinel_model_id'])&&$registroConfiguracion['sentinel_model_id']==$Data_listados['sentinel_model_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_model']); ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="vin" style="padding-top:8px;"># Vin:</label>
								<div class="col-md-7"><input class="form-control" id="vin" name="vin" type="text" placeholder="Vin del Sentinel" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['vin']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="sentinel_application_id" style="padding-top:8px;">Aplicación:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="sentinel_application_id" name="sentinel_application_id">
										<?php foreach ($listadoAplicaciones as $key => $Data_listados) { ?>
											<option value="<?php echo($Data_listados['sentinel_application_id']); ?>" <?php if(isset($registroConfiguracion['sentinel_application_id'])&&$registroConfiguracion['sentinel_application_id']==$Data_listados['sentinel_application_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_application']); ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="km" style="padding-top:8px;">Kilometraje:</label>
								<div class="col-md-7"><input class="form-control" id="km" name="km" type="text" placeholder="Kilometraje del Sentinel" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['km']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="garranty_start" style="padding-top:8px;">Fecha de inicio de la Garantía:</label>
								<div class="col-md-1"></div>
								<div class="col-md-5 input-group date">
							    	<input class="form-control" type="text" id="garranty_start" name="garranty_start" placeholder="Fecha de inicio Garantía" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['garranty_start']); ?>" <?php } ?>/>
							    	<span class="input-group-addon">
							    		<i class="fa fa-th"></i>
							    	</span>
								</div>
								<div class="col-md-1"></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="sentinel_service_id" style="padding-top:8px;">Tipo de Servicio:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="sentinel_service_id" name="sentinel_service_id">
										<?php foreach ($listadoServicios as $key => $Data_listados) { ?>
											<option value="<?php echo($Data_listados['sentinel_service_id']); ?>" <?php if(isset($registroConfiguracion['sentinel_service_id'])&&$registroConfiguracion['sentinel_service_id']==$Data_listados['sentinel_service_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_service']); ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="transmission" style="padding-top:8px;">Transmisión:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="transmission" name="transmission">
										<option value="Manual" <?php if(isset($registroConfiguracion['transmission']) && $registroConfiguracion['transmission']=="Manual"){ ?>selected="selected"<?php } ?>>Manual</option>
										<option value="Automatica" <?php if(isset($registroConfiguracion['transmission']) && $registroConfiguracion['transmission']=="Automatica"){ ?>selected="selected"<?php } ?>>Automatica</option>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="configuration" style="padding-top:8px;">Configuración:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="configuration" name="configuration">
										<option value="4X2" <?php if(isset($registroConfiguracion['configuration']) && $registroConfiguracion['configuration']=="4X2"){ ?>selected="selected"<?php } ?>>4X2</option>
										<option value="4X4" <?php if(isset($registroConfiguracion['configuration']) && $registroConfiguracion['configuration']=="4X4"){ ?>selected="selected"<?php } ?>>4X4</option>
										<option value="AWD" <?php if(isset($registroConfiguracion['configuration']) && $registroConfiguracion['configuration']=="AWD"){ ?>selected="selected"<?php } ?>>AWD</option>
										<option value="FWD" <?php if(isset($registroConfiguracion['configuration']) && $registroConfiguracion['configuration']=="FWD"){ ?>selected="selected"<?php } ?>>FWD</option>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<p class="separator text-center"><i class="icon-car icon-2x"></i></p>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="sentinel_system_id" style="padding-top:8px;">Sistema Afectado:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="sentinel_system_id" name="sentinel_system_id" onchange="CambiaSerie();">
										<?php foreach ($listadoSystems as $key => $Data_listados) { ?>
											<option value="<?php echo($Data_listados['sentinel_system_id']); ?>" <?php if(isset($registroConfiguracion['sentinel_system_id'])&&$registroConfiguracion['sentinel_system_id']==$Data_listados['sentinel_system_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_system']); ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="nserie" style="padding-top:8px;"># Serie:</label>
								<div class="col-md-7"><input class="form-control" id="nserie" name="nserie" type="text" placeholder="Número de Motor" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['nserie']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="sentinel_check_id" style="padding-top:8px;">Testigo Check:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="sentinel_check_id" name="sentinel_check_id">
										<?php foreach ($listadoCheks as $key => $Data_listados) { ?>
											<option value="<?php echo($Data_listados['sentinel_check_id']); ?>" <?php if(isset($registroConfiguracion['sentinel_check_id'])&&$registroConfiguracion['sentinel_check_id']==$Data_listados['sentinel_check_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_check']); ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="fail_code" style="padding-top:8px;">Código de Falla (DTC):</label>
								<div class="col-md-7"><input class="form-control" id="fail_code" name="fail_code" type="text" placeholder="Codigo(s) de falla (DTC)" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['fail_code']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="model_year" style="padding-top:8px;">Año Modelo:</label>
								<div class="col-md-7"><input class="form-control" id="model_year" name="model_year" type="text" placeholder="2016" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['model_year']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="model_year" style="padding-top:8px;">Versión:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="car_version" name="car_version">
										<option value="LS" <?php if(isset($registroConfiguracion['car_version']) && $registroConfiguracion['car_version']=="LS"){ ?>selected="selected"<?php } ?>>LS</option>
										<option value="LT" <?php if(isset($registroConfiguracion['car_version']) && $registroConfiguracion['car_version']=="LT"){ ?>selected="selected"<?php } ?>>LT</option>
										<option value="LTZ" <?php if(isset($registroConfiguracion['car_version']) && $registroConfiguracion['car_version']=="LTZ"){ ?>selected="selected"<?php } ?>>LTZ</option>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="fail_code" style="padding-top:8px;">Ticket CAT:</label>
								<div class="col-md-7"><input class="form-control" id="cat_id" name="cat_id" type="text" placeholder="Ticket CAT" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['case_cat']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">

						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">

						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<p class="separator text-center"><i class="icon-car icon-2x"></i></p>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-12">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-2 control-label" for="customer_complaint" style="padding-top:8px;">Queja del cliente:</label>
								<div class="col-md-10">
									<textarea id="customer_complaint" name="customer_complaint" class="wysihtml5 col-md-12 form-control" rows="5" placeholder="Queja del cliente"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo($registroConfiguracion['customer_complaint']); } ?></textarea>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-12">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-2 control-label" for="diagnostic" style="padding-top:8px;">Diagnostico realizado:</label>
								<div class="col-md-10">
									<textarea id="diagnostic" name="diagnostic" class="wysihtml5 col-md-12 form-control" rows="5" placeholder="(Incluya los valores, medidas encontradas y comparadas con el SI ó manual de servicio)"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo($registroConfiguracion['diagnostic']); } ?></textarea>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-12">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-2 control-label" for="cause_failure" style="padding-top:8px;">Causa de la falla:<br>Posible o Sospechada</label>
								<div class="col-md-10">
									<textarea id="cause_failure" name="cause_failure" class="wysihtml5 col-md-12 form-control" rows="5" placeholder="(Encontrada o sospechada)"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo($registroConfiguracion['cause_failure']); } ?></textarea>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-12">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-2 control-label" for="correction" style="padding-top:8px;">Corrección:</label>
								<div class="col-md-10">
									<textarea id="correction" name="correction" class="wysihtml5 col-md-12 form-control" rows="5" placeholder="Corrección"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo($registroConfiguracion['correction']); } ?></textarea>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<p class="separator text-center"><i class="icon-car icon-2x"></i></p>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<div class="row innerLR">
								<label class="col-md-5 control-label" style="padding-top:8px;" for="use_parts">Uso Partes:</label>
								<div class="col-md-7">
									<input type="checkbox" class="checkbox" value="SI" id="use_parts" name="use_parts" <?php if(isset($registroConfiguracion['use_parts'])&&$registroConfiguracion['use_parts']=="SI"){ ?> checked="checked" <?php } ?>/>SI
								</div>
								</div>
								<?php if(isset($_GET['id'])){
								// if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
								<div class="row innerLR">
									<label class="col-md-5 control-label" style="padding-top:8px;" for="date_requ_cdg">Solicitud de Partes</label>
									<div class="col-md-7 input-group date">
										<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
									    	<input class="form-control" type="text" id="date_requ_cdg" name="date_requ_cdg" placeholder="Fecha solicitud" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['date_requ_cdg']); ?>" <?php } ?>/>
									    	<span class="input-group-addon">
									    		<i class="fa fa-th"></i>
									    	</span>
									    <?php }else if($_GET['opcn']=="editar"){ ?>
									    	<input type="hidden" name="date_requ_cdg" id="date_requ_cdg" value="<?php echo($registroConfiguracion['date_requ_cdg']); ?>">
									    	<?php echo($registroConfiguracion['date_requ_cdg']); ?>
									    <?php }?>
									</div>
								</div>
								<div class="row innerLR">
									<label class="col-md-5 control-label" style="padding-top:8px;" for="date_start_cdg">Llegada de Partes CDG</label>
									<div class="col-md-7 input-group date">
										<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias' ||$_SESSION['SentinelData']=='Calidad' ){?>
									    	<input class="form-control" type="text" id="date_start_cdg" name="date_start_cdg" placeholder="Fecha llegada CDG" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['date_start_cdg']); ?>" <?php } ?>/>
									    	<span class="input-group-addon">
									    		<i class="fa fa-th"></i>
									    	</span>
									    <?php }else if($_GET['opcn']=="editar"){ ?>
									    	<input type="hidden" name="date_start_cdg" id="date_start_cdg" value="<?php echo($registroConfiguracion['date_start_cdg']); ?>">
									    	<?php echo($registroConfiguracion['date_start_cdg']); ?>
									    <?php }?>
									</div>
								</div>

									<!--  -->
									<div class="row">
										<div class="row innerLR">
											<label class="col-md-5 control-label" style="padding-top:8px;" for="date_analysis_cdg">Análisis de Partes CDG</label>
											<div class="col-md-7 input-group date">
												<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
											    	<input class="form-control" type="text" id="date_analysis_cdg" name="date_analysis_cdg" placeholder="Fecha Análisis CDG" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['fec_analysis_part']); ?>" <?php } ?>/>
											    	<span class="input-group-addon">
											    		<i class="fa fa-th"></i>
											    	</span>
											    <?php }else if($_GET['opcn']=="editar"){ ?>
											    	<input type="hidden" name="date_analysis_cdg" id="date_analysis_cdg" value="<?php echo($registroConfiguracion['fec_analysis_part']); ?>">
											    	<?php echo($registroConfiguracion['fec_analysis_part']); ?>
											    <?php }?>
											</div>
										</div>
										<div class="col-md-7"></div>
									</div>

									<div class="row">
										<div class="row innerLR">
											<label class="col-md-5 control-label" style="padding-top:8px;" for="date_retirement_cdg">Retiro de Partes CDG</label>
											<div class="col-md-7 input-group date">
												<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
											    	<input class="form-control" type="text" id="date_retirement_cdg" name="date_retirement_cdg" placeholder="Fecha llegada CDG" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['fec_retirement_part']); ?>" <?php } ?>/>
											    	<span class="input-group-addon">
											    		<i class="fa fa-th"></i>
											    	</span>
											    <?php }else if($_GET['opcn']=="editar"){ ?>
											    	<input type="hidden" name="date_retirement_cdg" id="date_retirement_cdg" value="<?php echo($registroConfiguracion['fec_retirement_part']); ?>">
											    	<?php echo($registroConfiguracion['fec_retirement_part']); ?>
											    <?php }?>
											</div>
										</div>
										<div class="col-md-7"></div>
									</div>

										<div class="row">
											<div class="row innerLR">
												<label class="col-md-5 control-label" style="padding-top:8px;" for="date_destruction_cdg">Destrucción de Partes CDG</label>
												<div class="col-md-7 input-group date">
													<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
												    	<input class="form-control" type="text" id="date_destruction_cdg" name="date_destruction_cdg" placeholder="Fecha llegada CDG" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['fec_destruction_part']); ?>" <?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
												    <?php }else if($_GET['opcn']=="editar"){ ?>
												    	<input type="hidden" name="date_destruction_cdg" id="date_destruction_cdg" value="<?php echo($registroConfiguracion['fec_destruction_part']); ?>">
												    	<?php echo($registroConfiguracion['fec_destruction_part']); ?>
												    <?php }?>
												</div>
											</div>
											<div class="col-md-7"></div>
										</div>
								<!--  -->
								<div class="row">
									<div class="row innerLR">
										<label class="col-md-5 control-label" style="padding-top:8px;" for="parts_cdg">Cantidad Partes CDG</label>
										<div class="col-md-7">
											<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
										    	<input class="form-control" type="text" id="parts_cdg" name="parts_cdg" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?> value="<?php echo($registroConfiguracion['parts_cdg']); ?>" <?php } ?>/>
										    <?php }else if($_GET['opcn']=="editar"){ ?>
										    	<input type="hidden" name="parts_cdg" id="parts_cdg" value="<?php echo($registroConfiguracion['parts_cdg']); ?>">
										    	<?php echo($registroConfiguracion['parts_cdg']); ?>
										    <?php }?>
										</div>
									</div>
									<div class="col-md-7"></div>
								</div>
								<div class="row innerLR">
									<label class="col-md-5 control-label" style="padding-top:8px;" for="ok_first">Ok 1er vez:</label>
									<div class="col-md-7">
										<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
											<input type="checkbox" class="checkbox" value="SI" id="ok_first" name="ok_first" <?php if(isset($registroConfiguracion['ok_first'])&&$registroConfiguracion['ok_first']=="SI"){ ?> checked="checked" <?php } ?>/>SI
										<?php }else if($_GET['opcn']=="editar"){ ?>
											<input type="hidden" name="ok_first" id="ok_first" value="<?php echo($registroConfiguracion['ok_first']); ?>">
											<?php echo($registroConfiguracion['ok_first']); ?>
										<?php }?>
									</div>
								</div>
								<div class="row innerLR">
									<label class="col-md-5 control-label" style="padding-top:8px;" for="solution_contribute">Aporte Solución</label>
									<div class="col-md-7">
										<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
											<input type="checkbox" class="checkbox" value="SI" id="solution_contribute" name="solution_contribute" <?php if(isset($registroConfiguracion['solution_contribute'])&&$registroConfiguracion['solution_contribute']=="SI"){ ?> checked="checked" <?php } ?>/>SI
										<?php }else if($_GET['opcn']=="editar"){ ?>
											<input type="hidden" name="solution_contribute" id="solution_contribute" value="<?php echo($registroConfiguracion['solution_contribute']); ?>">
											<?php echo($registroConfiguracion['solution_contribute']); ?>
										<?php }?>
									</div>
								</div>
								<?php }?>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-8">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-2 control-label" for="parts_description" style="padding-top:8px;">Descripción:</label>
								<div class="col-md-10">
									<textarea id="parts_description" name="parts_description" class="wysihtml5 col-md-12 form-control" rows="3" placeholder="Descripción de las partes"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo($registroConfiguracion['parts_description']); } ?></textarea>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<?php /*?><p class="separator text-center"><i class="icon-car icon-2x"></i></p>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="lubricant_mot" style="padding-top:8px;">Lub Motor:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="lubricant_mot" name="lubricant_mot">
										<?php foreach ($listadoLubricantes as $key => $Data_listados) {
											if($Data_listados['type']=='M'){ ?>
											<option value="<?php echo($Data_listados['sentinel_lubricant_id']); ?>" <?php if(isset($registroConfiguracion['lubricant_mot'])&&$registroConfiguracion['lubricant_mot']==$Data_listados['sentinel_lubricant_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_lubricant']); ?></option>
										<?php } } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="lubricant_tra" style="padding-top:8px;">Lub Transmisión:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="lubricant_tra" name="lubricant_tra">
										<?php foreach ($listadoLubricantes as $key => $Data_listados) {
											if($Data_listados['type']=='T'){ ?>
											<option value="<?php echo($Data_listados['sentinel_lubricant_id']); ?>" <?php if(isset($registroConfiguracion['lubricant_tra'])&&$registroConfiguracion['lubricant_tra']==$Data_listados['sentinel_lubricant_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_lubricant']); ?></option>
										<?php } } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="lubricant_dif" style="padding-top:8px;">Lub Diferencial:</label>
								<div class="col-md-7">
									<select style="width: 100%;" id="lubricant_dif" name="lubricant_dif">
										<?php foreach ($listadoLubricantes as $key => $Data_listados) {
											if($Data_listados['type']=='D'){ ?>
											<option value="<?php echo($Data_listados['sentinel_lubricant_id']); ?>" <?php if(isset($registroConfiguracion['lubricant_dif'])&&$registroConfiguracion['lubricant_dif']==$Data_listados['sentinel_lubricant_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['sentinel_lubricant']); ?></option>
										<?php } } ?>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="km_mot" style="padding-top:8px;">Km Motor:</label>
								<div class="col-md-7"><input class="form-control" id="km_mot" name="km_mot" type="text" placeholder="Último mantenimiento" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['km_mot']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="km_tra" style="padding-top:8px;">Km Transmisión:</label>
								<div class="col-md-7"><input class="form-control" id="km_tra" name="km_tra" type="text" placeholder="Último mantenimiento" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['km_tra']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="km_dir" style="padding-top:8px;">Km Diferencial:</label>
								<div class="col-md-7"><input class="form-control" id="km_dir" name="km_dir" type="text" placeholder="Último mantenimiento" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['km_dir']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END --> <?php */ ?>
				<p class="separator text-center"><i class="icon-car icon-2x"></i></p>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="cost_workforce" style="padding-top:8px;">$ Mano de Obra:</label>
								<div class="col-md-7"><input class="form-control" id="cost_workforce" name="cost_workforce" type="text" placeholder="" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['cost_workforce']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="cost_parts" style="padding-top:8px;">$ Repuestos:</label>
								<div class="col-md-7"><input class="form-control" id="cost_parts" name="cost_parts" type="text" placeholder="" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['cost_parts']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="cost_tot" style="padding-top:8px;">$ TOT:</label>
								<div class="col-md-7"><input class="form-control" id="cost_tot" name="cost_tot" type="text" placeholder="Si aplica" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['cost_tot']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="total" style="padding-top:8px;">$ Total:</label>
								<div class="col-md-7"><input class="form-control" id="total" name="total" type="text" placeholder="" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['total']; ?>"<?php } ?> /></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-8">
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
				<input type="hidden" name="status_id" id="status_id" value="<?php echo($registroConfiguracion['status_id']); ?>" />
				<?php } ?>

				<p class="separator text-center"><i class="icon-car icon-2x"></i></p>
				<div class="row innerLR">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<table class="display table table-striped table-condensed table-primary table-vertical-center">
							<thead>
								<tr>
									<th style="width: 25%;" class="center">VIN</th>
									<th style="width: 25%;" class="center">KM</th>
									<th style="width: 25%;" class="center">Fecha OT</th>
									<th style="width: 25%;" class="center">Comentarios</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="center"><input type="text" class="form-control" name="vin_1" id="vin_1" <?php if(isset($VinOT[0]['vin'])){ ?>value="<?php echo($VinOT[0]['vin']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" name="km_1" id="km_1" <?php if(isset($VinOT[0]['km'])){ ?>value="<?php echo($VinOT[0]['km']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_1" id="date_ot_1" <?php if(isset($VinOT[0]['date_ot'])){ ?>value="<?php echo($VinOT[0]['date_ot']); ?>"<?php } ?> /></td>
									<td class="center"><input data-id="<?php if(isset($VinOT[0]['sentinel_vin_ot_id'])){ echo $VinOT[0]['sentinel_vin_ot_id'];}?>" type="text" class="form-control" name="comments_1" id="comments_1" <?php if(isset($VinOT[0]['comments'])){ ?>value="<?php echo($VinOT[0]['comments']); ?>"<?php } ?> /></td>
								</tr>
								<tr>
									<td class="center"><input type="text" class="form-control" name="vin_2" id="vin_2" <?php if(isset($VinOT[1]['vin'])){ ?>value="<?php echo($VinOT[1]['vin']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" name="km_2" id="km_2" <?php if(isset($VinOT[1]['km'])){ ?>value="<?php echo($VinOT[1]['km']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_2" id="date_ot_2" <?php if(isset($VinOT[1]['date_ot'])){ ?>value="<?php echo($VinOT[1]['date_ot']); ?>"<?php } ?> /></td>
									<td class="center"><input data-id="<?php if(isset($VinOT[1]['sentinel_vin_ot_id'])){ echo $VinOT[1]['sentinel_vin_ot_id'];}?>" type="text" class="form-control" name="comments_2" id="comments_2" <?php if(isset($VinOT[1]['comments'])){ ?>value="<?php echo($VinOT[1]['comments']); ?>"<?php } ?> /></td>
								</tr>
								<tr>
									<td class="center"><input type="text" class="form-control" name="vin_3" id="vin_3" <?php if(isset($VinOT[2]['vin'])){ ?>value="<?php echo($VinOT[2]['vin']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" name="km_3" id="km_3" <?php if(isset($VinOT[2]['km'])){ ?>value="<?php echo($VinOT[2]['km']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_3" id="date_ot_3" <?php if(isset($VinOT[2]['date_ot'])){ ?>value="<?php echo($VinOT[2]['date_ot']); ?>"<?php } ?> /></td>
									<td class="center"><input data-id="<?php if(isset($VinOT[2]['sentinel_vin_ot_id'])){ echo $VinOT[2]['sentinel_vin_ot_id'];}?>" type="text" class="form-control" name="comments_3" id="comments_3" <?php if(isset($VinOT[2]['comments'])){ ?>value="<?php echo($VinOT[2]['comments']); ?>"<?php } ?> /></td>
								</tr>
								<tr>
									<td class="center"><input type="text" class="form-control" name="vin_4" id="vin_4" <?php if(isset($VinOT[3]['vin'])){ ?>value="<?php echo($VinOT[3]['vin']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" name="km_4" id="km_4" <?php if(isset($VinOT[3]['km'])){ ?>value="<?php echo($VinOT[3]['km']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_4" id="date_ot_4" <?php if(isset($VinOT[3]['date_ot'])){ ?>value="<?php echo($VinOT[3]['date_ot']); ?>"<?php } ?> /></td>
									<td class="center"><input data-id="<?php if(isset($VinOT[3]['sentinel_vin_ot_id'])){ echo $VinOT[3]['sentinel_vin_ot_id'];}?>" type="text" class="form-control" name="comments_4" id="comments_4" <?php if(isset($VinOT[3]['comments'])){ ?>value="<?php echo($VinOT[3]['comments']); ?>"<?php } ?> /></td>
								</tr>
								<tr>
									<td class="center"><input type="text" class="form-control" name="vin_5" id="vin_5" <?php if(isset($VinOT[4]['vin'])){ ?>value="<?php echo($VinOT[4]['vin']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" name="km_5" id="km_5" <?php if(isset($VinOT[4]['km'])){ ?>value="<?php echo($VinOT[4]['km']); ?>"<?php } ?> /></td>
									<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_5" id="date_ot_5" <?php if(isset($VinOT[4]['date_ot'])){ ?>value="<?php echo($VinOT[4]['date_ot']); ?>"<?php } ?> /></td>
									<td class="center"><input data-id="<?php if(isset($VinOT[4]['sentinel_vin_ot_id'])){ echo $VinOT[4]['sentinel_vin_ot_id'];}?>" type="text" class="form-control" name="comments_5" id="comments_5" <?php if(isset($VinOT[4]['comments'])){ ?>value="<?php echo($VinOT[4]['comments']); ?>"<?php } ?> /></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<br>

				<div class="separator"></div>
						<!-- Form actions -->
						<div class="form-actions">
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
								<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
								<input type="hidden" id="opcn" name="opcn" value="crear">
								<input type="hidden" id="idElemento" name="idElemento" value="0">
							<?php }elseif(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
								<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
								<input type="hidden" id="opcn" name="opcn" value="editar">
								<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
							<?php } ?>
							<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
						</div>
						<!-- // Form actions END -->
			</form>
		</div>
	</div>
<?php } ?>

<?php if( isset($_GET['opcn']) && ($_GET['opcn']=='galeria') ){ ?>
	<div class="widget widget-heading-simple widget-body-white">
		<div class="widget-head">
			<h4 class="heading glyphicons file_import"><i></i> Agregar Archivos</h4>
		</div>
		<div class="widget-body innerAll inner-2x">
			<form action="op_caso.php?opcn=galeria&id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data" method="post" id="form_CrearGaleria">
				<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
				<input type="hidden" id="opcn_g" name="opcn_g" value="editarGaleria">
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-5">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-3 control-label" for="nombre" style="padding-top:8px;">Archivo:</label>
								<div class="col-md-9">
									<div class="col-md-7">
										<div class="col-md-12">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											  	(PDF, JPG, ZIP) <input type="file" class="margin-none" id="image" name="image" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-7">
							<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Cargar</button>
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<div class="separator"></div>
			</form>
			<div class="gallery">
				<ul class="row">
					<?php
					if(isset($fotografias)){
					foreach ($fotografias as $iID => $data_Foto) { ?>
					<!-- Gallery item -->
					<li class="col-md-2">
						<?php if($data_Foto['type']=='jpg'){ ?>
							<a href="../assets/images/sentinel/<?php echo($data_Foto['file']); ?>" target="_blank">
								<img src="../assets/images/sentinel/<?php echo($data_Foto['file']); ?>" alt="<?php echo($data_Foto['file']); ?>" class="img-responsive" style="width: 170px;" />
							</a>
						<?php }else{ ?>
							<a href="../assets/images/sentinel/<?php echo($data_Foto['file']); ?>" target="_blank">
								<img src="../assets/images/sentinel/<?php echo($data_Foto['type']); ?>.png" alt="<?php echo($data_Foto['file']); ?>" class="img-responsive" style="width: 170px;" />
							</a>
						<?php } ?>
						<a class="thumb no-ajaxify" href="op_caso.php?opcn=galeria&id=<?php echo $_GET['id']; ?>&opcn_e=eliminar&idE=<?php echo($data_Foto['sentinel_file_id']); ?>&source=<?php echo($data_Foto['file']); ?>">
						[Borrar] <?php echo($data_Foto['file']); ?>
						</a>
					</li>
					<!-- // Gallery item END -->
					<?php }
					} ?>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>

<?php if( isset($_GET['opcn']) && ($_GET['opcn']=='gestion') ){ ?>
	<div class="widget widget-heading-simple widget-body-white">
		<div class="widget-head">
			<h4 class="heading glyphicons package"><i></i> Gestión del Caso (Histórico)</h4>
		</div>
		<div class="widget-body innerAll inner-2x">
			<div class="gallery">
				<table class="table table-condensed table-bordered table-primary table-striped table-vertical-center">
					<thead>
						<tr>
							<th class="center" style="width:8%">FECHA</th>
							<th class="center" style="width:12%">INICIAL</th>
							<th class="center" style="width:12%">FINAL</th>
							<th class="center">GESTIÓN</th>
							<th class="center" style="width:15%">USUARIO</th>
						</tr>
					</thead>
					<tbody id="cuerpo_tabla">
						<?php
						$substatus_id = 0;
						foreach ($gestiones as $key => $Data_Gestiones) { ?>
							<tr class="selectable">
								<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Data_Gestiones['date_gestion']); ?></td>
								<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Data_Gestiones['status_old']); ?></td>
								<td class="center" style="padding: 2px; font-size: 80%;">
								<?php echo($Data_Gestiones['status_new']); ?>
								<?php if(isset($Data_Gestiones['SubStatus'])){ $substatus_id = $Data_Gestiones['sub_status']?>
									<br><strong>Calidad:</strong> <?php echo($Data_Gestiones['SubStatus']); ?>
								<?php } ?>
								</td>
								<td class="justify" style="padding: 2px; font-size: 75%;">
									<?php if($Data_Gestiones['date_request']!=""){ ?>Solicitud de Partes: <strong><?php echo($Data_Gestiones['date_request']); ?></strong><br> <?php } ?>
									<?php if($Data_Gestiones['date_delivered']!=""){ ?>Entrega de Partes: <strong><?php echo($Data_Gestiones['date_delivered']); ?></strong><br> <?php } ?>
										<?php if($Data_Gestiones['date_scrap']!=""){ ?>Entrega de Scrap: <strong><?php echo($Data_Gestiones['date_scrap']); ?></strong><br> <?php } ?>
									<?php echo($Data_Gestiones['gestion']); ?>
									<?php if(isset($Data_Gestiones['Detail'])){ ?>
										<?php foreach ($Data_Gestiones['Detail'] as $key => $Data_Detail) { ?>
											<br/>Vin: <?php echo($Data_Detail['vin']); ?> | Fecha: <?php echo($Data_Detail['date']); ?> | Acción: <?php echo($Data_Detail['action']); ?>
										<?php } ?>
									<?php } ?>
									<?php if($Data_Gestiones['file_gestion']!=""){ ?>
										<br><a href="../assets/images/sentinel/<?php echo($Data_Gestiones['file_gestion']); ?>" target="_blank">Archivo Adjunto</a>
									<?php } ?>
								</td>
								<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Data_Gestiones['first_name'].' '.$Data_Gestiones['last_name']); ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="separator"></div><br>
			<form action="op_caso.php?opcn=gestion&id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data" method="post" id="form_GestionCaso">
				<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-12">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-1 control-label" for="gestion" style="padding-top:8px;">Gestión:</label>
								<div class="col-md-11">
									<?php if($_SESSION['SentinelData']=='Admin') { ?>
										<textarea id="gestion" name="gestion" class="wysihtml5 col-md-12 form-control" rows="6" placeholder="Escriba aquí la gestión realizada sobre el caso en el comité"></textarea>
									<?php }else if($_SESSION['SentinelData']=='Garantias'){ ?>
										<textarea id="gestion" name="gestion" class="wysihtml5 col-md-12 form-control" rows="6" placeholder="Puede transcribir el email con la solicitud de las partes (Si es solicitud), o la respuesta cuando fueron enviadas (Si es entrega de partes). O incluya información que considere relevante en el proceso">NA</textarea>
									<?php }else if($_SESSION['SentinelData']=='Calidad'){ ?>
										<textarea id="gestion" name="gestion" class="wysihtml5 col-md-12 form-control" rows="6" placeholder="Por favor detalle el proceso de calidad sobre los elementos que va a notificar"></textarea>
									<?php } ?>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<?php }?>
				<?php if($_SESSION['SentinelData']=='Calidad'){ ?>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-12">
							<div class="col-md-6">
								<table class="display table table-striped table-condensed table-primary table-vertical-center">
									<thead>
										<tr>
											<th style="width: 25%;" class="center">VIN</th>
											<th style="width: 25%;" class="center">Fecha</th>
											<th style="width: 50%;" class="center">Accion</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="center"><input type="text" class="form-control" name="vin_qa_1" id="vin_qa_1" /></td>
											<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_qa_1" id="date_qa_1" /></td>
											<td class="center"><input type="text" class="form-control" name="action_qa_1" id="action_qa_1" /></td>
										</tr>
										<tr>
											<td class="center"><input type="text" class="form-control" name="vin_qa_2" id="vin_qa_2" /></td>
											<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_qa_2" id="date_qa_2" /></td>
											<td class="center"><input type="text" class="form-control" name="action_qa_2" id="action_qa_2" /></td>
										</tr>
										<tr>
											<td class="center"><input type="text" class="form-control" name="vin_qa_3" id="vin_qa_3" /></td>
											<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_qa_3" id="date_qa_3" /></td>
											<td class="center"><input type="text" class="form-control" name="action_qa_3" id="action_qa_3" /></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="col-md-6">
								<div class="row innerLR">
									<!-- Group -->
									<div class="form-group">
										<label class="col-md-3 control-label" for="substatus_id" style="padding-top:8px;">Estado Calidad:</label>
										<div class="col-md-9">
											<select style="width: 100%;" id="substatus_id" name="substatus_id">
												<?php foreach ($listadoSubEstados as $key => $Data_listados) { ?>
													<option value="<?php echo($Data_listados['substatus_id']); ?>" <?php if($substatus_id == $Data_listados['substatus_id']){ ?>selected="selected"<?php } ?> ><?php echo($Data_listados['substatus']); ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<!-- // Group END -->
								</div>
								<br>
								<div class="row innerLR">
									<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
										(PDF o ZIP) <input type="file" class="margin-none" id="archivo" name="archivo" />
									</div>
								</div>
							</div>
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<?php } ?>
				<?php if($_SESSION['SentinelData']=='Garantias'){ ?>
				<!-- Row -->
					<div class="row innerLR">
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="date_request" style="padding-top:8px;">Fecha de Solicitud:</label>
								<div class="col-md-1"></div>
								<div class="col-md-5 input-group date">
							    	<input class="form-control" type="text" id="date_request" name="date_request" placeholder="Solicitud de Partes" />
							    	<span class="input-group-addon">
							    		<i class="fa fa-th"></i>
							    	</span>
								</div>
								<div class="col-md-1"></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="date_delivered" style="padding-top:8px;">Fecha de Entrega:</label>
								<div class="col-md-1"></div>
								<div class="col-md-5 input-group date">
							    	<input class="form-control" type="text" id="date_delivered" name="date_delivered" placeholder="Entrega de Partes" />
							    	<span class="input-group-addon">
							    		<i class="fa fa-th"></i>
							    	</span>
								</div>
								<div class="col-md-1"></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
						<!-- Column -->
						<div class="col-md-4">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-5 control-label" for="date_scrap" style="padding-top:8px;">Fecha de Scrap:</label>
								<div class="col-md-1"></div>
								<div class="col-md-5 input-group date">
							    	<input class="form-control" type="text" id="date_scrap" name="date_scrap" placeholder="Entrega de Scrap" />
							    	<span class="input-group-addon">
							    		<i class="fa fa-th"></i>
							    	</span>
								</div>
								<div class="col-md-1"></div>
							</div>
							<!-- // Group END -->
						</div>
						<!-- // Column END -->
					</div>
				<!-- // Row END -->
				<br>
				<?php } ?>
				<?php if($_SESSION['SentinelData']=='Admin'||$_SESSION['SentinelData']=='Garantias'||$_SESSION['SentinelData']=='Calidad'){?>
					<!-- Row -->
						<div class="row innerLR">
							<!-- Column -->
							<div class="col-md-4">
								<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
								<input type="hidden" id="opcn_g" name="opcn_g" value="Gestionar">
								<?php if($_SESSION['SentinelData']=='Admin') { ?>
									<!-- Group -->
									<div class="form-group">
										<label class="col-md-3 control-label" for="status_id_new" style="padding-top:8px;">Estado:</label>
										<div class="col-md-9">
											<select style="width: 100%;" id="status_id_new" name="status_id_new">
												<?php foreach ($listadoEstados as $key => $Data_listados) { ?>
													<option value="<?php echo($Data_listados['status_id']); ?>" <?php if(isset($registroConfiguracion['status_id'])&&$registroConfiguracion['status_id']==$Data_listados['status_id']){ ?> selected="selected" <?php } ?> ><?php echo($Data_listados['status']); ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<!-- // Group END -->
								<?php }else{ ?>
									<input type="hidden" id="status_id_new" name="status_id_new" value="<?php echo $registroConfiguracion['status_id']; ?>">
								<?php } ?>
							</div>
							<!-- // Column END -->
							<!-- Column -->
							<div class="col-md-7">
								<input type="hidden" id="status_act" name="status_act" value="<?php echo($registroConfiguracion['status_id']); ?>">
							</div>
							<!-- // Column END -->
							<!-- Column -->
							<div class="col-md-1">
								<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Gestionar</button>
							</div>
							<!-- // Column END -->
						</div>
					<!-- // Row END -->
				<?php }?>
			</form>
		</div>
	</div>
<?php } ?>
<?php if( isset($_GET['opcn']) && ($_GET['opcn']=='vines') ){ ?>
	<form action="op_caso.php?opcn=vines&id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data" method="post" id="form_VinesCaso">
		<div class="widget widget-heading-simple widget-body-white">
			<div class="widget-head">
				<h4 class="heading glyphicons package"><i></i> Agregar Vin al Sentinel</h4>
			</div>
			<div class="widget-body innerAll inner-2x">
				<div class="gallery">
					<table class="display table table-striped table-condensed table-primary table-vertical-center">
						<thead>
							<tr>
								<th style="width: 25%;" class="center">VIN</th>
								<th style="width: 5%;" class="center">KM</th>
								<th style="width: 10%;" class="center">Fecha OT</th>
								<th style="width: 25%;" class="center">Comentarios</th>
								<th style="width: 35%;" class="center">QUIEN</th>
								<th style="width: 35%;" class="center">Cuando</th>
							</tr>
						</thead>
						<tbody>
							<? foreach ($VinOT as $iID => $data_VINES) { ?>
								<tr>
									<td class="center"><?php echo($data_VINES['vin']); ?></td>
									<td class="center"><?php echo($data_VINES['km']); ?></td>
									<td class="center"><?php echo($data_VINES['date_ot']); ?></td>
									<td class="center"><?php echo($data_VINES['comments']); ?></td>
									<td class="center"><?php echo($data_VINES['first_name']." ".$data_VINES['last_name']."<br>".$data_VINES['dealer']."<br>".$data_VINES['headquarter']); ?></td>
									<td class="center"><?php echo($data_VINES['date_creation']); ?></td>
								</tr>
							<?php } ?>
							<tr>
								<td class="center"><input type="text" class="form-control" name="vin_1" id="vin_1" /></td>
								<td class="center"><input type="text" class="form-control" name="km_1" id="km_1" /></td>
								<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_1" id="date_ot_1" /></td>
								<td class="center"><input type="text" class="form-control" name="comments_1" id="comments_1" /></td>
								<td class="center"><?php echo($_SESSION['NameUsuario']); ?></td>
								<td class="center"></td>
							</tr>
							<tr>
								<td class="center"><input type="text" class="form-control" name="vin_2" id="vin_2" /></td>
								<td class="center"><input type="text" class="form-control" name="km_2" id="km_2" /></td>
								<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_2" id="date_ot_2" /></td>
								<td class="center"><input type="text" class="form-control" name="comments_2" id="comments_2" /></td>
								<td class="center"><?php echo($_SESSION['NameUsuario']); ?></td>
								<td class="center"></td>
							</tr>
							<tr>
								<td class="center"><input type="text" class="form-control" name="vin_3" id="vin_3" /></td>
								<td class="center"><input type="text" class="form-control" name="km_3" id="km_3" /></td>
								<td class="center"><input type="text" class="form-control" placeholder="YYYY-MM-DD" name="date_ot_3" id="date_ot_3" /></td>
								<td class="center"><input type="text" class="form-control" name="comments_3" id="comments_3" /></td>
								<td class="center"><?php echo($_SESSION['NameUsuario']); ?></td>
								<td class="center"></td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
					<input type="hidden" id="opcn_g" name="opcn_g" value="AddVines">
					<div class="col-md-1">
						<button type="submit" class="btn btn-success" id="agregarElemento"><i class="fa fa-check-circle"></i> Agregar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
<?php } ?>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/casos.js"></script>
</body>
</html>
