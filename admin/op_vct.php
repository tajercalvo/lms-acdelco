<?php include('src/seguridad.php'); ?>
<?php include('controllers/cursos_op_vct.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons briefcase"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/cursos.php">Configuración Cursos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Cursos </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if (isset($_GET['back']) && $_GET['back'] == "inicio") { ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare"><i></i>Regresar</a>
											<h5>
											<?php } else { ?>
												<h5><a href="cursos.php" class="glyphicons no-js unshare"><i></i>Regresar</a>
													<h5>
													<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
							<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">
							<!-- Formulario principal -->
							<form id="formulario_data" method="post" enctype="multipart/form-data" autocomplete="off">
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="course" style="padding-top:8px;">Nombre del Curso:</label>
											<div class="col-md-7"><input class="form-control" id="course" name="course" type="text" placeholder="Nombre del curso" <?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) { ?>value="<?php echo $registroConfiguracion['course']; ?>" <?php } ?> /></div>
										</div>
										<!-- // Group END -->
								 </div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
								<div class="form-group">
										<label class="col-md-5 control-label" for="description" style="padding-top:8px;">Objetivo general:</label>
										<div class="col-md-7">
												<textarea id="description" name="description" class="col-md-12 form-control contador" rows="3">
														<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) {
																echo $registroConfiguracion['description'];
														} ?>
												</textarea>
												<!-- <p>Posición del cursor: <span id="cursorPosition">0</span></p> Mostrar posición -->
										</div>
								</div>
</div>

								

									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<br>
								<!-- Row -->
								<div class="row innerLR">
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<br>
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<div class="form-group">
											<label class="col-md-5 control-label" for="specialty_id" style="padding-top:8px;">Especialidad:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="specialty_id" name="specialty_id">
													<?php foreach ($listadosEspecialidades as $key => $Data_listados) { ?>
														<option value="<?php echo ($Data_listados['specialty_id']); ?>" <?php if (isset($registroConfiguracion['specialty_id']) && $registroConfiguracion['specialty_id'] == $Data_listados['specialty_id']) { ?>selected="selected" <?php } ?>><?php echo ($Data_listados['specialty']); ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<div class="form-group">
											<label class="col-md-5 control-label" for="prerequisites" style="padding-top:8px;">Metodología:</label>
											<div class="col-md-7">
												<textarea id="prerequisites" name="prerequisites" class="col-md-12 form-control contador" rows="3">
													<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) {
														echo $registroConfiguracion['prerequisites'];
													} ?>
												</textarea>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<br>
								<!-- // Row END -->
								<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar')) { ?>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
											<label class="col-md-5 control-label" for="image" style="padding-top:8px;">Imagen Actual:</label>
											<div class="col-md-7">
												<img id="ImgCurso" src="../assets/images/distinciones/<?php echo $registroConfiguracion['image']; ?>" style="width: 240px;" alt="<?php echo $registroConfiguracion['course']; ?>">
											</div>
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="image_new" style="padding-top:8px;">Imagen:</label>
												<div class="col-md-7">
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file">
															<span class="fileupload-new">Seleccionar</span>
															<span class="fileupload-exists">Cambiar Imagen</span>
															<input type="file" class="margin-none" id="image_new" name="image_new" />
														</span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
													<div class="form-group">
														<button type="button" class="btn btn-primary" onclick="cargaImagen();"><i class="fa fa-check-circle"></i> Cambiar Imagen</button>
													</div>
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-2">
											<div class="ajax-loading hide" id="loading_imagen">
												<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
											</div>
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
								<?php } ?>
								<br>
								<!-- Row -->
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="type" style="padding-top:8px;">Tipo de Curso:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="type" name="type">
													<option value="WBT" <?php if (isset($registroConfiguracion['type']) && $registroConfiguracion['type'] == "WBT") { ?>selected="selected" <?php } ?>>WBT</option>
													<option value="ILT" <?php if (isset($registroConfiguracion['type']) && $registroConfiguracion['type'] == "ILT") { ?>selected="selected" <?php } ?>>ILT</option>
													<option value="VCT" <?php if (isset($registroConfiguracion['type']) && $registroConfiguracion['type'] == "VCT") { ?>selected="selected" <?php } ?>>VCT</option>
													<option value="OJT" <?php if (isset($registroConfiguracion['type']) && $registroConfiguracion['type'] == "OJT") { ?>selected="selected" <?php } ?>>OJT</option>
													<option value="VRT" <?php if (isset($registroConfiguracion['type']) && $registroConfiguracion['type'] == "VRT") { ?>selected="selected" <?php } ?>>VRT</option>
													<option value="MFR" <?php if (isset($registroConfiguracion['type']) && $registroConfiguracion['type'] == "MFR") { ?>selected="selected" <?php } ?>>MFR</option>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
									<div class="form-group">
												<label class="col-md-5 control-label" for="target" style="padding-top:8px;">Objetivos específicos:</label>
												<div class="col-md-7">
														<textarea id="target" name="target" class="col-md-12 form-control contador" rows="3">
																<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar' || $_GET['opcn'] == 'ver')) {
																		echo $registroConfiguracion['target'];
																} ?>
														</textarea>
												</div>
										</div>
										</div>
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<br>
								<div class="row innerLR">
									<!-- Column -->
									<br>
									<div class="col-md-5 form-group">
										<label class="col-md-5 control-label" for="target" style="padding-top:8px;">Archivo para el curso:</label>
										<div class="col-md-7">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												<span class="btn btn-default btn-file">
													<span class="fileupload-new">Seleccionar archivos .zip</span>
													<input type="file" class="margin-none" id="archivos" name="archivos" accept="application/zip" />
												</span>
												<span class="fileupload-preview"></span>
												<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
											</div>
										</div>

									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar')) { ?>
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
												<div class="col-md-7">
													<select style="width: 100%;" id="status_id" name="status_id">
														<option value="1" <?php if ($registroConfiguracion['status_id'] == "1") { ?>selected="selected" <?php } ?>>Activo</option>
														<option value="2" <?php if ($registroConfiguracion['status_id'] == "2") { ?>selected="selected" <?php } ?>>Inactivo</option>
													</select>
													<h6 style="color: red;">NOTA: Solo se podrá activar el curso cuando tenga un módulo, una evaluación, preguntas de profundización, material de apoyo y esté atado al menos a una trayectoria.</h6>
												</div>
											</div>
											<!-- // Group END -->
										<?php } ?>
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>
								<br>
								<div class="row innerLR">
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->

										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<br>
									<div class="col-md-5 ">
										<label class="col-md-5 control-label" for="target" style="padding-top:8px;">Administrador de archivos:</label>
										<div class="col-md-7" id="tabla_archivos_vct">

										</div>

									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-2">
									</div>
									<!-- // Column END -->
								</div>


								<div class="separator"></div>
								<!-- Form actions -->
								<div class="form-actions">
									<?php if (isset($_GET['opcn']) && ($_GET['opcn'] == 'nuevo')) { ?>
										<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
										<input type="hidden" id="opcn" name="opcn" value="crear">
									<?php } elseif (isset($_GET['opcn']) && ($_GET['opcn'] == 'editar')) { ?>
										<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
										<input type="hidden" id="imgant" name="imgant" value="<?php echo $registroConfiguracion['image']; ?>">
										<input type="hidden" id="opcn" name="opcn" value="editar">
										<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
									<?php } ?>
									<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
								</div>
								<!-- // Form actions END -->
							</form>
						</div>
					</div>
					<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>

		<!-- Modal editar archivos -->
		<form enctype="multipart/form-data" method="post" id="form_Editar_archivos"><br><br>
			<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Editar archivo del curso</h4>
						</div>
						<div class="modal-body">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-5">
											<label class="control-label">Numero: </label>
											<input type="number" id="numero_archivo" name="numero_nuevo" class="form-control col-md-8" placeholder="Orden en que aparecera el archivo" required />
										</div>
										<div class="col-md-5">
											<label class="control-label">Nombre: </label>
											<input type="text" id="nombre_archivo" name="nombre_nuevo" class="form-control col-md-8" placeholder="Nombre del archivo" required />
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-4">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												<span class="btn btn-default btn-file">
													<span class="fileupload-new">Imágen</span>
													<span class="fileupload-exists">Cambiar Imagen</span>
													<input type="file" class="margin-none" id="image" name="image" accept="image/*" />
												</span>
												<span class="fileupload-preview"></span>
												<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
											</div>
										</div>
										<div class="col-md-1">
										</div>
										<div class="col-md-4">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												<span class="btn btn-default btn-file">
													<span class="fileupload-new">Archivo</span>
													<span class="fileupload-exists">Cambiar Archivo</span>
													<input type="file" class="margin-none" id="archivo" name="archivo" accept="image/*, application/pdf, video/mp4" />
												</span>
												<span class="fileupload-preview"></span>
												<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
											</div>
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- Fin modal -->

		<!-- Modal crear archivo -->
		<form enctype="multipart/form-data" method="post" id="form_crear_archivo"><br><br>
			<div class="modal fade" id="Modal_crear_archivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Crear archivo para el curso</h4>
						</div>
						<div class="modal-body">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-5">
											<label class="control-label">Numero: </label>
											<input type="number" id="crear_numero_archivo" name="crear_numero_nuevo" class="form-control col-md-8" placeholder="Oren en que aparecera el archivo" required />
										</div>
										<div class="col-md-5">
											<label class="control-label">Nombre: </label>
											<input type="text" id="crear_nombre_archivo" name="crear_nombre_nuevo" class="form-control col-md-8" placeholder="Nombre del archivo" required />
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-4">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												<span class="btn btn-default btn-file">
													<span class="fileupload-new">Imágen</span>
													<span class="fileupload-exists">Cambiar Imagen</span>
													<input type="file" class="margin-none" id="crear_image_archivo" name="crear_image_archivo" accept="image/*" />
												</span>
												<span class="fileupload-preview"></span>
												<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
											</div>
										</div>
										<div class="col-md-1">
										</div>
										<div class="col-md-4">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												<span class="btn btn-default btn-file">
													<span class="fileupload-new">Archivo</span>
													<span class="fileupload-exists">Cargar Archivo</span>
													<input type="file" class="margin-none" id="crear_file" name="crear_file" accept="image/*, application/pdf, video/mp4" />
												</span>
												<span class="fileupload-preview"></span>
												<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
											</div>
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Crear</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- Fin modal -->


		<!-- Modal crear pregunta-->
		<form enctype="multipart/form-data" method="post" id="form_crear_pregunta"><br><br>
			<div class="modal fade" id="Modal_Crear_pregunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Agregar una pregunta al curso</h4>
						</div>
						<div class="modal-body">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-2">
											<label class="control-label">Numero: </label>
											<input type="number" id="numero_archivo" name="agregar_numero_nuevo_pregunta" class="form-control col-md-8" placeholder="No." required />
										</div>
										<div class="col-md-10">
											<label class="control-label">Pregunta: </label>
											<input type="text" id="nombre_archivo" name="agregar_nombre_nuevo_pregunta" class="form-control col-md-8" placeholder="Nombre del archivo" required />
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>

									<!-- Row -->
									<div class="row" id="row-respuestas">
										<!-- jquery -->
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>

									<div class="row">
										<div class="col-md-11"></div>
										<div class="col-md-1">
											<a id="agregar_repuesta" href="" class="glyphicons circle_plus primary"><i></i></a>
										</div>
									</div>

									<!-- Row -->
									<!-- <div class="row">
					<div class="col-md-4">
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Imágen</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="agregar_image_pregunta" name="agregar_image_pregunta" accept="image/*"/>
							</span>
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
					</div>
					<div class="col-md-7">
					</div>
					<div class="col-md-1">
                        <a id="agregar_repuesta" href="" class="col-md-3 glyphicons circle_plus primary"><i></i></a>
					</div>
				</div> -->
									<!-- Row END-->
									<div class="clearfix"><br></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Crear</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- Fin modal crear prpegunta-->

		<!-- Modal crear pregunta-->
		<form enctype="multipart/form-data" method="post" id="form_editar_pregunta"><br><br>
			<div class="modal fade" id="Modal_editar_pregunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Agregar una pregunta al curso</h4>
						</div>
						<div class="modal-body">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-2">
											<label class="control-label">Numero: </label>
											<input type="number" id="editar_numero_archivo" name="agregar_numero_nuevo_pregunta" class="form-control col-md-8" placeholder="No." required />
										</div>
										<div class="col-md-10">
											<label class="control-label">Pregunta: </label>
											<input type="text" id="editar_nombre_archivo" name="agregar_nombre_nuevo_pregunta" class="form-control col-md-8" placeholder="Nombre del archivo" required />
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>

									<!-- Row -->
									<div class="row" id="row-editar-respuestas">
									</div>

									<!-- Row END-->
									<div class="clearfix"><br></div>

									<!-- Row -->
									<!-- <div class="row">
					<div class="col-md-4">
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Imágen</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="agregar_image_pregunta" name="agregar_image_pregunta" accept="image/*"/>
							</span>
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
					</div>
					<div class="col-md-7">
					</div>
					<div class="col-md-1">
                        <a id="agregar_repuesta" href="" class="col-md-3 glyphicons circle_plus primary"><i></i></a>
					</div>
				</div> -->
									<!-- Row END-->
									<div class="clearfix"><br></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Editar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- Fin modal crear prpegunta-->

		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/cursos_op_vct.js?version=<?php echo md5(time()); ?> "></script>
</body>

</html>