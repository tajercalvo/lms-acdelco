<?php include('src/seguridad.php'); ?>
<?php
include('controllers/envivo.php');
$location = 'tutor';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons webcam"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/envivo.php">Transmisión en vivo</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left"><?php // echo($galeria_detailData['media']); ?></h2>
						<div class="btn-group pull-right">
							<?php if($_SESSION['max_rol']>=6){ ?><h5><a href="#ModalCrearNueva" data-toggle="modal" class="glyphicons no-js circle_plus" ><i></i>Agregar Transmisión</a><h5><?php } ?>
						</div>
						<div class="clearfix"></div>
						<p style="text-align: justify; "><?php // echo($galeria_detailData['description']); ?></p>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="width:830px;height:560px;top:40px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="TituloVideo">Cargando...</h4>
            </div>
            <div class="modal-body">
            	<video id="cartoonVideo" width="800" height="500" id="vid1" src="" class="video-js vjs-default-skin" controls preload="auto" width="100%" height="450">
				</video>
                <!--<iframe id="cartoonVideo" width="560" height="315" src="../assets/gallery/VideosCruze/04CruzeTraduccionCarPlay_v5.mp4" frameborder="0" allowfullscreen></iframe>-->
            </div>
        </div>
    </div>
</div>

 <input type="hidden" id="audiofile" size="80" value="../assets/Biblioteca/demo.mp3" />
									<audio id="myaudio">
									  HTML5 audio not supported
									</audio>

	<div data-toggle="gridalicious" data-gridalicious-width="380" data-gridalicious-gutter="0" class="">
		<?php  foreach ($galeria_detailData as $key => $Data_Gal) { ?>
		<div class="widget widget-heading-simple widget-body-white widget-pinterest active">
			<div class="widget-body padding-none" >
					<a data-toggle="prettyPhoto" class="thumb" href="<?php echo $Data_Gal['url_streaming']; ?>" onclick="streaming_views(<?php echo($Data_Gal['streaming_id']); ?>); return false;">
						<img style="width:375px" src="../assets/gallery/Transmision_en_vivo/<?php echo $Data_Gal['image'] ?>" alt="<?php // echo($Data_Gal['media_detail']); ?>" />
					</a>
				<div class="description">
					<span class="text-uppercase"><strong><?php  echo $Data_Gal['tittle']; ?></strong></span> |
						<a href="Likes" title="<?php  echo ("Quien dio like"); ?>" onclick="return false;">
							<i class="fa fa-fw fa-thumbs-o-up"></i>
							<span id="CantLikes<?php echo $Data_Gal['streaming_id']; ?>"><?php echo(number_format($Data_Gal['likes'],0)); ?></span>
						</a>
						<a href="Likes" title="<?php  echo ('Quien lo vio'); ?>" onclick="return false;">
							<i class="fa fa-fw fa-eye"></i>
							<span id="CantViews<?php echo($Data_Gal['streaming_id']); ?>"><?php echo(number_format($Data_Gal['views'],0)); ?></span>
						</a>
						<a href="#" data-toggle="modal" onclick="LikeGalery(<?php  echo($Data_Gal['streaming_id']); ?>); return false;"><i class="fa fa-fw fa-thumbs-o-up"></i> Me gusta 
						</a>
						<input type="hidden" id="status" value="<?php echo $Data_Gal['status_id']; ?>" name="status">
						<?php if($_SESSION['max_rol']>=6){ ?>
							<a href="#ModalEditar" data-toggle="modal" onclick="CargaDatosEditar_Vid(<?php echo($Data_Gal['streaming_id']); ?>,'<?php echo($Data_Gal['image']); ?>','<?php echo($Data_Gal['tittle']); ?>','<?php echo($Data_Gal['description']); ?>','<?php echo($Data_Gal['url_streaming']); ?>','<?php echo($Data_Gal['status_id']); ?>');"><i class="fa fa-fw fa-pencil"></i> Editar </a>
						<?php } ?><br>
					<p><?php   echo $Data_Gal['description']; ?></p>
				</div>
			</div>
		</div>
		<?php  } ?>
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
<!-- Modal -->
<form action="envivo.php" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
	<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Crear Transmisión</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<!-- Row -->
							<div class="row">
								<div class="col-md-1">
								</div>
								<div class="col-md-5">
									<label class="control-label">Titulo: </label>
									<input type="text" id="media_detail" name="media_detail" class="form-control col-md-8" placeholder="Título del video" />
								</div>
								<div class="col-md-5">
									<label class="control-label">Descripción: </label>
									<input type="text" id="description" name="description" class="form-control col-md-8" placeholder="Descripción del video" />
								</div>
								<div class="col-md-1">
									<input type="hidden" id="opcn" value="CrearTransmision" name="opcn">
								</div>
							</div>
							<!-- Row END-->
							<div class="clearfix"><br></div>
							<!-- Row -->
							<div class="row">
								<div class="col-md-1">
								</div>
								<div class="col-md-5">
									<label class="control-label">Transmisión </label>
									<input type="text" id="video" name="video" class="form-control col-md-8" placeholder="URL de la Transmisión " />
								</div>
								<div class="col-md-5">
								</div>
								<div class="col-md-1">
								</div>
									<div class="col-md-5">
						<label class="control-label">Estado: </label>
						<select style="width: 100%;" id="estado" name="estado">
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
					</div>
							</div>
							<!-- Row END-->
							<div class="clearfix"><br></div>
							<!-- Row -->
							<div class="row">
								<div class="col-md-1">
								</div>
								<div class="col-md-10">
									<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
									  	<span class="btn btn-default btn-file">
									  		<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
									  		<span class="fileupload-exists">Cambiar Imagen</span>
										  	<input type="file" class="margin-none" id="image_new" name="image_new" />
										</span>
									  	<span class="fileupload-preview"></span>
									  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
									</div>
								</div>
								<div class="col-md-1">
								</div>
							</div>
							<!-- Row END-->
							<div class="clearfix"><br></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->

<!-- Modal editar -->
<form action="envivo.php" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
	<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Editar Transmisión</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Titulo: </label>
						<input type="text" id="media_detail_ed" name="media_detail_ed" class="form-control col-md-8" placeholder="Título de la Transmisión" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Descripción: </label>
						<input type="text" id="description_ed" name="description_ed" class="form-control col-md-8" placeholder="Descripción de la Transmisión" />
					</div>
					<div class="col-md-1">
						<input type="hidden" id="media_detail_id" value="0" name="media_detail_id">
						<input type="hidden" id="opcn" value="editar_vid" name="opcn">
						<input type="hidden" id="imgant" value="0" name="imgant">
					</div>

				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Transmisión: </label>
						<input type="text" id="video_ed" name="video_ed" class="form-control col-md-8" placeholder="URL de la Transmisión " />
					</div>
					<div class="col-md-5">
						<label class="control-label">Estado: </label>
						<select style="width: 100%;" id="estado2" name="estado2">
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
						Estado Actual: <span id="EstadoActual"></span>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-10">
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="image_new_ed" name="image_new_ed" />
							</span>
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->




		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/envivo.js"></script>
</body>
</html>
