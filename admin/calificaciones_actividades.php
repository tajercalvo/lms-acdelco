<?php include('src/seguridad.php');

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', TRUE);


if (!isset($_SESSION['idUsuario'])) {
	header("location: login");
}
?>
<?php include('controllers/calificaciones_actividades.php');
$location = 'education';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons podium"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/calificaciones.php">Calificación de Estudiantes</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Calificación de Estudiantes </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<form method="post" id="frm_sesiones">
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-6">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-4 control-label">Fecha Inicial:</label>
													<div class="col-md-8">
														<div class="input-group date">
															<input class="form-control" type="text" id="fecha_inicial" name="fecha_inicial" placeholder="Fecha Inicial" autocomplete="off" required />
															<span class="input-group-addon"><i class="fa fa-th"></i></span>
														</div>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-6">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-4 control-label">Fecha Final:</label>
													<div class="col-md-8">
														<div class="input-group date">
															<input class="form-control" type="text" id="fecha_final" name="fecha_final" placeholder="Fecha Final" autocomplete="off" required />
															<span class="input-group-addon"><i class="fa fa-th"></i></span>
														</div>
													</div>
												</div>
												<!-- // Group END -->
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Consultar sesiones</button>
									</div>
								</div>
							</form>
							<br>
							<form method="post" id="frm_actividades">
								<div class="row">
									<div class="col-md-10">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Sesión a Calificar:</label>
											<div class="col-md-10">
												<select style="width: 100%;" id="schedule_id" name="schedule_id">
													<?php foreach ($cursos as $key => $value) { ?>
														<option value="<?php echo $value['schedule_id'] ?>"> <?php echo 'Sesión: ' . $value['schedule_id'] . ' - Modulo: ' . $value['module'] . ' - Curso: ' . $value['course'] ?> </option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
										<a href=""></a>
									</div>
									<div class="col-md-2 center">
										<button id="btn_consultar_actividades" type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Consultar actividades</button>
									</div>
								</div>
							</form>
						</div>
					</div>

					<div class="widget widget-heading-simple widget-body-white" id="ResultadosSesiones">
						<!-- Widget heading -->
						<div class="widget-head">
							<!-- <h4 class="heading glyphicons list"><i></i> <strong>Información:</strong> 8787</h4> -->
						</div>
						<table id="tabla" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
							<!-- Table heading -->
							<thead>
								<tr>
									<th data-hide="phone,tablet" style="width: 15%;">ACTIVIDAD</th>
									<th data-hide="phone,tablet" style="width: 15%;">FECHA ENTREGA</th>
									<th data-hide="phone,tablet">TIPO</th>
									<th data-hide="phone,tablet">PORCENTAJE</th>
									<th data-hide="phone,tablet">IDENTIFICACIÓN</th>
									<th data-hide="phone,tablet">ALUMNO</th>
									<th data-hide="phone,tablet" style="width: 10%;">ADJUNTO</th>
									<th data-hide="phone,tablet" style="width: 10%;">ESTADO</th>
									<th data-hide="phone,tablet" style="width: 5%;">RESULTADO</th>
								</tr>
							</thead>
							<!-- // Table heading END -->
							<tbody>
							</tbody>
						</table>
						<button class="btn btn-success" id="btn-calificar"><i class="fa fa-check-circle"></i> Calificar</button>
					</div>
					<!-- Nuevo ROW-->
					<!-- Modal -->
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Comentario</h4>
								</div>
								<div class="modal-body">
									<textarea style="width: 100%;" id="comemtario_modal" rows="10"></textarea>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-danger" data-dismiss="modal">Guardar</button>
								</div>
							</div>

						</div>
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/calificaciones_actividades.js?version=<?php echo md5(time()); ?> "></script>
</body>

</html>