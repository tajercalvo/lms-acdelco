<?php include('src/seguridad.php');
if(!$_SentinelColaborador && !$_SentinelAdmin && !$_SentinelGarantias && !$_SentinelCalidad){
	header("location: ../admin/");
}
?>
<?php include('controllers/casos.php');
$location = 'centinel';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">

	<?php if(isset($circulares) && count($circulares)>0){?>
	<!-- Modal -->
	<div class="modal fade " id="modal-sentinel" style="z-index:20000">
		<div class="modal-dialog " style="width:930px;top:-25px;">
			<div class="modal-content">
				<!-- Modal heading -->
				<div class="modal-header">
					<button type="button" class="close" style="top:0px;" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title center" style="height:25px;font-size:12px;line-height:22px;">-- ALERTAS SENTINEL DEALER: SOLICITUD DE CASOS --</h3>
				</div>
				<!--// Modal heading END -->
				<!-- Modal body -->
				<div class="modal-body">
					<?php foreach ($circulares as $key => $value) { ?>
						<span class="text-success"><b><?php echo($value['title']); ?></b></span>: <?php echo($value['newsletter']); ?><br>
					<?php }?>
					<br>Favor notificar a Fabian Olarte o Gerlen Manios.
				</div>
				<!-- // Modal body END -->
			</div>
		</div>
	</div>
	<!-- // Modal END -->
	<?php } ?>

	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons notes_2"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/casos.php">Administración de Casos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Casos Sentinel </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-9">
									<h5 style="text-align: justify; ">A continuación encontrarán un listado de casos Sentinel, los casos aquí descritos se observan por que usted tiene perfil Sentinel de administración, sin embargo los casos que aquí se muestran no son los mismos que observará el público; aquellos irán con un tratamiento especial.</h5><br>
									<!--<h5><a href="casos_excel.php" class="glyphicons no-js download_alt" ><i></i>Descargar</a><h5>-->
								</div>
								<div class="col-md-1">
								</div>
								<div class="col-md-2">
									<h5><a href="op_caso.php?opcn=nuevo" target="_blank" class="glyphicons no-js circle_plus" ><i></i>Agregar Caso</a><h5><br>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Administrar Casos</h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<!-- Total elements-->
							<div class="row">
								<div class="col-md-10 col-sm-12 col-xs-12 form-group">
					                    <div class="form-inline separator bottom small">
											Total de registros: <?php echo($cantidad_datos); ?><br>
										</div>
					                  </div>
					                  <div class="col-md-2 col-sm-12 col-xs-12 form-group">
					                    <div class="form-inline separator bottom small">
					                     <form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
											<h5 id="descargar"><a href="" class="glyphicons cloud-upload" onclick="return false;"><i></i>Descargar</a><br></h5>
											<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Reporte sentinel" />
											<input type="hidden" id="datos_tabla" name="datos_tabla" />
										</form>
											<div id="mensaje_descargando"> </div>
										</div>
					                  </div>
							</div>
							<!-- // Total elements END -->
							<!-- Table elements-->
							<table id="TableData" class="display table table-responsive swipe-horizontal table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-class="expand">SENTINEL</th>
										<th data-class="expand">CONCESIONARIO</th>
										<th data-hide="phone,tablet">MODELO</th>
										<th data-hide="phone,tablet">FECHAS SENTINEL</th>
										<th data-hide="phone,tablet">CREACIÓN</th>
										<th data-hide="phone,tablet">ESTADO</th>
										<th data-hide="phone,tablet"> - </th>
										<th data-hide="phone,tablet"> - </th>
									</tr>
								</thead>
								<!-- // Table heading END -->
							</table>
							<!-- // Table elements END -->
						</div>
					</div>

					<!-- Nuevo ROW-->
					<div class="row row-app">
						<p class="separator text-center"><i class="icon-filter icon-2x"></i></p>
						<!-- Filters -->
						<form id="filtrar_casos" method="post">
							<!-- <input type="hidden" id="opcn" name="opcn" value="filtrar"> -->
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-3">
											<label class="control-label" for="dealer_id_s">Concesionario:</label>
												<select style="width: 100%;" id="dealer_id_s" name="dealer_id_s">
													<option value="0">Todos...</option>
													<?php foreach ($listadoConcesionarios as $key => $Data_Conces) { ?>
														<option value="<?php echo($Data_Conces['dealer_id']); ?>" <?php if(isset($_SESSION['dealer_id_s']) && ($Data_Conces['dealer_id']==$_SESSION['dealer_id_s']) ){?>selected="selected"<?php }?> ><?php echo($Data_Conces['dealer']); ?></option>
													<?php } ?>
												</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="sentinel_model_id_s">Modelo:</label>
												<select style="width: 100%;" id="sentinel_model_id_s" name="sentinel_model_id_s">
													<option value="0">Todos...</option>
													<?php foreach ($listadoModelos as $key => $Data_Modelos) { ?>
														<option value="<?php echo($Data_Modelos['sentinel_model_id']); ?>" <?php if(isset($_SESSION['sentinel_model_id_s']) && ($Data_Modelos['sentinel_model_id']==$_SESSION['sentinel_model_id_s']) ){?>selected="selected"<?php }?>  ><?php echo($Data_Modelos['sentinel_model']); ?></option>
													<?php } ?>
												</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="status_id_s">Estado:</label>
												<select style="width: 100%;" id="status_id_s" name="status_id_s">
													<option value="0">Todos...</option>
													<?php foreach ($listadoEstados as $key => $Data_Estados) { ?>
														<option value="<?php echo($Data_Estados['status_id']); ?>" <?php if(isset($_SESSION['status_id_s']) && ($Data_Estados['status_id']==$_SESSION['status_id_s']) ){?>selected="selected"<?php }?>  ><?php echo($Data_Estados['status']); ?></option>
													<?php } ?>
												</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="sub_status_id_s">Estado Calidad:</label>
												<select style="width: 100%;" id="sub_status_id_s" name="sub_status_id_s">
													<option value="0">Todos...</option>
													<?php foreach ($listadoSubEstados as $key => $Data_Estados) { ?>
														<option value="<?php echo($Data_Estados['substatus_id']); ?>" <?php if(isset($_SESSION['sub_status_id_s']) && ($Data_Estados['substatus_id']==$_SESSION['sub_status_id_s']) ){?>selected="selected"<?php }?>  ><?php echo($Data_Estados['substatus']); ?></option>
													<?php } ?>
												</select>
										</div>
									</div>
									<!-- Row END-->
									<!-- Row -->
									<div class="row">
										<div class="col-md-3">
											<label class="control-label" for="sentinel_service_id_s">Tipo Servicio:</label>
												<select style="width: 100%;" id="sentinel_service_id_s" name="sentinel_service_id_s">
													<option value="0">Todos...</option>
													<?php foreach ($listadoServicios as $key => $Data_Serv) { ?>
														<option value="<?php echo($Data_Serv['sentinel_service_id']); ?>" <?php if(isset($_SESSION['sentinel_service_id_s']) && ($Data_Serv['sentinel_service_id']==$_SESSION['sentinel_service_id_s']) ){?>selected="selected"<?php }?>  ><?php echo($Data_Serv['sentinel_service']); ?></option>
													<?php } ?>
												</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="sentinel_system_id_s">Sistema Afectado:</label>
												<select style="width: 100%;" id="sentinel_system_id_s" name="sentinel_system_id_s">
													<option value="0">Todos...</option>
													<?php foreach ($listadoSystems as $key => $Data_Sys) { ?>
														<option value="<?php echo($Data_Sys['sentinel_system_id']); ?>" <?php if(isset($_SESSION['sentinel_system_id_s']) && ($Data_Sys['sentinel_system_id']==$_SESSION['sentinel_system_id_s']) ){?>selected="selected"<?php }?>  ><?php echo($Data_Sys['sentinel_system']); ?></option>
													<?php } ?>
												</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="date_start_s">Fecha creación desde:</label>
											<div class="col-md-10 input-group date">
												<input class="form-control" type="text" id="date_start_s" name="date_start_s" placeholder="Fecha desde" value="<?php if(isset($_SESSION['date_start_s'])){ echo($_SESSION['date_start_s']); } ?>"/>
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="date_end_s">Fecha creación hasta:</label>
											<div class="col-md-10 input-group date">
												<input class="form-control" type="text" id="date_end_s" name="date_end_s" placeholder="Fecha hast" value="<?php if(isset($_SESSION['date_end_s'])){ echo($_SESSION['date_end_s']); } ?>"/>
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-3">
											<label class="control-label" for="date_start_s">Fecha escalado desde:</label>
											<div class="col-md-10 input-group date">
												<input class="form-control" type="text" id="date_escalado_start" name="date_escalado_start" placeholder="Fecha desde" value="<?php if(isset($_SESSION['date_escalado_start'])){ echo($_SESSION['date_escalado_start']); } ?>"/>
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="date_end_s">Fecha escalado hasta:</label>
											<div class="col-md-10 input-group date">
												<input class="form-control" type="text" id="date_escalado_end" name="date_escalado_end" placeholder="Fecha hast" value="<?php if(isset($_SESSION['date_escalado_end'])){ echo($_SESSION['date_escalado_end']); } ?>"/>
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<div class="col-md-3">
											<button type="submit" class="btn btn-primary">Buscar <i class="icon-search"></i></button>
										</div>
									</div>
									<!-- Row END-->
								</div>
							</div>
						</form>
						<!-- // Filters END -->
					</div>
					<!-- // END Nuevo ROW-->

						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>



		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/casos.js"></script>
</body>
</html>
