<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_trayectorias.php');
$location = 'reporting';
$locData = true;
//$Asist = true;
$DashBoard = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons podium"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_trayectorias.php">Ranking por Trayectoria</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Informe de Ranking por Trayectoria</h2>
							<br><br>
						</div>
					<!-- // END heading -->
<!-- contenido filtros -->
	<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Contenido</h5><br>
			</div>
			<div class="col-md-2">

				<?php if(isset($_POST['start_date_day'])){?>
					<h5><a href="rep_trayectorias_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			
			</div>
		</div>
		<div class="row">
			<form action="rep_trayectorias.php" method="post" id="formulario_usuarios">
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Hasta..."<?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..."<?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
					<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- // END contenido filtros -->

<!-- inicio tabla  -->
<?php  if (isset($trayectorias_all) ) { ?>

<div class="well">
							<?php 
								foreach ($trayectorias_all as $key => $DatosListos) {
							 	if (isset($DatosListos['Details'])) {
							 		$Cant_ct = round(count($DatosListos['Details'])*0.10);
							 		if($Cant_ct==0) $Cant_ct=1;
							?>
								<table class="table table-invoice">
									<tbody>
										<tr>
											<td style="width: 30%;">
												<p class="lead">Trayectoria</p>
												<h4><?php echo $DatosListos['charge'] ?> </h4>
												<address class="margin-none">
													# Cursos: <strong><span style="color:blue;"><?php echo($DatosListos['cantidad']); ?></span></strong><br/>
													Estudiantes: <strong><span style="color:blue;"><?php echo(count($DatosListos['Details'])); ?></span></strong><br/>
													10% Punteando: <strong><span style="color:blue;"><?php echo($Cant_ct); ?></span></strong><br/>
												</address>
											</td>

											
											<td class="right">
												<p class="lead">Asistencia</p>
												<!-- // Table -->
												<table class="table table-condensed table-vertical-center table-thead-simple">
													<thead>
														<tr>
															<th class="center">No.</th>
															<th class="center">Identificacion</th>
															<th class="center">Nombre</th>
															<th class="center">Concesionario</th>
															<th class="center">Sede</th>
															<th class="center"># Cursos</th>
															<th class="center">Promedio de notas</th>
														</tr>
													</thead>

													<tbody id="cuerpo_tabla">
											<?php 
											$ctrl = 0;
											$cour_fn = 0;
											$prom_fn = 0;
											$cour_ac = 0;
											$prom_ac = 0;
												/*$fin = (count($DatosListos['Details'])*0.10);
												$i=0;*/$contador=1;
												//echo("Cantidad de Cursos: ".$DatosListos['cantidad']." | ");
												//echo "Cantidad de participantes ".count($DatosListos['Details'])." | ";
												//echo "el 10 % de esta trayectoria en este rango de tiempo son ".  round(count($DatosListos['Details'])*0.10)." Participantes";
											foreach ($DatosListos['Details'] as $key => $DatosDetalle) {
												$ctrl++;
													/*if ($DatosDetalle['promedio']<75) {
														continue;
													}*/
													if($ctrl==$Cant_ct){
														$cour_fn = $DatosDetalle['cantidad_curso'];
														$prom_fn = round($DatosDetalle['promedio'],1);
													}else if($ctrl>$Cant_ct){
														$cour_ac = $DatosDetalle['cantidad_curso'];
														$prom_ac = round($DatosDetalle['promedio'],1);
													}
												?>
															<!-- Item -->
															<tr class="selectable">
																<td class="center" style="padding: 2px; font-size: 80%;<?php if($ctrl<=$Cant_ct || ($cour_fn==$cour_ac && $prom_fn==$prom_ac) ){ echo("color:blue;"); }?>"><?php echo $contador; ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;<?php if($ctrl<=$Cant_ct || ($cour_fn==$cour_ac && $prom_fn==$prom_ac) ){ echo("color:blue;"); }?>"><?php echo $DatosDetalle['identification'] ; ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;<?php if($ctrl<=$Cant_ct || ($cour_fn==$cour_ac && $prom_fn==$prom_ac) ){ echo("color:blue;"); }?>"><?php echo $DatosDetalle['first_name'].' '.$DatosDetalle['last_name']; ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;<?php if($ctrl<=$Cant_ct || ($cour_fn==$cour_ac && $prom_fn==$prom_ac) ){ echo("color:blue;"); }?>"><?php echo $DatosDetalle['dealer']; ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;<?php if($ctrl<=$Cant_ct || ($cour_fn==$cour_ac && $prom_fn==$prom_ac) ){ echo("color:blue;"); }?>"><?php echo $DatosDetalle['headquarter']; ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;<?php if($ctrl<=$Cant_ct || ($cour_fn==$cour_ac && $prom_fn==$prom_ac) ){ echo("color:blue;"); }?>"><?php echo $DatosDetalle['cantidad_curso']; ?></td>
																<td class="center" style="padding: 2px; font-size: 80%;<?php if($ctrl<=$Cant_ct || ($cour_fn==$cour_ac && $prom_fn==$prom_ac) ){ echo("color:blue;"); }?>"><?php echo round($DatosDetalle['promedio'],1); ?></td>
															</tr>
															<!-- // Item END -->	
														<?php
														$contador++;
														 } ?>
														
													</tbody>
												</table>
												<!-- // Table END -->
											</td>
										</tr>
										
										<tr>
										
										</tr>
									<?php }}?>	
									</tbody>
								</table>
							</div>


<?php  } ?>
<!-- Fin tabla  -->
<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div> 
				</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_trayectorias.js"></script>
</body>
</html>