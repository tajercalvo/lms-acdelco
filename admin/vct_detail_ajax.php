<?php
include('src/seguridad.php');
if(isset($_POST['opcn'])){

  switch($_POST['opcn']){
    //
    //opcion que compara los textos para filtrar los nombres si estos tienen coincidencias
    //
    case 'search':
      include_once('models/vct_detail.php');
      $cursos_class = new CursosVCT();
      $registrosAjax= $cursos_class->consultaRegistros($_POST['idvcts'],$_POST['schedule'],'');
      ?>
        <?php
        if($_POST['datos'] == ''){
          foreach ($registrosAjax as $keyAjax => $valueAjax) { ?>
            <div class="col-md-6 padding-none" id="<?php echo($valueAjax['inscription_id']); ?>">
              <div class="media innerAll padding-none">
                <!--<a href="ver_perfil.php?back=usuarios&id=<?php echo($valueAjax['user_id']); ?>" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/<?php echo($valueAjax['image']); ?>" alt="Image" style="width: 50px;"/></a>-->
                <div class="media-body">
                  <span class="strong text-primary"><?php echo(ucwords(strtolower($valueAjax['first_name']." ".$valueAjax['last_name']))); ?></span><br/>
                  <small class="text-italic text-primary-light"></i><?php echo($valueAjax['dealer']); ?></small><br/>
                  <small><?php echo($valueAjax['headquarter']); ?></small>
                </div>
              </div>
            </div>
        <?php }//fin foreach
      }else{
        foreach ($registrosAjax as $keyAjax => $valueAjax){
          if(stristr($valueAjax['first_name'], $_POST['datos']) || stristr($valueAjax['last_name'],$_POST['datos']) ){?>
            <div class="col-md-6 padding-none" id="<?php echo($valueAjax['inscription_id']); ?>">
              <div class="media innerAll padding-none">
                <!--<a href="ver_perfil.php?back=usuarios&id=<?php echo($valueAjax['user_id']); ?>" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/<?php echo($valueAjax['image']); ?>" alt="Image" style="width: 50px;"/></a>-->
                <div class="media-body">
                  <span class="strong text-primary"><?php echo(ucwords(strtolower($valueAjax['first_name']." ".$valueAjax['last_name']))); ?></span><br/>
                  <small class="text-italic text-primary-light"></i><?php echo($valueAjax['dealer']); ?></small><br/>
                  <small><?php echo($valueAjax['headquarter']); ?></small>
                </div>
              </div>
            </div>
      <?php }//fin if
        }//fin foreach
      } // fin else ?>
    <?php
    break;//fin opcion search
    //
    // carga los ultimos comentarios hechos
    //
    case 'comments':
      include_once('models/vct_detail.php');
      $cursos_class = new CursosVCT();
      if(isset($_POST['lastId'])){
        sleep(2);
          @session_start();
          $comentariosAjax = $cursos_class->consultaUltimosComentarios($_POST['idvcts'],$_POST['lastId']);
          $datos_Usuarios = $cursos_class->consultaDatos_usuarios();
          $datosJson['mensajes'] = $comentariosAjax;
          $datosJson['datos_usuario'] = $datos_Usuarios;

          echo json_encode($datosJson);
     /* foreach ($comentariosAjax as $keyCom => $comValue) { ?>
        <li class="row row-merge border-none" id="<?php echo($comValue['comments_id']); ?>">
          <div class="col-md-12">
            <div class="innerAll padding-none">
              <div class="media">
                <!--<a href="ver_perfil.php?back=usuarios&id=<?php echo($comValue['user_id']); ?>" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/<?php echo($comValue['image']); ?>" alt="Image" style="width: 50px;"/></a>-->
                <div class="media-body">
                  <small class="pull-right text-primary-light"><?php if($_SESSION['idUsuario'] == $comValue['user_id'] ){?><i class="fa fa-fw icon-checkmark-thick text-primary"></i><?php }else{ ?><i class="fa fa-fw icon-envelope-fill-1 text-primary"></i> <?php } ?><?php echo($comValue['date_comment']); ?><em></em></small>
                  <strong class="text-primary"><?php echo(ucwords(strtolower($comValue['first_name']." ".$comValue['last_name']))); ?></strong><br/>
                  <small><?php echo($comValue['comment_text']); ?></small>
                </div>
              </div>
            </div>
          </div>
        </li>
    <?php ; }*/

    }
    break;//fin opcion comments
    case 'comments2':
      include_once('models/vct_detail.php');
      $cursos_class = new CursosVCT();
      if(isset($_POST['lastId'])){
        //sleep(2);
          @session_start();
          $comentariosAjax = $cursos_class->consultaUltimosComentarios2($_POST['idvcts'],$_POST['lastId']);
          echo json_encode($comentariosAjax);

    }
    break;//fin opcion comments
    //
    //Actualiza los usuarios registrados mediante ajax
    //
    case 'inscripcion':
      include_once('models/vct_detail.php');
      $cursos_class = new CursosVCT();
      $nuevosAjax = $cursos_class->consultaNuevoRegistros($_POST['idvcts'],$_POST['last_id']);
      foreach ($nuevosAjax as $keyNue => $nueValue){ ?>
        <li class="active" id="<?php echo($nueValue['inscription_id']); ?>">
          <div class="media innerAll">
            <div class="media-object pull-left thumb hidden-phone"><img src="../assets/images/usuarios/<?php echo($nueValue['image']); ?>" alt="Image" style="width: 50px;"/></div>
            <div class="media-body">
              <span class="strong"><?php echo($nueValue['first_name']." ".$nueValue['last_name']); ?></span><br/>
              <small class="text-italic text-primary-light"></i><?php echo($nueValue['dealer']); ?></small><br/>
              <small><?php echo($nueValue['headquarter']); ?></small>
            </div>
          </div>
        </li>
    <?php
    }
    break;//fin case inscription
  }//fin switch
}?>
