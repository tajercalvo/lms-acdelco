<?php include('src/seguridad.php'); ?>
<?php include('controllers/course_detail.php');
$location = 'home';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons briefcase"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/cursos.php">Detalle Cursos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Detalle del Curso </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="btn-group pull-right">
							<h5 onclick="history.back()"><a href="#" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<!-- row -->
	<div class="row ">
		<!-- col -->
		<div class="col-md-9">
			<!-- Media -->
			<div class="widget media innerB">
				<div class="innerAll">
			  		<span class="pull-left bg-primary innerAll text-center text-white"><?php echo('<img src="../assets/images/distinciones/'.$registroConfiguracionDetail['image'].'" style="width: 200px;" alt="'.$registroConfiguracionDetail['course'].'">'); ?></span>
			  	</div>
			  	<div class="media-body innerLR">
			    	<div class="innerLR">
			    		<h2 class="media-heading margin-none text-primary"><?php echo($registroConfiguracionDetail['course']); ?></h2>
			    		<h5>Objetivo general: <span class="text-warning"><?php echo(ucfirst(strtolower($registroConfiguracionDetail['description']))); ?></span></h5>
						<h5>Objetivos específicos: <span class="text-warning"><?php echo($registroConfiguracionDetail['target']); ?></span></h5>
			    		<h5>Metodología: <span class="text-warning"><?php echo(ucfirst(strtolower($registroConfiguracionDetail['prerequisites']))); ?></span></h5>
			    		<div class="btn-group pull-right">
				    		<?php if($registroConfiguracionDetail['type']=="WBT" && $registroConfiguracionDetail['status_id']=="1"){ ?>
				    			<?php if( !isset($inscritoCourse) || (!$inscritoCourse)){
									if(count($preguntas) > 0 ){ ?>
										<a href="#preguntasSeguridad" data-toggle="modal" class="btn btn-default" id="security_question"><i class="fa fa-fw fa-floppy-o"></i> Inscribirme</a>
										<a href="course_detail.php?token=<?php echo(md5('GMAcademy')); ?>&_valSco=<?php echo(md5('GMColmotores2015')); ?>&opcn=inscripcion&id=<?php echo($registroConfiguracionDetail['course_id']); ?>" class="btn btn-default" id="inscripcion" style="display: none;"><i class="fa fa-fw fa-floppy-o"></i> Inscribirme</a>
								<?php }else{?>
								<span class="text-danger">Para inscribirte debes tener las preguntas de seguridad completadas!</span>
								<?php }?>
				    			<?php }else{ ?>
				    				<a href="#" onclick="return false;" class="btn btn-default"><i class="fa fa-fw fa-floppy-o"></i> Ya estás inscrito!</a>
				    			<?php } ?>
				    		<?php }?>
			    		</div>
			    	</div>
			  	</div>
			</div>
			<!-- End Media -->
			<div class="widget innerAll inner-2x justify">
				<h4 class="innerTB half">Módulos</h4>
				<p>Este curso esta compuesto por los siguientes módulos:</p>
				<?php foreach ($registroConfiguracionModulosDetail as $iID => $data_modules) {
					?>
					<p>
						<span>
							<i class="fa fa-fw fa-archive"></i>
							<?php echo("<strong class='text-success'>".$data_modules['codigo_curso'].' | '.$data_modules['module']."</strong>"); ?>
							<p><strong class='text-primary'>Duración:</strong> <?php echo($data_modules['duration_time'].' Horas'); ?><br>
								<strong class='text-primary'>Tipo:</strong> <?php echo($data_modules['type']); ?></p>
						</span>
					</p>
				<?php } ?>
				<p><a href="http://www.chevrolet.com.co/" target="_blank"> <i class="fa fa-fw icon-scale-2"></i> Este material está basado en múltiples documentos de Isuzu, GM y otros fabricantes de autos, así como en material de uso libre obtenido de Internet</a></p>
				<?php if($data_modules['type']=="WBT"){ ?>
				<br/>
				<div class="well">
				<?php if( !isset($inscritoCourse) || (!$inscritoCourse)){ ?>
					<strong class='text-success'>Debes estar inscrito para ver el material!<br>Da clic en el botón "Inscribirme" para presentar el curso</strong>
				<? }else{ ?>
				<table class="table table-condensed table-vertical-center table-thead-simple">
					<thead>
						<tr>
							<th class="center">Módulo</th>
							<th class="center" style="width: 15%">Intentos</th>
							<th class="center" style="width: 15%">Máxima Nota</th>
							<th class="center" style="width: 15%">Estado</th>
							<th class="center" style="width: 15%">Disponibilidad</th>
							<th class="center" style="width: 15%">Ver</th>
						</tr>
					</thead>
					<tbody id="cuerpo_tabla">
						<?php
						$_posModel = 0;
						$_AntStatus = "Completo";
						$CantRecursos = 0;
						$sumaNotas = 0;
						foreach ($ModulosScorm as $iID => $data_scorm){
							$MaxNota = 0;
							$CantIntentos = 0;
							if($data_scorm['maxNota']!=''){
								$MaxNota = $data_scorm['maxNota'];
							}
							if($data_scorm['cantidadAttempts']!=''){
								$CantIntentos = $data_scorm['cantidadAttempts'];
							}
							$Estado = "Sin Iniciar";
							if($data_scorm['Status']=="completed"){
								$Estado = "Completo";
							}else if($data_scorm['Status']=="incomplete"){
								$Estado = "Incompleto";
							}else if($data_scorm['Status']=="failed"){
								$Estado = "Fallado";
							}else if($data_scorm['Status']=="browsed"){
								$Estado = "Visto";
							}else if($data_scorm['Status']=="passed"){
								$Estado = "Resuelto";
							}else if($data_scorm['Status']=="not attempted"){
								$Estado = "Sin Intentos";
							}
							$CantRecursos++;
							$sumaNotas = $MaxNota+$sumaNotas;
							?>
							<tr class="selectable">
								<td class="left" style="padding: 2px; font-size: 80%;"><?php echo($data_scorm['title']); ?></td>
								<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($CantIntentos); ?></td>
								<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($MaxNota); ?></td>
								<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary"><?php echo($Estado); ?></span></td>
								<?php if( ($_AntStatus=="Completo" || $_AntStatus=="Resuelto" ) && ($_posModel!=0) ){ ?>
									<td class="center" style="padding: 2px; font-size: 80%;">Disponible</td>
								<?php }else if($_posModel==0){ ?>
									<td class="center" style="padding: 2px; font-size: 80%;">Disponible</td>
								<?php }else{ ?>
									<td class="center" style="padding: 2px; font-size: 80%;">No Disponible</td>
								<?php } ?>
								<td class="center" style="padding: 2px; font-size: 80%;">
								<?php if( ($_AntStatus=="Completo" || $_AntStatus=="Resuelto" ) && ($_posModel!=0) ){ ?>
										<a href="CourseScorm.php?Validator=<?php echo(md5($data_scorm['md5hash'].$data_scorm['scorm_id'].$data_scorm['scorm_sco_id'])); ?>&Md5Hash=<?php echo($data_scorm['md5hash']); ?>&ScormId=<?php echo($data_scorm['scorm_id']); ?>&ScoId=<?php echo($data_scorm['scorm_sco_id']); ?>&Parent=<?php echo($data_scorm['parent']); ?>&identifier=<?php echo($data_scorm['identifier']); ?>&URLCourse=<?php echo($data_scorm['code']); ?>/<?php echo($data_scorm['launch']); ?>" target="_blank" class="btn btn-success" style="font-size: 80%;"><i class="fa fa-fw fa-sign-in"></i> ENTRAR </a>
									<?php }else if($_posModel==0){ ?>
										<a href="CourseScorm.php?Validator=<?php echo(md5($data_scorm['md5hash'].$data_scorm['scorm_id'].$data_scorm['scorm_sco_id'])); ?>&Md5Hash=<?php echo($data_scorm['md5hash']); ?>&ScormId=<?php echo($data_scorm['scorm_id']); ?>&ScoId=<?php echo($data_scorm['scorm_sco_id']); ?>&Parent=<?php echo($data_scorm['parent']); ?>&identifier=<?php echo($data_scorm['identifier']); ?>&URLCourse=<?php echo($data_scorm['code']); ?>/<?php echo($data_scorm['launch']); ?>" target="_blank" class="btn btn-success" style="font-size: 80%;"><i class="fa fa-fw fa-sign-in"></i> ENTRAR </a>
									<?php } ?>
								</td>
								<!--<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary"></span></td>-->
							</tr>
						<?php $_posModel = 1; $_AntStatus = $Estado;
						} ?>
					</tbody>
				</table>
				<br/>
				<br/>
				<strong class='text-success' style='padding: 2px; font-size: 140%;'>
					IMPORTANTE: A continuación encontrará la nota que el módulo virtual le esta asignando por su participación, puede registrar esta nota o ejecutar un nuevo intento, cuando de click en "REGISTRAR", la nota será computada en su historial y podrá descargar el certificado si obtiene un puntaje superior a 80.
				</strong>
				<br/>
				<br/>
				<table class="table table-condensed table-vertical-center table-thead-simple">
					<thead>
						<tr>
							<th>Resultado General</th>
							<th class="center" style="width: 15%">Nota</th>
							<th class="center" style="width: 10%">Acción</th>
						</tr>
					</thead>
					<tbody id="cuerpo_tabla">
					<?php
					$porcentaje = 0;
					if($CantRecursos>0){
						$porcentaje = number_format($sumaNotas/$CantRecursos,0);
					}

					?>
						<tr class="selectable">
							<input type="hidden" id="reg_module_id" name="reg_module_id" value="<?php echo($data_modules['module_id']); ?>"/>
							<input type="hidden" id="reg_nota" name="reg_nota" value="<?php echo($porcentaje); ?>"/>
							<input type="hidden" id="reg_course_id" name="reg_course_id" value="<?php echo($registroConfiguracionDetail['course_id']); ?>"/>
							<input type="hidden" id="reg_fecha" name="reg_fecha" value="<?php echo(date("Y-m-d")." ".(date("H")).":".date("i:s")); ?>"/>
							<td class="left" style="padding: 2px; font-size: 100%;"><?php echo($data_modules['module']); ?></td>
							<td class="center" style="padding: 2px; font-size: 100%;"><?php echo($porcentaje); ?></td>
							<td class="center" style="padding: 2px; font-size: 100%;">
								<a href="#preguntasSeguridad" data-toggle="modal" class="btn btn-success" id="registrarNotaSeguridad" style="font-size: 170%;height: 45px !important;width: 300px !important;"><i class="fa fa-check-circle"></i> REGISTRAR NOTA</a>
								<button type="submit" class="btn btn-success" id="registrarNota" style="font-size: 170%;height: 100px !important;width: 300px !important;display: none"><i class="fa fa-check-circle"></i> REGISTRAR NOTA</button>
							</td>
						</tr>
					</tbody>
				</table>
				<?php } ?>
				</div>
				<!--<u>

					<?php /*if(isset($_GET['id']) ){ ?>
						<a href="../Courses/COV_Bienvenidos/Modulo0/" target="_blank" class="text-success">SECCIÓN 0 - INTRODUCCIÓN</a><br>
						<a href="../Courses/COV_Bienvenidos/Modulo1/" target="_blank" class="text-success">SECCIÓN 1 - HISTORIA</a><br>
						<a href="../Courses/COV_Bienvenidos/Modulo2/" target="_blank" class="text-success">SECCIÓN 2 - LA EMPRESA</a><br>
						<a href="../Courses/COV_Bienvenidos/Modulo3/" target="_blank" class="text-success">SECCIÓN 3 - PRODUCTOS</a><br>
						<a href="../Courses/COV_Bienvenidos/Modulo4/" target="_blank" class="text-success">SECCIÓN 4 - RED DE CONCESIONARIOS</a><br>
						<a href="../Courses/COV_Bienvenidos/Modulo5/" target="_blank" class="text-success">SECCIÓN 5 - VENTAS Y POSVENTA</a><br>
						<a href="../Courses/COV_Bienvenidos/Modulo6/" target="_blank" class="text-success">SECCIÓN 6 - CUESTIONARIO</a><br>
					<?php }*/ ?>

				</u>-->
				<?php } ?>
			<!-- End Widget -->
			</div>
		</div>
		<!-- // END col -->

		<!-- col -->
		<div class="col-md-3">
			<div class="widget  padding-none">
				<div class="widget-body padding-none">
					<h5 class="innerAll bg-primary text-white margin-bottom-none">Recursos para lectura:</h5>
					<ul class="list-group list-group-1 margin-none borders-none">
						<?php foreach ($MaterialCargado as $key => $DataFiles) { ?>
							<li class="list-group-item"><a href="../assets/FilesCursos/<?php echo($DataFiles['file']); ?>" target="_blank"><i class="icon-car"></i>&nbsp; <?php echo($DataFiles['name']); ?></a>
							<?php if( $_SESSION['max_rol']==2 || $_SESSION['max_rol']>=5 ){ ?>
								<a href="course_detail.php?token=<?php echo($_GET['token']); ?>&_valSco=<?php echo($_GET['_valSco']); ?>&opcn=ver&id=<?php echo($_GET['id']); ?>&opcn_f=borrarFile&nomFile=<?php echo($DataFiles['file']); ?>">[Borrar]</a>
							<?php } ?></li>
						<?php } ?>
						<!--<li class="list-group-item border-top-none"><a href="#"><i class="icon-truck"></i>&nbsp; Material práctica 1</a></li>-->
					</ul>
				</div>
				<br><br>
				<?php if( $_SESSION['max_rol']==2 || $_SESSION['max_rol']>=5 ){ ?>
					<div class="widget-body padding-none center">
						<div class="ajax-loading hide" id="loading_file">
							<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
						</div>
						<input class="form-control" id="name_file" name="name_file" type="text" placeholder="Nombre" value="" /><br>
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Seleccionar Archivo PDF</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="image_new" name="image_new" />
							</span>
							<input type="hidden" id="course_file" name="course_file" value="<?php echo($_GET['id']); ?>">
							<input type="hidden" id="token_file" name="token_file" value="<?php echo(md5('GMAcademy')); ?>">
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
						<div class="form-group">
					    	<button type="button" class="btn btn-primary" onclick="cargaFile();"><i class="fa fa-check-circle"></i> Cargar Archivo</button>
					  	</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<!-- // END col -->
	</div>
	<!-- // END row -->
						<!-- Nuevo ROW-->
						<div class="row row-app">
						</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<!-- fin validacion preguntas -->
		<?php ?>
		<div class="modal fade" id="preguntasSeguridad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Crear Nuevo Argumentario Comercial</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<div class="row">
									<div class="col-md-1"></div>
									<div class="col-md-10">
										<h2>Preguntas de seguridad: </h2>
									</div>
									<div class="col-md-1"></div>
								</div>
								<div class="separator"></div>
								<?php if( count($preguntas) > 0 ){ ?>
								<div id="contenedorSeguridad">
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-10">
											<label for="ask1"><?php echo($preguntas[0]['security_question']); ?></label>
											<input type="hidden" name="id_1" id="id_1" value="<?php echo($preguntas[0]['security_answer_id']); ?>">
											<div class="col-md-11">
												<input type="password" name="ask1" id="ask1" placeholder="Pregunta 1:" class="form-control">
											</div>
											<div class="col-md-1"><i class="fa fa-eye btn" id="ask1Over"></i></div>
										</div>
										<div class="col-md-1"></div>
									</div>
									<div class="separator"></div>
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-10">
											<label for="ask2"><?php echo($preguntas[1]['security_question']); ?></label>
											<input type="hidden" name="id_2" id="id_2" value="<?php echo($preguntas[1]['security_answer_id']); ?>">
											<div class="col-md-11">
												<input type="password" name="ask2" id="ask2" placeholder="Pregunta 2:" class="form-control">
											</div>
											<div class="col-md-1"><i class="fa fa-eye btn" id="ask2Over"></i></div>
										</div>
										<div class="col-md-1"></div>
									</div>
									<div class="separator"></div>
								</div>
								<?php }else{ ?>
								<div id="contenedorSeguridad">
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-10">
											Es necesario primero realizar el registro de sus <a href="security_questions.php">preguntas de seguridad</a>, antes de continuar con la inscripción a este curso virtual.
										</div>
										<div class="col-md-1"></div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<?php if((isset($inscritoCourse) && $inscritoCourse > 0) && count($preguntas) > 0){ ?>
							<button type="button" id="btnValidarNota" class="btn btn-primary">Validar</button>
						<?php }else{ ?>
							<button type="button" id="btnValidar" class="btn btn-primary">Validar</button>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php ?>
		<!--Fin modal validacion preguntas -->
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/cursos.js"></script>
</body>
</html>
