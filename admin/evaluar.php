<?php
@session_start();
// if( !isset($_SESSION['id_evaluacion']) && !isset($_SESSION['_EvalCour_id_evaluacion']) ){
// 	header("location: index.php");
// }else{
// 	if(isset($_SESSION['id_evaluacion'])){
// 		$review_id = $_SESSION['id_evaluacion'];
// 	}
// 	if(isset($_SESSION['_EvalCour_id_evaluacion'])){
// 		$review_id = $_SESSION['_EvalCour_id_evaluacion'];
// 	}
// }
include_once('src/seguridad.php'); ?>
<?php include_once('controllers/evaluar.php');
$location = 'education';
//$evaluarDatConf = 1;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons circle_question_mark"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/evaluar.php">Evaluación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<!-- <h2 class="margin-none pull-left"><?php echo $registroEvaluacion['review']; ?></h2> -->
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="innerLR spacing-x2">
						<!-- row -->
						<div class="row">
							<!-- col -->
							<div class="col-md-6 col-md-offset-3">
								<div class="widget widget-body-white">
									<div class="border-bottom innerAll">
										<div class="media margin-none innerAll">
											<div class="pull-left">
												<img src="../assets/images/distinciones/default.png" style="width:100px;" class="img-responsive">
											</div>
											<div class="media-body">
												<div class="innerAll half media margin-none">
													<div class="pull-left innerLR half">
														<p class="strong">AtuotrainAcademy - Evaluaciones</p>
														<p class="margin-none"><strong>Nota! </strong>Por favor lea atentamente las recomendaciones:</p><br />
														<?php if (isset($registroEvaluacion['score'])) { ?>
															<p class="strong">Evaluación : <?php echo ($registroEvaluacion['review']); ?></p>
															<p class="margin-none"><strong>Importante! </strong>La nota de esta evaluación remplazará la nota que actualmente tiene del curso, por favor nuevamente lea las recomendaciones y conteste atentamente las preguntas que allí se hacen</p>
														<?php } ?>
														<?php if (isset($registroEvaluacion['review_id'])) { ?>
															<p class="strong">Evaluación : <?php echo ($registroEvaluacion['review']); ?></p>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="widget widget-survey">
									<?php if (!isset($consultaEvaluacionYa['reviews_answer_id'])) { ?>
										<div class="widget-head ">
											<div class="innerAll text-center">
												<ul class="nav nav-pills strong display-block-inline">
													<li class="active"><a href="#reloj" onclick="return false;">Disponible: <span id="minAvailable"><?php echo ($registroEvaluacion['time']); ?>:00</span></a></li>
													<li class="active"><a href="#reloj" onclick="return false;">Transcurrido: <span id="min">00</span>:<span id="seg">00</span></a></li>
													<!--<li class="active"><a href="#reloj" onclick="return false;">Preguntas: <span id="cantPreg">0</span> / <span id="cantAvailable"><?php echo ($registroEvaluacion['cant_question']); ?></span></a></li>-->
												</ul>
											</div>
										</div>
									<?php } ?>
									<div class="widget-body padding-none">
										<div class="tab-content">
											<div class="tab-pane active">
												<div class="ajax-loading center hide" id="loadingRespuesta"><br><br><br><br>
													<i class="fa fa-spinner fa fa-spin fa fa-4x"></i><br><br><br><br>
												</div>
												<?php if (!isset($consultaEvaluacionYa['reviews_answer_id'])) { ?>
													<form id="formulario_respuestas" method="post" autocomplete="off">
														<div class="innerAll" id="PanelPregunta">
															<h4 class="innerTB margin-none half center">- Recomendaciones Importantes - <?php //echo($_SESSION['_EvalCour_ResultId']); 
																																		?></h4>
															<div class="separator bottom"></div>
															<div class="row">
																1. Una vez de clic en "INICIAR" comenzará a correr el reloj.
																<br>2. Cuando el reloj inicie no habrá vuelta atrás, no se puede detener, deberá responder la evaluación.
																<br>3. No puede cerrar la página, de lo contrario no se guardará ninguna respuesta que haya dado hasta ese momento y se calificará en cero.
																<br>4. Debe estar seguro de contar con una conexión a internet estable.
																<br>5. Una vez el cronometro llegue a su fin, el sistema calificará en cero y no podrá presentar la evaluación de nuevo.
																<br>6. Si no alcanza a responder todas las preguntas, este pendiente del tiempo por favor y envíe la evaluación con las que alcanzó a responder.
																<br>7. No puede refrescar la página, si lo hace pueden salir estas instrucciones pero no podrá volver a contestarla.
																<br><span class="text-danger">8. Por favor verifíque su conexión a internet y tenga en cuenta que esta evaluación la puede realizar únicamente una vez.</span>
																<br><span class="text-success">9. Luego de presentar esta evaluación y si la supera con 80 puntos o más, tendrá la posibilidad de responder unas preguntas de profundización que le permitirán obtener hasta 10 puntos extra.</span>
																<br><br><strong>Muchos Éxitos!</strong>
															</div>
														</div>
														<input type="hidden" id="review_id" name="review_id" value="<?php echo ($review_id); ?>" />
														<input type="hidden" id="reviews_answer_id" name="reviews_answer_id" value="0" />
														<input type="hidden" id="available_score" name="available_score" value="100" />
														<input type="hidden" id="question_increment" name="question_increment" value="0" />
														<input type="hidden" id="opcn" name="opcn" value="RegistrarRespuesta">
														<input type="hidden" id="min_time" name="min_time" value="0">
														<input type="hidden" id="seg_time" name="seg_time" value="0">
													</form>
												<?php } else { ?>
													<div class="innerAll">
														<h4 class="innerTB margin-none half center">- Nota -</h4>
														<div class="separator bottom"></div>
														<div class="row">
															<?php
															unset($_SESSION['id_evaluacion']);
															unset($_SESSION['name_evaluacion']);
															unset($_SESSION['_EvalCour_id_evaluacion']);
															unset($_SESSION['_EvalCour_ResultId']);
															unset($_SESSION['_EvalCour_Name']);
															unset($_SESSION['_EvalCour_Date']);
															unset($_SESSION['_EvalCour_Score']);
															?>
															Usted ya presento esta evaluación al menos una vez el día <?php echo ($consultaEvaluacionYa['date_creation']); ?>! No puede volver a presentarla.
														</div>
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<?php if (!isset($consultaEvaluacionYa['reviews_answer_id'])) { ?>
									<div id="BtnIniciar" class="innerAll text-center">
										<a href="#IniciarEvaluacion" onclick="IniciarEvaluacion(); return false;" class="btn btn-primary">Iniciar</a>
									</div>
								<?php } ?>
							</div>
							<!-- // END col -->

						</div>
						<!-- // END row -->
					</div>



					<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/evaluar.js?temp=<?php echo (rand(0, 10000)); ?>"></script>
	<script type="text/javascript">
		window.onbeforeunload = function exitAlert() {
			var textillo = "Los datos que no se han guardado se perderán, siga adelante si ya vió la retroalimentación a su examen.";
			return textillo;
		}
	</script>
</body>

</html>