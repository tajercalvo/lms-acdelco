<?php include('src/seguridad.php'); 
 include('controllers/costo_especial.php');
$location = 'costo';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons globe"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/ciudades.php">Configuración Cobros Especiales</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Cobros Especiales </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10"></div>
								<div class="col-md-2">
									<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
										<h5><a href="costo_especial.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
							</div>	
						</div>
					</div>

<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Guardar ítem</h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body innerAll inner-2x">
		<form id="formulario_data" method="post" autocomplete="off">
			<!-- Row -->
			<div class="row innerLR">
				<!-- Column -->
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tipo" style="padding-top:8px;">Tipo de Cobro Especial:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="tipo" name="tipo">
								<option value="1" > Valor Ajuste a Sesion</option>
								<option value="2" > Valor Ajuste de Mes</option>
							</select>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-1"></div>
				<!-- // Column END -->
			</div>
			<!-- // Row END -->
			<br>
			<!-- Row -->
			<div class="row innerLR">
				<!-- Column -->
				<div class="col-md-1"></div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group" id="fecha_inicial">
						<label class="col-md-4 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial Sesion:</label>
						<div class="col-md-8 input-group date">
					    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" />
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<!-- // Column END -->
				<!-- Column -->
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group" id="fecha_final">
						<label class="col-md-4 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final Sesion:</label>
						<div class="col-md-8 input-group date">
					    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" />
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-1"></div>
				<!-- // Column END -->
			</div>
			<!-- // Row END -->
			<!-- Row -->
			<div class="row innerLR" id="sesiones">
				<!-- Column -->
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Sesión:</label>
						<div class="col-md-10">
							<!-- Inicio select de Sesiones -->
							<input id="schedule_id" name="schedule_id" type="text" value="" style="width: 100%;"/>
							<!-- fin de select -->
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-1"></div>
				<!-- // Column END -->
			</div>
			<!-- // Row END -->
			<br>
			<!-- Row -->
			<div class="row innerLR">
				<!-- Column -->
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="dealer_id" style="padding-top:8px;">Concesionario:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="dealer_id" name="dealer_id" >
			                </select>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-1"></div>
				<!-- // Column END -->
			</div>
			<!-- // Row END -->
			<br>
			<!-- Row -->
			<div class="row innerLR">
				<!-- Column -->
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="tarifa" style="padding-top:8px;">Tarifa:</label>
						<div class="col-md-5">
							<input class="form-control" id="tarifa" name="tarifa" type="number" placeholder="Valor tarifa total" value="">
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-1"></div>
				<!-- // Column END -->
			</div>
			<!-- // Row END -->

			<div class="separator"></div>
			<!-- Form actions -->
			<div class="form-actions center">
				<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Guardar Registro</button>
				<input type="hidden" id="opcn" name="opcn" value="crear">
				<input type="hidden" id="id_cost" name="id_cost" value="0">
			</div>
			<!-- // Form actions END -->
		</form>
	</div>
</div>

				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/costo_especial.js?varRand=<?php echo(rand(10,999999));?>"></script>

	<?php  if( isset($_GET['opcn']) && isset($_GET['id']) ){
	if ( $_GET['opcn'] == 'editar' ) { ?>
		<script>
			cargar_datos( <?php echo $_GET['id']; ?> );
		</script>
	<?php } } ?>
</body>
</html>