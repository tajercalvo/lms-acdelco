<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['year']) && isset($_GET['month']) && isset($_GET['type'])){
    include('controllers/rep_costo.php');
}
//echo('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Costo_Simplificado.xls");
header ("Content-Transfer-Encoding: binary");
if( isset($_GET['year']) && isset($_GET['month']) && isset($_GET['type']) ){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">COBRO ENTRENAMIENTO</th>
                <th data-hide="phone,tablet">DESCUENTO PLAN SEDE</th>
                <th data-hide="phone,tablet">DESCUENTO ALIMENTACION</th>
                <th data-hide="phone,tablet">PARTICIPANTES</th>
                <th data-hide="phone,tablet">EXCUSAS</th>
                <th data-hide="phone,tablet">AJUSTE MES</th>
                <th data-hide="phone,tablet">TOTAL A PAGAR</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php if($cantidad_datos > 0){ 
                foreach ($RegistrosCursos_all as $iID => $data) { 
                    $total = 0;
                    $total = ($data['cobro'] - $data['descuento'] - $data['alimentacion']) + ($data['ajustes']);
                    ?>
                <tr>
                    <td><?php echo utf8_decode(($data['dealer'])); ?></td>
                    <td><?php echo utf8_decode(($data['cobro'])); ?></td>
                    <td><?php echo utf8_decode(($data['descuento'])); ?></td>
                    <td><?php echo utf8_decode(($data['alimentacion'])); ?></td>
                    <td><?php echo utf8_decode(($data['asistentes'])); ?></td>
                    <td><?php echo utf8_decode(($data['excusas'])); ?></td>
                    <td><?php echo utf8_decode(($data['ajustes'])); ?></td>
                    <td><?php echo utf8_decode(($total)); ?></td>
                </tr>
            <?php } } ?>
        </tbody>
    </table>
<?php } ?>
