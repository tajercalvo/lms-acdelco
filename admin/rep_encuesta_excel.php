<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    $_POST['review_id'] = $_GET['review_id'];
    $_POST['start_date_day'] = $_GET['start_date_day'];
    $_POST['end_date_day'] = $_GET['end_date_day'];
    include('controllers/rep_encuesta.php');
}
//echo utf8_decode('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
header("Content-Type: application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Reporte_Encuesta.xls");

header ("Content-Transfer-Encoding: binary");
if(isset($_GET['start_date_day'])){
    //print_r($datosCursos_all);
    ?>

    <table border="1">
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">COD</th>
                <th data-hide="phone,tablet">ENCUESTA</th>
                <?php if( $type['type'] == 4 && isset( $datosCursos_all[0]['start_date'] ) ){ ?>
                <th data-hide="phone,tablet">FECHA CURSO</th>
                <th data-hide="phone,tablet">INSTRUCTOR</th>
                <?php } ?>
                <th data-hide="phone,tablet"><?php echo utf8_decode('IDENTIFICACIÓN') ?></th>
                <th data-hide="phone,tablet">NOMBRES</th>
                <th data-hide="phone,tablet">APELLIDOS</th>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">SEDE</th>
                <th data-hide="phone,tablet">ZONA</th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet"><?php echo utf8_decode('CÓDIGO') ?></th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">PREGUNTA</th>
                <th data-hide="phone,tablet">RESPUESTA</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php
                    if($cantidad_datos > 0){
                        foreach ($datosCursos_all as $iID => $data) {
                        if(isset($data['respuestas']))
                            foreach ($data['respuestas'] as $iID => $dataRespuestas) { ?>
                        <tr>
                            <td><?php echo utf8_decode($data['code']); ?></td>
                            <td><?php echo utf8_decode($data['review']); ?></td>
                            <?php if( $type['type'] == 4 && isset( $data['start_date'] ) ){ ?>
                            <td><?php echo utf8_decode($data['start_date']); ?></td>
                            <td><?php echo utf8_decode($data['teacher_name']." ".$data['teacher_last']); ?></td>
                            <?php } ?>
                            <td><?php echo utf8_decode($data['identification']); ?></td>
                            <td><?php echo utf8_decode($data['first_name']); ?></td>
                            <td><?php echo utf8_decode($data['last_name']); ?></td>
                            <td><?php echo utf8_decode($data['dealer']); ?></td>
                            <td><?php echo utf8_decode($data['headquarter']); ?></td>
                            <td><?php echo utf8_decode($data['zone']); ?></td>
                            <td><?php echo utf8_decode($data['date_creation']); ?></td>
                            <?php if($data['review_id']!='16'){ ?>
                                <td><?php if(isset($data['code_curso'])){ echo utf8_decode($data['code_curso']); }else{ echo " ";} ?></td>
                                <td><?php if(isset($data['course'])){ echo utf8_decode($data['course']); }else{ echo " ";} ?></td>
                            <?php }else{ ?>
                                <!--<td></td>-->
                                <td></td><?php } ?>
                            <td><?php echo utf8_decode($dataRespuestas['code']); ?> <?php echo utf8_decode($dataRespuestas['question']); ?></td>
                            <td><?php echo utf8_decode($dataRespuestas['answer']); ?></td>
                        </tr>
                    <?php } ?>
                    <?php if(isset($data['respuestasAbiertas']))
                        foreach ($data['respuestasAbiertas'] as $iID => $dataRespuestas) { ?>
                        <tr>
                            <td><?php echo utf8_decode($data['code']); ?></td>
                            <td><?php echo utf8_decode($data['review']); ?></td>
                            <?php if( $type['type'] == 4 && isset( $data['start_date'] ) ){ ?>
                            <td><?php echo utf8_decode($data['start_date']); ?></td>
                            <td><?php echo utf8_decode($data['teacher_name']." ".$data['teacher_last']); ?></td>
                            <?php } ?>
                            <td><?php echo utf8_decode($data['identification']); ?></td>
                            <td><?php echo utf8_decode($data['first_name']); ?></td>
                            <td><?php echo utf8_decode($data['last_name']); ?></td>
                            <td><?php echo utf8_decode($data['dealer']); ?></td>
                            <td><?php echo utf8_decode($data['headquarter']); ?></td>
                            <td><?php echo utf8_decode($data['zone']); ?></td>
                            <td><?php echo utf8_decode($data['date_creation']); ?></td>
                            <?php if($data['review_id']!='16'){ ?>
                                <td><?php if(isset($data['code_curso'])){ echo utf8_decode($data['code_curso']);}else{echo " ";}  ?></td>
                                <td><?php if(isset($data['course'])){ echo utf8_decode($data['course']);}else{echo " ";} ?></td>
                            <?php }else{ ?>
                                <!--<td></td>-->
                                <td></td><?php } ?>
                            <td><?php echo utf8_decode($dataRespuestas['code']); ?> <?php echo utf8_decode($dataRespuestas['question']); ?></td>
                            <td><?php echo utf8_decode($dataRespuestas['result']); ?></td>
                        </tr>
                        <?php } ?>
                <?php } ?><?php } ?>
        </tbody>
    </table>
<?php } ?>
