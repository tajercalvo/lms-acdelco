<?php include('src/seguridad.php'); ?>
<?php include('controllers/foros_ventas.php');
$location = 'distribucion';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
//$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>

	<link rel="stylesheet" href="css/vct_detail2.css" media="screen" title="biblioteca">
	<style>
		#screen-subscriber video{
			width: 100%;
			height: 600px;
			margin: auto;
		}

		a {
		    color: #0058a3;
		    outline: 0 !important;
		}
	</style>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons conversation"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/foros_ventas.php">Comunicación GM</a></li>
					<div class="btn-group pull-right">
						<?php if(isset($_GET['id'])){ ?>

							<?php if($_SESSION['max_rol']>=6){ ?>
							<a class="btn btn-xs btn-success pull-left" style="margin-right: 5px;"  id="finalizar_trasmision" >
								<i class="fa fa-stop"></i>
								Finalizar
							</a>
							<?php } ?>
							<a href="foros_ventas.php" class="btn btn-xs btn-primary pull-right" >
								<i class="fa fa-mail-reply-all fa-fw"></i>
								Regresar
							</a>
							<?php if($_SESSION['idUsuario']==$Foro_datos['creator']): ?>
								<button type="button" class="btn btn-xs btn-inverse pull-right" id="btn_compartir">
									<i class="fa fa-desktop fa-fw"></i>
									Compartir
								</button>
							<?php endif; ?>
						<?php } ?>
					</div>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div>

						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray" style="box-shadow: 1px 1px 10px #888 !important">
						<div class="row">
							<div class="col-md-12 center">
								<h3 class="margin-none content-heading bg-white text-primary" sytle="border:1px solid #efefef !important" ><?php echo($Foro_datos['conference']); ?></h3>
								<?php if(isset($_GET['id'])&&($_GET['id']=="20")){ ?>
										<a href="../assets/gallery/broadcast/Broadcast.pptx" target="_blank">Descargar presentación</a>
								<?php } ?>
							</div>
						</div>
						<div class="row">
							<!-- ======================================================== -->
							<!-- Espacio presentación -->
							<!-- ======================================================== -->
							<div class="col-md-9" style="border-right: 1px solid #efefef">

								<?php if ( $Foro_datos['video']!="" ): ?>
									<video id="vid1" src="../assets/gallery/broadcast/<?php echo($Foro_datos['video']); ?>" controls preload="auto" width="100%" height="auto" style="width: 100%;" autoplay="true">
										<source src="../assets/gallery/broadcast/<?php echo($Foro_datos['video']); ?>" type='video/mp4' data-res='SD 720px'>
									</video>
								<?php else: ?>
									<?php if($_SESSION['idUsuario']==$Foro_datos['creator']){ ?>
										<input type="hidden" id="tipo" value="instructor">
									<?php }else{ ?>
										<input type="hidden" id="tipo" value="alumno">
										<input type="hidden" id="user_id" value="<?= $_SESSION['idUsuario']."_".date("H:m:s") ?>">
									<?php } ?>

									<div id="screen-subscriber"></div>
									<div id="screen-publisher"></div>
									<?php if($_SESSION['idUsuario']==$Foro_datos['creator']){?>
										<input type="hidden" name="teacher_id" id="teacher_id" value="<?= $Foro_datos['creator'] ?>">
									<?php }?>

									<div class="carousel">
										<div class="control-inner" id="imagen_presentacion_act">
											<!-- <img id="imagen_presentacion_act" src="../assets/FilesForos/Chevrolet-logo.png" alt="presentación" class="img-responsive"> -->
										</div>

									</div>

									<!-- <?php if ( count($diaposivitas) > 0 ): ?>



									<?php else: ?>



									<?php endif; ?> -->


								<?php endif; ?>
							</div>
							<!-- ======================================================== -->
							<!-- END Espacio presentación -->
							<!-- ======================================================== -->

							<!-- ======================================================== -->
							<!--  -->
							<!-- ======================================================== -->
							<div class="col-md-3">
								<div class="row" style="border-bottom: 1px solid #efefef;">

									<div class="col-md-12" style="min-height: 200px; height: 200px;">
										<?php if($Foro_datos['video']==""){ ?>


											<?php if ( $_SESSION['idUsuario']==$Foro_datos['creator'] ): ?>
												<div id="camera-publisher" style="z-index: 9999; width: 200px; height: 150px; !important"></div>
											<?php else: ?>
												<div id="camera-subscriber" style="z-index: 9999;"></div>
											<?php endif; ?>

										<?php }else{?>
											<?php if($Foro_datos['image']!=""){ ?>
												<img class="img-responsive center" style="width: 50%;" id="imgModal_Lanzamiento" src="../assets/gallery/modal/<?php echo($Foro_datos['image']); ?>" title="<?php echo($Foro_datos['conference']); ?>" class="img-responsive" />
											<?php }else{ ?>
												<img class="img-responsive center" style="width: 50%;" id="imgModal_Lanzamiento" src="../assets/gallery/modal/default.jpg" title="<?php echo($Foro_datos['conference']); ?>" class="img-responsive" />
													<?php if(isset($_GET['id'])&&($_GET['id']=="20")){ ?>
														<a href="../assets/gallery/broadcast/Broadcast.pptx" target="_blank">Descargar presentación</a>
													<?php } ?>
											<?php } ?>
										<?php }?>
									</div>
								</div>
								<div class="row" style="border-bottom: 1px solid #efefef;">
									<!-- ======================================================== -->
									<!-- Editar comunicación -->
									<!-- ======================================================== -->
									<div class="col-md-12">
										<?php if($_SESSION['max_rol']>=6){ ?>
											<a href="#ModalEditar" data-toggle="modal" class="btn btn-xs btn-inverse pull-right"><i class="fa fa-pencil fa-fw"></i> Editar Hilo</a>
										<?php } ?>
										<b><span style="text-danger" id="Asistentes">[ 0 Asistentes ]:</span></b>
										<b>
											<span style="text-danger">
												<input id="enSala" class="" type="number" name="enSala" value="0" min="0" style="width:60px" title="Ingrese el número de compañeros que esten contigo en esta transmisión">
												<span class="btn btn-danger btn-xs" id="btnEnSala">OK</span>
											</span>
										</b>
										<br>
										<small class="margin-none" style="text-align: justify;"><?php echo($Foro_datos['conference_description']); ?></small><br>
										<div class="separator bottom"></div>
									</div>
									<!-- ======================================================== -->
									<!-- END Editar comunicación -->
									<!-- ======================================================== -->
								</div>
								<div class="row" style="border-bottom: 1px solid #efefef;">
									<!-- ======================================================== -->
									<!-- Tansmision streaming - TokBox -->
									<!-- ======================================================== -->
									<div class="col-md-12" id="ElDeBajar" style="max-height: 200px; height: 200px; overflow-y: scroll;">
										<div class="innerAll spacing-x2">
										<?php if($Foro_datos['video']==""){ ?>
												<input type="hidden" id="sessionId" name="sessionId" value="<?php echo($Foro_datos['session_id']); ?>">
												<input type="hidden" id="token" name="token" value="<?php echo($Foro_datos['token']); ?>">
										<?php } ?>
											<!-- row -->
											<div class="row" id="MensajesForo">
												<?php
												$MaxIdReply = 0;
												foreach ($Foros_Reply as $key => $Data_Reply) {
													$MaxIdReply = $Data_Reply['conference_reply_id'];
													?>
													<!-- Post List -->
													<div class="media border-bottom margin-none">
														<div class="media-body">
															<small class="label label-default" title="<?php echo($Data_Reply['date_creation']); ?> - <?php echo($Data_Reply['first_name'].' '.$Data_Reply['last_name']); ?>"><?php echo($Data_Reply['first_name']); ?>:</small> <span><?php echo($Data_Reply['conference_reply']); ?></span><br>
														</div>
													</div>
													<!-- // END Información General Listing -->
												<?php } ?>
												<input type="hidden" class="MaxIdReply" name="MaxIdReply" id="MaxIdReply" value="<?php echo($MaxIdReply);?>">
											</div>
										</div>
									</div>
									<!-- ======================================================== -->
									<!-- END Tansmision streaming - TokBox -->
									<!-- ======================================================== -->

									<!-- ======================================================== -->
									<!-- Formulario chat -->
									<!-- ======================================================== -->
									<div class="col-md-12">
										<form id="FormularioComentarios">
											<div class="innerB">
												<input type="hidden" id="opcn" name="opcn" value="enviar">
												<input type="hidden" id="conference_id" value="<?php echo $_GET['id']; ?>" name="conference_id">
												<textarea id="comentario" rows="3" name="comentario" placeholder="Escriba aquí su comentario..." class="col-md-10 form-control margin-bottom notebook padding-none" rows="5"></textarea>
											</div>
											<div class="separator bottom"></div>
											<div class="btn-group pull-right">
												<button type="button" id="botonChat" class="btn btn-primary">Enviar</button>
											</div>
										<?php if(isset($_GET['id'])&&($_GET['id']=="20")){ ?>
											<div class="separator bottom"></div>
											<div>
												<a href="../assets/gallery/broadcast/Broadcast.pptx" target="_blank">Descargar presentación</a>
											</div>
										<?php } ?>
										</form>
									</div>
									<!-- ======================================================== -->
									<!-- END Formulario chat -->
									<!-- ======================================================== -->
								</div>

							</div>
							<!-- ======================================================== -->
							<!--  -->
							<!-- ======================================================== -->
						</div>
					</div>
					<!-- ======================================================== -->
					<!-- Tira de imagenes -->
					<!-- ======================================================== -->
					<div class="row row-app">
						<div class="row" id="row_lista_diapositivas" style="visibility: visible;">
							<!-- <div class="row" id="row_lista_diapositivas" style="visibility: visible;"> -->
							<div class="col-md-12" style="height: 200px">
								<div class="scrollmenu" id="lista_diapositivas">
									<!-- COntenido Modificable por Javascript -->
								</div>
							</div>
						</div>
					</div>
					<!-- ======================================================== -->
					<!-- END Tira de imagenes -->
					<!-- ======================================================== -->
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->

			<?php if(isset($_GET['id'])){ ?>
				<!-- Modal -->
				<!-- ================================================ -->
				<!-- Modal Edicion de hilo -->
				<!-- ================================================ -->
				<form enctype="multipart/form-data" id="form_Editar"><br><br>
					<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Editar el Hilo del foro</h4>
								</div>
								<div class="modal-body">
									<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Titulo: </label>
										<input type="text" id="conference" name="conference" class="form-control col-md-8" placeholder="Pregunta para el foro" value="<?php echo $Foro_datos['conference']; ?>" />
									</div>
									<div class="col-md-1">
										<input type="hidden" id="conference_id" value="<?php echo $_GET['id']; ?>" name="conference_id">
										<input type="hidden" id="opcn" value="editar" name="opcn">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Descripción:</label>
										<textarea id="conference_description" name="conference_description" class="col-md-12 form-control" rows="5"><?php echo $Foro_datos['conference_description']; ?></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1"></div>
									<div class="col-md-10">
										<label class="control-label">Categoría: </label>
										<select style="width: 100%;" id="category" name="category" >
											<option value="Asesor Estrat&eacute;gico Chevrolet" <?php if($Foro_datos['category']=="Asesor Estrat&eacute;gico Chevrolet"){ ?>selected="selected"<?php } ?> >Asesor Estratégico Chevrolet</option>
											<option value="General" <?php if($Foro_datos['category']=="General"){ ?>selected="selected"<?php } ?> >General</option>
											<option value="Habilidades Blandas" <?php if($Foro_datos['category']=="Habilidades Blandas"){ ?>selected="selected"<?php } ?> >Habilidades Blandas</option>
											<option value="Habilidades T&eacute;cnicas" <?php if($Foro_datos['category']=="Habilidades T&eacute;cnicas"){ ?>selected="selected"<?php } ?> >Habilidades Técnicas</option>
										</select>
									</div>
									<div class="col-md-1"></div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Posición: </label>
										<input type="text" id="position" name="position" class="form-control col-md-8" placeholder="Posición del hilo en el foro" value="<?php echo $Foro_datos['position']; ?>" />
									</div>
									<div class="col-md-1">
									</div>
									<div class="col-md-4">
										<label class="control-label">Estado: </label>
										<select style="width: 100%;" id="estado" name="estado">
											<option value="1" <?php if($Foro_datos['status_id']==1){ ?>selected<?php } ?>>Activo</option>
											<option value="2" <?php if($Foro_datos['status_id']==2){ ?>selected<?php } ?>>Inactivo</option>
										</select>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Cargos </h5>
									<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]">
										<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
														$selected = "";
														foreach ($Foro_datos['charges'][0] as $key => $Data_ChargeUser) {
															if($Data_ActiveCharge['charge_id'] == $Data_ChargeUser['charge_id']){
																$selected = "selected";
															}
														}
											?>
											<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php echo $selected; ?> ><?php echo $Data_ActiveCharge['charge']; ?></option>
										<?php } ?>
						        	</select>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
							</div>
						</div>
								</div>
								<div class="modal-footer">
									<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- ================================================ -->
				<!-- END Modal Edicion de hilo -->
				<!-- ================================================ -->
				<!-- /.modal -->

			<?php } ?>

		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="//static.opentok.com/v2/js/opentok.js"></script>
	<script src="js/view_foros.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
