<?php include('src/seguridad.php'); ?>
<?php 
//$source = "vid";
include('controllers/argumentarios.php');
$location = 'tutor';
//$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>

</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons tie"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/argumentarios.php">Asesor Estratégico Chevrolet</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Asesor Estratégico Chevrolet
						</h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<div class="innerL row row-merge">
						<div class="col-md-3 center innerL innerTB">
							<div class="innerR innerT">
								<!-- Profile Photo -->
								<div class="thumb">
									<img src="../assets/gallery/asesor_estrategico.png" alt="Profile" class="img-responsive" />
								</div>
								<div class="separator bottom"></div>
							</div>
						</div>
						<div class="col-md-9 containerBg innerTB">
							<div class="innerLR">
								<div class="row innerTB">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<!-- About -->
												<div class="widget widget-heading-simple widget-body-white margin-none">
													<div class="widget-head"><h4 class="heading glyphicons star" style="color:#0058a3 !important"><i></i>¿Qué es el Asesor Estratégico Chevrolet?
</h4></div>
													<div class="widget-body">
														<p style="text-align: justify;">Es la nueva sección de la plataforma gmacademy.co, creada y diseñada exclusivamente  para el departamento de ventas y su equipo de asesores comerciales, con el fin de ser una herramienta de apoyo actualizada, útil y amigable que permita impulsar sus objetivos comerciales.<br><br>Esta herramienta es administrada por el Asesor Estratégico Chevrolet.</p>
													</div>
												</div>
												<!-- // About END -->
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<!-- About -->
												<div class="widget widget-heading-simple widget-body-white margin-none">
													<div class="widget-head"><h4 class="heading glyphicons tie" style="color:#0058a3 !important"><i></i>¿Quién es el Asesor Estratégico Chevrolet?</h4></div>
													<div class="widget-body">
														<p style="text-align: justify;">Leydi Rojas La Asesora Estratégica Chevrolet” es la persona que GM Colmotores pone a disposición de la Red de Concesionarios con el fin de apoyar su labor comercial,  solucionando inquietudes en cuanto a el portafolio Chevrolet y las estrategias de la marca  así como también brindando toda la información necesaria para desempeñar las funciones como Asesor Comercial.</p>
														<!--<object type="application/x-shockwave-flash" data="https://cdn.livestream.com/swf/LSPlayer.swf" width="100%" height="100%" id="live_player_113566049" style="visibility: visible;"><param name="wmode" value="opaque"><param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always"><param name="bgcolor" value="#000000"><param name="flashvars" value="account=17824281&amp;event=4879503&amp;autoPlay=true&amp;mute=false&amp;play_url=https://player-api.new.livestream.com/accounts/17824281/events/4879503/broadcasts/113566049.secure.smil&amp;generatedAt=undefined&amp;delayTuneIn=null&amp;thumbnail=https://img.new.livestream.com/events/00000000004a748f/243cf451-d2f4-49f2-9871-e60b585717c0_60262.jpg&amp;qualities_bitrate=289000&amp;qualities_height=360&amp;isVOD=false&amp;showLike=true&amp;isLiked=false&amp;showShare=true&amp;isWhiteLabel=false&amp;isStandAlone=false&amp;ad_account_id=null&amp;ad_provider_id=null&amp;ad_custom_params=null&amp;ad_enabled_for_vod=null&amp;ad_enabled_for_live=null&amp;ad_types=null&amp;isAdvancedAnyalitcsEnabled=null&amp;eventFullName=lanzamiento prueba con Felipe Heredia&amp;token=null&amp;callback=Application.flashPlayerCallbacks._113566049&amp;hide_external_links=false&amp;media=undefined&amp;viewerPlusId=undefined&amp;ownerToken=b0117824281............................4bd802ced0b90c02e9c75e6284c87a32........................................1456505830684...........1456678630684...........vyfU1sM0Nsj_WwTPl5J88A~~00000000000000...................................."></object>-->
													</div>
												</div>
												<!-- // About END -->
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12">
												<!-- About -->
												<div class="widget widget-heading-simple widget-body-white margin-none">
													<div class="widget-head"><h4 class="heading glyphicons gift" style="color:#0058a3 !important"><i></i>¿Qué beneficios ofrece el Asesor Estratégico Chevrolet?</h4></div>
													<div class="widget-body">
														<p style="text-align: justify;">Brindar información actualizada de nuestra marca, nuestros carros, el sector y la competencia. Así mismo, ser el canal directo de comunicación entre la fuerza de ventas y el Área Comercial de GM Colmotores.</p>
													</div>
												</div>
												<!-- // About END -->
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<!-- About -->
												<div class="widget widget-heading-simple widget-body-white margin-none">
													<div class="widget-head"><h4 class="heading glyphicons notes" style="color:#0058a3 !important"><i></i>¿Qué información brinda el Asesor Estratégico Chevrolet?
</h4></div>
													<div class="widget-body">
														<p style="text-align: justify;">En la sección del Asesor Estratégico Chevrolet podrás encontrar información como:<br><br>
																Consejos prácticos para el proceso de ventas. <br>
																Información actualizada de nuestro Portafolio y la competencia.<br>
																Noticias de Chevrolet Colombia y del sector automotor.<br>
																Novedades de Producto y Lanzamientos.<br>
																Interactividad en vivo y en directo para dudas puntuales de urgente solución.<br>
																Información actualizada de promociones de marca, campañas publicitarias y estrategias de mercadeo.</p>
													</div>
												</div>
												<!-- // About END -->
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<!-- About -->
												<div class="widget widget-heading-simple widget-body-white margin-none">
													<div class="widget-head"><h4 class="heading glyphicons conversation" style="color:#0058a3 !important"><i></i>¿Cómo contactar a Leydi Rojas, su Asesora Estratégica Chevrolet?
</h4></div>
													<div class="widget-body">
														<p style="text-align: justify;">
															Chat en vivo por medio de la plataforma gmacademy.co, de lunes a viernes de 8:00 am a 5:00 PM. Búsca a Leydi Rojas en <a href="mensajes.php" >Chat GM Academy</a><br><br>
															Vía email al correo <strong>asesorestrategico@gmacademy.co</strong>
														</p>
													</div>
												</div>
												<!-- // About END -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<!--<script src="js/asesor_estrategico.js"></script>-->
</body>
</html>