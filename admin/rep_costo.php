<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_costo.php');
$location = 'reporting';
$locData = true;
$qtip = 'qtip';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons calculator"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_costo.php">Costos de Capacitación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Resumen de Costos Vs Tarifas de Impartición de Capacitación </h2>
							<br><br><!--<div class="clearfix"> &nbsp;[<?php echo $fecha_inicial.' - '.$fecha_final; ?>] <?php echo $diferencia_mes; ?> Meses </div>-->
							<button type="button" data-toggle="print" class="btn btn-default print hidden-print"><i class="fa fa-fw fa-print"></i> Imprimir</button>
						</div>
					<!-- // END heading -->
					<!-- contenido filtros -->
						<div class="widget widget-heading-simple widget-body-gray hidden-print">
							<div class="widget-body">
								<div class="row">
									<div class="row">
										<form action="rep_costo.php" method="post">
											<div class="col-md-9">
												<!-- Group -->
												<div class="form-group">
													<div class="col-md-2">
														<label class="control-label" for="ano">Año:</label>
														<input type="hidden" name="opcn" id="opcn" value="proc"/>
														<select style="width: 100%;" id="ano" name="ano" >
															<?php for ($y=date("Y");$y>2017;$y--){ ?>
																<option value="<?php echo $y; ?>" <?php if($y==$ano){ ?>selected="selected"<?php } ?>><?php echo $y; ?></option>
															<?php } ?>
														</select>
													</div>
													<div class="col-md-2">
														<label class="control-label" for="mes">Mes:</label>
														<select style="width: 100%;" id="mes" name="mes" >
														    <option value="01" <?php if(1==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Enero</option>
															<option value="02" <?php if(2==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Febrero</option>
															<option value="03" <?php if(3==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Marzo</option>
															<option value="04" <?php if(4==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Abril</option>
															<option value="05" <?php if(5==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Mayo</option>
															<option value="06" <?php if(6==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Junio</option>
															<option value="07" <?php if(7==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Julio</option>
															<option value="08" <?php if(8==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Agosto</option>
															<option value="09" <?php if(9==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Septiembre</option>
															<option value="10" <?php if(10== ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Octubre</option>
															<option value="11" <?php if(11==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Noviembre</option>
															<option value="12" <?php if(12==ltrim($mes,"0")){ ?>selected="selected"<?php } ?>>Diciembre</option>
														</select>
													</div>
													<div class="col-md-8">
														A continuación se presentan los datos de cálculo tomando como base la asistencia a los cursos dictados de manera presencial y virtual, se debe tener en cuenta que los movimientos que se realicen sobre las programaciones de forma anterior a la generación de este informe, podrán cambiar en la misma ruta de los cambios que se le apliquen a las programaciones.
													</div>
												</div>
												<!-- // Group END -->
											</div>

											<div class="col-md-3 text-center">
												<?php if($cantidad_datos > 0){ ?>
												<div class="row" style="margin-bottom:10px;">
														<h5><a href="rep_costo_excel.php?year=<?php echo($_POST['ano']); ?>&month=<?php echo($_POST['mes']); ?>&type=1" class="glyphicons no-js download_alt" style="padding-left: 28px;"><i></i>Descargar Reporte Detallado</a><h5>
												</div>
												<div class="row" style="margin-bottom:10px;">
														<h5><a href="rep_costo_excel_s.php?year=<?php echo($_POST['ano']); ?>&month=<?php echo($_POST['mes']); ?>&type=2" class="glyphicons no-js download_alt" style="padding-left: 28px;"><i></i>Descargar Reporte Simplificado</a><h5>
												</div>
												<?php } ?>
												<div class="row">
													<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<!-- Nuevo ROW-->
							<div class="row row-app">
							</div>
							<!-- // END Nuevo ROW-->
							<div class="separator bottom"></div>
							<div class="separator bottom"></div>
						</div>
					<!-- // END contenido filtros -->
					<!-- PRIMER INDICADOR COMPLETO -->
					<?php if(isset($RegistrosConcesionarios) AND (count($RegistrosConcesionarios)>0)){
						foreach ($RegistrosConcesionarios as $key => $datos_Cobros) {
						?>
						<div class="well">
							<table class="table table-invoice">
								<tbody>
									<tr>
										<td style="width: 25%;">
											<p class="lead">Tarifas y Cobros</p>
												<h4>A tener en cuenta:</h4>
												<address class="margin-none" style="text-align: justify;">
													<strong>Periodo: </strong><?php echo($mes.'/'.$ano); ?><br/><br/>
													<strong>Concesionario: <span class="label label-success"><?php echo($datos_Cobros['dealer']); ?></span></strong><br/><br/>
													En la tabla siguiente se presenta la información de las sesiones de formación en que su concesionario participó, luego la información de descuentos que le aplican y el resmuen del periodo de tiempo.<br/><br/>
													A continuación un resumen de la sesión:<br/>
													<strong><span class="label label-success">$ <? echo(number_format($datos_Cobros['cobro'],0));?></span> Cobros</strong><br/>
													<strong><span class="label label-success">$ <? echo(number_format($datos_Cobros['descuento']));?></span> Plan Sede</strong><br/>
													<strong><span class="label label-success">$ <? echo(number_format($datos_Cobros['alimentacion']));?></span> Alimentación</strong><br/>
													<strong><span class="label label-success"><? echo ($datos_Cobros['excusas']); ?></span> # de Excusas</strong><br/>
													<strong><span class="label label-success">$ <? echo(number_format($datos_Cobros['ajustes']));?></span> Ajuste a Mes</strong><br/>
													<strong><span class="label label-primary">$ <? echo(number_format( ($datos_Cobros['cobro']-$datos_Cobros['descuento']-$datos_Cobros['alimentacion'])+($datos_Cobros['ajustes']),0));?></span> Total</strong><br/>
												</address>
												<p></p>
												<h5><a href="rep_costo_factura.php?factura=<?php echo($datos_Cobros['dealer_id']); ?>&year=<?php echo($_POST['ano']); ?>&month=<?php echo($_POST['mes']); ?>" class="glyphicons no-js download_alt" style="padding-left: 28px;"><i>Descargar Factura</i></a> </h5>
										</td>
										<td class="right">
											<p class="lead">Cobros</p>
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center"><span class="label label-inverse">SESION</span></th>
														<th class="center"><span class="label label-inverse">CURSO</span></th>
														<th class="center"><span class="label label-inverse">TIEMPO</span></th>
														<th class="center"><span class="label label-inverse">METODOLOGÍA</span></th>
														<th class="center"><span class="label label-inverse">LUGAR</span></th>
														<th class="center"><span class="label label-inverse">PLAN SEDE</span></th>
														<th class="center"><span class="label label-inverse">VALOR CUPO</span></th>
														<th class="center"><span class="label label-inverse">PARTICIPANTES</span></th>
														<th class="center"><span class="label label-inverse">COSTO</span></th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<?php if(isset($datos_Cobros['Detalle_Sesiones'])){ foreach ($datos_Cobros['Detalle_Sesiones'] as $key => $Sesiones_Det) { ?>
													  <tr class="selectable">
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['start_date']); ?><br><?php echo($Sesiones_Det['end_date']); ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['course']); ?> (<?php echo($Sesiones_Det['newcode']); ?>)</td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['duration_time']); ?> HORAS</td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['type']); ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['living']); ?></td>
														<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['plan_sede']); ?></td><!-- PLAN SEDE -->
													    <!-- <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['inscriptions']); ?></td> -->
													    <td class="center" style="padding: 2px; font-size: 80%;">$<?php echo number_format( ($Sesiones_Det['valor_cupo']) ); ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;">
													    	<?php if ($Sesiones_Det['excusas'] > 0) { ?>
													    	<span class="label label-primary" style="font-size: 90%;"><b><?php echo ($Sesiones_Det['dealer_inscriptions']); ?></b></span>
													    	<?php } else { echo($Sesiones_Det['dealer_inscriptions']); } ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;">$<?php echo number_format( ($Sesiones_Det['rate_course']) ); ?></td><!-- COSTo -->
													  </tr>
													<?php } } ?>
													<tr class="selectable">
													    <td class="center" style="padding: 2px; font-size: 80%;" colspan="4"></td>
													    <td class="left" style="padding: 2px; font-size: 100%;" colspan="3"><span class="label label-success"><b>TOTAL</b></span></td>
													    <td class="center" style="padding: 2px; font-size: 100%;"><span class="label label-success"><b><?php echo ($datos_Cobros['asistentes']); ?></b></span></td>
													    <td class="center" style="padding: 2px; font-size: 100%;"><span class="label label-success"><b>$<?php echo number_format( ($datos_Cobros['cobro']) ); ?></b></span></td>
												  </tr>
												</tbody>
											</table>
											<p class="lead">Descuentos Plan Sede</p>
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center"><span class="label label-inverse">SESION</span></th>
														<th class="center"><span class="label label-inverse">CURSO</span></th>
														<th class="center"><span class="label label-inverse">TIEMPO</span></th>
														<th class="center"><span class="label label-inverse">METODOLOGÍA</span></th>
														<th class="center"><span class="label label-inverse">LUGAR</span></th>
														<th class="center"><span class="label label-inverse">DESCUENTO</span></th>
														<th class="center"><span class="label label-inverse">COSTO</span></th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla2">
													<?php if(isset($datos_Cobros['Detalle_Sesiones'])){ foreach ($datos_Cobros['Detalle_Sesiones'] as $key => $Sesiones_Det) {
															if ( $Sesiones_Det['descuento'] != 0 ) {  ?>
													  <tr class="selectable">
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['start_date']); ?><br><?php echo($Sesiones_Det['end_date']); ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['course']); ?> (<?php echo($Sesiones_Det['newcode']); ?>)</td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['duration_time']); ?> HORAS</td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['type']); ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['living']); ?></td>
														<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary">Por Asistencia</span></td>
													    <td class="center" style="padding: 2px; font-size: 80%;">$<?php echo number_format( ($Sesiones_Det['descuento']) ); ?></td><!-- COSTo -->
													  </tr>
													<?php } } } ?>
												  <!-- <tr class="selectable">
												    <td class="center" style="padding: 2px; font-size: 80%;">2018-01-15 08:00:00 - 2018-01-15 17:00:00</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">FORMACIÓN COMERCIAL PREMIUM (IBT00953)</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">8 HORAS</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">IBT</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">CODIESEL GIRON (X Acuerdo)</td>
														<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary">Por Asistencia</span></td>
												    <td class="center" style="padding: 2px; font-size: 80%;">$170.000</td>
												  </tr> -->
												  <tr class="selectable">
												    <td class="center" style="padding: 2px; font-size: 80%;" colspan="3"></td>
												    <td class="left" style="padding: 2px; font-size: 100%;" colspan="3"><span class="label label-success"><b>TOTAL</b></span></td>
												    <td class="center" style="padding: 2px; font-size: 100%;"><span class="label label-success"><b>$<?php echo number_format( ($datos_Cobros['descuento']) ); ?></b></span></td>
												  </tr>
												</tbody>
											</table>
											<p class="lead">Descuentos por Alimentación</p>
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center"><span class="label label-inverse">SESION</span></th>
														<th class="center"><span class="label label-inverse">CURSO</span></th>
														<th class="center"><span class="label label-inverse">TIEMPO</span></th>
														<th class="center"><span class="label label-inverse">METODOLOGÍA</span></th>
														<th class="center"><span class="label label-inverse">LUGAR</span></th>
														<th class="center"><span class="label label-inverse">DESCUENTO</span></th>
														<th class="center"><span class="label label-inverse">COSTO</span></th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla3">
													<?php if(isset($datos_Cobros['Detalle_Sesiones'])){ foreach ($datos_Cobros['Detalle_Sesiones'] as $key => $Sesiones_Det) {
															if ( $Sesiones_Det['alimentacion'] != 0 ) {  ?>
													  <tr class="selectable">
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['start_date']); ?><br><?php echo($Sesiones_Det['end_date']); ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['course']); ?> (<?php echo($Sesiones_Det['newcode']); ?>)</td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['duration_time']); ?> HORAS</td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['type']); ?></td>
													    <td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Sesiones_Det['living']); ?></td>
														<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-warning">Alimentación</span></td>
													    <td class="center" style="padding: 2px; font-size: 80%;">$<?php echo number_format( ($Sesiones_Det['alimentacion']) ); ?></td><!-- COSTo -->
													  </tr>
													<?php } } } ?>
												  <!-- <tr class="selectable">
												    <td class="center" style="padding: 2px; font-size: 80%;">2018-01-17 08:00:00 - 2018-01-17 17:00:00</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">FORMACIÓN COMERCIAL PREMIUM (IBT00953)</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">8 HORAS</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">IBT</td>
												    <td class="center" style="padding: 2px; font-size: 80%;">CODIESEL BOCONO Anillo via (X Acuerdo)</td>
														<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-warning">Alimentación</span></td>
												    <td class="center" style="padding: 2px; font-size: 80%;">$384.000</td>
												  </tr> -->
													<tr class="selectable">
													    <td class="center" style="padding: 2px; font-size: 80%;" colspan="3"></td>
													    <td class="left" style="padding: 2px; font-size: 100%;" colspan="3"><span class="label label-success"><b>TOTAL</b></span></td>
													    <td class="center" style="padding: 2px; font-size: 100%;"><span class="label label-success"><b>$<?php echo number_format( ($datos_Cobros['alimentacion']) ); ?></b></span></td>
												  </tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } } ?>
					<!-- PRIMER INDICADOR COMPLETO-->
					<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div>
			<!-- // END Contenido proyectos -->
		</div>
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_costo.js"></script>
</body>
</html>
