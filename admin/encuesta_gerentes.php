<?php
@session_start();
if(!isset($_SESSION['evl_obligatoria']) && !isset($_GET['result']) ){
	header("location: index.php");
}else{
	$review_enc_id = $_SESSION['evl_obligatoria'];
}
include('src/seguridad.php'); ?>
<?php include('controllers/encuesta_gerentes.php');
$location = 'home';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons circle_question_mark"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/evaluar.php">Encuesta de Satisfacción</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->

<div class="innerLR spacing-x2">
	<!-- row -->
	<div class="row">
		<!-- col -->
		<div class="col-md-10 col-md-offset-1">
			<div class="widget widget-body-white">
				<div class="border-bottom innerAll">
					<div class="media margin-none innerAll">
						<div class="pull-left">
							<img src="../assets/images/distinciones/default.png" style="width:100px;" class="img-responsive">
						</div>
						<div class="media-body">
							<div class="innerAll half media margin-none">
								<div class="pull-left innerLR half">
									<p class="strong center">Desarrollemos a la Medida tu Equipo de Trabajo: 2017</p>
									<p class="margin-none" style="text-align:justify;">
									<b><?php echo($_SESSION['NameUsuario']); ?></b>, GMAcademy sabe que tu eres quien mejor conoce las necesidades de formación y desarrollo 
									de <b><?php echo($_SESSION['dealer']); ?></b>. Por este motivo hemos diseñado el presente formulario que nos permitirá generar en 2017 un alto impacto en los resultados del negocio a partir de nuestra planeación y sinergia de trabajo. <br><br>
									Diligenciar este formulario te tomará un corto periodo de tiempo, agradecemos completarlo antes del 25 de enero.
									</p><br/>
									<?php if(!isset($_GET['result'])){ ?>
									<form action="encuesta_gerentes.php" method="post" id="FormularioParte1">
										<p class="margin-none">
											<b>1. Selecciona cual(es) de los siguientes cargos están bajo tu liderazgo y da clic en Siguiente (No se permite regresar):</b><br><br>
											<div class="body">
												<div class="row">
													<div class="col-md-2 center">
													</div>
													<div class="col-md-4 left">
														<input type="checkbox" name="AVN" id="AVN" <?php if(isset($_POST['AVN'])){?>checked="true"<?php }?> <?php if(isset($_POST['Retorno_val'])){?>disabled="true"<?php }?>> Asesor de Vehículos Nuevos y Selective Channel<br>
														<input type="checkbox" name="ACC" id="ACC" <?php if(isset($_POST['ACC'])){?>checked="true"<?php }?> <?php if(isset($_POST['Retorno_val'])){?>disabled="true"<?php }?>> Agente Call Center<br>
														<input type="checkbox" name="CID" id="CID" <?php if(isset($_POST['CID'])){?>checked="true"<?php }?> <?php if(isset($_POST['Retorno_val'])){?>disabled="true"<?php }?>> Consultor Integral Diésel (CID)
													</div>
													<div class="col-md-4 left">
														<input type="checkbox" name="ARA" id="ARA" <?php if(isset($_POST['ARA'])){?>checked="true"<?php }?> <?php if(isset($_POST['Retorno_val'])){?>disabled="true"<?php }?>> Asesor de Repuestos y accesorios<br>
														<input type="checkbox" name="TIG" id="TIG" <?php if(isset($_POST['TIG'])){?>checked="true"<?php }?> <?php if(isset($_POST['Retorno_val'])){?>disabled="true"<?php }?>> Técnico Integral Gasolina (TIG)<br>
														<input type="checkbox" name="TID" id="TID" <?php if(isset($_POST['TID'])){?>checked="true"<?php }?> <?php if(isset($_POST['Retorno_val'])){?>disabled="true"<?php }?>> Técnico Integral Diésel (TID)
													</div>
													<div class="col-md-2 center">
															<input type="hidden" name="Retorno_val" id="Retorno_val" value="1">
													</div>
												</div>
											</div>
										</p>
											<?php if(!isset($_POST['Retorno_val'])){?>
												<button type="submit" class="pull-right btn btn-success" id="crearElemento"><i class="fa fa-arrow-circle-right"></i> Siguiente</button>
											<?php }?>
									</form>
									<?php }else{?>
										<span><b>¡Gracias por diligenciar la encuesta, a mediados del mes de febrero les estaremos compartiendo el contenido académico a desarrollar durante el 2017!</b></span>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php if(isset($_POST['Retorno_val']) && $RegistrarEncuesta>0 ){?>
			<form action="encuesta_gerentes.php" method="post" id="FormularioParte2">
				<div class="col-md-12 center">
					<!-- Tabs -->
					<div class="relativeWrap" id="PanelPreguntas">
						<div class="widget widget-tabs widget-tabs-double widget-tabs-vertical row row-merge widget-tabs-gray">
						
							<!-- Tabs Heading -->
							<?php
								$_Class1 = '';
								$_Class2 = '';
								$_Class3 = '';
								$_Class4 = '';
								$_Class5 = '';
								$_Class6 = '';
								if(isset($_POST['AVN'])){
									$_Class1 = 'active';
								}else if(isset($_POST['ACC'])){
									$_Class2 = 'active';
								}elseif(isset($_POST['CID'])){
									$_Class3 = 'active';
								}elseif(isset($_POST['ARA'])){
									$_Class4 = 'active';
								}elseif(isset($_POST['TIG'])){
									$_Class5 = 'active';
								}elseif(isset($_POST['TID'])){
									$_Class6 = 'active';
								}
							?>
							<div class="widget-head col-md-3">
								<ul>
									<?php if(isset($_POST['AVN'])){?><li class="<?php echo($_Class1);?>"><a href="#tab1-1" class="glyphicons user" data-toggle="tab" id="Tab_SEL1"><i></i><span class="strong">Asesor V. Nuevos y Selective Channel</span><span class="text-info">Clic aquí para completar este cargo:</span></a></li><?php }?>
									<?php if(isset($_POST['ACC'])){?><li class="<?php echo($_Class2);?>"><a href="#tab2-1" class="glyphicons user" data-toggle="tab" id="Tab_SEL2"><i></i><span class="strong">Agente Call Center</span><span class="text-info">Clic aquí para completar este cargo:</span></a></li><?php }?>
									<?php if(isset($_POST['CID'])){?><li class="<?php echo($_Class3);?>"><a href="#tab3-1" class="glyphicons user" data-toggle="tab" id="Tab_SEL3"><i></i><span class="strong">Consultor Integral Diésel (CID)</span><span class="text-info">Clic aquí para completar este cargo:</span></a></li><?php }?>
									<?php if(isset($_POST['ARA'])){?><li class="<?php echo($_Class4);?>"><a href="#tab4-1" class="glyphicons user" data-toggle="tab" id="Tab_SEL4"><i></i><span class="strong">Asesor de Repuestos y accesorios</span><span class="text-info">Clic aquí para completar este cargo:</span></a></li><?php }?>
									<?php if(isset($_POST['TIG'])){?><li class="<?php echo($_Class5);?>"><a href="#tab5-1" class="glyphicons user" data-toggle="tab" id="Tab_SEL5"><i></i><span class="strong">Técnico Integral Gasolina (TIG)</span><span class="text-info">Clic aquí para completar este cargo:</span></a></li><?php }?>
									<?php if(isset($_POST['TID'])){?><li class="<?php echo($_Class6);?>"><a href="#tab6-1" class="glyphicons user" data-toggle="tab" id="Tab_SEL6"><i></i><span class="strong">Técnico Integral Diésel (TID)</span><span class="text-info">Clic aquí para completar este cargo:</span></a></li><?php }?>
								</ul>
							</div>
							<!-- // Tabs Heading END -->
							
							<div class="widget-body col-md-9">
								<div class="tab-content">
								
									<!-- Tab content -->
									<div class="tab-pane <?php echo($_Class1);?>" id="tab1-1">
										<h4>Asesor de Vehículos Nuevos y Selective Channel</h4><br><br>
										<p><b>2. A continuación te presentamos <span class="text-danger">6</span> opciones de cursos de habilidades blandas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que serán el foco del entrenamiento del 2017.</b></p>
										<p>
											<input type="hidden" name="opcn_AVN" id="opcn_AVN" value="AVN">
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Vendale a la Mente para llegar a su Cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Edificar herramientas efectivas que influyan directamente en el proceso de relación con la empresa, para establecer el servicio como motor e hilo conductor en la fidelización del cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="AVN1" name="AVN1" value="1"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Comportamiento del Consumidor</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">El participante podra descubrir el poder que tiene su comportamiento para alimentar el de las personas con quienes se encuentra a diario. </td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="AVN2" name="AVN2" value="2"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Prospección de Venta por Canal<br>(Playas de ventas - vitrinas - btl por tipo de cliente)</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">El participante recibirá herramientas para focalizar sus esfuerzos comerciales de forma eficaz con base en el entorno y cliente al que se dirige.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="AVN3" name="AVN3" value="3"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Venta Persuasiva<br>Como abordar clientes al estilo retail</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Proporcionarle al asesor comercial herramietas que le permiten influir en las intenciones, motivaciones y comportamientos de compra de su cliente. </td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="AVN4" name="AVN4" value="4"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Técnicas de Cierre de Ventas</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Proporcionarle al participante tecnicas cientifcas basadas en el comportamiento del consumidor para impulsar la toma de decisiones.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="AVN5" name="AVN5" value="5"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">6</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Productividad y Felicidad en mi Trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Sensibilizar al participante sobre la importancia de dar lo mejor de sí en el puesto de trabajo, haciendo énfasis en cómo la alta productividad laboral y la felicidad van de la mano.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="AVN6" name="AVN6" value="6"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b><span class="text-danger">Nota:</span> Adicionalmente, el personal debe realizar 3 cursos obligatorios de refuerzo en producto; que serán informados durante el transcurso del año.</b></p>
									</div>
									<!-- // Tab content END -->
									
									<!-- Tab content -->
									<div class="tab-pane <?php echo($_Class2);?>" id="tab2-1">
										<h4>Agente Call Center</h4><br><br>
										<p><b>2. A continuación te presentamos <span class="text-danger">5</span> opciones de cursos de habilidades blandas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que serán el foco del entrenamiento del 2017.</b></p>
										<p>
										<input type="hidden" name="opcn_ACC" id="opcn_ACC" value="ACC">
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Habilidades Comunicación</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Identificar los factores de éxito de la comunicación en cada situación de desempeño para utilizar como herramienta en el puesto de trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ACC1" name="ACC1" value="1"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Cultura de servicio ventas</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Identificar y reconocer las habilidades y comportamientos que promueven el éxito y el cumplimiento de la promesa al cliente y el manejo de satisfacción asociadas a la oferta de atención</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ACC2" name="ACC2" value="2"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Vendale a la Mente para llegar a su Cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Edificar herramientas efectivas y eficaces que influyan directamente en el proceso de relación con la empresa, para establecer el servicio como motor e hilo conductor en la fidelización del cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ACC3" name="ACC3" value="3"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Comportamiento del Consumidor</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">El participante podra descubrir el poder que tiene su comportamiento para alimentar el de las personas con quienes se encuentra a diario.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ACC4" name="ACC4" value="4"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Productividad y Felicidad en mi Trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Sensibilizar al participante sobre la importancia de dar lo mejor de sí en el puesto de trabajo, haciendo énfasis en cómo la alta productividad laboral y la felicidad van de la mano.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ACC5" name="ACC5" value="5"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b><span class="text-danger">Nota:</span> Adicionalmente, el personal debe realizar 3 cursos obligatorios de refuerzo en producto; que serán informados durante el transcurso del año.</b></p>
									</div>
									<!-- // Tab content END -->
									
									<!-- Tab content -->
									<div class="tab-pane <?php echo($_Class3);?>" id="tab3-1">
										<h4>Consultor Integral Diésel (CID)</h4><br><br>
										<p><b>2. A continuación te presentamos <span class="text-danger">6</span> opciones de cursos de habilidades blandas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que serán el foco del entrenamiento del 2017.</b></p>
										<p>
											<input type="hidden" name="opcn_CID" id="opcn_CID" value="CID">
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Cultura de servicio ventas</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Identificar y reconocer las habilidades y comportamientos que promueven el éxito y el cumplimiento de la promesa al cliente y el manejo de satisfacción asociadas a la oferta de atención en taller GM.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID1" name="CID1" value="1"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Vendale a la Mente para llegar a su Cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Edificar herramientas efectivas y eficaces que influyan directamente en el proceso de relación con la empresa, para establecer el servicio como motor e hilo conductor en la fidelización del cliente.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID2" name="CID2" value="2"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Administración del Tiempo</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Brindarle al participante las principales herramientas y técnicas para la organización diaria de su trabajo, determinando la urgencia e importancia de cada una de las tareas que realizan y profundizando en los soportes informáticos que deben realizar en su gestión.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID3" name="CID3" value="3"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Liderazgo en Ventas</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Mejorar en la gestión a través de la incorporación de herramientas de soporte a la venta. desarrollar las habilidades necesarias para gestionar adecuadamente los procesos de apoyo a la gestión comercial</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID4" name="CID4" value="4"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">El Poder del Marketing en los Negocios</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">La visión estratégica de mercadeo y su relación con la organización y la profundización del cliente, clasificación, crecimiento y desarrollo</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID5" name="CID5" value="5"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">6</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Productividad y Felicidad en mi Trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;"> Sensibilizar al participante sobre la importancia de dar lo mejor de sí en el puesto de trabajo, haciendo énfasis en cómo la alta productividad laboral y la felicidad van de la mano.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID6" name="CID6" value="6"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b>3. A continuación te presentamos <span class="text-danger">3</span> opciones de cursos de habilidades técnicas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">1</span> que será el foco del entrenamiento del 2017.</b></p>
										<p>
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Liderazgo y gestión estrategica en el negocio B&C</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">El curso de liderazgo tiene como objetivo que los CID, tengan el siguiente conocimiento: Psicologia del consumidor B&C (consultar avaladores)  (Escalera de progreso JAM) Plan administración de vida con forecast, finanzas personales y proyectos personales con plan de negocios de la persona, DOFA.  Así como información sobre administración de flotas, taller de servio y áreas de repuestos.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID7" name="CID7" value="7"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Finanzas, presupuestos y proyectos especiales para negocios B&C</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Este curso tiene como objetivo que el estudiante, tenga la formación básica frente a los siguientes temas: Técnicas efectivas de negociación y Manejo de presupuesto</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID8" name="CID8" value="8"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Caracterización y manejo de clientes B&C</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Este curso tiene como objetivo que el estudiante, tenga la formación básica frente a los siguientes temas: Servicio al cliente y Proceso consultivo de prospección y venta</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="CID9" name="CID9" value="9"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b><span class="text-danger">Nota:</span> Adicionalmente, el personal debe realizar 3 cursos obligatorios de refuerzo en producto; que serán informados durante el transcurso del año.</b></p>
									</div>
									<!-- // Tab content END -->
									
									<!-- Tab content -->
									<div class="tab-pane <?php echo($_Class4);?>" id="tab4-1">
										<h4>Asesor de Repuestos y accesorios</h4><br><br>
										<p><b>2. A continuación te presentamos <span class="text-danger">5</span> opciones de cursos de habilidades blandas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que serán el foco del entrenamiento del 2017.</b></p>
										<p>
											<input type="hidden" name="opcn_ARA" id="opcn_ARA" value="ARA">
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Vendale a la Mente para llegar a su Cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Edificar herramientas efectivas que influyan directamente en el proceso de relación con la empresa, para establecer el servicio como motor e hilo conductor en la fidelización del cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ARA1" name="ARA1" value="1"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Mejorando Resultados de mi <b>Negociación</b> 1 Y 2</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Incorporar los conocimientos necesarios para mantener una negociación efectiva con los clientes de repuestos tanto a nivel minorista como mayorista.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ARA2" name="ARA2" value="2"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Poder del Comportamiento</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Descubrir el poder que tiene su comportamiento para alimentar el de las personas con quienes se encuentra a diario.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ARA3" name="ARA3" value="3"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Productividad y Felicidad en mi Trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;"> Sensibilizar al participante sobre la importancia de dar lo mejor de sí en el puesto de trabajo, haciendo énfasis en cómo la alta productividad laboral y la felicidad van de la mano.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ARA4" name="ARA4" value="4"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Proceso de Venta de Repuestos 1 y 2</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Incorporar un proceso homogéneo de asesorar a los clientes de los concesionarios, asegurando la calidad en la atención e impulsando a su vez los resultados comerciales en la unidad de repuestos</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="ARA5" name="ARA5" value="5"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b><span class="text-danger">Nota:</span> Adicionalmente, el personal debe realizar 3 cursos obligatorios de refuerzo en producto; que serán informados durante el transcurso del año.</b></p>
									</div>
									<!-- // Tab content END -->

									<!-- Tab content -->
									<div class="tab-pane <?php echo($_Class5);?>" id="tab5-1">
										<h4>Técnico Integral Gasolina (TIG)</h4><br><br>
										<p><b>2. A continuación te presentamos <span class="text-danger">5</span> opciones de cursos de habilidades blandas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que serán el foco del entrenamiento del 2017.</b></p>
										<p>
											<input type="hidden" name="opcn_TIG" id="opcn_TIG" value="TIG">
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Habilidades Comunicación</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;"> Identificar los factores de éxito de la comunicación en cada situación de desempeño para utilizar como herramienta en el puesto de trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG1" name="TIG1" value="1"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Vendale a la Mente para llegar a su Cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Edificar herramientas efectivas y eficaces que influyan directamente en el proceso de relación con la empresa, para establecer el servicio como motor e hilo conductor en la fidelización del cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG2" name="TIG2" value="2"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Bien Hecho a la Primera Vez 1 y 2</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Desarrollar las habilidades relacionadas con la implementación del proceso de calidad en el día a día del técnico especialista</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG3" name="TIG3" value="3"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Poder del Comportamiento</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Descubrir el poder que tiene su comportamiento para alimentar el de las personas con quienes se encuentra a diario.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG4" name="TIG4" value="4"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Productividad y Felicidad en mi Trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Sensibilizar al participante sobre la importancia de dar lo mejor de sí en el puesto de trabajo, haciendo énfasis en cómo la alta productividad laboral y la felicidad van de la mano.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG5" name="TIG5" value="5"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b>3. A continuación te presentamos <span class="text-danger">6</span> opciones de cursos de habilidades técnicas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que será el foco del entrenamiento del 2017.</b></p>
										<p>
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Metrología aplicada en motores gasolina</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Conocer los instrumentos de medición usados en el diagnóstico y reparación de motores a gasolina para realizar operaciones bien hechas a la primera vez.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG7" name="TIG7" value="7"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Interpretación y análisis de alambrados y planos eléctricos</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Suministrar herramientas y técnicas de búsqueda e interpretación de diagramas eléctricos mediante la aplicación del si para facilitar la ejecución de las rutinas de diagnótico que involucren el uso de los diagramas.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG8" name="TIG8" value="8"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Mantenimiento y reparación de suspensión y dirección</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Describir los métodos usados en la reparación de los sistemas de direccion y suspensión, conocer las especificaciones indicadas en el manual de servicio.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG9" name="TIG9" value="9"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Diagnóstico estratégico</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Aplicar un proceso repetitivo para diagnosticar y reparar los sistemas del vehiculo utilizando una estrategia basica de diagnóstico.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG10" name="TIG10" value="10"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Conocimiento y principios del sistema de frenos hidráulico y practicas de mantenimiento</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Describir los diferentes sistemas de frenos hidraulicos y sus procedimientos de diagnóstico y reparación, conocer  los procedimientos  descritos en el  manual de servicio</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG11" name="TIG11" value="11"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">6</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Análisis de fallas transmisión manual, y diagnostico de transfer y diferenciales</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Diagnosticar los componentes del sistema del sistema de transmisión y diferencial para asi determinar el correctivo adecuado en cada caso particular, de acuerdo a los manuales del fabricante.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TIG12" name="TIG12" value="12"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b><span class="text-danger">Nota:</span> Adicionalmente, el personal debe realizar 3 cursos obligatorios de refuerzo en producto; que serán informados durante el transcurso del año.</b></p>
									</div>
									<!-- // Tab content END -->

									<!-- Tab content -->
									<div class="tab-pane <?php echo($_Class6);?>" id="tab6-1">
										<h4>Técnico Integral Diésel (TID)</h4><br><br>
										<p><b>2. A continuación te presentamos <span class="text-danger">5</span> opciones de cursos de habilidades blandas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que serán el foco del entrenamiento del 2017.</b></p>
										<p>
											<input type="hidden" name="opcn_TID" id="opcn_TID" value="TID">
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Habilidades Comunicación</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Identificar los factores de éxito de la comunicación en cada situación de desempeño para utilizar como herramienta en el puesto de trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID1" name="TID1" value="1"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Vendale a la Mente para llegar a su Cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Edificar herramientas efectivas y eficaces que influyan directamente en el proceso de relación con la empresa, para establecer el servicio como motor e hilo conductor en la fidelización del cliente</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID2" name="TID2" value="2"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Bien Hecho a la Primera Vez 1 y 2</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Desarrollar las habilidades relacionadas con la implementación del proceso de calidad en el día a día del técnico especialista</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID3" name="TID3" value="3"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Poder del Comportamiento</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Descubrir el poder que tiene su comportamiento para alimentar el de las personas con quienes se encuentra a diario.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID4" name="TID4" value="4"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Productividad y Felicidad en mi Trabajo</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Sensibilizar al participante sobre la importancia de dar lo mejor de sí en el puesto de trabajo, haciendo énfasis en cómo la alta productividad laboral y la felicidad van de la mano.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID5" name="TID5" value="5"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b>3. A continuación te presentamos <span class="text-danger">5</span> opciones de cursos de habilidades técnicas para tu equipo de trabajo de las cuales debe seleccionar <span class="text-danger">3</span> que será el foco del entrenamiento del 2017.</b></p>
										<p>
											<table class="table table-condensed table-vertical-center table-thead-simple">
												<thead>
													<tr>
														<th class="center" width="5%">#</th>
														<th class="center" width="20%">Nombre</th>
														<th class="center" width="50%">Descripción</th>
														<th class="center" width="10%">Metodología</th>
														<th class="center" width="10%">Duración</th>
														<th class="center" width="5%">Seleccione</th>
													</tr>
												</thead>
												<tbody id="cuerpo_tabla">
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">1</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Inspección, diagnostico y reparación de los sistemas de frenos 100% aire con ABS</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Dar a conocer al técnico integral diesel, el funcionamiento, la reparación y el diagnóstico del sistema de frenos de aire y abs</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">8 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID7" name="TID7" value="7"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">2</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Inspección, diagnóstico y reparación de diferenciales</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Suministrar al técnico de servicio diesel los conocimientos necesarios que requiere para el desempeño de tareas mecanicas en la red de concesionarios , con sentido de pertenencia y cultura empresarial.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID8" name="TID8" value="8"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">3</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Inspección, diagnóstico y reparación de transmisiones ISUZU</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Conozca los principales conceptos para una eficaz reparacion y un acertado diagnostico de las transmisiones</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID9" name="TID9" value="9"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">4</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Diagnóstico general de suspension y sistemas de dirección</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Al finalizar el curso, el estudiante esta en capacidad de diagnosticar fallas en los sitemas de direccion y suspension, usar herramientas especiales e interpretar cartas de diagnostico</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID10" name="TID10" value="10"></td>
													</tr>
													<tr class="selectable">
														<td class="left" style="padding: 2px; font-size: 80%;">5</td>
														<td class="left" style="padding: 2px; font-size: 80%;">Procedimiento de reparación de los motores electrónicos ISUZU 4JJ1 “Common Rail”</td>
														<td class="left" style="padding: 2px; font-size: 80%;text-align:justify;">Dar a conocer el alumnos las partes internas de los motores, su funcionamiento, diagnóstico y reparación, con herramientas propias de dichos modelos.</td>
														<td class="left" style="padding: 2px; font-size: 80%;">IBT</td>
														<td class="left" style="padding: 2px; font-size: 80%;">4 Horas</td>
														<td class="left" style="padding: 2px; font-size: 80%;"><input type="checkbox" id="TID11" name="TID11" value="11"></td>
													</tr>
												</tbody>
											</table>
										</p>
										<p><b><span class="text-danger">Nota:</span> Adicionalmente, el personal debe realizar 3 cursos obligatorios de refuerzo en producto; que serán informados durante el transcurso del año.</b></p>
									</div>
									<!-- // Tab content END -->
									
								</div>
								
							</div>
						</div>
					</div>
					<!-- // Tabs END -->
				</div>
				<div class="widget widget-survey border-none col-sm-3 center">
				</div>
				<div class="widget widget-survey col-md-12 center" id="PanelBotones">
					<div class="widget-body padding-none">
				  		<div class="innerAll border-bottom">
							<button type="submit" class="pull-right btn btn-success" id="crearDetalle"><i class="fa fa-check-circle"></i> Enviar</button><br/><br/>
							<div class="separator bottom"></div>
						</div>
					</div>
				</div>
				<input type="hidden" name="opcn" id="opcn" value="<?php echo($RegistrarEncuesta);?>">
			</form>
			<?php }?>
		</div>
		<!-- // END col -->

	</div>
	<!-- // END row -->
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/encuesta_gerentes.js"></script>
</body>
</html>