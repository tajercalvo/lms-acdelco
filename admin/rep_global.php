<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_global.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons globe"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_global.php">Reporte Global</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte Global </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Esta opción descarga un archivo con visualización completa de reporte en cuanto a seguimiento, notas e información de los concesionarios en las diferentes trayectorias académicas.</h5></br>
			</div>
			<div class="col-md-2">
			</div>
		</div>
		<form action="rep_global.php" method="post">	
		<div class="row">
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="charge_id" style="padding-top:8px;">Curso:</label>
					<div class="col-md-10">
						<select style="width: 100%;" id="charge_id" name="charge_id" <?php if($_SESSION['max_rol']<=4){ ?>readonly="readonly"<?php } ?>>
							<?php foreach ($datosCursos as $key => $Data_Cursos) { ?>
								<option value="<?php echo($Data_Cursos['charge_id']); ?>" <?php if( (isset($_POST['charge_id']) && $_POST['charge_id']==$Data_Cursos['charge_id']) ){ ?>selected="selected"<?php }elseif( (isset($_SESSION['charge_id']) && $_SESSION['charge_id']==$Data_Cursos['charge_id']) ){ ?>selected="selected"<?php } ?> ><?php echo($Data_Cursos['charge']); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
					<div class="col-md-9 input-group date">
				    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final:</label>
					<div class="col-md-9 input-group date">
				    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
			</div>
		</div>
		</form>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información:</h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<?php if($cantidad_datos > 0){ ?>
			<!-- Total elements-->
			<div class="form-inline separator bottom small">
				Total de registros: <?php echo($cantidad_datos); ?>
			</div>
			<!-- // Total elements END -->
			<h5><a target="_blank" href="rep_global_excel.php?charge_id=<?php echo($_POST['charge_id']); ?>&start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<!--<table>
			        <thead>
			                <tr>
			                    <th data-hide="phone,tablet" style="width: 10%;">C&Oacute;DIGO</th>
			                    <th data-hide="phone,tablet" style="width: 30%;">CURSO</th>
			                    <th data-hide="phone,tablet" style="width: 5%;">USUARIOS</th>
			                    <th data-hide="phone,tablet" style="width: 30%;">TRAYECTORIAS</th>
			                    <th data-hide="phone,tablet" style="width: 5%;">PROMEDIO</th>
			                    <th data-hide="phone,tablet" style="width: 5%;">MAXIMA</th>
			                    <th data-hide="phone,tablet" style="width: 5%;">MININMA</th>
			                </tr>
			            </thead>
			            <tbody>
			                <?php 
			                if($cantidad_datos > 0){ 
			                    foreach ($datosCursos_all as $iID => $data) { 
			                        ?>
			                    <tr>
			                        <td><?php echo(($data['code'])); ?></td>
			                        <td><?php echo(($data['course'])); ?></td>
			                        <td><?php echo($data['usr_pres']); ?></td>
			                        <td><?php 
			                            foreach ($data['cargos'] as $iID => $data_c) {
			                                echo(("<i class='fa fa-fw icon-identification'></i>".$data_c['charge']."<br>"));
			                            } 
			                         ?></td>
			                        <td align="center"><?php echo(number_format($data['avg_score'],0)); ?></td>
			                        <td><?php echo($data['max_score']); ?></td>
			                        <td><?php echo($data['min_score']); ?></td>
			                    </tr>
			                <?php } } ?>
			            </tbody>
			    </table>-->
		<?php }else{ ?>
			<h5>Aún no ha consultado o no se encuentra información disponible para descargar, configure la búsqueda y de click en Consultar<h5>
		<?php } ?>
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
							
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_global.js"></script>
</body>
</html>