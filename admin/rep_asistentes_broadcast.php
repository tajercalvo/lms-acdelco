<?php include('src/seguridad.php'); ?>
<?php

$location = 'rep_asistentes_broadcast';
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<head><meta charset="gb18030">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    
</head>

<style type="text/css">
		
	</style>
<body class="document-body ">

<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted est¨¢ en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Asignacion de cursos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Broadcast</h2>
						
						<div class="clearfix"></div>
					</div>
			
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Informaci&oacuten: <span id="num_asistentes" style="color: orange"></span> </h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
						
							<div class="row">
									<div class="col-md-10">
										<h5 style="text-align: justify; ">Consulta las asistencias del personal en el Broadcast</h5>
									</div>
									<br>
								
									<div class="col-md-2">
										<!-- <form action="ficheroExcel.php" method="post" id="FormularioExportacion">
											<h5 id="descargar" style="display: none;" ><a class="glyphicons cloud-upload"  ><i></i>Descargar</a><br></h5>
											<input type="hidden" id="datos_tabla" name="datos_tabla" />
											<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Reporte_broadcast" />
										</form> -->
								</div>
							</div>
							<div class="row">
								<form id="frm" >
									<div class="col-md-10">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="conference_id" style="padding-top:8px;">Sesión:</label>
											<div class="col-md-10">
												<select style="width: 100%;" id="conference_id" name="conference_id"></select>
											</div>
									    </div>
										<!-- // Group END -->
									</div>
									<div class="col-md-1">
										<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
										<span id="consultando">
											
										</span>
									</div>
									<div class="col-md-1">
										 <button id="chat" class="btn btn-success" type="button" data-toggle="modal" data-target="#modal_suscribir" style="display: none">Chat</button> 
									</div>
								</form>
							</div>
							<div class="widget widget-heading-simple widget-body-white">
	
							<div class="widget-head">
								<h4 class="heading glyphicons list"><i></i> Información: <span id="num_asistentes" style="color: orange"></span> </h4>
							</div>
							<!-- // Widget heading END -->
							<div class="widget-body">
								<!-- Table elements-->
								<table id="asistentes" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable table-hover" style="width: 100%;" >
									<!-- Table heading -->
									<thead>
										<tr>
											<th data-hide="phone,tablet">IDENTIFICACIÓN</th>
											<th data-hide="phone,tablet">NOMBRE Y APELLIDO</th>
											<th data-hide="phone,tablet">EMPRESA</th>
											<th data-hide="phone,tablet">SEDE</th>
											<th data-hide="phone,tablet">TRAYECTORIA</th>
											<th data-hide="phone,tablet">PERFIL</th>
										</tr>
									</thead>
									
									<tbody></tbody>
								</table>
							</div>
						</div>
						<div id="modal_suscribir" class="modal fade" role="dialog">
	              <div class="modal-dialog modal-lg" style="width: 90%;padding-top: 1%;">
	                <div class="modal-content">
	                 	<div class="modal-header">
                      		<button type="button" class="close btn btn-danger" data-dismiss="modal">&times;</button>
                      		<h2 style="text-align: center;color: #bd272c;">CHAT</h2>
                    	</div>

                 	<div class="row contenedor" >
                        <div class="row">
                         <div class="col-md-12" style="text-align: center;">
                          <img src="https://autotrain.com.co/wp-content/uploads/2020/03/LogoAutoTrain-1.png" style="width:30%">
                          </div>
                        </div>
                 	   <div class="contenido" style="color: black; padding: 1%">
		                     
		                     <div class="row">
		                     	<div class="col-md-12">
				                      <table id="tabl_chat" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable table-hover" style="width: 100%;" >
										
										<thead>
											<tr>
												<th style="color: white">Comentario</th>
		                                		<th style="color: white">Nombres</th>
				                                <th style="color: white">Empresa</th>
				                                <th style="color: white">Fecha</th>
				                                <th style="color: white">Conferencia</th>
				                                
											</tr>
										</thead>
										<tbody>
											<div style="text-align: center;color: white"><span id="num_mensajes" style="text-align: center;color: #bd272c;font-size: 17px;"></span></div>
										</tbody>
										
									</table>
		                    	</div>
		                    </div>
              		   </div>
             
		                  </div>
				             <div class="modal-footer">
				               <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				             </div>
		                </div>
                   </div>
                </div>



			          </div><!-- // widget-body-->
		  		 </div>
					
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	  <!--  <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript" ></script> -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js" type="text/javascript" ></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js" type="text/javascript" ></script>
	<script src="js/rep_chat_asistentes.js"></script>
	
  </body>
</html>