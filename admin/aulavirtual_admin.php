<?php include('src/seguridad.php'); ?>
<?php include('controllers/aulavirtual_admin.php');
$location = 'distribucion';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
//$CalendarData = true;
$script_select2v4 = 'select2v4';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<style>
		.select2-container--default .select2-results__option--highlighted[aria-selected] {
			background-color: #BD272B;
			color: white;
		}
	</style>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons conversation"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/aulavirtual_admin.php">AULAS VIRTUALES</a></li>
					<div class="btn-group pull-right">
						<?php if (isset($_GET['id'])) { ?>
							<a href="foros_ventas.php" class="btn btn-xs btn-primary pull-right">
								<i class="fa fa-mail-reply-all fa-fw"></i>
								Regresar</a>
						<?php } ?>
					</div>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div>

						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<!-- ================================================ -->
					<!-- Lista programaciones -->
					<!-- ================================================ -->
					<h1 class="margin-none innerAll content-heading bg-white border-bottom">AULAS VIRTUALES</h1>
					<div class="innerAll spacing-x2">
						<div class="innerAll half">
							<a id="btn_crear" class="openClose btn btn-sm btn-success"><i class="fa fa-plus fa-fw"></i> Nueva Aula Virtual</a>
						</div>
						<div class="row" id="div_crear_editar" style="display: none;">
							<form id="form_Crear">
								<div class="col-md-12">
									<div class="widget-body">
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Moderador: </label>
												<select id="teacher_id" name="teacher_id" class="js-data-example-ajax" style="width: 100%;"></select>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Auxiliares: </label>
												<select id="auxiliares" name="auxiliares[]" multiple class="js-data-example-ajax" style="width: 100%;">
												</select>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Aula Privada: </label>
												<select id="privada" name="privada" style="width: 100%;">
													<option value="0">No</option>
													<option value="1">Si</option>
												</select>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Cargos: </label>
												<select id="cargos" name="cargos[]" multiple style="width: 100%;">
												</select>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>

										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Comunicación: </label>
												<input type="text" id="conference" name="conference" class="form-control col-md-8" placeholder="Nombre de la comunicación" />
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Sesión ID: </label>
												<input type="text" id="session_id" name="session_id" class="form-control col-md-8" placeholder="Sesión ID" />
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Token: </label>
												<textarea id="token" name="token" class="col-md-12 form-control" rows="5"></textarea>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
									</div>
									<div class="modal-footer" style="border-top: 0px solid; padding: 0px 20px 10px; margin-top: 0px;">
										<button type="submit" id="BtnCreacion" class="btn btn-success">Crear</button>
										<button class="openClose btn btn-danger">Cerrar</button>
									</div>
								</div>
							</form>
						</div>
						<!-- row -->
						<div class="row">
							<!-- ===================================================================================== -->
							<!-- Lista de POST -->
							<!-- ===================================================================================== -->
							<div class="col-md-12">
								<div class="widget" id="listado_post">
									<!-- Post List -->
									<!-- Post List -->
								</div>
							</div>

						</div>
					</div>
					<!-- ================================================ -->
					<!-- END Lista programaciones -->
					<!-- ================================================ -->
					<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
			<!-- ================================================ -->
			<!-- Fomulario Cargar Imagenes Presentación -->
			<!-- ================================================ -->
			<form enctype="multipart/form-data" id="form_presentacion"><br><br>
				<div class="modal fade" id="ModalPresentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Cargar presentación</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<!-- Row -->
										<!-- ================================================ -->
										<!-- Cargar Imagenes -->
										<!-- ================================================ -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Presentación: </label>
												<input type="file" id="imagenes" name="imagenes" multiple />
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- ================================================ -->
										<!-- END Cargar Imagenes -->
										<!-- ================================================ -->
										<br>
										<!-- ================================================ -->
										<!-- tipo cargue -->
										<!-- ================================================ -->
										<!-- <div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Tipo de cargue: </label>
												<select style="width: 100%;" id="tipo_cargue" name="tipo_cargue">
													<option value="nuevas">Nuevas</option>
													<option value="agregar">agregar</option>
												</select>
											</div>
											<div class="col-md-1">
											</div>
										</div> -->

										<!-- ================================================ -->
										<!-- END tipo cargue -->
										<!-- ================================================ -->
										<!-- Row END-->
										<div class="clearfix"><br></div>
									</div>
								</div>
							</div>
							<div class="modal-footer loader_s">
								<input type="hidden" name="opcn" id="opcn" value="imagenes">
								<input type="hidden" name="conference_id" id="conference_id" value="">
								<button type="submit" id="uploadFiles" class="btn btn-primary">Cargar</button>
								<button type="reset" id="resetFiles" style="display:none" class="btn btn-primary">Cargar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- ================================================ -->
			<!-- END Fomulario Cargar Imagenes Presentación -->
			<!-- ================================================ -->

			<!-- ================================================ -->
			<!-- Fomulario pregnutas -->
			<!-- ================================================ -->
			<div class="modal fade" id="modalpreguntas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Cargar presentación</h4>
						</div>
						<div class="modal-body">
							<form id="frm_pregunta">
								<div class="form-group">
									<label for="email">Orden</label>
									<input type="text" class="form-control" id="orden" name="orden">
								</div>
								<div class="form-group">
									<label for="email">Pregunta</label>
									<input type="text" class="form-control" id="pregunta" name="pregunta">
								</div>
								<button type="submit" class="btn btn-success">Crear preguntas</button>
							</form>
							<br>
							<table style="width: 100%;" class="table table-bordered table-hover">
								<thead>
									<th>Orden</th>
									<th>Pregunta</th>
									<th style="width: 1%;">Acciones</th>
								</thead>
								<tbody id="tabla_preguntas">
									<!-- <tr>
										<td>1</td>
										<td>Que le parece esto</td>
										<td style="text-align: center;">
											<span style="cursor: pointer;" class="text-primary"><i class="fa fa-trash-o"></i></span>
											<span style="cursor: pointer;" class="text-success"><i class="fa fa-eye" ></i></span>
										</td>

									</tr> -->
								</tbody>
							</table>
							<form id="frm_respuesta">
								<div class="form-group">
									<label for="email">Respuestas</label>
									<input type="text" class="form-control" id="respuesta">
								</div>
								<button type="submit" class="btn btn-success">Crear respuesta</button>
								<br>
							</form>
							<br>
							<table style="width: 100%;" class="table table-bordered table-hover">
								<thead>
									<th>Respuestas</th>
									<th style="width: 1%;">Acciones</th>
								</thead>
								<tbody id="tabla_respuestas">
								</tbody>
							</table>
						</div>
						<div class="modal-footer loader_s">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
			<!-- ================================================ -->
			<!-- Formulario preguntas
			 ================================================ -->

		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<!-- <script src="//static.opentok.com/v2/js/opentok.js"></script> -->
	<script src="js/aulavirtual_admin.js?varRand=<?php echo (rand(10, 999999)); ?>"></script>
</body>

</html>