<?php include('src/seguridad.php'); ?>
<?php 
$_GET['opcn'] = 'ver';
$_GET['id'] = $_GET['course_id'];
$id_usuario = 0;
if(isset($_GET['user_id'])){
	$id_usuario = $_GET['user_id'];
	include_once('models/usuarios.php');
	$usuario_Class_CS = new Usuarios();
	$datos_Usr = $usuario_Class_CS->consultaDatosUsuario($id_usuario);
	$name_usr = $datos_Usr['first_name'].' '.$datos_Usr['last_name'];
	$identification_usr = $datos_Usr['identification'];
	unset($usuario_Class_CS);
}else{
	$name_usr = $_SESSION['NameUsuario'];
	$identification_usr = $_SESSION['identification'];
}
include('controllers/cursos.php');
/*Confirma que haya pasado el curso*/
if($confirmaResultado == 0){
	header("location: login.php");
}
/*Confirma que haya pasado el curso*/
/*funciones
--Al español fechas.
*/
	$date = $_GET['date'];
	$mes = date('F', strtotime(str_replace('-','/', $date)));
	$ano = date('Y', strtotime(str_replace('-','/', $date)));
	$dia_m = date('D', strtotime(str_replace('-','/', $date)));
	$dia_m = str_replace('Sun', 'Domingo', $dia_m);
	$dia_m = str_replace('Mon', 'Lunes', $dia_m);
	$dia_m = str_replace('Tue', 'Martes', $dia_m);
	$dia_m = str_replace('Wed', 'Mi&eacute;rcoles', $dia_m);
	$dia_m = str_replace('thu', 'Jueves', $dia_m);
	$dia_m = str_replace('Fri', 'Viernes', $dia_m);
	$dia_m = str_replace('Sat', 'S&aacute;bado', $dia_m);

	$mes = str_replace('January', 'Enero', $mes);
	$mes = str_replace('February', 'Febrero', $mes);
	$mes = str_replace('March', 'Marzo', $mes);
	$mes = str_replace('April', 'Abril', $mes);
	$mes = str_replace('May', 'Mayo', $mes);
	$mes = str_replace('June', 'Junio', $mes);
	$mes = str_replace('July', 'Julio', $mes);
	$mes = str_replace('August', 'Agosto', $mes);
	$mes = str_replace('September', 'Septiembre', $mes);
	$mes = str_replace('October', 'Octubre', $mes);
	$mes = str_replace('November', 'Noviembre', $mes);
	$mes = str_replace('December', 'Diciembre', $mes);

	$dia = date('d', strtotime(str_replace('-','/', $date)));
	$miFecha = $dia_m.' '.$dia.' de '.$mes.' de '.$ano;
/*funciones
--Al español fechas.
*/

$duration = 0;
if(isset($registroConfiguracionModulosDetail)){
	foreach ($registroConfiguracionModulosDetail as $iID => $data_modules) {
		$duration = $data_modules['duration_time']+$duration;
	}
	if($registroConfiguracionDetail['type']!="WBT"){
		$tipo = "presencial ";
	}else{
		$tipo = "virtual ";
	}
}else{
	header("location: login.php");
}

?>
<page>
	<style type="text/css">
<!--
		@font-face {
			font-family: LouisRegular;
			src: url('../assets/fonts/LouisRegular.ttf') format('truetype');
		}
		@font-face {
			font-family: LouisItalici;
			src: url('../assets/fonts/LouisItalic.ttf') format('truetype');
		}
		@font-face {
			font-family: DurantItalici;
			src: url('../assets/fonts/DurantItalic.ttf') format('truetype');
		}
		.cont_principal{
			width: 100%;
			height: 100%;
			/*background-image: url(../assets/images/fondo_cert.png);
			background-position: left top; 
			background-repeat: no-repeat;
			*/
		}
		.cont_inter{
			width: 90%;
			height: 95%;
			/*border: 10px solid #d65050;*/
			/*border-bottom: 1px solid rgb(101, 106, 109);
			border-top: 1px solid rgb(101, 106, 109);
			border-right: 1px solid rgb(101, 106, 109);
			border-left: 1px solid rgb(101, 106, 109);*/
		}
		table{
			border-collapse: collapse;
		}
		.imgLogo{
			width: 160px;
		}
		.certificado{
			text-align: center;
			font-family: LouisRegular;
			font-size: 30px;
			color: #0058a3;
		}
		.borde{
			border: 10px solid #d65050;
		}
		.gmaca{
			text-align: center;
			font-size: 15px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			height: 25px;
		}
		.persona{
			text-align: center;
			font-family: LouisRegular;
			font-size: 45px;
			font-weight: bold;
			width: 90%;
			margin-left: 5%;
			margin-right: 5%;
			border-bottom: 1px solid rgb(101, 106, 109);
		}
		.cedula{
			text-align: center;
			font-size: 17px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			height: 50px;
		}
		.leyenda{
			text-align: center;
			font-size: 15px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			width: 90%;
			margin-left: 5%;
			margin-right: 5%;
		}
		.imgVamos{
			width: 180px;
		}
		.cargo{
			text-align: center;
			font-size: 10px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
		}
		.linea_sep{
			border-bottom: 1px solid #000000;
			width: 85%;
			margin-left: 10px;
		}
		.TextFirma{
			text-align: center;
			font-family: LouisItalici;
			vertical-align: top;

		}
		.imgFirma{
			width: 160px;
		}
-->
	</style>
	<table align="center" width: "100%;" class="cont_principal" >
		<tr>
			<td class="borde">
				<table align="center" class="cont_inter">
					<tr>
						<td align="center" colspan="5">
							<img src="../assets/images/distinciones/default.png" class="imgLogo">
						</td>
					</tr>
					<tr>
						<td align="center" class="certificado" colspan="5">
							CERTIFICADO
						</td>
					</tr>
					<tr>
						<td align="center" class="gmaca" colspan="5">
							GMACADEMY certifica que:
						</td>
					</tr>
					<tr>
						<td align="center" class="persona" colspan="5">
							<?php echo $name_usr;?>
						</td>
					</tr>
					<tr>
						<td align="center" class="cedula" colspan="5">
							<strong>C.C. <?php echo $identification_usr; ?></strong>
						</td>
					</tr>
					<tr>
						<td align="center" class="leyenda" colspan="5">
							Tom&oacute; el curso <?php echo($tipo); ?>correspondiente a "<?php echo($registroConfiguracionDetail['course']); ?>", <?php if($tipo=="presencial "){ ?>con una duraci&oacute;n de <?php echo($duration); ?> horas <?php } ?>el d&iacute;a <?php echo $miFecha; ?>.<br>
							Bogot&aacute;, Colombia.
						</td>
					</tr>
					<tr>
						<td align="right" colspan="5" style="padding-right: 145px; height: 120px; vertical-align: top;" >
							<img src="../assets/images/vamoscontoda.png" class="imgVamos">
						</td>
					</tr>
					<tr>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma1.png" class="imgFirma" ><br>
							<span class="linea_sep"></span><br>
							Jorge Meg&iacute;a.<br>
							<span class="cargo">Presidente Director Gerente<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma2.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Jaime Gil.<br>
							<span class="cargo">Vicepresidente Comercial<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma3.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Carlos Pacheco.<br>
							<span class="cargo">Gerente de desarrollo de concesionarios<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma4.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Carlos Vargas.<br>
							<span class="cargo">Supervisor de innovaci&oacute;n<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma5.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Juan R. Luj&aacute;n.<br>
							<span class="cargo">Coordinador estrat&eacute;gico<br>Universidad Corporativa GMACADEMY<br>GM Colmotores</span>
						</td>
					</tr>
					<tr>
						<td align="right" colspan="5" style="height: 25px;" >
							&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		
</page>