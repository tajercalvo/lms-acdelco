<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_disponibilidad_historica.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_disponibilidad_historica.php">Reporte de Cupos Vs Inscripciones Vs Asistencia</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Cupos Vs Inscripciones Vs Asistencia </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de los inscritos por cada curso en los programadas definidos entre 2 fechas.</h5></br>
								</div>
								<div class="col-md-2">
									<?php if($cantidad_datos > 0){ ?>
										<h5><a href="rep_disponibilidad_historica_excel.php?rep=detalle&start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Detalle</a><h5><br>
										<h5><a href="rep_disponibilidad_historica_excel.php?rep=agrupado&start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Agrupado</a><h5><br>
									<?php } ?>
								</div>
							</div>
							<div class="row">
								<form action="rep_disponibilidad_historica.php" method="post">
								<div class="col-md-5">
									<!-- Group -->
									<div class="form-group">
										<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
										<div class="col-md-7 input-group date">
											<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
											<span class="input-group-addon">
												<i class="fa fa-th"></i>
											</span>
										</div>
									</div>
									<!-- // Group END -->
								</div>
								<div class="col-md-5">
									<!-- Group -->
									<div class="form-group">
										<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Final:</label>
										<div class="col-md-7 input-group date">
											<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
											<span class="input-group-addon">
												<i class="fa fa-th"></i>
											</span>
										</div>
									</div>
									<!-- // Group END -->
								</div>
								<div class="col-md-2">
									<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
								</div>
								</form>
							</div>
						</div>
					</div>
					<div class="well">
						<table class="table table-condensed table-vertical-center table-thead-simple">
							<!-- Table heading -->
							<thead>
								<tr>
									<th class="center">CODIGO</th>
									<th class="center">CURSO</th>
									<th class="center">CONCESIONARIO</th>
									<th class="center">FECHA</th>
									<th class="center">FINAL</th>
									<th class="center">LUGAR</th>
									<th class="center">PROFESOR</th>
									<th class="center">MAX-MIN</th>
									<th class="center">INS/ASIS</th>
									<th class="center">DISP</th>
									<th class="center">OPCION</th>
								</tr>
							</thead>
							<!-- // Table heading END -->
							<tbody id="cuerpo_tabla">
								<?php
										$asistieron_tot = 0;
										$noasistieron_tot = 0;
										$aprobaron = 0;
										$noaprobaron = 0;
										$invitados = 0;
										$puntaje = 0;
										$aprobo = 0;
										$puntaje_e = "0";
										$puntaje_h = "0";
										$var_Ctrl = 0;
									if($cantidad_datos > 0){
											foreach ($datosCursos_all as $iID => $data) {
												$contador = 0;
												$contador2 = 0;
												?>
										<tr>
											<td class="text-left" style="padding: 2px; font-size: 80%; width: 5%"><?php echo(($data['newcode'])); ?></td>
											<td class="text-left" style="padding: 2px; font-size: 80%; width: 15%"><?php echo(($data['course'])); ?></td>
											<td class="text-left" style="padding: 2px; font-size: 80%;"><?php echo(($data['dealer'])); ?></td>
											<td class="text-left" style="padding: 2px; font-size: 80%;"><?php echo(($data['start_date'])); ?></td>
											<td class="text-left" style="padding: 2px; font-size: 80%;"><?php echo(($data['end_date'])); ?></td>
											<td class="text-left" style="padding: 2px; font-size: 80%;"><?php echo(($data['living'])); ?></td>
											<td class="text-left" style="padding: 2px; font-size: 80%;"><?php echo(($data['first_name'].' '.$data['last_name'])); ?></td>
											<td class="text-center" style="padding: 2px; font-size: 80%;"><?php echo(($data['max_size']." - ".$data['min_size'])); ?></td>
											<td class="text-center" style="padding: 2px; font-size: 80%;"><a href="#" onclick=" mostrar( <?php echo( $data['dealer_schedule_id'] ); ?> ) "> <?php echo(($data['inscriptions']." - ".$data['asistance'])); ?> </a></td>
											<td class="text-center" style="padding: 2px; font-size: 80%;"><?php echo(count($data['disponibilidad'])); ?></td>
											<td class="text-center" style="padding: 2px; font-size: 80%;" class="text-primary"><a href="op_asignaciones.php?schedule_id=<?php echo(($data['schedule_id'])); ?>" target="_blank">Ver</a></td>
										</tr>

										<tr class="hide <?php echo( "_".$data['dealer_schedule_id'] ); ?>">
											<td colspan="12" style="background-color: white">
												<?php foreach ($asistencia_all as $key => $value): ?>
													<?php if ( $value['dealer_id'] == $data['dealer_id'] && $value['schedule_id'] == $data['schedule_id'] ): ?>
														<?php $contador++; ?>
														<?php if ( $value['status_id'] == 2 ){ ?>
															<span class="label label-success"><?php echo( $contador.". ".$value['first_name']." ".$value['last_name']." | ".$value['identification'] ); ?></span>
														<?php }else{ ?>
															<span class="label label-danger"><?php echo( $contador.". ".$value['first_name']." ".$value['last_name']." | ".$value['identification'] ); ?></span>
														<?php } ?>
													<?php endif; ?>
												<?php endforeach; ?>
											</td>
										</tr>

										<tr class="hide <?php echo( "_".$data['dealer_schedule_id'] ); ?>">
											<td colspan="12" style="background-color: white">
												<?php foreach ($data['disponibilidad'] as $key => $values): ?>
													<?php $contador2++; ?>
														<span class="label label-warning"><?php echo( $contador2.". ".$values['first_name']." ".$values['last_name']." | ".$values['identification'] ); ?></span>
												<?php endforeach; ?>
											</td>
										</tr>
									<?php }
									} ?>
							</tbody>
						</table>
					</div>

						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_disponibilidad_historica.js"></script>
</body>
</html>
