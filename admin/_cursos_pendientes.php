<head>
	<meta charset="utf-8">
	<style>
		*,*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:62.5%;-webkit-tap-highlight-color:rgba(0,0,0,0)}body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.428571429;color:#333;background-color:#fff}html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}body{margin:0}table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table>thead>tr>th,.table>tbody>tr>th,.table>tfoot>tr>th,.table>thead>tr>td,.table>tbody>tr>td,.table>tfoot>tr>td{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table>caption+thead>tr:first-child>th,.table>colgroup+thead>tr:first-child>th,.table>thead:first-child>tr:first-child>th,.table>caption+thead>tr:first-child>td,.table>colgroup+thead>tr:first-child>td,.table>thead:first-child>tr:first-child>td{border-top:0}.table>tbody+tbody{border-top:2px solid #ddd}.table
		.table{background-color:#fff}.table-condensed>thead>tr>th,.table-condensed>tbody>tr>th,.table-condensed>tfoot>tr>th,.table-condensed>thead>tr>td,.table-condensed>tbody>tr>td,.table-condensed>tfoot>tr>td{padding:5px}.table-bordered{border:1px
		solid #ddd}.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px
		solid #ddd}.table-bordered>thead>tr>th,.table-bordered>thead>tr>td{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*="col-"]{position:static;display:table-column;float:none}table td[class*="col-"],
		table th[class*="col-"]{display:table-cell;float:none}.table>thead>tr>.active,.table>tbody>tr>.active,.table>tfoot>tr>.active,.table>thead>.active>td,.table>tbody>.active>td,.table>tfoot>.active>td,.table>thead>.active>th,.table>tbody>.active>th,.table>tfoot>.active>th{background-color:#f5f5f5}.table-hover>tbody>tr>.active:hover,.table-hover>tbody>.active:hover>td,.table-hover>tbody>.active:hover>th{background-color:#e8e8e8}.table>thead>tr>.success,.table>tbody>tr>.success,.table>tfoot>tr>.success,.table>thead>.success>td,.table>tbody>.success>td,.table>tfoot>.success>td,.table>thead>.success>th,.table>tbody>.success>th,.table>tfoot>.success>th{background-color:#dff0d8}.table-hover>tbody>tr>.success:hover,.table-hover>tbody>.success:hover>td,.table-hover>tbody>.success:hover>th{background-color:#d0e9c6}.table>thead>tr>.danger,.table>tbody>tr>.danger,.table>tfoot>tr>.danger,.table>thead>.danger>td,.table>tbody>.danger>td,.table>tfoot>.danger>td,.table>thead>.danger>th,.table>tbody>.danger>th,.table>tfoot>.danger>th{background-color:#f2dede}.table-hover>tbody>tr>.danger:hover,.table-hover>tbody>.danger:hover>td,.table-hover>tbody>.danger:hover>th{background-color:#ebcccc}.table>thead>tr>.warning,.table>tbody>tr>.warning,.table>tfoot>tr>.warning,.table>thead>.warning>td,.table>tbody>.warning>td,.table>tfoot>.warning>td,.table>thead>.warning>th,.table>tbody>.warning>th,.table>tfoot>.warning>th{background-color:#fcf8e3}.table-hover>tbody>tr>.warning:hover,.table-hover>tbody>.warning:hover>td,.table-hover>tbody>.warning:hover>th{background-color:#faf2cc}@media (max-width: 767px){.table-responsive{width:100%;margin-bottom:15px;overflow-x:scroll;overflow-y:hidden;border:1px
		solid #ddd;-ms-overflow-style:-ms-autohiding-scrollbar;-webkit-overflow-scrolling:touch}.table-responsive>.table{margin-bottom:0}.table-responsive>.table>thead>tr>th,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tfoot>tr>td{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>thead>tr>th:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child{border-left:0}.table-responsive>.table-bordered>thead>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>td{border-bottom:0}}.well{min-height:20px;padding:19px;margin-bottom:20px;background-color:#f5f5f5;border:1px
		solid #e3e3e3;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)}
	</style>
</head>
<?php

include_once('models/cursos.php');
include_once('controllers/email.php');
$email = new EmailLudus();


$fecha = date_create( date( 'Y-m-d H:i:s' ) );
date_add( $fecha, date_interval_create_from_date_string( '1 days' ) );
$nuevafecha = date_format( $fecha, 'Y-m-d' );

$cursos = Cargos::getCursosIncompletos( $nuevafecha, '' );
$res_cursos = [];

foreach ($cursos as $key => $value) {
    $progress = Cargos::progress_bar( $value['course_id'], '' );
    $res_cursos[] = [
        "curso" => $value,
        "cumplimiento" => $progress
    ];
}

$mensaje_cuerpo = "<div style='background-color: white;'>
<table class='table table-primary'>
    <tr>
        <th>CURSO</th>
        <th>CODIGO</th>
        <th>MATERIAL DE APOYO</th>
    </tr>";
$contador_cursos = 0;
// recorro los cursos a evaluar
foreach ( $res_cursos as $key => $rc ) {
    $contador_no = 0;
    $subhtml = "<table class='table table-striped'>";

    // valido las opciones en busca de una faltante
    foreach ( $rc['cumplimiento'] as $key => $cm ) {
        if ( $cm['tiene'] == "NO" ) {
            $contador_no ++;
			$caso = $cm['caso'];
			$caso .= $cm['caso'] == "Preguntas de profundizacion" ? " ({$cm['aux']} preguntas)" : "";
            $subhtml .= "<tr>
                <td>$caso</td>
                <td>PENDIENTE</td>
            </tr>";
        }
    }

    $subhtml .= "</table>";

    if ( $contador_no > 0 ) {
        $contador_cursos++;
        $mensaje_cuerpo .= "<tr>
            <td>{$rc['curso']['course']}</td>
            <td>{$rc['curso']['newcode']}</td>
            <td>".$subhtml."</td>
        </tr>";
    }
}

$mensaje_cuerpo .= "</table>
            </div>";

// recorro los cursos validando si tienen manterial pendiente

// pinto la tabla de los cursos con su material sin cargar

// enviar correo a los seleccionados
if( $contador_cursos > 0 ){
    $titulo = "Material curso pendiente";
    $destinos = "";
    $titulo_mensaje = "Cursos pendientes programados el $nuevafecha";
    // $para = "elb.andres8@gmail.com, a.vega@luduscolombia.com.co";
    $para = "elb.andres8@gmail.com, zoraya.ordonez@gm.com, asistente.gmacademy@autotrain.com, alfonso.albarracin@autotrain.com, ricardo.osorio@autotrain.com, duvan.morales@autotrain.com";
    echo $mensaje_cuerpo;
    // $resultado = $email->enviaNewEmailCursosIncompletos($titulo,$para,$mensaje_cuerpo,$destinos, "Programaciones GMAcademy <programaciones@gmacademy.co>", $titulo_mensaje);
}

// print_r( $res_cursos );
