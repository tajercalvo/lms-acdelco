<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_resultados_gestion.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_resultados_gestion.php">Reporte de resultados gestión</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de resultados gestión </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
                    <div class="widget widget-heading-simple widget-body-gray">
                    	<div class="widget-body">
                    		<div class="row">
                    			<div class="col-md-10">
                    				<h5 style="text-align: justify;">Aquí encuentra las opciones para filtrar de forma estructurada la información de los cupos asignados a cada concesionario así como los asistentes correspondientes.</h5></br>
                    			</div>
                    			<div class="col-md-2">
                    				<h5 id="btn_descarga" style="display: none">
                                        <a id="ref_descarga" href="" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a>
                                    <h5>
                    			</div>
                    		</div>
                    		<div class="row">
                    			<form id="form_repGestion">
                        			<div class="col-md-5">
                        				<!-- Group -->
                        				<div class="form-group">
                        					<label class="col-md-5 control-label" for="start_date" style="padding-top:8px;">Fecha Inicial:</label>
                        					<div class="col-md-7 input-group date">
                        				    	<input class="form-control" type="text" id="start_date" name="start_date" placeholder="Inicia el"/>
                        				    	<span class="input-group-addon">
                        				    		<i class="fa fa-th"></i>
                        				    	</span>
                        					</div>
                        				</div>
                        				<!-- // Group END -->
                        			</div>
                        			<div class="col-md-5">
                        				<!-- Group -->
                        				<div class="form-group">
                        					<label class="col-md-5 control-label" for="end_date" style="padding-top:8px;">Fecha Final:</label>
                        					<div class="col-md-7 input-group date">
                        				    	<input class="form-control" type="text" id="end_date" name="end_date" placeholder="Finaliza el"/>
                        				    	<span class="input-group-addon">
                        				    		<i class="fa fa-th"></i>
                        				    	</span>
                        					</div>
                        				</div>
                        				<!-- // Group END -->
                        			</div>
                        			<div class="col-md-2">
                        				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
                        			</div>
                    			</form>
                    		</div>
                    	</div>
                    </div>
					<!-- Nuevo ROW-->
					<div class="row row-app">
                        <div class="col-md-12">
                            <div class="well" id="espacio_tabla">

                            </div>
                        </div>
                    </div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
				    <!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_resultados_gestion.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
