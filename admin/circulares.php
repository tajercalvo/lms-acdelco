<?php include('src/seguridad.php'); ?>
<?php include('controllers/circulares.php');
$location = 'education';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
//$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons envelope"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/circulares.php">Circulares</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="btn-group pull-right">
							<?php if(isset($_GET['id'])){ ?>
								<a href="circulares.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><br><br>
							<?php } ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<?php if(!isset($_GET['id'])){ ?>
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="innerAll spacing-x2">
									<!-- row -->
									<div class="row">
										<div class="col-md-12">
											<div class="widget ">
														<!-- Category Heading -->
														<div class="innerAll half bg-gray border-bottom">
															<?php if($_SESSION['max_rol']>=6){ ?>
																<a  href="#ModalCrearNueva" data-toggle="modal" class="btn btn-xs btn-inverse pull-right"><i class="fa fa-plus fa-fw"></i> Nueva Circular</a>
															<?php } ?>
															<h4 class=" margin-bottom-none">Circulares</h4>
															<div class="clearfix"></div>
														</div>
														<!-- End Category Heading -->
												<?php
												if(isset($Circulares_datos)){
												foreach ($Circulares_datos as $key => $Data_Newsletter) {
												?>
													<!-- Category Listing -->
													<div class="bg-gray-hover overflow-hidden">
														<div class="row innerAll half border-bottom">
															<div class="col-sm-9 col-xs-9">
																<ul class="media-list margin-none">
																	<li class="media">
																	    <a class="pull-left innerAll half " href="#">
																	  		<span class="empty-photo"><i class="fa fa-list-ul fa-2x text-muted"></i></span>
																	    </a>
																	    <div class="media-body">
																		    <div class="innerAll half">
																		    	<h4 class="margin-none">
																		    		<a href="circulares.php?id=<?php echo($Data_Newsletter['newsletter_id']); ?>" class="media-heading strong text-primary"><?php echo($Data_Newsletter['title']); ?></a>
																		    	</h4>
																		  		<div class="clearfix"></div>
																		    	<small class="margin-none"><?php echo(substr($Data_Newsletter['newsletter'],0,250)); ?>...</small>
																	    	</div>
																	    </div>
																	</li>
																</ul>
															</div>
															<div class="col-sm-1 col-xs-1">
																<div class="text-center">
																	<p class="lead strong margin-bottom-none"><?php echo number_format($Data_Newsletter['visits']);?></p>
																	<span class="text-muted">VISITAS</span>
																</div>
															</div>
															<div class="col-sm-2 col-xs-hidden">
																<div class="innerAll half">
																	<div class="media">
																		<a href="circulares.php?id=<?php echo($Data_Newsletter['newsletter_id']); ?>" class="pull-left">
																			<img src="../assets/images/usuarios/<?php echo $Data_Newsletter['img_post']; ?>" style="width:35px;" class="media-object"/>
																		</a>
																		<div class="media-body">
																			<a href="circulares.php?id=<?php echo($Data_Newsletter['newsletter_id']); ?>" class="text-small"><?php echo(substr($Data_Newsletter['first_post'].' '.$Data_Newsletter['last_post'],0,15));?></a>
																			<div class="clearfix"></div>
																			<small>Último Post Publicado</small>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- // END Category Listing -->
												<?php } } ?>
											</div>
											<!-- // END col-separator -->
										</div>
										<!-- // END col -->
									</div>
									<!-- // END row -->
							</div>
						</div>
					<?php } ?>
					<?php if(isset($_GET['id'])){ ?>
					<div class="widget" data-toggle="collapse-widget">
						<div class="widget-head"><h4 class="heading">Detalle</h4></div>
						<div class="widget-body">
							<div class="row">
								<div class="col-md-9">
									<div class="media margin-none" style="padding-left:5px">
										<div class="media-body innerTB" style="padding:0px">
											<h4 style="color:#0058a3"><?php echo($Circular_datos['title']); ?></h4><?php if($Circular_datos['file_data']!=""){ ?></br><h5 class="text-success"><b>Archivo Adjunto:</b> <a href="../assets/gallery/circulares/<?php echo($Circular_datos['file_data']); ?>" target="_blank"><?php echo($Circular_datos['file_data']); ?></a></h5><?php } ?>
											<p class="text-muted"><?php echo $Circular_datos['date_edition']; ?></p>
											<p class="margin-none" style="text-align: justify;"><?php echo($Circular_datos['newsletter']); ?></p>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<?php if($_SESSION['max_rol']>=6){ ?>
										<a href="#ModalEditar" data-toggle="modal" class="btn btn-xs btn-primary pull-right"><i class="fa fa-pencil fa-fw"></i> Editar Circular</a>
									<?php } ?>
								</div>
							</div>
						</div>


					</div>
					<div class="well">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-8">
								<?php if ( isset($Circular_datos['file_data']) && $Circular_datos['file_data'] != '' ): ?>
									<iframe src="../assets/gallery/circulares/<?php echo($Circular_datos['file_data']); ?>" style="width:700px; height:700px;" frameborder="0" id="pdf_view"></iframe>
								<?php endif; ?>
								<!-- <embed src="../assets/gallery/circulares/<?php echo($Circular_datos['file_data']); ?>" width="100%" height="1000"> -->
									<!-- ../assets/gallery/circulares/<?php echo($Circular_datos['file_data']); ?>#toolbar=0  -->
									<!-- #toolbar=0 al final del archivo para que no muestre opciones de pdf  -->
							</div>
							<div class="col-md-2"></div>
						</div>
					</div>
					<?php } ?>
						<!-- Nuevo ROW-->
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->





<?php if(!isset($_GET['id'])){ ?>
	<!-- Modal -->
	<form action="circulares.php" enctype="multipart/form-data" method="post" id="form_Crear"><br><br>
		<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Crear Nueva Circular</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body">
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Título: </label>
							<input type="text" id="title" name="title" class="form-control col-md-8" placeholder="Título de la circular" />
						</div>
						<div class="col-md-1">
							<input type="hidden" id="opcn" value="crear" name="opcn">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Circular: </label>
							<textarea id="newsletter" name="newsletter" class="col-md-12 form-control" rows="5"></textarea>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">EMPRESA: </label>
							<select style="width: 100%;" id="type" name="type">
							<?php foreach ($dealers as $key => $value) {?>
								<option value="<?php echo $value['dealer_id'] ?>" ><?php echo $value['dealer'] ?></option>
							<?php }?>
							</select>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<p>La extensión del archivo debe ser .ZIP o .PDF</p>
							<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
							  	<input type="file" class="text-primary" id="Archivo_new" name="Archivo_new" />
							</div>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Trayectoria </h5>
						<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]">
							<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
								?>
								<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" ><?php echo $Data_ActiveCharge['charge']; ?></option>
							<?php } ?>
			        	</select>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
				</div>
			</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- /.modal -->
<?php } ?>
<?php if(isset($_GET['id'])){ ?>
	<!-- Modal -->
	<form action="circulares.php?id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data" method="post" id="form_Editar"><br><br>
		<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Editar la Circular</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body">
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Titulo: </label>
							<input type="text" id="title_ed" name="title_ed" class="form-control col-md-8" placeholder="Titulo de la Circular" value="<?php echo $Circular_datos['title']; ?>" />
						</div>
						<div class="col-md-1">
							<input type="hidden" id="newsletter_id" value="<?php echo $_GET['id']; ?>" name="newsletter_id">
							<input type="hidden" id="opcn" value="editar" name="opcn">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Descripción:</label>
							<textarea id="newsletter_ed" name="newsletter_ed" class="col-md-12 form-control" rows="5"><?php echo $Circular_datos['newsletter']; ?></textarea>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
							  	<input type="file" class="text-primary" id="Archivo_new" name="Archivo_new" />
							</div>
						</div>
						<div class="col-md-1">
							<input type="hidden" name="Archivo_old" id="Archivo_old" value="<?php echo $Circular_datos['file_data']; ?>">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Estado: </label>
							<select style="width: 100%;" id="estado" name="estado">
								<option value="1" <?php if($Circular_datos['status_id']==1){ ?>selected<?php } ?>>Activo</option>
								<option value="2" <?php if($Circular_datos['status_id']==2){ ?>selected<?php } ?>>Inactivo</option>
							</select>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Cargos </h5>
						<select multiple="multiple" style="width: 100%;" id="cargos_ed" name="cargos_ed[]">
							<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
											$selected = "";
											foreach ($Circular_datos['charges'][0] as $key => $Data_ChargeUser) {
												if($Data_ActiveCharge['charge_id'] == $Data_ChargeUser['charge_id']){
													$selected = "selected";
												}
											}
								?>
								<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php echo $selected; ?> ><?php echo $Data_ActiveCharge['charge']; ?></option>
							<?php } ?>
			        	</select>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
				</div>
			</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- /.modal -->
<?php } ?>






		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/circulares.js"></script>
</body>
</html>
