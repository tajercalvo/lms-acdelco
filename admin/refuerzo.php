<?php include('src/seguridad.php'); ?>
<?php include('controllers/refuerzo.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/refuerzo.php">Preguntas de profundización</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración respuestas de profundización </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-12">
									<h5 style="text-align: justify; ">Encontrará en esta opción los grupos de preguntas de refuerzo que puede responder para obtener un puntaje extra en sus cursos. Estas preguntas de refuerzo estarán disponibles relacionando una nota obtenida y un curso al cual aplica, debe tener en cuenta que para responder estas preguntas de refuerzo deberá tener un puntaje en su curso o evaluación de al menos 80 puntos y haber respondido a tiempo la encuesta de satisfacción. Recuerde que una vez recibida la nota bien sea por parte del profesor o por su evaluación en plataforma (se activa 8 días luego de haber recibido la capacitación) tendrá 48 horas para responder las preguntas de refuerzo, luego de haber respondido su encuesta de satisfacción.<br><br>A continuación se presenta un listado de la mas reciente a la mas antigua de las respondidas o las pendientes, en las respondidas podrá ver una retroalimentación básica en donde se indica que contesto mal, y en las pendientes ud debe ingresar y responder las preguntas para obtener los 10 puntos extra.</h5>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Administrar ítems de lista</h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de registros: <?php echo(count($porRefuerzo)); ?>
							</div>
							<!-- // Total elements END -->
							<!-- Table elements-->
							<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-class="expand" width="400px">CURSO</th>
										<th data-hide="phone,tablet">ALUMNO</th>
										<th data-hide="phone,tablet">DOCUMENTO</th>
										<th data-hide="phone,tablet">FECHA EVALUACION</th>
										<th data-hide="phone,tablet">NOTA</th>
										<th data-hide="phone,tablet"> - </th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($porRefuerzo as $key => $value): ?>
										<tr>
											<td><?php echo $value['course'] ?></td>
											<td><?php echo $value['first_name']." ".$value['last_name'] ?></td>
											<td><?php echo $value['identification'] ?></td>
											<td><?php echo $value['date_creation'] ?></td>
											<td><?php echo $value['score'] ?></td>
											<td><a href="evaluar_r.php?curso=<?php echo $value['course_id'] ?>&module_result_usr_id=<?php echo $value['module_result_usr_id'] ?>" class="btn btn-success btn-xs">Responder</a></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
								<!-- // Table heading END -->
							</table>
							<!-- // Table elements END -->
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/hojas.js"></script>
</body>
</html>
