<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_cupos_programaciones.php');
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=rep_cupos_programaciones.csv');
if(isset($_GET['start_date_day'])){
    echo("INICIA;FINALIZA;MINIMO;MAXIMO;LUGAR;PMINIMO;PMAXIMO;INSCRITOS;CODIGO;CURSO;TIPO;MODULO;DURACION;PROVEEDOR;\n");
    if($cantidad_datos > 0){
        foreach ($datosCursos_all as $iID => $data) {
            echo(utf8_decode($data['start_date'].";".$data['end_date'].";".$data['min_size'].";".$data['max_size'].";".$data['dealer'].";".$data['puntoMin_size'].";".$data['puntoMax_size'].";".$data['inscriptions'].";".$data['newcode'].";".$data['course'].";".$data['type'].";".$data['module'].";".$data['duration_time'].";".$data['supplier']."\n"));
        }
    }
}
