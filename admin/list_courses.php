<?php include('src/seguridad.php'); ?>
<?php
$source = "vid";
//include('controllers/argumentarios.php');
include('controllers/index.php');
$location = 'virtuales';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons display"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/list_courses.php">Cursos Virtuales</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Cursos Virtuales
							<?php if (isset($_GET['id'])) { ?> - <?php echo ($Argumentario_datos['argument']); ?>
								<i class="fa fa-fw fa-thumbs-o-up"></i>
								<h2 style="font-weight:normal;color:#0058a3 !important" id="CantLikes<?php echo ($Argumentario_datos['argument_id']); ?>"><?php echo (number_format($Argumentario_datos['likes'], 0)); ?></h2>
							<?php } ?>
						</h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno margin-none border-none padding-none -->
					<!-- Tabs -->
					<div class="relativeWrap">
						<div class="box-generic">

							<!-- Tabs Heading -->
							<div class="tabsbar tabsbar-2">
								<ul class="row row-merge">
									<li class="col-md-2 active glyphicons blacksmith"><a href="#tab1" data-toggle="tab"><i></i>H Blandas</a></li>
									<li class="col-md-2 glyphicons kiosk"><a href="#tab2" data-toggle="tab"><i></i> <span>H Técnicas</span></a></li>
									<li class="col-md-2 glyphicons check"><a href="#tab3" data-toggle="tab"><i></i> <span>Producto</span></a></li>
									<li class="col-md-2 glyphicons check"><a href="#tab4" data-toggle="tab"><i></i> <span>Ventas</span></a></li>
								</ul>
							</div>
							<!-- // Tabs Heading END -->
							<!-- <?php echo "<pre>";
									print_r($_SESSION);
									echo "</pre>"; ?>   -->

							<?php
							$perfil = '';
							$perfil2 = 3;
							$compara_roles = array("2");
							foreach ($charge_user as $key => $value_dat) {
								if (in_array($value_dat['charge_id'], $compara_roles)) {
									$perfil = $value_dat['charge_id'];
									//print_r($value_dat['charge_id']);
								}
							}
							?>



							<?php if ($perfil === 2 || $perfil2 === 3) { ?>

								<div class="tab-content">

									<!-- Tab content -->
									<div class="tab-pane active" id="tab1">
										<div class="widget widget-heading-simple">
											<div class="widget widget-heading-simple widget-body-simple text-right">
												<form class="input-group">
													<input type="text" id="cur_tecnicas" name="cur_tecnicas" class="form-control" placeholder="Buscar curso en este listado ... " />
													<span class="input-group-btn"><button type="button" onclick="BuscarCursos('tecnicas');" class="btn btn-inverse">Buscar</button></span>
												</form>
											</div>
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body" id="divTecnicas">
														<?php if ($cuenta['inscritos']['1'] > 0) { ?>
															<div class="row">
																<h4>Continua Viendo</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosDisponibleInsc as $iID => $datosCurso_inscritos) {

																	$ct_row++;
																	if ($datosCurso_inscritos['specialty_id'] == 1) {

																?>
																		<div style="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $datosCurso_inscritos['course_id']; ?>" dat-Completo="<?php echo strtoupper($datosCurso_inscritos['course']); ?>" dat-Corto="<?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $datosCurso_inscritos['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $datosCurso_inscritos['course_id']; ?>"><?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $datosCurso_inscritos['course_id']; ?>"><i class="fa fa-fw fa-youtube-play" style="color: red;"></i> <strong style="color: red;">Continuar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($datosCurso_inscritos['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($datosCurso_inscritos['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['finalizados']['1'] > 0) { ?>
															<div class="row">
																<h4>Finalizados</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosFinalizadoInsc as $iID => $DatosCursos_Finalizado) {
																	$ct_row++;
																	if ($DatosCursos_Finalizado['specialty_id'] == 1) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Finalizado['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Finalizado['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Finalizado['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Finalizado['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Finalizado['course_id']; ?>"><i class="fa fa-fw fa-youtube-play" style="color: red;"></i> <strong style="color: red;">Repasar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Finalizado['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Finalizado['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['disponibles']['1'] > 0) { ?>
															<div class="row" id="DivCursosDisponibles">
																<h4>Disponibles</h4>
																<?php
																$ct_row = 0;
																$valida = false;
																foreach ($DatosDisponibleSinInsc as $iID => $DatosCursos_Dispo) {

																	foreach ($DatosDisponibleInsc as $key2 => $DatosCursos_Insc) {
																		$valida = false;

																		if ($DatosCursos_Dispo['course_id'] == $DatosCursos_Insc['course_id']) {
																			$valida = true;

																			unset($DatosDisponibleInsc[$key]);
																			break;
																		}
																	}

																	if ($valida) {
																		continue;
																	}

																	$ct_row++;
																	if ($DatosCursos_Dispo['specialty_id'] == 1) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Dispo['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Dispo['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Dispo['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Dispo['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Dispo['course_id']; ?>"><i class="fa fa-fw fa-youtube-play" style="color: red;"></i> <strong style="color: red;">Inscribirme</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Dispo['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Dispo['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->

									<!-- Tab content -->
									<div class="tab-pane" id="tab2">
										<div class="widget widget-heading-simple">
											<div class="widget widget-heading-simple widget-body-simple text-right">
												<form class="input-group">
													<input type="text" id="cur_blandas" name="cur_blandas" class="form-control" placeholder="Buscar curso en este listado ... " />
													<span class="input-group-btn"><button type="button" onclick="BuscarCursos('blandas');" class="btn btn-inverse">Buscar</button></span>
												</form>
											</div>
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body" id="divBlandas">
														<?php if ($cuenta['inscritos']['2'] > 0) { ?>
															<div class="row" id="DivCursosRealizados">
																<h4>Continua Viendo</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosDisponibleInsc as $iID => $datosCurso_inscritos) {
																	$ct_row++;
																	if ($datosCurso_inscritos['specialty_id'] == 2) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $datosCurso_inscritos['course_id']; ?>" dat-Completo="<?php echo strtoupper($datosCurso_inscritos['course']); ?>" dat-Corto="<?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $datosCurso_inscritos['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $datosCurso_inscritos['course_id']; ?>"><?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $datosCurso_inscritos['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Continuar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">

																						<div class="ribbon" style="background-color: <?php if ($datosCurso_inscritos['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($datosCurso_inscritos['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['finalizados']['2'] > 0) { ?>
															<div class="row" id="DivCursosFinalizados">
																<h4>Finalizados</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosFinalizadoInsc as $iID => $DatosCursos_Finalizado) {
																	$ct_row++;
																	if ($DatosCursos_Finalizado['specialty_id'] == 2) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Finalizado['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Finalizado['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Finalizado['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Finalizado['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Finalizado['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Repasar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Finalizado['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Finalizado['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['disponibles']['2'] > 0) { ?>
															<div class="row" id="DivCursosDisponibles">
																<h4>Disponibles</h4>
																<?php

																$ct_row = 0;
																foreach ($DatosDisponibleSinInsc as $iID => $DatosCursos_Dispo) {
																	// echo "<pre>";
																	// print_r($DatosCursos_Dispo);
																	// echo "</pre>";
																	$ct_row++;
																	if ($DatosCursos_Dispo['specialty_id'] == 2) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Dispo['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Dispo['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Dispo['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Dispo['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Dispo['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Inscribirme</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Dispo['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Dispo['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->

									<!-- Tab content -->
									<div class="tab-pane" id="tab3">
										<div class="widget widget-heading-simple">
											<div class="widget widget-heading-simple widget-body-simple text-right">
												<form class="input-group">
													<input type="text" id="cur_producto" name="cur_producto" class="form-control" placeholder="Buscar curso en este listado ... " />
													<span class="input-group-btn"><button type="button" onclick="BuscarCursos('producto');" class="btn btn-inverse">Buscar</button></span>
												</form>
											</div>
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body" id="divProducto">
														<?php if ($cuenta['inscritos']['3'] > 0) { ?>
															<div class="row" id="DivCursosRealizados">
																<h4>Continua Viendo</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosDisponibleInsc as $iID => $datosCurso_inscritos) {
																	$ct_row++;
																	if ($datosCurso_inscritos['specialty_id'] == 3) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $datosCurso_inscritos['course_id']; ?>" dat-Completo="<?php echo strtoupper($datosCurso_inscritos['course']); ?>" dat-Corto="<?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $datosCurso_inscritos['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $datosCurso_inscritos['course_id']; ?>"><?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $datosCurso_inscritos['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Continuar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<!-- <div class="ribbon" style="background-color: <?php if ($datosCurso_inscritos['disposicion'] == "Autodirigidos") {
																																				echo ("red");
																																			} else {
																																				echo ("green");
																																			} ?>;font-size: 10px;"><?php echo ($datosCurso_inscritos['disposicion']); ?></div> -->
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['finalizados']['3'] > 0) { ?>
															<div class="row" id="DivCursosFinalizados">
																<h4>Finalizados</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosFinalizadoInsc as $iID => $DatosCursos_Finalizado) {
																	$ct_row++;
																	if ($DatosCursos_Finalizado['specialty_id'] == 3) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Finalizado['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Finalizado['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Finalizado['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Finalizado['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Finalizado['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Repasar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Finalizado['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Finalizado['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['disponibles']['3'] > 0) { ?>
															<div class="row" id="DivCursosDisponibles">
																<h4>Disponibles</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosDisponibleSinInsc as $iID => $DatosCursos_Dispo) {

																	$ct_row++;
																	if ($DatosCursos_Dispo['specialty_id'] == 3) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Dispo['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Dispo['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Dispo['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Dispo['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Dispo['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Inscribirme</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Dispo['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Dispo['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->

									<!-- Tab content -->
									<div class="tab-pane" id="tab4">
										<div class="widget widget-heading-simple">
											<div class="widget widget-heading-simple widget-body-simple text-right">
												<form class="input-group">
													<input type="text" id="cur_marca" name="cur_marca" class="form-control" placeholder="Buscar curso en este listado ... " />
													<span class="input-group-btn"><button type="button" onclick="BuscarCursos('marca');" class="btn btn-inverse">Buscar</button></span>
												</form>
											</div>
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body" id="divMarca">
														<?php if ($cuenta['inscritos']['4'] > 0) { ?>
															<div class="row" id="DivCursosRealizados">
																<h4>Continua Viendo</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosDisponibleInsc as $iID => $datosCurso_inscritos) {
																	$ct_row++;
																	if ($datosCurso_inscritos['specialty_id'] == 4) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $datosCurso_inscritos['course_id']; ?>" dat-Completo="<?php echo strtoupper($datosCurso_inscritos['course']); ?>" dat-Corto="<?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $datosCurso_inscritos['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $datosCurso_inscritos['course_id']; ?>"><?php echo strtoupper(substr($datosCurso_inscritos['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $datosCurso_inscritos['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Continuar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($datosCurso_inscritos['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($datosCurso_inscritos['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['finalizados']['4'] > 0) { ?>
															<div class="row" id="DivCursosFinalizados">
																<h4>Finalizados</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosFinalizadoInsc as $iID => $DatosCursos_Finalizado) {
																	$ct_row++;
																	if ($DatosCursos_Finalizado['specialty_id'] == 4) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Finalizado['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Finalizado['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Finalizado['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Finalizado['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Finalizado['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Finalizado['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Repasar</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Finalizado['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Finalizado['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>

															</div>
														<?php } ?>
														<br>
														<?php if ($cuenta['disponibles']['4'] > 0) { ?>
															<div class="row" id="DivCursosDisponibles">
																<h4>Disponibles</h4>
																<?php
																$ct_row = 0;
																foreach ($DatosDisponibleSinInsc as $iID => $DatosCursos_Dispo) {
																	$ct_row++;
																	if ($DatosCursos_Dispo['specialty_id'] == 4) {
																?>
																		<div class="">
																			<div class="col-md-2 padding-none margin-none DivCourse_Detail" style="box-shadow: 1px 1px 10px #888 !important" dat-Id="<?php echo $DatosCursos_Dispo['course_id']; ?>" dat-Completo="<?php echo strtoupper($DatosCursos_Dispo['course']); ?>" dat-Corto="<?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?>">
																				<div class="box-generic padding-none margin-none overflow-hidden">
																					<div class="relativeWrap overflow-hidden" style="height:150px">
																						<img src="../assets/images/distinciones/<?php echo $DatosCursos_Dispo['image']; ?>" alt="" class="img-responsive padding-none border-none" />
																						<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.9) !important">
																							<div class="media margin-none innerAll">
																								<div class="media-body text-white overflow-hidden">
																									<strong class="" style="font-size: 11px !important;" id="DivTexto_Com<?php echo $DatosCursos_Dispo['course_id']; ?>"><?php echo strtoupper(substr($DatosCursos_Dispo['course'], 0, 28)); ?></strong>
																									<p class="text-small margin-none"><a href="course_detail.php?token=<?php echo (md5("GMAcademy")); ?>&_valSco=<?php echo (md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Dispo['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Inscribirme</strong></a></p>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="ribbon-wrapper">
																						<div class="ribbon" style="background-color: <?php if ($DatosCursos_Dispo['disposicion'] == "Autodirigidos") {
																																			echo ("red");
																																		} else {
																																			echo ("green");
																																		} ?>;font-size: 10px;"><?php echo ($DatosCursos_Dispo['disposicion']); ?></div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }
																} ?>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->

								</div>

							<?php } ?>




						</div>
					</div>
					<!-- // Tabs END -->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/list_courses.js"></script>
</body>

</html>