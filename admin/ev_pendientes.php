aa<head>
	<meta charset="utf-8">
	<style>
		*,*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:62.5%;-webkit-tap-highlight-color:rgba(0,0,0,0)}body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.428571429;color:#333;background-color:#fff}html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}body{margin:0}table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table>thead>tr>th,.table>tbody>tr>th,.table>tfoot>tr>th,.table>thead>tr>td,.table>tbody>tr>td,.table>tfoot>tr>td{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table>thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table>caption+thead>tr:first-child>th,.table>colgroup+thead>tr:first-child>th,.table>thead:first-child>tr:first-child>th,.table>caption+thead>tr:first-child>td,.table>colgroup+thead>tr:first-child>td,.table>thead:first-child>tr:first-child>td{border-top:0}.table>tbody+tbody{border-top:2px solid #ddd}.table
		.table{background-color:#fff}.table-condensed>thead>tr>th,.table-condensed>tbody>tr>th,.table-condensed>tfoot>tr>th,.table-condensed>thead>tr>td,.table-condensed>tbody>tr>td,.table-condensed>tfoot>tr>td{padding:5px}.table-bordered{border:1px
		solid #ddd}.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px
		solid #ddd}.table-bordered>thead>tr>th,.table-bordered>thead>tr>td{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*="col-"]{position:static;display:table-column;float:none}table td[class*="col-"],
		table th[class*="col-"]{display:table-cell;float:none}.table>thead>tr>.active,.table>tbody>tr>.active,.table>tfoot>tr>.active,.table>thead>.active>td,.table>tbody>.active>td,.table>tfoot>.active>td,.table>thead>.active>th,.table>tbody>.active>th,.table>tfoot>.active>th{background-color:#f5f5f5}.table-hover>tbody>tr>.active:hover,.table-hover>tbody>.active:hover>td,.table-hover>tbody>.active:hover>th{background-color:#e8e8e8}.table>thead>tr>.success,.table>tbody>tr>.success,.table>tfoot>tr>.success,.table>thead>.success>td,.table>tbody>.success>td,.table>tfoot>.success>td,.table>thead>.success>th,.table>tbody>.success>th,.table>tfoot>.success>th{background-color:#dff0d8}.table-hover>tbody>tr>.success:hover,.table-hover>tbody>.success:hover>td,.table-hover>tbody>.success:hover>th{background-color:#d0e9c6}.table>thead>tr>.danger,.table>tbody>tr>.danger,.table>tfoot>tr>.danger,.table>thead>.danger>td,.table>tbody>.danger>td,.table>tfoot>.danger>td,.table>thead>.danger>th,.table>tbody>.danger>th,.table>tfoot>.danger>th{background-color:#f2dede}.table-hover>tbody>tr>.danger:hover,.table-hover>tbody>.danger:hover>td,.table-hover>tbody>.danger:hover>th{background-color:#ebcccc}.table>thead>tr>.warning,.table>tbody>tr>.warning,.table>tfoot>tr>.warning,.table>thead>.warning>td,.table>tbody>.warning>td,.table>tfoot>.warning>td,.table>thead>.warning>th,.table>tbody>.warning>th,.table>tfoot>.warning>th{background-color:#fcf8e3}.table-hover>tbody>tr>.warning:hover,.table-hover>tbody>.warning:hover>td,.table-hover>tbody>.warning:hover>th{background-color:#faf2cc}@media (max-width: 767px){.table-responsive{width:100%;margin-bottom:15px;overflow-x:scroll;overflow-y:hidden;border:1px
		solid #ddd;-ms-overflow-style:-ms-autohiding-scrollbar;-webkit-overflow-scrolling:touch}.table-responsive>.table{margin-bottom:0}.table-responsive>.table>thead>tr>th,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tfoot>tr>td{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>thead>tr>th:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child{border-left:0}.table-responsive>.table-bordered>thead>tr>th:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>td{border-bottom:0}}.well{min-height:20px;padding:19px;margin-bottom:20px;background-color:#f5f5f5;border:1px
		solid #e3e3e3;border-radius:4px;-webkit-box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05);box-shadow:inset 0 1px 1px rgba(0, 0, 0, 0.05)}
	</style>
</head>

<?php
include_once('src/seguridad.php');
include_once('../config/init_db.php');
include_once('controllers/email.php');

$email = new EmailLudus();

$fecha = date('Y-m-j H:i:s');
// $nuevafecha_8 = strtotime('-11 day',strtotime($fecha));
// $nuevafecha_7 = strtotime('-8 day',strtotime($fecha));
$nuevafecha_8 = date_create(date('Y-m-j H:i:s'));
$nuevafecha_7 = date_create(date('Y-m-j H:i:s'));

date_add($nuevafecha_8, date_interval_create_from_date_string('-12 days'));
$nuevafecha_8 = date_format($nuevafecha_8, 'Y-m-d');
date_add($nuevafecha_7, date_interval_create_from_date_string('-8 days'));
$nuevafecha_7 = date_format($nuevafecha_7, 'Y-m-d');


$query_pendientes = "SELECT r.review, mru.module_id, mru.user_id , u.first_name, u.last_name, u.email,
    mru.module_result_usr_id, i.invitation_id, i.status_id, s.start_date, s.schedule_id, h.headquarter, h.dealer_id,
    wu.first_name AS teacher_f_name, wu.last_name AS teacher_ls_name
    FROM ludus_schedule s, ludus_invitation i, ludus_modules_results_usr mru, ludus_users u, ludus_modules m,
    ludus_reviews_courses rc, ludus_reviews r, ludus_headquarters h, ludus_users wu
    WHERE s.schedule_id = i.schedule_id
        AND i.invitation_id = mru.invitation_id
        AND mru.user_id = u.user_id
        AND u.headquarter_id = h.headquarter_id
        AND mru.module_id = m.module_id
        AND m.course_id = rc.course_id
        AND rc.review_id = r.review_id
        AND s.teacher_id = wu.user_id
        AND i.status_id = 2
        AND s.end_date BETWEEN '$nuevafecha_8' AND '$nuevafecha_7'
       	AND mru.module_result_usr_id NOT IN ( SELECT rw.module_result_usr_id FROM ludus_reviews_answer rw WHERE rw.review_id NOT IN ( 62, 63, 64 ) )";
$pendientes = DB::query( $query_pendientes );

// echo $query_pendientes."<br>";

$query_lideres = "SELECT d.dealer_id, u.first_name, u.last_name, u.email, u.headquarter_id, c.charge, d.dealer
        FROM ludus_users u, ludus_headquarters h, ludus_charges_users cu, ludus_charges c, ludus_dealers d
        WHERE u.headquarter_id = h.headquarter_id AND u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND h.dealer_id = d.dealer_id
        AND c.charge_id = 128 AND u.status_id = 1 AND cu.status_id = 1 AND d.status_id = 1 ORDER BY d.dealer, u.last_name";

$lideres = DB::query( $query_lideres );

//recorre cada uno de los lideres de concesionario
foreach ($lideres as $key => $lid) {
    $destinos = "";
    $cuenta = 0;
    $titulo = "Sus estudiantes tienen evaluaciones pendientes";
    $descripcion = "Sus estudiantes tienen evaluaciones pendientes: ".$lid['dealer'];
    $para = $lid['email'];
    // $para = "a.vega@luduscolombia.com.co, diegolamprea@gmail.com, zoraya.ordonez@gm.com";
    $mensaje_cuerpo = '<div style="background-color: white;">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Participante</th>
                                        <th>Correo</th>
                                        <th>Evaluación</th>
                                        <th>Instructor</th>
                                        <th>F. curso</th>
                                        <th>Sede</th>
                                    </tr>
                                </thead>
                                <tbody class="prueba">';
    foreach ($pendientes as $key => $valPend) {
        if ( $lid['dealer_id'] == $valPend['dealer_id'] ){
            $cuenta++;
            $mensaje_cuerpo .= "<tr>";
            $mensaje_cuerpo .= "<td>".$valPend['first_name']." ".$valPend['last_name']."</td>";
            $mensaje_cuerpo .= "<td>".$valPend['email']."</td>";
            $mensaje_cuerpo .= "<td>".$valPend['review']."</td>";
            $mensaje_cuerpo .= "<td>".$valPend['teacher_f_name']." ".$valPend['teacher_ls_name']."</td>";
            $mensaje_cuerpo .= "<td>".$valPend['start_date']."</td>";
            $mensaje_cuerpo .= "<td>".$valPend['headquarter']."</td>";
            $mensaje_cuerpo .= "</tr>";

        }
    }//for que separa y lista las diferentes schedule
    $mensaje_cuerpo .= "</tbody>
                    </table>
                </div><br>";


    if( $cuenta > 0 ){
        // echo( "titulo: $titulo <br>" );
        // echo( "para: $para <br>" );
        echo( $mensaje_cuerpo );
    	$resultado = $email->enviaNewEmail($titulo,$para.",zoraya@gmacademy.co, zoraya.ordonez@gm.com, leidy.rojas@gm.com",$mensaje_cuerpo,$destinos, "Evaluación GMAcademy <evaluaciones@gmacademy.co>", $descripcion);
        // break;
    }

    // break;
}//for que separa por lider GMAcademy

//============================================================================
// Envio de correos a cada uno de los estudiantes pendientes por evaluación
//============================================================================

$l_usuarios = [];
foreach ($pendientes as $key => $value) {
    $cuenta = 0;
    foreach ($l_usuarios as $key2 => $v2['user_id']) {
        if( $v2['user_id'] == $value['user_id'] ){
            $cuenta++;
        }
    }//fin foreach $l_usuarios
    if( $cuenta == 0 ){
        $l_usuarios[] =["user_id" => $value['user_id'], "email" => $value['email'] ];
    }
}//fin agrupacion usuarios


//agrupa a cada usuario
foreach ($l_usuarios as $key => $valUser) {
    $destinos = "";
    $cuenta = 0;
    $titulo = "Tienes evaluaciones pendientes";
    $descripcion = "No has presentado las siguientes evaluaciones";
    $para = $valUser['email'];
    // $para = "a.vega@luduscolombia.com.co, diegolamprea@gmail.com, zoraya.ordonez@gm.com";
    $mensaje_cuerpo = '<div style="background-color: white;">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Evaluación</th>
                                        <th>Instructor</th>
                                        <th>F. curso</th>
                                    </tr>
                                </thead>
                                <tbody class="prueba">';
    foreach ($pendientes as $key => $valPend) {
        if ( $valUser['user_id'] == $valPend['user_id'] ){
            $cuenta++;
            $mensaje_cuerpo .= "<tr>";
            $mensaje_cuerpo .= "<td>".$valPend['review']."</td>";
            $mensaje_cuerpo .= "<td>".$valPend['teacher_f_name']." ".$valPend['teacher_ls_name']."</td>";
            $mensaje_cuerpo .= "<td>".$valPend['start_date']."</td>";
            $mensaje_cuerpo .= "</tr>";

        }
    }//for que separa y lista las diferentes schedule
    $mensaje_cuerpo .= "</tbody>
                    </table>
                </div><br>";


    if( $cuenta > 0 ){
        // echo( "titulo: $titulo <br>" );
        // echo( "para: $para <br>" );
        echo( $mensaje_cuerpo );
    	$resultado = $email->enviaNewEmail($titulo,$para,$mensaje_cuerpo,$destinos, "Evaluaciones GMAcademy <evaluaciones@gmacademy.co>", $descripcion);
    }

    // break;
}//foreach

?>
