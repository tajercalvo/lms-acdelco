<?php include('src/seguridad.php'); ?>
<?php include('controllers/asignaciones.php');
$location = 'education';
$qtip = 'qtip';
$locData = true;
// die();
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons keynote"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/asignaciones.php">Asignación de Estudiantes</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Asignación de Estudiantes, Empresa: <strong class="text-primary"><?php echo $_SESSION['dealer']; ?></strong></h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-12">
									<h5 style="text-align: justify; ">Aquí encuentra las opciones para asignar un estudiante o varios a las sesiones de educación presencial disponibles.<br></h5>
									<button type="button" data-toggle="print" class="btn btn-default print hidden-print"><i class="fa fa-fw fa-print"></i> Imprimir Listado</button>
									<?php  if ( $datoSchedule['type'] == 'MFR'|| $datoSchedule['type'] == 'ILT' && $datoSchedule['finalizado'] == 0 )  { ?>
									<button id="Cerrar_MFR" type="button" class="btn btn-danger"><i class="fa fa-fw fa-check"></i> Cerrar asignación y ejecutar actividades</button>
									<?php }?>
								</div>
							</div>
							<div class="separator bottom"></div>
							<div class="row">
								<div class="col-md-8">
									<?php echo ("<strong class='text-primary'>CURSO: " . $datoSchedule['newcode'] . '</strong> - ' . $datoSchedule['course'] . "<br><strong class='text-primary'>MÓDULO: </strong> " . $datoSchedule['module'] . "<br><strong class='text-primary'>FECHA: </strong> " . $datoSchedule['start_date'] . " - " . $datoSchedule['end_date'] . "<br><strong class='text-primary'>UBICACIÓN:</strong> " . $datoSchedule['living'] . "<br><strong class='text-primary center'>CIUDAD: </strong><strong>" . $datoSchedule['area'] . "</strong>"); ?>
									<?php
									@session_start();
									if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 4) { ?>
										</br>
										</br>
										<!-- Group -->

										<div class="col-md-12">
											<form method="post" action="op_asignaciones.php?schedule_id=<?php echo $_GET['schedule_id']; ?>">
												<label class="control-label text-primary" for="dealer_id">EMPRESA:</label>
												<select style="width: 100%;" id="dealer_id" name="dealer_id" onchange="this.form.submit();">
													<?php foreach ($listadosConcesionarios as $key => $Data_listados) { ?>
														<option value="<?php echo ($Data_listados['dealer_id']); ?>" <?php if (isset($_POST['dealer_id']) && $_POST['dealer_id'] == $Data_listados['dealer_id']) { ?>selected="selected" <?php } ?>><?php echo ($Data_listados['dealer']); ?></option>
													<?php } ?>
												</select>
											</form>
										</div>

										<!-- // Group END -->
									<?php } else { ?>
										<input type="hidden" name="dealer_id" id="dealer_id" value="<?php echo $_SESSION['dealer_id']; ?>">
									<?php } ?>
								</div>
								<div class="col-md-4">
									<strong class="text-primary">Trayectorias para asignación: </strong><br>
									<?php foreach ($cargosSchedule as $iID => $dataCargos) { ?>
										<?php echo ("<i class='fa fa-fw icon-identification'></i>" . $dataCargos['charge'] . "<br>"); ?>
									<?php } ?>
									<br>
									<br>
									<?php $fecha_max = substr($datoSchedule['max_inscription_date'], 0, 4) . "" . substr($datoSchedule['max_inscription_date'], 5, 2) . "" . substr($datoSchedule['max_inscription_date'], 8, 2); ?>
									<?php if ($cantidad_usuarios > 0) { ?>
										<div class="ajax-loading hide" id="loading_asignacion">
											<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
										</div>
										<?php if ($fecha_max >= date("Ymd") || $_SESSION['max_rol'] >= 6) { ?>
											<button type="button" class="btn btn-success hidden-print" id="EnvResults"><i class="fa fa-check-circle"></i> Guardar Asignación</button>
										<?php } ?>
										<!-- // Table elements END -->
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<?php if ($datoSchedule['type'] == "OJT") { ?>
						<div class="widget widget-heading-simple widget-body-white" id="ResultadosSesiones">
							<!-- Widget heading -->
							<div class="widget-head">
								<h4 class="heading glyphicons list"><i></i> Curso OJT, no requiere inscripciones </h4>
							</div>
						</div>
					<?php } else { ?>
						<?php if (isset($cantidad_usuarios)) { ?>
							<div class="widget widget-heading-simple widget-body-white" id="ResultadosSesiones">
								<!-- Widget heading -->
								<div class="widget-head">
									<h4 class="heading glyphicons list"><i></i> Alumnos disponibles: </h4>
								</div>
								<!-- // Widget heading END -->
								<div class="widget-body widget-employees">
									<!-- Total elements-->
									<ul class="team">
										<li style="border: solid 1px rgba( 0, 0, 0, 0.1 );">
											<span class="crt" style="color: black"><?php echo ($cantidad_usuarios); ?></span>
											<span class="strong text-primary">Total Registros</span>
											<span class="muted"> &nbsp;</span>
										</li>
										<li style="border: solid 1px rgba( 0, 0, 0, 0.1 );">
											<span class="crt" style="color: black" id="Cant_sel"> <?php echo count($listadoInscritos); ?> </span>
											<span class="strong text-success">Seleccionados</span>
											<span class="muted"> &nbsp;</span>
										</li>
										<li style="border: solid 1px rgba( 0, 0, 0, 0.1 );">
											<span class="crt" style="color: black" id="Cant_cap"><?php echo ($DatosSchedule_Dealer['max_size'] - count($listadoInscritos)); ?></span>
											<span class="strong text-primary">Cupos</span>
											<span class="muted">Disponibles</span>
										</li>
									</ul>
									<div class="form-inline separator bottom small" style="margin-left: 5px">
										<input type="hidden" name="Cant_capOriginal" id="Cant_capOriginal" value="<?php echo ($DatosSchedule_Dealer['max_size']); ?>">
										<!-- <span class="text-success">Total de registros: </span> | <span class="text-danger">Seleccionados: <span id="Cant_sel"><?php echo count($listadoInscritos); //$datoSchedule['inscriptions']; 
																																									?></span></span> | <span class="text-primary">Capacidad: <span id="Cant_cap"><?php echo ($DatosSchedule_Dealer['max_size'] - count($listadoInscritos)); ?></span> (Cupos)</span>  -->
										<span class="text-inverse"> <i class="icon-user-2 innerR text-primary"></i> Inscripción hasta: <strong><?php echo substr($datoSchedule['max_inscription_date'], 0, 10); ?></strong></span><?php if ($fecha_max < date("Ymd")) { ?><span class="text-danger"> <strong> VENCIDA !</strong></span><?php } ?>
										<?php
										$var_ctrl = @$DatosSchedule_Dealer['max_size'] - count($listadoInscritos);
										?>
										<div class="row">
											<div class="col-md-10">
												<div class="input-group">
													<input class="form-control" id="busqueda_pal" name="busqueda_pal" type="text" placeholder="Busqueda" value="" />
													<span class="input-group-addon">
														<i class="fa fa-search"></i>
													</span>
												</div>
											</div>
											<div class="col-md-2">
												<button type="button" id="boton_filtro" class="btn btn-primary pull-rigth">Buscar <i class="fa fa-search"></i></button>
												<input type="hidden" id="dealer_id" name="dealer_id" value="<?php echo isset($_POST['dealer_id']) ? $_POST['dealer_id'] : (isset($_SESSION['dealer_id']) ? $_SESSION['dealer_id'] : ''); ?>">
												<input type="hidden" id="schedule_id" name="schedule_id" value="<?php echo $_GET['schedule_id']; ?>">
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-10">
												<div class="input-group">
													<input class="form-control" id="txt_asignacion" name="txt_asignacion" type="text" value="" />
													<span class="input-group-addon">
														<i class="fa fa-list"></i>
													</span>
												</div>
											</div>
											<div class="col-md-2">
												<button type="button" id="btn_asignacion" class="btn btn-primary pull-rigth">Asignación Masiva</button>
											</div>
										</div>
									</div>
									<!-- // Total elements END -->
									<!-- Table elements-->
									<?php if ($var_ctrl > 0 || @$DatosSchedule_Dealer['inscriptions'] > 0) { ?>
										<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
											<!-- Table heading -->
											<thead>
												<tr>
													<th data-hide="phone,tablet">#</th>
													<th data-hide="phone,tablet">USUARIO</th>
													<th data-hide="phone,tablet">USUARIO</th>
													<th data-hide="phone,tablet">IDENTIFICACION</th>
													<th data-hide="phone,tablet">EMPRESA</th>
													<th data-hide="phone,tablet">CIUDAD</th>
													<th data-hide="phone,tablet">TRAYECTORIA</th>
													<th data-hide="phone,tablet" style="width: 10%;">INVITAR</th>
												</tr>
											</thead>
											<!-- // Table heading END -->
											<tbody id="tablaUsuarios">
												<?php
												$cant_usr = 0;
												if ($cantidad_usuarios > 0) {
													foreach ($datos_Usuarios as $iID => $data) {
														$claseTexto = "";
														$contar = 0;
														foreach ($data['cargos'] as $key => $value) {
															foreach ($cargosSchedule as $cargosKey => $chargesK) {
																if ($value == $chargesK['charge']) {
																	$contar++;
																	break;
																}
															}
															if ($contar > 0) {
																break;
															}
														}
														$claseTexto = $contar > 0 ? "si" : "no";
														$cant_usr++;
												?>
														<tr class="reg_busqueda">
															<td><?php echo ($cant_usr); ?></td>
															<td><?php echo ('<img src="../assets/images/usuarios/' . $data['image'] . '" style="width: 40px;" alt="' . $data['first_name'] . ' ' . $data['last_name'] . '">'); ?></td>
															<td><?php echo ($data['first_name'] . ' ' . $data['last_name']); ?>
																<?php
																$titulo_otr = "";
																$cant_otras = 0;
																$ano_curso = date("Y");
																if (isset($data['otras'])) {
																	foreach ($data['otras'] as $iID => $otras) {
																		$pos = strpos($otras['start_date'], $ano_curso);
																		$startDate_val = $otras['start_date'];
																		if ($pos !== false) {
																			$cant_otras++;
																		}
																		$titulo_otr .= "<i class='fa fa-calendar'></i> Curso: <strong class='text-danger'>" . $otras['course'] . "</strong> | Fecha: <strong class='text-danger'>" . $otras['start_date'] . "</strong> | Lugar: <strong class='text-danger'>" . $otras['living'] . "</strong> <br>";
																	}
																} ?>
																<?php if (isset($data['otras'])) { ?><br><a href="Otr_Asignacion" <?php if ($cant_otras >= 6) { ?>class="text-danger" <?php } ?> onclick="return false;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-calendar"></i> Programado!! Este año: <?php echo ($cant_otras); ?> veces
																	</a><br><?php } ?>
															</td>
															<?php if (isset($data['notas'])) {
																$titulo = "";
																$class_result = "text-danger";
																$fa_result = "fa-thumbs-down";
																foreach ($data['notas'] as $iID => $notas) {
																	if ($notas['approval'] == "SI") {
																		$class_result = "text-success";
																		$fa_result = "fa-thumbs-up";
																		$titulo .= "<i class='fa fa-tasks'></i> Fecha: <strong class='text-success'>" . $notas['date_creation'] . "</strong> | Aprobó: <strong class='text-success'>" . $notas['approval'] . "</strong> | Puntaje: <strong class='text-success'>" . $notas['score'] . "</strong><br>";
																	} else {
																		$titulo .= "<i class='fa fa-tasks'></i> Fecha: <strong class='text-danger'>" . $notas['date_creation'] . "</strong> | Aprobó: <strong class='text-danger'>" . $notas['approval'] . "</strong> | Puntaje: <strong class='text-danger'>" . $notas['score'] . "</strong><br>";
																	}
																}
															} ?>
															<td align="center"><?php if (isset($data['notas'])) { ?>
																	<a href="Historico" class="<?php echo ($class_result); ?>" onclick="return false;" title="<?php echo $titulo; ?>"><i class="fa <?php echo ($fa_result); ?>"></i> Ya Cursado!!</a><br>
																<?php } ?>
																<?php echo ($data['identification']); ?>
															</td>
															<td><?php echo ($data['headquarter']); ?></td>
															<td <?php if ($datoSchedule['area'] != $data['area']) { ?> style="color: red;" <?php } else { ?>style="color: green;" <?php } ?>><?php echo ($data['area']); ?></td>
															<td><?php
																foreach ($data['cargos'] as $iID => $cargos) {
																	$claseSpan = $claseTexto == "si" ? "" : "text-danger";
																	echo ("<i class='fa fa-fw icon-identification'></i> <span class='" . $claseSpan . "'>" . $cargos . '</span><br>');
																}
																?></td>
															<td>
																<input type="hidden" name="id" class="id" value="<?php echo $data['user_id']; ?>">
																<div class="make-switch" id="Sw_ChkAsig<?php echo $data['user_id']; ?>" data-on="<?php if ($datoSchedule['area'] == $data['area'] && $claseTexto == "si") { ?>success<?php } else { ?>danger<?php } ?>" data-off="default">
																	<input id="ChkAsig<?php echo $data['user_id']; ?>" type="checkbox" value="SI" <?php if ($data['YaenInvitacion'] > 0) { ?>checked="checked" <?php } ?> onchange="CheckAsigna('#ChkAsig<?php echo $data['user_id']; ?>',<?php echo $data['user_id']; ?>);">
																</div>
															</td>
														</tr>
												<?php }
												} ?>
											</tbody>
										</table>
									<?php } ?>
								</div>
							</div>
						<?php } else { ?>
							<div class="widget widget-heading-simple widget-body-white" id="ResultadosSesiones">
								<!-- Widget heading -->
								<div class="widget-head">
									<h4 class="heading glyphicons list"><i></i> Alumnos disponibles:</h4>
								</div>
								<!-- // Widget heading END -->
								<div class="widget-body">
									<!-- Total elements-->

									<div class="form-inline separator bottom small">
										Total de registros: 0 - No hay alumnos para asignar
									</div>
									<!-- // Total elements END -->
								</div>
							</div>
						<?php } ?>
					<?php } ?>
					<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/asignaciones.js?varRand=<?php echo (rand(10, 999999)); ?>"></script>
</body>

</html>