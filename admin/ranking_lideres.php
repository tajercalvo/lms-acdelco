<?php include('src/seguridad.php'); ?>
<?php 
if(!isset($_GET['id'])){
	$_GET['id'] = $_SESSION['idUsuario'];
}
include('controllers/club_lideres.php');
$location = 'tutor';
$RankingLid = true;

$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
<!-- Modal HTML -->
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="ranking_lideres.php" class="glyphicons cup"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Ranking Club de líderes</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
				<!-- Sección club de líderes -->


<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
<!-- Las modales -->
    <div id="RankingMarzo_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMarzo_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMarzo_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMarzo_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMarzo_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMarzo_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMarzo_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMarzo_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMarzo_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMarzo_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMarzo_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMarzo_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMarzo_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMarzo_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMarzo_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMarzo_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMarzo_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMarzo_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMarzo_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMarzo_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMarzo_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingFebrero_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingFebrero_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingFebrero_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingFebrero_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingFebrero_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingFebrero_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingFebrero_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingFebrero_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingFebrero_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingFebrero_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingFebrero_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingFebrero_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingFebrero_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingFebrero_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingFebrero_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingFebrero_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingFebrero_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingFebrero_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingFebrero_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingFebrero_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingFebrero_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAbril_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
    	            <a href="../assets/gallery/rankingClubLideres/RankingAbril_56_1.png" target="_blank">
    	            	<img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAbril_56_1.png" alt="Ranking 1" />
    	            </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAbril_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
    	            <a href="../assets/gallery/rankingClubLideres/RankingAbril_56_2.png" target="_blank">
    	            	<img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAbril_56_2.png" alt="Ranking 1" />
    	            </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAbril_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAbril_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAbril_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAbril_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAbril_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAbril_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAbril_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAbril_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAbril_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAbril_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAbril_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAbril_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAbril_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAbril_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAbril_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMayo_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMayo_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMayo_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMayo_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMayo_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMayo_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMayo_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMayo_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMayo_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMayo_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMayo_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMayo_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMayo_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMayo_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMayo_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMayo_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMayo_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMayo_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingMayo_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingMayo_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingMayo_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingEnero_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
    	            <a href="../assets/gallery/rankingClubLideres/RankingEnero_56_1.png" target="_blank">
    	            	<img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingEnero_56_1.png" alt="Ranking 1" />
    	            </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingEnero_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
    	            <a href="../assets/gallery/rankingClubLideres/RankingEnero_56_2.png" target="_blank">
    	            	<img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingEnero_56_2.png" alt="Ranking 1" />
    	            </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingEnero_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingEnero_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingEnero_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingEnero_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingEnero_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingEnero_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingEnero_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingEnero_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingEnero_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingEnero_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingEnero_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingEnero_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingEnero_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingEnero_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingEnero_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJunio_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJunio_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJunio_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJunio_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJunio_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJunio_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJunio_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJunio_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJunio_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJunio_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJunio_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJunio_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJunio_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJunio_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJunio_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJunio_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJunio_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJunio_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJunio_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJunio_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJunio_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJulio_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJulio_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJulio_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJulio_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJulio_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJulio_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJulio_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJulio_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJulio_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJulio_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJulio_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJulio_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJulio_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJulio_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJulio_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJulio_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJulio_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJulio_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingJulio_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingJulio_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingJulio_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAgosto_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAgosto_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAgosto_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAgosto_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAgosto_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAgosto_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAgosto_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAgosto_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAgosto_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAgosto_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAgosto_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAgosto_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAgosto_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAgosto_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAgosto_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAgosto_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAgosto_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAgosto_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingAgosto_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingAgosto_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingAgosto_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingSeptiembre_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingSeptiembre_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingSeptiembre_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingSeptiembre_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingSeptiembre_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingSeptiembre_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingSeptiembre_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingSeptiembre_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingSeptiembre_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingSeptiembre_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingSeptiembre_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingSeptiembre_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingSeptiembre_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingSeptiembre_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingOctubre_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingOctubre_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingOctubre_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingOctubre_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingOctubre_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingOctubre_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingOctubre_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingOctubre_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingOctubre_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingOctubre_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingOctubre_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingOctubre_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingOctubre_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingOctubre_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingOctubre_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingOctubre_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingOctubre_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingOctubre_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingOctubre_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingOctubre_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingOctubre_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingNoviembre_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingNoviembre_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingNoviembre_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingNoviembre_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingNoviembre_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingNoviembre_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingNoviembre_56_3" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingNoviembre_56_3.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingNoviembre_56_3.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingNoviembre_56_4" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingNoviembre_56_4.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingNoviembre_56_4.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingNoviembre_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingNoviembre_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingNoviembre_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingNoviembre_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingNoviembre_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingNoviembre_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingNoviembre_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingNoviembre_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingNoviembre_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingDiciembre_56_1" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingDiciembre_56_1.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingDiciembre_56_1.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingDiciembre_56_2" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingDiciembre_56_2.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingDiciembre_56_2.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingDiciembre_79" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingDiciembre_79.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingDiciembre_79.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingDiciembre_1014" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingDiciembre_1014.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingDiciembre_1014.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="RankingDiciembre_15" class="modal fade">
        <div class="modal-dialog" style="left: -120px;">
            <div class="modal-content" style="width:955px;height:660px;top:20px;overflow-y: scroll;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="TituloVideo">Ranking Club de Líderes</h4>
                </div>
                <div class="modal-body">
                    <a href="../assets/gallery/rankingClubLideres/RankingDiciembre_15.png" target="_blank">
                        <img style="width:930px" src="../assets/gallery/rankingClubLideres/RankingDiciembre_15.png" alt="Ranking 1" />
                    </a>
                </div>
            </div>
        </div>
    </div>
<!-- Las modales -->
	<div class="widget-body padding-none border-none">
		<div class="row">
			<div class="col-md-1 detailsWrapper">
			</div>
			<div class="col-md-10 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
				<div class="innerAll">
					<div class="body">
						<div class="row padding">
							<div class="col-md-12">
								<div class="row">
									
									<div class="col-md-3 center">
										<img align="center" src="../assets/gallery/banners/introClubLideres.png" style="width:300px;" alt="" class="img-responsive padding-none border-none" />
									</div>
									<div class="col-md-9">
										<p style="font-size: 25px;"><b>Nuestros Puntos</b></p>
										<p style="text-align:justify">Estos son los resultados mensuales de cada una de las categorías de nuestros Asesores Comerciales, ellos están compitiendo por uno de los 160 cupos disponibles para nuestra gran Convención Anual y el Reconocimiento de ser el mejor.</p>
										<p style="font-size: 15px;text-align:justify"><em><b>Ranking Trimestral:</b>Conoce el Ranking Trimestral y entérate si eres uno de los ganadores que se están acercando a la gran convención.</em></p>

										<p>
	<!-- Tabs -->
	<div class="relativeWrap" >
		<div class="widget widget-tabs widget-tabs-gray">
		
			<!-- Tabs Heading -->
			<div class="widget-head">
				<ul>
					<li><a class="glyphicons cup" href="#tab-1" data-toggle="tab"><i></i>Primer Trimestre</a></li>
					<li><a class="glyphicons cup" href="#tab-2" data-toggle="tab"><i></i>Segundo Trimestre</a></li>
					<li class="active"><a class="glyphicons cup" href="#tab-3" data-toggle="tab"><i></i>Tercer Trimestre</a></li>
                    <!--<li><a class="glyphicons cup" href="#tab-4" data-toggle="tab"><i></i>Cuarto Trimestre</a></li>-->
				</ul>
			</div>
			<!-- // Tabs Heading END -->
			
			<div class="widget-body">
				<div class="tab-content">
				
					<!-- Tab content -->
					<div class="tab-pane box-generic" id="tab-1" style="min-height: 350px;max-height: 350px;overflow-y: scroll;">
						<img align="center" src="../assets/gallery/club_lideres/ganadores2016.png" style="width:40%;" alt="" class="img-responsive padding-none border-none" />
						<p><em>Tu esfuerzo y talento en ventas te han hecho sobresalir en la carrera de los mejores. <b>¡Sigue con más empeño para que la recompensa sea mayor!</b></em></p>
						<p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO A</b></p>
						<p><img align="center" src="../assets/gallery/club_lideres/Q1A.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
						<p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO B</b></p>
						<p><img align="center" src="../assets/gallery/club_lideres/Q1B.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
						<p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO C</b></p>
						<p><img align="center" src="../assets/gallery/club_lideres/Q1C.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
						<p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO D</b></p>
						<p><img align="center" src="../assets/gallery/club_lideres/Q1D.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
						<p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO E</b></p>
						<p><img align="center" src="../assets/gallery/club_lideres/Q1E.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
						<p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GERENTES</b></p>
						<p align="center"><img align="center" src="../assets/gallery/club_lideres/Q1GER.png" style="width:60%;" alt="" class="img-responsive padding-none border-none" /></p>
					</div>
					<!-- // Tab content END -->
					
					<!-- Tab content -->
					<div class="tab-pane box-generic" id="tab-2" style="min-height: 350px;max-height: 350px;overflow-y: scroll;">
						<img align="center" src="../assets/gallery/club_lideres/ganadores2016_2.png" style="width:40%;" alt="" class="img-responsive padding-none border-none" />
                        <p><em>Tu esfuerzo y talento en ventas te han hecho sobresalir en la carrera de los mejores. <b>¡Sigue con más empeño para que la recompensa sea mayor!</b></em></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO A</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q2A.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO B</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q2B.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO C</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q2C.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO D</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q2D.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO E</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q2E.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GERENTES</b></p>
                        <p align="center"><img align="center" src="../assets/gallery/club_lideres/Q2GER.png" style="width:60%;" alt="" class="img-responsive padding-none border-none" /></p>
					</div>
					<!-- // Tab content END -->

					<!-- Tab content -->
					<div class="tab-pane box-generic active" id="tab-3" style="min-height: 350px;max-height: 350px;overflow-y: scroll;">
						<img align="center" src="../assets/gallery/club_lideres/ganadores2016_3.png" style="width:40%;" alt="" class="img-responsive padding-none border-none" />
                        <p><em>Tu esfuerzo y talento en ventas te han hecho sobresalir en la carrera de los mejores. <b>¡Sigue con más empeño para que la recompensa sea mayor!</b></em></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO A</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q3A.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO B</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q3B.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO C</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q3C.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO D</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q3D.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GRUPO E</b></p>
                        <p><img align="center" src="../assets/gallery/club_lideres/Q3E.png" style="width:100%;" alt="" class="img-responsive padding-none border-none" /></p>
                        <p style="border-bottom: 1px solid #414042;color: #013C9E;"><b>GERENTES</b></p>
                        <p align="center"><img align="center" src="../assets/gallery/club_lideres/Q3GER.png" style="width:60%;" alt="" class="img-responsive padding-none border-none" /></p>
					</div>
					<!-- // Tab content END -->

                    <!-- Tab content -->
                    <!--<div class="tab-pane box-generic" id="tab-3" style="min-height: 350px;max-height: 350px;overflow-y: scroll;">
                        <p><em>Aún no tenemos Ranking en esta etapa, recuerda que tu esfuerzo, conocimiento en producto y las mejores ventas te llevarán a la Gran Convención de Asesores.</em></p>
                    </div>-->
                    <!-- // Tab content END -->
				</div>
			</div>
		</div>
	</div>
	<!-- // Tabs END -->
										</p>
									</div>
									
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-1 center">
									</div>
									<div class="col-md-10">
										<p style="font-size: 18px;"><b>Ranking Mensual:</b></p>
										<p style="text-align:justify">Si quieres conocer como vas en el Ranking del Club de Líderes, descarga la tabla correspondiente al mes y anímate a seguir adelante con tus puntos, así lograras llegar a la cima e ir a nuestra Convención.</p>
									</div>
									<div class="col-md-1 center">
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-1 center">
									</div>
									<div class="col-md-10 center widget-body-gray">
										<div class="row">
<div class="col-md-3 center">
	<!-- Collapsible Widget (closed by default) -->
	<div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
		<div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Enero" data-toggle="collapse"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Enero</b></h4></div>
		<div class="widget-body" id="WC_Enero">
			<p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Enero. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
			<div class="row"><a data-toggle="modal" href="#RankingEnero_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingEnero_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingEnero_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingEnero_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingEnero_79">Ranking 7 y 9 Matrículas</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingEnero_1014">Ranking 10 y 14 Matrículas</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingEnero_15">Ranking 15 o más Matrículas</a></div>
		</div>
	</div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
	<!-- Collapsible Widget (closed by default) -->
	<div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
		<div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Febrero" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Febrero</b></h4></div>
		<div class="widget-body" id="WC_Febrero">
			<p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Febrero. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
			<div class="row"><a data-toggle="modal" href="#RankingFebrero_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingFebrero_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingFebrero_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingFebrero_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingFebrero_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingFebrero_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingFebrero_15">Ranking 15 o más Matrículas</a></div>
		</div>
	</div>
	<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
	<!-- Collapsible Widget (closed by default) -->
	<div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
		<div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Marzo" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Marzo</b></h4></div>
		<div class="widget-body" id="WC_Marzo">
			<p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Marzo. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
			<div class="row"><a data-toggle="modal" href="#RankingMarzo_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMarzo_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMarzo_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMarzo_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMarzo_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMarzo_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMarzo_15">Ranking 15 o más Matrículas</a></div>
		</div>
	</div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
	<!-- Collapsible Widget (closed by default) -->
	<div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
		<div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Abril" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Abril</b></h4></div>
		<div class="widget-body" id="WC_Abril">
			<p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Abril. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
			<div class="row"><a data-toggle="modal" href="#RankingAbril_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingAbril_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingAbril_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingAbril_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingAbril_79">Ranking 7 y 9 Matrículas</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingAbril_1014">Ranking 10 y 14 Matrículas</a></div>
			<div class="row"><a data-toggle="modal" href="#RankingAbril_15">Ranking 15 o más Matrículas</a></div>
		</div>
	</div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Mayo" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Mayo</b></h4></div>
        <div class="widget-body" id="WC_Mayo">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Mayo. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingMayo_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMayo_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMayo_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMayo_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMayo_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMayo_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingMayo_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Junio" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Junio</b></h4></div>
        <div class="widget-body" id="WC_Junio">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Junio. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingJunio_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJunio_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJunio_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJunio_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJunio_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJunio_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJunio_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Julio" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Julio</b></h4></div>
        <div class="widget-body" id="WC_Julio">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Julio. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingJulio_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJulio_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJulio_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJulio_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJulio_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJulio_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingJulio_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Agosto" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Agosto</b></h4></div>
        <div class="widget-body" id="WC_Agosto">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Agosto. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingAgosto_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingAgosto_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingAgosto_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingAgosto_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingAgosto_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingAgosto_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingAgosto_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Septiembre" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Septiembre</b></h4></div>
        <div class="widget-body" id="WC_Septiembre">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Septiembre. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingSeptiembre_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingSeptiembre_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingSeptiembre_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingSeptiembre_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingSeptiembre_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingSeptiembre_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingSeptiembre_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Octubre" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Octubre</b></h4></div>
        <div class="widget-body" id="WC_Octubre">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Octubre. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingOctubre_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingOctubre_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingOctubre_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingOctubre_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingOctubre_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingOctubre_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingOctubre_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Noviembre" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Noviembre</b></h4></div>
        <div class="widget-body" id="WC_Noviembre">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Noviembre. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingNoviembre_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingNoviembre_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingNoviembre_56_3">Ranking 5 y 6 Matrículas Tabla 3</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingNoviembre_56_4">Ranking 5 y 6 Matrículas Tabla 4</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingNoviembre_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingNoviembre_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingNoviembre_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
<div class="col-md-3 center">
    <!-- Collapsible Widget (closed by default) -->
    <div class="widget" data-toggle="collapse-widget" data-collapse-closed="true">
        <div class="widget-head"><h4 class="heading text-primary" data-target="#WC_Diciembre" data-toggle="collapse" class="heading text-primary"><img src="../assets/gallery/asesor.png" width="24px"> <b>Ranking Diciembre</b></h4></div>
        <div class="widget-body" id="WC_Diciembre">
            <p style="text-align:justify">Consulta las categorías del Ranking Club de Líderes y conoce tu posición para el mes de Diciembre. Recuerda descargar las tablas para poder verlas en detalle y con mejor resolución.</p>
            <div class="row"><a data-toggle="modal" href="#RankingDiciembre_56_1">Ranking 5 y 6 Matrículas Tabla 1</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingDiciembre_56_2">Ranking 5 y 6 Matrículas Tabla 2</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingDiciembre_79">Ranking 7 y 9 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingDiciembre_1014">Ranking 10 y 14 Matrículas</a></div>
            <div class="row"><a data-toggle="modal" href="#RankingDiciembre_15">Ranking 15 o más Matrículas</a></div>
        </div>
    </div>
<!-- // Collapsible Widget (closed by default) END -->
</div>
										</div>
									</div>
									<div class="col-md-1 center">
									</div>
								</div>
							</div>
						</div>
						<div class="separator bottom"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




				<!-- Sección club de líderes -->
				</div>	
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/index.js"></script>
</body>
</html>