<?php include('src/seguridad.php'); ?>
<?php include('controllers/asignaturas.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons link"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/asignaturas.php">Configuración Asignaturas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Asignaturas </h2>
						<!--<div class="btn-group pull-right">
								<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
								<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
								<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-9">
									<h5 style="text-align: justify; ">A continuación encontrará las opciones necesarias para administrar las Asignaturas; tenga en cuenta que una vez creado un elemento de lista, este no podrá ser eliminado. Únicamente podrá inactivarse para que no aparezca en el sistema, pero en este listado aparecerá como inactivo.</h5>
								</div>
								<div class="col-md-1">
								</div>
								<div class="col-md-2">
									<?php if($_SESSION['max_rol']>=5){ ?><h5><a href="op_asignatura.php?opcn=nuevo" class="glyphicons no-js circle_plus" ><i></i>Agregar Asignatura</a><h5><?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Administrar ítems de lista</h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de registros: <?php echo($cantidad_datos); ?>
							</div>
							<!-- // Total elements END -->
							<!-- Table elements-->
							<table id="TableData" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-class="expand">ESPECIALIDAD</th>
										<th data-hide="phone,tablet">CREADOR</th>
										<th data-hide="phone,tablet">FECHA CREACIÓN</th>
										<th data-hide="phone,tablet">EDITOR</th>
										<th data-hide="phone,tablet">FECHA EDICIÓN</th>
										<th data-hide="phone,tablet">ESTADO</th>
										<th data-hide="phone,tablet"> - </th>
									</tr>
								</thead>
								<!-- // Table heading END -->
							</table>
							<!-- // Table elements END -->
						</div>
					</div>
					<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/asignaturas.js"></script>
</body>
</html>