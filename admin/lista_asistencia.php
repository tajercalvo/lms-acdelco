<?php
/**
 * Html2Pdf Library - example
 *
 * HTML => PDF converter
 * distributed under the OSL-3.0 License
 *
 * @package   Html2pdf
 * @author    Laurent MINGUET <webmaster@html2pdf.fr>
 * @copyright 2017 Laurent MINGUET
 */
//require_once dirname(__FILE__).'/../vendor/autoload.php';
require_once(dirname(__FILE__).'/src/html2pdf/vendor/autoload.php');

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    ob_start();
    //include dirname(__FILE__).'/res/certificado.php';
    include('lista_asistencia_source.php');
    $content = ob_get_clean();

    $html2pdf = new HTML2PDF('L', 'A3', 'es', true, 'UTF-8', array(5, 10, 5, 5));
    /*$html2pdf->pdf->addTTFfont('https://colombia.academiachevrolet.com/assets/fonts/LouisRegular.ttf', 'TrueTypeUnicode', '', 32);
    
    $html2pdf->pdf->addFont('LouisRegular', '', '../../assets/fonts/LouisRegular.ttf');
    $html2pdf->pdf->SetFont('LouisRegular', '', 14);*/
    
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->pdf->SetAuthor('Ludus Learning Management System');
    $html2pdf->pdf->SetCreator('Ludus Learning Management System');
    $html2pdf->pdf->SetTitle('Listado de Asistencia - AutoTrain');
    $html2pdf->pdf->SetSubject('Listado de Asistencia - AutoTrain');
    $html2pdf->pdf->SetMargins(0, 0, 0, true);
    $html2pdf->pdf->setPrintHeader(false);
    $html2pdf->pdf->SetFooterMargin(0);
    $html2pdf->pdf->setPrintFooter(false);
    $html2pdf->pdf->SetAutoPageBreak(TRUE, 0);
    $html2pdf->pdf->SetLeftMargin(0);
    $html2pdf->pdf->SetTopMargin(0);
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('ListadoAsistencia_AutoTrain.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}
