<?php include('src/seguridad.php'); ?>
<?php
if (!isset($_GET['id'])) {
	if (isset($_SESSION['idUsuario'])) {
		$_GET['id'] = $_SESSION['idUsuario'];
	} else {
		header("location: login");
	}
}
include('controllers/op_actividades.php');
$location = 'biblioteca';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<meta charset="gb18030">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<style type="text/css" media="screen">
	input[type=text],
	input[type=password],
	select,
	textarea {
		border-color: #efefef !important;
		color: #a7a7a7;
	}

	#comentarios {
		text-align: left;
	}
</style>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php">Biblioteca</a></li>
				</ul>
				<!-- inner -->
				<?php if (isset($actividad['orden'])) { ?>
					<div class="innerLR">
						<!-- heading -->
						<!-- // END heading -->
						<?php //print_r($actividad); 
						?>
						<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
							<div class="widget-body padding-none border-none">
								<div class="row">
									<div class="col-md-12 padding-none">
										<h5 class="text-uppercase strong text-primary"><i class="fa fa-book text-regular fa-fw"></i> Curso: <?php echo $actividad['course'] . ' - Módulo: ' . $actividad['course']; ?> </h5>
										<ul class="team">
											<li>
												<span class="crt"> <?php echo $actividad['orden'] ?> </span><span class="strong"><?php echo $actividad['activity']; ?></span><span class="muted"> <?php echo $actividad['instrucciones']; ?> </span>
												<?php if ($actividad['adjunto'] != '') { ?>
													<a href="../assets/actividades/<?php echo $actividad['adjunto']; ?>" target="_blank">Descargar Guía</a>
												<?php } ?>
											</li>
										</ul>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 detailsWrapper">
										<div class="innerAll" style="padding: 0px;">
											<div class="body">
												<div class="row padding">
													<div class="col-md-12">
														<!-- // Inicio cuerpo de la pagina -->
														<span id="module_result_usr_id"><?php echo isset($actividad['module_result_usr_id']) ? $actividad['module_result_usr_id'] : 0; ?></span>

														<?php
														if (@$actividad && !empty(@$actividad['link_video'])) {
														?>
															<div class="row" style="display: flex; align-items: center; justify-content: center; margin: 30px 0;">
																<div class="col-md-8 col-12">
																	
																		<!-- Mostrar como iframe -->
																		<div class="embed-responsive embed-responsive-21by9">
																			<iframe width="100%" height="400px" src="<?php echo trim($actividad['link_video']); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; gyroscope; picture-in-picture" allowfullscreen></iframe> <br>
																		</div>
																	
																	
																	
																</div>
																<br>
															</div>

															<!-- <div class="row">
																<div class="col-md-8 col-12">
																	<a class="btn btn-primary" target="_blank" href="<?php echo trim($actividad['link_video']); ?>">Link video</a>
																</div>
															</div> -->
														<?php }
														?>


														<form id="frm" autocomplete="off" style="max-width: 1000px; margin: 0 auto;">
															<div class="row innerLR">
																<!-- Column -->
																<div class="col-md-4">
																	<!-- Group -->
																	<div class="form-group">
																		<label class="col-md-3 control-label" for="adjunto" style="padding-top:8px;">Adjunto:</label>
																		<!-- accept="application/pdf" -->
																		<div class="col-md-9"><input type="file" accept="application/pdf, .docx, .xlsx, .pptx" class="form-control" id="adjunto" name="adjunto" type="text" /></div>
																	</div>
																	<!-- // Group END -->
																</div>
																<!-- // Column END -->
																<!-- Column -->
																<div class="col-md-6">
																	<?php if ($actividad['archivo'] != '') { ?>
																		<a id="archivo_actual" data-archivo_actual="<?php echo $actividad['archivo'] ?>" href="../assets/actividades_usuarios/<?php echo $actividad['archivo'] ?>" target="_blank">Archivo actual entregado</a>
																	<?php } ?>
																</div>

																<!-- // Column END -->
															</div>
															<br>
															<?php 
															if ($actividad['adjuntos'] == 0) { ?>
																<p style="color: red;">No es obligatorio adjuntar archivos.</p>
															<?php } ?>
															<br>
															<div class="row innerLR">
																<!-- Column -->
																<div class="col-md-12">
																	<!-- Group -->
																	<div class="form-group">
																		<label class="col-md-2 control-label" for="comentarios">Comentarios:</label>
																		<div class="col-md-9">
																			<textarea class="form-control" name="comentarios" id="comentarios" style="text-align: left; text-indent: 0;">
<?php echo $actividad['comentarios']; ?>
</textarea>
																		</div>
																	</div>
																	<!-- // Group END -->
																</div>
																<!-- // Column END -->
															</div>
															<br>
															<div class="row">
																<div class="col-md-6"></div>
																<div class="col-md-1">
																	<button class="btn btn-success form-control" type="submit">Enviar <img id="animacion" style="width: 25px; display:none; background-color: #2572a8;"></button>
																</div>
															</div>
														</form>

														<!-- // Fin cuerpo de la pagina -->
														<!-- // Inicio pie de pagina -->
														<div class="separator bottom"></div>
														<div class="row">
															<div class="col-md-3 center"></div>
															<div class="col-md-6 center">
															</div>
															<div class="col-md-3 center">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- // Fin class widget-heading  -->
						</div>
						<!-- Fin -->
					</div>
				<?php } ?>
				<?php if (!isset($actividad['orden'])) { ?>
					<div style="text-align: center; margin-top: 50px; font-size: 18px;">
						<h1>Esta actividad no se encuentra activa, gracias.</h1>
					</div>
				<?php } ?>

			</div>
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/op_actividades.js?v=<?php echo md5(time()); ?>"></script>
</body>

</html>