<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/trayectoria.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="../assets/css/estilos/trayectorias.css" media="screen" title="no title">
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content"><!-- content -->
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="marca_chevrolet.php" class="glyphicons cars"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Marca - Chevrolet</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR"><!-- innerLR -->
					<!-- // END heading -->
					<!-- Sección club de líderes -->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row"><!-- row-->
								<div class="col-md-1 detailsWrapper"></div>
								<div class="col-md-10 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
									<div class="innerAll">
										<div class="body">
											<div class="row padding">

												<div class="col-md-12">
													<div class="marco">
											      <div class="logochevro"></div>
											      <div class="imagen1" style="background-image: url('../assets/gallery/trayectorias/<?php if($cargo['image'] == ''){ echo('Imagen20.jpg'); }else{ echo($cargo['image']); } ?>');"></div>
											      <div class="distinciones"></div>
											      <div class="fondo1 row">
															<div class="col-md-4">
											      		<div class="row">
																	<div class="col-md-1"></div>
																	<div class="col-md-11">
																		<ul class="">
																			<?php
																			foreach ($lista_trayectorias as $key => $keyTray) {
																				if($keyTray['level_id'] == 3){
																					$color = "text-primary";
																					if( count($cursos_aprobados)>0 ){
																						foreach ( $cursos_aprobados as $key => $kcuraprob ) {
																							if( $kcuraprob['course_id'] == $keyTray['course_id'] ){
																								$color = "text-success";
																								break;
																							}
																						}
																					}
																				?>
																				<li class="text-justify <?php echo($color); ?>" <?php if($color=='text-success'){?>style="color:white;font-weight: bold;"<?php }?>><?php echo($keyTray['newcode'].' | '.$keyTray['course']); ?></li>
																			<?php }//fin if
																		}//fin foreach ?>
																		</ul>
																	</div>
											      		</div>
											      	</div>
															<div class="col-md-4">
																<div class="row">
																	<div class="col-md-1"></div>
																	<div class="col-md-11">
																		<ul class="">
																			<?php
																			foreach ($lista_trayectorias as $key => $keyTray) {
																				if($keyTray['level_id'] == 2){
																					$color = "text-primary";
																					if( count($cursos_aprobados)>0 ){
																						foreach ( $cursos_aprobados as $key => $kcuraprob ) {
																							if( $kcuraprob['course_id'] == $keyTray['course_id'] ){
																								$color = "text-success";
																								break;
																							}
																						}
																					}
																				?>
																				<li class="text-justify <?php echo($color); ?>" <?php if($color=='text-success'){?>style="color:white;font-weight: bold;"<?php }?>><?php echo($keyTray['newcode'].' | '.$keyTray['course']); ?></li>
																			<?php }//fin if
																		}//fin foreach ?>
																		</ul>
																	</div>
																</div>
											      	</div>
															<div class="col-md-4">
																<div class="row">
																	<div class="col-md-1"></div>
																	<div class="col-md-11">
																		<ul class="">
													      			<?php
																			foreach ($lista_trayectorias as $key => $keyTray) {
																				if($keyTray['level_id'] == 1){
																					$color = "text-primary";
																					if( count($cursos_aprobados)>0 ){
																						foreach ( $cursos_aprobados as $key => $kcuraprob ) {
																							if( $kcuraprob['course_id'] == $keyTray['course_id'] ){
																								$color = "text-success";
																								break;
																							}
																						}
																					}
																				?>
																				<li class="text-justify <?php echo($color); ?>" <?php if($color=='text-success'){?>style="color:white;font-weight: bold;"<?php }?>><?php echo($keyTray['newcode'].' | '.$keyTray['course']); ?></li>
																			<?php }//fin if
																		}//fin foreach ?>
																	</ul>
																	</div>
																</div>
											      	</div>
														</div>
														<div class="emergente"></div>
											      <div class="fondo2">
											        <div class="columna-1">
											          <div class="logoacade"></div>
											        </div>
											        <div class="columna-2">
											          <div id="titulo-curso"><strong><?php echo($cargo['charge']); ?></strong></div>
											        </div>
											      </div>
											    </div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div><!-- row-->
						</div>
					</div>
				</div><!-- Fin innerLR -->
				<div class="clearfix"></div>
				<!-- // Sidebar menu & content wrapper END -->

			</div><!-- Fin content -->

		</div>
		<?php include('src/footer.php'); ?>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/trayectorias.js"></script>
</body>
</html>
