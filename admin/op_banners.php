<?php include('src/seguridad.php'); ?>
<?php include('controllers/banners.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons sheriffs_star"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/especialidades.php">Configuración Banners</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Banners </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-white">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
			</div>
			<div class="col-md-2">
				<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
					<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
				<?php }else{ ?>
					<h5><a href="banners.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body innerAll inner-5x">
		<form method="post" id="formActualizarData" autocomplete="off" enctype="multipart/form-data">
			<input type="hidden" id="opcn" name="opcn" value="editar">
			<input type="hidden" name="banner_id" value="<?php echo($registroConfiguracion['banner_id']); ?>">
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="nombre" style="padding-top:8px;">Tipo de Banner: </label>
							<select class="" id="banner_type" name="banner_type" class="col-md-5" style="width: 50%;" <?php if($_GET['opcn']=="ver"){ ?> disable <?php } ?> >
								<?php foreach ($tipoBanner as $key => $tbanner) { ?>
									<option value="<?php echo($tbanner['banner_type_id']); ?>" <?php if($tbanner['banners_type'] == $registroConfiguracion['banners_type']){ ?> selected="selected" <?php } ?> > <?php echo($tbanner['banners_type']); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<!-- // Group END -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="banner_name" style="padding-top:8px;">Nombre del Banner</label>
							<div class="col-md-7"><input class="form-control" id="banner_name" name="banner_name" type="text" placeholder="Nombre del banner" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['banner_name']; ?>"<?php } ?> /></div>
						</div>
						<!-- // Group END -->
					</div>
				</div>
				<!-- // Column END -->
				<!-- Column -->
				<div class="row innerLR">
					<div class="col-md-5">
						<div class="form-group">
							<label class="col-md-5 control-label" for="start_date" style="padding-top:8px;">Fecha Inicial:</label>
							<div class="col-md-7 input-group date">
									<input class="form-control" type="text" id="start_date" name="start_date" placeholder="Inicia el" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['start_date']; ?>"<?php } ?>/>
									<span class="input-group-addon">
										<i class="fa fa-th"></i>
									</span>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label class="col-md-5 control-label" for="end_date" style="padding-top:8px;">Fecha Final:</label>
							<div class="col-md-7 input-group date">
									<input class="form-control" type="text" id="end_date" name="end_date" placeholder="Finaliza el" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['end_date']; ?>"<?php } ?>/>
									<span class="input-group-addon">
										<i class="fa fa-th"></i>
									</span>
							</div>
						</div>
					</div>
				</div>
				<!-- // Column END -->
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row innerLR">
					<div class="col-md-9">
						<div class="col-md-3">
							<label class="control-label" for="cargos" style="padding-top:8px;">Género: </label>
						</div>
						<div class="col-md-5">
							<select style="width: 100%;" id="gender" name="gender">
								<option value="0" <?php if($registroConfiguracion['gender'] == "0"){ ?> selected="selected" <?php }?>>No definido</option>
								<option value="1" <?php if($registroConfiguracion['gender'] == "1"){ ?> selected="selected" <?php }?>>Masculino</option>
								<option value="2" <?php if($registroConfiguracion['gender'] == "2"){ ?> selected="selected" <?php }?>>Femenino</option>
							</select>
						</div>
					</div>
				</div>
				<!-- End Column-->
				<div class="clearfix"><br></div>
				<!-- Column -->
				<div class="row innerLR">
					<div class="col-md-10">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-3 control-label" for="banner_url" style="padding-top:8px;">Ruta del Banner</label>
							<div class="col-md-7"><input class="form-control" id="banner_url" name="banner_url" type="text" placeholder="Ruta del banner" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['banner_url']; ?>"<?php } ?> /></div>
						</div>
						<!-- // Group END -->
					</div>
				</div>
				<!-- // Column END -->
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<!-- Column -->
				<div class="row innerLR">
					<div class="col-md-10">
						<div class="col-md-3">
							<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
									<span class="btn btn-default btn-file">
										<span class="fileupload-new">Seleccionar Imágen</span>
										<span class="fileupload-exists">Cambiar Imágen</span>
										<input type="file" class="margin-none" id="file_name" name="file_name" />
								</span>
									<span class="fileupload-preview"></span>
									<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
							</div>
						</div>
						<div class="col-md-7">
							<input type="hidden" id="imgant" name="imgant" value="<?php echo($registroConfiguracion['file_name']); ?>">
							<img src="../assets/gallery/banners/<?php echo($registroConfiguracion['file_name']); ?>" alt="<?php echo($registroConfiguracion['file_name']);?>" style="width: 150px"/>
						</div>
					</div>
				</div>
				<!-- // Column END -->
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row innerLR">
					<div class="col-md-9">
						<div class="col-md-3">
							<label class="control-label" for="cargos" style="padding-top:8px;">Cargos: </label>
						</div>
						<div class="col-md-5">
							<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]">
								<optgroup label="Activos">
									<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
										$selected = "";
										if(count($cargosSeleccionados>0) ){
											foreach ($cargosSeleccionados as $key => $Data_ChargeUser) {
												if($Data_ActiveCharge['charge_id'] == $Data_ChargeUser){
													$selected = "selected";
												}
											}
										}
										?>
										<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php echo $selected; ?>><?php echo $Data_ActiveCharge['charge']; ?></option>
									<?php } ?>
									<?php if($_SESSION['max_rol']<=6){ ?>
										<?php if(isset($_GET['opcn'])&&($_GET['opcn']=="nuevo")){ ?>
											<option value="123" selected="selected">VOLUNTARIO</option>
											<option value="125" selected="selected">CURSOS LIBRES</option>
										<?php } ?>
									<?php } ?>
								</optgroup>
							</select>
						</div>
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<!-- Listado de concesionarios-->
				<div class="row innerLR">
					<div class="col-md-9">
						<div class="col-md-3">
							<label class="control-label" for="concesionarios" style="padding-top:8px;">Concesionarios: </label>
						</div>
						<div class="col-md-5">
							<select multiple="multiple" style="width: 100%;" id="concesionarios" name="concesionarios[]">
								<optgroup label="Activos">
									<?php foreach ($listaConcesionarios as $key => $Data_Dealers) {
										$selected = "";
										if(count($dealersSeleccionados>0) ){
											foreach ($dealersSeleccionados as $key2 => $Data_DealerUser) {
												if($Data_Dealers['dealer_id'] == $Data_DealerUser){
													$selected = "selected";
												}
											}
										}
										?>
										<option value="<?php echo $Data_Dealers['dealer_id']; ?>" <?php echo $selected; ?>><?php echo $Data_Dealers['dealer']; ?></option>
									<?php } ?>
								</optgroup>
							</select>
						</div>
					</div>
				</div>
				<!-- Fin Listado de concesionarios-->
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<!-- Column -->
				<div class="row innerLR">
					<div class="col-md-5">
						<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
							<div class="col-md-7">
								<select style="width: 100%;" id="status_id" name="status_id">
									<option value="1" <?php if($registroConfiguracion['status_id']=="1"){ ?>selected="selected"<?php } ?>>Activo</option>
									<option value="2" <?php if($registroConfiguracion['status_id']=="2"){ ?>selected="selected"<?php } ?>>Inactivo</option>
								</select>
							</div>
						</div>
						<!-- // Group END -->
						<?php } ?>
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2">
					</div>
					<!-- // Column END -->
				</div>
			<!-- // Row END -->
			<div class="separator"></div>
					<!-- Form actions -->
					<div class="form-actions">
						<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
							<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
						<?php } ?>
						<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
					</div>
					<!-- // Form actions END -->
		</form>
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/banners.js"></script>
</body>
</html>
