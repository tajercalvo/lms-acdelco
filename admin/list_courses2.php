<?php include('src/seguridad.php'); ?>
<?php
$source = "vid";
include('controllers/index.php');
$location = 'virtuales';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/biblioteca2.css" media="screen" title="Cusros virtuales">
	<link rel="stylesheet" href="../assets/components/modules/admin/forms/elements/select2v4/assets/lib/css/select2.css" media="screen" title="biblioteca">
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> -->
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/list_courses.php">Cursos Virtuales</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
						<!--Contenido cursos-->
						<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
							<div class="widget-body padding-none border-none">
								<div class="row">
									<!-- <div class="col-md-1 detailsWrapper"></div> -->
									<div class="col-md-12 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
										<div class="innerAll">
											<div class="body" id="body" style="visibility: hidden;">
												<div class="row">
													<div class="col-md-4"></div>
													<div class="col-md-4">
														<img src="../assets/images/font.png" align="left" border="1" style="width: 83%;">
													</div>
													<div class="col-md-4"></div>
												</div>
												<div class="clearfix"><br></div>
												<div class="row" id="menuPrincipal">
													<div class="col-md-2 text-center padding-none border-right">
														<span data-specialty_id="1"  class="btn btn-colorgm active col-md-12">Habilidades técnicas</span>
													</div>
													<div class="col-md-2 text-center padding-none border-right">
														<span data-specialty_id="2"  class="btn btn-colorgm col-md-12">Habilidades blandas</span>
													</div>
													<div class="col-md-2 text-center padding-none border-right">
														<span data-specialty_id="3" class="btn btn-colorgm col-md-12">Producto</span>
													</div>
													<div class="col-md-2 text-center padding-none border-right">
														<span data-specialty_id="4"  class="btn btn-colorgm col-md-12">Procesos de marca</span>
													</div>
													<div class="col-md-1 text-center padding-none">
													</div>
													<div class="col-md-2 text-center" style="border-bottom: 1px solid;">
														<div class="input-group">
															<div class="input-group-btn"><span><i class="fa fa-search"></i></span></div>
															<?php if ( isset($_SESSION['idUsuario']) && $_SESSION['max_rol'] == 1 && $_SESSION['charge_id'] == 150 ): ?>
																<input id="cursoInvitados" class="form-control" type="text" placeholder="Escribe tu búsqueda" style="display: none;" value="La Poderosa Chevrolet DMAX">
															<?php else: ?>
																<input id="txtSearch" class="form-control" type="text" placeholder="Escribe tu búsqueda" style="border: none;">
															<?php endif; ?>
														</div>
													</div>
													<div class="col-sm-1 text-center">
													</div>
												</div>
												<div class="clearfix"><br></div>
												<div class="row"><!--Cuadro configurado-->
													<div class="col-md-12 box-generic padding-none margin-none">
														<div class="innerAll padding-none">
															<div class="body">
																<div class="row" id="rowUltimo">
																	<div class="col-md-1 padding-none">
																		<span class="btn btn-colorgraybigW col-md-12" style="text-align: right;"><i id="en_proceso" class="fa fa-angle-double-up"></i></span>
																	</div>
																	<div class="col-md-2 padding-none">
																		<!-- <span onclick="consulta('consultaLoultimo', '0', '0');" class="btn btn-colorgraybig col-md-12" style="text-align: left;">HACIENDO</span> -->
																		<span  class="btn btn-colorgraybig col-md-12" style="text-align: left;">En proceso</span>
																	</div>
																	<div class="col-md-9 text-center padding-none">
																	</div>
																</div>
																<div class="row fondoGMOscuro expandir" id="icono_en_proceso">
																	<div class="innerAll">
																		<div class="col-md-6 text-center Mostrar" id="Mostrar-en_proceso">
																			<!-- Codigo jquery -->
																		</div>
																		<div class="col-md-6">
																			  <div class="contenidoVertical" id="contenidoVertical_1" style="overflow: scroll;" data-toggle="Mostrar-en_proceso">
																					<!-- Codigo jquery -->
																				</div> <!-- Fin div contenido vertical -->
																		</div>
																	</div>
																</div> <!-- Fin div ultimo -->
																<div class="row fondoGMMasOscuro">
																	<div class="col-md-12 text-center padding-none">
																		<span class="btn btn-vermas col-md-12" id="verMasUltimo">Ver más <i class="fa fa-plus"> </i></span>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</div>
												<!-- Fin cuadro lo ultimo -->
												<!-- Inicio cuadro Recomendados -->
												<div class="clearfix"><br></div>
												<div class="row"><!--Cuadro configurado-->
													<div class="col-md-12 box-generic padding-none margin-none">
														<div class="innerAll padding-none">
															<div class="body">
																<div class="row" id="rowrecomendados" style=" background: #1552C1 !important;">
																	<div class="col-md-1 padding-none">
																		<span class="btn btn-colorgraybigW col-md-12" style="text-align: right; background: #1552C1 !important;border: none;"><i style=" background: #1552C1 !important;border: none;" id="expandirRecomendado" class="fa fa-angle-double-up"></i></span>
																	</div>
																	<div class="col-md-2 padding-none">
																		<!-- <span style=" background: #1552C1 !important;border: none;" onclick="consulta('consultaRecomendados', '0', '0');" class="btn btn-colorgraybig col-md-12" style="text-align: left;">DISPONIBLE</span> -->
																		<span style=" background: #1552C1 !important;border: none; text-align: left;" class="btn btn-colorgraybig col-md-12">Disponible</span>
																	</div>
																	<div class="col-md-9 text-center padding-none">
																	</div>
																</div>
																<div class="row fondoGMOscuro" id="recomendadoh">
																	<div class="innerAll">
																		<div class="col-md-6 text-center Mostrar" id="Mostrar-disponible">
																		<!-- Codigo jquery -->
																		</div>
																		<div class="col-md-6">
																			  <div class="contenidoVertical" id="contenidoVertical_2" style="overflow: scroll;" data-toggle="Mostrar-disponible">
																			  	<!-- jquery -->
																				</div> <!-- Fin div contenido vertical -->
																		</div>
																	</div>
																</div>
																<div class="row fondoGMMasOscuro">
																	<div class="col-md-12 text-center padding-none">
																		<span class="btn btn-vermas col-md-12" id="verMasRecomendados">Ver más <i class="fa fa-plus"> </i></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div><!-- Fin cuadro recomendados -->
												<!-- Inicio cuadro Lo más visto -->
												<div class="clearfix"><br></div>
												<div class="row"><!--Cuadro configurado-->
													<div class="col-md-12 box-generic padding-none margin-none">
														<div class="innerAll padding-none">
															<div class="body">
																<div class="row" id="rowMasvisto">
																	<div class="col-md-1 padding-none">
																		<span class="btn btn-colorgraybigW col-md-12" style="text-align: right;"><i id="expandirMasvisto" class="fa fa-angle-double-up"></i></span>
																	</div>
																	<div class="col-md-2 padding-none">
																		<span class="btn btn-colorgraybig col-md-12" style="text-align: left;">Finalizados</span>
																	</div>
																	<div class="col-md-9 text-center padding-none">
																	</div>
																</div>
																<div class="row fondoGMOscuro" id="masvistoH">
																	<div class="innerAll">
																		<div class="col-md-6 text-center Mostrar" id="Mostrar-finalizados">
																				<!-- Codigo jquery -->
																		</div>
																		<div class="col-md-6">
																		  <div class="contenidoVertical" id="contenidoVertical_3" style="overflow: scroll;" data-toggle="Mostrar-finalizados">
																		  		<!-- jquery -->
																			</div> <!-- Fin div contenido vertical -->
																		</div>
																	</div>
																</div>
																<div class="row fondoGMMasOscuro">
																	<div class="col-md-12 text-center padding-none">
																		<span class="btn btn-vermas col-md-12" id="verMasVisto">Ver más <i class="fa fa-plus"> </i></span>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</div>
												<!-- Fin cuadro lo mas visto -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- END Contenido cursos-->
						</div> <!-- end div class="innerLR" -->
					</div> <!-- end div id="wrapper" -->
				</div> <!-- end div id="content" -->
				<div class="clearfix"></div>
				<!-- // Sidebar menu & content wrapper END -->
				<?php include('src/footer.php'); ?>
			</div>
			<!-- // Main Container Fluid END -->
			<?php include('src/global.php'); ?>
			<script src="js/list_courses2.js"></script>
		</body>
		</html>
