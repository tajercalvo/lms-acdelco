<?php include('src/seguridad.php'); ?>
<?php include('controllers/cursos.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons briefcase"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/cursos.php">Configuración Cursos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Cursos </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-white">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
			</div>
			<div class="col-md-2">
				<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
					<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
				<?php }else{ ?>
					<h5><a href="cursos.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
				<?php } ?>
			</div>
		</div>	
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body innerAll inner-2x">
		<form id="formulario_data" method="post" enctype="multipart/form-data" autocomplete="off">
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="course" style="padding-top:8px;">Curso:</label>
							<div class="col-md-7"><input class="form-control" id="course" name="course" type="text" placeholder="Nombre del curso" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['course']; ?>"<?php } ?> /></div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<!--<div class="form-group">
							<label class="col-md-5 control-label" for="code" style="padding-top:8px;">Código:</label>
							<div class="col-md-7"><input class="form-control" id="code" name="code" type="text" placeholder="Código del curso" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['code']; ?>"<?php } ?> /></div>
						</div>-->
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2">
					</div>
					<!-- // Column END -->
				</div>
			<!-- // Row END -->
	
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="subject_id" style="padding-top:8px;">Asignatura:</label>
							<div class="col-md-7">
								<select style="width: 100%;" id="subject_id" name="subject_id">
								<?php foreach ($listadosAsignaturas as $key => $Data_listados) { ?>
									<option value="<?php echo($Data_listados['subject_id']); ?>" <?php if(isset($registroConfiguracion['subject_id']) &&$registroConfiguracion['subject_id']==$Data_listados['subject_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_listados['subject']); ?></option>
								<?php } ?>
								</select>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="supplier_id" style="padding-top:8px;">Proveedor:</label>
							<div class="col-md-7">
								<select style="width: 100%;" id="supplier_id" name="supplier_id">
								<?php foreach ($listadosProveedores as $key => $Data_listados) { ?>
									<option value="<?php echo($Data_listados['supplier_id']); ?>" <?php if(isset($registroConfiguracion['supplier_id']) &&$registroConfiguracion['supplier_id']==$Data_listados['supplier_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_listados['supplier']); ?></option>
								<?php } ?>
								</select>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2"></div>
					<!-- // Column END -->
				</div>
			<!-- // Row END -->
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="specialty_id" style="padding-top:8px;">Especialidad:</label>
							<div class="col-md-7">
								<select style="width: 100%;" id="specialty_id" name="specialty_id">
								<?php foreach ($listadosEspecialidades as $key => $Data_listados) { ?>
									<option value="<?php echo($Data_listados['specialty_id']); ?>" <?php if(isset($registroConfiguracion['specialty_id']) &&$registroConfiguracion['specialty_id']==$Data_listados['specialty_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_listados['specialty']); ?></option>
								<?php } ?>
								</select>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-5">
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2">
					</div>
					<!-- // Column END -->
				</div>
			<!-- // Row END -->
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="description" style="padding-top:8px;">Descripción:</label>
							<div class="col-md-7">
								<textarea id="description" name="description" class="col-md-12 form-control" rows="3"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['description']; } ?></textarea>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="prerequisites" style="padding-top:8px;">Notas:</label>
							<div class="col-md-7">
								<textarea id="prerequisites" name="prerequisites" class="col-md-12 form-control" rows="3"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['prerequisites']; } ?></textarea>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2">
					</div>
					<!-- // Column END -->
				</div>

			<!-- // Row END -->
			<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<label class="col-md-5 control-label" for="image" style="padding-top:8px;">Imagen Actual:</label>
						<div class="col-md-7">
							<img id="ImgCurso" src="../assets/images/distinciones/<?php echo $registroConfiguracion['image']; ?>" style="width: 240px;" alt="<?php echo $registroConfiguracion['course']; ?>">
						</div>
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="image_new" style="padding-top:8px;">Imagen:</label>
							<div class="col-md-7">
								<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
								  	<span class="btn btn-default btn-file">
								  		<span class="fileupload-new">Seleccionar</span>
								  		<span class="fileupload-exists">Cambiar Imagen</span>
									  	<input type="file" class="margin-none" id="image_new" name="image_new" />
									</span>
								  	<span class="fileupload-preview"></span>
								  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
								</div>
								<div class="form-group">
							    	<button type="button" class="btn btn-primary" onclick="cargaImagen();"><i class="fa fa-check-circle"></i> Cambiar Imagen</button>
							  	</div>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2">
						<div class="ajax-loading hide" id="loading_imagen">
							<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
						</div>
					</div>
					<!-- // Column END -->
				</div>
			<!-- // Row END -->
			<?php } ?>
			<!-- Row -->
				<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="type" style="padding-top:8px;">Tipo de Curso:</label>
							<div class="col-md-7">
								<select style="width: 100%;" id="type" name="type">
									<option value="WBT" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="WBT"){ ?>selected="selected"<?php } ?>>WBT</option>
									<option value="IBT" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="IBT"){ ?>selected="selected"<?php } ?>>IBT</option>
									<option value="VCT" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="VCT"){ ?>selected="selected"<?php } ?>>VCT</option>
									<option value="OJT" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="OJT"){ ?>selected="selected"<?php } ?>>OJT</option>
								</select>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-5">
						<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
							<div class="col-md-7">
								<select style="width: 100%;" id="status_id" name="status_id">
									<option value="1" <?php if($registroConfiguracion['status_id']=="1"){ ?>selected="selected"<?php } ?>>Activo</option>
									<option value="2" <?php if($registroConfiguracion['status_id']=="2"){ ?>selected="selected"<?php } ?>>Inactivo</option>
								</select>
							</div>
						</div>
						<!-- // Group END -->
						<?php } ?>
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2">
					</div>
					<!-- // Column END -->
				</div>
			<!-- // Row END -->

					<div class="row innerLR">
					<!-- Column -->
					<div class="col-md-5">
						<!-- Group -->
						<div class="form-group">
							<label class="col-md-5 control-label" for="target" style="padding-top:8px;">Objetivos:</label>
							<div class="col-md-7">
								<textarea id="target" name="target" class="col-md-12 form-control" rows="3"><?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['target']; } ?></textarea>
							</div>
						</div>
						<!-- // Group END -->
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-5">
						
					</div>
					<!-- // Column END -->
					<!-- Column -->
					<div class="col-md-2">
					</div>
					<!-- // Column END -->
				</div>		
					

			<div class="separator"></div>
					<!-- Form actions -->
					<div class="form-actions">
						<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
							<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
							<input type="hidden" id="opcn" name="opcn" value="crear">
						<?php }elseif(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
							<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
							<input type="hidden" id="imgant" name="imgant" value="<?php echo $registroConfiguracion['image']; ?>">
							<input type="hidden" id="opcn" name="opcn" value="editar">
							<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
						<?php } ?>
						<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
					</div>
					<!-- // Form actions END -->
		</form>
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/cursos.js"></script>
</body>
</html>