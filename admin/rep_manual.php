<?php
include_once('../config/database.php');
include_once('../config/config.php');
$DataBase_Acciones = new Database();

$query_sql1 = "SELECT DISTINCT u.user_id, u.identification, u.first_name, u.last_name, u.date_birthday, u.date_startgm, u.education, u.phone, u.email, d.dealer, h.headquarter, c.charge, n.newcode, n.course, n.type, n.specialty, n.duration_time, n.score, n.datec, s.start_date, p.identification as idp, p.first_name as fp, p.last_name as lp, s.course_id as cid, i.status_id, ar.area, z.zone, TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) as edad
FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, ludus_users p,
(SELECT mr.user_id, c.newcode, c.course_id, c.course, c.type, s.specialty, m.duration_time, MAX(mr.score) as score, mr.date_creation as datec, mr.invitation_id FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c, ludus_specialties s WHERE mr.module_id = m.module_id AND m.course_id = c.course_id AND mr.date_creation BETWEEN '2016-10-01' AND '2017-02-14' AND c.specialty_id = s.specialty_id AND mr.invitation_id > 0 
	GROUP BY mr.user_id, c.newcode, c.course_id, c.course, c.type, s.specialty, m.duration_time) as n, 
ludus_charges_courses cc, ludus_invitation i, ludus_schedule s, ludus_areas ar, ludus_zone z
WHERE u.status_id = 1 AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND 
u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND 
c.charge_id = cc.charge_id AND cc.course_id = n.course_id AND u.user_id = n.user_id
AND n.invitation_id = i.invitation_id AND i.schedule_id = s.schedule_id AND s.teacher_id = p.user_id
AND h.area_id = ar.area_id AND ar.zone_id = z.zone_id
ORDER BY d.dealer, h.headquarter, u.first_name, u.last_name";
$Rows_config1 = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql1);

$query_sql = "SELECT DISTINCT u.user_id, u.identification, u.first_name, u.date_birthday, u.date_startgm, u.education, u.phone, u.email, u.last_name, d.dealer, h.headquarter, c.charge, n.newcode, n.course, n.type, n.specialty, n.duration_time, n.score, n.datec, ar.area, z.zone, TIMESTAMPDIFF(YEAR, u.date_birthday, CURDATE()) as edad
FROM ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu, ludus_charges c, 
(SELECT mr.user_id, c.newcode, c.course_id, c.course, c.type, s.specialty, m.duration_time, MAX(mr.score) as score, mr.date_creation as datec
	FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c, ludus_specialties s 
	WHERE mr.module_id = m.module_id AND m.course_id = c.course_id AND 
	mr.date_creation BETWEEN '2016-10-01' AND '2017-02-14' AND c.specialty_id = s.specialty_id AND mr.invitation_id = 0
	GROUP BY mr.user_id, c.newcode, c.course_id, c.course, c.type, s.specialty, m.duration_time) as n, ludus_charges_courses cc, ludus_areas ar, ludus_zone z
WHERE u.status_id = 1 AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND 
u.user_id = cu.user_id AND cu.charge_id = c.charge_id AND 
c.charge_id = cc.charge_id AND cc.course_id = n.course_id AND u.user_id = n.user_id
AND h.area_id = ar.area_id AND ar.zone_id = z.zone_id
ORDER BY d.dealer, h.headquarter, u.first_name, u.last_name";
$Rows_config2 = $DataBase_Acciones->SQL_SelectMultipleRows($query_sql);
//filtrar
	/*echo("ENCUESTAID;IDENTIFICATION;NOMBRES;APELLIDOS;CONCESIONARIO;SEDE;FECHAENCUESTA;PREGUNTA;RESPUESTA;FECHACURSO;IDENTIFICACION_PROFE;NOMBRES_PROFE;APELLIDOS_PROFE;CODIDO;CURSO;TIPOCURSO;CIUDAD;ZONA;\n");
	$query_otr = "SELECT r.reviews_answer_id, u.identification, u.first_name, u.last_name, d.dealer, h.headquarter, r.date_creation, q.question, an.answer, s.start_date, p.identification as i, p.first_name as f, p.last_name as l, c.newcode, c.type, c.course, ar.area, z.zone
	FROM ludus_reviews_answer r, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_reviews_answer_detail a, 
	ludus_answers an, ludus_questions q, ludus_modules_results_usr m, ludus_invitation i, ludus_schedule s, ludus_users p,
	ludus_courses c, ludus_areas ar, ludus_zone z
	WHERE r.review_id = 2 AND r.module_result_usr_id > 0 AND r.date_creation BETWEEN '2016-10-01' AND '2017-02-14' AND
	r.user_id = u.user_id AND u.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND r.reviews_answer_id = a.reviews_answer_id 
	AND a.question_id = q.question_id
	AND a.answer_id = an.answer_id
	AND r.module_result_usr_id = m.module_result_usr_id
	AND m.invitation_id = i.invitation_id
	AND i.schedule_id = s.schedule_id
	AND s.teacher_id = p.user_id AND s.course_id = c.course_id
	AND h.area_id = ar.area_id AND ar.zone_id = z.zone_id";
	$Rows_configP = $DataBase_Acciones->SQL_SelectMultipleRows($query_otr);
	unset($DataBase_Acciones);*/
//filtrar

header('Content-Type: text/csv; charset=utf-8');

header('Content-Disposition: attachment; filename=Reporte_Gral.csv');

//header('Content-Disposition: attachment; filename=ReporteEncSat.csv');
/*if(count($Rows_configP)){
	foreach ($Rows_configP as $iID => $data) {
		echo(utf8_decode($data['reviews_answer_id'].";".$data['identification'].";".$data['first_name'].";". $data['last_name'].";". $data['dealer'].";". $data['headquarter'].";". $data['date_creation'].";". $data['question'].";". $data['answer'].";". $data['start_date'].";". $data['i'].";". $data['f'].";". $data['l'].";". $data['newcode'].";". $data['type'].";". $data['course'].";". $data['area'].";". $data['zone']).";\n");
	}
}*/

    echo("IDENTIFICATION;NOMBRES;APELLIDOS;CONCESIONARIO;SEDE;CARGO;CODIGO;CURSO;TIPO;ESPECIALIDAD;DURACION;PUNTAJE;FECHANOTA;FECHACURSO;IDENTIFICACION_PROFE;NOMBRES_PROFE;APELLIDOS_PROFE;ASISTENCIA;CIUDAD;AREA;CUMPLEANOS;FECHAINGRESO;NIVELEDUCACION;TELEFONO;EMAIL;EDAD;\n");
    
    if(count($Rows_config1) > 0){
        foreach ($Rows_config1 as $iID => $data) {
        	$estado_inv = "";
        	if($data['status_id']==1){
				$estado_inv = "Invitado";
			}elseif($data['status_id']==2){
				$estado_inv = "Asitió";
			}else{
				$estado_inv = "No Asitió";
			}
            echo(utf8_decode($data['identification'].";".$data['first_name'].";".$data['last_name'].";".$data['dealer'].";".$data['headquarter'].";".$data['charge'].";".$data['newcode'].";".$data['course'].";".$data['type'].";".$data['specialty'].";".$data['duration_time'].";".$data['score'].";".$data['datec'].";".$data['start_date'].";".$data['idp'].";".$data['fp'].";".$data['lp'].";".$estado_inv.";".$data['area'].";".$data['zone'].";".$data['date_birthday'].";".$data['date_startgm'].";".$data['education'].";".$data['phone'].";".$data['email'].";".$data['edad'].";\n"));
        }
    }
    if(count($Rows_config2) > 0){
        foreach ($Rows_config2 as $iID => $data) {
            echo(utf8_decode($data['identification'].";".$data['first_name'].";".$data['last_name'].";".$data['dealer'].";".$data['headquarter'].";".$data['charge'].";".$data['newcode'].";".$data['course'].";".$data['type'].";".$data['specialty'].";".$data['duration_time'].";".$data['score'].";".$data['datec'].";;;;;;".$data['area'].";".$data['zone'].";".$data['date_birthday'].";".$data['date_startgm'].";".$data['education'].";".$data['phone'].";".$data['email'].";".$data['edad'].";\n"));
        }
    }


