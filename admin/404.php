<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<title>FLAT KIT Template (v2.3.0)</title>

	<!-- Meta -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">


	<!--
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
	<link rel="stylesheet/less" href="../template/assets/less/admin/module.admin.page.error.less" />
	-->

		<!--[if lt IE 9]><link rel="stylesheet" href="../template/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.error.min.css" />

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


<script src="../template/assets/components/library/jquery/jquery.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/jquery/jquery-migrate.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/modernizr/modernizr.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/less-js/less.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v2.3.0"></script>	<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>

</head>
<body class="document-body login">

	<div class="innerAll">
	<div class="error innerAll ">
		<div class="container">
			<div class="well text-center">
				<div class="center text-large innerAll">
					<img class="center text-large innerAll img-responsive" src="../template/assets/images/acdelcoLogo.png" style="width:40%">
				</div>
				<!-- <h1 class="strong innerTB">Oups!</h1> -->
				<!-- <h4 class="innerB"></h4> -->
				<div class="well bg-white text-danger strong ">
					La página a la que intentas acceder no existe, verifica la URL o haz clic en el boton de inicio
				</div>
				<div class="inerAll">
				<a href="index.php" class=" btn btn-primary"><i class="fa fa-home"></i> Inicio</a></li>
				</div>
			</div>
		</div>
	</div>
</div>


	<!-- Global -->
	<script>
	var basePath = '',
		commonPath = '../template/assets/',
		rootPath = '../',
		DEV = false,
		componentsPath = '../template/assets/components/';

	var primaryColor = '#e5412d',
		dangerColor = '#b55151',
		infoColor = '#5cc7dd',
		successColor = '#609450',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';

	var themerPrimaryColor = primaryColor;
	</script>

	<script src="../template/assets/components/library/bootstrap/js/bootstrap.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/breakpoints/breakpoints.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.move/js/jquery.event.move.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.swipe/js/jquery.event.swipe.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/megamenu.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/core.init.js?v=v2.3.0"></script>
</body>
</html>