<?php
include('../config/config.php');
@session_start();
// if( isset($_SESSION['id_evaluacion']) && $_SESSION['id_evaluacion'] > 0 && isset($_SESSION['NameUsuario']) ){
// 	header("location: evaluar.php");
// }else if( isset($_SESSION['_EvalCour_ResultId']) && $_SESSION['_EvalCour_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
// 	header("location: evaluar.php");
// }else if( isset($_SESSION['_EncSat_ResultId']) && $_SESSION['_EncSat_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
// 	echo ('si'); die();
// 	header("location: encuestar.php");
// }else if( isset($_SESSION['_EnCarg_ReviewId']) && $_SESSION['_EnCarg_ReviewId'] > 0 && isset($_SESSION['NameUsuario']) ){
// 	header("location: encuestar_Cargo.php");
// }else if( isset($_SESSION['evl_obligatoria']) && $_SESSION['evl_obligatoria'] > 0 && isset($_SESSION['NameUsuario']) ){
// 	header("location: encuesta_gerentes.php");
// }else
if (isset($_SESSION['NameUsuario'])) {
  if (isset($_SESSION['url_solicitada']) and !strpos($_SESSION['url_solicitada'], 'login')) {
    header("location: " . $_SESSION['url_solicitada']);
  } else {
    header("location: ../admin/");
  }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <?php include('src/tittle.php'); ?>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="icon" type="image/png" href="../template/login/images/acdelcomini.png" />
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/vendor/bootstrap/css/bootstrap.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/vendor/animate/animate.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/vendor/css-hamburgers/hamburgers.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/vendor/animsition/css/animsition.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/vendor/select2/select2.min.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/vendor/daterangepicker/daterangepicker.css">
  <!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../template/login/css/util.css">
  <link rel="stylesheet" type="text/css" href="../template/login/css/main.css">
  <!--===============================================================================================-->
</head>
<style type="text/css">
  /*responsive*/
  @media only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
    .wrap-login100 {
      margin-left: 0px;
    }
  }

  @media only screen and (min-device-width : 375px) and (max-device-width : 667px) {
    .wrap-login100 {
      margin-left: 0px;
    }
  }

  @media only screen and (min-device-width : 414px) and (max-device-width : 736px) {
    .wrap-login100 {
      margin-left: 0px;
    }
  }

  @media only screen and (min-device-width : 320px) and (max-device-width : 568px) {
    .wrap-login100 {
      margin-left: 0px;
    }
  }

  select:focus,
  input:focus {
    outline: none;
  }

  select option {
    color: black !important;
  }

  select option:hover {
    background: red !important;
  }

  /*the container must be positioned relative:*/
  .custom-select {
    position: relative;
    font-family: Arial;
    background: transparent;
  }

  .custom-select select {
    display: none;
    /*hide original SELECT element:*/
  }

  .select-selected {
    background-color: #3f51b5;
  }

  /*style the arrow inside the select element:*/
  .select-selected:after {
    position: absolute;
    content: "";
    top: 14px;
    right: 10px;
    width: 0;
    height: 0;
    border: 6px solid transparent;
    border-color: #fff transparent transparent transparent;
  }

  /*point the arrow upwards when the select box is open (active):*/
  .select-selected.select-arrow-active:after {
    border-color: transparent transparent #fff transparent;
    top: 7px;
  }

  /*style the items (options), including the selected item:*/
  .select-items div,
  .select-selected {
    color: #ffffff;
    padding: 8px 16px;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
    user-select: none;
    background: transparent;
  }

  /*style items (options):*/
  .select-items {
    position: absolute;
    background-color: #3f51b5;
    top: 100%;
    left: 0;
    right: 0;
    z-index: 99;
  }

  /*hide the items when the select box is closed:*/
  .select-hide {
    display: none;
  }

  .select-items div:hover,
  .same-as-selected {
    background-color: rgba(0, 0, 0, 0.1);
  }

  .contacto {
    color: white;
  }

  .select2-container--default .select2-selection--multiple {
    background-color: transparent;
  }

  .select2-container--default.select2-container--focus .select2-selection--multiple {
    border: solid 0px;
  }

  .select2-container--default .select2-selection--multiple {
    border: solid 0px;
  }
</style>

<body>

  <div class="limiter">
    <div class="container-login100" style="background-image: url('../template/login/images/FondoLogin-12.jpg');">
      <div class="wrap-login100 p-b-100">
        <span class="login100-form-title p-b-41">
          <img src="../template/login/images/Logo.png" style="width: 100%;border-radius: 15px;">
        </span>
        <form action="controllers/usuarios.php" method="post" name="login" id="login" class="login100-form validate-form p-b-33 p-t-5">
          <div class="wrap-input100 validate-input" data-validate="Enter username">
            <input class="input100" type="text" name="username" id="username" placeholder="Usuario" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
          </div>
          <br>
          <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" id="password" placeholder="Contraseña" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder="&#xe80f;"></span>
          </div>
          <br><input type="hidden" name="opcn" id="opcn" value="login">
          <!-- <div class="wrap-input100 validate-input" data-validate="Enter password">
						<select class="input100" style="background-color: transparent; border: 0px solid;">
							<option value="">Pais</option>
							<option value="colombia">Chile</option>
							<option value="colombia">Colombia</option>
							<option value="colombia">Paraguay</option>
							<option value="colombia">Uruguay</option>
							<option value="colombia">Peru</option>
							<option value="colombia">Bolivia</option>
						</select>
					</div> -->
          <div class="container-login100-form-btn m-t-32">
            <button class="login100-form-btn">
              Ingresar
            </button>
          </div>
          <?php
          @session_start();
          // $_SESSION["error"] = "";
          if (isset($_SESSION["error"]) && !empty($_SESSION["error"])) { ?>
            <div class="alert alert-warning mt-4" role="alert">
              <?php echo $_SESSION["error"]; ?>
            </div>
          <?php } ?>

          <br>
          <div style="text-align-last: center;"><a id="btn-registrarme" style="color: white;" href="">Registrarme</a></div>
          <div style="padding-top: 20px">
            <p class="contacto">Contáctenos:<a href="mailto:alexander.riscanevo@acdelco.com.co " target="_blank" style="color: #61bcff;"> Click Aquí</a></p>
            <p class="contacto">Soporte Comercial: Fernando Molina</p>
            <p class="contacto">Soporte Técnico: Cesar Monroy </p>

            <!-- <p class="contacto">Email: <a href="mailto:trainingacademy@acdelco.com.co">trainingacademy@acdelco.com.co</a></p> -->
            <!--  <p class="contacto">Celular: <a href="#">+57 313 3812821</a></p>
               <p class="contacto">Website: <a  href="https://autotrain.com.co">autotrain.com.co</a></p> -->
          </div>
        </form>

        <form style="display: none;" method="post" name="frm-registrarme" id="frm-registrarme" class="login100-form validate-form p-b-33 p-t-5">

          <div class="wrap-input100">
            <input class="input100" type="text" name="identification" id="identification" placeholder="Identificación" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder="&#xe822;"></span>
          </div>
          <div class="wrap-input100">
            <input class="input100" type="text" name="first_name" id="first_name" placeholder="Nombres" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
          </div>
          <div class="wrap-input100">
            <input class="input100" type="text" name="last_name" id="last_name" placeholder="Apellidos" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
          </div>
          <div class="wrap-input100">
            <input class="input100" type="email" name="email" id="email" placeholder="Correo" autocomplete="off" required>
            <span class="focus-input100" data-placeholder="&#xe818;"></span>
          </div>
          <div class="wrap-input100">
            <input class="input100" type="text" name="mobile_phone" id="mobile_phone" placeholder="Teléfono" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder="&#xe830;"></span>
          </div>
          <div class="wrap-input100">
            <input class="input100" type="date" name="date_birthday" id="date_birthday" placeholder="Fecha de nacimiento" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder="&#xe844;"></span>
          </div>
          <div class="wrap-input100 custom-select">
            <select name="gender" id="gender" class="input100 required" style="background-color: transparent; border: 0px solid;">
              <option value="">Genero</option>
              <option value="1">Masculino</option>
              <option value="2">Femenino</option>
            </select>
          </div>
          <div class="wrap-input100">
            <input class="input100" type="text" name="Empresa" id="Empresa" placeholder="Empresa" autocomplete="off" required="">
            <span class="focus-input100" data-placeholder=""></span>
          </div>
          <div class="wrap-input100">
            <select name="trayectoria[]" id="trayectoria" multiple class="input100 required" style="background-color: transparent; border: 1px solid;">


            </select>
          </div>
          <div class="wrap-input100 custom-select">
            <select name="pais" id="pais" class="input100 required" style="background-color: transparent; border: 0px solid;" required>
              <option value="">País</option>
              <option value="Colombia">Colombia</option>
              <option value="Venezuela">Venezuela</option>
              <option value="Chile">Chile</option>
              <option value="Perú">Perú</option>
              <option value="Ecuador">Ecuador</option>
              <option value="Brasil">Brasil</option>
              <option value="Argentina">Argentina</option>
              <option value="Bolivia">Bolivia</option>
              <option value="Paraguay">Paraguay</option>
            </select>
          </div>
          <br>
          <div class="form-check">
            <input style="margin-left: 0px;" type="checkbox" class="form-check-input" id="treatment_policy" name="treatment_policy" require>
            <label style="color: white;" class="form-check-label" for="exampleCheck1">Aceptar <a style="color: white; text-decoration:underline;" href="politica_datos.php" target="_blank">Política de tratamiento</a> de datos</label>
          </div>
          <input type="hidden" name="opcn" value="registrarme">
          <div class="container-login100-form-btn m-t-32">
            <button id="registrarme" class="login100-form-btn">
              Registrarme
            </button>
          </div>
          <br>
          <div style="text-align-last: center;"><a id="btn-ingresar" style="color: white;" href="">Ingresar</a></div>
        </form>
      </div>
    </div>
  </div>


  <div id="dropDownSelect1"></div>

  <!--===============================================================================================-->
  <script src="../template/login/vendor/jquery/jquery-3.2.1.min.js"></script>
  <!--===============================================================================================-->
  <script src="../template/login/vendor/animsition/js/animsition.min.js"></script>
  <!--===============================================================================================-->
  <script src="../template/login/vendor/bootstrap/js/popper.js"></script>
  <script src="../template/login/vendor/bootstrap/js/bootstrap.min.js"></script>
  <!--===============================================================================================-->
  <script src="../template/login/vendor/select2/select2.min.js"></script>
  <!--===============================================================================================-->
  <script src="../template/login/vendor/daterangepicker/moment.min.js"></script>
  <script src="../template/login/vendor/daterangepicker/daterangepicker.js"></script>
  <!--===============================================================================================-->
  <script src="../template/login/vendor/countdowntime/countdowntime.js"></script>
  <!--===============================================================================================-->
  <script src="../template/login/js/main.js"></script>
  <script src="../btn-registrarme?v<?php echo md5(time()); ?>"></script>
  <script>
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
      selElmnt = x[i].getElementsByTagName("select")[0];
      /*for each element, create a new DIV that will act as the selected item:*/
      a = document.createElement("DIV");
      a.setAttribute("class", "select-selected");
      a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
      x[i].appendChild(a);
      /*for each element, create a new DIV that will contain the option list:*/
      b = document.createElement("DIV");
      b.setAttribute("class", "select-items select-hide");
      for (j = 1; j < selElmnt.length; j++) {
        /*for each option in the original select element,
        create a new DIV that will act as an option item:*/
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
          /*when an item is clicked, update the original select box,
          and the selected item:*/
          var y, i, k, s, h;
          s = this.parentNode.parentNode.getElementsByTagName("select")[0];
          h = this.parentNode.previousSibling;
          for (i = 0; i < s.length; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
              s.selectedIndex = i;
              h.innerHTML = this.innerHTML;
              y = this.parentNode.getElementsByClassName("same-as-selected");
              for (k = 0; k < y.length; k++) {
                y[k].removeAttribute("class");
              }
              this.setAttribute("class", "same-as-selected");
              break;
            }
          }
          h.click();
        });
        b.appendChild(c);
      }
      x[i].appendChild(b);
      a.addEventListener("click", function(e) {
        /*when the select box is clicked, close any other select boxes,
        and open/close the current select box:*/
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
      });
    }

    function closeAllSelect(elmnt) {
      /*a function that will close all select boxes in the document,
      except the current select box:*/
      var x, y, i, arrNo = [];
      x = document.getElementsByClassName("select-items");
      y = document.getElementsByClassName("select-selected");
      for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
          arrNo.push(i)
        } else {
          y[i].classList.remove("select-arrow-active");
        }
      }
      for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
          x[i].classList.add("select-hide");
        }
      }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
  </script>
</body>
<footer style="background-color: #000000; height: 7%; padding: 1%; ">
  <img src="../template/login/images/chile.png" style="width: 20px; margin: 3px;display: none">
  <img src="../template/login/images/colombia.png" style="width: 20px; margin: 3px;display: none">
  <img src="../template/login/images/paraguay.png" style="width: 20px; margin: 3px;display: none">
  <img src="../template/login/images/uruguay.png" style="width: 20px; margin: 3px;display: none">
  <img src="../template/login/images/peru.png" style="width: 20px; margin: 3px;display: none">
  <img src="../template/login/images/bolivia.png" style="width: 20px; margin: 3px;display: none">
  <!-- <a href="https://luduscolombia.com.co/" target="_blank"><img src="../template/login/images/logoLudus.png" style="width:30px; margin: 3px; float: right;"></a>
	<a href="https://autotrain.com.co/" target="_blank"><img src="../template/login/images/LogoAutoTrain.png" style="width: 150px; margin: 3px; float: right;"></a> -->
</footer>

</html>