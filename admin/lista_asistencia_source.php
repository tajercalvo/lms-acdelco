<?php 
include('src/seguridad.php');
include('controllers/lista_asistencia.php');
?>
<page class="back">
    <style type="text/css">

    	table.tableizer-table {
    		font-size: 11px;
    		border: 1px solid #CCC;
    		font-family: Arial, Helvetica, sans-serif;
    	}
    	.tableizer-table td {
    		border: 1px solid #CCC;
    	}
    	.tableizer-table th {
    		background-color: #FFFFFF;
    		color: #000000;
    		font-weight: bold;
    		border: 1px solid #CCC;
    	}
    </style>
    <table class="tableizer-table" >
        <thead border = "1">
            <tr>
                <th colspan="2" scope="col"><img src="https://acdelco.com.co/wp-content/uploads/2017/09/logo.png" style="width: 200px;  float: left;"></th>
                <th colspan="2" scope="col" align="left">Curso: <?php echo($dato_sesion['course']); ?></th>
                <th colspan="3" scope="col" align="left">Empresa:</th>
                <th colspan="3" scope="col" align="left">Lugar: <?php echo($dato_sesion['living']); ?></th>
            </tr>
            <tr>
                <th colspan="2" scope="col" align="left">Lista de asistencia</th>
                <th colspan="2" scope="col" align="left">Intructor: <?php echo($dato_sesion['first_name'].' '.$dato_sesion['last_name']); ?></th>
                <th scope="col" align="left">Ciudad: <?php echo($dato_sesion['area']); ?></th>
                <th scope="col" align="left">Año: <?php echo(substr($dato_sesion['start_date'], 0, 4)); ?></th>
                <th scope="col" align="left">Mes: <?php echo(substr($dato_sesion['start_date'], 5, 2)); ?></th>
                <th scope="col" align="left">Dia: <?php echo(substr($dato_sesion['start_date'], 8, 2)); ?></th>
                <th colspan="2" scope="col" align="left">Numero de hoja: 1</th>
            </tr>
            <tr bgcolor="#b0b0b0" >
                <td>No.</td>
                <td align="center" WIDTH="200">NOMBRE Y APELLIDO</td>
                <td align="center" WIDTH="100">#IDENTIFICACIÓN</td>
                <td align="center" WIDTH="200">ESPECIALIDAD Y CARGO</td>
                <td align="center" WIDTH="200">SEDE</td>
                <td align="center" WIDTH="50">E.I.</td>
                <td align="center" WIDTH="50" >E.F.</td>
                <td align="center"> DESEMPEÑO</td>
                <td align="center">NOTA FINAL</td>
                <td align="center" WIDTH="300" >FIRMA</td>
            </tr>
        </thead>
        <tbody>
            <?php if(count($datosInvitaciones) > 0){
                    $var_ids = 0;
                    foreach ($datosInvitaciones as $iID => $data) {
                    $var_ids++; ?>
            <tr>
                <td height="30"><?php echo($var_ids); ?></td>
                <td height="30"><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
                <td height="30"><?php echo($data['identification']); ?></td>
                <td height="30"><?php foreach ($data['Cargos'] as $iID => $data_cargos) { echo($data_cargos['charge'].'<br>'); } ?> </td>
                <td height="30"><?php echo($data['headquarter']); ?></td>
                <td height="30"></td>
                <td height="30"></td>
                <td height="30"></td>
                <td height="30"></td>
                <td height="30"></td>
            </tr>
            <?php }
                    }else{ ?>
                        <tr>
                            <td height="30" colspan="10">No hay Alumnos inscrtos para esta sesión</td>
                        </tr>
                    <?php } ?>
        </tbody>
    </table>
</page>