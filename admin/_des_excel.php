<?php
include('src/seguridad.php');
include('../config/config.php');
include('../config/database.php');

$db = new Database();

$query_sql = "SELECT c.course, c.newcode, d.dealer, s.start_date, tu.first_name AS first_teacher, tu.last_name AS last_teacher, sp.specialty, m.type, u.first_name, u.last_name, u.identification, i.status_id
FROM ludus_invitation i, ludus_schedule s, ludus_headquarters h, ludus_dealers d, ludus_users u, ludus_users tu, ludus_courses c, ludus_modules m, ludus_specialties sp
WHERE i.schedule_id = s.schedule_id
AND i.headquarter_id = h.headquarter_id
AND h.dealer_id = d.dealer_id
AND i.user_id = u.user_id
AND s.teacher_id = tu.user_id
AND s.course_id = c.course_id
AND c.course_id = m.course_id
AND c.specialty_id = sp.specialty_id
AND s.status_id = 1
AND s.start_date < NOW()";
$resultSet = $db->SQL_SelectMultipleRows( $query_sql );

header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Asistentes_cursos.xls");
?>
<table>
    <!-- Table heading -->
    <thead>
            <tr>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">CODIGO</th>
                <th data-hide="phone,tablet">CONCESIOANRIO</th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet">INSTRUCTOR</th>
                <th data-hide="phone,tablet">ESPECIALIDAD</th>
                <th data-hide="phone,tablet">TIPO</th>
                <th data-hide="phone,tablet">USUARIO</th>
                <th data-hide="phone,tablet">IDENTIFICACION</th>
                <th data-hide="phone,tablet">ASISTENCIA</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php
            if(count($resultSet) > 0){
                foreach ($resultSet as $iID => $data) {
                    switch ($data['status_id']) {
                        case 1:
                            $val_estado = 'Pendiente';
                            break;
                        case 2:
                            $val_estado = 'Asistió';
                            break;
                        case 3:
                            $val_estado = 'No Asistió';
                            break;

                        default:
                            $val_estado = 'Error registro';
                            break;
                    }
                    ?>
                <tr>
                    <td><?= utf8_decode($data['course']) ?></td>
                    <td><?= utf8_decode($data['newcode']) ?></td>
                    <td><?= utf8_decode($data['dealer']) ?></td>
                    <td><?= utf8_decode($data['start_date']) ?></td>
                    <td><?= utf8_decode($data['first_teacher'])." ".utf8_decode($data['last_teacher']) ?></td>
                    <td><?= utf8_decode($data['specialty']) ?></td>
                    <td><?= utf8_decode($data['type']) ?></td>
                    <td><?= utf8_decode($data['first_name'])." ".utf8_decode($data['last_name']) ?></td>
                    <td><?= utf8_decode($data['identification']) ?></td>
                    <td><?= utf8_decode($val_estado) ?></td>


                </tr>
            <?php } } ?>
        </tbody>
</table>

// header('Content-Type: text/csv; charset=utf-8');
// header("Content-Disposition: attachment; filename=Reporte_Indicadores_Cursos_Virtuales.csv");
//
// echo("EVALUACION;CODIGO;DESCRIPCION;TIEMPO;CANTIDAD PREGUNTAS;ESTADO;PREGUNTA;OPCION;RESPUESTA\n");
// if(count($resultSet) > 0){
//     foreach ($resultSet as $key => $value) {
//         $string = "";
//         $string .= $value['evaluacion'].";";
//         $string .= $value['Codigo'].";";
//         $string .= $value['description'].";";
//         $string .= $value['time'].";";
//         $string .= $value['cant_question'].";";
//         $string .= ($value['status_id'] == 1 ? "Activo" : "Inactivo" ).";";
//         $string .= $value['pregunta'].";";
//         $string .= $value['opcion'].";";
//         $string .= $value['respuesta'].";";
//         $string .= "\n";
//         echo utf8_encode( $string );
//     }
// }
