<?php
if(isset($_POST['opcn']) && $_POST['opcn']=="cargar"){
	include('models/usuarios.php');
	include('src/funciones_globales.php');
	$usuario_Class = new Usuarios();
	$contador = 0;
	//$titulos = array('NOMBRES','APELLIDOS','IDENTIFICACIÓN','EMAIL','TELÉFONO','NACIMIENTO (YYYY-MM-DD)','INGRESO A LA EMPRESA (YYYY-MM-DD)','CARGO CONCESIONARIO','CONTRATACIÓN','GENERO','TIPO USUARIO','TRAYECTORIA', 'NIVEL ACADEMICO');
	$errores = [];
	$archivo_csv = Funciones::guardarCSV('archivo_csv','csv_','../assets/filescsv/');
	if(strpos($archivo_csv, "default") !== false){
		echo("No se pudo cargar el archivo ".$archivo_csv);
	}else{ //fin if -- validacion de que se halla cargado bien el archivo
		$archivo = fopen('../assets/filescsv/'.$archivo_csv, "r");
		while (($data = fgetcsv($archivo, 1000, ";")) == true) {
			$yaRegistrado = [];
			$contador++;
			if($contador > 1){ // omitira los titulos
				@session_start();
				$documento = is_numeric( $data[0] ) ? $data[0] : '' ;
				$n_correo = trim( $data[6] );
				$n_correo = str_replace(' ', '', $n_correo);
				/*
				consulta si el usuario ingresado ya existe
				*/
				if( $documento != "" && $documento > 100000 ){
					$yaRegistrado = $usuario_Class->consultaUsuario($documento);
					//print_r($yaRegistrado);
				}else{
					/*
					Valida que el numero de documento sea valido
					*/
					$errores[] = Array("error"=>"No es un numero de documento valido","linea"=>$contador);
					continue;
				}
				/*
				recorre y valida que todos los campos tengan datos
				*/
				for ( $i=0; $i <= 2 ; $i++ ) {
					if($data[$i] == ""){
						if( $i != 9 ){
							$errores[] = Array("error"=>"No pueden haber campos en blanco","linea"=>$contador." : ".($i+1));
							//echo("campo ".$i);
							continue;
						}

					}
				}
				/*
				Si el correo no esta en blanco, valida que no este registrado con otro usuario
				*/
				if( $n_correo != "" ){

					$conf_correo = $usuario_Class->getCorreo( $n_correo );
					if( isset( $conf_correo['error'] ) ){
						$errores[] = Array( "error"=> $conf_correo['mensaje']." <strong>$n_correo</strong>" ,"linea"=>$contador);
						continue;
					}elseif( isset( $conf_correo['email'] ) && isset( $conf_correo['identification'] ) && $conf_correo['identification'] != $data[0] ){
						$errores[] = Array( "error"=> "El correo ya esta registrado con otro usuario" ,"linea"=>$contador);
						continue;
					}
				}else{
					$n_correo="No tiene";
				}
				$concesionario = $usuario_Class->consultarSede($data[4]);
				if( is_null($concesionario) ){ $errores[] = Array( "error"=> "La sede no existe" ,"linea"=>$contador); continue;  }
				$data[9] = ucfirst(strtolower($data[9]));
				if( ($data[9] == 'Primaria/Secundaria' || $data[9] == 'Tecnico' || $data[9] == 'Tecnologo' || $data[9] == 'Profesional' || $data[9] == 'Posgrado') ){}else{ $errores[] = Array( "error"=> "Nivel educativo no existe" ,"linea"=>$contador); continue;  }
				if($concesionario['dealer_id'] == $_SESSION['dealer_id'] || $_SESSION['max_rol'] >= 5 ){
					// $date_ary = explode('-',$data[11]);
					// if( count( $date_ary ) > 2 && checkdate($date_ary[1], $date_ary[2], $date_ary[0]) ){
					// 	$datenew = date_create($data[11]);
					// 	$fechaNacimiento = date_format($datenew,"Y-m-d");
					// }else{
					// 	$errores[] = Array("error"=>"la fecha debe ser <strong>aaaa-mm-dd</strong> $data[11]","linea"=>$contador);
					// 	continue;
					// }
					// $daten_ary2 = explode('-',$data[12]);
					// if( count( $daten_ary2 ) > 2 && checkdate($daten_ary2[1], $daten_ary2[2], $daten_ary2[0]) ){
					// 	$datenew2 = date_create($data[12]);
					// 	$fechaGM = date_format($datenew2 , "Y-m-d");
					// }else{
					// 	$errores[] = Array("error"=>"la fecha debe ser <strong>aaaa-mm-dd</strong> $data[12]","linea"=>$contador);
					// 	continue;
					// }
					
					$fechaGM = date("Y-m-d")." ".(date("H")).":".date("i:s");
					$fecha = date("Y-m-d")." ".(date("H")).":".date("i:s");
					$data[8] = ucfirst(strtolower($data[8]));
					switch($data[8]){
						case "Masculino": $genero = 1; break;
						case "Femenino": $genero = 2; break;
						default: $genero = 0; break;
					}
					if( count($errores) == 0 && isset($yaRegistrado['user_id']) ){ //actualiza los datos si el usuario ya existe
						$nombre = trim( $data[2] );
						$apellido = trim( $data[1] );

						Usuarios::updateHeadquarterHistory( $concesionario['headquarter_id'], $documento );

						$query = "UPDATE ludus_users SET last_name ='$apellido', first_name = '$nombre', email = '$n_correo', phone = '$data[7]', headquarter_id = {$concesionario['headquarter_id']}, status_id = 1,";
						$query .= "gender = $genero, education = '$data[9]' WHERE identification = '$documento'";
						$utf_query = utf8_encode($query);
						$id = $usuario_Class->actualizarRegistro($utf_query);
						$consulta = $usuario_Class->consultaTipoUsuario($data[5]);
						if( is_null($consulta) ){ $errores[] = Array( "error"=> "La trayectoria no existe" ,"linea"=>$contador); continue;  }
						$cargo = $consulta['charge_id'];
						$user_id = $yaRegistrado['user_id'];
						$delete_query = "DELETE FROM ludus_charges_users WHERE user_id = $user_id";
						$resultado = $usuario_Class->actualizarRegistro($delete_query);
						$NOW_data 	= date("Y-m-d")." ".date("H").":".date("i:s");
						$query_charges = "INSERT INTO ludus_charges_users (charge_id,user_id,date_creation,status_id) VALUES ($cargo,$user_id,'$NOW_data',1)";
						$resultado = $usuario_Class->cargarRegistro($query_charges);

					}elseif( count($errores) == 0 && !isset($yaRegistrado['user_id']) ){ //inserta un nuevo usuario
						$var_key_bd	= 'LdLmsDoorGM_2015*';
						$password = "AES_ENCRYPT('$data[0]','".$var_key_bd."')";
						$nombre = trim( $data[2] );
						$apellido = trim( $data[1] );
						$correo = trim( $data[6] );
						$query = "INSERT INTO ludus_users (last_name,first_name,identification, password,email,phone,date_birthday,date_creation,headquarter_id,status_id,date_startgm,local_charge,gender,education)";
						$query .= " VALUES ('$apellido','$nombre','$data[0]',$password,'$correo','$data[7]','0000-00-00','0000-00-00',{$concesionario['headquarter_id']},1,'$fechaGM','$data[5]',$genero,'$data[9]')";
						$utf_query = utf8_encode($query);
						$id = $usuario_Class->cargarRegistro($utf_query);

						Usuarios::insertHeadquarterHistory( $concesionario['headquarter_id'], $id );

						$consulta = $usuario_Class->consultaTipoUsuario($data[5]);
						$cargo = $consulta['charge_id'];
						$query_charges = "INSERT INTO ludus_charges_users (charge_id,user_id,date_creation,status_id) VALUES ($cargo,$id,'$fecha',1)";
						$resultado = $usuario_Class->cargarRegistro($query_charges);
						$query_rol = "INSERT INTO ludus_roles_users (rol_id,user_id,date_creation,status_id) VALUES (6,$id,'$fecha',1)";
						$resultadoRol = $usuario_Class->cargarRegistro($query_rol);

					}

				}else{ //si el documento del registro se hara para un documento de otra sede no se puede actualizar
					$errores[] = Array("error"=>"Documento del usuario registrado en otra sede","linea"=>$contador);
					continue;
				}
				//unset($yaRegistrado);
			} //fin if(contador > 1)
		} //fin while
		fclose($archivo);
		$resultado = "cantidad de registros: ".($contador-1)."<br>";
		$resultado .= "cantidad de registros <span class='text-primary'>correctos</span>: ".($contador-1-count($errores))."<br>";
		$resultado .= "cantidad de registros <span class='text-danger'>incorrectos</span>: ".count($errores)."<br><br>";
		if(count($errores)>0){
			$resultado .= "<table class='table table-striped'>";
			$resultado .= "<tr><th>error</th><th>linea:campo</th></tr>";
			foreach ($errores as $keyE => $keyEDato) {
				$resultado .= "<tr><td>".$keyEDato['error']."</td><td>".$keyEDato['linea']."</td></tr>";
			}
			$resultado .= "</table>";
		}
		echo($resultado);
	}
	unset($usuario_Class);
}
if(isset($_GET['opcion']) && $_GET['opcion']=="plantilla"){
	//header("Content-disposition: attachment; filename=plantilla.xlsx");
	header("Content-disposition: attachment; filename=plantilla.csv");
	header("Content-type: application/vnd.ms-excel");
	readfile("../assets/filescsv/plantilla.csv");
	//readfile("../assets/filescsv/plantilla.xlsx");
}
?>
