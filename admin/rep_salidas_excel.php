<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['dealer_id'])){
    include('controllers/rep_salidas.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_Salidas.xls");
if(isset($_GET['dealer_id'])){
    ?>
    <table>
        <!-- Table heading -->
            <thead>
                <tr>
                    <?php if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){ ?>
                        <th data-hide="phone,tablet" style="width: 10%;">PAIS</th>
                        <th data-hide="phone,tablet" style="width: 5%;">PROM</th>
                        <th data-hide="phone,tablet" style="width: 5%;">CANT</th>
                        <th data-hide="phone,tablet" style="width: 5%;">HRS</th>
                        <th data-hide="phone,tablet" style="width: 10%;">ZONA</th>
                        <th data-hide="phone,tablet" style="width: 5%;">PROM</th>
                        <th data-hide="phone,tablet" style="width: 5%;">CANT</th>
                        <th data-hide="phone,tablet" style="width: 5%;">HRS</th>
                        <?php } ?>
                    <th data-hide="phone,tablet" style="width: 20%;">CONCESIONARIO</th>
                    <th data-hide="phone,tablet" style="width: 5%;">PROM</th>
                    <th data-hide="phone,tablet" style="width: 5%;">CANT</th>
                    <th data-hide="phone,tablet" style="width: 5%;">HRS</th>
                    <th data-hide="phone,tablet" style="width: 20%;">TRAYECTORIA</th>
                    <th data-hide="phone,tablet" style="width: 5%;">PROM</th>
                    <th data-hide="phone,tablet" style="width: 5%;">CANT</th>
                    <th data-hide="phone,tablet" style="width: 5%;">HRS</th>
                </tr>
            </thead>
            <!-- // Table heading END -->
            <tbody>
                <?php 
                if($cantidad_datos > 0){ 
                    $cantColombia = 0;
                    $hrsColombia = 0;

                    $zona = '';
                    $cantHorasZona = 0;
                    $cantGenteZona = 0;

                    $Concesionario = '';
                    $cantHorasConcesionario = 0;
                    $cantGenteConcesionario = 0;

                    $Zonas = array();
                    $Concesionarios = array();

                    foreach ($datosCursos_all as $iID => $data) { 
                        $cantColombia = $cantColombia + $data['cantidad'];
                        $hrsColombia = $hrsColombia + $data['tiempo'];

                        if($zona != $data['zone']){
                            if($zona!=''){
                                $Zonas[$zona][0] = $cantHorasZona;
                                $Zonas[$zona][1] = $cantGenteZona;
                            }
                            $cantHorasZona = $data['tiempo'];
                            $cantGenteZona = $data['cantidad'];
                            $zona = $data['zone'];
                        }else{
                            $cantHorasZona = $cantHorasZona + $data['tiempo'];
                            $cantGenteZona = $cantGenteZona + $data['cantidad'];
                        }

                        if($Concesionario != $data['dealer']){
                            if($Concesionario!=''){
                                $Concesionarios[$Concesionario][0] = $cantHorasConcesionario;
                                $Concesionarios[$Concesionario][1] = $cantGenteConcesionario;
                            }
                            $cantHorasConcesionario = $data['tiempo'];
                            $cantGenteConcesionario = $data['cantidad'];
                            $Concesionario = $data['dealer'];
                        }else{
                            $cantHorasConcesionario = $cantHorasConcesionario + $data['tiempo'];
                            $cantGenteConcesionario = $cantGenteConcesionario + $data['cantidad'];
                        }
                    }
                    $Zonas[$zona][0] = $cantHorasZona;
                    $Zonas[$zona][1] = $cantGenteZona;

                    $Concesionarios[$Concesionario][0] = $cantHorasConcesionario;
                    $Concesionarios[$Concesionario][1] = $cantGenteConcesionario;
                    //print_r($Zonas);
                    foreach ($datosCursos_all as $iID => $data) { 
                        ?>
                    <tr>
                        <?php if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>5){ ?>
                            <td>Colombia</td>
                            <td align="center"><?php echo number_format( (($hrsColombia/8)/$cantColombia),2 ); ?></td>
                            <td align="center"><?php echo($cantColombia); ?></td>
                            <td align="center"><?php echo($hrsColombia); ?></td>
                            <td><?php echo($data['zone']); ?></td>
                            <td align="center"><?php echo number_format( (($Zonas[$data['zone']][0]/8)/$Zonas[$data['zone']][1]),2 ); ?></td>
                            <td align="center"><?php echo($Zonas[$data['zone']][1]); ?></td>
                            <td align="center"><?php echo($Zonas[$data['zone']][0]); ?></td>
                        <?php } ?>
                        <td><?php echo($data['dealer']); ?></td>
                        <td align="center"><?php echo number_format( (($Concesionarios[$data['dealer']][0]/8)/$Concesionarios[$data['dealer']][1]),2 ); ?></td>
                        <td align="center"><?php echo($Concesionarios[$data['dealer']][1]); ?></td>
                        <td align="center"><?php echo($Concesionarios[$data['dealer']][0]); ?></td>
                        <td><?php echo($data['charge']); ?></td>
                        <td align="center"><?php echo number_format( ($data['tiempo']/8/$data['cantidad']),2 ); ?></td>
                        <td align="center"><?php echo($data['cantidad']); ?></td>
                        <td align="center"><?php echo($data['tiempo']); ?></td>
                    </tr>
                <?php } } ?>
            </tbody>
    </table>
<?php } ?>