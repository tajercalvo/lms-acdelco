<?php include('src/seguridad.php'); ?>
<?php include('controllers/asignaciones.php');
$location = 'tutor';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons train"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/foros_tutor.php">Foro - Tutor GM Academy</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Foro - Tutor GM Academy:</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->

<h3 class="margin-none innerAll content-heading bg-white border-bottom">Cómo comenzar?</h3>


<div class="innerAll spacing-x2">
	<!-- row -->
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<div class="widget">
				<!-- Post List -->
				<div class="media innerAll inner-2x border-bottom margin-none">
					<div class="pull-left media-object" style="width:100px">
						<div class="text-center">
							<a href="#" class="clearfix">
								<img src="../assets//images/people/50/1.jpg" class="rounded-none"/>
							</a>
							<small class="text-small">Miembro Activo</small>
							<div class="innerT half">
								<a href="" class="text-muted"><i class="icon-user-1"></i></a>
								<a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
								<a href="" class="text-muted"><i class="icon-star-fill"></i></a>
							</div>
						</div>
					</div>
					<div class="media-body">
						<small class="pull-right label label-default">15/05/2015 12:45:19</small>
						<h5><a href="" class="text-primary">Diana</a> <span>escribió:</span></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
						<small class="display-block text-muted">
							Escrito por <a href="">por Felipe Heredia</a> | 
							Posteado en <a href="">Información General</a> |
							<span class="text-success"><i class="fa fa-check"></i> Aprobada</span>
						</small>
					</div>
				</div>
				<div class="media innerAll inner-2x border-bottom margin-none">
					<div class="pull-left media-object" style="width:100px">
						<div class="text-center">
							<a href="#" class="clearfix">
								<img src="../assets//images/people/50/2.jpg" class="rounded-none"/>
							</a>
							<small class="text-small">Miembro Activo</small>
							<div class="innerT half">
								<a href="" class="text-muted"><i class="icon-user-1"></i></a>
								<a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
								<a href="" class="text-muted"><i class="icon-star-fill"></i></a>
							</div>
						</div>
					</div>
					<div class="media-body">
						<small class="pull-right label label-default">15/05/2015 12:45:19</small>
						<h5><a href="" class="text-primary">Ricardo</a> <span>escribió:</span></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
						<small class="display-block text-muted">
							Escrito por <a href="">por Felipe Heredia</a> | 
							Posteado en <a href="">Información General</a> |
							<span class="text-success"><i class="fa fa-check"></i> Aprobada</span>
						</small>
					</div>
				</div>
				<div class="media innerAll inner-2x border-bottom margin-none">
					<div class="pull-left media-object" style="width:100px">
						<div class="text-center">
							<a href="#" class="clearfix">
								<img src="../assets//images/people/50/3.jpg" class="rounded-none"/>
							</a>
							<small class="text-small">Miembro Activo</small>
							<div class="innerT half">
								<a href="" class="text-muted"><i class="icon-user-1"></i></a>
								<a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
								<a href="" class="text-muted"><i class="icon-star-fill"></i></a>
							</div>
						</div>
					</div>
					<div class="media-body">
						<small class="pull-right label label-default">15/05/2015 12:45:19</small>
						<h5><a href="" class="text-primary">Alfonso</a> <span>escribió:</span></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
						<small class="display-block text-muted">
							Escrito por <a href="">por Felipe Heredia</a> | 
							Posteado en <a href="">Información General</a> |
							<span class="text-success"><i class="fa fa-check"></i> Aprobada</span>
						</small>
					</div>
				</div>
				<div class="media innerAll inner-2x border-bottom margin-none">
					<div class="pull-left media-object" style="width:100px">
						<div class="text-center">
							<a href="#" class="clearfix">
								<img src="../assets//images/people/50/4.jpg" class="rounded-none"/>
							</a>
							<small class="text-small">Miembro Activo</small>
							<div class="innerT half">
								<a href="" class="text-muted"><i class="icon-user-1"></i></a>
								<a href="" class="text-muted"><i class="icon-trophy-fill"></i></a>
								<a href="" class="text-muted"><i class="icon-star-fill"></i></a>
							</div>
						</div>
					</div>
					<div class="media-body">
						<small class="pull-right label label-default">15/05/2015 12:45:19</small>
						<h5><a href="" class="text-primary">Leidy</a> <span>escribió:</span></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, animi, quis, perferendis unde atque eveniet itaque magnam id error eius rerum amet quibusdam nobis voluptatem vitae aut quos consequuntur impedit quo corporis. Est, atque, accusantium, voluptates, distinctio molestias culpa provident quos in consectetur quo optio voluptate tempora. Ex, veniam in.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, accusamus!</p>
						<small class="display-block text-muted">
							Escrito por <a href="">por Felipe Heredia</a> | 
							Posteado en <a href="">Información General</a> |
							<span class="text-success"><i class="fa fa-check"></i> Aprobada</span>
						</small>
					</div>
				</div>
				<!-- // END Información General Listing -->
				<div class="innerAll text-right">
					<ul class="pagination margin-none">
						<li class="disabled"><a href="#">&laquo;</a></li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">&raquo;</a></li>
					</ul>
				</div>
				<h4 class="innerAll half border-bottom bg-gray margin-none">Responder al Mensaje</h4>
				<form class="text-right innerAll">
					<div class="innerB">
						<textarea name="" id="" cols="30" rows="3" class="form-control rounded-none margin-bottom notebook border-none padding-none" placeholder="Escriba aquí..."></textarea>
					</div>
					<a href="" class="btn btn-primary btn-sm strong">Enviar <i class="fa fa-arrow-right fa-fw"></i></a>
				</form>
				
			</div>
		</div>
		<div class="col-md-3 col-sm-4">
			<div class="widget ">
				<div class="innerAll half border-bottom bg-gray">
					<div class="input-group">
			      		<input type="text" class="form-control" placeholder="Search posts ...">
			      		<span class="input-group-btn">
			        		<button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
			      		</span>
			    	</div>
				</div>
			</div>
			<div class="widget">
				<!-- Heading -->
				<h5 class="innerAll half margin-bottom-none bg-primary"><i class="fa fa-refresh fa-fw"></i> Posts Recientes</h5>
				<!-- Listing -->
				<div class="bg-gray-hover innerAll half border-bottom">
			    	<div class="innerAll half">
			    		<h5 class="innerB half margin-none"><a href="#" class="text-primary">Como consultar los colores disponibles del vehículo?</a></h5>
						<small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> por Felipe Heredia | <i class="fa fa-fw fa-calendar "></i> 17/01/2015</small>
			    	</div>
				</div>
				<div class="bg-gray-hover innerAll half border-bottom">
			    	<div class="innerAll half">
			    		<h5 class="innerB half margin-none"><a href="#" class="text-primary">En donde consulto capacidades por tipo de carrocería?</a></h5>
						<small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> por Felipe Heredia | <i class="fa fa-fw fa-calendar "></i> 17/01/2015</small>
			    	</div>
				</div>
				<div class="bg-gray-hover innerAll half border-bottom">
			    	<div class="innerAll half">
			    		<h5 class="innerB half margin-none"><a href="#" class="text-primary">Como navego en el tutor de GM Academy?</a></h5>
						<small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> por Felipe Heredia | <i class="fa fa-fw fa-calendar "></i> 17/01/2015</small>
			    	</div>
				</div>
				<div class="bg-gray-hover innerAll half border-bottom">
			    	<div class="innerAll half">
			    		<h5 class="innerB half margin-none"><a href="#" class="text-primary">How do you guys change navbar into white?</a></h5>
						<small class="text-center text-inverse text-muted"><i class="fa fa-fw fa-user "></i> por Felipe Heredia | <i class="fa fa-fw fa-calendar "></i> 17/01/2015</small>
			    	</div>
				</div>
				<!-- // END Listing -->
			</div>
		</div>
	</div>
</div>






						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/asignaciones.js"></script>
</body>
</html>