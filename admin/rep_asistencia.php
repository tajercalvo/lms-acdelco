<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_asistencia.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Reporte de Asistencia</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Asistencia </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de los inscritos por cada curso en un programa establecido.</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<h5><a href="rep_asistencia_excel.php?schedule_id=<?php echo($_POST['schedule_id']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<form action="rep_asistencia.php" method="post">
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Sesión:</label>
					<div class="col-md-10">
						<select style="width: 100%;" id="schedule_id" name="schedule_id">
							<?php foreach ($datosCursos as $key => $Data_Cursos) { ?>
								<option value="<?php echo($Data_Cursos['schedule_id']); ?>" <?php if(isset($_POST['schedule_id']) && $_POST['schedule_id']==$Data_Cursos['schedule_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Cursos['city'].' '.$Data_Cursos['start_date'].' '.$Data_Cursos['course']); ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información: </h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<!-- Table elements-->
		<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
			<!-- Table heading -->
			<thead>
				<tr>
					<th data-hide="phone,tablet">CÓDIGO</th>
					<th data-hide="phone,tablet">CURSO</th>
					<th data-hide="phone,tablet">ALUMNO</th>
					<th data-hide="phone,tablet">IDENTIFICACIÓN</th>
					<th data-hide="phone,tablet">CONCESIONARIO</th>
					<th data-hide="phone,tablet">FECHA</th>
					<th data-hide="phone,tablet">FINAL</th>
					<th data-hide="phone,tablet">LUGAR</th>
					<th data-hide="phone,tablet">PROFESOR</th>
					<th data-hide="phone,tablet">ASISTENCIA</th>
					<th data-hide="phone,tablet">PUNTAJE</th>
					<th data-hide="phone,tablet">RESULTADO</th>
				</tr>
			</thead>
			<!-- // Table heading END -->
			<tbody>
				<?php 
					$asistieron_tot = 0;
					$noasistieron_tot = 0;
					$aprobaron = 0;
					$noaprobaron = 0;
					$invitados = 0;
					$puntaje = 0;
					$aprobo = 0;

				if($cantidad_datos > 0){ 
						foreach ($datosCursos_all as $iID => $data) { 
							$result_org = "Reprobó";
							$estado_inv = "Invitado";
							if($data['estado']==1){
								$invitados++;
								$estado_inv = "Invitado";
							}elseif($data['estado']==2){
								$asistieron_tot++;
								$estado_inv = "Asitió";
							}else{
								$noasistieron_tot++;
								$estado_inv = "No Asitió";
							}
							if(isset($data['resultados'][0]['score'])){
								$puntaje = $data['resultados'][0]['score'];
								$aprobo = $data['resultados'][0]['approval'];
								if($aprobo=="SI"){
									$aprobaron++;
									$result_org = "Aprobó";
								}else{
									$noaprobaron++;
									$result_org = "Reprobó";
								}
							}
							?>
					<tr>
						<td><?php echo($data['newcode']); ?></td>
						<td><?php echo($data['course']); ?></td>
						<td><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
						<td><?php echo($data['identification']); ?></td>
						<td><?php echo($data['headquarter']); ?></td>
						<td><?php echo($data['start_date']); ?></td>
						<td><?php echo($data['end_date']); ?></td>
						<td><?php echo($data['living']); ?></td>
						<td><?php echo($data['first_prof'].' '.$data['last_prof']); ?></td>
						<td><?php echo($estado_inv); ?></td>
						<td><?php echo($puntaje); ?></td>
						<td><?php echo($result_org); ?></td>
					</tr>
				<?php } } ?>
			</tbody>
		</table>
		<!-- // Table elements END -->
		<!-- Total elements-->
		<div class="form-inline separator bottom small">
			Total de inscritos: <strong class='text-primary'><?php echo($cantidad_datos); ?></strong> | Invitados: <strong class='text-primary'><?php echo($invitados); ?></strong> | Asistieron: <strong class='text-success'><?php echo($asistieron_tot); ?></strong> | No Asitieron: <strong class='text-danger'><?php echo($noasistieron_tot); ?> </strong> | Aprobaron: <strong class='text-success'><?php echo($aprobaron); ?></strong> | No Aprobaron: <strong class='text-danger'><?php echo($noaprobaron); ?></strong>
		</div>
		<!-- // Total elements END -->
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_asistencia.js"></script>
</body>
</html>