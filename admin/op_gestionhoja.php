<?php include('src/seguridad.php');
	if(!isset($_SESSION['idUsuario'])){
		header("location: login");
	}
?>
<?php include('controllers/gestion_hojas.php');
$location = 'interno';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/gestion_hojas.php">Retroalimentación Hojas de Ruta</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Hojas de Ruta </h2>
						<!--<div class="btn-group pull-right">
									<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
									<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
									<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
									<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
									<h5><a href="gestion_hojas.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Gestionar Hoja de Ruta: <?php echo($_GET['course']); ?></h4><?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">
							<!-- Formulario Hoja de Ruta-->
								<div class="row innerLR">
									<div class="block block-inline">
										<div class="box-generic">
											<div class="timeline-top-info">
												<a href="ver_perfil.php" class="text-primary"><i class="text-primary fa fa-user"></i> <?php echo($registroConfiguracion['first_name']); ?></a>
											</div>
											<div class="media margin-none">
												<div class="row innerLR innerB">
													<div class="col-sm-2 col-lg-1">
														<div class="innerT">
															<img class="img-responsive" src="../assets/images/usuarios/<?php echo($registroConfiguracion['image']); ?>" style="width: 74px;" alt="<?php echo($registroConfiguracion['first_name']); ?>" />
														</div>
													</div>
													<div class="col-sm-10 col-lg-11">
														<div class="innerT">
															<h5 class="strong">Mis Compromisos (Qué?)</h5>
															<p>
																<?php if($_SESSION['max_rol']!="2" && $registroConfiguracion['status_id']!="3"){ ?>
																	<textarea id="description" name="description" class="wysihtml5 col-md-12 form-control" rows="5">
																	<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['description']; } ?>
																	</textarea>
																<?php }else{ ?>
																	<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['description']; } ?>
																<?php } ?>
															</p>
															<h5 class="strong">Que quiero lograr (Para qué?)</h5>
															<p>
																<?php if($_SESSION['max_rol']!="2" && $registroConfiguracion['status_id']!="3"){ ?>
																	<textarea id="target" name="target" class="wysihtml5 col-md-12 form-control" rows="5">
																		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['target']; } ?>
																	</textarea>
																<?php }else{ ?>
																	<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['target']; } ?>
																<?php } ?>
															</p>
															<h5 class="strong">Cómo lo lograré:</h5>
															<p>
																<?php if($_SESSION['max_rol']!="2" && $registroConfiguracion['status_id']!="3"){ ?>
																	<textarea id="action" name="action" class="wysihtml5 col-md-12 form-control" rows="5">
																		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['action']; } ?>
																	</textarea>
																<?php }else{ ?>
																	<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['action']; } ?>
																<?php } ?>
															</p>
															<p>
																<?php if($_SESSION['max_rol']!="2" && $registroConfiguracion['status_id']!="3"){ ?>
																	<div class="col-md-4 input-group date">
																		<input class="form-control" type="text" id="fec_fulfillment" name="fec_fulfillment" placeholder="Fecha" <?php if(isset($registroConfiguracion['fec_fulfillment'])){ ?> value="<?php echo($registroConfiguracion['fec_fulfillment']); ?>" <?php } ?>/>
																		<span class="input-group-addon">
																			<i class="fa fa-th"></i>
																		</span>
																	</div>
																<?php }else{ ?>
																	<span class="innerL col-md-1 text-primary">Cumplimiento: </span>
																	<span class="innerL col-md-11"><i class="fa fa-calendar fa-fw"></i> <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['fec_fulfillment']; } ?></span>
																<?php } ?>
																<span class="innerL col-md-1 text-success">Diligenciamiento: </span>
																<span class="innerL col-md-11"><i class="fa fa-calendar fa-fw"></i> <?php echo($registroConfiguracion['date_creation'].' | '.$registroConfiguracion['date_edition']); ?></span>
																<span class="innerL col-md-1 text-danger">Diferencia: </span>
																<span class="innerL col-md-11"><i class="fa fa-clock-o"></i> <?php echo($registroConfiguracion['difDias']); ?> días atrás</span>
															</p>
														</div>
													</div>
												</div>
											</div>		
										</div>
									</div>		
								</div>
								<?php if( ($_SESSION['max_rol']>2 || $_SESSION['max_rol']=="6") && ($registroConfiguracion['status_id']=="2" || $registroConfiguracion['status_id']=="3") ){ ?>
								<div class="row innerLR">
									<div class="block block-inline">
										<div class="box-generic">
											<div class="timeline-top-info">
												<a href="ver_perfil.php" class="text-primary"><i class="text-primary fa fa-user"></i> <?php echo($registroConfiguracion['first_prof']); ?></a>
											</div>
											<div class="media margin-none">
												<div class="row innerLR innerB">
													<div class="col-sm-2 col-lg-1">
														<div class="innerT">
															<img class="img-responsive" src="../assets/images/usuarios/<?php echo($registroConfiguracion['img_prof']); ?>" style="width: 74px;" alt="<?php echo($registroConfiguracion['first_prof']); ?>" />
														</div>
													</div>
													<div class="col-sm-10 col-lg-11">
														<div class="innerT">
															<h5 class="strong">Comentarios del profesor (Feedback):</h5>
															<p>
																<?php if($_SESSION['max_rol']=="2"){ ?>
																	<textarea id="feedback" name="feedback" class="wysihtml5 col-md-12 form-control" rows="5">
																		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['feedback']; } ?>
																	</textarea>
																<?php }else{ ?>
																	<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['feedback']; } ?>
																<?php } ?>
															</p>
															<h5 class="strong">Nota (Calificación):</h5>
															<p>
																<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='ver')){ ?>
																	<!-- Group -->
																	<div class="form-group">
																		<label class="col-md-1 control-label" for="score" style="padding-top:8px;">Calificación:</label>
																		<div class="col-md-11">
																			<input type="text" readonly class="form-control" style="width: 20%" id="score" name="score" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo 'value="'.$registroConfiguracion['score'].'"'; } ?> />
																		</div>
																	</div>
																	<!-- // Group END -->
																<?php } ?>
															</p>
															<h5 class="strong">Estado de la hoja de ruta:</h5>
															<p>
																<!-- Group -->
																<div class="form-group">
																	<label class="col-md-1 control-label" for="estado" style="padding-top:8px;">Estado:</label>
																	<div class="col-md-11">
																		<select style="width: 20%;" id="estado" name="estado" disabled>
																			<option value="1" <?php if($registroConfiguracion['status_id']=="1"){ ?>selected="selected"<?php } ?>>Pendiente</option>
																			<option value="2" <?php if($registroConfiguracion['status_id']=="2"){ ?>selected="selected"<?php } ?>>Diligenciado</option>
																			<option value="3" <?php if($registroConfiguracion['status_id']=="3"){ ?>selected="selected"<?php } ?>>Gestionado</option>
																		</select>
																	</div>
																</div>
																<!-- // Group END -->
															</p>
														</div>
													</div>
												</div>
											</div>		
										</div>
									</div>
								</div>
								<?php }else if( $_SESSION['max_rol']=="1" && $registroConfiguracion['status_id']=="3" ){ ?>
								<div class="row innerLR">
									<div class="block block-inline">
										<div class="box-generic">
											<div class="timeline-top-info">
												<a href="ver_perfil.php" class="text-primary"><i class="text-primary fa fa-user"></i> <?php echo($registroConfiguracion['first_prof']); ?></a>
											</div>
											<div class="media margin-none">
												<div class="row innerLR innerB">
													<div class="col-sm-2 col-lg-1">
														<div class="innerT">
															<img class="img-responsive" src="../assets/images/usuarios/<?php echo($registroConfiguracion['img_prof']); ?>" style="width: 74px;" alt="<?php echo($registroConfiguracion['first_prof']); ?>" />
														</div>
													</div>
													<div class="col-sm-10 col-lg-11">
														<div class="innerT">
															<h5 class="strong">Comentarios del profesor (Feedback):</h5>
															<p>
																<?php if($_SESSION['max_rol']=="2"){ ?>
																	<textarea id="feedback" name="feedback" class="wysihtml5 col-md-12 form-control" rows="5">
																		<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['feedback']; } ?>
																	</textarea>
																<?php }else{ ?>
																	<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo $registroConfiguracion['feedback']; } ?>
																<?php } ?>
															</p>
															<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
																<h5 class="strong">Nota (Calificación):</h5>
																<p>
																	<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
																		<!-- Group -->
																		<div class="form-group">
																			<label class="col-md-1 control-label" for="score" style="padding-top:8px;">Calificación:</label>
																			<div class="col-md-11">
																				<input type="text" style="width: 20%;" class="form-control" id="score" name="score" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { echo 'value="'.$registroConfiguracion['score'].'"'; } ?> />
																			</div>
																		</div>
																		<!-- // Group END -->
																	<?php } ?>
																</p>
															<?php } ?>
															<h5 class="strong">Estado de la hoja de ruta:</h5>
															<p>
																<!-- Group -->
																<div class="form-group">
																	<label class="col-md-1 control-label" for="estado" style="padding-top:8px;">Estado:</label>
																	<div class="col-md-11">
																		<select style="width: 20%;" id="estado" name="estado">
																			<option value="1" <?php if($registroConfiguracion['status_id']=="1"){ ?>selected="selected"<?php } ?>>Pendiente</option>
																			<option value="2" <?php if($registroConfiguracion['status_id']=="2"){ ?>selected="selected"<?php } ?>>Diligenciado</option>
																			<option value="3" <?php if($registroConfiguracion['status_id']=="3"){ ?>selected="selected"<?php } ?>>Gestionado</option>
																		</select>
																	</div>
																</div>
																<!-- // Group END -->
															</p>
														</div>
													</div>
												</div>
											</div>		
										</div>
									</div>
								</div>
								<?php } ?>
								<div class="separator"></div>
						</div>
					</div>
					<!-- Nuevo ROW-->
					<div class="row row-app"></div>
					<!-- // END Nuevo ROW-->
					<div class="row">
						<div class="col-md-12">
							<!-- About -->
							<!-- feedback del coordinador -->
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-head"><h4 class="heading glyphicons binoculars" style="color:#0058a3 !important"><i></i>Feedback del coordinador</h4></div>
								<div class="widget-body" style="padding-left: 25px;">
									<!-- About -->
									<div class="widget widget-heading-simple widget-body-white margin-none">
<?php 
$var_ctrlRow = 0;
foreach ($registroFeedback as $key => $value) { 
	$var_ctrlRow++;
	?>
	<?php if($var_ctrlRow==1){
		?>
		<div class="row">
	<?php }?>
	<div class="inline-block box-generic col-md-3">
		<div class="timeline-top-info content-filled border-bottom">
			<i class="fa fa-user"></i> <a href="ver_perfil.php"><?php echo $value['first_name']." ".$value['last_name']; ?></a>
		</div>			
		<div class="media innerAll margin-none">
			<a class="pull-left" href="#">
				<img src="../assets/images/usuarios/<?php echo($value['image']); ?>" alt="photo" class="media-object" width="60px">
			</a>
			<div class="media-body">
				<p>
					<span class="text-warning"><?php echo $value['feedback_date']; ?></span>: <?php echo $value['feedback']; ?>
				</p>
			</div>
		</div>
	</div>
	<div class="inline-block box-generic col-md-3">
		<div class="timeline-top-info content-filled border-bottom">
			<i class="fa fa-user"></i> <a href="ver_perfil.php"><?php echo $value['first_w']." ".$value['last_w']; ?></a>
		</div>	
		<div class="media innerAll margin-none ">
			<a class="pull-left" href="#">
				<img src="../assets/images/usuarios/<?php echo($value['img_prof']); ?>" alt="photo" class="media-object" width="60px">
			</a>
			<div class="media-body">
				<p>
					<span class="text-success"><?php echo $value['feedback_rdate']; ?></span>: <?php echo $value['feedback_r']; ?>
				</p>
			</div>
		</div>
	</div>
	<?php if($var_ctrlRow==2){?>
		</div>
	<?php $var_ctrlRow = 0; }?>
<?php } ?>
<?php if($var_ctrlRow!=0){ echo("</div>"); }?>

										<!-- // About END -->
										<!-- // Formulario para almacenar el feedback -->
										<div class="separator"></div>
										<div class="separator"></div>

										<!-- solo se activa para coordinadores -->
										<?php if($_SESSION['max_rol']== "3"){ ?>
											<div class="row innerLR">
												<div class="block block-inline">
													<div class="box-generic">
														<div class="timeline-top-info">
															<a href="ver_perfil.php" class="text-primary"><i class="text-primary fa fa-user"></i> <?php echo($_SESSION['NameUsuario']); ?></a>
														</div>
														<div class="media margin-none">
															<div class="row innerLR innerB">
																<div class="col-sm-2 col-lg-1">
																	<div class="innerT">
																		<img class="img-responsive" src="../assets/images/usuarios/<?php echo($_SESSION['imagen']); ?>" style="width: 74px;" alt="<?php echo($_SESSION['NameUsuario']); ?>" />
																	</div>
																</div>
																<div class="col-sm-10 col-lg-11">
																	<div class="innerT">
																		<h5 class="strong">Comentarios del Coordinador (Feedback GMAcademy):</h5>
																		<p>
																			<form id="formulario_feedback" method="post" autocomplete="off">
																				<input type="hidden" id="opcn_fb" name="opcn" value="feedback_coor">
																				<input type="hidden" name="id_pj" value="<?php echo $_GET['id']; ?>">
																				<div class="separator bottom"></div>
																				<div class="col-md-11">
																					<textarea name="feedbackText" id="feedbackText" rows="5" cols="160" style="width:75%" placeholder="Escriba su comentario"></textarea>
																				</div>
																				<div class="col-md-1">
																					<button class="btn btn-success" type="submit" name="btn-feedback"><i class="fa fa-check-circle"></i> Guardar</button>
																				</div>
																			</form>
																		</p>
																	</div>
																</div>
															</div>
														</div>		
													</div>
												</div>
											</div>
										<!-- Fin grupo-->
										<?php } ?>
										<!-- fin feedback coordinadores -->
										<!-- -->
										<!-- Respuesta de feedback por parte del evaluado -->
										<?php if($_SESSION['max_rol']== "1" && count($registroFeedback)>0 && isset($registro) && $registro == "" ){ ?>
											<div class="row innerLR">
												<div class="block block-inline">
													<div class="box-generic">
														<div class="timeline-top-info">
															<a href="ver_perfil.php" class="text-primary"><i class="text-primary fa fa-user"></i> <?php echo($_SESSION['NameUsuario']); ?></a>
														</div>
														<div class="media margin-none">
															<div class="row innerLR innerB">
																<div class="col-sm-2 col-lg-1">
																	<div class="innerT">
																		<img class="img-responsive" src="../assets/images/usuarios/<?php echo($_SESSION['imagen']); ?>" style="width: 74px;" alt="<?php echo($_SESSION['NameUsuario']); ?>" />
																	</div>
																</div>
																<div class="col-sm-10 col-lg-11">
																	<div class="innerT">
																		<h5 class="strong">Comentarios del Alumno (Respuesta al Feedback):</h5>
																		<p>
																			<form id="formulario_feedback" method="post" autocomplete="off">
																				<input type="hidden" id="opcn_fb" name="opcn" value="feedback_coor">
																				<input type="hidden" name="id_fb" value="<?php echo $ultimoFeedback;?>">
																				<div class="separator bottom"></div>
																				<div class="col-md-11">
																					<textarea name="feedbackText" id="feedbackText" rows="5" cols="160" sytle="width:75%" placeholder="Escriba su comentario"></textarea>
																				</div>
																				<div class="col-md-1">
																					<button class="btn btn-success" type="submit" name="btn-feedback"><i class="fa fa-check-circle"></i> Guardar</button>
																				</div>
																			</form>
																		</p>
																	</div>
																</div>
															</div>
														</div>		
													</div>
												</div>
											</div>
										<!-- Fin grupo-->
										<?php } ?>
										<!-- fin respuesta -->
									</div>
									<div class="col-md-12">

									</div>
								</div>
							</div>
							<!-- // fin feedback del coordinador-->
						</div>
					</div>
					<div class="row row-app"></div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/gestion_hojas.js"></script>
</body>
</html>
