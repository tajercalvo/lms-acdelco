<?php include('src/seguridad.php'); ?>
<?php include('controllers/perfil.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons old_man"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/ver_perfil.php">Perfil</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Mi información </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<form id="formulario_usuarios" method="post" autocomplete="off">
						<div class="widget widget-heading-simple widget-body-gray widget-employees">
							<div class="widget-body padding-none">
								<div class="row">
									<div class="col-md-12 detailsWrapper">
										<div class="ajax-loading hide">
											<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
										</div>
										<div class="innerAll">
											<div class="title">
												<div class="row">
													<div class="col-md-6">
														<h3 class="text-primary"><?php if (isset($datosUsuario['first_name'])) {
																						echo ($datosUsuario['first_name'] . ' ' . $datosUsuario['last_name']);
																					} else {
																						echo "Nuevo Usuario";
																					} ?></h3>
													</div>
													<div class="col-md-6 text-right">
													</div>
												</div>
											</div>
											<hr />
											<div class="body">
												<div class="row">
													<div class="col-md-3 overflow-hidden">
														<!-- Profile Photo -->
														<div>
															<?php if (isset($datosUsuario['first_name'])) { ?>
																<img id="ImgUsuario" src="../assets/images/usuarios/<?php echo ($datosUsuario['image']); ?>" style="width: 200px;" alt="<?php echo ($datosUsuario['first_name'] . ' ' . $datosUsuario['last_name']); ?>" />
															<?php } else { ?>
																<img src="../assets/images/usuarios/default.png" style="width: 200px;" alt="Ludus LMS" />
															<?php } ?>
															<div class="ajax-loading hide" id="loading_imagen">
																<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
															</div>
														</div>
														<div class="separator bottom"></div>
														<!-- // Profile Photo END -->
														<?php if (isset($datosUsuario['first_name'])) { ?>
															<ul class="icons-ul separator bottom">
																<li><i class="fa fa-envelope fa fa-li fa-fw"></i> @: <?php echo ($datosUsuario['email']); ?></li>
																<li><i class="fa fa-phone fa fa-li fa-fw"></i> Tel: <?php echo ($datosUsuario['phone']); ?></li>
																<li><i class="fa fa-skype fa fa-li fa-fw"></i> <?php echo ($datosUsuario['headquarter']); ?> </li>
															</ul>
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
															<span class="btn btn-default btn-file">
																<span class="fileupload-new">Seleccionar Fotografía</span>
																<span class="fileupload-exists">Cambiar Imagen</span>
																<input type="file" class="margin-none" id="image_new" name="image_new" />
															</span>
															<span class="fileupload-preview"></span>
															<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
														<div class="form-group">
															<button type="button" class="btn btn-primary" onclick="cargaImagen();"><i class="fa fa-check-circle"></i> Cambiar Fotografía</button>
														</div>
														<?php if (isset($_GET['back']) && $_GET['back'] == "inicio") { ?>
															<h5><a href="../admin/" class="glyphicons no-js unshare"><i></i>Regresar</a>
																<h5>
																<?php } elseif (isset($_GET['back']) && $_GET['back'] == "usuarios") { ?>
																	<h5><a href="../admin/usuarios.php" class="glyphicons no-js unshare"><i></i>Regresar</a>
																		<h5>
																		<?php } ?>
													</div>
													<div class="col-md-9 padding-none">
														<?php if (isset($datosUsuario['first_name'])) { ?>
															<h5 class="strong">Datos Necesarios:</h5>
															<p style="text-align:justify">A continuación se presenta la información disponible del usuario, dichos datos han sido ingresados en la herramienta para la disponbilidad del directorio.</p>
														<?php } else { ?>
															<h5 class="strong">ACDelco- Datos necesarios:</h5>
															<p style="text-align:justify">A continuación se solicita la información mínima necesaria para crear un usuario, dichos datos han serán ingresados en la herramienta para la disponbilidad del directorio.</p>
														<?php } ?>
														<div class="row">
															<div class="col-md-6 padding-none">
																<p class="muted"><strong>Usuario:</strong>
																<div class="col-md-10 input-group">
																	<input type="text" class="form-control" id="identification" name="identification" placeholder="ID de ingreso" <?php if (isset($datosUsuario['identification'])) { ?> value="<?php echo ($datosUsuario['identification']); ?>" <?php } ?> <?php if ((isset($_SESSION['max_rol']) && $_SESSION['max_rol'] < 5) && (isset($_GET['opcn']) && $_GET['opcn'] == "editar")) { ?> readonly="readonly" <?php } ?> />
																</div>
																</p>
																<p class="muted"><strong>Nombres:</strong>
																<div class="col-md-10 input-group">
																	<input type="text" class="form-control" id="first_name" name="first_name" placeholder="Nombre del usuario" <?php if (isset($datosUsuario['first_name'])) { ?> value="<?php echo ($datosUsuario['first_name']); ?>" <?php } ?> />
																</div>
																</p>
																<p class="muted"><strong>Apellidos:</strong>
																<div class="col-md-10 input-group">
																	<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Apellido del usuario" <?php if (isset($datosUsuario['last_name'])) { ?> value="<?php echo ($datosUsuario['last_name']); ?>" <?php } ?> />
																</div>
																</p>
																<p class="muted"><strong>Nacimiento:</strong>
																<div class="col-md-4 input-group date">
																	<input class="form-control" type="text" id="date_birthday" name="date_birthday" placeholder="Fecha" <?php if (isset($datosUsuario['date_birthday'])) { ?> value="<?php echo ($datosUsuario['date_birthday']); ?>" <?php } ?> />
																	<span class="input-group-addon">
																		<i class="fa fa-th"></i>
																	</span>
																</div>
																</p>
																<p class="muted"><strong>Pais:</strong>
																<div class="col-md-4 input-group date">
																	<select style="width: 100%;" id="pais" name="pais">
																		<option value="Colombia" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Colombia") { ?> selected="selected" <?php } ?>>Colombia</option>
																		<option value="Venezuela" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Venezuela") { ?> selected="selected" <?php } ?>>Venezuela</option>
																		<option value="Chile" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Chile") { ?> selected="selected" <?php } ?>>Chile</option>
																		<option value="Perú" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Perú") { ?> selected="selected" <?php } ?>>Perú</option>
																		<option value="Ecuador" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Ecuador") { ?> selected="selected" <?php } ?>>Ecuador</option>
																		<option value="Brasil" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Brasil") { ?> selected="selected" <?php } ?>>Brasil</option>
																		<option value="Argentina" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Argentina") { ?> selected="selected" <?php } ?>>Argentina</option>
																		<option value="Bolivia" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Bolivia") { ?> selected="selected" <?php } ?>>Bolivia</option>
																		<option value="Paraguay" <?php if (isset($datosUsuario['pais']) && $datosUsuario['pais'] == "Paraguay") { ?> selected="selected" <?php } ?>>Paraguay</option>
																	</select>
																</div>
																</p>
																<?php if (isset($datosUsuario['first_name'])) { ?>
																	<input type="hidden" id="imgant" name="imgant" value="<?php echo ($datosUsuario['image']); ?>">
																	<input type="hidden" id="user_id" name="user_id" value="<?php echo ($datosUsuario['user_id']); ?>">
																	<p class="muted"><strong>Fecha Creación:</strong> <span class="text-primary"><?php echo ($datosUsuario['date_creation']); ?></span></p>
																	<p class="muted"><strong>Restablecer contraseña:</strong>
																		<label class="checkbox">
																			<input type="checkbox" class="checkbox" value="SI" id="restablece" name="restablece" />
																			Restablece a la misma identificación
																		</label>
																	</p>
																<?php } ?>
																<div class="separator bottom"></div>
																<!-- <p class="muted"><strong>T. Contratación:</strong>
																	<div class="col-md-4 input-group date">
																		<select style="width: 100%;" id="linking" name="linking" <?php if ($_SESSION['max_rol'] < 3) { ?>readonly="readonly"<?php } ?>>
																			<option value="Directo" <?php if (isset($datosUsuario['linking']) && $datosUsuario['linking'] == "Directo") { ?> selected="selected" <?php } ?>>Directo</option>
																			<option value="Tercerizado" <?php if (isset($datosUsuario['linking']) && $datosUsuario['linking'] == "Tercerizado") { ?> selected="selected" <?php } ?>>Tercerizado</option>
																			<option value="" <?php if (isset($datosUsuario['linking']) && $datosUsuario['linking'] == "") { ?> selected="selected" <?php } ?>>Por definir</option>
																		</select>
																	</div>
																</p>
																<p class="muted"><strong>Tipo de usuario:</strong>
																	<div class="col-md-4 input-group date">
																		<select style="width: 100%;" id="type" name="type" <?php if ($_SESSION['max_rol'] < 3) { ?>readonly="readonly"<?php } ?>>
																			<?php foreach ($type_user as $key => $value) { ?>
																				<option value="<?php echo ($value['type_user_id']); ?>" <?php if (isset($datosUsuario['type_user_id']) && $datosUsuario['type_user_id'] == $value['type_user_id']) { ?>selected="selected" <?php } ?> > <?php echo ($value['type_user']); ?></option>
																			<?php } ?>
																		</select>
																	</div>
																</p> -->
															</div>
															<div class="col-md-6 padding-none">
																<p class="muted"><strong>Correo:</strong>
																<div class="col-md-10 input-group">
																	<input type="text" class="form-control" id="email" name="email" placeholder="Email" <?php if (isset($datosUsuario['email'])) { ?> value="<?php echo ($datosUsuario['email']); ?>" <?php } else {
																																																													echo (" value='' ");
																																																												} ?> />
																</div>
																</p>
																<p class="muted"><strong>Teléfono:</strong>
																<div class="col-md-8 input-group">
																	<input type="text" class="form-control" id="phone" name="phone" onkeypress="return justNumbers(event);" placeholder="Teléfono" <?php if (isset($datosUsuario['phone'])) { ?> value="<?php echo ($datosUsuario['phone']); ?>" <?php } ?> />
																</div>
																</p>
																<p class="muted"><strong>Compañia:</strong>
																<div class="col-md-10 input-group">
																	<input type="text" class="form-control" id="empresa" name="empresa" placeholder="Empresa" <?php if (isset($datosUsuario['empresa'])) { ?> value="<?php echo ($datosUsuario['empresa']); ?>" <?php } ?> />
																</div>
																</p>
																<p class="muted"><strong>Empresa:</strong>
																<div class="col-md-12 input-group date">
																	<select style="width: 100%;" id="headquarter_id" name="headquarter_id">
																		<?php foreach ($listadoConcesionarios as $key => $Data_listados) { ?>
																			<option value="<?php echo ($Data_listados['headquarter_id']); ?>" <?php if (isset($datosUsuario['headquarter_id']) && $datosUsuario['headquarter_id'] == $Data_listados['headquarter_id']) { ?> selected="selected" <?php } ?>><?php echo ($Data_listados['headquarter'] . ' (' . ($Data_listados['address1']) . ')'); ?></option>
																		<?php } ?>
																	</select>
																</div>
																</p>
																<p class="muted"><strong>Estado:</strong>
																<div class="col-md-4 input-group date">
																	<input type="hidden" id="status_id_ant" name="status_id_ant" value="<?php echo isset($datosUsuario['status_id']) ? $datosUsuario['status_id'] : 0; ?>" />
																	<select style="width: 100%;" id="status_id" name="status_id" <?php if ($_SESSION['max_rol'] < 3) { ?>readonly="readonly" <?php } ?>>
																		<option value="1" <?php if (isset($datosUsuario['status_id']) && $datosUsuario['status_id'] == "1") { ?> selected="selected" <?php } ?>>Activo</option>
																		<option value="2" <?php if (isset($datosUsuario['status_id']) && $datosUsuario['status_id'] == "2") { ?> selected="selected" <?php } ?>>Inactivo</option>
																	</select>
																</div>
																</p>
																<p class="muted"><strong>Género:</strong>
																<div class="col-md-4 input-group date">
																	<select style="width: 100%;" id="gender" name="gender" <?php if ($_SESSION['max_rol'] < 3) { ?>readonly="readonly" <?php } ?>>
																		<option value="1" <?php if (isset($datosUsuario['gender']) && $datosUsuario['gender'] == "1") { ?> selected="selected" <?php } ?>>Masculino</option>
																		<option value="2" <?php if (isset($datosUsuario['gender']) && $datosUsuario['gender'] == "2") { ?> selected="selected" <?php } ?>>Femenino</option>
																		<option value="0" <?php if (isset($datosUsuario['gender']) && $datosUsuario['gender'] == "0") { ?> selected="selected" <?php } ?>>Por Definir</option>
																	</select>
																</div>
																</p>
																<p class="muted"><strong>Última Navegación:</strong> <span class="text-primary"><?php if (isset($DatMaxNav['maxnav'])) {
																																					echo ($DatMaxNav['maxnav']);
																																				} ?></span></p>
																<p class="muted">
																	<strong>Nivel académico:</strong>
																<div class="col-md-4 input-group date">
																	<select style="width: 100%;" name="education" id="education" <?php if ($_SESSION['max_rol'] < 3) { ?>readonly="readonly" <?php } ?>>
																		<option value="Primaria/Secundaria" <?php if (isset($datosUsuario['education']) && $datosUsuario['education'] == 'Primaria/Secundaria') {
																												echo ('selected');
																											} ?>>Primaria/Secundaria</option>
																		<option value="Tecnico" <?php if (isset($datosUsuario['education']) && $datosUsuario['education'] == 'Tecnico') {
																									echo ('selected');
																								} ?>>Técnico</option>
																		<option value="Tecnologo" <?php if (isset($datosUsuario['education']) && $datosUsuario['education'] == 'Tecnologo') {
																										echo ('selected');
																									} ?>>Tecnólogo</option>
																		<option value="Profesional" <?php if (isset($datosUsuario['education']) && $datosUsuario['education'] == 'Profesional') {
																										echo ('selected');
																									} ?>>Profesional</option>
																		<option value="Posgrado" <?php if (isset($datosUsuario['education']) && $datosUsuario['education'] == 'Posgrado') {
																										echo ('selected');
																									} ?>>Posgrado</option>
																	</select>
																</div>
																</p>

																<!-- filtro solo para coordinadores de zona -->
																<?php if ($coordinador > 0 && $_GET['opcn'] == "editar") : ?>
																	<p class="muted">
																		<strong>Concesionarios Zonas:</strong>
																	<div class="col-md-10 input-group date">
																		<select multiple="multiple" style="width: 100%;" name="zone_id[]" id="zone_id" <?php if ($_SESSION['max_rol'] < 3) { ?>readonly="readonly" <?php } ?>>
																			<optgroup label="Concesionarios">
																				<?php foreach ($listadoCcs as $key => $ccs) : ?>
																					<?php $selected = ""; ?>
																					<?php foreach ($ccs_coordinadores as $key => $value) {
																						if ($ccs['dealer_id'] == $value['dealer_id']) {
																							$selected = "selected";
																						}
																					} ?>
																					<option value="<?php echo $ccs['dealer_id'] ?>" <?php echo $selected; ?>><?php echo $ccs['dealer'] ?></option>
																				<?php endforeach; ?>
																			</optgroup>
																		</select>
																	</div>
																	</p>
																<?php endif; ?>
																<!-- fin filtro coordinadores de zona -->
																<br>
																<div class="separator bottom"></div>
															</div>
														</div>
														<div class="row">
															<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Perfíl </h5>
															<select multiple style="width: 100%;" id="roles" name="roles[]" <?php if ($_SESSION['max_rol'] < 5) { ?>readonly="readonly" <?php } ?>>
																<optgroup label="Activos">

																<?php 
foreach ($rol_active as $key => $Data_ActiveRol) {
    $selected = "";

    if (isset($rol_user)) {
        foreach ($rol_user as $key => $Data_RolUser) {
            if ($Data_ActiveRol['rol_id'] == $Data_RolUser['rol_id']) {
                $selected = "selected";
            }
        }
    }

    if (isset($_GET['opcn']) && ($_GET['opcn'] == "nuevo") && $Data_ActiveRol['rol_id'] == 6) {
        if (in_array(1, array_column($_SESSION['id_rol'], 'rol_id'))) {
            // Si el usuario tiene el rol con id 1, puedes agregar lógica adicional aquí si es necesario.
        } else {
            if ($_SESSION['max_rol'] != 1 && $_SESSION['max_rol'] != 2) {
                if ($Data_ActiveRol['rol_id'] == 1 || $Data_ActiveRol['rol_id'] == 2) {
                    continue;
                }
            }
        }
        if (isset($_GET['opcn']) && ($_GET['opcn'] == "nuevo") && $Data_ActiveRol['rol_id'] == 6) {
            $selected = "selected";
        }
        if ($Data_ActiveRol['rol_id'] == 1 && $_SESSION['max_rol'] < 6) {
            continue;
        }
    }

    // Excluir la opción de administrador si el usuario tiene el rol_id = 2
    if (in_array(2, array_column($_SESSION['id_rol'], 'rol_id')) && ($Data_ActiveRol['rol_id'] == 1 || $Data_ActiveRol['rol_id'] == 2)) {
        continue;
    }

    ?>
    <option value="<?php echo $Data_ActiveRol['rol_id']; ?>" <?php echo $selected; ?>><?php echo $Data_ActiveRol['rol']; ?></option>
    <?php 
}
?>



																</optgroup>
																<optgroup label="Inactivos">
																	<?php foreach ($rol_inactive as $key => $Data_InactiveRol) {
																		$selected = "";
																		if (isset($rol_user)) {
																			foreach ($rol_user as $key => $Data_RolUser) {
																				if ($Data_ActiveRol['rol_id'] == $Data_RolUser['rol_id']) {
																					$selected = "selected";
																				}
																			}
																		}
																	?>
																		<option value="<?php echo $Data_InactiveRol['rol_id']; ?>" <?php echo $selected; ?>><?php echo $Data_InactiveRol['rol']; ?></option>
																	<?php } ?>
																</optgroup>
															</select>
														</div>
														<div class="separator bottom"></div>
														<div class="row">
															<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Trayectoria </h5>
															<?php if ($_SESSION['max_rol'] >= 5) { ?>

																<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]">
																	<optgroup label="Activos">
																		<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
																			$selected = "";
																			if (is_array($charge_user) && count($charge_user) > 0) {
																				foreach ($charge_user as $key => $Data_ChargeUser) {
																					if ($Data_ActiveCharge['charge_id'] == $Data_ChargeUser['charge_id']) {
																						$selected = "selected";
																					}
																				}
																			}
																		?>
																			<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php echo $selected; ?>><?php echo $Data_ActiveCharge['charge']; ?></option>
																		<?php } ?>
																	</optgroup>
																</select>

															<?php } else { ?>
																<select style="width: 100%;" id="cargos" name="cargos[]">
																	<?php foreach ($charge_active as $key => $Data_ActiveCharge) {
																		$selected = "";
																		if (is_array($charge_user) && count($charge_user) > 0) {
																			if ($charge_user[0]['charge_id'] == $Data_ActiveCharge['charge_id']) {
																				$selected = "selected";
																			}
																		}
																	?>
																		<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php echo $selected; ?>><?php echo $Data_ActiveCharge['charge']; ?></option>
																	<?php } ?>
																</select>
															<?php } ?>
														</div>
														<div class="separator bottom"></div>
														<div class="row">
															<div class="col-md-9">
															</div>
															<div class="col-md-3">
																<div class="form-actions">
																	<?php if (isset($datosUsuario['user_id'])) { ?>
																		<input type="hidden" id="opcn" name="opcn" value="editar">
																		<button type="submit" class="btn btn-success" id="guardarEstudio"><i class="fa fa-check-circle"></i> Actualizar</button>
																	<?php } else { ?>
																		<input type="hidden" id="opcn" name="opcn" value="crear">
																		<button type="submit" class="btn btn-success" id="guardarEstudio"><i class="fa fa-check-circle"></i> Crear</button>
																	<?php } ?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/op_usuarios.js?v=<?php echo md5(rand(0, 100)); ?>"></script>
</body>

</html>