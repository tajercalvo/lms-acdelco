<?php include('src/seguridad.php'); ?>
<?php include('controllers/foros_tutor.php');
$location = 'tutor';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
//$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons train"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/foros_tutor.php">Foro - Tutor GM Academy</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div>
						<div class="btn-group pull-right">
							<?php if(isset($_GET['id'])){ ?>
								<a href="foros_tutor.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><br><br>
							<?php } ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<?php if(!isset($_GET['id'])){ ?>
	<div class="widget widget-heading-simple widget-body-gray">
		<div class="innerAll spacing-x2">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="widget ">
							<?php 
							$category_name = "";
							foreach ($Foros_datos as $key => $Data_Forum) {
							?>
								<?php if($Data_Forum['category']!=$category_name) { 
									$category_name = $Data_Forum['category'];
									?>
									<!-- Category Heading -->
									<div class="innerAll half bg-gray border-bottom">
										<?php if($_SESSION['max_rol']>=6){ ?>
											<a  href="#ModalCrearNueva" data-toggle="modal" class="btn btn-xs btn-inverse pull-right"><i class="fa fa-plus fa-fw"></i> Nuevo Hilo</a>
										<?php } ?>
										<h4 class=" margin-bottom-none"><?php echo($category_name);?></h4>
										<div class="clearfix"></div>
									</div>
									<!-- End Category Heading -->
								<?php } ?>
								<!-- Category Listing -->
								<div class="bg-gray-hover overflow-hidden">
									<div class="row innerAll half border-bottom">
										<div class="col-sm-9 col-xs-9">
											<ul class="media-list margin-none">
												<li class="media">
												    <a class="pull-left innerAll half " href="foros_tutor.php?id=<?php echo($Data_Forum['forum_id']); ?>">
												    	<?php if($Data_Forum['image']!=""){ ?>
												    		<img class="img-responsive" style="width: 150px;" id="imgModal_Lanzamiento" src="../assets/gallery/modal/<?php echo($Data_Forum['image']); ?>" title="<?php echo($Data_Forum['forum']); ?>" class="img-responsive" />
												    	<?php }else{ ?>
												    		<img class="img-responsive" style="width: 150px;" id="imgModal_Lanzamiento" src="../assets/gallery/modal/default.jpg" title="<?php echo($Data_Forum['forum']); ?>" class="img-responsive" />
												    	<?php } ?>
												    </a>
												    <div class="media-body">
													    <div class="innerAll half">
													    	<h4 class="margin-none">
													    		<a href="foros_tutor.php?id=<?php echo($Data_Forum['forum_id']); ?>" class="media-heading strong text-primary"><?php echo($Data_Forum['forum']); ?></a>
													    	</h4>
													  		<div class="clearfix"></div>
													    	<small class="margin-none"><?php echo(substr($Data_Forum['forum_description'],0,350)); ?>...</small> 
												    	</div>
												    </div>
												</li>
											</ul>
										</div>
										<div class="col-sm-1 col-xs-1">
											<div class="text-center">
												<p class="lead strong margin-bottom-none"><?php echo number_format($Data_Forum['num_posts']);?></p>
												<span class="text-muted">POSTS</span>
											</div>
										</div>
										<div class="col-sm-2 col-xs-hidden">
											<div class="innerAll half">
												<div class="media">
													<a href="foros_tutor.php?id=<?php echo($Data_Forum['forum_id']); ?>" class="pull-left">
														<img src="../assets/images/usuarios/<?php echo $Data_Forum['image_usr']; ?>" style="width:35px;" class="media-object"/>
													</a>
													<div class="media-body">
														<a href="foros_tutor.php?id=<?php echo($Data_Forum['forum_id']); ?>" class="text-small"><?php echo(substr($Data_Forum['first_name'].' '.$Data_Forum['last_name'],0,15));?></a>
														<div class="clearfix"></div>
														<small>Moderador del foro</small>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- // END Category Listing -->
							<?php } ?>
						</div>
						<!-- // END col-separator -->	
					</div>
					<!-- // END col -->
				</div>
				<!-- // END row -->
		</div>
	</div>
<?php } ?>
<?php if(isset($_GET['id'])){ ?>
	<h3 class="margin-none content-heading bg-white text-primary" sytle="border:1px solid #efefef !important" ><?php echo($Foro_datos['forum']); ?></h3>
	<div class="innerAll half">
  		<div class="clearfix"></div>
  		<?php if($_SESSION['max_rol']>=6){ ?>
			<a href="#ModalEditar" data-toggle="modal" class="btn btn-xs btn-inverse pull-right"><i class="fa fa-pencil fa-fw"></i> Editar Hilo</a>
		<?php } ?>
    	<small class="margin-none" style="text-align: justify;"><?php echo($Foro_datos['forum_description']); ?></small> 
	</div>
<div class="row">
	<div class="col-md-8">
		<div id="screen-subscriber"></div>
			<?php if($_SESSION['idUsuario']==$Foro_datos['creator']){?>
				<input type="hidden" name="teacher_id" id="teacher_id" value="17627">
				<button onclick="javascript:screenshare();" disabled id="shareBtn" class="btn btn-success"><i class="fa fa-fw fa-desktop"></i>Compartir</button>
				<!--<button onclick="javascript:startArchive();" disabled id="StartBTN" class="btn btn-success"><i class="fa fa-fw fa-save"></i>Guardar</button>
				<button onclick="javascript:stopArchive();" disabled id="StopBTN" class="btn btn-danger"><i class="fa fa-fw fa-stop"></i>Detener</button>-->
			<?php }?>
			<?php if($_SESSION['idUsuario']==$Foro_datos['creator']){?>
				<div id="camera-publisher" style="z-index: 9999; width: 200px; height: 150px; !important"></div>
			<?php }?>
			<div id="camera-subscriber" style="z-index: 9999;"></div>
	</div>
	<div class="col-md-4" id="ElDeBajar" style="max-height: 450px; height: 450px; overflow-y: scroll;">
		<div class="innerAll spacing-x2">
		<?php if($Foro_datos['session_id']!=""){ ?>
			<input type="hidden" id="sessionId" name="sessionId" value="<?php echo($Foro_datos['session_id']); ?>">
			<input type="hidden" id="token" name="token" value="<?php echo($Foro_datos['token']); ?>">
		<?php }?>
			<!-- row -->
			<div class="row" id="MensajesForo">
				<?php 
				$MaxIdReply = 0;
				foreach ($Foros_Reply as $key => $Data_Reply) {
					$MaxIdReply = $Data_Reply['forum_reply_id'];
					?>
					<!-- Post List -->
					<div class="media border-bottom margin-none">
						<div class="media-body">
							<small class="label label-default"><?php echo($Data_Reply['date_creation']); ?> - <?php echo($Data_Reply['first_name'].' '.$Data_Reply['last_name']); ?>:</small> <span><?php echo($Data_Reply['forum_reply']); ?></span><br>
						</div>
					</div>
					<!-- // END Información General Listing -->
				<?php } ?>
				<input type="hidden" class="MaxIdReply" name="MaxIdReply" id="MaxIdReply" value="<?php echo($MaxIdReply);?>">
			</div>
		</div>
	</div>
	
</div>
<div class="row">
	<div class="col-md-8">
	</div>
	<div class="col-md-4">
		<div>
			<form id="FormularioComentarios">
				<div class="innerB">
					<input type="hidden" id="opcn" name="opcn" value="enviar">
					<input type="hidden" id="forum_id" value="<?php echo $_GET['id']; ?>" name="forum_id">
					<textarea id="comentario" rows="3" name="comentario" placeholder="Escriba aquí su comentario..." class="col-md-12 form-control margin-bottom notebook padding-none" rows="5"></textarea>
				</div>
				<button type="button" id="botonChat" class="btn btn-primary">Enviar</button>
			</form>
		</div>
	</div>
</div>
<?php } ?>




						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->





<?php if(!isset($_GET['id'])){ ?>
	<!-- Modal -->
	<form action="foros_tutor.php" enctype="multipart/form-data" method="post" id="form_Crear"><br><br>
		<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Crear Nuevo Hilo para el Foro</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body">
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Pregunta: </label>
							<input type="text" id="forum" name="forum" class="form-control col-md-8" placeholder="Pregunta para el foro" />
						</div>
						<div class="col-md-1">
							<input type="hidden" id="opcn" value="crear" name="opcn">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Descripción: </label>
							<textarea id="forum_description" name="forum_description" class="col-md-12 form-control" rows="5"></textarea>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-4">
							<label class="control-label">Categoría: </label>
							<select style="width: 100%;" id="category" name="category" >
								<option value="Asesor Estratégico Chevrolet">Asesor Estratégico Chevrolet</option>
								<option value="General">General</option>
								<option value="Habilidades Blandas">Habilidades Blandas</option>
								<option value="Habilidades Técnicas">Habilidades Técnicas</option>
								<option value="Producto">Producto</option>
							</select>
						</div>
						<div class="col-md-1">
						</div>
						<div class="col-md-4">
							<label class="control-label">Posición: </label>
							<input type="text" id="position" name="position" class="form-control col-md-8" placeholder="# Posición de este hilo en el foro" />
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Cargos </h5>
						<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]">
							<?php foreach ($charge_active as $key => $Data_ActiveCharge) { 
								?>
								<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" ><?php echo $Data_ActiveCharge['charge']; ?></option>
							<?php } ?>
			        	</select>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
				</div>
			</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- /.modal -->
<?php } ?>
<?php if(isset($_GET['id'])){ ?>
	<!-- Modal -->
	<form action="foros_tutor.php?id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data" method="post" id="form_Editar"><br><br>
		<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Editar el Hilo del foro</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body">
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Titulo: </label>
							<input type="text" id="forum_ed" name="forum_ed" class="form-control col-md-8" placeholder="Pregunta para el foro" value="<?php echo $Foro_datos['forum']; ?>" />
						</div>
						<div class="col-md-1">
							<input type="hidden" id="forum_id" value="<?php echo $_GET['id']; ?>" name="forum_id">
							<input type="hidden" id="opcn" value="editar" name="opcn">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Descripción:</label>
							<textarea id="forum_description_ed" name="forum_description_ed" class="col-md-12 form-control" rows="5"><?php echo $Foro_datos['forum_description']; ?></textarea>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-10">
							<label class="control-label">Categoría: </label>
							<select style="width: 100%;" id="category_ed" name="category_ed" >
								<option value="Asesor Estrat&eacute;gico Chevrolet" <?php if($Foro_datos['category']=="Asesor Estrat&eacute;gico Chevrolet"){ ?>selected="selected"<?php } ?> >Asesor Estratégico Chevrolet</option>
								<option value="General" <?php if($Foro_datos['category']=="General"){ ?>selected="selected"<?php } ?> >General</option>
								<option value="Habilidades Blandas" <?php if($Foro_datos['category']=="Habilidades Blandas"){ ?>selected="selected"<?php } ?> >Habilidades Blandas</option>
								<option value="Habilidades T&eacute;cnicas" <?php if($Foro_datos['category']=="Habilidades T&eacute;cnicas"){ ?>selected="selected"<?php } ?> >Habilidades Técnicas</option>
							</select>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-5">
							<label class="control-label">Posición: </label>
							<input type="text" id="position_ed" name="position_ed" class="form-control col-md-8" placeholder="Posición del hilo en el foro" value="<?php echo $Foro_datos['position']; ?>" />
						</div>
						<div class="col-md-1">
						</div>
						<div class="col-md-4">
							<label class="control-label">Estado: </label>
							<select style="width: 100%;" id="estado" name="estado">
								<option value="1" <?php if($Foro_datos['status_id']==1){ ?>selected<?php } ?>>Activo</option>
								<option value="2" <?php if($Foro_datos['status_id']==2){ ?>selected<?php } ?>>Inactivo</option>
							</select>
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
					<!-- Row -->
					<div class="row">
						<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Cargos </h5>
						<select multiple="multiple" style="width: 100%;" id="cargos_ed" name="cargos_ed[]">
							<?php foreach ($charge_active as $key => $Data_ActiveCharge) { 
											$selected = "";
											foreach ($Foro_datos['charges'][0] as $key => $Data_ChargeUser) {
												if($Data_ActiveCharge['charge_id'] == $Data_ChargeUser['charge_id']){
													$selected = "selected";
												}
											}
								?>
								<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php echo $selected; ?> ><?php echo $Data_ActiveCharge['charge']; ?></option>
							<?php } ?>
			        	</select>
					</div>
					<!-- Row END-->
					<div class="clearfix"><br></div>
				</div>
			</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- /.modal -->
<?php } ?>






		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="//static.opentok.com/v2/js/opentok.js"></script>
	<script src="js/foros_tutor.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>