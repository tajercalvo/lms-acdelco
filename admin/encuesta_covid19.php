<?php include('src/seguridad.php'); ?>
<?php
$location = 'encuesta_covid19';
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<head><meta charset="gb18030">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<style type="text/css">
.margendiv{
	max-height: 169px;
	min-height: 127px;
    min-width: 322px;
    max-width: 710px;
    border: 1px solid;
    border-radius: 24px;
    margin: 12px;
    padding: 24px;
    padding-top: 22px;
}
input:focus {
  border: 1px solid #ab2b3e !important;
  
}
.ptext{
	text-align: initial;
	font-size: 17px;
	font-weight: 400;
	color: #202124;
	font-family: -webkit-body;
}
</style>
<body class="document-body ">

	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted est¨¢ en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asischat_vct.php">Chat Vct</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
						<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Encuesta Covid</h2>
						          <div class="clearfix"></div>
					</div>

					<div class="widget widget-heading-simple widget-body-white">
							
							<!-- // Widget heading END -->
						<div class="widget-body">
									<div class="row">
										 <div class="col-md-12" style="text-align: center;">
										 	<img src="https://autotrain.com.co/AG/img/LogoAutoTrain-1.png" alt="AutoTrain" style="width: 40%">
										 </div>									</div>
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h4 class="heading glyphicons list"><i></i> Información: <span id="num_asistentes" style="color: #bd272c;font-weight: revert;"></span> </h4>
										</div>
										<!-- // Widget heading END -->
										<div class="widget-body">
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10" style="text-align: -webkit-center;">
													<div class="sesion1" style="min-width: 322px;border: 1px solid;margin: 12px;padding: 24px;padding-top: 22px;max-width: 710px;border-radius: 24px;border-top: 8px solid #ab2b3e;">

														<p style="font-size: 35px;font-weight: 400;color: #202124;font-family: -webkit-body;">Formulario diario para trabajo presencial:</p>

														<p style="text-align: initial;font-size: 17px;font-weight: 400;color: #202124;font-family: -webkit-body;">Este formulario hace parte del ABC Autotrain para trabajo presencial ocasional y/o permanente. Debe ser diligenciado con total veracidad y responsabilidad. Su registro permite el control eficaz de los protocolos de bioseguridad y la atención temprana del virus.</p>
														<p  style="text-align: initial;color: #d93025; display: block;">*Obligatorio</p>

													</div>
												<form id="frm_covid">
													<div class="sesion1">
												
													<div class="margendiv">

														<div class="form-group">
															<p class="col-md-5 ptext">Motivo de la Salida<span style="color: red"> *</span></p>
															 <div class="col-md-7">
																<select style="width: 100%;" id="mot_salida" name="mot_salida">
																<option value="">Seleccione Una Opcion</option>
																<option value="Trabajo Cat">Trabajo Cat</option>
																<option value="Trabajo Taller de Servicio">Trabajo Taller de Servicio</option>
																<option value="Trabajo Concesionario">Trabajo Concesionario</option>
																<option value="Live Store">Live Store</option>
																<option value="Trabajo Publicidad">Trabajo Publicidad</option>
																<!-- <option value="Trabajo Publicidad">Estudiante</option>
																<option value="Trabajo Publicidad">Instructor</option> -->
																<option value="Trabajo en sede AutoTrain">Trabajo en sede AutoTrain</option>
																<option value="Otros">Otros</option>
															    </select>
															 </div>
														</div>
													</div>

													<div class="margendiv otros">
														<div class="form-group">
															<p class="col-md-5 ptext" >Si su respuesta fué otro, especifique</p>

															 <div class="col-md-7" >
																<input class="form-control" type="text" name="opc_otros" id="opc_otros"  placeholder="Tú respuesta" autocomplete="off">
															 </div>
														</div>
													</div>

													<div class="margendiv">
														<div class="form-group">
															<p class="col-md-12 ptext" >Indique el lugar donde se desarrollará la actividad ( Dirección )<span style="color: #d93025;">*</span></p>

															<!--  <div class="col-md-12" >
																<input class="form-control" type="text" name="opc_otros" id="opc_otros"  placeholder="Tú respuesta" >
															 </div> -->
															 <div class="form-group has-feedback">
                                  								<input type="text" id="lugar_a_trabajar" name="lugar_a_trabajar" pattern=".{0}|.{3,}" class="form-control" placeholder="Tú respuesta" required="" title="5 caracteres mínimo" autocomplete="off">
                              								 </div>
														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >Medio de transporte a utilizar<span style="color: #d93025;">*</span></p>

														<input type="radio" id="opc_1_1" name="md_transporte" value="Carro propio">
														<label for="Carro_propio">Carro propio</label><br>

														<input type="radio" id="opc_1_2" name="md_transporte" value="Moto">
														<label for="Moto">Moto</label><br>

														 <input type="radio" id="opc_1_3" name="md_transporte" value="Bicicleta">
														<label for="Bicicleta">Bicicleta</label><br>

														<input type="radio" id="opc_1_4" name="md_transporte" value="Servicio Público">
														<label for="Servicio_Publico">Servicio Público</label><br>

														<input type="radio" id="opc_1_5" name="md_transporte" value="A pie">
														<label for="a_pie">A pie</label> 

														</div>
													</div>

													<div class="margendiv">
														<div class="form-group">
															<p class="col-md-12 ptext" >Nombre y teléfono de contacto en caso de emergencia<span style="color: #d93025;">*</span></p>

															 <div class="form-group has-feedback">
                                  								<input type="text" id="nombreycontacto" name="nombreycontacto" pattern=".{0}|.{3,}" class="form-control" placeholder="Tú respuesta" required="" title="5 caracteres mínimo" autocomplete="off">
                              								 </div>
														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >1.¿Ha sido diagnosticado con enfermedad por COVID-19 en los últimos 15 días?<span style="color: #d93025;">*</span></p>

														<input type="radio" id="check1_1" name="check1" value="SI">
														<label for="check1">SI</label><br>

														<input type="radio" id="check1_2" name="check1" value="NO">
														<label for="check1">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >2. ¿En los últimos 7 días ha estado usted en contacto con alguna persona de su entorno familiar o laboral con diagnóstico de COVID-19?<span style="color: #d93025;">*</span></p>

														<input type="radio" id="check2_1" name="check2" value="SI">
														<label for="check2">SI</label><br>

														<input type="radio" id="check2_2" name="check2" value="NO">
														<label for="check2">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >3.¿En los últimos 7 días ha estado usted en contacto con alguna persona de su entorno familiar o laboral con fiebre, tos, dificultad respiratoria, diarrea o dolor de estómago?<span style="color: #d93025;">*</span></p>

														<input type="radio" id="check3_1" name="check3" value="SI">
														<label for="check3">SI</label><br>

														<input type="radio" id="check3_2" name="check3" value="NO">
														<label for="check3">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >4. ¿Ha tenido fiebre en los últimos 7 días?<span style="color: #d93025;">*</span></p>

														<input type="radio" id="check4_1" name="check4" value="SI">
														<label for="check4">SI</label><br>

														<input type="radio" id="check4_2" name="check4" value="NO">
														<label for="check4">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >5. ¿Ha tenido tos en los últimos 7 días? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check5_1" name="check5" value="SI">
														<label for="check5">SI</label><br>

														<input type="radio" id="check5_2" name="check5" value="NO">
														<label for="check5">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >6. ¿Ha tenido dificultad para respirar en los últimos 7 días? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check6_1" name="check6" value="SI">
														<label for="check6">SI</label><br>

														<input type="radio" id="check6_2" name="check6" value="NO">
														<label for="check6">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >7. Ha tenido dolor muscular o fatiga en los últimos 7 días? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check7_1" name="check7" value="SI">
														<label for="check7">SI</label><br>

														<input type="radio" id="check7_2" name="check7" value="NO">
														<label for="check7">NO</label> 

														</div>
													</div>


													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >8. ¿Ha tenido dolor en el pecho en los últimos 7 días? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check8_1" name="check8" value="SI">
														<label for="check8">SI</label><br>

														<input type="radio" id="check8_2" name="check8" value="NO">
														<label for="check8">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >9. ¿Ha tenido dolor de garganta en los últimos 7 días? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check9_1" name="check9" value="SI">
														<label for="check9">SI</label><br>

														<input type="radio" id="check9_2" name="check9" value="NO">
														<label for="check9">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >10. ¿Ha tenido diarrea en los últimos 7 días? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check10_1" name="check10" value="SI">
														<label for="check10">SI</label><br>

														<input type="radio" id="check10_2" name="check10" value="NO">
														<label for="check10">NO</label> 

														</div>
													</div>

													<div class="margendiv" style="max-height: 200px !important;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >11. ¿Ha experimentado pérdida del olfato o el gusto en los últimos 7 días? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check11_1" name="check11" value="SI">
														<label for="check11">SI</label><br>

														<input type="radio" id="check11_2" name="check11" value="NO">
														<label for="check11">NO</label> 

														</div>
													</div>

													<div style="min-width: 322px;border: 1px solid;margin: 12px;padding: 24px;padding-top: 22px;max-width: 710px;border-radius: 24px;border-top: 8px solid #ab2b3e;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext" >12. ¿Conozco los riesgos asociados a la exposición al virus del COVID-19 como son; muerte, síndrome de dificultad respiratoria del adulto, coagulopatías, eventos trombóticos severos, alteraciones gastrointestinales, alteraciones neurológicas, y otras, que pueden aumentar la probabilidad de complicar enfermedades pre existentes, así como las probables secuelas derivadas del padecimiento de la enfermedad? <span style="color: #d93025;">*</span></p>

														<input type="radio" id="check12_1" name="check12" value="SI">
														<label for="check12">SI</label><br>

														<input type="radio" id="check12_2" name="check12" value="NO">
														<label for="check12">NO</label> 

														</div>
													</div>

												</div> <!-- fin sesion1 -->

												<div id="sesion2" style="display: none">
													<div style="min-width: 322px;border: 1px solid;margin: 12px;padding: 24px;padding-top: 22px;max-width: 710px;border-radius: 24px;border-top: 8px solid #ab2b3e;">
														<p style="font-size: 35px;font-weight: 400;color: #202124;font-family: -webkit-body;">Formulario diario para trabajo presencial:</p>
													</div>

													<div style="min-width: 322px;border: 1px solid;max-width: 710px;border-radius: 24px;">

														<p style="font-size: 23px;font-weight: 400;color: #ffffff;font-family: initial;background: #9a262f;border-radius: 25px;">Medidas generales para la prevención del covid 19</p>

														<div class="ptext" style="text-align: initial; padding: 12px;">
															*Identificar con que personas tuvo contacto durante la actividad <br>*Guarde al menos 1 metro de distancia entre usted y otras personas.<br>*El uso de la mascarilla es obligatorio en su interacción con otras personas.(Asegúrese de que le cubre la nariz, la boca y el mentón).<br>*Evite las 3 “C”: espacios Cerrados, Congestionados y Contactos Cercanos.<br>*Lávese periódica y cuidadosamente las manos con un gel hidroalcohólico o con agua y jabón. Esto elimina los gérmenes y virus.<br>*Evite tocarse los ojos, la nariz y la boca.<br>*Al toser o estornudar cúbrase la boca y la nariz con el codo flexionado o con un pañuelo.<br>*Limpie y desinfecte frecuentemente las superficies, en particular las que se tocan con regularidad.<br>*Permanecer en casa en autoaislamiento, aun cuando tenga síntomas leves tales como tos, dolor de cabeza y fiebre.<br>
															<p  style="color: #d93025; display: block;">*Obligatorio</p>
														</div>

														

													</div>

													<div style="min-width: 322px;border: 1px solid;margin: 12px;padding: 24px;padding-top: 22px;max-width: 710px;border-radius: 24px;border-top: 8px solid #ab2b3e;">
														<div class="form-group" style="text-align: initial;">
														<p class="col-md-12 ptext">Certifico que he leído detenidamente el formulario y he sido informado de las medidas generales de bioseguridad para la prevención del Covid 19. Las entiendo y estoy comprometido a cumplirlas y  a reportar algún resultado positivo mío o de mi familia o cualquier situación de importancia para evitar la propagación del virus.<span style="color: #d93025;">*</span></p>

														<input type="radio" id="check13" name="check13" value="SI">
														<label for="check13">SI</label><br>

														</div>
													</div>


												</div>
												<div class="col-md-4"></div>
												<button type="button" id="Siguiente" class="col-md-1"  style="margin-right: 40px;">siguiente</button> 
												<button type="button" id="atras" class="col-md-1"  style="margin-right: 40px; display: none">Atrás</button> 
												<button type="submit" id="submit" class="col-md-1"  style="display: none">Enviar</button>
											    <div class="col-md-4"></div>
												</form>


												</div> <!-- todo el contenido -->
												<div class="col-md-1"></div>
											</div>
										</div>
									</div>
						</div><!-- // widget-body-->
					</div><!-- // widget-->

						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
						<!-- // END contenido interno -->
			    </div>
			<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/encuesta_covid19.js"></script>
	</body>
</html>