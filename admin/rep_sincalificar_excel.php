<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
include('controllers/rep_sincalificar.php');
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_CursosSinCalificacion.xls");
if(isset($_GET['schedule_id'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet"><?php echo utf8_decode("PROGRAMACIÓN"); ?></th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet"><?php echo utf8_decode("CÓDIGO"); ?></th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">INSTRUCTOR</th>
                <th data-hide="phone,tablet"><?php echo utf8_decode("IDENTIFICACIÓN");?></th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php 
                if($cantidad_datos > 0){ 
                        foreach ($datosCursos_all as $iID => $data) { 
                            ?>
                    <tr>
                        <td><?php echo($data['schedule_id']); ?></td>
                        <td><?php echo($data['start_date']); ?></td>
                        <td><?php echo($data['newcode']); ?></td>
                        <td><?php echo(utf8_decode($data['course'])); ?></td>
                        <td><?php echo(utf8_decode($data['first_name'].' '.$data['last_name'])); ?></td>
                        <td><?php echo($data['identification']); ?></td>
                    </tr>
                <?php } } ?>
        </tbody>
    </table>
<?php } ?>