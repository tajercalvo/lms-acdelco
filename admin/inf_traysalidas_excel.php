<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['dealer_id'])){
    include('controllers/inf_traysalidas.php');
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=InformeSalidas.csv');
if(isset($_GET['dealer_id'])){
    echo("CONCESIONARIO;SEDE;ESPECIALIDAD;NOMBRE;DOCUMENTO;HORAS;SALIDAS\n");
    foreach ($datosCursos_all as $iID => $data) {
        echo(utf8_decode($data['dealer'].";".$data['headquarter'].";".$data['specialty'].";".$data['first_name']." ".$data['last_name'].";".$data['identification'].";".number_format( ($data['suma']),2 ).";".number_format( ($data['salidas']),2 )."\n"));
    }
    ?>
<?php } ?>
