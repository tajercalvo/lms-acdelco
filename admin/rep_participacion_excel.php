<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_participacion.php');
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_Participacion.csv');
header ("Content-Transfer-Encoding: binary");
if(isset($_GET['start_date_day'])){
    echo("CODIGO;CURSO;HORAS;HABILIDAD;TRAYECTORIA;INSTRUCTOR;INICIO;FIN;LUGAR;MIN;MAX;INS;ESTADO\n");
    foreach ($datosCursos_all as $key => $value) {
        $registro = "";
        $registro .= $value['newcode'].";";
        $registro .= $value['course'].";";
        $registro .= $value['duration_time'].";";
        $registro .= $value['specialty'].";";
        if( isset( $value['trayectoria'] ) && count( $value['trayectoria'] ) > 0 ){
            $trayectoria = "";
            foreach ($value['trayectoria'] as $key => $valueT) {
                $trayectoria .= $valueT." - ";
            }
            $registro .= $trayectoria.";";
        }else{
            $registro .= ";";
        }
        $registro .= $value['first_name']." ".$value['last_name'].";";
        $registro .= $value['start_date'].";";
        $registro .= $value['end_date'].";";
        $registro .= $value['living'].";";
        $registro .= $value['min_size'].";";
        $registro .= $value['max_size'].";";
        $registro .= $value['inscritos'].";";
        switch ( $value['status_id'] ) {
            case '1': $registro .= "Activo\n"; break;
            case '2': $registro .= "Inactivo\n"; break;
        }
        echo( utf8_decode( $registro ) );
    }
}
