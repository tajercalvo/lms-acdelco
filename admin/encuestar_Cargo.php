<?php
@session_start();
if(!isset($_SESSION['_EnCarg_ReviewId'])){
	header("location: index.php");
}else{
	$review_id = $_SESSION['_EnCarg_ReviewId'];
}
include('src/seguridad.php'); ?>
<?php include('controllers/encuestar_Cargo.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons circle_question_mark"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/encuestar_Cargo.php">Encuesta</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->

<div class="innerLR spacing-x2">
	<!-- row -->
	<div class="row">
		<!-- col -->
		<div class="col-md-10 col-md-offset-1">
			<div class="widget widget-body-white">
				<div class="border-bottom innerAll">
					<div class="media margin-none innerAll">
						<div class="pull-left">
							<img src="../assets/images/distinciones/default.png" style="width:100px;" class="img-responsive">
						</div>
						<div class="media-body">
							<div class="innerAll half media margin-none">
								<div class="pull-left innerLR half">
									<p class="strong center">GMAcademy - <?php echo($registroEncuesta['review']); ?></p>
									<!--<p class="margin-none" style="text-align:justify;">Encuesta de satisfacción para evaluar la convención a la que asistió en días pasados el <strong><?php echo $_SESSION['_EnCarg_Date']; ?></strong>.</p><br/>-->
									<p class="margin-none" style="text-align:justify;"><?php echo($registroEncuesta['description']); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="widget widget-survey border-none col-sm-3 center">
			</div>
			<div class="widget widget-survey col-sm-6 center">
				<div class="widget-body padding-none">
			  		<div class="ajax-loading center hide" id="loadingRespuesta"><br><br><br><br>
						<i class="fa fa-spinner fa fa-spin fa fa-4x"></i><br><br><br><br>
					</div>
					<div id="ContenidoEnc">
						<?php if(!isset($consultaEvaluacionYa['reviews_answer_id'])){ ?>
							<form id="formulario_respuestas" method="post" autocomplete="off">
								<input type="hidden" name="Review_Id_Source" id="Review_Id_Source" value="<?php echo($review_id); ?>">
						  		<?php foreach ($registroRespuestas as $key => $Data_Rep) { ?>
							  		<?php if($Data_Rep['type_response']=="1"){ ?>
							  			<div class="innerAll border-bottom" id="lbl_Quest<?php echo($Data_Rep['question_id']); ?>">
						  				  	<h4 class="innerTB margin-none half"><?php echo($Data_Rep['question']); ?></h4>
						  				  	<?php if($Data_Rep['source']!=''){ ?>
							  				  	<div class="row">
							  				  		<img src="../assets/gallery/source/<?php echo($Data_Rep['source']); ?>" style="width:600px;" class="img-responsive center">
							  				  	</div>
 	 										<?php } ?>
									  		<div class="row">
									  			<input class="Data_Radio" type="hidden" value="<?php echo($Data_Rep['question_id']); ?>" name="Quest<?php echo($Data_Rep['question_id']); ?>" data-lbl="<?php echo($Data_Rep['question']); ?>"/>
									  			<?php foreach ($Data_Rep['AnswersData'] as $key => $Data_Ans) { ?>
										  			<div class="col-sm-3">
												  		<label>
												    		<input class="Quest<?php echo($Data_Rep['question_id']); ?>" type="radio" name="<?php echo($Data_Rep['question_id']); ?>" id="Resp<?php echo($Data_Ans['answer_id']); ?>" data-vlr="<?php echo($Data_Ans['answer']); ?>" value="<?php echo($Data_Ans['answer_id']); ?>" >
											    			<?php echo($Data_Ans['answer']); ?>
												  		</label>
													</div>
												<?php } ?>
									  		</div>
										</div>
										<div class="separator bottom"></div>
									<?php }else{ ?>
										<div class="innerAll border-bottom">
									  		<h4 class="innerTB margin-none half"><?php echo($Data_Rep['question']); ?></h4>
									  		<textarea class="form-control CampText" placeholder="Escriba su respuesta aquí" name="<?php echo($Data_Rep['question_id']); ?>" id="<?php echo($Data_Rep['question_id']); ?>" data-lbl="<?php echo($Data_Rep['question']); ?>"></textarea>
								  		</div>
								  		<div class="separator bottom"></div>
									<?php } ?>
								<?php } ?>
								<input type="hidden" id="opcn" name="opcn" value="GuardaEncuesta">
								<div class="innerAll border-bottom">
									<button type="submit" class="pull-right btn btn-success" id="crearElemento"><i class="fa fa-check-circle"
									></i> Responder</button><br/><br/>
									<div class="separator bottom"></div>
								</div>
							</form>
					  	<?php }else{ ?>
					  		<div class="innerAll">
								<h4 class="innerTB margin-none half center">- Nota -</h4>
								<div class="separator bottom"></div>
								<div class="row">
					  				Usted ya presento esta evaluación, no puede volver a presentarla.
								</div>
						  	</div>
					  	<?php } ?>
				  	</div>
				</div>
			</div>
		</div>
		<!-- // END col -->

	</div>
	<!-- // END row -->
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/encuestar_Cargo.js"></script>
</body>
</html>