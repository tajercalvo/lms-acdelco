<?php include('src/seguridad.php'); ?>
<?php include('controllers/calificaciones.php');
$location = 'education';
$locData = true;
header("Content-Type: application/vnd.ms-excel; name='excel'");
header("Content-Disposition: filename=ListaAsistencia.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html>
<head>
	<style type="text/css">
		td{
			font-family: Arial, Helvetica, sans-serif;
			font-size: 12px;
			color: #000000;
			border: 1px solid black;
		}
		table{
			border: 0;
			border-collapse: collapse;
		}
		.EstiloHeader {
			font-family: Arial, Helvetica, sans-serif;
			font-size: 14px;
			color: #000000;
			font-weight: bold;
		}
		.fondo-tit{
			background-color: #e0e0e0;
			text-align: center;
		}
		.fondo-center{
			text-align: center;
		}
	</style>
</head>
<body>
<?php if( isset($cantidad_datos) ){ ?>
<table width="100%" border="1">
  <tbody>
    <tr>
      <td rowspan="2" align="center" style="background-color: yellow; height: 70px; min-height: 70px;"><img src="https://www.gmacademy.co/assets/images/gmacademy.png" style="width: 80px;height: 80px;"  alt="GM Academy Logo"></td>
      <td rowspan="2" align="center" style="background-color: yellow;"><b>LISTA DE ASISTENCIA</b></td>
      <td colspan="3" style="background-color: yellow;">CURSO: <strong><?php echo(utf8_decode($dato_sesion['course'])); ?></strong></td>
      <td colspan="3" style="background-color: yellow;">CONCESIONARIO: <strong></strong></td>
      <td colspan="3" style="background-color: yellow;">LUGAR: <strong><?php echo(utf8_decode($dato_sesion['living'])); ?></strong></td>
    </tr>
    <tr>
      <td colspan="3" style="background-color: yellow;">INSTRUCTOR: <strong><?php echo(utf8_decode($dato_sesion['first_name'].' '.$dato_sesion['last_name'])); ?></strong></td>
      <td style="background-color: yellow;"> </td>
      <td style="background-color: yellow;">A&Ntilde;O: <strong><?php echo substr($dato_sesion['start_date'], 0, 4); ?></td>
      <td style="background-color: yellow;">MES: <strong><?php echo substr($dato_sesion['start_date'], 5, 2); ?></strong></td>
      <td style="background-color: yellow;">D&Iacute;A:  <strong><?php echo substr($dato_sesion['start_date'], 8, 2); ?></strong></td>
      <td style="background-color: yellow;" colspan="2">N&Uacute;MERO DE HOJA: <strong>1</strong></td>
    </tr>
    <tr>
      <td style="background-color: lightgrey;" align="center"><strong>No.</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>NOMBRE Y APELLIDO</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>No. IDENTIFICACI&Oacute;N</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>E-MAIL</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>ESPECIALIDAD Y CARGO</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>CONCESIONARIO Y SEDE</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>E.I.</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>E.F.</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>DESEMPE&Ntilde;O</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>NOTA FINAL</strong></td>
      <td style="background-color: lightgrey;" align="center"><strong>FIRMA</strong></td>
    </tr>
    <?php if($cantidad_datos > 0){ 
		$var_ids = 0;
		foreach ($datosInvitaciones as $iID => $data) { 
			$var_ids++; 
			?>
    <tr>
      <td><?php echo($var_ids); ?></td>
      <td><?php echo(utf8_decode($data['first_name'].' '.$data['last_name'])); ?></td>
      <td><?php echo(utf8_decode($data['identification'])); ?></td>
      <td><?php echo(utf8_decode($data['email'])); ?></td>
      <td><?php foreach ($data['Cargos'] as $iID => $data_cargos) { echo(utf8_decode($data_cargos['charge'].'<br>')); }?></td>
      <td><?php echo(utf8_decode($data['headquarter'])); ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <?php } } ?>
  </tbody>
</table>
<?php }else{ ?>
	No hay Alumnos para esta sesión
<?php } ?>
</body>
</html>