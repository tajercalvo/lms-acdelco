<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/comparativo.php');
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_ComparativoTrayectorias.csv');
if(isset($_GET['start_date_day'])){
    if(count($CargosDatosListos) > 0){
        foreach ($cargos as $key => $kcargos) {
            echo($kcargos['charge']."\n\n");
            echo("CONCESIONARIO;CANTIDAD CURSOS;CANTIDAD USUARIOS;PROMEDIO;AVANCE\n");
            foreach ($concesionario as $key2 => $kconce) {
                $numeroCursos = 0;
                $numeroUsuarios = 0;
                $promedioNotas = 0;
                $sumaNotas = 0;
                $contadorNota = 0;
                $porcentajeAvance = 0;
                $cantidadCursosPaso = 0;
                $cantidadCursosPersona = 0;
                foreach ($CargosDatosListos as $key => $Data_InfoCargosAna) {
                    if($kconce == $Data_InfoCargosAna['dealer'] && $Data_InfoCargosAna['charge_id'] == $kcargos['charge_id']){
                        if(isset($Data_InfoCargosAna['_CantidadCursos'])){
                            $numeroCursos = $Data_InfoCargosAna['_CantidadCursos'] > $numeroCursos ? $Data_InfoCargosAna['_CantidadCursos'] : $numeroCursos ;
                        }
                        if(isset($Data_InfoCargosAna['_PromedioXCargo'])){
                            $sumaNotas += $Data_InfoCargosAna['_PromedioXCargo'] ;
                            $contadorNota++;
                        }
                        if(isset($Data_InfoCargosAna['cantidadPersonas'])){
                            $numeroUsuarios += $Data_InfoCargosAna['cantidadPersonas'];
                        }
                        if(isset($Data_InfoCargosAna['_CantidadCursosPaso'])){
                            $cantidadCursosPaso+= $Data_InfoCargosAna['_CantidadCursosPaso'];
                        }
                        if(isset($Data_InfoCargosAna['_CantidadCursosPersonas'])){
                            $cantidadCursosPersona += $Data_InfoCargosAna['_CantidadCursosPersonas'] ;
                        }
                        $colorZona = $Data_InfoCargosAna['zone'];
                    }//fin if
                }//fin foreach calculos vatos
                $porcentajeAvance = $cantidadCursosPersona > 0 ? number_format($cantidadCursosPaso/$cantidadCursosPersona*100,2) : 0;
                $promedioNotas = $contadorNota > 0 ? ($sumaNotas / $contadorNota) : 0;
                if( $numeroCursos > 0 ){
                        echo(utf8_decode($kconce.";".$numeroCursos.";".$numeroUsuarios.";".number_format($promedioNotas,1).";".$porcentajeAvance."\n"));
                }
            }//fin foreach concesionarios
            echo("\n");
            echo("ZONA;CANTIDAD CURSOS;CANTIDAD USUARIOS;PROMEDIO;AVANCE\n");
            foreach ($zona as $key => $kzona) {
                $numeroCursos = 0;
                $numeroUsuarios = 0;
                $promedioNotas = 0;
                $sumaNotas = 0;
                $contadorNota = 0;
                $porcentajeAvance = 0;
                $cantidadCursosPaso = 0;
                $cantidadCursosPersona = 0;
                foreach ($CargosDatosListos as $key => $Data_InfoCargosAna) {
                    if($kzona == $Data_InfoCargosAna['zone'] && $Data_InfoCargosAna['charge_id'] == $kcargos['charge_id']){
                        if(isset($Data_InfoCargosAna['_CantidadCursos'])){
                            $numeroCursos = $Data_InfoCargosAna['_CantidadCursos'] > $numeroCursos ? $Data_InfoCargosAna['_CantidadCursos'] : $numeroCursos ;
                        }
                        if(isset($Data_InfoCargosAna['_PromedioXCargo'])){
                            $sumaNotas += $Data_InfoCargosAna['_PromedioXCargo'] ;
                            $contadorNota++;
                        }
                        if(isset($Data_InfoCargosAna['cantidadPersonas'])){
                            $numeroUsuarios += $Data_InfoCargosAna['cantidadPersonas'];
                        }
                        if(isset($Data_InfoCargosAna['_CantidadCursosPaso'])){
                            $cantidadCursosPaso+= $Data_InfoCargosAna['_CantidadCursosPaso'];
                        }
                        if(isset($Data_InfoCargosAna['_CantidadCursosPersonas'])){
                            $cantidadCursosPersona += $Data_InfoCargosAna['_CantidadCursosPersonas'] ;
                        }
                        $colorZona = $Data_InfoCargosAna['zone'];
                    }//fin if
                }//fin foreach calculos vatos
                $porcentajeAvance = $cantidadCursosPersona > 0 ? number_format($cantidadCursosPaso/$cantidadCursosPersona*100,2) : 0;
                $promedioNotas = $contadorNota > 0 ? ($sumaNotas / $contadorNota) : 0;
                if( $numeroCursos > 0 ){
                        echo(utf8_decode($kzona.";".$numeroCursos.";".$numeroUsuarios.";".number_format($promedioNotas,1).";".$porcentajeAvance."\n"));
                }
            }//fin foreach zonas
            echo("\n");
            echo("PAIS;CANTIDAD CURSOS;CANTIDAD USUARIOS;PROMEDIO;AVANCE\n");
            $numeroCursos = 0;
            $numeroUsuarios = 0;
            $promedioNotas = 0;
            $sumaNotas = 0;
            $contadorNota = 0;
            $porcentajeAvance = 0;
            $cantidadCursosPaso = 0;
            $cantidadCursosPersona = 0;
            foreach ($CargosDatosListos as $key => $Data_InfoCargosAna) {
                if($Data_InfoCargosAna['charge_id'] == $kcargos['charge_id']){
                    if(isset($Data_InfoCargosAna['_CantidadCursos'])){
                        $numeroCursos = $Data_InfoCargosAna['_CantidadCursos'] > $numeroCursos ? $Data_InfoCargosAna['_CantidadCursos'] : $numeroCursos ;
                    }
                    if(isset($Data_InfoCargosAna['_PromedioXCargo'])){
                        $sumaNotas += $Data_InfoCargosAna['_PromedioXCargo'] ;
                        $contadorNota++;
                    }
                    if(isset($Data_InfoCargosAna['cantidadPersonas'])){
                        $numeroUsuarios += $Data_InfoCargosAna['cantidadPersonas'];
                    }
                    if(isset($Data_InfoCargosAna['_CantidadCursosPaso'])){
                        $cantidadCursosPaso+= $Data_InfoCargosAna['_CantidadCursosPaso'];
                    }
                    if(isset($Data_InfoCargosAna['_CantidadCursosPersonas'])){
                        $cantidadCursosPersona += $Data_InfoCargosAna['_CantidadCursosPersonas'] ;
                    }
                    $colorZona = $Data_InfoCargosAna['zone'];
                }
            }//fin foreach calculos vatos
            $porcentajeAvance = $cantidadCursosPersona > 0 ? number_format($cantidadCursosPaso/$cantidadCursosPersona*100,2) : 0;
            $promedioNotas = $contadorNota > 0 ? ($sumaNotas / $contadorNota) : 0;
            if( $numeroCursos > 0 ){
                    echo(utf8_decode("COLOMBIA;".$numeroCursos.";".$numeroUsuarios.";".number_format($promedioNotas,1).";".$porcentajeAvance."\n"));
            }
            echo("\n");
        }
    }//fin if
}
