<?php include('src/seguridad.php'); ?>
<?php include('controllers/excusas.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons list"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/excusas.php">Configuración Excusas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Excusas </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
										<h5><a href="excusas.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar Excusa</h4><?php } ?>
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar Excusa</h4><?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">
							<form id="formulario_data" method="post" autocomplete="off">
								<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-3 control-label" for="user_id" style="padding-top:8px;">Usuario:</label>
												<div class="col-md-9">
													<?php if ($_GET['opcn']=='editar'): ?>
														<select style="width: 100%;" id="user_id" name="user_id" onchange="CambiaUsuario();return false;">
														<?php foreach ($listadosUsuarios as $key => $Data_Usuarios) { ?>
															<option value="<?php echo($Data_Usuarios['user_id']); ?>" <?php if(isset($excusa['user_id']) && $excusa['user_id']==$Data_Usuarios['user_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Usuarios['identification'].' | '.$Data_Usuarios['first_name'].' '.$Data_Usuarios['last_name']); ?></option>
														<?php } ?>
														</select>
													<?php else: ?>
														<input id="user_id" name="user_id" type="text" value="" style="width: 100%;" onchange="CambiaUsuario();"/>
													<?php endif; ?>
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-7">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Programación:</label>
												<div class="col-md-10">
													<select style="width: 100%;" id="schedule_id" name="schedule_id">
													</select>
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									<br>
									<div class="row innerLR">
										<div class="col-md-5">
											<div class="form-group">
												<label class="col-md-3 control-label" for="type" style="padding-top:8px;">Tipo:</label>
												<div class="col-md-9">
													<select style="width: 100%;" id="type" name="type">
														<option value="cal_domestica" <?php if( isset($excusa) && isset($excusa['type']) && $excusa['type'] == "cal_domestica" ){ echo "selected"; } ?>>Calamidad domestica</option>
														<option value="inc_medica" <?php if( isset($excusa) && isset($excusa['type']) && $excusa['type'] == "inc_medica" ){ echo "selected"; } ?>>Incapacidad médica</option>
														<option value="retiro" <?php if( isset($excusa) && isset($excusa['type']) && $excusa['type'] == "retiro" ){ echo "selected"; } ?>>Retiro</option>
														<option value="otro" <?php if( isset($excusa) && isset($excusa['type']) && $excusa['type'] == "otro" ){ echo "selected"; } ?>>Otra</option>
													</select>
												</div>
											</div>
										</div>
										<!-- Column -->
										<div class="col-md-5">
											<div class="form-group" id="espacio_texto">
												<label class="col-md-3 control-label" for="description" style="padding-top:8px;">Observaciones:</label>
												<div class="col-md-9">
													<textarea class="form-control" name="description" id="description" rows="8" cols="80"><?php if( isset($excusa) && isset($excusa['description']) ){ echo $excusa['description']; } ?></textarea>
												</div>
											</div>
										</div>
										<!-- // Column END -->
									</div>
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<div class="col-md-5">
												<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
												  	<span class="btn btn-default btn-file">
												  		<span class="fileupload-new">Seleccionar Excusa (PDF Únicamente)</span>
												  		<span class="fileupload-exists">Cambiar Excusa</span>
													  	<input type="file" class="margin-none" id="image_new" name="image_new" />
													</span>
												  	<span class="fileupload-preview"></span>
												  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
												</div>
											</div>
										<!-- Column -->
										<div class="col-md-2">
										</div>
										<!-- // Column END -->
									</div>
									<br>

									<br>
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-10" style="<?php if( !isset($excusa) || !isset($excusa['file']) || $excusa['file'] == "" ){ echo "display:none"; } ?>">
											<iframe src="../assets/excusas/<?php echo $excusa['file']; ?>" style="width:700px; height:700px;" frameborder="0" id="pdf_view"></iframe>
										</div>
										<div class="col-md-1"></div>
									</div>
									<br>
									<?php if ( $_SESSION['max_rol'] >= 5 && $_GET['opcn'] == "editar" ): ?>

										<div class="row">
											<div class="col-md-5">
												<div class="form-group">
													<label class="col-md-2 control-label" for="excuse_status_id" style="padding-top:8px;">Estado:</label>
													<div class="col-md-10">
														<select style="width: 100%;" id="excuse_status_id" name="excuse_status_id">
															<option value="1" <?php if( $excusa['status_id'] == 1 ){ echo "selected"; } ?>>Aprobar</option>
															<option value="2" <?php if( $excusa['status_id'] == 2 ){ echo "selected"; } ?>>Pendiente</option>
															<option value="3" <?php if( $excusa['status_id'] == 3 ){ echo "selected"; } ?>>Rechazar</option>
														</select>
													</div>
												</div>

											</div>
											<!-- EXCUSAS PARA COBOROS O PAC -->
											<div class="col-md-5">
												<div class="form-group">
													<label class="col-md-2 control-label" for="excuse_aplica" style="padding-top:8px;">Aplica para:</label>
													<div class="col-md-10">
														<input type="hidden" id="aplica_para" name="aplica_para" value="EXCUSA" readonly>
														<!-- <select style="width: 100%;" id="excuse_aplica" name="excuse_aplica">
															<option value="" <?php if( $excusa['aplica_para'] == "" ){ echo "selected"; } ?>></option>
															<option value="COBRO" <?php if( $excusa['aplica_para'] == "COBRO" ){ echo "selected"; } ?>>COBRO</option>
															<option value="PAC" <?php if( $excusa['aplica_para'] == "PAC" ){ echo "selected"; } ?>>PAC</option>
														</select> -->
													</div>
												</div>
											</div>
										</div>
									<?php endif; ?>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Form actions -->
								<div class="form-actions">
									<input type="hidden" id="excuse_id" name="excuse_id" value="<?php echo isset($_GET['id']) ? $_GET['id'] : ""; ?>">
									<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
										<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
										<input type="hidden" id="opcn" name="opcn" value="crear">
										<button type="reset" name="button" id="btn_reset" style="display:none"></button>
									<?php } ?>
									<?php if ( isset($_GET['opcn']) && ($_GET['opcn']=='editar') ): ?>
										<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
										<input type="hidden" id="opcn" name="opcn" value="editar">
										<input type="hidden" id="invitation_id" name="invitation_id" value="<?php echo( $excusa['invitation_id'] ); ?>">
										<input type="hidden" id="module_result_usr_id" name="module_result_usr_id" value="<?php echo( $excusa['module_result_usr_id'] ); ?>">
									<?php endif; ?>
								</div>
								<!-- // Form actions END -->
							</form>
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->

				<!-- modal -->
				<a href="#modalRechazar" class="btn btn-success btn-xs" style="display: none;" data-toggle="modal" id="btnReabrirExcusax"><i class="fa fa-check"></i></a>
				<div class="modal fade" id="modalRechazar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cerrarBtnRExcusa">&times;</button>
								<h4 class="modal-title">Rechazar Excusa</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<h4 class="text-uppercase strong"><i class="fa fa-ticket text-regular fa-fw"></i> Rechazar Excusa</h4>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<!-- Row -->
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												<h5 class="text-uppercase strong"><i class="fa fa-ticket text-primary fa-fw"></i> Observaciones </h5>
												<textarea placeholder="Rechazado por: " class="form-control col-md-12 wysihtml5" id="excusa_text_r" name="excusa_text_r" rows="7"></textarea>
											</div>
										</div>

									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" name="" class="btn btn-primary" id="btnRechazarExcusa"> Rechazar </button>
								<button type="reset" name="" id="limpiarReabrir" style="display: none">limpiar</button>
							</div>
						</div>
					</div>
				</div>
				<!-- fin modal -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/excusas.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
