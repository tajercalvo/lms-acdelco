<?php include('src/seguridad.php'); ?>
<?php include('controllers/asignaciones.php');
$location = 'megaMenu';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/staff.php">Staff GM Academy</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Staff GM Academy</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<!-- Nuevo ROW-->
					<div class="row row-app">
						<div class="col-md-3">
							<div class="box-generic padding-none margin-none overflow-hidden">
								<div class="relativeWrap overflow-hidden" style="height:400px">
									<img src="../assets/images/staff/nicolas_staff.jpg" alt="" class="img-responsive padding-none border-none" />
									<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
										<div class="media margin-none innerAll">
											<div class="media-body text-white">
												<strong>Nicolás Soto Barros</strong>
												<p class="text-small margin-none"><i class="fa fa-fw fa-user"></i> Director GM Academy</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="box-generic padding-none margin-none overflow-hidden">
								<div class="relativeWrap overflow-hidden" style="height:400px">
									<img src="../assets/images/staff/zoraya_staff.jpg" alt="" class="img-responsive padding-none border-none" />
									<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
										<div class="media margin-none innerAll">
											<div class="media-body text-white">
												<strong>Zoraya Ordoñez</strong>
												<p class="text-small margin-none"><i class="fa fa-fw fa-user"></i> Analista Estratégico GMAcademy</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="box-generic padding-none margin-none overflow-hidden">
								<div class="relativeWrap overflow-hidden" style="height:400px">
									<img src="../assets/images/staff/leidy_staff.jpg" alt="" class="img-responsive padding-none border-none" />
									<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
										<div class="media margin-none innerAll">
											<div class="media-body text-white">
												<strong>Leidy Rojas</strong>
												<p class="text-small margin-none"><i class="fa fa-fw fa-user"></i> Tutor GM Academy</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="box-generic padding-none margin-none overflow-hidden">
								<div class="relativeWrap overflow-hidden" style="height:400px">
									<img src="../assets/images/staff/Duvan_staff.jpg" alt="" class="img-responsive padding-none border-none" />
									<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
										<div class="media margin-none innerAll">
											<div class="media-body text-white">
												<strong>Duvan Morales</strong>
												<p class="text-small margin-none"><i class="fa fa-fw fa-user"></i> Coordinador de Entrenamiento</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator "></div>
					<!-- Nuevo ROW-->
					<div class="row row-app">
						<div class="col-md-3">
							<div class="box-generic padding-none margin-none overflow-hidden">
								<div class="relativeWrap overflow-hidden" style="height:400px">
									<img src="../assets/images/staff/Diosa_Staff.jpg" alt="" class="img-responsive padding-none border-none" />
									<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
										<div class="media margin-none innerAll">
											<div class="media-body text-white">
												<strong>Diosa Malaver</strong>
												<p class="text-small margin-none"><i class="fa fa-fw fa-user"></i> Asistente de Entrenamiento</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="box-generic padding-none margin-none overflow-hidden">
								<div class="relativeWrap overflow-hidden" style="height:400px">
									<img src="../assets/images/staff/yuli_staff.jpg" alt="" class="img-responsive padding-none border-none" />
									<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
										<div class="media margin-none innerAll">
											<div class="media-body text-white">
												<strong>Yuli Galindo</strong>
												<p class="text-small margin-none"><i class="fa fa-fw fa-user"></i> Apoyo logístico</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="box-generic padding-none margin-none overflow-hidden">
								<div class="relativeWrap overflow-hidden" style="height:400px">
									<img src="../assets/images/staff/andres_staff.jpg" alt="" class="img-responsive padding-none border-none" />
									<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
										<div class="media margin-none innerAll">
											<div class="media-body text-white">
												<strong>Andrés Vega</strong>
												<p class="text-small margin-none"><i class="fa fa-fw fa-wrench"></i> Soporte Primario</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row row-app">
							<div class="col-md-3">
								<div class="box-generic padding-none margin-none overflow-hidden">
									<div class="relativeWrap overflow-hidden" style="height:400px">
										<img src="../assets/images/staff/diego_staff.jpg" alt="" class="img-responsive padding-none border-none" />
										<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
											<div class="media margin-none innerAll">
												<div class="media-body text-white">
													<strong>Diego A Lamprea</strong>
													<p class="text-small margin-none"><i class="fa fa-fw fa-wrench"></i> Soporte Especializado</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator "></div>
					<!-- Nuevo ROW-->

					<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/asignaciones.js"></script>
</body>
</html>
