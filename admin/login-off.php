<?php
include('../config/config.php');
@session_start();
if( isset($_SESSION['id_evaluacion']) && $_SESSION['id_evaluacion'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: evaluar.php");
}else if( isset($_SESSION['_EvalCour_ResultId']) && $_SESSION['_EvalCour_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: evaluar.php");
}else if( isset($_SESSION['_EncSat_ResultId']) && $_SESSION['_EncSat_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuestar.php");
}else if( isset($_SESSION['_EnCarg_ReviewId']) && $_SESSION['_EnCarg_ReviewId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuestar_Cargo.php");
}else if( isset($_SESSION['evl_obligatoria']) && $_SESSION['evl_obligatoria'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuesta_gerentes.php");
}elseif(isset($_SESSION['NameUsuario'])){
	if( isset($_SESSION['url_solicitada'] ) and !strpos($_SESSION['url_solicitada'], 'login') ){
		header("location: ".$_SESSION['url_solicitada']);
	}else{
		header("location: ../admin/");
	}
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
    <?php include('src/head_login.php'); ?>
</head>
<body class="document-body login">
	<!-- Wrapper -->
<div id="login">
	<div class="container">
		<div class="row">
			<div class="col-md-8"></div>
			<div class="col-md-4" style="background-color: rgba(255, 255, 255, 0.7);">
				<div class="wrapper">
					<h1>
						<p><img src="../assets/images/wall/logo_ludus.png" width="100px"></p>
						<p><img src="../assets/images/wall/chevrolet_logo.png" width="384px"></p>
					</h1>
					<!-- Box -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<!-- Form -->
							<form action="controllers/usuarios.php" method="post" name="login" id="login" onSubmit="return validateForm();"class="row">
								<label>Usuario</label>
								<input type="text" class="input-block-level form-control" placeholder="Nombre de usuario asignado en Chevrolet" id="username" name="username"/>
								<label>Contrase&ntilde;a</label>
								<input type="hidden" name="opcn" id="opcn" value="login">
								<input type="password" class="input-block-level form-control margin-none" placeholder="Su contraseña asignada" id="password" name="password"/><br/>
								<label><a class="password" href="recordar_pss">Lo ha olvidado?</a></label>
								<div class="separator bottom"></div>
									<div class="col-md-8 padding-none ">
										<!--<div class="uniformjs innerL"><label class="checkbox"><input type="checkbox" value="remember-me" title="Se recordar&aacute; en este equipo">Recordarme</label></div>-->
									</div>
									<div class="col-md-4 pull-right padding-none">
										<button class="btn btn-block btn-primary" type="submit">Ingresar</button>
									</div>
							</form>
							<!-- // Form END -->
						</div>
						<div class="widget-footer" id="Error_gral">
							<p class="glyphicons restart"><i></i>Por favor ingrese un usuario o contrase&ntilde;a validos ...</p>
						</div>
					</div>
					<!-- // Box END -->
					<div class="innerT center">
						<!--<a href="signup.html?lang=es" class="btn btn-icon-stacked btn-block btn-success glyphicons user_add"><i></i><span>Don't have an account?</span><span class="strong">Sign up</span></a>
						<p class="innerT">Alternatively</p>
						<a href="index.html?lang=es" class="btn btn-icon-stacked btn-block btn-facebook glyphicons-social facebook"><i></i><span>Join using your</span><span class="strong">Facebook Account</span></a>
						<p>or</p>
						<a href="index.html?lang=es" class="btn btn-icon-stacked btn-block btn-google glyphicons-social google_plus"><i></i><span>Join using your</span><span class="strong">Google Account</span></a>-->
						<p>Tiene problemas? <a href="mailto:comunicaciones@gmacademy.co" target="_blank">Obtenga ayuda</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- // Wrapper END -->
<?php include('src/boot_login.php'); ?>
<script src="js/login.js"></script>
<?php if(isset($_SESSION['errorLogin'])){ ?>
	<script language="javascript">
		$(document).ready(function(){
			$('#Error_gral').css( "border-color", "#b94a48" );
			$('#Error_gral').show();
		});
	</script>
<?php unset($_SESSION['errorLogin']); } ?>
<!--
<div id="footer" class="hidden-print" style="margin-top:60px;">-->
	<!--  Copyright Line -->
	<!--<div class="copy">&copy; <?php echo date("Y"); ?> - <a href="http://www.doortraining.com.co/" target="_blank">Door Training Colombia</a> - Todos los derechos reservados - Versión Actual: v2.0.0 </div>-->
	<!--  End Copyright Line -->
<!--</div>-->
<!-- // Footer END -->
</body>
</html>