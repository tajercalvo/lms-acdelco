<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_logistica.php');
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_Logistica.csv');
if(isset($_GET['start_date_day'])){
    echo utf8_decode("PROGRAMACIÓN;CÓDIGO;CURSO;INICIO;FINALIZACIÓN;TAMANO MINIMO;TAMANO MÁXIMO;LUGAR;CONCESIONARIO;SEDE;INSCRITOS;\n");
    if($cantidad_datos > 0){
        foreach ($datosLogistica_all as $iID => $data) {
            echo(utf8_decode($data['schedule_id'].";".$data['newcode'].";".$data['course'].";".$data['start_date'].";".$data['end_date'].";".$data['min_size'].";".$data['max_size'].";".$data['living'].";".$data['headquarter'].";".$data['dealer'].";".$data['inscriptions'].";\n"));
        }
    }
}
