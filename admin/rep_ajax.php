<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_ajax.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia_total.php">Reporte de Asistencia Total</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Asistencia Total </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de los inscritos por cada curso en los programadas definidos entre 2 fechas.</h5></br>
			</div>
			<div class="col-md-2">
<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion" enctype="multipart/form-data">
<!-- <p id="descargar_excel"><a href="#" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte por JS</a></p> -->
<p id="descargar">Descarga Ajax</p>
<p id="descargar_excel">Descarga Excel</p>
<!-- El contenido de la tabla se le agrega a este input oculto, luego este es enviado al archivo ficheroExcel.php que procede a hacer la descarga -->
<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Reporte ajax" />
<input type="hidden" id="datos_tabla" name="datos_tabla" />
</form>

			</div>
		</div>
		<div class="row">
			<form action="rep_asistencia_total.php" method="post">
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
					<div class="col-md-7 input-group date">
				    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" value="" >
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Final:</label>
					<div class="col-md-7 input-group date">
				    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" />
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
			<h1 class="btn btn-success" id="consulta"><i class="fa fa-check-circle"></i>consulta</h1>
				<!-- <button type="submit" class="btn btn-success" id="id="consulta""><i class="fa fa-check-circle"></i> Consultar</button> -->
			</div>
			<div class="col-md-10">
				<div class="form-group">
					<label class="col-md-5 control-label" for="cursosF" style="padding-top:8px;">Curso :</label>
					<select style="width: 100%;" id="cursosF" name="cursosF">
						<option value="0">Todos los Cursos</option>
						<?php foreach ($listaCursos as $key => $valueCursos) { ?>
							<option value="<?php echo($valueCursos['course_id']); ?>"><?php echo($valueCursos['course']); ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<div id='progreso_visible' style="visibility: hidden;" class="widget">
	<div  class="widget-head progress" id="widget-progress-bar">
		<div id="barra_progreso" class="progress-bar progress-bar-primary" style="width: 0%;"> <strong></strong> <strong class="steps-percent"></strong></div>
	</div>	
</div>
<h4 id="descargando_reporte"><i class="fa fa-fw icon-cloud-download"></i></h2>
		<!-- // Total elements END -->
		<!--  -->
			<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
				<div  id="miTabla_descarga" name = "datos_a_enviar">
				<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
			</form>
		<!--  -->
<section id="miTablaaa">
		</section>

		<span id="cargando" style="visibility: hidden;" class="label label-success">Realizando la consulta, por favor espere…</span><br><br>
		<!-- <div id="mostrarmas">sisisiis</div> -->
		 <!-- <h1 class="btn btn-success" id="mostrarmas" style="visibility: hidden;"><i class="fa fa-check-circle"></i>Mostrar mas</h1> -->
		 <input type="hidden" id="cantidad_cursos" value="<?php echo $cantidad_cursos ?>">
		 
		</div>

		
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_ajax.js"></script>
</body>
</html>