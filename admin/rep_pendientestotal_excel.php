<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['course_id'])){
    include('controllers/rep_pendientestotal.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_PendientesCursoTotal.xls");
if(isset($_GET['course_id'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">C&Oacute;DIGO</th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">TIPO</th>
                <th data-hide="phone,tablet">CANTIDAD</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php 
            if($cantidad_datos > 0){ 
                    foreach ($datosCursos_all as $iID => $data) { 
                        ?>
                <tr>
                    <td><?php echo(utf8_decode($data['newcode'])); ?></td>
                    <td><?php echo(utf8_decode($data['course'])); ?></td>
                    <td><?php echo($data['type']); ?></td>
                    <td><?php echo($data['cant_usr']); ?></td>
                </tr>
            <?php } } ?>
        </tbody>
    </table>
<?php } ?>