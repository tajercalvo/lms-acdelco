<?php include('src/seguridad.php'); ?>
<?php //include('controllers/index.php'); 
$location = 'home';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
		<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
				<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		<?php include('src/menu.php'); ?>
		<!-- Content -->
		<div id="content">
		<?php include('src/top_nav_bar.php'); ?>
		<!-- Camino de Hormigas -->
		<ul class="breadcrumb">
			<li>Estás aquí</li>
			<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
			<li>Dashboard</li>
		</ul>
		<!-- Camino de Hormigas -->
		<!-- Búsqueda -->
		<div class="innerLR">
			<div class="widget-search form-inline">
			 	<div class="input-group">
		     		<input type="text" value=""  class="form-control" placeholder="Digíte la(s) palabra(s) clave(s) de su búsqueda ..">
		      		<span class="input-group-btn">
		       			<button type="button" class="btn btn-primary">Buscar <i class="fa fa-search"></i></button>
		      		</span>
				</div>
			</div>
		</div>
		<!-- Búsqueda -->

<div class="innerAll">
	<h1 class="margin-none pull-left">Dashboard &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h1>
	<div class="btn-group pull-right">
		<a href="dashboard_analytics.html?lang=es" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Análisis</a>
		<a href="dashboard_users.html?lang=es" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Mis Datos</a>
		<a href="dashboard_overview.html?lang=es" class="btn btn-default"><i class="fa fa-fw fa-dashboard"></i> Mis Clases</a>
	</div>
	<div class="clearfix"></div>
</div>

<!-- Stats Widgets -->
<div class="row border-top border-bottom row-merge margin-none">
	
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-default widget-stats-4">
	<span class="txt">Meta Semestral</span>
	<span class="count">4.3</span>
	<span class="glyphicons cup"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->
		</div>
	</div>
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-primary widget-stats-4">
	<span class="txt">Progreso General</span>
	<span class="count">58%</span>
	<span class="glyphicons refresh"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->
		</div>
	</div>
	<div class="col-md-3 border-right innerAll">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-gray widget-stats-4">
	<span class="txt">Alumnos</span>
	<span class="count">25<span>Hoy</span></span>
	<span class="glyphicons user"><i></i></span>
	<div class="clearfix"></div>
	<i class="icon-play-circle"></i>
</a>
<!-- // Stats Widget END -->
		</div>
	</div>
	<div class="col-md-3">
		<div class=" padding-bottom-none-phone">
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-2">
	<span class="count">125</span>
	<span class="txt">Cursos en línea</span>
</a>
<!-- // Stats Widget END -->
		</div>
	</div>
</div>
<!-- // Stats Widgets END -->
<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
<div class="innerLR innerB">
	<div class="row">
		<div class="col-md-9 tablet-column-reset">
			<div class="widget widget-inverse">
	<div class="widget-head">
		<h4 class="heading"><i class="fa fa-sitemap fa-fw"></i> Mi línea de tiempo</h4>
	</div>
	<div class="widget-body padding-none">
		
		<div class="relativeWrap overflow-hidden">
		<div class="row row-merge layout-timeline layout-timeline-mirror">
			<div class="col-sm-6"></div>
			<div class="col-sm-6">
				<div class="innerAll">
					
					<ul class="timeline">
						<li class="active">
							<div class="separator bottom">
								<span class="date box-generic">Ahora</span>
								<span class="type glyphicons suitcase">Clases <i></i><span class="time">10:00</span></span>
							</div>
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<div class="media">
										<div class="media-object pull-left thumb"><img src="../assets//images/avatar-51x51.jpg" alt="Image" /></div>
										<div class="media-body">
											<a class="author">Ricardo Osorio</a><br/>
											<span class="muted">ricardo.osorio@estrellaies.com</span>
										</div>
									</div>
									<div class="alert alert-gray">
										<p class="glyphicons circle_info margin-none"><i></i> Se ha completado la revisión del material de Camiones <a href="#">Ludus LMS</a></p>
									</div>
									<a class="glyphicons single pencil"><i></i></a>
								</div>
							</div>
						</li>
						<li class="active">
							<span class="type glyphicons comments">Cursos <i></i><span class="time">09:00</span></span>
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<p class="glyphicons user margin-none"><i></i> <strong><a href="">LudusLMS</a> dice</strong>: Felicitaciones ha completado la actividad de formación virtual <strong>Camiones</strong> en el plan de formación <strong>Asesor Comercial GM</strong> ...</p>
								</div>
							</div>
						</li>
						<li>
							<div class="separator">
								<span class="date box-generic"><strong>15</strong>Sep</span>
								<span class="type glyphicons picture">Archivos <i></i><span class="time">12/09/2015 10:45</span></span>
							</div>
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<div class="media">
										<div class="media-object pull-left thumb"><img src="../assets//images/avatar-51x51.jpg" alt="Image" /></div>
										<div class="media-body">
											<a class="author">Ricardo Osorio</a><br/>
											<span class="muted">ricardo.osorio@estrellaies.com</span>
										</div>
									</div>
									<div class="alert alert-gray">
										<p class="glyphicons circle_info"><i></i> Ha agregado material al curso <a href="">Camiones</a></p>
										
										<!-- Gallery Layout -->
										<div class="gallery gallery-2 separator top">
											<ul class="row" data-toggle="modal-gallery" data-target="#modal-gallery" id="gallery-4" data-delegate="#gallery-4">
																								<li class="col-md-4 hidden-phone"><a class="thumb" href="../assets/images/gallery-2/6.jpg" data-gallery="gallery"><img src="../assets/images/gallery-2/6.jpg" alt="photo" class="img-responsive" /></a></li>
																								<li class="col-md-4 hidden-phone"><a class="thumb" href="../assets/images/gallery-2/5.jpg" data-gallery="gallery"><img src="../assets/images/gallery-2/5.jpg" alt="photo" class="img-responsive" /></a></li>
																								<li class="col-md-4 hidden-phone"><a class="thumb" href="../assets/images/gallery-2/4.jpg" data-gallery="gallery"><img src="../assets/images/gallery-2/4.jpg" alt="photo" class="img-responsive" /></a></li>
																								<li class="col-md-4"><a class="thumb" href="../assets/images/gallery-2/3.jpg" data-gallery="gallery"><img src="../assets/images/gallery-2/3.jpg" alt="photo" class="img-responsive" /></a></li>
																								<li class="col-md-4"><a class="thumb" href="../assets/images/gallery-2/2.jpg" data-gallery="gallery"><img src="../assets/images/gallery-2/2.jpg" alt="photo" class="img-responsive" /></a></li>
																								<li class="col-md-4"><a class="thumb" href="../assets/images/gallery-2/1.jpg" data-gallery="gallery"><img src="../assets/images/gallery-2/1.jpg" alt="photo" class="img-responsive" /></a></li>
																							</ul>
										</div>
										<!-- // Gallery Layout END -->
										
									</div>
									<a class="glyphicons single pencil"><i></i></a>
								</div>
							</div>
						</li>
					</ul>
					
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
			<div class="separator bottom"></div>
			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
			<!-- Widget -->
			<h2 class="margin-none separator bottom"><i class="fa fa-group text-primary fa-fw"></i> Mis Compañeros</h2>
<div class="widget widget-heading-simple widget-body-gray widget-employees">
	<div class="widget-body padding-none">
		
		<div class="row">
			<div class="col-md-4 listWrapper">
				<div class="innerAll">
					<form autocomplete="off" class="form-inline">
						<div class="widget-search separator bottom">
							<button type="button" class="btn btn-default pull-right"><i class="fa fa-search"></i></button>
							<div class="overflow-hidden">
								<input type="text" value="" class="form-control" placeholder="Buscar alguien ..">
							</div>
						</div>
						<select style="width: 100%;">
			               <optgroup label="Tipos de usuario">
				               	<option value="design">Administradores</option>
				                <option value="design">Alumnos</option>
				                <option value="development">Profesores</option>
			               </optgroup>
						</select>
					</form>
				</div>
				<span class="results">2 Compañeros encontrados <i class="fa fa-circle-arrow-down"></i></span>
				<ul class="list unstyled">
					<li>
						<div class="media innerAll">
							<div class="media-object pull-left thumb hidden-phone"><img src="../assets/images/avatar-51x51.jpg" alt="Ricardo Osorio" /></div>
							<div class="media-body">
								<span class="strong">Ricardo Osorio</span>
								<span class="muted">ricardo.osorio@doortraining.com.co</span>
								<i class="fa fa-envelope"></i>
								<i class="fa fa-phone"></i> 
								<i class="fa fa-skype"></i> 
							</div>
						</div>
					</li>
					<li class="active">
						<div class="media innerAll">
							<div class="media-object pull-left thumb hidden-phone"><img src="../assets/images/avatar-51x51_1.jpg" alt="Alfonso Albarracín" /></div>
							<div class="media-body">
								<span class="strong">Alfonso Albarracín</span>
								<span class="muted">alfonso.albarracin@doortraining.com.co</span>
								<i class="fa fa-envelope"></i>
								<i class="fa fa-phone"></i> 
								<i class="fa fa-skype"></i> 
							</div>
						</div>
					</li>
					<li>
						<div class="media innerAll">
							<div class="media-object pull-left thumb hidden-phone"><img src="../assets/images/avatar-51x51_2.jpg" alt="Jennifer del Busto" /></div>
							<div class="media-body">
								<span class="strong">Jennifer del Busto</span>
								<span class="muted">jennifer.delbusto@doortraining.com.co</span>
								<i class="fa fa-envelope"></i>
								<i class="fa fa-phone"></i> 
								<i class="fa fa-skype"></i> 
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-md-8 detailsWrapper">
				<div class="ajax-loading hide">
					<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
				</div>
				<div class="innerAll">
					<div class="title">
						<div class="row">
							<div class="col-md-8">
								<h3 class="text-primary">Ricardo Osorio</h3>
								<span class="muted">Administrador / Profesor / Alumno</span>
							</div>
							<div class="col-md-4 text-right">
								<p class="muted">4 cursos en proceso <a href=""><i class="fa fa-circle-arrow-right"></i></a></p>
								<div class="margin-bottom-none progress progress-small progress-inverse count-outside">
									<div class="count">72%</div>
									<div class="progress-bar" role="progressbar" aria-valuenow="72" style="width: 72%;"></div>
								</div>
							</div>
						</div>
					</div>
					<hr/>
					<div class="body">
						<div class="row">
							<div class="col-md-4 overflow-hidden">
								<!-- Profile Photo -->
								<div><a href="" class="thumb inline-block"><img src="../assets/images/avatar-2-large.jpg" alt="Profile" /></a></div>
								<div class="separator bottom"></div>
								<!-- // Profile Photo END -->
								<ul class="icons-ul separator bottom">
									<li><i class="fa fa-envelope fa fa-li fa-fw"></i> ricardo.osorio@doortraining.com.co</li>
									<li><i class="fa fa-phone fa fa-li fa-fw"></i> +57 325 45 78</li>
									<li><i class="fa fa-skype fa fa-li fa-fw"></i> rosorio57</li>
								</ul>
							</div>
							<div class="col-md-8 padding-none">
								<h5 class="strong">Acerca de</h5>
								<p>Gerente gral de Door Training Colombia, cuenta con una amplia experiencia en la coordinación de planes de capacitación general en empresas de la industria automotriz; profesional en administración con especialización en gerencia automotriz (12 años de experiencia).</p>
								<div class="row">
									<div class="col-md-4 padding-none">
										<h5 class="strong">Análisis</h5>
										<a href="" class="btn btn-primary btn-small btn-block"><i class="fa fa-download-alt fa-fw"></i> Sep</a>
										<a href="" class="btn btn-inverse btn-small btn-block"><i class="fa fa-download-alt fa-fw"></i> Ago</a>
										<a href="" class="btn btn-inverse btn-small btn-block"><i class="fa fa-download-alt fa-fw"></i> Jul</a>
										<div class="separator bottom"></div>
									</div>

									<div class="col-md-7 col-md-offset-1 padding-none">
										<h5 class="strong">Habilidades</h5>
										<div class="progress progress-mini count-outside-primary add-outside">
											<div class="count">100%</div>
											<div class="progress-bar progress-bar-primary" style="width: 100%;"></div>
											<div class="add">RS001</div>
										</div>
										<div class="progress progress-mini count-outside-primary add-outside">
											<div class="count">75%</div>
											<div class="progress-bar progress-bar-primary" style="width: 75%;"></div>
											<div class="add">RS003</div>
										</div>
										<div class="progress progress-mini count-outside-primary add-outside">
											<div class="count">93%</div>
											<div class="progress-bar progress-bar-primary" style="width: 93%;"></div>
											<div class="add">RS008</div>
										</div>
										<div class="progress progress-mini count-outside-primary add-outside">
											<div class="count">79%</div>
											<div class="progress-bar progress-bar-primary" style="width: 79%;"></div>
											<div class="add">RS025</div>
										</div>
										<div class="progress progress-mini count-outside add-outside">
											<div class="count">58%</div>
											<div class="progress-bar" style="width: 58%;"></div>
											<div class="add">RS012</div>
										</div>

										<div class="separator bottom"></div>
									</div>
								</div>
								<h5 class="text-uppercase strong text-primary"><i class="fa fa-group text-regular fa-fw"></i> LUDUS LMS <span class="text-lowercase strong padding-none">Equipo</span> <span class="text-lowercase padding-none">(2 personas)</span></h5>
								<ul class="team">
									<li><span class="crt">1</span><span class="strong">Alfonso Albarracín</span><span class="muted">Profesor Senior</span></li>
									<li><span class="crt">2</span><span class="strong">Jennifer del Busto</span><span class="muted">Diseñador Senior</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!-- // Widget END -->
			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
			
			<!-- Widget -->
			<h2 class="margin-none separator bottom"><i class="fa fa-time-clock text-primary icon-fixed-width"></i> Actividad Reciente</h2>
			
			<!-- Stats Widgets -->
			<div class="row">
				<div class="col-md-3">
					<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-default widget-stats-5">
	<span class="glyphicons cars"><i></i></span>
	<span class="txt">Materiales<span>gasto</span></span>
	<div class="clearfix"></div>
</a>
<!-- // Stats Widget END -->

				</div>
				<div class="col-md-3">
					<!-- Stats Widget -->
<a href="courses_listing.html?lang=es" class="widget-stats widget-stats-info widget-stats-5">
	<span class="glyphicons notes"><i></i></span>
	<span class="txt">Mis<span>cursos</span></span>
	<div class="clearfix"></div>
</a>
<!-- // Stats Widget END -->

				</div>
				<div class="col-md-3">
					<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-inverse widget-stats-5">
	<span class="glyphicons charts"><i></i></span>
	<span class="txt">Mis<span>Datos</span></span>
	<div class="clearfix"></div>
</a>
<!-- // Stats Widget END -->

				</div>
				<div class="col-md-3">
					<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-primary widget-stats-5">
	<span class="glyphicons certificate"><i></i></span>
	<span class="txt">Mis<span>Certificados</span></span>
	<div class="clearfix"></div>
</a>
<!-- // Stats Widget END -->

				</div>
				
			</div>
			<div class="separator bottom"></div>
			<!-- // Stats Widgets END -->
			
			<!-- Widget -->
<div class="widget widget-heading-simple widget-body-simple">
		
	<!-- Widget Heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons cardio"><i></i>Actividad Detallada</h4>
	</div>
	<!-- // Widget Heading END -->
	
	<div class="widget-body">

		<div class="widget widget-tabs widget-tabs-icons-only-2 widget-activity margin-none">

			<!-- Tabs Heading -->
			<div class="widget-head">
				<ul>
					<li class="active"><a class="glyphicons user_add" data-toggle="tab" href="#filterUsersTab"><i></i>Acciones</a></li>
					<li><a class="glyphicons cars" data-toggle="tab" href="#filterOrdersTab"><i></i>Mi Grupo</a></li>
					<li><a class="glyphicons link" data-toggle="tab" href="#filterLinksTab"><i></i></a></li>
				</ul>
			</div>
			<!-- // Tabs Heading END -->
			
			<div class="widget-body">
				<div class="tab-content">
						
										
					<!-- Filter Users Tab -->
					<div class="tab-pane active" id="filterUsersTab">
						<ul class="list">
						
														<!-- Activity item -->
							<li>
								<span class="date">18/09</span>
								<span class="glyphicons activity-icon user_add"><i></i></span>
								<span class="ellipsis"><a href="">Ricardo Osorio</a> calificó a <a href="">Jennifer del Busto</a> 10.</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">18/09</span>
								<span class="glyphicons activity-icon user_add"><i></i></span>
								<span class="ellipsis"><a href="">Ricardo Osorio</a> calificó a <a href="">Alfonso Albarracín</a> 9.5.</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">18/09</span>
								<span class="glyphicons activity-icon user_add"><i></i></span>
								<span class="ellipsis"><a href="">Ricardo Osorio</a> revisó el material <a href="">Curso Camiones</a> OK.</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">18/09</span>
								<span class="glyphicons activity-icon user_add"><i></i></span>
								<span class="ellipsis"><a href="">Alfonso Albarracín</a> escribió a <a href="">Ricardo Osorio</a> ver.</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														
						</ul>
					</div>
					<!-- // Filter Users Tab END -->
					
					<!-- Filter Orders Tab -->
					<div class="tab-pane" id="filterOrdersTab">
						<ul class="list">
						
														<!-- Activity item -->
							<li>
								<span class="date">18/09</span>
								<span class="glyphicons activity-icon cars"><i></i></span>
								<span class="ellipsis"><a href="">Jennifer del Busto</a> 2 poleas de 5/8 (<a href="">Orden #2301</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">18/09</span>
								<span class="glyphicons activity-icon cars"><i></i></span>
								<span class="ellipsis"><a href="">Alfonso Albarracín</a> 1 motor diesel (<a href="">Orden #2302</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">18/09</span>
								<span class="glyphicons activity-icon cars"><i></i></span>
								<span class="ellipsis"><a href="">Jennifer del Busto</a> 5 Carter con lubricación (<a href="">Orden #2303</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">17/09</span>
								<span class="glyphicons activity-icon cars"><i></i></span>
								<span class="ellipsis"><a href="">Alfonso Albarracín</a> 1 motor diesel (<a href="">Orden #2304</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">15/09</span>
								<span class="glyphicons activity-icon cars"><i></i></span>
								<span class="ellipsis"><a href="">Jennifer del Busto</a> 5 Carter con lubricación (<a href="">Orden #2305</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														
						</ul>
					</div>
					<!-- // Filter Orders Tab END -->
					
					<!-- Filter Links Tab -->
					<div class="tab-pane" id="filterLinksTab">
						<ul class="list">
						
														<!-- Activity item -->
							<li>
								<span class="date">21/07</span>
								<span class="glyphicons activity-icon link"><i></i></span>
								<span class="ellipsis"><a href="">Ricardo Osorio</a> Hangout con personal de méxico (<a href="">grabación #2301</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">21/07</span>
								<span class="glyphicons activity-icon link"><i></i></span>
								<span class="ellipsis"><a href="">Alfonso Albarracín</a> Hangout con personal de Ecuador (<a href="">gabación #2302</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														<!-- Activity item -->
							<li>
								<span class="date">21/07</span>
								<span class="glyphicons activity-icon link"><i></i></span>
								<span class="ellipsis"><a href="">Jennifer del Busto</a> Documentación curso camiones (<a href="">material #2303</a>)</span>
								<div class="clearfix"></div>
							</li>
							<!-- // Activity item END -->
														
						</ul>
					</div>
					<!-- // Filter Links Tab END -->
				
				</div>
			</div>
		</div>
		
	</div>
</div>

<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
		
			<h2 class="margin-none separator bottom">Nuevos Cursos</h2>
			<div class="row">
				<div class="col-md-6">
					<!-- Special Offers Widget -->
					<div class="widget widget-heading-simple widget-body-gray widget-offers">
						<!-- Widget Heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons gift"><i></i>Septiembre</h4>
						</div>
						<!-- // Widget Heading END -->
						<div class="widget-body">
							<!-- Media item -->
							<div class="media">
								<img class="media-object pull-left thumb hidden-tablet hidden-phone" data-src="holder.js/100x100/white" alt="Image" />
								<div class="media-body">
									<h5><a href="courses_listing.html?lang=es">Curso Diesel - Mecánica</a></h5>
									<p>Curso para completar el título como mecánico intermedio diesel, se impartirá en GM Colmotores Bogotá Colombia</p>
									<div class="row">
										<div class="col-md-6 tablet-column-reset">
											<span class="price">Inscritos: <strong>12 (20 Cupos)</strong></span>
										</div>
										<div class="col-md-6 text-right tablet-column-reset">
											<button type="button" class="btn btn-inverse btn-icon glyphicons pen btn-sm"><i></i>Inscribirse</button>
										</div>
									</div>
								</div>
							</div>
							<!-- // Media item END -->
						</div>
					</div>
					<!-- // Special Offers Widget END -->
									</div>
									<div class="col-md-6">
					<!-- Special Offers Widget -->
					<div class="widget widget-heading-simple widget-body-gray widget-offers">
						<!-- Widget Heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons gift"><i></i>Octubre</h4>
						</div>
						<!-- // Widget Heading END -->
						<div class="widget-body">
							<!-- Media item -->
							<div class="media">
								<img class="media-object pull-left thumb hidden-tablet hidden-phone" data-src="holder.js/100x100/white" alt="Image" />
								<div class="media-body">
									<h5><a href="courses_listing.html?lang=es">Curso Diesel - Electricidad</a></h5>
									<p>Curso para completar el título como mecánico intermedio diesel, se impartirá en GM Colmotores Bogotá Colombia</p>
									<div class="row">
										<div class="col-md-6 tablet-column-reset">
											<span class="price">Inscritos: <strong>12 (18 Cupos)</strong></span>
										</div>
										<div class="col-md-6 text-right tablet-column-reset">
											<button type="button" class="btn btn-inverse btn-icon glyphicons pen btn-sm"><i></i>Inscribirse</button>
										</div>
									</div>
								</div>
							</div>
							<!-- // Media item END -->
						</div>
					</div>
					<!-- // Special Offers Widget END -->
				</div>
			</div>
			
			
	
		</div>
		<div class="col-md-3 tablet-column-reset">
		
			<h2 class="margin-none separator bottom"><i class="fa fa-exclamation-circle innerR text-primary"></i>Estadísticas</h2>
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-2 widget-stats-easy-pie widget-stats-primary">
	<div data-percent="23" class="easy-pie primary easyPieChart"><span class="value">45</span>%</div>
	<span class="txt"><span class="count text-large inline-block">120</span> cursos</span>
	<div class="clearfix"></div>
</a>
<!-- // Stats Widget END -->


			<div class="separator bottom"></div>
			<!-- Stats Widget -->
<a href="" class="widget-stats widget-stats-2 widget-stats-gray widget-stats-easy-pie txt-single">
	<div data-percent="35" class="easy-pie primary easyPieChart"><span class="value">35</span>%</div>
	<span class="txt">Asistencia clases</span>
	<div class="clearfix"></div>
</a>
<!-- // Stats Widget END -->


			<div class="separator bottom"></div>
			<a href="" class="widget-stats widget-stats-2">
				<div class="sparkline"></div>
				<span class="txt"><span class="count text-large text-primary inline-block">50</span> AVG En línea</span>
				<div class="clearfix"></div>
			</a>
			
			<p class="separator text-center"><i class="fa fa-ellipsis-h fa-3x"></i></p>
			
			<!-- Activity/List Widget -->
<h2 class="margin-none separator bottom"><i class="fa fa-signal text-primary icon-fixed-width"></i> Registros</h2>
<div class="widget widget-heading-simple widget-body-gray" data-toggle="collapse-widget">
	<div class="widget-body list">
		<ul>
		
			<!-- List item -->
			<li>
				<span>Cursos Dictados</span>
				<span class="count">55</span>
			</li>
			<!-- // List item END -->
			
			<!-- List item -->
			<li>
				<span>Inasistencias</span>
				<span class="count">2</span>
			</li>
			<!-- // List item END -->
			
			<!-- List item -->
			<li>
				<span>Cursos Completados</span>
				<span class="count">1</span>
			</li>
			<!-- // List item END -->
			
			<!-- List item -->
			<li>
				<span>Planes de Carrera</span>
				<span class="count">3</span>
			</li>
			<!-- // List item END -->
			
		</ul>
	</div>
</div>
<!-- // Activity/List Widget END -->
			<!-- Latest Orders/List Widget -->
			<h2 class="margin-none separator bottom"><i class="fa fa-shopping-cart innerR text-primary"></i>Certificados</h2>
			<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body list products">
					<ul>
						<!-- List item -->
						<li>
							<span class="img"><img data-src="holder.js/40x40/dark" class="thumb" /></span>
							<span class="title">RSS001<br/><strong>Ver</strong></span>
							<span class="count"></span>
						</li>
						<!-- // List item END -->
					</ul>
				</div>
			</div>
			<!-- // Latest Orders/List Widget END -->
			<!-- Widget -->
			<h2 class="margin-none separator bottom"><i class="fa  fa-file-text-o innerR text-primary"></i>Notas</h2>
			<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body">
					Actualmente cuenta con privilegios de administración, profesor y alumno inscrito en algunos cursos, si desea cambiar dicha información comuniquela al administrador.
				</div>
			</div>
			<!-- // Widget END -->
			<!-- Alert -->
<div class="alert alert-primary">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<strong>Alerta!</strong> Recuerde el curso <strong>Camiones</strong> programado para el 22/09/2014 en el cual usted es PROFESOR.
</div>
<!-- // Alert END -->
		</div>
	</div>
	
</div>	
	
		
		</div>
		<!-- // Content END -->
		
				</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
</body>
</html>