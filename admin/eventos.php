<?php include('src/seguridad.php'); ?>
<?php include('controllers/eventos.php');
$source = "vid";
$location = 'tutor';
$CalendarData = true;
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons calendar"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/eventos.php">Eventos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Calendario de Eventos:</h2>
						<div class="btn-group pull-right">
							<?php if(isset($_GET['id'])){ ?>
								<a href="eventos.php" class="glyphicons no-js unshare" ><i></i>Regresar</a>
							<?php }else{ ?>
								<?php if($_SESSION['max_rol']>=6){ ?>
									<a href="#ModalCrearNueva" data-toggle="modal" class="btn btn-primary"><i class="fa fa-fw fa-plus-square"></i> Agregar Evento</a>
								<?php } ?>
							<?php } ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<?php if(!isset($_GET['id'])){ ?>
						<div class="widget widget-heading-simple widget-body-gray">
							<div data-component>
								<div id='loading' style='display:none'>Consultando...</div>
								<div id="calendar_eventos_asesor"></div>
							</div>
						</div>
					<?php } ?>
					<?php if(isset($_GET['id'])){ ?>
						<div class="widget innerAll" id="DetailEvento">
							<div class="row row-merge border-none">
								<div class="col-md-12 border-none">
									<div class="media margin-none" style="padding-left:5px">
										<div class="media-body innerTB" style="padding:0px">
											<h4 style="color:#0058a3"><?php echo $Evento_datos['event']; ?></h4>
											<p class="text-muted"><?php echo $Evento_datos['date_event']; ?></p>
											<?php if($_SESSION['max_rol']>=6){ ?>
												<p class="text-small margin-none">
													<a href="#ModalEditar" data-toggle="modal"><i class="fa fa-fw fa-pencil"></i> Editar </a>
												</p>
											<?php } ?>
											<p class="margin-none" style="text-align: justify;"><?php echo $Evento_datos['description']; ?></p>
										</div>
									</div><br>

									<!-- Blueimp Gallery -->
									<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
									    <div class="slides"></div>
									    <h3 class="title">Titulo de la fografía</h3>
									    <a class="prev no-ajaxify">‹</a>
									    <a class="next no-ajaxify">›</a>
									    <a class="close no-ajaxify">×</a>
									    <a class="play-pause no-ajaxify"></a>
									    <ol class="indicator"></ol>
									</div>
									<!-- // Blueimp Gallery END -->
									<!-- About -->
									<div class="widget widget-heading-simple widget-body-white margin-none">
										<div class="widget-head"><h4 class="heading glyphicons picture" style="color:#0058a3 !important;margin-left: 5px;"><i></i>Galería de Imágenes</h4></div>
										<div class="widget-body">
											<div class="widget-gallery" data-toggle="collapse-widget">
												<!-- Gallery Layout -->
												<div class="gallery gallery-2">
													<ul class="row">
														<?php foreach ($Galeria_datos as $key => $Data_Gal) { ?>
															<li class="col-md-2">
																<a class="thumb no-ajaxify" data-description="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" data-title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" data-gallery="gallery-2" href="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>">
																	<img src="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>" title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" alt="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" class="img-responsive" />
																</a>
															</li>
														<?php } ?>
													</ul>
												</div>
												<!-- // Gallery Layout END -->
											</div>
										</div>
									</div>
									<!-- // About END -->

								</div>
							</div>
						</div>
					<?php } ?>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>

<?php if(!isset($_GET['id'])){ ?>
<!-- Modal -->
<form action="eventos.php" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
	<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Crear Nuevo Evento</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Nombre: </label>
						<input type="text" id="event" name="event" class="form-control col-md-8" placeholder="Nombre del evento" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Galeria: </label>
						<select style="width: 100%;" id="media_id" name="media_id" >
							<option value="0">Ninguna por ahora</option>
						<?php foreach ($Eventos_Meddatos as $key => $Data_Med) { ?>
							<option value="<?php echo($Data_Med['media_id']); ?>" ><?php echo($Data_Med['media']); ?></option>
						<?php } ?>
						</select>
					</div>
					<div class="col-md-1">
						<input type="hidden" id="opcn" value="crear" name="opcn">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Fecha: </label>
						<input type="text" id="date_event" name="date_event" class="form-control col-md-8" placeholder="Fecha del evento" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Lugar: </label>
						<input type="text" id="living" name="living" class="form-control col-md-8" placeholder="Lugar del evento" />
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-10">
						<label class="control-label">Detalles del Evento: </label>
						<textarea id="description" name="description" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->
<?php } ?>
<?php if(isset($_GET['id'])){ ?>
<!-- Modal -->
<form action="eventos.php?id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
	<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Editar el Evento</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Nombre: </label>
						<input type="text" id="event" name="event" class="form-control col-md-8" placeholder="Nombre del Evento" value="<?php echo($Evento_datos['event']); ?>" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Galeria: </label>
						<select style="width: 100%;" id="media_id" name="media_id" >
						<option value="0" <?php if($Evento_datos['media_id']==0){ ?> selected="selected" <?php } ?> >Ninguna por ahora</option>
						<?php foreach ($Eventos_Meddatos as $key => $Data_Cat) { ?>
							<option value="<?php echo($Data_Cat['media_id']); ?>" <?php if($Evento_datos['media_id']==$Data_Cat['media_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Cat['media']); ?></option>
						<?php } ?>
						</select>
					</div>
					<div class="col-md-1">
						<input type="hidden" id="event_id" value="<?php echo $_GET['id']; ?>" name="event_id">
						<input type="hidden" id="opcn" value="editar" name="opcn">
						<input type="hidden" id="imgant" value="<?php echo $Evento_datos['image']; ?>" name="imgant">
						<input type="hidden" id="pdfant" value="<?php echo $Evento_datos['file_pdf']; ?>" name="pdfant">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Fecha: </label>
						<input type="text" id="date_event" name="date_event" class="form-control col-md-8" placeholder="Fecha del evento" value="<?php echo($Evento_datos['date_event']); ?>" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Lugar: </label>
						<input type="text" id="living" name="living" class="form-control col-md-8" placeholder="Lugar del evento" value="<?php echo($Evento_datos['living']); ?>" />
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Estado: </label>
						<select style="width: 100%;" id="estado" name="estado">
							<option value="1" <?php if($Evento_datos['status_id']=="1"){ ?>selected="selected"<?php } ?>>Activo</option>
							<option value="2" <?php if($Evento_datos['status_id']=="2"){ ?>selected="selected"<?php } ?>>Inactivo</option>
						</select>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-10">
						<label class="control-label">Detalles del Evento: </label>
						<textarea id="description" name="description" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Evento_datos['description']); ?></textarea>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->
<?php } ?>



		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/eventos.js"></script>
</body>
</html>