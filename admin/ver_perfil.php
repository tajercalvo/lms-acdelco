<?php include('src/seguridad.php');
	if(!isset($_SESSION['idUsuario'])){
		header("location: login");
	}
?>
<?php
if(!isset($_GET['id'])){
	$_GET['id'] = $_SESSION['idUsuario'];
}
?>
<?php include('controllers/perfil.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link href="../assets/components/library/gauge/jquerysctipttop.css" rel="stylesheet" type="text/css">
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons old_man"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/ver_perfil.php">Perfil</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Mi información </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray widget-employees">
						<div class="widget-body padding-none">
							<div class="row">
								<div class="col-md-12 detailsWrapper">
									<div class="ajax-loading hide">
										<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
									</div>
									<div class="innerAll">
										<div class="title">
											<div class="row">
												<div class="col-md-6">
													<h3 class="text-primary"><?php echo($datosUsuario['first_name'].' '.$datosUsuario['last_name']); ?></h3>
												</div>
												<div class="col-md-6 text-right">
												</div>
											</div>
										</div>
										<hr/>
										<div class="body">
											<div class="row">
												<div class="col-md-3 overflow-hidden">
													<!-- Profile Photo -->
													<div>
													<img id="ImgUsuario" src="../assets/images/usuarios/logouser.png"<?php echo($datosUsuario['image']); ?> style="width: 200px;" alt="<?php echo($datosUsuario['first_name'].' '.$datosUsuario['last_name']); ?>" />
													</div>
													<div class="separator bottom"></div>
													<!-- // Profile Photo END -->
													<ul class="icons-ul separator bottom">
														<li><i class="fa fa-envelope fa fa-li fa-fw"></i> @: <?php echo($datosUsuario['email']); ?></li>
														<li><i class="fa fa-phone fa fa-li fa-fw"></i> Tel: <?php echo($datosUsuario['phone']); ?></li>
														<li><i class="fa fa-skype fa fa-li fa-fw"></i> <?php echo($datosUsuario['headquarter']); ?> </li>
													</ul>
													<ul class="icons-ul separator bottom">
														<li><i class="fa fa-clock-o fa fa-li fa-fw"></i> <strong class="text-primary">Horas (<?php echo($anoCurso); ?>): <?php echo($Horas); ?></strong></li>
														<li><i class="fa fa-star fa fa-li fa-fw"></i> <strong class="text-primary">Salidas (<?php echo($anoCurso); ?>): <?php echo($Salidas); ?></strong></li>
													</ul>
													<input type="hidden" id="imgant" name="imgant" value="<?php echo($datosUsuario['image']);?>">
													<input type="hidden" id="user_id" name="user_id" value="<?php echo($datosUsuario['user_id']);?>">
													<div class="fileupload fileupload-new margin-none center" data-provides="fileupload">
													  	<span class="btn btn-default btn-file">
													  		<span class="fileupload-new">Seleccionar Fotografía</span>
													  		<span class="fileupload-exists">Cambiar Imagen</span>
														  	<input type="file" class="margin-none" id="image_new" name="image_new" />
														</span>
													  	<span class="fileupload-preview"></span>
													  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
													</div>
													<div class="form-group center">
												    	<button type="button" class="btn btn-primary" onclick="cargaImagen();"><i class="fa fa-check-circle"></i> Cambiar Fotografía</button>
												  	</div>
													<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
														<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
													<?php }elseif(isset($_GET['back'])&&$_GET['back']=="usuarios"){ ?>
														<h5><a href="../admin/usuarios.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
													<?php }else{ ?>
														<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
													<?php } ?>
													<div class="separator bottom"></div>
													<div class="row">
														<?php foreach ($charge_user as $key => $Data_Charges) {
															if($Data_Charges['charge_id'] != 123 && $Data_Charges['charge_id'] != 125 && $Data_Charges['charge_id'] != 126){ ?>
															<div class="widget">
																<!-- Widget heading -->
																<div class="widget-head">
																	<h4 class="heading"><?php echo($Data_Charges['charge']); ?></h4>
																</div>
																<!-- // Widget heading END -->
																<div class="widget-body innerAll inner-2x">
																	<div class="js-gauge demoGauge gauge" data-val="ResTray<?php echo($Data_Charges['charge_id']); ?>" data-ref="<?php echo($Data_Charges['charge_id']); ?>"></div>
																	<br>
																	Cursos Aprobados: <span id="Span_CursosAprobados<?php echo($Data_Charges['charge_id']); ?>">0</span>
																	Promedio Notas: <span id="Span_PromedioNotas<?php echo($Data_Charges['charge_id']); ?>">0</span>
																</div>
															</div>
														<?php }
														} ?>
													</div>
												</div>
												<div class="col-md-9 padding-none">
													<h5 class="strong"><a href="ed_usuarios.php?back=ver_perfil&opcn=editar" class="btn-action glyphicons pencil btn-success"><i></i> </a> <a href="ed_usuarios.php?back=ver_perfil&opcn=editar" class="text-success">Editar mi información </a> Datos disponibles:</h5>
													<p style="text-align:justify">A continuación se presenta la información disponible del usuario, dichos datos han sido ingresados en la herramienta para la disponbilidad del directorio.</p>
													<div class="row">
														<div class="col-md-4 padding-none">
															<p class="muted"><strong>Usuario:</strong> <span class="text-primary"><?php echo($datosUsuario['identification']); ?></span></p>
															<p class="muted"><strong>Nombres:</strong> <span class="text-primary"><?php echo($datosUsuario['first_name']); ?></span></p>
															<p class="muted"><strong>Apellidos:</strong> <span class="text-primary"><?php echo($datosUsuario['last_name']); ?></span></p>
															<p class="muted"><strong>Nacimiento:</strong> <span class="text-primary"><?php echo($datosUsuario['date_birthday']); ?></span></p>
															<p class="muted"><strong>Pais:</strong> <span class="text-primary"><?php echo($datosUsuario['pais']); ?></span></p>
															<p class="muted"><strong>Fecha Creación:</strong> <span class="text-primary"><?php echo($datosUsuario['date_creation']); ?></span></p>
															<p class="muted"><strong>Género:</strong> <span class="text-primary">
																<?php if($datosUsuario['gender']=="1"){ echo("Masculino"); }elseif($datosUsuario['gender']=="2"){ echo("Femenino"); }else{ echo("No Indicado"); } ?>
															</span></p>
															<p class="muted"><strong>Última Navegación:</strong> <span class="text-primary"><?php echo($DatMaxNav['maxnav']); ?></span></p>
															<div class="separator bottom"></div>
														</div>
														<div class="col-md-4 padding-none">
															<p class="muted"><strong>Correo:</strong> <span class="text-primary"><?php echo($datosUsuario['email']); ?></span></p>
															<p class="muted"><strong>Teléfono:</strong> <span class="text-primary"><?php echo($datosUsuario['phone']); ?></span></p>
															<p class="muted"><strong>Empresa:</strong> <span class="text-primary"><?php echo($datosUsuario['headquarter']); ?></span></p>
															<p class="muted"><strong>Compañia:</strong> <span class="text-primary"><?php echo($datosUsuario['empresa']); ?></span></p>
															<p class="muted"><strong>Estado:</strong> <span class="text-primary"><?php if($datosUsuario['status_id']=="1"){ echo('Activo'); }else{ echo('Inactivo'); } ?></span></p>
															<p class="muted"><strong>Ultima Actualización:</strong> <span class="text-primary"><?php echo($datosUsuario['date_session']); ?></span></p>
															<p class="muted"><strong>Nivel académico:</strong> <span class="text-primary"><?php if( isset( $datosUsuario['education'] ) ){ echo($datosUsuario['education']  ); } ?></span></p><br>
															<div class="separator bottom"></div>
														</div>
														<div class="col-md-4 padding-none">
															<div class="row">
																<h5 class="text-uppercase strong text-primary"><i class="fa fa-key text-regular fa-fw"></i> Perfil </h5>
																<ul class="team">
																	<?php
																	$cant_ele = 1;
																	foreach ($rol_user as $key => $Data_Roles) { ?>
																		<li><span class="crt"><?php echo $cant_ele; ?></span><span class="strong"><?php echo($Data_Roles['rol']); ?></span><span class="muted"><?php echo($Data_Roles['rol']); ?></span></li>
																	<?php
																	$cant_ele++;
																	} ?>
																</ul>
															</div>
															<div class="separator bottom"></div>
															<div class="row">
																<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Trayectoria </h5>
																<ul class="team">
																	<?php
																		$cant_ele = 1;
																		if(COUNT($charge_user) == 0) {?>
																			<select id="trayectoria" style="background-color: red; border: 1px solid; width: -webkit-fill-available;">
																			    <option value="">TRAYECTORIA A INICIAR</option>
																				<?php
																				foreach ($trayectorias as $key => $trayectoria) { ?>
																					<option value="<?php echo($trayectoria['charge_id']) ?>"><?php echo($trayectoria['charge']) ?></option>
																				<?php } ?>
																			</select>
																			<label>Seleccione la trayectoria</label>
																			<?php }
																		foreach ($charge_user as $key => $Data_Charges) { ?>
																			<li><span class="crt"><?php echo $cant_ele; ?></span><span class="strong"><?php echo($Data_Charges['charge']); ?></span><span class="muted"><a href="trayectoria.php?charge=<?php echo($Data_Charges['charge_id']); ?>" target="_blank"><?php echo($Data_Charges['category']); ?></a></span></li>
																		<?php
																		$cant_ele++;
																	} ?>
																</ul>
															</div>
														</div>
													</div>
													<div class="separator bottom"></div>
													<div class="row center">
														<a target="_blank" href="descargar_trayectoria.php?id=<?php echo $_GET['id']; ?>" class="right btn btn-success"><i class="fa fa-fw icon-inbox-fill-2"></i> Descargar mi trayectoria Académica <i class="fa fa-fw icon-inbox-fill-2"></i></a>
													</div>
													<div class="row">
														<h5 class="text-uppercase strong text-primary"><i class="fa fa-tasks text-regular fa-fw"></i> Trayectoria Académica</h5>
														<!--<a target="_blank" href="descargar_trayectoria.php?id=<?php echo $_GET['id']; ?>" class="right btn btn-success"><i class="fa text-regular fa-fw icon-inbox-fill-2"></i> Descargar</a>-->
														<!-- Widget Cursos Tomados -->
														<?php include_once('ver_perfil/trayectoria.php'); ?>
														<!-- // Widget END Cursos Tomados -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
    <script src="../assets/components/library/gauge/raphael-min.js"></script>
    <script src="../assets/components/library/gauge/kuma-gauge.jquery.js"></script>
    <script>
    $(".demoGauge").each(function (index,element) {
    	var ValueDat = $(element).attr("data-val");
		var ValCal = $("#"+ValueDat).val();
    	var IdTray = $(element).attr("data-ref");
    	$("#Span_CursosAprobados"+IdTray).html($("#CursosAprobados"+IdTray).val());
    	$("#Span_PromedioNotas"+IdTray).html($("#PromedioNotas"+IdTray).val());
    	$(element).kumaGauge({
            value : ValCal,
            paddingX : 70,
        });
    });
    </script>
	<script src="js/trayectoria.js"></script>
	<script src="js/op_usuarios.js"></script>
</body>
</html>
