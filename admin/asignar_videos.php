<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/asignar_videos.php');
$location = 'asignacion de video vct';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head><meta charset="gb18030">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/asignar_videos.php" >Asignacion de video vct</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<!-- Sección biblioteca -->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">

								<div class="col-md-12 detailsWrapper">
									<div class="innerAll" style="padding: 0px;">
										<div class="body">
											<div class="row padding">
												<div class="col-md-12">
													<!-- // Inicio cuerpo de la pagina -->

													<div class="row">
														<h4 class="margin-none pull-left">  <a href="#Modalprogress_bar" data-toggle="modal" style=" color: black;" id="agregar" ><i class="fa fa-cloud-upload" ></i> Agregar Archivo </a>
														</h4>
													</div>
													<div clas="separator"></div>

													<div class="widget widget-heading-simple widget-body-white">
														<!-- Widget heading -->
														<div class="widget-head">
														</div>
														<div class="widget-head">
															<h4 class="heading glyphicons list"><i>Información:</i> <strong>   </strong> Total de registros:<strong class="text-primary" id="cant_registros"> </strong>  </h4>
														</div>
														<!-- // Widget heading END -->
														<style type="text/css" media="screen">
															#tabla thead>tr>th{
																background: white !important;
																color:#d65050 !important;
																text-align: center;
																border-top: 1px solid #d65050  !important;
																border-bottom: 1px solid #d65050  !important;
																width: 25%;
																height: 25px;
																text-transform: uppercase;
															}

															#tabla tbody>tr>td{
																background: white !important;
																color:black !important;
																text-align: center;
																border: 1px solid lightgray  !important;
																width: 25%;
																height: 100px;
																font-size: 10px;
															}

															#tabla tbody>tr>tr:nth-child(odd) {
																background-color:red !important;
															}
															#tabla tbody>tr>tr:nth-child(even) {
																background-color:green !important;
															}

															#tabla tbody>tr>tr:hover {
																background-color: yellow;
															}

														</style>
														<div class="widget-body" id="primero" style="border: none; display: block;">
															<!-- Table elements-->
															<table id="tabla">

															</table>
															<!-- // Table elements END -->
															<!-- Total elements-->
															<div class="form-inline separator bottom small">
																<?php if(isset($cantidad_datos)){echo "Total de registros:";} ?><strong class='text-primary'> <?php if(isset($cantidad_datos)){echo($cantidad_datos);} ?> </strong>
															</div>
															<!-- // Total elements END -->
														</div>
													</div>

													<!-- Inicio Modal carga de archivos con progressBar  -->
													<form id="upload_form" enctype="multipart/form-data" method="post">
														<div class="modal fade" id="Modalprogress_bar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															<div class="modal-dialog" style="width: 35%">
																<div class="modal-content" style="margin-top: 53px;">
																	<div class="modal-header">
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																		<h4 class="modal-title">Crear Nuevo archivo para la biblioteca</h4>
																	</div>
																	<div class="modal-body">
																		<div class="widget widget-heading-simple widget-body-gray">
																			<div class="widget-body">
																				<!-- Row  -->
																				<div class="row">
																					<div class="col-md-12">
																						<label class="control-label">Nombre: </label>
																						<input type="text" id="name" name="name" class="form-control col-md-8" />
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-12">
																						<label class="control-label">Descripcion: </label>
																						<textarea id="description" name="description" class="form-control col-md-8"></textarea>
																					</div>
																				</div>
																				<div class="col-md-12" style="text-align: center;top:10px;"><strong>Curso VCT: Identifica a los usuarios que asistieron al vct</strong></div>
																				<div class="row" style="text-align: center;">

																					<div class="col-md-4">
																						<br>
																						<label class="control-label">Especialidad: </label> <br>
																						<select name="segment_id" id="segment_id">
																							<?php foreach ($especialidades as $key2 => $value2) {?>
																								<option value="<?php echo $value2['specialty_id']; ?>"><?php echo utf8_encode($value2['specialty']); ?></option>
																							<?php } ?>
																						</select>
																					</div>
																					<div class="col-md-2">
																						<br>
																						<label class="control-label">Estado: </label> <br>
																						<select name="status_id" id="status_id">
																							<option value="1">Activo</option>
																							<option value="2">Inactivo</option>
																						</select>
																					</div>
																					<div class="col-md-6">

																						<br>
																						<label class="control-label">Curso VCT:</label> <br>
																						<select name="schedule_id" id="schedule_id">
																							<option value="">Seleccione una Opción</option>
																							<?php foreach ($Cursos_Vct as $key => $value) {?>
																								<option value="<?php echo $value['schedule_id']; ?>"><?php echo ($value['schedule_id'] .'-'.$value['course']); ?></option>
																							<?php } ?>
																						</select>
																					</div>
																				</div>
																				<div class="row">
																					<div class="col-md-12">
																						<br>
																						<p style="text-align: center;color: #bd272c ">Recuerde que el Archivo debe pesar menos de <b>100 MegaByte (MB)</b></p>
																					</div>
																					<div class="col-md-6">
																						<div class="checkbox">
																							<label class="checkbox-custom">
																								<input type="checkbox" name="checkbox" id="checkbox">
																								<i class="fa fa-fw fa-square-o checked"></i>URL de youtube
																							</label>
																						</div>
																						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																							<span class="btn btn-default btn-file">
																								<span class="fileupload-new">Seleccionar Imagen</span>
																								<input type="file" class="margin-none" id="image" name="image" accept="image/jpeg, image/png" />
																							</span>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<br>
																						<div id="video_youtube" style="display: none;">
																							<label class="control-label">Url Youtube: </label> <br>
																							<input type="text" placeholder="URL Ej: https://www.youtube.com" class="form-control" id="url" name="url"/>
																						</div>
																						<div id="archivo_biblioteca" class="fileupload fileupload-new margin-none" data-provides="fileupload">
																							<label class="control-label">Seleccione el archivo</label>
																							<br>
																							<span class="btn btn-default btn-file">
																								<span class="fileupload-new">Seleccionar Archivo</span>
																								<input type="file" class="margin-none" id="file1" name="file1"/>
																							</span>
																						</div>
																					</div>
																				</div>
																				<div class="clearfix"><br></div>

																				<!-- Row END -->
																				<div class="clearfix"><br></div>
																				<div class="modal-footer">
																					<div class="progress progress-mini" style="margin-bottom: 5px;">
																						<div class="progress-bar progress-bar-primary" id="progressBar" style="width: 0%;"></div>
																					</div>
																					<div class="row">
																						<div class="col-md-10" style="text-align: left">
																							<label id="status" class="control-label"></label>
																						</div>
																						<div class="col-md-2"><a onclick="uploadFile()" class="btn btn-primary">Subir <i class="fa fa-cloud-upload"></i></a></div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</form>
													<!-- Fin Modal carga de archivos con progressBar  -->
													<!-- // Fin modal editar archi-->
													<!-- inico modal visto por -->

													<div class="modal fade" id="Visto_por" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																	<h4 id="nombre_archivo_visto" class="modal-title"></h4>
																</div>
																<div class="modal-body">
																	<div class="widget widget-heading-simple widget-body-gray">
																		<div class="widget-body">
																			<br>
																			<br>
																			<!-- Row -->
																			<div class="row">

																				<div class="block block-inline">
																					<div class="caret"></div>
																					<div class="box-generic">
																						<div class="timeline-top-info content-filled border-bottom">
																							<i class="fa fa-user"></i> Subido por: <a href="#"><img src="../assets/images/usuarios/default.png" id="imagen" alt="photo" width="20"></a> <a href="#"><label id="nombre_usuario" class="control-label"> </label></a>
																							<i class="fa fa-clock-o"></i> <label id="fecha" class="control-label"> </label>
																						</div>

																						<!-- <section id="miTablaaa"></section> -->

																						<div id="vistos"></div>
																					</div>
																				</div>
																			</div>

																			<!-- Row END-->
																			<div class="clearfix"><br></div>



																			<div class="clearfix"><br></div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

													<!-- asignar Usuario -->	
													<div class="widget-body" id="segundo" style="display: none;">
														<button type="bottom" id="atras" style="background:white; color: #040404;border:2px solid #c52f2b;border-radius: 69px;padding: 0px 15px 0px 15px;font-size: 24px;margin-bottom: 3px;">Atrás</button>
														<div class="row" style="padding: 20px">
															<div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="color:#0d8fd0;font-size: 20px;"></i>Invitado</div>

															<div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="color: #12d44f;font-size: 20px;"></i>Asistió</div>
															<div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="color: #af2b2d;font-size: 20px;"></i>No Asistió</div>

															<div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="font-size: 20px;"></i>usuarios</div>
														</div>
														<div class="row" style="margin-bottom: 20px">
															<div class="col-md-10">
																<div class="input-group">
																	<input class="form-control" id="busqueda" name="busqueda" type="text" placeholder="Busqueda" value="" style="border: 2px solid #337ab7;" autocomplete="off">
																	<span class="input-group-addon" style="border: 2px solid #337ab7">
																		<i class="fa fa-search" ></i>
																	</span>
																</div>
															</div>
															<div class="col-md-2">
																<button type="button" id="boton_filtro" class="btn btn-primary pull-rigth">Buscar <i class="fa fa-search"></i></button>
																
																
															</div>
														</div>
										    
														<div style="overflow-y: scroll;height:600px;width: auto;background-color: #ffffff82;">
															<table id="asignar_usuario" class="table table-bordered no-more-tables tabla table-hover" >
																<thead>
																	<tr>
																		<th class="text-center th" style="width:1%; font-size: 20px;">Identificación</th>
																		<th class="text-center th" style="width:12%; font-size: 20px;">Usuarios</th>
																		<th class="text-center th" style="width:6%; font-size: 20px;">Permiso</th>
																	</tr>
																</thead>
																<tbody>

																</tbody>
															</table>
														</div>
														

													</div>	

													<!-- asignar Usuario -->			 



													<!-- Modal Acceso Usuario -->
										<!-- <div class="modal" role="dialog" id="accesoUsuario">
										  <div class="modal-dialog modal-lg" role="document">
										    <div class="modal-content" style="top: 16px;">
										      <div class="modal-body">
										        <h3 class="modal-title" style="text-transform: uppercase; text-align: center;">PERMISOS para visualizar videos</h3>
										         <div class="row">
											       <div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="color:#0d8fd0;font-size: 20px;"></i>Invitado</div>
											      
											       	<div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="color: #12d44f;font-size: 20px;"></i>Asistió</div>
											       	<div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="color: #af2b2d;font-size: 20px;"></i>No Asistió</div>
											      
											       	<div class="col-md-3"><i class="fa fa-circle" aria-hidden="true" style="font-size: 20px;"></i>usuarios</div>
											       </div>
										        </div>

										        <h4 class="modal-title"></h4>
										       
										        	<table id="asignar_usuario" class="table table-bordered no-more-tables tabla">
											          <thead>
											            <tr>
											              <th class="text-center th" style="width:12%; font-size: 20px;">Usuarios</th>
											              <th class="text-center th" style="width:6%; font-size: 20px;">Permiso</th>
											            </tr>
											          </thead>
											          <tbody>
											         
											          </tbody>
											        </table>
											      
										      </div>
										        <div class="modal-footer">
									          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        	</div>
										    </div>
										  </div>
										</div> -->
										<!-- Modal Acceso Usuario -->


										<!-- // Fin cuerpo de la pagina -->
										<!-- // Inicio pie de pagina -->
										<div class="separator bottom"></div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div> </div>
				<!-- // Fin class widget-heading  -->
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<!-- // Sidebar menu & content wrapper END -->
<?php include('src/footer.php'); ?>
</div>
<!-- // Main Container Fluid END -->
<?php include('src/global.php'); ?>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript" ></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="js/asignar_videos.js?v=<?php echo md5(time()); ?>"></script>
</body>
</html>
