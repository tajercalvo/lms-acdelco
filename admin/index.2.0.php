<?php include('src/seguridad.php'); ?>
<?php 
if(!isset($_GET['id'])){
	$_GET['id'] = $_SESSION['idUsuario'];
}
include('controllers/index.php'); 
$location = 'home';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
<?php if($var_Publicidad<=0){ ?>
<!-- Modal -->
<div class="modal fade " id="modal-lanzamiento" style="z-index:20000">
	<div class="modal-dialog " style="width:930px;top:-25px;">
		<div class="modal-content">
			<!-- Modal heading -->
			<div class="modal-header">
				<button type="button" class="close" style="top:0px;" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title" style="height:25px;font-size:12px;line-height:22px;">GM Academy a un clic de distancia</h3>
			</div>
			<!--// Modal heading END -->
			<!-- Modal body -->
			<div class="modal-body">
				<a href="ver_perfil.php">
					<img class="img-responsive" id="imgModal_Lanzamiento" src="../assets/gallery/modal/trayectoriaAcademica.jpg" title="GM Trayectoria Académica" class="img-responsive" />
					<!--<img class="img-responsive" id="imgModal_Lanzamiento" src="../assets/gallery/modal/lanzamientoCruce.jpg" title="GM Lanzamiento Cruze" class="img-responsive" />-->
				</a>
				<input type="hidden" name="ValImgSel_Lanzamiento" id="ValImgSel_Lanzamiento" value="0"/>
			</div>
			<!-- // Modal body END -->
			<!-- Modal footer 
			<div class="modal-footer">
				<a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a> 
			</div>
			 // Modal footer END -->
		</div>
	</div>
</div>
<!-- // Modal END -->
<?php } ?>

	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		<?php include('src/menu.php'); ?>
		<!-- Content -->
		<div id="content">
		<?php include('src/top_nav_bar.php'); ?>
		<!-- Camino de Hormigas -->
		<ul class="breadcrumb">
			<li>Estás aquí</li>
			<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
			<li><a href="../admin/">Dashboard Estudiante</a></li>
			<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
		</ul>
		<!-- Camino de Hormigas -->

		<!-- Saludo estudiante -->
		<?php include_once('index/info.php'); ?>
		<!-- Saludo estudiante -->

		<!-- Widget Cursos Tomados -->
		<?php //include_once('index/cursos_actuales.php'); ?>
		<!-- // Widget END Cursos Tomados -->

		<!-- Widget Cursos Tomados -->
		<?php include_once('index/cursos_tomados.php'); ?>
		<!-- // Widget END Cursos Tomados -->

</div>	
		</div>
		<!-- // Content END -->
				</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/index.js"></script>
</body>
</html>