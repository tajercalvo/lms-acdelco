<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<title>Ludus LMS - Door Training</title>
	<link rel="shortcut icon" href="../template/assets/ludus.ico" >
	
	<!-- Meta -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	
		
	<!-- 
	**********************************************************
	In development, use the LESS files and the less.js compiler
	instead of the minified CSS loaded by default.
	**********************************************************
	<link rel="stylesheet/less" href="../template/assets/less/admin/module.admin.page.course.less" />
	-->

		<!--[if lt IE 9]><link rel="stylesheet" href="../template/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.course.min.css" />
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


	<script src="../template/assets/components/library/jquery/jquery.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/jquery/jquery-migrate.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/modernizr/modernizr.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/less-js/less.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v2.3.0"></script>	<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>
	
</head>
<body class="document-body ">
	
		<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		
				<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		
		<!-- Sidebar Menu -->
		<div id="menu" class="hidden-print">
		
			<!-- Brand -->
			<a href="index.html?lang=es" class="appbrand"><span class="text-primary">LUDUS</span> <span>LMS</span></a>
		
			<!-- Scrollable menu wrapper with Maximum height -->
			<!-- <div class="slim-scroll" data-scroll-height="800px"> -->
			<div class="slim-scroll">
			
				<!-- Menu Toggle Button -->
				<button type="button" class="btn btn-navbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<!-- // Menu Toggle Button END -->
				
				<!-- Sidebar Profile -->
				<span class="profile center">
					<a href="my_account_advanced.html?lang=es"><img class="img-responsive" src="../template/assets/images/avatar-74x74.jpg" alt="Avatar" /></a>
				</span>
				<!-- // Sidebar Profile END -->
				
				<!-- Menu -->
				<ul>
				
					<li class="hasSubmenu">
	<a data-toggle="collapse" href="#menu_dashboard" class="glyphicons dashboard"><i></i><span>Dashboard</span><span class="fa fa-chevron-down"></span></a>
	<ul class="collapse" id="menu_dashboard">
		<li class=""><a href="index.html?lang=es">Default</a></li>
		<li class=""><a href="dashboard_analytics.html?lang=es">Análisis</a></li>
		<li class=""><a href="dashboard_users.html?lang=es">Mis Datos</a></li>
		<li class=""><a href="dashboard_overview.html?lang=es">Mis Clases</a></li>
	</ul>
</li>

<li class="hasSubmenu">
	<a href="#menu_components" data-toggle="collapse" class="glyphicons settings"><i></i><span>Configuración</span><span class="fa fa-chevron-down"></span></a>
	<ul class="collapse" id="menu_components">
	
		<!-- Components Submenu Level 2 -->
		<li class="hasSubmenu">
			<a href="#menu_components_forms" data-toggle="collapse">General<span class="badge badge-primary fix">4</span></a>
			<ul class="collapse" id="menu_components_forms">
				<li class=""><a href="courses_listing.html?lang=es">Cursos</a></li>
				<li class=""><a href="form_elements.html?lang=es" onclick="return false;">Programación</a></li>
				<li class=""><a href="form_validator.html?lang=es" onclick="return false;">Estudiantes</a></li>
				<li class=""><a href="file_managers.html?lang=es" onclick="return false;">Planes de formación</a></li>
			</ul>
		</li>
		<!-- // Components Submenu Level 2 END -->
	</ul>
</li>


					
				</ul>
				<div class="clearfix"></div>
				<!-- // Menu END -->
				
				<div class="menu-hidden-element alert alert-primary">
					<a class="close" data-dismiss="alert">&times;</a>
					<p>Esta es una alerta del LMS, comuníquese con el Administrador.</p>
				</div>
			
			</div>
			<!-- // Scrollable Menu wrapper with Maximum Height END -->
			
		</div>
		<!-- // Sidebar Menu END -->
				
		<!-- Content -->
		<div id="content">
		
		<!-- Top navbar -->
<div class="navbar main hidden-print">

	<!-- Menu Toggle Button -->
	<button type="button" class="btn btn-navbar pull-left visible-xs">
		<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
	</button>
	<!-- // Menu Toggle Button END -->
	
	<!-- Full Top Style -->
	<ul class="topnav pull-left">

		<li class="dropdown dd-1 active">
			<a data-toggle="dropdown" class="dropdown-toggle glyphicons dashboard"><i></i>Dashboard <span class="caret"></span></a>
			<ul class="dropdown-menu pull-left">
				<li class=""><a href="index.html?lang=es">Default</a></li>
				<li class=" active"><a href="dashboard_analytics.html?lang=es">Analytics</a></li>
				<li class=""><a href="dashboard_users.html?lang=es">Mis Datos</a></li>
				<li class=""><a href="dashboard_overview.html?lang=es">Mis Clases</a></li>
			</ul>
		</li>

		<li class="dropdown dd-1">
			<a href="" data-toggle="dropdown" class="glyphicons settings"><i></i>Configuración <span class="caret"></span></a>
			<ul class="dropdown-menu pull-left">
				<!-- Components Submenu Level 2 -->
				<li class="dropdown submenu">
					<a data-toggle="dropdown" class="dropdown-toggle">General</a>
					<ul class="dropdown-menu submenu-show submenu-hide pull-right">
						<li class=""><a href="courses_listing.html?lang=es">Cursos</a></li>
						<li class=""><a href="form_elements.html?lang=es" onclick="return false;">Programación</a></li>
						<li class=""><a href="form_validator.html?lang=es" onclick="return false;">Estudiantes</a></li>
						<li class=""><a href="file_managers.html?lang=es" onclick="return false;">Planes de formación</a></li>
					</ul>
				</li>
				<!-- // Components Submenu Level 2 END -->
			</ul>
		</li>
		<li class="mega-menu hidden-xs">
			<a href="#" class="glyphicons sheriffs_star"><i></i> Ludus LMS</a>
			<div class="mega-sub">
				<div class="mega-sub-inner">
					<div class="row"> 
						<div class="col-md-2">
							<h4><i class="fa fa-book icon-fixed-width text-primary"></i> Learning</h4>
							<ul class="icons-ul">
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Manual de usuario</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Ayuda General</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Análisis de datos</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Learning BI</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> FAQ</a></li>
							</ul>
						</div>
						<div class="col-md-2">
							<h4><i class="fa fa-wrench icon-fixed-width text-primary"></i> Utilidades</h4>
							<ul class="icons-ul">
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> API Conexión</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Audios</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Documentos</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Material</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Videos</a></li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4><i class="fa fa-android icon-fixed-width text-primary"></i> Door Training</h4>
							<ul class="icons-ul">
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Trayectoria</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Maestros</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Procesos</a></li>
							</ul>
							<h4><i class="fa fa-apple icon-fixed-width text-primary"></i> Profesional</h4>
							<ul class="icons-ul">
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Reload Editor</a></li>
								<li><a href="#"><span class="icon-li fa fa-arrow-right"></span> Scorm Player</a></li>
							</ul>
						</div>
						<div class="col-md-5">
							<div class="innerTB innerR">
							    <div class="glyphicons glyphicon-xlarge glyphicon-top sheriffs_star glyphicon-primary">
						        	<i></i>
						        	<h4>Soporte para cualquier tema</h4>
							        <p class="margin-none">Si tiene alguna duda puede consultarlo en el módulo de preguntas frecuentes FAQ link a continuación, si no logra ser despejada allí, puede ponerse en contacto con el administrador admin@doortraining.com.co
							            <br> <a href="#">FAQ</a>
							        </p>
							    </div>
							</div>
						</div>
					</div>
				</div><!-- /cbp-hrsub-inner -->
			</div><!-- /cbp-hrsub -->
		</li>
	</ul>
	
	<!-- Top Menu Right -->
	<ul class="topnav pull-right hidden-sm hidden-xs">
	
		<!-- Profile / Logout menu -->
		<li class="account dropdown dd-1">
						<a data-toggle="dropdown" href="my_account_advanced.html?lang=es" class="glyphicons logout lock"><span class="hidden-md hidden-sm hidden-desktop-1">Ricardo Osorio</span><i></i></a>
			<ul class="dropdown-menu pull-right">
				<li><a href="my_account_advanced.html?lang=es" class="glyphicons cogwheel">Configuración<i></i></a></li>
				<li class="profile">
					<span>
						<span class="heading">Perfil <a href="my_account_advanced.html?lang=es" class="pull-right">Editar</a></span>
						<span class="media display-block margin-none">
							<span class="pull-left display-block thumb">
								<img src="../template/assets/images/people/35/2.jpg" alt="">
							</span>
							<span class="media-body display-block">
								<a href="my_account_advanced.html?lang=es">Ricardo Osorio</a>
								<small>ricardo.osorio@doortraining.com.co</small>
							</span>
						</span>
					</span>
				</li>
				<li class="innerTB half">
					<span>
						<a class="btn btn-default btn-xs pull-right" href="login.html?lang=es">Salir</a>
						<span class="clearfix"></span>
					</span>
				</li>
			</ul>
					</li>
		<!-- // Profile / Logout menu END -->
	</ul>
	<!-- // Top Menu Right END -->
	<ul class="topnav pull-right hidden-sm hidden-xs">
		<li class="dropdown dd-1">
			<a href="" class="glyphicons envelope single-icon" data-toggle="dropdown"><i></i><span class="badge fix badge-primary">5</span></a>
			<ul class="dropdown-menu pull-right inbox">
				<li class="primary"><a href="my_account_advanced.html?lang=es" class="glyphicons envelope">Mensajes<i></i></a></li>
				<li>
					<div class="innerAll">
						<span class="padding-none">De <a href="#" class="clean">Ludus LMS</a> 16/09/2014</span>
						<small>Recuerde programar los materiales para el curso de Camiones....</small>
					</div>

					<a href="#" class="clean btn btn-icon innerAll"><i class="fa fa-link"></i> Archivo Curso</a> 
				</li>
				<li>
					<div class="innerAll">
						<span class="padding-none">De <a href="#" class="clean">Alfonso Albarracín</a> 15/09/2014</span>
						<small>Estimado profesor, hay clase programada el fin para el 22 para dejarlo listo....</small>
					</div>

				</li>
				<li>
					<div class="innerAll">
						<span class="padding-none">De <a href="#" class="clean">Ludus LMS</a> 01/09/2014</span>
						<small>Le ha sido asignada una actividad de clase presencial para el 22/09/2014....</small>
					</div>

					<a href="#" class="clean btn btn-icon innerAll"><i class="fa fa-link"></i> Archivo Curso</a> 
				</li>
				<li>
					<div class="innerAll">
						<span class="padding-none">De <a href="#" class="clean">Ludus LMS</a> 12/04/2014</span>
						<small>No ha completado la actividad virtual <strong>Camiones</strong> en el tiempo máximo permitido....</small>
					</div>

					<a href="#" class="clean btn btn-icon innerAll"><i class="fa fa-link"></i> Curso</a> 
				</li>		
				<li>
					<div class="innerAll">
						<span class="padding-none">De <a href="#" class="clean">Ludus LMS</a> 05/04/2014</span>
						<small>Recuerde que tiene hasta el 11/04/2014 para presentar la actividad virtual <strong>Camiones</strong>....</small>
					</div>

					<a href="#" class="clean btn btn-icon innerAll"><i class="fa fa-link"></i> Curso</a> 
				</li>
			</ul>
		</li>
		<li class="dropdown dd-1">
			<a href="" class="glyphicons bell single-icon" data-toggle="dropdown"><i></i><span class="badge fix badge-primary">3</span></a>
			<ul class="dropdown-menu pull-right notifications">
				<li class="first"><a href="">Ver todas las notificaciones <i class="fa fa-arrow-right"></i></a></li>
				<li class="innerAll">
					<div class="media">
						<i class="fa fa-cloud icon-2x icon-muted pull-left"></i>
						<div class="media-body">
							<span>Preparación de material</span>
							<div class="progress progress-mini">
								<div class="progress-bar progress-bar-success" style="width: 75%;"></div>
							</div>
						</div>
					</div>
				</li>
				<li class="innerAll">
					<div class="media">
						<i class="fa fa-list icon-2x icon-muted pull-left"></i>
						<div class="media-body">
							<span>Cursos en proceso</span>
							<div class="progress progress-mini">
								<div class="progress-bar progress-bar-danger" style="width: 30%;"></div>
							</div>
						</div>
					</div>
				</li>

				<li class="innerAll">
					<div class="media">
						<i class="fa fa-user icon-2x icon-muted pull-left"></i>
						<div class="media-body">
							<span><a href="">Alfonso Albarracín</a> confirmar curso</span>
							<em><small>15/09/2014</small></em>
						</div>
					</div>
				</li>
				
			</ul>
		</li>
	</ul>
	<div class="clearfix"></div>
	
</div>
<!-- Top navbar END -->



				
<ul class="breadcrumb">
	<li>Estás aquí</li>
	<li><a href="index.html?lang=es" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Dashboard</li>
				<li class="divider"><i class="fa fa-caret-right"></i></li>
		<li>Conceptos Básicos de Buses y Camiones</li>
			</ul>
				
	<div class="innerLR spacing-x2">
	<!-- row -->
	<div class="row ">
		<!-- col -->
		<div class="col-md-9">
			<!-- Media -->
			<div class="widget media innerB">
				<div class="innerAll">
			  		<a class="pull-left bg-success innerAll text-center text-white" href="#"><i class="icon-cloudy-fill fa-5x fa fa-fw"></i></a>
			  	</div>
			  	<div class="media-body innerLR">
			    	<div class="innerLR">
			    		<h2 class="media-heading margin-none"><a href="" class="text-inverse">Conceptos Básicos de Buses y Camiones V 1.0</a></h2>
			    		<p class="lead">Conceptos Básicos para entender los Camiones Diesel</p>
			    	</div>
			  	</div>
			  	<!-- Tab content -->
				<div id="tabReports" class="tab-pane active widget-body-regular innerAll inner-2x text-center">
					<div data-percent="85" data-size="95" class="easy-pie inline-block easy-pie-gender primary" data-scale-color="false" data-track-color="#efefef" data-line-width="8">
						<div class="value text-center">
							<span class="strong"><i class="icon-graph-up-1 fa-3x text-primary"></i></span>
						</div>
					</div>
				</div>
				<!-- // Tab content END -->
			  	<!-- End Media -->
			</div>


			<div class="widget innerAll inner-2x justify">
				<p>Introducción: El conocimiento de los productos que vendemos es importante para todos en nuestra organización, desde los Asesores de ventas que muestran y venden nuestros productos a nuestros clientes, hasta nuestros empleados de oficina de concesionarios y de General Motors Colmotores que no tienen contacto directo con el producto pero que, a diario trabajan enfocados en su calidad, su venta, su rentabilidad o su servicio. Debido a que muchas personas que trabajan en nuestra organización no tienen antecedentes técnicos o experiencia con la totalidad de los vehículos, hemos hecho nuestro mejor esfuerzo para desarrollar esta información de forma completa y simple.</p>
				<p>En este compendio el lector encontrará los fundamentos del diseño de camiones, una descripción de sus diversos sistemas, conjuntos, componentes, así como sus funciones y descripciones de los componentes importantes del vehículo y sus relaciones.</p>
				<p>Los elementos estándar tales como tornillos, rodamientos, sellantes, etc. no están descritos en las diversas secciones, sino que están contenidos en la primera sección.</p>
				<p>Es nuestra esperanza que esta publicación será de valor para todos los miembros de la Familia GM y que, a largo plazo, ayude en la promoción de nuestros productos.</p>

				<h4 class="innerTB half">Autores:</h4>
				<p class="innerT">General Motors Camiones Andinos GMICA<br>Door Training & Consulting Colombia S.A.</p>
				<p><a href="hhttp://www.wampserver.com/en/"> <i class="fa fa-fw icon-scale-2"></i> Este material está basado en múltiples documentos de Isuzu, GM y otros fabricantes de camiones, así como en material de uso libre obtenido de Internet</a></p>
<pre class="prettyprint">
	<a href="../Courses/Camiones/Modulo0/index.html" target="_blank">1. Modulo 0 (Completo)</a>
	<a href="../Courses/Camiones/Modulo1/index.html" target="_blank">2. Modulo 1 (En proceso)</a>
	<a href="../Courses/Camiones/Modulo2/index.html" target="_blank">3. Modulo 2 (Sin Visualizar)</a>
	<a href="../Courses/Camiones/Modulo3/index.html" target="_blank">4. Modulo 3 (Sin Visualizar)</a>
	<a href="../Courses/Camiones/Modulo4/index.html" target="_blank">5. Modulo 4 (Sin Visualizar)</a>
	<a href="../Courses/Camiones/Modulo5/index.html" target="_blank">6. Modulo 5 (Sin Visualizar)</a>
</pre>
			<!-- End Widget -->		
			</div>

		</div>
		<!-- // END col -->

		<!-- col -->
		<div class="col-md-3">		
			<div class="widget  padding-none">
				<div class="widget-body padding-none">
					<h5 class="innerAll bg-primary text-white margin-bottom-none">Seleccione una categoría:</h5>
					<ul class="list-group list-group-1 margin-none borders-none">
						<li class="list-group-item border-top-none"><a href="courses_listing.html?lang=es"><span class="badge pull-right bg-primary ">5</span><i class="icon-truck"></i>&nbsp; Mecánica Básica Diesel</a></li>
						<li class="list-group-item"><a href="courses_listing.html?lang=es"><span class="badge pull-right bg-primary ">2</span><i class="icon-car"></i>&nbsp; Mecánica Avanzada Diesel</a></li>
						<li class="list-group-item"><a href="courses_listing.html?lang=es"><i class="icon-trailer"></i>&nbsp; Mecánica Básica Gasolina</a></li>
						<li class="list-group-item"><a href="courses_listing.html?lang=es"><i class="icon-biological"></i>&nbsp; Mecánica Avanzada Gasolina</a></li>
						<li class="list-group-item"><a href="courses_listing.html?lang=es"><i class="icon-collage"></i>&nbsp; Electricista Junior</a></li>
					</ul>
				</div>
			</div>
			<div class="widget">
				<h5 class="innerAll margin-none border-bottom bg-white">Videos Populares</h5>
									<a href="#" class="display-block media margin-none bg-white innerAll border-bottom">
				  		<img class="pull-left" data-src="holder.js/50x50" alt="..."/>
						<span class="display-block media-body ">
					    	<h5 class="innerT half">Tutorial Ludus LMS</h5>
					    	<div class="clearfix"></div>
							<small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 1:00</small>
						</span>
					</a>
										<a href="#" class="display-block media margin-none bg-white innerAll border-bottom">
				  		<img class="pull-left" data-src="holder.js/50x50" alt="..."/>
						<span class="display-block media-body ">
					    	<h5 class="innerT half">Hangout Julio - Ricardo Osorio</h5>
					    	<div class="clearfix"></div>
							<small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 0:30</small>
						</span>
					</a>
										<a href="#" class="display-block media margin-none bg-white innerAll border-bottom">
				  		<img class="pull-left" data-src="holder.js/50x50" alt="..."/>
						<span class="display-block media-body ">
					    	<h5 class="innerT half">Como reparar un motor Diesel</h5>
					    	<div class="clearfix"></div>
							<small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 3:15</small>
						</span>
					</a>
										<a href="#" class="display-block media margin-none bg-white innerAll border-bottom">
				  		<img class="pull-left" data-src="holder.js/50x50" alt="..."/>
						<span class="display-block media-body ">
					    	<h5 class="innerT half">Como reparar un motor a Gasolina</h5>
					    	<div class="clearfix"></div>
							<small class="text-center text-inverse muted"><i class="fa fa-fw fa-clock-o"></i> 2:40</small>
						</span>
					</a>
									</div>
			</div>
		</div>
		<!-- // END col -->
	</div>
	<!-- // END row -->
</div>
<!-- // END inner -->




	
	
		
		</div>
		<!-- // Content END -->
		
				</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
				
		<div id="footer" class="hidden-print">
			
			<!--  Copyright Line -->
			<div class="copy">&copy; - 2014 - <a href="http://www.doortraining.com.co/" target="_blank">Door training (Licencia de uso)</a> - Todos los derechos reservados. <a href="http://www.luduslms.com.co" target="_blank">Lumus LMS</a> - Versión Actual: v1.0.0 </div>
			<!--  End Copyright Line -->
	
		</div>
		<!-- // Footer END -->
		
	</div>
	<!-- // Main Container Fluid END -->
	


	<!-- Global -->
	<script>
	var basePath = '',
		commonPath = '../template/assets/',
		rootPath = '../',
		DEV = false,
		componentsPath = '../template/assets/components/';
	
	var primaryColor = '#e5412d',
		dangerColor = '#b55151',
		infoColor = '#5cc7dd',
		successColor = '#609450',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';
	
	var themerPrimaryColor = primaryColor;
	</script>
	
	<script src="../template/assets/components/library/bootstrap/js/bootstrap.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/breakpoints/breakpoints.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/prettyprint/assets/js/prettify.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/holder/holder.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.move/js/jquery.event.move.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.swipe/js/jquery.event.swipe.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/megamenu.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/core.init.js?v=v2.3.0"></script>	
</body>
</html>