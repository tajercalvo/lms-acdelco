<?php include('src/seguridad.php'); ?>
<?php include('controllers/tickets.php');
$location = 'megaMenu';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/tickets.php">Tickets</a></li>
				</ul>
				<!-- Titulo Tickets -->
				<div class="innerAll"><!-- Titulo Tickets-->
					<div class="row">
						<div class="col-md-6">
							<h1 class="margin-none padding-none"><i class="fa fa-fw fa-ticket text-primary"></i> Ticket</h1>
						</div>
					</div>
				</div>
				<!-- // END Titulo Tickets -->
				<!-- // Pestañas para validar estado de tickets -->
				<!-- hasta aqui -->
				<!-- Formulario para reportar un problema -->
				<!-- -->
				<!-- -->
				<div class="wizard">
					<div class="widget widget-tabs widget-tabs-double widget-tabs-gray">

						<!-- Widget heading -->
						<div class="widget-head">
							<ul>
								<li class="active"><a href="#tab1-2" class="glyphicons circle_exclamation_mark" data-toggle="tab"><i></i><span class="strong"></span><span> Reportar un problema</span></a></li>
								<li id="actualizarTablaTicket"><a href="#tab2-3" class="glyphicons repeat" data-toggle="tab"><i></i><span class="strong"></span><span> Mis Tickets generados</span></a></li>
							</ul>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<div class="tab-content">
								<!-- Step 1 -->
								<div class="tab-pane active" id="tab1-2">
									<form id="formu_FAQ" method="post" enctype="multipart/form-data">
										<div class="widget">
											<!-- Widget heading -->
											<div class="widget-head">
												<h1 class="heading"><strong>Reporta un problema</strong></h1>
											</div>
											<!-- // Widget heading END -->
											<div class="widget-body innerAll inner-2x" id="espacioTicket">
												<!-- Row -->
												<div class="row">
													<div class="col-md-4">
														<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> ¿Dónde está el problema? </h5>
														<select style="width: 100%" name="tema" id="tema">
															<?php foreach ($lista_general as $key => $valueLG) { ?>
																<option value="<?php echo($valueLG['key_id']); ?>"><?php echo($valueLG['key_description']); ?></option>
															<?php } ?>
														</select>
													</div>
													<div class="col-md-4">
														<!-- Group -->
														<div class="form-group">
															<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> Prioridad </h5>
															<select style="width: 100%" name="prioridad" id="prioridad">
																<option value="3">Baja</option>
																<option value="2">Media</option>
																<option value="1">Alta</option>
															</select>
														</div>
														<!-- // Group END -->
													</div>
													<!-- // Column END -->
													<!-- Column -->
													<div class="col-md-4">
														<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> Captura de pantalla </h5>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
															<span class="btn btn-default btn-file"><span class="fileupload-new">Sube captura de pantalla: </span><span class="fileupload-exists">Cambiar</span><input type="file" class="margin-none" name="faq_img" id="faq_img" accept="image/*"/></span>
															<span class="fileupload-preview"></span>
															<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none" id="eliminarImagen">&times;</a>
														</div>
													</div>
													<!-- // Column END -->
												</div>
												<!-- // Row END -->
												<div class="separator"></div>
												<!-- Row -->
												<div class="bg-gray innerAll inner-2x">
													<div class="row">
														<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> ¿Qué pasó? </h5>
														<textarea placeholder="Explica brevemente lo que ocurrió y qué pasos podemos llevar a cabo para reproducir el problema…" class="form-control col-md-12 wysihtml5" id="faq_text" name="faq_text" rows="7" required></textarea>
													</div>
													<!-- // Row END -->
												</div>
												<div class="separator"></div>
												<!-- Form actions -->
												<div class="form-actions">
													<button type="submit" class="btn btn-primary" id="btn-enviar"><i class="fa fa-check-circle"></i> Reportar</button>
													<button type="reset" class="btn btn-default" id="limpiar"> Limpiar</button>
												</div>
												<!-- // Form actions END -->
											</div>
										</div>
									</form>
								</div>
								<!-- // Step 1 END -->
								<!-- Step 2 -->
								<!-- // Step 2 END -->
								<div class="tab-pane" id="tab2-3">
									<div class="widget">
										<!-- Widget heading -->
										<div class="widget-head">
											<h1 class="heading"><strong>Tickets</strong></h1>
										</div>
										<!-- // Widget heading END -->
										<div class="widget-body innerAll inner-2x">
											<div class="row">
												<div class="col-md-12" id="campoTablaTickets">
												</div>
											</div>
											<div class="row" id="resultadoFiltroTicket">
												<div class="col-md-10 col-sm-12 border-top padding-none">
													<div class="email-content innerAll" id="reabrirTicket"></div>
												</div>
											</div>
											<!-- Row -->
											<div class="separator"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- modal respuesta -->
				<!-- -->
				<!-- -->
				<a href="#FAQmodalTicket" data-toggle="modal" style="display: none" id="FAQMTicket"></a>
				<div class="modal fade" id="FAQmodalTicket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Reportar un problema</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> Ticket </h5>
												<div class="row" id="modal-content-FAQ"></div>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
									</div>
								</div>
							</div>
							<div class="modal-footer"></div>
						</div>
					</div>
				</div>
				<!-- -->
				<!-- -->
				<!-- Fin modal respuesta -->
				<!-- Modal reabrir -->
				<a href="#modalReabrirTicket" class="btn btn-success btn-xs" style="display: none;" data-toggle="modal" id="btnReabrirTicketx"><i class="fa fa-check"></i></a>
				<form method="post" enctype="multipart/form-data" id="formReabrirTicket">
					<div class="modal fade" id="modalReabrirTicket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cerrarBtnRTicket">&times;</button>
									<h4 class="modal-title">Reabrir Ticket</h4>
								</div>
								<div class="modal-body">
									<div class="widget widget-heading-simple widget-body-gray">
										<div class="widget-body">
											<div class="clearfix"><br></div>
											<!-- Row -->
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<h4 class="text-uppercase strong"><i class="fa fa-ticket text-regular fa-fw"></i> Ticket <strong id="numeroTicketReabrir" class="text-primary"></strong></h4>
												</div>
											</div>
											<!-- Row END-->
											<div class="clearfix"><br></div>
											<!-- Row -->
											<!-- Row -->
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<h5 class="text-uppercase strong"><i class="fa fa-ticket text-primary fa-fw"></i> Texto nuevo comentario </h5>
													<textarea placeholder="Respuesta: " class="form-control col-md-12 wysihtml5" id="faq_text_r" name="faq_text_r" rows="7"></textarea>
												</div>
											</div>
											<!-- Row END-->
											<div class="clearfix"><br></div>
											<!-- Row -->
											<div class="row">
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<h5 class="text-uppercase strong text-primary"><i class="fa fa-picture-o text-regular fa-fw"></i> Captura de pantalla </h5>
													<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														<span class="btn btn-default btn-file"><span class="fileupload-new">Sube captura de pantalla: </span><span class="fileupload-exists">Cambiar</span><input type="file" class="margin-none" name="faq_img_r" id="faq_img_r" accept="image/*"/></span>
														<span class="fileupload-preview"></span>
														<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none" id="faq_img_r_close">&times;</a>
													</div>
												</div>
											</div>
											<!-- Row END-->
											<div class="clearfix"><br></div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" name="" class="btn btn-primary" id="btnReabrirTicket"> Enviar </button>
									<button type="reset" name="" id="limpiarReabrir" style="display: none">limpiar</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- Fin Modal reabrir -->
				<!-- Fin Mensajes Tickets -->
			</div>
		<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/tickets.js"></script>
</body>
</html>
