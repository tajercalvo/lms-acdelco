<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_evaluaciones_incompletas.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons roundabout"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_evaluaciones_incompletas.php">Reporte de examenes en cero</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de examenes en cero</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Aquí encuentra los usuarios que tienen la nota y el tiempo de ejecución del examen en cero.</h5></br>
								</div>
								<div class="col-md-2">

								</div>
							</div>
							<form id="form_evaluaciones">
								<div class="row">
									<div class="col-md-10">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Curso:</label>
											<div class="col-md-10">
												<select style="width: 100%;" id="review_id" name="review_id">
													<option value=""></option>
													<?php foreach ($datosCursos as $key => $Data_Cursos) { ?>
														<option value="<?php echo($Data_Cursos['review_id']); ?>" <?php if(isset($_POST['review_id']) && $_POST['review_id']==$Data_Cursos['review_id']){
														echo "selected="."selected"; } else if(isset($_GET['review_id']) && $_GET['review_id']==$Data_Cursos['review_id']) { echo "selected="."selected"; } ?> > <?php echo($Data_Cursos['review']); ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
								</div>
							</form>
							<br>
							<form id="form_evaluaciones_r">
								<div class="row">
									<div class="col-md-10">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Evaluación Refuerzo:</label>
											<div class="col-md-10">
												<select style="width: 100%;" id="review_id_r" name="review_id_r">
													<option value=""></option>
													<?php foreach ($datosCursos_r as $key => $Data_Cursos) { ?>
														<option value="<?php echo($Data_Cursos['review_id']); ?>" <?php if(isset($_POST['review_id']) && $_POST['review_id']==$Data_Cursos['review_id']){
														echo "selected="."selected"; } else if(isset($_GET['review_id']) && $_GET['review_id']==$Data_Cursos['review_id']) { echo "selected="."selected"; } ?> > <?php echo( $Data_Cursos['code']." | ". $Data_Cursos['review']); ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-success" id="ConsultaRepEstado_r"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i>Información:</i>
								<strong>
									<?php if(isset ($resultado) and $resultado > 0 ){
										echo "Se eliminino la nota satisfactoriamente";
									} else if(isset ($resultado) and $resultado < 1) {
										echo "Intentelo nuevamente por favor";
									} ?>
								</strong> <?php if(isset($cantidad_datos)){echo "Total de registros:";} ?>
								<strong class='text-primary'> <?php if(isset($cantidad_datos)){echo($cantidad_datos);} ?></strong>
							</h4>
						</div>

						<!-- // Widget heading END -->
						<div class="widget-body">
							<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
								<thead>
									<tr>
										<th data-hide="phone,tablet">EXAMEN</th>
										<th data-hide="phone,tablet">NOMBRE</th>
										<th data-hide="phone,tablet">IDENTIFICACIÓN</th>
										<th data-hide="phone,tablet">SEDE</th>
										<th data-hide="phone,tablet">CONCESIONARIO</th>
										<th data-hide="phone,tablet">PUNTAJE</th>
										<th data-hide="phone,tablet">FECHA DE CREACION</th>
										<th data-hide="phone,tablet">TIEMPO RESPONDIENDO</th>
										<th data-hide="phone,tablet">Eliminar</th>
									</tr>
								</thead>

								<tbody id="evaluaciones">

								</tbody>
							</table>

							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de registros: <strong class='text-primary' id="numero_registros"></strong>
							</div>
							<!-- // Total elements END -->
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app"></div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_evaluaciones_incompletas.js"></script>
</body>
</html>
