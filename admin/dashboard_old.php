<?php include('src/seguridad.php'); ?>
<?php include('controllers/dashboard.php');
$location = 'reporting';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons charts"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/dashboard.php">Dashboard</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Análisis de información general </h2>
							<div class="clearfix"> &nbsp;[<?php echo $fecha_inicial.' - '.$fecha_final; ?>] <?php echo $diferencia_mes; ?> Meses </div>
							<button type="button" data-toggle="print" class="btn btn-default print hidden-print"><i class="fa fa-fw fa-print"></i> Imprimir</button>
						</div>
					<!-- // END heading -->
<!-- contenido filtros -->
	<div class="widget widget-heading-simple widget-body-gray hidden-print">
		<div class="widget-body">
			<div class="row">
				<div class="col-md-10">
					<h5 style="text-align: justify; ">De acuerdo a su Perfil se presentarán a continuación diferentes opciones de filtro.</h5></br>
				</div>
				<div class="row">
					<form action="dashboard.php" method="post">
						<div class="col-md-5">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-3 control-label" for="end_date" style="padding-top:8px;">Inicio:</label>
								<div class="col-md-4">
									<label class="control-label" for="ano_id_start">Año:</label>
									<select style="width: 100%;" id="ano_id_start" name="ano_id_start" >
										<?php for ($y=date("Y");$y>2010;$y--){ ?>
												<option value="<?php echo $y; ?>" <?php if($y==$year_start){ ?>selected="selected"<?php } ?>><?php echo $y; ?></option>
											<?php } ?>
									</select>
								</div>
								<div class="col-md-5">
									<label class="control-label" for="mes_id_start">Mes: </label>
									<select style="width: 100%;" id="mes_id_start" name="mes_id_start" >
										<option value="01" <?php if($mes_start == "01"){ ?>selected="selected"<?php } ?> >Enero</option>
										<option value="02" <?php if($mes_start == "02"){ ?>selected="selected"<?php } ?> >Febrero</option>
										<option value="03" <?php if($mes_start == "03"){ ?>selected="selected"<?php } ?> >Marzo</option>
										<option value="04" <?php if($mes_start == "04"){ ?>selected="selected"<?php } ?> >Abril</option>
										<option value="05" <?php if($mes_start == "05"){ ?>selected="selected"<?php } ?> >Mayo</option>
										<option value="06" <?php if($mes_start == "06"){ ?>selected="selected"<?php } ?> >Junio</option>
										<option value="07" <?php if($mes_start == "07"){ ?>selected="selected"<?php } ?> >Julio</option>
										<option value="08" <?php if($mes_start == "08"){ ?>selected="selected"<?php } ?> >Agosto</option>
										<option value="09" <?php if($mes_start == "09"){ ?>selected="selected"<?php } ?> >Septiembre</option>
										<option value="10" <?php if($mes_start == "10"){ ?>selected="selected"<?php } ?> >Octubre</option>
										<option value="11" <?php if($mes_start == "11"){ ?>selected="selected"<?php } ?> >Noviembre</option>
										<option value="12" <?php if($mes_start == "12"){ ?>selected="selected"<?php } ?> >Diciembre</option>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<div class="col-md-5">
							<!-- Group -->
							<div class="form-group">
								<label class="col-md-3 control-label" for="start_date" style="padding-top:8px;">Final:</label>
								<div class="col-md-4">
									<label class="control-label" for="ano_id_end">Año:</label>
									<select style="width: 100%;" id="ano_id_end" name="ano_id_end" >
										<?php for ($y=date("Y");$y>2010;$y--){ ?>
											<option value="<?php echo $y; ?>" <?php if($y==$year_end){ ?>selected="selected"<?php } ?>><?php echo $y; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-5">
									<label class="control-label" for="mes_id_end">Mes:</label>
									<select style="width: 100%;" id="mes_id_end" name="mes_id_end" >
										<option value="01" <?php if($mes_end == "01"){ ?>selected="selected"<?php } ?> >Enero</option>
										<option value="02" <?php if($mes_end == "02"){ ?>selected="selected"<?php } ?> >Febrero</option>
										<option value="03" <?php if($mes_end == "03"){ ?>selected="selected"<?php } ?> >Marzo</option>
										<option value="04" <?php if($mes_end == "04"){ ?>selected="selected"<?php } ?> >Abril</option>
										<option value="05" <?php if($mes_end == "05"){ ?>selected="selected"<?php } ?> >Mayo</option>
										<option value="06" <?php if($mes_end == "06"){ ?>selected="selected"<?php } ?> >Junio</option>
										<option value="07" <?php if($mes_end == "07"){ ?>selected="selected"<?php } ?> >Julio</option>
										<option value="08" <?php if($mes_end == "08"){ ?>selected="selected"<?php } ?> >Agosto</option>
										<option value="09" <?php if($mes_end == "09"){ ?>selected="selected"<?php } ?> >Septiembre</option>
										<option value="10" <?php if($mes_end == "10"){ ?>selected="selected"<?php } ?> >Octubre</option>
										<option value="11" <?php if($mes_end == "11"){ ?>selected="selected"<?php } ?> >Noviembre</option>
										<option value="12" <?php if($mes_end == "12"){ ?>selected="selected"<?php } ?> >Diciembre</option>
									</select>
								</div>
							</div>
							<!-- // Group END -->
						</div>
						<div class="col-md-2">
							<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Procesar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Nuevo ROW-->
		<div class="row row-app">
		</div>
		<!-- // END Nuevo ROW-->
		<div class="separator bottom"></div>
		<div class="separator bottom"></div>
	</div>
<!-- // END contenido filtros -->
<div class="clearfix"><br></div>
<?php /*?><!-- Head Widgets -->
	<div class="row border-none row-merge margin-none">
		<div class="col-md-3">
			<!-- Stats Widget -->
			<a href="#" class="widget-stats widget-stats-primary widget-stats-5">
				<span class="glyphicons calendar"><i></i></span>
				<span class="txt">Cursos Programados<span><?php echo number_format($cantidadProgramados); ?></span></span>
				<div class="clearfix"></div>
			</a>
			<!-- // Stats Widget END -->
		</div>
		<div class="col-md-3">
			<!-- Stats Widget -->
			<a href="#" class="widget-stats widget-stats-default widget-stats-5">
				<span class="glyphicons briefcase"><i></i></span>
				<span class="txt">Cargos con trayectoria<span class="text-inverse"><?php echo $cantidadCargosTrayectoria.' de '.$cantidadCargos?></span></span>
				<div class="clearfix"></div>
			</a>
			<!-- // Stats Widget END -->
		</div>
		<div class="col-md-3">
			<div class=" padding-bottom-none-phone">
				<!-- Stats Widget -->
				<a href="#" class="widget-stats widget-stats-gray widget-stats-4">
					<span class="txt">Usuarios en Ludus</span>
					<span class="count" style="font-size:25px;"><?php echo number_format($cantidadUsuarios); ?><span style="font-size:14px;">Activos</span></span>
					<span class="glyphicons user"><i></i></span>
					<div class="clearfix"></div>
					<i class="icon-play-circle"></i>
				</a>
				<!-- // Stats Widget END -->
			</div>
		</div>
		<div class="col-md-3">
			<div class=" padding-bottom-none-phone">
				<!-- Stats Widget -->
				<a href="#" class="widget-stats widget-stats-info widget-stats-2">
					<span class="count"><?php echo $cantidadCursos; ?></span>
					<span class="txt">Cursos Activos</span>
				</a>
				<!-- // Stats Widget END -->
			</div>
		</div>
	</div>
	<br>
	<div class="row border-top border-none row-merge margin-none">
		<div class="col-md-3">
			<div class="padding-bottom-none-phone">
				<!-- Stats Widget -->
				<a href="#" class="widget-stats widget-stats-default widget-stats-4">
					<span class="txt">Cursos Aprobados</span>
					<span class="count text-inverse"><?php echo number_format($cantidadModAprobados); ?></span>
					<span class="glyphicons cup"><i></i></span>
					<div class="clearfix"></div>
					<i class="icon-play-circle"></i>
				</a>
				<!-- // Stats Widget END -->
			</div>
		</div>
		<div class="col-md-3">
			<!-- Stats Widget -->
			<a href="#" class="widget-stats widget-stats-info widget-stats-5">
				<span class="glyphicons home"><i></i></span>
				<span class="txt">Concesionarios formándose<span><?php echo $cantidadConcesionariosFormacion; ?> de <?php echo $cantidadConcesionarios; ?></span></span>
				<div class="clearfix"></div>
			</a>
			<!-- // Stats Widget END -->
		</div>
		<div class="col-md-3">
			<div class=" padding-bottom-none-phone">
				<!-- Stats Widget -->
				<a href="#" class="widget-stats widget-stats-inverse widget-stats-4">
					<span class="txt">Progreso</span>
					<span class="count">60<?php //echo round(($cantidadUsuariosFormacion/$cantidadUsuarios)*100,0); ?>%</span>
					<span class="glyphicons refresh"><i></i></span>
					<div class="clearfix"></div>
					<i class="icon-play-circle"></i>
				</a>
				<!-- // Stats Widget END -->
			</div>
		</div>
		<div class="col-md-3">
			<!-- Stats Widget -->
			<a href="#" class="widget-stats widget-stats-primary widget-stats-5">
				<span class="glyphicons group"><i></i></span>
				<span class="txt">Usuarios con formación<span><?php echo number_format($cantidadUsuariosFormacion); ?></span></span>
				<div class="clearfix"></div>
			</a>
			<!-- // Stats Widget END -->
		</div>
	</div>
<!-- Head Widgets --><?php */ ?>
<p class="separator text-center"></p>
<p class="text-center text-success" style="font-size:17px;"><strong>Indicador de avance por Cargo</sotrong></p>
<p class="separator text-center"><i class="fa fa-bar-chart-o fa-3x"></i></p>
<!-- Tabs -->
<div class="relativeWrap" >
	<div class="widget widget-tabs widget-tabs-double widget-tabs-vertical row row-merge widget-tabs-gray">
		<!-- Tabs Heading -->
		<div class="widget-head col-md-3">
			<ul>
				<?php 
				$ctrl = 0;
				foreach ($cargos_Trayectorias as $iID => $dataCargos) { ?>
					<li <?php if($ctrl==0){ $ctrl=1; ?>class="active"<?php } ?>><a href="#tab1-<?php echo $dataCargos['charge_id']; ?>" class="glyphicons briefcase" data-toggle="tab" onclick="plotWithOptions_Id(<?php echo $dataCargos['charge_id']; ?>);"><i></i><span class="strong"><?php echo substr($dataCargos['charge'], 0, 28); ?></span><span># Alumnos <strong class="text-danger"><?php echo $dataCargos['cantidad']; ?></strong></span></a></li>
				<?php } ?>
			</ul>
		</div>
		<!-- // Tabs Heading END -->
		<div class="widget-body col-md-9">
			<div class="tab-content">
				<?php 
				$ctrl = 0;
				foreach ($cargos_Trayectorias as $iID => $dataCargos) { 
					?>
					<!-- Tab content -->
					<div class="tab-pane <?php if($ctrl==0){ $ctrl=1; ?>active<?php } ?>" id="tab1-<?php echo $dataCargos['charge_id']; ?>">
						<div class="ajax-loading hide" id="loadingAvanceXCargo<?php echo $dataCargos['charge_id']; ?>">
							<i class="fa fa-spinner fa fa-spin fa fa-2x"></i> <span class="text-info">Cargando...</span>
						</div>
						<h4><?php echo $dataCargos['charge']; ?></h4><br><br>
						<div id="placeholder-<?php echo $dataCargos['charge_id']; ?>" class="LineChart charges-placeholder" style="width: 98%; height: 350px; font-size: 14px; line-height: 1.2em;"></div>
						<p class="separator text-center"></p>
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-4">
									<label class="radio-custom">
										<input type="radio" name="radio-<?php echo $dataCargos['charge_id']; ?>" id="Chk_lineal-<?php echo $dataCargos['charge_id']; ?>" checked="checked" onclick="CambiaGrafica('<?php echo $dataCargos['charge_id']; ?>','lineal');"> 
										<i class="fa fa-circle-o checked"></i> Lineal
									</label> 
								</div> 
								<div class="col-md-4">
									<label class="radio-custom"> 
										<input type="radio" name="radio-<?php echo $dataCargos['charge_id']; ?>" id="Chk_Barras-<?php echo $dataCargos['charge_id']; ?>" onclick="CambiaGrafica('<?php echo $dataCargos['charge_id']; ?>','barras');"> 
										<i class="fa fa-circle-o"></i> Barras
									</label> 
								</div>
								<div class="col-md-4">
									<label class="radio-custom"> 
										<input type="radio" name="radio-<?php echo $dataCargos['charge_id']; ?>" id="Chk_Puntos-<?php echo $dataCargos['charge_id']; ?>" onclick="CambiaGrafica('<?php echo $dataCargos['charge_id']; ?>','puntos');"> 
										<i class="fa fa-circle-o"></i> Puntos
									</label> 
								</div>
							</div>
						</div>
					</div>
					<!-- // Tab content END -->
					<input type="hidden" class="IdCharge_val" value="<?php echo $dataCargos['charge_id']; ?>" name="Charge<?php echo $dataCargos['charge_id']; ?>"/>
				<?php } ?>
			</div>
			<input type="hidden" value="<?php echo $diferencia_mes; ?>" name="cant_meses" id="cant_meses" />
			<input type="hidden" value="<?php echo $array_meses; ?>" name="intervalos_fechas" id="intervalos_fechas"/>
		</div>
	</div>
</div>
<!-- // Tabs END -->
<p class="separator text-center"></p>
<p class="text-center text-success" style="font-size:17px;"><strong>Nivel de Certificación</sotrong></p>
<p class="separator text-center"><i class="fa fa-bar-chart-o fa-3x"></i></p>
<div class="row border-none row-merge margin-none">
	<div class="col-md-12">
		<div id="placeholder-AvConcesionario" class="charges-placeholder" style="width: 98%; height: 350px; font-size: 14px; line-height: 1.2em;"></div>
	</div>
</div>

<p class="separator text-center"></p>
<p class="text-center text-success" style="font-size:17px;"><strong>Indicadores</sotrong></p>
<p class="separator text-center"><i class="fa fa-bar-chart-o fa-3x"></i></p>
<div class="row border-none row-merge margin-none">
	<div class="col-md-4">
		<span class="text-center">&nbsp; <i class='fa fa-fw icon-pie-graph'></i> Indicador de Ocupación</span>
		<div id="donut-ocupacion" style="width:100%;height:250px;"></div>
	</div>
	<div class="col-md-4">
		<span class="text-center">&nbsp; <i class='fa fa-fw icon-pie-graph'></i> Indicador de Avance Tipo Curso</span>
		<div id="donut-tipocurso" style="width:100%;height:250px;"></div>
	</div>
	<div class="col-md-4">
		<span class="center"> &nbsp;<i class='fa fa-fw icon-pie-graph'></i> Indicador de Cursos Programado Vs Ejecutado</span>
		<div id="donut-cursoprog" style="width:100%;height:250px;"></div>
	</div>
</div>
<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_dashboard.js"></script>
</body>
</html>