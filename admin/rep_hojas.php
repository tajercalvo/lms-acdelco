<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_hojas.php');
$location = 'reporting';
$locData = true;
$qtip = 'qtip';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_acumulado.php">Reporte Hojas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte Reporte Hojas</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">A continuación encontrará la información estructurada de las hojas de ruta de sus colaboradores durante determinado periodo de tiempo, por favor configure la información y de click en consultar para presentarle la información de su interés. Una vez consultada puede descargarla para realizar operaciones localmente (En su computador).</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<h5><a href="rep_hojas_excel.php?dealer_id=<?php echo($_POST['dealer_id']); ?>&start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			</div>
		</div>
		<form action="rep_hojas.php" method="post">
		<div class="row">
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="dealer_id" style="padding-top:8px;">Concesionario:</label>
					<div class="col-md-10">
					<input type="hidden" id="dealer_id_opc" name="dealer_id_opc" value="<?php echo($_SESSION['dealer_id']); ?>">
						<select style="width: 100%;" id="dealer_id" name="dealer_id" <?php if($_SESSION['max_rol']<=4){ ?>readonly="readonly"<?php } ?>>
							<option value="todos">Todos los concesionarios</option>
							<?php foreach ($datosHojas as $key => $Data_Cursos) { ?>
								<option value="<?php echo($Data_Cursos['dealer_id']); ?>" <?php if( (isset($_POST['dealer_id']) && $_POST['dealer_id']==$Data_Cursos['dealer_id']) ){ ?>selected="selected"<?php }elseif( (!isset($_POST['dealer_id'])) && (isset($_SESSION['dealer_id']) && $_SESSION['dealer_id']==$Data_Cursos['dealer_id']) ){ ?>selected="selected"<?php } ?> ><?php echo($Data_Cursos['dealer']); ?></option>
							<?php } ?>
						</select>
					</div>

				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
					<div class="col-md-9 input-group date">
				    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final:</label>
					<div class="col-md-9 input-group date">
				    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
			</div>
		</div>
		</form>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información: </h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<!-- Table elements-->
		<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
			<!-- Table heading -->
			<thead>
				<tr>
					<th data-hide="phone,tablet" style="width: 10%;">CONCESIONARIO</th>
					<th data-hide="phone,tablet" style="width: 25%;">COMPROMISOS</th>
					<th data-hide="phone,tablet" style="width: 25%;">FEEDBACK</th>
					<th data-hide="phone,tablet" style="width: 10%;">ENVIADA</th>
					<th data-hide="phone,tablet" style="width: 10%;">CUANDO</th>
					<th data-hide="phone,tablet" style="width: 10%;">CALIFICACIÓN</th>
					<th data-hide="phone,tablet" style="width: 10%;">USUARIO</th>
					<th data-hide="phone,tablet" style="width: 10%;">CURSO</th>
					<th data-hide="phone,tablet" style="width: 10%;">PROFESOR</th>
					<th data-hide="phone,tablet" style="width: 7%;">NOTA</th>
					<th data-hide="phone,tablet" style="width: 8%;">COMPLETA</th>
				</tr>
			</thead>
			<!-- // Table heading END -->
			<tbody>
				<?php
				if($cantidad_datos > 0){
					foreach ($datosHojas_all as $iID => $data) {
						?>
					<tr>
						<td><?php echo($data['dealer']); ?></td>
						<td><?php echo("<b>Compromisos:</b> ".$data['description']."<br><b>Qué:</b> ".$data['target']."<br><b>Cómo:</b> ".$data['action']); ?></td>
						<td><?php echo($data['feedback']); ?></td>
						<td><?php echo($data['date_creation']); ?></td>
						<td><?php echo($data['fec_fulfillment']); ?></td>
						<td><?php echo($data['fec_calificacion']); ?></td>
						<td><?php echo($data['first_name']." ".$data['last_name']); ?></td>
						<td><?php echo($data['newcode']." : ".$data['course']); ?></td>
						<td><?php echo($data['first_prof']." ".$data['last_prof']); ?></td>
						<td><?php echo($data['score']); ?></td>
						<td><?php echo($data['notaTotal']); ?></td>
					</tr>
				<?php } } ?>
			</tbody>
		</table>
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_hojas.js"></script>
</body>
</html>
