<?php
include('../config/config.php');
@session_start();
if( isset($_SESSION['id_evaluacion']) && $_SESSION['id_evaluacion'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: evaluar.php");
}else if( isset($_SESSION['_EvalCour_ResultId']) && $_SESSION['_EvalCour_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: evaluar.php");
}else if( isset($_SESSION['_EncSat_ResultId']) && $_SESSION['_EncSat_ResultId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuestar.php");
}else if( isset($_SESSION['_EnCarg_ReviewId']) && $_SESSION['_EnCarg_ReviewId'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuestar_Cargo.php");
}else if( isset($_SESSION['evl_obligatoria']) && $_SESSION['evl_obligatoria'] > 0 && isset($_SESSION['NameUsuario']) ){
	header("location: encuesta_gerentes.php");
}elseif(isset($_SESSION['NameUsuario'])){
	if( isset($_SESSION['url_solicitada'] ) and !strpos($_SESSION['url_solicitada'], 'login') ){
		header("location: ".$_SESSION['url_solicitada']);
	}else{
		header("location: ../admin/");
	}
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Autotrain</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="login/images/faviconAutotrain.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="login/css/util.css">
	<link rel="stylesheet" type="text/css" href="login/css/main.css">
<!--===============================================================================================-->
</head>
<style type="text/css">
	/*responsive*/
	@media only screen 
        and (min-device-width : 768px) 
        and (max-device-width : 1024px)  {
        .wrap-login100 {
            margin-left: 0px;
          }
      }

    @media only screen 
      and (min-device-width : 375px) 
      and (max-device-width : 667px) { 
        .wrap-login100 {
            margin-left: 0px;
        }
    }

    @media only screen 
    and (min-device-width : 414px) 
    and (max-device-width : 736px){ 
        .wrap-login100 {
            margin-left: 0px;
        }
    }

    @media only screen 
    and (min-device-width : 320px) 
    and (max-device-width : 568px)
    { 
        .wrap-login100 {
            margin-left: 0px;
        }
    }

    select:focus, input:focus{
    	outline: none;
	}
</style>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="wrap-login100 p-b-100">
				<span class="login100-form-title p-b-41">
					<img src="images/Logo.png" style="width: 100%;background: white;border-radius: 15px;">
				</span>
				<form class="login100-form validate-form p-b-33 p-t-5">

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Usuario" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="Contraseña" autocomplete="off" required="">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>
					<br>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<select class="input100" style="background-color: transparent; border: 0px solid;">
							<option value="">Pais</option>
							<option value="colombia">Chile</option>
							<option value="colombia">Colombia</option>
							<option value="colombia">Paraguay</option>
							<option value="colombia">Uruguay</option>
							<option value="colombia">Peru</option>
							<option value="colombia">Bolivia</option>
						</select>
					</div>
					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn">
							Ingresar
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="login/vendor/bootstrap/js/popper.js"></script>
	<script src="login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="login/vendor/daterangepicker/moment.min.js"></script>
	<script src="login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="login/js/main.js"></script>
</body>
<footer style="background-color: #1F2838; height: 7%; padding: 1%;">
	<img src="login/images/chile.png" style="width: 20px; margin: 3px;">
	<img src="login/images/colombia.png" style="width: 20px; margin: 3px;">
	<img src="login/images/paraguay.png" style="width: 20px; margin: 3px;">
	<img src="login/images/uruguay.png" style="width: 20px; margin: 3px;">
	<img src="login/images/peru.png" style="width: 20px; margin: 3px;">
	<img src="login/images/bolivia.png" style="width: 20px; margin: 3px;">
	<a href="https://luduscolombia.com.co/" target="_blank"><img src="images/logoLudus.png" style="width:30px; margin: 3px; float: right;"></a>
	<a href="https://autotrain.com.co/" target="_blank"><img src="images/LogoAutoTrain.png" style="width: 150px; margin: 3px; float: right;"></a>
</footer>
</html>