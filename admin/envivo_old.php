<?php include('src/seguridad.php'); ?>
<?php include('controllers/foros_tutor.php');
$location = 'tutor';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons webcam"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/foros_tutor.php">Transmisiones en Vivo</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- contenido interno -->
<?php if(!isset($_GET['id'])){ ?>
	<div class="widget widget-heading-simple widget-body-gray">
		<div class="innerAll spacing-x2">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="widget ">
									<!-- Category Heading -->
									<div class="innerAll half bg-gray border-bottom">
										<h4 class=" margin-bottom-none">Transmisiones en Vivo</h4>
										<div class="clearfix"></div>
									</div>
									<!-- End Category Heading -->
								<!-- Category Listing -->
								<div class="bg-gray-hover overflow-hidden">
									<div class="row innerAll half border-bottom">
										<div class="col-sm-9 col-xs-9">
											<ul class="media-list margin-none">
												<li class="media">
												    <a class="pull-left innerAll half " href="foros_tutor_detalle.php">
												  		<span class="empty-photo"><i class="fa fa-video-camera fa-2x text-muted"></i></span>
												    </a>
												    <div class="media-body">
													    <div class="innerAll half">
													    	<h4 class="margin-none">
													    		<a href="VideoEnVivo"onclick="OpcnVideo(1); return false;" class="media-heading strong text-primary" id="BtnVer1">Ver</a>
													    		<a href="VideoEnVivo" onclick="OpcnVideo(1); return false;" class="media-heading strong text-primary">Lanzamiento Cruze 2017</a>
													    	</h4>
													  		<div class="clearfix"></div>
													    	<small class="margin-none">Streaming del lanzamiento de la nueva generación del Chevrolet Cruze 2017, el carro que llega para desafiar el sentido común.</small> 
												    	</div>
												    	<div style="height: 485px; display: block;width: 858px;text-align: center;" id="transmision1">
												    		<!--<object type="application/x-shockwave-flash" data="https://cdn.livestream.com/swf/LSPlayer.swf" width="100%" height="100%" id="vod_player_114101675" style="visibility: visible;"><param name="wmode" value="opaque"><param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always"><param name="bgcolor" value="#000000"><param name="flashvars" value="account=17824327&amp;event=4906926&amp;autoPlay=true&amp;mute=undefined&amp;play_url=http://player-api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.smil&amp;generatedAt=null&amp;delayTuneIn=null&amp;thumbnail=http://img.new.livestream.com/events/00000000004adfae/b946d7e2-f75c-47f5-b14e-bf7b20015736_120.jpg&amp;qualities_bitrate=1000000&amp;qualities_height=540&amp;isVOD=true&amp;showLike=true&amp;isLiked=false&amp;showShare=true&amp;isWhiteLabel=false&amp;isStandAlone=false&amp;ad_account_id=null&amp;ad_provider_id=null&amp;ad_custom_params=null&amp;ad_enabled_for_vod=null&amp;ad_enabled_for_live=null&amp;ad_types=null&amp;isAdvancedAnyalitcsEnabled=null&amp;eventFullName=Lanzamiento Interno GMAcademy&amp;token=null&amp;callback=Application.flashPlayerCallbacks._114101675&amp;hide_external_links=false&amp;media=%7B%22mp4%22%3A%22http%3A//pdvod.new.livestream.com/events/00000000004adfae/b946d7e2-f75c-47f5-b14e-bf7b20015736_1000.mp4%3F__gda__%3D1456976354_6970cbf4e8aaec83e7617fad7806978f%22%2C%22smil%22%3A%22http%3A//api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.smil%22%2C%22m3u8%22%3A%22http%3A//api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.m3u8%22%2C%22f4m%22%3A%22http%3A//api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.f4m%22%7D&amp;viewerPlusId=undefined"></object>-->
												    		<iframe width="853" height="480" src="https://www.youtube.com/embed/52sgobKw5yc" frameborder="0" allowfullscreen></iframe>
												    	</div>
												    </div>
												</li>
												<li class="media">
												    <a class="pull-left innerAll half " href="foros_tutor_detalle.php">
												  		<span class="empty-photo"><i class="fa fa-video-camera fa-2x text-muted"></i></span>
												    </a>
												    <div class="media-body">
													    <div class="innerAll half">
													    	<h4 class="margin-none">
													    		<a href="VideoEnVivo"onclick="OpcnVideo(2); return false;" class="media-heading strong text-primary" id="BtnVer2">Ver</a>
													    		<a href="VideoEnVivo" onclick="OpcnVideo(2); return false;" class="media-heading strong text-primary">60 Años de GM Colmotores</a>
													    	</h4>
													  		<div class="clearfix"></div>
													    	<small class="margin-none">Transmisión en Vivo de la celebración de los 60 Años de GM Colmotores.</small> 
												    	</div>
												    	<div style="height: 855px; display: block;width: 858px;text-align: center;" id="transmision2">
												    		<!--<object type="application/x-shockwave-flash" data="https://cdn.livestream.com/swf/LSPlayer.swf" width="100%" height="100%" id="vod_player_114101675" style="visibility: visible;"><param name="wmode" value="opaque"><param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always"><param name="bgcolor" value="#000000"><param name="flashvars" value="account=17824327&amp;event=4906926&amp;autoPlay=true&amp;mute=undefined&amp;play_url=http://player-api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.smil&amp;generatedAt=null&amp;delayTuneIn=null&amp;thumbnail=http://img.new.livestream.com/events/00000000004adfae/b946d7e2-f75c-47f5-b14e-bf7b20015736_120.jpg&amp;qualities_bitrate=1000000&amp;qualities_height=540&amp;isVOD=true&amp;showLike=true&amp;isLiked=false&amp;showShare=true&amp;isWhiteLabel=false&amp;isStandAlone=false&amp;ad_account_id=null&amp;ad_provider_id=null&amp;ad_custom_params=null&amp;ad_enabled_for_vod=null&amp;ad_enabled_for_live=null&amp;ad_types=null&amp;isAdvancedAnyalitcsEnabled=null&amp;eventFullName=Lanzamiento Interno GMAcademy&amp;token=null&amp;callback=Application.flashPlayerCallbacks._114101675&amp;hide_external_links=false&amp;media=%7B%22mp4%22%3A%22http%3A//pdvod.new.livestream.com/events/00000000004adfae/b946d7e2-f75c-47f5-b14e-bf7b20015736_1000.mp4%3F__gda__%3D1456976354_6970cbf4e8aaec83e7617fad7806978f%22%2C%22smil%22%3A%22http%3A//api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.smil%22%2C%22m3u8%22%3A%22http%3A//api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.m3u8%22%2C%22f4m%22%3A%22http%3A//api.new.livestream.com/accounts/17824327/events/4906926/videos/114101675.f4m%22%7D&amp;viewerPlusId=undefined"></object>-->
												    		<!--<iframe width="853" height="480" src="https://www.facebook.com/chevroletcolombia/videos/1179108762148999/" frameborder="0" allowfullscreen></iframe>-->
												    		<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fchevroletcolombia%2Fvideos%2F1179108762148999%2F&show_text=0&width=853" width="853" height="853" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
												    	</div>
												    </div>
												</li>
											</ul>
										</div>
										<div class="col-sm-1 col-xs-1">
											<div class="text-center">
												<p class="lead strong margin-bottom-none"><?php echo(rand(5, 35)); ?></p>
												<span class="text-muted">EN LÍNEA</span>
											</div>
										</div>
										<div class="col-sm-2 col-xs-hidden">
											<div class="innerAll half">
												<div class="media">
													<a href="#" onclick="return false;" class="pull-left">
														<img src="../assets/images/usuarios/GM_LUDUS_20150709112904.jpg" style="width:35px;" class="media-object"/>
													</a>
													<div class="media-body">
														<a href="#" onclick="return false;" class="text-small">Felipe Heredia</a>
														<div class="clearfix"></div>
														<small>Asesor Estratégico</small>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- // END Category Listing -->
						</div>
						<!-- // END col-separator -->	
					</div>
					<!-- // END col -->
				</div>
				<!-- // END row -->
		</div>
	</div>
<?php } ?>



						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/envivo.js"></script>
</body>
</html>