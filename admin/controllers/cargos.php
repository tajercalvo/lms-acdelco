<?php
if(isset($_GET['opcn'])){
	include_once('models/cargos.php');
	$cargos_Class = new Cargos();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $cargos_Class->consultaRegistro($_GET['id']);
		$data_category = $cargos_Class->consultaCategorias();
		$data_school = $cargos_Class->consultaEscuelas();
	}else{
		$registroConfiguracion['nombre_lista'] 	= "";
		$registroConfiguracion['school_id'] 	= 0;
		$data_category = $cargos_Class->consultaCategorias();
		$data_school = $cargos_Class->consultaEscuelas();
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/cargos.php');
	$cargos_Class = new Cargos();
	if($_POST['opcn']=="crear"){
		$resultado = $cargos_Class->Crearcargos($_POST['nombre']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
		$resultado = $cargos_Class->Actualizarcargos($_POST['idElemento'],$_POST['nombre'],$_POST['estado']);
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas cargos
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.charge';
    if(isset($_GET['iSortCol_0'])){
		switch ($_GET['iSortCol_0']) {
			case '0':
				$sOrder = ' ORDER BY c.charge '.$_GET['sSortDir_0'];
			break;
			case '1':
				$sOrder = ' ORDER BY c.group_id '.$_GET['sSortDir_0'];
			break;
			case '2':
				$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
			break;
			case '3':
				$sOrder = ' ORDER BY c.date_creation '.$_GET['sSortDir_0'];
			break;
			case '4':
				$sOrder = ' ORDER BY s.first_name, s.last_name '.$_GET['sSortDir_0'];
			break;
			case '5':
				$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
			break;
			default:
				$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
			break;
		}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/cargos.php');
	$cargos_Class = new Cargos();
	$datosConfiguracion = $cargos_Class->consultaDatoscargos($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $cargos_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '<a href="op_cargo.php?opcn=editar&id='.$aInfo['charge_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	$aItem = array(
			$aInfo['charge'],
			// $aInfo['school'],
			// $aInfo['category'],
			// $aInfo['group_id'],
			$aInfo['first_name'].' '.$aInfo['last_name'],
			$aInfo['date_creation'],
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'],
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['charge_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/cargos.php');
	$cargos_Class = new Cargos();
	$cantidad_datos = $cargos_Class->consultaCantidad();
}
unset($cargos_Class);
