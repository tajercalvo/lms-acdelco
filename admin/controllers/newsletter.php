<?php
  include_once('models/newsletter.php');
  if(isset($_POST['opcn'])){
    switch($_POST['opcn']){
      case 'enviar':
      include_once('src/funciones_globales.php');
      //$funciones = new Funciones();
      $obj_newsl = new Newsletter();
      $para = "comunicaciones@gmacademy.co";
      $tipo_destinatario = isset($_POST['grupo']) ? $_POST['grupo'] : "";
      //print_r($_POST);
      if(isset($_POST['s_persona'])){
        $dpersona = count($_POST['s_persona']) > 0 ? implode(",",$_POST['s_persona']) : $_POST['s_persona'][0];
      }else{
        $dpersona = "";
      }
      if(isset($_POST['s_concesionario'])){
        $dconcesionario = count($_POST['s_concesionario']) > 0 ? implode(",",$_POST['s_concesionario']) : $_POST['s_concesionario'][0];
      }else{
        $dconcesionario = "";
      }
      if(isset($_POST['s_cargos'])){
        $dcargos = count($_POST['s_cargos']) > 0 ? implode(",",$_POST['s_cargos']) : $_POST['s_cargos'][0];
      }else{
        $dcargos = "";
      }
      $dgenero = ($_POST['s_genero'] == 1 || $_POST['s_genero'] == 2) ? $_POST['s_genero'] : 0;
      if(isset($_POST['s_rol'])){
        $drol = count($_POST['s_rol']) > 0 ? implode(",",$_POST['s_rol']) : $_POST['s_rol'][0];
      }else{
        $drol = "";
      }
      $asunto = isset($_POST['asunto']) ? filter_var($_POST['asunto'],FILTER_SANITIZE_STRING) : '';
      $archivo = (isset($_FILES['inputFile']) && $_FILES['inputFile']['name']!= "") ? Funciones::guardarArchivos('inputFile',"../assets/emails/") : '' ;
      $imagen = (isset($_FILES['imagen-mail']) && $_FILES['imagen-mail']['name']!= "") ? Funciones::guardarArchivos('imagen-mail',"../assets/emails/") : '' ;
      $texto = isset($_POST['textoEmail']) ? $_POST['textoEmail'] : '';
      $guardar = $obj_newsl->guardarCorreo($dpersona,$dconcesionario,$dcargos,$dgenero,$drol,'',$asunto,$archivo,$imagen,$texto);
      if($dpersona != ""){
        $epersona = $obj_newsl->emailPersonas($dpersona);
        foreach ($epersona as $key => $value) {
          if(strstr($value['email'],'@')){
            $para .= ",".$value['email'];
          }
        }
      }//fin if persona
      if($dconcesionario != "" || $dgenero != "" || $drol != "" || $dcargos != ""){
        $erol = $obj_newsl->email($dconcesionario,$dgenero,$drol,$dcargos);
        foreach ($erol as $key => $value) {
          if(strstr($value['email'],'@')){
            $para .= ",".$value['email'];
          }
        }
      }//fin validacion
      //echo($para);
      $resultado = $obj_newsl->redactarCorreo($asunto,"comunicaiones@gmacademy.co",$texto,$para,$imagen,$archivo);
      unset($obj_newsl);
      break;//fin case enviar
    }//fin switch
  }//fin if
  /*
  Se ejecutara las siguentes instrucciones cuando se cargue la pagina
  - Carga los datos principales en la pagina
  */
  $obj_newsl = new Newsletter();
  $correosEnviados = $obj_newsl->cargarCorreos();
  $listaConcesionarios = $obj_newsl->consultaConcesionarios();
  $listaPersonal = $obj_newsl->consultaPersonal();
  $listaCargos = $obj_newsl->consultaCargos();
  $listaRoles = $obj_newsl->consultaRoles();
  unset($obj_newsl);
?>
