<?php

if(isset($_POST['opcn'])){
	
	switch ( $_POST['opcn'] ) {

		case 'consulta_sesion':
			$json = [];
			include_once('../models/costo_especial.php');
			$p = $_POST;

            $sesiones_datos = Costo_Especial::consulta_Schedule( $p, "../" );

			foreach ($sesiones_datos as $key => $v) {
				 $json[] = ['id'=>$v['schedule_id'], 'text'=>($v['city'].' | '.$v['start_date'].' | '.$v['newcode'].' | '.$v['type'].' | '.$v['course'])];
			}
			echo json_encode($json);
		break;

		case 'sel_dealer':
			include_once('../models/costo_especial.php');
			
			$dealer_datos = Costo_Especial::consulta_Dealer( $_POST['schedule_id'], "../" );
			echo( json_encode( $dealer_datos ) );
		break;

		case 'sel_all_dealer':
			include_once('../models/costo_especial.php');
			
			$dealer_datos = Costo_Especial::consulta_All_Dealer( "../" );
			echo( json_encode( $dealer_datos ) );
		break;

		case 'crear':
			include_once('../models/costo_especial.php');
			
			$p = $_POST;
			if ($p['tipo'] == 1) {
				$sesiones_fecha = Costo_Especial::consulta_Fecha_Schedule( $p['schedule_id'], "../" );
				$p['start_date_day'] = $sesiones_fecha['start_date'];
			}
			if ($p['tipo'] == 2) {
				$p['schedule_id'] = 0;
			}

			$nuevo_costo = Costo_Especial::crear_Costo( $p, "../" );

			if( $nuevo_costo > 0 ){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;

		case 'editar':
			include_once('../models/costo_especial.php');
			
			$p = $_POST;
			if ($p['tipo'] == 1) {
				$sesiones_fecha = Costo_Especial::consulta_Fecha_Schedule( $p['schedule_id'], "../" );
				$p['start_date_day'] = $sesiones_fecha['start_date'];
			}
			if ($p['tipo'] == 2) {
				$p['schedule_id'] = 0;
			}

			$editar_costo = Costo_Especial::actualizar_Costo( $p, "../" );

			if( $editar_costo > 0 ){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;

		case 'consulta_costo_especial':
			include_once('../models/costo_especial.php');

			$json = [];
			$costo_especial_datos = Costo_Especial::consulta_Costo_Especial( $_POST['id_extra_cost'], "../" );

			if ( $costo_especial_datos > 0 ) {

			 	$costo_especial_datos['start_date'] = substr($costo_especial_datos['start_date'], 0, 10);
				$costo_especial_datos['end_date'] = substr($costo_especial_datos['end_date'], 0, 10);
				$costo_especial_datos['date_cost'] = substr($costo_especial_datos['date_cost'], 0, 10);

				$json = $costo_especial_datos;
			 } 

			 echo( json_encode( $json ) ); 

		break;


	}
	
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.schedule_id';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.schedule_id '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='1') {
    		$sOrder = ' ORDER BY c.dealer_id '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY c.course '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='3') {
    		$sOrder = ' ORDER BY c.start_date '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY c.type '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/costo_especial.php');

	$costo_datos = Costo_Especial::consulta_Costo( $sWhere,$sOrder,$sLimit, "../" );
	$cantidad_datos = Costo_Especial::consulta_Costo_CantidadFull( $sWhere,$sOrder,$sLimit, "../" );

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($costo_datos),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );

    $val_type = '';
    $meses_ES = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	$meses_EN = array("January","February","March","April","May","June","July","August","September","October","November","December");
	  
    foreach ($costo_datos as $iID => $aInfo) {

    	if($aInfo['status_id']==1){ //Estado del Costo Especial
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Inactivo</span>';
    	}

    	// Tipo de Costo Especial
    	if($aInfo['type']==1){ 
    		$val_type = 'Valor Ajuste a Sesion';
    	}
    	if($aInfo['type']==2){ 
    		$val_type = 'Valor Ajuste de Mes';
    	}

    	//Ajuste Idioma de Fecha
		 $fecha = substr($aInfo['date_cost'], 0, 10);
		 $mes = date('F', strtotime($fecha));
		 $anio = date('Y', strtotime($fecha));
		 $nombreMes = str_replace($meses_EN, $meses_ES, $mes);

    	$valOpciones = '<a href="op_costo_especial.php?opcn=editar&id='.$aInfo['id_extra_cost'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	$aItem = array( 
			$val_type, 
			$aInfo['cost_total'], 
			$nombreMes.'-'.$anio,
			$aInfo['schedule_id'], 
			$aInfo['dealer'], 
			$aInfo['course'], 
			$aInfo['headquarter'], 
			$aInfo['start_date'].'-'.$aInfo['end_date'],
			$aInfo['first_name'].' '.$aInfo['last_name'], 
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['id_extra_cost']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}
else{
	include_once('models/costo_especial.php');
	$cantidad_datos = Costo_Especial::consulta_Costo_CantidadFull( "","","","" );
}