<?php
//print_r($_POST);
if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/categorias_productos.php');
	$especialidades_Class = new Especialidades();
	if($_POST['opcn']=="crear"){
		$resultado = $especialidades_Class->Crear_categoria($_POST['txt_crearEspecialidad'], $_POST['select_crear_estado'], $_POST['txt_colorEspecialidad']);
		//echo $resultado;
		if($resultado>0){
			$datos['error']= false;
			$datos['mensaje']= 'Categoría creada correctamente';
			echo json_encode($datos);
		}else if(!$resultado){
			$datos['error']= true;
			$datos['mensaje']= 'Error al crear la categoría';
			echo json_encode($datos);
		}
	}elseif($_POST['opcn']=="editar"){
		$resultado = $especialidades_Class->Actualizar_categoria($_POST['id_Especialidad'],$_POST['txt_Especialidad'],$_POST['estado']);
		if($resultado == 1){
			echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		}
	return false;
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas especialidades
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.specialty';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY argument_cat '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='2') {
    		$sOrder = ' ORDER BY first_name, last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY date_creation '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY first_name, last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='5') {
    		$sOrder = ' ORDER BY date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/categorias_productos.php');
	$especialidades_Class = new Especialidades();
	$datosConfiguracion = $especialidades_Class->consulta_categorias($sWhere,$sOrder,$sLimit);
	//$cantidad_datos = $especialidades_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);
	$cantidad_datos = count($datosConfiguracion);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    		$data_estado = 'Activo';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    		$data_estado = 'Inactivo';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    		$data_estado = 'Error';
    	}
    	$valOpciones = '<a data-id="'.$aInfo['argument_cat_id'].'" data-estado="'.$data_estado.'" href="" class="editar_archivos btn-action glyphicons pencil btn-success"><i></i></a>';
    	$aItem = array(
			$aInfo['argument_cat'],
			$aInfo['first_name'].' '.$aInfo['last_name'],
			$aInfo['date_creation'],
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'],
			$aInfo['date_edition'],
			$aInfo['color'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['argument_cat_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/especialidades.php');
	$especialidades_Class = new Especialidades();
	$cantidad_datos = $especialidades_Class->consultaCantidad();
}
unset($especialidades_Class);