<?php
if(isset($_GET['opcn'])){
	include_once('models/asignaturas.php');
	$asignaturas_Class = new asignaturas();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $asignaturas_Class->consultaRegistro($_GET['id']);
	}else{
		$registroConfiguracion['nombre_lista'] = "";
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/asignaturas.php');
	$asignaturas_Class = new asignaturas();
	if($_POST['opcn']=="crear"){
		$resultado = $asignaturas_Class->Crearasignaturas($_POST['nombre']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
		$resultado = $asignaturas_Class->Actualizarasignaturas($_POST['idElemento'],$_POST['nombre'],$_POST['estado']);
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas asignaturas
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.subject';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.subject '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='2') {
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY c.date_creation '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY s.first_name, s.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='5') {
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/asignaturas.php');
	$asignaturas_Class = new asignaturas();
	$datosConfiguracion = $asignaturas_Class->consultaDatosasignaturas($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $asignaturas_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '<a href="op_asignatura.php?opcn=editar&id='.$aInfo['subject_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	$aItem = array(
			$aInfo['subject'], 
			$aInfo['first_name'].' '.$aInfo['last_name'], 
			$aInfo['date_creation'], 
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'], 
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['subject_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/asignaturas.php');
	$asignaturas_Class = new asignaturas();
	$cantidad_datos = $asignaturas_Class->consultaCantidad();
}
unset($asignaturas_Class);