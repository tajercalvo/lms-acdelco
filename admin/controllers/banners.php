<?php
if(isset($_POST['opcn'])){
	//operaciones con los datos
	switch($_POST['opcn']){
		case 'crear':
			include_once('models/banners.php');
			include_once('src/funciones_globales.php');
			$funciones = new Funciones();
			$banner_Class = new Banners();
			$resultado = 0;
			$bannerName = filter_var($_POST['banner_name'],FILTER_SANITIZE_STRING);
			$url= filter_var($_POST['banner_url'],FILTER_SANITIZE_STRING);
			if(isset($_FILES['file_name'])){
				$fileName = $funciones->guardarArchivo('file_name',"../assets/gallery/banners/");
				$charges= isset($_POST['cargos']) ? implode(",",$_POST['cargos']) : "";
				$dealers = isset($_POST['concesionarios']) ? implode(",",$_POST['concesionarios']) : "";
				$resultado = $banner_Class->CrearBanner($_POST['banner_type'],$bannerName,$fileName,$_POST['status_id'],$_POST['start_date'],$_POST['end_date'],$url,$charges,$dealers,$_POST['gender']);
			}
			header("location: banners.php");
			break;//crear
		case 'editar':
			include_once('src/funciones_globales.php');
			include_once('models/banners.php');
			$banner_Class = new Banners();
			$funciones = new Funciones();
			$bannerName = filter_var($_POST['banner_name'],FILTER_SANITIZE_STRING);
			$url= filter_var($_POST['banner_url'],FILTER_SANITIZE_STRING);
			$charges= ( isset( $_POST['cargos'] ) && count( $_POST['cargos'] ) > 0 ) ? implode(",",$_POST['cargos']) : '';
			$dealers = ( isset( $_POST['concesionarios'] ) && count( $_POST['concesionarios'] ) > 0 ) ? implode(",",$_POST['concesionarios']) : '';
			if($_FILES['file_name']['name'] != ""){
				$fileName = $funciones->guardarArchivo('file_name',"../assets/gallery/banners/",$_POST['imgant']);
			}else{
				$fileName = $_POST['imgant'];
			}
			$resultado = $banner_Class->ActualizarBanner($_POST['banner_id'],$_POST['banner_type'],$bannerName,$fileName,$_POST['status_id'],$_POST['start_date'],$_POST['end_date'],$url,$charges,$dealers,$_POST['gender']);
			header("location: op_banners.php?opcn=editar&id={$_POST['banner_id']}");
		break;//editar
		case 'click':
			include_once("../models/banners.php");
			$banner_Class = new Banners();
			$banner_id = $_POST['id'];
			$resultado_insert = $banner_Class->guardarClick($banner_id);
		break;
	}//fin switch opcn
	unset($banner_Class);
}else if(isset($_GET['opcn'])){
	switch($_GET['opcn']){
		case "editar":
			if(isset($_GET['id'])){
				include_once('models/banners.php');
				$banner_Class = new Banners();
				//consultas individuales
				$registroConfiguracion = $banner_Class->consultaRegistro($_GET['id']);
				$tipoBanner= $banner_Class->tipoBanner();
				$listaConcesionarios = $banner_Class->consultaConcesionarios();
				$charge_active = $banner_Class->consultaAllDatosCargos('',1);
				$cargosSeleccionados = explode(",",$registroConfiguracion['charge_id']);
				$dealersSeleccionados = explode(",",$registroConfiguracion['dealers_id']);
			}
			//header("location: banners.php");
		break;


/*
Cristian
Semana del 24 de septiembre
Recordad que en se movio la linea 2 y 3 y se colocoaron en la 6  y 7
Inicia la elaboracion de la paginación
*/
		case 'tabla':
		$sWhere = '';
		if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
				$sWhere = $_GET['sSearch'];
		}

		$sOrder = ' ORDER BY b.banner_id';
    if(isset($_GET['iSortCol_0'])){
    	if ($_GET['iSortCol_0']=='2') {
				$sOrder = ' ORDER BY t.banners_type'.$_GET['sSortDir_0'];


			}else{
    		$sOrder = ' ORDER BY b.status_id '.$_GET['sSortDir_0'];
    	}
    }
		$sLimit = '';
		if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
				$sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
		}

			include_once('../models/banners.php');
			$banner_Class = new Banners();
			$tablaBanners = $banner_Class->consultaDatosBanners($sWhere,$sOrder,$sLimit,'../');
			$cantidad_datos = $banner_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit,'../');
			$data = array(
				'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($tablaBanners),
        'iTotalDisplayRecords' => count( $cantidad_datos ),
        'aaData' => array()
			);
			$cont = 0;
			foreach ($tablaBanners as $key => $value) {
				if($value['status_id']==1){
	    		$estado = '<span class="label label-success">Activo</span>';
	    	}elseif($value['status_id']==2){
	    		$estado = '<span class="label label-danger">Inactivo</span>';
	    	}else{
	    		$estado = '<span class="label label-important">Error</span>';
	    	}

				$aux[0] = $value['banners_type'];
				$aux[1] = $value['banner_name'];
				$aux[2] = $value['file_name'];
				$aux[3] = $estado;
				$aux[4] = $value['first_name']." ".$value['last_name'];
				$aux[5] = $value['start_date'];
				$aux[6] = $value['end_date'];
				$aux[7] = $value['banner_url'];
				$aux[8] = $value['create_date'];
				$aux[9] = $value['clicks'];
				$aux[10] = '<a href="op_banners.php?opcn=editar&id='.$value['banner_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';

				$data['aaData'][$cont] = $aux;
				$cont ++;
			}
			echo json_encode($data);
		break;
	}
	unset($banner_Class);
// Fin de la paginacion
}else{
	include_once('models/banners.php');
	$banner_Class = new Banners();
	$cantidad_datos = $banner_Class->consultaCantidad();
	$tipo_banner = $banner_Class->tipoBanner();
	$listaConcesionarios = $banner_Class->consultaConcesionarios();
	$charge_active = $banner_Class->consultaAllDatosCargos('',1);
	$lista_cargo= array();
	$lista_concesionarios = array();
	$dealersSeleccionados = array();

	unset($banner_Class);
}//fin else
