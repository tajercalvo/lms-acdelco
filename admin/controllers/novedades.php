<?php
if(isset($_POST['opcn'])){
	if($_POST['opcn']=="crear"){
		include_once('models/noticias.php');
		$Noticias_Class = new Noticias();
		//Proceso de creación para la imagen
		if(isset($_FILES['image_new'])){
			if(isset($_FILES['image_new']['tmp_name'])){
				$imgant = "";
				if(isset($_POST['imgant'])){
					$imgant = $_POST['imgant'];
				}
				$nom_archivo1 = $_FILES["image_new"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../assets/gallery/source/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/source/".$imgant)){
									unlink("../assets/gallery/source/".$imgant);
								}
							}
						}else{
							$new_name1 = "default.png";
						}
				}else{
					$new_name1 = "default.png";
				}
			}else{
				$new_name1 = "default.png";
			}
		}else{
			$new_name1 = "default.png";
		}
		//Proceso de creación para la imagen
		$resultado = $Noticias_Class->CrearNoticia($_POST['news'],$_POST['description'],$new_name1,'2',$_POST['media_id']);
		$Noticias_datos = $Noticias_Class->consultaNoticias('2');
		$Argumentarios_Meddatos = $Noticias_Class->consultaMed();
	}elseif($_POST['opcn']=="editar"){
		include_once('models/noticias.php');
		$Noticias_Class = new Noticias();
		//Proceso de creación para la imagen
			$imgant = "";
			if(isset($_POST['imgant'])){
				$imgant = $_POST['imgant'];
			}
		if(isset($_FILES['image_new_ed'])){
			if(isset($_FILES['image_new_ed']['tmp_name']) && $_FILES['image_new_ed']['tmp_name']!="" ){
				$nom_archivo1 = $_FILES["image_new_ed"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new_ed"]["tmp_name"], "../assets/gallery/source/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/source/".$imgant)){
									unlink("../assets/gallery/source/".$imgant);
								}
							}
						}else{
							$new_name1 = $imgant;
						}
				}else{
					$new_name1 = $imgant;
				}
			}else{
				$new_name1 = $imgant;
			}
		}else{
			$new_name1 = $imgant;
		}
		//Proceso de creación para la imagen
		$resultado = $Noticias_Class->ActualizarNoticia($_POST['news_id'],$_POST['news_ed'],$_POST['description_ed'],$new_name1,$_POST['estado'],$_POST['media_id_ed']);
	}elseif($_POST['opcn']=="LikeGalery"){
		include_once('../models/noticias.php');
		$Noticias_Class = new Noticias();
		$resultado = $Noticias_Class->CrearLike($_POST['news_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}
}
if(isset($_GET['id'])){
	include_once('models/noticias.php');
	$Noticias_Class = new Noticias();
	$Noticia_datos = $Noticias_Class->consultaNoticia($_GET['id']);
	$Argumentarios_Meddatos = $Noticias_Class->consultaMed();
	$Galeria_datos = $Noticias_Class->consultaGaleria($_GET['id']);
}

if( !isset($_POST['opcn']) && !isset($_GET['opcn']) ){
	include_once('models/noticias.php');
	$Noticias_Class = new Noticias();
	$Noticias_datos = $Noticias_Class->consultaNoticias('2');
	$Argumentarios_Meddatos = $Noticias_Class->consultaMed();
}
unset($Noticias_Class);