<?php
include_once('models/rep_asistencia_cupos.php');
$Asistencias_Class = new RepAsistenciasTotales();
$cantidad_datos = 0;

if(isset($_POST['start_date_day'])){
	$datosCursos_all = $Asistencias_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Asistencias_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Asistencias_Class);