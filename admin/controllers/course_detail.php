<?php
if($_GET['token']!=md5("GMAcademy")){
	header("location: ../admin/");
  }elseif(isset($_GET['opcn'])){
	include_once('models/course_detail.php');
	$Cursos_Class = new Cursos();
	if(isset($_GET['id'])){
		if(isset($_GET['opcn_f'])&&($_GET['opcn_f']=="borrarFile")){
			$nomFile = $_GET['nomFile'];
			$ResultadoBorrar = $Cursos_Class->BorrarArchivo($nomFile);
			if($ResultadoBorrar>0){
				if(file_exists("../assets/FilesCursos/".$nomFile)){
					unlink("../assets/FilesCursos/".$nomFile);
				}
			}
		}
		$id_usuario_ctrl = 0;
		if(isset($id_usuario)){
			$id_usuario_ctrl = $id_usuario;
		}
		//consultas individuales
		$paso = false;
		$registroConfiguracion = $Cursos_Class->consultaRegistro($_GET['id']);
		//$listadosAsignaturas = $Cursos_Class->consultaAsignaturas('');
		$listadosProveedores = $Cursos_Class->consultaProveedores('');
		$registroConfiguracionDetail = $Cursos_Class->consultaRegistroDetail($_GET['id']);
		$registroConfiguracionModulosDetail = $Cursos_Class->consultaRegistroDetailModulos($_GET['id']);
		$inscritoCourse = $Cursos_Class->ConsultaInscripcion($_GET['id']);
		$confirmaResultado = $Cursos_Class->consultaPasoCurso($_GET['id'],$id_usuario_ctrl);
		$ModulosScorm = $Cursos_Class->ConsultaScos($_GET['id']);
		$MaterialCargado = $Cursos_Class->ConsultaArchivos($_GET['id']);
		$preguntas = $Cursos_Class->consultaPreguntasSeguridad($_SESSION['idUsuario']);
		if(!$inscritoCourse){
			if($_GET['opcn']=="inscripcion"){
				$resultadoInscripcion = $Cursos_Class->InscribirseCurso($_GET['id']);
				if($resultadoInscripcion>0){
					$inscritoCourse = true;
				}
			}
		}

		if($confirmaResultado>0){
			$inscritoCourse = true;
			$paso = true;
		}
	}else{
		header("location: ../admin/");
	}
}elseif(isset($_POST['opcn'])){
	if($_POST['opcn']=='CrearRecurso'){
		if(isset($_FILES['image_new'])){
			if(isset($_FILES['image_new']['tmp_name'])){
				include_once('../models/course_detail.php');
				$Cursos_Class = new Cursos();
				$name = $_POST['name'];
				$course_id = $_POST['course_id'];
				$nom_archivo1 = $_FILES["image_new"]["name"];
				$new_name1 = "GM_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "pdf";
				preg_match("'^(.*)\.(pdf|PDF|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'pdf' : $new_extension1 = ".pdf";
						break;
					case 'PDF' : $new_extension1 = ".PDF";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../../assets/FilesCursos/".$new_name1);
						if($resultArchivo1==1){
							$op_cursoImagen = $Cursos_Class->actualizaFile($course_id,$name,$new_name1);
							echo('{"resultado":"SI","archivo":"'.$new_name1.'"}');
						}else{
							echo('{"resultado":"NO"}');
						}
				}else{
					echo('{"resultado":"NO"}');
				}
			}else{
				echo('{"resultado":"NO"}');
			}
		}
	}
}
unset($Cursos_Class);
