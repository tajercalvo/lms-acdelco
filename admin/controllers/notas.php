<?php
if(isset($_GET['opcn'])){
	include_once('models/notas.php');
	$notas_Class = new Notas();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $notas_Class->consultaRegistro($_GET['id']);
		$listadosCursos = $notas_Class->consultaCursos('');
        $listadosUsuarios = $notas_Class->consultaUsuarios('');
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		$listadosCursos = $notas_Class->consultaCursos('');
        $listadosUsuarios = $notas_Class->consultaUsuarios('');
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	if($_POST['opcn']=="crear"){
        include_once('../models/notas.php');
        $notas_Class = new Notas();
		$resultado = $notas_Class->CrearNotas($_POST['score'],$_POST['user_id'],$_POST['module_id'],$_POST['approval']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
        include_once('../models/notas.php');
        $notas_Class = new Notas();
		$resultado = $notas_Class->ActualizarNotas($_POST['idElemento'],$_POST['score'],$_POST['user_id'],$_POST['module_id'],$_POST['approval']);
		echo('{"resultado":"SI"}');
	}elseif($_POST['opcn']=="filtrar"){
        include_once('models/notas.php');
        $notas_Class = new Notas();
        @session_start();
        /* OK - Filtro completo*/
        if($_POST['module_id']!='0'){
            $_SESSION['module_id_not_flr'] = $_POST['module_id'];
        }else{
            if(isset($_SESSION['module_id_not_flr'])){
                unset($_SESSION['module_id_not_flr']);
            }
        }
        if($_POST['user_id']!='0'){
            $_SESSION['user_id_not_flr'] = $_POST['user_id'];
        }else{
            if(isset($_SESSION['user_id_not_flr'])){
                unset($_SESSION['user_id_not_flr']);
            }
        }
        if($_POST['score']!=''){
            $_SESSION['score_not_flr'] = $_POST['score'];
        }else{
            if(isset($_SESSION['score_not_flr'])){
                unset($_SESSION['score_not_flr']);
            }
        }
        if($_POST['approval']!='0'){
            $_SESSION['approval_not_flr'] = $_POST['approval'];
        }else{
            if(isset($_SESSION['approval_not_flr'])){
                unset($_SESSION['approval_not_flr']);
            }
        }
        $cantidad_datos = $notas_Class->consultaCantidad();
        $listadosCursos = $notas_Class->consultaCursos('');
        $listadosUsuarios = $notas_Class->consultaUsuarios('');
        /* OK - Filtro completo*/
    }
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas notas
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY m.newcode, m.module';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY m.newcode, m.module '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY mr.score '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
            $sOrder = ' ORDER BY mr.score_review '.$_GET['sSortDir_0'];
        }elseif($_GET['iSortCol_0']=='4'){
            $sOrder = ' ORDER BY mr.score_waybill '.$_GET['sSortDir_0'];
        }elseif($_GET['iSortCol_0']=='5'){
    		$sOrder = ' ORDER BY mr.approval '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='6') {
    		$sOrder = ' ORDER BY u1.first_name, u1.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='7') {
    		$sOrder = ' ORDER BY c.date_creation '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY m.module '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/notas.php');
	$notas_Class = new Notas();
	$datosConfiguracion = $notas_Class->consultaDatosNota($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $notas_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    @session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
    		$valOpciones = '<a href="op_nota.php?opcn=editar&id='.$aInfo['module_result_usr_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	}
    	$aItem = array(
			$aInfo['newcode'].' | '.$aInfo['module'],
			$aInfo['first_name'].' '.$aInfo['last_name'].' '.$aInfo['identification'],
			$aInfo['score'],
            $aInfo['score_review'],
            $aInfo['score_waybill'],
			$aInfo['approval'], 
			$aInfo['name_crea'].' '.$aInfo['ape_crea'],
			$aInfo['date_creation'],
			$valOpciones,
			'DT_RowId' => $aInfo['module_result_usr_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/notas.php');
	$notas_Class = new Notas();
	$cantidad_datos = $notas_Class->consultaCantidad();
	$listadosCursos = $notas_Class->consultaCursos('');
    $listadosUsuarios = $notas_Class->consultaUsuarios('');
}
unset($notas_Class);