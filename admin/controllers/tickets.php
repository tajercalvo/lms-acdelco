<?php
if( count($_GET) == 0 && count($_POST) == 0 ){//fin get
    include_once("models/tickets.php");
    $object_class = new Tickets();
    $lista_general = $object_class->tiposCasos();
    unset($object_class);
}//fin post

if( isset($_POST['opcn']) ){
    include_once("../models/tickets.php");
    $object_class = new Tickets();
    switch( $_POST['opcn'] ){
        case 'crear':
            include_once('../src/funciones_globales.php');
            $imagen = (isset($_FILES['faq_img']) && $_FILES['faq_img']['tmp_name'] != "" ) ? Funciones::guardarImagenes( 'faq_img', "faq_","../../assets/FAQ/") : "" ;
            if( strpos( $imagen, 'default' ) === false ){
                $texto = filter_var( $_POST['faq_text'], FILTER_SANITIZE_STRING );
                //$texto = $_POST['faq_text'] ;
                $nuevo_ticket = $object_class->crearTicket( $_POST['prioridad'], $texto, $_POST['tema'], $imagen );
                if( $nuevo_ticket > 0 ){
                    $respuesta = [
                        "error"     => "no",
                        "ticket"    => "<div class='col-md-10'>Se ha reportado correctamente el problema. Su número de ticket es <strong class='text-primary'>AT-00$nuevo_ticket</strong></div>"
                    ];
                    $mensaje = "Se ha reportado corréctamente el problema. Su número de ticket es <a><strong>AT-00$nuevo_ticket</strong></a><br>En el transcurso de las próximas <a><strong>24 horas</strong></a> se estará brindando la respuesta correspodiente.";
                    $resultadoMensaje = $object_class->crearMensaje( $mensaje, $_SESSION['idUsuario'], 1 );
                } else {
                    $respuesta = [
                        "error"         => "si",
                        "descripcion"   => "Error al intentar generar el ticket"
                    ];
                }
            } else {
                $respuesta = [
                    "error"         => "si",
                    "descripcion"   => "La imagen no se pudo enviar, por favor valide que esta sea en formato .png o .jpg"
                ];
            }
            echo( json_encode( $respuesta ) );
        break;

        case 'reabrir':
            include_once('../src/funciones_globales.php');
            $imagen = (isset($_FILES['faq_img_r']) && $_FILES['faq_img_r']['tmp_name'] != "" ) ? Funciones::guardarImagenes( 'faq_img_r', "faq_","../../assets/FAQ/") : "" ;
            if( strpos( $imagen, 'default' ) === false ){
                $texto = addslashes( $_POST['faq_text_r'] );
                $nuevo_ticket = $object_class->actualizarEstado( 3, $_POST['ticket_id'] );
                $nuevo_texto = $object_class->cerrarTicket($_POST['ticket_id'] ,$texto, $imagen );
                if( $nuevo_ticket > 0 ){
                    $respuesta = [
                        "error"     => "no",
                        "ticket"    => "<div class='col-md-10'>Se ha reportado correctamente el problema. En el transcurso de las próximas 24 horas validaremos nuevamente el ticket <strong class='text-primary'>AT-00$nuevo_ticket</strong></div>"
                    ];
                    $mensaje = "Su numero de ticket <a><strong>AT-00$nuevo_ticket</strong></a><br> será validado nuevamente en el transcurso de las próximas <a><strong>24 horas</strong></a> se estará brindando la respuesta correspodiente.";
                    $resultadoMensaje = $object_class->crearMensaje( $mensaje, $_SESSION['idUsuario'], 1 );
                } else {
                    $respuesta = [
                        "error"         => "si",
                        "descripcion"   => "Error al intentar reabrir el ticket"
                    ];
                }
            } else {
                $respuesta = [
                    "error"         => "si",
                    "descripcion"   => "La imagen no se pudo enviar, por favor valide que esta sea en formato .png o .jpg"
                ];
            }
            echo( json_encode( $respuesta ) );
        break;

        case 'tablaTickets':
            @session_start();
            $user = $_SESSION['idUsuario'];
            $tickets = $object_class->traerListaTickets( 1, "", $user );
            ?>
            <div class="panel-group">
                <table class="footable table table-striped table-primary default footable-loaded">
                    <thead>
                        <tr class="row">
                            <th class="col-md-1">Ticket</th>
                            <th class="col-md-1">Fecha Creación</th>
                            <th class="col-md-2">Tipo</th>
                            <th class="col-md-4">Descripción</th>
                            <th class="col-md-2">Captura de pantalla</th>
                            <th class="col-md-1">Estado</th>
                            <th class="col-md-1">Opción</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php if( count( $tickets ) > 0 ){
                foreach( $tickets as $key => $value ){ ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading<?php echo($value['ticket_id']); ?>">
                        <div class="row">
                            <div class="col-md-1">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo($value['ticket_id']); ?>" aria-expanded="false" aria-controls="collapse<?php echo($value['ticket_id']); ?>">
                                    AT-00<?php echo( $value['ticket_id'] ); ?>
                                </a>
                            </div>
                            <div class="col-md-1"><?php echo( $value['date_creation'] ); ?></div>
                            <div class="col-md-2"><?php echo( $value['key_description'] ); ?></div>
                            <div class="col-md-4"><?php echo( $value['description'] ); ?></div>
                            <div class="col-md-2"><span style="width: 100%"><?php if( $value['image'] != "" ){ echo( $value['image'] ); } ?></span></div>
                            <div class="col-md-1"id='estado_<?php echo( $value['ticket_id'] ); ?>'>
                                <?php
                                switch( $value['status_id'] ){
                                    case 0: echo("<label class='label label-danger'>Cerrado</label>"); break;
                                    case 1: echo("<label class='label label-primary'>Abierto</label>"); break;
                                    case 3: echo("<label class='label label-info'>Reabierto</label>"); break;
                                } ?>
                            </div>
                            <div class="col-md-1" id='reabrir_<?php echo( $value['ticket_id'] ); ?>'>
                                <?php if( $value['status_id'] == 0 ){ echo("<a href='#' onclick='abrirModal( {$value['ticket_id']} )'>reabrir</a>"); } ?>
                            </div>
                        </div>
                    </div>
                    <div id="collapse<?php echo($value['ticket_id']); ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo($value['ticket_id']); ?>">
                        <div class="panel-body">
                            <div class="row">
                                <?php if( isset( $value['respuestas'] ) && count( $value['respuestas'] ) > 0 ){
                                foreach ($value['respuestas'] as $key => $valueRes) { ?>
                                <div class="media">
                                    <a class="pull-left" href="#">
                                         <span class="empty-photo agent">
                                            <i class="icon-life-raft"></i>
                                        </span>
                                    </a>
                                    <div class="media-body">
                                        <h5><span class="media-heading <?php if( $valueRes['user_id'] == $_SESSION['idUsuario'] ){ echo('text-default'); } else { echo('text-primary'); } ?>"> <?php echo( $valueRes['first_name']." ".$valueRes['last_name'] ); ?></span> </h5>
                                        <h6><strong class="pull-right"><?php echo $valueRes['date_creation'] ?> <i class="icon-time-clock "></i></strong></h6>
                                        <div class="clearfix"></div>
                                        <div class="innerT half">
                                            <span class="prettyprint"><strong class="text-warning">Respuesta:</strong> <?php echo $valueRes['description']; ?></span>
                                            <!-- <pre class="prettyprint"></pre> -->
                                        </div>
                                        <br>
                                        <?php if( $valueRes['image_answer'] != "" ){ ?>
                                        <span>
                                            <i class="icon-life-raft innerL"></i><strong>Captura de pantalla:</strong>
                                            <br>
                                            <a style="width:90%"><span class="text-primary"><img style="max-width: 1000px" src="../assets/FAQ/<?php echo($valueRes['image_answer']); ?>" alt="<?php echo($valueRes['image_answer']); ?>"></span></a>
                                        </span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <hr>
                                <?php }
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }
                } ?>
            </div>
            <?php
        break;
    }//fin switch
}//fin isset POST

if( isset($_GET['opcn']) ){
    include_once("../models/tickets.php");
    $object_class = new Tickets();
    switch( $_GET['opcn'] ){

        case 'tickets':
            $total = 0;
            $numero_tickets = $object_class->cuentaTickets();
            $pendientes_tickets = $object_class->cuentaTickets( 'pendientes' );
            $reabiertos_tickets = $object_class->cuentaTickets( 'reabiertos' );
            $lista_tickets = $object_class->traerListaTickets( $_GET['pagina'], $_GET['seccion'] );
            switch ($_GET['seccion']) {
                case 'Bandeja Entrada': $total = $numero_tickets['tickets']; break;
                case 'Pendientes': $total = $pendientes_tickets['tickets']; break;
                case 'Reabiertos': $total = $reabiertos_tickets['tickets']; break;
                case 'Cerrados':
                    $aux = $object_class->cuentaTickets( 'cerrados' );
                    $total = $aux['tickets'];
                break;
            }
            $data = [
                "numero_tickets" => $numero_tickets['tickets'],
                "pendientes"     => $pendientes_tickets['tickets'],
                "reabiertos"     => $reabiertos_tickets['tickets'],
                "total_bandeja"  => $total,
                "tickets"        => $lista_tickets
            ];
            echo json_encode( $data );
        break;

        case 'filtro':
            $total = 0;
            $numero_tickets = $object_class->cuentaTickets();
            $pendientes_tickets = $object_class->cuentaTickets( 'pendientes' );
            $reabiertos_tickets = $object_class->cuentaTickets( 'reabiertos' );
            $lista_tickets = $object_class->traerFiltroTickets( $_GET['dato'], $_GET['fecha'] );
            $total = count( $lista_tickets );
            $data = [
                "numero_tickets" => $numero_tickets['tickets'],
                "pendientes"     => $pendientes_tickets['tickets'],
                "reabiertos"     => $reabiertos_tickets['tickets'],
                "total_bandeja"  => $total,
                "tickets"        => $lista_tickets
            ];
            echo json_encode( $data );
        break;

        case 'marcar'://cierra un ticket ingresando las imagenes y el texto correspondiente
            include_once('../src/funciones_globales.php');
            $respuesta = 0;
            $actualizado = 0;
            $imagen = (isset($_FILES['imagen']) && $_FILES['imagen']['tmp_name'] != "" ) ? Funciones::guardarImagenes( 'imagen', "faq_","../../assets/FAQ/") : "" ;
            if( strpos( $imagen, 'default' ) === false ){
                $texto = filter_var( $_POST['textoRespuesta'], FILTER_SANITIZE_STRING );
                $respuesta = $object_class->cerrarTicket( $_POST['ticket'], $texto, $imagen );
                $actualizado = $object_class->actualizarEstado( 0, $_POST['ticket'] );
                $consulta = $object_class->ticketSeleccionado( $_POST['ticket'] );
                $mensaje = "Respuesta ticket #".$_POST['ticket'].":<br>".$texto;
                if(isset($consulta['email']) && stristr($consulta['email'],"@") ){
                    $titulo_mail = "Respuesta ticket #".$_POST['ticket'];
                    $body_mail = $texto;
                    if($imagen != "" && !stristr($imagen, "default")){
                        $body_mail .= "<br><br><label style=''><strong>Captura de pantalla<strong></label><br><img href='https://gmacademy.co/assets/FAQ/$imagen'>";
                    }
                    $correo = $object_class->enviarCorreoTicket($titulo_mail,$consulta['email'],$body_mail);
                }
                if( $respuesta > 0 && $actualizado > 0 ){
                    $respuesta = [
                        "resultado" => "si",
                        "cerrar"    => $respuesta,
                        "estado"    => $actualizado
                    ];
                }else{
                    $respuesta = [
                        "resultado" => "no",
                        "error"    => "No se puede actualizar el estado"
                    ];
                }

            }else{//else guardar imagen
                $respuesta = [
                    "resultado" => "no",
                    "error"    => $imagen
                ];
            }//fin if imagen
            echo( json_encode( $respuesta ) );
        break;

        case 'descarga'://descarga un archivo excel con los tickets abiertos y/o cerrados
            $tipo = "";
            if( $_GET['nuevos'] == "si" && $_GET['cerrados'] == "si" ){
                $tipo = 3;
            }else if( $_GET['nuevos'] == "si" && $_GET['cerrados'] == "no" ){
                $tipo = 1;
            }else if( $_GET['cerrados'] == "si" && $_GET['nuevos'] == "no" ){
                $tipo = 2;
            }
            $tickets = $object_class->descargaTickets( $tipo );
            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=bandeja_tickets.xls");
            ?>
            <table>
                <thead>
                    <tr>
                        <th>Ticket</th>
                        <th>Estado</th>
                        <th>Nombre</th>
                        <th>Concesionario</th>
                        <th>user_id</th>
                        <th>Creación</th>
                        <th>Descripción</th>
                        <th>Tipo</th>
                        <th>Prioridad</th>
                        <th>Imagen</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $tickets as $key => $value ){ ?>
                    <tr>
                        <td><?php echo( $value['ticket_id'] ); ?></td>
                        <td><?php
                            switch( $value['status_id'] ){
                                case 0 : echo("Cerrado"); break;
                                case 1 : echo("Pendiente"); break;
                                case 3 : echo("Reabierto"); break;
                            }
                        ?></td>
                        <td><?php echo( $value['first_name'].' '.$value['last_name'] ); ?></td>
                        <td><?php echo( $value['dealer'] ); ?></td>
                        <td><?php echo( $value['user_id'] ); ?></td>
                        <td><?php echo( $value['date_creation'] ); ?></td>
                        <td><?php echo( $value['description'] ); ?></td>
                        <td><?php echo( $value['key_description'] ); ?></td>
                        <td><?php
                        switch( $value['priority'] ){
                            case 1 : echo("Baja"); break;
                            case 2 : echo("Media"); break;
                            case 3 : echo("Alta"); break;
                        }
                        ?></td>
                        <td><?php echo( $value['image'] ); ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php
        break;

    }
    unset($object_class);
}

?>
