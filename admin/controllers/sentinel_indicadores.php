<?php

include('models/sentinel_indicadores.php');
$datos_Class = new Datos();

if(isset($_POST['ano_id_end'])){
	$year_end = $_POST['ano_id_end'];
}else{
	$year_end = date("Y");
}
$year_act = $year_end-1;

$fec_inicialQ = $year_act.'-01-01';
$fec_inicialQF = $year_act.'-12-31';

$fec_finalQI = $year_end.'-01-01';
$fec_finalQ = $year_end.'-12-31';


$DatosSentinelXConcesionario = $datos_Class->InfoSentinelXConcesionario($fec_inicialQ,$fec_finalQ);
$DatosSentinelXConcesionarioXMes = $datos_Class->InfoSentinelXConcesionarioXMes($fec_inicialQ,$fec_finalQ);

$DatosSentinelXEstado_I = $datos_Class->InfoSentinelXEstado($fec_inicialQ,$fec_inicialQF);
$DatosSentinelXEstado_F = $datos_Class->InfoSentinelXEstado($fec_finalQI,$fec_finalQ);

$DatosSentinelXEstado_IC = $datos_Class->InfoSentinelXSubEstado($fec_inicialQ,$fec_inicialQF);
$DatosSentinelXEstado_FC = $datos_Class->InfoSentinelXSubEstado($fec_finalQI,$fec_finalQ);

$EscaladosCalidad_I = count($datos_Class->InfoEscaladosCalidad($fec_inicialQ,$fec_inicialQF));
$CerradosSinCalidad_I = count($datos_Class->CerradosSinCalidad($fec_inicialQ,$fec_inicialQF));
$Seguimiento_I = count($datos_Class->EnSeguimiento($fec_inicialQ,$fec_inicialQF));

$EscaladosCalidad_F = count($datos_Class->InfoEscaladosCalidad($fec_finalQI,$fec_finalQ));
$CerradosSinCalidad_F = count($datos_Class->CerradosSinCalidad($fec_finalQI,$fec_finalQ));
$Seguimiento_F = count($datos_Class->EnSeguimiento($fec_finalQI,$fec_finalQ));

$SubEstadosEscaladosCalidad_I = $datos_Class->InfoEscaladosCalidadSubEs($fec_inicialQ,$fec_inicialQF);
$SubEstadosEscaladosCalidad_F = $datos_Class->InfoEscaladosCalidadSubEs($fec_finalQI,$fec_finalQ);

$Tipos_I = $datos_Class->TipoVsCalidad($fec_inicialQ,$fec_inicialQF);
$Sistemas_I = $datos_Class->SistemaVsCalidad($fec_inicialQ,$fec_inicialQF);

$Tipos_F = $datos_Class->TipoVsCalidad($fec_finalQI,$fec_finalQ);
$Sistemas_F = $datos_Class->SistemaVsCalidad($fec_finalQI,$fec_finalQ);

$cantidad_concesionarios = count($DatosSentinelXConcesionario);


/*
$CargosDatosListos = $datos_Class->InformacionCargosDatos($fechaCtrl,$_dealer_id);
$CantidadCupos = $datos_Class->GetCantCupos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadIncritos = $datos_Class->GetCantInscritos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadAsistencia = $datos_Class->GetCantAsistencia($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);

$HorasAsistencia = $datos_Class->GetCantHorasAsistencia($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadSesiones = $datos_Class->GetCantSesiones($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);

$HorasAsistenciaXProv = $datos_Class->GetCantHorasAsistenciaProveedor($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadSesionesXProv = $datos_Class->GetCantSesionesProveedor($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);


$GenteSinCursos = $datos_Class->GetGenteNoCursos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$DatosPAC = $datos_Class->GetPAC($year_end,$mes_end,$_dealer_id);
$CantidadRegion = $datos_Class->GetCantRegion($DatosPAC['region']);

$datosConcesionarios = $datos_Class->consultaConcesionarios();*/

unset($datos_Class);