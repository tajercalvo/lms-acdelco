<?php

use FontLib\Table\Type\post;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);	
if (isset($_POST['opcn']) && $_POST['opcn']==='asistencia' ) {
    include_once('../models/calificaciones.php');
    $Calificaciones_Class = new Calificaciones();
    session_start();
    $idQuien = $_SESSION['idUsuario'];
    $Users_select = explode(',', $_POST['ArrIds']);
	$course_id = $_POST['course_id'];
	$schedule_id = $_POST['schedule'];
	$Calificaciones_Class->CerrarCurso($_POST['schedule']); 
	foreach ($Users_select as $val_users) {
		
		if($val_users!=""){
			
			
			if(isset($_POST['Calif'.$val_users])){
				$idUsuario = $val_users;
				$Califica = $_POST['Calif'.$val_users];  
		        $Inv_id = $_POST['Inv'.$val_users];           
				$Asistio = "SI";
				$resultado =1;
		        $resultado=$Calificaciones_Class->CambiarAproval($idUsuario,$Inv_id,$Califica); 
				/* $resultado = $Calificaciones_Class->CambiarAsistencia($idUsuario,$Inv_id); */ 
			}else{
				$idUsuario = $val_users;
				$Asistio = "NO";
				$Inv_id = $_POST['Inv'.$val_users];
				/* $Calificaciones_Class->CambiarEstadoFinalizadoDelschedule($course_id,$schedule_id);  */
			 	$resultado = $Calificaciones_Class->ActualizarScore($idUsuario,$Inv_id);  
			}
		}	
	} 

	if($resultado>0){
		echo('{"resultado":"SI"}');
	}else{
		echo('{"resultado":"NO"}');
	}
	return $resultado;
}
if (isset($_POST['opcn'])) {
    include_once('../models/calificaciones.php');
    $Calificaciones_Class = new Calificaciones();
    session_start();
    $idQuien = $_SESSION['idUsuario'];
    $Users_select = explode(',', $_POST['ArrIds']);
    $course_id = $_POST['course_id'];
	$schedule_id = $_POST['schedule_id'];
	$Calificaciones_Class->CerrarCurso($_POST['schedule']); 
    foreach ($Users_select as $val_users) {
		
		if($val_users!=""){
			if(isset($_POST['Calif'.$val_users])){
				$idUsuario = $val_users;
				$Califica = $_POST['Calif'.$val_users];              
				$Inv_id = $_POST['Inv'.$val_users];           
				$Asistio = "SI";
				$resultado = $Calificaciones_Class->CrearAsistencia($idQuien,$val_users,$course_id,$Inv_id,$Califica,$Asistio);
				
				/*Envia email*/
				$DatosUsuario = $Calificaciones_Class->consultaDatosUsuarioJs($val_users);
				$pos = strpos($DatosUsuario['email'], '@');
				if($pos !== false){
					// include_once('../controllers/email.php');
					// $EmailLudus_Class = new EmailLudus();
					// $para = $DatosUsuario['email'];
					// $titulo = $DatosUsuario['first_name'].", Se ha recibido una asistencia!";
					// $mensaje_cuerpo =  "GMAcademy te invita a realizar la evaluación para este curso 8 días después de la asistencia, por favor recuerda que luego de presentar esta evaluación y si obtienes 80 puntos o más, podrás mejorar tu calificación con 15 puntos extra que otorgan la encuesta de satisfacción y las preguntas de profundización. NOTA: Recuerda que dichos puntos estarán disponibles hasta 48 horas después de presentar la evalaución.!<br><br><a href='https://gmacademy.co/'>GMAcademy</a>";
				    // $result_Email = $EmailLudus_Class->EnviaEmailNotas_New($titulo,$para,$mensaje_cuerpo,'comunicaciones@gmacademy.co');
				}
			}else{
				$idUsuario = $val_users;
				$Califica = "0";
				$Asistio = "NO";
				$Inv_id = $_POST['Inv'.$val_users];
				$resultado = $Calificaciones_Class->CrearCalificacion($idQuien,$val_users,$course_id,$Inv_id,$Califica,$Asistio);
			}
		}
	}
}

if(isset($_POST['schedule_id'])){
	if(file_exists('../models/calificaciones.php')){
		include_once('../models/calificaciones.php');

	}else{
		include_once('models/calificaciones.php');
		
	}
	
	$Calificaciones_Class = new Calificaciones();
	/* print_r($_POST);
	return; */
	$datosInvitaciones = $Calificaciones_Class->consultaDatos($_POST['schedule_id']);
	
	//$cantidad_datos = $Calificaciones_Class->consultaCantidad($_POST['schedule_id']);
	$cantidad_datos = count($datosInvitaciones);
	
	$dato_sesion = $Calificaciones_Class->consultaSesion($_POST['schedule_id']);
	
	
	//Si es OJT para la calificación con invitación y todo
		if($dato_sesion['type']=='OJT'){
			$UsuariosSitios = $Calificaciones_Class->consultaUsuariosIns($_POST['schedule_id']);
		}
	//Si es OJT para la calificación con invitación y todo
	unset($Calificaciones_Class);
}else if(isset($_POST['opcn']) && ($_POST['opcn']=="sesiones")){
	include_once('../models/calificaciones.php');
	$Calificaciones_Class = new Calificaciones();
	$fec_ini = $_POST['fec_ini'];
	$fec_fin = $_POST['fec_fin'];
	$datosSesiones = $Calificaciones_Class->consultaSesiones($fec_ini,$fec_fin);
	foreach ($datosSesiones as $iID => $data) {
		echo("<option value='".$data['schedule_id']."'>".$data['newcode'].' | '.$data['course']." ".$data['start_date']." ".$data['living']."</option>");
	}
	unset($Calificaciones_Class);
}else if(isset($_GET['opcn']) and $_GET['opcn'] == "descargarLista"){
$ruta = '../../../';
	include_once($ruta.'models/calificaciones.php');
	$Calificaciones_Class = new Calificaciones();

	$dato_sesion = $Calificaciones_Class->descargarConsultaSesion($_GET['schedule_id']);
	$datosInvitaciones = $Calificaciones_Class->descargarConsultaDatos($_GET['schedule_id']);

	unset($Calificaciones_Class);
}else if(isset($_GET['schedule_id'])){
	$_POST['fecha_inicial'] = $_GET['fecha_inicial'];
	$_POST['fecha_final'] = $_GET['fecha_final'];
	$_POST['schedule_id'] = $_GET['schedule_id'];
	include_once('models/calificaciones.php');
	$Calificaciones_Class = new Calificaciones();
	//Realiza la creación de las calificaciones por OJT
		if(isset($_POST['Schedule_idOJT'])){
			$p = $_POST;
			// $CalificacionOJT_Res = Calificaciones::CalificaOJT($_POST['Schedule_idOJT'],$_POST['userOJT_id'],$_POST['ObservOJT'],$_POST['CalifOJT'],$_POST['course_idOJT']);
			$CalificacionOJT_Res = Calificaciones::CalificaOJT( $p, '' );
			/*Envia email*/
			$DatosUsuario = $Calificaciones_Class->consultaDatosUsuario($_POST['userOJT_id']);
			$pos = strpos($DatosUsuario['email'], '@');
			if($pos !== false){
				include_once('email.php');
				// $EmailLudus_Class = new EmailLudus();
				// $para = $DatosUsuario['email'];
				// $titulo = $DatosUsuario['first_name'].", Se ha recibido una asistencia!";
				// $mensaje_cuerpo =  "GMAcademy te invita a realizar la evaluación para este curso 8 días después de la asistencia, por favor recuerda que luego de presentar esta evaluación y si obtienes 80 puntos o más, podrás mejorar tu calificación con 15 puntos extra que otorgan la encuesta de satisfacción y las preguntas de profundización. NOTA: Recuerda que dichos puntos estarán disponibles hasta 48 horas después de presentar la evalaución.!<br><br><a href='https://gmacademy.co/'>GMAcademy</a>";
			    //$result_Email = $EmailLudus_Class->EnviaEmailNotas_New($titulo,$para,$mensaje_cuerpo,'comunicaciones@gmacademy.co');
			}
		}
	//Realiza la creación de las calificaciones por OJT
	$datosInvitaciones = $Calificaciones_Class->consultaDatos($_GET['schedule_id']);
	//$cantidad_datos = $Calificaciones_Class->consultaCantidad($_GET['schedule_id']);
	$cantidad_datos = count($datosInvitaciones);
	$dato_sesion = $Calificaciones_Class->consultaSesion($_GET['schedule_id']);
	//Si es OJT para la calificación con invitación y todo
		if($dato_sesion['type']=='OJT'){
			$UsuariosSitios = $Calificaciones_Class->consultaUsuariosIns($_GET['schedule_id']);
		}
	//Si es OJT para la calificación con invitación y todo
	unset($Calificaciones_Class);
}else if(isset($_POST['ArrIds'])){
	$_POST['schedule_id']=$_POST['schedule'];
	include_once('../models/calificaciones.php');
	$Calificaciones_Class = new Calificaciones();
	session_start();
	$idQuien = $_SESSION['idUsuario'];
	$Users_select = explode(',', $_POST['ArrIds']);
	$course_id = $_POST['course_id'];
	$schedule_id = $_POST['schedule_id'];
	$Calificaciones_Class->CerrarCurso($_POST['schedule']); 
    foreach ($Users_select as $val_users) {
		if($val_users!=""){
			if(isset($_POST['Calif'.$val_users])){
				$idUsuario = $val_users;
				$Califica = $_POST['Calif'.$val_users];              
				$Inv_id = $_POST['Inv'.$val_users];           
				$Asistio = "SI";
				$resultado = $Calificaciones_Class->CrearCalificacion($idQuien,$val_users,$course_id,$Inv_id,$Califica,$Asistio);
				
				/*Envia email*/
				$DatosUsuario = $Calificaciones_Class->consultaDatosUsuarioJs($val_users);
				$pos = strpos($DatosUsuario['email'], '@');
				//if($pos !== false){
					// include_once('../controllers/email.php');
					// $EmailLudus_Class = new EmailLudus();
					// $para = $DatosUsuario['email'];
					// $titulo = $DatosUsuario['first_name'].", Se ha recibido una asistencia!";
					// $mensaje_cuerpo =  "GMAcademy te invita a realizar la evaluación para este curso 8 días después de la asistencia, por favor recuerda que luego de presentar esta evaluación y si obtienes 80 puntos o más, podrás mejorar tu calificación con 15 puntos extra que otorgan la encuesta de satisfacción y las preguntas de profundización. NOTA: Recuerda que dichos puntos estarán disponibles hasta 48 horas después de presentar la evalaución.!<br><br><a href='https://gmacademy.co/'>GMAcademy</a>";
				    // $result_Email = $EmailLudus_Class->EnviaEmailNotas_New($titulo,$para,$mensaje_cuerpo,'comunicaciones@gmacademy.co');
				//}
			}else{
				$idUsuario = $val_users;
				$Califica = "0";
				$Asistio = "NO";
				$Inv_id = $_POST['Inv'.$val_users];
				$resultado = $Calificaciones_Class->CrearCalificacion($idQuien,$val_users,$course_id,$Inv_id,$Califica,$Asistio);
			}
		}
	}
	unset($Calificaciones_Class);
	if($resultado>0){
		echo('{"resultado":"SI"}');
	}else{
		echo('{"resultado":"NO"}');
	}
}else{
	$cantidad_datos = 0;
}
