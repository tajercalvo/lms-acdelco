<?php

include('models/dashboard.php');
$datos_Class = new Datos();
if(isset($_POST['ano_id_end'])){
	$year_end = $_POST['ano_id_end'];
}else{
	$year_end = date("Y");
}
if(isset($_POST['mes_id_end'])){
	$mes_end = $_POST['mes_id_end'];
}else{
	$mes_end = date("m");
}

if(isset($_POST['dealer_id'])){
	$_dealer_id = $_POST['dealer_id'];
}else if(isset($_POST['dealer_id_opc'])){
	$_POST['dealer_id'] = $_POST['dealer_id_opc'];
	$_dealer_id = $_POST['dealer_id'];
}else{
	@session_start();
	if(isset($_SESSION['dealer_id'])){
		$_dealer_id = $_SESSION['dealer_id'];
	}else{
		$_dealer_id = 0;
	}
	
}

$mesCtr_end = $mes_end;
$mesCtr_end = $mesCtr_end+1;
$fechaCtrl = $year_end.'-'.$mesCtr_end.'-01 00:00:00';
$fechaCtrl_Ini = $year_end.'-'.$mes_end.'-01 00:00:00';
$dealer_active = $datos_Class->InformacionDealers();
$CargosDatosListos = $datos_Class->InformacionCargosDatos($fechaCtrl,$_dealer_id);
$CantidadCupos = $datos_Class->GetCantCupos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadIncritos = $datos_Class->GetCantInscritos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadAsistencia = $datos_Class->GetCantAsistencia($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);

$HorasAsistencia = $datos_Class->GetCantHorasAsistencia($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadSesiones = $datos_Class->GetCantSesiones($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);

$HorasAsistenciaXProv = $datos_Class->GetCantHorasAsistenciaProveedor($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadSesionesXProv = $datos_Class->GetCantSesionesProveedor($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadVoluntariosLibres = $datos_Class->GetCantVoluntarios($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$CantidadTotalGente = $datos_Class->GetCantTotalPeople($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$ListadosPAC = $datos_Class->GetPAC_Evolutivo($_dealer_id);


$GenteSinCursos = $datos_Class->GetGenteNoCursos($fechaCtrl_Ini,$fechaCtrl,$_dealer_id);
$DatosPAC = $datos_Class->GetPAC($year_end,$mes_end,$_dealer_id);
$CantidadRegion = $datos_Class->GetCantRegion($DatosPAC['region']);

$datosConcesionarios = $datos_Class->consultaConcesionarios();
/*
	if(isset($_POST['opcn'])){
		if($_POST['opcn']=="ConsultaAvanceCargo"){
			$Cantidad_TOM = 0;
			$Cantidad_TRA = 0;
			include('../models/dashboard.php');
			$datos_Class = new Datos();
			$out = array();
			$output_tom = array(
		        'label' => 'CURSOS TOMAR',
		        'data' => array()
		    );
		    $output_tra = array(
		        'label' => 'CURSOS TRAYECTORIA',
		        'data' => array()
		    );
			$idCharge = $_POST['Charge_id'];
			$ArrayMes = explode(",",$_POST['ArrayMes']);
	        for($i = 0; $i < count($ArrayMes); $i++){
	        	$fec_1 = $ArrayMes[$i]."-01";
				$fec_2 = strtotime ('+1 month' , strtotime ($fec_1));
				$fec_2 = date('Y-m-d' , $fec_2);
				$Cantidad_TOM = $Cantidad_TOM+$datos_Class->Cantidad_GeneralXCargo($idCharge,$fec_1,$fec_2);
				$Cantidad_TRA = $Cantidad_TRA+$datos_Class->Cantidad_DetalladaXCargo($idCharge,$fec_1,$fec_2);
				$item_aro = array(
					$ArrayMes[$i], $Cantidad_TOM
				);
				$output_tom['data'][] = $item_aro;
				$item_ary = array(
					$ArrayMes[$i], $Cantidad_TRA
				);
				$output_tra['data'][] = $item_ary;
			}
			$out[] = $output_tom;
			$out[] = $output_tra;
			unset($datos_Class);
			echo json_encode($out);
		}
	}else{
		include('models/dashboard.php');
		if(!isset($_POST['ano_id_start'])){
			$newdate_s = strtotime ('-3 month' , strtotime (date("Y-m-d")));
			$mes_start = date('m' , $newdate_s);
			$year_start = date('Y' , $newdate_s);
			$newdate_e = strtotime ('+2 month' , strtotime (date("Y-m-d")));
			$mes_end = date('m' , $newdate_e);
			$mes_end = $mes_end;
			$year_end = date('Y' , $newdate_e);
		}else{
			$mes_start = $_POST['mes_id_start'];
			$year_start = $_POST['ano_id_start'];
			$mes_end = $_POST['mes_id_end'];
			$year_end = $_POST['ano_id_end'];
		}
		$newdate_c = strtotime ('-1 day' , strtotime ($year_end."-".($mes_end+1)."-01"));
		$fecha_inicial = $year_start."-".$mes_start."-01";
		$fecha_final = date('Y-m-d' , $newdate_c );
		$date1=date_create($fecha_inicial);
		$date2=date_create($fecha_final);
		$interval=$date2->diff($date1);
		$intervalMeses=$interval->format("%m");
		$intervalAno=$interval->format("%y");
		$anos_dif = $intervalAno*12;
		$diferencia_mes = ($anos_dif+$intervalMeses+1);
		$array_meses = "0";
		$fechaMostrar = $fecha_inicial;
		while(strtotime($fechaMostrar) <= strtotime($fecha_final)) {
			$array_meses .= ",".date("Y-m", strtotime($fechaMostrar));
			$fechaMostrar = date("Y-m", strtotime($fechaMostrar . " + 1 month"));
		}
		$array_meses = str_replace("0,", "", $array_meses);
		$datos_Class = new Datos();
		$cantidadCursos = $datos_Class->CantidadCourses();
		$cantidadUsuarios = $datos_Class->CantidadUsers();
		$cantidadProgramados = $datos_Class->CantidadProgramados();
		$cantidadModAprobados = $datos_Class->CantidadModAprobados();
		$cantidadCargos = $datos_Class->CantidadCargos();
		$cantidadCargosTrayectoria = $datos_Class->CantidadCargosTrayectoria();

		$cantidadConcesionarios = $datos_Class->CantidadConcesionarios();
		$cantidadConcesionariosFormacion = $datos_Class->CantidadConcesionariosFormacion();
		$cantidadUsuariosFormacion = $datos_Class->CantidadUsuariosFormacion();

		$cargos_Trayectorias = $datos_Class->CargosTrayectorias();

		unset($datos_Class);
	}
*/