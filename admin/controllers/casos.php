<?php
if(isset($_GET['opcn'])){
	include_once('models/casos.php');
	$Casos_Class = new Casos();
	if(isset($_GET['id'])){
		$registroConfiguracion = $Casos_Class->consultaRegistro($_GET['id']);
		if(isset($_POST['opcn_g'])){
			if($_POST['opcn_g']=="editarGaleria"){
				/*include_once('models/casos.php');
				$Casos_Class = new Casos();*/
				if(isset($_FILES['image'])){
					if(isset($_FILES['image']['tmp_name'])){
						$nom_archivo1 = $_FILES["image"]["name"];
						$new_name1 = "SEN_GAL_";
						$new_name1 .= date("Ymdhis");
						$new_extension1 = "jpg";
						preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG|ZIP|zip|PDF|pdf)$'i", $nom_archivo1, $ext1);
						switch (strtolower($ext1[2])) {
							case 'jpeg' : $new_extension1 = ".jpg";
								break;
							case 'jpg' : $new_extension1 = ".jpg";
								break;
							case 'JPG' : $new_extension1 = ".jpg";
								break;
							case 'JPEG' : $new_extension1 = ".jpg";
								break;
							case 'ZIP' : $new_extension1 = ".zip";
								break;
							case 'zip' : $new_extension1 = ".zip";
								break;
							case 'PDF' : $new_extension1 = ".pdf";
								break;
							case 'pdf' : $new_extension1 = ".pdf";
								break;
							default    : $new_extension1 = "no";
								break;
						}
						if($new_extension1 != "no"){
							$new_name1 .= $new_extension1;
							$resultArchivo1 = copy($_FILES["image"]["tmp_name"], "../assets/images/sentinel/".$new_name1);
							if($resultArchivo1==1){
								$resultado = $Casos_Class->CrearArchivo($_POST['idElemento'],$new_name1,str_replace('.', '', $new_extension1));
							}
						}
					}
				}
			}else if($_POST['opcn_g']=="Gestionar"){
				$archivoName = '';
				if(isset($_FILES['archivo'])){
					if(isset($_FILES['archivo']['tmp_name'])){
						$nom_archivo1 = $_FILES["archivo"]["name"];
						$new_name1 = "SEN_FILE_";
						$new_name1 .= date("Ymdhis");
						$new_extension1 = "jpg";
						preg_match("'^(.*)\.(ZIP|zip|PDF|pdf)$'i", $nom_archivo1, $ext1);
						switch (strtolower($ext1[2])) {
							case 'ZIP' : $new_extension1 = ".zip";
								break;
							case 'zip' : $new_extension1 = ".zip";
								break;
							case 'PDF' : $new_extension1 = ".pdf";
								break;
							case 'pdf' : $new_extension1 = ".pdf";
								break;
							default    : $new_extension1 = "no";
								break;
						}
						if($new_extension1 != "no"){
							$new_name1 .= $new_extension1;
							$resultArchivo1 = copy($_FILES["archivo"]["tmp_name"], "../assets/images/sentinel/".$new_name1);
							if($resultArchivo1==1){
								$archivoName = $new_name1;
							}
						}
					}
				}
					$_Var_date_request = "";
					$_Var_date_delivered = "";
					$_Var_date_scrap = "";
					if(isset($_POST['date_request'])){
						$_Var_date_request = $_POST['date_request'];
					}
					if(isset($_POST['date_delivered'])){
						$_Var_date_delivered = $_POST['date_delivered'];
					}
					if(isset($_POST['date_scrap'])){
						$_Var_date_scrap = $_POST['date_scrap'];
					}
					$_varGestion = "Sin gestión indicada";
					if(isset($_POST['gestion']) && $_POST['gestion'] !=""){
						$_varGestion = $_POST['gestion'];
					}
					$subEstado = 0;

					if(isset($_POST['substatus_id'])){
						$subEstado = $_POST['substatus_id'];
					}else{
						if($_POST['status_id_new']=="7"){
							$subEstado = 2;
						}
					}

					if(isset($_POST['substatus_id'])){
						if($_POST['substatus_id'] == "5"){
							$_POST['status_id_new'] = "8";
							$Emails = $Casos_Class->GetEmailAdminSentinel('np');
							$DataDetail = $Casos_Class->GetDetailsCompleteSentinel('np',$_POST['idElemento']);
							$Emails_listos = "";
							foreach ($Emails as $key => $value) {
								$Emails_listos .= $value['email'].",";
							}
							$para = $Emails_listos;
							$Emails_listos .= "diegolamprea@gmail.com, luisfernando.carlos@gm.com";
							$titulo = "LUDUS: Un caso ha sido DEVUELTO A SERVICIO! ";
							$mensaje_cuerpo = "Este correo se envia para informar a las personas de servicio cuando Calidad devuelve un caso para nuevo análisis:<br><br>";
							$mensaje_cuerpo .= "<strong>ID SENTINEL:</strong>".$DataDetail['sentinel_case_id']."<br>";
							$mensaje_cuerpo .= "<strong>MODELO:</strong>".$DataDetail['sentinel_model']."<br>";
							$mensaje_cuerpo .= "<strong>QUEJA DEL CLIENTE:</strong>".$DataDetail['customer_complaint']."<br>";
							$mensaje_cuerpo .= "<strong>FECHA GARANTÍA:</strong>".$DataDetail['garranty_start']."<br>";
							$mensaje_cuerpo .= "<strong>DIAGNÓSTICO:</strong>".$DataDetail['diagnostic']."<br>";
							$mensaje_cuerpo .= "<strong>CAUSA:</strong>".$DataDetail['cause_failure']."<br><br>";
							$mensaje_cuerpo .= "<strong>VIN: </strong> ".$registroConfiguracion['vin']."<br>
										<strong>Fecha Sentinel: </strong> ".$registroConfiguracion['date_sentinel']."<br>
										<strong>Quien Reporta: </strong> ".$_SESSION['NameUsuario']."<br>
										<strong>Concesionario: </strong> ".$_SESSION['dealer']."<br>";
							include_once('email.php');
							$EmailLudus_Class = new EmailLudus();
							$result_Email = $EmailLudus_Class->EnviaEmailDet_New($titulo,$para,$mensaje_cuerpo,'sentinel@gmacademy.co');
							echo(":::: Devuelto: ".$result_Email.":::::");
							unset($EmailLudus_Class);
						}else if($_POST['substatus_id'] == "3" || $_POST['substatus_id'] == "4"){
							$Emails = $Casos_Class->GetEmailAdminSentinel('np');
							$DataDetail = $Casos_Class->GetDetailsCompleteSentinel('np',$_POST['idElemento']);
							$Emails_listos = "";
							foreach ($Emails as $key => $value) {
								$Emails_listos .= $value['email'].",";
							}
							$para = $Emails_listos;
							$Emails_listos .= "diegolamprea@gmail.com, luisfernando.carlos@gm.com";
							$titulo = "LUDUS: Un caso ha sido CERRADO POR CALIDAD! ";
							$mensaje_cuerpo = "Este correo se envia para informar a las personas de servicio cuando Calidad cierra un caso elevado:<br><br>";
							$mensaje_cuerpo .= "<strong>ID SENTINEL:</strong>".$DataDetail['sentinel_case_id']."<br>";
							$mensaje_cuerpo .= "<strong>MODELO:</strong>".$DataDetail['sentinel_model']."<br>";
							$mensaje_cuerpo .= "<strong>QUEJA DEL CLIENTE:</strong>".$DataDetail['customer_complaint']."<br>";
							$mensaje_cuerpo .= "<strong>FECHA GARANTÍA:</strong>".$DataDetail['garranty_start']."<br>";
							$mensaje_cuerpo .= "<strong>DIAGNÓSTICO:</strong>".$DataDetail['diagnostic']."<br>";
							$mensaje_cuerpo .= "<strong>CAUSA:</strong>".$DataDetail['cause_failure']."<br><br>";
							$mensaje_cuerpo .= "<strong>VIN: </strong> ".$registroConfiguracion['vin']."<br>
										<strong>Fecha Sentinel: </strong> ".$registroConfiguracion['date_sentinel']."<br>
										<strong>Quien Reporta: </strong> ".$_SESSION['NameUsuario']."<br>
										<strong>Concesionario: </strong> ".$_SESSION['dealer']."<br>";
							include_once('email.php');
							$EmailLudus_Class = new EmailLudus();
							$result_Email = $EmailLudus_Class->EnviaEmailDet_New($titulo,$para,$mensaje_cuerpo,'sentinel@gmacademy.co');
							echo(":::: Cerrado: ".$result_Email.":::::");
							unset($EmailLudus_Class);
						}
					}
					$resultado = $Casos_Class->CrearGestion($_POST['idElemento'],$_varGestion,$_POST['status_act'],$_POST['status_id_new'],$_Var_date_request,$_Var_date_delivered,$_Var_date_scrap,$subEstado,$archivoName);
					if($_POST['status_id_new']=="7"){

						if($subEstado == 2){

							$Emails = $Casos_Class->GetEmailCalidadSentinel('np',$_POST['idElemento']);
							$DataDetail = $Casos_Class->GetDetailsCompleteSentinel('np',$_POST['idElemento']);
							$Emails_listos = "";
							foreach ($Emails as $key => $value) {
								$Emails_listos .= $value['email'].",";
							}
							$Emails_listos .= "fabian.olarte@gm.com,diegolamprea@gmail.com, luisfernando.carlos@gm.com";
							$para = $Emails_listos;
							$titulo = "LUDUS: Le ha sido escalado un Sentinel";
							$mensaje_cuerpo = "Este correo se envia para informar a las personas involucradas en Calidad de casos Sentinel:<br><br>";
							$mensaje_cuerpo .= "<strong>ID SENTINEL:</strong>".$DataDetail['sentinel_case_id']."<br>";
							$mensaje_cuerpo .= "<strong>MODELO:</strong>".$DataDetail['sentinel_model']."<br>";
							$mensaje_cuerpo .= "<strong>AÑO:</strong>".$DataDetail['model_year']."<br>";
							$mensaje_cuerpo .= "<strong>QUEJA DEL CLIENTE:</strong>".$DataDetail['customer_complaint']."<br>";
							$mensaje_cuerpo .= "<strong>FECHA GARANTÍA:</strong>".$DataDetail['garranty_start']."<br>";
							$mensaje_cuerpo .= "<strong>DIAGNÓSTICO:</strong>".$DataDetail['diagnostic']."<br>";
							$mensaje_cuerpo .= "<strong>CAUSA:</strong>".$DataDetail['cause_failure']."<br><br>";
							$mensaje_cuerpo .= "<strong>VIN: </strong> ".$registroConfiguracion['vin']."<br>
										<strong>Fecha Sentinel: </strong> ".$registroConfiguracion['date_sentinel']."<br>
										<strong>Quien Reporta: </strong> ".$_SESSION['NameUsuario']."<br>
										<strong>Concesionario: </strong> ".$_SESSION['dealer']."<br>";
							include_once('email.php');
							$EmailLudus_Class = new EmailLudus();
							$result_Email = $EmailLudus_Class->EnviaEmailDet_New($titulo,$para,$mensaje_cuerpo,'sentinel@gmacademy.co');

							unset($EmailLudus_Class);
						}
					}
					//Crea registros Vin OT
						if(isset($_POST['vin_qa_1']) && $_POST['vin_qa_1']!=""){
							$resul_vinOT = $Casos_Class->CrearVinQA($resultado,$_POST['vin_qa_1'],$_POST['date_qa_1'],$_POST['action_qa_1']);
						}
						if(isset($_POST['vin_qa_2']) && $_POST['vin_qa_2']!=""){
							$resul_vinOT = $Casos_Class->CrearVinQA($resultado,$_POST['vin_qa_2'],$_POST['date_qa_2'],$_POST['action_qa_2']);
						}
						if(isset($_POST['vin_qa_3']) && $_POST['vin_qa_3']!=""){
							$resul_vinOT = $Casos_Class->CrearVinQA($resultado,$_POST['vin_qa_3'],$_POST['date_qa_3'],$_POST['action_qa_3']);
						}
			}else if($_POST['opcn_g']=="AddVines"){
				$idSentinel = $_POST['idElemento'];
				if(isset($_POST['vin_1']) && $_POST['vin_1']!=""){
					$resul_vinOT = $Casos_Class->CrearVinOT_O($idSentinel,$_POST['vin_1'],$_POST['km_1'],$_POST['date_ot_1'],$_POST['comments_1']);
				}
				if(isset($_POST['vin_2']) && $_POST['vin_2']!=""){
					$resul_vinOT = $Casos_Class->CrearVinOT_O($idSentinel,$_POST['vin_2'],$_POST['km_2'],$_POST['date_ot_2'],$_POST['comments_2']);
				}
				if(isset($_POST['vin_3']) && $_POST['vin_3']!=""){
					$resul_vinOT = $Casos_Class->CrearVinOT_O($idSentinel,$_POST['vin_3'],$_POST['km_3'],$_POST['date_ot_3'],$_POST['comments_3']);
				}
			}
		}
		if(isset($_GET['opcn_e'])){
			if($_GET['opcn_e']=="eliminar"){
				$idE = $_GET['idE'];
				$source = $_GET['source'];
				if(file_exists("../../assets/sentinel/".$source)){
					unlink("../../assets/sentinel/".$source);
				}
				$resultado = $Casos_Class->BorrarArchivo($idE);
			}
		}
		//consultas individuales

		$fotografias = $Casos_Class->ArchivosCargados($_GET['id']);
		$gestiones = $Casos_Class->GestionesCargadas($_GET['id']);
		$VinOT = $Casos_Class->ConsultaVinOT($_GET['id']);
	}else{
		$registroConfiguracion['nombre_lista'] = "";
	}
	$listadoModelos = $Casos_Class->consultaBiblioteca('model','status');
	$listadoAplicaciones = $Casos_Class->consultaBiblioteca('application','');
	$listadoServicios = $Casos_Class->consultaBiblioteca('service','');
	$listadoSystems = $Casos_Class->consultaBiblioteca('system','');
	$listadoCheks = $Casos_Class->consultaBiblioteca('check','');
	$listadoLubricantes = $Casos_Class->consultaBiblioteca('lubricant','');
	$listadoEstados = $Casos_Class->consultaEstados();
	$listadoSubEstados = $Casos_Class->consultaSubEstados();
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	if($_POST['opcn']=="crear"){
		include_once('../models/casos.php');
		$Casos_Class = new Casos();
		$photos = '';
		$video = '';
		$history = '';
		$use_parts = '';
		if(isset($_POST['photos'])){
			$photos = $_POST['photos'];
		}
		if(isset($_POST['video'])){
			$video = $_POST['video'];
		}
		if(isset($_POST['history'])){
			$history = $_POST['history'];
		}
		if(isset($_POST['use_parts'])){
			$use_parts = $_POST['use_parts'];
		}
		$resultado = $Casos_Class->CrearCasos($_POST['date_sentinel'],$_POST['nro_ot'],$_POST['date_ot'],$photos,$video,$history,$_POST['sentinel_model_id'],$_POST['vin'],$_POST['sentinel_application_id'],$_POST['km'],$_POST['garranty_start'],$_POST['sentinel_service_id'],$_POST['transmission'],$_POST['configuration'],$_POST['sentinel_system_id'],$_POST['nserie'],$_POST['sentinel_check_id'],$_POST['fail_code'],$_POST['customer_complaint'],$_POST['diagnostic'],$_POST['cause_failure'],$_POST['correction'],$use_parts,$_POST['parts_description'],$_POST['km_mot'],$_POST['cost_workforce'],$_POST['cost_parts'],$_POST['cost_tot'],$_POST['total'],$_POST['model_year'],$_POST['car_version'],$_POST['cat_id']);
		if($resultado>0){
			$Emails = $Casos_Class->GetEmailAdminSentinel('js');
			$DataDetail = $Casos_Class->GetDetailsCompleteSentinel('js',$resultado);
			$Emails_listos = "";
			foreach ($Emails as $key => $value) {
				$Emails_listos .= $value['email'].",";
			}
			$para = $Emails_listos;
			$titulo = "LUDUS: Ha ingresado un nuevo Sentinel";
			$mensaje_cuerpo = "Este correo se envia para informar a las personas involucradas en la administración de casos Sentinel:<br><br>";
							$mensaje_cuerpo .= "<strong>ID SENTINEL:</strong>".$DataDetail['sentinel_case_id']."<br>";
							$mensaje_cuerpo .= "<strong>MODELO:</strong>".$DataDetail['sentinel_model']."<br>";
							$mensaje_cuerpo .= "<strong>QUEJA DEL CLIENTE:</strong>".$DataDetail['customer_complaint']."<br>";
							$mensaje_cuerpo .= "<strong>FECHA GARANTÍA:</strong>".$DataDetail['garranty_start']."<br>";
							$mensaje_cuerpo .= "<strong>DIAGNÓSTICO:</strong>".$DataDetail['diagnostic']."<br>";
							$mensaje_cuerpo .= "<strong>CAUSA:</strong>".$DataDetail['cause_failure']."<br><br>";
			$mensaje_cuerpo .= "<strong>VIN: </strong> ".$_POST['vin']."<br>
                            <strong>Fecha Sentinel: </strong> ".$_POST['date_sentinel']."<br>
                            <strong>Quien Reporta: </strong> ".$_SESSION['NameUsuario']."<br>
                            <strong>Concesionario: </strong> ".$_SESSION['dealer']."<br>";
            include_once('../controllers/email.php');
            $EmailLudus_Class = new EmailLudus();
            $result_Email = $EmailLudus_Class->EnviaEmailDet_New($titulo,$para,$mensaje_cuerpo,'sentinel@gmacademy.co');
            unset($EmailLudus_Class);
			//Crea registros Vin OT
			if(isset($_POST['vin_1']) && $_POST['vin_1']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_1'],$_POST['km_1'],$_POST['date_ot_1'],$_POST['comments_1']);
			}
			if(isset($_POST['vin_2']) && $_POST['vin_2']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_2'],$_POST['km_2'],$_POST['date_ot_2'],$_POST['comments_2']);
			}
			if(isset($_POST['vin_3']) && $_POST['vin_3']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_3'],$_POST['km_3'],$_POST['date_ot_3'],$_POST['comments_3']);
			}
			if(isset($_POST['vin_4']) && $_POST['vin_4']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_4'],$_POST['km_4'],$_POST['date_ot_4'],$_POST['comments_4']);
			}
			if(isset($_POST['vin_5']) && $_POST['vin_5']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_5'],$_POST['km_5'],$_POST['date_ot_5'],$_POST['comments_5']);
			}
			//Crea registros Vin OT
			echo('{"resultado":"SI","idRegistro":"'.$resultado.'"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
		include_once('../models/casos.php');
		$Casos_Class = new Casos();
		$photos = '';
		$video = '';
		$history = '';
		$use_parts = '';
		$ok_first = '';
		$solution_contribute = '';

		if(isset($_POST['photos'])){
			$photos = $_POST['photos'];
		}
		if(isset($_POST['video'])){
			$video = $_POST['video'];
		}
		if(isset($_POST['history'])){
			$history = $_POST['history'];
		}
		if(isset($_POST['use_parts'])){
			$use_parts = $_POST['use_parts'];
		}
		if(isset($_POST['ok_first'])){
			$ok_first = $_POST['ok_first'];
		}
		if(isset($_POST['solution_contribute'])){
			$solution_contribute = $_POST['solution_contribute'];
		}
		$enviaremail = $Casos_Class->consultaFechaParte($_POST['idElemento'], $_POST['date_start_cdg']);
		$resultado = $Casos_Class->ActualizarCasos($_POST['date_sentinel'],$_POST['nro_ot'],$_POST['date_ot'],$photos,$video,$history,$_POST['sentinel_model_id'],$_POST['vin'],$_POST['sentinel_application_id'],$_POST['km'],$_POST['garranty_start'],$_POST['sentinel_service_id'],$_POST['transmission'],$_POST['configuration'],$_POST['sentinel_system_id'],$_POST['nserie'],$_POST['sentinel_check_id'],$_POST['fail_code'],$_POST['customer_complaint'],$_POST['diagnostic'],$_POST['cause_failure'],$_POST['correction'],$use_parts,$_POST['parts_description'],$_POST['km_mot'],$_POST['cost_workforce'],$_POST['cost_parts'],$_POST['cost_tot'],$_POST['total'],$_POST['idElemento'],$_POST['status_id'],$_POST['model_year'],$_POST['car_version'],$_POST['date_requ_cdg'],$_POST['date_start_cdg'],$ok_first,$solution_contribute,$_POST['parts_cdg'], $_POST['date_analysis_cdg'], $_POST['date_retirement_cdg'], $_POST['date_destruction_cdg'],$_POST['cat_id']);

			echo('{"resultado":"SI","ColumnasModificadas":"'.$resultado.'","EnviarEmail":"'.$enviaremail.'" }');
			if ($enviaremail == 'SI') {

						// $Emails = $Casos_Class->GetEmailAdminSentinel('js');
						$DataDetail = $Casos_Class->GetDetailsCompleteSentinel('js',$_POST['idElemento']);
						$Emails_listos = "fabian.olarte@gm.com, gerlen.manios@gm.com";
						// foreach ($Emails as $key => $value) {
						// 	$Emails_listos .= $value['email'].",";
						// }
						$para = $Emails_listos;
						$titulo = "LUDUS: El CDG actualizó la llegada de una nueva parte";
						$mensaje_cuerpo = "Este correo se envia para informar a las personas involucradas en la administración de casos Sentinel:<br><br>";
										$mensaje_cuerpo .= "<strong>ID SENTINEL:</strong>".$DataDetail['sentinel_case_id']."<br>";
										$mensaje_cuerpo .= "<strong>MODELO:</strong>".$DataDetail['sentinel_model']."<br>";
										$mensaje_cuerpo .= "<strong>QUEJA DEL CLIENTE:</strong>".$DataDetail['customer_complaint']."<br>";
										$mensaje_cuerpo .= "<strong>FECHA GARANTÍA:</strong>".$DataDetail['garranty_start']."<br>";
										$mensaje_cuerpo .= "<strong>DIAGNÓSTICO:</strong>".$DataDetail['diagnostic']."<br>";
										$mensaje_cuerpo .= "<strong>CAUSA:</strong>".$DataDetail['cause_failure']."<br><br>";
						$mensaje_cuerpo .= "<strong>VIN: </strong> ".$_POST['vin']."<br>
			                            <strong>Fecha Sentinel: </strong> ".$_POST['date_sentinel']."<br>
			                            <strong>Quien Reporta: </strong> ".$_SESSION['NameUsuario']."<br>
			                            <strong>Concesionario: </strong> ".$_SESSION['dealer']."<br>";
			            include_once('../controllers/email.php');
			            $EmailLudus_Class = new EmailLudus();
			            $result_Email = $EmailLudus_Class->EnviaEmailDet_New($titulo,$para,$mensaje_cuerpo,'sentinel@gmacademy.co');
			            unset($EmailLudus_Class);

			}
			//Crea registros Vin OT
			$res_BorraOT = $Casos_Class->BorraOT_Ant($_POST['idElemento']);
			if(isset($_POST['vin_1']) && $_POST['vin_1']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_1'],$_POST['km_1'],$_POST['date_ot_1'],$_POST['comments_1']);
			}
			if(isset($_POST['vin_2']) && $_POST['vin_2']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_2'],$_POST['km_2'],$_POST['date_ot_2'],$_POST['comments_2']);
			}
			if(isset($_POST['vin_3']) && $_POST['vin_3']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_3'],$_POST['km_3'],$_POST['date_ot_3'],$_POST['comments_3']);
			}
			if(isset($_POST['vin_4']) && $_POST['vin_4']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_4'],$_POST['km_4'],$_POST['date_ot_4'],$_POST['comments_4']);
			}
			if(isset($_POST['vin_5']) && $_POST['vin_5']!=""){
				$resul_vinOT = $Casos_Class->CrearVinOT($resultado,$_POST['vin_5'],$_POST['km_5'],$_POST['date_ot_5'],$_POST['comments_5']);
			}
			//Crea registros Vin OT
	}elseif($_POST['opcn']=="CrearImagen"){
		include_once('../models/casos.php');
		$Casos_Class = new Casos();
        if(isset($_FILES['image_new'])){
            if(isset($_FILES['image_new']['tmp_name'])){
                $imgant = $_POST['imgant'];
                $sentinel_case_id = $_POST['id'];
                $nom_archivo1 = $_FILES["image_new"]["name"];
                $new_name1 = "SENTINEL_";
                $new_name1 .= date("Ymdhis");
                $new_extension1 = "jpg";
                preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
                switch (strtolower($ext1[2])) {
                    case 'jpeg' : $new_extension1 = ".jpg";
                        break;
                    case 'jpg' : $new_extension1 = ".jpg";
                        break;
                    case 'JPG' : $new_extension1 = ".jpg";
                        break;
                    case 'JPEG' : $new_extension1 = ".jpg";
                        break;
                    default    : $new_extension1 = "no";
                        break;
                }
                if($new_extension1 != "no"){
                    $new_name1 .= $new_extension1;
                    $resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../../assets/images/sentinel/".$new_name1);
                        if($resultArchivo1==1){
                            if($imgant != "" && $imgant != "default.jpg"){
                                if(file_exists("../../assets/images/sentinel/".$imgant)){
                                    unlink("../../assets/images/sentinel/".$imgant);
                                }
                            }
                            $op_cursoImagen = $Casos_Class->actualizaImagen($sentinel_case_id,$new_name1);
                            echo('{"resultado":"SI","archivo":"'.$new_name1.'"}');
                        }else{
                            echo('{"resultado":"NO"}');
                        }
                }else{
                    echo('{"resultado":"NO"}');
                }
            }else{
                echo('{"resultado":"NO"}');
            }
        }
    }elseif($_POST['opcn']=="CrearArchivo"){
		include_once('../models/casos.php');
		$Casos_Class = new Casos();
        if(isset($_FILES['archivo_new'])){
            if(isset($_FILES['archivo_new']['tmp_name'])){
                $sentinel_case_id = $_POST['id'];
                $nom_archivo1 = $_FILES["archivo_new"]["name"];
                $new_name1 = "SEN_GAL_";
                $new_name1 .= date("Ymdhis");
                $new_extension1 = "jpg";
                preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG|ZIP|zip|PDF|pdf)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					case 'ZIP' : $new_extension1 = ".zip";
						break;
					case 'zip' : $new_extension1 = ".zip";
						break;
					case 'PDF' : $new_extension1 = ".pdf";
						break;
					case 'pdf' : $new_extension1 = ".pdf";
						break;
					default    : $new_extension1 = "no";
						break;
				}
                if($new_extension1 != "no"){
                    $new_name1 .= $new_extension1;
                    $resultArchivo1 = copy($_FILES["archivo_new"]["tmp_name"], "../../assets/images/sentinel/".$new_name1);
                        if($resultArchivo1==1){
                            $resultado = $Casos_Class->CrearArchivoJS($sentinel_case_id,$new_name1,str_replace('.', '', $new_extension1));
                            echo('{"resultado":"SI","archivo":"'.$new_name1.'"}');
                        }else{
                            echo('{"resultado":"NO"}');
                        }
                }else{
                    echo('{"resultado":"NO"}');
                }
            }else{
                echo('{"resultado":"NO"}');
            }
        }
    }elseif($_POST['opcn']=="filtrar"){
		include_once('models/casos.php');
		$Casos_Class = new Casos();
		if($_POST['dealer_id_s']!='0'){
			$_SESSION['dealer_id_s'] = $_POST['dealer_id_s'];
		}else{
			if(isset($_SESSION['dealer_id_s'])){
				unset($_SESSION['dealer_id_s']);
			}
		}
		if($_POST['sentinel_model_id_s']!='0'){
			$_SESSION['sentinel_model_id_s'] = $_POST['sentinel_model_id_s'];
		}else{
			if(isset($_SESSION['sentinel_model_id_s'])){
				unset($_SESSION['sentinel_model_id_s']);
			}
		}
		if($_POST['status_id_s']!='0'){
			$_SESSION['status_id_s'] = $_POST['status_id_s'];
		}else{
			if(isset($_SESSION['status_id_s'])){
				unset($_SESSION['status_id_s']);
			}
		}
		if($_POST['sub_status_id_s']!='0'){
			$_SESSION['sub_status_id_s'] = $_POST['sub_status_id_s'];
		}else{
			if(isset($_SESSION['sub_status_id_s'])){
				unset($_SESSION['sub_status_id_s']);
			}
		}
		if($_POST['sentinel_service_id_s']!='0'){
			$_SESSION['sentinel_service_id_s'] = $_POST['sentinel_service_id_s'];
		}else{
			if(isset($_SESSION['sentinel_service_id_s'])){
				unset($_SESSION['sentinel_service_id_s']);
			}
		}
		if($_POST['sentinel_system_id_s']!='0'){
			$_SESSION['sentinel_system_id_s'] = $_POST['sentinel_system_id_s'];
		}else{
			if(isset($_SESSION['sentinel_system_id_s'])){
				unset($_SESSION['sentinel_system_id_s']);
			}
		}
		if($_POST['date_start_s']!=''){
			$_SESSION['date_start_s'] = $_POST['date_start_s'];
		}else{
			if(isset($_SESSION['date_start_s'])){
				unset($_SESSION['date_start_s']);
			}
		}
		if($_POST['date_end_s']!=''){
			$_SESSION['date_end_s'] = $_POST['date_end_s'];
		}else{
			if(isset($_SESSION['date_end_s'])){
				unset($_SESSION['date_end_s']);
			}
		}
		$cantidad_datos = $Casos_Class->consultaCantidad();
		$circulares = $Casos_Class->consultaCircular();
		$listadoConcesionarios = $Casos_Class->consultaConcesionarios();
		$listadoModelos = $Casos_Class->consultaBiblioteca('model','status');
		$listadoAplicaciones = $Casos_Class->consultaBiblioteca('application','');
		$listadoServicios = $Casos_Class->consultaBiblioteca('service','');
		$listadoSystems = $Casos_Class->consultaBiblioteca('system','');
		$listadoEstados = $Casos_Class->consultaEstados();
		$listadoSubEstados = $Casos_Class->consultaSubEstados();
	}elseif($_POST['opcn']=="Descargar"){
		//Tiempo de espera
		//sleep(2);
		include_once('../models/casos.php');
		$Casos_Class = new Casos();
		$tabla_Datos = $Casos_Class->consultaDatosCasos_para_descarga();

		// print_r($tabla_Datos);

	echo '<table border>
				<tr>
				  <td><strong>SENTINEL</strong></td>
				  <td><strong>VIN</strong></td>
				  <td><strong>CONCESIONARIO</strong></td>
				  <td><strong>MODELO</strong></td>
				  <td><strong>QUEJA</strong></td>
				  <td><strong>CREACIÓN</strong></td>
				  <td><strong>ESTADO</strong></td>
				  <td><strong>DÍAS EN SENTINEL</strong></td>
				  <td><strong>DÍAS EN CALIDAD ÚLTIMO ESCALAMIENTO</strong></td>
				  <td><strong>DIAS EN CALIDAD TOTAL ESCALAMIENTO</strong></td>
				  <td><strong>FECHA ESCALADO ÚLTIMO</strong></td>
				  <td><strong>FECHA PRIMER ESCALAMIENTO</strong></td>
				</tr>';
		foreach ($tabla_Datos as $key => $value) {

			echo '<tr>
				  <td>';echo $value['sentinel_case_id'];	echo '</td>
				  <td>';echo $value['vin'];	echo '</td>
				  <td>';echo $value['dealer']; echo '</td>
				  <td>';echo $value['sentinel_model'];	echo '</td>
				  <td>';echo $value['customer_complaint'];	echo '</td>
				  <td>';echo $value['date_creation']; echo '</td>
				  <td>';echo $value['status']; if(isset($value['sub_estado'])){ echo ' - '.$value['sub_estado'];}	echo '</td>
				  <td>';echo $value['day_difsentinel'];	echo '</td>
				  <td>';echo $value['day_difquality'];	echo '</td>
				  <td>';echo $value['day_difquality_tot'];	echo '</td>
				  <td>';echo $value['fec_quality'];	echo '</td>
				  <td>';echo $value['fec_qualityTot'];	echo '</td>
				</tr>';
			}

			echo '</table>';

	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas Casos
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY ca.date_sentinel DESC';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		// $sOrder = ' ORDER BY ca.sentinel_case_id '.$_GET['sSortDir_0'];
    		$sOrder = ' ORDER BY ca.date_creation desc';
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY d.dealer '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='2') {
    		$sOrder = ' ORDER BY mo.sentinel_model '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='3') {
    		$sOrder = ' ORDER BY ca.date_sentinel '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY ca.date_creation '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY ca.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	$p = $_GET;
	include_once('../models/casos.php');
	$Casos_Class = new Casos();
	$datosConfiguracion = $Casos_Class->consultaDatosCasos($p,$sOrder,$sLimit);
	$cantidad_datos = $Casos_Class->consultaCantidadFull($p,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==4){
    		$val_estado = '<span class="label label-inverse">'.$aInfo['status'].'</span>';
    	}else if($aInfo['status_id']==5){
    		$val_estado = '<span class="label label-default">'.$aInfo['status'].'</span>';
    	}else if($aInfo['status_id']==6){
    		$val_estado = '<span class="label label-primary">'.$aInfo['status'].'</span>';
    	}else if($aInfo['status_id']==7){
    		$val_estado = '<span class="label label-success">'.$aInfo['status'].'</span>';
    	}else if($aInfo['status_id']==8){
    		$val_estado = '<span class="label label-info">'.$aInfo['status'].'</span>';
    	}else if($aInfo['status_id']==9){
    		$val_estado = '<span class="label label-danger">'.$aInfo['status'].'</span>';
    	}
    	//if($_SESSION['SentinelData']=='Admin'){}
    	//if($_SESSION['SentinelData']=='Colab'){}
    	//if($_SESSION['SentinelData']=='Garantias'){}
    	//if($_SESSION['SentinelData']=='Calidad'){}

    	$substatus = "";
    	$FechaCalidad = "";

    	$valOpciones = '<a title="Ver Caso" target="_blank" href="op_caso.php?opcn=ver&id='.$aInfo['sentinel_case_id'].'" class="btn-action glyphicons eye_open btn-default"><i></i></a><br>';

    	if($_SESSION['SentinelData']=='Admin' || ($_SESSION['SentinelData']=='Colab' && ($aInfo['status'] == 'En proceso' || $aInfo['status'] == 'Pendiente x info Concs.') ) ){
	    	$valOpciones .= '<a title="Editar Caso" target="_blank" href="op_caso.php?opcn=editar&id='.$aInfo['sentinel_case_id'].'" class="btn-action glyphicons edit btn-success"><i></i></a>';
	    }
	    $valOpciones2 = '';
    	if($_SESSION['SentinelData']=='Calidad' || $_SESSION['SentinelData']=='Admin' || ($_SESSION['SentinelData']=='Colab' && ($aInfo['status'] == 'En proceso' || $aInfo['status'] == 'Pendiente x info Concs.') ) ){
    		$valOpciones2 = '<a title="Archivos Adjuntos" target="_blank" href="op_caso.php?opcn=galeria&id='.$aInfo['sentinel_case_id'].'" class="btn-action glyphicons inbox_out btn-inverse"><i></i></a><br>';
    	}
    	if($_SESSION['SentinelData']=='Admin' || ($_SESSION['SentinelData']=='Garantias' && ($aInfo['status'] == 'En Seguimiento'||$aInfo['status'] == 'En proceso'||$aInfo['status'] == 'Escalado a Calidad') && $aInfo['use_parts'] == "SI" ) || ($_SESSION['SentinelData']=='Calidad' && $aInfo['status'] == 'Escalado a Calidad') ){
    		$valOpciones2 .= '<a title="Gestionar Caso" target="_blank" href="op_caso.php?opcn=gestion&id='.$aInfo['sentinel_case_id'].'" class="btn-action glyphicons wallet btn-warning"><i></i></a>';
    	}elseif($_SESSION['SentinelData']=='Colab'){
    		$valOpciones2 .= '<a title="Gestionar Caso" target="_blank" href="op_caso.php?opcn=gestion&id='.$aInfo['sentinel_case_id'].'" class="btn-action glyphicons wallet btn-warning"><i></i></a>';
    	}
    	$fechas = "Fecha Sentinel: ".$aInfo['date_sentinel'];
    	$fechas .= '<span class="text-info">'."<br>Días Sentinel: ".$aInfo['day_difsentinel'].'</span>';
    	$fechas .= "<br>Escalado: ".$aInfo['fec_quality'];
    	$fechas .= '<span class="text-danger">'."<br>Días Diferencia: ".$aInfo['day_difquality'].'</span>';
    	$fechas .= "<br>Escalado T: ".$aInfo['fec_qualityTot'];
    	$fechas .= '<span class="text-danger">'."<br>Días Total: ".$aInfo['day_difquality_tot'].'</span>';
    	$fechas .= '<span class="text-warning">'."<br>Fecha Cierre/Calculo: ".$aInfo['fec_close'].'</span>';

    	$FechaCalidad = "";
    	$diasDiferencia = 0;
    	$FechaCierre = "";


    	if($aInfo['status'] == 'Escalado a Calidad'){
    		if(isset($aInfo['SubStatus'][0]['substatus'])){
    			if($aInfo['SubStatus'][0]['sub_status']=="2"){
    				$diasDiferencia = $aInfo['SubStatus'][0]['diasDif'];
    				$FechaCalidad = $aInfo['SubStatus'][0]['date_gestion'];
    			}
    			if($aInfo['SubStatus'][0]['sub_status']=="1"){
    				$FechaCalidad = $aInfo['SubStatus'][0]['date_gestion'];
    				$datetime1 = date_create($aInfo['SubStatus'][0]['date_gestion']);
					$datetime2 = date_create(date('Y-m-d'));
					$y = date_diff($datetime1, $datetime2);
					$diasDiferencia = $y->format('%d');
    			}
    			if($aInfo['SubStatus'][0]['sub_status']=="3" || $aInfo['SubStatus'][0]['sub_status']=="4"){
    				$FechaCierre = $aInfo['SubStatus'][0]['date_gestion'];
    				$datetime1 = date_create($aInfo['SubStatus'][0]['date_gestion']);
    				$datetime2 = "";
    				//buscar la fecha en que se escaló
    					foreach ($aInfo['StatusComplete'] as $key => $value) {
    						if($datetime2=="" && ($aInfo['SubStatus'][0]['sub_status'] != $value['sub_status']) ){
    							$datetime2 = date_create($value['date_gestion']);
    							$FechaCalidad = $value['date_gestion'];
    						}
    					}
    				//buscar la fecha en que se escaló
    				$y = date_diff($datetime1, $datetime2);
					$diasDiferencia = $y->format('%d');
    			}
    			$substatus = '<br><span class="label label-warning"><b>Calidad: </b>'.$aInfo['SubStatus'][0]['substatus'].'</span>';

    			/*if($FechaCierre!=""){
    				$fechas .= "<br>Cierre: ".$FechaCierre;
    			}*/
    			//$fechas .= '<span class="text-danger">'."<br>Días Diferencia: ".$diasDiferencia.'</span>';
    		}
    	}

    	$valOpciones2 .= '<a title="Agregar Vin" target="_blank" href="op_caso.php?opcn=vines&id='.$aInfo['sentinel_case_id'].'" class="btn-action glyphicons cars btn-primary"><i></i></a>';
    	$aItem = array(
    		'<b>ID:</b> SDC'.$aInfo['sentinel_case_id'].'<br><b>VIN:</b> '.$aInfo['vin'].'<br><b>Ticket CAT:<b>'.$aInfo['case_cat'],
			$aInfo['dealer'].'<br><span style="font-size: 70%;"><b>QUEJA: </b>'.$aInfo['customer_complaint'].'</label>',
			$aInfo['sentinel_model'],
			$fechas,
			$aInfo['date_creation'].'<br><span class="label label-inverse">'.$aInfo['first_name'].' '.$aInfo['last_name'].'</span>',
			$val_estado.$substatus,
			$valOpciones,
			$valOpciones2,
			'DT_RowId' => $aInfo['sentinel_case_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/casos.php');
	$Casos_Class = new Casos();
	$cantidad_datos = $Casos_Class->consultaCantidad();
	$circulares = $Casos_Class->consultaCircular();
	$listadoConcesionarios = $Casos_Class->consultaConcesionarios();
	$listadoModelos = $Casos_Class->consultaBiblioteca('model','status');
	$listadoAplicaciones = $Casos_Class->consultaBiblioteca('application','');
	$listadoServicios = $Casos_Class->consultaBiblioteca('service','');
	$listadoSystems = $Casos_Class->consultaBiblioteca('system','');
	$listadoCheks = $Casos_Class->consultaBiblioteca('check','');
	$listadoLubricantes = $Casos_Class->consultaBiblioteca('lubricant','');
	$listadoEstados = $Casos_Class->consultaEstados();
	$listadoSubEstados = $Casos_Class->consultaSubEstados();
}
unset($Casos_Class);
