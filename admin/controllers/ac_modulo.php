<?php

if(isset($_POST['opcn'])){
  $partes = parse_url($_SERVER['HTTP_REFERER']);
  parse_str($partes['query'], $vars);
  $_POST['module_id'] = $vars['id'];

  include_once('../models/ac_modulo.php');
  $obj = new Ac_modulo();
  if( $_POST['opcn'] == 'crear'){

    // valida si existe el nombre de la actividad
    $valida = $obj->valida_actividades($_POST);
    if($valida['error']){
      echo json_encode($valida);
      die();
    }

    include_once('../src/funciones_globales.php');

    $adjunto = '';
    if(isset($_FILES["adjunto"])){
      $ruta = '../../assets/actividades/';
      $tmp_name = $_FILES["adjunto"]["tmp_name"];
      $image = $_FILES['adjunto']['name'];
      $iniciales = 'autoTrain_actividad_';
      $adjunto = Funciones::renombrar_y_subir_archivo($ruta, $tmp_name, $image, $iniciales, '' );
    }

    $_POST['review_id'] = $_POST['tipo'] != 'Evaluacion' ? 0 : $_POST['review_id'];

    $_POST['adjunto'] = $adjunto;
    $datos = $obj->crearActividades($_POST);
    echo json_encode($datos);
  }elseif( $_POST['opcn'] == 'borrar'){
    $adjunto = $obj->getActividad($_POST);
    if( $adjunto != '' ){
      if(unlink('../../assets/actividades/'.$adjunto)){
        $data = $obj->borrar($_POST);
      }
    }else{
      $data = $obj->borrar($_POST);
    }
    echo json_encode($data);
  }
  die();
}
if(isset($_GET['opcn'])){
    include_once('models/ac_modulo.php');
    if(isset($_GET['id'])){
      $obj = new Ac_modulo();
      $datos = $obj->getActividades($_GET['id']);
    }
    
    unset($obj);
}

