<?php
$ano = 0;
$mes = 0;
$cantidad_datos = 0;
if(isset($_POST['opcn']) && $_POST['opcn']=='proc' ){

	$year = $_POST['ano'];
	$ano = $year;
	include('models/rep_costo.php');
	$costo_Class = new Costo();
	$diaInicial = $_POST['ano'].'-'.$_POST['mes'].'-01 00:00:00';
	$month = $_POST['ano'].'-'.$_POST['mes'];
	$mes = $_POST['mes'];
	$aux = date('Y-m-d', strtotime("{$month} + 1 month"));
	$diaFinal = date('Y-m-d', strtotime("{$aux} - 1 day"));
	$diaFinal .= " 23:59:59";
	$RegistrosConcesionarios = $costo_Class->consultaDatosAll(0,$diaInicial,$diaFinal);
	$RegistrosAjustes = $costo_Class->consultaAjustesMes(0,$diaInicial,$diaFinal);

	//Totales
	for($i=0;$i<count($RegistrosConcesionarios);$i++) { //Recorre cada concesionario

		$total_cobros = 0;
		$total_descuentos = 0;
		$total_alimentacion = 0;
		$total_asistentes = 0;
		$total_excusas = 0;
		if(isset($RegistrosConcesionarios[$i]['Detalle_Sesiones'])){ //  Si el concesionario tiene sesiones

			for($j=0;$j<count($RegistrosConcesionarios[$i]['Detalle_Sesiones']);$j++) { // Recorre las sesiones por cada concesionario
				$total_cobros += $RegistrosConcesionarios[$i]['Detalle_Sesiones'][$j]['rate_course']; // Costo total de cada sesion 
				$total_descuentos += $RegistrosConcesionarios[$i]['Detalle_Sesiones'][$j]['descuento'];
				$total_alimentacion += $RegistrosConcesionarios[$i]['Detalle_Sesiones'][$j]['alimentacion'];
				$total_asistentes += $RegistrosConcesionarios[$i]['Detalle_Sesiones'][$j]['dealer_inscriptions'];
				$total_excusas += $RegistrosConcesionarios[$i]['Detalle_Sesiones'][$j]['excusas'];
			} 

		}

		$RegistrosConcesionarios[$i]['cobro'] = $total_cobros; //Costo total del concesionario
		$RegistrosConcesionarios[$i]['descuento'] = $total_descuentos; //Descuento total al concesionario
		$RegistrosConcesionarios[$i]['alimentacion'] = $total_alimentacion; //Alimentacion total al concesionario
		$RegistrosConcesionarios[$i]['asistentes'] = $total_asistentes; //Alimentacion total al concesionario
		$RegistrosConcesionarios[$i]['excusas'] = $total_excusas; //Alimentacion total al concesionario

		for($x=0;$x<count($RegistrosAjustes);$x++) {
			if ( $RegistrosAjustes[$x]['dealer_id'] == $RegistrosConcesionarios[$i]['dealer_id'] ) {
				$RegistrosConcesionarios[$i]['ajustes'] = $RegistrosAjustes[$x]['cost_total']; //Ajuste valor de mes al concesionario
			}
		}
		
	}

	$cantidad_datos = count($RegistrosConcesionarios);

} else {
	include_once('models/rep_costo.php');
	$costo_Class = new Costo();
	//CREACION DEL REPORTES PARA DESCARGAR
	if( isset($_GET['year']) && isset($_GET['month']) && isset($_GET['type']) ){

		//CREACION DEL REPORTE DETALLADO PARA DESCARGAR
		if ( $_GET['type'] == '1' ) {
			$diaInicial = $_GET['year'].'-'.$_GET['month'].'-01 00:00:00';
		    $month = $_GET['year'].'-'.$_GET['month'];
		    $aux = date('Y-m-d', strtotime("{$month} + 1 month"));
		    $diaFinal = date('Y-m-d', strtotime("{$aux} - 1 day"));
		    $diaFinal .= " 23:59:59";

			$datosCursos_all = $costo_Class->Reporte_consultaDatosAll($diaInicial,$diaFinal);
			$datosCursosNoInv = $costo_Class->Reporte_consultaDatosNoInv($diaInicial,$diaFinal);
			$cantidad_datos = count($datosCursos_all);
			$registros = 0;
			$valor = 0;

			// INCLUSION DE LOS CUPOS NO INVITADOS
			for($i=0;$i<count($datosCursos_all);$i++) { // Recorre las sesiones (schedule_id) existentes por curso
				if(isset($datosCursos_all[$i]['noinvitado'])){ // Si existen no invitados
					for($j=0;$j<count($datosCursos_all[$i]['noinvitado']);$j++) { // Recorre las sesiones (schedule_id) de no invitados
						for($k=0;$k<count($datosCursosNoInv);$k++) { // Recorre las sesiones de Reporte_consultaDatosNoInv
							if ( $datosCursos_all[$i]['noinvitado'][$j]['schedule_id'] == $datosCursosNoInv[$k]['schedule_id'] ) {
								if ( $datosCursos_all[$i]['noinvitado'][$j]['origin_dealer'] == $datosCursosNoInv[$k]['dealer_id'] ) {
									$datosCursosNoInv[$k]['valor'] = $datosCursos_all[$i]['noinvitado'][$j]['rate_course'];
									$datosCursosNoInv[$k]['cupos'] = $datosCursos_all[$i]['noinvitado'][$j]['dealer_inscriptions'];
									break;
								}
							}
						}
					}
				}
			}
			
		}

		//CREACION DEL REPORTE SIMPLIFICADO PARA DESCARGAR
		if ( $_GET['type'] == '2' ) {
			$diaInicial = $_GET['year'].'-'.$_GET['month'].'-01 00:00:00';
		    $month = $_GET['year'].'-'.$_GET['month'];
		    $aux = date('Y-m-d', strtotime("{$month} + 1 month"));
		    $diaFinal = date('Y-m-d', strtotime("{$aux} - 1 day"));
		    $diaFinal .= " 23:59:59";

		    $RegistrosCursos_all = $costo_Class->consultaDatosAll(0,$diaInicial,$diaFinal);
			$RegistrosAjustesMes = $costo_Class->consultaAjustesMes(0,$diaInicial,$diaFinal);
			//Totales
			for($i=0;$i<count($RegistrosCursos_all);$i++) { //Recorre cada concesionario

				$total_cobros = 0;
				$total_descuentos = 0;
				$total_alimentacion = 0;
				$total_asistentes = 0;
				$total_excusas = 0;
				if(isset($RegistrosCursos_all[$i]['Detalle_Sesiones'])){ //  Si el concesionario tiene sesiones

					for($j=0;$j<count($RegistrosCursos_all[$i]['Detalle_Sesiones']);$j++) { // Recorre las sesiones por cada concesionario
						$total_cobros += $RegistrosCursos_all[$i]['Detalle_Sesiones'][$j]['rate_course']; // Costo total de cada sesion 
						$total_descuentos += $RegistrosCursos_all[$i]['Detalle_Sesiones'][$j]['descuento'];
						$total_alimentacion += $RegistrosCursos_all[$i]['Detalle_Sesiones'][$j]['alimentacion'];
						$total_asistentes += $RegistrosCursos_all[$i]['Detalle_Sesiones'][$j]['dealer_inscriptions'];
						$total_excusas += $RegistrosCursos_all[$i]['Detalle_Sesiones'][$j]['excusas'];
					} 

				}
				$RegistrosCursos_all[$i]['cobro'] = $total_cobros; //Costo total del concesionario
				$RegistrosCursos_all[$i]['descuento'] = $total_descuentos; //Descuento total al concesionario
				$RegistrosCursos_all[$i]['alimentacion'] = $total_alimentacion; //Alimentacion total al concesionario
				$RegistrosCursos_all[$i]['asistentes'] = $total_asistentes; //Alimentacion total al concesionario
				$RegistrosCursos_all[$i]['excusas'] = $total_excusas; //Alimentacion total al concesionario

				for($x=0;$x<count($RegistrosAjustesMes);$x++) {
					if ( $RegistrosAjustesMes[$x]['dealer_id'] == $RegistrosCursos_all[$i]['dealer_id'] ) {
						$RegistrosCursos_all[$i]['ajustes'] = $RegistrosAjustesMes[$x]['cost_total']; //Ajuste valor de mes al concesionario
					}
				}

			}
			
			$cantidad_datos = count($RegistrosCursos_all);

		}

	}
	//CREACION DE FACTURA POR CONCESIONARIO PARA DESCARGAR
	if( isset($_GET['factura']) && isset($_GET['year']) && isset($_GET['month']) ){

		$diaInicial = $_GET['year'].'-'.$_GET['month'].'-01 00:00:00';
	    $month = $_GET['year'].'-'.$_GET['month'];
	    $aux = date('Y-m-d', strtotime("{$month} + 1 month"));
	    $diaFinal = date('Y-m-d', strtotime("{$aux} - 1 day"));
	    $diaFinal .= " 23:59:59";
	    $concesionario = $_GET['factura'];
	    $periodoInicial =  $_GET['year'].'-'.$_GET['month'].'-01';
	    $periodoFinal = date('Y-m-d', strtotime("{$aux} - 1 day"));

		$datosCursos = $costo_Class->consultaDatosAll($concesionario,$diaInicial,$diaFinal);
		$datosEspecialidad = $costo_Class->consulta_especialidades();
		$datosAjustesMes = $costo_Class->consultaAjustesMes($concesionario,$diaInicial,$diaFinal);

		//Totales
		for($i=0;$i<count($datosCursos);$i++) { //Recorre cada concesionario

			if(isset($datosCursos[$i]['Detalle_Sesiones'])){ //  Si el concesionario tiene sesiones
				
				for($j=0;$j<count($datosCursos[$i]['Detalle_Sesiones']);$j++) { // Recorre las sesiones por cada concesionario

					for($x=0;$x<count($datosEspecialidad);$x++) {

						if ($datosCursos[$i]['Detalle_Sesiones'][$j]['specialty_id'] == $datosEspecialidad[$x]['specialty_id']) {
							//Se guardan por especialidad los totales
							$datosEspecialidad[$x]['cobro'] += $datosCursos[$i]['Detalle_Sesiones'][$j]['rate_course']; 
							//  Costo total del concesionario

							$datosEspecialidad[$x]['descuento'] += $datosCursos[$i]['Detalle_Sesiones'][$j]['descuento']; 
							//Descuento total al concesionario

							$datosEspecialidad[$x]['alimentacion'] += $datosCursos[$i]['Detalle_Sesiones'][$j]['alimentacion'];
							//Alimentacion total al concesionario
							
							//Numro de prestamos por sala
							if ( $datosCursos[$i]['Detalle_Sesiones'][$j]['descuento'] > 0 ) {
								$datosEspecialidad[$x]['prestamos'] += 1;
							}

							$datosEspecialidad[$x]['excusas'] += $datosCursos[$i]['Detalle_Sesiones'][$j]['excusas'];
							//Excusas total al concesionario
							
						}
					}
				} 
			}
			//Total por especialidades
			for($y=0;$y<count($datosEspecialidad);$y++) {
				$datosCursos[$i]['cobro'] += $datosEspecialidad[$y]['cobro']; 
				$datosCursos[$i]['descuento'] += $datosEspecialidad[$y]['descuento']; 
				$datosCursos[$i]['alimentacion'] += $datosEspecialidad[$y]['alimentacion'];
				$datosCursos[$i]['excusas'] += $datosEspecialidad[$y]['excusas'];
			}
			//Total de Ajustes por mes
			for($z=0;$z<count($datosAjustesMes);$z++) {
				if ( $datosAjustesMes[$z]['dealer_id'] == $datosCursos[$i]['dealer_id'] ) {
					$datosCursos[$i]['ajustes'] = $datosAjustesMes[$z]['cost_total']; //Ajuste valor de mes al concesionario
				}
			}

		}

		$cantidad_datos = count($datosCursos);
	}

}
unset($costo_Class);
