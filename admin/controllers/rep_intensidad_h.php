<?php

if( isset( $_POST['opcn'] ) ){

    switch ( $_POST['opcn'] ) {

        //================================================================
        // Obtiene los datos del reporte
        //================================================================
        case 'consultar':
            include_once( '../models/rep_intensidad_h.php' );
            $p = $_POST;
            if( isset($p['concesionarios']) ){
                $p['dealer_id'] = implode( $p['concesionarios'], ',' );
            }else{
                $p['dealer_id'] = "";
            }
            $cursos = RepIntensidadH::getCursos( $p );
            $inscritos = RepIntensidadH::getInscritos( $p );
            $wbt = RepIntensidadH::consultaWBT( $p );
            $_datos = RepIntensidadH::sabanaDatos( $cursos, $inscritos, $wbt );

            echo( json_encode( $_datos ) );
        break;
    }

}elseif( isset( $_GET['opcn'] ) ){
    include_once('models/rep_intensidad_h.php');
    switch ( $_GET['opcn'] ) {

        //================================================================
        // Obtiene los datos del reporte en formado excel
        //================================================================
        case 'descargar':
            include_once( 'models/rep_intensidad_h.php' );
            $p = $_GET;
            $cursos = RepIntensidadH::getCursos( $p, '' );
            $inscritos = RepIntensidadH::getInscritos( $p, '' );
            $wbt = RepIntensidadH::consultaWBT( $p, '' );
            $_datos = RepIntensidadH::sabanaDatos( $cursos, $inscritos, $wbt );

        break;
    }

}else{   //fin isset $_GET['opcn']
    include_once('models/rep_intensidad_h.php');
    $concesionarios = RepIntensidadH::getConcesionarios("",'');
}
