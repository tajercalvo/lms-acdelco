<?php

if(isset($_POST['opcn'])){
	//ini_set("session.cookie_lifetime",1900);
	//ini_set("session.gc_maxlifetime",1900);
	session_start();
	if($_POST['opcn']=="login"){
		$usuario	= "";
		$contrasena = "";
		$usuario 	= stripslashes(str_replace(' AND ','',$_POST['username']));
		$usuario	= str_replace(' OR ','',$usuario);
		$usuario	= str_replace('=','',$usuario);
		$contrasena = stripslashes(str_replace(' AND ','',$_POST['password']));
		$contrasena	= str_replace(' OR ','',$contrasena);
		$contrasena	= str_replace('=','',$contrasena);
		include('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		$cant_user = $usuario_Class->login($usuario,$contrasena);
		if($cant_user>0){
			$_SESSION["ultimoAcceso"]= date("Y-n-j H:i:s");
			$cant_user = $usuario_Class->ConsultaEncuestasSatIBTVCT();
			$cant_user = $usuario_Class->ConsultaEncuestasSatWBT();
			$cant_user = $usuario_Class->ConsultaEncuestasUsrPorCargo();
			$cant_user = $usuario_Class->ConsultaEvaluacionCargo();
			$cant_user = $usuario_Class->ConsultaEvaluacionCurso();
			$cant_user = $usuario_Class->ConsultaEncuestaObl_Ger();
			header("location: ../login.php");
		}else{
			$_SESSION['errorLogin'] = "ErrorPresentado";
			header("location: ../login.php");
		}
		unset($usuario_Class);
	}else if($_POST['opcn']=="contrasena"){
		include('../models/usuarios.php');
		$idUsuario = $_POST['idUsuario'];
		$contrasena = $_POST['contrasena'];
		$usuario_Class = new Usuarios();
		$op_user = $usuario_Class->cambiaContrasena($idUsuario,$contrasena);
		if($op_user>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
		unset($usuario_Class);
	}else if($_POST['opcn']=="remember"){
		include('../models/usuarios.php');
		$email = $_POST['email'];
		$email 	= stripslashes(str_replace(' AND ','',$email));
		$email	= str_replace(' OR ','',$email);
		$email	= str_replace('=','',$email);
		$usuario_Class = new Usuarios();
		$op_user = $usuario_Class->consultaDatosUsuario_By('email',$email);
		if($op_user>0){
			$result = $usuario_Class->cambiaContrasena($op_user['user_id'],$op_user['identification']);
			$_SESSION['successRecord'] = "ProcesoEjecutado";
			//echo('{"resultado":"SI"}');
			header("location: ../recordar_pss");
		}else{
			$_SESSION['errorRecord'] = "ErrorPresentado";
			//echo('{"resultado":"NO"}');
			header("location: ../recordar_pss");
		}
	}else if($_POST['opcn']=="registrarme"){
		include('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		$res = $usuario_Class->registrarme($_POST);
		echo json_encode($res);
	}else if($_POST['opcn']=="listarTrayectorias"){
		include('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		$res = $usuario_Class->ListarTrayectorias();
		echo json_encode($res);
	}
}else{
	session_start();
	$_SESSION['errorLogin'] = "ErrorPresentado";
	header("location: ../login.php");
}
