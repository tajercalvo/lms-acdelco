<?php

include('models/rep_impacto.php');
$datos_Class = new Datos();

if(isset($_POST['ano_id_end'])){
	$year_end = $_POST['ano_id_end'];
}else{
	$year_end = date("Y");
}

$fec_finalQI = $year_end.'-01-01';
$fec_finalQ = $year_end.'-12-31';

$modelo_sel = "''";
if(isset($_POST['model'])){
	foreach ($_POST['model'] as $iID => $model_selec) {
		$modelo_sel .= ",'".$model_selec."'";
	}
}

$course_sel = "0";
if(isset($_POST['course_id'])){
	foreach ($_POST['course_id'] as $iID => $course_selec) {
		$course_sel .= ",".$course_selec;
	}
}

$zona = "0";
$concesionario = "0";

if(isset($_POST['zone_id'])){
	$zona = $_POST['zone_id'];
}
if(isset($_POST['dealer_id'])){
	$concesionario = $_POST['dealer_id'];
}



$DatosCant_RangosEdad		= $datos_Class->CantUsuarios_RangosEdad($year_end,$zona,$concesionario);
$DatosCant_Genero			= $datos_Class->CantUsuarios_Genero($year_end,$zona,$concesionario);
$DatosCant_Trayectoria		= $datos_Class->CantUsuarios_Trayectoria($year_end,$zona,$concesionario);
$DatosCant_VentasModelo		= $datos_Class->CantVentas_Modelo($fec_finalQI,$fec_finalQ,$modelo_sel,$zona,$concesionario);
$DatosAvg_CursosPromedio	= $datos_Class->CursosPromedio($fec_finalQI,$fec_finalQ,$course_sel,$zona,$concesionario);

$Modelos 		= $datos_Class->Modelos();
$Cursos 		= $datos_Class->Cursos();
$Concesionarios	= $datos_Class->Concesionarios();
$Zonas			= $datos_Class->Zonas();

/*
$DatosHoraHabilidad 		= $datos_Class->HorasHabilidad($fec_finalQI,$fec_finalQ);
$DatosHoraTipo 				= $datos_Class->HoraHabilidadTipo($fec_finalQI,$fec_finalQ);
$DatosHoraProveedor 		= $datos_Class->HoraHabilidadTipoProveedor($fec_finalQI,$fec_finalQ);
$Datos_HorasCurso			= $datos_Class->HorasPorHorasCurso($fec_finalQI,$fec_finalQ);
$DatosHoras_Tipo 			= $datos_Class->HorasTipo($fec_finalQI,$fec_finalQ);
$DatosHoras_Proveedor 		= $datos_Class->HorasProveedor($fec_finalQI,$fec_finalQ);
$DatosHoras_TipoProveedor 	= $datos_Class->HorasTipoProveedor($fec_finalQI,$fec_finalQ);

$DatosCursoHabilidad 		= $datos_Class->CursosHabilidad($fec_finalQI,$fec_finalQ);
$DatosCursoTipo 			= $datos_Class->CursoHabilidadTipo($fec_finalQI,$fec_finalQ);
$DatosCursoProveedor 		= $datos_Class->CursoHabilidadTipoProveedor($fec_finalQI,$fec_finalQ);
$Datos_CursosCurso			= $datos_Class->CursosPorHorasCurso($fec_finalQI,$fec_finalQ);
$DatosCursos_Tipo 			= $datos_Class->CursosTipo($fec_finalQI,$fec_finalQ);
$DatosCursos_Proveedor 		= $datos_Class->CursosProveedor($fec_finalQI,$fec_finalQ);
$DatosCursos_TipoProveedor 	= $datos_Class->CursosTipoProveedor($fec_finalQI,$fec_finalQ);

$DatosCant_Usuarios			= $datos_Class->CantUsuarios($year_end);
$DatosCant_Genero			= $datos_Class->CantUsuarios_Genero($year_end);
$DatosCant_Zona				= $datos_Class->CantUsuarios_Zona($year_end);
$DatosCant_Jerarquia		= $datos_Class->CantUsuarios_Jerarquia($year_end);
$DatosCant_Trayectoria		= $datos_Class->CantUsuarios_Trayectoria($year_end);
$DatosCant_RangosEdad		= $datos_Class->CantUsuarios_RangosEdad($year_end);


$DatosCant_PrestSala		= $datos_Class->CantPrestamos_Sala($fec_finalQI,$fec_finalQ);
$DatosProm_CalifProveedor	= $datos_Class->PromCalificacion_Proveedor($fec_finalQI,$fec_finalQ);
$DatosProm_CalifProveedorP	= $datos_Class->PromCalificacion_ProveedorProfesor($fec_finalQI,$fec_finalQ);
$DatosProm_CalifTipoCurso	= $datos_Class->PromCalificacion_ProveedorTCurso($fec_finalQI,$fec_finalQ);*/



$cantidad_datos = count($DatosCant_RangosEdad);

unset($datos_Class);