<?php
include_once('models/rep_evaluacion_mia.php');
$Evaluaciones_Class = new RepEvaluaciones();
$cantidad_datos = 0;

if(isset($_POST['start_date_day'])){
	$datosCursos_all = $Evaluaciones_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Evaluaciones_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Evaluaciones_Class);