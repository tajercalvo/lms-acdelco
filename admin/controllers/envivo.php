<?php
if(isset($_POST['opcn'])){
	//operaciones con los datos

	if($_POST['opcn']=="CrearTransmision"){
		
		include_once('models/envivo.php');
		$multimedias_Class = new Multimedias_Detail();
		//Proceso de creación para la imagen
		if(isset($_FILES['image_new'])){
			if(isset($_FILES['image_new']['tmp_name'])){
				$imgant = "";
				if(isset($_POST['imgant'])){
					$imgant = $_POST['imgant'];
				}
				$nom_archivo1 = $_FILES["image_new"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../assets/gallery/Transmision_en_vivo/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/Transmision_en_vivo/".$imgant)){
									unlink("../assets/gallery/Transmision_en_vivo/".$imgant);
								}
							}
						}else{
							$new_name1 = "default.png";
						}
				}else{
					$new_name1 = "default.png";
				}
			}else{
				$new_name1 = "default.png";
			}
		}else{
			$new_name1 = "default.png";
		}
		//Proceso de creación para la imagen
		$resultado = $multimedias_Class->CrearTransmision($_POST['media_detail'],$_POST['description'],$_POST['video'],$new_name1, $_POST['estado']);
		$galeria_detailData = $multimedias_Class->consultaTransmision();
	
	}else if($_POST['opcn']=="editar_vid"){
		include_once('models/envivo.php');
		$multimedias_Class = new Multimedias_Detail();
		//Proceso de creación para la imagen
			$imgant = "";
			if(isset($_POST['imgant'])){
				$imgant = $_POST['imgant'];
			}
		if(isset($_FILES['image_new_ed'])){
			if(isset($_FILES['image_new_ed']['tmp_name']) && $_FILES['image_new_ed']['tmp_name']!="" ){
				$nom_archivo1 = $_FILES["image_new_ed"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new_ed"]["tmp_name"], "../assets/gallery/Transmision_en_vivo/".$new_name1, $_POST['estado2']);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/Transmision_en_vivo/".$imgant)){
									unlink("../assets/gallery/Transmision_en_vivo/".$imgant);
								}
							}
						}else{
							$new_name1 = $imgant;
						}
				}else{
					$new_name1 = $imgant;
				}
			}else{
				$new_name1 = $imgant;
			}
		}else{
			$new_name1 = $imgant;
		}
		//Proceso de creación para la imagen
		$resultado = $multimedias_Class->ActualizarMultimedias($_POST['media_detail_id'], $_POST['media_detail_ed'], $_POST['description_ed'], $_POST['video_ed'], $new_name1,$_POST['estado2']);
		$galeria_detailData = $multimedias_Class->consultaTransmision();
	}elseif($_POST['opcn']=="LikeGalery"){
		include_once('../models/envivo.php');
		$multimedias_Class = new Multimedias_Detail();
		$resultado = $multimedias_Class->CrearLike($_POST['media_detail_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}

	//Codigo para contador de veces que se vio un video
	else if($_POST['opcn']=="streaming_views"){
		// echo "entro";
		include_once('../models/envivo.php');
		$multimedias_Class = new Multimedias_Detail();
		$resultado = $multimedias_Class->CrearMediaView($_POST['media_detail_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}
		//Fin codigo para contador de veces que se vio un video
} else {

	include_once('models/envivo.php');
$multimedias_Class = new Multimedias_Detail();
$galeria_detailData = $multimedias_Class->consultaTransmision();
}


unset($multimedias_Class);
