<?php
if(isset($_POST['opcn'])){
  include_once('../models/asignar_videos.php');
  include_once('../src/funciones_globales.php');
  $biblioteca_Class = new Libraries();

  if($_POST['opcn']=="crear"){
    //Capturando datos del js
  
    $name = $_POST['name'];
    $description = $_POST['description'];
    $segment_id = $_POST['segment_id'];
    $status_id = $_POST['status_id'];
    $Cvct_id = $_POST['schedule_id'];

    //thumbnails
    $ruta = '../../assets/Biblioteca_Cursos_VCT/imagenes/';
    $tmp_name = $_FILES["image"]["tmp_name"];
    $image = $_FILES['image']['name'];
    $iniciales = 'AutoTrain_LUDUS_';
    $image = Funciones::renombrar_y_subir_archivo($ruta, $tmp_name, $image, $iniciales, '' );

    if( $image != false ){

        $fileTmpLocm = $_FILES["image"]["tmp_name"];

         if (isset($_FILES["file1"])!='') {

              //Renombrando el archivo que se va a subir
              $ruta = '../../assets/Biblioteca_Cursos_VCT/';
              $tmp_name = $_FILES["file1"]["tmp_name"];
              $new_name = $_FILES['file1']['name'];
              $iniciales = 'AutoTrain_LIB_';
              $new_name = Funciones::renombrar_y_subir_archivo($ruta, $tmp_name, $new_name, $iniciales, '' );

              if( $new_name != false ){
                  $res = $biblioteca_Class->CrearLibraries($new_name, $name, $image, $status_id, $description, $segment_id,$Cvct_id);
                 $library_id = $res['library_id'];

                  // $cargos = $biblioteca_Class->CrearCargos($library_id, $_POST['charge_id']);
                  // $res['cargos'] = $cargos['cargos'];


                  echo json_encode($res);
                  die();
              }else{
                echo "No se pudo cargar le archivo";
              }

      } elseif (isset($_POST['url'])) {
            $new_name = $_POST['url'];
            $res = $biblioteca_Class->CrearLibraries($new_name, $name, $image, $status_id, $description, $segment_id,$Cvct_id);
             $library_id = $res['library_id'];

              // $cargos = $biblioteca_Class->CrearCargos($library_id, $_POST['charge_id']);
              // $res['cargos'] = $cargos['cargos'];

              echo json_encode($res);
      }// FIN else url
    }else{
      echo 'No se pudo cargar la imagen';
    }// fin del if new_extencion1 != no
  }else
  if($_POST['opcn']=="editar"){

    $library_id = $_POST['library_id'];
    $name = $_POST['name'];
    $description = $_POST['description'];
    $status_id = $_POST['status_id'];
    $segment_id = $_POST['segment_id'];
    $Cvct_id = $_POST['schedule_id'];


    if( isset($_FILES["image"]["tmp_name"]) ){
        $ruta = '../../assets/Biblioteca_Cursos_VCT/imagenes/';
        $tmp_name = $_FILES["image"]["tmp_name"];
        $image = $_FILES['image']['name'];
        $iniciales = 'AutoTrain_LUDUS_';
        $libreria = $biblioteca_Class->consultar_libreria( $library_id );
        $img_anterior = $libreria['libreria']['image'];

        $image = Funciones::renombrar_y_subir_archivo($ruta, $tmp_name, $image, $iniciales, $img_anterior );
    }else{
        $image = '';
    }

    if( isset($_FILES["file1"]["tmp_name"]) ){
        $ruta = '../../assets/Biblioteca_Cursos_VCT/';
        $tmp_name = $_FILES["file1"]["tmp_name"];
        $file = $_FILES['file1']['name'];
        $iniciales = 'AutoTrain_LIB_';
        $libreria = $biblioteca_Class->consultar_libreria( $library_id );
        $file_anterior = $libreria['libreria']['file'];

        $file = Funciones::renombrar_y_subir_archivo($ruta, $tmp_name, $file, $iniciales, $file_anterior);
    }else{
      $file = '';
    }

    $res = $biblioteca_Class->ActualizarLibraries($library_id, $file, $image,  $name, $status_id, $description, $segment_id,$Cvct_id);

    // $cargos = $biblioteca_Class->CrearCargos($library_id, $_POST['charge_id']);
    // $res['cargos'] = $cargos['cargos'];


    echo json_encode($res);
  }
//Consultar usuarios
  else if ($_POST['opcn']=='consultar_Usuarios') {

     $resultado = $biblioteca_Class->consultar_Usuarios($_POST['vct_id']);
    echo json_encode($resultado);
  } 
  else if ($_POST['opcn']=='asignar_video') {
           $resultado = $biblioteca_Class->asignar_video($_POST);
           echo json_encode($resultado);
  }
  else if ($_POST['opcn']=='busqueda') {
           $resultado = $biblioteca_Class->busqueda($_POST);
           echo json_encode($resultado);
  }


  include_once('../models/asignar_videos.php');
  $biblioteca_Class = new Libraries();
  if ( $_POST['opcn']=="init" ) {
    $resultado = $biblioteca_Class->consultaLibraries();
    echo json_encode($resultado);
  }else
  if(($_POST['opcn'])=="eliminar"){
    $json = array();
    if(file_exists("../assets/Biblioteca_Cursos_VCT/imagenes".$_POST['image'])){
                  unlink("../assets/Biblioteca_Cursos_VCT/imagenes".$_POST['image']);
                }else{
                  $json['obs1'] = 'No existe thumbnail';
                }
    if(file_exists("../assets/Biblioteca_Cursos_VCT/".$_POST['file'])){
                  unlink("../assets/Biblioteca_Cursos_VCT/".$_POST['file']);
                }else{
                  $json['obs2'] = 'No existe archivo';
                }
    $res = $biblioteca_Class->eliminarLibraries($_POST['library_id']);
    $json['msj'] = $res['msj'];
    $json['error'] = $res['error'];
    $json['type'] = $res['type'];
    echo json_encode($json);
  }else
  if ( $_POST['opcn']=="consultar_libreria" ) {
    $resultado = $biblioteca_Class->consultar_libreria( $_POST['library_id'] );
    echo json_encode($resultado);
  }

}
//fin del if isset  POST opcn

  if((!isset($_POST['opcn'])) and (!isset($_GET['opcn']))){
    include_once('models/asignar_videos.php');
    $biblioteca_Class = new Libraries();
    // Preguntar donde consulta la caja de texto "otros" de la vista en la base de datos
   
    $especialidades = $biblioteca_Class->getSpecialities();
    $Cursos_Vct = $biblioteca_Class->get_Vct();
  }


   if(isset($_GET['opcn'])){

   include_once('models/asignar_videos.php');
     $biblioteca_Class = new Libraries();


     if(($_GET['opcn'])=="verArch"){

     //consultas individuales de los detalles del archivo - para mostrar en biblioteca_details.php
    $archivoid = $biblioteca_Class->consultaLibrariesid($_GET['id']);
    $cargosid = $biblioteca_Class->consultaCargosid($_GET['id']);
    $concesionariosid = $biblioteca_Class->consultaDealersid($_GET['id']);
    $archivos = $biblioteca_Class->consultaLibraries('');
   
  }
}
unset($biblioteca_Class);