<?php

// print_r($_POST);
// return;
if(isset($_POST['opcn'])){

	switch ( $_POST['opcn'] ) {
		//=====================================================================
		// Crea un nuevo POST
		//=====================================================================
		case 'buscar_moderador':
			include_once('../models/foros_ventas.php');
			$resultado = Foros::buscar_moderador( $_POST );
			echo json_encode( $resultado );
			break;
		case 'crear':
			include_once('../models/foros_ventas.php');
			@session_start();
			$p = $_POST;
			//$p['user_id'] = $_SESSION['idUsuario'];
			$resultado = Foros::CrearForo( $p );
			if($resultado>0){
				foreach ($_POST['cargos'] as $iID => $cargos_selec) {
					$resu = Foros::CrearCargos($resultado,$cargos_selec);
				}
				echo( json_encode( ['error'=>false,'message'=>'OK'] ) );
			}else{
				echo( json_encode( ['error'=>true,'message'=>'No se ha podido crear la comunicación'] ) );
			}

		break;
		//=====================================================================
		// Modifica un nuevo POST
		//=====================================================================
		case 'editar':
			include_once('../models/foros_ventas.php');
			@session_start();
			$p = $_POST;
			// print_r( $_POST );
			$p['user_id'] = $_SESSION['idUsuario'];
			$resultado = Foros::ActualizarForo( $p );
			if(isset($_POST['cargos'])){
				$resu = Foros::BorraCargos($_POST['conference_id']);
				foreach ($_POST['cargos'] as $iID => $cargos_selec) {
					$resu = Foros::CrearCargos($_POST['conference_id'], $cargos_selec);
				}
			}
			if( $resultado > 0 ){
				echo json_encode( ['error'=>false,'message'=>'OK'] );
			}else{
				echo json_encode( ['error'=>true,'message'=>'No se ha podido modificar el registro'] );
			}
		break;
		//=====================================================================
		// Agrega un Like a un POST
		//=====================================================================
		case 'LikeReply':
			include_once('../models/foros_ventas.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->CrearLike($_POST['conference_reply_id']);
			if($resultado>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;
		//=====================================================================
		//
		//=====================================================================
		case 'enviar':
			include_once('../models/foros_ventas.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->CrearReply($_POST['conference_id'],$_POST['comentario']);
			unset($Foros_Class);
			if($resultado>0){
				echo('{"resultado":"SI","MaxId":"'.$resultado.'"}');
			}
		break;
		//=====================================================================
		//
		//=====================================================================
		case 'ProcesoAct':
			include_once('models/foros_ventas.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->StatusPost($_POST['conference_reply_id'],$_POST['status_id']);
			if($resultado>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;
		//=====================================================================
		// Retorna los comentarios hechos para el chat
		//=====================================================================
		case 'comments':
			include_once('../models/foros_ventas.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->ConsultaMensajes($_POST['conference_id'],$_POST['MaxIdReply']);
			$mostrar = "";

			// include_once('../models/foros_ventas.php');
			// $Foros_Class = new Foros();
			// $asistentes = $Foros_Class->ConsultaAsistentes($_POST['conference_id']);

			if(count($resultado)>0){

				foreach ($resultado as $key => $msg) {
					$mostrar .= '<div class="media border-bottom margin-none">
						<div class="media-body">
							<small class="label label-default" title="'.$msg['date_creation'].' - '.$msg['first_name'].' '.$msg['last_name'].'">'.$msg['first_name'].':</small> <span>'.$msg['conference_reply'].'</span><br>
						</div>
					</div>';
					$mostrar .= '<input type="hidden" class="MaxIdReply" name="MaxIdReply" id="MaxIdReply" value="'.$msg['conference_reply_id'].'">';

				}
			}
			echo( json_encode( [ "mensajes"=>$mostrar] ));
		break;
		//=====================================================================
		// Retorna el numero de asistentes por cada conferencia
		//=====================================================================
		case 'assist':
			include_once('../models/foros_ventas.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->ConsultaAsistentes($_POST['conference_id']);
			echo( json_encode( [ 'asistentes' => $resultado ] ) );
		break;
		//=====================================================================
		// Suma el numero de asistentes enviados a la cantidad almacenada por conexion
		//=====================================================================
		case 'suma':
			include_once('../models/foros_ventas.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->AgregarAsistentes($_POST['conference_id'], $_POST['numEnSala']);
			echo( $resultado );
		break;
		//=====================================================================
		// Carga las imagenes en la para mostrar a los participantes
		//=====================================================================
		case 'imagenes':
			include_once( '../src/funciones_globales.php' );
			include_once( '../models/foros_ventas.php' );
			$cuenta = count( $_FILES );
			$p['conference_id'] = $_POST['conference_id'];
			$p['tipo_cargue'] = $_POST['tipo_cargue'];

			$pre = Foros::getFotos( $p, "../" );

			if( count( $pre ) > 0 && isset($p['tipo_cargue']) && $p['tipo_cargue'] == "nuevas" ){
				$eliminados = Foros::eliminarFoto( $p, '../' );
			}

			for ($i=0; $i < $cuenta; $i++) {
				$aux = $i + 1;
				$p['idx'] = $aux;
				sleep(1);
				$p['imagen'] = Funciones::guardarImagenes( "imagen$aux", "F".$aux."_", '../../assets/FilesForos/' );
				$subida = Foros::nuevaFoto( $p, "../" );
			}
			$imagenes = Foros::getFotos( $p, "../" );

			echo( json_encode( ['error'=>false,"message"=>"OK","imagenes"=>$imagenes] ) );
		break;
		//=====================================================================
		// Retorna la imagen actual a todos los conectados al Foro
		//=====================================================================
		case 'consultarImagenes':
			include_once( '../models/foros_ventas.php' );
			$p = $_POST;
			$imagenes = Foros::getFotos( $p, "../" );
			$estado = Foros::get_estado_foro( $p );
			$response['error']		=	false;
			$response['message']	=	'OK';
			$response['imagenes']	=	$imagenes;
			$response['estado']		=	$estado;

			 //echo( json_encode( ['error'=>false, "message"=>"OK", "imagenes"=>$imagenes] ) );
			 echo json_encode( $response );
		break;
		//=====================================================================
		//
		//=====================================================================
		case 'consultarAsistencia':
			include_once( '../models/foros_ventas.php' );
			$p = $_POST;
			$imagenes = Foros::getFotos( $p, "../" );
			echo( json_encode( ['error'=>false,"message"=>"OK","imagenes"=>$imagenes] ) );
		break;
		//=====================================================================
		// Modifica la imagen que se esta presentando actualmente ( durante la transmision )
		//=====================================================================
		case 'cambiarImagenIdx':
			include_once( '../models/foros_ventas.php' );
			$p = $_POST;
			$editar = Foros::unselectImagen( $p, "../" );
			$editar = Foros::seleccionarImagen( $p, "../" );
			if( $editar > 0 ){
				echo( json_encode( ['error'=>false,"message"=>"OK"] ) );
			}else{
				echo( json_encode( ['error'=>true,"message"=>"No se ha podido modificar la imagen"] ) );
			}
		break;
		case 'buscar_cargos_conferencias':
			include_once( '../models/foros_ventas.php' );
			$editar = Foros::buscar_cargos_conferencias( $_POST );
			echo json_encode($editar);
		break;
		case 'finalizar_trasmision':
			include_once( '../models/foros_ventas.php' );
			$response = Foros::finalizar_trasmision( $_POST );

			echo json_encode($response);
		break;


	}//fin switch

}else if( isset($_GET['opcn']) ){
	switch ( $_GET['opcn'] ) {

		case 'listado':
			include_once('../models/foros_ventas.php');
			include_once('../models/usuarios.php');
			@session_start();
			$rol = $_SESSION['max_rol'] >= 5 ? "admin" : "user";
			if(!isset($usuario_Class)){
				$usuario_Class = new Usuarios();
			}
			$p = $_GET;
			$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario'], "../");
			$p['charge_id'] = "0";
			foreach ($charge_user as $key => $Data_Charges) {
				$p['charge_id'] .= ",".$Data_Charges['charge_id'];
			}
			$Foros_datos = Foros::consultaForos( $p );
			unset($usuario_Class);
			echo( json_encode( [ "error" => false, "message" => "OK", "data" => $Foros_datos, "rol" => $rol ] ) );
		break;

		case 'cargos':
			include_once('../models/foros_ventas.php');
			$cargos = Foros::getCargos();
			echo( json_encode( [ "error" => false, "message" => "OK", "cargos"=> $cargos ] ) );
		break;
	}

}else if( isset( $_GET['id'] ) ){
	$p['conference_id'] = $_GET['id'];
	include_once('models/foros_ventas.php');
	$Foros_Class = new Foros();
	$Foro_datos = $Foros_Class->consultaForo($_GET['id']);
	$Foros_Reply = $Foros_Class->consultaReply($_GET['id']);
	$Foros_Class->AsistenciaForo($_GET['id']);
	include_once('models/usuarios.php');
	if(!isset($usuario_Class)){
		$usuario_Class = new Usuarios();
	}
	$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
	$diaposivitas = Foros::getFotos( $p );

	unset($usuario_Class);
}
