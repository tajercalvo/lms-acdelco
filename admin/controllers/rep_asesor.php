<?php
include_once('models/rep_asesor.php');
$RepAsesores_Class = new RepAsesoresClass();
$cantidad_datos = 0;
$cargos = $RepAsesores_Class->getCargos();
$trayectoria = "";
if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){
	$trayectoria_sel = isset( $_POST['trayectoria'] ) ? $_POST['trayectoria'] : [] ;
	$secciones = "'argumentarios.php','asesor_estrategico.php','eventos.php','foros_tutor.php','fotografias.php','fotografias_detail.php','noticias.php','novedades.php','videos.php','videos_detail.php','marca_chevrolet.php','envivo.php'";
	// print_r( $_POST );
	if( isset( $_POST['trayectoria'] ) && is_array( $_POST['trayectoria'] ) ){
		if( count( $_POST['trayectoria'] ) > 1 ){
			$trayectoria =  implode( ',', $_POST['trayectoria'] );
		}else if( count( $_POST['trayectoria'] ) == 1 ){
			$trayectoria = $_POST['trayectoria'][0] ;
		}else{
			$trayectoria = '0';
		}

		$_POST['ntrayectoria'] = $trayectoria;
	}else{
		$trayectoria = '0';
		$_POST['ntrayectoria'] = $trayectoria;
	}
	$p = [
		"end_date" => $_POST['end_date_day'],
		"start_date" => $_POST['start_date_day'],
		"trayectoria" => $trayectoria,
		"secciones" => $secciones
	];
	//$UsuariosActivos = $Generales_Class->consultaUsuarios($_POST['end_date_day'],$cargos_id);
	$concesionarios = $RepAsesores_Class->consultaConcesionarios();
	$zonas = $RepAsesores_Class->consultaZonas();
	$desc_zonas = $RepAsesores_Class->consultaZonasSecciones( $p );
	$zonasCcs = $RepAsesores_Class->consultaZonasCcs();

	$categorias = $RepAsesores_Class->consultaListaCategorias();
	$argumeCaterorias = $RepAsesores_Class->consultaCategoriasArg();
	$DetalleArgumentario = $RepAsesores_Class->consultaDetalleArgumentario(  $p  );

	$SeccionesDatos = $RepAsesores_Class->consultaUsuarios( $p );
	$SeccionesDatosCcs = $RepAsesores_Class->consultaUsuariosCcs( $p );
	$ArgumentarioDatos = $RepAsesores_Class->consultaModelosArgumentario( $p );
	$NovedadesDatos = $RepAsesores_Class->consultaNovedades( $p );
	$NoticiasDatos = $RepAsesores_Class->consultaNoticias( $p );
	$ForosDatos = $RepAsesores_Class->consultaForos( $p );
	$EventosDatos = $RepAsesores_Class->consultaEventos( $p );
	$EventosGaleriasF = $RepAsesores_Class->consultaGaleriasF( $p );
	$EventosGaleriasV = $RepAsesores_Class->consultaGaleriasV( $p );
	$DetallesGaleriasF = $RepAsesores_Class->consultaDetGaleriasF( $p );
	$DetallesGaleriasV = $RepAsesores_Class->consultaDetGaleriasV( $p );

	// seccion de audios biblioteca
	$datosAudios		= $RepAsesores_Class->consultaAudios( $p );
	// $datosAudiosViews	= $RepAsesores_Class->consultaAudiosViews( $p );
	// $datosAudiosLikes	= $RepAsesores_Class->consultaAudiosLikes( $p );

	$cantidad_concesionarios 	= count($concesionarios);
	$cantidad_zonas 			= count($zonas);
	$cantidad_zonasCcs 			= count($zonasCcs);
	$cantidad_listaArgu			= count( $categorias );
	$cantidad_zonasSecciones	= count( $desc_zonas );
	$cantidad_datosCcs 			= count($SeccionesDatosCcs);
	$cantidad_datos 			= count($SeccionesDatos);
	$cantidad_argumentario 		= count($ArgumentarioDatos);
	$cantidad_novedad 			= count($NovedadesDatos);
	$cantidad_noticia 			= count($NoticiasDatos);
	$cantidad_foro 				= count($ForosDatos);
	$cantidad_evento 			= count($EventosDatos);
	$cantidad_galeriaF 			= count($EventosGaleriasF);
	$cantidad_galeriaV 			= count($EventosGaleriasV);

	$secc_list = ['argumentarios.php'=>'Productos Chevrolet','asesor_estrategico.php'=>'Asesor Estratégico','eventos.php'=>'Eventos','foros_tutor.php'=>'Foros','fotografias.php'=>'Fotografías','fotografias_detail.php'=>'Foto Detalle',
		'noticias.php'=>'Noticias','novedades.php'=>'Novedades','videos.php'=>'Videos','videos_detail.php'=>'Video Detalle','marca_chevrolet.php'=>'Marca Chevrolet','ranking_lideres.php'=>'Ranking Líderes','envivo.php'=>'Transmisión en Vivo'];

	for ($i=0; $i < $cantidad_concesionarios ; $i++) {
		$concesionarios[$i]['cantidadVisitas'] = 0;
		$concesionarios[$i]['tiempoTotal'] = 0;
		$concesionarios[$i]['tiempoPromedio'] = 0;
		$concesionarios[$i]['cantidadUsuarios'] = 0;
		for ($j=0; $j < $cantidad_datosCcs ; $j++) {
			if( $concesionarios[$i]['dealer'] == $SeccionesDatosCcs[$j]['dealer'] ){
				$concesionarios[$i]['cantidadVisitas'] 	+= isset( $SeccionesDatosCcs[$j]['cantidadVisitas'] ) ? $SeccionesDatosCcs[$j]['cantidadVisitas'] : 0;
				$concesionarios[$i]['tiempoTotal'] 		+= isset( $SeccionesDatosCcs[$j]['tiempoTotal'] ) ? $SeccionesDatosCcs[$j]['tiempoTotal'] : 0;
				$concesionarios[$i]['tiempoPromedio'] 	+= isset( $SeccionesDatosCcs[$j]['tiempoPromedio'] ) ? $SeccionesDatosCcs[$j]['tiempoPromedio'] : 0;
				$concesionarios[$i]['cantidadUsuarios'] += isset( $SeccionesDatosCcs[$j]['cantidadUsuarios'] ) ? $SeccionesDatosCcs[$j]['cantidadUsuarios'] : 0;
			}
		}
	}

	for ($k=0; $k < $cantidad_datosCcs ; $k++) {
		for ($l=0; $l < $cantidad_zonasCcs ; $l++) {
			if ($SeccionesDatosCcs[$k]['dealer'] == $zonasCcs[$l]['dealer']) {
				$SeccionesDatosCcs[$k]['zone'] = $zonasCcs[$l]['zone'];
			}
		}
	}

	for ($i=0; $i < $cantidad_zonas ; $i++) {
		$zonas[$i]['cantidadVisitas'] = 0;
		$zonas[$i]['tiempoTotal'] = 0;
		$zonas[$i]['tiempoPromedio'] = 0;
		$zonas[$i]['cantidadUsuarios'] = 0;
		for ($j=0; $j < $cantidad_datosCcs ; $j++) {
			if( $zonas[$i]['zone'] == $SeccionesDatosCcs[$j]['zone'] ){
				$zonas[$i]['cantidadVisitas'] 	+= isset( $SeccionesDatosCcs[$j]['cantidadVisitas'] ) ? $SeccionesDatosCcs[$j]['cantidadVisitas'] : 0;
				$zonas[$i]['tiempoTotal'] 		+= isset( $SeccionesDatosCcs[$j]['tiempoTotal'] ) ? $SeccionesDatosCcs[$j]['tiempoTotal'] : 0;
				$zonas[$i]['tiempoPromedio'] 	+= isset( $SeccionesDatosCcs[$j]['tiempoPromedio'] ) ? $SeccionesDatosCcs[$j]['tiempoPromedio'] : 0;
				$zonas[$i]['cantidadUsuarios'] 	+= isset( $SeccionesDatosCcs[$j]['cantidadUsuarios'] ) ? $SeccionesDatosCcs[$j]['cantidadUsuarios'] : 0;
			}
		}
	}

	for ($i=0; $i < $cantidad_zonasSecciones ; $i++) {
		foreach ($SeccionesDatosCcs as $keySec => $valueSec) {

			if( $desc_zonas[$i]['zone'] == $valueSec['zone'] && $desc_zonas[$i]['section'] == $valueSec['section'] ) {
				$desc_zonas[$i]['cantidadVisitas'] 	+= $valueSec['cantidadVisitas'];
				$desc_zonas[$i]['tiempoTotal'] 		+= $valueSec['tiempoTotal'];
				$desc_zonas[$i]['tiempoPromedio'] 	+= $valueSec['tiempoPromedio'];
				$desc_zonas[$i]['cantidadUsuarios'] += $valueSec['cantidadUsuarios'];
			}
		}
	}

	for ($j=0; $j < count( $DetalleArgumentario ) ; $j++) {
		$DetalleArgumentario[$j]['argument'] = "";
		$DetalleArgumentario[$j]['image'] = "";
		$DetalleArgumentario[$j]['argument_cat'] = "";
		foreach ($argumeCaterorias as $key => $value) {
			if( $value['argument_id'] == $DetalleArgumentario[$j]['element_id'] ){

				$DetalleArgumentario[$j]['argument'] = $value['argument'];
				$DetalleArgumentario[$j]['image'] = $value['image'];
				$DetalleArgumentario[$j]['argument_cat'] = $value['argument_cat'];

			}else if( $DetalleArgumentario[$j]['element_id'] == 0 ){

				$DetalleArgumentario[$j]['argument'] = "Sección";
				$DetalleArgumentario[$j]['image'] = "";
				$DetalleArgumentario[$j]['argument_cat'] = "Galeria";

			}
		}
	}

		// print_r( $DetalleArgumentario );

	for ($i=0; $i < $cantidad_listaArgu ; $i++) {
		foreach ($DetalleArgumentario as $key => $value) {
			if( $value['argument_cat'] == $categorias[$i]['argument_cat'] ){
				$categorias[$i]['cantidadVisitas'] 	+= $value['cantidadVisitas'];
				$categorias[$i]['tiempoTotal'] 		+= $value['tiempoTotal'];
				$categorias[$i]['tiempoPromedio'] 	+= $value['tiempoPromedio'];
				$categorias[$i]['cantidadUsuarios'] += $value['cantidadUsuarios'];
			}
		}
	}

	// print_r( $categorias );

}

if( isset($_GET['end_date_day']) && isset($_GET['start_date_day']) ){
	// print_r( $_GET );
	$secciones = "'argumentarios.php','asesor_estrategico.php','eventos.php','foros_tutor.php','fotografias.php','fotografias_detail.php','noticias.php','novedades.php','videos.php','videos_detail.php','marca_chevrolet.php','envivo.php'";
	// ,'club_lideres.php','ranking_lideres.php'
	// print_r( $_POST );
	$p = [
		"end_date" => $_GET['end_date_day'],
		"start_date" => $_GET['start_date_day'],
		"trayectoria" => $_GET['trayectoria'],
		"secciones" => $secciones
	];

	$secc_list = ['argumentarios.php'=>'Productos Chevrolet','asesor_estrategico.php'=>'Asesor Estratégico','eventos.php'=>'Eventos','foros_tutor.php'=>'Foros','fotografias.php'=>'Fotografías','fotografias_detail.php'=>'Foto Detalle',
		'noticias.php'=>'Noticias','novedades.php'=>'Novedades','videos.php'=>'Videos','videos_detail.php'=>'Video Detalle','marca_chevrolet.php'=>'Marca Chevrolet','club_lideres.php'=>'Club Líderes','ranking_lideres.php'=>'Ranking Líderes','envivo.php'=>'Transmisión en Vivo'];

	$SeccionesDatosCcs = $RepAsesores_Class->desc_consultaUsuariosCcs( $p );
	$datosAudios = $RepAsesores_Class->consultaDescAudios( $p );
	$cantidad_datos = count( $SeccionesDatosCcs );

}

unset($RepAsesores_Class);
