<?php

if( isset( $_POST['opcn'] ) ){
    include_once('../models/rep_facturacion.php');
    switch ( $_POST['opcn']  ) {
        
        case 'sede':
            include_once('../src/funciones_globales.php');
            $sedes = Funciones::getSedes( $_POST['concesionario'], '../' );
            print_r( $sedes );
        break;

        case 'consulta':
            // print_r( $_POST );
            // echo("<br>");
            $cursos = RepFacturacion::consultaDatosCursos( $_POST, '../' );
            $n_cursos = count( $cursos );
            $datos = RepFacturacion::consultaDetalle( $_POST, '../' );
            $n_datos = count( $datos );

            foreach ($datos as $key => $value) {
                for ($i=0; $i < $n_cursos ; $i++) { 
                    if( $cursos[$i]['schedule_id'] == $value['schedule_id']  ){//incremente cada iteracion que pertenezca al curso
                        $cursos[$i]['Invitados']++;
                        $cursos[$i]['Capacitacion'] += $value['duration_time'];
                        if( $value['asistencia'] == 2 ){//incrementa el indice si el estudiante se inscribio al curso
                            $cursos[$i]['Asistentes']++;
                        }
                        break;
                    }
                }//fin for -> schedules[]
            }//fin foreach -> Listado notas usuarios

            //Se genera los datos que se retornaran para las graficas JavaScript
            $datos_graficas = [
                "cursos" => [ "blandas" => 0, "tecnicas" => 0, "marca" => 0, "producto" => 0 ],
                "horas" => [ "blandas" => 0, "tecnicas" => 0, "marca" => 0, "producto" => 0 ],
                "asistentes" => [ "blandas" => 0, "tecnicas" => 0, "marca" => 0, "producto" => 0 ],
                "capacitacion" => [ "blandas" => 0, "tecnicas" => 0, "marca" => 0, "producto" => 0 ],
            ];
            foreach ($cursos as $key => $val) {
                switch( $val['specialty_id'] ){
                    case '1'://Tecnicas
                        $datos_graficas['cursos']['tecnicas']++;
                        $datos_graficas['horas']['tecnicas'] += $val['duration_time'];
                        $datos_graficas['asistentes']['tecnicas']+= $val['Asistentes'];
                        $datos_graficas['capacitacion']['tecnicas'] += $val['Capacitacion'];
                    break;
                    case '2'://Blandas
                        $datos_graficas['cursos']['blandas']++;
                        $datos_graficas['horas']['blandas'] += $val['duration_time'];
                        $datos_graficas['asistentes']['blandas']+= $val['Asistentes'];
                        $datos_graficas['capacitacion']['blandas'] += $val['Capacitacion'];
                    break;
                    case '3'://Producto
                        $datos_graficas['cursos']['producto']++;
                        $datos_graficas['horas']['producto'] += $val['duration_time'];
                        $datos_graficas['asistentes']['producto']+= $val['Asistentes'];
                        $datos_graficas['capacitacion']['producto'] += $val['Capacitacion'];  
                    break;
                    case '4'://Marca
                        $datos_graficas['cursos']['marca']++;
                        $datos_graficas['horas']['marca'] += $val['duration_time'];
                        $datos_graficas['asistentes']['marca']+= $val['Asistentes'];
                        $datos_graficas['capacitacion']['marca'] += $val['Capacitacion'];
                    break;
                }//fin switch
            } 


            //generar los datos para las tablas
            $tipos = [ "IBT","WBT","VCT","OJT" ];
            $datos_tablas = [ 
                "IBT" => [ "titulo" => "IBT", "data" => [] ], 
                "WBT" => [ "titulo" => "WBT", "data" => [] ], 
                "VCT" => [ "titulo" => "VCT", "data" => [] ], 
                "OJT" => [ "titulo" => "OJT", "data" => [] ] 
            ];
            //recorre los tipos de cursos
            foreach ( $tipos as $key => $vt ) {

                $resumen = [
                    "blandas"   => [ "cursos" => 0, "horas" => 0, "asistencia" => 0, "capacitacion" => 0, "especialidad" => "HABILIDADES BLANDAS"],
                    "tecnicas"  => [ "cursos" => 0, "horas" => 0, "asistencia" => 0, "capacitacion" => 0, "especialidad" => "HABILIDADES TECNICAS"],
                    "marca"     => [ "cursos" => 0, "horas" => 0, "asistencia" => 0, "capacitacion" => 0, "especialidad" => "PROCESOS DE MARCA"],
                    "producto"  => [ "cursos" => 0, "horas" => 0, "asistencia" => 0, "capacitacion" => 0, "especialidad" => "PRODUCTOS"],
                    "total"     => [ "cursos" => 0, "horas" => 0, "asistencia" => 0, "capacitacion" => 0, "especialidad" => "TOTAL"]
                ];
                //itera cada uno de los cursos realizados entre las fechas seleccionadas
                foreach ( $cursos as $key2 => $value ) {
                    if( $value['type'] == $vt ){//selecciona el tipo de curso
                        switch( $value['specialty_id'] ){
                            case '1'://Tecnicas
                                $resumen['tecnicas']['cursos']++;
                                $resumen['tecnicas']['horas'] += $value['duration_time'];
                                $resumen['tecnicas']['asistencia'] += $value['Asistentes'];
                                $resumen['tecnicas']['capacitacion'] += $value['Capacitacion'];
                            break;
                            case '2'://Blandas
                                $resumen['blandas']['cursos']++;
                                $resumen['blandas']['horas'] += $value['duration_time'];
                                $resumen['blandas']['asistencia'] += $value['Asistentes'];
                                $resumen['blandas']['capacitacion'] += $value['Capacitacion'];
                            break;
                            case '3'://Producto
                                $resumen['producto']['cursos']++;
                                $resumen['producto']['horas'] += $value['duration_time'];
                                $resumen['producto']['asistencia'] += $value['Asistentes'];
                                $resumen['producto']['capacitacion'] += $value['Capacitacion'];
                            break;
                            case '4'://Marca
                                $resumen['marca']['cursos']++;
                                $resumen['marca']['horas'] += $value['duration_time'];
                                $resumen['marca']['asistencia'] += $value['Asistentes'];
                                $resumen['marca']['capacitacion'] += $value['Capacitacion'];
                            break;
                        }//fin switch
                        $resumen['total']['cursos']++;
                        $resumen['total']['horas'] += $value['duration_time'];
                        $resumen['total']['asistencia'] += $value['Asistentes'];
                        $resumen['total']['capacitacion'] += $value['Capacitacion'];
                    }//fin if ['type']
                }//fin foreach
                $datos_tablas[$vt]['data'] = $resumen;
            }
            $resultado = [];

            foreach ( $datos_tablas as $key => $dato ) {
                $html = "<table class='footable table table-striped table-primary default footable-loaded'>
                            <caption>{$dato['titulo']}</caption>
                            <thead>
                                <tr>
                                    <th>ESPECIALIDAD</th>
                                    <th>CURSOS</th>
                                    <th>HORAS</th>
                                    <th>ASISTENTES</th>
                                    <th>CAPACITACIONES</th>
                                </tr>
                            </thead>";
                foreach( $dato['data'] as $key => $d ){
                    $html .= "<tr>";
                    $html .= "<td><strong>".$d['especialidad']."</strong></td>";
                    $html .= "<td class='text-center'>".$d['cursos']."</td>";
                    $html .= "<td class='text-center'>".$d['horas']."</td>";
                    $html .= "<td class='text-center'>".$d['asistencia']."</td>";
                    $html .= "<td class='text-center'>".$d['capacitacion']."</td>";
                    $html .= "</tr>";
                }
                $html .= "</table><br>";
                $resultado[ $dato['titulo'] ] = $html;
            }
            $resultado['datos_tablas'] = $datos_graficas;
            echo( json_encode( $resultado ) );
        break;
    }
}else{
    include_once('models/rep_facturacion.php');
    include_once('src/funciones_globales.php');
    
    $zonas = Funciones::getZonas();
    $concesionarios = Funciones::getConcesionarios('');
    $cargos = Funciones::getCargos();
}