<?php

if( isset( $_GET['opcn'] ) ){
	include_once( '../models/rep_evaluaciones_incompletas.php' );
	switch ( $_GET['opcn'] ) {
		case 'listado':
			$datosCursos_all = RepAsistencias::consultaDatosAll( $_GET['review_id'] );
			echo json_encode( [ 'error' => false, 'message' => "OK", 'registros' => $datosCursos_all  ] );
		break;
		case 'listado_r':
			$datosCursos_all = RepAsistencias::consultaDatosAllR( $_GET['review_id'] );
			echo json_encode( [ 'error' => false, 'message' => "OK", 'registros' => $datosCursos_all  ] );
		break;
	}

}else if( isset( $_POST['opcn'] ) ){
	include_once( '../models/rep_evaluaciones_incompletas.php' );
	switch ( $_POST['opcn'] ) {

		case 'eliminar':
			$resultado = RepAsistencias::eliminar_Respuesta( $_POST['reviews_answer_id'] );
			if( $resultado > 0 ){
				$res = [ 'error' => false, 'eliminado' => "SI" ];
			}else{
				$res = [ 'error' => false, 'eliminado' => "NO" ];
			}
			echo json_encode( $res );
		break;
		case 'eliminar_r':
			$resultado = RepAsistencias::eliminar_RespuestaR( $_POST['reviews_answer_id'] );
			if( $resultado > 0 ){
				$res = [ 'error' => false, 'eliminado' => "SI" ];
			}else{
				$res = [ 'error' => false, 'eliminado' => "NO" ];
			}
			echo json_encode( $res );
		break;

	}

}else{

	include_once('models/rep_evaluaciones_incompletas.php');
	$datosCursos = RepAsistencias::consultaDatos('');
	$datosCursos_r = RepAsistencias::consultaDatosR('');

}
