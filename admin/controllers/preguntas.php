<?php
if(isset($_GET['opcn'])){
	include_once('models/preguntas.php');
	$Preguntas_Class = new Preguntas();
	$numero_resp = 0;
	if(isset($_GET['id'])){
		$numero_resp = Preguntas::respuestasPregunta( $_GET['id'], '' );
		//consultas individuales
        $answer1 = "";
        $answer2 = "";
        $answer3 = "";
        $answer4 = "";
        $answer5 = "";
        $answer6 = "";
        $answer7 = "";
        $answer8 = "";
        $answer9 = "";
        $answer10 = "";
        $RespSel = "0";
        $img_answer1 = "";
        $img_answer2 = "";
        $img_answer3 = "";
        $img_answer4 = "";
        $img_answer5 = "";
        $img_answer6 = "";
        $img_answer7 = "";
        $img_answer8 = "";
        $img_answer9 = "";
        $img_answer10 = "";

		$registroConfiguracion = $Preguntas_Class->consultaRegistro($_GET['id']);
        $RespuestasEvaluacion = $Preguntas_Class->consultaRespuestas($_GET['id']);
        $ctrl = 1;
        $RespSel = "1";
        foreach ($RespuestasEvaluacion as $iID => $aInfo) {
            if($ctrl==1){
                $answer1 = $aInfo['answer'];
                $img_answer1 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "1";
                }
            }elseif($ctrl==2){
                $answer2 = $aInfo['answer'];
                $img_answer2 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "2";
                }
            }elseif($ctrl==3){
                $answer3 = $aInfo['answer'];
                $img_answer3 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "3";
                }
            }elseif($ctrl==4){
                $answer4 = $aInfo['answer'];
                $img_answer4 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "4";
                }
            }elseif($ctrl==5){
                $answer5 = $aInfo['answer'];
                $img_answer5 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "5";
                }
            }elseif($ctrl==6){
                $answer6 = $aInfo['answer'];
                $img_answer6 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "6";
                }
            }elseif($ctrl==7){
                $answer7 = $aInfo['answer'];
                $img_answer7 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "7";
                }
            }elseif($ctrl==8){
                $answer8 = $aInfo['answer'];
                $img_answer8 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "8";
                }
            }elseif($ctrl==9){
                $answer9 = $aInfo['answer'];
                $img_answer9 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "9";
                }
            }elseif($ctrl==10){
                $answer10 = $aInfo['answer'];
                $img_answer10 = $aInfo['source'];
                if($aInfo['result']=="SI"){
                    $RespSel = "10";
                }
            }
            $ctrl++;
        }
		//$listadosCursos = $Preguntas_Class->consultaCursos('');
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		//$listadosCursos = $Preguntas_Class->consultaCursos('');
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/preguntas.php');
	$Preguntas_Class = new Preguntas();
        //Proceso de creación para la imagen
            $imgant = "";
            if(isset($_POST['imgant'])){
                $imgant = $_POST['imgant'];
            }
            if(isset($_FILES['image_new']) && $_FILES['image_new']['tmp_name'] != ""){
                if(isset($_FILES['image_new']['tmp_name'])){

                    $nom_archivo1 = $_FILES["image_new"]["name"];
                    $new_name1 = "ACDelco_LUDUS_PR";
                    $new_name1 .= date("Ymdhis");
                    $new_extension1 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
                    switch (strtolower($ext1[2])) {
                        case 'jpeg' : $new_extension1 = ".jpg";
                            break;
                        case 'jpg' : $new_extension1 = ".jpg";
                            break;
                        case 'JPG' : $new_extension1 = ".jpg";
                            break;
                        case 'JPEG' : $new_extension1 = ".jpg";
                            break;
                        default    : $new_extension1 = "no";
                            break;
                    }
                    if($new_extension1 != "no"){
                        $new_name1 .= $new_extension1;
                        $resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../../assets/gallery/source/".$new_name1);
                            if($resultArchivo1==1){
                                if($imgant != "" && $imgant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$imgant)){
                                        unlink("../../assets/gallery/source/".$imgant);
                                    }
                                }
                            }else{
                                $new_name1 = $imgant;
                            }
                    }else{
                        $new_name1 = $imgant;
                    }
                }else{
                    $new_name1 = $imgant;
                }
            }else{
                $new_name1 = $imgant;
            }
        //Proceso de creación para la imagen
        //Proceso de creación para la imagen Respuesta 1
            $ImgResp1_ant = "";
            if(isset($_POST['ImgResp1_ant'])){
                $ImgResp1_ant = $_POST['ImgResp1_ant'];
            }
            if(isset($_FILES['ImgResp1_image_new']) && $_FILES['ImgResp1_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp1_image_new']['tmp_name'])){
                    $nom_archivo1 = $_FILES["ImgResp1_image_new"]["name"];
                    $Resp_new_name1 = "ACDelco_LUDUS_PR";
                    $Resp_new_name1 .= date("Ymdhis");
                    $Resp_new_extension1 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $resp_ext1);
                    switch (strtolower($resp_ext1[2])) {
                        case 'jpeg' : $Resp_new_extension1 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension1 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension1 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension1 = ".jpg";
                            break;
                        default    : $Resp_new_extension1 = "no";
                            break;
                    }
                    if($Resp_new_extension1 != "no"){
                        $Resp_new_name1 .= $Resp_new_extension1;
                        $resultArchivo1 = copy($_FILES["ImgResp1_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name1);
                            if($resultArchivo1==1){
                                if($ImgResp1_ant != "" && $ImgResp1_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp1_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp1_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name1 = $ImgResp1_ant;
                            }
                    }else{
                        $Resp_new_name1 = $ImgResp1_ant;
                    }
                }else{
                    $Resp_new_name1 = $ImgResp1_ant;
                }
            }else{
                $Resp_new_name1 = $ImgResp1_ant;
            }
        //Proceso de creación para la imagen Respuesta 1
        //Proceso de creación para la imagen Respuesta 2
            $ImgResp2_ant = "";
            if(isset($_POST['ImgResp2_ant'])){
                $ImgResp2_ant = $_POST['ImgResp2_ant'];
            }
            if(isset($_FILES['ImgResp2_image_new']) && $_FILES['ImgResp2_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp2_image_new']['tmp_name'])){

                    $nom_archivo2 = $_FILES["ImgResp2_image_new"]["name"];
                    $Resp_new_name2 = "ACDelco_LUDUS_PR";
                    $Resp_new_name2 .= date("Ymdhis");
                    $Resp_new_extension2 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo2, $resp_ext2);
                    switch (strtolower($resp_ext2[2])) {
                        case 'jpeg' : $Resp_new_extension2 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension2 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension2 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension2 = ".jpg";
                            break;
                        default    : $Resp_new_extension2 = "no";
                            break;
                    }
                    if($Resp_new_extension2 != "no"){
                        $Resp_new_name2 .= $Resp_new_extension2;
                        $resultArchivo2 = copy($_FILES["ImgResp2_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name2);
                            if($resultArchivo2==1){
                                if($ImgResp2_ant != "" && $ImgResp2_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp2_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp2_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name2 = $ImgResp2_ant;
                            }
                    }else{
                        $Resp_new_name2 = $ImgResp2_ant;
                    }
                }else{
                    $Resp_new_name2 = $ImgResp2_ant;
                }
            }else{
                $Resp_new_name2 = $ImgResp2_ant;
            }
        //Proceso de creación para la imagen Respuesta 2
        //Proceso de creación para la imagen Respuesta 3
            $ImgResp3_ant = "";
            if(isset($_POST['ImgResp3_ant'])){
                $ImgResp3_ant = $_POST['ImgResp3_ant'];
            }
            if(isset($_FILES['ImgResp3_image_new']) && $_FILES['ImgResp3_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp3_image_new']['tmp_name'])){
                    $nom_archivo3 = $_FILES["ImgResp3_image_new"]["name"];
                    $Resp_new_name3 = "ACDelco_LUDUS_PR";
                    $Resp_new_name3 .= date("Ymdhis");
                    $Resp_new_extension3 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo3, $resp_ext3);
                    switch (strtolower($resp_ext3[2])) {
                        case 'jpeg' : $Resp_new_extension3 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension3 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension3 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension3 = ".jpg";
                            break;
                        default    : $Resp_new_extension3 = "no";
                            break;
                    }
                    if($Resp_new_extension3 != "no"){
                        $Resp_new_name3 .= $Resp_new_extension3;
                        $resultArchivo3 = copy($_FILES["ImgResp3_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name3);
                            if($resultArchivo3==1){
                                if($ImgResp3_ant != "" && $ImgResp3_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp3_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp3_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name3 = $ImgResp3_ant;
                            }
                    }else{
                        $Resp_new_name3 = $ImgResp3_ant;
                    }
                }else{
                    $Resp_new_name3 = $ImgResp3_ant;
                }
            }else{
                $Resp_new_name3 = $ImgResp3_ant;
            }
        //Proceso de creación para la imagen Respuesta 3
        //Proceso de creación para la imagen Respuesta 4
            $ImgResp4_ant = "";
            if(isset($_POST['ImgResp4_ant'])){
                $ImgResp4_ant = $_POST['ImgResp4_ant'];
            }
            if(isset($_FILES['ImgResp4_image_new']) && $_FILES['ImgResp4_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp4_image_new']['tmp_name'])){

                    $nom_archivo4 = $_FILES["ImgResp4_image_new"]["name"];
                    $Resp_new_name4 = "ACDelco_LUDUS_PR";
                    $Resp_new_name4 .= date("Ymdhis");
                    $Resp_new_extension4 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo4, $resp_ext4);
                    switch (strtolower($resp_ext4[2])) {
                        case 'jpeg' : $Resp_new_extension4 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension4 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension4 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension4 = ".jpg";
                            break;
                        default    : $Resp_new_extension4 = "no";
                            break;
                    }
                    if($Resp_new_extension4 != "no"){
                        $Resp_new_name4 .= $Resp_new_extension4;
                        $resultArchivo4 = copy($_FILES["ImgResp4_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name4);
                            if($resultArchivo4==1){
                                if($ImgResp4_ant != "" && $ImgResp4_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp4_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp4_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name4 = $ImgResp4_ant;
                            }
                    }else{
                        $Resp_new_name4 = $ImgResp4_ant;
                    }
                }else{
                    $Resp_new_name4 = $ImgResp4_ant;
                }
            }else{
                $Resp_new_name4 = $ImgResp4_ant;
            }
        //Proceso de creación para la imagen Respuesta 4
        //Proceso de creación para la imagen Respuesta 5
            $ImgResp5_ant = "";
            if(isset($_POST['ImgResp5_ant'])){
                $ImgResp5_ant = $_POST['ImgResp5_ant'];
            }
            if(isset($_FILES['ImgResp5_image_new']) && $_FILES['ImgResp5_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp5_image_new']['tmp_name'])){

                    $nom_archivo5 = $_FILES["ImgResp5_image_new"]["name"];
                    $Resp_new_name5 = "ACDelco_LUDUS_PR";
                    $Resp_new_name5 .= date("Ymdhis");
                    $Resp_new_extension5 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo5, $resp_ext5);
                    switch (strtolower($resp_ext5[2])) {
                        case 'jpeg' : $Resp_new_extension5 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension5 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension5 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension5 = ".jpg";
                            break;
                        default    : $Resp_new_extension5 = "no";
                            break;
                    }
                    if($Resp_new_extension5 != "no"){
                        $Resp_new_name5 .= $Resp_new_extension5;
                        $resultArchivo5 = copy($_FILES["ImgResp5_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name5);
                            if($resultArchivo5==1){
                                if($ImgResp5_ant != "" && $ImgResp5_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp5_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp5_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name5 = $ImgResp5_ant;
                            }
                    }else{
                        $Resp_new_name5 = $ImgResp5_ant;
                    }
                }else{
                    $Resp_new_name5 = $ImgResp5_ant;
                }
            }else{
                $Resp_new_name5 = $ImgResp5_ant;
            }
        //Proceso de creación para la imagen Respuesta 5
        //Proceso de creación para la imagen Respuesta 6
            $ImgResp6_ant = "";
            if(isset($_POST['ImgResp6_ant'])){
                $ImgResp6_ant = $_POST['ImgResp6_ant'];
            }
            if(isset($_FILES['ImgResp6_image_new']) && $_FILES['ImgResp6_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp6_image_new']['tmp_name'])){

                    $nom_archivo6 = $_FILES["ImgResp6_image_new"]["name"];
                    $Resp_new_name6 = "ACDelco_LUDUS_PR";
                    $Resp_new_name6 .= date("Ymdhis");
                    $Resp_new_extension6 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo6, $resp_ext6);
                    switch (strtolower($resp_ext6[2])) {
                        case 'jpeg' : $Resp_new_extension6 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension6 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension6 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension6 = ".jpg";
                            break;
                        default    : $Resp_new_extension6 = "no";
                            break;
                    }
                    if($Resp_new_extension6 != "no"){
                        $Resp_new_name6 .= $Resp_new_extension6;
                        $resultArchivo6 = copy($_FILES["ImgResp6_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name6);
                            if($resultArchivo6==1){
                                if($ImgResp6_ant != "" && $ImgResp6_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp6_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp6_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name6 = $ImgResp6_ant;
                            }
                    }else{
                        $Resp_new_name6 = $ImgResp6_ant;
                    }
                }else{
                    $Resp_new_name6 = $ImgResp6_ant;
                }
            }else{
                $Resp_new_name6 = $ImgResp6_ant;
            }
        //Proceso de creación para la imagen Respuesta 6
        //Proceso de creación para la imagen Respuesta 7
            $ImgResp7_ant = "";
            if(isset($_POST['ImgResp7_ant'])){
                $ImgResp7_ant = $_POST['ImgResp7_ant'];
            }
            if(isset($_FILES['ImgResp7_image_new']) && $_FILES['ImgResp7_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp7_image_new']['tmp_name'])){

                    $nom_archivo7 = $_FILES["ImgResp7_image_new"]["name"];
                    $Resp_new_name7 = "ACDelco_LUDUS_PR";
                    $Resp_new_name7 .= date("Ymdhis");
                    $Resp_new_extension7 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo7, $resp_ext7);
                    switch (strtolower($resp_ext7[2])) {
                        case 'jpeg' : $Resp_new_extension7 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension7 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension7 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension7 = ".jpg";
                            break;
                        default    : $Resp_new_extension7 = "no";
                            break;
                    }
                    if($Resp_new_extension7 != "no"){
                        $Resp_new_name7 .= $Resp_new_extension7;
                        $resultArchivo7 = copy($_FILES["ImgResp7_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name7);
                            if($resultArchivo7==7){
                                if($ImgResp7_ant != "" && $ImgResp7_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp7_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp7_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name7 = $ImgResp7_ant;
                            }
                    }else{
                        $Resp_new_name7 = $ImgResp7_ant;
                    }
                }else{
                    $Resp_new_name7 = $ImgResp7_ant;
                }
            }else{
                $Resp_new_name7 = $ImgResp7_ant;
            }
        //Proceso de creación para la imagen Respuesta 7
        //Proceso de creación para la imagen Respuesta 8
            $ImgResp8_ant = "";
            if(isset($_POST['ImgResp8_ant'])){
                $ImgResp8_ant = $_POST['ImgResp8_ant'];
            }
            if(isset($_FILES['ImgResp8_image_new']) && $_FILES['ImgResp8_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp8_image_new']['tmp_name'])){

                    $nom_archivo8 = $_FILES["ImgResp8_image_new"]["name"];
                    $Resp_new_name8 = "ACDelco_LUDUS_PR";
                    $Resp_new_name8 .= date("Ymdhis");
                    $Resp_new_extension8 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo8, $resp_ext8);
                    switch (strtolower($resp_ext8[2])) {
                        case 'jpeg' : $Resp_new_extension8 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension8 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension8 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension8 = ".jpg";
                            break;
                        default    : $Resp_new_extension8 = "no";
                            break;
                    }
                    if($Resp_new_extension8 != "no"){
                        $Resp_new_name8 .= $Resp_new_extension8;
                        $resultArchivo8 = copy($_FILES["ImgResp8_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name8);
                            if($resultArchivo8==1){
                                if($ImgResp8_ant != "" && $ImgResp8_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp8_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp8_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name8 = $ImgResp8_ant;
                            }
                    }else{
                        $Resp_new_name8 = $ImgResp8_ant;
                    }
                }else{
                    $Resp_new_name8 = $ImgResp8_ant;
                }
            }else{
                $Resp_new_name8 = $ImgResp8_ant;
            }
        //Proceso de creación para la imagen Respuesta 8
        //Proceso de creación para la imagen Respuesta 9
            $ImgResp9_ant = "";
            if(isset($_POST['ImgResp9_ant'])){
                $ImgResp9_ant = $_POST['ImgResp9_ant'];
            }
            if(isset($_FILES['ImgResp9_image_new']) && $_FILES['ImgResp9_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp9_image_new']['tmp_name'])){

                    $nom_archivo9 = $_FILES["ImgResp9_image_new"]["name"];
                    $Resp_new_name9 = "ACDelco_LUDUS_PR";
                    $Resp_new_name9 .= date("Ymdhis");
                    $Resp_new_extension9 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo9, $resp_ext9);
                    switch (strtolower($resp_ext9[2])) {
                        case 'jpeg' : $Resp_new_extension9 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension9 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension9 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension9 = ".jpg";
                            break;
                        default    : $Resp_new_extension9 = "no";
                            break;
                    }
                    if($Resp_new_extension9 != "no"){
                        $Resp_new_name9 .= $Resp_new_extension9;
                        $resultArchivo9 = copy($_FILES["ImgResp9_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name9);
                            if($resultArchivo9==1){
                                if($ImgResp9_ant != "" && $ImgResp9_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp9_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp9_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name9 = $ImgResp9_ant;
                            }
                    }else{
                        $Resp_new_name9 = $ImgResp9_ant;
                    }
                }else{
                    $Resp_new_name9 = $ImgResp9_ant;
                }
            }else{
                $Resp_new_name9 = $ImgResp9_ant;
            }
        //Proceso de creación para la imagen Respuesta 9
        //Proceso de creación para la imagen Respuesta 10
            $ImgResp10_ant = "";
            if(isset($_POST['ImgResp10_ant'])){
                $ImgResp10_ant = $_POST['ImgResp10_ant'];
            }
            if(isset($_FILES['ImgResp10_image_new']) && $_FILES['ImgResp10_image_new']['tmp_name'] != "" ){
                if(isset($_FILES['ImgResp10_image_new']['tmp_name'])){

                    $nom_archivo10 = $_FILES["ImgResp10_image_new"]["name"];
                    $Resp_new_name10 = "ACDelco_LUDUS_PR";
                    $Resp_new_name10 .= date("Ymdhis");
                    $Resp_new_extension10 = "jpg";
                    preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo10, $resp_ext10);
                    switch (strtolower($resp_ext10[2])) {
                        case 'jpeg' : $Resp_new_extension10 = ".jpg";
                            break;
                        case 'jpg' : $Resp_new_extension10 = ".jpg";
                            break;
                        case 'JPG' : $Resp_new_extension10 = ".jpg";
                            break;
                        case 'JPEG' : $Resp_new_extension10 = ".jpg";
                            break;
                        default    : $Resp_new_extension10 = "no";
                            break;
                    }
                    if($Resp_new_extension10 != "no"){
                        $Resp_new_name10 .= $Resp_new_extension10;
                        $resultArchivo10 = copy($_FILES["ImgResp10_image_new"]["tmp_name"], "../../assets/gallery/source/".$Resp_new_name10);
                            if($resultArchivo10==1){
                                if($ImgResp10_ant != "" && $ImgResp10_ant != "default.png"){
                                    if(file_exists("../../assets/gallery/source/".$ImgResp10_ant)){
                                        unlink("../../assets/gallery/source/".$ImgResp10_ant);
                                    }
                                }
                            }else{
                                $Resp_new_name10 = $ImgResp10_ant;
                            }
                    }else{
                        $Resp_new_name10 = $ImgResp10_ant;
                    }
                }else{
                    $Resp_new_name10 = $ImgResp10_ant;
                }
            }else{
                $Resp_new_name10 = $ImgResp10_ant;
            }
        //Proceso de creación para la imagen Respuesta 10
	if($_POST['opcn']=="crear"){
        if(!isset($_POST['RespSel'])){
            $_POST['RespSel'] = "0";
        }
		$resultado = $Preguntas_Class->CrearPreguntas($_POST['question'],$_POST['code'],$_POST['type'],$_POST['type_response'],$_POST['answer1'],$_POST['answer2'],$_POST['answer3'],$_POST['answer4'],$_POST['answer5'],$_POST['answer6'],$_POST['answer7'],$_POST['answer8'],$_POST['answer9'],$_POST['answer10'],$_POST['RespSel'],$new_name1,$Resp_new_name1,$Resp_new_name2,$Resp_new_name3,$Resp_new_name4,$Resp_new_name5,$Resp_new_name6,$Resp_new_name7,$Resp_new_name8,$Resp_new_name9,$Resp_new_name10);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
        if(!isset($_POST['RespSel'])){
            $_POST['RespSel'] = "0";
        }
		$resultado = $Preguntas_Class->ActualizarPreguntas($_POST['idElemento'],$_POST['status_id'],$_POST['question'],$_POST['code'],$_POST['type'],$_POST['type_response'],$_POST['answer1'],$_POST['answer2'],$_POST['answer3'],$_POST['answer4'],$_POST['answer5'],$_POST['answer6'],$_POST['answer7'],$_POST['answer8'],$_POST['answer9'],$_POST['answer10'],$_POST['RespSel'],$new_name1,$Resp_new_name1,$Resp_new_name2,$Resp_new_name3,$Resp_new_name4,$Resp_new_name5,$Resp_new_name6,$Resp_new_name7,$Resp_new_name8,$Resp_new_name9,$Resp_new_name10);
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas Preguntas
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.code';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.code '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY c.question '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY c.type '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='3') {
    		$sOrder = ' ORDER BY s.first_name, s.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/preguntas.php');
	$Preguntas_Class = new Preguntas();
	$datosConfiguracion = $Preguntas_Class->consultaDatosPreguntas($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $Preguntas_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}

        if($aInfo['type']==1){
            $val_tipo = '<span class="label label-success">Evaluación Abierta</span>';
        }elseif($aInfo['type']==2){
            $val_tipo = '<span class="label label-inverse">Encuesta Abierta</span>';
        }elseif($aInfo['type']==3){
            $val_tipo = '<span class="label label-important">Evaluación Curso</span>';
        }else{
            $val_tipo = '<span class="label label-danger">Encuesta Curso</span>';
        }

    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
    		$valOpciones = '<a href="op_pregunta.php?opcn=editar&id='.$aInfo['question_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	}
    	$aItem = array(
			$aInfo['code'],
			$aInfo['question'],
            $val_tipo,
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'],
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['question_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/preguntas.php');
	$Preguntas_Class = new Preguntas();
	$cantidad_datos = $Preguntas_Class->consultaCantidad();
}
unset($Preguntas_Class);
