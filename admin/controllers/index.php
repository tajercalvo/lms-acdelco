<?php
include_once('models/usuarios.php');

$usuario_Class = new Usuarios();

$idUsr = 0;

if(isset($_SESSION['idUsuario'])){
	$idUsr = $_SESSION['idUsuario'];
}
$cuenta = ["inscritos" => [ '1' => 0, '2' => 0, '3' => 0, '4' => 0 ]
			,"finalizados" => [ '1' => 0, '2' => 0, '3' => 0, '4' => 0 ]
			,"disponibles" => [ '1' => 0, '2' => 0, '3' => 0, '4' => 0 ] ];
$data_user = $usuario_Class->consultaDatosUsuario($idUsr);
$charge_user = $usuario_Class->consultaDatosCargos($idUsr);
$rol_user = $usuario_Class->consultaDatosRoles($idUsr);
/*$datosUsuario = $usuario_Class->consultaDatosUsuario($IdUsuario);
$charge_user = $usuario_Class->consultaDatosCargos($IdUsuario);
$rol_user = $usuario_Class->consultaDatosRoles($IdUsuario);*/

/*Transmisiones de streaming en el home con modal*/
$var_streaming = 0;
$var_transmision = 0;
/*if($_SESSION['idUsuario']!=1){
	@session_start();
	$DelaerGM = $_SESSION['dealer_id'];
	if($DelaerGM==1){
		$var_streaming = 1;
	}
	$compara_roles = array("23","35","36","49","89","93","94","95","96","97","99","100","130","134");
	foreach ($charge_user as $key => $value_dat) {
		if(in_array($value_dat['charge_id'],$compara_roles)){
			$var_streaming = 1;
		}
	}
	if($var_streaming>0){
		$var_DatRegStreaming = $usuario_Class->registraStreaming();
	}
}else{
	//$var_transmision = 1;
	$var_streaming = 1;
}*/
/*Transmisiones de streaming en el home con modal*/


$cantidad_datos = $usuario_Class->consultaCantidadIndex();
$datosUsuarios = $usuario_Class->consultaDatosUsuariosIndex();

$Datos_CursosTomados = $usuario_Class->consultaTomados();
$Cant_CursosTomados = count($Datos_CursosTomados);

$Datos_CA_Inscrito = $usuario_Class->consulta_CA_Inscrito();
$CantDatos_CA_Inscrito = count($Datos_CA_Inscrito);
$Datos_CT_Inscrito = $usuario_Class->consulta_CT_Inscrito();
$CantDatos_CT_Inscrito = count($Datos_CT_Inscrito);

$Datos_CA_Finalizado = $usuario_Class->consulta_CA_Finalizado();
$CantDatos_CA_Finalizado = count($Datos_CA_Finalizado);
$Datos_CT_Finalizado = $usuario_Class->consulta_CT_Finalizado();
$CantDatos_CT_Finalizado = count($Datos_CT_Finalizado);

$Datos_CA_Disponible = $usuario_Class->consulta_CA_Disponible();
$CantDatos_CA_Disponible = count($Datos_CA_Disponible);
$Datos_CT_Disponible = $usuario_Class->consulta_CT_Disponible();
$CantDatos_CT_Disponible = count($Datos_CT_Disponible);
//Consulta de cursos disponibles para asignacion
$Datos_CT_Disponible2 = $usuario_Class->consulta_CT_Disponible2();
$CantDatos_CT_Disponible2 = count($Datos_CT_Disponible2);


//Cursos que tiene disponibles para inscripción
$DatosDisponibleSinInsc = array_merge($Datos_CT_Disponible, $Datos_CA_Disponible);
foreach ($DatosDisponibleSinInsc as $key => $value) {
	switch ($value['specialty_id']) {
		case '1': $cuenta['disponibles'][1]++; break;
		case '2': $cuenta['disponibles'][2]++; break;
		case '3': $cuenta['disponibles'][3]++; break;
		case '4': $cuenta['disponibles'][4]++; break;
	}
}
//Cursos que tiene disponibles2 para inscripción
$DatosDisponibleSinInsc2 = array_merge($Datos_CT_Disponible2);
foreach ($DatosDisponibleSinInsc2 as $key => $value) {
	switch ($value['specialty_id']) {
		case '1': $cuenta['disponibles'][1]++; break;
		case '2': $cuenta['disponibles'][2]++; break;
		case '3': $cuenta['disponibles'][3]++; break;
		case '4': $cuenta['disponibles'][4]++; break;
	}
}
//Cursos En los que se encuentra inscrito
$DatosDisponibleInsc = array_merge($Datos_CT_Inscrito, $Datos_CA_Inscrito);
foreach ($DatosDisponibleInsc as $key => $value) {
	switch ($value['specialty_id']) {
		case '1': $cuenta['inscritos'][1]++; break;
		case '2': $cuenta['inscritos'][2]++; break;
		case '3': $cuenta['inscritos'][3]++; break;
		case '4': $cuenta['inscritos'][4]++; break;
	}
}
//Cursos que ha finalizado y quiere repasar
$DatosFinalizadoInsc = array_merge($Datos_CT_Finalizado, $Datos_CA_Finalizado);
foreach ($DatosFinalizadoInsc as $key => $value) {
	switch ($value['specialty_id']) {
		case '1': $cuenta['finalizados'][1]++; break;
		case '2': $cuenta['finalizados'][2]++; break;
		case '3': $cuenta['finalizados'][3]++; break;
		case '4': $cuenta['finalizados'][4]++; break;
	}
}

/*Invocación Banners Usuario*/
include('models/banners.php');
$banners_Class = new Banners();

$listado_BannerTop = $banners_Class->consultaTop($_SESSION['dealer_id']);
$listado_BannerCal = $banners_Class->consultaCal($_SESSION['dealer_id']);
$listado_BannerHis = $banners_Class->consultaHis($_SESSION['dealer_id']);
$listado_BannerMod = $banners_Class->consultaMod($_SESSION['dealer_id']);

$var_Publicidad = 1;
try {
    if(@$listado_BannerMod['banner_type_id'] != 5){
    	if(is_array($listado_BannerMod) && count($listado_BannerMod) > 0){
    		$var_Publicidad = $usuario_Class->consulta_Publicidad($listado_BannerMod['banner_id']);
    		if($var_Publicidad<=0){
    			$act_Publicidad = $usuario_Class->crea_Publicidad($listado_BannerMod['banner_id']);
    		}
    	}else{
    	    $var_Publicidad = 1;
    	}
    }else{
    	$var_Publicidad = 1;
    }
} catch (Exception $e) {
    $var_Publicidad = 1;
    //echo 'Excepci��n capturada: ',  $e->getMessage(), "\n";
} finally {
    $var_Publicidad = 1;
    //echo "Segundo finally.\n";
}


unset($banners_Class);
/*Invocación Banners Usuario*/

unset($usuario_Class);
