<?php
@session_start();
if(isset($_GET['opcn'])){
	include_once('models/parametros.php');
	$parametros_Class = new Parametros();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $parametros_Class->consultaRegistro($_GET['id']);
	}else{
		$registroConfiguracion['nombre_lista'] = "";
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/parametros.php');
	$parametros_Class = new Parametros();
	if($_POST['opcn']=="crear"){
		$resultado = $parametros_Class->Crearparametros($_POST['nombre'],$_POST['valor']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}

	}elseif($_POST['opcn']=="editar"){
		$resultado = $parametros_Class->Actualizarparametros($_POST['idElemento'],$_POST['nombre'],$_POST['valor']);
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas especialidades
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY p.parameter';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY p.parameter '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='1') {
    		$sOrder = ' ORDER BY p.value '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='2') {
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='3') {
    		$sOrder = ' ORDER BY p.date_edition '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/parametros.php');
	$parametros_Class = new Parametros();
	$datosConfiguracion = $parametros_Class->consultaDatosparametros($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $parametros_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    //Activacion de boton editar
    if($_SESSION['max_rol']>=6){ 

	    foreach ($datosConfiguracion as $iID => $aInfo) {
	    	$valOpciones = '<a href="op_parametros.php?opcn=editar&id='.$aInfo['parameter_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
	    	$aItem = array(
				$aInfo['parameter'], 
				$aInfo['value'], 
				$aInfo['first_name'].' '.$aInfo['last_name'],
				$aInfo['date_edition'],
				$valOpciones,
				'DT_RowId' => $aInfo['parameter_id']
			);
			$output['aaData'][] = $aItem;
	    }

	} else{
		foreach ($datosConfiguracion as $iID => $aInfo) {
	    	$aItem = array(
				$aInfo['parameter'], 
				$aInfo['value'], 
				$aInfo['first_name'].' '.$aInfo['last_name'],
				$aInfo['date_edition'],
				'DT_RowId' => $aInfo['parameter_id']
			);
			$output['aaData'][] = $aItem;
	    }
	}
    echo json_encode($output);
} else{
	include_once('models/parametros.php');
	$parametros_Class = new Parametros();
	$cantidad_datos = $parametros_Class->consultaCantidad();
}
unset($parametros_Class);