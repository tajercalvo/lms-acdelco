<?php

if( isset( $_POST['opcn'] ) ){
    include_once('../models/analytics.php');
    $analytics = new Analytics();
    switch ( $_POST['opcn'] ) {
        case 'sel_sede':
            $concesionarios = $_POST['concesionarios'];
            // var_dump($concesionarios);
            $sedes = $analytics->getSedes( $concesionarios ,"../" );
            foreach ($sedes as $key => $value) { ?>
                <option value="<?php echo $value['headquarter_id'] ?>"><?php echo $value['headquarter'] ?></option>
        <?php    }
        break;

        case 'sel_Concs':
            $zonas = $_POST['zonas'];
            // var_dump($zonas);
            $concs = $analytics->getConcesionarios( $zonas ,"../" );
            foreach ($concs as $key => $value) { ?>
                <option value="<?php echo $value['dealer_id'] ?>"><?php echo $value['dealer'] ?></option>
        <?php    }
        break;

        case 'datos':
            $p = [
                "prof"              => "../",
                "zonas"             => $_POST['zonas'],
                "concesionarios"    => $_POST['concesionarios'],
                "sedes"             => $_POST['sedes'],
                "cargos"            => $_POST['cargos'],
                "es_activo"         => $_POST['es_activo'],
                "es_inactivo"       => $_POST['es_inactivo'],
                "femenino"          => $_POST['femenino'],
                "masculino"         => $_POST['masculino'],
                "contratacion"      => $_POST['contratacion'],
                "tipo_usuario"      => $_POST['tipo_usuario'],
                "start_date_day"    => $_POST['start_date_day'],
                "end_date_day"      => $_POST['end_date_day']
            ];
            // print_r( $p );
            $datos = $analytics->consultaUsuariosCcs( $p );
            echo( json_encode( $datos ) );
        break;
    }

}else{   //fin isset $_GET['opcn']
    include_once('models/analytics.php');
    $analytics = new Analytics();
    $concesionarios = $analytics->getConcesionarios("");
    $zonas          = $analytics->getZonas();
    $cargos         = $analytics->getCargos();
    $tipoUsuarios   = $analytics->getTipoUsuarios();
}

?>
