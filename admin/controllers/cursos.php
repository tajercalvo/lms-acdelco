<?php
if(isset($_GET['opcn'])){
    include_once('models/cursos.php');
    $cargos_Clas = new Cargos();
    if(isset($_GET['id'])){
        //consultas individuales
        $registroConfiguracionDetail = $cargos_Clas->consultaRegistroDetail($_GET['id']);
        $registroConfiguracionModulosDetail = $cargos_Clas->consultaRegistroDetailModulos($_GET['id']);
        

      } 
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/cursos.php');
	$cargos_Class = new Cargos();
	switch ($_POST['opcn']) {
		case 'crear':
			$resultado = $cargos_Class->Crearcargos($_POST['course'],'',$_POST['subject_id'],$_POST['supplier_id'],$_POST['description'],$_POST['prerequisites'],$_POST['type'],$_POST['specialty_id'],$_POST['target']);
			if($resultado>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;

		case 'editar':
			$resultado = $cargos_Class->Actualizarcargos($_POST['idElemento'],$_POST['course'],$_POST['status_id'],'',$_POST['subject_id'],$_POST['supplier_id'],$_POST['description'],$_POST['prerequisites'],$_POST['type'],$_POST['specialty_id'],$_POST['target']);
			echo('{"resultado":"SI"}');
		break;

		case 'RegistrarNota':
			$resultado = $cargos_Class->RegistrarNota($_POST['Nota'],$_POST['Modulo_id']);
			echo('{"resultado":"SI"}');
		break;

		case 'actualizarParametros':
			$p = $_POST;
			$resultado = Cargos::guardarCambiosParametros( $p, "../" );
			if( $resultado > 0 ){
				$res = ["error"=>false, "message"=>"Se ha modificado correctamente los valores"];
			}else{
				$res = ["error"=>true, "message"=>"No se han realizado modificaciones en ningún registro"];
			}
			echo json_encode( $res );
		break;

		case 'CrearImagen':
			if(isset($_FILES['image_new'])){
				if(isset($_FILES['image_new']['tmp_name'])){
					$imgant = $_POST['imgant'];
					$course_id = $_POST['course_id'];
					$nom_archivo1 = $_FILES["image_new"]["name"];
					$new_name1 = "ACDelco_LUDUS_";
					$new_name1 .= date("Ymdhis");
					$new_extension1 = "jpg";
					preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
					switch (strtolower($ext1[2])) {
						case 'jpeg' : $new_extension1 = ".jpg";
							break;
						case 'jpg' : $new_extension1 = ".jpg";
							break;
						case 'JPG' : $new_extension1 = ".jpg";
							break;
						case 'JPEG' : $new_extension1 = ".jpg";
							break;
						default    : $new_extension1 = "no";
							break;
					}
					if($new_extension1 != "no"){
						$new_name1 .= $new_extension1;
						$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../../assets/images/distinciones/".$new_name1);
							if($resultArchivo1==1){
								if($imgant != "" && $imgant != "default.jpg"){
									if(file_exists("../../assets/images/distinciones/".$imgant)){
										unlink("../../assets/images/distinciones/".$imgant);
									}
								}
								$op_cursoImagen = $cargos_Class->actualizaImagen($course_id,$new_name1);
								echo('{"resultado":"SI","archivo":"'.$new_name1.'"}');
							}else{
								echo('{"resultado":"NO"}');
							}
					}else{
						echo('{"resultado":"NO"}');
					}
				}else{
					echo('{"resultado":"NO"}');
				}
			}
		break;
		case 'descargar':
			$resultado = Cargos::descargo_material( $_POST );
			echo json_encode($resultado);
			break;
	}
}elseif(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.image';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.image '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY d.newcode '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY d.course '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY c.type '.$_GET['sSortDir_0'];
    	}/*elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY c.code '.$_GET['sSortDir_0'];
    	}*/elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY z.subject '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='5') {
    		$sOrder = ' ORDER BY p.specialty '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='6'){
    		$sOrder = ' ORDER BY d.supplier '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='7') {
    		$sOrder = ' ORDER BY s.first_name, s.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='8') {
    		$sOrder = ' ORDER BY c.date_creation '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='9') {
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/cursos.php');
	$cargos_Class = new Cargos();
	$datosConfiguracion = $cargos_Class->consultaDatossedes($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $cargos_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    @session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	$datos_progreso = $cargos_Class->progress_bar($aInfo['course_id']);

    	$progreso = 0;
    	$no_tiene = 'No tiene: ';
    	foreach ($datos_progreso as $key => $value) {
    			if($value['tiene'] == 'SI'){
    				$progreso += 1;
    			}else{
    				$no_tiene .= $value['caso'].', '; // Concatenamos lo que le curso no tiene, ejemplo, evaluacion, modulo, matrial de apoyo etc
    			}
    		}
    	$no_tiene = substr($no_tiene, 0, -2);// Le quitamos el guion - al final
    	$no_tiene .= '.';// Le agregamos un punto al final
    	if($progreso == 5){$no_tiene ='';} // SI el array es igual a 5 quiere decir qeu tiene todos los requisitos para estar en 100 % y la variable $no_tiene queda vacia
    	$progreso = 20 * $progreso;
    	if($progreso < 50){
    		$color = 'danger';
    	}else if ($progreso < 81){
    		$color = 'primary';
    	}else{
    		$color = 'success';
    	}

    	if($_SESSION['max_rol'] >= 5){
    		//$barra_progreso = '<div data-toggle="tooltip" data-original-title="'.$no_tiene.'" data-placement="rigth" class="progress progress"><div class="progress-bar progress-bar-'.$color.'" style="width: '.$progreso.'%;">'.round($progreso). ' %</div></div>';
    	}else{
    		//$barra_progreso = '';
    	}
    	/*@session_start();
		$idQuien = $_SESSION['idUsuario'];*/
    	if($aInfo['type'] == 'WBT'){
    		$barra_progreso = '';
    	}

    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">En Migración</span>';
    	}
    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
    		$valOpciones = '<a href="op_vct.php?opcn=editar&id='.$aInfo['course_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	}
    	$href = '<a href="course_detail.php?token='.md5('GMAcademy').'&_valSco='.md5('GMColmotores').'&opcn=ver&id='.$aInfo['course_id'].'">';
    	$imagen = $href.'<img src="../assets/images/distinciones/'.$aInfo['image'].'" style="width: 80px;" alt="'.$aInfo['course'].'"></a>';
    	$aItem = array(
    		$imagen,
			$aInfo['newcode'],
			$href.$aInfo['course'].'</a>',
			$aInfo['type'],
			$aInfo['specialty'],
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'],
			$aInfo['date_creation'],
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['course_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/cursos.php');
	$cargos_Class = new Cargos();
	$cantidad_datos = $cargos_Class->consultaCantidad();
}