<?php
if(isset($_POST['opcn'])){
  include_once('../models/encuestas_pendientes.php');
  $obj = new Encuestas_pendientes();

  if($_POST['opcn']=="getEncuesta"){
    $resultado = $obj->getEncuesta( $_POST['encuesta_id'] );
    echo json_encode($resultado);
  }elseif($_POST['opcn']=="crear"){
    $resultado = $obj->crearEncuesta( $_POST );
    echo json_encode($resultado);
  }elseif($_POST['opcn']=="editar"){
    $resultado = $obj->editarEncuesta( $_POST );
    echo json_encode($resultado);
  }elseif($_POST['opcn']=="guardarEncuesta"){
    $resultado = $obj->guardarEncuesta( $_POST );
    echo json_encode($resultado);
  }
  
  
}else{
    include_once('models/encuestas_pendientes.php');
    $obj = new Encuestas_pendientes();
    $encuestas = $obj->getEncuestas();
    $empresas = $obj->consultaDealers();
    $cargos = $obj->consultaCharges();
    $cursos = $obj->consultaCourses();
    unset($obj);
}

