<?php
if(isset($_POST['opcn'])){
	if($_POST['opcn']=="crear"){
		include_once('models/eventos.php');
		$Eventos_Class = new Eventos();
		$resultado = $Eventos_Class->CrearEvento($_POST['event'],$_POST['media_id'],$_POST['date_event'],$_POST['living'],$_POST['description']);
		$Eventos_Meddatos = $Eventos_Class->consultaMedDatos();
	}elseif($_POST['opcn']=="editar"){
		include_once('models/eventos.php');
		$Eventos_Class = new Eventos();
		$resultado = $Eventos_Class->ActualizaEvento($_POST['event_id'],$_POST['event'],$_POST['media_id'],$_POST['date_event'],$_POST['living'],$_POST['description'],$_POST['estado']);
	}
}

if(isset($_GET['opcn'])){
	if($_GET['opcn']=="consultar"){
		include_once('../models/eventos.php');
		$Eventos_Class = new Eventos();
		if(isset($_GET['start'])){
			$fecha_INI = date("Y-m-d",$_GET['start']);
		}
		if(isset($_GET['end'])){
			$fecha_FIN = date("Y-m-d",$_GET['end']);
		}
		$datosEventos = $Eventos_Class->consultaEventos($fecha_INI,$fecha_FIN);
		foreach ($datosEventos as $iID => $data) {
			$description = "<strong style='font-size: large;'>".$data['date_event']."</strong><br>
			<strong>".$data['event']."</strong><br>
			<strong>Lugar:</strong> ".$data['living'];
			$color = "blue";
			$aItem = array(
				'id' => $data['event_id'],
				'title' => $data['event']." : ".$data['date_event'],
				'start' => substr($data['date_event'], 0, 10)."T08:00:00",
				'end' => substr($data['date_event'], 0, 10)."T23:59:59",
				'url' => "eventos.php?id=".$data['event_id'],
				'description' => $description,
				'color' => $color,
				'allDay' => false
			);
			$output[] = $aItem;
		}
		echo json_encode($output);
	}
}

if(isset($_GET['id'])){
	include_once('models/eventos.php');
	$Eventos_Class = new Eventos();
	$Evento_datos = $Eventos_Class->consultaEvento($_GET['id']);
	$Galeria_datos = $Eventos_Class->consultaGaleria($_GET['id']);
	$Eventos_Meddatos = $Eventos_Class->consultaMedDatos();
}

if( !isset($_POST['opcn']) && !isset($_GET['opcn']) ){
	include_once('models/eventos.php');
	$Eventos_Class = new Eventos();
	$Eventos_Meddatos = $Eventos_Class->consultaMedDatos();
}
unset($Eventos_Class);