<?php
if(isset($_POST['opcn']) || isset($_GET['opcn']) ){
	include_once('models/timeline.php');
	$timeline = new Timeline();
	if((isset($_POST['opcn'])&&$_POST['opcn']=="buscar") || (isset($_GET['opcn'])&&$_GET['opcn']=="buscar")){
		$id = '';
		$start_date_day = '';
		$end_date_day = '';
		if(isset($_POST['identificacion'])){
			$id = $_POST['identificacion'];	
		}else if(isset($_GET['identificacion'])){
			$id = $_GET['identificacion'];
		}
		if(isset($_POST['start_date_day'])){
			$start_date_day = $_POST['start_date_day'];	
		}else if(isset($_GET['start_date_day'])){
			$start_date_day = $_GET['start_date_day'];
		}
		if(isset($_POST['end_date_day'])){
			$end_date_day = $_POST['end_date_day'];	
		}else if(isset($_GET['end_date_day'])){
			$end_date_day = $_GET['end_date_day'];
		}

		if(isset($_GET['reviews_answer_id'])){
			$timeline->BorrarEvaluacion($_GET['reviews_answer_id']);
		}

		if (isset($_POST['checkbox'])=='si') {

		$usuario = $timeline->Tl_User($id); //Datos del usuario
		$uconexion = $timeline->ultimanavegacion($id); // Ultima coonexion
		$cargos = $timeline->Tl_User_Charge($id, $start_date_day, $end_date_day); // Asignacion d ecargos
		$excusas = $timeline->Tl_User_excuses($id, $start_date_day,$end_date_day); // Excusas
		$foro = $timeline->Tl_User_forum($id, $start_date_day,$end_date_day);
		foreach ($foro as $key => $value) {
			if ($value['descripcion']>0) {
				$foro2[]=$value;
			}
		}
		if (!isset($foro2)) {
			$foro2=array();
		}
		$invitaciones = $timeline->Tl_User_invitation($id, $start_date_day,$end_date_day ); // Invitaciones
		$megusta = $timeline->Tl_User_library_likes($id, $start_date_day, $end_date_day); // likes a los archivos de la biblioteca
		$vistos = $timeline->Tl_User_library_views($id, $start_date_day, $end_date_day); // Vistos archivos de la biblioteca
		$navegacion = $timeline->Tl_User_navigation($id, $start_date_day, $end_date_day); // Por donde navego mas tiempo
		$respuestas = $timeline->Tl_User_answer($id, $start_date_day, $end_date_day); // Foros y encuentas que respondio
		$resultados = $timeline->Tl_User_modules_results($id, $start_date_day, $end_date_day); // Hojas de ruta
		$hrutas = $timeline->Tl_User_waybills($id, $start_date_day, $end_date_day);
		
		$infor = array_merge($cargos, $excusas, $foro2, $invitaciones, $megusta, $vistos, $navegacion, $respuestas, $resultados, $hrutas); // Unificando los array
		
		$var = count($infor);
		if ($var==0) {
				$mensaje = "No existen resultado para esta busqueda";
		}else{
			foreach ($infor as $key => $datos) {
   		 $aux[$key] = $datos['fecha']; // Recorriendo el array y ordenandolo por fechas
		}
		array_multisort($aux, SORT_ASC, $infor); // Ordenando las fechas del aray en orden de menor a mayor
		}

		}else{

		$usuario = $timeline->Tl_User($id); //Datos del usuario
		$uconexion = $timeline->ultimanavegacion($id); // Ultima coonexion
		$cargos = $timeline->Tl_User_Charge($id, $start_date_day, $end_date_day); // Asignacion d ecargos
		$excusas = $timeline->Tl_User_excuses($id, $start_date_day,$end_date_day); // Excusas
		$foro = $timeline->Tl_User_forum($id, $start_date_day,$end_date_day);
		
		foreach ($foro as $key => $value) {
			if ($value['descripcion']>0) {
				$foro2[]=$value;
		}
		}
		if (!isset($foro2)) {
			$foro2=array();
		}
		$invitaciones = $timeline->Tl_User_invitation($id, $start_date_day,$end_date_day ); // Invitaciones
		$megusta = $timeline->Tl_User_library_likes($id, $start_date_day, $end_date_day); // likes a los archivos de la biblioteca
		$vistos = $timeline->Tl_User_library_views($id, $start_date_day, $end_date_day); // Vistos archivos de la biblioteca
		//$navegacion = $timeline->Tl_User_navigation($id, $start_date_day, $end_date_day); // Por donde navego mas tiempo
		$respuestas = $timeline->Tl_User_answer($id, $start_date_day, $end_date_day); // Foros y encuentas que respondio
		$resultados = $timeline->Tl_User_modules_results($id, $start_date_day, $end_date_day); // Hojas de ruta
		$hrutas = $timeline->Tl_User_waybills($id, $start_date_day, $end_date_day);
		
		$infor = array_merge($cargos, $excusas, $foro2, $invitaciones, $megusta, $vistos, $respuestas, $resultados, $hrutas); // Unificando los array
		
		$var = count($infor);
		if ($var==0) {
				$mensaje = "No existen resultado para esta busqueda";
		}else{
			foreach ($infor as $key => $datos) {
   		 $aux[$key] = $datos['fecha']; // Recorriendo el array y ordenandolo por fechas
		}
		array_multisort($aux, SORT_ASC, $infor); // Ordenando las fechas del aray en orden de menor a mayor
		}

		}
		
 	 }
 	}	
unset($timeline);