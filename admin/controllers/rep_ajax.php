<?php

if(isset($_POST['opcn']) and ($_POST['opcn'])== 'consulta'){

include_once('../models/rep_ajax.php');
	$PendientesCursos_Class = new RepPendientesCursos();

	$inicio = $_POST['start_date_day'];
	$fin = $_POST['end_date_day'];
	$curso_id = $_POST['idconsulta'];

	// EL ultimo array de la consulta siempre vendra vacio
	if ($curso_id=='') {
		echo "FIN";
		return false;
	}

	if ($curso_id==0) {

		$cursos_para_consulta = $PendientesCursos_Class->consultaCursos_en_consultaDatosAll($inicio, $fin);
		$cantidad_cursos_en_consulta = 	count($cursos_para_consulta);

		foreach ($cursos_para_consulta as $key => $value) {
			echo $value['course_id'].'-';
		}
		// echo json_encode($cursos_para_consulta);
		// echo "";
		
	}else{

				$datosCursos_all = $PendientesCursos_Class->consultaDatosAll($inicio, $fin, $curso_id);

				$asistieron_tot = 0;
				$noasistieron_tot = 0;
				$aprobaron = 0;
				$noaprobaron = 0;
				$invitados = 0;
				$puntaje = 0;
				$aprobo = 0;

					// echo '{"resultado":"SI", "tabla":';

						foreach ($datosCursos_all as $iID => $data) {
						echo '	<div class="well">
								<table class="table table-invoice">
									<tbody>
										<tr>
											<td style="width: 30%;">
												<p class="lead">Sesión de entrenamiento</p>
												<h4>'; echo($data['newcode']).'|'; echo($data['course']); echo '</h4>
												<address class="margin-none">
													<strong>Tipo: </strong>'; echo($data['type']); echo '<br/>
													<strong>Duración: </strong>'; echo($data['duration_time']); echo '<br/>
													<strong>Profesor: </strong>'; echo $data['first_prof'].' '; echo $data['last_prof']; echo '<br/>
													<strong>Inicio: </strong>'; echo($data['start_date']); echo '<br />
													<strong>Final: </strong>'; echo($data['end_date']); echo '<br/>
													<strong>Lugar: </strong>'; echo(substr($data['living'],0,30)); echo '<br/>
													<strong>Dirección: </strong>'; echo(substr($data['address'],0,30)); echo '<br/>
													<strong>Ciudad: </strong>'; echo($data['city']).' | ';echo($data['schedule_id']);
												echo '</address>
											</td>
											<td class="right">
												<p class="lead">Asistencia</p>
												<!-- // Table -->
												<table class="table table-condensed table-vertical-center table-thead-simple">
													<thead>
														<tr>
															<th class="center">Alumno</th>
															<th class="center">Identificación</th>
															<th class="center">Cargo</th>
															<th class="center">Concesionario</th>
															<th class="center">Asistencia</th>
															<th class="center" style="width: 1%">Puntaje</th>
															<th class="center" style="width: 1%">ES</th>
															<th class="center" style="width: 1%">HR</th>
															<th class="center">Resultado</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">';
													
															$var_Ctrl = 0;
															foreach ($data['detail'] as $iID => $dataDetail) {
																$var_Ctrl++;
																$result_org = "Reprobó";
																$estado_inv = "Invitado";
																$puntaje_e = "0";
																$puntaje_h = "0";
																//print_r($dataDetail);
																if($dataDetail['estado']==1){
																	$invitados++;
																	$estado_inv = "Invitado";
																}elseif($dataDetail['estado']==2){
																	$asistieron_tot++;
																	$estado_inv = "Asistió";
																}else{
																	$noasistieron_tot++;
																	$estado_inv = "No Asitió";
																}
																if(isset($dataDetail['resultados'][0]['score'])){
																	$puntaje = $dataDetail['resultados'][0]['score'];
																	$puntaje_e = $dataDetail['resultados'][0]['score_review'];
																	$puntaje_h = $dataDetail['resultados'][0]['score_waybill'];
																	$aprobo = $dataDetail['resultados'][0]['approval'];
																	if($aprobo=="SI"){
																		$aprobaron++;
																		$result_org = "Aprobó";
																	}else{
																		$noaprobaron++;
																		$result_org = "Reprobó";
																	}
																}
														
															echo '<!-- Item -->
															<tr class="selectable">
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo(substr($dataDetail['first_name'],0,10).' '.substr($dataDetail['last_name'],0,10)); echo '</td>
																<td class="text-left important" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['identification']); echo '</td>
																<td class="center important" style="padding: 2px; font-size: 80%;">';
																
																	if(isset($dataDetail['charges'])){
																		foreach ($dataDetail['charges'] as $iID => $dataCharges) {
																			echo(" - ".$dataCharges['charge']);
																		}
																	}
																
																echo '</td>
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['headquarter']); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($estado_inv); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($puntaje); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($puntaje_e); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($puntaje_h); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary">'; echo($result_org); echo '</span></td>
															</tr>
															<!-- // Item END -->';
														
														$puntaje = 0;
													}
														
														 if(isset($data['excusas'])){ foreach ($data['excusas'] as $iID => $dataDetail) {
															echo '<!-- Item -->
															<tr class="selectable">
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo(substr($dataDetail['first_name'],0,10).' '.substr($dataDetail['last_name'],0,10));echo '</td>
																<td class="text-left important" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['identification']); echo' </td>
																<td class="center important" style="padding: 2px; font-size: 80%;">';
																
																	if(isset($dataDetail['charges'])){
																		foreach ($dataDetail['charges'] as $iID => $dataCharges) {
																			echo(" - ".$dataCharges['charge']);
																		}
																	}
																
																echo '</td>
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['headquarter']); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">No Asistió</td>
																<td class="center" style="padding: 2px; font-size: 80%;"></td>
																<td class="center" style="padding: 2px; font-size: 80%;"></td>
																<td class="center" style="padding: 2px; font-size: 80%;"></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><a href="../assets/excusas/'; echo($dataDetail['file']); echo '" target="_blank" class="label label-primary">Excusa</a></td>
															</tr>
															<!-- // Item END -->';
														 } }
													echo '</tbody>
												</table>
												<!-- // Table END -->
											</td>
										</tr>';
										 if(isset($data['asistencia'])){ 
										echo '<tr>
											<td class="center" colspan="2">';
												
												$Cant_Datos_var = count($data['asistencia']);
												$Porcentaje_Var = "0";
												if($Cant_Datos_var < 5){
													$Porcentaje_Var = $Cant_Datos_var*20;
												}else{
													$Porcentaje_Var = 100;
												}
												foreach ($data['asistencia'] as $iID => $dataAsistencia) { 
													echo '<input type="hidden" class="ValSche'; echo($data['schedule_id']); echo '" name="ValSche_'; echo($data['schedule_id']).'_'; echo($dataAsistencia['dealer_id']); echo '" id="ValSche_'; echo($data['schedule_id']).'_'; echo($dataAsistencia['dealer_id']); echo '" value="ValSche_'; echo($data['schedule_id']).'_'; echo($dataAsistencia['dealer_id']); echo '" data-deal="'; echo($dataAsistencia['dealer']); echo '" data-min="'; echo($dataAsistencia['min_size']); echo'" data-max="'; echo($dataAsistencia['max_size']); echo'" data-ins="'; echo($dataAsistencia['inscriptions']); echo '">';
												 } 
												echo '<p class="lead">Asignación de Cupos</p>
												<!-- Widget -->
												<div class="widget widget-heading-simple widget-body-gray" style="width:'; echo $Porcentaje_Var; echo '%;">
													<div class="widget-body">
														<input type="hidden" class="ElementID" id="Schedule'; echo($data['schedule_id']); echo '" name="Schedule'; echo($data['schedule_id']); echo'" value="'; echo($data['schedule_id']); echo '" />
														<!-- Ordered bars Chart -->
														<div id="chart_ordered_bars'; echo($data['schedule_id']); echo '" class="flotchart-holder" style="height: 300px;"></div>
													</div>
												</div>
												<!-- // Widget END -->
											</td>
										</tr>';
										 } 
									echo '</tbody>
								</table>
							</div>';
						 } 
			} // FIn else

					// echo '}';

}else if(isset($_POST['opcn']) and ($_POST['opcn'])== 'descargar'){

	include_once('../models/rep_ajax.php');
	$PendientesCursos_Class = new RepPendientesCursos();

	$inicio = $_POST['start_date_day'];
	$fin = $_POST['end_date_day'];
	$curso_id = $_POST['idconsulta'];

	// EL ultimo array de la consulta siempre vendra vacio
	if ($curso_id=='') {
		echo "FIN";
		return false;
	}

	if ($curso_id==0) {

		$cursos_para_consulta = $PendientesCursos_Class->consultaCursos_en_consultaDatosAll($inicio, $fin);
		$cantidad_cursos_en_consulta = 	count($cursos_para_consulta);

		foreach ($cursos_para_consulta as $key => $value) {
			echo $value['course_id'].'p';
		}
		// echo json_encode($cursos_para_consulta);
		// echo "";
		
	}else{

				$datosCursos_all = $PendientesCursos_Class->consultaDatosAll($inicio, $fin, $curso_id);



	//print_r($datosCursos_all);

				echo '<table>
        
        <thead>
            <tr>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">C&Oacute;DIGO</th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">SEDE</th>
                <th data-hide="phone,tablet">CIUDAD</th>
                <th data-hide="phone,tablet">ZONA</th>
                <th data-hide="phone,tablet">ALUMNO</th>
                <th data-hide="phone,tablet">IDENTIFICACI&Oacute;N</th>
                <th data-hide="phone,tablet">CARGO</th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet">FINAL</th>
                <th data-hide="phone,tablet">LUGAR</th>
                <th data-hide="phone,tablet">PROFESOR</th>
                <th data-hide="phone,tablet">ASISTENCIA</th>
                <th data-hide="phone,tablet">PUNTAJE</th>
                <th data-hide="phone,tablet">SATISFACCIÓN</th>
                <th data-hide="phone,tablet">HOJARUTA</th>
                <th data-hide="phone,tablet">RESULTADO</th>
                <th data-hide="phone,tablet">MINIMOS</th>
                <th data-hide="phone,tablet">MAXIMOS</th>
                <th data-hide="phone,tablet">INSCRITOS</th>
                <th data-hide="phone,tablet">EXCUSA</th>
                <th data-hide="phone,tablet">HORAS CURSO</th>
                <th data-hide="phone,tablet">DIAS CURSO</th>
                <th data-hide="phone,tablet">ESPECIALIDAD</th>
                <th data-hide="phone,tablet">PROVEEDOR</th>
                <th data-hide="phone,tablet">TIPO DE CURSO</th>
            </tr>
        </thead>
        
        <tbody>';
         
                    $asistieron_tot = 0;
                    $noasistieron_tot = 0;
                    $aprobaron = 0;
                    $noaprobaron = 0;
                    $invitados = 0;
                    $puntaje = 0;
                    $aprobo = 0;
                    $puntaje_e = "0";
                    $puntaje_h = "0";
                    $var_Ctrl = 0;
                if($datosCursos_all > 0){ 
                        foreach ($datosCursos_all as $iID => $data) { 
                            foreach ($data['detail'] as $iID => $dataDetail) { 
                                $var_Ctrl++;
                                $result_org = "Reprobo";
                                $estado_inv = "Invitado";
                                //print_r($dataDetail);
                                if($dataDetail['estado']==1){
                                    $invitados++;
                                    $estado_inv = "Invitado";
                                }elseif($dataDetail['estado']==2){
                                    $asistieron_tot++;
                                    $estado_inv = "Asistio";
                                }else{
                                    $noasistieron_tot++;
                                    $estado_inv = "No Asitio";
                                }
                                if(isset($dataDetail['resultados'][0]['score'])){
                                    $puntaje = $dataDetail['resultados'][0]['score'];
                                    $aprobo = $dataDetail['resultados'][0]['approval'];
                                    $puntaje_e = $dataDetail['resultados'][0]['score_review'];
                                    $puntaje_h = $dataDetail['resultados'][0]['score_waybill'];
                                    if($aprobo=="SI"){
                                        $aprobaron++;
                                        $result_org = "Aprobo";
                                    }else{
                                        $noaprobaron++;
                                        $result_org = "Reprobo";
                                    }
                                }
                                $minimos = "0";
                                $maximos = "0";
                                $inscritos = "0";
                                if(isset($data['asistencia'])){
                                    foreach ($data['asistencia'] as $iID => $dataAsistencia) {
                                        if( $dataAsistencia['dealer_id']== $dataDetail['dealer_id']){
                                            $minimos = $dataAsistencia['min_size'];
                                            $maximos = $dataAsistencia['max_size'];
                                            $inscritos = $dataAsistencia['inscriptions'];
                                        }
                                    }
                                }
                        
                   echo ' <tr>
                        <td>'; echo(($dataDetail['dealer'])); echo '</td>
                        <td>'; echo(($data['newcode'])); echo '</td>
                        <td>'; echo(($data['course'])); echo '</td>
                        <td>'; echo(($dataDetail['headquarter'])); echo '</td>
                        <td>'; echo(($dataDetail['area'])); echo '</td>
                        <td>'; echo(($dataDetail['zone'])); echo '</td>
                        <td>'; echo(($dataDetail['first_name'].' '.$dataDetail['last_name'])); echo '</td>
                        <td>'; echo($dataDetail['identification']); echo '</td>
                        <td>'; 
                                if(isset($dataDetail['charges'])){
                                    foreach ($dataDetail['charges'] as $iID => $dataCharges) { 
                                        echo(" - ".$dataCharges['charge']);
                                    }
                                }
                        
                       echo ' </td>
                        <td>'; echo(($data['start_date'])); echo '</td>
                        <td>'; echo(($data['end_date'])); echo '</td>
                        <td>'; echo(($data['living'])); echo '</td>
                        <td>'; echo(($data['first_prof'].' '.$data['last_prof'])); echo '</td>
                        <td>'; echo(($estado_inv)); echo '</td>
                        <td>'; echo($puntaje); echo '</td>
                        <td>'; echo($puntaje_e); echo '</td>
                        <td>'; echo($puntaje_h); echo '</td>
                        <td>'; echo(($result_org)); echo '</td>
                        <td>'; echo(($minimos)); echo '</td>
                        <td>'; echo(($maximos)); echo '</td>
                        <td>'; echo(($inscritos)); echo '</td>
                        <td></td>
                        <td>'; echo($data['duration_time']); echo '</td>
                        <td>'; echo($data['duration_time']/8); echo '</td>
                        <td>'; echo(($data['specialty']));  echo '</td>
                        <td>'; echo(($data['supplier']));  echo '</td>
                        <td>'; echo(($data['type']));  echo '</td>
                    </tr>';
                    
                    $puntaje = 0;
                    } 
                     if(isset($data['excusas'])){ foreach ($data['excusas'] as $iID => $dataDetail) {
                        
                       echo ' <tr>
                            <td>'; echo(($dataDetail['dealer'])); echo '</td>
                            <td>'; echo(($data['newcode'])); echo '</td>
                            <td>'; echo(($data['course'])); echo '</td>
                            <td>'; echo(($dataDetail['headquarter'])); echo '</td>
                            <td>'; echo(($dataDetail['area'])); echo '</td>
                            <td>'; echo(($dataDetail['zone'])); echo '</td>
                            <td>'; echo($dataDetail['first_name'].' '.$dataDetail['last_name']); echo '</td>
                            <td>'; echo($dataDetail['identification']); echo '</td>
                            <td>';

                                if(isset($dataDetail['charges'])){
                                    foreach ($dataDetail['charges'] as $iID => $dataCharges) { 
                                        echo(" - ".$dataCharges['charge']);
                                    }
                                }

                           echo ' </td>
                            <td>'; echo(($data['start_date'])); echo '</td>
                            <td>'; echo(($data['end_date'])); echo '</td>
                            <td>'; echo(($data['living'])); echo '</td>
                            <td>'; echo(($data['first_prof'].' '.$data['last_prof'])); echo '</td>
                            <td>No Asistio</td>
                            <td>'; echo($puntaje); echo '</td>
                            <td>'; echo($puntaje_e); echo '</td>
                            <td>'; echo($puntaje_h); echo '</td>
                            <td></td>
                            <td>'; echo(($minimos)); echo '</td>
                            <td>'; echo(($maximos)); echo '</td>
                            <td>'; echo(($inscritos)); echo '</td>
                            <td>Excusa</td>
                            <td>'; echo($data['duration_time']);echo '</td>
                            <td>'; echo($data['duration_time']/8);echo '</td>
                            <td>'; echo(($data['specialty'])); echo '</td>
                            <td>'; echo(($data['supplier'])); echo '</td>
                            <td>'; echo(($data['type'])); echo '</td>
                        </tr>';
                     }
                    }
                 } 
                } 
        echo '</tbody>
    </table>';
 }

/*
				$asistieron_tot = 0;
				$noasistieron_tot = 0;
				$aprobaron = 0;
				$noaprobaron = 0;
				$invitados = 0;
				$puntaje = 0;
				$aprobo = 0;

				//echo json_encode($datosCursos_all);

				// echo '{"resultado":"SI", "tabla":';

						foreach ($datosCursos_all as $iID => $data) {
						echo '	<div class="well">
								<table class="table table-invoice">
									<tbody>
										<tr>
											<td style="width: 30%;">
												<p class="lead">Sesión de entrenamiento</p>
												<h4>'; echo($data['newcode']).'|'; echo($data['course']); echo '</h4>
												<address class="margin-none">
													<strong>Tipo: </strong>'; echo($data['type']); echo '<br/>
													<strong>Duración: </strong>'; echo($data['duration_time']); echo '<br/>
													<strong>Profesor: </strong>'; echo $data['first_prof'].' '; echo $data['last_prof']; echo '<br/>
													<strong>Inicio: </strong>'; echo($data['start_date']); echo '<br />
													<strong>Final: </strong>'; echo($data['end_date']); echo '<br/>
													<strong>Lugar: </strong>'; echo(substr($data['living'],0,30)); echo '<br/>
													<strong>Dirección: </strong>'; echo(substr($data['address'],0,30)); echo '<br/>
													<strong>Ciudad: </strong>'; echo($data['city']).' | ';echo($data['schedule_id']);
												echo '</address>
											</td>
											<td class="right">
												<p class="lead">Asistencia</p>
												<!-- // Table -->
												<table class="table table-condensed table-vertical-center table-thead-simple">
													<thead>
														<tr>
															<th class="center">Alumno</th>
															<th class="center">Identificación</th>
															<th class="center">Cargo</th>
															<th class="center">Concesionario</th>
															<th class="center">Asistencia</th>
															<th class="center" style="width: 1%">Puntaje</th>
															<th class="center" style="width: 1%">ES</th>
															<th class="center" style="width: 1%">HR</th>
															<th class="center">Resultado</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">';
													
															$var_Ctrl = 0;
															foreach ($data['detail'] as $iID => $dataDetail) {
																$var_Ctrl++;
																$result_org = "Reprobó";
																$estado_inv = "Invitado";
																$puntaje_e = "0";
																$puntaje_h = "0";
																//print_r($dataDetail);
																if($dataDetail['estado']==1){
																	$invitados++;
																	$estado_inv = "Invitado";
																}elseif($dataDetail['estado']==2){
																	$asistieron_tot++;
																	$estado_inv = "Asistió";
																}else{
																	$noasistieron_tot++;
																	$estado_inv = "No Asitió";
																}
																if(isset($dataDetail['resultados'][0]['score'])){
																	$puntaje = $dataDetail['resultados'][0]['score'];
																	$puntaje_e = $dataDetail['resultados'][0]['score_review'];
																	$puntaje_h = $dataDetail['resultados'][0]['score_waybill'];
																	$aprobo = $dataDetail['resultados'][0]['approval'];
																	if($aprobo=="SI"){
																		$aprobaron++;
																		$result_org = "Aprobó";
																	}else{
																		$noaprobaron++;
																		$result_org = "Reprobó";
																	}
																}
														
															echo '<!-- Item -->
															<tr class="selectable">
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo(substr($dataDetail['first_name'],0,10).' '.substr($dataDetail['last_name'],0,10)); echo '</td>
																<td class="text-left important" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['identification']); echo '</td>
																<td class="center important" style="padding: 2px; font-size: 80%;">';
																
																	if(isset($dataDetail['charges'])){
																		foreach ($dataDetail['charges'] as $iID => $dataCharges) {
																			echo(" - ".$dataCharges['charge']);
																		}
																	}
																
																echo '</td>
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['headquarter']); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($estado_inv); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($puntaje); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($puntaje_e); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">'; echo($puntaje_h); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;"><span class="label label-primary">'; echo($result_org); echo '</span></td>
															</tr>
															<!-- // Item END -->';
														
														$puntaje = 0;
													}
														
														 if(isset($data['excusas'])){ foreach ($data['excusas'] as $iID => $dataDetail) {
															echo '<!-- Item -->
															<tr class="selectable">
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo(substr($dataDetail['first_name'],0,10).' '.substr($dataDetail['last_name'],0,10));echo '</td>
																<td class="text-left important" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['identification']); echo' </td>
																<td class="center important" style="padding: 2px; font-size: 80%;">';
																
																	if(isset($dataDetail['charges'])){
																		foreach ($dataDetail['charges'] as $iID => $dataCharges) {
																			echo(" - ".$dataCharges['charge']);
																		}
																	}
																
																echo '</td>
																<td class="text-left" style="padding: 2px; font-size: 80%;">'; echo($dataDetail['headquarter']); echo '</td>
																<td class="center" style="padding: 2px; font-size: 80%;">No Asistió</td>
																<td class="center" style="padding: 2px; font-size: 80%;"></td>
																<td class="center" style="padding: 2px; font-size: 80%;"></td>
																<td class="center" style="padding: 2px; font-size: 80%;"></td>
																<td class="center" style="padding: 2px; font-size: 80%;"><a href="../assets/excusas/'; echo($dataDetail['file']); echo '" target="_blank" class="label label-primary">Excusa</a></td>
															</tr>
															<!-- // Item END -->';
														 } }
													echo '</tbody>
												</table>
												<!-- // Table END -->
											</td>
										</tr>';
										 if(isset($data['asistencia'])){ 
										echo '<tr>
											<td class="center" colspan="2">';
												
												$Cant_Datos_var = count($data['asistencia']);
												$Porcentaje_Var = "0";
												if($Cant_Datos_var < 5){
													$Porcentaje_Var = $Cant_Datos_var*20;
												}else{
													$Porcentaje_Var = 100;
												}
												foreach ($data['asistencia'] as $iID => $dataAsistencia) { 
													echo '<input type="hidden" class="ValSche'; echo($data['schedule_id']); echo '" name="ValSche_'; echo($data['schedule_id']).'_'; echo($dataAsistencia['dealer_id']); echo '" id="ValSche_'; echo($data['schedule_id']).'_'; echo($dataAsistencia['dealer_id']); echo '" value="ValSche_'; echo($data['schedule_id']).'_'; echo($dataAsistencia['dealer_id']); echo '" data-deal="'; echo($dataAsistencia['dealer']); echo '" data-min="'; echo($dataAsistencia['min_size']); echo'" data-max="'; echo($dataAsistencia['max_size']); echo'" data-ins="'; echo($dataAsistencia['inscriptions']); echo '">';
												 } 
												echo '<p class="lead">Asignación de Cupos</p>
												<!-- Widget -->
												<div class="widget widget-heading-simple widget-body-gray" style="width:'; echo $Porcentaje_Var; echo '%;">
													<div class="widget-body">
														<input type="hidden" class="ElementID" id="Schedule'; echo($data['schedule_id']); echo '" name="Schedule'; echo($data['schedule_id']); echo'" value="'; echo($data['schedule_id']); echo '" />
														<!-- Ordered bars Chart -->
														<div id="chart_ordered_bars'; echo($data['schedule_id']); echo '" class="flotchart-holder" style="height: 300px;"></div>
													</div>
												</div>
												<!-- // Widget END -->
											</td>
										</tr>';
										 } 
									echo '</tbody>
								</table>
							</div>';
						 } */

	

}else if(isset($_GET['course_id'])){

	$_POST['course_id'] = $_GET['course_id'];
	$datosCursos_all = $PendientesCursos_Class->consultaDatosAll($_POST['cargo']);
	$cantidad_datos = count($datosCursos_all);


	

}else{

	include_once('models/rep_ajax.php');
	$PendientesCursos_Class = new RepPendientesCursos();
	$listaCursos = $PendientesCursos_Class->consultaCursos();
	$cantidad_cursos = count($listaCursos);
}



unset($PendientesCursos_Class);