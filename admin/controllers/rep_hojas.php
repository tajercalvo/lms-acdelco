<?php
include_once('models/rep_hojas.php');
$hojas_Class = new RepHojas();
$datosHojas = $hojas_Class->consultaDatos();
$cantidad_datos = 0;

if(isset($_POST['dealer_id']) || isset($_POST['dealer_id_opc']) ){
	if(!isset($_POST['dealer_id'])){
		$_POST['dealer_id'] = $_POST['dealer_id_opc'];
	}
	$datosHojas_all = $hojas_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosHojas_all);
}else if(isset($_GET['dealer_id'])){
	$_POST['dealer_id'] = $_GET['dealer_id'];
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosHojas_all = $hojas_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosHojas_all);
}
unset($hojas_Class);
