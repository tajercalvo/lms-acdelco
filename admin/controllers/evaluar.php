<?php
if (isset($_GET['review_id'])) {
	include_once('models/evaluar.php');
	$evaluar_Class = new evaluarlos();
	$review_id = $_GET['review_id'];
	$registroEvaluacion = $evaluar_Class->consultaEvaluacion($review_id);
	$consultaEvaluacionYa = $evaluar_Class->consultaEvaluacionYa($review_id);
}

if (isset($_POST['opcn'])) {
	include_once('../models/evaluar.php');
	$evaluar_Class = new evaluarlos();
	if ($_POST['opcn'] == "IniciarEvaluacion") {
		$partes = parse_url($_SERVER['HTTP_REFERER']);
		parse_str($partes['query'], $vars);
		$review_id = $vars['review_id'];
		$activity_schedule_id = $vars['activity_schedule_id'];

		$res_aux = $evaluar_Class->getActividad($activity_schedule_id, '../');
		$module_result_usr_id = @$res_aux['module_result_usr_id'];
		
		/* if ($module_result_usr_id == 0) {
			$array_res = array("resultado" => "NO", "msj" => "No existe module_result_usr_id, comuníquese con el equipo de soporte");
			echo json_encode($array_res);
			die();
		} */

		 if(empty($module_result_usr_id)) {
			$array_res = array("resultado" => "NOID", "msj" => "No puede presentar la evaluacion por que el modulo no se ha finalizado, comuníquese con el instructor");
			echo json_encode($array_res);
			die();
		} 


		$resultado = $evaluar_Class->CrearRespEvaluacion($review_id, $module_result_usr_id);
		if ($resultado > 0) {
			echo ('{"resultado":"SI","reviews_answer_id":"' . $resultado . '"}');
		} else {
			$array_res = array("resultado" => "NO", "msj" => "Usted no puede volver a presentar esta evaluación!<br>si cree que recibió este mensaje por error por favor comuníquese con el equipo de soporte");
			echo json_encode($array_res);
		}
	}
	if ($_POST['opcn'] == "ConsultarPregunta") {
		@session_start();
		$question_increment = $_POST['question_increment'];
		$available_score = $_POST['available_score'];
		/*$var = $question_increment-1 >= $available_score;
		echo($question_increment.':'.$available_score.':'.$var.':<br>');*/
		if ($question_increment >= $available_score) {
			$consultaEvaluacionYa = $evaluar_Class->consultaResultadoYa($_POST['reviews_answer_id']);
			$variable_pregunta = '<h4 class="innerTB margin-none half center">- LA EVALUACIÓN HA FINALIZADO -</h4><br>';
			$variable_pregunta .= '<div class="separator bottom"></div>';
			$variable_pregunta .= '<div class="row">
									 <div class="col-sm-12">
									 	<h5>Usted ha finalizado la evaluación, a partir de este momento puede seguir utilizando el sistema de forma normal en los demás módulos.</h5>
									 	<br><br>
									 </div>
								</div>';
			unset($_SESSION['id_evaluacion']);
			unset($_SESSION['name_evaluacion']);
			unset($_SESSION['_EvalCour_id_evaluacion']);
			unset($_SESSION['_EvalCour_ResultId']);
			unset($_SESSION['_EvalCour_Name']);
			unset($_SESSION['_EvalCour_Date']);
			unset($_SESSION['_EvalCour_Score']);
		} else {
			$variable_pregunta = "";
			$var_Preguntas = $_SESSION['EvaluacionCompleta'];
			$var_Ctrl = 0;
			$var_EnlazaPreguntas = "0";
			foreach ($var_Preguntas as $key => $preguntas) {
				$var_EnlazaPreguntas .= ',' . $preguntas['question_id'];
				$var_Ctrl++;
				$variable_pregunta .= '
				<div class="innerAll border-bottom">
					<div class="row glyphicons circle_question_mark primary"><i></i> <label class="text-success">' . $var_Ctrl . ' [' . $preguntas['agrup'] . '] ' . '. ' . $preguntas['question'] . '</label></div><br><br>';
				if (isset($preguntas['source']) && $preguntas['source'] != "") {
					$variable_pregunta .= '<img src="../assets/gallery/source/' . $preguntas['source'] . '" style="width:400px;" class="img-responsive center"><br>';
				}
				$variable_pregunta .= '<div class="separator bottom"></div>';
				$variable_pregunta .= '<div class="row">
						 <div class="col-sm-12">';
				$control_v = 0;
				foreach ($preguntas['AnswersData'] as $key => $Data_Answer) {
					if ($control_v == 0) {
						$variable_pregunta .= '<div>
										<label>';
						if ($Data_Answer['source'] != "") {
							$variable_pregunta .= '<img src="../assets/gallery/source/' . $Data_Answer['source'] . '" style="width:100px;" class="img-responsive center"><br>';
						}
						$variable_pregunta .= '<input type="radio" name="answer_selected' . $preguntas['question_id'] . '" id="answer_selected' . $Data_Answer['answer_id'] . '" value="' . $Data_Answer['answer_id'] . '" checked>
									     	' . $Data_Answer['answer'] . '
								  		</label>
									</div>';
					} else {
						$variable_pregunta .= '<div>
										<label>';
						if ($Data_Answer['source'] != "") {
							$variable_pregunta .= '<img src="../assets/gallery/source/' . $Data_Answer['source'] . '" style="width:100px;" class="img-responsive center"><br>';
						}
						$variable_pregunta .= '<input type="radio" name="answer_selected' . $preguntas['question_id'] . '" id="answer_selected' . $Data_Answer['answer_id'] . '" value="' . $Data_Answer['answer_id'] . '">
									     	' . $Data_Answer['answer'] . '
								  		</label>
									</div>';
					}
					$control_v++;
				}
				$variable_pregunta .= '</div></div></div>';
				//$variable_pregunta .= '<input type="hidden" id="question_id" name="question_id" value="'.$var_Preguntas['question_id'].'" />';
			}
			$variable_pregunta .= '
			<br><br>
					<input type="hidden" id="Questions_Register" name="Questions_Register" value="' . $var_EnlazaPreguntas . '" />
					<button type="submit" class="pull-right btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Responder</button>
					<div class="ajax-loading hide" id="loading_evaluacion">
						<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
					</div>
				';
		}

		echo ($variable_pregunta);
	} else if ($_POST['opcn'] == "ConsultarRespuesta") {
		@session_start();
		//echo(":::".$_POST['review_id'].":::");
		$variable_pregunta = '<h4 class="innerTB margin-none half center">- LA EVALUACIÓN HA FINALIZADO -</h4><br>';
		$variable_pregunta .= '<div class="separator bottom"></div>';
		$variable_pregunta .= '<div class="row">
									 <div class="col-sm-12">
									 	<h5>Usted ha finalizado la evaluación, a partir de este momento puede seguir utilizando el sistema de forma normal en los demás módulos.</h5>
									 	<br><br>
									 </div>
									</div>';
		$RegistrosEvaluacion = $evaluar_Class->RecordEvaluacion($_POST['review_id'], $_POST['reviews_answer_id']);
		$variable_pregunta .= '<div class="row">
									 <div class="col-sm-12">
										<table class="table table-condensed table-vertical-center table-thead-simple">
													<thead>
														<tr>
															<th class="center">Pregunta</th>
															<th class="center">Tu respuesta</th>
															<th class="center">Resultado</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">';
		if (isset($RegistrosEvaluacion)) {
			$Correctas = 0;
			$Incorrectas = 0;
			$Cant_Puntos = 0;
			foreach ($RegistrosEvaluacion as $iID => $dataRespuestas) {
				$Var_Label = "label-danger";
				$Respuesta_cor = '';
				if ($dataRespuestas['result'] == "SI") {
					$Var_Label = "label-primary";
					$Cant_Puntos = $Cant_Puntos + $dataRespuestas['valor'];
					$Correctas++;
				} else {
					$Respuesta_cor = $dataRespuestas['answer'];
					$Incorrectas++;
				}
				$variable_pregunta .= '<tr class="selectable">
						<td style="padding: 2px; font-size: 80%;" align="left">' . $dataRespuestas['code'] . ' ' . $dataRespuestas['question'] . '</td>
						<td class="important" style="padding: 2px; font-size: 80%;" align="left">' . $Respuesta_cor . '</td>
						<td class="center" style="padding: 2px; font-size: 80%;"><span class="label ' . $Var_Label . '">' . $dataRespuestas['result'] . '</span></td>
					</tr>';
			}
		}
		$variable_pregunta	.=			'</tbody>
											</table>
										</div>
									</div>';
		$variable_pregunta .= '<div class="row">
									 <div class="col-sm-12">
									 	<h5>Ha obtenido ' . $Correctas . ' Respuestas correctas y ' . $Incorrectas . ' Respuestas incorrectas; tu calificación es ' . $Cant_Puntos . ' de 100.</h5>
										 <br><br>
										 <a class="pull-left btn btn-danger" href="index.php">Salir</a>
									 </div>
									</div>';
		unset($_SESSION['id_evaluacion']);
		unset($_SESSION['name_evaluacion']);
		unset($_SESSION['_EvalCour_id_evaluacion']);
		unset($_SESSION['_EvalCour_ResultId']);
		unset($_SESSION['_EvalCour_Name']);
		unset($_SESSION['_EvalCour_Date']);
		unset($_SESSION['_EvalCour_Score']);
		echo ($variable_pregunta);
	}
	if ($_POST['opcn'] == "RegistrarRespuesta") {
		@session_start();

		$resJson = array();
		if (!isset($_SESSION['idUsuario'])) {
			$resJson['error'] = true;
			$resJson['msj'] = 'Su sesión se cerró, por favor acrualice la página, si el problema persiste comunícate con el departamento de soporte';
		} else {

			$Resp_Data = explode(',', $_POST['Questions_Register']);
			$resultado = 0;
			foreach ($Resp_Data as $val_resp_data) {
				if ($val_resp_data > 0) {
					$answer_selected = $_POST['answer_selected' . $val_resp_data];
					$datos_respuesta = $evaluar_Class->consultaRespuesta($answer_selected);
					$resultado = $evaluar_Class->CrearRespuesta($_POST['reviews_answer_id'], $val_resp_data, $answer_selected, $datos_respuesta['result'], $_POST['min_time'] . ':' . $_POST['seg_time'], $_POST['review_id']);
				}
			}

			//consultar el peso de la evaluación
			$partes = parse_url($_SERVER['HTTP_REFERER']);
			parse_str($partes['query'], $vars);
			$activity_schedule_id = $vars['activity_schedule_id'];

			$resultado = $evaluar_Class->actualizarNota($activity_schedule_id, $_POST['reviews_answer_id']);

			if ($resultado > 0) {
				$resJson['error'] = false;
				$resJson['msj'] = 'Su <strong>EVALUACIÓN</strong> ha sido almacenada correctamente (Encontrará en el mensaje inferior la retroalimentación a su evaluación), puedes seguir operando la plataforma normalmente, por ej: consulta en estudiante la nota que obtuviste';
			} else {
				$resJson['error'] = true;
				$resJson['msj'] = 'No se almacenó la información, por favor intenta nuevamente, si el problema persiste comunícate con el departamento de soporte';
			}
		}

		echo json_encode($resJson);
	}
}
unset($evaluar_Class);
