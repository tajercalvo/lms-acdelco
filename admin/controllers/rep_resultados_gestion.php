<?php

if( isset($_POST['opcn']) ){

    switch ( $_POST['opcn'] ) {
        case 'consulta':
            include_once('../models/rep_resultados_gestion.php');

            $p = $_POST;

            $cupos = ResultadoGestion::getCupos( $p );
            $excusas = ResultadoGestion::getExcusas( $p );
            $excusasCupos = ResultadoGestion::getExcusasCupos( $p );
            $users = ResultadoGestion::getUsuarios( $p );

            $cuenta = count( $cupos );
            $num_users = count( $users );

            for ($i=0; $i < $cuenta ; $i++) {

                $cupos[$i]['excusas'] = 0;
                $cupos[$i]['inscritos'] = 0;
                $cupos[$i]['asistentes'] = 0;

                foreach ( $excusas as $key => $e ) {
                    if( $cupos[$i]['dealer_id'] == $e['dealer_id'] ){
                        $cupos[$i]['excusas']++;
                    }
                }

                foreach ( $excusasCupos as $key => $ec ) {
                    if( $cupos[$i]['dealer_id'] == $ec['dealer_id'] ){
                        $cupos[$i]['excusas'] += intval($ec['num_invitation']);
                    }
                }

                foreach ($users as $key => $u) {
                    if( $cupos[$i]['dealer_id'] == $u['dealer_id'] ){
                        $cupos[$i]['inscritos']++;
                        if( $u['status_id'] == 2 ) {
                            $cupos[$i]['asistentes']++;
                        }
                    }
                }

            }


            echo json_encode( [ "error" => false, "message" => "OK", "data" => $cupos ] );
        break;
    }//fin switch

}else if( isset( $_GET['opcn'] ) ){
    switch ( $_GET['opcn'] ) {
        case 'descarga':
            include_once('models/rep_resultados_gestion.php');

            $p = $_GET;

            $cupos = ResultadoGestion::getCupos( $p, '' );
            $excusas = ResultadoGestion::getExcusas( $p, '' );
            $excusasCupos = ResultadoGestion::getExcusasCupos( $p, '' );
            $users = ResultadoGestion::getUsuarios( $p, '' );

            $cuenta = count( $cupos );
            $num_users = count( $users );

            for ($i=0; $i < $cuenta ; $i++) {

                $cupos[$i]['excusas'] = 0;
                $cupos[$i]['inscritos'] = 0;
                $cupos[$i]['asistentes'] = 0;

                foreach ( $excusas as $key => $e ) {
                    if( $cupos[$i]['dealer_id'] == $e['dealer_id'] ){
                        $cupos[$i]['excusas']++;
                    }
                }

                foreach ( $excusasCupos as $key => $ec ) {
                    if( $cupos[$i]['dealer_id'] == $ec['dealer_id'] ){
                        $cupos[$i]['excusas'] += intval($ec['num_invitation']);
                    }
                }

                foreach ($users as $key => $u) {
                    if( $cupos[$i]['dealer_id'] == $u['dealer_id'] ){
                        $cupos[$i]['inscritos']++;
                        if( $u['status_id'] == 2 ) {
                            $cupos[$i]['asistentes']++;
                        }
                    }
                }

            }// for

        break;
    }
}
