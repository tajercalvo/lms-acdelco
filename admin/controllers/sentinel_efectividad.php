<?php
	/*Parametros de configuracion POST */
		if(isset($_POST['ano_id_end'])){
			$year_end = $_POST['ano_id_end'];
		}else{
			$year_end = date("Y");
		}
		if(isset($_POST['mes_id_end'])){
			$month_end = $_POST['mes_id_end'];
		}else{
			$month_end = date("m");
		}
		if($month_end==1){
			$month_act = '07';
			$year_act_calc = $year_end-1;
		}elseif($month_end==2){
			$year_act_calc = $year_end-1;
			$month_act = '08';
		}elseif($month_end==3){
			$year_act_calc = $year_end-1;
			$month_act = '09';
		}elseif($month_end==4){
			$year_act_calc = $year_end-1;
			$month_act = 10;
		}elseif($month_end==5){
			$year_act_calc = $year_end-1;
			$month_act = 11;
		}elseif($month_end==6){
			$year_act_calc = $year_end-1;
			$month_act = 12;
		}elseif($month_end==7){
			$year_act_calc = $year_end;
			$month_act = '01';
		}elseif($month_end==8){
			$year_act_calc = $year_end;
			$month_act = '02';
		}elseif($month_end==9){
			$year_act_calc = $year_end;
			$month_act = '03';
		}elseif($month_end==10){
			$year_act_calc = $year_end;
			$month_act = '04';
		}elseif($month_end==11){
			$year_act_calc = $year_end;
			$month_act = '05';
		}elseif($month_end==12){
			$year_act_calc = $year_end;
			$month_act = '06';
		}

		$year_act = $year_end-1;

		$fec_inicialQ = $year_act.'-01-01';
		$fec_inicialQF = $year_act.'-12-31';

		$fec_finalQI = $year_end.'-01-01';
		$fec_finalQ = $year_end.'-12-31';
	/*Parametros de configuracion POST */
if(isset($_POST['opcn']) && $_POST['opcn']=='proc' ){
	include('../models/sentinel_efectividad.php');
	$efectividad_Class = new Efectividad();
	$AnoF = $_POST['ano_id_end'];
	$AnoI = $_POST['ano_id_end'];
	$MesF = $_POST['mes_id_end']+1;
	$MesC = $_POST['mes_id_end'];
	if($MesF==13){
		$MesF = "01";
		$AnoF = $AnoF+1;
	}
	$FecConsulta_F = $AnoF."-".$MesF."-01 00:00:00";
	$MesConsulta_I = date('m', strtotime($AnoI."-".$MesC."-01 00:00:00".' -6 month'));
	$AnoConsulta_I = date('Y', strtotime($AnoI."-".$MesC."-01 00:00:00".' -6 month'));
	$FecConsulta_I = $AnoConsulta_I.'-'.$MesConsulta_I.'-01 00:00:00';
	//$date_ctrl_month = date('m', strtotime('-6 month')) ;
	//echo($date_ctrl_month."::");
	//$FecConsulta_F = $year_end.'-'.$date_ctrl_month.'-01';
	//$FecConsulta_I = $year_act_calc.'-'.$month_act.'-01';
	//echo($FecConsulta_I.'::'.$FecConsulta_F);
	$ResCalculo = $efectividad_Class->CalcularEfectividadSentinel($FecConsulta_I,$FecConsulta_F,$AnoI,$MesC);
	/*$ConsultaListaConcesionarios = 
	$CantidadSentinel = 
	$CantidadReprocesados = 
	$CantidadBienPrimera = 
	$CantidadConPartes = 
	$CantidadPartesRec = 
	$CantidadPartesMenos10Dias = 
	$CantidadSentinelCalidad = */
	echo('{"resultado":"SI"}');
}else{
	include('models/sentinel_efectividad.php');
	$efectividad_Class = new Efectividad();
	$DatosSentinel_6Mes = $efectividad_Class->InfoSentinel6Mes($month_end,$year_end);
	$DatosSentinel_Todo = $efectividad_Class->InfoSentinelTodo($year_act_calc,$year_end);
	$DatosSentinelDetalle = $efectividad_Class->InfoSentinelDet($month_end,$year_end);
}
unset($efectividad_Class);