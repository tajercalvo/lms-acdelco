<?php
include_once('models/rep_pendientescurso.php');
$PendientesCursos_Class = new RepPendientesCursos();
$datosCursos = $PendientesCursos_Class->consultaDatos();
$cantidad_datos = 0;

if(isset($_POST['course_id'])){
	$datosCursos_all = $PendientesCursos_Class->consultaDatosAll($_POST['course_id']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['course_id'])){
	$_POST['course_id'] = $_GET['course_id'];
	$datosCursos_all = $PendientesCursos_Class->consultaDatosAll($_POST['course_id']);
	$cantidad_datos = count($datosCursos_all);
}

unset($PendientesCursos_Class);