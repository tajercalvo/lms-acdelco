<?php

if(isset($_POST['opcn'])){
	
	if($_POST['opcn']=="crear"){
		include_once('models/argumentarios.php');
		include_once('src/funciones_globales.php');//llamo la clase de funciones globales para guardar archivos
		$funciones = new Funciones();
		$Argumentarios_Class = new Argumentarios();

		$image_new = $funciones->guardarImagenes('image_new','IN','../assets/gallery/source/');
		$file_pdf = $funciones->guardarPDF('file_pdf','PF','../assets/gallery/source/');
		$img_top1 = $funciones->guardarImagenes('img_top1','I1','../assets/gallery/source/');
		$img_top2 = $funciones->guardarImagenes('img_top2','I2','../assets/gallery/source/');
		$img_top3 = $funciones->guardarImagenes('img_top3','I3','../assets/gallery/source/');
		$prominent = $funciones->guardarImagenes('prominent','PR','../assets/gallery/source/');
		//Proceso de creación para la imagen
		if(isset($_FILES['mp3_file'])){
		  if(isset($_FILES['mp3_file']['tmp_name'])){
		    $pdfantMp3 = "";
		    if(isset($_POST['pdfantMp3'])){
		      $pdfantMp3 = $_POST['pdfantMp3'];
		    }
		    $nom_archivoMp3 = $_FILES["mp3_file"]["name"];
		    $new_nameMp3 = "GM_LUDUS_4_";
		    $new_nameMp3 .= date("Ymdhis");
		    $new_extensionMp3 = "jpg";
		    preg_match("'^(.*)\.(mp3|MP3)$'i", $nom_archivoMp3, $extMp3);
		    $extMP3new = isset($extMp3[2]) ? $extMp3[2] : '' ;
		    switch (strtolower($extMP3new)) {
		      case 'mp3' : $new_extensionMp3 = ".mp3";
		        break;
		      case 'MP3' : $new_extensionMp3 = ".mp3";
		        break;
		      default    : $new_extensionMp3 = "no";
		        break;
		    }
		    if($new_extensionMp3 != "no"){
		      $new_nameMp3 .= $new_extensionMp3;
		      $resultArchivoMp3 = copy($_FILES["mp3_file"]["tmp_name"], "../assets/gallery/source/".$new_nameMp3);
		        if($resultArchivoMp3==1){
		          if($pdfantMp3 != "" && $pdfantMp3 != "default.mp3"){
		            if(file_exists("../assets/gallery/source/".$pdfantMp3)){
		              unlink("../assets/gallery/source/".$pdfantMp3);
		            }
		          }
		        }else{
		          $new_nameMp3 = "";
		        }
		    }else{
		      $new_nameMp3 = "";
		    }
		  }else{
		    $new_nameMp3 = "";
		  }
		}else{
		  $new_nameMp3 = "";
		}//fin crear archivo mp3
		$resultado = $Argumentarios_Class->CrearArgumentario($_POST['argument'],$_POST['argument_cat_id'],$_POST['characteristics'],$_POST['competition'],$_POST['accesories'],$_POST['media_id'],$image_new,$file_pdf,$_POST['commercial'],$_POST['versions'],$_POST['media_idv'],'','',$new_nameMp3,$_POST['description_principal'],$img_top1,$img_top2,$img_top3,$prominent);
		if(isset($_FILES['pdf_new']) && $_FILES['pdf_new']['size']>0){
			$pdf_new = $funciones->guardarPDF('pdf_new','P1','../assets/gallery/MaterialApoyo/');
			$img_pdf_new = $funciones->guardarImagenes('img_pdf_new','IP1','../assets/gallery/source/');
			$materialA1 = $Argumentarios_Class->guardarMaterialApoyo($resultado,$_POST['text_f1'],$pdf_new,$img_pdf_new);
		}
		if(isset($_FILES['pdf_new2']) && $_FILES['pdf_new2']['size']>0){
			$pdf_new2 = $funciones->guardarPDF('pdf_new2','P2','../assets/gallery/MaterialApoyo/');
			$img_pdf_new2 = $funciones->guardarImagenes('img_pdf_new2','IP2','../assets/gallery/source/');
			$materialA2 = $Argumentarios_Class->guardarMaterialApoyo($resultado,$_POST['text_f2'],$pdf_new2,$img_pdf_new2);
		}
		if(isset($_FILES['pdf_new3']) && $_FILES['pdf_new3']['size']>0){
			$pdf_ne3w = $funciones->guardarPDF('pdf_new3','P3','../assets/gallery/source/');
			$img_pdf_new3 = $funciones->guardarImagenes('img_pdf_new3','IP3','../assets/gallery/MaterialApoyo/');
			$materialA3 = $Argumentarios_Class->guardarMaterialApoyo($resultado,$_POST['text_f3'],$pdf_new,$img_pdf_new3);
		}
		header("Location: argumentarios.php");
	}elseif($_POST['opcn']=="editar"){
		include_once('models/argumentarios.php');
		$Argumentarios_Class = new Argumentarios();
		include_once('src/funciones_globales.php');//llamo la clase de funciones globales para guardar archivos
		/*----------------------------
		Juan Carlos Villar
		16 de junio de 2017
		Actualizar archivos relacionados con la biblioteca
		----------------------------*/
		$eliminarRelacion = $Argumentarios_Class->eliminar_relacion_libreria($_POST['argument_id']);

		if (isset($_POST['librarylibrary_id']) and is_array($_POST['librarylibrary_id']) and count($_POST['librarylibrary_id']) > 0) {
			foreach ($_POST['librarylibrary_id'] as $key => $value) {
				$Relacion = $Argumentarios_Class->crear_relacion_libreria($value, $_POST['argument_id']);
			}
		}
		
		
		/*-------------FIN---------------*/
		$funciones = new Funciones();
		//Proceso de creación para el archivo
		//Proceso de creación para el archivo de audio que contiene el comercial en mp3
		if(isset($_FILES['mp3_file'])){
			if(isset($_FILES['mp3_file']['tmp_name'])){
				$pdfantMp3 = "";
				if(isset($_POST['ant_new_nameMp3'])){
					$pdfantMp3 = $_POST['ant_new_nameMp3'];
				}
				$nom_archivoMp3 = $_FILES["mp3_file"]["name"];
				$new_nameMp3 = "GM_LUDUS_4_";
				$new_nameMp3 .= date("Ymdhis");
				$new_extensionMp3 = "jpg";
				preg_match("'^(.*)\.(mp3|MP3)$'i", $nom_archivoMp3, $extMp3);
				$extencion = isset($extMp3[2]) ? $extMp3[2] : "" ;
				switch (strtolower($extencion)) {
					case 'mp3' : $new_extensionMp3 = ".mp3";
						break;
					case 'MP3' : $new_extensionMp3 = ".mp3";
						break;
					default    : $new_extensionMp3 = "no";
						break;
				}
				if($new_extensionMp3 != "no"){
					$new_nameMp3 .= $new_extensionMp3;
					$resultArchivoMp3 = copy($_FILES["mp3_file"]["tmp_name"], "../assets/gallery/source/".$new_nameMp3);
						if($resultArchivoMp3==1){
							if($pdfantMp3 != "" && $pdfantMp3 != "default.mp3"){
								if(file_exists("../assets/gallery/source/".$pdfantMp3)){
									unlink("../assets/gallery/source/".$pdfantMp3);
								}
							}
						}else{
							$new_nameMp3 = "";
						}
				}else{
					$new_nameMp3 = "";
				}
			}else{
				$new_nameMp3 = "";
			}
		}else{
			$new_nameMp3 = "";
		}
		if(isset($_POST['ant_image_new']) && $_POST['ant_image_new'] != ""){$ant_image_new = $_POST['ant_image_new']; }else{ $ant_image_new = ""; }
		if(isset($_POST['ant_file_pdf']) && $_POST['ant_file_pdf'] != ""){$ant_file_pdf = $_POST['ant_file_pdf']; }else{ $ant_file_pdf = ""; }
		if(isset($_POST['ant_img_top1']) && $_POST['ant_img_top1'] != ""){$ant_img_top1 = $_POST['ant_img_top1']; }else{ $ant_img_top1 = ""; }
		if(isset($_POST['ant_img_top2']) && $_POST['ant_img_top2'] != ""){$ant_img_top2 = $_POST['ant_img_top2']; }else{ $ant_img_top2 = ""; }
		if(isset($_POST['ant_img_top3']) && $_POST['ant_img_top3'] != ""){$ant_img_top3 = $_POST['ant_img_top3']; }else{ $ant_img_top3 = ""; }
		if(isset($_POST['ant_prominent']) && $_POST['ant_prominent'] != ""){$ant_prominent = $_POST['ant_prominent']; }else{ $ant_prominent = ""; }
		//Proceso de creación para el archivo de audio que contiene el comercial en mp3
		$image_new = $funciones->guardarImagenes('image_new','IN','../assets/gallery/source/',$ant_image_new);
		$file_pdf = $funciones->guardarPDF('file_pdf','PF','../assets/gallery/source/',$ant_file_pdf);
		$img_top1 = $funciones->guardarImagenes('img_top1','I1','../assets/gallery/source/',$ant_img_top1);
		$img_top2 = $funciones->guardarImagenes('img_top2','I2','../assets/gallery/source/',$ant_img_top2);
		$img_top3 = $funciones->guardarImagenes('img_top3','I3','../assets/gallery/source/',$ant_img_top3);
		$prominent = $funciones->guardarImagenes('prominent','PR','../assets/gallery/source/',$ant_prominent);
		$resultado = $Argumentarios_Class->ActualizarArgumentario($_POST['argument_id'],$_POST['argument'],$_POST['argument_cat_id'],$_POST['characteristics'],$_POST['competition'],$_POST['accesories'],$_POST['media_id'],$image_new,$file_pdf,$_POST['commercial'],$_POST['versions'],$_POST['media_idv'],'','',$new_nameMp3,$_POST['description_principal'],$img_top1,$img_top2,$img_top3,$prominent,$_POST['estado']);
		//actualiza los archivos adjuntos para material de apoyo
		if(isset($_FILES['pdf_new']) && $_FILES['pdf_new']['size']>0){
			if(isset($_POST['ant_pdf1']) && $_POST['ant_pdf1'] != ""){
				$anteriorF = $Argumentarios_Class->archivoEspecifico($_POST['ant_pdf1']);
				$pdf_new = $funciones->guardarPDF('pdf_new','P1','../assets/gallery/MaterialApoyo/',$anteriorF['file_name']);
				$img_pdf_new = $funciones->guardarImagenes('img_pdf_new','IP1','../assets/gallery/source/',$anteriorF['img_file']);
				$materialA1 = $Argumentarios_Class->actualizarMaterialApoyo($_POST['ant_pdf1'],$_POST['text_f1'],$pdf_new,$img_pdf_new);
			}else{
				$pdf_new = $funciones->guardarPDF('pdf_new','P1','../assets/gallery/MaterialApoyo/');
				$img_pdf_new = $funciones->guardarImagenes('img_pdf_new','IP1','../assets/gallery/source/');
				$materialA1 = $Argumentarios_Class->guardarMaterialApoyo($_POST['argument_id'],$_POST['text_f1'],$pdf_new,$img_pdf_new);
			}
		}
		if(isset($_FILES['pdf_new2']) && $_FILES['pdf_new2']['size']>0){
			if(isset($_POST['ant_pdf2']) && $_POST['ant_pdf2'] != ""){
				$anteriorF = $Argumentarios_Class->archivoEspecifico($_POST['ant_pdf2']);
				$pdf_new = $funciones->guardarPDF('pdf_new2','P2','../assets/gallery/MaterialApoyo/',$anteriorF['file_name']);
				$img_pdf_new = $funciones->guardarImagenes('img_pdf_new2','IP2','../assets/gallery/source/',$anteriorF['img_file']);
				$materialA2 = $Argumentarios_Class->actualizarMaterialApoyo($_POST['ant_pdf2'],$_POST['text_f2'],$pdf_new,$img_pdf_new);
			}else{
				$pdf_new = $funciones->guardarPDF('pdf_new2','P2','../assets/gallery/MaterialApoyo/');
				$img_pdf_new = $funciones->guardarImagenes('img_pdf_new2','IP2','../assets/gallery/source/');
				$materialA2 = $Argumentarios_Class->guardarMaterialApoyo($_POST['argument_id'],$_POST['text_f2'],$pdf_new,$img_pdf_new);
			}
		}
		if(isset($_FILES['pdf_new3']) && $_FILES['pdf_new3']['size']>0){
			if(isset($_POST['ant_pdf3']) && $_POST['ant_pdf3'] != ""){
				$anteriorF = $Argumentarios_Class->archivoEspecifico($_POST['ant_pdf3']);
				print_r($anteriorF);
				$pdf_new = $funciones->guardarPDF('pdf_new3','P3','../assets/gallery/MaterialApoyo/',$anteriorF['file_name']);
				$img_pdf_new = $funciones->guardarImagenes('img_pdf_new3','IP3','../assets/gallery/source/',$anteriorF['img_file']);
				$materialA3 = $Argumentarios_Class->actualizarMaterialApoyo($_POST['ant_pdf3'],$_POST['text_f3'],$pdf_new,$img_pdf_new);
			}else{
				$pdf_new = $funciones->guardarPDF('pdf_new3','P3','../assets/gallery/MaterialApoyo/');
				$img_pdf_new = $funciones->guardarImagenes('img_pdf_new3','IP3','../assets/gallery/source/');
				$materialA3 = $Argumentarios_Class->guardarMaterialApoyo($_POST['argument_id'],$_POST['text_f3'],$pdf_new,$img_pdf_new);
			}
		}
		header("Location: argumentarios.php?id=".$_POST['argument_id']);
		//actualizar archivos material apoyo
	}elseif($_POST['opcn']=="LikeGalery"){
		include_once('../models/argumentarios.php');
		$Argumentarios_Class = new Argumentarios();
		$resultado = $Argumentarios_Class->CrearLike($_POST['news_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="pdfApoyo"){
		include_once('models/argumentarios.php');
		if(isset($_FILES['pdf_adjunto'])){
			if(isset($_FILES['pdf_adjunto']['tmp_name']) && $_FILES['pdf_adjunto']['tmp_name'] != ""){
				$titulo= $_POST['tittle_file'];
				$nombre = $_FILES['pdf_adjunto']['name'];
				$id = isset($_GET['id']) ? $_GET['id'] : "no";
				//opendir($carpeta);
				$new_namePDF = "GM_LUDUS_4_";
				$new_namePDF .= date("Ymdhis");
				$new_extensionPDF = "jpg";
				preg_match("'^(.*)\.(pdf|PDF|zip|ZIP)$'i", $nombre, $extPDF);
				switch (strtolower($extPDF[2])) {
					case 'pdf' : $new_extensionPDF = ".pdf";
						break;
					case 'PDF' : $new_extensionPDF = ".pdf";
						break;
					case 'zip' : $new_extensionPDF = ".zip";
						break;
					case 'ZIP' : $new_extensionPDF = ".zip";
						break;
					default    : $new_extensionPDF = "no";
						break;
				}
				if($new_extensionPDF != "no"){
					$new_namePDF .= $new_extensionPDF;
					$resultArchivo = copy($_FILES["pdf_adjunto"]["tmp_name"], "../assets/gallery/MaterialApoyo/".$new_namePDF);
					if($resultArchivo){
						$Argumentario_Class = new Argumentarios();
						$insert= $Argumentario_Class->guardarMaterialApoyo($id,$titulo,$new_namePDF);
					}
				}
			}
		}
		header("location: argumentarios.php?id=".$_GET['id']);
	}elseif($_POST['opcn']=="faqCreate"){
		include_once('models/argumentarios.php');
		$Argumentario_Class = new Argumentarios();
		$insert= $Argumentario_Class->guardarFaq($_GET['id'],$_POST['faq'],$_POST['faq_answer']);
	}elseif ($_POST['opcn']=="Good_practice") {
		include_once('../models/argumentarios.php');
		$Argumentario_Class = new Argumentarios();
		$insert= $Argumentario_Class->good_Practices($_POST['argument_id'],$_POST['good_practices']);
		if ($insert>0) {
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
		
	}elseif($_POST['opcn']=="aprobar"){
		include_once('../models/argumentarios.php');
		$Argumentarios_Class = new Argumentarios();
		$resultado = $Argumentarios_Class->actualizar_good_Practices($_POST['news_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="eliminar"){
		include_once('../models/argumentarios.php');
		$Argumentarios_Class = new Argumentarios();
		$resultado = $Argumentarios_Class->eliminar_good_Practices($_POST['news_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}
}
if(isset($_GET['id'])){
	include_once('models/argumentarios.php');
	$Argumentarios_Class = new Argumentarios();
	$Argumentario_datos = $Argumentarios_Class->consultaArgumentario($_GET['id']);
	$Galeria_datos = $Argumentarios_Class->consultaGaleria($_GET['id']);
	$Galeria_datosv = $Argumentarios_Class->consultaGaleriaV($_GET['id']);
	$Argumentarios_Catdatos = $Argumentarios_Class->consultaCatArgumentario();
	$Argumentarios_Meddatos = $Argumentarios_Class->consultaMedArgumentario('1');
	$Argumentarios_MeddatosV = $Argumentarios_Class->consultaMedArgumentario('2');
	$archivos = $Argumentarios_Class->consultaMaterialApoyo($_GET['id']);
	$preguntasFAQ = $Argumentarios_Class->consultaFaqs($_GET['id']);
	$accesorios = explode("<br>",$Argumentario_datos['accesories']);
	$Buentaspracticas = $Argumentarios_Class->ludus_argument_good_practices($_GET['id']);
	$relacionadadConBiblioteca = $Argumentarios_Class->relacionadadConBiblioteca($_GET['id']);
	
}


if( !isset($_POST['opcn']) && !isset($_GET['opcn']) ){
	include_once('models/argumentarios.php');
	$Argumentarios_Class = new Argumentarios();
	$Argumentarios_datos = $Argumentarios_Class->consultaArgumentarios();
	$Argumentarios_Catdatos = $Argumentarios_Class->consultaCatArgumentario();
	$Argumentarios_Meddatos = $Argumentarios_Class->consultaMedArgumentario('1');
	$Argumentarios_MeddatosV = $Argumentarios_Class->consultaMedArgumentario('2');
	$biblioteca = $Argumentarios_Class->consultaLibraries();

}

unset($Argumentarios_Class);
