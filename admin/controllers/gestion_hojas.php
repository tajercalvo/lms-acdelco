<?php
if(isset($_GET['opcn'])){
	include_once('models/gestion_hojas.php');
	$ghojas_Class = new GestionHojas();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $ghojas_Class->consultaRegistro($_GET['id']);
		$registroFeedback = $ghojas_Class->consultaFeedback($_GET['id']);
		$indice = count($registroFeedback)-1;
		if(isset($registroFeedback[$indice]['waybills_feedback_id'])){
			$ultimoFeedback = $registroFeedback[$indice]['waybills_feedback_id'];
			$registro = $registroFeedback[$indice]['feedback_r'];
		}else{
			$ultimoFeedback = 0;
		}
	}else{
		$registroConfiguracion['nombre_lista'] = "";
	}
}elseif(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/gestion_hojas.php');
	$ghojas_Class = new GestionHojas();
	if($_POST['opcn']=="feedback_coor") {
		if(isset($_POST['id_fb'])){
			//se ingresa la respuesta del asesor a un feedback anterior
			$resultado = $ghojas_Class->insertarRespuestaFeedback($_POST['feedbackText'],$_POST['id_fb']);
			if($resultado > 0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		}else{
			$resultado = $ghojas_Class->insertarFeedback($_POST['id_pj'],$_POST['feedbackText']);
			if($resultado){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		}//fin opcion coordinador / asesor
	}//fin opcn == feedback
}elseif(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas hojas
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.newcode,c.course';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.newcode,c.course '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='1') {
    		$sOrder = ' ORDER BY t.first_name, t.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='2') {
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY h.date_creation '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY h.score '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY h.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/gestion_hojas.php');
	$ghojas_Class = new GestionHojas();
	$datosConfiguracion = $ghojas_Class->consultaDatoshojas($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $ghojas_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-danger">Pendiente</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-info">Diligenciado</span>';
    	}else{
    		$val_estado = '<span class="label label-success">Gestionado</span>';
    	}
    	$valOpciones = '<a href="op_gestionhoja.php?opcn=ver&id='.$aInfo['waybill_id'].'&course='.$aInfo['newcode'].' | '.$aInfo['course'].'" class="btn-action glyphicons eye_open btn-success"><i></i></a>';
    	$aItem = array(
			$aInfo['newcode'].' | '.$aInfo['course'],
			$aInfo['first_prof'].' '.$aInfo['last_prof'],
			$aInfo['first_name'].' '.$aInfo['last_name'],
			$aInfo['date_creation'],
			$aInfo['score'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['waybill_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/gestion_hojas.php');
	$ghojas_Class = new GestionHojas();
	$cantidad_datos = $ghojas_Class->consultaCantidad();
}
unset($ghojas_Class);
