<?php
if(isset($_POST['opcn'])){
	//operaciones con los datos
	if($_POST['opcn']=="editar"){
		include_once('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		// print_r( $_POST );
		if(isset($_POST['status_id'])&&$_POST['status_id']>0){
			$status_id = $_POST['status_id'];
		}else{
			$status_id = isset($_POST['status_id_ant']) ? $_POST['status_id_ant'] : 2 ;
		}
		if(isset($_POST['restablece'])){
			$restablece = $_POST['restablece'];
		}else{
			$restablece = "NO";
		}
		$Var_Iden = trim($_POST['identification']);
		$Var_Iden = str_replace(' ', '', $Var_Iden);
		$n_correo = strtolower(trim($_POST['email']));
		// $n_correo = str_replace(' ', '', $n_correo);

		// $zone_id = isset( $_POST['zone_id'] ) ? implode(",", $_POST['zone_id'] ) : "0" ;
		if( isset( $_POST['zone_id'] ) ){
			Usuarios::eliminaCoordinadoresCcs( $_POST['user_id'] );
			foreach ($_POST['zone_id'] as $key) {
				Usuarios::actualizarCoordinadoresCcs( ["user_id"=>$_POST['user_id'],"dealer_id"=>$key ] );
			}
		}

		if( $n_correo != "" && $n_correo != "no tiene" ){
			$correo = $usuario_Class->getCorreo( $n_correo, "../" );
		}else{
			$correo =[];
			$n_correo = "No tiene";
		}
		if( isset( $correo['error'] )  ){
			//si no es un correo valido retorna este error
			echo( json_encode( $correo ) );

		}else if( isset( $correo['email'] ) && isset( $correo['identification'] ) && $correo['identification'] != $Var_Iden ){
			//si ya viene un correo dentro de la consulta, retorna error
			echo( json_encode( ['error' => true,'mensaje' => 'El correo que esta ingresando ya esta registrado'] ) );
		}else{
			//si no vienen correos en la consulta, el correo es valido

			$resultado = $usuario_Class->actualizaUsuario_all($_POST['first_name'],$_POST['last_name'],$_POST['date_birthday'],$_POST['user_id'],$n_correo,$_POST['phone'],$_POST['headquarter_id'],$status_id,$restablece,$Var_Iden,$_POST['gender'],$_POST['education'],$_POST['empresa'],$_POST['pais']);
			/*Asigna cargos y roles*/
			if( $resultado > 0 ){
				if(isset($_POST['roles'])){
					$resu = $usuario_Class->BorraRoles($_POST['user_id']);
					if( count( $_POST['roles'] ) > 0 ){
						foreach ($_POST['roles'] as $iID => $roles_selec) {
							$resu = $usuario_Class->CrearRoles($_POST['user_id'],$roles_selec);
						}
					}
					$res_cuenta = $usuario_Class->resetZonas( $_POST['user_id'] );
				}
				if(isset($_POST['cargos'])){
					$resu = $usuario_Class->BorraCargos($_POST['user_id']);
					if( count( $_POST['cargos'] ) > 0 ){
						foreach ($_POST['cargos'] as $iID => $cargos_selec) {
							$resu = $usuario_Class->CrearCargos($_POST['user_id'],$cargos_selec);
						}
					}
				}/*Asigna cargos y roles*/
				echo( json_encode( ['error' => false,'mensaje' => 'La operación ha sido completada satisfactoriamente'] ) );
			}else{
				echo( json_encode( ['error' => true,'mensaje' => 'No ha sido posible actualizar la información del usuario, por favor valide que el número de documento no este duplicado'] ) );
			}

		}
		unset($usuario_Class);

	}elseif($_POST['opcn']=="crear"){
		include_once('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		$Var_Iden = trim($_POST['identification']);
		$Var_Iden = str_replace(' ', '', $Var_Iden);
		$n_correo = trim($_POST['email']);
		$n_correo = str_replace(' ', '', $n_correo);
		if( $n_correo != "" && $n_correo !="No tiene" ){
			$correo = $usuario_Class->getCorreo( $n_correo, "../" );
		}else{
			$correo =[];
		}
		if( isset( $correo['error'] ) ){
			//si no es un correo valido retorna este error
			echo( json_encode( $correo ) );
		}else if( isset( $correo['email'] ) ){
			//si ya viene un correo dentro de la consulta, retorna error
			echo( json_encode( ['error' => true,'mensaje' => 'El correo que esta ingresando ya esta registrado'] ) );
		}else{
			//si no vienen correos en la consulta, el correo es valido
			$id_usuario = $usuario_Class->creaUsuario_all($_POST['first_name'],$_POST['last_name'],$_POST['date_birthday'],$Var_Iden,$n_correo,$_POST['phone'],$_POST['headquarter_id'],$_POST['gender'],$_POST['education'],$_POST['empresa'],$_POST['pais'] );
			if($id_usuario>0){
				/*Asigna cargos y roles*/
				if(isset($_POST['roles'])){
					foreach ($_POST['roles'] as $iID => $roles_selec) {
						$resu = $usuario_Class->CrearRoles($id_usuario,$roles_selec);
					}
				}else{
					$resu = $usuario_Class->CrearRoles($id_usuario,'2');
				}
				foreach ($_POST['cargos'] as $iID => $cargos_selec) {
					$resu = $usuario_Class->CrearCargos($id_usuario,$cargos_selec);
				}
				/*Asigna cargos y roles*/
				echo( json_encode( ['error' => false,'mensaje' => 'La operación ha sido completada satisfactoriamente'] ) );
			}else{
				echo( json_encode( ['error' => true,'mensaje' => 'El usuario ya se encuentra creado en otro CONCESIONARIO, comuníquese con el administrador para que le asigne dicho USUARIO'] ) );

			}
			unset($usuario_Class);

		}


	}elseif($_POST['opcn']=="RegNav"){
		include_once('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		$section = $_POST['section'];
		$section_arr = explode("#",$section);
		$section = $section_arr[0];
		$_varUrlNav = str_replace('http://localhost/ludus/admin/', '', $section);
		$_varUrlNav = str_replace('http://www.gmacademy.co/', '', $section);
		$_varUrlNav = str_replace('http://www.gmacademy.co/admin/', '', $section);

		$_varUrlNav = str_replace('http://gmacademy.co//', '', $section);
		$_varUrlNav = str_replace('http://gmacademy.co/admin/', '', $section);
		$_varUrlNav = str_replace('https://gmacademy.co//', '', $section);
		$_varUrlNav = str_replace('https://gmacademy.co/admin/', '', $section);


		$_varUrlNav = str_replace('https://gmacademy.luduscolombia.com.co/admin/', '', $_varUrlNav);
		$_varUrlNav = str_replace('http://gmacademy.luduscolombia.com.co/admin/', '', $_varUrlNav);
		$_varUrlNav = str_replace('https://gmacademy.luduscolombia.com.co/admin/gmacademy/', '', $_varUrlNav);
		$_varUrlNav = str_replace('http://gmacademy.luduscolombia.com.co/admin/gmacademy/', '', $_varUrlNav);
		$_varUrlNav = str_replace('https://www.gmacademy.luduscolombia.com.co/admin/', '', $_varUrlNav);
		$_varUrlNav = str_replace('http://www.gmacademy.luduscolombia.com.co/admin/', '', $_varUrlNav);
		$_varUrlNav = str_replace('https://www.gmacademy.luduscolombia.com.co/admin/gmacademy/', '', $_varUrlNav);
		$_varUrlNav = str_replace('http://www.gmacademy.luduscolombia.com.co/admin/gmacademy/', '', $_varUrlNav);
		$_varUrlNav = str_replace('https://gmacademy.luduscolombia.com.co/gmacademy/admin/', '', $_varUrlNav);
		$_varUrlNav = str_replace('http://gmacademy.luduscolombia.com.co/gmacademy/admin/', '', $_varUrlNav);
		$_varUrlNav = str_replace('admin/', '', $section);

		if($_varUrlNav==""){
			$_varUrlNav = "index.php";
		}
		$_varUrlNavArr = explode("?", $_varUrlNav);
		$_valElId = "0";
		$_varUrlElement = str_replace('?','',$_POST['element_id']);
		$_varUrlElementArr = explode("&", $_varUrlElement);
		foreach ($_varUrlElementArr as $key => $value) {
			$pos = strpos($value, 'id=');
			if($pos != ''){
				$_valElId = "0";
			}else{
				$_valElId = str_replace('id=','',$value);
			}
		}
		$usuario_res = $usuario_Class->crea_Navegacion($_varUrlNavArr[0],$_valElId,'',$_POST['time_navigation']);
		unset($usuario_Class);
		echo('{"resultado":"SI"}');
	}elseif($_POST['opcn']=="filtrar"){
		/* OK - Filtro completo*/
		include('models/usuarios.php');
		$usuario_Class = new Usuarios();
		if($_POST['charge_id']!='0'){
			$_SESSION['charge_id_usr_flr'] = $_POST['charge_id'];
		}else{
			if(isset($_SESSION['charge_id_usr_flr'])){
				unset($_SESSION['charge_id_usr_flr']);
			}
		}
		if($_POST['rol_id']!='0'){
			$_SESSION['rol_id_usr_flr'] = $_POST['rol_id'];
		}else{
			if(isset($_SESSION['rol_id_usr_flr'])){
				unset($_SESSION['rol_id_usr_flr']);
			}
		}
		if($_POST['status_id']!='0'){
			$_SESSION['status_id_usr_flr'] = $_POST['status_id'];
		}else{
			if(isset($_SESSION['status_id_usr_flr'])){
				unset($_SESSION['status_id_usr_flr']);
			}
		}
		if($_POST['headquarter_id']!='0'){
			$_SESSION['headquarter_id_usr_flr'] = $_POST['headquarter_id'];
		}else{
			if(isset($_SESSION['headquarter_id_usr_flr'])){
				unset($_SESSION['headquarter_id_usr_flr']);
			}
		}

		if(isset($_POST['dealer_id']) && $_POST['dealer_id']!='0'){
			$_SESSION['dealer_id_usr_flr'] = $_POST['dealer_id'];
		}else{
			if(isset($_SESSION['dealer_id_usr_flr'])){
				unset($_SESSION['dealer_id_usr_flr']);
			}
		}

		if($_POST['type_user_id']!='0'){
			$_SESSION['type_user_id_usr_flr'] = $_POST['type_user_id'];
		}else{
			if(isset($_SESSION['type_user_id_usr_flr'])){
				unset($_SESSION['type_user_id_usr_flr']);
			}
		}
		if( $_POST['no_time_ev'] == '1' ){
			$_SESSION['no_time_ev_usr_flr'] = $_POST['no_time_ev'];
		}else{
			if(isset($_SESSION['no_time_ev_usr_flr'])){
				unset($_SESSION['no_time_ev_usr_flr']);
			}
		}
		$cantidad_datos = $usuario_Class->consultaCantidad('');
		$datosCargos_ini = $usuario_Class->consultaAllDatosCargos('','');
		$datosRoles_ini = $usuario_Class->consultaAllDatosRoles('','');
		$datosConcesionarios_ini = $usuario_Class->consultaAllDatosConcesionarios('');
		$datosConce_Gral = $usuario_Class->consultaAllDatosConcesionariosAll('');
		$type_user = $usuario_Class->tipoUsuarios();
		unset($usuario_Class);
		/* OK - Filtro completo*/
	}elseif($_POST['opcn']=="CrearImagen"){
		include('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		if(isset($_FILES['image_new'])){
			if(isset($_FILES['image_new']['tmp_name'])){
				$imgant = $_POST['imgant'];
				$user_id = $_POST['user_id'];
				$nom_archivo1 = $_FILES["image_new"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../../assets/images/usuarios/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../../assets/images/usuarios/".$imgant)){
									unlink("../../assets/images/usuarios/".$imgant);
								}
							}
							$op_cursoImagen = $usuario_Class->actualizaImagen($user_id,$new_name1);
							if($user_id == $_SESSION['idUsuario']){
								$_SESSION['imagen'] = $new_name1;
							}
							echo('{"resultado":"SI","archivo":"'.$new_name1.'"}');
						}else{
							echo('{"resultado":"NO"}');
						}
				}else{
					echo('{"resultado":"NO"}');
				}
			}else{
				echo('{"resultado":"NO"}');
			}
		}
		unset($usuario_Class);
	}elseif($_POST['opcn']=="BuscarUsuario"){
		include('../models/usuarios.php');
		$usuario_Class = new Usuarios();
		$Var_Iden = trim($_POST['usr_iden']);
		$Var_Iden = str_replace(' ', '', $Var_Iden);
		$datosUsuario = $usuario_Class->consultaUsuarioSel($Var_Iden);
		if(isset($datosUsuario['first_name'])){
			echo('{"resultado":"SI","nombres":"'.$datosUsuario['first_name'].' '.$datosUsuario['last_name'].'","concesionario":"'.$datosUsuario['headquarter'].'"}');
		}else{
			echo('{"resultado":"NO"}');
		}
		unset($usuario_Class);
	}elseif($_POST['opcn']=="ejecutar"){
		include('models/usuarios.php');
		$usuario_Class = new Usuarios();
		// print_r( $_POST );
		if( $_POST['identification_list'] != "" ){
			$query = "SELECT * FROM ludus_users WHERE identification IN ( {$_POST['identification_list']} )";
			$usuarios = Usuarios::query( $query, '' );
			$ProcesoUsuario = $usuario_Class->ProcesoMasivo($_POST['action'], $_POST['identification_list'] );
		}

		$cantidad_datos = $usuario_Class->consultaCantidad('');
		unset($usuario_Class);
	}
	/*
	fin validacion POST
	*/
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	/* OK - Consulta la información completa*/
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY u.identification DESC';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY u.identification '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY u.image '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY u.first_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='4'){
    		$sOrder = ' ORDER BY u.first_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='5'){
    		$sOrder = ' ORDER BY d.dealer '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='6'){
    		$sOrder = ' ORDER BY u.empresa '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='7'){
    		$sOrder = ' ORDER BY c.headquarter '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='8'){
    		$sOrder = ' ORDER BY u.email '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='9'){
    		$sOrder = ' ORDER BY u.phone '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='10'){
    		$sOrder = ' ORDER BY u.date_birthday '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='11'){
    		$sOrder = ' ORDER BY u.gender '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY s.status '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/usuarios.php');
	$usuario_Class = new Usuarios();

	$datosUsuarios = $usuario_Class->consultaDatosUsuarios($sWhere,$sOrder,$sLimit);
	//print_r($datosUsuarios);
	$cantidad_datos = $usuario_Class->consultaCantidadFilter('js',$sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosUsuarios),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosUsuarios as $iID => $aInfo) {
    	$confirmado = $aInfo['confirmed'];
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}else{
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}
    	$valOpciones = '<a href="ver_perfil.php?back=usuarios&id='.$aInfo['user_id'].'" class="btn-action glyphicons eye_open btn-default"><i></i></a><br>';
		if($_SESSION['max_rol']>=3){
			$valOpciones .= '<a href="op_usuarios.php?back=usuarios&opcn=editar&id='.$aInfo['user_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
		}
		if( $aInfo['image']=="" || $aInfo['image']==" "){
			$imagen = '<img src="../assets/images/usuarios/default.png" style="width: 40px;" alt="'.$aInfo['first_name'].' '.$aInfo['last_name'].'">';
		}else{
			$imagen = '<img src="../assets/images/usuarios/'.$aInfo['image'].'" style="width: 40px;" alt="'.$aInfo['first_name'].' '.$aInfo['last_name'].'">';
		}

		$cargos_det = "";
		foreach ($aInfo['cargos'] as $iID => $aCarg) {
			$cargos_det .= "<i class='fa fa-fw icon-identification'></i> ".$aCarg['charge']."<br>";
		}
		if(strlen($aInfo['email'])>15){
			$email_tam = substr($aInfo['email'], 0, 15);
			$email_tam .= '<br>'.substr($aInfo['email'], 15, strlen($aInfo['email']));
		}else{
			$email_tam = $aInfo['email'];
		}

		$gender = 'No Definido';
		if($aInfo['gender']=='1'){
			$gender = 'Masculino';
		}else if($aInfo['gender']=='2'){
			$gender = 'Femenino';
		}
		if($confirmado==1){
			$aItem = array(
	    		$aInfo['identification'],
				$imagen,
				$aInfo['last_name'],
				$aInfo['first_name'],
				$cargos_det,
				$aInfo['dealer'],
				$aInfo['empresa'],
				$aInfo['headquarter'],
				'<span class="label label-success">'.$email_tam.'</span>',
				$aInfo['phone'],
				$aInfo['date_birthday'],
				$gender,
				$val_estado,
				$valOpciones,
				'DT_RowId' => $aInfo['user_id']
			);
		}else{
			$aItem = array(
	    		'<span class="label label-danger">'.$aInfo['identification'].'</span>',
				$imagen,
				'<span class="label label-danger">'.$aInfo['last_name'].'</span>',
				'<span class="label label-danger">'.$aInfo['first_name'].'</span>',
				$cargos_det,
				$aInfo['dealer'],
				$aInfo['empresa'],
				$aInfo['headquarter'],
				'<span class="label label-danger">'.$email_tam.'</span>',
				$aInfo['phone'],
				$aInfo['date_birthday'],
				$gender,
				$val_estado,
				$valOpciones,
				'DT_RowId' => $aInfo['user_id']
			);
		}

		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
    unset($usuario_Class);
    /* OK - Consulta la información completa*/
}else{
	/* OK - Consulta la información inicial*/
	include('models/usuarios.php');
	$usuario_Class = new Usuarios();
	$cantidad_datos = $usuario_Class->consultaCantidad('');
	$datosCargos_ini = $usuario_Class->consultaAllDatosCargos('','');
	$datosRoles_ini = $usuario_Class->consultaAllDatosRoles('','');
	$datosConcesionarios_ini = $usuario_Class->consultaAllDatosConcesionarios('');
	$datosConce_Gral = $usuario_Class->consultaAllDatosConcesionariosAll('');
	$type_user = $usuario_Class->tipoUsuarios();
	unset($usuario_Class);
	/* OK - Consulta la información inicial*/
}
