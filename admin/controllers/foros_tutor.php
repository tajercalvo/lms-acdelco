<?php
if(isset($_POST['opcn'])){
	if($_POST['opcn']=="crear"){
		include_once('models/foros_tutor.php');
		$Foros_Class = new Foros();
		$resultado = $Foros_Class->CrearForo($_POST['forum'],$_POST['forum_description'],$_POST['category'],$_POST['position']);
		include_once('models/usuarios.php');
		if(!isset($usuario_Class)){
			$usuario_Class = new Usuarios();
		}
		$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario']);
		$cargos_id = "0";
		foreach ($charge_user as $key => $Data_Charges) {
			$cargos_id .= ",".$Data_Charges['charge_id'];
		}
		unset($usuario_Class);
		$Foro_datos = $Foros_Class->consultaForos($cargos_id);
		if($resultado>0){
			foreach ($_POST['cargos'] as $iID => $cargos_selec) {
				$resu = $Foros_Class->CrearCargos($resultado,$cargos_selec);
			}
		}
	}elseif($_POST['opcn']=="editar"){
		include_once('models/foros_tutor.php');
		$Foros_Class = new Foros();
		$resultado = $Foros_Class->ActualizarForo($_POST['forum_id'],$_POST['forum_ed'],$_POST['forum_description_ed'],$_POST['category_ed'],$_POST['estado'],$_POST['position_ed']);
		if(isset($_POST['cargos_ed'])){
			$resu = $Foros_Class->BorraCargos($_POST['forum_id']);
			foreach ($_POST['cargos_ed'] as $iID => $cargos_selec) {
				$resu = $Foros_Class->CrearCargos($_POST['forum_id'],$cargos_selec);
			}
		}
	}elseif($_POST['opcn']=="LikeReply"){
		include_once('../models/foros_tutor.php');
		$Foros_Class = new Foros();
		$resultado = $Foros_Class->CrearLike($_POST['forum_reply_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="enviar"){
		include_once('../models/foros_tutor.php');
		$Foros_Class = new Foros();
		$resultado = $Foros_Class->CrearReply($_POST['forum_id'],$_POST['comentario']);
		if($resultado>0){
			echo('{"resultado":"SI","MaxId":"'.$resultado.'"}');
		}
		unset($Foros_Class);
	}elseif($_POST['opcn']=="ProcesoAct"){
		include_once('models/foros_tutor.php');
		$Foros_Class = new Foros();
		$resultado = $Foros_Class->StatusPost($_POST['forum_reply_id'],$_POST['status_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="comments"){
		include_once('../models/foros_tutor.php');
		$Foros_Class = new Foros();
		$resultado = $Foros_Class->ConsultaMensajes($_POST['forum_id'],$_POST['MaxIdReply']);
		$mostrar = "";
		if(count($resultado)>0){

			foreach ($resultado as $key => $msg) {
				$mostrar .= '<div class="media border-bottom margin-none">
					<div class="media-body">
						<small class="label label-default">'.$msg['date_creation'].' - '.$msg['first_name'].' '.$msg['last_name'].':</small> <span>'.$msg['forum_reply'].'</span><br>
					</div>
				</div>';
				$mostrar .= '<input type="hidden" class="MaxIdReply" name="MaxIdReply" id="MaxIdReply" value="'.$msg['forum_reply_id'].'">';

			}
			echo($mostrar);
		}else{
			echo("");
		}
	}
}
if(isset($_GET['id'])){
	include_once('models/foros_tutor.php');
	$Foros_Class = new Foros();
	$Foro_datos = $Foros_Class->consultaForo($_GET['id']);
	$Foros_Reply = $Foros_Class->consultaReply($_GET['id']);
	if(isset($_POST['opcn'])){
		include_once('models/usuarios.php');
		if(!isset($usuario_Class)){
			$usuario_Class = new Usuarios();
		}
		$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
		unset($usuario_Class);	
	}
}

/*if( !isset($_GET['opcn']) && ( isset($_POST['opcn']) && ($_POST['opcn']!="LikeReply" && $_POST['opcn']!="ProcesoAct") ) ){
	include_once('models/foros_tutor.php');
	$Foros_Class = new Foros();
	include_once('models/usuarios.php');
	if(!isset($usuario_Class)){
		$usuario_Class = new Usuarios();
	}
	$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario']);
	$cargos_id = "0";
	foreach ($charge_user as $key => $Data_Charges) {
		$cargos_id .= ",".$Data_Charges['charge_id'];
	}
	unset($usuario_Class);
	$Foros_datos = $Foros_Class->consultaForos($cargos_id);
}*/

if( !isset($_GET['opcn']) && !isset($_POST['opcn']) ){
	include_once('models/foros_tutor.php');
	$Foros_Class = new Foros();
	include_once('models/usuarios.php');
	if(!isset($usuario_Class)){
		$usuario_Class = new Usuarios();
	}
	$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario']);
	$cargos_id = "0";
	foreach ($charge_user as $key => $Data_Charges) {
		$cargos_id .= ",".$Data_Charges['charge_id'];
	}
	$Foros_datos = $Foros_Class->consultaForos($cargos_id);
	$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
	unset($usuario_Class);
}
unset($Foros_Class);