<?php
if(isset($_GET['opcn'])){
	include_once('models/escalamientos.php');
	$Escalamientos_Class = new Escalamientos();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $Escalamientos_Class->consultaRegistro($_GET['id']);
		$listadosModelos = $Escalamientos_Class->ListadoModelos();
		$listadosUsuarios = $Escalamientos_Class->listadosUsuarios();
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		$listadosModelos = $Escalamientos_Class->ListadoModelos();
		$listadosUsuarios = $Escalamientos_Class->listadosUsuarios();
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/escalamientos.php');
	$Escalamientos_Class = new Escalamientos();
	if($_POST['opcn']=="crear"){
		$resultado = $Escalamientos_Class->CrearEscalamientos($_POST['sentinel_model_id'],$_POST['user_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
		$resultado = $Escalamientos_Class->ActualizarEscalamientos($_POST['idElemento'],$_POST['sentinel_model_id'],$_POST['user_id'],$_POST['estado']);
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas Escalamientos
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY m.sentinel_model';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY m.sentinel_model '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='2') {
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY m.date_creation '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='5') {
    		$sOrder = ' ORDER BY m.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY m.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/escalamientos.php');
	$Escalamientos_Class = new Escalamientos();
	$datosConfiguracion = $Escalamientos_Class->consultaDatosEscalamientos($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $Escalamientos_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '<a href="op_escalamiento.php?opcn=editar&id='.$aInfo['sentinel_model_users_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	$aItem = array(
			$aInfo['sentinel_model'], 
			$aInfo['first_name'].' '.$aInfo['last_name'], 
			$aInfo['date_creation'], 
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['sentinel_model_users_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/escalamientos.php');
	$Escalamientos_Class = new Escalamientos();
	$cantidad_datos = $Escalamientos_Class->consultaCantidad();
}
unset($Escalamientos_Class);