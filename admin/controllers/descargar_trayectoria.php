<?php
include('models/usuarios.php');
$usuario_Class = new Usuarios();
$datosUsuario = $usuario_Class->consultaDatosUsuario($idUsuario);
$charge_user = $usuario_Class->consultaDatosCargos($idUsuario);
$rol_user = $usuario_Class->consultaDatosRoles($idUsuario);
$cursos_usuario = $usuario_Class->consulta_CoursesUsuario($idUsuario);

$cargos_cursos = $usuario_Class->consulta_CoursesCharges($idUsuario);
$resultados_usuario = $usuario_Class->consulta_CoursesChargesResult($idUsuario);
$Salidas = 0;
$Horas = 0;
$CantidadSalidas = $usuario_Class->CantidadSalidas($idUsuario);
if($CantidadSalidas['tiempo']>0){
	$Salidas = $CantidadSalidas['tiempo']/8;
	$Horas = $CantidadSalidas['tiempo'];
}
$anoCurso = date('Y');