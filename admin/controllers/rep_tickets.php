<?php
include_once('models/rep_tickets.php');
$tickets_Class = new RepTickets();
$cantidad_datos = 0;

if(isset($_POST['start_date_day'])){
	$grafica = [
		"abiertos"		=>0,
		"cerrados"		=>0,
		"reabiertos"	=>0
	];
	$datos_tickets = $tickets_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day'], "html");
	foreach ($datos_tickets as $keys => $valueTickets) {
		switch ($valueTickets['status_id']) {
			case '1': $grafica['abiertos'] += 1; break;
			case '3': $grafica['reabiertos'] += 1; break;
			case '0': $grafica['cerrados'] += 1; break;
		}
	}
	$cantidad_datos = count($datos_tickets);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datos_tickets = $tickets_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datos_tickets);
}

unset($tickets_Class);
