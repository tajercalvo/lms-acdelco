<?php
if(isset($_POST['_type'])){
	include_once('../models/aulavirtual_admin.php');
	if ( isset($_GET['opcn']) && $_GET['opcn'] == 'cargos' ) {
		$resultado['results'] = Foros::buscar_cargo($_POST);
		echo json_encode($resultado);
	} else {
		
		$resultado['results'] = Foros::buscar_moderador($_POST);
		echo json_encode($resultado);	
	}
	
}else if (isset($_POST['opcn'])) {

	switch ($_POST['opcn']) {
			//=====================================================================
			// Crea un nuevo POST
			//=====================================================================
		case 'buscar_moderador':
			include_once('../models/aulavirtual_admin.php');
			$resultado = Foros::buscar_moderador($_POST);
			echo json_encode($resultado);
			break;
		case 'crear':
			include_once('../models/aulavirtual_admin.php');
			@session_start();
			$p = $_POST;
			//$p['user_id'] = $_SESSION['idUsuario'];
			$resultado = Foros::CrearForo($p);
			if ($resultado > 0) {
				echo (json_encode(['error' => false, 'message' => 'OK']));
			} else {
				echo (json_encode(['error' => true, 'message' => 'No se ha podido crear la comunicación']));
			}

			break;
			//=====================================================================
			// Modifica un nuevo POST
			//=====================================================================
		case 'editar':
			include_once('../models/aulavirtual_admin.php');
			@session_start();
			$p = $_POST;
			// print_r( $_POST );
			$p['user_id'] = $_SESSION['idUsuario'];
			$resultado = Foros::ActualizarForo($p);
			if ($resultado > 0) {
				echo json_encode(['error' => false, 'message' => 'OK']);
			} else {
				echo json_encode(['error' => true, 'message' => 'No se ha podido modificar el registro']);
			}
			break;
			//=====================================================================
			// Agrega un Like a un POST
			//=====================================================================
		case 'LikeReply':
			include_once('../models/aulavirtual_admin.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->CrearLike($_POST['conference_reply_id']);
			if ($resultado > 0) {
				echo ('{"resultado":"SI"}');
			} else {
				echo ('{"resultado":"NO"}');
			}
			break;
			//=====================================================================
			//
			//=====================================================================
		case 'enviar':
			include_once('../models/aulavirtual_admin.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->CrearReply($_POST['conference_id'], $_POST['comentario']);
			unset($Foros_Class);
			if ($resultado > 0) {
				echo ('{"resultado":"SI","MaxId":"' . $resultado . '"}');
			}
			break;
			//=====================================================================
			//
			//=====================================================================
		case 'ProcesoAct':
			include_once('models/aulavirtual_admin.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->StatusPost($_POST['conference_reply_id'], $_POST['status_id']);
			if ($resultado > 0) {
				echo ('{"resultado":"SI"}');
			} else {
				echo ('{"resultado":"NO"}');
			}
			break;
			//=====================================================================
			// Retorna los comentarios hechos para el chat
			//=====================================================================
		case 'comments':
			include_once('../models/aulavirtual_admin.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->ConsultaMensajes($_POST['conference_id'], $_POST['MaxIdReply']);
			$mostrar = "";

			// include_once('../models/aulavirtual_admin.php');
			// $Foros_Class = new Foros();
			// $asistentes = $Foros_Class->ConsultaAsistentes($_POST['conference_id']);

			if (count($resultado) > 0) {

				foreach ($resultado as $key => $msg) {
					$mostrar .= '<div class="media border-bottom margin-none">
						<div class="media-body">
							<small class="label label-default" title="' . $msg['date_creation'] . ' - ' . $msg['first_name'] . ' ' . $msg['last_name'] . '">' . $msg['first_name'] . ':</small> <span>' . $msg['conference_reply'] . '</span><br>
						</div>
					</div>';
					$mostrar .= '<input type="hidden" class="MaxIdReply" name="MaxIdReply" id="MaxIdReply" value="' . $msg['conference_reply_id'] . '">';
				}
			}
			echo (json_encode(["mensajes" => $mostrar]));
			break;
			//=====================================================================
			// Retorna el numero de asistentes por cada conferencia
			//=====================================================================
		case 'assist':
			include_once('../models/aulavirtual_admin.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->ConsultaAsistentes($_POST['conference_id']);
			echo (json_encode(['asistentes' => $resultado]));
			break;
			//=====================================================================
			// Suma el numero de asistentes enviados a la cantidad almacenada por conexion
			//=====================================================================
		case 'suma':
			include_once('../models/aulavirtual_admin.php');
			$Foros_Class = new Foros();
			$resultado = $Foros_Class->AgregarAsistentes($_POST['conference_id'], $_POST['numEnSala']);
			echo ($resultado);
			break;
			//=====================================================================
			// Carga las imagenes en la para mostrar a los participantes
			//=====================================================================
		case 'imagenes':
			include_once('../src/funciones_globales.php');
			include_once('../models/aulavirtual_admin.php');
			$conference_id = $_POST['conference_id'];
			if (isset($_FILES['imagen']['tmp_name'])) {
				Foros::borrar_archivos_vct($conference_id); // insertamos los nuevos archivos en la base de datos
				$ruta = '../../assets/fileVCT_av/VCT_' . $conference_id; // nombre de la carpeta donde se extraeran los achivos mas el id del curso

				$files = glob("$ruta/*"); //obtenemos todos los nombres de los ficheros
				foreach ($files as $file) {
					if (is_file($file))
						unlink($file); //elimino el fichero
				}
				$archivo_zip = $_FILES['imagen']['tmp_name']; // almacenamos el archivo en una variable
				$zip = new ZipArchive;
				if ($zip->open($archivo_zip) === TRUE) { // abrimos el archivo .zip

					//					$ruta = '../../assets/fileVCT_av/VCT_' . $conference_id; // nombre de la carpeta donde se extraeran los achivos mas el id del curso
					//$fecha_Actual 	= date("Y-m-d") . (date("H")) . ":" . date("i:s");
					$fecha_Actual = md5(time());
					if ($zip->extractTo($ruta)) { // extraemos los archivos en la carpeta que tiene la variable ruta
						$ficheros_new  = scandir($ruta); // listamos los nuevos archivos
						natsort($ficheros_new); // listamos los nuevos archivos
						$file = 1; // iniiamos la variable file en 1 para darle un orden a los archivos en la base de datos
						foreach ($ficheros_new as $value) { // recoremos el array con los nuevos archivos
							if ($value == '.' || $value == '..' || $value == '__MACOSX') { // scandir devuelve estos puntos como dos ficheros, asi que los anulamos
							} else {

								if (rename($ruta . '/' . $value, $ruta . '/' . $fecha_Actual . $value)) {
									$resultado = Foros::Crear_archivos_vct($file, $fecha_Actual . $value, $conference_id, ''); // insertamos los nuevos archivos en la base de datos
									$file++; // variable para ordenar los archivos en la base e datos
								}
							}
						} // end foreach


						$array_datos = ["resultado" => "SI", 'id' => $conference_id];
						echo json_encode($array_datos);
					}
					$zip->close();
				} // end if open($archivo_zip)
				return false; // terminamos la ejecución si existe un archivo
			}
			break;
		case 'consultarImagenes':
			include_once('../models/aulavirtual_admin.php');
			$p = $_POST;
			$imagenes = Foros::getFotos($p, "../");
			$estado = Foros::get_estado_foro($p);
			$response['error']		=	false;
			$response['message']	=	'OK';
			$response['imagenes']	=	$imagenes;
			$response['estado']		=	$estado;

			//echo( json_encode( ['error'=>false, "message"=>"OK", "imagenes"=>$imagenes] ) );
			echo json_encode($response);
			break;
			//=====================================================================
			//
			//=====================================================================
		case 'consultarAsistencia':
			include_once('../models/aulavirtual_admin.php');
			$p = $_POST;
			$imagenes = Foros::getFotos($p, "../");
			echo (json_encode(['error' => false, "message" => "OK", "imagenes" => $imagenes]));
			break;
			//=====================================================================
			// Modifica la imagen que se esta presentando actualmente ( durante la transmision )
			//=====================================================================
		case 'cambiarImagenIdx':
			include_once('../models/aulavirtual_admin.php');
			$p = $_POST;
			$editar = Foros::unselectImagen($p, "../");
			$editar = Foros::seleccionarImagen($p, "../");
			if ($editar > 0) {
				echo (json_encode(['error' => false, "message" => "OK"]));
			} else {
				echo (json_encode(['error' => true, "message" => "No se ha podido modificar la imagen"]));
			}
			break;
		case 'buscar_cargos_conferencias':
			include_once('../models/aulavirtual_admin.php');
			$editar = Foros::buscar_cargos_conferencias($_POST);
			echo json_encode($editar);
			break;
		case 'finalizar_trasmision':
			include_once('../models/aulavirtual_admin.php');
			$response = Foros::finalizar_trasmision($_POST);

			echo json_encode($response);
			break;
		case 'get_preguntas':
			include_once('../models/aulavirtual_admin.php');
			$res = Foros::get_preguntas($_POST['conference_id']);
			echo json_encode($res);
			break;
		case 'get_respuestas':
			include_once('../models/aulavirtual_admin.php');
			$res = Foros::get_respuestas($_POST['question_id']);
			echo json_encode($res);
			break;
		case 'crear_pregunta':
			include_once('../models/aulavirtual_admin.php');
			$res = Foros::crear_pregunta($_POST);
			echo json_encode($res);
			break;
		case 'crear_respuesta':
			include_once('../models/aulavirtual_admin.php');
			$res = Foros::crear_respuesta($_POST);
			echo json_encode($res);
			break;
		case 'eliminar_pregunta':
			include_once('../models/aulavirtual_admin.php');
			$res = Foros::eliminar_pregunta($_POST);
			echo json_encode($res);
			break;
	} //fin switch

} else if (isset($_GET['opcn'])) {
	switch ($_GET['opcn']) {

		case 'listado':
			include_once('../models/aulavirtual_admin.php');
			include_once('../models/usuarios.php');
			@session_start();
			$rol = $_SESSION['max_rol'] >= 5 ? "admin" : "user";
			if (!isset($usuario_Class)) {
				$usuario_Class = new Usuarios();
			}
			$p = $_GET;
			$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario'], "../");
			$p['charge_id'] = "0";
			foreach ($charge_user as $key => $Data_Charges) {
				$p['charge_id'] .= "," . $Data_Charges['charge_id'];
			}
			$Foros_datos = Foros::consultaForos($p);
			unset($usuario_Class);
			echo (json_encode(["error" => false, "message" => "OK", "data" => $Foros_datos, "rol" => $rol]));
			break;
		case 'descargar':
			include_once('models/aulavirtual_admin.php');
			$reporte = Foros::descargar($_GET);
			//echo json_encode($datos); die();
			header('Content-type: application/vnd.ms-excel;charset=iso-8859-15');
			header('Content-Disposition: attachment; filename=Reporte de preguntas.xls');
			$tabla = "<table border='1'>
							<thead>
								<tr>
									<th>Usuario</th>
									<th>Identificacion</th>
									<th>Logueado</th>
									<th>Pregunta</th>
									<th>respuesta</th>
								</tr>
							</thead>
							<tbody>";
			foreach ($reporte as $key => $value) {
				$logueado = $value['user_id'] == 0 ? 'NO' : $logueado = 'SI';

				$tabla .= "<tr>
												<td>" . utf8_decode( utf8_encode( $value['usuario'] ) ). "</td>
												<td>" . utf8_decode( utf8_encode( $value['identification'] ) ). "</td>
												<td>" . $logueado. "</td>
												<td>" . utf8_decode( utf8_encode( $value['pregunta'] ) ). "</td>
												<td>" . utf8_decode( utf8_encode( $value['answer'] ) ). "</td>
											</tr>";
			}
			$tabla .= "</tbody>
						</table>";
			echo $tabla;
			die();
		break;
	}
} else if (isset($_GET['id'])) {
	$p['conference_id'] = $_GET['id'];
	include_once('models/aulavirtual_admin.php');
	$Foros_Class = new Foros();
	$Foro_datos = $Foros_Class->consultaForo($_GET['id']);
	$Foros_Reply = $Foros_Class->consultaReply($_GET['id']);
	$Foros_Class->AsistenciaForo($_GET['id']);
	include_once('models/usuarios.php');
	if (!isset($usuario_Class)) {
		$usuario_Class = new Usuarios();
	}
	$charge_active = $usuario_Class->consultaAllDatosCargos('', '1');
	$diaposivitas = Foros::getFotos($p);

	unset($usuario_Class);
}
