<?php

$method = $_SERVER['REQUEST_METHOD'];
switch ($method) {
    case 'POST':
        include_once('../models/virtualclass.php');
        $obj = new CursosVCT();
        switch ($_POST['opcn']) {
            case 'cambiardiapositiva':
                $partes = parse_url($_SERVER['HTTP_REFERER']);
                parse_str($partes['query'], $vars);
                $schedule = $vars['schedule'];
                $diapositiva = $_POST['diapositiva'];
                $res = $obj->cambiardiapositiva($diapositiva, $schedule);
                echo json_encode($res);
                break;
            case 'ctrModulos':
                include_once('../models/actividades.php');
                $obj = Actividades::mdlModulos($_POST['curso']);
                echo json_encode($obj);
                break;
            case 'ctrActividades':
                include_once('../models/actividades.php');
                $obj = new Actividades();
                $actividades = $obj->mdlActividades('', @$_POST['curso'], @$_POST['modulo']);
                echo json_encode($actividades);
                break;

            case 'ctrCursos':
                include_once('../models/actividades.php');
                $obj = new Actividades();
                $cursos = $obj->mdlCursos();
                echo json_encode($cursos);
                break;
        }
        break;
    case 'GET':
        if (isset($_GET['curso']) && isset($_GET['modulo'])) {
            include_once('models/actividades.php');
            $obj = new Actividades();
            $actividades = $obj->mdlActividades('', @$_GET['curso'], @$_GET['modulo']);
        } else if(isset($_GET["ctrBuscar"])) {
            if (isset($_GET['curso']) && isset($_GET['modulo'])) {
                include_once('models/actividades.php');
                $obj = new Actividades();
                $actividades = $obj->mdlActividades('', @$_GET['curso'], @$_GET['modulo']);
            }
        } else {
            include_once('models/actividades.php');
            $obj = new Actividades();
            $actividades = $obj->getActivities();
        }
}
