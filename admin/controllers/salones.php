<?php
if(isset($_GET['opcn'])){
	include_once('models/salones.php');
	$salones_Class = new Salones();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $salones_Class->consultaRegistro($_GET['id']);
		$listadosCiudades = $salones_Class->consultaCiudades('');
        $listadosConcesionarios = $salones_Class->consultaConcesionarios('');
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		$listadosCiudades = $salones_Class->consultaCiudades('');
        $listadosConcesionarios = $salones_Class->consultaConcesionarios('');
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/salones.php');
	$salones_Class = new Salones();
	if($_POST['opcn']=="crear"){
		$resultado = $salones_Class->CrearSalones($_POST['living'],$_POST['address'],$_POST['vacant'],$_POST['city_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
		$resultado = $salones_Class->ActualizarSalones($_POST['idElemento'],$_POST['living'],$_POST['status_id'],$_POST['address'],$_POST['vacant'],$_POST['city_id']);
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas Salones
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.living';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.living '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY c.vacant '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY c.address '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY c.area '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY h.headquarter '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='6') {
    		$sOrder = ' ORDER BY s.first_name, s.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='7') {
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/salones.php');
	$salones_Class = new Salones();
	$datosConfiguracion = $salones_Class->consultaDatosAll($sWhere,$sOrder,$sLimit);
    //print_r($datosConfiguracion);
	$cantidad_datos = $salones_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    @session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {

    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
    		$valOpciones = '<a href="op_salon.php?opcn=editar&id='.$aInfo['living_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	}
    	$aItem = array(
			$aInfo['living'],
			$aInfo['vacant'],
			$aInfo['address'],
			$aInfo['area'],
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'], 
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['living_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/salones.php');
	$salones_Class = new Salones();
	$cantidad_datos = $salones_Class->consultaCantidad();
	$listadosCiudades = $salones_Class->consultaCiudades('');
}
unset($salones_Class);