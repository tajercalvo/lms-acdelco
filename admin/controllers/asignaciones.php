<?php
if (isset($_GET['schedule_id'])) {
	include_once('models/asignaciones.php');

	$datoSchedule = Asignaciones::consultaAsignacion($_GET['schedule_id'], "");
	$cargosSchedule = Asignaciones::consultaCargosAsign($_GET['schedule_id']);
	$var_dealer_sel = "";
	if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] > 4) {
		$listadosConcesionarios = Asignaciones::consultaConcesionarios($_GET['schedule_id']);
		if (isset($_POST['dealer_id'])) {
			$var_dealer_sel = $_POST['dealer_id'];
		} else {
			if (isset($listadosConcesionarios[0]['dealer_id'])) {
				$var_dealer_sel = $listadosConcesionarios[0]['dealer_id'];
			}
		}
	} else {
		$_POST['dealer_id'] = $_SESSION['dealer_id'];
		$var_dealer_sel = $_POST['dealer_id'];
	}
	$listadoInscritos = Asignaciones::listaUsuariosRegistrados($_GET['schedule_id'], $var_dealer_sel);
	if (!isset($_POST['busqueda_pal']) || $_POST['busqueda_pal'] == "") {
		$datos = Asignaciones::consultaUsuariosAsig($_GET['schedule_id'], $var_dealer_sel);
	} else {
		$datos = Asignaciones::consultaUsuariosNoAsig($_GET['schedule_id'], $var_dealer_sel, '', $_POST['busqueda_pal']);
	}
	$datos_Usuarios = array_merge($listadoInscritos, $datos);
	$cantidad_usuarios = count($datos_Usuarios);
	$DatosSchedule_Dealer = Asignaciones::consultaSchedule($_GET['schedule_id'], $var_dealer_sel);
	// print_r($DatosSchedule_Dealer);
	// unset($Calificaciones_Class);
}

if (isset($_POST['opcn'])) {

	$schedule_id = $_POST['schedule_id'];
	include_once('../models/asignaciones.php');
	include_once('../models/usuarios.php');
	@session_start();

	switch ($_POST['opcn']) {
			// ========================================================
			//
			// ========================================================
		case "GuardarSchedule":
			$p = $_POST;
			$user_obj = new Usuarios();
			/*Borra las asignaciones que tenga en schedule el concesionario*/
			if (isset($_SESSION['max_rol']) && $_SESSION['max_rol'] >= 5) {
				if (isset($_POST['dealer_id'])) {
					$var_dealer_sel = $_POST['dealer_id'];
				} else {
					$var_dealer_sel = $_SESSION['dealer_id'];
				}
			} else {
				$var_dealer_sel = $_SESSION['dealer_id'];
			}

			// consultar si la sesión, "VCT" ya ha finalizado, si esta finalizada el proceso no puede continuar para noa fectar las notas y las actividades

			// $estado = Asignaciones::scheduleFInalizado($_POST['schedule_id']);
			// if ($estado == 1) {
			// 	echo json_encode(array("error" => true, "tipo" => "error", "message" => "Esta sesión se encuentra finalizada, no se puede hacer ninguna modificación"));
			// 	die();
			// }

			$dealer_schedule = Asignaciones::consultaSchedule($_POST['schedule_id'], $var_dealer_sel, '../');

			if (isset($_POST['cedulas'])) {
				$_POST['cedulas'] = trim($_POST['cedulas'], ',');
				$usuarios = Asignaciones::usuariosSeleccionados(trim($_POST['cedulas']));

				$p['DataSeleccionados'] = "";
				foreach ($usuarios as $key => $value) {
					$p['DataSeleccionados'] .= $value['user_id'] . ',';
				}
				$p['DataSeleccionados'] = trim($p['DataSeleccionados'], ',');
			}

			/*Borra las asignaciones que tenga en schedule el concesionario*/
			/*Invita a los seleccionados*/
			$Users_select = explode(',', $p['DataSeleccionados']);

			$agregados = intval($dealer_schedule['max_size']) - count($Users_select);

			$cant_user = 0;
			// count( $Users_select ) > 0 || ( isset( $Users_select[0] ) && $Users_select[0] != '' ) &&
			if ($agregados >= 0) {
				$resul = Asignaciones::BorraSchedule($schedule_id, $var_dealer_sel, $p['DataSeleccionados'], "../");
				foreach ($Users_select as $val_users) {
					$usr = $user_obj->consultaDatosUsuario($val_users, '../');
					$resultado = Asignaciones::CrearInvitation($val_users, $schedule_id, $var_dealer_sel, $usr['headquarter_id'], "../");
					if ($resultado > 0) {
						$datoSchedule = Asignaciones::consultaAsignacion($schedule_id, "../");
						$correos_destino = Asignaciones::traerCorreos($val_users, "../");
						$correos_destino['email'];
						//$lista_correo = $correos_destino['email'];
						// $fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
						// $fecha_entrada = strtotime($datoSchedule['start_date']);
						include_once('email.php');
						$correo = new EmailLudus();

						$mensaje = '<table>
											<thead>
												<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/head_email.png"></td>
											</thead>
											<tbody>
												<td style="text-align: center;" >
													<h3>Bienvenido a un mundo de aprendizaje dise&ntilde;ado especialmente para ti:</h3>
													<p><strong>Nombre del curso: </strong>' . $datoSchedule['course'] . '</p>
													<p><strong>Tipo de Curso: </strong>' . $datoSchedule['type'] . '</p>
													<p><strong>Hora Inicio: </strong>' . $datoSchedule['start_date'] . '</p>
													<p><strong>Hora Fin: </strong>' . $datoSchedule['end_date'] . '</p>
													<p><strong>Profesor: </strong>' . $datoSchedule['first_name'] . ' ' . $datoSchedule['last_name'] . '</p>
													<p><strong>Muchas Gracias</strong></p></td>
											</tbody>
											<tfoot>
												<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/footer_email.png" ></td>
											</tfoot>
										</table>';
						$destinatario = $correos_destino['email'];
						$asunto = 'Invitación al curso ' . $datoSchedule['course'];
						$de = 'Invitación';
						// $email = $correo->emailInvitacion($mensaje, $destinatario, $asunto, $de);
					}
				}
				/*Invita a los seleccionados*/
				/*Actualiza datos en schedule*/
				$resul = Asignaciones::ActualizaSchedule($schedule_id, "../");
				$email = isset($email) ? $email : 'no';
				$resp = ['error' => false, "message" => "Se guardaron los ajustes correctamente", "email" => $email];
			} else {
				if ($agregados < 0) {
					$message = "La cantidad de personas seleccionadas superan los cupos asignados";
					$tipo = "error";
				} else {
					$message = "No se han seleccionado asistentes para asignar al curso";
					$tipo = "information";
				}
				$resp = ['error' => true, "message" => $message, "tipo" => $tipo];
			}
			/*Actualiza datos en schedule*/
			echo json_encode($resp);
			break;

			// ========================================================
			//
			// ========================================================
		case 'buscar':
			$cant_usr = 0;
			$datoSchedule = Asignaciones::consultaAsignacion($_POST['schedule_id'], "../");
			$datos_Usuarios = Asignaciones::consultaUsuariosNoAsig($_POST['schedule_id'], $_POST['dealer_id'], $_POST['filtro'], $_POST['usuarios'], "../");
			$cantidad_usuarios = count($datos_Usuarios);
			if ($cantidad_usuarios > 0) {
				foreach ($datos_Usuarios as $iID => $data) {
					$cant_usr++;
?>
					<tr class="reg_busqueda">
						<td><?php echo ($cant_usr); ?></td>
						<td><?php echo ('<img src="../assets/images/usuarios/' . $data['image'] . '" style="width: 40px;" alt="' . $data['first_name'] . ' ' . $data['last_name'] . '">'); ?></td>
						<td><?php echo ($data['first_name'] . ' ' . $data['last_name']); ?>
							<?php
							$titulo_otr = "";
							$cant_otras = 0;
							$ano_curso = date("Y");
							if (isset($data['otras'])) {
								foreach ($data['otras'] as $iID => $otras) {
									$pos = strpos($otras['start_date'], $ano_curso);
									$startDate_val = $otras['start_date'];
									if ($pos !== false) {
										$cant_otras++;
									}
									$titulo_otr .= "<i class='fa fa-calendar'></i> Curso: <strong class='text-danger'>" . $otras['course'] . "</strong> | Fecha: <strong class='text-danger'>" . $otras['start_date'] . "</strong> | Lugar: <strong class='text-danger'>" . $otras['living'] . "</strong> <br>";
								}
							} ?>
							<?php if (isset($data['otras'])) { ?><br><a href="Otr_Asignacion" <?php if ($cant_otras >= 6) { ?>class="text-danger" <?php } ?> onclick="return false;" title="<?php echo $titulo_otr; ?>"><i class="fa fa-calendar"></i> Programado!! Este año: <?php echo ($cant_otras); ?> veces
								</a><br><?php } ?>
						</td>
						<?php if (isset($data['notas'])) {
							$titulo = "";
							$class_result = "text-danger";
							$fa_result = "fa-thumbs-down";
							foreach ($data['notas'] as $iID => $notas) {
								if ($notas['approval'] == "SI") {
									$class_result = "text-success";
									$fa_result = "fa-thumbs-up";
									$titulo .= "<i class='fa fa-tasks'></i> Fecha: <strong class='text-success'>" . $notas['date_creation'] . "</strong> | Aprobó: <strong class='text-success'>" . $notas['approval'] . "</strong> | Puntaje: <strong class='text-success'>" . $notas['score'] . "</strong><br>";
								} else {
									$titulo .= "<i class='fa fa-tasks'></i> Fecha: <strong class='text-danger'>" . $notas['date_creation'] . "</strong> | Aprobó: <strong class='text-danger'>" . $notas['approval'] . "</strong> | Puntaje: <strong class='text-danger'>" . $notas['score'] . "</strong><br>";
								}
							}
						} ?>
						<td align="center"><?php if (isset($data['notas'])) { ?>
								<a href="Historico" class="<?php echo ($class_result); ?>" onclick="return false;" title="<?php echo $titulo; ?>"><i class="fa <?php echo ($fa_result); ?>"></i> Ya Cursado!!</a><br>
							<?php } ?>
							<?php echo ($data['identification']); ?>
						</td>
						<td><?php echo ($data['headquarter']); ?></td>
						<td <?php if ($datoSchedule['city'] != $data['area']) { ?> style="color: red;" <?php } else { ?>style="color: green;" <?php } ?>><?php echo ($data['area']); ?></td>
						<td><?php
								foreach ($data['cargos'] as $iID => $cargos) {
									echo ("<i class='fa fa-fw icon-identification'></i> <span class='text-danger'>" . $cargos . '</span><br>');
								}
								?></td>
						<td>
							<input type="hidden" name="id" class="id" value="<?php echo $data['user_id']; ?>">
							<div class="nuevos make-switch" id="Sw_ChkAsig<?php echo $data['user_id']; ?>" data-on="danger" data-off="default">
								<input disabled="disabled" id="ChkAsig<?php echo $data['user_id']; ?>" type="checkbox" value="SI" <?php if ($data['YaenInvitacion'] > 0) { ?>checked="checked" <?php } ?> onchange="CheckAsigna('#ChkAsig<?php echo $data['user_id']; ?>',<?php echo $data['user_id']; ?>);">
							</div>
						</td>
					</tr>
<?php }
			}
			break;
		case 'finalizar_curso_MFR':
			$res = Asignaciones::finalizar_curso_MFR($_POST['schedule_id']);
			echo json_encode($res);
			break;
	}
	// unset($Calificaciones_Class);
}
