<?php
//@session_start();
// echo '<pre>';
// // print_r($_SERVER);
// print_r($_SESSION);
// echo '</pre>';
$method = $_SERVER['REQUEST_METHOD'];
switch ($method) {
    case 'POST':
        include_once('../models/aulavirtual.php');
        $obj = new Aulavirtual();
        $partes = parse_url($_SERVER['HTTP_REFERER']);
        parse_str($partes['query'], $vars);
        if (!isset($vars['schedule'])) {
            echo 'Schedule no existe';
            die();
        }
        $schedule_id = $vars['schedule'];

        switch ($_POST['opcn']) {
            case 'login':
                $usuario = $_POST['username'];
                $res = $obj->registrarInvitado($schedule_id, $usuario);
                header("Location: ../aulavirtual.php?token=" . md5(time()) . '&schedule=' . $schedule_id);
                die();
                break;
            case 'cambiardiapositiva':
                $diapositiva = $_POST['diapositiva'];
                $res = $obj->cambiardiapositiva($diapositiva, $schedule_id);
                echo json_encode($res);
                break;
            case 'nuevomsj':
                $msj = $_POST['msj'];
                $idmsj = $_POST['idmsj'];
                $res = $obj->nuevomsj($schedule_id, $msj, $idmsj);
                echo json_encode($res);
                break;
            case 'msjNuevos':
                $msj_id = $_POST['msj_id'];
                $res = $obj->msjNuevos($schedule_id, $msj_id);
                echo json_encode($res);
                break;
            case 'diapositivaActual':
                $res = $obj->diapositivaActual($schedule_id);
                echo json_encode($res);
                break;
            case 'levantarMano':

                $_POST['schedule_id'] = $schedule_id;
                $res = $obj->levantarMano($_POST);
                echo json_encode($res);
                break;
            case 'quienLevanto':    
                $res = $obj->quienLevanto($schedule_id);
                echo json_encode($res);
                break;
            case 'silenciar':    
                    $res = $obj->silenciar($schedule_id, $_POST['user_id']);
                    echo json_encode($res);
                break;
            case 'autorizarAlumno':
                $_POST['schedule_id'] = $schedule_id;
                $res = $obj->autorizarAlumno($_POST);
                echo json_encode($res);
                break;
            case 'quitar_alumno':
                    $_POST['schedule_id'] = $schedule_id;
                    $res = $obj->quitar_alumno($_POST);
                    echo json_encode($res);
                    break;
            case 'quitarNotificaciones':
                $res = $obj->quitarNotificaciones($schedule_id);
                echo json_encode($res);
                break;
            case 'validarAutorizacion':
                $res = $obj->validarAutorizacion($schedule_id);
                echo json_encode($res);
                break;
            case 'finAlumnoTransmisor':
                $res = $obj->finAlumnoTransmisor($schedule_id);
                echo json_encode($res);
                break;
            case 'cargarPregunta':
                $res = $obj->cargarPregunta($_POST['question_id']);
                echo $res;
                break;
            case 'responderPreguntas':
                $_POST['schedule_id'] = $schedule_id;
                $res = $obj->responderPreguntas($_POST);
                echo json_encode($res);
                break;
            case 'conectados':
                $res = $obj->conectados($schedule_id, "../");
                echo json_encode($res);
                break;
                // case inscripcion, se trae del controlador vct_details, tal cual como está allá
            case 'inscripcion':
                $database_class = new Aulavirtual();
                @session_start();
                $yaRegistrado = $database_class->yaInscrito($_SESSION['idUsuario'], $_POST['schedule'], "../");
                $DetallesProgramacion = $database_class->DetProgramacion($_POST['schedule'], '../');

                //permite el acceso al instructor
                //o super administradores
                if (isset($_SESSION['idUsuario']) && $_SESSION['idUsuario'] == $DetallesProgramacion['teacher_id']) {
                    $validacion = array("resultado" => "si", "mensaje" => "Ingreso del instructor");
                } else {
                    // si no esta registrado -> $yaRegistrado = 0
                    if ((isset($yaRegistrado) && $yaRegistrado == 0)) {
                        // si no han pasado 10 min de la capacitacion
                        //if( $_POST['tiempo'] < 900000 ){
                        $registro = $database_class->inscribirseCurso($DetallesProgramacion['module_id'], $_POST['id'], $_POST['schedule'], "../");
                        //valida si se pudo realizar la inscripcion
                        if (isset($registro) && $registro > 0) {
                            $validacion = array("resultado" => "si", "mensaje" => "registro OK");
                        } else {
                            $validacion = array("resultado" => "no", "mensaje" => "No se pudo realizar la inscripcion");
                        }
                        // }else{ // si ya pasaron 10 min
                        // 	$validacion = array( "resultado" => "no", "mensaje" => "Ya han transcurrido 10 minutos de la capacitación, permitidos para la inscripcion" );
                        // }
                    } elseif (isset($yaRegistrado) && $yaRegistrado == 1) { // si el registro es = 1 es decir ya registrado
                        $validacion = array("resultado" => "si", "mensaje" => "ya registro OK");
                        //agregar un filtro para no ingresar si ya se termino el curso
                    } else {
                        $validacion = array("resultado" => "No", "mensaje" => "Multiples registros");
                    }
                }
                echo (json_encode($validacion));
                break;
            case 'finalizar_curso':
                $schedule_id = $_POST['schedule_id'];
                $json = $obj->finalizar_curso( $schedule_id );
                echo json_encode($json);
                break;
        } // fin switch $_POST['opcn']        
        break;
    case 'GET':
        if (!isset($_GET['schedule'])) {
            echo 'No existe schedule';
            die();
            //header("Location: index.php");
        } else {
            $schedule_id = $_GET['schedule'];
            include_once('models/aulavirtual.php');
            $cursos_Class = new Aulavirtual();
            $aula_virtual = $cursos_Class->statusSchedule($schedule_id);
            
            @session_start();
            if( $aula_virtual['status_id'] == 2 ){
                echo 'Trasmisión finalizada';
                unset($_SESSION['user_id_av']);
                die();
            }

            // validamos si está logueado
            if (isset($_SESSION['idUsuario']) and $_SESSION['idUsuario'] != 0) {
                $cursos_Class->registrarUsuario($schedule_id);
                if( $aula_virtual['privada'] == 1 ){
                    $res = $cursos_Class->cargos_aula($schedule_id);
                    @session_start();
                    $user_id = $_SESSION['idUsuario'];
                    
                    if( count($res) == 0 && $aula_virtual['teacher_id'] != $user_id){
                        echo "Aula privada";
                        die();
                    }
                    
                }
            } else {
                
                if( $aula_virtual['privada'] == 1 ){
                    header("Location: index.php");
                    die();
                }elseif (!isset($_SESSION['user_id_av'])) {
                    include('login_av.php');
                    die();
                }elseif( isset($_SESSION['user_id_av'] ) ){ // validamos el tiempo de la sesión del invitado
                        //print_r($_SESSION['time_sesion_av']); die();
                    if( ($_SESSION['time_sesion_av'] + 60 * 240 ) < time() ){ // validamos si pasaron 2 horas
                        unset($_SESSION['user_id_av']);
                        include('login_av.php');
                        die();
                    }
                }
            }
            //$course_id = $_GET['id'];

            $ahora = date("Y-m-d") . " " . (date("H")) . ":" . date("i:s");
            $DetallesProgramacion = $cursos_Class->DetProgramacion($schedule_id);
            $comentarios = $cursos_Class->consultaComentarios($schedule_id);
            $diapositivas = $cursos_Class->get_diapositivas($schedule_id);
            $registrados = $cursos_Class->conectados($schedule_id);
            //$invitado = $cursos_Class->invitado( $_SESSION['idUsuario'], $schedule_id );
            //$archivosVCT = $cursos_Class->consultaVCTFiles( $schedule_id );

            //Si el usuarios está invitado, 1 = invitado, 0 no invitado
            // if( $invitado == 1 ){
            //     // validamos si está inscrito
            //     $yaRegistrado = $cursos_Class->yaInscrito( $_SESSION['idUsuario'], $schedule_id, "" );

            //     if( $yaRegistrado == 0 ){
            //         $registro = $cursos_Class->inscribirseCurso( $DetallesProgramacion['module_id'], $DetallesProgramacion['course_id'], $schedule_id, "" );
            //     }
            // }
            //$inscritoCourse = $cursos_Class->consultaInscripcion( $course_id, $schedule_id );
            $registroConfiguracionDetail = $cursos_Class->consultaRegistroDetail($schedule_id);
            //$comentarios = $cursos_Class->consultaComentarios($schedule_id);
            // if( ( $invitado == 1 && $inscritoCourse ) || $DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario'] || $_SESSION['max_rol'] >= 5 ){
            //     $registrados = $cursos_Class->consultaRegistros($course_id,$schedule_id);
            //     $registroConfiguracionDetail = $cursos_Class->consultaRegistroDetail($course_id);
            //     $comentarios = $cursos_Class->consultaComentarios($schedule_id);
            //     $diapositivas = $cursos_Class->get_diapositivas($schedule_id);
            //     // $programaciones = $cursos_Class->consultarProgramaciones($course_id);
            //     // $cantidadRegistrados= count($registrados);
            //     // $horarioCurso = $cursos_Class->horarios($schedule_id);
            //     // $archivosVCT = $cursos_Class->consultaVCTFiles( $schedule_id );
            // }else{
            //     //header("Location: index.php");
            // }
        }

        if (($DetallesProgramacion['teacher_id'] != $_SESSION['idUsuario'])) {
            include_once('models/aulavirtual.php');
            $cursos_Class = new Aulavirtual();
            $schedule_id = $_GET['schedule'];

            if (isset($_GET['alumnoTransmisor'])) {
                $valida = $cursos_Class->alumnoTransmisor($schedule_id);
                // echo 'SI intenta transmitir';
                // die();
                if (count($valida) == 0) {
                    // echo 'No autorizADO';
                    // die();
                    header('Location: aulavirtual.php?token=f9a324946c883db2f221d80c457b1144&_valSco=50203f8bc0ac1121b683a40f1c55d7fd&opcn=ver&id=&schedule=' . $schedule_id);
                }
            } else {
                $valida = $cursos_Class->alumnoTransmisor($schedule_id);
                // echo 'No intenta transmitir';
                // die();
                if (count($valida) > 0) {
                    // echo 'Autorizado';
                    // die();
                    header('Location: aulavirtual.php?token=f9a324946c883db2f221d80c457b1144&_valSco=50203f8bc0ac1121b683a40f1c55d7fd&opcn=ver&id=&schedule=' . $schedule_id . '&alumnoTransmisor=1');
                }
            }
        }
        break;
}
