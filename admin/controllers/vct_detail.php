<?php
if(isset($_GET['id'])){
		include_once('models/vct_detail.php');
		$cursos_Class = new CursosVCT();
		$id = $_GET['id'];
		$vct = $_GET['schedule'];
		$ahora = date("Y-m-d")." ".(date("H")).":".date("i:s");
		$DetallesProgramacion = $cursos_Class->DetProgramacion($vct);
		$invitado = $cursos_Class->invitado( $_SESSION['idUsuario'], $vct );
		$inscritoCourse = $cursos_Class->consultaInscripcion( $id, $vct );
		if( ( $invitado == 1 && $inscritoCourse ) || $DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario'] || $_SESSION['max_rol'] == 6 ){
			$registrados = $cursos_Class->consultaRegistros($id,$vct);
			$registroConfiguracionDetail = $cursos_Class->consultaRegistroDetail($id);
			//$comentarios = $cursos_Class->consultaComentarios($vct);
			$programaciones = $cursos_Class->consultarProgramaciones($id);
			$cantidadRegistrados= count($registrados);
			$horarioCurso = $cursos_Class->horarios($vct);
			$archivosVCT = $cursos_Class->consultaVCTFiles( $vct );
		}else{
			header("Location: index.php");
		}
		unset($cursos_Class);
}

if(isset($_POST['opcn'])){
	include_once('../models/vct_detail.php');
	switch ($_POST['opcn']) {
		case 'newComment':
			if ($_POST['texto_comentario'] != '') {
				@session_start();
				if (isset($_SESSION['idUsuario'])) {
						//sleep(2);
						$database_class = new CursosVCT();
						$resultado = $database_class->insertarComentario($_POST['idvct'],$_POST['texto_comentario'],$_POST['id_msj_usuario'] );
						if($resultado>0){

							echo('{"resultado":"SI", "id_comments_users":"'.$resultado[0]['id_comments_users'].'", "fecha":"'.$resultado[0]['date_comment'].'"}');
						}else{
							echo('{"resultado":"NO"}');
						}

				}else{echo('{"resultado":"no existe variable de sesion"}');}

			}
			unset($database_class);
		break;
		case 'reload':
			$database_class = new CursosVCT();
			//$comentarios = $cursos_class->consultaComentarios($_POST['idvct']);
			unset($database_class);
		break;
		case 'programar':
			$database_class = new CursosVCT();
			$comentarios = $cursos_class->nuevaInscripcion($_POST['schedule_id']);
			unset($database_class);
		break;
		case 'inscripcion':
			$database_class = new CursosVCT();
			@session_start();
			$yaRegistrado = $database_class->yaInscrito( $_SESSION['idUsuario'], $_POST['schedule'], "../" );
			$DetallesProgramacion = $database_class->DetProgramacion($_POST['schedule'], '../');
			// print_r( $DetallesProgramacion );
			//permite el acceso al instructor
			//o super administradores
			if( isset( $_SESSION['idUsuario']) && $_SESSION['idUsuario'] == $DetallesProgramacion['teacher_id'] ){
				$validacion = array( "resultado" => "si", "mensaje" => "Ingreso del instructor" );
			}else{
				// si no esta registrado -> $yaRegistrado = 0
				if( ( isset( $yaRegistrado ) && $yaRegistrado == 0 ) ){
					// si no han pasado 10 min de la capacitacion
					//if( $_POST['tiempo'] < 900000 ){
						$registro = $database_class->inscribirseCurso( $_POST['id'], $_POST['schedule'], "../" );
						//valida si se pudo realizar la inscripcion
						if( isset( $registro ) && $registro > 0 ){
							$validacion = array( "resultado" => "si", "mensaje" => "registro OK" );
						}else{
							$validacion = array( "resultado" => "no", "mensaje" => "No se pudo realizar la inscripcion" );
						}
					// }else{ // si ya pasaron 10 min
					// 	$validacion = array( "resultado" => "no", "mensaje" => "Ya han transcurrido 10 minutos de la capacitación, permitidos para la inscripcion" );
					// }
				}elseif( isset( $yaRegistrado ) && $yaRegistrado == 1 ){ // si el registro es = 1 es decir ya registrado
					$validacion = array( "resultado" => "si", "mensaje" => "ya registro OK" );
					//agregar un filtro para no ingresar si ya se termino el curso
				}else{
					$validacion = array( "resultado" => "No", "mensaje" => "Multiples registros" );
				}
			}
			echo( json_encode( $validacion ) );
		break;
	}//fin switch
}
