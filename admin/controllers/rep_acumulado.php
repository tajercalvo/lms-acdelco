<?php
include_once('models/rep_acumulado.php');
$Acumulados_Class = new RepAcumulado();
$datosCursos = $Acumulados_Class->consultaDatos();
$cantidad_datos = 0;

if(isset($_POST['dealer_id']) || isset($_POST['dealer_id_opc']) ){
	if(!isset($_POST['dealer_id'])){
		$_POST['dealer_id'] = $_POST['dealer_id_opc'];
	}
	$datosCursos_all = $Acumulados_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['dealer_id'])){
	$_POST['dealer_id'] = $_GET['dealer_id'];
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Acumulados_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Acumulados_Class);