<?php

///var_dump($_POST);
if(isset($_GET['opcn'])){
	include_once('models/programaciones.php');
	$cargos_Class = new Cargos();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $cargos_Class->consultaRegistro($_GET['id']);
		$modulos = $cargos_Class->getModulesCourse($_GET['id']);
		$listadosCursos = $cargos_Class->consultaCursos('');
		$listadosProfesores = $cargos_Class->consultaProfesores('');
		$listadosSalones = $cargos_Class->consultaSalones('');
		$listadosProfesoresAux = $cargos_Class->consultaProfesoresAux( $_GET['id']);
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		$listadosCursos = $cargos_Class->consultaCursos('');
		$listadosProfesores = $cargos_Class->consultaProfesores('');
		$listadosSalones = $cargos_Class->consultaSalones('');
	}
}else if(isset($_POST['opcn'])){

	switch ( $_POST['opcn'] ) {

		case 'crear':
			include_once('../models/programaciones.php');
			include_once('../controllers/email.php');
			$cargos_Class = new Cargos();
			$email = new EmailLudus();
			//Crear schedule
			$_POST['teacher_id_aux'] = isset($_POST['teacher_id_aux']) ? $_POST['teacher_id_aux'] : array() ;
			$resultado = $cargos_Class->insertSchedule($_POST['start_date_day'],$_POST['start_date_time'],$_POST['end_date_day'],$_POST['end_date_time'],$_POST['max_inscription_date'],$_POST['living_id'],$_POST['min_size'],$_POST['max_size'],$_POST['waiting_size'],$_POST['course_id'],$_POST['teacher_id'], $_POST['module_id'], $_POST['teacher_id_aux']);
			
			$DatosProgramacion = Cargos::DatosCompletosProgramacion($resultado);

			//Se encarga de crear las asignaciones individuales "En la misma ciudad"
			$Dealers_sel = explode(',', $_POST['SeleccionadosID_si']);

			//se crea el archivo ics y se archiva en la db
			//
			$datoSchedule = $cargos_Class->consultaAsignacion( $resultado, "../" );//[datos del schedule]

			// $meetingstamp = strtotime( $datoSchedule['start_date'] . " UTC" );//20170418T140000
			// $dtstart= gmdate("Ymd\THis",$meetingstamp);
			// $meetingstampb = strtotime( $datoSchedule['end_date'] . " UTC" );//20170418T160000
			// $dtend= gmdate("Ymd\THis",$meetingstampb);
			//texto que se va a insertar en el archivo .ics
			// $txt_archivo = $email->ICS( $dtstart, $dtend, $datoSchedule['course'], $datoSchedule['newcode'], $datoSchedule['living']);
			// $archivo = $email->save( $txt_archivo, "../");
			// $save_archivo = $cargos_Class->guardarICS( $resultado, $archivo, "../" );
			//
			//fin crear archivo ics

			foreach ($Dealers_sel as $val_users_bor) {
				//echo(":::::::::::::::::::".$val_users_bor);
				if($val_users_bor!=""){
					$max_sizeDat = $_POST['S_max_size_'.$val_users_bor];
					$min_sizeDat = $_POST['S_min_size_'.$val_users_bor];
					$resultado_ind = $cargos_Class->Crear_InvDealer($val_users_bor,$resultado,$min_sizeDat,$max_sizeDat);
				}
			}
			//Se encarga de crear las asignaciones individuales "En la misma ciudad"
			//Se encarga de crear las asignaciones individuales "No en la misma ciudad"
			$Dealers_sel = explode(',', $_POST['SeleccionadosID_no']);
			foreach ($Dealers_sel as $val_users_bor) {
				if($val_users_bor!=""){
					$max_sizeDat = $_POST['N_max_size_'.$val_users_bor];
					$min_sizeDat = $_POST['N_min_size_'.$val_users_bor];
					$resultado_ind = $cargos_Class->Crear_InvDealer($val_users_bor,$resultado,$min_sizeDat,$max_sizeDat);
				}
			}
			//Se encarga de crear las asignaciones individuales "No en la misma ciudad"
			//consulta los involucrados en la sesión
			$correo = new EmailLudus();
			if( isset($_POST['notificar_email']) && $_POST['notificar_email']=="on" ){
				$mensaje = '<table>
									<thead>
										<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/head_email.png"></td>
									</thead>
									<tbody>
										<td style="text-align: center;" >
											<h3>El curso '.$DatosProgramacion['course'].' se ha reprogramado, por favor valida la informaci&oacute;n</h3>
											<p><strong>Nombre del curso: </strong>'.$DatosProgramacion['course'].'</p>
											<p><strong>Tipo de Curso: </strong>'.$DatosProgramacion['type'].'</p>
											<p><strong>Hora Inicio: </strong>'.$DatosProgramacion['start_date'].'</p>
											<p><strong>Hora Fin: </strong>'.$DatosProgramacion['end_date'].'</p>
											<p><strong>Profesor: </strong>'.$DatosProgramacion['first_prof'].' '.$DatosProgramacion['last_prof'].'</p>
											<p><strong>Muchas Gracias</strong></p></td>
									</tbody>
									<tfoot>
										<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/footer_email.png" ></td>
									</tfoot>
								</table>';
				$destinatario = $DatosProgramacion['email_prof'];
				$asunto = 'Programado al curso '.$DatosProgramacion['course'];
				$de = 'Programado al curso '.$DatosProgramacion['course'];
				$email = $correo->emailInvitacion( $mensaje, $destinatario, $asunto, $de );	
			}
			unset($EmailLudus_Class);
			//consulta los involucrados en la sesión

			if($resultado>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;

		case 'editar':
			include_once('../models/programaciones.php');
			include_once('../controllers/email.php');
			  $email = new EmailLudus(); 
			
			$cargos_Class = new Cargos();
			$DatosProgramacion = Cargos::DatosCompletosProgramacion($_POST['idElemento']);
			/* print_r($DatosProgramacion); */
			/*  print_r($DatosProgramacion['type']);
			return;  */
			if( $DatosProgramacion['finalizado'] && $DatosProgramacion['tipemodule']!='MFR'){
				$json = array('resultado' => false, 'msj' => 'Esta curso ya ha finalizado, no se puden hacer cambios en la programación' );
				echo json_encode($json);
				die();
			}
			
			$resultado = $cargos_Class->Actualizarcargos($_POST['idElemento'],$_POST['status_id'],$_POST['start_date_day'],$_POST['start_date_time'],$_POST['end_date_day'],$_POST['end_date_time'],$_POST['max_inscription_date'],$_POST['living_id'],$_POST['min_size'],$_POST['max_size'],$_POST['waiting_size'],$_POST['course_id'],$_POST['teacher_id'], $_POST['module_id']);
			
			if( $resultado > 0 ){
				$datoSchedule = $cargos_Class->consultaAsignacion( $_POST['idElemento'], "../" );//[datos del schedule]
				$meetingstamp = strtotime( $datoSchedule['start_date'] . " UTC" );//20170418T140000
				$dtstart= gmdate("Ymd\THis",$meetingstamp);
				$meetingstampb = strtotime( $datoSchedule['end_date'] . " UTC" );//20170418T160000
				$dtend= gmdate("Ymd\THis",$meetingstampb);
				//texto que se va a insertar en el archivo .ics
				$txt_archivo = $email->ICS( $dtstart, $dtend, $datoSchedule['course'], $datoSchedule['newcode'], $datoSchedule['living']);
				$archivo = $email->save( $txt_archivo, "../");
				$save_archivo = $cargos_Class->guardarICS( $_POST['idElemento'], $archivo, "../" );
			}

			//Se encarga de crear las asignaciones individuales
			$Dealers_sel_cr = explode(',', $_POST['SeleccionadosID_si']);
			foreach ($Dealers_sel_cr as $val_users_bor_cr) {
				if($val_users_bor_cr!=""){
					$max_sizeDat = $_POST['S_max_size_'.$val_users_bor_cr];
					$min_sizeDat = $_POST['S_min_size_'.$val_users_bor_cr];
					$resultado_ind = $cargos_Class->Crear_InvDealer($val_users_bor_cr,$_POST['idElemento'],$min_sizeDat,$max_sizeDat);
				}
			}
			//Se encarga de crear las asignaciones individuales
			//Se encarga de actualizar las asignaciones individuales anteriormente creadas
			$Dealers_sel_up = explode(',', $_POST['SeleccionadosID_si']);
			foreach ($Dealers_sel_up as $val_users_bor_up) {
				if($val_users_bor_up!=""){
					$max_sizeDat = $_POST['S_max_size_'.$val_users_bor_up];
					$min_sizeDat = $_POST['S_min_size_'.$val_users_bor_up];
					$resultado_ind = $cargos_Class->Editar_InvDealer($val_users_bor_up,$_POST['idElemento'],$min_sizeDat,$max_sizeDat);
				}
			}
			//Se encarga de actualizar las asignaciones individuales anteriormente creadas
			//Se encarga de crear las asignaciones individuales "Diferente ciudad"
			$Dealers_sel_cr = explode(',', $_POST['SeleccionadosID_no']);
			foreach ($Dealers_sel_cr as $val_users_bor_cr) {
				if($val_users_bor_cr!=""){
					$max_sizeDat = $_POST['N_max_size_'.$val_users_bor_cr];
					$min_sizeDat = $_POST['N_min_size_'.$val_users_bor_cr];
					$resultado_ind = $cargos_Class->Crear_InvDealer($val_users_bor_cr,$_POST['idElemento'],$min_sizeDat,$max_sizeDat);
				}
			}
			//Se encarga de crear las asignaciones individuales "Diferente ciudad"
			//Se encarga de actualizar las asignaciones individuales anteriormente creadas "Diferente ciudad"
			$Dealers_sel_up = explode(',', $_POST['SeleccionadosID_no']);
			foreach ($Dealers_sel_up as $val_users_bor_up) {
				if($val_users_bor_up!=""){
					$max_sizeDat = $_POST['N_max_size_'.$val_users_bor_up];
					$min_sizeDat = $_POST['N_min_size_'.$val_users_bor_up];
					$resultado_ind = $cargos_Class->Editar_InvDealer($val_users_bor_up,$_POST['idElemento'],$min_sizeDat,$max_sizeDat);
				}
			}
			//Se encarga de actualizar las asignaciones individuales anteriormente creadas "Diferente ciudad"

			//actualizar instructores adicionales
			$resultado_ind = $cargos_Class->ActualizarIntructoresAux( $_POST );
			

			$correo = new EmailLudus();
			if ( $resultado > 0 ) {
				if( isset($_POST['notificar_email']) && $_POST['notificar_email']=="on" ){
					//consulta los involucrados en la sesión
					$emails = $cargos_Class->DatosCompletosInvolucrados($_POST['idElemento']);

					//Enviar email a los alumnos
					foreach ($emails as $key => $value) {

						$mensaje = '<table>
											<thead>
												<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/head_email.png"></td>
											</thead>
											<tbody>
											<td style="text-align: center;" >
													<h3>El curso '.$DatosProgramacion['course'].' se ha reprogramado, por favor valida la informaci&oacute;n nuevamente</h3>
													<p><strong>Nombre del curso: </strong>'.$DatosProgramacion['course'].'</p>
													<p><strong>Tipo de Curso: </strong>'.$DatosProgramacion['type'].'</p>
													<p><strong>Hora Inicio: </strong>'.$DatosProgramacion['start_date'].'</p>
													<p><strong>Hora Fin: </strong>'.$DatosProgramacion['end_date'].'</p>
													<p><strong>Profesor: </strong>'.$DatosProgramacion['first_prof'].' '.$DatosProgramacion['last_prof'].'</p>
													<p><strong>Muchas Gracias</strong></p></td>
											</tbody>
											<tfoot>
												<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/footer_email.png" ></td>
											</tfoot>
										</table>';

						$destinatario = $value['email'];
						$asunto = 'Reprogramación del curso '.$datoSchedule['course'];
						$de = 'Reprogramación del curso '.$DatosProgramacion['course'];
					//	$email = $correo->emailInvitacion( $mensaje, $destinatario, $asunto, $de );
					}
					// Fin enviar email a los alumnos

					//enviar email al instructor
					$mensaje = '<table>
									<thead>
										<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/head_email.png"></td>
									</thead>
									<tbody>
									<td style="text-align: center;" >
											<h3>El curso '.$DatosProgramacion['course'].' se ha reprogramado, por favor valida la informaci&oacute;n nuevamente</h3>
											<p><strong>Nombre del curso: </strong>'.$DatosProgramacion['course'].'</p>
											<p><strong>Tipo de Curso: </strong>'.$DatosProgramacion['type'].'</p>
											<p><strong>Hora Inicio: </strong>'.$DatosProgramacion['start_date'].'</p>
											<p><strong>Hora Fin: </strong>'.$DatosProgramacion['end_date'].'</p>
											<p><strong>Profesor: </strong>'.$DatosProgramacion['first_prof'].' '.$DatosProgramacion['last_prof'].'</p>
											<p><strong>Muchas Gracias</strong></p></td>
									</tbody>
									<tfoot>
										<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/footer_email.png" ></td>
									</tfoot>
								</table>';
					$destinatario = $DatosProgramacion['email_prof'];
					$asunto = 'Reprogramación del curso '.$DatosProgramacion['course'];
					$de = 'Reprogramación del curso '.$DatosProgramacion['course'];
					$email = $correo->emailInvitacion( $mensaje, $destinatario, $asunto, $de );
				}else{
						$mensaje = '<table>
											<thead>
												<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/head_email.png"></td>
											</thead>
											<tbody>
												<td style="text-align: center;" >
													<h3>El curso '.$DatosProgramacion['course'].' se ha reprogramado, por favor valida la informaci&oacute;n nuevamente</h3>
													<p><strong>Nombre del curso: </strong>'.$DatosProgramacion['course'].'</p>
													<p><strong>Tipo de Curso: </strong>'.$DatosProgramacion['type'].'</p>
													<p><strong>Hora Inicio: </strong>'.$DatosProgramacion['start_date'].'</p>
													<p><strong>Hora Fin: </strong>'.$DatosProgramacion['end_date'].'</p>
													<p><strong>Profesor: </strong>'.$DatosProgramacion['first_prof'].' '.$DatosProgramacion['last_prof'].'</p>
													<p><strong>Muchas Gracias</strong></p></td>
											</tbody>
											<tfoot>
												<td><img style="width: 100%;" src="https://www.acdelco.com.co/lms/assets/images/footer_email.png" ></td>
											</tfoot>
										</table>';
						$destinatario = $DatosProgramacion['email_prof'];
						$asunto = 'Reprogramación del curso '.$DatosProgramacion['course'];
						$de = 'Reprogramación del curso '.$DatosProgramacion['course'];
					//	$email = $correo->emailInvitacion( $mensaje, $destinatario, $asunto, $de );
				}
			}
			unset($EmailLudus_Class);
			$json = array('resultado' => 'SI', 'email' => $email );
			echo json_encode($json);
		break;

		case 'filtrar':
			/* OK - Filtro completo*/
	        include_once('models/programaciones.php');
	        $usuario_Class = new Cargos();
	        if($_POST['course_id_search']!='0'){
	            $_SESSION['course_id_search_prfl'] = $_POST['course_id_search'];
	        }else{
	            if(isset($_SESSION['course_id_search_prfl'])){
	                unset($_SESSION['course_id_search_prfl']);
	            }
	        }
	        if($_POST['teacher_id_search']!='0'){
	            $_SESSION['teacher_id_search_prfl'] = $_POST['teacher_id_search'];
	        }else{
	            if(isset($_SESSION['teacher_id_search_prfl'])){
	                unset($_SESSION['teacher_id_search_prfl']);
	            }
	        }
	        if($_POST['status_id_search']!='0'){
	            $_SESSION['status_id_search_prfl'] = $_POST['status_id_search'];
	        }else{
	            if(isset($_SESSION['status_id_search_prfl'])){
	                unset($_SESSION['status_id_search_prfl']);
	            }
	        }
	        if($_POST['living_id_search']!='0'){
	            $_SESSION['living_id_search_prfl'] = $_POST['living_id_search'];
	        }else{
	            if(isset($_SESSION['living_id_search_prfl'])){
	                unset($_SESSION['living_id_search_prfl']);
	            }
	        }
	        if($_POST['start_date_search']!=''){
	            $_SESSION['start_date_search_prfl'] = $_POST['start_date_search'];
	        }else{
	            if(isset($_SESSION['start_date_search_prfl'])){
	                unset($_SESSION['start_date_search_prfl']);
	            }
	        }
	        if($_POST['end_date_search']!=''){
	            $_SESSION['end_date_search_prfl'] = $_POST['end_date_search'];
	        }else{
	            if(isset($_SESSION['end_date_search_prfl'])){
	                unset($_SESSION['end_date_search_prfl']);
	            }
	        }
	        $cargos_Class = new Cargos();
	        $cantidad_datos = $cargos_Class->consultaCantidad();
	        $listadosCursos = $cargos_Class->consultaCursos('');
	        $listadosProfesores = $cargos_Class->consultaProfesores('');
	        $listadosSalones = $cargos_Class->consultaSalones('');
	        /* OK - Filtro completo*/
		break;

		case 'Cupos':
			include_once('../models/programaciones.php');
	        $cargos_Class = new Cargos();
			$partes = parse_url($_SERVER['HTTP_REFERER']);
			parse_str($partes['query'], $vars);

			$CantidadCupos = $cargos_Class->consultaCupos('js',$_POST['living_id']);

			$vars['id'] = isset($vars['id']) ? $vars['id'] : '' ;
			$modulos = $cargos_Class->getModules( $_POST['course_id'],  $vars['id']);

	        $MaxCupo = $CantidadCupos['vacant'];
	        $city_id = $CantidadCupos['city_id'];
	        $MinCupo = number_format($MaxCupo - ($MaxCupo*0.30),0);
			$waitCupo = 0;
			
			$json = array();
			$json['resultado'] = 'SI';
			$json['CuposMax'] = $MaxCupo;
			$json['CuposMin'] = $MinCupo;
			$json['CuposWait'] = $waitCupo;
			$json['city_id'] = $city_id;
			$json['modulos'] = $modulos;
			echo json_encode($json);
		break;
		case 'getModulesActivities':
			include_once('../models/programaciones.php');
	        $cargos_Class = new Cargos();
			$CantidadCupos = $cargos_Class->getModulesActivities($_POST['module_id'], $_POST['schedule_id']);
			echo json_encode($CantidadCupos);
		break;
		case 'programar_actividades':
			include_once('../models/programaciones.php');
	        $cargos_Class = new Cargos();
			$CantidadCupos = $cargos_Class->programar_actividades($_POST['datos']);
			echo json_encode($CantidadCupos);
		break;
		case 'getToken':
			include_once('../models/programaciones.php');
	        $cargos_Class = new Cargos();
			$CantidadCupos = $cargos_Class->getToken($_POST['schedule_id']);
			echo json_encode($CantidadCupos);
		break;
		case 'asignar_token':
			include_once('../models/programaciones.php');
	        $cargos_Class = new Cargos();
			$CantidadCupos = $cargos_Class->asignar_token($_POST);
			echo json_encode($CantidadCupos);
		break;
	}//fin switch

}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY z.course';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY z.newcode, z.course '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY d.first_name, d.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY c.start_date '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='3') {
    		$sOrder = ' ORDER BY c.max_inscription_date '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='5'){
    		$sOrder = ' ORDER BY l.living '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='6'){
    		$sOrder = ' ORDER BY c.min_size '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='7'){
    		$sOrder = ' ORDER BY c.max_size '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='8') {
    		$sOrder = ' ORDER BY  c.inscriptions '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/programaciones.php');
	$cargos_Class = new Cargos();
	$datosConfiguracion = $cargos_Class->consultaDatossedes($sWhere,$sOrder,$sLimit);
	//print_r($datosConfiguracion);
	$cantidad_datos = $cargos_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    @session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
			$valOpciones = '<a href="op_programacion.php?opcn=editar&id='.$aInfo['schedule_id'].'" class="label label-success"><i class="fa fa-pencil"></i></a>';

			if($aInfo['type'] != 'WBT'){
				$valOpciones .= ' <a data-toggle="modal" data-target="#myModal" data-schedule_id = "'.$aInfo['schedule_id'].'" data-module_id="'.$aInfo['module_id'].'" class="actividades label label-success"><i class="fa fa-files-o"></i></a>';
			}
		}
		if($_SESSION['max_rol']>=5){
			//$valOpciones = '<a href="op_programacion.php?opcn=editar&id='.$aInfo['schedule_id'].'" class="label label-success"><i class="fa fa-pencil"></i></a>';

			if($aInfo['type'] == 'VCT'){
				$valOpciones .= ' <a data-toggle="modal" data-target="#modalToken" data-schedule_id = "'.$aInfo['schedule_id'].'" class="tokens label label-success"><i class="fa fa-camera"></i></a>';
			}
    	}
    	$aItem = array(
			'Sesión '.$aInfo['schedule_id'].'<br>'.$aInfo['newcode'].' '.$aInfo['course'].'<br>(<small>Modulo: '.$aInfo['module'].'</small>)',
    	$aInfo['first_prof'].' '.$aInfo['last_prof'],
			$aInfo['start_date'],
			substr($aInfo['max_inscription_date'],0,10),
			substr($aInfo['review_date'],0,10),
			$aInfo['living'],
			$aInfo['min_size'],
			$aInfo['max_size'],
			$aInfo['inscriptions'],
			$aInfo['duration_time'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['schedule_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/programaciones.php');
	$cargos_Class = new Cargos();
	$cantidad_datos = $cargos_Class->consultaCantidad();
	$listadosCursos = $cargos_Class->consultaCursos('');
	$listadosProfesores = $cargos_Class->consultaProfesores('');
	$listadosSalones = $cargos_Class->consultaSalones('');

    $programacionesCompletas = $cargos_Class->consultaTodaProgramacion('');
}
unset($cargos_Class);
