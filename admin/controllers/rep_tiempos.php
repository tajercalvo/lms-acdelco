<?php
include_once('models/rep_acumulado.php');
$Acumulados_Class = new RepAcumulado();
$datosCursos = $Acumulados_Class->consultaDatos();
$cantidad_datos = 0;

if(isset($_POST['dealer_id']) || isset($_POST['dealer_id_opc']) ){
	if(!isset($_POST['dealer_id'])){
		$_POST['dealer_id'] = $_POST['dealer_id_opc'];
	}
	$datosCursos_all = $Acumulados_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['dealer_id'])){
	$_POST['dealer_id'] = $_GET['dealer_id'];
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Acumulados_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Acumulados_Class);

/*SELECT d.dealer, h.headquarter, us.first_name, us.last_name, sum(m.duration_time)
FROM ludus_modules_results_usr mr, ludus_users us, ludus_headquarters h, ludus_dealers d, ludus_modules m
WHERE mr.user_id = us.user_id AND us.headquarter_id = h.headquarter_id AND h.dealer_id = d.dealer_id AND mr.module_id = m.module_id
AND mr.date_creation BETWEEN '' AND ''
GROUP BY d.dealer, h.headquarter, us.first_name, us.last_name*/