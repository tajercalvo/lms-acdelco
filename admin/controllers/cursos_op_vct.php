<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_GET['opcn'])){
	include_once('models/cursos_op_vct.php');
	$cargos_Class = new Cargos();
	if(isset($_GET['id'])){
		$registroConfiguracion = $cargos_Class->consultaRegistro($_GET['id']);
		$listadosEspecialidades = $cargos_Class->consultaEspecialidades('');
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		$listadosEspecialidades = $cargos_Class->consultaEspecialidades('');
	}
}else if(isset($_POST['opcn'])){
	include_once('../models/cursos_op_vct.php');
	$cargos_Class = new Cargos();

	switch ( $_POST['opcn'] ) {
		// ========================================================
		//
		// ========================================================
		case 'crear':
			$course_id = $cargos_Class->crearCurso($_POST['course'],$_POST['description'],$_POST['prerequisites'],$_POST['type'],$_POST['specialty_id'],$_POST['target']);
			// Inicio carga de archivo .zip
			if (isset($_FILES['archivo']['tmp_name'])) {

				$archivo_zip = $_FILES['archivo']['tmp_name']; // almacenamos el archivo en una variable
				$zip = new ZipArchive;
				if ($zip->open($archivo_zip) === TRUE ) { // abrimos el archivo .zip

						$ruta = '../../assets/fileVCT/VCT_'.$course_id; // nombre de la carpeta donde se extraeran los achivos mas el id del curso
						$fecha_Actual 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
					if ($zip->extractTo($ruta)) { // extraemos los archivos en la carpeta que tiene la variable ruta
						$ficheros_new  = scandir($ruta); // listamos los nuevos archivos
						natsort($ficheros_new); // listamos los nuevos archivos
						$file = 1; // iniiamos la variable file en 1 para darle un orden a los archivos en la base de datos
						foreach ($ficheros_new as $value) { // recoremos el array con los nuevos archivos
							if ($value == '.' || $value == '..' || $value == '__MACOSX') { // scandir devuelve estos puntos como dos ficheros, asi que los anulamos
							}else{

								if(rename ($value, $value.$fecha_Actual)){
									$resultado = $cargos_Class-> Crear_archivos_vct($file, $value, $course_id, ''); // insertamos los nuevos archivos en la base de datos
									$file++; // variable para ordenar los archivos en la base e datos
								}
							}
						} // end foreach

				
						$array_datos = ["resultado" => "SI", 'id' => $course_id];
						echo json_encode($array_datos);
					}
					$zip->close();
				} // end if open($archivo_zip)
				return false; // terminamos la ejecución si existe un archivo
			}else if($course_id){
				$array_datos = ["resultado" => "SI", 'id' => $course_id];
				echo json_encode($array_datos); // devolvemos el array con la lista de los nuevos archivos
			}// end if (isset($_FILES['archivos']
		break;
		// ========================================================
		//
		// ========================================================
		case 'editar':
			if($_POST['status_id'] == 1){
				$progreso_del_curso = Cargos::progress_bar($_POST['idElemento']);
				// *! se modifica de 5 a 3 como número top de validaciones
				if($progreso_del_curso['progreso'] < 2){
					$array_datos = ["resultado" => "incompleto", "datos" => $progreso_del_curso['datos']]; // creamos array listo para devlver
					echo json_encode($array_datos); // devolvemos el array con la lista de los nuevos archivos
					return false;
				}
			}
			$resultado = $cargos_Class->Actualizarcargos($_POST['idElemento'],$_POST['course'],$_POST['status_id'],$_POST['description'],$_POST['prerequisites'],$_POST['type'],$_POST['specialty_id'],$_POST['target']);

			if (isset($_FILES['archivo']['tmp_name'])) {

				$archivo_zip = $_FILES['archivo']['tmp_name']; // almacenamos el archivo en una variable
				$zip = new ZipArchive;
				if ($zip->open($archivo_zip) === TRUE) {// abrimos el archivo .zip

						$ruta = '../../assets/fileVCT/VCT_'.$_POST['idElemento'].''; // nombre de la carpeta donde se extraeran los achivos mas el id del curso
						if (file_exists($ruta)) { // validamos si existe la carpeta
							$ficheros_old  = scandir($ruta); // obtenemos todos los archivos del directorio
							natsort($ficheros_old);
							foreach ($ficheros_old as $archivos_old) {
								if ($archivos_old == '.' || $archivos_old == '..' || $archivos_old == '__MACOSX') {
										 // scandir devuelve estos puntos como dos ficheros, asi que los anulamos
									  }else{
											unlink ($ruta.'/'.$archivos_old); // eliminamos todos los archivos del directorio
										}
							}

						}
					$fecha_Actual 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
					$resultado = $cargos_Class->eliminar_archivos_vct($_POST['idElemento']); // consultamos si el curso tiene archivos en la BD y los eliminamos
					if ($zip->extractTo($ruta)) { // extraemos los archivos en la carpeta que tiene la variable ruta
						$ficheros_new  = scandir($ruta); // listamos los nuevos archivos
						natsort($ficheros_new); // natsort, ordena el array de forma natural
						$file = 1; // iniiamos la variable file en 1 para darle un orden a los archivos en la base de datos
						foreach ($ficheros_new as $value) { // recoremos el array con los nuevos archivos
							if ($value == '.' || $value == '..' || $value == '__MACOSX') { // scandir devuelve estos puntos como dos ficheros, asi que los anulamos
							}else{
								$renombrar_Archivo = $fecha_Actual.$value;
								if(rename ($ruta.'/'.$value, $ruta.'/'.$renombrar_Archivo)){ // renombramos el archivo para que no se quede pegado en cache cuando se actualiza el .zip
									$resultado = $cargos_Class-> Crear_archivos_vct($file, $renombrar_Archivo, $_POST['idElemento'], ''); // insertamos los nuevos archivos en la base de datos
									$file++; // variable para ordenar los archivos en la base e datos
								}
							}
						}
						$array_datos = ["resultado" => "SI"]; // creamos array listo para devlver
						echo json_encode($array_datos); // devolvemos el array con la lista de los nuevos archivos
					}else{
						 echo('{"resultado":"NO, archivos no extraidos"}');
						}
					$zip->close();

				} else {
				   echo('{"resultado":"NO, no se encontro el archivo zip"}');
				}

			}else{
				//print_r($resultado);
				echo json_encode($resultado);
				//echo('{"resultado":"SI"}');
			}
		break;
		// ========================================================
		//
		// ========================================================
		case 'RegistrarNota':
			$resultado = $cargos_Class->RegistrarNota($_POST['Nota'],$_POST['Modulo_id']);
			echo('{"resultado":"SI"}');
		break;
		// ========================================================
		//
		// ========================================================
		case 'CrearImagen':
			if(isset($_FILES['image_new'])){
				if(isset($_FILES['image_new']['tmp_name'])){
					$imgant = $_POST['imgant'];
					$course_id = $_POST['course_id'];
					$nom_archivo1 = $_FILES["image_new"]["name"];
					$new_name1 = "ACDelco_LUDUS_";
					$new_name1 .= date("Ymdhis");
					$new_extension1 = "jpg";
					preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
					switch (strtolower($ext1[2])) {
						case 'jpeg' : $new_extension1 = ".jpg";
							break;
						case 'jpg' : $new_extension1 = ".jpg";
							break;
						case 'JPG' : $new_extension1 = ".jpg";
							break;
						case 'JPEG' : $new_extension1 = ".jpg";
							break;
						default    : $new_extension1 = "no";
							break;
					}
					if($new_extension1 != "no"){
						$new_name1 .= $new_extension1;
						$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../../assets/images/distinciones/".$new_name1);
							if($resultArchivo1==1){
								if($imgant != "" && $imgant != "default.jpg"){
									if(file_exists("../../assets/images/distinciones/".$imgant)){
										unlink("../../assets/images/distinciones/".$imgant);
									}
								}
								$op_cursoImagen = $cargos_Class->actualizaImagen($course_id,$new_name1);
								echo('{"resultado":"SI","archivo":"'.$new_name1.'"}');
							}else{
								echo('{"resultado":"NO"}');
							}
					}else{
						echo('{"resultado":"NO"}');
					}
				}else{
					echo('{"resultado":"NO"}');
				}
			}
		break;
		// ========================================================
		//
		// ========================================================
		case 'cargar_tabla_archivos':
			$array_archivos = $cargos_Class-> get_archivos_vct($_POST['course_id']);
			$array_datos = ["resultado" => "SI", "datos" => $array_archivos];
			echo json_encode($array_datos);
		break;
		// ========================================================
		//
		// ========================================================
		case 'editar_archivos':
			/*print_r($_POST);
			print_r($_FILES); return;*/
			$ruta = '../../assets/fileVCT/VCT_'.$_POST['course_id'].'/';

			if (isset($_FILES['imagen'])) {

				//Validamos si la imagen es la misma del archivo
				$archivo_imagen = $cargos_Class-> consultar_imagen_vct($_POST['coursefile_id']);

				if ($archivo_imagen['valida'] == 'iguales') {

					$archivo_new = $_FILES['imagen']['tmp_name'];
					 $name_new = $_FILES['imagen']['name'];
					 $extencion = strtolower(substr($name_new, -4));
					 $NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
					 $comodin = $NOW_data.''.rand(1,1000);
					if (move_uploaded_file($archivo_new, $ruta.$comodin.$extencion)) {
						$archivo_Actualizado = $cargos_Class-> actualizar_archivos_vct($_POST['numero_nuevo'], $_POST['nombre_nuevo'], $comodin.$extencion, '',$_POST['course_id'], $_POST['coursefile_id']);
					}// end if move_uploaded_file
				}else if (file_exists($ruta.''.$_POST['image'])) {

					if(unlink($ruta.''.$_POST['image'])){
						$archivo_new = $_FILES['imagen']['tmp_name'];
						$name_new = $_FILES['imagen']['name'];
						$extencion = strtolower(substr($name_new, -4));
						 $NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
						 $comodin = $NOW_data.''.rand(1,1000);
						if (move_uploaded_file($archivo_new, $ruta.$comodin.$extencion)) {
							$archivo_Actualizado = $cargos_Class-> actualizar_archivos_vct($_POST['numero_nuevo'], $_POST['nombre_nuevo'], $comodin.$extencion, '',$_POST['course_id'], $_POST['coursefile_id']);
						}
					} //end if unlink
				}else{
						$archivo_new = $_FILES['imagen']['tmp_name'];
						$name_new = $_FILES['imagen']['name'];
						$extencion = strtolower(substr($name_new, -4));
						$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
						$comodin = $NOW_data.''.rand(1,1000);
						if (move_uploaded_file($archivo_new, $ruta.$comodin.$extencion)) {
							$archivo_Actualizado = $cargos_Class-> actualizar_archivos_vct($_POST['numero_nuevo'], $_POST['nombre_nuevo'], $comodin.$extencion, '',$_POST['course_id'], $_POST['coursefile_id']);
						}
				}//end if file_exists
			} // End if $archivo_imagen

			if (isset($_FILES['archivo'])) {

					//Validamos si el archivo es la misma imagen
					 $archivo_imagen = $cargos_Class-> consultar_imagen_vct($_POST['coursefile_id']);

					if($archivo_imagen['valida'] == 'iguales'){
						$archivo_new = $_FILES['archivo']['tmp_name'];
						$nombre_new = $_FILES['archivo']['name'];
						$extencion = strtolower(substr($nombre_new, -4));
						$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
						$comodin = $NOW_data.''.rand(1,1000);
						if (move_uploaded_file($archivo_new, $ruta.$comodin.$extencion)) {
							$archivo_Actualizado = $cargos_Class-> actualizar_archivos_vct($_POST['numero_nuevo'], $_POST['nombre_nuevo'], '', $comodin.$extencion, $_POST['course_id'], $_POST['coursefile_id']);
						} // end if move_uploaded_file

					}else{
						$ruta.$_POST['archivo_vct'];
						if (file_exists($ruta.$_POST['archivo_vct'])) {
						if(unlink($ruta.''.$_POST['archivo_vct'])){
								$archivo_new = $_FILES['archivo']['tmp_name'];
								$nombre_new = $_FILES['archivo']['name'];
								$extencion = strtolower(substr($nombre_new, -4));
								$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
								$comodin = $NOW_data.''.rand(1,1000);
								if (move_uploaded_file($archivo_new, $ruta.$comodin.$extencion)) {
									$archivo_Actualizado = $cargos_Class-> actualizar_archivos_vct($_POST['numero_nuevo'], $_POST['nombre_nuevo'], '', $comodin.$extencion, $_POST['course_id'], $_POST['coursefile_id']);
								} //end move_uploaded_file
							}// end unlink
						}// end if file_exists

					} // end if $archivo_imagen

			}

			$archivo_Actualizado = $cargos_Class-> actualizar_archivos_vct($_POST['numero_nuevo'], $_POST['nombre_nuevo'], '','',  $_POST['course_id'], $_POST['coursefile_id']);
			$array_archivos = $cargos_Class-> get_archivos_vct($_POST['course_id']);
			$array_datos = ["resultado" => "SI", "datos" => $array_archivos, "datos_nuevos" => $archivo_Actualizado]; // creamos array listo para devlver
			$array_datos = ["resultado" => "SI"]; // creamos array listo para devlver
			echo json_encode($array_datos);
		break;
		// ========================================================
		//
		// ========================================================
		case 'crear_archivos':
			$ruta = '../../assets/fileVCT/VCT_'.$_POST['course_id'].'/';

			if (isset($_FILES['archivo'])) {

						$archivo = $_FILES['archivo']['tmp_name'];
						$nombre = $_FILES['archivo']['name'];
						$extencion = strtolower(substr($nombre, -4));
						$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
						$nombre_new = $NOW_data.''.rand(1,1000);

						if (!file_exists($ruta)) {
						    mkdir($ruta, 0777, true);
						}
						if (move_uploaded_file($archivo, $ruta.$nombre_new.$extencion)) {
								$Id_insert = $cargos_Class->Crear_archivos_vct($_POST['crear_numero_nuevo'], $nombre_new.$extencion, $_POST['course_id'], $_POST['crear_nombre_nuevo']);
							}

			} else{
				$Id_insert = $cargos_Class->Crear_archivos_vct($_POST['crear_numero_nuevo'], '', $_POST['course_id'], $_POST['crear_nombre_nuevo']);
			}// end if isset isset($_FILES['archivo'])


			if (isset($_FILES['imagen'])) {

					 $archivo_new = $_FILES['imagen']['tmp_name'];
					 $name_new = $_FILES['imagen']['name'];
					 $extencion = strtolower(substr($name_new, -4));
					 $NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
					 $comodin = $NOW_data.''.rand(1,1000);
					if (move_uploaded_file($archivo_new, $ruta.$comodin.$extencion)) {
						$archivo_Actualizado = $cargos_Class-> actualizar_archivos_vct($_POST['crear_numero_nuevo'], $_POST['crear_nombre_nuevo'], $comodin.$extencion, '',$_POST['course_id'], $Id_insert);
					}

			} // End if isset image

			//$array_archivos = $cargos_Class-> get_archivos_vct($_POST['course_id']);
			//$array_datos = ["resultado" => "SI", "datos" => $array_archivos]; // creamos array listo para devlver
			$array_datos = ["resultado" => "SI"]; // creamos array listo para devlver
			echo json_encode($array_datos);
		break;
		// ========================================================
		//
		// ========================================================
		case 'eliminar_archivo':
			//print_r($_POST);
			//print_r($_FILES);
			$array_datos = array();
			if ( $_POST['archivo_vct'] == '.preg') {
				$cargos_Class->eliminar_respuesta_pregunta_vct($_POST['coursefile_id']);
				$cargos_Class->eliminar_archivo_vct($_POST['coursefile_id']);
				$array_datos = ["resultado" => "SI"];
			}else{
				$ruta = '../../assets/fileVCT/VCT_'.$_POST['course_id'].'/'.$_POST['archivo_vct'];
					if (file_exists($ruta)) {
							//mkdir($ruta, 0777, true);
							unlink ($ruta); // eliminamos el archivos del directorio
							$cargos_Class->eliminar_archivo_vct($_POST['coursefile_id']);
							$array_datos = ["resultado" => "SI"]; // creamos array listo para devlver
						}else{
							$array_datos = ["error" => true, "mensaje" => 'No se encontro la ruta del archivo que quiere elimianr']; // creamos array listo para devlver
						}
			}
			
			echo json_encode($array_datos);
		break;
		// ========================================================
		//
		// ========================================================
		case 'crear_pregunta':
			unset($_POST['opcn']);
			//$question_id = $cargos_Class->crear_pregunta($_POST['course_id'], $_POST['agregar_nombre_nuevo_pregunta']);
			$question_id = $cargos_Class->get_max_id_question($_POST['course_id']);
			$question_id = $question_id['question_id'] + 1;
			$cargos_Class->Crear_archivos_vct($_POST['agregar_numero_nuevo_pregunta'], $_POST['agregar_nombre_nuevo_pregunta'].'.preg', $_POST['course_id'], $question_id);
			if($question_id > 0){
				unset($_POST['agregar_numero_nuevo_pregunta']);
				unset($_POST['agregar_nombre_nuevo_pregunta']);
				foreach ($_POST as $key => $value) {
					if( is_array($value) ){
						$resultado = $cargos_Class->crear_respuestas($_POST['course_id'], $question_id, $value[1], $value[0]);
					}
				} // end foreach
				if($resultado > 0){
					$array['error'] = false;
					$array['mensaje'] = 'Preguntas creadas correctamente';
				}
			}else{
					$array['error'] = true;
					$array['mensaje'] = 'Error al crear las preguntas';
			} // end if
			echo json_encode($array);
		break;
		// ========================================================
		//
		// ========================================================
		case 'editar_preguntas':
			//print_r($_POST); return;
			$cargos_Class->update_pregunta($_POST['pregunta_id'], $_POST['agregar_nombre_nuevo_pregunta']);
			$cargos_Class->eliminar_respuestas($_POST['pregunta_id']);
			foreach ($_POST as $key => $value) {
				if( is_array($value) ){
					$resultado = $cargos_Class->crear_respuestas($_POST['course_id'], $_POST['pregunta_id'], $value[1], $value[0]);
				}
			} // end foreach
			//return;
			if($resultado >= 0){

				$array['error'] = false;
				$array['mensaje'] = 'Modificación exitosa';

			}else{
					$array['error'] = true;
					$array['mensaje'] = 'Error al modificar';
			} // end if
			echo json_encode($array);
		break;

	}// fin switch

	/*elseif($_POST['opcn']=="subirarchivo"){
					// print_r($_FILES);
					$ruta = '../../assets/fileVCT/VCT_'.'juank'.'/';
					$archivo = $_FILES['archivo']['tmp_name'];
					$nombre = $_FILES['archivo']['name'];
					$extencion = strtolower(substr($nombre, -4));
					$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
					$nombre_new = $NOW_data.''.rand(1,1000);
			if (move_uploaded_file($archivo, $ruta.$nombre_new.$extencion)) {
				echo "archivo cargado correctamente";
			}else{
				echo "archivo no cargado";
			}
	}*/
}else{
	include_once('models/cursos_op_vct.php');
	$cargos_Class = new Cargos();
	$cantidad_datos = $cargos_Class->consultaCantidad();
	$estrategias = Cargos::getEstrategias('');
}
unset($cargos_Class);
