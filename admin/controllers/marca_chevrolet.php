<?php


if(isset($_POST['opcn'])){
	if($_POST['opcn']=='editarGaleria'){
		include_once('models/marca_chevrolet.php');
		$Marca_Class = new MarcaChevrolet();
		if(isset($_POST['galerias'])){
			$resu = $Marca_Class->BorraGalerias();
			foreach ($_POST['galerias'] as $iID => $galerias_selec) {
				$resu = $Marca_Class->CrearGalerias($galerias_selec);
			}
		}
		header("location: marca_chevrolet.php");
	}elseif ($_POST['opcn']=='pdfApoyo') {
		include_once('models/marca_chevrolet.php');
		$Marca_Class = new MarcaChevrolet();
		//Proceso de creación para el archivo
		if(isset($_FILES['pdf_new'])){
			if(isset($_FILES['pdf_new']['tmp_name'])){
				$new_name2 = "";
				$pdfant = "";
				if(isset($_POST['pdfant'])){
					$pdfant = $_POST['pdfant'];
				}
				$nom_archivo2 = $_FILES["pdf_new"]["name"];
				$new_name2 = "ACDelco_LUDUS_";
				$new_name2 .= date("Ymdhis");
				$new_extension2 = "jpg";
				preg_match("'^(.*)\.(pdf|PDF|zip|ZIP|jpg|JPG|png|PNG|gif|GIF)$'i", $nom_archivo2, $ext2);
				switch (strtolower($ext2[2])) {
					case 'pdf' : $new_extension2 = ".pdf";
					break;
					case 'PDF' : $new_extension2 = ".pdf";
					break;
					case 'zip' : $new_extension2 = ".zip";
					break;
					case 'ZIP' : $new_extension2 = ".zip";
					break;
					case 'jpg' : $new_extension2 = ".jpg";
					break;
					case 'JPG' : $new_extension2 = ".jpg";
					break;
					case 'png' : $new_extension2 = ".png";
					break;
					case 'PNG' : $new_extension2 = ".png";
					break;
					case 'gif' : $new_extension2 = ".gif";
					break;
					case 'GIF' : $new_extension2 = ".gif";
					break;
					default    : $new_extension2 = "no";
					break;
				}
				if($new_extension2 != "no"){
					$new_name2 .= $new_extension2;
					$resultArchivo1 = copy($_FILES["pdf_new"]["tmp_name"], "../assets/gallery/marca_chevrolet/".$new_name2);
					if($resultArchivo1==1){
						if($pdfant != "" && $pdfant != "default.png"){
							if(file_exists("../assets/gallery/marca_chevrolet/".$pdfant)){
								unlink("../assets/gallery/marca_chevrolet/".$pdfant);
							}
						}
					}else{
						$new_name2 = "";
					}
				}else{
					$new_name2 = "";
				}
			}else{
				$new_name2 = "";
			}
		}else{
			$new_name2 = "";
		}
		//Proceso de creación para el archivo
		$ResultadoCreacion = $Marca_Class->CrearApoyo($_POST['tittle_file'],$new_name2,$_POST['url_mat']);
		header("location: marca_chevrolet.php");
	}else if($_POST['opcn']=='LikeGalery'){
		include_once('../models/marca_chevrolet.php');
		$Marca_Class = new MarcaChevrolet();
		 	$result = $Marca_Class->CrearLike();
			if($result>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
	}

	else if($_POST['opcn']=='ViewGalery'){
		include_once('../models/marca_chevrolet.php');
		$Marca_Class = new MarcaChevrolet();
		 	$result = $Marca_Class->CrearView();
			if($result>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
	}
}

if(isset($_GET['opcn'])){
	include_once('models/marca_chevrolet.php');
	$Marca_Class = new MarcaChevrolet();
	if($_GET['opcn']=="eliminar" && isset($_GET['id'])){
		$idEliminar = $_GET['id'];
		if(isset($_GET['file'])){
			if(file_exists("../assets/gallery/marca_chevrolet/".$_GET['file'])){
				unlink("../assets/gallery/marca_chevrolet/".$_GET['file']);
			}
		}
		$Marca_Class->BorrarMaterial($idEliminar);
		header("location: marca_chevrolet.php");
	}
}


if(!isset($_POST['opcn'])){
	include_once('models/marca_chevrolet.php');
	$Marca_Class = new MarcaChevrolet();
	$Galeria_datos = $Marca_Class->consultaGaleria('1');
	$Galeria_datosv = $Marca_Class->consultaGaleria('2');
	$Galerias_disponibles = $Marca_Class->consultaTodas();
	$Galeria_pdf = $Marca_Class->consultaApoyo();
	$Cant_like = $Marca_Class->consultaLike();
	$Cant_view = $Marca_Class->ConsultaView();
}
unset($Marca_Class);
