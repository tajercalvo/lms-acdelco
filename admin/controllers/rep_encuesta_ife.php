<?php
include_once('models/rep_encuesta_ife.php');
$Evaluaciones_Class = new RepEncuestas();
$cantidad_datos = 0;
$datosZonas = $Evaluaciones_Class->consultaZonas();
$datosEspecialidades = $Evaluaciones_Class->consultaEspecialidades();
$datosProveedores = $Evaluaciones_Class->consultaProveedores();
if(isset($_POST['start_date_day'])){
	$datosCursos_all = $Evaluaciones_Class->consultaDatosAll($_POST['specialty_id'],$_POST['supplier_id'],$_POST['zone_id'],$_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Evaluaciones_Class->consultaDatosAll($_POST['specialty_id'],$_POST['supplier_id'],$_POST['zone_id'],$_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}
unset($Evaluaciones_Class);

include_once('models/rep_salida.php');
$Salidas_Class = new RepSalida();
$datosCursos = $Salidas_Class->consultaDatos();

unset($Salidas_Class);

