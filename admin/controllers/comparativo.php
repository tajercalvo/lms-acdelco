<?php
include('models/comparativo.php');
include('src/funciones_globales.php');
$datos_Class = new Comparativo();
$cargos = [['charge_id'=>23,'charge'=>'ASESOR DE VEHICULOS NUEVOS'],
['charge_id'=>35,'charge'=>'ASESORES DE VEHICULOS NUEVOS SELECTIVE CHANNEL'],
['charge_id'=>137,'charge'=>'CID CONSULTOR INTEGRAL DIESEL'],
['charge_id'=>32,'charge'=>'ASESOR TECNICO GASOLINA'],
['charge_id'=>31,'charge'=>'ASESOR TECNICO DIESEL'],
['charge_id'=>34,'charge'=>'ASESORES DE REPUESTOS Y ACCESORIOS VEHICULOS LIVIANOS'],
['charge_id'=>135,'charge'=>'AGENTE CONTACT CENTER'],
['charge_id'=>141,'charge'=>'JEFE DE SALA'],
['charge_id'=>105,'charge'=>'JEFE DE TALLER'],
['charge_id'=>89,'charge'=>'GERENTE DE SERVICIO'],
['charge_id'=>94,'charge'=>'GERENTE DE REPUESTOS'],
['charge_id'=>93,'charge'=>'GERENTE ADMINISTRATIVO'],
['charge_id'=>68,'charge'=>'COORDINADOR CONTACT CENTER'],
['charge_id'=>97,'charge'=>'GERENTE DIRECTOR DE MERCADEO'],
['charge_id'=>98,'charge'=>'GERENTE DIRECTOR DE TALENTO HUMANO'],
['charge_id'=>128,'charge'=>'LIDER GMACADEMY'],
['charge_id'=>134,'charge'=>'GERENTE DE VENTAS CORPORATIVAS'],
['charge_id'=>100,'charge'=>'GERENTE Y JEFE COMERCIAL'],
['charge_id'=>49,'charge'=>'GERENTE DE POSVENTA'],
['charge_id'=>99,'charge'=>'GERENTE GENERAL']];
$zona = $zona = ['BOGOTA','CAFETERA','CENTRO','COSTA','MEDELLIN','PACIFICO','SANTANDER'];
$concesionario = ['AUTOGRANDE','AUTONIZA','CENTRODIESEL','CONTINAUTOS','IMPORTADORA CELESTE','INTERNACIONAL DE VEHICULOS','SAN JORGE','CAMINOS','CASA RESTREPO','CAESCA','COLTOLIMA','DISAUTOS','LLANO GRANDE','RIO GRANDE','AUTOLITORAL','COUNTRY MOTORS','MARAUTOS','VEHICOSTA','ANDAR','AUTOLARTE','AYURA MOTOR','DIESEL ANDINO','IMPORTADORA CELESTE','AUTODENAR','AUTOMARCALI','AUTOPACIFICO','AUTOSUPERIOR','YANACONAS ','CAMPESA','CODIESEL'];

if(isset($_POST['start_date_day'])){
	$fecha_inicial = $_POST['start_date_day'];
if(isset($_POST['end_date_day'])){
	$fecha_final = $_POST['end_date_day'];
}else{
	$fecha_final = date("Y-m-d");
}
$colores = array("BOGOTA"=>"#d65050","CAFETERA"=>"#ab7a4b","CENTRO"=>"#609450","COSTA"=>"#bd362f","MEDELLIN"=>"#496cad","PACIFICO"=>"#424242","SANTANDER"=>"#5cc7dd");
$CargosDatosListos = $datos_Class->InformacionCargosDatos($fecha_inicial,$fecha_final);
$cantidad_datos = count($CargosDatosListos);
//$cursosConcesionario = $datos_Class->InformacionCursos();
//print_r($cursosConcesionario);
}
if(isset($_GET['start_date_day'])){
	$fecha_inicial = $_GET['start_date_day'];
	$fecha_final = $_GET['end_date_day'];
	$CargosDatosListos = $datos_Class->InformacionCargosDatos($fecha_inicial,$fecha_final);
	$cantidad_datos = count($CargosDatosListos);
	//print_r($CargosDatosListos);
}
unset($datos_Class);
?>
