<?php

if (isset($_POST['opcn'])) {

		include_once('../models/rep_indnavegacion.php');
		$obj = new RepNavegacionClass();

		if (isset($_POST['zona'])) {
		//echo "zona = ".$_POST["zona"];

		$concesionarios = $obj->consulta_zona_Ajax($_POST["zona"]);

		echo '<option value=""></option>';
		foreach ($concesionarios as $key => $value) {

		    echo '<option value="'.$value['dealer_id']; echo'">'.$value['dealer'].'</option>';
		}

		unset($obj);

		}else if (isset($_POST['dealer'])) {
		//echo "concesionario = ".$_POST["dealer"];
		$sedes = $obj->consulta_concesionario_Ajax($_POST["dealer"]);

		echo '<option value=""></option>';
		foreach ($sedes as $key => $value) {

		    echo '<option value="'.$value['headquarter_id']; echo'">'.$value['headquarter'].'</option>';
		}

		unset($obj);

		}else if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){

			$condicion ='';
			// print_r($_POST);
			if ($_POST['Zona']!='' and $_POST['Zona']!='null') {
					//echo "entro a zona";
					$condicion ="and z.zone_id = ".$_POST['Zona'].' ';

				}

			if ($_POST['Concesionario']!='' and $_POST['Concesionario']!='null') {
					//echo "entro a concesionario";
					$prueba1 = $_POST['Concesionario'];
					$condicion ='and d.dealer_id = '.$_POST['Concesionario'].' ';

				}

			if ($_POST['Sede']!='' and $_POST['Sede']!='null') {
				$prueba = $_POST['Sede'];
				//echo "entro a sede";
				$condicion ='and h.headquarter_id = '.$_POST['Sede'].' ';

			}

			$visitantes_unicos = $obj->consulta_visitantes_unicos1_Ajax($_POST['start_date_day'], $_POST['end_date_day'], $condicion, $_POST['Cargo']);

			if (count($visitantes_unicos)>0) {

				$tabla1 = '<table class="table table-condensed table-vertical-center table-thead-simple">
						<thead>
							<tr>
								<th class="center">No.</th>
								<th class="center">Año</th>
								<th class="center">Mes</th>
								<th class="center">Visitantes únicos</th>
								<th class="center">Total cédulas</th>
								<th class="center">% navegantes </th>
							</tr>
						</thead>

						<tbody>';
						$contador =1;
						foreach ($visitantes_unicos as $key => $value) {

							$tabla1 .= '<tr class="selectable">
										<td class="center" style="padding: 2px; font-size: 80%;">'. $contador .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['ano'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['mes'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['visitantes_unicos'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['cant_cedulas'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. round($value['visitantes_unicos'] / $value['cant_cedulas']*100,1) .'</td>
									</tr>';

					$contador++;
							}
					$tabla1 .= '</tbody>
					</table>';

					$tabla2 = '<table class="table table-condensed table-vertical-center table-thead-simple">
						<thead>
							<tr>
								<th class="center">No.</th>
								<th class="center">Año</th>
								<th class="center">Mes</th>
								<th class="center">Visitas </th>
								<th class="center">Total cédulas</th>
								<th class="center">Visitantes únicos</th>
								<th class="center">Visitas x cédula</th>
							</tr>
						</thead>

						<tbody>';
						$contador =1;
						foreach ($visitantes_unicos as $key => $value) {

							$tabla2 .= '<tr class="selectable">
										<td class="center" style="padding: 2px; font-size: 80%;">'. $contador .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['ano'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['mes'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['visitas'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['cant_cedulas'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. $value['visitantes_unicos'] .'</td>
										<td class="center" style="padding: 2px; font-size: 80%;">'. round($value['visitas'] / $value['visitantes_unicos'],1) .'</td>
									</tr>';
									$contador++;
							}
					$tabla2 .= '</tbody>
					</table>';

				$data = [
					"vacio"	=> "no",
					"data"	=> $visitantes_unicos,
					"tabla1"=> $tabla1,
					"tabla2"=> $tabla2
				];
			}else{
				$data = [
					"vacio"	=> "si",
					"data"	=> $visitantes_unicos
				];
			}
			echo( json_encode( $data ) );


		}// FIN post stardate y enddate
	}//Fin POST
else{

include_once('models/rep_indnavegacion.php');
$RepNavegacion_Class = new RepNavegacionClass();
$cantidad_datos = 0;

$concesionarios = $RepNavegacion_Class->consulta_Concesioanrios();
$trayectorias = $RepNavegacion_Class->consulta_Trayectorias();
$sedes = $RepNavegacion_Class->consulta_sedes();
$zona = $RepNavegacion_Class->consulta_zona();

}

unset($RepNavegacion_Class);
