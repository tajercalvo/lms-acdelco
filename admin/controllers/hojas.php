<?php
if(isset($_GET['opcn'])){
	include_once('models/hojas.php');
	$hojas_Class = new Hojas();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $hojas_Class->consultaRegistro($_GET['id']);
	}else{
		$registroConfiguracion['nombre_lista'] = "";
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/hojas.php');
	$hojas_Class = new Hojas();
	if($_POST['opcn']=="crear"){
		$resultado = $hojas_Class->Crearhojas($_POST['nombre']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
		if(!isset($_POST['feedback'])){
			$_POST['feedback'] = "No fue calificado por el profesor";
		}
		if(!isset($_POST['estado'])){
			$resultado = $hojas_Class->Actualizarhojas($_POST['idElemento'],$_POST['description'],$_POST['target'],$_POST['action'],$_POST['fec_fulfillment']);
		}else{
			$resultado = $hojas_Class->ActualizarhojasF($_POST['idElemento'],$_POST['estado'],$_POST['feedback'],$_POST['score'],$_POST['module_result_usr_id']);
		}
		echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas hojas
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.code';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.code '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='1') {
    		$sOrder = ' ORDER BY t.first_name, t.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='2') {
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY h.date_creation '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY h.score '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY h.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/hojas.php');
	$hojas_Class = new Hojas();
	$datosConfiguracion = $hojas_Class->consultaDatoshojas($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $hojas_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-danger">Pendiente</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-info">Diligenciado</span>';
    	}else{
    		$val_estado = '<span class="label label-success">Gestionado</span>';
    	}
    	$valOpciones = '<a href="op_hoja.php?opcn=editar&id='.$aInfo['waybill_id'].'&course='.$aInfo['newcode'].' | '.$aInfo['course'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	if($aInfo['status_id']==3){
    		$valOpciones .= '</br><a href="op_gestionhoja.php?opcn=ver&id='.$aInfo['waybill_id'].'&course='.$aInfo['newcode'].' | '.$aInfo['course'].'" class="btn-action glyphicons camera_small btn-warning" title="Mi retroalimentación"><i></i></a>';
    	}
    	$aItem = array(
			$aInfo['newcode'].' | '.$aInfo['course'],
			$aInfo['first_prof'].' '.$aInfo['last_prof'],
			$aInfo['first_name'].' '.$aInfo['last_name'],
			$aInfo['date_creation'],
			$aInfo['score'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['waybill_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/hojas.php');
	$hojas_Class = new Hojas();
	$cantidad_datos = $hojas_Class->consultaCantidad();
}
unset($hojas_Class);
