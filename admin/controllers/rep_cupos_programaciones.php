<?php
include_once('models/rep_cupos_programaciones.php');
$Acumulados_Class = new RepCalificacion();
$datosCursos = $Acumulados_Class->consultaDatos();
$cantidad_datos = 0;

if(isset($_POST['start_date_day'])){
	$datosCursos_all = $Acumulados_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Acumulados_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Acumulados_Class);
