<?php
include_once('models/inf_traysalidas.php');
$Salidas_Class = new InfSalida();
$datosConcesionarios = $Salidas_Class->consultaDatos();
$datosEspecialidades = $Salidas_Class->consultaMdetalle('ludus_specialties');
$datosCargos = $Salidas_Class->consultaMdetalle('ludus_charges');
$cantidad_datos = 0;

if( isset($_POST['dealer_id']) ){
	$datosCursos_all = $Salidas_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day'],$_POST['specialty_id'],$_POST['charge_id']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['dealer_id'])){
	$_POST['dealer_id'] = $_GET['dealer_id'];
	$_POST['specialty_id'] = $_GET['specialty_id'];
	$_POST['charge_id'] = $_GET['charge_id'];
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Salidas_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day'],$_POST['specialty_id'],$_POST['charge_id']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Salidas_Class);
