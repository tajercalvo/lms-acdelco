<?php
include_once('models/rep_global.php');
$Globales_Class = new RepGlobales();
$datosCursos = $Globales_Class->consultaDatos();
$cantidad_datos = 0;

if(isset($_POST['charge_id'])){
	$datosCursos_all = $Globales_Class->consultaDatosAll($_POST['charge_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['charge_id'])){
	$_POST['charge_id'] = $_GET['charge_id'];
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$datosCursos_all = $Globales_Class->consultaDatosAll($_POST['charge_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Globales_Class);