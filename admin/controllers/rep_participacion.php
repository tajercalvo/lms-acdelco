<?php
include_once('models/rep_participacion.php');
$Asistencias_Class = new RepParticipacion();
$cantidad_datos = 0;
$listaCursos = $Asistencias_Class->consultaCursos();

if(isset($_POST['start_date_day'])){
	$datosCursos_all = $Asistencias_Class->consultaDatosAll( $_POST['start_date_day'], $_POST['end_date_day'],$_POST['cursosF']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$_POST['cursosF'] = $_GET['cursosF'];
	$datosCursos_all = $Asistencias_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day'],$_POST['cursosF']);
	$cantidad_datos = count($datosCursos_all);
}
unset($Asistencias_Class);
