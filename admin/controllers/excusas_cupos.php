<?php
if(isset($_POST['opcn'])){
	//operaciones con los datos
	switch ( $_POST['opcn'] ) {

		case 'crear':
			include_once('../models/excusas_cupos.php');
			include_once('../src/funciones_globales.php');
			@session_start();
			$resultado = 0;
			$p = $_POST;
			$p['creator_id'] = $_SESSION['idUsuario'];
			$p['file'] = "";
			if(isset($_FILES['excusa'])){
				$p['file'] = Funciones::guardarPDF( 'excusa', 'EXC_', "../../assets/excusas_cupos/" );
			}//fin archivo

			$resultado = Excusas::CrearExcusa( $p );
			if($resultado>0){
				echo json_encode( ["error" => false, "message" => 'La excusa ha sido creada Correctamente, puede ir al listado y buscarla por la identificación del usuario' ] );
			}else{
				echo json_encode( ["error" => true, "message" => 'No se ha logrado subir la información, revise su conexión a internet e intente nuevamente' ] );
			}
		break;

		case 'GetSchedule':
			include_once('../models/excusas_cupos.php');
			// print_r( $_POST );
			$resultado = Notas::GetProgramacion($_POST['usuarioId']);
			if( isset($_POST['excuse_id']) && $_POST['excuse_id'] != "" ){
				$excusa = Notas::getExcusa( $_POST['excuse_id'], '../' );
			}else{
				$excusa = [];
			}
			$retorna = "";

			foreach ($resultado as $key => $Data) {
				$selected = "";
				if( isset( $excusa['invitation_id'] ) && $Data['invitation_id'] == $excusa['invitation_id'] ){
					$selected = "selected";
				}
				$retorna .= '<option value="'.$Data['invitation_id'].':'.$Data['schedule_id'].':'.$Data['module_result_usr_id'].'" '.$selected.'>'.$Data['newcode'].' | '.$Data['course'].' | '.$Data['start_date'].'</option>';
			}
			echo($retorna);
		break;

		case 'editar':
			include_once('../models/excusas_cupos.php');
			include_once( '../src/funciones_globales.php' );
			$p = $_POST;
			$p['file'] = "";
			if( isset($_FILES['excusa']) ){
				$p['file'] = Funciones::guardarPDF( 'excusa', 'EXC_', '../../assets/excusas_cupos/' );
			}
			// print_r( $p );
			// echo("<br>");
			$actualizar_pdf = Excusas::actualizarExcusa( $p );
			$estado = $actualizar_pdf > 0 ? "Se actualizó correctamente" : "No se han realizado cambios";
			$archivo = isset( $p['file'] ) ? $p['file'] : "";
			echo json_encode( ["error" => false, "message" => $estado, "archivo" => $archivo ] );
		break;

		case 'consulta_schedule':
			include_once('../models/excusas_cupos.php');
			@session_start();
			$p = $_POST;
			$p['user_id'] = $_SESSION['idUsuario'];
			$listaSesiones = Excusas::consultaSchedule( $p );
			$datos= [];
			foreach ($listaSesiones as $key => $l ) {
				$datos[] = [
					"id"=> $l['schedule_id'],
					"text" => $l['newcode']." | ".$l['course']." | ".$l['start_date']
				];
			}
			echo json_encode( $datos );
		break;

		case 'getScheduleDealers':
			include_once('../models/excusas_cupos.php');
			@session_start();
			$p = $_POST;
			$p['user_id'] = $_SESSION['idUsuario'];
			$listaCcs = Excusas::getScheduleDealers( $p );
			$num = count( $listaCcs );
			$res = [];
			if( $num > 0 ){
				$res = [ "error" => false, "message" => "OK", "dealers" => $listaCcs ];
			}else{
				$res = [ "error" => true , "message" => "No hay coincidencia de concesionarios para esta sesión"  ];
			}
			echo json_encode( $res );
		break;

	}//fin switch

}else if( isset($_GET['opcn']) ){

	switch ( $_GET['opcn'] ) {
		case 'editar':
			include_once('models/excusas_cupos.php');
			$excusa = Excusas::getExcusa( $_GET['id'] );
			$cantidad_datos = Excusas::consultaCantidad( '' );
			$dealers = Excusas::getScheduleDealers( $excusa,'');

		break;

		case 'nuevo':
			include_once('models/excusas_cupos.php');
			$cantidad_datos = Excusas::consultaCantidad( '' );
		break;

		case 'cursos':
			include_once('../models/excusas_cupos.php');
			$p = $_GET;
			$cursos = Notas::getCursos( $p );
			echo json_encode( [ 'error' => false, 'message' => 'OK', "data" => $cursos ] );
		break;

		case 'sesiones':
			include_once('../models/excusas_cupos.php');
			$p = $_GET;
			$sesiones = Notas::getSesiones( $p );
			echo json_encode( [ 'error' => false, 'message' => 'OK', "data" => $sesiones ] );
		break;
	}//fin switch

}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	@session_start();
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY s.start_date';

    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY s.start_date '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY d.dealer '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY e.file '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY e.status_id '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='5') {
    		$sOrder = ' ORDER BY t.first_name, t.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='6') {
    		$sOrder = ' ORDER BY e.date_creation '.$_GET['sSortDir_0'];
    	}
    }

    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/excusas_cupos.php');
	$datosConfiguracion = Excusas::consultaDatosNota($sWhere,$sOrder,$sLimit,$_GET['mes'],$_GET['anio'], $_GET['dealer']);
	// print_r( $datosConfiguracion );
	$cantidad_datos = Excusas::consultaCantidadFull($sWhere,$sOrder,$sLimit,$_GET['mes'],$_GET['anio'], $_GET['dealer']);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    @session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	$valOpciones = '<a href="../assets/excusas_cupos/'.$aInfo['file'].'" target="_blank" class="btn-action glyphicons hospital_h btn-success"><i></i></a>';
		$valEditar = '';
		$estado = '';
		// print_r( $aInfo );
		switch ($aInfo['status_id'] ) {
			case 1:
				$estado = '<span class="label label-success">Aprobado</span>';
				$valEditar = '<a class="btn btn-primary" href="op_excusa_cupo.php?opcn=editar&id='.$aInfo['excuse_dealer_id'].'"><i class="fa fa-edit"></i></a>';
			break;
			case 2:
				$estado = '<span class="label label-primary">Pendiente</span>';
				if( $_SESSION['max_rol'] >= 5 ){
					$valEditar = '<a class="btn btn-primary" href="op_excusa_cupo.php?opcn=editar&id='.$aInfo['excuse_dealer_id'].'"><i class="fa fa-edit"></i></a>';
				}
			break;
			case 3:
				$estado = '<span class="label label-danger">Rechazado</span>';
				$valEditar = '<a class="btn btn-primary" href="op_excusa_cupo.php?opcn=editar&id='.$aInfo['excuse_dealer_id'].'"><i class="fa fa-edit"></i></a>';
			break;
		}
    	$aItem = array(
			$aInfo['start_date'].' | '.$aInfo['course'].' | '.$aInfo['excuse_dealer_id'],
			$aInfo['dealer'],
			$valOpciones,
			$estado,
			$aInfo['first_creator'].' '.$aInfo['last_creator'],
			$aInfo['date_creation'],
			'DT_RowId' => $aInfo['excuse_dealer_id'],
			$valEditar
		);
		$output['aaData'][] = $aItem;
    }
	// print_r( $output );
    echo json_encode($output);
}else{
	include_once('models/excusas_cupos.php');
	include_once('src/funciones_globales.php');
	$cantidad_datos = Excusas::consultaCantidad( '' );
	$concesionarios = Funciones::getConcesionarios('');
}
