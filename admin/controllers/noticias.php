<?php
if(isset($_POST['opcn'])){
	if($_POST['opcn']=="crear"){
		include_once('models/noticias.php');
		$Noticias_Class = new Noticias();
		$archivo = "";
		//Proceso de creación para la imagen
		if( isset( $_FILES['file_new'] ) ){
			if( isset( $_FILES['file_new']['tmp_name'] ) ){
				include_once('src/funciones_globales.php');
				$archivo = Funciones::guardarPDF('file_new', 'fnew_', '../assets/gallery/source/');
				if( stripos($archivo, "default") !== false ){
					$archivo = "";
				}
			}
		}

		if(isset($_FILES['image_new'])){
			if(isset($_FILES['image_new']['tmp_name'])){
				$imgant = "";
				if(isset($_POST['imgant'])){
					$imgant = $_POST['imgant'];
				}
				$nom_archivo1 = $_FILES["image_new"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../assets/gallery/source/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/source/".$imgant)){
									unlink("../assets/gallery/source/".$imgant);
								}
							}
						}else{
							$new_name1 = "default.png";
						}
				}else{
					$new_name1 = "default.png";
				}
			}else{
				$new_name1 = "default.png";
			}
		}else{
			$new_name1 = "default.png";
		}
		//Proceso de creación para la imagen
		$resultado = $Noticias_Class->CrearNoticia($_POST['news'],$_POST['description'],$new_name1,'1',$_POST['media_id'],$_POST['video_id'], $_POST['category'], $archivo);
		$Noticias_datos = $Noticias_Class->consultaNoticias('1');
		$categorias = $Noticias_Class->listaCategorias();
		$grupos = [];
		foreach ($categorias as $key => $value1 ) {
			$grupos[ $value1['category'] ] = [];
		}
		foreach ( $Noticias_datos as $key => $value2 ) {
			$grupos[ $value2['category'] ][]  = $value2;
		}


	}elseif($_POST['opcn']=="editar"){
		include_once('models/noticias.php');
		$Noticias_Class = new Noticias();
		//Proceso de creación para la imagen
		$archivo = "";
		if( isset( $_FILES['file_new_ed'] ) ){
			if( isset( $_FILES['file_new_ed']['tmp_name'] ) && $_FILES['file_new_ed']['tmp_name'] != "" ){
				include_once('src/funciones_globales.php');
				$archivo = Funciones::guardarPDF('file_new_ed', 'fnew_', '../assets/gallery/source/');
				if( stripos($archivo, "default") !== false ){
					$archivo = "";
				}
			}else{
				$archivo = $_POST['fileant'];
			}
		}else{
			$archivo = $_POST['fileant'];
		}
		$imgant = "";
		if(isset($_POST['imgant'])){
			$imgant = $_POST['imgant'];
		}
		if(isset($_FILES['image_new_ed'])){
			if(isset($_FILES['image_new_ed']['tmp_name']) && $_FILES['image_new_ed']['tmp_name']!="" ){
				$nom_archivo1 = $_FILES["image_new_ed"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new_ed"]["tmp_name"], "../assets/gallery/source/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/source/".$imgant)){
									unlink("../assets/gallery/source/".$imgant);
								}
							}
						}else{
							$new_name1 = $imgant;
						}
				}else{
					$new_name1 = $imgant;
				}
			}else{
				$new_name1 = $imgant;
			}
		}else{
			$new_name1 = $imgant;
		}
		//Proceso de creación para la imagen
		$resultado = $Noticias_Class->ActualizarNoticia($_POST['news_id'],$_POST['news_ed'],$_POST['description_ed'],$new_name1,$_POST['estado'],$_POST['media_id_ed'],$_POST['video_id_ed'], $_POST['category_ed'], $archivo);
	}elseif($_POST['opcn']=="LikeGalery"){
		include_once('../models/noticias.php');
		$Noticias_Class = new Noticias();
		$resultado = $Noticias_Class->CrearLike($_POST['news_id']);
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}
}
if(isset($_GET['id'])){
	include_once('models/noticias.php');
	$Noticias_Class = new Noticias();
	$Noticia_datos = $Noticias_Class->consultaNoticia($_GET['id']);
	$Galeria_datos = $Noticias_Class->consultaGaleria($_GET['id'],'1');
	$Galeria_videos = $Noticias_Class->consultaGaleria($_GET['id'],'2');
}

if( !isset($_POST['opcn']) && !isset($_GET['opcn']) ){
	include_once('models/noticias.php');
	$Noticias_Class = new Noticias();
	$Noticias_datos = $Noticias_Class->consultaNoticias('1');
	$Argumentarios_Fotos = $Noticias_Class->consultaMed('1');
	$Argumentarios_Videos = $Noticias_Class->consultaMed('2');
	$categorias = $Noticias_Class->listaCategorias();
	$grupos = [];
	// Creo un Array que almacena las categorias
	foreach ($categorias as $key => $value1 ) {
		$grupos[ $value1['category'] ] = [];//$grupos['Sin categoria','Nuevas']
	}
	//
	foreach ( $Noticias_datos as $key => $value2 ) {
		$grupos[ $value2['category'] ][]  = $value2;
	}
	
}
unset($Noticias_Class);
