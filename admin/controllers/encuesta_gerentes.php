<?php
if(isset($_POST['Retorno_val'])){
	include_once('models/encuesta_gerentes.php');
	$encuesta_Class = new EncuestaGerentes();
	$val_AVN = '';
	$val_ACC = '';
	$val_CID = '';
	$val_ARA = '';
	$val_TIG = '';
	$val_TID = '';
	if(isset($_POST['AVN'])){
		$val_AVN = 1;
	}
	if(isset($_POST['ACC'])){
		$val_ACC = 1;
	}
	if(isset($_POST['CID'])){
		$val_CID = 1;
	}
	if(isset($_POST['ARA'])){
		$val_ARA = 1;
	}
	if(isset($_POST['TIG'])){
		$val_TIG = 1;
	}
	if(isset($_POST['TID'])){
		$val_TID = 1;
	}
	$RegistrarEncuesta = $encuesta_Class->CreaEncuesta($val_AVN,$val_ACC,$val_CID,$val_ARA,$val_TIG,$val_TID);
}else if(isset($_POST['opcn'])){
	if(isset($_POST['opcn_AVN'])){
		include_once('models/encuesta_gerentes.php');
		$encuesta_Class = new EncuestaGerentes();
		$tray = $_POST['opcn_AVN'];
		$blanda1 = '';
		$blanda2 = '';
		$blanda3 = '';
		$tecnica1 = '';
		$tecnica2 = '';
		$tecnica3 = '';
		if(isset($_POST['AVN1'])){
			if($blanda1==''){
				$blanda1 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda2==''){
				$blanda2 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda3==''){
				$blanda3 = 'Vendale a la Mente para llegar a su Cliente';
			}
		}
		if(isset($_POST['AVN2'])){
			if($blanda1==''){
				$blanda1 = 'Comportamiento del Consumidor';
			}else if($blanda2==''){
				$blanda2 = 'Comportamiento del Consumidor';
			}else if($blanda3==''){
				$blanda3 = 'Comportamiento del Consumidor';
			}
		}
		if(isset($_POST['AVN3'])){
			if($blanda1==''){
				$blanda1 = 'Prospección de Venta por Canal';
			}else if($blanda2==''){
				$blanda2 = 'Prospección de Venta por Canal';
			}else if($blanda3==''){
				$blanda3 = 'Prospección de Venta por Canal';
			}
		}
		if(isset($_POST['AVN4'])){
			if($blanda1==''){
				$blanda1 = 'Venta Persuasiva como abordar clientes al estilo retail';
			}else if($blanda2==''){
				$blanda2 = 'Venta Persuasiva como abordar clientes al estilo retail';
			}else if($blanda3==''){
				$blanda3 = 'Venta Persuasiva como abordar clientes al estilo retail';
			}
		}
		if(isset($_POST['AVN5'])){
			if($blanda1==''){
				$blanda1 = 'Técnicas de Cierre de Ventas';
			}else if($blanda2==''){
				$blanda2 = 'Técnicas de Cierre de Ventas';
			}else if($blanda3==''){
				$blanda3 = 'Técnicas de Cierre de Ventas';
			}
		}
		if(isset($_POST['AVN6'])){
			if($blanda1==''){
				$blanda1 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda2==''){
				$blanda2 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda3==''){
				$blanda3 = 'Productividad y Felicidad en mi Trabajo';
			}
		}
		$RegistrarEncuesta = $encuesta_Class->CreaDetalle($tray,$blanda1,$blanda2,$blanda3,$tecnica1,$tecnica2,$tecnica3);
	}
	if(isset($_POST['opcn_ACC'])){
		$tray = $_POST['opcn_ACC'];
		$blanda1 = '';
		$blanda2 = '';
		$blanda3 = '';
		$tecnica1 = '';
		$tecnica2 = '';
		$tecnica3 = '';
		if(isset($_POST['ACC1'])){
			if($blanda1==''){
				$blanda1 = 'Habilidades Comunicación';
			}else if($blanda2==''){
				$blanda2 = 'Habilidades Comunicación';
			}else if($blanda3==''){
				$blanda3 = 'Habilidades Comunicación';
			}
		}
		if(isset($_POST['ACC2'])){
			if($blanda1==''){
				$blanda1 = 'Cultura de servicio ventas';
			}else if($blanda2==''){
				$blanda2 = 'Cultura de servicio ventas';
			}else if($blanda3==''){
				$blanda3 = 'Cultura de servicio ventas';
			}
		}
		if(isset($_POST['ACC3'])){
			if($blanda1==''){
				$blanda1 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda2==''){
				$blanda2 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda3==''){
				$blanda3 = 'Vendale a la Mente para llegar a su Cliente';
			}
		}
		if(isset($_POST['ACC4'])){
			if($blanda1==''){
				$blanda1 = 'Comportamiento del Consumidor';
			}else if($blanda2==''){
				$blanda2 = 'Comportamiento del Consumidor';
			}else if($blanda3==''){
				$blanda3 = 'Comportamiento del Consumidor';
			}
		}
		if(isset($_POST['ACC5'])){
			if($blanda1==''){
				$blanda1 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda2==''){
				$blanda2 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda3==''){
				$blanda3 = 'Productividad y Felicidad en mi Trabajo';
			}
		}
		$RegistrarEncuesta = $encuesta_Class->CreaDetalle($tray,$blanda1,$blanda2,$blanda3,$tecnica1,$tecnica2,$tecnica3);
	}
	if(isset($_POST['opcn_CID'])){
		$tray = $_POST['opcn_CID'];
		$blanda1 = '';
		$blanda2 = '';
		$blanda3 = '';
		$tecnica1 = '';
		$tecnica2 = '';
		$tecnica3 = '';
		if(isset($_POST['CID1'])){
			if($blanda1==''){
				$blanda1 = 'Cultura de servicio ventas';
			}else if($blanda2==''){
				$blanda2 = 'Cultura de servicio ventas';
			}else if($blanda3==''){
				$blanda3 = 'Cultura de servicio ventas';
			}
		}
		if(isset($_POST['CID2'])){
			if($blanda1==''){
				$blanda1 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda2==''){
				$blanda2 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda3==''){
				$blanda3 = 'Vendale a la Mente para llegar a su Cliente';
			}
		}
		if(isset($_POST['CID3'])){
			if($blanda1==''){
				$blanda1 = 'Administración del Tiempo';
			}else if($blanda2==''){
				$blanda2 = 'Administración del Tiempo';
			}else if($blanda3==''){
				$blanda3 = 'Administración del Tiempo';
			}
		}
		if(isset($_POST['CID4'])){
			if($blanda1==''){
				$blanda1 = 'Liderazgo en Ventas';
			}else if($blanda2==''){
				$blanda2 = 'Liderazgo en Ventas';
			}else if($blanda3==''){
				$blanda3 = 'Liderazgo en Ventas';
			}
		}
		if(isset($_POST['CID5'])){
			if($blanda1==''){
				$blanda1 = 'El Poder del Marketing en los Negocios';
			}else if($blanda2==''){
				$blanda2 = 'El Poder del Marketing en los Negocios';
			}else if($blanda3==''){
				$blanda3 = 'El Poder del Marketing en los Negocios';
			}
		}
		if(isset($_POST['CID6'])){
			if($blanda1==''){
				$blanda1 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda2==''){
				$blanda2 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda3==''){
				$blanda3 = 'Productividad y Felicidad en mi Trabajo';
			}
		}

		if(isset($_POST['CID7'])){
			if($tecnica1==''){
				$tecnica1 = 'Liderazgo y gestión estrategica en el negocio B&C';
			}
		}
		if(isset($_POST['CID8'])){
			if($tecnica1==''){
				$tecnica1 = 'Finanzas, presupuestos y proyectos especiales para negocios B&C';
			}
		}
		if(isset($_POST['CID9'])){
			if($tecnica1==''){
				$tecnica1 = 'Caracterización y manejo de clientes B&C';
			}
		}
		$RegistrarEncuesta = $encuesta_Class->CreaDetalle($tray,$blanda1,$blanda2,$blanda3,$tecnica1,$tecnica2,$tecnica3);
	}
	if(isset($_POST['opcn_ARA'])){
		$tray = $_POST['opcn_ARA'];
		$blanda1 = '';
		$blanda2 = '';
		$blanda3 = '';
		$tecnica1 = '';
		$tecnica2 = '';
		$tecnica3 = '';
		if(isset($_POST['ARA1'])){
			if($blanda1==''){
				$blanda1 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda2==''){
				$blanda2 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda3==''){
				$blanda3 = 'Vendale a la Mente para llegar a su Cliente';
			}
		}
		if(isset($_POST['ARA2'])){
			if($blanda1==''){
				$blanda1 = 'Mejorando Resultados de mi Negociación 1 Y 2';
			}else if($blanda2==''){
				$blanda2 = 'Mejorando Resultados de mi Negociación 1 Y 2';
			}else if($blanda3==''){
				$blanda3 = 'Mejorando Resultados de mi Negociación 1 Y 2';
			}
		}
		if(isset($_POST['ARA3'])){
			if($blanda1==''){
				$blanda1 = 'Poder del Comportamiento';
			}else if($blanda2==''){
				$blanda2 = 'Poder del Comportamiento';
			}else if($blanda3==''){
				$blanda3 = 'Poder del Comportamiento';
			}
		}
		if(isset($_POST['ARA4'])){
			if($blanda1==''){
				$blanda1 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda2==''){
				$blanda2 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda3==''){
				$blanda3 = 'Productividad y Felicidad en mi Trabajo';
			}
		}
		if(isset($_POST['ARA5'])){
			if($blanda1==''){
				$blanda1 = 'Proceso de Venta de Repuestos 1 y 2';
			}else if($blanda2==''){
				$blanda2 = 'Proceso de Venta de Repuestos 1 y 2';
			}else if($blanda3==''){
				$blanda3 = 'Proceso de Venta de Repuestos 1 y 2';
			}
		}
		$RegistrarEncuesta = $encuesta_Class->CreaDetalle($tray,$blanda1,$blanda2,$blanda3,$tecnica1,$tecnica2,$tecnica3);
	}
	if(isset($_POST['opcn_TIG'])){
		$tray = $_POST['opcn_TIG'];
		$blanda1 = '';
		$blanda2 = '';
		$blanda3 = '';
		$tecnica1 = '';
		$tecnica2 = '';
		$tecnica3 = '';
		if(isset($_POST['TIG1'])){
			if($blanda1==''){
				$blanda1 = 'Habilidades Comunicación';
			}else if($blanda2==''){
				$blanda2 = 'Habilidades Comunicación';
			}else if($blanda3==''){
				$blanda3 = 'Habilidades Comunicación';
			}
		}
		if(isset($_POST['TIG2'])){
			if($blanda1==''){
				$blanda1 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda2==''){
				$blanda2 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda3==''){
				$blanda3 = 'Vendale a la Mente para llegar a su Cliente';
			}
		}
		if(isset($_POST['TIG3'])){
			if($blanda1==''){
				$blanda1 = 'Bien Hecho a la Primera Vez 1 y 2';
			}else if($blanda2==''){
				$blanda2 = 'Bien Hecho a la Primera Vez 1 y 2';
			}else if($blanda3==''){
				$blanda3 = 'Bien Hecho a la Primera Vez 1 y 2';
			}
		}
		if(isset($_POST['TIG4'])){
			if($blanda1==''){
				$blanda1 = 'Poder del Comportamiento';
			}else if($blanda2==''){
				$blanda2 = 'Poder del Comportamiento';
			}else if($blanda3==''){
				$blanda3 = 'Poder del Comportamiento';
			}
		}
		if(isset($_POST['TIG5'])){
			if($blanda1==''){
				$blanda1 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda2==''){
				$blanda2 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda3==''){
				$blanda3 = 'Productividad y Felicidad en mi Trabajo';
			}
		}

		if(isset($_POST['TIG7'])){
			if($tecnica1==''){
				$tecnica1 = 'Metrología aplicada en motores gasolina';
			}else if($tecnica2==''){
				$tecnica2 = 'Metrología aplicada en motores gasolina';
			}else if($tecnica3==''){
				$tecnica3 = 'Metrología aplicada en motores gasolina';
			}
		}
		if(isset($_POST['TIG8'])){
			if($tecnica1==''){
				$tecnica1 = 'Interpretación y análisis de alambrados y planos eléctricos';
			}else if($tecnica2==''){
				$tecnica2 = 'Interpretación y análisis de alambrados y planos eléctricos';
			}else if($tecnica3==''){
				$tecnica3 = 'Interpretación y análisis de alambrados y planos eléctricos';
			}
		}
		if(isset($_POST['TIG9'])){
			if($tecnica1==''){
				$tecnica1 = 'Mantenimiento y reparación de suspensión y dirección';
			}else if($tecnica2==''){
				$tecnica2 = 'Mantenimiento y reparación de suspensión y dirección';
			}else if($tecnica3==''){
				$tecnica3 = 'Mantenimiento y reparación de suspensión y dirección';
			}
		}
		if(isset($_POST['TIG10'])){
			if($tecnica1==''){
				$tecnica1 = 'Diagnóstico estratégico';
			}else if($tecnica2==''){
				$tecnica2 = 'Diagnóstico estratégico';
			}else if($tecnica3==''){
				$tecnica3 = 'Diagnóstico estratégico';
			}
		}
		if(isset($_POST['TIG11'])){
			if($tecnica1==''){
				$tecnica1 = 'Conocimiento y principios del sistema de frenos hidráulico y practicas de mantenimiento';
			}else if($tecnica2==''){
				$tecnica2 = 'Conocimiento y principios del sistema de frenos hidráulico y practicas de mantenimiento';
			}else if($tecnica3==''){
				$tecnica3 = 'Conocimiento y principios del sistema de frenos hidráulico y practicas de mantenimiento';
			}
		}
		if(isset($_POST['TIG12'])){
			if($tecnica1==''){
				$tecnica1 = 'Análisis de fallas transmisión manual, y diagnostico de transfer y diferenciales';
			}else if($tecnica2==''){
				$tecnica2 = 'Análisis de fallas transmisión manual, y diagnostico de transfer y diferenciales';
			}else if($tecnica3==''){
				$tecnica3 = 'Análisis de fallas transmisión manual, y diagnostico de transfer y diferenciales';
			}
		}
		$RegistrarEncuesta = $encuesta_Class->CreaDetalle($tray,$blanda1,$blanda2,$blanda3,$tecnica1,$tecnica2,$tecnica3);
	}
	if(isset($_POST['opcn_TID'])){
		$tray = $_POST['opcn_TID'];
		$blanda1 = '';
		$blanda2 = '';
		$blanda3 = '';
		$tecnica1 = '';
		$tecnica2 = '';
		$tecnica3 = '';
		if(isset($_POST['TID1'])){
			if($blanda1==''){
				$blanda1 = 'Habilidades Comunicación';
			}else if($blanda2==''){
				$blanda2 = 'Habilidades Comunicación';
			}else if($blanda3==''){
				$blanda3 = 'Habilidades Comunicación';
			}
		}
		if(isset($_POST['TID2'])){
			if($blanda1==''){
				$blanda1 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda2==''){
				$blanda2 = 'Vendale a la Mente para llegar a su Cliente';
			}else if($blanda3==''){
				$blanda3 = 'Vendale a la Mente para llegar a su Cliente';
			}
		}
		if(isset($_POST['TID3'])){
			if($blanda1==''){
				$blanda1 = 'Bien Hecho a la Primera Vez 1 y 2';
			}else if($blanda2==''){
				$blanda2 = 'Bien Hecho a la Primera Vez 1 y 2';
			}else if($blanda3==''){
				$blanda3 = 'Bien Hecho a la Primera Vez 1 y 2';
			}
		}
		if(isset($_POST['TID4'])){
			if($blanda1==''){
				$blanda1 = 'Poder del Comportamiento';
			}else if($blanda2==''){
				$blanda2 = 'Poder del Comportamiento';
			}else if($blanda3==''){
				$blanda3 = 'Poder del Comportamiento';
			}
		}
		if(isset($_POST['TID5'])){
			if($blanda1==''){
				$blanda1 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda2==''){
				$blanda2 = 'Productividad y Felicidad en mi Trabajo';
			}else if($blanda3==''){
				$blanda3 = 'Productividad y Felicidad en mi Trabajo';
			}
		}

		if(isset($_POST['TID7'])){
			if($tecnica1==''){
				$tecnica1 = 'Inspección, diagnostico y reparación de los sistemas de frenos 100% aire con ABS';
			}else if($tecnica2==''){
				$tecnica2 = 'Inspección, diagnostico y reparación de los sistemas de frenos 100% aire con ABS';
			}else if($tecnica3==''){
				$tecnica3 = 'Inspección, diagnostico y reparación de los sistemas de frenos 100% aire con ABS';
			}
		}
		if(isset($_POST['TID8'])){
			if($tecnica1==''){
				$tecnica1 = 'Inspección, diagnóstico y reparación de diferenciales';
			}else if($tecnica2==''){
				$tecnica2 = 'Inspección, diagnóstico y reparación de diferenciales';
			}else if($tecnica3==''){
				$tecnica3 = 'Inspección, diagnóstico y reparación de diferenciales';
			}
		}
		if(isset($_POST['TID9'])){
			if($tecnica1==''){
				$tecnica1 = 'Inspección, diagnóstico y reparación de transmisiones ISUZU';
			}else if($tecnica2==''){
				$tecnica2 = 'Inspección, diagnóstico y reparación de transmisiones ISUZU';
			}else if($tecnica3==''){
				$tecnica3 = 'Inspección, diagnóstico y reparación de transmisiones ISUZU';
			}
		}
		if(isset($_POST['TID10'])){
			if($tecnica1==''){
				$tecnica1 = 'Diagnóstico general de suspension y sistemas de dirección';
			}else if($tecnica2==''){
				$tecnica2 = 'Diagnóstico general de suspension y sistemas de dirección';
			}else if($tecnica3==''){
				$tecnica3 = 'Diagnóstico general de suspension y sistemas de dirección';
			}
		}
		if(isset($_POST['TID11'])){
			if($tecnica1==''){
				$tecnica1 = 'Procedimiento de reparación de los motores electrónicos ISUZU 4JJ1 Common Rail';
			}else if($tecnica2==''){
				$tecnica2 = 'Procedimiento de reparación de los motores electrónicos ISUZU 4JJ1 Common Rail';
			}else if($tecnica3==''){
				$tecnica3 = 'Procedimiento de reparación de los motores electrónicos ISUZU 4JJ1 Common Rail';
			}
		}
		$RegistrarEncuesta = $encuesta_Class->CreaDetalle($tray,$blanda1,$blanda2,$blanda3,$tecnica1,$tecnica2,$tecnica3);
	}
	$_GET['result'] = md5('GMACADEMY_VAlEnc');
	unset($_SESSION['evl_obligatoria']);
}else{
	include_once('models/encuesta_gerentes.php');
	$encuesta_Class = new EncuestaGerentes();
	$ConsultaDatEncuesta = $encuesta_Class->ConsultaEncuesta();
	if($ConsultaDatEncuesta['encuesta_ger_id']>0){
		$_POST['Retorno_val'] = '1';
		if($ConsultaDatEncuesta['AVN']=='1'){
			$_POST['AVN'] = 1;
		}
		if($ConsultaDatEncuesta['ACC']=='1'){
			$_POST['ACC'] = 1;
		}
		if($ConsultaDatEncuesta['CID']=='1'){
			$_POST['CID'] = 1;
		}
		if($ConsultaDatEncuesta['ARA']=='1'){
			$_POST['ARA'] = 1;
		}
		if($ConsultaDatEncuesta['TIG']=='1'){
			$_POST['TIG'] = 1;
		}
		if($ConsultaDatEncuesta['TID']=='1'){
			$_POST['TID'] = 1;
		}
		$RegistrarEncuesta = $ConsultaDatEncuesta['encuesta_ger_id'];
	}
}
/*
if(isset($review_enc_id)){
	include_once('models/encuestar.php');
	$evaluar_Class = new encuestarlos();
	$registroEncuesta = $evaluar_Class->consultaEncuesta(2);
	$registroRespuestas = $evaluar_Class->consultaRespuestas(2);
}

if(isset($_POST['opcn'])){
	include_once('../models/encuestar.php');
	$evaluar_Class = new encuestarlos();
	if($_POST['opcn']=="GuardaEncuesta"){
		$var_FechaDebio = $_POST['FechaDebio'];
		$var_FechaDebio = substr($var_FechaDebio, 0, 10);
		$var_FechaDebio = str_replace('-', '', $var_FechaDebio);
		$var_FechaDebio = $var_FechaDebio+2;
		$varAhora_fec = date("Ymd");
		$var_score = "0";
		if($var_FechaDebio>$varAhora_fec){
			$var_score = "5";
		}else{
			$var_score = "0";
		}
		$resultado = $evaluar_Class->CrearRespEncuesta(2,5,$var_score);
		if($resultado>0){
			$resultado_35 = $evaluar_Class->CrearRespuesta($resultado,35,$_POST['val_rep35'],$_POST['_val_resp35'],'00:00');
			$resultado_36 = $evaluar_Class->CrearRespuesta($resultado,36,$_POST['val_rep36'],$_POST['_val_resp36'],'00:00');
			$resultado_37 = $evaluar_Class->CrearRespuesta($resultado,37,$_POST['val_rep37'],$_POST['_val_resp37'],'00:00');
			$resultado_38 = $evaluar_Class->CrearRespuesta($resultado,38,$_POST['val_rep38'],$_POST['_val_resp38'],'00:00');
			$resultado_39 = $evaluar_Class->CrearRespuesta($resultado,39,$_POST['val_rep39'],$_POST['_val_resp39'],'00:00');
			$resultado_40 = $evaluar_Class->CrearRespuesta($resultado,40,$_POST['val_rep40'],$_POST['_val_resp40'],'00:00');
			$resultado_41 = $evaluar_Class->CrearRespuesta($resultado,41,$_POST['val_rep41'],$_POST['_val_resp41'],'00:00');
			$resultado_42 = $evaluar_Class->CrearRespuesta($resultado,42,$_POST['val_rep42'],$_POST['_val_resp42'],'00:00');
			$resultado_43 = $evaluar_Class->CrearRespuesta($resultado,43,$_POST['val_rep43'],$_POST['_val_resp43'],'00:00');
			$resultado_44 = $evaluar_Class->CrearRespuesta($resultado,44,0,$_POST['val_rep44'],'00:00');
				include('../models/usuarios.php');
				$usuario_Class = new Usuarios();
				$cant_user = $usuario_Class->ConsultaEncuestasSat();
				unset($usuario_Class);
			echo('{"resultado":"SI","reviews_answer_id":"'.$resultado.'"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}
	if($_POST['opcn']=="RegistrarRespuesta"){
		
		if($resultado>0){
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}
}


unset($evaluar_Class);
*/