<?php
if(isset($_GET['opcn'])){
	include_once('models/sedes.php');
	$cargos_Class = new Cargos();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $cargos_Class->consultaRegistro( $_GET['id'] );
		$listadosCiudades = $cargos_Class->consultaCiudades('');
		$listadosConcesionarios = $cargos_Class->consultaConcesionarios('');
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		$listadosCiudades = $cargos_Class->consultaCiudades('');
		$listadosConcesionarios = $cargos_Class->consultaConcesionarios('');
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/sedes.php');
	$cargos_Class = new Cargos();
	switch ( $_POST['opcn'] ) {
		case 'crear':
			$p = $_POST;
			unset( $p['idElemento'] );
			unset( $p['opcn'] );
			$resultado = $cargos_Class->CrearSede( $p );
			if($resultado>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;
		case 'editar':
			$p = $_POST;
			$headquarter_id = $p['idElemento'];
			unset( $p['idElemento'] );
			unset( $p['opcn'] );
			$resultado = $cargos_Class->ActualizarSede( $p, $headquarter_id );
			if($resultado>0){
				echo('{"resultado":"SI"}');
			}else{
				echo('{"resultado":"NO"}');
			}
		break;
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.headquarter';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.headquarter '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY d.dealer '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY c.bac '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY c.type_headquarter '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY c.address1 '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='5'){
    		$sOrder = ' ORDER BY c.phone1 '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='6'){
    		$sOrder = ' ORDER BY z.area '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='7') {
    		$sOrder = ' ORDER BY s.first_name, s.last_name '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='8') {
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/sedes.php');
	$cargos_Class = new Cargos();
	$datosConfiguracion = $cargos_Class->consultaDatossedes($sWhere,$sOrder,$sLimit);
	//print_r($datosConfiguracion);
	$cantidad_datos = $cargos_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '<a href="op_sede.php?opcn=editar&id='.$aInfo['headquarter_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
			$nomHeadquarter = $aInfo['headquarter'];
			if($aInfo['rate_course']!="0"){
					//$nomHeadquarter .= '<br> <b>$ '.number_format($aInfo['rate_course'],0)."</b>";
			}
			$aItem = array(
				$nomHeadquarter,
				$aInfo['dealer'],
				$aInfo['type_headquarter'],
				$aInfo['address1'],
				$aInfo['phone1'],
				$aInfo['area'],
				$val_estado,
				$valOpciones,
				'DT_RowId' => $aInfo['headquarter_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/sedes.php');
	$cargos_Class = new Cargos();
	$cantidad_datos = $cargos_Class->consultaCantidad();
	$listadosCiudades = $cargos_Class->consultaCiudades('');
	$listadosConcesionarios = $cargos_Class->consultaConcesionarios('');
}
unset($cargos_Class);
