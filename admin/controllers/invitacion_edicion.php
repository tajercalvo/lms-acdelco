<?php

if(isset($_POST['opcn'])){
	//operaciones con los datos
	switch ( $_POST['opcn'] ) {

		case 'sel_curso':
			include_once('../models/invitacion_edicion.php');
			$invitaciones_Class = new Invitaciones();
			$sesion_datos = $invitaciones_Class->consulta_Schedule( $_POST['curso'], "../" );
			echo( json_encode( $sesion_datos ) );
		break;

		case 'consulta_curso':
			$json = [];
			include_once('../models/invitacion_edicion.php');
			$invitaciones_Class = new Invitaciones();

			$cursos_datos = $invitaciones_Class->consulta_Cursos($_POST['searchTerm'], "../" );
			foreach ($cursos_datos as $key => $cursos_datos) {
				 $json[] = ['id'=>$cursos_datos['course_id'], 'text'=>($cursos_datos['newcode'].' | '.$cursos_datos['type'].' | '.$cursos_datos['course'])];
			}
			echo json_encode($json);
		break;

		case 'invitados_schedule':
			include_once('../models/invitacion_edicion.php');
			$schedule = Invitaciones::consultaSchedule( $_POST['schedule_id'] );
			$schedule_invitados = Invitaciones::consulta_Invitacion_Schedule( $_POST['schedule_id'] );
			$new_sesion_datos = Invitaciones::consulta_New_Schedule( $schedule['course_id'], $schedule['start_date'] );
			// $cantidad_datos = count($schedule_invitados);
			$data = [
				"error" => false,
				"message" => "OK",
				"usuarios" => $schedule_invitados,
				"sesiones" => $new_sesion_datos
			];
			echo( json_encode( $data ) );
		break;

		// case 'sel_curso_new_schedule':
		// 	include_once('../models/invitacion_edicion.php');
		// 	$new_sesion_datos = Invitaciones::consulta_New_Schedule( $_POST['schedule_id'], "../" );
		// 	echo( json_encode( $new_sesion_datos ) );
		// break;

		case 'modificar_sesion':
			include_once('../models/invitacion_edicion.php');
			$invitaciones_Class = new Invitaciones();
			//print_r($_POST);

			//Primero se consulta que el usuario no tenga una invitacion existente en la nueva sesion
			$invitacion_existente = $invitaciones_Class->consulta_Existencia_Invitacion( $_POST['user_id'], $_POST['new_schedule_id'], "../" );
			if ( $invitacion_existente > 0) {
				echo('{"resultado":"FALSE1"}');
			}else {
				//Se cambia los cupos de la antigua sesion a la nueva sesion
				$cambio_cupos = $invitaciones_Class->edita_Dealer_Schedule( $_POST['new_schedule_id'], $_POST['schedule_id'], $_POST['dealer_id'], "../" );

				if( $cambio_cupos > 0 ){
					//Se cambia la sesion en la invitacion del usuario
					$cambio_schedule = $invitaciones_Class->Edicion_Schedule_User_Invitacion( $_POST['new_schedule_id'], $_POST['schedule_id'], $_POST['user_id'], $_POST['invitation_id'], "../" );

					if ( $cambio_schedule > 0 ) {
						//Se elimina el registro de la sesion antigua en la inscripcion
						$inscripcion = $invitaciones_Class->borrar_User_Inscriptions( $_POST['schedule_id'], $_POST['user_id'], $_POST['course_id'], "../" );

						if ( $inscripcion > 0 ) {
							echo('{"resultado":"TRUE"}');
						} else {
							//echo "Error de eliminar inscripcion";
							echo('{"resultado":"FALSE eliminando el registro antiguo"}');
						}

					}  else {
						//echo "Error de cambio sesion en invitacion";
						echo('{"resultado":"FALSE cambiar la sesion en invitacion "}');
					}
				} else {
					//echo "Error en cambio de cupos";
					echo('{"resultado":"FALSE cambiar cupos antiguos a nueva max"}');
				}

			}

		break;


	}

}
else{
	include_once('models/invitacion_edicion.php');
	$cantidad_datos = 0;
	$invitaciones_Class = new Invitaciones();
	$cursos_datos = $invitaciones_Class->consulta_All_Cursos();
}
unset($invitaciones_Class);
