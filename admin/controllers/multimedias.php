<?php
if(isset($_GET['opcn'])){
	include_once('models/multimedias.php');
	$multimedias_Class = new Multimedias();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $multimedias_Class->consultaRegistro($_GET['id']);
	}else{
		$registroConfiguracion['nombre_lista'] = "";
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('models/multimedias.php');
	$multimedias_Class = new Multimedias();
	if($_POST['opcn']=="crear"){
		//Proceso de creación para la imagen
		if(isset($_FILES['image_new'])){
			if(isset($_FILES['image_new']['tmp_name'])){
				$imgant = "";
				if(isset($_POST['imgant'])){
					$imgant = $_POST['imgant'];
				}
				$nom_archivo1 = $_FILES["image_new"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../assets/gallery/thumbs/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/thumbs/".$imgant)){
									unlink("../assets/gallery/thumbs/".$imgant);
								}
							}
						}else{
							$new_name1 = "default.png";
						}
				}else{
					$new_name1 = "default.png";
				}
			}else{
				$new_name1 = "default.png";
			}
		}else{
			$new_name1 = "default.png";
		}
		//Proceso de creación para la imagen
		$resultado = $multimedias_Class->CrearMultimedias($_POST['media'],$_POST['description'],$_POST['type_gal'],$new_name1);
		$galeria_datos = $multimedias_Class->consultaGalerias($source);
	}elseif($_POST['opcn']=="editar"){
		//Proceso de creación para la imagen
			$imgant = "";
			if(isset($_POST['imgant'])){
				$imgant = $_POST['imgant'];
			}
		if(isset($_FILES['image_new_ed'])){
			if(isset($_FILES['image_new_ed']['tmp_name']) && $_FILES['image_new_ed']['tmp_name']!="" ){
				$nom_archivo1 = $_FILES["image_new_ed"]["name"];
				$new_name1 = "ACDelco_LUDUS_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG)$'i", $nom_archivo1, $ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES["image_new_ed"]["tmp_name"], "../assets/gallery/thumbs/".$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								if(file_exists("../assets/gallery/thumbs/".$imgant)){
									unlink("../assets/gallery/thumbs/".$imgant);
								}
							}
						}else{
							$new_name1 = $imgant;
						}
				}else{
					$new_name1 = $imgant;
				}
			}else{
				$new_name1 = $imgant;
			}
		}else{
			$new_name1 = $imgant;
		}
		//Proceso de creación para la imagen
		$resultado = $multimedias_Class->ActualizarMultimedias($_POST['media_id'],$_POST['media_ed'],$_POST['description_ed'],$_POST['type_gal'],$new_name1,$_POST['estado']);
		$galeria_datos = $multimedias_Class->consultaGalerias($source);
	}
}else{
	include_once('models/multimedias.php');
	$multimedias_Class = new Multimedias();
	$galeria_datos = $multimedias_Class->consultaGalerias($source);
}
unset($multimedias_Class);