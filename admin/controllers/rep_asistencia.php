<?php
include_once('models/rep_asistencia.php');
$Asistencias_Class = new RepAsistencias();
$datosCursos = $Asistencias_Class->consultaDatos();
$cantidad_datos = 0;

if(isset($_POST['schedule_id'])){
	$datosCursos_all = $Asistencias_Class->consultaDatosAll($_POST['schedule_id']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['schedule_id'])){
	$_POST['schedule_id'] = $_GET['schedule_id'];
	$datosCursos_all = $Asistencias_Class->consultaDatosAll($_POST['schedule_id']);
	$cantidad_datos = count($datosCursos_all);
}

unset($Asistencias_Class);