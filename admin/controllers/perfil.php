<?php
$charge_user = "";
$coordinador = 0;
if( isset( $_POST['opcn'] ) ){
	switch ( $_POST['opcn'] ) {

		case 'activar':
			include_once('../models/evaluar.php');
			@session_start();
			$p = $_POST;
			$p['creator_id'] = $_SESSION['idUsuario'];
			$res = evaluarlos::activarEvNoTime( $p );
			if( $res == -1 ){
				$data = ['error' => true, "message" => "Ya hay un registro de evaluación pendiente"];
			}else if( $res > 0 ){
				$data = ['error' => false, "message" => "OK", "review_notime_id" => $res ];
			}else{
				$data = ['error' => true, "message" => "No se ha insertado ningún registro, actualice la pagina e intente nuevamente."];
			}
			echo json_encode( $data );
		break;

		case 'inactivar':
			include_once('../models/evaluar.php');
			@session_start();
			$p = $_POST;
			$p['editor_id'] = $_SESSION['idUsuario'];
			$res = evaluarlos::inactivarEvNoTime( $p );
			if ( $res > 0 ) {
				$data = ['error' => false, "message" => "Se ha modificado correctamente"];
			}else{
				$data = ['error' => true, "message" => "No hay datos para modificar"];
			}
			echo json_encode( $data );
		break;
		case 'agregarTrayectoria':
			include('../models/usuarios.php');
			$usuario_Class = new Usuarios();
			$data = $usuario_Class->agregarTrayectoria($_POST);
			echo json_encode( $data );
		break;
	}//fin switch
}else if(isset($idUsuario_log)){
	$IdUsuario = $idUsuario_log;
	include('models/usuarios.php');
	$usuario_Class = new Usuarios();

	$datosUsuario = $usuario_Class->consultaDatosUsuario($IdUsuario);
	$DatMaxNav = $usuario_Class->LastNav($IdUsuario);

	$charge_user = $usuario_Class->consultaDatosCargos($IdUsuario);
	$rol_user = $usuario_Class->consultaDatosRoles($IdUsuario);
	$type_user = $usuario_Class->tipoUsuarios();


	$Salidas = 0;
	$Horas = 0;
	$CantidadSalidas = $usuario_Class->CantidadSalidas($IdUsuario);
	if($CantidadSalidas['tiempo']>0){
		$Salidas = $CantidadSalidas['tiempo']/8;
		$Horas = $CantidadSalidas['tiempo'];
	}
	$anoCurso = date('Y');

	$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
	$charge_inactive = $usuario_Class->consultaAllDatosCargos('','2');
	$rol_active = $usuario_Class->consultaAllDatosRoles('','1');
	$rol_inactive = $usuario_Class->consultaAllDatosRoles('','2');

	unset($usuario_Class);
}else if(isset($_GET['id'])&&(!isset($_GET['opcn']))){
	$IdUsuario = $_GET['id'];
	include('models/usuarios.php');
	$usuario_Class = new Usuarios();
	$datosUsuario = $usuario_Class->consultaDatosUsuario($IdUsuario);
	$DatMaxNav = $usuario_Class->LastNav($IdUsuario);
	$charge_user = $usuario_Class->consultaDatosCargos($IdUsuario);
	$trayectorias = $usuario_Class->consultaTrayectorias();
	$rol_user = $usuario_Class->consultaDatosRoles($IdUsuario);
	$type_user = $usuario_Class->tipoUsuarios();
$Salidas = 0;
$Horas = 0;
$CantidadSalidas = $usuario_Class->CantidadSalidas($IdUsuario);
if($CantidadSalidas['tiempo']>0){
	$Salidas = $CantidadSalidas['tiempo']/8;
	$Horas = $CantidadSalidas['tiempo'];
}
$anoCurso = date('Y');

	$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
	$charge_inactive = $usuario_Class->consultaAllDatosCargos('','2');
	$rol_active = $usuario_Class->consultaAllDatosRoles('','1');
	$rol_inactive = $usuario_Class->consultaAllDatosRoles('','2');
	$listadoConcesionarios = $usuario_Class->consultaAllDatosConcesionarios('');
	unset($usuario_Class);

// opcion editar usuario
}else if(isset($_GET['id'])&&(isset($_GET['opcn']))&&($_GET['opcn']=='editar')){
	$IdUsuario = $_GET['id'];
	include('models/usuarios.php');
	include('models/evaluar.php');
	$usuario_Class = new Usuarios();
	$ev_pendientes = evaluarlos::evaluacionesNoTime( $_GET['id'], '' );
	$datosUsuario = $usuario_Class->consultaDatosUsuario($IdUsuario);
	$DatMaxNav = $usuario_Class->LastNav($IdUsuario);
	$charge_user = $usuario_Class->consultaDatosCargos($IdUsuario);
	$rol_user = $usuario_Class->consultaDatosRoles($IdUsuario);
	//concesionarios asignados a los coordinadores
	$ccs_coordinadores = Usuarios::getCoordinadoresCcs( $IdUsuario, '' );
	$type_user = $usuario_Class->tipoUsuarios();
	// $zonas = $usuario_Class->getZonas();

	// if( strpos( $datosUsuario['zone_id'], ',' ) !== false ){
	// 	$zonas_user = explode( ',', $datosUsuario['zone_id'] );
	// }else{
	// 	$zonas_user[] = $datosUsuario['zone_id'];
	// }

	

	$Salidas = 0;
	$Horas = 0;
	$CantidadSalidas = $usuario_Class->CantidadSalidas($IdUsuario);
	if($CantidadSalidas['tiempo']>0){
		$Salidas = $CantidadSalidas['tiempo']/8;
		$Horas = $CantidadSalidas['tiempo'];
	}
	$anoCurso = date('Y');

	$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
	$charge_inactive = $usuario_Class->consultaAllDatosCargos('','2');
	$rol_active = $usuario_Class->consultaAllDatosRoles('','1');
	$rol_inactive = $usuario_Class->consultaAllDatosRoles('','2');
	$listadoConcesionarios = $usuario_Class->consultaAllDatosConcesionarios('');
	//consulta los concesionarios para asignar a los coordinadores
	$listadoCcs = $usuario_Class->consultaAllDatosConcesionariosAll('');


	foreach ($rol_user as $key => $value) {
		if( $value['rol_id'] == 3 ){
			$coordinador++;
		}
	}

	unset($usuario_Class);


//opcion crear nuevo usuario
}else if(!isset($_GET['id'])&&(isset($_GET['opcn']))&&($_GET['opcn']=='nuevo')){
	include('models/usuarios.php');
	$usuario_Class = new Usuarios();
	$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
	$charge_inactive = $usuario_Class->consultaAllDatosCargos('','2');
	$rol_active = $usuario_Class->consultaAllDatosRoles('','1');
	$rol_inactive = $usuario_Class->consultaAllDatosRoles('','2');
	$listadoConcesionarios = $usuario_Class->consultaAllDatosConcesionarios('');
	$type_user = $usuario_Class->tipoUsuarios();
	unset($usuario_Class);
}
