<?php
if(isset($_POST['opcn'])){
	//operaciones con los datos
	switch ( $_POST['opcn'] ) {
		case 'crear':
			include_once('../models/excusas.php');
			$resultado = 0;
			if(isset($_FILES['image_new'])){
				if(isset($_FILES['image_new']['tmp_name'])){
					$nom_archivo1 = $_FILES["image_new"]["name"];
					$new_name1 = "GM_EXCUSA_";
					$new_name1 .= date("Ymdhis");
					$new_extension1 = "jpg";
					preg_match("'^(.*)\.(PDF|pdf)$'i", $nom_archivo1, $ext1);
					switch (strtolower($ext1[2])) {
						case 'PDF' : $new_extension1 = ".pdf";
						break;
						case 'pdf' : $new_extension1 = ".pdf";
						break;
						default    : $new_extension1 = "no";
						break;
					}
					if($new_extension1 != "no"){
						$new_name1 .= $new_extension1;
						$resultArchivo1 = copy($_FILES["image_new"]["tmp_name"], "../../assets/excusas/".$new_name1);
						if($resultArchivo1==1){
							$Users_select = explode(':', $_POST['schedule_id']);
							$invitation_id = $Users_select[0];
							$schedule_id = $Users_select[1];
							$module_result_usr_id = $Users_select[2];
							$notas_Class = new Notas();
							$resultado = $notas_Class->CrearExcusa($schedule_id,$invitation_id,$module_result_usr_id,$_POST['user_id'],$new_name1,$_POST['type'],$_POST['description']);
						}

					}
					if($resultado>0){
						echo json_encode( ["error" => false, "message" => 'La excusa ha sido creada Correctamente, puede ir al listado y buscarla por la identificación del usuario' ] );
					}else{
						echo json_encode( ["error" => true, "message" => 'No se ha logrado subir la información, revise su conexión a internet e intente nuevamente' ] );
					}
				}//fin if
			}//fin archivo
		break;

		case 'GetSchedule':
			include_once('../models/excusas.php');
			// print_r( $_POST );
			$resultado = Notas::GetProgramacion($_POST['usuarioId']);
			if( isset($_POST['excuse_id']) && $_POST['excuse_id'] != "" ){
				$excusa = Notas::getExcusa( $_POST['excuse_id'], '../' );
			}else{
				$excusa = [];
			}
			$retorna = "";

			foreach ($resultado as $key => $Data) {
				$selected = "";
				if( isset( $excusa['invitation_id'] ) && $Data['invitation_id'] == $excusa['invitation_id'] ){
					$selected = "selected";
				}
				$retorna .= '<option value="'.$Data['invitation_id'].':'.$Data['schedule_id'].':'.$Data['module_result_usr_id'].'" '.$selected.'>'.$Data['newcode'].' | '.$Data['course'].' | '.$Data['start_date'].'</option>';
			}
			echo($retorna);
		break;

		case 'editar':
			include_once('../models/excusas.php');
			include_once( '../src/funciones_globales.php' );
			$notas_Class = new Notas();
			$p = $_POST;
			if( isset($_FILES['image_new']) ){
				$p['file'] = Funciones::guardarPDF( 'image_new', 'EXCUSA_', '../../assets/excusas/' );
			}
			// print_r( $p );
			// echo("<br>");
			$actualizar_pdf = Notas::actualizarExcusa( $p );
			$eliminar_mru = Notas::eliminarRegistroExcusa( $p );
			$estado = $actualizar_pdf > 0 ? "Se actualizó correctamente" : "No se han realizado cambios";
			$archivo = isset( $p['file'] ) ? $p['file'] : "";
			echo json_encode( ["error" => false, "message" => $estado, "archivo" => $archivo ] );
		break;

		case 'getUsuarios':
			include_once('../models/excusas.php');
			$p = $_POST['searchTerm'];
			$listadosUsuarios = Notas::consultaUsuarios( $p );
			$datos= [];
			foreach ($listadosUsuarios as $key => $l ) {
				$datos[] = [
					"id"=> $l['user_id'],
					"text" => $l['identification']." | ".$l['first_name']." ".$l['last_name']
				];
			}
			echo json_encode( $datos );
		break;
	}//fin switch

}else if( isset($_GET['opcn']) ){

	switch ( $_GET['opcn'] ) {
		case 'editar':
			include_once('models/excusas.php');
			$notas_Class = new Notas();
			$excusa = Notas::getExcusa( $_GET['id'] );
			$cantidad_datos = $notas_Class->consultaCantidad();
			$listadosUsuarios = Notas::consultaUsuarios('','');
			$usuario = Notas::consultaUsuarioExcusa($excusa['user_id'],'');
			$listadosUsuarios[] = $usuario;

		break;

		case 'nuevo':
			include_once('models/excusas.php');
			$notas_Class = new Notas();
			$cantidad_datos = $notas_Class->consultaCantidad();
			$listadosUsuarios = Notas::consultaUsuarios('','');
		break;
	}//fin switch

}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	@session_start();
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY s.start_date';

    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY s.start_date '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY d.dealer '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY u.first_name, u.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY e.file '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY e.status_id '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='5') {
    		$sOrder = ' ORDER BY t.first_name, t.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='6') {
    		$sOrder = ' ORDER BY e.date_creation '.$_GET['sSortDir_0'];
    	}
    }

    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/excusas.php');
	$notas_Class = new Notas();
	$datosConfiguracion = $notas_Class->consultaDatosNota($sWhere,$sOrder,$sLimit,$_GET['mes'],$_GET['anio'], $_GET['dealer']);
	// print_r( $datosConfiguracion );
	$cantidad_datos = $notas_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit,$_GET['mes'],$_GET['anio'], $_GET['dealer']);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    @session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	$valOpciones = '<a href="../assets/excusas/'.$aInfo['file'].'" target="_blank" class="btn-action glyphicons hospital_h btn-success"><i></i></a>';
		$valEditar = '';
		$estado = '';
		// print_r( $aInfo );
		switch ($aInfo['status_id'] ) {
			case 1:
				$estado = '<span class="label label-success">Aprobado</span>';
			break;
			case 2:
				$estado = '<span class="label label-primary">Pendiente</span>';
				if( $_SESSION['max_rol'] >= 5 ){
					$valEditar = '<a class="btn btn-primary" href="op_excusa.php?opcn=editar&id='.$aInfo['excuse_id'].'"><i class="fa fa-edit"></i></a>';
				}
			break;
			case 3:
				$estado = '<span class="label label-danger">Rechazado</span>';
				$valEditar = '<a class="btn btn-primary" href="op_excusa.php?opcn=editar&id='.$aInfo['excuse_id'].'"><i class="fa fa-edit"></i></a>';
			break;
		}
    	$aItem = array(
			$aInfo['start_date'].' | '.$aInfo['course'].' | '.$aInfo['excuse_id'],
			$aInfo['dealer'],
			$aInfo['first_name'].' '.$aInfo['last_name'].' '.$aInfo['identification'],
			$valOpciones,
			$estado,
			$aInfo['first_creator'].' '.$aInfo['last_creator'],
			$aInfo['date_creation'],
			'DT_RowId' => $aInfo['excuse_id'],
			$valEditar
		);
		$output['aaData'][] = $aItem;
    }
	// print_r( $output );
    echo json_encode($output);
}else{
	include_once('models/excusas.php');
	include_once('src/funciones_globales.php');
	$notas_Class = new Notas();
	$cantidad_datos = $notas_Class->consultaCantidad();
	$concesionarios = Funciones::getConcesionarios('');
}

unset($notas_Class);
