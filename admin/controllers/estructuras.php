<?php
if(isset($_GET['opcn'])){
	include_once('models/estructuras.php');
	$cargos_Class = new Cargos();
	if(isset($_GET['id'])){
		//consultas individuales
		$registroConfiguracion = $cargos_Class->consultaRegistro($_GET['id']);
		$listadosCursos = $cargos_Class->consultaCursos('');
		$listadosCargos = $cargos_Class->consultaCargos('');
		$listadosNiveles = $cargos_Class->consultaNiveles('');
		
	}else{
		$registroConfiguracion['nombre_lista'] = "";
		$listadosCursos = $cargos_Class->consultaCursos('');
		$listadosCargos = $cargos_Class->consultaCargos('');
		$listadosNiveles = $cargos_Class->consultaNiveles('');
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/estructuras.php');
	$cargos_Class = new Cargos();

	if($_POST['opcn']=="crear"){
		/* echo '<pre>';
		print_r($_POST);
		return; */
		
		 $resultado = $cargos_Class->Crearcargos($_POST['charge_id'],$_POST['course_id'],$_POST['level_id']);

		
		/* if($resultado>0){
			echo ('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		} */
  echo json_encode($resultado);

	}elseif($_POST['opcn']=="editar"){
		// print_r($_POST);
		$resultado = $cargos_Class->Actualizarcargos($_POST['idElemento'],$_POST['status_id'],$_POST['charge_id'][0],$_POST['course_id'],$_POST['level_id']);
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas cargos
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY v.level';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY v.level '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY z.newcode, z.course '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY x.charge '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
    		$sOrder = ' ORDER BY v.level '.$_GET['sSortDir_0'];
    	}elseif ($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY s.first_name, s.last_name '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='5'){
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/estructuras.php');
	$cargos_Class = new Cargos();
	$datosConfiguracion = $cargos_Class->consultaDatoscargos($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $cargos_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
    		$valOpciones = '<a href="op_estructura.php?opcn=editar&id='.$aInfo['charge_course_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	}
    	$imagen = '<center><img src="../assets/images/distinciones/'.$aInfo['image'].'" style="width: 33px;" alt="'.$aInfo['course'].'"></center>';
    	$aItem = array(
			$imagen,
			$aInfo['newcode'].' | '.$aInfo['course'],
			$aInfo['charge'],
			$aInfo['level'],
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'], 
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['charge_course_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/estructuras.php');
	$cargos_Class = new Cargos();
	$cantidad_datos = $cargos_Class->consultaCantidad();
	$listadosCursos = $cargos_Class->consultaCursos('');
	$listadosCargos = $cargos_Class->consultaCargos('');
	$listadosNiveles = $cargos_Class->consultaNiveles('');
	// echo '<pre>';
	// var_dump($cantidad_datos);
	// // print_r($listadosCursos);
	// // print_r($listadosCargos);
	// // print_r($listadosNiveles);
	// die();
}
unset($cargos_Class);