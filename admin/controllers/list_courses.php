<?php
if(isset($_POST['opcn'])){
	include('../models/usuarios.php');
	$usuario_Class = new Usuarios();

	if($_POST['opcn']=="ConsultarListado"){
		$cuenta = ["inscritos" => [ '1' => 0, '2' => 0, '3' => 0, '4' => 0 ]
					,"finalizados" => [ '1' => 0, '2' => 0, '3' => 0, '4' => 0 ]
					,"disponibles" => [ '1' => 0, '2' => 0, '3' => 0, '4' => 0 ] ];
		$_Publish = "";
		$specialty = isset( $_POST['especialidad'] ) ? $_POST['especialidad'] : 1;
		$contenido_data = isset( $_POST['filtro'] ) ? $_POST['filtro'] : "";
		//traer los datos
		//cursos Inscritos
		$Datos_CA_Inscrito = $usuario_Class->consulta_CA_Inscrito_S( $specialty, $contenido_data );
		$Datos_CT_Inscrito = $usuario_Class->consulta_CT_Inscrito_S( $specialty, $contenido_data );
		//cursos Finalizados
		$Datos_CA_finalizado = $usuario_Class->consulta_CA_Finalizado_S( $specialty, $contenido_data );
		$Datos_CT_finalizado = $usuario_Class->consulta_CT_Finalizado_S( $specialty, $contenido_data );
		//cursos disponibles
		//cursos Finalizados
		$Datos_CA_disponible = $usuario_Class->consulta_CA_Disponible_S( $specialty, $contenido_data );
		$Datos_CT_disponible = $usuario_Class->consulta_CT_Disponible_S( $specialty, $contenido_data );
		//unifica los dos tipos de cursos
		$DatosDisponibleInsc = array_merge($Datos_CT_Inscrito, $Datos_CA_Inscrito);
		foreach ($DatosDisponibleInsc as $key => $value) {
			switch ($value['specialty_id']) {
				case '1': $cuenta['inscritos'][1]++; break;
				case '2': $cuenta['inscritos'][2]++; break;
				case '3': $cuenta['inscritos'][3]++; break;
				case '4': $cuenta['inscritos'][4]++; break;
			}
		}
		$DatosDisponibleFina = array_merge($Datos_CA_finalizado, $Datos_CT_finalizado);
		foreach ($DatosDisponibleFina as $key => $value) {
			switch ($value['specialty_id']) {
				case '1': $cuenta['finalizados'][1]++; break;
				case '2': $cuenta['finalizados'][2]++; break;
				case '3': $cuenta['finalizados'][3]++; break;
				case '4': $cuenta['finalizados'][4]++; break;
			}
		}
		$DatosDisponibleDisp = array_merge($Datos_CA_disponible, $Datos_CT_disponible);
		foreach ($DatosDisponibleDisp as $key => $value) {
			switch ($value['specialty_id']) {
				case '1': $cuenta['disponibles'][1]++; break;
				case '2': $cuenta['disponibles'][2]++; break;
				case '3': $cuenta['disponibles'][3]++; break;
				case '4': $cuenta['disponibles'][4]++; break;
			}
		}

		if( $cuenta['inscritos'][$specialty] > 0 ){
			$ct_row = 0;
			$_Publish .= "<div class='row'>
				<h4>Continua Viendo</h4>";
			foreach ($DatosDisponibleInsc as $iID => $datosCurso_inscritos) {
				$ct_row++;
				if( $datosCurso_inscritos['specialty_id'] == $specialty ){
					$color = $datosCurso_inscritos['disposicion'] == "Autodirigidos" ? "red" : "green" ;
					$_Publish .= "<div class=''>
							<div class='col-md-2 padding-none margin-none DivCourse_Detail' style='box-shadow: 1px 1px 10px #888 !important' dat-Id='". $datosCurso_inscritos['course_id'] ."' dat-Completo='". $datosCurso_inscritos['course'] ."' dat-Corto='". strtoupper(substr($datosCurso_inscritos['course'],0,28)) ."'>
								<div class='box-generic padding-none margin-none overflow-hidden'>
									<div class='relativeWrap overflow-hidden' style='height:150px'>
										<img src='../assets/images/distinciones/". $datosCurso_inscritos['image']."' alt='' class='img-responsive padding-none border-none' />
										<div class='fixed-bottom bg-inverse-faded' style='background:rgba(66,66,66,0.9) !important'>
											<div class='media margin-none innerAll'>
												<div class='media-body text-white overflow-hidden'>
													<strong class='' style='font-size: 11px !important;' id='DivTexto_Com".$datosCurso_inscritos['course_id']."'>". strtoupper(substr($datosCurso_inscritos['course'],0,28))."'</strong>
													<p class='text-small margin-none'><a href='course_detail.php?token=".md5("GMAcademy")."&_valSco=".md5("GMColmotores")."&opcn=ver&id=". $datosCurso_inscritos['course_id']."'><i class='fa fa-fw fa-youtube-play'></i> <strong>Continuar</strong></a></p>
												</div>
											</div>
										</div>
									</div>
									<div class='ribbon-wrapper'>
										<div class='ribbon' style='background-color:". $color .";font-size: 10px;'>".$datosCurso_inscritos['disposicion']."</div>
									</div>
								</div>
							</div>
						</div>";
					}
				}
			$_Publish .=	"</div>
				<br>";
		 }

		 if( $cuenta['finalizados'][$specialty] > 0 ){
				$ct_row = 0;
				$_Publish .= "<div class='row'>
					<h4>Finalizados</h4>";
				foreach ($DatosDisponibleFina as $iID => $datosCurso_finalizado) {
					$ct_row++;
					if( $datosCurso_finalizado['specialty_id'] == $specialty ){
						$color = $datosCurso_finalizado['disposicion'] == "Autodirigidos" ? "red" : "green" ;
						$_Publish .= "<div class=''>
								<div class='col-md-2 padding-none margin-none DivCourse_Detail' style='box-shadow: 1px 1px 10px #888 !important' dat-Id='". $datosCurso_finalizado['course_id'] ."' dat-Completo='". $datosCurso_finalizado['course'] ."' dat-Corto='". strtoupper(substr($datosCurso_finalizado['course'],0,28)) ."'>
									<div class='box-generic padding-none margin-none overflow-hidden'>
										<div class='relativeWrap overflow-hidden' style='height:150px'>
											<img src='../assets/images/distinciones/". $datosCurso_finalizado['image']."' alt='' class='img-responsive padding-none border-none' />
											<div class='fixed-bottom bg-inverse-faded' style='background:rgba(66,66,66,0.9) !important'>
												<div class='media margin-none innerAll'>
													<div class='media-body text-white overflow-hidden'>
														<strong class='' style='font-size: 11px !important;' id='DivTexto_Com".$datosCurso_finalizado['course_id']."'>". strtoupper(substr($datosCurso_finalizado['course'],0,28))."'</strong>
														<p class='text-small margin-none'><a href='course_detail.php?token=".md5("GMAcademy")."&_valSco=".md5("GMColmotores")."&opcn=ver&id=". $datosCurso_finalizado['course_id']."'><i class='fa fa-fw fa-youtube-play'></i> <strong>Continuar</strong></a></p>
													</div>
												</div>
											</div>
										</div>
										<div class='ribbon-wrapper'>
											<div class='ribbon' style='background-color:". $color .";font-size: 10px;'>".$datosCurso_finalizado['disposicion']."</div>
										</div>
									</div>
								</div>
							</div>";
						}
					}
				$_Publish .=	"</div>
					<br>";
			 }

		 if( $cuenta['disponibles'][$specialty] > 0 ){
				$ct_row = 0;
				$_Publish .= "<div class='row'>
					<h4>Disponibles</h4>";
				foreach ($DatosDisponibleDisp as $iID => $datosCurso_disponible) {
					$ct_row++;
					if( $datosCurso_disponible['specialty_id'] == $specialty ){
						$color = $datosCurso_disponible['disposicion'] == "Autodirigidos" ? "red" : "green" ;
						$_Publish .= "<div class=''>
								<div class='col-md-2 padding-none margin-none DivCourse_Detail' style='box-shadow: 1px 1px 10px #888 !important' dat-Id='". $datosCurso_disponible['course_id'] ."' dat-Completo='". $datosCurso_disponible['course'] ."' dat-Corto='". strtoupper(substr($datosCurso_disponible['course'],0,28)) ."'>
									<div class='box-generic padding-none margin-none overflow-hidden'>
										<div class='relativeWrap overflow-hidden' style='height:150px'>
											<img src='../assets/images/distinciones/". $datosCurso_disponible['image']."' alt='' class='img-responsive padding-none border-none' />
											<div class='fixed-bottom bg-inverse-faded' style='background:rgba(66,66,66,0.9) !important'>
												<div class='media margin-none innerAll'>
													<div class='media-body text-white overflow-hidden'>
														<strong class='' style='font-size: 11px !important;' id='DivTexto_Com".$datosCurso_disponible['course_id']."'>". strtoupper(substr($datosCurso_disponible['course'],0,28))."'</strong>
														<p class='text-small margin-none'><a href='course_detail.php?token=".md5("GMAcademy")."&_valSco=".md5("GMColmotores")."&opcn=ver&id=". $datosCurso_disponible['course_id']."'><i class='fa fa-fw fa-youtube-play'></i> <strong>Continuar</strong></a></p>
													</div>
												</div>
											</div>
										</div>
										<div class='ribbon-wrapper'>
											<div class='ribbon' style='background-color:". $color .";font-size: 10px;'>".$datosCurso_disponible['disposicion']."</div>
										</div>
									</div>
								</div>
							</div>";
						}
					}
				$_Publish .=	"</div>
					<br>";
			 }

		echo($_Publish);
	}
	unset($usuario_Class);
}
/*

Procesos para consulta mediante Angular, metodos asincronos

if(isset($_GET['opcn'])){
	include('../models/usuarios.php');
	$usuario_Class = new Usuarios();
	if($_GET['opcn']=="consulta"){
		$data = [];
		$specialty = isset( $_GET['id'] ) ? $_GET['id'] : 1;
		$contenido_data = isset( $_GET['filtro'] ) ? $_GET['filtro'] : "";
		//traer los datos
		//cursos Inscritos
		$Datos_CA_Inscrito = $usuario_Class->consulta_CA_Inscrito_S( $specialty, $contenido_data );
		$Datos_CT_Inscrito = $usuario_Class->consulta_CT_Inscrito_S( $specialty, $contenido_data );
		//cursos Finalizados
		$Datos_CA_finalizado = $usuario_Class->consulta_CA_Finalizado_S( $specialty, $contenido_data );
		$Datos_CT_finalizado = $usuario_Class->consulta_CT_Finalizado_S( $specialty, $contenido_data );
		//cursos disponibles
		//cursos Finalizados
		$Datos_CA_disponible = $usuario_Class->consulta_CA_Disponible_S( $specialty, $contenido_data );
		$Datos_CT_disponible = $usuario_Class->consulta_CT_Disponible_S( $specialty, $contenido_data );
		//unifica los dos tipos de cursos
		$DatosDisponibleInsc = array_merge($Datos_CT_Inscrito, $Datos_CA_Inscrito);
		$DatosDisponibleFina = array_merge($Datos_CA_finalizado, $Datos_CT_finalizado);
		$DatosDisponibleDisp = array_merge($Datos_CA_disponible, $Datos_CT_disponible);
		//objeto de datos final
		$data['inscritos'] = $DatosDisponibleInsc;
		$data['finalizados'] = $DatosDisponibleFina;
		$data['disponibles'] = $DatosDisponibleDisp;
		echo( json_encode( $data ) );
	}
}*/
