<?php

if( isset( $_POST['opcn'] ) ){
    include_once('../models/dashboard_new.php');
    switch ( $_POST['opcn'] ) {
        //================================================================
        // modifica los concesionarios segun la zona seleccionada
        //================================================================
        case 'sel_Concs':
            $zonas = $_POST['zonas'];
            // var_dump($zonas);
            $concs = Dashboard::getConcesionarios( $zonas );
            foreach ($concs as $key => $value) { ?>
                <option value="<?php echo $value['dealer_id'] ?>"><?php echo $value['dealer'] ?></option>
        <?php    }
        break;

        //================================================================
        // Obtiene los datos del reporte
        //================================================================
        case 'datos':
            $p = $_POST;
            $p['filtro_concesionarios'] = $_POST['concesionarios'];
            // print_r( $p );
            // die();
            $quarters = [ "q1"=>0, "q2"=>0, "q3"=>0, "q4"=>0 ];
            $concesionarios_zona = [];

            //validamos si se filtra por zona, se consultan los ccs de esa zona

            if( $p['zonas'] != "" ){

                $concesionarios_zona = Dashboard::getConcesionarios( $p['zonas'] );
                $p['concesionarios'] = "0";
                foreach ($concesionarios_zona as $key => $v) {
                    $p['concesionarios'] .= ", ".$v['dealer_id'];
                }//fin foreach
            }//fin if

            $p_pais = $p;

            $_resp_inicial = Dashboard::sabanaDatos( $p );
            $_data = $_resp_inicial['schedules'];
            $_num_usuarios_tray = Dashboard::getNumeroUsuariosInscritos( $p );
            $encuestas = $_resp_inicial['encuestas'];
            $_quarters = Dashboard::quartersDashboard( $_data );

            $trayectorias = Dashboard::getTrayectorias( $p );//listado de trayectorias
            $categorias = Dashboard::listaCategorias();//listado de categorias de las trayectorias
            $usuarios = Dashboard::getTotalUsuarios( $p );//listado de usuarios en plataforma


            $p_pais['concesionarios'] = '';
            $p_pais['filtro_concesionarios'] = '';
            $_resp = Dashboard::sabanaDatos( $p_pais );
            $_data_pais = $_resp['schedules'];
            //promedio notas pais

            //se agrupa las notas generales para obtener el promedio del pais
            $notas_p = 0;
            $cupos_p = 0;
            foreach ($_data_pais as $key => $psd) {
                $notas_p += intval( $psd['promedio'] );
                $cupos_p += intval( $psd['max_size'] );
            }

            $pais = $cupos_p > 0 ? $notas_p / $cupos_p : 0;

            //encuestas de satisfaccion
            // $encuestas = Dashboard::encuestas( $p );
            //ranking de estudiantes segun cursos aprobados y notas
            $estudiantes_ranking = Dashboard::getRankingAlumnos( $p, 'mensual' );
            $estudiantes_cursos = Dashboard::estudiantesCursos( $p, 'mensual' );

            $datos = [
                "parametros"        => $p,
                "sdatos"            => $_data,
                "sdatos_pais"       => $_data_pais,
                "quarters"          => $_quarters,
                "trayectorias"      => $trayectorias,
                "usuarios"          => $usuarios,

                "concesionarios"    => $concesionarios_zona,
                "encuestas"         => $encuestas,
                "categorias"        => $categorias,
                // "notas"             => $notas,
                "rankingCursados"   => $estudiantes_cursos,
                "rankingEstudiantes"=> $estudiantes_ranking,
                // "inscripciones"     => $l_inscripciones,
                "pais"              => $pais
            ];

            echo( json_encode( $datos ) );
        break;

        //================================================================
        // Retorna los datos para generar el dashboard
        //================================================================
        case 'dashboard':
            $p = $_POST;
            $p['filtro_concesionarios'] = $_POST['concesionarios'];
            // print_r( $p );
            // die();
            $quarters = [ "q1"=>0, "q2"=>0, "q3"=>0, "q4"=>0 ];
            $concesionarios_zona = [];
            //validamos si se filtra por zona, se consultan los ccs de esa zona

            if( $p['zonas'] != "" ){
                $concesionarios_zona = Dashboard::getConcesionarios( $p['zonas'] );
                $p['concesionarios'] = "0";
                foreach ($concesionarios_zona as $key => $v) {
                    $p['concesionarios'] .= ", ".$v['dealer_id'];
                }//fin foreach
            }//fin if

            $trayectorias = Dashboard::getTrayectorias( $p );//listado de trayectorias
            $usuarios = Dashboard::getTotalUsuarios( $p );//listado de usuarios en plataforma

            $p_pais = $p;

            $datos = [
                "parametros"        => $p,
                "concesionarios"    => $concesionarios_zona,
                "trayectorias"      => $trayectorias,
                "usuarios"          => $usuarios,
                "quarters"          => $resQuarters,
                "encuestas"         => $encuestas,
                "notas"             => $notas,
                "rankingCursados"   => $estudiantes_cursos,
                "rankingEstudiantes"=> $estudiantes_ranking,
                "inscripciones"     => $l_inscripciones,
                "pais"              => $pais,
                "sabana_datos"      => $d_sabana
            ];

            echo( json_encode( $datos ) );
        break;
    }

}elseif( isset( $_GET['opcn'] ) ){

}else{   //fin isset $_GET['opcn']
    @session_start();
    include_once('models/dashboard_new.php');
    if ( $_SESSION['max_rol'] < 5 ) {
        $zona_sel = Dashboard::getZonaByCcs( $_SESSION['dealer_id'], '' );
        $concesionarios = Dashboard::getConcesionarios($zona_sel['zone_id'],'');
    }
    // echo json_encode( $zona_sel );
    // die();
    $zonas          = Dashboard::getZonas();
}
