<?php
if(isset($_GET['opcn'])){
	include_once('models/encuestas.php');
	$Encuestas_Class = new Encuestas();
	if(isset($_GET['id'])){
        if(isset($_POST['opcn_preg'])&&$_POST['opcn_preg']=="agregarPreg"){
            $resultado = $Encuestas_Class->AgrPregunta($_POST['question_id'],"0",$_POST['review_id']);
        }elseif(isset($_GET['opcn_preg'])&&$_GET['opcn_preg']=="inactivaPreg"){
            $resultado = $Encuestas_Class->ActPregunta($_GET['reviews_questions_id'],'0');
            header("location: op_encuesta.php?opcn=editar&id=".$_GET['id']);
        }elseif(isset($_GET['opcn_preg'])&&$_GET['opcn_preg']=="activaPreg"){
            $resultado = $Encuestas_Class->ActPregunta($_GET['reviews_questions_id'],'1');
            header("location: op_encuesta.php?opcn=editar&id=".$_GET['id']);
        }
		//consultas individuales
		$registroConfiguracion = $Encuestas_Class->consultaRegistro($_GET['id']);
		$listadosCursos = $Encuestas_Class->consultaCursos('');
        $listadosCargos = $Encuestas_Class->consultaCargos('');
        $listadoPreguntasSel = $Encuestas_Class->consultaPreguntasSel($_GET['id']);
        $listadoPreguntas = $Encuestas_Class->consultaPreguntas('');
	}else{
		$listadosCursos = $Encuestas_Class->consultaCursos('');
        $listadosCargos = $Encuestas_Class->consultaCargos('');
	}
}else if(isset($_POST['opcn'])){
	//operaciones con los datos
	include_once('../models/encuestas.php');
	$Encuestas_Class = new Encuestas();
	if($_POST['opcn']=="crear"){
		$resultado = $Encuestas_Class->CrearEncuestas($_POST['review'],$_POST['code'],$_POST['type'],$_POST['description']);
		if($resultado>0){
            if($_POST['type']=="1"){
                $resultado_A = $Encuestas_Class->CrearRelacion($resultado,$_POST['cargos'],'charge');
            }else{
                $resultado_A = $Encuestas_Class->CrearRelacion($resultado,$_POST['cursos'],'course');
            }
			echo('{"resultado":"SI"}');
		}else{
			echo('{"resultado":"NO"}');
		}
	}elseif($_POST['opcn']=="editar"){
        $resultado_Bor = $Encuestas_Class->BorraEvaluacion_Rel($_POST['idElemento']);
		$resultado = $Encuestas_Class->ActualizarEncuestas($_POST['idElemento'],$_POST['review'],$_POST['status_id'],$_POST['code'],$_POST['type'],$_POST['description']);
            if($_POST['type']=="2"){
                $resultado_A = $Encuestas_Class->CrearRelacion($_POST['idElemento'],$_POST['cargos'],'charge');
            }else{
                $resultado_A = $Encuestas_Class->CrearRelacion($_POST['idElemento'],$_POST['cursos'],'course');
            }
			echo('{"resultado":"SI"}');
	}
}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas Encuestas
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.code';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.code '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY c.review '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY c.type '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3') {
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/encuestas.php');
	$Encuestas_Class = new Encuestas();
	$datosConfiguracion = $Encuestas_Class->consultaDatosEncuestas($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $Encuestas_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    session_start();
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
    		$valOpciones = '<a href="op_encuesta.php?opcn=editar&id='.$aInfo['review_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	}
        if($aInfo['type']==1){
            $val_tipo = '<span class="label label-success">Evaluación Abierta</span>';
        }elseif($aInfo['type']==2){
            $val_tipo = '<span class="label label-inverse">Encuesta Abierta</span>';
        }elseif($aInfo['type']==3){
            $val_tipo = '<span class="label label-important">Evaluación Curso</span>';
        }else{
            $val_tipo = '<span class="label label-danger">Encuesta Curso</span>';
        }
    	$aItem = array(
			$aInfo['code'],
			$aInfo['review'],
            $val_tipo,
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'], 
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['review_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/encuestas.php');
	$Encuestas_Class = new Encuestas();
	$cantidad_datos = $Encuestas_Class->consultaCantidad();
}
unset($Encuestas_Class);