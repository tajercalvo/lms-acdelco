<?php
include_once('models/rep_indicadores_virtuales.php');
$CursosVirtuales_Class = new RepIndicadoresVirtuales();
$cantidad_datos = 0;
if(isset($_POST['start_date_day'])){
	$datosCursos_all = $CursosVirtuales_Class->consultaCursosRealizados($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$_POST['concesionarios'] = $_GET['concesionarios'];
	$datosCursos_all = $CursosVirtuales_Class->consultaCursosRealizadosCSV($_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

unset($CursosVirtuales_Class);
