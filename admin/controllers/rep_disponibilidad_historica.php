<?php
include_once('models/rep_disponibilidad_historica.php');
$Asistencias_Class = new RepDisponibilidadHistorica();
$cantidad_datos = 0;

if(isset($_POST['start_date_day'])){

	// $total = 0;
	$datosCursos_all = $Asistencias_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
	$asistencia_all = $Asistencias_Class->getAsistencia($_POST['start_date_day'],$_POST['end_date_day']);
	$cant = count( $datosCursos_all );
	for ($i=0; $i < $cant ; $i++) {
		$data = $Asistencias_Class->getDisponibilidad( $datosCursos_all[$i]['dealer_id'], $datosCursos_all[$i]['course_id'],$_POST['start_date_day']);
		// $total += count( $data );
		$datosCursos_all[$i]['disponibilidad'] = $data;
	}
	// echo( "<br>$total<br>" );
	$cantidad_datos = count($datosCursos_all);

}else if(isset($_GET['start_date_day'])){

	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];

	if( isset( $_GET['rep'] ) ){
		
		$datosCursos_all = $Asistencias_Class->consultaDatosAll($_POST['start_date_day'],$_POST['end_date_day']);
		$asistencia_all = $Asistencias_Class->getAsistencia($_POST['start_date_day'],$_POST['end_date_day']);
		$cant = count( $datosCursos_all );
		for ($i=0; $i < $cant ; $i++) {
			$data = $Asistencias_Class->getDisponibilidad( $datosCursos_all[$i]['dealer_id'], $datosCursos_all[$i]['course_id'],$_POST['start_date_day']);

			$datosCursos_all[$i]['disponibilidad'] = $data;
		}
		$cantidad_datos = count($datosCursos_all);

		switch ( $_GET['rep'] ) {
			case 'detalle': $nombre_file = "Reporte_Disponibilidad_Historica_detalle.csv";
			break;

			case 'agrupado': $nombre_file = "Reporte_Disponibilidad_Historica_agrupado.csv";
			break;
		}
	}

}

unset($Asistencias_Class);
