<?php
include_once('../models/programaciones.php');
$cargos_Class = new Cargos();

$fecha_hoy = date_create(date("Y-m-d"));
date_add($fecha_hoy, date_interval_create_from_date_string('2 days'));
$FechaConsulta = date_format($fecha_hoy, 'Y-m-d');
$Datos_ParaNotificar = $cargos_Class->ConsultaNotificaciones($FechaConsulta);

$Datos_ParaNotificarAlumn = $cargos_Class->ConsultaQuienesTienen($FechaConsulta);

include_once('../controllers/email.php');
$EmailLudus_Class = new EmailLudus();
$control = 0;
echo("---------------------------------------------COORDINADORES---------------------------------------------
    ");
foreach ($Datos_ParaNotificar as $key => $Datos_Notificar) {
	$para = $Datos_Notificar['email'];
	$titulo = $Datos_Notificar['coordinador'].", su personal tiene una programación pronto al curso: ".$Datos_Notificar['curso'];
	$mensaje_cuerpo =  "Este correo se envia para informar a las personas involucradas en una sesión que esta por ejecutarse y su concesionario tiene cupos allí:<br><br>";
	$mensaje_cuerpo .= "<strong>Curso: </strong> ".$Datos_Notificar['curso']."<br>
                    <strong>Profesor: </strong> ".$Datos_Notificar['profesor']."<br>
                    <strong>Lugar: </strong> ".$Datos_Notificar['living']."<br>
                    <strong>Dirección: </strong> ".$Datos_Notificar['address']."<br>
                    <strong>Fechas: </strong> ".$Datos_Notificar['start_date']." - ".$Datos_Notificar['end_date'];
    $result_Email = $EmailLudus_Class->EnviaEmail_New($titulo,$para,$mensaje_cuerpo,'comunicaciones@gmacademy.co');
    echo("Resultado:".$result_Email." Titulo:".$titulo." Para:".$para."<br>Cuerpo:<br>".$mensaje_cuerpo."<br><br><br>");
}
echo("---------------------------------------------ESTUDIANTES CON EMAIL---------------------------------------------
    ");
foreach ($Datos_ParaNotificarAlumn as $key => $Datos_Notificar) {
    $para = $Datos_Notificar['email'];
    $titulo = $Datos_Notificar['alumno'].", USTED tiene una programación pronto al curso: ".$Datos_Notificar['curso'];
    $mensaje_cuerpo =  "Este correo se envia para informar a las personas involucradas en una sesión de entrenamiento:<br><br>";
    $mensaje_cuerpo .= "<strong>Curso: </strong> ".$Datos_Notificar['curso']."<br>
                    <strong>Profesor: </strong> ".$Datos_Notificar['profesor']."<br>
                    <strong>Lugar: </strong> ".$Datos_Notificar['living']."<br>
                    <strong>Dirección: </strong> ".$Datos_Notificar['address']."<br>
                    <strong>Fechas: </strong> ".$Datos_Notificar['start_date']." - ".$Datos_Notificar['end_date'];
    $result_Email = $EmailLudus_Class->EnviaEmail_New($titulo,$para,$mensaje_cuerpo,'comunicaciones@gmacademy.co');
    echo("Resultado:".$result_Email." Titulo:".$titulo." Para:".$para."<br>Cuerpo:<br>".$mensaje_cuerpo."<br><br><br>");
}