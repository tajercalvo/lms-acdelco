<?php
if(isset($_POST['opcn'])){
	if($_POST['opcn']=="crear"){
		//Metodo para la inclusión de archivo
		$nom_archivoCre = "";
			if(isset($_FILES['Archivo_new']) && $_FILES['Archivo_new']!=""){
				if(isset($_FILES['Archivo_new']['tmp_name'])){
					$nom_archivo1 = $_FILES["Archivo_new"]["name"];
					$new_name1 = "ACDelco_LUDUS_";
					$new_name1 .= date("Ymdhis");
					$new_extension1 = "pdf";
					preg_match("'^(.*)\.(pdf|PDF|zip|ZIP)$'i", $nom_archivo1, $ext1);
					if(isset($ext1[2])){
						switch (strtolower($ext1[2])) {
							case 'pdf' : $new_extension1 = ".pdf";
								break;
							case 'PDF' : $new_extension1 = ".pdf";
								break;
							case 'zip' : $new_extension1 = ".zip";
								break;
							case 'ZIP' : $new_extension1 = ".zip";
								break;
							default    : $new_extension1 = "no";
								break;
						}
					}
					if($new_extension1 != "no"){
						$new_name1 .= $new_extension1;
						$resultArchivo1 = copy($_FILES["Archivo_new"]["tmp_name"], "../assets/gallery/circulares/".$new_name1);
							if($resultArchivo1==1){
								$nom_archivoCre = $new_name1;
							}else{
								$nom_archivoCre = "";
							}
					}else{
						$nom_archivoCre = "";
					}
				}else{
					$nom_archivoCre = "";
				}
			}
		//Metodo para la inclusión de archivo
		include_once('models/circulares.php');
		$Circulares_Class = new Circulares();
		$resultado = $Circulares_Class->CrearCircular($_POST['title'],$_POST['newsletter'],$nom_archivoCre);
		include_once('models/usuarios.php');
		if(!isset($usuario_Class)){
			$usuario_Class = new Usuarios();
		}
		$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario']);
		$cargos_id = "0";
		foreach ($charge_user as $key => $Data_Charges) {
			$cargos_id .= ",".$Data_Charges['charge_id'];
		}
		unset($usuario_Class);
		$Circular_datos = $Circulares_Class->consultaCirculares($cargos_id);
		if($resultado>0 && isset($_POST['cargos'])){
			foreach ($_POST['cargos'] as $iID => $cargos_selec) {
				$resu = $Circulares_Class->CrearCargos($resultado,$cargos_selec);
			}
		}
	}elseif($_POST['opcn']=="editar"){
		//Metodo para la inclusión de archivo
		$nom_archivoCre = "";
		$imgant = $_POST['Archivo_old'];
			if(isset($_FILES['Archivo_new']) && $_FILES['Archivo_new'] !=""){
				if(isset($_FILES['Archivo_new']['tmp_name'])){
					$nom_archivo1 = $_FILES["Archivo_new"]["name"];
					$new_name1 = "ACDelco_LUDUS_";
					$new_name1 .= date("Ymdhis");
					$new_extension1 = "pdf";
					preg_match("'^(.*)\.(pdf|PDF|zip|ZIP)$'i", $nom_archivo1, $ext1);
					switch (strtolower($ext1[2])) {
						case 'pdf' : $new_extension1 = ".pdf";
							break;
						case 'PDF' : $new_extension1 = ".pdf";
							break;
						case 'zip' : $new_extension1 = ".zip";
							break;
						case 'ZIP' : $new_extension1 = ".zip";
							break;
						default    : $new_extension1 = "no";
							break;
					}
					if($new_extension1 != "no"){
						$new_name1 .= $new_extension1;
						$resultArchivo1 = copy($_FILES["Archivo_new"]["tmp_name"], "../assets/gallery/circulares/".$new_name1);
							if($resultArchivo1==1){
								if($imgant != ""){
									if(file_exists("../assets/gallery/circulares/".$imgant)){
										unlink("../assets/gallery/circulares/".$imgant);
									}
								}
								$nom_archivoCre = $new_name1;
							}else{
								$nom_archivoCre = $imgant;
							}
					}else{
						$nom_archivoCre = $imgant;
					}
				}else{
					$nom_archivoCre = $imgant;
				}
			}else{
				$nom_archivoCre = $imgant;
			}
		//Metodo para la inclusión de archivo
		include_once('models/circulares.php');
		$Circulares_Class = new Circulares();
		$resultado = $Circulares_Class->ActualizarCircular($_POST['newsletter_id'],$_POST['title_ed'],$_POST['newsletter_ed'],$_POST['estado'],$nom_archivoCre);
		if(isset($_POST['cargos_ed'])){
			$resu = $Circulares_Class->BorraCargos($_POST['newsletter_id']);
			foreach ($_POST['cargos_ed'] as $iID => $cargos_selec) {
				$resu = $Circulares_Class->CrearCargos($_POST['newsletter_id'],$cargos_selec);
			}
		}
	}
}
if(isset($_GET['id'])){
	include_once('models/circulares.php');
	$Circulares_Class = new Circulares();
	$Circular_datos = $Circulares_Class->consultaCircular($_GET['id']);
	$resultVisita = $Circulares_Class->CrearVisitado($_GET['id']);
	if(isset($_POST['opcn'])){
		include_once('models/usuarios.php');
		if(!isset($usuario_Class)){
			$usuario_Class = new Usuarios();
		}
		$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
		unset($usuario_Class);	
	}
}

if( !isset($_GET['opcn']) && ( isset($_POST['opcn']) && ($_POST['opcn']!="LikeReply" && $_POST['opcn']!="ProcesoAct") ) ){
	include_once('models/circulares.php');
	$Circulares_Class = new Circulares();
	include_once('models/usuarios.php');
	if(!isset($usuario_Class)){
		$usuario_Class = new Usuarios();
	}
	$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario']);
	$cargos_id = "0";
	foreach ($charge_user as $key => $Data_Charges) {
		$cargos_id .= ",".$Data_Charges['charge_id'];
	}
	unset($usuario_Class);
	$Circulares_datos = $Circulares_Class->consultaCirculares($cargos_id);
}


	include_once('models/circulares.php');
	$Circulares_Class = new Circulares();
	include_once('models/usuarios.php');
	$dealers = $Circulares_Class->getDEalers();
	if(!isset($usuario_Class)){
		$usuario_Class = new Usuarios();
	}
	$charge_user = $usuario_Class->consultaDatosCargos($_SESSION['idUsuario']);
	$cargos_id = "0";
	foreach ($charge_user as $key => $Data_Charges) {
		$cargos_id .= ",".$Data_Charges['charge_id'];
	}
	$Circulares_datos = $Circulares_Class->consultaCirculares($cargos_id);
	$charge_active = $usuario_Class->consultaAllDatosCargos('','1');
	unset($usuario_Class);

unset($Circulares_Class);