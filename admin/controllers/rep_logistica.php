<?php

if(isset($_POST['opcn'])){
	switch ( $_POST['opcn'] ) {

		case 'horas':
			include_once('../models/rep_logistica.php');
			$Logistica_Class = new RepLogistica();
			$datos_modulos = $Logistica_Class->consultaHoras( $_POST['schedule_id'], $_POST['course_id'], "../" );
			echo( json_encode( $datos_modulos ) );
		break;

		case 'cargar_feeding':
			include_once('../models/rep_logistica.php');
			$Logistica_Class = new RepLogistica();
			$datos_feeding = $Logistica_Class->consultaLogistica( $_POST['schedule_id'], "../" );
			if ( $datos_feeding > 0) {
				echo( json_encode( $datos_feeding ) );
			} else {
				echo('{"schedule_id":"0"}');
			}
		break;

		case 'crear_feeding':
			include_once('../models/rep_logistica.php');
			$Logistica_Class = new RepLogistica();

			$datos_dinamicos = $_POST['datos_dinamicos'];

			//Validacion para campos
			for ($i=1; $i <= $datos_dinamicos; $i++) { 
				if ( isset($_POST['cost_'.$i]) ) {
					$costos[$i] = $_POST['cost_'.$i];
				} else {
					$costos[$i] = "";
				}
			}

			if ( $_POST['status_id'] == null ) {
				$_POST['status_id'] = 0;
			}
			if ( $_POST['cost_dom'] == "" ) {
				$_POST['cost_dom'] = 0;
			}

			for ($i=1; $i <= $datos_dinamicos; $i++) { 
				$nom_archivoCre = "";
				if(isset($_FILES['file_'.$i]) && $_FILES['file_'.$i]!=""){
					if(isset($_FILES['file_'.$i]['tmp_name'])){
						$nom_archivo1 = $_FILES["file_".$i]["name"];
						$new_name1 = "ACDelco_LUDUS_".$i;
						$new_name1 .= date("Ymdhis");
						$new_extension1 = "pdf";
						preg_match("'^(.*)\.(pdf|PDF|zip|ZIP)$'i", $nom_archivo1, $ext1);
						if(isset($ext1[2])){
							switch (strtolower($ext1[2])) {
								case 'pdf' : $new_extension1 = ".pdf";
									break;
								case 'PDF' : $new_extension1 = ".pdf";
									break;
								case 'zip' : $new_extension1 = ".zip";
									break;
								case 'ZIP' : $new_extension1 = ".zip";
									break;
								default    : $new_extension1 = "no";
									break;
							}
						}
						if($new_extension1 != "no"){
							$new_name1 .= $new_extension1;
							//$resultArchivo1 = copy($_FILES["file_".$i]["tmp_name"], "/Applications/XAMPP/xamppfiles/htdocs/gmacademy/assets/logistica/facturas/".$new_name1);
							$resultArchivo1 = copy($_FILES["file_".$i]["tmp_name"], "/home/gmacademy/public_html/assets/logistica/facturas/".$new_name1);
							
								if($resultArchivo1==1){
									$nom_archivoCre = $new_name1;
								}else{
									$nom_archivoCre = "";
								}
						}else{
							$nom_archivoCre = "";
						}
					}else{
						$nom_archivoCre = "";
					}
				}else{
					 if ( isset($_POST['file_edit_'.$i]) && $_POST['file_edit_'.$i]!="" ) {
						$nom_archivoCre = $_POST['file_edit_'.$i];
					}
				}
				//Cargar nombre de las facturas
				$facturas[$i] = $nom_archivoCre;
			}

			$total =  (array_sum($costos))+$_POST['cost_dom'];

			$feeding = $Logistica_Class->guardarLogistica( $_POST['schedule_id'], $costos, $facturas, $_POST['cost_dom'], $total, $_POST['status_id'], $_POST['notes'], $datos_dinamicos, "../" );
			if ( $feeding > 0) {
				echo('{"resultado":"TRUE"}');
			} else {
				echo('{"resultado":"FALSE"}');
			}


		break;

			 unset($Logistica_Class);
		}
}
else{

	include_once('models/rep_logistica.php');
	$Salidas_Class = new RepLogistica();
	$datosCursos = $Salidas_Class->consultaDatos();
	$datosParametros = $Salidas_Class->consultaParametros();
	$i = 0;

	foreach ($datosParametros as $key => $value) {
		$costos[$i] = $value['value'];
		$i++;
	}
	$refrigerio = $costos[0];
	$almuerzo = $costos[1];
	$cantidad_datos = 0;


	if( isset($_POST['dealer_id']) ){
		$datosLogistica_all = $Salidas_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
		$cantidad_datos = count($datosLogistica_all);
		//$cambio = $Salidas_Class->cambioTabla();
	}else if(isset($_GET['dealer_id'])){
		$_POST['dealer_id'] = $_GET['dealer_id'];
		$_POST['start_date_day'] = $_GET['start_date_day'];
		$_POST['end_date_day'] = $_GET['end_date_day'];
		$datosLogistica_all = $Salidas_Class->consultaDatosAll($_POST['dealer_id'],$_POST['start_date_day'],$_POST['end_date_day']);
		$cantidad_datos = count($datosLogistica_all);
	}else if( !empty($_GET['file']) ){
		
    		$fileName = basename($_GET['file']);
			//$name = $_POST['name_file'];
			//$fileName = basename($name);
			//$filePath = '/Applications/XAMPP/xamppfiles/htdocs/gmacademy/assets/logistica/facturas/'.$fileName;
			$filePath = '/home/gmacademy/public_html/assets/logistica/facturas/'.$fileName;
			if(!empty($fileName) && file_exists($filePath)){
			    // Define headers
			   header('Content-Description: File Transfer');
        	   header('Content-Type: application/pdf');
			   header('Content-Disposition: attachment; filename='.$fileName);
			   header('Content-Transfer-Encoding: binary');
			   header('Content-Length: '.filesize($filePath));
			   /*ob_clean();
        	   flush();*/
			   readfile($filePath);
			   exit;
			}else{
			    echo 'El archivo no existe.';
			}
	}

	unset($Salidas_Class);

}
