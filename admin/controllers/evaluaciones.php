<?php
if(isset($_GET['opcn'])){

	if( $_GET['opcn'] == 'questions' ) {
		include_once('../models/evaluaciones.php');
		$Evaluaciones_Class = new Evaluaciones();
		$listadoPreguntas = $Evaluaciones_Class->consultaPreguntas('js');
		$num = count( $listadoPreguntas );
		for ($i=0; $i < $num ; $i++) {
			$listadoPreguntas[$i]['question'] = utf8_encode( $listadoPreguntas[$i]['question'] );
		}
		echo( json_encode( ["resultado" => "SI","questions" => $listadoPreguntas] ) );

	}else if( $_GET['opcn'] == 'agrupacion' ) {
		include_once('../models/evaluaciones.php');
		$agrupaciones = Evaluaciones::consultaAgrupacion( $_GET['review_id'] );
		$data = ["resultado"=>"SI", "agrupaciones" => $agrupaciones ];
		echo( json_encode( $data ) );

	}else if( $_GET['opcn'] == 'preguntasSel' ) {
		include_once('../models/evaluaciones.php');
		$Evaluaciones_Class = new Evaluaciones();
		$listadoPreguntasSel = $Evaluaciones_Class->consultaPreguntasSel( $_GET['review_id'] );
		$data = ["resultado"=>"SI", "preguntasSel" => $listadoPreguntasSel ];
		echo( json_encode( $data ) );

	}else if(isset($_GET['id'])){
		include_once('models/evaluaciones.php');
		$Evaluaciones_Class = new Evaluaciones();

        if(isset($_POST['opcn_preg'])&&$_POST['opcn_preg']=="agregarPreg"){
			// echo("ingreso a editar");
            $resultado = $Evaluaciones_Class->AgrPregunta($_POST['question_id'],$_POST['value'],$_POST['review_id'],$_POST['select_agrup']);
        }elseif(isset($_GET['opcn_preg'])&&$_GET['opcn_preg']=="inactivaPreg"){
            $resultado = $Evaluaciones_Class->ActPregunta($_GET['reviews_questions_id'],'0');
            header("location: op_evaluacion.php?opcn=editar&id=".$_GET['id']);
        }elseif(isset($_GET['opcn_preg'])&&$_GET['opcn_preg']=="activaPreg"){
            $resultado = $Evaluaciones_Class->ActPregunta($_GET['reviews_questions_id'],'1');
            header("location: op_evaluacion.php?opcn=editar&id=".$_GET['id']);
        }
		//consultas individuales
		$registroConfiguracion = $Evaluaciones_Class->consultaRegistro($_GET['id']);
		$listadosCursos = $Evaluaciones_Class->consultaCursos('');
        $listadosCargos = $Evaluaciones_Class->consultaCargos('');
        // $listadoPreguntasSel = $Evaluaciones_Class->consultaPreguntasSel($_GET['id']);
        $listadoPreguntas = $Evaluaciones_Class->consultaPreguntas('');
	}else{
		include_once('models/evaluaciones.php');
		$Evaluaciones_Class = new Evaluaciones();
		$listadosCursos = $Evaluaciones_Class->consultaCursos('');
        $listadosCargos = $Evaluaciones_Class->consultaCargos('');
	}
}else if(isset($_POST['opcn'])){

	include_once('../models/evaluaciones.php');
	$Evaluaciones_Class = new Evaluaciones();

	switch ( $_POST['opcn'] ) {
		case 'crear':
			@session_start();
			$resultado = $Evaluaciones_Class->CrearEvaluaciones($_POST['review'],$_POST['code'],$_POST['type'],$_POST['description'],$_POST['time'],$_POST['cant_question']);
			if($resultado>0){

				$p = [
					"review_id" => $resultado,
					"agrup" => $_POST['code'],
					"obligatorias" => $_POST['cant_question'],
					"user_id" => $_SESSION['idUsuario']
				];

				$agrupacion = Evaluaciones::crearAgrupacion( $p );

				if($_POST['type']=="1"){
					$resultado_A = $Evaluaciones_Class->CrearRelacion($resultado,$_POST['cargos'],'charge');
				}else{
					$resultado_A = $Evaluaciones_Class->CrearRelacion($resultado,$_POST['cursos'],'course');
				}
				echo( json_encode( ["resultado"=>"SI"] ) );
			}else{
				echo( json_encode( ["resultado"=>"NO"] ) );
			}
		break;

		case 'agregarPregunta':
			@session_start();
			$p = $_POST;
			$p['user_id'] = $_SESSION['idUsuario'];
			$resultado = Evaluaciones::consultaPregunta( $p );
			if( !isset( $resultado['reviews_questions_id'] ) ){

				$reviews_questions_id = Evaluaciones::agregarPregunta( $p );
				$reviews_questions = Evaluaciones::getPregunta( $reviews_questions_id );
				if( $reviews_questions_id > 0 ){
					echo( json_encode( ["resultado"=>"SI", "reviews_questions" => $reviews_questions, "reviews_questions_id" => $reviews_questions_id ] ) );
				}else{
					echo( json_encode( ["resultado"=>"NO", "message" => "No se puede agregar dos veces la misma pregunta" ] ) );
				}

			}else{
				echo( json_encode( ["resultado"=>"NO", "message" => "La pregunta ya esta seleccionada para esta evaluación" ] ) );
			}
		break;

		case 'editarPreguntaSel':
			@session_start();
			$p = $_POST;
			$p['user_id'] = $_SESSION['idUsuario'];
			$resultado = Evaluaciones::editarPreguntaSel( $p );
			if( $resultado > 0 ){
				$reviews_questions = Evaluaciones::getPregunta( $p['reviews_questions_id'] );
				echo( json_encode( ["resultado"=>"SI", "reviews_questions" => $reviews_questions ] ) );
			}else{
				echo( json_encode( ["resultado"=>"NO" ] ) );
			}
		break;

		case 'editar':
			$resultado_Bor = $Evaluaciones_Class->BorraEvaluacion_Rel($_POST['idElemento']);
			$resultado = $Evaluaciones_Class->ActualizarEvaluaciones($_POST['idElemento'],$_POST['review'],$_POST['status_id'],$_POST['code'],$_POST['type'],$_POST['description'],$_POST['time'],$_POST['cant_question']);
			if($_POST['type']=="1"){
				$resultado_A = $Evaluaciones_Class->CrearRelacion($_POST['idElemento'],$_POST['cargos'],'charge');
			}else{
				$resultado_A = $Evaluaciones_Class->CrearRelacion($_POST['idElemento'],$_POST['cursos'],'course');
			}
			echo( json_encode( ["resultado"=>"SI"] ) );
		break;

		case 'estadoPregunta':
			$p = $_POST;
			$resultado_Bor = Evaluaciones::estadoPregunta( $p );
			if( isset( $resultado_Bor ) && $resultado_Bor > 0 ){
				echo( json_encode( ["resultado"=>"SI"] ) );
			}else{
				echo( json_encode( ["resultado"=>"NO"] ) );
			}
		break;

		case 'editarAgrupacion':
			$p = $_POST['datos'];
			$resultado = $Evaluaciones_Class->editarAgrupacion( $p );
			if( $resultado > 0 ){
				$res = ["resultado"=>"SI"];
			}else{
				$res = ["resultado"=>"NO"];
			}
			echo( json_encode( $res ) );
		break;

		case 'nuevaAgrupacion':
			@session_start();
			$p = $_POST;
			$p['user_id'] = $_SESSION['idUsuario'];
			$resultado = $Evaluaciones_Class->crearAgrupacion( $p );
			if( $resultado > 0 ){
				$agrupaciones = Evaluaciones::consultaAgrupacion( $p['review_id'] );
				$res = ["resultado"=>"SI", "agrupaciones" => $agrupaciones ];
			}else{
				$res = ["resultado"=>"NO"];
			}
			echo( json_encode( $res ) );
		break;

		case 'eliminarAgrupacion':
			@session_start();
			$p = $_POST;
			$resultado = $Evaluaciones_Class->eliminarAgrupacion( $p['review_dist_id'] );
			if( $resultado > 0 ){
				$res = [ "resultado"=>"SI" ];
			}else{
				$res = [ "resultado"=>"NO" ];
			}
			echo( json_encode( $res ) );
		break;

		case 'ConsultarPreguntas':
			$idQues = $_POST['idQuestion'];
			$listadoPreguntas = $Evaluaciones_Class->consultaPreguntas('js');
			echo('<select style="width: 1000px;" id="preg'.$idQues.'" name="preg'.$idQues.'">');
				foreach ($listadoPreguntas as $key => $Data_Pregunta) {
					$type_preg = "";
					if($Data_Pregunta['question_id']=="1"){
						$type_preg = "Evaluación Abierta";
					}elseif($Data_Pregunta['question_id']=="2"){
						$type_preg = "Encuesta Abierta";
					}elseif($Data_Pregunta['question_id']=="3"){
						$type_preg = "Evaluación Curso";
					}else{
						$type_preg = "Encuesta Curso";
					}
					if($Data_Pregunta['question_id']==$idQues){
						echo('<option value="'.$Data_Pregunta['question_id'].'" selected="selected" >'.$Data_Pregunta['code'].' | '.$Data_Pregunta['question'].' | '.$type_preg.'</option>');
					}else{
						echo('<option value="'.$Data_Pregunta['question_id'].'" >'.$Data_Pregunta['code'].' | '.$Data_Pregunta['question'].' | '.$type_preg.'</option>');
					}

				}
			echo('</select>');
		break;

		case 'GuardarPreguntas':
			$idQues = $_POST['idQuestion'];
			$idRev= $_POST['idReview'];
			$pregSel = $_POST['pregSel'];
			$ValueSel = $_POST['ValueSel'];
			$resultado= $Evaluaciones_Class->ActPreguntaNueva($idQues,$pregSel,$ValueSel,$idRev);
			if($resultado>0){
				echo( json_encode( [ "resultado" => "SI" ] ) );
			}else{
				echo( json_encode( [ "resultado" => "NO" ] ) );
			}
		break;

	}//fin switch $_POST['opcn']

}else if(isset($_GET['action'])&&($_GET['action']=="getElementsAjax")){
	//consultas Evaluaciones
	// SQL limit
    $sLimit = '';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
        $sLimit = 'LIMIT ' . (int)$_GET['iDisplayStart'] . ', ' . (int)$_GET['iDisplayLength'];
    }
    $sOrder = ' ORDER BY c.code';
    if(isset($_GET['iSortCol_0'])){
    	if($_GET['iSortCol_0']=='0'){
    		$sOrder = ' ORDER BY c.code '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='1'){
    		$sOrder = ' ORDER BY c.review '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='2'){
    		$sOrder = ' ORDER BY c.type '.$_GET['sSortDir_0'];
    	}elseif($_GET['iSortCol_0']=='3'){
            $sOrder = ' ORDER BY c.cant_question '.$_GET['sSortDir_0'];
        }elseif($_GET['iSortCol_0']=='4') {
    		$sOrder = ' ORDER BY c.date_edition '.$_GET['sSortDir_0'];
    	}else{
    		$sOrder = ' ORDER BY c.status_id '.$_GET['sSortDir_0'];
    	}
    }
    $sWhere = '';
    if (isset($_GET['sSearch']) && $_GET['sSearch'] != '') {
        $sWhere = $_GET['sSearch'];
    }
	include_once('../models/evaluaciones.php');
	$Evaluaciones_Class = new Evaluaciones();
	$datosConfiguracion = $Evaluaciones_Class->consultaDatosEvaluaciones($sWhere,$sOrder,$sLimit);
	$cantidad_datos = $Evaluaciones_Class->consultaCantidadFull($sWhere,$sOrder,$sLimit);

	$output = array(
        'sEcho' => intval($_GET['sEcho']),
        'iTotalRecords' => count($datosConfiguracion),
        'iTotalDisplayRecords' => $cantidad_datos,
        'aaData' => array()
    );
    session_start();
    //print_r($datosConfiguracion);
    foreach ($datosConfiguracion as $iID => $aInfo) {
    	if($aInfo['status_id']==1){
    		$val_estado = '<span class="label label-success">Activo</span>';
    	}elseif($aInfo['status_id']==2){
    		$val_estado = '<span class="label label-danger">Inactivo</span>';
    	}else{
    		$val_estado = '<span class="label label-important">Error</span>';
    	}
    	$valOpciones = '';
    	if($_SESSION['max_rol']>=5){
    		$valOpciones = '<a href="op_evaluacion.php?opcn=editar&id='.$aInfo['review_id'].'" class="btn-action glyphicons pencil btn-success"><i></i></a>';
    	}
        if($aInfo['type']==1){
            $val_tipo = '<span class="label label-success">Evaluación Abierta</span>';
        }elseif($aInfo['type']==2){
            $val_tipo = '<span class="label label-inverse">Encuesta Abierta</span>';
        }elseif($aInfo['type']==3){
            $val_tipo = '<span class="label label-important">Evaluación Curso</span>';
        }else{
            $val_tipo = '<span class="label label-danger">Encuesta Curso</span>';
        }
    	$aItem = array(
			$aInfo['code'],
			$aInfo['review'].' T:'.$aInfo['time'].' mín',
            $val_tipo,
            $aInfo['cant_question'],
			$aInfo['nom_edita'].' '.$aInfo['ape_edita'],
			$aInfo['date_edition'],
			$val_estado,
			$valOpciones,
			'DT_RowId' => $aInfo['review_id']
		);
		$output['aaData'][] = $aItem;
    }
    echo json_encode($output);
}else{
	include_once('models/evaluaciones.php');
	$Evaluaciones_Class = new Evaluaciones();
	$cantidad_datos = $Evaluaciones_Class->consultaCantidad();
}
unset($Evaluaciones_Class);
