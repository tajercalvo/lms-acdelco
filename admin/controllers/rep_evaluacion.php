<?php
include_once('models/rep_evaluacion.php');
$Evaluaciones_Class = new RepEvaluaciones();
$cantidad_datos = 0;

if(isset($_POST['start_date_day'])){
	$datosCursos_all = $Evaluaciones_Class->consultaDatosAll($_POST['review_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}else if(isset($_GET['start_date_day'])){
	$_POST['start_date_day'] = $_GET['start_date_day'];
	$_POST['end_date_day'] = $_GET['end_date_day'];
	$_POST['review_id'] = $_GET['review_id'];
	$datosCursos_all = $Evaluaciones_Class->consultaDatosAll($_POST['review_id'],$_POST['start_date_day'],$_POST['end_date_day']);
	$cantidad_datos = count($datosCursos_all);
}

$reviews_active = $Evaluaciones_Class->EvaluacionesEncuestas('1,3');

unset($Evaluaciones_Class);