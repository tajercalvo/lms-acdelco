<?php
if( isset( $_POST['opcn'] ) ){
	switch ( $_POST['opcn'] ) {
		case 'sel_sede':
			include_once("../models/rep_ventas.php");
			$datos_Class = new RepVentas();
			$sedes = $datos_Class->sede( $_POST['sede'], "../" );
			?>
			<optgroup label="Sede">
				<?php foreach ($sedes as $key => $sedesV) { ?>
					<option value="<?php echo( $sedesV['headquarter_id'] ); ?>"><?php echo( $sedesV['headquarter'] ); ?> ( <?php echo( $sedesV['address1'] ); ?> )</option>
				<?php } ?>
			</optgroup>
			<?php
		break;

		case 'pais':
			include_once("../models/rep_ventas.php");
			$datos_Class = new RepVentas();
			//parametros
			// $aux = $this->stringConcesionarios( $p['zona'], $p['prof'] );
			$p = [
				"start_date" 	=> $_POST['start_date_day'],
				"end_date"		=> $_POST['end_date_day'],
				"prof"			=> "../",
				"opcn"			=> "pais",
			];
			//grafica pais
			$d_pais = $datos_Class->consultaPais( $p );
			//grafica total por zonas
			$gr_zona_fac = $datos_Class->numFacturasZona( $p );
			$gr_zona_mat = $datos_Class->numMatriculasZona( $p );
			//grafica total por concesionarios
			$gr_concs_fac = $datos_Class->numFacturasConcs( $p );
			$gr_concs_mat = $datos_Class->numMatriculasConcs( $p );
			//grafica total por modelos
			$gr_modelos_fac = $datos_Class->numFacturasModelos( $p );
			$gr_modelos_mat = $datos_Class->numMatriculasModelos( $p );
			//grafica genero
			$gr_genero = $datos_Class->numGenero( $p );
			//grafica edad
			$gr_edad = $datos_Class->numEdad( $p );
			//grafica usuarios
			$gr_agentes = $datos_Class->numXAgentes( $p );

			//genera el arreglo final
			$pais = [
				"gr_pais" 			=> $d_pais,
				"gr_zona_fac"		=> $gr_zona_fac,
				"gr_zona_mat"		=> $gr_zona_mat,
				"gr_concs_mat"		=> $gr_concs_mat,
				"gr_concs_fac"		=> $gr_concs_fac,
				"gr_modelos_mat"	=> $gr_modelos_mat,
				"gr_modelos_fac"	=> $gr_modelos_fac,
				"gr_genero"			=> $gr_genero,
				"gr_edad"			=> $gr_edad,
				"gr_agentes"		=> $gr_agentes
			];
			echo( json_encode( $pais ) );
		break;

		case 'zona':
			include_once("../models/rep_ventas.php");
			$datos_Class = new RepVentas();
			$p = [
				"start_date" 	=> $_POST['start_date_day'],
				"end_date"		=> $_POST['end_date_day'],
				"prof"			=> "../",
				"opcn"			=> "zona",
				"zona"			=> $_POST['zone_id']
			];
			//grafica total por zonas
			$gr_zona_fac = $datos_Class->numFacturasZona( $p );
			$gr_zona_mat = $datos_Class->numMatriculasZona( $p );
			//grafica total por concesionarios
			$gr_concs_fac = $datos_Class->numFacturasConcs( $p );
			$gr_concs_mat = $datos_Class->numMatriculasConcs( $p );
			// grafica total por modelos
			$gr_modelos_fac = $datos_Class->numFacturasModelos( $p );
			$gr_modelos_mat = $datos_Class->numMatriculasModelos( $p );
			//grafica genero
			$gr_genero = $datos_Class->numGenero( $p );
			//grafica edad
			$gr_edad = $datos_Class->numEdad( $p );
			//grafica usuarios
			$gr_agentes = $datos_Class->numXAgentes( $p );
			$zonas = [
				"gr_zona_fac"		=> $gr_zona_fac,
				"gr_zona_mat"		=> $gr_zona_mat,
				"gr_concs_fac"		=> $gr_concs_fac,
				"gr_concs_mat"		=> $gr_concs_mat,
				"gr_modelos_fac"	=> $gr_modelos_fac,
				"gr_modelos_mat"	=> $gr_modelos_mat,
				"gr_genero"			=> $gr_genero,
				"gr_edad"			=> $gr_edad,
				"gr_agentes"		=> $gr_agentes
			];
			echo( json_encode( $zonas ) );
		break;

		case 'concesionario':
			include_once("../models/rep_ventas.php");
			$datos_Class = new RepVentas();
			$p = [
				"start_date" 	=> $_POST['start_date_day'],
				"end_date"		=> $_POST['end_date_day'],
				"prof"			=> "../",
				"opcn"			=> "concesionario",
				"concesionario"	=> $_POST['dealer_id']
			];
			//grafica total por concesionarios
			$gr_concs_fac = $datos_Class->numFacturasConcs( $p );
			$gr_concs_mat = $datos_Class->numMatriculasConcs( $p );
			// grafica total por modelos
			$gr_modelos_fac = $datos_Class->numFacturasModelos( $p );
			$gr_modelos_mat = $datos_Class->numMatriculasModelos( $p );
			//grafica genero
			$gr_genero = $datos_Class->numGenero( $p );
			//grafica edad
			$gr_edad = $datos_Class->numEdad( $p );
			//grafica usuarios
			$gr_agentes = $datos_Class->numXAgentes( $p );
			$zonas = [
				"gr_concs_fac"		=> $gr_concs_fac,
				"gr_concs_mat"		=> $gr_concs_mat,
				"gr_modelos_fac"	=> $gr_modelos_fac,
				"gr_modelos_mat"	=> $gr_modelos_mat,
				"gr_genero"			=> $gr_genero,
				"gr_edad"			=> $gr_edad,
				"gr_agentes"		=> $gr_agentes
			];
			echo( json_encode( $zonas ) );
		break;

		case 'sede':
			include_once("../models/rep_ventas.php");
			$datos_Class = new RepVentas();
			$p = [
				"start_date" 	=> $_POST['start_date_day'],
				"end_date"		=> $_POST['end_date_day'],
				"prof"			=> "../",
				"opcn"			=> "sede",
				"sede"			=> $_POST['headquarter_id']
			];
			//grafica total por Sedes
			$gr_concs_fac = $datos_Class->numFacturasConcs( $p );
			$gr_concs_mat = $datos_Class->numMatriculasConcs( $p );
			// grafica total por modelos
			$gr_modelos_fac = $datos_Class->numFacturasModelos( $p );
			$gr_modelos_mat = $datos_Class->numMatriculasModelos( $p );
			//grafica genero
			$gr_genero = $datos_Class->numGenero( $p );
			//grafica edad
			$gr_edad = $datos_Class->numEdad( $p );
			//grafica usuarios
			$gr_agentes = $datos_Class->numXAgentes( $p );
			$sedes = [
				"gr_concs_fac"		=> $gr_concs_fac,
				"gr_concs_mat"		=> $gr_concs_mat,
				"gr_modelos_fac"	=> $gr_modelos_fac,
				"gr_modelos_mat"	=> $gr_modelos_mat,
				"gr_genero"			=> $gr_genero,
				"gr_edad"			=> $gr_edad,
				"gr_agentes"		=> $gr_agentes
			];
			echo( json_encode( $sedes ) );
		break;

		case 'usuario':
			include_once("../models/rep_ventas.php");
			$datos_Class = new RepVentas();
			$p = [
				"start_date" 	=> $_POST['start_date_day'],
				"end_date"		=> $_POST['end_date_day'],
				"prof"			=> "../",
				"opcn"			=> "usuario",
				"usuario"		=> $_POST['users']
			];
			// //grafica total por concesionarios
			// $gr_concs_fac = $datos_Class->numFacturasConcs( $p );
			$gr_concs_mat = $datos_Class->numMatriculasConcs( $p );
			// grafica total por modelos
			// $gr_modelos_fac = $datos_Class->numFacturasModelos( $p );
			$gr_modelos_mat = $datos_Class->numMatriculasModelos( $p );
			//grafica genero
			$gr_genero = $datos_Class->numGenero( $p );
			//grafica edad
			$gr_edad = $datos_Class->numEdad( $p );
			//grafica usuarios
			$gr_agentes = $datos_Class->numXAgentes( $p );
			$zonas = [
				"gr_concs_mat"		=> $gr_concs_mat,
				"gr_modelos_mat"	=> $gr_modelos_mat,
				"gr_genero"			=> $gr_genero,
				"gr_edad"			=> $gr_edad,
				"gr_agentes"		=> $gr_agentes
			];
			echo( json_encode( $zonas ) );
		break;
	}
}else{
	include('models/rep_ventas.php');
	$datos_Class = new RepVentas();
	$zona = "0";
	$concesionario = "0";

	if(isset($_POST['zone_id'])){
		$zona = $_POST['zone_id'];
	}
	if(isset($_POST['dealer_id'])){
		$concesionario = $_POST['dealer_id'];
	}

	$Concesionarios	= $datos_Class->Concesionarios();
	$Zonas			= $datos_Class->Zonas();

	unset($datos_Class);
}
