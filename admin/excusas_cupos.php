<?php include('src/seguridad.php'); ?>
<?php include('controllers/excusas_cupos.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons hospital_h"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/excusas_cupos.php">Configuración Excusas cupos</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración de Excusas cupos</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-9">
									<h5 style="text-align: justify; ">A continuación encontrará las opciones necesarias para las excusas, debe tener especial cuidado al <strong class="text-danger">CARGAR UNA EXCUSA</strong> pues esta eliminará el registro de la nota y la invitación para que no afecten los resultados de dicho usuario y no podrá reversarlo, por ende no ejecute el proceso si no se encuentra completamente seguro del mismo.</h5>
								</div>
								<div class="col-md-1">
								</div>
								<div class="col-md-2">
									<?php if($_SESSION['max_rol']>=5 || $_SESSION['max_rol']==3 ){ ?>
										<h5>
											<span class="btn glyphicons no-js circle_plus" id="crearExcusa"><i></i>Agregar Excusa</span>
											<input type="hidden" id="tipoU" value="<?php echo $_SESSION['max_rol'] >= 5 ? "1" : "0" ; ?>">
										</h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Administrar ítems de lista</h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de excusas cargadas: <?php echo($cantidad_datos); ?>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-inline separator bottom small">
										<div class="form-group">
										  <label for="anio">año:</label>
										  <select style="width:100%" id="anio" placeholder="">
											  <option value="0" <?php if( date('m') == "0" ){ echo("selected"); } ?>>Todos</option>
											  <option value="2015" <?php if( date('Y') == "2015" ){ echo("selected"); } ?>>2015</option>
											  <option value="2016" <?php if( date('Y') == "2016" ){ echo("selected"); } ?>>2016</option>
											  <option value="2017" <?php if( date('Y') == "2017" ){ echo("selected"); } ?>>2017</option>
											  <option value="2018" <?php if( date('Y') == "2018" ){ echo("selected"); } ?>>2018</option>
										  </select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-inline separator bottom small">
										<div class="form-group">
										  <label for="mes">Mes:</label>
										  <select style="width:100%" id="mes" placeholder="">
											  <option value="0" <?php if( date('m') == "0" ){ echo("selected"); } ?>>Todos</option>
											  <option value="1" <?php if( date('m') == "1" ){ echo("selected"); } ?>>Enero</option>
											  <option value="2" <?php if( date('m') == "2" ){ echo("selected"); } ?>>Febrero</option>
											  <option value="3" <?php if( date('m') == "3" ){ echo("selected"); } ?>>Marzo</option>
											  <option value="4" <?php if( date('m') == "4" ){ echo("selected"); } ?>>Abril</option>
											  <option value="5" <?php if( date('m') == "5" ){ echo("selected"); } ?>>Mayo</option>
											  <option value="6" <?php if( date('m') == "6" ){ echo("selected"); } ?>>Junio</option>
											  <option value="7" <?php if( date('m') == "7" ){ echo("selected"); } ?>>Julio</option>
											  <option value="8" <?php if( date('m') == "8" ){ echo("selected"); } ?>>Agosto</option>
											  <option value="9" <?php if( date('m') == "9" ){ echo("selected"); } ?>>Septiembre</option>
											  <option value="10" <?php if( date('m') == "10" ){ echo("selected"); } ?>>Octubre</option>
											  <option value="11" <?php if( date('m') == "11" ){ echo("selected"); } ?>>Noviembre</option>
											  <option value="12" <?php if( date('m') == "12" ){ echo("selected"); } ?>>Diciembre</option>
										  </select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-inline separator bottom small" <?php if( $_SESSION['max_rol'] < 5 ){ echo "style='display:none;'"; } ?>>
										<div class="form-group">
										  <label for="concesionario_l">Concesionario:</label>
										  <select style="width:100%" id="concesionario_l" placeholder="">
											  <option value="0">Todos</option>
											  <?php foreach ($concesionarios as $key => $c): ?>
												  <option value="<?php echo( $c['dealer_id'] ); ?>" <?php if( $_SESSION['max_rol'] < 5 && $c['dealer_id'] == $_SESSION['dealer_id'] ){ echo 'selected'; } ?> > <?php echo( $c['dealer'] ); ?></option>
											  <?php endforeach; ?>
										  </select>
										</div>
									</div>
								</div>
							</div>
							<!-- // Total elements END -->
							<!-- Table elements-->
							<table id="TableData" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-hide="phone,tablet">PROGRAMACIÓN</th>
										<th data-hide="phone,tablet">CONCESIONARIO</th>
										<th data-hide="phone,tablet">EXCUSA</th>
										<th data-hide="phone,tablet">ESTADO</th>
										<th data-hide="phone,tablet">CREADOR</th>
										<th data-hide="phone,tablet">FECHA CREACIÓN</th>
										<th data-hide="phone,tablet">OPCION</th>
									</tr>
								</thead>
								<!-- // Table heading END -->
							</table>
							<!-- // Table elements END -->
						</div>
					</div>
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/excusas_cupos.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
