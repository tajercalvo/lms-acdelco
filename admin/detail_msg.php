<?php include('src/seguridad.php'); 
$detail_msg = "SI";
$idPersona = $_POST['idUsr'];
include('models/usuarios.php');
$usuario_Class = new Usuarios();
$datosUsuario = $usuario_Class->consultaDatosUsuario($idPersona);
$nomPersona = $datosUsuario['first_name'].' '.$datosUsuario['last_name'];
unset($usuario_Class);
?>
<?php include('controllers/mensajes.php'); ?>
<span href="" class="load"><i class="fa fa-calendar innerR third"></i>Mensajes / Notificaciones <?php if(isset($name_per)){ echo('Con: '.$name_per); }?></span>
<input type="hidden" name="name_user_newMsg" id="name_user_newMsg" value="<?php echo ($name_per); ?>" />
<?php foreach ($mensajes_Persona as $iID => $dataMessage_detail) { ?>
<div class="row row-merge borders border-top">
	<div class="col-md-12">
		<div class="innerAll">
			<div class="media">
				<a href="" class="thumb pull-left visible-md visible-lg"><img src="../assets/images/usuarios/<?php echo $dataMessage_detail['cr_im']; ?>" alt="Image" class="media-object" style="width: 50px; height: 50px;" /></a>
				<div class="media-body">
					<small class="pull-right text-primary-light"><?php if($dataMessage_detail['type']="1"){ ?><i class="fa fa-comment fa fa-fixed-width"></i><?php }else{?><i class="fa fa-bell fa fa-fixed-width"></i><?php } ?> <i class="fa fa-calendar fa fa-fixed-width"></i> <?php echo $dataMessage_detail['date_creation']; ?></small>
					<strong class="text-primary"><?php echo $dataMessage_detail['cr_fn'].' '.$dataMessage_detail['cr_ln']; ?></strong><br/>
					<small><?php echo $dataMessage_detail['message']; ?></small>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>