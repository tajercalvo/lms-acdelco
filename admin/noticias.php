<?php include('src/seguridad.php');
	if(!isset($_SESSION['idUsuario'])){
		header("location: login");
	}

?>
<?php include('controllers/noticias.php');
$location = 'tutor';
$qtip = 'qtip';
$locData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons coffe_cup"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/noticias.php">Noticias</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Noticias:</h2>
						<div class="btn-group pull-right">
							<h5>
								<?php if(isset($_GET['id'])){ ?>
									<a href="noticias.php" class="glyphicons no-js unshare" ><i></i>Regresar</a>
								<?php }else{?>
									<?php if($_SESSION['max_rol']>=6){ ?>
										<a href="#ModalCrearNueva" data-toggle="modal" class="glyphicons no-js circle_plus" ><i></i>Agregar Noticia</a>
									<?php } ?>
								<?php } ?>
							<h5>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<?php if( !isset( $_GET['id'] ) ){ ?>
						<div class="relativeWrap" >
							<div class="box-generic">

								<!-- Inicio Noticia principal -->
								<?php $categ = $Noticias_datos[0]; ?>
								<div class="row">
									<div class="col-md-1"></div>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-5">
												<div data-toggle="gridalicious" data-gridalicious-width="280" class="hide2">
													<div class="widget widget-heading-simple widget-body-white widget-pinterest">
														<div class="widget-body padding-none">
															<a href="noticias.php?id=<?php echo($categ['news_id']); ?>" class="thumb no-ajaxify" data-gallery>
																<img src="../assets/gallery/source/<?php echo($categ['image']); ?>" alt="<?php echo($categ['news']); ?>"/>
																<!-- <img src="../assets/images/gallery/1.jpg" alt="photo" /> -->
															</a>
															<div class="description">
																<div class="row row-merge">
																	<div class="col-md-6">
																		<a href="noticias.php?id=<?php echo($categ['news_id']); ?>" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> <?php echo($categ['comments']); ?></a>
																	</div>
																	<?php
																	$titulo_otr = "";
																	if(isset($categ[0]['likes_data'])){
																		foreach ($categ[0]['likes_data'] as $iID => $otras) {
																			$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																		}
																	}
																	?>
																	<div class="col-md-6">
																		<a href="noticias.php?id=<?php echo($categ['news_id']); ?>" class="innerAll text-center display-block text-muted" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo($categ['likes']); ?></a>
																	</div>
																</div>
																<!-- <h5 class="text-uppercase">Photo title</h5>
																<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p> -->
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-7">
												<h2>
													<a href="noticias.php?id=<?php echo($categ['news_id']); ?>"><?php echo $categ['news'] ?></a>
												</h2>
												<hr>
												<?php
													$var_descripcion_D = $categ['description'];
													$var_descripcion_D = strip_tags( $var_descripcion_D );
													$posicion = strpos( $var_descripcion_D, '.' ) ? strpos( $var_descripcion_D, '.' ) : strlen( $var_descripcion_D );
													$var_descripcion_D = substr($var_descripcion_D,0, $posicion );
												?>
												<h4><?php echo( $var_descripcion_D ); ?>.</h4>
												<p>editado: <span><?php echo( $categ['date_edition'] ); ?></span></p>
											</div>
										</div>
										<!-- <pre><?php //print_r($categ[0]); ?></pre> -->

									</div>
									<div class="col-md-1"></div>
								</div>
								<div class="separator bottom"></div>
								<!-- Fin Noticia principal -->


								<hr>
								<!-- Tabs Heading -->
								<div class="tabsbar">
									<ul class="">

										<!-- genera el menu dinamicamente con los titulos -->
										<?php for($i = 0; $i<count( $categorias ); $i++){ ?>
											<li class="<?php if( $i == 0 ){ echo('active'); } ?>">
												<a href="#tab<?php echo($i+1); ?>-4" data-toggle="tab">
													<span><?php echo( $categorias[$i]['category'] ); ?></span>
												</a>
											</li>
										<?php } ?>
									</ul>
								</div>

								<!-- <pre><?php print_r( $categorias ); ?></pre> -->
								<!-- // Tabs Heading END -->
								<div class="tab-content">
									<!-- Tab content -->
									<?php for ( $j = 0; $j < count( $categorias ) ; $j++ ) { ?>
										<?php $categ = $grupos[ $categorias[$j]['category'] ]; ?>
										<?php
											$num_c = count( $categ );
											$cierre = "si";
											$aux = 0;
										?>
										<div class="tab-pane <?php if( $j == 0 ){ echo 'active'; } ?>" id="tab<?php echo( $j+1 ); ?>-4">

											<?php for ($i = 0; $i < $num_c ; $i++) { ?>
												<?php $aux++; ?>
												<?php if ( isset( $categ[$i]['news_id'] ) ): ?>
													<?php
														if ( $cierre == "si" ) {
														$cierre = "no"; ?>
														<div class="row">
													<?php } ?>

													<div class="item col-md-2">
														<div class="box-generic padding-none margin-none overflow-hidden" style="border: 2px solid gold;">
															<div class="innerAll inner-2x border-bottom">
																<h4>
																	<a href="noticias.php?id=<?php echo($categ[$i]['news_id']); ?>"><?php echo(substr($categ[$i]['news'],0,25)); ?>...</a>
																</h4>
																<?php $var_descripcion_D = $categ[$i]['description'];
																// $var_descripcion_D = str_replace('<br>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('<p>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('<span>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('</span>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('</p>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('<b>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('</b>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('<ul>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('</ul>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('<li>', '', $var_descripcion_D);
																// $var_descripcion_D = str_replace('</li>', '', $var_descripcion_D);
																$var_descripcion_D = strip_tags( $var_descripcion_D );
																$var_descripcion_D = substr($var_descripcion_D,0,65);
																?>
																<p class="margin-none"><?php echo($var_descripcion_D); ?>...</p>
															</div>

															<div class="relativeWrap overflow-hidden" style="height:150px">
																<img src="../assets/gallery/source/<?php echo($categ[$i]['image']); ?>" alt="<?php echo($categ[$i]['news']); ?>" class="img-responsive padding-none border-none" />
																<div class="fixed-bottom bg-inverse-faded" style="background: rgba(66, 66, 66, 0.9) !important;">
																	<div class="media margin-none innerAll">
																		<div class="row">
																			<div class="col-md-10">
																				<div class="media-body text-white">
																					<p class="text-small margin-none"><i class="fa fa-fw fa-clock-o"></i> <?php echo($categ[$i]['date_edition']); ?></p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="row row-merge">
																<div class="col-md-6">
																	<a href="noticias.php?id=<?php echo($categ[$i]['news_id']); ?>" class="innerAll text-center display-block text-muted"><i class="fa fa-fw icon-comment-fill-1"></i> <?php echo($categ[$i]['comments']); ?></a>
																</div>
																<?php
																$titulo_otr = "";
																if(isset($categ[$i]['likes_data'])){
																	foreach ($categ[$i]['likes_data'] as $iID => $otras) {
																		$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																	}
																}
																?>
																<div class="col-md-6">
																	<a href="noticias.php?id=<?php echo($categ[$i]['news_id']); ?>" class="innerAll text-center display-block text-muted" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> <?php echo($categ[$i]['likes']); ?></a>
																</div>
															</div>
														</div>
													</div>
													<!-- <?php //echo "fin $aux estado $cierre $aux == 6 || $i >= $num_c" ?> -->
													<?php if ( $cierre == "no" && ( $aux == 6 || $i >= $num_c-1 ) ) { ?>
														</div>
														<div class="separator bottom"></div>
													<?php
													$aux = 0;
													$cierre = "si";
													} //fin if $cierre == no ?>

												<?php endif; ?>
											<?php } ?>
										</div>
									<?php } //fin foreach categorias ?>
									<!-- // Tab content END -->
								</div>
							</div>
						</div>
					<?php } ?>
					<!-- fin seccion de las categorias -->


					<!-- Seccion de noticia seleccionada -->
					<?php if(isset($_GET['id'])){ ?>
						<div class="widget innerAll" id="DetailNoticia">
							<div class="row row-merge border-none">
								<div class="col-md-4 border-none" style="padding-right:5px">
									<img src="../assets/gallery/source/<?php echo($Noticia_datos['image']); ?>" alt="<?php echo $Noticia_datos['news']; ?>" width="100%" id="imagenNoticia" style="cursor:pointer">
									<br>
									<hr>
									<?php if ( $Noticia_datos['file'] != "" ): ?>
										<a href="../assets/gallery/source/<?php echo( $Noticia_datos['file'] ); ?>" target="_blank" class="btn btn-default"> <i class="fa fa-file-text"></i> Información adicional</a>
									<?php endif; ?>
									<br>
									<br>
									<p>A <span style="font-family:FontAwesome;font-weight:normal;font-style:normal;color:#4d4d4d !important" id="CantLikes<?php echo($Noticia_datos['news_id']); ?>"><?php echo(number_format($Noticia_datos['likes'],0)); ?></span> Usuarios les gusta este artículo <br></p>
									<a href="#" onclick="LikeGalery(<?php echo($Noticia_datos['news_id']); ?>); return false;"><i class="fa fa-fw fa-thumbs-o-up"></i> Me gusta</a><br><br>
									<?php if($_SESSION['max_rol']>=6){ ?>
										<p class="text-small margin-none">
											<a href="#ModalEditar" data-toggle="modal"><i class="fa fa-fw fa-pencil"></i> Editar </a>
										</p>
									<?php } ?>
								</div>
								<div class="col-md-8 border-none">
									<div class="media margin-none" style="padding-left:5px">
										<div class="media-body innerTB" style="padding:0px">
											<h4 style="color:#0058a3"><?php echo $Noticia_datos['news']; ?></h4>
											<p class="text-muted"><?php echo $Noticia_datos['date_edition']; ?></p>
											<p class="margin-none" style="text-align: justify;"><?php echo $Noticia_datos['description']; ?></p>
										</div>
									</div><br>
									<!-- Blueimp Gallery -->
									<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
										<div class="slides"></div>
										<h3 class="title">Titulo de la fografía</h3>
										<a class="prev no-ajaxify">‹</a>
										<a class="next no-ajaxify">›</a>
										<a class="close no-ajaxify">×</a>
										<a class="play-pause no-ajaxify"></a>
										<ol class="indicator"></ol>
									</div>
									<!-- // Blueimp Gallery END -->
									<!-- About -->
									<div class="widget widget-heading-simple widget-body-white margin-none">
										<div class="widget-head"><h4 class="heading glyphicons picture" style="color:#0058a3 !important;margin-left: 5px;"><i></i>Galería de Imágenes</h4></div>
										<div class="widget-body">
											<div class="widget-gallery" data-toggle="collapse-widget">
												<!-- Gallery Layout -->
												<div class="gallery gallery-2">
													<ul class="row">
														<?php foreach ($Galeria_datos as $key => $Data_Gal) { ?>
															<li class="col-md-2">
																<a class="thumb no-ajaxify" data-description="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" data-title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" data-gallery="gallery-2" href="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>">
																	<img src="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>" title="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" alt="<?php echo($Data_Gal['media_detail'].' - '.$Data_Gal['description']); ?>" class="img-responsive" />
																</a>
															</li>
														<?php } ?>
													</ul>
												</div>
												<!-- // Gallery Layout END -->
											</div>
										</div>
									</div>
									<!-- // About END -->
									<!-- About -->
									<div class="widget widget-heading-simple widget-body-white margin-none">
										<div class="widget-head"><h4 class="heading glyphicons collapse" style="color:#0058a3 !important;margin-left: 5px;"><i></i>Galería de Videos</h4></div>
										<div class="widget-body">
											<div class="widget-gallery" data-toggle="collapse-widget">
												<!-- Gallery Layout -->
													<div class="row">
														<?php foreach ($Galeria_videos as $key => $Data_Gal) { ?>
															<div class="col-md-2">
																<?php
																$_VarVidYoutube = strpos($Data_Gal['video'],'youtu');
																if($_VarVidYoutube !== false){ ?>
																	<a data-toggle="prettyPhoto" class="thumb" href="<?php echo($Data_Gal['video']); ?>" onclick="ViewGalery(<?php echo($Data_Gal['media_detail_id']); ?>); return false;">
																		<img style="width:100%" src="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>" alt="<?php echo($Data_Gal['media_detail']); ?>" />
																	</a>
																<?php }else{ ?>
																	<a data-toggle="modal" class="thumb" href="#myModal" onclick="DatosVideo('<?php echo($Data_Gal['video']); ?>','<?php echo($Data_Gal['media_detail']); ?>'); ViewGalery(<?php echo($Data_Gal['media_detail_id']); ?>);">
																		<img style="width:100%" src="../assets/gallery/source/<?php echo($Data_Gal['source']); ?>" alt="<?php echo($Data_Gal['media_detail']); ?>" />
																	</a>
																<?php } ?>
															</div>
														<?php } ?>
													</div>
												<!-- // Gallery Layout END -->
											</div>
										</div>
									</div>
									<!-- // About END -->
								</div>
							</div>
						</div>
					<?php } ?>
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>

		<!-- Modal -->
		<form action="noticias.php" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
			<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Crear Nueva Noticia</h4>
						</div>
						<div class="modal-body">
							<div class="widget widget-heading-simple widget-body-gray">
					<div class="widget-body">
						<!-- Row -->
						<div class="row">
							<div class="col-md-1">
							</div>
							<div class="col-md-10">
								<label class="control-label">Titulo: </label>
								<input type="text" id="news" name="news" class="form-control col-md-8" placeholder="Título de la noticia" />
							</div>
							<div class="col-md-1">
								<input type="hidden" id="opcn" value="crear" name="opcn">
							</div>
						</div>
						<!-- Row END-->
						<div class="clearfix"><br></div>
						<!-- Row -->
						<div class="row">
							<div class="col-md-1">
							</div>
							<div class="col-md-10">
								<label class="control-label">Descripción: </label>
								<textarea id="description" name="description" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
							</div>
							<div class="col-md-1">
							</div>
						</div>
						<!-- Row END-->
						<div class="clearfix"><br></div>
						<!-- Row -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<label class="control-label">Categoria: </label>
								<input type="text" id="category" name="category" class="form-control col-md-8" placeholder="Nombre de la categoria de la noticia" />
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- Row END-->
						<div class="clearfix"><br></div>
						<!-- Row -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<label class="control-label">Galería de Imágenes: </label>
								<select style="width: 100%;" id="media_id" name="media_id" >
									<option value="0">Sin Galería</option>
									<?php foreach ($Argumentarios_Fotos as $key => $Data_Med) { ?>
										<option value="<?php echo($Data_Med['media_id']); ?>" ><?php echo($Data_Med['media']); ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- Row END-->
						<div class="clearfix"><br></div>
						<!-- Row -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<label class="control-label">Galería de Videos: </label>
								<select style="width: 100%;" id="video_id" name="video_id" >
									<option value="0">Sin Galería</option>
									<?php foreach ($Argumentarios_Videos as $key => $Data_Med) { ?>
										<option value="<?php echo($Data_Med['media_id']); ?>" ><?php echo($Data_Med['media']); ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- Row END-->
						<div class="clearfix"><br></div>
						<!-- Row -->
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
								  	<span class="btn btn-default btn-file">
								  		<span class="fileupload-new">Adjuntar Archivo</span>
								  		<span class="fileupload-exists">Cambiar Archivo</span>
									  	<input type="file" class="margin-none" id="file_new" name="file_new" accept="application/pdf" />
									</span>
								  	<span class="fileupload-preview"></span>
								  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
								</div>
							</div>
							<div class="col-md-1"></div>
						</div>
						<!-- Row END-->
						<div class="clearfix"><br></div>
						<!-- Row -->
						<div class="row">
							<div class="col-md-1">
							</div>
							<div class="col-md-10">
								<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
								  	<span class="btn btn-default btn-file">
								  		<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
								  		<span class="fileupload-exists">Cambiar Imagen</span>
									  	<input type="file" class="margin-none" id="image_new" name="image_new" />
									</span>
								  	<span class="fileupload-preview"></span>
								  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
								</div>
							</div>
							<div class="col-md-1">
							</div>
						</div>
						<!-- Row END-->
						<div class="clearfix"><br></div>
					</div>
				</div>
						</div>
						<div class="modal-footer">
							<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- /.modal -->

		<!-- Modal -->
		<form action="noticias.php?id=<?php echo $_GET['id']; ?>" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
			<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Editar la Noticia</h4>
						</div>
						<div class="modal-body">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-10">
											<label class="control-label">Titulo: </label>
											<input type="text" id="news_ed" name="news_ed" class="form-control col-md-8" placeholder="Título de la noticia" value="<?php echo $Noticia_datos['news']; ?>" />
										</div>
										<div class="col-md-1">
											<input type="hidden" id="news_id" value="<?php echo $_GET['id']; ?>" name="news_id">
											<input type="hidden" id="opcn" value="editar" name="opcn">
											<input type="hidden" id="imgant" value="<?php echo $Noticia_datos['image']; ?>" name="imgant">
											<input type="hidden" id="fileant" value="<?php echo $Noticia_datos['file']; ?>" name="fileant">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-10">
											<label class="control-label">Descripción: </label>
											<textarea id="description_ed" name="description_ed" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo $Noticia_datos['description']; ?></textarea>
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-10">
											<label class="control-label">Categoria: </label>
											<input type="text" id="category_ed" name="category_ed" class="form-control col-md-8" placeholder="Nombre de la categoria de la noticia" value="<?php echo $Noticia_datos['category']; ?>" />
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-10">
											<label class="control-label">Galería de Imágenes:</label>
											<select style="width: 100%;" id="media_id_ed" name="media_id_ed" >
												<option value="0">Sin Galería</option>
											<?php foreach ($Argumentarios_Fotos as $key => $Data_Med) { ?>
												<option value="<?php echo($Data_Med['media_id']); ?>" <?php if(isset($Noticia_datos['media_id']) && $Noticia_datos['media_id']==$Data_Med['media_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
											</select>
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-10">
											<label class="control-label">Galería de Videos:</label>
											<select style="width: 100%;" id="video_id_ed" name="video_id_ed" >
												<option value="0">Sin Galería</option>
											<?php foreach ($Argumentarios_Videos as $key => $Data_Med) { ?>
												<option value="<?php echo($Data_Med['media_id']); ?>" <?php if(isset($Noticia_datos['video_id']) && $Noticia_datos['video_id']==$Data_Med['media_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
											</select>
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-5">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											  	<span class="btn btn-default btn-file">
											  		<span class="fileupload-new">Seleccionar Archivo</span>
											  		<span class="fileupload-exists">Cambiar Archivo</span>
												  	<input type="file" class="margin-none" id="file_new_ed" name="file_new_ed" accept="application/pdf" value="../assets/gallery/source/<?php echo( $Noticia_datos['file'] ); ?>"/>
												</span>
											  	<span class="fileupload-preview"></span>
											  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
											</div>
										</div>
										<div class="col-md-1">
										</div>
										<div class="col-md-4">
											<!-- <label class="control-label">Estado: </label>
											<select style="width: 100%;" id="estado" name="estado">
												<option value="1">Activo</option>
												<option value="2">Inactivo</option>
											</select> -->
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-1">
										</div>
										<div class="col-md-5">
											<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											  	<span class="btn btn-default btn-file">
											  		<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
											  		<span class="fileupload-exists">Cambiar Imagen</span>
												  	<input type="file" class="margin-none" id="image_new_ed" name="image_new_ed" />
												</span>
											  	<span class="fileupload-preview"></span>
											  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
											</div>
										</div>
										<div class="col-md-1">
										</div>
										<div class="col-md-4">
											<label class="control-label">Estado: </label>
											<select style="width: 100%;" id="estado" name="estado">
												<option value="1">Activo</option>
												<option value="2">Inactivo</option>
											</select>
										</div>
										<div class="col-md-1">
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!-- /.modal -->

		<div class="modal fade" id="ModalImagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<!-- <div class="modal-dialog" style="min-width: 600px;max-width: 1000px;"> -->
				<div class="modal-content" style="top:50px;width: 90%; margin: auto;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cerrarBtnC">&times;</button>
						<h4 class="modal-title">Imagen</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<img style="width: 100%;" src="../assets/gallery/source/<?php echo($Noticia_datos['image']); ?>" alt="<?php echo $Noticia_datos['news']; ?>">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="cerrar_modal" class="btn btn-primary">Cerrar</button>
					</div>
				</div>
			<!-- </div> -->
		</div>


		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/noticias.js"></script>
</body>
</html>
