<!DOCTYPE html>
<html>
<head>
   <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
 
  <title>Politicas de datos</title>
</head>
<body>
          <div id="contenido">

            <div class="row"></div>

            <div class="row">
              <div class="col-xl-1"></div>

              <div class="col-xl-10">
                <div style="text-align: center;"> <img src="../assets/images/LogoAcademyAcdelco-03.png" alt=""></div>
           

                  <div style="border: 2px solid #0d57a2">

                                          
                                <h1 style="color: #0d57a2"><strong>POL&iacuteTICAS DE PRIVACIDAD</strong></h1>
                      <h3 style="color: #0d57a2"><strong>Seguridad y Protecci&oacuten de sus Datos Personales</strong></h3>
                      <p>La seguridad de los datos personales es una prioridad para General Motors Colmotores S.A. Este sitio en Internet se esfuerza por ofrecer el m&aacutes alto nivel de seguridad para lo cual se utiliza tecnolog&iacutea de avanzada. No obstante, considerando el estado de la tecnolog&iacutea de transmisi&oacuten de datos por Internet, ning&uacuten sistema resulta actualmente 100% seguro o libre de ataques. Considere los m&eacutetodos de protecci&oacuten que utiliza General Motors Colmotores S.A.<br>
                      para proteger sus datos y su privacidad, que se indican a continuaci&oacuten:</p>
                      <p>1) Contrase&ntildea del usuario<br>
                      Para garantizar que terceros no autorizados no tengan acceso a los datos personales del usuario, cada usuario deber&aacute seleccionar una contrase&ntildea para acceder al sitio web, la cual podr&aacute ser modificada en cualquier momento. La contrase&ntildea para el acceso es personal e intransferible y la responsabilidad de guardarla en secreto es de cada usuario.</p>
                      <p>2) Otros m&eacutetodos para proteger la privacidad<br>
                      Adem&aacutes de los m&eacutetodos antes mencionados de seguridad, despu&eacutes de 15 minutos de inactividad en el sitio web, el sistema cancela autom&aacuteticamente la sesi&oacuten que se encuentra en proceso. De este modo, se evita que terceros no autorizados accedan a los datos personales del usuario en caso de que &eacutel/ella se ausente alg&uacuten tiempo de su computadora.</p>
                      <p><strong>Su privacidad</strong></p>
                      <p>General Motors Colmotores S.A . respeta su privacidad. Toda la informaci&oacuten que usted nos proporcione se tratar&aacute con sumo cuidado y con la mayor seguridad posible, y s&oacutelo se utilizar&aacute de acuerdo con los l&iacutemites establecidos en este documento y para actividades relacionadas con Mercadeo de acuerdo a los par&aacutemetros aprobados previamente por usted.</p>
                      <p><strong>C&oacutemo se re&uacutene la informaci&oacuten</strong></p>
                      <p>General Motors Colmotores S.A. solamente re&uacutene sus datos personales cuando usted los proporciona en forma directa.</p>
                      <p><strong>C&oacutemo General Motors Colmotores S.A utiliza su informaci&oacuten</strong></p>
                      <p>La informaci&oacuten por usted proporcionada a General Motors Colmotores S.A. tiene como finalidad el desarrollo del objeto social de la compa&ntilde&iacutea, su matriz, filiales, subsidiarias, controladas y similares, lo cual incluye pero no se limita a: fines estad&iacutesticos, comerciales, informativos, de seguimiento al producto, de mercadeo, de notificaci&oacuten y contacto al cliente para las campa&ntildeas de seguridad y/o de satisfacci&oacuten, mercadeo relacional y/o similares, verificaci&oacuten en centrales de riesgo, aspectos contables y de n&oacutemina, y dem&aacutes aplicables a cada uno de los titulares.</p>
                      <p><strong>¿Qui&eacuten tiene acceso a la informaci&oacuten?</strong></p>
                      <p>General Motors Colmotores S.A. siempre est&aacute comprometido a presentar nuevas soluciones que mejoren el valor de sus productos y servicios para ofrecerle a usted oportunidades especiales de comercializaci&oacuten, tales como incentivos y promociones. Para alcanzar esa meta, su informaci&oacuten podr&aacute ser compartida internamente y con algunos de nuestros socios comerciales, tales como los concesionarios Chevrolet a nivel nacional. General Motors Colmotores S.A. toma todas las medidas posibles para que se haga uso de la informaci&oacuten suministrada respetando la misma pol&iacutetica de privacidad que tiene General Motors Colmotores S.A.</p>
                      <p>La informaci&oacuten no identificable y estad&iacutestica tambi&eacuten podr&aacute ser compartida con socios comerciales. A excepci&oacuten de los casos anteriores, General Motors Colmotores S.A. no compartir&aacute informaci&oacuten que podr&iacutea identificar personalmente a sus clientes.</p>
                      <p><strong>¿C&oacutemo desea que se utilice la informaci&oacuten?</strong></p>
                      <p>Al proporcionar sus datos personales, usted esta autorizando autom&aacuteticamente a General Motors Colmotores S.A. a utilizarlos de acuerdo con esta Pol&iacutetica de Seguridad y Privacidad. Cuando solicita informaci&oacuten, General Motors Colmotores S.A indaga a usted sobre c&oacutemo desea que se utilice su informaci&oacuten para la comunicaci&oacuten futura con General Motors Colmotores S.A y con sus socios comerciales. En ese momento, si no est&aacute de acuerdo con la propuesta de uso que sugiere General Motors Colmotores S.A, usted podr&aacute desactivar las opciones no deseadas con un clic en el icono correspondiente. Si elige mantener activadas las opciones, autom&aacuteticamente usted estar&aacute autorizando a General Motors Colmotores S.A a utilizar sus datos personales para recibir correspondencia futura de las empresas de General Motors Colmotores S.A o sus socios comerciales.</p>
                      <p><strong>Recomendaciones generales sobre la protecci&oacuten de la privacidad</strong></p>
                      <p>Usted tambi&eacuten tiene un importante papel que desempe&ntildear en la protecci&oacuten de su privacidad. Expresamente recomendamos que no divulgue su contrase&ntildea a ninguna persona, sea por tel&eacutefono, mensaje de correo electr&oacutenico u otro medio disponible. Adem&aacutes, recomendamos que se desconecte del sitio de General Motors Colmotores S.A. y que cierre la ventana de su navegador al finalizar su visita para que terceros no tengan acceso a sus datos personales, especialmente cuando utiliza equipos p&uacuteblicos o compartidos. General Motors Colmotores S.A. no ser&aacute responsable en caso de que usted no tome en cuenta estas recomendaciones, ni por los da&ntildeos causados por dicho descuido.</p>
                      <p>Su usted es menor de edad, solicite permiso a sus padres o tutores antes de revelar sus datos personales a cualquier persona en Internet.</p>
                      <p><strong>Errores/Omisiones</strong></p>
                      <p>La informaci&oacuten y material contenido en este sitio en Internet han sido controlados para que sean exactos; sin embargo, dicha informaci&oacuten y material son suministrados sin ninguna garant&iacutea expresa o impl&iacutecita. General Motors Colmotores S.A. no asume ninguna responsabilidad por cualquier error u omisi&oacuten en dicha informaci&oacuten y material.</p>
                      <p><strong>Propiedad Intelectual</strong></p>
                      <p>Los elementos contenidos en este sitio en Internet (fotos, dise&ntildeos, logos, etc.) no pueden ser reproducidos, usados, adaptados o comercializados sin la aprobaci&oacuten escrita de General Motors Colmotores S.A.</p>
                      <p>Modificaciones de nuestras Pol&iacuteticas de privacidad / Notas Legales / Pol&iacuteticas de Tratamiento de Datos</p>
                      <p>General Motors Colmotores S.A. informa que todo cambio sustancial en las pol&iacuteticas y/o en las notas legales ser&aacute comunicado oportunamente por el presente medio, por lo cual recomendamos que examine esta Pol&iacutetica cada vez que visite el sitio de internet de General Motors Colmotores S.A.</p>
                      <p>POL&iacuteTICAS DE TRATAMIENTO DE DATOS PERSONALES</p>
                      <p>Las presentes pol&iacuteticas se encuentran dirigidas a todos nuestros Clientes Finales, Empleados, Ex empleados, Proveedores, Contratistas, Accionistas, Concesionarios y dem&aacutes titulares y legitimados de los datos personales registrados en cada una de nuestras bases de datos.</p>
                      <p>GENERAL MOTORS COLMOTORES S.A., (en lo sucesivo “GM COLMOTORES”), identificada con el NIT 860.002.304-3, con domicilio en la ciudad de Bogot&aacute D.C., en la avenida Boyac&aacute Calle 56 A Sur No. 33- 53, como responsable del tratamiento de los datos personales que actualmente reposan en las bases de datos de la compa&ntilde&iacutea, informa por este medio las Pol&iacuteticas de Tratamiento de Datos Personales, los cuales podr&aacuten ser objeto de almacenamiento tanto nacional como internacional, uso, circulaci&oacuten, supresi&oacuten, recepci&oacuten, recolecci&oacuten, actualizaci&oacuten, transferencia, y transmisi&oacuten tanto nacional como internacional.</p>
                      <p>El tratamiento de estos datos personales tiene como finalidad el desarrollo del objeto social de la compa&ntilde&iacutea, su matriz, filiales, subsidiarias, controladas y similares, lo cual incluye pero no se limita a: fines estad&iacutesticos, comerciales, informativos, de seguimiento al producto, de mercadeo, de notificaci&oacuten y contacto al cliente para las campa&ntildeas de seguridad y/o de satisfacci&oacuten, mercadeo relacional y/o similares, verificaci&oacuten en centrales de riesgo, aspectos contables y de n&oacutemina, y dem&aacutes aplicables a cada uno de los titulares.</p>
                      <p>Los titulares de la informaci&oacuten as&iacute como los legalmente legitimados tendr&aacuten derecho a: acceder, conocer, actualizar, rectificar, revocar y solicitar prueba de la autorizaci&oacuten, as&iacute como a la supresi&oacuten de los datos personales en los casos contemplados en la Ley, para lo cual podr&aacuten enviar su solicitud a: atencionchevrolet_col@cliente.gm.com, comunicarse con nuestro centro de atenci&oacuten al cliente Chevrolet®, a nivel nacional al n&uacutemero 0180001 chevy (24389), en Bogot&aacute al 4249393 o desde cualquier operador celular al #249, solicitudes que ser&aacuten remitidas a las &aacutereas correspondientes y responsables como lo son el &aacuterea de Servicio al Cliente, de Mercadeo y de Relaciones Laborales.</p>
                      <p>Las presentes pol&iacuteticas entran en vigencia el 27 de junio del a&ntildeo 2013 por un t&eacutermino indefinido.</p>
                      <p>Call center Bogot&aacute 424 93 93</p>
    

                    
                  </div>
              </div>
              <div class="col-xl-1"></div>
            </div>
            
          </div>
          
          <?php include('src/footer.php'); ?>
</body>
</html>