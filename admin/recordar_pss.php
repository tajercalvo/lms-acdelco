<?php
include('../config/config.php');
session_start();
if(isset($_SESSION['NameUsuario'])){
	header("location: login");
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
    <?php include('src/head_login.php'); ?>
</head>
<body class="document-body login">
	<!-- Wrapper -->
<div id="login">
	<div class="container">
		<div class="wrapper">
			<h1>
				<p><img src="../assets/images/wall/logo_ludus.png" width="100px"></p>
				<p><img src="../assets/images/wall/chevrolet_logo.png" width="384px"></p>
			</h1>
			<!-- Box -->
			<div class="widget widget-heading-simple widget-body-gray">
				<div class="widget-body">
					<!-- Form -->
					<form action="controllers/usuarios.php" method="post" name="login" id="login" onSubmit="return validateForm();"class="row">
						<label>e-Mail</label>
						<input type="text" class="input-block-level form-control" placeholder="email de su usuario en Ludus" id="email" name="email"/> 
						<input type="hidden" name="opcn" id="opcn" value="remember">
						<label><a class="password" href="login">Regresar</a></label>
						<div class="separator bottom"></div>
							<div class="col-md-4 pull-right padding-none">
								<button class="btn btn-block btn-primary" type="submit">Recordar</button>
							</div>
					</form>
					<!-- // Form END -->
				</div>
				<div class="widget-footer" id="Error_gral">
					<p class="glyphicons restart"><i></i>El eMail indicado no existe en la plataforma</p>
				</div>
				<div class="widget-footer" id="Success_gral">
					<p class="glyphicons restart"><i></i>Se ha reseteado su contraseña, use su identificación</p>
				</div>
			</div>
			<!-- // Box END -->
			<div class="innerT center">
				<!--<a href="signup.html?lang=es" class="btn btn-icon-stacked btn-block btn-success glyphicons user_add"><i></i><span>Don't have an account?</span><span class="strong">Sign up</span></a>
				<p class="innerT">Alternatively</p>
				<a href="index.html?lang=es" class="btn btn-icon-stacked btn-block btn-facebook glyphicons-social facebook"><i></i><span>Join using your</span><span class="strong">Facebook Account</span></a>
				<p>or</p>
				<a href="index.html?lang=es" class="btn btn-icon-stacked btn-block btn-google glyphicons-social google_plus"><i></i><span>Join using your</span><span class="strong">Google Account</span></a>-->
				<!--<p>Tiene problemas? <a href="mailto:ayuda@gmacademy.co" target="_blank">Obtenga ayuda</a></p>-->
			</div>
		</div>
	</div>
</div>
<!-- // Wrapper END -->
<?php include('src/boot_login.php'); ?>
<script src="js/recordar"></script>
<?php if(isset($_SESSION['errorRecord'])){ ?>
	<script language="javascript">
		$(document).ready(function(){
			$('#Error_gral').css( "border-color", "#b94a48" );
			$('#Error_gral').show();
		});
	</script>
<?php unset($_SESSION['errorRecord']); } ?>

<?php if(isset($_SESSION['successRecord'])){ ?>
	<script language="javascript">
		$(document).ready(function(){
			$('#Success_gral').css( "border-color", "#99CC92" );
			$('#Success_gral').show();
		});
	</script>
<?php unset($_SESSION['successRecord']); } ?>
</body>
</html>