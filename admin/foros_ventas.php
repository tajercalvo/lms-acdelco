<?php include('src/seguridad.php'); ?>
<?php include('controllers/foros_ventas.php');
$location = 'distribucion';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
//$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons conversation"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/foros_ventas.php">BROADCAST</a></li>
					<div class="btn-group pull-right">
						<?php if(isset($_GET['id'])){ ?>
							<a href="foros_ventas.php" class="btn btn-xs btn-primary pull-right" >
							<i class="fa fa-mail-reply-all fa-fw"></i>
							Regresar</a>
						<?php } ?>
					</div>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div>

						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<!-- ================================================ -->
					<!-- Lista programaciones -->
					<!-- ================================================ -->
					<h1 class="margin-none innerAll content-heading bg-white border-bottom">BROADCAST</h1>
					<div class="innerAll spacing-x2">
						<!-- row -->
						<div class="row">
							<!-- ===================================================================================== -->
							<!-- Lista de POST -->
							<!-- ===================================================================================== -->
							<div class="col-md-9 col-sm-8">
								<div class="widget" id="listado_post">
									<!-- Post List -->
									<!-- Post List -->
								</div>
							</div>
							<!-- ===================================================================================== -->
							<!-- END Lista de POST -->
							<!-- ===================================================================================== -->

							<!-- ===================================================================================== -->
							<!-- Opciones -->
							<!-- ===================================================================================== -->
							<div class="col-md-3 col-sm-4">
								<!-- ===================================================================================== -->
								<!-- Buscar un Post -->
								<!-- ===================================================================================== -->
								<div class="widget ">
									<div class="innerAll half border-bottom bg-gray">
										<div class="input-group">
								      		<input type="text" class="form-control" placeholder="Buscar una comunicación ..." id="filtro">
								      		<span class="input-group-btn">
								        		<button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
								      		</span>
								    	</div>
									</div>
								</div>
								<!-- ===================================================================================== -->
								<!-- END Buscar un Post -->
								<!-- ===================================================================================== -->

								<!-- ===================================================================================== -->
								<!-- Opciones para crear o buscar post -->
								<!-- ===================================================================================== -->
								<div class="widget">

									<!-- Heading -->
									<h5 class="innerAll half margin-bottom-none bg-primary"><i class="fa fa-refresh fa-fw"></i> Opciones</h5>

									<!-- Listing -->
									<div class="bg-gray-hover innerAll half border-bottom">
										<div class="innerAll half">
											<label class="" for="filtro_anio">año de comunicación</label>
											<input type="text" id="filtro_anio" style="width: 100%" value="">
										</div>
										<?php if ( isset( $_SESSION['max_rol'] ) && $_SESSION['max_rol'] >= 5 ): ?>
											<hr>
											<div class="innerAll half">
												<a id="btn_crear"  href="#ModalCrearNueva" data-toggle="modal" class="btn btn-xs btn-inverse"><i class="fa fa-plus fa-fw"></i> Nueva Conferencia</a>
											</div>
										<?php endif; ?>
									</div>
									<!-- // END Listing -->
								</div>
								<!-- ===================================================================================== -->
								<!-- END Opciones para crear o buscar post -->
								<!-- ===================================================================================== -->
							</div>
							<!-- ===================================================================================== -->
							<!-- END Opciones -->
							<!-- ===================================================================================== -->
						</div>
					</div>
					<!-- ================================================ -->
					<!-- END Lista programaciones -->
					<!-- ================================================ -->
					<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
			<!-- ================================================ -->
			<!-- Modal Edicion de hilo -->
			<!-- ================================================ -->
			<form enctype="multipart/form-data" id="form_Crear" autocomplete="off"><br><br>
				<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Crear Nuevo Hilo para comunicarse</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Moderador: </label>
												<input list="browsers" id="moderador" name="moderador" class="form-control col-md-8" placeholder="Nombre del moderador" >
													<datalist id="browsers">
													</datalist>
											</div>
											<div class="col-md-1">
												<!-- <input type="hidden" id="opcn" value="crear" name="opcn"> -->
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>

										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Comunicación: </label>
												<input type="text" id="conference" name="conference" class="form-control col-md-8" placeholder="Nombre de la comunicación" />
											</div>
											<div class="col-md-1">
												<!-- <input type="hidden" id="opcn" value="crear" name="opcn"> -->
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Descripción: </label>
												<textarea id="conference_description" name="conference_description" class="col-md-12 form-control" rows="5"></textarea>
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-4">
												<label class="control-label">Categoría: </label>
												<select style="width: 100%;" id="category" name="category" >
													<option value="Comunícate con tu fuerza de ventas">Comunícate con tu fuerza de ventas</option>
												</select>
											</div>
											<div class="col-md-1">
											</div>
											<div class="col-md-4">
												<label class="control-label">Posición: </label>
												<input type="text" id="position" name="position" class="form-control col-md-8" placeholder="# Posición de este hilo en el foro" />
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Cargos </h5>
											<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]">

								        	</select>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Sesión ID: </label>
												<input type="text" id="session_id" name="session_id" class="form-control col-md-8" placeholder="Sesión ID" />
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
										<!-- Row -->
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Token: </label>
												<textarea id="token" name="token" class="col-md-12 form-control" rows="5"></textarea>
											</div>
										</div>
										<!-- Row END-->
										<div class="clearfix"><br></div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
								<button type="reset" id="BtnReset" class="btn btn-default" style="display:none">Limpliar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- ================================================ -->
			<!-- END Modal crear foro -->
			<!-- ================================================ -->

			<!-- ================================================ -->
			<!-- Fomulario Cargar Imagenes Presentación -->
			<!-- ================================================ -->
			<form enctype="multipart/form-data" id="form_presentacion"><br><br>
				<div class="modal fade" id="ModalPresentacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Cargar presentación</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<!-- Row -->
										<!-- ================================================ -->
										<!-- Cargar Imagenes -->
										<!-- ================================================ -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Presentación: </label>
												<input type="file" id="imagenes" name="imagenes[]" multiple/>
											</div>
											<div class="col-md-1">
											</div>
										</div>
										<!-- ================================================ -->
										<!-- END Cargar Imagenes -->
										<!-- ================================================ -->
										<br>
										<!-- ================================================ -->
										<!-- tipo cargue -->
										<!-- ================================================ -->
										<div class="row">
											<div class="col-md-1">
											</div>
											<div class="col-md-10">
												<label class="control-label">Tipo de cargue: </label>
												<select style="witdh: 100%" id="tipo_cargue" name="tipo_cargue">
													<option value="nuevas">Nuevas</option>
													<option value="agregar">agregar</option>
												</select>
											</div>
											<div class="col-md-1">
											</div>
										</div>

										<!-- ================================================ -->
										<!-- END tipo cargue -->
										<!-- ================================================ -->
										<!-- Row END-->
										<div class="clearfix"><br></div>
									</div>
								</div>
							</div>
							<div class="modal-footer loader_s">
								<input type="hidden" name="opcn" id="opcn" value="imagenes">
								<input type="hidden" name="conference_id" id="conference_id" value="">
								<button type="submit" id="uploadFiles" class="btn btn-primary">Cargar</button>
								<button type="reset" id="resetFiles" style="display:none" class="btn btn-primary">Cargar</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- ================================================ -->
			<!-- END Fomulario Cargar Imagenes Presentación -->
			<!-- ================================================ -->

		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<!-- <script src="//static.opentok.com/v2/js/opentok.js"></script> -->
	<script src="js/foros_ventas.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
