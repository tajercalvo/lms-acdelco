<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['schedule_id'])){
    include('controllers/rep_asistencia.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_Asistencia.xls");
if(isset($_GET['schedule_id'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">CÓDIGO</th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">ALUMNO</th>
                <th data-hide="phone,tablet">IDENTIFICACIÓN</th>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">FECHA</th>
                <th data-hide="phone,tablet">FINAL</th>
                <th data-hide="phone,tablet">LUGAR</th>
                <th data-hide="phone,tablet">PROFESOR</th>
                <th data-hide="phone,tablet">ASISTENCIA</th>
                <th data-hide="phone,tablet">PUNTAJE</th>
                <th data-hide="phone,tablet">RESULTADO</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php 
                    $asistieron_tot = 0;
                    $noasistieron_tot = 0;
                    $aprobaron = 0;
                    $noaprobaron = 0;
                    $invitados = 0;
                    $puntaje = 0;
                    $aprobo = 0;

                if($cantidad_datos > 0){ 
                        foreach ($datosCursos_all as $iID => $data) { 
                            $estado_inv = "Invitado";
                            if($data['estado']==1){
                                $invitados++;
                                $estado_inv = "Invitado";
                            }elseif($data['estado']==2){
                                $asistieron_tot++;
                                $estado_inv = "Asitió";
                            }else{
                                $noasistieron_tot++;
                                $estado_inv = "No Asitió";
                            }
                            if(isset($data['resultados'][0]['score'])){
                                $puntaje = $data['resultados'][0]['score'];
                                $aprobo = $data['resultados'][0]['approval'];
                                if($aprobo=="SI"){
                                    $aprobaron++;
                                }else{
                                    $noaprobaron++;
                                }
                            }
                            ?>
                    <tr>
                        <td><?php echo(utf8_decode($data['newcode'])); ?></td>
                        <td><?php echo(utf8_decode($data['course'])); ?></td>
                        <td><?php echo(utf8_decode($data['first_name'].' '.$data['last_name'])); ?></td>
                        <td><?php echo($data['identification']); ?></td>
                        <td><?php echo(utf8_decode($data['headquarter'])); ?></td>
                        <td><?php echo(utf8_decode($data['start_date'])); ?></td>
                        <td><?php echo(utf8_decode($data['end_date'])); ?></td>
                        <td><?php echo(utf8_decode($data['living'])); ?></td>
                        <td><?php echo(utf8_decode($data['first_prof'].' '.$data['last_prof'])); ?></td>
                        <td><?php echo(utf8_decode($estado_inv)); ?></td>
                        <td><?php echo($puntaje); ?></td>
                        <td><?php echo(utf8_decode($aprobo)); ?></td>
                    </tr>
                <?php } } ?>
        </tbody>
    </table>
<?php } ?>