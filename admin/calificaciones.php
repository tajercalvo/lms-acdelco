<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include('src/seguridad.php');


if (!isset($_SESSION['idUsuario'])) {
  header("location: login");
}
?>
<?php include('controllers/calificaciones.php');
$location = 'education';
$locData = true;

// echo '<pre>';
// echo $cantidad_datos;
// print_r($datosInvitaciones);
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->
<!-- -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <?php include('src/tittle.php'); ?>
  <?php include('src/header.php'); ?>
</head>

<body class="document-body ">
  <!-- Main Container Fluid -->
  <div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
    <!-- Sidebar menu & content wrapper -->
    <div id="wrapper">
      <?php include('src/menu.php'); ?>
      <!-- Contenido proyectos -->
      <div id="content">
        <?php include('src/top_nav_bar.php'); ?>
        <ul class="breadcrumb">
          <li key="li_usted">Usted está en: </li>
          <li><a href="../admin/" class="glyphicons podium"><i></i> LUDUS LMS</a></li>
          <li class="divider"><i class="fa fa-caret-right"></i></li>
          <li><a href="../admin/calificaciones.php" key="Calificación de Estudiantes">Calificación de
              Estudiantes</a></li>
        </ul>
        <!-- inner -->
        <div class="innerLR">
          <!-- heading -->
          <div class="innerB">
            <h2 class="margin-none pull-left" key="Calificación de Estudiantes">Calificación de Estudiantes </h2>
            <!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
            <div class="clearfix"></div>
          </div>
          <!-- // END heading -->
          <!-- contenido interno -->
          <div class="widget widget-heading-simple widget-body-gray">
            <form action="calificaciones.php" method="post" id="formulario_search">
              <div class="widget-body">
                <div class="row">
                  <div class="col-md-6">
                    <h5 style="text-align: justify;" key="Aquí encuentra las opciones para listar y calificar de forma estructurada los cursos que dicta como maestro">
                      Aquí encuentra las opciones para listar y calificar de forma estructurada los cursos que dicta
                      como maestro.</h5></br>
                    <!--<h5><a href="rep_log_excel.php?fec_ini=<?php echo ($_POST['fecha_inicial']); ?>&fec_fin=<?php echo ($_POST['fecha_final']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>-->
                    <?php
                    $DescargarListadoPDF = "";

                    if (isset($_POST['schedule_id'])) {
                      $DescargarListadoPDF = "lista_asistencia.php?schedule_id=" . $_POST['schedule_id'] . "&opcn=descargarLista";
                    }
                    ?>
                    <!-- <a href="<?php echo $DescargarListadoPDF; ?>" target="_blank" class="btn" style="background:#2572a8; color:white"><i class="fa fa-fw fa-print"></i> Descargar Listado PDF</a> -->


                    <!-- <a href="lista_asistencia.php?schedule_id=<?php echo $_POST['schedule_id']; ?>&opcn=descargarLista" target="_blank" class="btn btn-primary lang" key="Descargar Listado PDF"><i class="fa fa-fw fa-print"></i> Descargar Listado PDF</a> -->
                    <a href="descarga_calificaciones.php?schedule_id=<?php echo $_POST['schedule_id']; ?>&" target="_blank" class="btn" style="background:#04a9bf; color:white"><i class="fa fa-fw fa-print"></i> Descargar Listado en excel</a>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-6">
                        <!-- Group -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" key="Fecha Inicial">Fecha Inicial:</label>
                          <div class="col-md-8">
                            <div class="input-group date" id="fecha_inicial_ctr">
                              <input class="form-control" type="text" id="fecha_inicial" name="fecha_inicial" placeholder="Fecha Inicial" <?php if (isset($_POST['fecha_inicial'])) { ?> value="<?php echo ($_POST['fecha_inicial']); ?>" <?php } ?> />
                              <span class="input-group-addon"><i class="fa fa-th"></i></span>
                            </div>
                          </div>
                        </div>
                        <!-- // Group END -->
                      </div>
                      <div class="col-md-6">
                        <!-- Group -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" key="Fecha Inicial">Fecha Final:</label>
                          <div class="col-md-8">
                            <div class="input-group date" id="fecha_final_ctr">
                              <input class="form-control" type="text" id="fecha_final" name="fecha_final" placeholder="Fecha Final" <?php if (isset($_POST['fecha_final'])) { ?> value="<?php echo ($_POST['fecha_final']); ?>" <?php } ?> />
                              <span class="input-group-addon"><i class="fa fa-th"></i></span>
                            </div>
                          </div>
                        </div>
                        <!-- // Group END -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-10">
                    <!-- Group -->
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;" key="Sesión a Calificar">Sesión a Calificar:</label>
                      <div class="col-md-10">
                        <select style="width: 100%;" id="schedule_id" name="schedule_id">
                          <?php if (isset($_POST['schedule_id'])) { ?>
                            <option value='<?php echo ($_POST['schedule_id']); ?>'>
                              <?php echo ($dato_sesion['newcode'] . " | " . $dato_sesion['course'] . " " . $dato_sesion['start_date'] . " " . $dato_sesion['living']); ?>
                            </option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <!-- // Group END -->
                  </div>
                 <!--  <?php 
                  //echo '<pre>';
                  //print_r($dato_sesion); ?>
                  die(); -->
                  <div class="col-md-2 center">
                    <button type="submit" class="btn btn-success" id="ConsultaRepLog" key="Consultar"><i class="fa fa-check-circle"></i> Consultar</button>
                  </div>
                  <input type="text"  id="calificacion_curso" name="calificacion_curso" style="display:none;"  >
                  <input type="text"  id="acti" name="acti" style="display:none;"  >
                  <input type="text"  id="type" name="type" style="display: none;" value="<?php echo $dato_sesion['type']; ?>" readonly>
                  <input type="text"  id="finally" name="finally" style="display:none;" >
                </div>
              </div>
            </form>
          </div>
          <?php if (isset($dato_sesion['type']) && ($dato_sesion['type'] == "ILT" || $dato_sesion['type'] == "VCT" || $dato_sesion['type'] == "WBT" || $dato_sesion['type'] == "OJT" || $dato_sesion['type'] == "MFR")) { ?>
            <?php if (isset($cantidad_datos)) { ?>
              <div class="widget widget-heading-simple widget-body-white" id="ResultadosSesiones">
                <!-- Widget heading -->
                <div class="widget-head">
                  <h4 class="heading glyphicons list"> <strong key="Información">Información:</strong>
                    <?php echo ($dato_sesion['course']); ?></h4>
                </div>
                <div class="widget-head">
                  <h4 class="heading glyphicons user"> <strong class="heading glyphicons list"> <strong key="Instructor">Instructor</strong>
                      <?= $dato_sesion['first_name'] . " " . $dato_sesion['last_name']; ?></h4>
                </div>
                <!-- // Widget heading END -->
                <div class="widget-body">
                  <!-- Total elements-->
                  <div class="form-inline separator bottom small">
                    <?php
                    if ($cantidad_datos > 0) {
                      $fecha = date_create(substr($dato_sesion['end_date'], 0, 10));
                      // date_add($fecha, date_interval_create_from_date_string('3 days'));
                      $fecMostrar = date_format($fecha, 'Y-m-d');
                      $fecha_max = substr($fecMostrar, 0, 4) . "" . substr($fecMostrar, 5, 2) . "" . substr($fecMostrar, 8, 2);
                    }
                    ?>
                    Alumnos a calificar: <?= $cantidad_datos ?>
                    <?php if ($cantidad_datos > 0) { ?>| <strong class='text-success' key="Fecha">Fecha: </strong><span class='text-success'><?php echo ($dato_sesion['start_date'] . "  -  " . $dato_sesion['end_date'] . "</span> | <strong class='text-inverse' >Ubicación: </strong><span class='text-inverse'>" . $dato_sesion['living']); ?></span>
                    | <span class='text-danger'><strong key="Fecha Máxima Registros Asistencia">Fecha Máxima
                        Registros Asistencia: <?= $fecMostrar ?></strong></span>
                    <?php if ($fecha_max < date("Ymd")) : ?>
                      <span class="text-danger"> <strong key="VENCIDA"> VENCIDA !</strong></span>
                    <?php endif; ?>
                  <?php } ?>
                  </div>
                  <!-- // Total elements END -->
                  <form id="formulario_data" method="post" autocomplete="off" style="display: none;">
                    <input type="hidden" name="course_id" id="course_id" value="<?php echo ($dato_sesion['course_id']); ?>">
                    <input type="hidden" name="schedule" id="schedule_id" value="<?php echo ($dato_sesion['schedule_id']); ?>">
                    <!-- Table elements-->
                    <table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
                      <!-- Table heading -->
                      <thead>
                        <tr>
                          <th data-hide="phone,tablet" key="ALUMNO">ALUMNO</th>
                          <th data-hide="phone,tablet" style="width: 15%;" key="EMPRESA">EMPRESA</th>
                          <th data-hide="phone,tablet" style="width: 15%;" key="IDENTIFICACION">IDENTIFICACION</th>
                          <th data-hide="phone,tablet" style="width: 10%;" key="CALIFICACIÓN">CALIFICACIÓN</th>
                          <th data-hide="phone,tablet" style="width: 5%;" key="RES">RES</th>
                          <th data-hide="phone,tablet" style="width: 10%;" key="ASISTIÓ">ASISTIÓ</th>
                        </tr>
                      </thead>
                      <!-- // Table heading END -->
                      <tbody>
                        <?php
                        $Ctrl_CalifNot = 0;
                        if ($cantidad_datos > 0) {
                          $var_ids = "";
                          // echo '<pre>';
                          // echo $cantidad_datos;
                          // print_r($datosInvitaciones);

                          foreach ($datosInvitaciones as $iID => $data) {
                            $var_ids .= $data['user_id'] . ",";
                            $calif = "0";
                            $asis = "SI";
                            $apro = "";
                            foreach ($data['Califica_Historico'] as $iID => $historico) {
                              $calif = $historico['score'];
                              if ($historico['status_id'] == "3") {
                                $asis = "NO";
                              }
                              if ($calif > 0) {
                                $Ctrl_CalifNot++;
                              }
                              $apro = $historico['approval'];
                            }
                        ?>
                            <tr>
                              <td><?php echo ($data['first_name'] . ' ' . $data['last_name']); ?>
                              </td>
                              <td><?php echo ($data['headquarter']); ?></td>
                              <td><?php echo ($data['identification']); ?></td>
                              <td>
                                <?php if ($dato_sesion['type'] === "ILT") { ?>
                                  <input class="form-control" id="Calif<?php echo $data['user_id']; ?>" name="Calif<?php echo $data['user_id']; ?>" type="number" placeholder="Puntaje obtenido" value="<?php echo ($calif); ?>" <?php if ($asis == "NO") { ?>disabled="disabled" <?php } else { ?>readonly="readonly" <?php } ?> />
                                  <input type="hidden" name="ArrIds" id="ArrIds" value="<?php echo ($var_ids); ?>">
                                  <input type="hidden" name="Inv<?php echo $data['user_id']; ?>" id="Inv<?php echo $data['user_id']; ?>" value="<?php echo ($data['invitation_id']); ?>" />
                                <?php  } else { ?>
                                  <input class="form-control" id="Calif<?php echo $data['user_id']; ?>" name="Calif<?php echo $data['user_id']; ?>" type="number" placeholder="Puntaje obtenido" value="<?php echo ($calif); ?>" <?php if ($asis == "NO") { ?>disabled="disabled" <?php } ?> onkeypress="return justNumbersCal(event,this);" min="0" max="100" maxlength="3" onkeyup="if(this.value>100){this.value='100';}else if(this.value<0){ this.value='0'; }" />
                                  <input type="hidden" name="Inv<?php echo $data['user_id']; ?>" id="Inv<?php echo $data['user_id']; ?>" value="<?php echo ($data['invitation_id']); ?>" />
                              </td>
                            <?php } ?>
                            <td>
                              <?php if ($dato_sesion['type'] === "ILT") { ?>

                                <?php if ($calif >= 80) { ?>
                                  <span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
                                <?php  } else{ ?>
                                  <span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
                                  <?php  } 
                              } else { ?>
                                <?php if ($apro == "SI") { ?>
                                  <span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
                                <?php } else if ($apro == "NO") { ?>
                                  <span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
                                <?php } } ?>
                            </td>
                            <td>
                              <div class="make-switch" data-on="success" data-off="default">
                                <input id="ChkCali<?php echo $data['user_id']; ?>" name="ChkCali<?php echo $data['user_id']; ?>" type="checkbox" value="SI" <?php if ($asis != "NO") { ?>checked="checked" <?php } ?> onchange="CheckAsigna('#ChkCali<?php echo $data['user_id']; ?>',<?php echo $data['user_id']; ?>);" />
                              </div>
                            </td>
                            </tr>
                          <?php } ?>
                          <input type="hidden" name="ArrIds" id="ArrIds" value="<?php echo ($var_ids); ?>">
                        <?php } ?>
                      </tbody>
                    </table>
                  </form>
                  <?php if ($cantidad_datos > 0) { ?>
                    <?php //if($fecha_max >= date("Ymd")){ 
                    ?>
                    <?php //if(Fcn_TieneRol('2')){ 
                    ?>
                    <div class="ajax-loading hide" id="loading_calificacion">
                      <i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
                    </div>
                    <button type="button" class="btn btn-success" id="btnAsistencia"><i class="fa fa-check-circle"></i>Enviar Asistencia</button>
                    <!--                     <button type="button" class="btn btn-success hidden-print" id="CalificarAsistencia" key="Enviar Resultados" disabled><i class="fa fa-check-circle"></i>Calificar y terminar curso</button> -->
                    <?php if ($Ctrl_CalifNot <= 0) { ?>
                      <button type="button" class="btn btn-success hidden-print" id="EnvResults" key="Enviar Resultados"><i class="fa fa-check-circle"></i> Enviar Resultados</button>
                    <?php } else { ?>
                      <p id="mensaje">Para modificar estas notas, debe ponerse en contacto con los coordinadores.</p>
                    <?php } ?>
                    <!-- // Table elements END -->
                    <?php //} 
                    ?>
                    <?php //} PA
                    ?>
                  <?php } ?>
                </div>
              </div>
            <?php } else { ?>
              <div class="widget widget-heading-simple widget-body-white" id="ResultadosSesiones">
                <!-- Widget heading -->
                <div class="widget-head">
                  <h4 class="heading glyphicons list" key="Información: Sin sesión seleccionada"><i></i>
                    Información: Sin sesión seleccionada</h4>
                </div>
                <!-- // Widget heading END -->
                <div class="widget-body">
                  <!-- Total elements-->
                  <div class="form-inline separator bottom small" key="Total de registros: 0 - No hay usuarios para calificar">
                    Total de registros: 0 - No hay usuarios para calificar
                  </div>
                  <!-- // Total elements END -->
                </div>
              </div>
            <?php } ?>
          <?php } else if (isset($dato_sesion['type']) && $dato_sesion['type'] == "OJT") { ?>
            <div class="widget widget-heading-simple widget-body-white" id="ResultadosSesiones">
              <!-- Widget heading -->
              <div class="widget-head">
                <h4 class="heading glyphicons list" key="Información"><i></i> Información:
                  <?php echo ($dato_sesion['course']); ?> </h4>
              </div>
              <!-- // Widget heading END -->
              <div class="widget-body">
                <!-- Total elements-->
                <div class="form-inline separator bottom small">
                  <?php
                  if ($cantidad_datos > 0) {
                    $fecha = date_create(substr($dato_sesion['end_date'], 0, 10));
                    date_add($fecha, date_interval_create_from_date_string('3 days'));
                    $fecMostrar = date_format($fecha, 'Y-m-d');
                    $fecha_max = substr($fecMostrar, 0, 4) . "" . substr($fecMostrar, 5, 2) . "" . substr($fecMostrar, 8, 2);
                  }
                  ?>
                  Alumnos Calificados: <?php echo ($cantidad_datos); ?>

                  <?php if ($cantidad_datos > 0) { ?>
                    <?php if ($fecha_max < date("Ymd")) { ?><span class="text-danger"> <strong key="VENCIDA">
                          VENCIDA !</strong></span><?php } ?>
                  <?php } ?>
                </div>
                <!-- // Total elements END -->
                <input type="hidden" name="course_id" id="course_id" value="<?php echo ($dato_sesion['course_id']); ?>">
                <!-- Table elements-->
                <table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
                  <!-- Table heading -->
                  <thead>
                    <tr>
                      <th data-hide="phone,tablet" key="ALUMNO">ALUMNO</th>
                      <th data-hide="phone,tablet" style="width: 15%;" key="EMPRESA">EMPRESA</th>
                      <th data-hide="phone,tablet" style="width: 15%;" key="IDENTIFICACION">IDENTIFICACION
                      </th>
                      <th data-hide="phone,tablet" style="width: 10%;" key="CALIFICACIÓN">CALIFICACIÓN</th>
                      <th data-hide="phone,tablet" style="width: 5%;" key="RES">RES</th>
                      <th data-hide="phone,tablet" style="width: 10%;" key="ASISTIÓ">ASISTIÓ</th>
                    </tr>
                  </thead>
                  <!-- // Table heading END -->
                  <tbody>
                    <?php
                    $Ctrl_CalifNot = 0;
                    if ($cantidad_datos > 0) {
                      $var_ids = "";
                      foreach ($datosInvitaciones as $iID => $data) {
                        $var_ids .= $data['user_id'] . ",";
                        $calif = "0";
                        $asis = "SI";
                        $apro = "";
                        foreach ($data['Califica_Historico'] as $iID => $historico) {
                          $calif = $historico['score'];
                          if ($historico['status_id'] == "3") {
                            $asis = "NO";
                          }
                          if ($calif > 0) {
                            $Ctrl_CalifNot++;
                          }
                          $apro = $historico['approval'];
                        }
                    ?>
                        <tr>
                          <td><?php echo ($data['first_name'] . ' ' . $data['last_name']); ?>
                          </td>
                          <td><?php echo ($data['headquarter']); ?></td>
                          <td><?php echo ($data['identification']); ?></td>
                          <td>
                            <input class="form-control" id="Calif<?php echo $data['user_id']; ?>" name="Calif<?php echo $data['user_id']; ?>" type="number" placeholder="Puntaje obtenido" value="<?php echo ($calif); ?>" <?php if ($asis == "NO") { ?>disabled="disabled" <?php } ?> onkeypress="return justNumbersCal(event,this);" min="0" max="100" maxlength="3" />
                            <input type="hidden" name="Inv<?php echo $data['user_id']; ?>" id="Inv<?php echo $data['user_id']; ?>" value="<?php echo ($data['invitation_id']); ?>" />
                          </td>
                          <td>
                            <?php if ($apro == "SI") { ?>
                              <span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
                            <?php } else if ($apro == "NO") { ?>
                              <span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
                            <?php } ?>
                          </td>
                          <td>
                            <div class="make-switch" data-on="success" data-off="default">
                              <input id="ChkCali<?php echo $data['user_id']; ?>" name="ChkCali<?php echo $data['user_id']; ?>" type="checkbox" value="SI" <?php if ($asis != "NO") { ?>checked="checked" <?php } ?> onchange="CheckAsigna('#ChkCali<?php echo $data['user_id']; ?>',<?php echo $data['user_id']; ?>);" />
                            </div>
                          </td>
                        </tr>
                      <?php } ?>
                      <input type="hidden" name="ArrIds" id="ArrIds" value="<?php echo ($var_ids); ?>">
                    <?php } ?>
                  </tbody>
                </table>
                <div class="separator bottom"></div>
                <form action="calificaciones.php?schedule_id=<?php echo $_POST['schedule_id']; ?>&fecha_inicial=<?php echo $_POST['fecha_inicial']; ?>&fecha_final=<?php echo $_POST['fecha_final']; ?>" id="formulario_CalifOJT" method="post" autocomplete="off">
                  <table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
                    <thead>
                      <tr>
                        <th data-hide="phone,tablet" style="width: 40%;" key="ALUMNO">ALUMNO</th>
                        <th data-hide="phone,tablet" style="width: 40%;" key="OBSERVACIONES">OBSERVACIONES
                        </th>
                        <th data-hide="phone,tablet" style="width: 10%;" key="RESULTADO">RESULTADO</th>
                        <th data-hide="phone,tablet" style="width: 10%;" key="OPERACIÓN">OPERACIÓN</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <select style="width: 100%;" id="userOJT_id" name="userOJT_id">
                            <?php foreach ($UsuariosSitios as $key => $dataUsr) { ?>
                              <option value='<?php echo ($dataUsr['user_id']); ?>'>
                                <?php echo ($dataUsr['identification'] . " | " . $dataUsr['first_name'] . " " . $dataUsr['last_name'] . " | " . $dataUsr['headquarter']); ?>
                              </option>
                            <?php } ?>
                          </select>
                        </td>
                        <td>
                          <textarea id="ObservOJT" name="ObservOJT" class="col-md-12 form-control" rows="5"></textarea>
                        </td>
                        <td>
                          <input type="hidden" id="course_idOJT" name="course_idOJT" value="<?php echo ($dato_sesion['course_id']); ?>" />
                          <input type="hidden" id="Schedule_idOJT" name="Schedule_idOJT" value="<?php echo $_POST['schedule_id']; ?>" />
                          <input class="form-control" id="CalifOJT" name="CalifOJT" type="text" placeholder="Puntaje obtenido" value="" onkeypress="return justNumbers(event);" />
                        </td>
                        <td>
                          <button type="submit" class="btn btn-success" id="CalifOJT_Btn" key="Calificar"><i class="fa fa-check-circle"></i> Calificar</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
          <?php } ?>

          <!-- Nuevo ROW-->
          <div class="row row-app">

          </div>
          <!-- // END Nuevo ROW-->
          <div class="separator bottom"></div>
          <div class="separator bottom"></div>
          <!-- // END contenido interno -->
        </div>
        <!-- // END inner -->
      </div>
      <!-- // END Contenido proyectos -->
    </div>
    <div class="clearfix"></div>
    <!-- // Sidebar menu & content wrapper END -->
    <?php include('src/footer.php'); ?>
  </div>
  <!-- // Main Container Fluid END -->
  <?php include('src/global.php'); ?>
  <script type="module" src="js/calificaciones.js"></script>
</body>

</html>