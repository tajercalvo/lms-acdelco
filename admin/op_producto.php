<?php include('src/seguridad.php');
include('controllers/producto.php');
$location = 'producto';
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/producto.css">
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons list"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/producto.php" >Producto</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<?php if(isset($_GET['function'])&&($_GET['function']=='editar')) { ?>
						<h2 class="margin-none pull-left">Editar Producto</h2>
						<?php } else { ?>
						<h2 class="margin-none pull-left">Agregar Producto</h2>
						<?php } ?>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
										<h5><a href="producto.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if(isset($_GET['function'])&&($_GET['function']=='editar')) { ?>
							<h4 class="heading glyphicons list"><i></i> Editar Producto</h4> 
							<?php } else { ?>
							<h4 class="heading glyphicons list"><i></i> Agregar Producto</h4>
							<?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">
							<!-- // Pestañas Botones -->
							<div class="row" id="botones-principales">
								<div class="col-md-3"></div>
								<?php if(isset($_GET['function'])&&($_GET['function']=='editar')) { ?>
								<div class="col-md-3 boton-pestana">
						 			<strong class="centrar"><h5 data-mostrar="row-segmento">EDITAR SEGMENTO</h5></strong>
								</div>
								<div class="col-md-3 boton-pestana">
									<strong class="centrar"><h5 data-mostrar="row-producto">EDITAR PRODUCTO</h5></strong>
								</div>
								<?php } else { ?>
						 		<div class="col-md-3 boton-pestana">
						 			<strong class="centrar"><h5 data-mostrar="row-segmento">AGREGAR SEGMENTO</h5></strong>
								</div>
								<div class="col-md-3 boton-pestana">
									<strong class="centrar"><h5 data-mostrar="row-producto">AGREGAR PRODUCTO</h5></strong>
								</div>
								<?php } ?>
								<div class="col-md-3"></div>
							</div>
							<!-- // Pestañas Botones END -->

							<!-- // Contenido Nuevo Segmento -->
							<div class="contenido" id="row-segmento">
								<form id="formulario_segmento" autocomplete="off">
									<br><br><br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Nombre del Segmento -->
										<div class="col-md-3"></div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="segmento" style="padding-top:8px;">Segmento de Vehiculos:</label>
												<div class="col-md-8">
													<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
													<input class="form-control" id="segmento" name="segmento" type="text" placeholder="Nombre del segmento de vehiculos" value="<?php echo($datos_segmento['product_segment']); ?>">
													<?php } else{ ?>
													<input class="form-control" id="segmento" name="segmento" type="text" placeholder="Nombre del segmento de vehiculos">
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="col-md-3"></div>
										<!-- // Nombre del Segmento END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Categoria del Segmento -->
										<div class="col-md-3"></div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="categoria" style="padding-top:8px;">Categoria del segmento:</label>
												<div class="col-md-8">
													<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
													<select style="width: 100%;" id="categoria" name="categoria">
														<?php foreach ($Categoria_datos  as $key => $Data_categorias) { ?>
															<option value="<?php echo($Data_categorias['product_cat_id']); ?>" <?php if( $datos_segmento['product_cat_id'] == $Data_categorias['product_cat_id'] ){ echo "selected"; } ?> > <?php echo($Data_categorias['product_cat']); ?></option>
														<?php } ?>
													</select>
													<?php } else{ ?>
													<select style="width: 100%;" id="categoria" name="categoria">
														<?php foreach ($Categoria_datos  as $key => $Data_categorias) { ?>
															<option value="<?php echo($Data_categorias['product_cat_id']); ?>"><?php echo($Data_categorias['product_cat']); ?></option>
														<?php } ?>
													</select>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="col-md-3"></div>
										<!-- // Categoria del Segmento END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Imagen Mini del Segmento -->
										<div class="col-md-3"></div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="imagen_mini2" style="padding-top:8px;">Imagen de perfil:</label>
												<div class="col-md-4">
													<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
													<label for="imagen_mini" class="control-label" style="padding-top:8px;">
														<img id="imagen_mini1" src="../assets/products/images/mini/<?php echo($datos_segmento['image_mini_profile']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
													</label>
                                        			<input type="file" id="imagen_mini" name="imagen_mini" style="display:none">
                                        			<?php } else{ ?>
													<label for="imagen_mini" class="control-label" style="padding-top:8px;">
														<img id="imagen_mini1" src="../assets/products/images/mini/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
													</label>
                                        			<input type="file" id="imagen_mini" name="imagen_mini" style="display:none">
                                        			<?php } ?>
												</div>
												<div class="col-md-4"></div>
											</div>
										</div>
										<div class="col-md-3"></div>
										<!-- // Imagen Mini del Segmento END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<div class="col-md-5"></div>
										<div class="col-md-2">
											<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
											<input type="hidden" name="edicion" id="edicion" value="<?php echo($datos_segmento['product_segment_id']); ?>">
											<button type="submit" class="btn btn-success" id="editarSegmento"><i class="fa fa-check-circle"></i> Editar Segmento</button>
											<?php } else{ ?>
											<input type="hidden" name="edicion" id="edicion" value="0">
											<button type="submit" class="btn btn-success" id="crearSegmento"><i class="fa fa-check-circle"></i> Crear Segmento</button>
											<?php } ?>
										</div>
										<div class="col-md-5"></div>
									</div>
									<!-- // Row END -->
								</form>

							</div>
							<!-- // Contenido Nuevo Segmento END -->

							<!-- // Contenido Nuevo Producto -->
							<div class="contenido" style="display: none" id="row-producto">
								<form id="formulario_producto" method="post" autocomplete="off">
									<br><br><br>
									<!-- Contenido Datos Basicos de Producto -->
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h3 class="heading glyphicons book"><i></i> Datos Basicos</h3>
										</div>
										<div class="widget-body">
											<!-- Row -->
											<div class="row innerLR">
												<!-- Nombre del Producto -->
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-3 control-label" for="vehiculo" style="padding-top:8px;">Vehiculo:</label>
														<div class="col-md-9">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<input class="form-control" id="vehiculo" name="vehiculo" type="text" placeholder="Nombre del vehiculo" value="<?php echo($datos_producto['product']); ?>" >
															<?php } else{ ?>
															<input class="form-control" id="vehiculo" name="vehiculo" type="text" placeholder="Nombre del vehiculo">
															<?php } ?>
														</div>
													</div>
												</div>
												<!-- // Nombre del Producto END -->
												<!-- Categoria del Producto -->
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-3 control-label" for="segmento_sel" style="padding-top:8px;">Segmento:</label>
														<div class="col-md-9">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<select style="width: 100%;" id="segmento_sel" name="segmento_sel">
																<?php foreach ($Productos_Categoria_datos  as $key => $Data_segmento) { ?>
																	<option value="<?php echo($Data_segmento['product_segment_id']); ?>" <?php if( $datos_producto['product_segment_id'] == $Data_segmento['product_segment_id'] ){ echo "selected"; } ?> > <?php echo($Data_segmento['product_segment']); ?></option>
																<?php } ?>
															</select>
															<?php } else{ ?>
															<select style="width: 100%;" id="segmento_sel" name="segmento_sel">
																<?php foreach ($Productos_Categoria_datos  as $key => $Data_segmento) { ?>
																	<option value="<?php echo($Data_segmento['product_segment_id']); ?>" ><?php echo($Data_segmento['product_segment']); ?></option>
																<?php } ?>
															</select>
															<?php } ?>
														</div>
													</div>
												</div>
												<!-- // Categoria del Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
										</div>
									</div>
									<!-- Contenido Datos Basicos de Producto END -->
									<!-- Row Crear Producto -->
									<div class="row innerLR">
										<div class="col-md-5"></div>
										<div class="col-md-2">
											<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
											<button type="submit" class="btn btn-success" id="editarProducto"><i class="fa fa-check-circle"></i> Editar Producto</button>
											<?php } else{ ?>
											<button type="submit" class="btn btn-success" id="crearProducto"><i class="fa fa-check-circle"></i> Crear Producto</button>
											<?php } ?>
										</div>
										<div class="col-md-5"></div>
									</div>
									<!-- // Row Crear Producto END -->
								</form>
									<br>
									<br>
								<form id="formulario_imagenes" method="post" autocomplete="off">
									<!-- Contenido Imagenes de Producto -->
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h3 class="heading glyphicons camera"><i></i> Imagenes</h3>
										</div>
										<div class="widget-body">
											<!-- Row -->
											<div class="row innerLR">
												<!-- Imagen de Perfil Producto -->
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-4 control-label" for="image_profile2" style="padding-top:8px;">Imágen Principal     ( Imágen (1024X678) ):</label>
														<div class="col-md-4">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<label for="image_profile" class="control-label" style="padding-top:8px;">
																<img id="image_profile1" src="../assets/products/images/profile/<?php echo($datos_producto['image_profile']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_profile" name="image_profile" style="display:none">
		                                        			<?php } else{ ?>
															<label for="image_profile" class="control-label" style="padding-top:8px;">
																<img id="image_profile1" src="../assets/products/images/profile/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_profile" name="image_profile" style="display:none">
		                                        			<?php } ?>
														</div>
														<div class="col-md-4"></div>
													</div>
												</div>
												<!-- // Imagen de Perfil Producto END -->
												<div class="col-md-6"></div>
											</div>
											<!-- // Row END -->
											<br>
											<br>
											<!-- Row -->
											<div class="row innerLR">
												<!-- Caracteristicas de Producto -->
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<div class="form-group">
														<label class="col-md-2 control-label" for="caracteristicas" style="padding-top:8px;">Características     ( Imágen (1024X678) ):</label>
														<!-- Caracteristicas de Exterior Producto -->
														<div class="col-md-2">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<label for="image_exterior" class="control-label" style="padding-top:8px;text-align: center;">Exterior
																<img id="image_exterior1" src="../assets/products/images/characteristics/<?php echo($datos_producto['outside']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_exterior" name="image_exterior" style="display:none">
															<?php } else{ ?>
															<label for="image_exterior" class="control-label" style="padding-top:8px;text-align: center;">Exterior
																<img id="image_exterior1" src="../assets/products/images/characteristics/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_exterior" name="image_exterior" style="display:none">
		                                        			<?php } ?>
														</div>
														<div class="col-md-1"></div>
														<!-- Caracteristicas de Exterior Producto END -->
														<!-- Caracteristicas de Interior Producto -->
														<div class="col-md-2">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<label for="image_interior" class="control-label" style="padding-top:8px;text-align: center;">Interior
																<img id="image_interior1" src="../assets/products/images/characteristics/<?php echo($datos_producto['inside']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_interior" name="image_interior" style="display:none">
															<?php } else{ ?>
															<label for="image_interior" class="control-label" style="padding-top:8px;text-align: center;">Interior
																<img id="image_interior1" src="../assets/products/images/characteristics/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_interior" name="image_interior" style="display:none">
		                                        			<?php } ?>
														</div>
														<div class="col-md-1"></div>
														<!-- Caracteristicas de Interior Producto END -->
														<!-- Caracteristicas de Desempeño Producto -->
														<div class="col-md-2">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<label for="image_desempeno" class="control-label" style="padding-top:8px;text-align: center;">Desempeño
																<img id="image_desempeno1" src="../assets/products/images/characteristics/<?php echo($datos_producto['performance']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_desempeno" name="image_desempeno" style="display:none">
															<?php } else{ ?>
															<label for="image_desempeno" class="control-label" style="padding-top:8px;text-align: center;">Desempeño
																<img id="image_desempeno1" src="../assets/products/images/characteristics/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_desempeno" name="image_desempeno" style="display:none">
		                                        			<?php } ?>
														</div>
														<!-- Caracteristicas de Desempeño Producto END -->
													</div>
												</div>
												<!-- // Caracteristicas de Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
											<!-- Row -->
											<div class="row innerLR">
												<!-- Caracteristicas de Producto -->
												<div class="col-md-1"></div>
												<div class="col-md-10">
													<div class="form-group">
														<label class="col-md-2 control-label" style="padding-top:8px;"></label>
														<!-- Caracteristicas de Tecnología Producto -->
														<div class="col-md-2">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<label for="image_tecnologia" class="control-label" style="padding-top:8px;text-align: center;">Tecnología
																<img id="image_tecnologia1" src="../assets/products/images/characteristics/<?php echo($datos_producto['technology']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_tecnologia" name="image_tecnologia" style="display:none">
															<?php } else{ ?>
															<label for="image_tecnologia" class="control-label" style="padding-top:8px;text-align: center;">Tecnología
																<img id="image_tecnologia1" src="../assets/products/images/characteristics/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_tecnologia" name="image_tecnologia" style="display:none">
		                                        			<?php } ?>
														</div>
														<div class="col-md-1"></div>
														<!-- Caracteristicas de Tecnología Producto END -->
														<!-- Caracteristicas de Seguridad Producto -->
														<div class="col-md-2">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<label for="image_seguridad" class="control-label" style="padding-top:8px;text-align: center;">Seguridad
																<img id="image_seguridad1" src="../assets/products/images/characteristics/<?php echo($datos_producto['security']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_seguridad" name="image_seguridad" style="display:none">
															<?php } else{ ?>
															<label for="image_seguridad" class="control-label" style="padding-top:8px;text-align: center;">Seguridad
																<img id="image_seguridad1" src="../assets/products/images/characteristics/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_seguridad" name="image_seguridad" style="display:none">
		                                        			<?php } ?>
														</div>
														<div class="col-md-3"></div>
														<!-- Caracteristicas de Seguridad Producto END -->
													</div>
												</div>
												<!-- // Caracteristicas de Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
										</div>
									</div>
									<!-- Contenido Imagenes de Producto END -->
									<!-- Row Crear Imagenes -->
									<div class="row innerLR">
										<div class="col-md-5"></div>
										<div class="col-md-2">
											<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
											<button type="submit" class="btn btn-success" id="editarImagenes"><i class="fa fa-check-circle"></i> Editar Imagenes</button>
											<?php } else{ ?>
											<button type="submit" class="btn btn-success" id="crearImagenes"><i class="fa fa-check-circle"></i> Cargar Imagenes</button>
											<?php } ?>
										</div>
										<div class="col-md-5"></div>
									</div>
									<!-- // Row Crear Imagenes END -->
								</form>
									<br>
									<br>
								<form id="formulario_archivos" method="post" autocomplete="off">
									<!-- Contenido Archivos de Producto -->
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h3 class="heading glyphicons file"><i></i> Archivos</h3>
										</div>
										<div class="widget-body">
											<!-- Row -->
											<div class="row innerLR">
												<div class="col-md-1"></div>
												<!-- Ficha Tecnica Producto -->
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-3 control-label" for="ficha" style="padding-top:8px;">Ficha Tecnica:</label>
														<div class="col-md-9">
															<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																<span class="btn btn-default btn-file">
																	<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
																	<span class="fileupload-exists">Cambiar Archivo</span>
																	<input type="file" class="margin-none" id="ficha" name="ficha"/>
																</span>
																<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { if( $datos_producto['data_sheet'] != "default.pdf" && $datos_producto['data_sheet'] != ""  ) { ?>
																<span class="label label-success"><i class="fa fa-check-circle">
																</i></span>	
																<?php } } ?>
																<span class="fileupload-preview"></span>
																<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
															</div>
														</div>
													</div>
												</div>
												<!-- // Ficha Tecnica Producto END -->
												<!-- Accesorios Producto -->
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-3 control-label" for="accesorio" style="padding-top:8px;">Accesorios:</label>
														<div class="col-md-9">
															<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																<span class="btn btn-default btn-file">
																	<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
																	<span class="fileupload-exists">Cambiar Archivo</span>
																	<input type="file" class="margin-none" id="accesorio" name="accesorio"/>
																</span>
																<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { if( $datos_producto['accessories'] != "default.pdf" && $datos_producto['accessories'] != ""  ) { ?>
																<span class="label label-success"><i class="fa fa-check-circle">
																</i></span>	
																<?php } } ?>
																<span class="fileupload-preview"></span>
																<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
															</div>
														</div>
													</div>
												</div>
												<!-- // Accesorios Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
											<br>
											<br>
											<!-- Row -->
											<div class="row innerLR">
												<div class="col-md-1"></div>
												<!-- Versiones Producto -->
												<div class="col-md-5">
													<div class="form-group" >
														<label class="col-md-3 control-label" for="version" style="padding-top:8px;">Versiones:</label>
														<div class="col-md-9">
															<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																<span class="btn btn-default btn-file">
																	<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
																	<span class="fileupload-exists">Cambiar Archivo</span>
																	<input type="file" class="margin-none" id="version" name="version" />
																</span>
																<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { if( $datos_producto['versions'] != "default.pdf" && $datos_producto['versions'] != ""  ) { ?>
																<span class="label label-success"><i class="fa fa-check-circle">
																</i></span>	
																<?php } } ?>
																<span class="fileupload-preview"></span>
																<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
															</div>
														</div>
													</div>
												</div>
												<!-- // Versiones Producto END -->
												<!-- Competencia Producto -->
												<div class="col-md-5">
													<div class="form-group" >
														<label class="col-md-3 control-label" for="competencia" style="padding-top:8px;">Competencia:</label>
														<div class="col-md-9">
															<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																<span class="btn btn-default btn-file">
																	<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
																	<span class="fileupload-exists">Cambiar Archivo</span>
																	<input type="file" class="margin-none" id="competencia" name="competencia" />
																</span>
																<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { if( $datos_producto['competitions'] != "default.pdf" && $datos_producto['competitions'] != ""  ) { ?>
																<span class="label label-success"><i class="fa fa-check-circle">
																</i></span>	
																<?php } } ?>
																<span class="fileupload-preview"></span>
																<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
															</div>
														</div>
													</div>
												</div>
												<!-- // Competencia Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
										</div>
									</div>
									<!-- Contenido Archivos de Producto END -->
									<!-- Row Crear Archivos -->
									<div class="row innerLR">
										<div class="col-md-5"></div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-success" id="crearArchivos"><i class="fa fa-check-circle"></i> Cargar Archivos PDF</button>
										</div>
										<div class="col-md-5"></div>
									</div>
									<!-- // Row Crear Archivos END -->
								</form>
									<br>
									<br>
								<form id="formulario_videos" method="post" autocomplete="off">
									<!-- Contenido Videos de Producto-->
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h3 class="heading glyphicons film"><i></i> Videos</h3>
										</div>
										<div class="widget-body">
											<!-- Row -->
											<div class="row innerLR">
												<div class="col-md-1"></div>
												<!-- Video de Perfil Producto -->
												<div class="col-md-5">
													<div class="form-group" >
														<label class="col-md-3 control-label" for="video_profile" style="padding-top:8px;">Video Principal:</label>
														<div class="col-md-9">
															<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																<span class="btn btn-default btn-file">
																	<span class="fileupload-new">Seleccionar Video (mp4)</span>
																	<span class="fileupload-exists">Cambiar Video</span>
																	<input type="file" class="margin-none" id="video_profile" name="video_profile" />
																</span>
																<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { if( $datos_producto['video_profile'] != "default.mp4" && $datos_producto['video_profile'] != ""  ) { ?>
																<span class="label label-success"><i class="fa fa-check-circle">
																</i></span>	
																<?php } } ?>
																<span class="fileupload-preview"></span>
																<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
															</div>
														</div>
													</div>
												</div>
												<!-- // Video de Perfil Producto END -->
												<!-- Video Recorrido 360 Producto -->
												<div class="col-md-5">
													<div class="form-group" >
														<label class="col-md-3 control-label" for="video_recorrido" style="padding-top:8px;">Video Recorrido 360:</label>
														<div class="col-md-9">
															<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																<span class="btn btn-default btn-file">
																	<span class="fileupload-new">Seleccionar Video (mp4)</span>
																	<span class="fileupload-exists">Cambiar Video</span>
																	<input type="file" class="margin-none" id="video_recorrido" name="video_recorrido" />
																</span>
																<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { if( $datos_producto['video_tour'] != "default.mp4" && $datos_producto['video_tour'] != ""  ) { ?>
																<span class="label label-success"><i class="fa fa-check-circle">
																</i></span>	
																<?php } } ?>
																<span class="fileupload-preview"></span>
																<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
															</div>
														</div>
													</div>
												</div>
												<!-- // Video Recorrido 360 Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
										</div>
									</div>
									<!-- Contenido Videos de Producto END -->
									<!-- Row Crear Videos -->
									<div class="row innerLR">
										<div class="col-md-5"></div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-success" id="crearVideos"><i class="fa fa-check-circle"></i> Cargar Videos</button>
										</div>
										<div class="col-md-5"></div>
									</div>
									<!-- // Row Crear Videos END -->
								</form>
									<br>
									<br>
								<form id="formulario_links" method="post" autocomplete="off">
									<!-- Contenido Links de Producto-->
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h3 class="heading glyphicons link"><i></i> Links</h3>
										</div>
										<div class="widget-body">
											<!-- Row -->
											<div class="row innerLR">
												<!-- Imagen de Material Apoyo Producto -->
												<div class="col-md-1"></div>
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-4 control-label" for="img_link_recorrido2" style="padding-top:8px;">Imágen para Link Material de Apoyo (Recorrido 360):</label>
														<div class="col-md-4">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<label for="img_link_recorrido" class="control-label" style="padding-top:8px;">
																<img id="img_link_recorrido1" src="../assets/products/images/links/<?php echo($datos_producto['image_tour']); ?>" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="img_link_recorrido" name="img_link_recorrido" style="display:none">
		                                        			<?php } else{ ?>
															<label for="img_link_recorrido" class="control-label" style="padding-top:8px;">
																<img id="img_link_recorrido1" src="../assets/products/images/links/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="img_link_recorrido" name="img_link_recorrido" style="display:none">
		                                        			<?php } ?>
														</div>
														<div class="col-md-4"></div>
													</div>
												</div>
												<!-- // Imagen de Material Apoyo Producto END -->
												<!-- Link VCT Producto -->
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-3 control-label" for="vct" style="padding-top:8px;">Link mejor VCT del Producto:</label>
														<div class="col-md-9">
															<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
															<input class="form-control" id="vct" name="vct" type="text" placeholder="https://www.gmacademy.co" value="<?php echo($datos_producto['vct']); ?>" >
															<?php } else{ ?>
															<input class="form-control" id="vct" name="vct" type="text" placeholder="https://www.gmacademy.co">
															<?php } ?>
														</div>
													</div>
												</div>
												<!-- // Link VCT Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
										</div>
									</div>
									<!-- Contenido Links de Producto END -->
									<!-- // Row Crear Links -->
									<div class="row innerLR">
										<div class="col-md-5"></div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-success" id="crearLinks"><i class="fa fa-check-circle"></i> Cargar Links</button>
										</div>
										<div class="col-md-5"></div>
									</div>
									<!-- // Row Crear Links END -->
								</form>
									<br>
									<br>
								<form id="formulario_audios" method="post" autocomplete="off">
									<!-- Contenido Audios de Producto-->
									<div class="widget widget-heading-simple widget-body-white">
										<div class="widget-head">
											<h3 class="heading glyphicons albums"><i></i> Audios</h3>
										</div>
										<div class="widget-body">
											<!-- Row -->
											<div class="row innerLR">
												<div class="col-md-1"></div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="col-md-4 control-label" for="biblioteca" style="padding-top:8px;">Tipo de AudioLibro:</label>
														<div class="col-md-8">
															<select style="width: 100%;" id="biblioteca" name="biblioteca">
																<option value="-1" >SELECCIONA UN TIPO DE AUDIOLIBRO</option>
																<option value="0" >NUEVO AUDIOLIBRO</option>
																<option value="1" >AUDIOLIBRO DE BIBLIOTECA</option>
															</select>
														</div>
													</div>
												</div>
												<div class="col-md-4"></div>
												<div class="col-md-1"></div>
											</div>
											<!-- // Row END -->
											<br>
											<!-- Row Nuevo AudioLibro -->
											<div class="row innerLR" id="nuevoAudio">
												<div class="col-md-1"></div>
												<!-- Imagen Audio de Producto -->
												<div class="col-md-5">
													<div class="form-group">
														<label class="col-md-4 control-label" for="image_audio2" style="padding-top:8px;">Imágen para AudioLibros ( Imágen (1024X678) ):</label>
														<div class="col-md-4">
															<label for="image_audio" class="control-label" style="padding-top:8px;">
																<img id="image_audio1" src="../assets/products/images/upload_image.png" width="500" class="img-responsive" alt="image" style="margin: auto">
															</label>
		                                        			<input type="file" id="image_audio" name="image_audio" style="display:none">
														</div>
														<div class="col-md-4"></div>
													</div>
												</div>
												<!-- // Imagen Audio de Producto END -->
												<!-- Audio de Producto -->
												<div class="col-md-5">
													<div class="row">
														<div class="form-group" >
															<label class="col-md-3 control-label" for="audioLibro" style="padding-top:8px;">AudioLibro:</label>
															<div class="col-md-9">
																<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
																	<span class="btn btn-default btn-file">
																		<span class="fileupload-new">Seleccionar Audio (mp3)</span>
																		<span class="fileupload-exists">Cambiar Audio</span>
																		<input type="file" class="margin-none" id="audioLibro" name="audioLibro" />
																	</span>
																	<span class="fileupload-preview"></span>
																	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group" >
															<label class="col-md-3 control-label" for="descripcion" style="padding-top:8px;">Descripción:</label>
															<div class="col-md-9">
																<textarea class="form-control" name="descripcion" id="descripcion" rows="2" cols="20" placeholder="Ingrese una breve descripciòn"></textarea>
															</div>
														</div>
													</div>
												</div>
												<!-- // Audio de Producto END -->
												<div class="col-md-1"></div>
											</div>
											<!-- // Row Nuevo AudioLibro END -->

											<!-- Row Relacion Biblioteca -->
											<div class="row innerLR" id="nuevaBiblioteca">
												<div class="col-md-1"></div>
												<!-- Biblioteca de Producto -->
												<div class="col-md-8">
														<div class="form-group" >
															<label class="col-md-3 control-label" for="library_id" style="padding-top:8px;">Relación con Biblioteca:</label>
															<div class="col-md-9">
																<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>

																<select multiple="multiple" style="width: 100%;" id="library_id" name="library_id[]">

																	<?php foreach ($Producto_biblioteca as $key => $Data_library_id) { 
																		$selected = ''; ?>
																		<?php if( isset($datos_producto['Detalle_Producto']) ){ 
																			foreach ($datos_producto['Detalle_Producto'] as $key => $value) {
																				foreach ($value as $key => $value2) {
																					if ($Data_library_id ['library_id'] == $value2['library_id']) { $selected = 'selected'; } }
																			} } ?>
																		<option value="<?php echo $Data_library_id['library_id']; ?>" <?php echo $selected ?> > <?php echo $Data_library_id['name']; ?> </option>
																	<?php } ?>

																</select>

																<?php } else{ ?>

																<select multiple="multiple" style="width: 100%;" id="library_id" name="library_id[]">
																	<?php foreach ($Producto_biblioteca as $key => $Data_library_id) { ?>
																	<option value="<?php echo $Data_library_id['library_id']; ?>"><?php echo $Data_library_id['name']; ?></option>
																	<?php } ?>
																</select>

																<?php } ?>

															</div>
														</div>
												</div>
												<!-- // Row Biblioteca de Producto END -->

												<div class="col-md-3"></div>
											</div>
											<!-- // Row Relacion Biblioteca END -->

										</div>
									</div>
									<!-- Contenido Audios de Producto END -->
									<!-- Row Crear Audios -->
									<div class="row innerLR">
										<div class="col-md-5"></div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-success" id="crearAudios"><i class="fa fa-check-circle"></i> Cargar Audios</button>
										</div>
										<div class="col-md-5"></div>
									</div>
									<!-- // Row Crear Audios END -->
								</form>

									<div class="separator"></div>

									<!-- Form actions -->
									<div class="form-actions">
										<?php if(isset($_GET['function'])&&($_GET['function']=='editar')&&isset($_GET['id'])) { ?>
										<input type="hidden" name="opcion" id="opcion" value="1">
										<input type="hidden" name="idproducto" id="idproducto" value="<?php echo($datos_producto['product_id']); ?>"> 
										<?php } else{ ?>
										<input type="hidden" name="opcion" id="opcion" value="0">
										<input type="hidden" name="idproducto" id="idproducto" value="0"> 
										<?php } ?>
									</div>
									<!-- // Form actions END -->
								</form>
							</div>
							<!-- // Contenido Nuevo Producto END -->
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->

			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/op_producto.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
