<?php
include('src/seguridad.php');
include('controllers/newsletter.php');
if(!isset($_SESSION['idUsuario'])){
	header("location: login");
}
?>
<?php
$detail_msg = "SI";
$location = 'correos';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
		<?php include('src/menu.php'); ?>
		<!-- Content -->
		<div id="content">
		<?php include('src/top_nav_bar.php'); ?>
		<!-- Camino de Hormigas -->
		<ul class="breadcrumb">
			<li>Estás aquí</li>
			<li><a href="index.php" class="glyphicons message_flag"><i></i> LUDUS LMS</a></li>
			<li class="divider"><i class="fa fa-caret-right"></i></li>
			<li><a href="../admin/">Correos</a></li>
		</ul>
		<!-- Camino de Hormigas -->
		<!-- Mensajes -->
		<div class="widget widget-messages widget-heading-simple widget-body-white">
			<div class="widget-body padding-none">

				<div class="row row-merge borders">
					<div class="ajax-loading hide" id="Loading_Msg_Usr">
						<i class="fa fa-refresh text-primary-light fa fa-spin fa fa-4x"></i>
					</div>
					<div class="col-md-3 listWrapper" id="ListadoRemitentes" style="min-height: 765px !important;">
						<div class="innerAll">
							<div class="input-group">
									<button type="button" id="redactar" class="btn btn-block btn-primary btn-icon glyphicons message_new" data-toggle="modal" style=""><i></i>Redactar</button>
							</div>
						</div>
						<span class="results">Mensajes enviados: <?php echo(count($correosEnviados)); ?><i class="fa fa-circle-arrow-down"></i></span>
						<ul class="list unstyled">
							<?php foreach ($correosEnviados as $key => $corrEnv) { ?>
								<li id="<?php echo($corrEnv['email_id']); ?>">
									<div class="media innerAll" id="<?php echo($corrEnv['email_id']); ?>" onclick="mostrar(this.id);">
										<div class="media-object pull-left thumb hidden-phone"><img alt="Image" src="../assets/images/usuarios/<?php echo $corrEnv['img_pro']; ?>" style="width: 50px; height: 50px;" /></div>
										<div class="media-body">
											<span class="usr_list strong"><?php echo $corrEnv['first_name'].' '.$corrEnv['last_name']; ?></span>

											<small>Asunto:</small>
											<strong class="text-italic text-primary-light"><i class=" fa fa-fixed-width"></i> <?php echo $corrEnv['affair']; ?></strong><br>
											<small>Mensaje enviado el:</small>
											<small class="text-italic text-primary-light"><i class="fa fa-calendar fa fa-fixed-width"></i> <?php echo $corrEnv['date_creation']; ?></small>
										</div>
									</div>
								</li>
							<?php } ?>
						</ul>
					</div>
          <!-- Seccion de correo-->
					<div class="col-md-9 detailsWrapper" id="contenedorCorreo">
						<form method="post" enctype="multipart/form-data" id="formCorreo">
				      <input type="hidden" name="opcn" value="enviar">
				      <div class="innerAll"  style="overflow-y:scroll;height:600px;width:100%;">
				        <div class="modal-header" id="mail">
				          <div class="row modal-header" id="mail-header">
										<div class="col-md-6 form-group">
											<label for="s_persona">Persona</label>
											<select multiple="multiple" style="width: 100%;" name="s_persona[]" id="s_persona">
												<optgroup label="concesionarios" id="">
								          <?php foreach ($listaPersonal as $key => $keyPersona) {
								            ?>
								            <option value="<?php echo $keyPersona['user_id']; ?>"><?php echo $keyPersona['first_name']." ".$keyPersona['last_name']; ?></option>
								          <?php } ?>
								        </optgroup>
											</select>
										</div>
										<div class="col-md-6 form-group">
											<label for="s_concesionario">concesionarios</label>
											<select multiple="multiple" style="width: 100%;" name="s_concesionario[]" id="s_concesionario">
												<optgroup label="concesionarios">
									          <?php foreach ($listaConcesionarios as $key => $Data_Dealers) { ?>
									            <option value="<?php echo $Data_Dealers['dealer_id']; ?>"><?php echo $Data_Dealers['dealer']; ?></option>
									          <?php } ?>
									        </optgroup>
											</select>
										</div>
										<div class="col-md-4 form-group">
											<label for="s_cargos">Cargos</label>
											<select multiple="multiple" style="width: 100%;" name="s_cargos[]" id="s_cargos">
												<optgroup label="Cargos">
								          <?php foreach ($listaCargos as $key => $keyCargos) { ?>
								            <option value="<?php echo $keyCargos['charge_id']; ?>"><?php echo $keyCargos['charge']; ?></option>
								          <?php } ?>
								        </optgroup>
											</select>
										</div>
										<div class="col-md-4 form-group">
											<label for="s_genero">Género</label>
											<select style="width: 100%;" name="s_genero" id="s_genero">
												<option></option>
												<option value="1">Hombre</option>
												<option value="2">Mujer</option>
											</select>
										</div>
										<div class="col-md-4 form-group">
											<label for="s_rol">Rol</label>
											<select multiple="multiple" style="width: 100%;" name="s_rol[]" id="s_rol">
												<optgroup label="Roles">
								          <?php foreach ($listaRoles as $key => $keyRoles) { ?>
								            <option value="<?php echo $keyRoles['rol_id']; ?>"><?php echo $keyRoles['rol']; ?></option>
								          <?php } ?>
								        </optgroup>
											</select>
										</div>
				            <div class="col-md-9 form-group">
				              <label for="asunto">Asunto</label>
				              <input type="text" name="asunto" id="asunto" class="form-control">
				            </div>
				            <div class="col-md-3 form-group">
				              <label for="">Adjunto</label>
				                <label for="inputFile" class="form-control">
				                <span id="textoFile"></span>
				                </label>
				              <input type="file" id="inputFile" name="inputFile" class="form-control" style="display: none;">
				            </div>
				          </div>
				          <div id="mail-body" style="margin: 20px auto;padding:5px;width: 85%;max-width: 900px;justify-content: center;border: solid 1px silver;border-radius: 3px;">
				              <div style="margin:0 auto; padding:5px 10px; display: flex;flex-wrap; wrap;">
				                <div style="margin: 5px;width: 25%;min-width: 220px;">
				                <label for="imagen-mail">
				                  <div id="contentImagen">
				                    <img src="../assets/images/upload-img.png" alt="upload" style="width: 100%">
				                  </div>
				                </label>
				                <input type="file" name="imagen-mail" id="imagen-mail" style="display: none;">
				                </div>
				                <div class="" style="border: solid 1px #eee;margin: 5px;width: 70%;min-height: 200px;">
				                	<textarea class="wysihtml5" name="textoEmail" id="textoEmail" style="width: 95%;min-height: 150px;border: none;" row="15" placeholder="Ingrese su comentario"></textarea>
				                </div>
				              </div>
				          </div>
				        </div>
				      </div>
				      <div class="modal-footer">
				        <div class="row">
				          <button type="submit" name="enviar" id="enviar" class="btn btn-primary">Enviar</button>
				        </div>
				      </div>
				    </form><!-- fin formulario -->
					</div>
				</div>
				</div>
			</div>
		<!-- // Mensajes -->
		</div>
	</div>
</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/newsletter.js"></script>
</body>
</html>
