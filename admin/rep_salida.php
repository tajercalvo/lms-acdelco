<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_salida.php');
$location = 'reporting';
$locData = true;
$qtip = 'qtip';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons security_camera"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_salida.php">Reporte Salidas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte Salidas </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">A continuación encontrará la información estructurada de las salidas a entrenamiento por zona, por concesionario, por cargo en los cursos vistos durante determinado periodo de tiempo, por favor configure la información y de click en consultar para presentarle la información de su interés. Una vez consultada puede descargarla para realizar operaciones localmente (En su computador).</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<h5><a href="rep_salida_excel.php?dealer_id=<?php echo($_POST['dealer_id']); ?>&start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			</div>
		</div>
		<form action="rep_salida.php" method="post">	
		<div class="row">
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="dealer_id" style="padding-top:8px;">Concesionario:</label>
					<div class="col-md-10">
						<select style="width: 100%;" id="dealer_id" name="dealer_id" >
							<?php foreach ($datosCursos as $key => $Data_Cursos) { ?>
								<option value="<?php echo($Data_Cursos['dealer_id']); ?>"><?php echo($Data_Cursos['dealer']); ?></option>
							<?php } ?>
						</select>
					</div>
					
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
					<div class="col-md-9 input-group date">
				    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-3 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final:</label>
					<div class="col-md-9 input-group date">
				    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
			</div>
		</div>
		</form>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información: </h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<!-- Table elements-->
		<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
			<!-- Table heading -->
			<thead>
				<tr>
					<th data-hide="phone,tablet" style="width: 15%;">CONCESIONARIO</th>
					<th data-hide="phone,tablet" style="width: 20%;">SEDE</th>
					<th data-hide="phone,tablet" style="width: 5%;">IDENTIFICACIÓN</th>
					<th data-hide="phone,tablet" style="width: 20%;">USUARIO</th>
					<th data-hide="phone,tablet" style="width: 20%;">TRAYECTORIAS</th>
					<th data-hide="phone,tablet" style="width: 5%;">SALIDAS</th>
					<th data-hide="phone,tablet" style="width: 5%;">HORAS</th>
				</tr>
			</thead>
			<!-- // Table heading END -->
			<tbody>
				<?php 
				if($cantidad_datos > 0){ 
					foreach ($datosCursos_all as $iID => $data) { 
						?>
					<tr>
						<td><?php echo($data['dealer']); ?></td>
						<td><?php echo($data['headquarter']); ?></td>
						<td><?php echo($data['identification']); ?></td>
						<td><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
						<td align="center">
							<?php 
								if(isset($data['charges'])){
									foreach ($data['charges'] as $iID => $dataCharges) { 
										echo(" - ".$dataCharges['charge']);
									}
								}
							?>
						</td>
						<td align="center"><?php echo($data['tiempo']/8); ?></td>
						<td align="center"><?php echo($data['tiempo']); ?></td>
					</tr>
				<?php } } ?>
			</tbody>
		</table>
		<!-- // Table elements END -->
		<!-- Total elements 
		<div class="form-inline separator bottom small">
			Total de inscritos: <strong class='text-primary'><?php echo($cantidad_datos); ?></strong> | Invitados: <strong class='text-primary'><?php echo($invitados); ?></strong> | Asistieron: <strong class='text-success'><?php echo($asistieron_tot); ?></strong> | No Asitieron: <strong class='text-danger'><?php echo($noasistieron_tot); ?> </strong> | Aprobaron: <strong class='text-success'><?php echo($aprobaron); ?></strong> | No Aprobaron: <strong class='text-danger'><?php echo($noaprobaron); ?></strong>
		</div>
		 // Total elements END -->
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_salida.js"></script>
</body>
</html>