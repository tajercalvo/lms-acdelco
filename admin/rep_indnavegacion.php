<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_indnavegacion.php');
$location = 'reporting';
$locData = true;
$Asist = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<!--  -->
<div id="cargando_pagina"> <span class="label label-success">Cargando elementos, por favor espere...</span><img style="width: 15px; " src="../assets/loading-course.gif"> </div>
<body class="document-body " >
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left" style="visibility: hidden;" id="mibody">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons link"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_indasistencia.php">Reporte Indicadores de Navegación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Indicadores Navegación</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Este es el reporte general de navegación en donde se encuentra la información de Personas Vs Secciones de la plataforma, es un reporte completo que tiene un nivel de entendimiento elevado y el recurso necesario para procesarlo también es elevado, sea muy consciente de su necesidad</h5></br>
								</div>
								<div class="col-md-2">
									<?php if($cantidad_datos > 0){ ?>
										<h5><a href="rep_navegacion_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>&dealer= <?php echo($_POST['dealer']); ?>&charge= <?php echo($_POST['charge']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									<?php } ?>
								</div>
							</div>

							<div class="row">
								<form action="rep_navegacion.php" method="post" onsubmit="return false">
									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-3 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
											<div class="col-md-6 input-group date">
										    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Desde..." value=""/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-3 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
											<div class="col-md-6 input-group date">
										    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..." value=""/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
										<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-3 control-label" style="padding-top:8px;">Cargo:</label>
											<div class="col-md-6 input-group date">
										    	<select style="width: 100%;" id="charge" name="charge">
										    	<option value=""></option>
												<?php foreach ($trayectorias as $key => $Data_ActiveCharge) { ?>
														<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php if(isset($_POST['charge']) && ($_POST['charge']==$Data_ActiveCharge['charge_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $Data_ActiveCharge['charge']; ?></option>
													<?php } ?>
											</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>

									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-3 control-label" style="padding-top:8px;">Zona:</label>
											<div class="col-md-6 input-group date">
										    	<select style="width: 100%;" id="zone" name="zone">
										    	<option value=""></option>
												<?php  foreach ($zona as $key => $value) { ?>
													<option value="<?php echo $value['zone_id']; ?>" <?php if(isset($_POST['zone']) && ($_POST['zone']==$value['zone_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $value['zone']; ?></option>

												<?php  } ?>
											</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
										<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-3 control-label" style="padding-top:8px;">Concesionario:</label>
											<div class="col-md-6 input-group date">
										    	<select style="width: 100%;" id="dealer" name="dealer">
										    	<option value=""></option>
												<?php  foreach ($concesionarios as $key => $value) { ?>
													<option value="<?php echo $value['dealer_id']; ?>" <?php if(isset($_POST['dealer']) && ($_POST['dealer']==$value['dealer_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $value['dealer']; ?></option>

												<?php  } ?>
											</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
										<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-3 control-label" style="padding-top:8px;">Sede:</label>
											<div class="col-md-6 input-group date">
										    	<select style="width: 100%;" id="headquarters" name="headquarters">
										    	<option value=""></option>
												<?php  foreach ($sedes as $key => $value) { ?>
													<option value="<?php echo $value['headquarter_id']; ?>" <?php if(isset($_POST['headquarter']) && ($_POST['headquarter']==$value['headquarter_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $value['headquarter']; ?></option>

												<?php  } ?>
											</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-1">
										<!-- <span class="label label-success">Descargando...</span> -->
									</div>
									<div class="col-md-4">
										<div id="consultando"></div>
									</div>
									<!-- <div class="col-md-3">

									</div> -->
									<div class="col-md-4">
										<!-- <img style="width: 15px; " src="../assets/loading-course.gif"> -->
									</div>

									<div class="col-md-3">
										<!-- <h1 class="btn btn-success" id="btn_consulta"><i class="fa fa-check-circle"></i> Consultar</h1> -->
									 <button type="submit" class="btn btn-success" id="btn_consulta"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>

								</form>
							</div>
						</div>
					</div>
					<!-- Inicio tablas -->
					<div class="well" id="contenido_tablas" style="visibility: hidden;">

						<!-- tabla de datos -->
						<div class="row border-top border-bottom innerLR">
							<div class="col-md-5" style="background-color: white">
								<div id="tabla_visitantes1"></div>
							</div>

							<div class="col-md-2"></div>

							<div class="col-md-5" style="background-color: white">
								<div id="tabla_visitantes2"></div>
							</div>
						</div>
						<br>
						<br>
						<!-- linea de graficas -->
						<div class="row border-top border-bottom innerLR">
							<div class="col-md-5" style="background-color: white">
								<div class="box-body">
					              <div class="chart">
					                <canvas id="lineChart1" style="height:250px"></canvas>
					              </div>
					            </div>
							</div>
							<div class="col-md-2">

							</div>
							<div class="col-md-5" style="background-color: white">
								<div class="box-body">
					              <div class="chart">
					                <canvas id="lineChart2" style="height:250px"></canvas>
					              </div>
					            </div>
							</div>
						</div>

					</div>

					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_indnavegacion.js"></script>
</body>
</html>
