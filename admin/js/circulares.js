$(document).ready(function(){
    $("#cargos").select2({
        placeholder: "Seleccione las trayectorias",
        allowClear: true
    });
    $("#type").select2({
        placeholder: "Seleccione el (los) tipos",
        allowClear: true
    });
    $("#cargos_ed").select2({
        placeholder: "Seleccione las trayectorias",
        allowClear: true
    });
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});
$( "#form_Crear" ).submit(function( event ) {
    if(valida()){
       return true;
    }else{
        return false;
    }
    event.preventDefault();
});

function valida(){
    var valid = 0;
    $('#title').css( "border-color", "#efefef" );
    $('#newsletter').css( "border-color", "#efefef" );
    $('#Archivo_new').css( "border-color", "#efefef" );
    var inputFileImage = document.getElementById("Archivo_new");
    var file = inputFileImage.files[0];
    if(file!= undefined){
        if(file.type == "application/pdf" || file.type == "application/zip"){
            if(file.size>2500000){
                $('#Archivo_new').css( "border-color", "#b94a48" );
                valid = 1;
                notyfy({
                    text: 'Por favor verifíque la información que esta pendiente: Hay problemas con el archivo',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            $('#Archivo_new').css( "border-color", "#b94a48" );
            valid = 1;
            notyfy({
                    text: 'Por favor verifíque la información que esta pendiente: Hay problemas con el archivo',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
        }
    }
    if($('#title').val() == ""){
        $('#title').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#newsletter').val() == ""){
        $('#newsletter').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}
