  //-----------------------------//
 // Creado por: Fredy Mendoza   //
//-----------------------------//
$(document).ready(function(){
    consultar_usuarios()
    // cursos()
    $("#multiple").select2({
                placeholder: "Seleccione una sesión",
                allowClear: true
      });


});


function cursos(){
    $.ajax({
        url: 'controllers/asignacion_curso.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn: 'consultar_allCursos'},
    })
    .done(function(data) {
        console.log("success");
        datos = ''
        for (var i = 0; i < data.length; i++) {
            datos += '<option value="' + data[i].course_id + '">' + data[i].course +' </option>';
        }

         $('#multiple').html(datos);
    })
    .fail(function() {
        console.log("error");
    })
    
    
}

function consultar_usuarios(){
var data = {opcn:'consultar_outside_user'};
 $.ajax({
         url: 'controllers/asignacion_curso.php',
         type: 'POST',
         dataType: 'JSON',
         data: data,
     })
.done(function(data) {
    table = ''
    if(!data) return;
    xnum = data.length
    
   for (var i = 0; i < data.length; i++) {
        var editar = '<button data-id="'+data[i].user_id+'" type="button" class="editar btn btn-success btn-xs" ><i class="fa fa-edit" ></i></button>'
       table+='<tr>'
       table+='<td>'+data[i].identification+'</td>'
       table+='<td>'+data[i].nombre+ '</td>'
       table+='<td>'+data[i].charge+'</td>'
       table+='<td>'+data[i].courser+'</td>'
       table+='<td style="text-align: center;">'+editar+'</td>'
       table+='</tr>'
   }

   
         $('#outside_user tbody').html(table);
         //table.destroy();
         $('#outside_user').DataTable({
                "language": {
                  "sProcessing": "Procesando...",
                  "sLengthMenu": "Mostrar MENU registros",
                  "sZeroRecords": "No se encontraron resultados",
                  "sEmptyTable": "Ning¨²n dato disponible en esta tabla",
                  "sInfo": "Mostrando registros del START al END de un total de TOTAL registros",
                  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered": "(de MAX registros)",
                  "sInfoPostFix": "",
                  "sSearch": "Buscar:",
                  "sUrl": "",
                  "sInfoThousands": ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                  "sFirst": "Primero",
                  "sLast": "0‰3ltimo",
                  "sNext": "Siguiente",
                  "sPrevious": "Anterior"
                  },
                  "oAria": {
                  "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
                  },
                  pageLength: 12,
                  dom: 'lBfrtip',
                  buttons: [
                  {extend : 'excelHtml5',
                  text: '<i class="las la-file-excel">EXCEL</i>',
                  filename: 'clientes_autotrain',
                  },
                  {extend : 'pdfHtml5',
                  text: '<i class="las la-file-pdf">PDF</i>',
                  //orientation: 'landscape',
                  filename: 'clientes_autotrain',
                  exportOptions: {
                   columns: [0,1,2,3,4],
                  }
                  },
                  {extend: 'print',text: '<i class="las la-print">IMPRIMIR</i>', filename: 'clientes_autotrain'}
                  ],
                    deferRender:    true,
                        scrollX:        true,
                        scrollCollapse: true,
                        scroller:       true
         });//convierte en datatable
         $('#num_asistentes').html(xnum+' Usuarios');


})
.fail(function() {
    console.log("error");
})
}


$('body').on('click', '.editar', function() {
      var data=[]
        id = $(this).data("id")
        data.push({ name: "opcn", value: 'consultarCurso_id' });
        data.push({ name: "id", value: id });
         $('#agregar_cursos').modal('show')
        $.ajax({
                url: 'controllers/asignacion_curso.php',
                type: 'POST',
                dataType: 'JSON',
                data: data,
            })
            .done(function(data) {
                let cursos_id = []
                let select = data.cursosTotal.map((el) => {
                    if(data.cursosUsuario.some((x) => x.course_id == el.course_id)) cursos_id.push(el.course_id)
                    return `<option value="${el.course_id}" >${el.course} </option>`
                })
                $('#multiple').html(select);
                $("#multiple").val(cursos_id).trigger('change');
            })

            .fail(function() {
                console.log("error");
            })
    })

$('#edit_cursos').submit(function(event) {
event.preventDefault();
// data = []
     info = $(this).serializeArray();
     if (info == '') {
      console.log('vacio');
      info.push({name:'curos', value:0})
      console.log(info);

     }

    $.ajax({
        url: 'controllers/asignacion_curso.php',
        type: 'POST',
        dataType: 'json',
        data:{opcn:'editar_asignacion',id:id, info:info},
    })
    .done(function(data) {
        console.log(data);
         
        $('#agregar_cursos').modal('hide');
         location. reload();
       // consultar_usuarios();

    })
    .fail(function() {
        console.log("error");
    })
    
    


})
// $('#edit_usuario').submit(function(e) {
//            e.preventDefault();

//            var datos = $(this).serializeArray();
//            datos.push({ name: "opcn", value: opcn });
//            datos.push({ name: "id", value: id });

//            $.ajax({
//                    url: 'controllers/trabajadores.php',
//                    type: 'POST',
//                    dataType: 'JSON',
//                    data: datos,
//                })
//                .done(function(e) {

//                    consultarusuarios()
//                    alert(e.msj)
//                    $("#resultados").html('usuario actualizado')
//                    $("#resultados").css('display', 'block')
//                    $("#resultados").fadeOut(5000)
//                    $('#editUsuario').modal('hide');

//                })
//                .fail(function() {
//                    console.log("error");
//                })
//        })

