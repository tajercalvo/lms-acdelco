$(document).ready(function() {
	
	 // $("#mot_salida").select2({
  //       placeholder: "Seleccione un estado",
  //       allowClear: true
  //   });
	Consultar_encuesta()
	graficos()
});



function Consultar_encuesta() {
	
	$.ajax({
		url: 'controllers/encuesta_covid19.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'consultar_encuesta'},
	})
	.done(function(data) {
		console.log(data);

		table =''
		xnum = data.length
		for (var i = 0; i < data.length; i++) {
			table+='<tr>'
			table+='<td>'+data[i].encuesta_id+'</td>'
			table+='<td>'+data[i].name_user+'</td>'
			table+='<td>'+data[i].mot_salida+'</td>'
			table+='<td>'+data[i].opc_otros+'</td>'
			table+='<td>'+data[i].lugar_a_trabajar+'</td>'
			table+='<td>'+data[i].md_transporte+'</td>'
			table+='<td>'+data[i].nombreycontacto+'</td>'
			table+='<td>'+data[i].check1+'</td>'
			table+='<td>'+data[i].check2+'</td>'
			table+='<td>'+data[i].check3+'</td>'
			table+='<td>'+data[i].check4+'</td>'
			table+='<td>'+data[i].check5+'</td>'
			table+='<td>'+data[i].check6+'</td>'
			table+='<td>'+data[i].check7+'</td>'
			table+='<td>'+data[i].check8+'</td>'
			table+='<td>'+data[i].check9+'</td>'
			table+='<td>'+data[i].check10+'</td>'
			table+='<td>'+data[i].check11+'</td>'
			table+='<td>'+data[i].check12+'</td>'
			table+='<td>'+data[i].check13+'</td>'
			table+='<td>'+data[i].date+'</td>'
			table+='</tr>'
		}
		$('#tabla_encuesta tbody').html(table)
		  $('#tabla_encuesta').DataTable({
		  	order:[[0,"desc"]],
                    "language": {
                      "sProcessing": "Procesando...",
                      "sLengthMenu": "",
                      "sZeroRecords": "No se encontraron resultados",
                      "sEmptyTable": "Ning¨²n dato disponible en esta tabla",
                      "sInfo": "Mostrando registros del Inicio al Final de un total de TOTAL registros",
                      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered": "(de MAX registros)",
                      "sInfoPostFix": "",
                      "sSearch": "Buscar:",
                      "sUrl": "",
                      "sInfoThousands": ",",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                      "sFirst": "Primero",
                      "sLast": "0‰3ltimo",
                      "sNext": "Siguiente",
                      "sPrevious": "Anterior"
                      },
                      "oAria": {
                      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                      }
                      ,
                      dom: 'lBfrtip',
                      pageLength: 12,
                      buttons: [
                      {extend : 'excelHtml5',
                      text: '<i class="las la-file-excel">EXCEL</i>',
                      filename: 'clientes_autotrain',
                      },
                      {extend : 'pdfHtml5',
                      text: '<i class="las la-file-pdf">PDF</i>',
                      //orientation: 'landscape',
                      filename: 'Encuesta_Usuarios_AutoTrain',
                      exportOptions: {
                       columns: [0,1,2,3,4,5],
                      }
                      },
                      {extend: 'print',text: '<i class="las la-print">IMPRIMIR</i>', filename: 'AutoTrain_Encuesta_Covid19'}
                      ],
                        
                        deferRender:    true,
			            scrollX:        true,
			            scrollCollapse: true,
			            scroller: true
             });//convierte en datatable
             $('#num_usuarios').html('Usuarios Registrados: '+xnum);

	})
	
	
}



//---------------DONA6---------------------//
function graficos(){

	$.ajax({
		url: 'controllers/encuesta_covid19.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'consult_Graficos'},
	})
	.done(function(data) {
			console.log(data);

		if (data['Motivo_salida']) {
			var Motivo_salida= data['Motivo_salida']
				    Morris.Donut({
					    element: 'dona1',
					     data: Motivo_salida,
					   colors: ['#42dabe','#d25f5f','#538fc1','#83efef','#f5dd88','#480505','#a1ca04'],
					})
		}
		 
		if (data['md_transporte']) {
			var md_transporte= data['md_transporte']
				    Morris.Donut({
					    element: 'dona2',
					     data: md_transporte,
					   colors: ['#42dabe','#d25f5f','#538fc1','#83efef','#f5dd88'],
					})
		}

		if (data['check1']) {
			var check1= data['check1']
				    Morris.Donut({
					    element: 'dona3',
					     data: check1,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		if (data['check2']) {
			var check2= data['check2']
				    Morris.Donut({
					    element: 'dona4',
					     data: check2,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		if (data['check3']) {
			var check3= data['check3']
				    Morris.Donut({
					    element: 'dona5',
					     data: check3,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		if (data['check4']) {
			var check4= data['check4']
				    Morris.Donut({
					    element: 'dona6',
					     data: check4,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		if (data['check5']) {
			var check5= data['check5']
				    Morris.Donut({
					    element: 'dona7',
					     data: check5,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		if (data['check6']) {
			var check6= data['check6']
				    Morris.Donut({
					    element: 'dona8',
					     data: check6,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		if (data['check7']) {
			var check7= data['check7']
				    Morris.Donut({
					    element: 'dona9',
					     data: check7,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		if (data['check8']) {
			var check8= data['check8']
				    Morris.Donut({
					    element: 'dona10',
					     data: check8,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
		
			if (data['check9']) {
			var check9= data['check9']
				    Morris.Donut({
					    element: 'dona11',
					     data: check9,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
			if (data['check10']) {
			var check10= data['check10']
				    Morris.Donut({
					    element: 'dona12',
					     data: check10,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
			if (data['check11']) {
			var check11= data['check11']
				    Morris.Donut({
					    element: 'dona13',
					     data: check11,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
			if (data['check12']) {
			var check12= data['check12']
				    Morris.Donut({
					    element: 'dona14',
					     data: check12,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
			if (data['check13']) {
			var check13 = data['check13']
				    Morris.Donut({
					    element: 'dona15',
					     data: check13,
					   colors: ['#42dabe','#d25f5f'],
					})
		}
	})
	.fail(function() {
		console.log("error");
	})
	
	


}



		
