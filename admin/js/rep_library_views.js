$(document).ready(function(){
        consultar_views();

});

        $("#library_id").change(function(){
        $("#descargar").css("display","none");
        });


        $("#descargar").click(function(event) {
        $("#datos_tabla").val( $("<div>").append( $("#asistentes").eq(0).clone()).html());
        $("#FormularioExportacion").submit();

    });

//consulta y llena el select con el nombre y id del broadcast
function consultar_views(){

var data = {opcn:'consultar_views'};

         $.ajax({
             url: 'controllers/rep_library_views.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             console.log(data);
            var datos = "";
            for (var i = 0; i < data.length; i++) {
              datos += '<option value="'+data[i].library_id+'">'+data[i].library_id+' - '+data[i].name+' </option>';

             }

             $('#library_id').html(datos)

                $("#library_id").select2({
                    placeholder: "Seleccione una sesión",
                    allowClear: true
                 });
         })
         .fail(function() {
             console.log("error");
         })
}

$('#frm').submit(function(e) {
    e.preventDefault();
    var library_id = $('#library_id').val()

    $('#consultando').html('<img style="width: 15px; " src="../assets/loading.gif">');

    var data = {opcn:'consultar_libreria', library_id: library_id};
    $.ajax({
             url: 'controllers/rep_library_views.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             console.log(data);
             $('#consultando').html('');//imagen gif se quite
             $("#descargar").css("display","block");
             var table = "";
            for (var i = 0; i < data.length; i++) {

                table +='<tr>';
                table +='<td>'+data[i].identificacion+'</td>';
                table +='<td>'+data[i].nombre+' '+data[i].apellido+'</td>';
                table +='<td>'+data[i].concesionario+'</td>';
                table +='<td>'+data[i].sede+'</td>';
                table +='<td>'+data[i].zona+'</td>';
                table +='<td>'+data[i].play+'</td>';
                table +='<td>'+data[i].finish+'</td>';
                table +='</tr>';

             }

             $('#asistentes tbody').html(table);

         })
         .fail(function() {
             $('#consultando').html('');
             console.log("error");
         })
});





