$(document).ready(function(){

// var fecha1 = new Date(2016,01,01);
// var fecha2 = new Date(2016,04,01);
// var diasDif = fecha2.getTime() - fecha1.getTime();
// var dias = Math.round(diasDif/(1000 * 60 * 60 * 24));
// alert("dias de diferencia: " + dias);
	
    // Select Placeholders
    $("#cursosF").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });

     $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

    //Para descargar tabla que se muestra en el html
     $("#descargar_excel").click(function(event) {

        $("#FormularioExportacion").submit();

    });

});

//Contador para consultar los siguientes registros por id
siguiente= 0;

$( "#consulta" ).click(function() {

    siguiente = 0;
    //$('#miTablaaa').append('');

    //Borra el contenido de la tabla
    document.getElementById("miTablaaa").innerHTML = '';
    

    idconsulta = $( "#cursosF" ).val();

    $("#cargando").css("visibility","visible");
    $("#cargando").text("Realizando la consulta, por favor espere…");

    var data = new FormData();
    data.append('opcn','consulta');
    data.append('idconsulta',idconsulta);
    data.append('start_date_day',$("#start_date_day").val());
    data.append('end_date_day',$("#end_date_day").val());
    
    $.ajax({
        url:"controllers/rep_ajax.php",
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "text",
        //dataType: "json",
        cache:false,
        success: function(data) {
            
            //SI data biene vacio va y busca el siguiente id del curso
          // document.getElementById("miTablaaa").innerHTML = data;
            if(data==""){
                
                    $("#cargando").text("consulta si resultados");
   
                     // $("#cargando").css("visibility","hidden");
                 
                }else{

                        if (idconsulta == 0) {

                            array_registros = data.split("-");
                            cantidad_registros = array_registros.length-1;
                            console.log('Resultado total ' + cantidad_registros);
                            //consulta_siguiente_ajax(array_registros[contador]);

                            for(var i in array_registros) {

                                                console.log(array_registros[i]);
                                                
                                        } 

                            consulta_siguiente_ajax(array_registros[siguiente]);


                            // scroll = document.getElementById("wrapper").scrollHeight;
                            // console.log('Scroll ='+document.getElementById("content").scrollHeight); 

                        }else{

                            console.log('Id '+$( "#cursosF" ).val()+' OK'); 

                            // Muestra la tabla de la consulta, oculta el mensaje consultando... y muestra el boton para mostrar mas resultados
                            $('#miTablaaa').append(data);
                            $("#cargando").css("visibility","hidden");

                            //Capturo e imprimo el tamaño del scroll actual
                            
                            CargarGraficas();

                        }
                                                 
            } // Fin else
        } // Fin function data
    });// Fin ajax
    
});
// Fin funcion click

/*//Contador para consultar los siguientes registros por id
contador= 0;

$( "#consulta" ).click(function() {

    //Borra el contenido de la tabla
    document.getElementById("miTablaaa").innerHTML = '';

    idconsulta = $( "#cursosF" ).val();

    $("#cargando").css("visibility","visible");
    $("#cargando").text("Realizando la consulta, por favor espere…");

    var data = new FormData();
    data.append('opcn','consulta');
    data.append('idconsulta',idconsulta);
    data.append('contador',0);
    data.append('start_date_day',$("#start_date_day").val());
    data.append('end_date_day',$("#end_date_day").val());
    
    $.ajax({
        url:"controllers/rep_ajax.php",
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "text",
        //dataType: "json",
        cache:false,
        success: function(data) {
            
            //SI data biene vacio va y busca el siguiente id del curso
          // document.getElementById("miTablaaa").innerHTML = data;
            if(data==""){
            
                    $("#cargando").text("consulta si resultados");

            //         // $("#cargando").css("visibility","hidden");
                 
                }else{
                
                    console.log('Id '+$( "#cursosF" ).val()+' OK'); 

                    // Muestra la tabla de la consulta, oculta el mensaje consultando... y muestra el boton para mostrar mas resultados
                    $('#miTablaaa').append(data);
                    $("#cargando").css("visibility","hidden");

                    //Capturo e imprimo el tamaño del scroll actual
                    scroll = document.getElementById("wrapper").scrollHeight;
                    console.log('Scroll ='+document.getElementById("content").scrollHeight); 

                   // Carga los graficos del html
                   CargarGraficas();

                   //Le sumamos 1 al contador para cuando se le de clic a mostrar mas vaya por el siguiente id del curso 
                   if ($( "#cursosF" ).val()=='0') {

                            contador= +1;

                        }            
            }
        } // Fin function data
    });// Fin ajax
    
});
// Fin funcion click*/

//Inicio funcion cuando se hace scroll
scroll = 1000;

$('#wrapper').scroll(function(){

    Altura_div = $('#wrapper').scrollTop();
    Altura_ventana = $(document).height();
    //console.log('El scrollTop de wraper es= '+Altura_div+' Y la altura de la ventana es ='+Altura_ventana+' Y el scroll actual es de = '+scroll);


    //console.log('Scroll actual'+$('#wrapper').scrollTop());
    // console.log('Actual de la vetana'+$(document).height());
    //console.log('Scroll actual'+$('#content').scrollTop());
    // console.log('Scroll total '+scroll);

    // El evento scroll solo se activa cuando la opcion selecionada es la 0
    if ( idconsulta == '0' && array_registros[siguiente] != "") {


                 if (Altura_div + Altura_ventana == scroll) {

                        scroll = Altura_div + Altura_ventana + 1000;

                        // console.log(contador);
                        // Si el contador es igual a la cantidad de cursos que vienen por PHP
                       
                        //console.log('Buscar id ' + array_registros[siguiente]);
                        $("#cargando").css("visibility","visible");
                        consulta_siguiente_ajax(array_registros[siguiente]);   
                 }
     }else{

        $("#cargando").css("visibility","visible");

        $("#cargando").text("Fin consulta");

     }
     
          
});

//Fin funcion cuando se hace scroll

//Inicio Consulta el siguiente id

function consulta_siguiente_ajax(id_siguiente){

    $("#cargando").text("Mostrando "+(siguiente+1)+" de "+ cantidad_registros);

    $("#cargando").css("visibility","visible");

    var data = new FormData();
    data.append('opcn','consulta');
    data.append('idconsulta', id_siguiente);
    data.append('start_date_day',$("#start_date_day").val());
    data.append('end_date_day',$("#end_date_day").val());

    $.ajax({
        url:"controllers/rep_ajax.php",
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "text",
        cache:false,
        success: function(data) {
            //Si la consulta viene vacia, se le suma uno al contador y se busca el siguiente id
            if (data=='FIN') {

                $("#cargando").text("Fin consulta");
                      
            }else{

                //Muestra la tabla
                $('#miTablaaa').append(data);

                // //Boton mostrar mas si se requiere
                // //$("#mostrarmas").css("visibility","visible");

                // //Oculta mensaje mostrando resultados
                 $("#cargando").css("visibility","hidden");


                // // Se le asigna a la variable scroll la nueva altura del id wraper
                scroll = document.getElementById("wrapper").scrollHeight;
                console.log('Scroll ='+document.getElementById("wrapper").scrollHeight); 

                // // Muestra los graficos de la tabla
                 CargarGraficas();
                 siguiente++;
            }
   
        } //Fin function data
    }); // Fin Ajax
}

// Inicio Consulta el siguiente id

function CargarGraficas(){
    $(".ElementID").each(function (index) {
        var Sche_id_val = $(this).val();
        var DatConce_min = [];
        var DatConce_max = [];
        var DatConce_ins = [];
        var DataNames = [];
        var _CtrlVar = 0;
        $(".ValSche"+Sche_id_val).each(function (index) {
            _CtrlVar = _CtrlVar + 1;
            //console.log("Concesionario:"+this.value);
            DatConce_min.push([_CtrlVar, $(this).attr('data-min') ]);
            DatConce_max.push([_CtrlVar, $(this).attr('data-max') ]);
            DatConce_ins.push([_CtrlVar, $(this).attr('data-ins') ]);
            DataNames.push([_CtrlVar, $(this).attr('data-deal') ]);
        });
        var ds = new Array();
        ds.push({
            label: "Mínimos",
            data:DatConce_min,
            bars: {order: 1}
        });
        ds.push({
            label: "Máximos",
            data:DatConce_max,
            bars: {order: 2}
        });
        ds.push({
            label: "Inscripciones",
            data:DatConce_ins,
            bars: {order: 3}
        });
        var data = ds;

        $.plot($("#chart_ordered_bars"+Sche_id_val), data, {
            bars: {
                show:true,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y.0",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames
            }
        });
    });
}

$( "#descargar" ).click(function() {

    acumula = 0;
    cien = 0;
    sum = 0;

    document.getElementById("miTablaaa").innerHTML = '';
    document.getElementById("miTabla_descarga").innerHTML = '';
    $("#barra_progreso").width(0);
    $("#descargando_reporte").text('');
    $("#progreso_visible").css("visibility","visible");
    

    id_descarga = $( "#cursosF" ).val();

    //$("#cargando").css("visibility","visible");
    //$("#cargando").text("Realizando la consulta, por favor espere…");

    var data = new FormData();
    data.append('opcn','descargar');
    data.append('idconsulta',id_descarga);
    
    data.append('start_date_day',$("#start_date_day").val());
    data.append('end_date_day',$("#end_date_day").val());
    
    $.ajax({
        url:"controllers/rep_ajax.php",
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "text",
        //dataType: "json",
        cache:false,
        success: function(data) {
  
             if(data==""){
            
                    $("#cargando").text("consulta si resultados");

                 
                }else{

                    $("#progreso_visible").css("visibility","visible");

                        if (id_descarga == 0) {

                                    // convertimos el texto que biene de php en un array, la letra p es el delimitador del array
                                    res = data.split("p");

                                    console.log('Resultado total '+(res.length-1));

                                    //Contamos el array y le restamos 1 por que el ultimo array biene vacio, asi obtenemos la cantidad
                                    // de cursos a concultar
                                    registros = res.length-1;

                                    // Dividimos la cantidad de registros etre 100 para asignarlo por parte a la barra de progreso
                                    cien = 100 /registros;

                                    //Recorremos el array para obtener el siguiente resultado a consultar
                                    for(var i in res) {

                                            console.log(res[i]);
                                            descargar_siguiente(res[i]);
                                    } 

                            }else{

                                  $('#miTabla_descarga').append(data);
                                  $("#cargando").css("visibility","hidden");

                            }
                } //Fin Else
        } // Fin function dta
    }); // FIn ajax
    
}); // Fin  FUnction click

tabla = '';
function descargar_siguiente(id){

    var data = new FormData();
    data.append('opcn','descargar');
    data.append('idconsulta',id);
    data.append('start_date_day',$("#start_date_day").val());
    data.append('end_date_day',$("#end_date_day").val());

    $.ajax({
        url:"controllers/rep_ajax.php",
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "text",
        cache:false,
        success: function(data) {

                    tabla = tabla + data;
                    
                    // $('#miTabla_descarga').append(data);
                    console.log("progress"+(acumula=acumula+cien));
                    $("#barra_progreso").width(acumula+'%');
                    console.log('Progreso igual a '+acumula);

                    sum++;

                    if (sum >= registros) {
                        $("#descargando_reporte").text('Descarga finalizada');
                        $("#barra_progreso").width(0);
                        acumula = 0;
                        //$("#cargando").css("visibility","hidden");
                        // $("#cargando").text("FIN descarga");
                        //$("#progreso_visible").remove();
                        //$("#progreso_visible").css("visibility","hidden");
                    }else{

                         $("#descargando_reporte").text('Descargando '+ sum + ' de ' + registros );
                         

                    }

                    if (sum > registros) {
                        $("#datos_tabla").val(tabla);
                        $("#FormularioExportacion").submit();
                    }

                    
        } // Fin function data 

    }); //Fin ajax

}// Fin Function descargar reporte


