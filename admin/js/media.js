$( "#form_CrearGaleria" ).submit(function( event ) {
    if(valida()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

$(document).ready(function(){
    // Select Placeholders
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});

function valida(){
    var valid = 0;
    $('#media').css( "border-color", "#efefef" );
    $('#description').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#media').val() == ""){
        $('#media').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description').val() == ""){
        $('#description').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new').val()==""){
        valid=1;
        notyfy({
                text: 'Debe seleccionar una imagen',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
    }else{
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }
    
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function CargaDatosEditar(media_id,image,media,description,estadoActual){
    console.log('dio click');
    $('#media_id').val(media_id);
    $('#imgant').val(image);
    $('#media_ed').val(media);
    $('#description_ed').val(description);
    if(estadoActual=="1"){
        $('#EstadoActual').html('Activo');
    }else{
        $('#EstadoActual').html('Inactivo');
    }
}

$( "#form_EditarGaleria" ).submit(function( event ) {
    if(validaEdt()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

function validaEdt(){
    var valid = 0;
    $('#media_ed').css( "border-color", "#efefef" );
    $('#description_ed').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#media_ed').val() == ""){
        $('#media_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description_ed').val() == ""){
        $('#description_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new_ed').val()!=""){
        var inputFileImage = document.getElementById("image_new_ed");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }
    
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}