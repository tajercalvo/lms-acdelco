$(function () {
    $(document).ready(function () {
        $("#schedule_id").select2({
            placeholder: "Seleccione una trayectoria",
            allowClear: true
        });
        $('#fecha_inicial').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2014-01-01"
        });
        $('#fecha_final').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2014-01-01"
        });
    });

    $('#schedule_id').change(function (e) {
        e.preventDefault();
        $('#tabla tbody').html('');
    });


    $('#frm_sesiones').submit(function (e) {
        e.preventDefault();

        fecha_inicial = $('#fecha_inicial').val();
        fecha_final = $('#fecha_final').val();
        var fechaInicio = new Date(fecha_inicial).getTime();
        var fechaFin = new Date(fecha_final).getTime();
        var diff = fechaFin - fechaInicio;
        console.log(diff / (1000 * 60 * 60 * 24));
        if (diff / (1000 * 60 * 60 * 24) > 31) {
            alert('La fecha no puede ser mayor a un mes');
            return false;
        }

        //$('#consultando').html('<img style="width: 15px; " src="../assets/loading.gif">');
        $('#tabla tbody').html('');
        var data = { opcn: 'getCoursesTeacher', schedule_id: $('#schedule_id').val(), fecha_inicial: fecha_inicial, fecha_final: fecha_final };
        $.ajax({
            url: 'controllers/reiniciar_actividades.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
        })
            .done(function (data) {
                console.log(data);
                select = '';
                data.forEach(function (e, i) {
                    select += `<option value="` + e.schedule_id + `" >Sesión: ` + e.schedule_id + ` - Curso: ` + e.course + ` - Sesión: ` + e.module + `</option>`
                });
                $('#schedule_id').html(select)
                $('#schedule_id').select2('destroy');
                $("#schedule_id").select2({
                    placeholder: "Seleccione una trayectoria",
                    allowClear: true
                });
            })
            .fail(function () {
                $('#consultando').html('');
                console.log("error");
            })
    });

    $('#frm_actividades').submit(function (e) {
        e.preventDefault();
        getAcitivitieUser();
    });

    function getAcitivitieUser() {
        var data = { opcn: 'getAcitivitieUser', schedule_id: $('#schedule_id').val() };
        $.ajax({
            url: 'controllers/reiniciar_actividades.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
        })
            .done(function (data) {

                tabla = '';
                data.forEach(function (e, i) {
                    if (e.teacher_id == 0) { estado = 'Pendiente' } else { estado = 'Calificada' }
                    tabla += `<tr data-pos="` + i + `">
                                <td>`+ e.identification + `</td>
                                <td>`+ e.first_name + ` ` + e.last_name + `</td>
                                <td>`+ e.date_creation + `</td>
                                <td>`+ e.resultado + `</td>
                                <td><button class="reiniciar btn btn-danger" data-activities_user_id="`+ e.activities_user_id + `"><i class="fa fa-trash-o"></i></button></td>
                            </tr>`;
                });
                $('#tabla tbody').html(tabla);
            })
            .fail(function () {
                console.log("error");
            })
    }

    $('body').on('click', '.reiniciar', function (e) {
        e.preventDefault();

        if (!confirm("Esta seguro de reiniciar esta evaluación?")) { return false; }
        let activities_user_id = $(this).data('activities_user_id');
        var data = { opcn: 'reiniciar_evaluacion', activities_user_id: activities_user_id };
        $.ajax({
            url: 'controllers/reiniciar_actividades.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
        })
            .done(function (data) {
                getAcitivitieUser();
                notyfy({
                    text: data.msj,
                    type: data.type // alert|error|success|information|warning|primary|confirm
                });
            })
            .fail(function () {
                console.log("error");
            })
    });

    $(document).ajaxStart(function () {
        $(".btn").prop("disabled", true);
        $('.btn').prepend('<i class="fa fa-refresh fa-spin"></i> ');
    })
    $(document).ajaxComplete(function () {
        $(".btn").prop("disabled", false);
        $('.fa-refresh.fa-spin').replaceWith('');
    })

});
