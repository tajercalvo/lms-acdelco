$(document).ready(function() {
	TextoCompleto();
	iniciarOwl();
});

function BuscarCursos( contenido ){
	var data = {
		opcn : 'ConsultarListado'
	};
	switch (contenido) {
		case 'tecnicas':
			data.filtro 	=  typeof($('#cur_tecnicas').val()) == 'undefined' ? '' : $('#cur_tecnicas').val() ;
			data.div 			= 'divTecnicas';
			data.especialidad 	= '1';
		break;
		case 'blandas':
			data.filtro 	=  typeof($('#cur_blandas').val()) == 'undefined' ? '' : $('#cur_blandas').val() ;
			data.div 			= 'divBlandas';
			data.especialidad 	= '2';
		break;
		case 'producto':
			data.filtro 	=  typeof($('#cur_producto').val()) == 'undefined' ? '' : $('#cur_producto').val() ;
			data.div 			= 'divProducto';
			data.especialidad 	= '3';
		break;
		case 'marca':
			data.filtro 	=  typeof($('#cur_marca').val()) == 'undefined' ? '' : $('#cur_marca').val() ;
			data.div 			= 'divMarca';
			data.especialidad 	= '4';
		break;
	}
	CargarContenido( data );
}

function CargarContenido( data ){
	console.log("data", data);
	$('#'+data.div).load('controllers/list_courses.php', data , function(response, status, xhr){
        if ( status == "error" ) {
            var msg = "Ha ocurrido un error: ";
            notyfy({
                text: msg + xhr.status + " " + xhr.statusText,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }else{
			iniciarOwl();
            //$('#loadingRespuesta').addClass('hide');
        }
        /*if($('#cantPreg').html() == $('#cantAvailable').html()){
            detenerse();
        }*/
    });
    setTimeout(TextoCompleto, 2000);
}

function TextoCompleto(){
    $('.DivCourse_Detail')
        .mouseover(function() {
            //console.log('#DivTexto_Com'+$(this).attr('dat-Id'));
            $( '#DivTexto_Com'+$(this).attr('dat-Id') ).html($(this).attr('dat-Completo'));
        })
        .mouseout(function() {
           $( '#DivTexto_Com'+$(this).attr('dat-Id') ).html($(this).attr('dat-Corto'));
        });
}

function iniciarOwl(){
	$('.owl-carousel').owlCarousel({
	    loop:false,
		navText : ['anterior','siguiente'],
	    margin:10,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	});
}
