$(document).ready(function(){
  
    // Select Placeholders
    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#type").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#supplier_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#subject_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#specialty_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#list_precios").select2({
        placeholder: "Seleccione un valor",
        allowClear: true
    });
    $("#ask1Over").mouseover(function(){
        $("#ask1").attr("type","text");
    });
    $("#ask2Over").mouseover(function(){
        $("#ask2").attr("type","text");
    });
    $("#ask1Over").mouseout(function(){
        $("#ask1").attr("type","password");
    });
    $("#ask2Over").mouseout(function(){
        $("#ask2").attr("type","password");
    });
});

$('#formPrecios').submit( function( e ){
    e.preventDefault();
    console.log("datos enviados", $(this).serializeArray());
    var datos = {
        opcn:'actualizarParametros',
        parameter_id: $(this).serializeArray()[0].value
    };
    $.ajax({
        url:'controllers/cursos.php',
        data:datos,
        type:'post',
        dataType:'json'
    })
    .done(function( data ){
        if( data.error ){
            notyfy({
                text: data.message,
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }else{
            $('#cerrarBtnPrecio').trigger('click');
            notyfy({
                text: data.message,
                type: 'success' // alert|error|success|information|warning|primary|confirm
            });
        }
    });
} );

$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/cursos.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 8, "desc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});

$( "#formulario_data" ).submit(function( event ) {
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
    event.preventDefault();
});

$("#retornaElemento").click(function() {
    window.location="cursos.php";
});

function valida(){
    var valid = 0;
    $('#course').css( "border-color", "#efefef" );
    //$('#code').css( "border-color", "#efefef" );
    $('#description').css( "border-color", "#efefef" );
    $('#prerequisites').css( "border-color", "#efefef" );
    $('#target').css( "border-color", "#efefef" );

    var valid = 0;
    if($('#course').val() == ""){
        $('#course').css( "border-color", "#b94a48" );
        valid = 1;
    }
    /*if($('#code').val() == ""){
        $('#code').css( "border-color", "#b94a48" );
        valid = 1;
    }*/
    if($('#description').val() == ""){
        $('#description').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#prerequisites').val() == ""){
        $('#prerequisites').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#target').val() == ""){
        $('#target').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg){
    $.ajax({ url: 'controllers/cursos.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function cargaImagen(){
    $('#loading_imagen').removeClass('hide');
    try {
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type == "image/jpeg"){
            if(file.size<1500000){
                var data = new FormData();
                data.append('image_new',file);
                data.append('course_id',$('#idElemento').val());
                data.append('imgant',$('#imgant').val());
                data.append('opcn','CrearImagen');
                var url = "controllers/cursos.php";
                    $.ajax({
                        url:url,
                        type:'post',
                        contentType:false,
                        data:data,
                        processData:false,
                        dataType: "json",
                        cache:false,
                        success: function(data) {
                            if(data.resultado=="SI"){
                                $("#ImgCurso").attr("src","../assets/images/distinciones/"+data.archivo);
                                notyfy({
                                    text: 'La imagen ha sido cambiada correctamente',
                                    type: 'success' // alert|error|success|information|warning|primary|confirm
                                });
                            }else{
                                notyfy({
                                    text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                                    type: 'error' // alert|error|success|information|warning|primary|confirm
                                });
                            }
                            $('#loading_imagen').addClass('hide');
                        }
                    });
            }else{
                notyfy({
                    text: 'La imagen NO ha sido cambiada, el tamaño es demasiado GRANDE',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            notyfy({
                text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }catch(err) {
        $('#loading_imagen').addClass('hide');
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function cargaFile(){
    $('#loading_file').removeClass('hide');
    try {
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        //console.log(file.type);
        if(file.type == "application/pdf"){
            if(file.size<10000000){
                var data = new FormData();
                data.append('image_new',file);
                data.append('course_id',$('#course_file').val());
                data.append('name',$('#name_file').val());
                data.append('opcn','CrearRecurso');
                var url = "controllers/course_detail.php?token="+$('#token_file').val();
                    $.ajax({
                        url:url,
                        type:'post',
                        contentType:false,
                        data:data,
                        processData:false,
                        dataType: "json",
                        cache:false,
                        success: function(data) {
                            if(data.resultado=="SI"){
                                notyfy({
                                    text: 'El archivo ha sido cargado correctamente, recarga la página para observarlo!',
                                    type: 'success' // alert|error|success|information|warning|primary|confirm
                                });
                                setTimeout(function(){ location.reload() }, 1000);
                            }else{
                                notyfy({
                                    text: 'El archivo NO ha sido cargado, revise la extensión del mismo sólo se acepta PDF',
                                    type: 'error' // alert|error|success|information|warning|primary|confirm
                                });
                            }
                            $('#loading_file').addClass('hide');
                        }
                    });
            }else{
                notyfy({
                    text: 'El archivo NO ha sido cargado, el tamaño es demasiado GRANDE; máximo 2 Megas',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            notyfy({
                text: 'El archivo NO ha sido cargado, revise la extensión del mismo sólo se acepta PDF',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }catch(err) {
        $('#loading_file').addClass('hide');
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}

$( "#registrarNota" ).click(function( event ) {
    var Nota = $('#reg_nota').val();
    var Modulo = $('#reg_module_id').val();
    var Course_id = $('#reg_course_id').val();
    var Fecha_nota = $('#reg_fecha').val();
    notyfy({
        text: 'Esta seguro de registrar la nota: '+Nota+' Puntos',
        type: 'confirm',
        dismissQueue: true,
        layout: 'top',
        buttons: ([{
            addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
            text: '<i></i> Aceptar',
            onClick: function ($notyfy) {
                $notyfy.close();
                var data = new FormData();
                data.append('Nota',Nota);
                data.append('Modulo_id',Modulo);
                data.append('opcn','RegistrarNota');
                var url = "controllers/cursos.php";
                $.ajax({
                    url:url,
                    type:'post',
                    contentType:false,
                    data:data,
                    processData:false,
                    dataType: "json",
                    cache:false,
                    success: function(data) {
                        if(data.resultado=="SI"){
                            if(Nota>79){
                                notyfy({
                                    text: 'La nota ha sido registrada correctamente, de click aquí para descargar su certificado.<br><br>Certificado: <a target="_blank" class="btn btn-success" href="certificado.php?course_id='+Course_id+'&date='+Fecha_nota+'">DESCARGAR</a><br><br>También puede descargarlo en cualquier momento en su perfil, o en el panel inicial de información',
                                    type: 'primary' // alert|error|success|information|warning|primary|confirm
                                });
                            }else{
                                notyfy({
                                    text: 'La nota ha sido registrada correctamente, sin embargo los puntos obtenidos no alcanzán para superar el curso; debería intentar nuevamente superar el Objeto Virtual de Aprendizaje',
                                    type: 'information' // alert|error|success|information|warning|primary|confirm
                                });
                            }

                        }else{
                            notyfy({
                                text: 'La nota NO ha podido ser Registrada, confirme por favor su conexión a internet he intente nuevamente',
                                type: 'error' // alert|error|success|information|warning|primary|confirm
                            });
                        }
                    }
                });
            }
        }, {
            addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
            text: '<i></i> Cancelar',
            onClick: function ($notyfy) {
                $notyfy.close();
                notyfy({
                    force: true,
                    text: '<strong>Se ha cancelado la asignación<strong>',
                    type: 'error',
                    layout: 'top'
                });
            }
        }])
    });
    event.preventDefault();
});

$("#btnValidar").click(function(){
    if( validaPreguntas() ){
        var datos = new FormData();
        datos.append( "idAnswer1", $("#id_1").val() );
        datos.append( "idAnswer2", $("#id_2").val() );
        datos.append( "answer1", $("#ask1").val() );
        datos.append( "answer2", $("#ask2").val() );
        $.ajax({
            url:'course_ajax.php',
            type:'post',
            contentType:false,
            data:datos,
            processData:false,
            dataType: "json",
            cache:false,
            success: function(data){
                switch (data.resultado) {
                    case 0:
                        $("#contenedorSeguridad").html("Se ingresaron correctamente las preguntas de seguridad, por favor vuelva a dar clic en <strong>Inscribirme</strong>");
                        $("#inscripcion").show();
                        $("#security_question").hide();
                        $("#btnValidar").hide();
                    break;
                    case 1:
                        $('#ask1').css( "border-color", "#efefef" );
                        $('#ask2').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'La respuesta a la pregunta de seguridad no es correcta',
                            type: 'information' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 2:
                        $('#ask2').css( "border-color", "#efefef" );
                        $('#ask1').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'La respuesta a la pregunta de seguridad no es correcta',
                            type: 'information' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 3:
                        $('#ask1').css( "border-color", "#b94a48" );
                        $('#ask2').css( "border-color", "#b94a48" );
                    break;
                }//fin switch
            }
        });
    }//fin if
});

function validaPreguntas(){
    var valid = 0;
    var retornar = false;
    $('#ask1').css( "border-color", "#efefef" );
    $('#ask2').css( "border-color", "#efefef" );
    if( $('#ask1').val() == "" ){
        $('#ask1').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if( $('#ask2').val() == "" ){
        $('#ask2').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if( valid == 0 ){
        retornar = true;
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
    return retornar;
}//fin funcion validaPreguntas

$("#btnValidarNota").click(function(){
    if( validaPreguntas() ){
        var datos = new FormData();
        datos.append( "idAnswer1", $("#id_1").val() );
        datos.append( "idAnswer2", $("#id_2").val() );
        datos.append( "answer1", $("#ask1").val() );
        datos.append( "answer2", $("#ask2").val() );
        $.ajax({
            url:'course_ajax.php',
            type:'post',
            contentType:false,
            data:datos,
            processData:false,
            dataType: "json",
            cache:false,
            success: function(data){
                switch (data.resultado) {
                    case 0:
                        $("#contenedorSeguridad").html("Se ingresaron correctamente las preguntas de seguridad, por favor vuelva a dar clic en <strong>Registrar nota</strong>");
                        $("#registrarNota").show();
                        $("#registrarNotaSeguridad").hide();
                        $("#btnValidarNota").hide();
                    break;
                    case 1:
                        $('#ask1').css( "border-color", "#efefef" );
                        $('#ask2').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'Por favor verifíque la información que esta pendiente',
                            type: 'warning' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 2:
                        $('#ask2').css( "border-color", "#efefef" );
                        $('#ask1').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'Por favor verifíque la información que esta pendiente',
                            type: 'warning' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 3:
                        $('#ask1').css( "border-color", "#b94a48" );
                        $('#ask2').css( "border-color", "#b94a48" );
                    break;
                }//fin switch
            }
        });
    }//fin if
});


// Descargar archivo
function descargar( archivo, course_file_id ) {
        $.ajax({
            url:'controllers/cursos.php',
            type: 'post',
            dataType: 'json',
            data: {opcn: 'descargar', name_file: archivo, course_file_id: course_file_id },
        })
        .done(function( data ) {
            if( !data.error ){
                   window.open('../assets/FilesCursos/'+archivo, '_blank');
            }else{
                notyfy({
                        text: data.msj,
                        type: 'error' // alert|error|success|information|warning|primary|confirm
                    });
            }
        })
        .fail(function() {
            console.log("error");
        })
}

// Borrar archivo
function borrar(){
    return confirm ("Está seguro de borrar este archivo?");
}