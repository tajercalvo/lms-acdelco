$(document).ready(function(){
    $("#cargos").select2({
        placeholder: "Seleccione la (las) trayectorias",
        allowClear: true
    });
   
    $("#cargos_ed").select2({
        placeholder: "Seleccione la (las) trayectorias",
        allowClear: true
    });
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    }); 
});

//Evento al hacer clic dentro de un a href que contenga dentro un span
    $("a span").click(function() {
            var ID = $(this).attr("id");
            var textoEstado = $(this).text();
            //Validamoe si existe un id para esta ejecución
            if (ID) {
                 bootbox.confirm("Estas seguro de "+textoEstado+ " esta circular", function(result){
           
                if (result) {
                            var data = new FormData();
                            //Capturar el id de la estiqueta a la que se le dio clic
                            
                            data.append('idCircular',ID);
                            data.append('opcn','inactivarCircular');
                            var url = "controllers/circulares_s.php";
                            $.ajax({
                                url:url,
                                type:'post',
                                contentType:false,
                                data:data,
                                processData:false,
                                dataType: "json",
                                cache:false,
                                success: function(data) {
                                    if(data.resultado=="SI"){

                                        if (data.estadoFinal== 1) {
                                            $("#"+ID).removeClass( "label label-success" ).addClass( "label label-danger" );

                                            $("#"+ID).text('Inactivar');
                                            //$("#"+ID).addClass("label label-danger");
                                        }else{
                                            $("#"+ID).removeClass( "label label-danger" ).addClass( "label label-success" );
                                            $("#"+ID).text('Activar');
                                            // $("#"+ID).addClass("label label-success");
                                        }
                                        
                                    }else{
                                        console.log('resultado no es igual a si');
                                    }
                                }//fin Function data
                            });//Fin ajax
                       }//fin if result
                       else{
                             notyfy({
                                text: 'has cancelado la operación',
                                type: 'information' // alert|error|success|information|warning|primary|confirm
                            }); 
                       }//fin else

                    });
        return false;
            }//fin if ID
            else{
                return false;
            }
        
    });// Fin funcion clic a span

$( "#form_Crear" ).submit(function( event ) {
    if(valida()){
       return true;
    }else{
        return false;
    }
    event.preventDefault();
});

function valida(){
    var valid = 0;
    $('#title').css( "border-color", "#efefef" );
    $('#newsletter').css( "border-color", "#efefef" );
    $('#Archivo_new').css( "border-color", "#efefef" );
    var inputFileImage = document.getElementById("Archivo_new");
    var file = inputFileImage.files[0];
    if(file!= undefined){
        if(file.type == "application/pdf" || file.type == "application/zip"){
            if(file.size>2500000){
                $('#Archivo_new').css( "border-color", "#b94a48" );
                valid = 1;
                notyfy({
                    text: 'Por favor verifíque la información que esta pendiente: Hay problemas con el archivo',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            $('#Archivo_new').css( "border-color", "#b94a48" );
            valid = 1;
            notyfy({
                    text: 'Por favor verifíque la información que esta pendiente: Hay problemas con el archivo',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
        }
    }
    if($('#title').val() == ""){
        $('#title').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#newsletter').val() == ""){
        $('#newsletter').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}