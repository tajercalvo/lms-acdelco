$(document).ready(function(){
    $('#loadingRespuesta').addClass('hide');
    //$('#PanelPregunta').hide();

});
var cronometro;
var AvisoMsg = 0;
function detenerse(){
    clearInterval(cronometro);
}
function iniciar(){
    $('#BtnIniciar').hide();
    /*contador_s =0;
    contador_m =0;*/
    contador_s = $('#seg').html();
    contador_m = $('#min').html();
    cronometro = setInterval(
        function(){
            if(contador_s==60)
            {
                contador_s=0;
                contador_m++;
                if(contador_m.toString().length==1){
                    $('#min').html('0'+contador_m);
                    $('#min_time').val('0'+contador_m);
                }else{
                    $('#min').html(contador_m);
                    $('#min_time').val(contador_m);
                }
                if(contador_m==60)
                {
                    contador_m=0;
                }
            }
            if(contador_s.toString().length==1){
                $('#seg').html('0'+contador_s);
                $('#seg_time').val('0'+contador_s);
            }else{
                $('#seg').html(contador_s);
                $('#seg_time').val(contador_s);
            }
            if(parseInt($('#min').html()) > parseInt($('#minAvailable').html()) ){
                detenerse();
                $('#question_increment').val( $('#available_score').val() );
                CargaPregunta($('#review_id').val(),$('#reviews_answer_id').val());
            }
            contador_s++;
            CronometroStop();
        }
        ,1000);
}

function CronometroStop(){
    var ArrayTime = $('#minAvailable').html().split(":");
    var min = ArrayTime[0];
    var seg = ArrayTime[1];
    var minSeg = ArrayTime[0]+""+ArrayTime[1];
    var minSegVar = ArrayTime[0]-2+""+ArrayTime[1];
    var minSegTot = $('#min_time').val()+""+$('#seg_time').val();
    if(parseInt(minSeg)<parseInt(minSegTot)){
        notyfy({
            text: '<strong>HA SOBREPASADO EL TIEMPO LÍMITE PARA ESTA EVALUACIÓN, LO SENTIMOS PERO NO HA LOGRADO RESPONDER ESTA EVALUACIÓN</strong> ',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        CargaRespuesta();
    }else{
        if(parseInt(minSegVar)<parseInt(minSegTot)){
            if(AvisoMsg == 0){
                notyfy({
                    text: '<strong>Esta por llegar al LÍMITE DE TIEMPO, por favor conteste la evaluación antes de terminarlo, de lo contrario su puntaje será de cero.</strong> ',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
                AvisoMsg = 1;
            }
        }
    }
}

function IniciarEvaluacion(){
    $('#loadingRespuesta').removeClass('hide');
    $('#PanelPregunta').html('');
    var data = new FormData();
    data.append('review_id',$('#review_id').val());
    data.append('available_score',$('#available_score').val());
    data.append('opcn','IniciarEvaluacion');
    var url = "controllers/evaluar.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                iniciar();
                $('#reviews_answer_id').val(data.reviews_answer_id);
                CargaPregunta($('#review_id').val(),data.reviews_answer_id);
                return;
            }
            if(data.resultado=="NOID"){
                notyfy({
                    text: data.msj,
                    type: 'information' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(data.resultado=="NO") {
                notyfy({
                    text: data.msj,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
   
}

function CargaPregunta(review_id,reviews_answer_id){
    $('#PanelPregunta').load('controllers/evaluar.php', {opcn:'ConsultarPregunta',question_increment:$('#question_increment').val(),available_score:$('#available_score').val(),reviews_answer_id:reviews_answer_id}, function(response, status, xhr){
        if ( status == "error" ) {
            var msg = "Ha ocurrido un error: ";
            notyfy({
                text: msg + xhr.status + " " + xhr.statusText,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }else{
            $('#loadingRespuesta').addClass('hide');
        }
        /*if($('#cantPreg').html() == $('#cantAvailable').html()){
            detenerse();
        }*/
    });
}

function CargaRespuesta(){
    var review_id = $('#review_id').val();
    var reviews_answer_id = $('#reviews_answer_id').val();
    $('#PanelPregunta').load('controllers/evaluar.php', {opcn:'ConsultarRespuesta',review_id:review_id,reviews_answer_id:reviews_answer_id}, function(response, status, xhr){
        if ( status == "error" ) {
            var msg = "Ha ocurrido un error: ";
            notyfy({
                text: msg + xhr.status + " " + xhr.statusText,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }else{
            $('#loadingRespuesta').addClass('hide');
        }
        detenerse();
    });
}

$( "#formulario_respuestas" ).submit(function( event ) {
  if(confirm('¿Esta seguro de enviar la evaluación?')){
    var Question_val = parseInt($('#question_increment').val());
    $('#loadingRespuesta').addClass('show');
    $('#crearElemento').hide();
    $('#loading_evaluacion').addClass('show');
    $('#question_increment').val(Question_val+1);
    $('#cantPreg').html(Question_val+1);
    $.ajax({ url: 'controllers/evaluar.php',
        data: $('#formulario_respuestas').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            
            if( !data.error ){
                notyfy({
                    text: data.msj,
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                CargaRespuesta();
            }else{
                notyfy({
                    text: data.msj,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
    //$('#loading_evaluacion').removeClass('show');
    $('#loadingRespuesta').removeClass('show');
    $('#loadingRespuesta').addClass('hide');
    event.preventDefault();
  }else{
      notyfy({
          text: 'ha cancelado la operación',
          type: 'error' // alert|error|success|information|warning|primary|confirm
      });
      $('#loadingRespuesta').removeClass('show');
      $('#loadingRespuesta').addClass('hide');
      return false;
  }
});
