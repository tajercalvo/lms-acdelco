    $("#btn_inscription").hide();
    var CameraMov;
    var SAMPLE_SERVER_BASE_URL = 'https://www.gmacademy.co/assets/vct/'

    var apiKey    = '46039172',
    sessionId = $('#sessionId').val();//'1_MX40NTY5NjgzMn5-MTQ4NzI2MzkzMzA2M35NZ3FoNzVsMVVnSTVPUitHRlVUbFcxTjF-fg',
    token     = $('#token').val();//'T1==cGFydG5lcl9pZD00NTY5NjgzMiZzaWc9ZWQxMDVmOTA1YmFlOGJmNjNiMTJmYTk2OTk3ZDk1NDlhMWZhNmIzZDpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOamd6TW41LU1UUTROekkyTXprek16QTJNMzVOWjNGb056VnNNVlZuU1RWUFVpdEhSbFZVYkZjeFRqRi1mZyZjcmVhdGVfdGltZT0xNDg3MjYzOTUzJm5vbmNlPTAuMzQ0NTc4MTczMTgyNTgwNTUmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTQ4NzI4NTU1Mg==';
    // Replace this with the ID for your Chrome screen-sharing extension, which you can
    // get at chrome://extensions/:
    var extensionId = 'ijgfggkmnahehfeimghpomadjmmkiphd';
    // If you register your domain with the the Firefox screen-sharing whitelist, instead of using
    // a Firefox screen-sharing extension, set this to the Firefox version number, such as 36, in which
    // your domain was added to the whitelist:
    var ffWhitelistVersion; // = '36';
    var session = OT.initSession(apiKey, sessionId);
    session.connect(token, function(error) {
      if (error) {
        console.log('Error connecting to session: ' + error.message);
        return;
      }
      if($('#teacher_id').length){
        // publish a stream using the camera and microphone:
        var publisher = OT.initPublisher('camera-publisher',{
          insertMode: "append",
          name: 'Transmisión VCT',
          width: '180px',
          height: '135px'
        });
        session.publish(publisher);
        document.getElementById('shareBtn').disabled = false;
        CameraMov = setInterval(CambiaDrag,10000);

      }
    });
    session.on('streamCreated', function(event) {
      if (event.stream.videoType === 'screen') {
        // This is a screen-sharing stream published by another client
        var subOptions = {
          insertMode: "append",
          width: '900px',
          height: '600px'
        };
        session.subscribe(event.stream, 'screen-subscriber', subOptions);
        $('#camera-subscriber').css('top','-230px');
        $('#camera-subscriber').css('left','-27px');
        CameraMov = setInterval(CambiaDrag,10000);
      } else {
        // This is a stream published by another client using the camera and microphone
        session.subscribe(event.stream, 'camera-subscriber', {
          insertMode: "append",
          name: 'Transmisión VCT',
          width: '180px',
          height: '135px'
        });
        CameraMov = setInterval(CambiaDrag,10000);
      }
    });
    // For Google Chrome only, register your extension by ID,
    // You can find it at chrome://extensions once the extension is installed
    OT.registerScreenSharingExtension('chrome', extensionId, 2);
    function screenshare() {
      OT.checkScreenSharingCapability(function(response) {
        console.info(response);
        if (!response.supported || response.extensionRegistered === false) {
          alert('This browser does not support screen sharing.');
        } else if (response.extensionInstalled === false
            && (response.extensionRequired || !ffWhitelistVersion)) {
          alert('Please install the screen-sharing extension and load this page over HTTPS.');
        } else if (ffWhitelistVersion && navigator.userAgent.match(/Firefox/)
          && navigator.userAgent.match(/Firefox\/(\d+)/)[1] < ffWhitelistVersion) {
            alert('For screen sharing, please update your version of Firefox to '
              + ffWhitelistVersion + '.');
        } else {
          // Screen sharing is available. Publish the screen.
          // Create an element, but do not display it in the HTML DOM:
          var screenContainerElement = document.createElement('div');
          var screenSharingPublisher = OT.initPublisher(
            screenContainerElement,
            { videoSource : 'screen' },
            function(error) {
              if (error) {
                alert('Something went wrong: ' + error.message);
              } else {
                session.publish(
                  screenSharingPublisher,
                  function(error) {
                    if (error) {
                      alert('Something went wrong: ' + error.message);
                    }
                  });
              }
            });
          }
        });
      CameraMov = setInterval(CambiaDrag,2000);
    }

function startArchive(){
  $.post(SAMPLE_SERVER_BASE_URL + '/start/' + sessionId);
  $('#StartBTN').hide();
  $('#StopBTN').show();
}
function stopArchive(){
  $.post(SAMPLE_SERVER_BASE_URL + '/stop/' + archiveID);
  $('#StopBTN').hide();
  $('#StartBTN').show();
}
function CambiaDrag(){
  clearInterval(CameraMov);
  if($('#camera-publisher').length){
    $('#camera-publisher').draggable();
    //$('#camera-publisher').css('width','180px');
    //$('#camera-publisher').css('height','135px');
  }
  if($('#camera-subscriber').length){
    $('#camera-subscriber').draggable();
    //$('#camera-subscriber').css('width','180px');
    //$('#camera-subscriber').css('height','135px');
  }
  console.log("camera draggablle");
}

$(document).ready(function(){
    /*$('#StopBTN').hide();
    $('#StartBTN').show();
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop:false,
        nav:false,
        margin:10,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            960:{
                items:5
            },
            1200:{
                items:6
            }
        }
    });*/
    // Select Placeholders
    $("#contenido_interno").css("visibility","visible");
    $.ajaxSetup({"cache":false});
    setInterval("mostrarHora('mostrarTiempo')",1000);
    clic = false;
    setInterval("cargarComentarios2()",5000);
});


//Cargar mensajes cada cierto tiempo
function cargarComentarios2() {
    console.log(clic);
    //Validar si se dio clic en l pestaña comunicaciones
    if (clic) {
      var vct = $("#idvct").val();
      //console.log('consultar id '+ultimo_mendaje_id_BD);
  var data = new FormData();
    data.append('idvcts',vct);
    data.append('lastId',ultimo_mendaje_id_BD);
    data.append('opcn','comments2');
    var url = "vct_detail_ajax.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        async: true,
        cache:false,
        success: function(data) {
         console.log('si ajax');
          //Capturamos el ultimo id del mensaje que biene de la base de datos para compararlo con el ultimo que se consulto, si son iguales no se hace nada
          var  cantidad = $(data).length - 1;

          if (cantidad >= 0) {
                          ultimo_mendaje_id_BD2 = data[cantidad]['comments_id'];
                          //console.log('id mensaje que biene de ajax automaticamente'+ultimo_mendaje_id_BD2);
                          //console.log('Ultimo id mensaje '+ultimo_mendaje_id_BD);
                          if (ultimo_mendaje_id_BD == ultimo_mendaje_id_BD2 ) { console.log('NO hay mensaje snuevos'); return false;}
                          ultimo_mendaje_id_BD = ultimo_mendaje_id_BD2;
                          for(var i in  data) {
                                                var mensaje = '<li class="row row-merge border-none" id="'+data[i]['comments_id']+'">';
                                              mensaje = mensaje +'<div class="col-md-12">';
                                                mensaje = mensaje +'<div class="innerAll padding-none">';
                                                 mensaje = mensaje +' <div class="media">';
                                                   mensaje = mensaje +' <!--<a href="ver_perfil.php?back=usuarios&id=" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/" alt="Image" style="width: 50px;"/></a>-->';
                                                    mensaje = mensaje +'<div class="media-body">';
                                                      mensaje = mensaje +'<div><small class="pull-right text-primary-light"><i class="fa fa-fw icon-envelope-fill-1 text-primary"></i>'+data[i]['date_comment']+'<em></em></small></div>';
                                                      mensaje = mensaje +'<strong class="text-primary">'+data[i]['first_name']+' '+data[i]['last_name']+'</strong><br/>';
                                                      mensaje = mensaje +'<small> '+data[i]['comment_text']+'</small>';
                                                    mensaje = mensaje +'</div>';
                                                  mensaje = mensaje +'</div>';
                                                mensaje = mensaje +'</div>';
                                              mensaje = mensaje +'</div>';
                                            mensaje = mensaje +'</li>';

                                            $("#cajaComentarios").append(mensaje);
                                            var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
                                            $("#cajaComentarios").scrollTop(alturaScroll);
                                            $("#cajaComentarios li").first().remove(); //elminamos el primer mensaje de la lista para solo mantener siempre la misma cantidad y no poner lento el chat
                                        }

                      }else{console.log('Sin mensajes')}// Fin cantidad mayor a 0
        }// Fin function data
    });
  }// Fin If clic, comunicaciones
  else{return false;}


}

/*
Funcion que evalua el tiempo transcurrido y pendiente y cambia el estilo de acuerdo al resultado
@tiempo= es el tiempo que se esta tomando para un estado, 15 min es el readye para ejecutar los cambios
@idDiv= es el id del DIV donde se esta mostrando el tiempo transcurrido
@etapa= es el momento que determina el estado del boton de inscripcion
  1. Es el tiempo entre la hora actual y la hora programada de la capacitacion si aun no a empezado
  2. Es el tiempo despues que empezo la capacitacion
*/
function calcularTiempo(tiempo,idDiv,etapa){
  console.log(tiempo+":"+etapa+"|");
  if(etapa == 1 && tiempo > 900000){
    $("#btn_inscription").hide();
  }
  if(etapa == 1 && tiempo >= 0 && tiempo <= 300000){
    $("#"+idDiv).attr("style","color: orange;");
    $("#btn_inscription").show();
  }
  if(etapa == 1 && tiempo >= 300773 && tiempo <= 900000){
    $("#"+idDiv).attr("style","color: orange;");
    $("#btn_inscription").hide();
  }
  if(etapa == 2 && tiempo >= 0 && tiempo <= 900000){
    $("#"+idDiv).attr("style","color: green;");
    $("#btn_inscription").show();

  }
  if(etapa == 2 && tiempo > 900000){
    if($("#btn_inscription").attr("href") != "#"){
      $("#btn_inscription").attr({"href":"#modalVCT","data-toggle":"modal"});
      $("#btn_inscription").show();
    }
  }
}//fin funcion calcularTiempo

// Cargar los mensajes al hacer clic en la pestaña comunicaciones o cuando se le de clic en el boton actualizar chat
$("#list_chat").click(function(){
  Cargar_chat();
});

//Carga por primera vez o actualiza la ventana de chat
function Cargar_chat() {

  $('#cajaComentarios').html('');
  $('#cargando_mensajes').html('<span class="label label-success">Consultando mensajes, por favor espere...</span><img style="width: 15px; " src="../assets/loading.gif">');
  var vct = $("#idvct").val();

  var data = new FormData();
    data.append('idvcts',vct);
    data.append('lastId',vct);
    data.append('opcn','comments');
    var url = "vct_detail_ajax.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        async: true,
        cache:false,
        success: function(data) {
            //if(data.resultado=="SI"){
                console.log($(data.mensajes).length);
                nombre = data.datos_usuario['first_name']+' '+data.datos_usuario['last_name'];
                id_usuario = data.datos_usuario['user_id']
                 // console.log(data[0]['comments_id']);
          //Capturo la ultima posicion del array
          var  cantidad = $(data.mensajes).length;
          if(cantidad>=1){cantidad = $(data.mensajes).length - 1}
            else{
                $('#cargando_mensajes').html('<div class="btn-group btn-group-xs pull-right"><button class="btn btn-primary" onclick="Cargar_chat()"><i class="fa fa-refresh " ></i> Actualizar chat</button></div>');
                console.log('chat sin mensajes');
                return false
                };
          console.log(cantidad);

          //Se captura el ultimo id de mensaje que biene de la base d edatos, para despues buscar los siguientes
          ultimo_mendaje_id_BD = data.mensajes[cantidad]['comments_id'];
          clic = true;

          console.log('Ultimo id mensaje primera carga '+ultimo_mendaje_id_BD);
          //inicio mostrar los chats
          for(var i in  data.mensajes) {
            //Validar si el id de la base que biene de la de datos es igual al que tiene la seccion iniciada, para cambiar la clase del estado del mensaje, con el icono de un chulito o una carta
            if (data.mensajes[i]['user_id'] == id_usuario ) { var estadomensaje = '<i class="fa fa-fw icon-checkmark-thick text-primary"></i>';}else{ estadomensaje = '<i class="fa fa-fw icon-envelope-fill-1 text-primary"></i>'; }

                                var mensaje = '<li class="row row-merge border-none" id="'+data.mensajes[i]['comments_id']+'">';
                              mensaje = mensaje +'<div class="col-md-12">';
                                mensaje = mensaje +'<div class="innerAll padding-none">';
                                 mensaje = mensaje +' <div class="media">';
                                   mensaje = mensaje +' <!--<a href="ver_perfil.php?back=usuarios&id=" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/" alt="Image" style="width: 50px;"/></a>-->';
                                    mensaje = mensaje +'<div class="media-body">';
                                      mensaje = mensaje +'<div><small class="pull-right text-primary-light">'+estadomensaje+data.mensajes[i]['date_comment']+'<em></em></small></div>';
                                      mensaje = mensaje +'<strong class="text-primary">'+data.mensajes[i]['first_name']+' '+data.mensajes[i]['last_name']+'</strong><br/>';
                                      mensaje = mensaje +'<small> '+data.mensajes[i]['comment_text']+'</small>';
                                    mensaje = mensaje +'</div>';
                                  mensaje = mensaje +'</div>';
                                mensaje = mensaje +'</div>';
                              mensaje = mensaje +'</div>';
                            mensaje = mensaje +'</li>';

                            $("#cajaComentarios").append(mensaje);
                        }// Fin for

                        var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
                        $("#cajaComentarios").scrollTop(alturaScroll);
                        //$("#cajaComentarios li").first().remove(); //elminamos el primer mensaje de la lista para solo mantener siempre la misma cantidad y no poner lento el chat
                        $('#cargando_mensajes').html('<div class="btn-group btn-group-xs pull-right"><button class="btn btn-primary" onclick="Cargar_chat()"><i class="fa fa-refresh " ></i> Actualizar chat</button></div>');

            //}
        }
    });
}

//fin mouseleave
/*
Funcion que genera un temporizador que mostrarHora
  - tiempo faltante para empezar un curso y asi mismo inscribirseCurso
  - si el curso ya inicio, el tiempo que ha transcurrido
*/
function mostrarHora(idSeleccion){
  var actual = new Date();
  var d = new Date($("#fecha_hora").val());
  var msMinute = 1000 * 60;
  var msHour = msMinute * 60;
  var msDay = msHour * 24;
  var actualMS = actual.getTime();
  var dMS = d.getTime();
  var tiempoFinal = 0;
  if (actualMS < dMS) {
    var diferencia = dMS - actualMS;
    calcularTiempo(diferencia,"mostrarTiempo",1);
    var dias = Math.floor(diferencia / msDay);
    diferencia = diferencia - (dias * msDay);
    var horas = Math.floor(diferencia / msHour);
    diferencia = diferencia - (horas * msHour);
    var minutos = Math.floor(diferencia / msMinute);
    diferencia = diferencia - (minutos * msMinute);
    var segundos = Math.floor(diferencia / 1000);
    tiempoFinal = dias+" dias, "+horas+":"+(minutos < 10 ? "0" : "")+minutos+":"+(segundos < 10 ? "0" : "")+segundos;
  }else{
    var diferencia = actualMS - dMS;
    calcularTiempo(diferencia,"mostrarTiempo",2);
    var dias = Math.floor(diferencia / msDay);
    diferencia = diferencia - (dias * msDay);
    var horas = Math.floor(diferencia / msHour);
    diferencia = diferencia - (horas * msHour);
    var minutos = Math.floor(diferencia / msMinute);
    diferencia = diferencia - (minutos * msMinute);
    var segundos = Math.floor(diferencia / 1000);
    tiempoFinal = dias+" dias, "+horas+":"+(minutos < 10 ? "0" : "")+minutos+":"+(segundos < 10 ? "0" : "")+segundos;
  }
  document.getElementById(idSeleccion).innerHTML = tiempoFinal;
}//fin funcion mostrarHora

/*
Realiza la carga asincrona de los mensajes
*/

//Inicio enviar mensaje al chat
//Al presinonar boton de chat
$("#botonChat").click(function(){
  enviarMensaje();
});

// al presionar enter dentro de la caja de texto del chat
$("#texto_comentario").keypress(function(e) {
       if(e.which == 13) {
          enviarMensaje();
          return false;
       }
    });

//funcion qu eenvia los mensajes
function enviarMensaje(){

      //var chat = $('#texto_comentario').val().length;
      var chat = $("#texto_comentario").val();
      if (chat!='') {
      var usuarioID = id_usuario;
      var datos = $("#comentario").serialize();
      //nombre = $("#first_name").text()+' '+$("#last_name").text();

      var id = $("#cajaComentarios li").last().attr("id");//id del ultimo comentario en el chat
      var f = new Date();
      id_msj_usuario = usuarioID +""+f.getTime();
      datos = datos + '&id_msj_usuario='+id_msj_usuario;

    //console.log("datos = "+datos);

       var mensaje = '<li class="row row-merge border-none" id="'+id_msj_usuario+'">';
          mensaje = mensaje +'<div class="col-md-12">';
            mensaje = mensaje +'<div class="innerAll padding-none">';
             mensaje = mensaje +' <div class="media">';
               mensaje = mensaje +' <!--<a href="ver_perfil.php?back=usuarios&id=" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/" alt="Image" style="width: 50px;"/></a>-->';
                mensaje = mensaje +'<div class="media-body">';
                  mensaje = mensaje +'<div id=estado'+id_msj_usuario+'><small class="pull-right text-primary-light"><i class="fa fa-fw icon-refresh-location-fill text-primary"></i> Enviando<em></em></small></div>';
                  mensaje = mensaje +'<strong class="text-primary">'+nombre+'</strong><br/>';
                  mensaje = mensaje +'<small> '+chat+'</small>';
                mensaje = mensaje +'</div>';
              mensaje = mensaje +'</div>';
            mensaje = mensaje +'</div>';
          mensaje = mensaje +'</div>';
        mensaje = mensaje +'</li>';

        $("#cajaComentarios").append(mensaje);
        mensaje = '';
        //$("#cajaComentarios").animate({ scrollTop: $('#cajaComentarios')[0].scrollHeight}, 1000);
        var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
        $("#cajaComentarios").scrollTop(alturaScroll);
        $('#texto_comentario').val('');

          $.ajax({
            url     : "controllers/vct_detail.php",
            data    : datos,
            type    : "post",
            dataType: "json",
            async: true,
            success: function(data){
              //var res0 = data.resultado;
                  if (data.resultado == "SI") {

                      //console.log('mensaje enviado');
                      $('#estado'+data.id_comments_users).html('<small class="pull-right text-primary-light"><i class="fa fa-fw icon-checkmark-thick text-primary"></i> '+data.fecha+' <em></em></small>');
                      $('audio')[0].play();
                      //var res0 = data.resultado;
                      var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
                      $("#cajaComentarios").scrollTop(alturaScroll);
                      if (cantidad_de_mensajes = $("#cajaComentarios li").length > 20) {
                        $("#cajaComentarios li").first().remove();// elminamos el primer mensaje de la lista para solo mantener siempre la misma cantidad y no poner lento el chat
                      }

                      //$( "#cajaComentarios li" ).first().removeClass( "row row-merge border-none" );
                      //$("#cajaComentarios").animate({ scrollTop: $('#cajaComentarios')[0].scrollHeight}, 1000);
                  }else{
                    $('#estado'+id_msj_usuario).html('<small class="pull-right text-primary-light"><i class="fa fa-fw icon-delete-symbol text-primary"></i> <em></em></small>');
                  }
            }
          })
          .fail(function() {
             $('#estado'+id_msj_usuario).html('<small class="pull-right text-primary-light"><i class="fa fa-fw icon-delete-symbol text-primary"></i> <em></em></small>');
      });
    }else{
      return false;
    }

}

$("#buscarUsuario").keyup(function(){
  var nombre = $("#buscarUsuario").val();
  var idsearch = $("#idsearch").val();
  var schedule_id = $("#schedule_id").val();
  cargarDatosAjax('asistentes','search',idsearch,nombre,schedule_id);
});

/*
Realiza la peticion ajax y recarga el div seleccionado
*/
function cargarDatosAjax(divs,opcion,ids,datosajax,schedule_id){
  $("#"+divs).load('vct_detail_ajax.php',{opcn:opcion, idvcts:ids, datos:datosajax, schedule:schedule_id},function(response, status, xhr){
    if ( status == "error" ) {
        var msg = "Sorry but there was an error: ";
        console.log( msg + xhr.status + " " + xhr.statusText );
    }
  });
}//fin funcion cargarDatosAjax
/*
Carga el o los ultimos mensajes agregados a un chat
*/
function cargarComentarios(){
  var id = $("#cajaComentarios li").last().attr("id");//id del ultimo comentario en el chat
  var vct = $("#idvct").val();
  $.post('vct_detail_ajax.php',{opcn:'comments',idvcts:vct,lastId:id},function(datos,status,xhr){
    if(status== 'success' && datos != ''){
      $("#cajaComentarios").append(datos);
      var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
                      $("#cajaComentarios").scrollTop(alturaScroll);
      //$("#cajaComentarios").animate({ scrollTop: $('#cajaComentarios')[0].scrollHeight}, 1000);
    }
  });
}//fin cargarComentarios
/*
Carga el o los inscritos al curso
*/
function cargarInscritos(){
  var id = $("#asistentes li").last().attr("id");//id del ultimo inscrito al curso
  var vct = $("#idvct").val();
  $.post('vct_detail_ajax.php',{opcn:'inscripcion',idvcts:vct,last_id:id},function(datos,status,xhr){
    if(status== 'success' && datos != ''){
      $("#asistentes").append(datos);
    }
  });
}//fin cargarComentarios
/*
*Carga informacion en una modal
*/
function DatosVideo(var_src, var_title){
    var TiempoVideo = setTimeout(function(){ Carga_DatosVideo(var_src, var_title); }, 2000);
}

function Carga_DatosVideo(var_src, var_title){
    $("#TituloVideo").html(var_title);
    $("#cartoonVideo").attr('src', var_src);
    var video = document.getElementById("cartoonVideo");
    video.play();
    console.log(var_src);
}
