$(document).ready(function(){
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
     $("#charge").select2({
        placeholder: "Seleccione una trayectoria",
        allowClear: true
    });
      $("#dealer").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
   
});

$("#descargar_excel").click(function(event) {

       $("#datos_a_enviar").val( $("<div>").append( $("#tab_Navegacion").eq(0).clone()).html());
        $("#FormularioExportacion").submit();
         //console.log( $("#datos_a_enviar").val());
    });

$("#descargar_TODOS").click(function(event) {
       $("#datos_a_enviar_tab_TODOS").val( $("<div>").append( $("#tab_TODOS").eq(0).clone()).html());
        $("#FormularioExportacion_TODOS").submit();
         //console.log( $("#datos_a_enviar").val());
    });

$("#descargar_excel_tab_GMAcademy").click(function(event) {

       $("#datos_a_enviar_tab_GMAcademy").val( $("<div>").append( $("#tab_GMAcademy").eq(0).clone()).html());
        $("#FormularioExportacion_tab_GMAcademy").submit();
        // console.log( $("#datos_a_enviar").val());
    });


// $( "#descargar" ).click(function() {
//     $('#js-tabla tbody').remove();

//     acumula = 0;
//     cien = 0;
//     sum = 0;

//     document.getElementById("miTabla_descarga").innerHTML = '';
//     $("#barra_progreso").width(0);
//     $("#descargando_reporte").text('');
    

//     //id_descarga = $( "#cursosF" ).val();

//     //$("#cargando").css("visibility","visible");
//     //$("#cargando").text("Realizando la consulta, por favor espere…");

//     var data = new FormData();
//     data.append('opcn','descargar');    
//     data.append('start_date_day',$("#start_date_day").val());
//     data.append('end_date_day',$("#end_date_day").val());
    
//     $.ajax({
//         url:"controllers/rep_navegacion_beta.php",
//         type:'post',
//         contentType:false,
//         data:data,
//         processData:false,
//         dataType: "text",
//         //dataType: "json",
//         cache:false,
//         success: function(data) {
  
//              if(data==""){
            
//                     //$("#cargando").text("consulta si resultados");

                 
//                 }else{

//                         $("#progreso_visible").css("visibility","visible");
//                         // convertimos el texto que biene de php en un array, la letra p es el delimitador del array
//                         res = data.split("::");

//                         console.log('Resultado total '+(res.length-1));

//                         //Contamos el array y le restamos 1 por que el ultimo array biene vacio, asi obtenemos la cantidad
//                         // de cursos a concultar
//                         registros = res.length-1;

//                         // Dividimos la cantidad de registros etre 100 para asignarlo por parte a la barra de progreso
//                         cien = 100 /registros;

//                         //Recorremos el array para obtener el siguiente resultado a consultar
//                         for(var i in res) {

//                                 console.log(res[i]);
//                                 descargar_siguiente(res[i]);
//                         } 

//                             // else{

//                             //       $('#miTabla_descarga').append(data);
//                             //       $("#cargando").css("visibility","hidden");

//                             // }
//                 } //Fin Else
//         } // Fin function dta
//     }); // FIn ajax
    
// }); // Fin  FUnction click

$("#descargar").click(function(){

        $('#review_id').css( "border-color", "#ffffff" );
        $('#start_date_day').css( "border-color", "#ffffff" );
        $('#end_date_day').css( "border-color", "#ffffff" );

        inicio= new Date($('#start_date_day').val());
        finalq= new Date($('#end_date_day').val());

        if(inicio>finalq){
                    bootbox.alert("La fecha inicial no puede ser menor que la fecha final", function(result) 
                {
                    
                });
                    return false;
        }


        var  charge = $('#charge').val();
        var  dealer = $('#dealer').val();
        var start_date_day = $('#start_date_day').val();
        var end_date_day = $('#end_date_day').val();

     if ( valida() ) {
            var url = "rep_navegacion_excel_beta.php?start_date_day=" + start_date_day + "&end_date_day=" + end_date_day + "&" + "&dealer=" + dealer + "&"+"&charge=" + charge + "&";
            //WINDOWS.OPEN, PARA REDIRECIONAR A OTRA PAGINA CON TARGET BLANK
            window.open(url, '_blank');

            //DESCARGAR EN LA MISMA PAGINA
            //document.location.href = "rep_calificacion_excel.php?start_date_day=" + start_date_day + "&end_date_day=" + end_date_day + "&";


            

        }else{
           
           bootbox.alert("Por favor completa la información antes de realizar la descarga, escoge una fecha inicial, una fecha final y una encuesta", function(result) {
                //Codigo...            
                });

            $('#start_date_day').css( "border-color", "#b94a48");
            $('#end_date_day').css( "border-color", "#b94a48" );

            
    }
    
    
    
});

function valida() {

    s_date = document.getElementById("start_date_day").value;
    e_date = document.getElementById("end_date_day").value;
   
    $('#start_date_day').css( "border-color", "#ffffff" );
    $('#end_date_day').css( "border-color", "#ffffff" );
    //$('#identificacion').css( "border-color", "#ffffff" );


    if( s_date == '' & e_date == '') {
    $('#start_date_day').css( "border-color", "#b94a48" );
    $('#end_date_day').css( "border-color", "#b94a48");
    //$('#identificacion').css( "border-color", "#b94a48" );
    $( "#start_date_day" ).focus();
    return false;
    } else if( e_date == '') {
    $('#end_date_day').css( "border-color", "#b94a48" );
    $( "#end_date_day" ).focus();
    return false;
    } else if( s_date == '') {
    // alert('Complete la informacion antes de consultarla');
    $('#start_date_day').css( "border-color", "#b94a48" );
    //$('#identificacion').css( "border-color", "#b94a48" );
    $( "#start_date_day" ).focus();
    return false;
    }else{
        return true;
    }
    
    }

