$(document).ready(function(){

    $('#imagen_mini').setImagen( 'imagen_mini1' );
    $('#image_profile').setImagen( 'image_profile1' );
    $('#image_exterior').setImagen( 'image_exterior1' );
    $('#image_interior').setImagen( 'image_interior1' );
    $('#image_desempeno').setImagen( 'image_desempeno1' );
    $('#image_tecnologia').setImagen( 'image_tecnologia1' );
    $('#image_seguridad').setImagen( 'image_seguridad1' );
    $('#image_audio').setImagen( 'image_audio1' );
    $('#img_link_recorrido').setImagen( 'img_link_recorrido1' );

    $("#categoria").select2({
        placeholder: "Seleccione una categoria de segmento",
        allowClear: true
    });
    $("#segmento_sel").select2({
        placeholder: "Seleccione un segmento de vehiculo",
        allowClear: true
    });
    $("#biblioteca").select2({
        placeholder: "Seleccione un AudioLibro",
        allowClear: true
    });
    $("#library_id").select2({
        placeholder: "Seleccione una Relación con Biblioteca",
        allowClear: true
    });

    if ( $('#opcion').val() == 0 ) {
        limpiar();
    } else {
        $('#nuevoAudio').hide();
        $('#nuevaBiblioteca').hide();
    }

   
});

// *** VARIABLES GLOBALES ***
var nombre_vehiculo=''; 
// *** ***

//SE CAPTURA EL AUDIOLIBRO DE BIBLIOTECA
$('#biblioteca').change( function(){

    var biblioteca_id = $(this).val();

    if (biblioteca_id == 0) {
        $('#nuevoAudio').show();
        $('#nuevaBiblioteca').hide();
    } else{
        if (biblioteca_id > 0) {
            $('#nuevoAudio').hide();
            $('#nuevaBiblioteca').show();
        }
    }

} );

//PESTAÑAS DE SEGMENTO Y PRODUCTO
$('.boton-pestana').click(function () {
    // cambiar colores a los botones
    $('.boton-pestana').removeClass('active');
    $(this).addClass('active');
    var id = $(this).children().children().attr("data-mostrar"); // obtenemos el id del div que vamos a mostrar
    $('.contenido').css( "display", "none" ); // ocultamos todos los div con la clae contenido modal
    $('#'+id).hide().slideDown('slow');
});

//FORMULARIO CREAR SEGMENTO
$( '#formulario_segmento' ).submit( function( e ){
    e.preventDefault();

    if( $("#segmento").val() == "" ){
        notyfy({
          text: 'Debe completar el nombre del segmento de vehiculos.',
          type: 'error' 
        });
    }
    else{

        var inputFileI = document.getElementById("imagen_mini");
        var fileI = inputFileI.files[0] ? inputFileI.files[0] : '' ;
        var datos = new FormData();

            datos.append("opcn","guardarSegmento");
            datos.append("id_edicion",$('#edicion').val() );
            datos.append("segmento", $('#segmento').val() );
            datos.append("categoria", $('#categoria').val() );
            datos.append( "imagen_mini", fileI );

        $.ajax({
            url:'controllers/producto.php',
            data: datos,
            cache: false,
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var res0 = data.resultado;
                if(res0 == "TRUE"){
                    reset( 'imagen_mini' );
                    $("#imagen_mini1").attr( "src","../assets/products/images/mini/upload_image.png");
                    notyfy({
                            text: 'La operación ha sido completada satisfactoriamente',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                } else {
                    notyfy({
                            text: 'No se ha logrado guardar el Segmento del Producto.',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                }
            }
        });

    }

} );

//FORMULARIO CREAR PRODUCTO
$( '#formulario_producto' ).submit( function( e ){
    e.preventDefault();

    if( $("#vehiculo").val() == "" ){
        notyfy({
          text: 'Debe completar el nombre del vehiculo.',
          type: 'error' 
        });
    }
    else{
        var datos = new FormData();
        nombre_vehiculo = $('#vehiculo').val();

            datos.append("opcn","guardarProducto");
            datos.append("vehiculo", $('#vehiculo').val() );
            datos.append("segmento_sel", $('#segmento_sel').val() );
            datos.append("idproducto", $('#idproducto').val() );

        $.ajax({
            url:'controllers/producto.php',
            data: datos,
            cache: false,
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var res0 = data.resultado;
                if(res0 == "TRUE"){
                    if ( $('#opcion').val() == 0 ) {
                        $('#crearProducto').attr('disabled', true);
                        $('#crearImagenes').attr('disabled', false);
                        $('#idproducto').val(data.id);
                    }
                    notyfy({
                            text: 'Se han cargado satisfactoriamente los Datos Basicos del Producto.',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                } else {
                    notyfy({
                            text: 'No se ha logrado guardar los Datos Basicos del Producto.',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                }
            }
        });

    }

} );

//FORMULARIO CREAR IMAGENES DE PRODUCTO
$( '#formulario_imagenes' ).submit( function( e ){
    e.preventDefault();

    var file_img = ["", "", "", "", "", ""];
    var validar = 0;

    var inputFileProfile = document.getElementById("image_profile");
    file_img[0] = inputFileProfile.files[0] ? inputFileProfile.files[0] : '' ;
    var inputFileExterior = document.getElementById("image_exterior");
    file_img[1] = inputFileExterior.files[0] ? inputFileExterior.files[0] : '' ;
    var inputFileInterior = document.getElementById("image_interior");
    file_img[2] = inputFileInterior.files[0] ? inputFileInterior.files[0] : '' ;
    var inputFileDesempeno = document.getElementById("image_desempeno");
    file_img[3] = inputFileDesempeno.files[0] ? inputFileDesempeno.files[0] : '' ;
    var inputFileTecnologia = document.getElementById("image_tecnologia");
    file_img[4] = inputFileTecnologia.files[0] ? inputFileTecnologia.files[0] : '' ;
    var inputFileSeguridad = document.getElementById("image_seguridad");
    file_img[5] = inputFileSeguridad.files[0] ? inputFileSeguridad.files[0] : '' ;
    var datos = new FormData();

    for (var i = 0; i <= 5; i++) {
        if ( file_img[i].type != undefined ) {
            if( file_img[i].type == "image/jpeg" || file_img[i].type == "image/jpg" || file_img[i].type == "image/JPG" || file_img[i].type == "image/JPEG" || file_img[i].type == "image/png" || file_img[i].type == "image/PNG" || file_img[i].type == "image/gif" || file_img[i].type == "image/GIF"  ){
                if(file_img[i].size<10000000){
                    validar = 0;
                } else {
                    validar = 1;
                    break;
                }
            } else {
                validar = 2;
                break;
            }
        }
    }

    if ( validar == 0 ) {
            datos.append("opcn","guardarImagenes");
            datos.append("idproducto", $('#idproducto').val() );
            datos.append("opcion", $('#opcion').val() );
            datos.append( "image_profile", file_img[0] );
            datos.append( "image_characteristics_1", file_img[1] );
            datos.append( "image_characteristics_2", file_img[2] );
            datos.append( "image_characteristics_3", file_img[3] );
            datos.append( "image_characteristics_4", file_img[4] );
            datos.append( "image_characteristics_5", file_img[5] );

        $.ajax({
            url:'controllers/producto.php',
            data: datos,
            cache: false,
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var res0 = data.resultado;
                if(res0 == "TRUE"){
                    if ( $('#opcion').val() == 0 ) {
                        $('#crearImagenes').attr('disabled', true);
                        $('#crearArchivos').attr('disabled', false);

                        $("#image_profile1").attr( "src","../assets/products/images/characteristics/upload_image.png");
                        $("#image_exterior1").attr( "src","../assets/products/images/characteristics/upload_image.png");
                        $("#image_interior1").attr( "src","../assets/products/images/characteristics/upload_image.png");
                        $("#image_desempeno1").attr( "src","../assets/products/images/characteristics/upload_image.png");
                        $("#image_tecnologia1").attr( "src","../assets/products/images/characteristics/upload_image.png");
                        $("#image_seguridad1").attr( "src","../assets/products/images/characteristics/upload_image.png");
                    }

                        reset( 'image_profile' );
                        reset( 'image_exterior' );
                        reset( 'image_interior' );
                        reset( 'image_desempeno' );
                        reset( 'image_tecnologia' );
                        reset( 'image_seguridad' );

                    notyfy({
                            text: 'Se han cargado satisfactoriamente las Imagenes del Producto.',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                } else {
                    notyfy({
                            text: 'No se ha logrado guardar las Imagenes del Producto.',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                }
            }
        });
    } else {
        if (validar == 1) {
            notyfy({
                text: 'Hay problemas con el tamaño de una de las imagenes del Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
        if (validar == 2) {
            notyfy({
                text: 'Hay problemas con el formato de una de las imagenes del Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

} );

//FORMULARIO CREAR ARCHIVOS DE PRODUCTO
$( '#formulario_archivos' ).submit( function( e ){
    e.preventDefault();

    var file_pdf = ["", "", "", ""];
    var validar = 0;

    var inputFileFicha = document.getElementById("ficha");
    file_pdf[0] = inputFileFicha.files[0] ? inputFileFicha.files[0] : '' ;
    var inputFileAccesorio = document.getElementById("accesorio");
    file_pdf[1] = inputFileAccesorio.files[0] ? inputFileAccesorio.files[0] : '' ;
    var inputFileVersion = document.getElementById("version");
    file_pdf[2] = inputFileVersion.files[0] ? inputFileVersion.files[0] : '' ;
    var inputFileCompetencia = document.getElementById("competencia");
    file_pdf[3] = inputFileCompetencia.files[0] ? inputFileCompetencia.files[0] : '' ;
    var datos = new FormData();

    for (var i = 0; i <= 3; i++) {
        if ( file_pdf[i].type != undefined ) {
            if( file_pdf[i].type == "application/pdf" || file_pdf[i].type == "application/PDF" ){
                if(file_pdf[i].size<20000000){
                    validar = 0;
                } else {
                    validar = 1;
                    break;
                }
            } else {
                validar = 2;
                break;
            }
        }
    }

    if ( validar == 0 ) {
            datos.append("opcn","guardarArchivos");
            datos.append("idproducto", $('#idproducto').val() );
            datos.append("opcion", $('#opcion').val() );
            datos.append( "pdf_1", file_pdf[0] );
            datos.append( "pdf_2", file_pdf[1] );
            datos.append( "pdf_3", file_pdf[2] );
            datos.append( "pdf_4", file_pdf[3] );

        $.ajax({
            url:'controllers/producto.php',
            data: datos,
            cache: false,
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var res0 = data.resultado;
                if(res0 == "TRUE"){
                    if ( $('#opcion').val() == 0 ) {
                        $('#crearArchivos').attr('disabled', true);
                        $('#crearVideos').attr('disabled', false);
                    }
                        reset( 'ficha' );
                        reset( 'accesorio' );
                        reset( 'version' );
                        reset( 'competencia' );

                    notyfy({
                            text: 'Se han cargado satisfactoriamente los Archivos PDF del Producto.',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                } else {
                    notyfy({
                            text: 'No se ha logrado guardar los Archivos PDF del Producto.',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                }
            }
        });
    } else {
        if (validar == 1) {
            notyfy({
                text: 'Hay problemas con el tamaño de uno de los archivos PDF del Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
        if (validar == 2) {
            notyfy({
                text: 'Hay problemas con el formato de una de los archivos PDF del Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

} );

//FORMULARIO CREAR VIDEOS DE PRODUCTO
$( '#formulario_videos' ).submit( function( e ){
    e.preventDefault();

    var file_mp4 = ["", ""];
    var validar = 0;

    var inputFileVideoProfile = document.getElementById("video_profile");
    file_mp4[0] = inputFileVideoProfile.files[0] ? inputFileVideoProfile.files[0] : '' ;
    var inputFileVideo360 = document.getElementById("video_recorrido");
    file_mp4[1] = inputFileVideo360.files[0] ? inputFileVideo360.files[0] : '' ;
    var datos = new FormData();

    for (var i = 0; i <= 1; i++) {
        if ( file_mp4[i].type != undefined ) {
            if( file_mp4[i].type == "video/mp4" || file_mp4[i].type == "video/mpeg" || file_mp4[i].type == "video/MP4" || file_mp4[i].type == "video/MPEG" ){
                if(file_mp4[i].size<200000000){
                    validar = 0;
                } else {
                    validar = 1;
                    break;
                }
            } else {
                validar = 2;
                break;
            }
        }
    }

    if ( validar == 0 ) {
            datos.append("opcn","guardarVideo");
            datos.append("idproducto", $('#idproducto').val() );
            datos.append("opcion", $('#opcion').val() );
            datos.append( "mp4_1", file_mp4[0] );
            datos.append( "mp4_2", file_mp4[1] );

        $.ajax({
            url:'controllers/producto.php',
            data: datos,
            cache: false,
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var res0 = data.resultado;
                if(res0 == "TRUE"){
                    if ( $('#opcion').val() == 0 ) {
                        $('#crearVideos').attr('disabled', true);
                        $('#crearLinks').attr('disabled', false);
                    }

                    reset( 'video_profile' );
                    reset( 'video_recorrido' );

                    notyfy({
                            text: 'Se han cargado satisfactoriamente los Videos del Producto.',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                } else {
                    notyfy({
                            text: 'No se ha logrado guardar los Videos del Producto.',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                }
            }
        });
    } else {
        if (validar == 1) {
            notyfy({
                text: 'Hay problemas con el tamaño de uno de los Videos del Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
        if (validar == 2) {
            notyfy({
                text: 'Hay problemas con el formato de uno de los Videos del Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

} );

//FORMULARIO CREAR LINKS DE PRODUCTO
$( '#formulario_links' ).submit( function( e ){
    e.preventDefault();

    var file_link = "";
    var validar = 0;  

    var inputFileApoyo = document.getElementById("img_link_recorrido");
    file_link = inputFileApoyo.files[0] ? inputFileApoyo.files[0] : '' ;
    var datos = new FormData();

    if ( file_link.type != undefined ) {
        if ( file_link.type == "image/jpeg" || file_link.type == "image/jpg" || file_link.type == "image/JPG" || file_link.type == "image/JPEG" || file_link.type == "image/png" || file_link.type == "image/PNG" || file_link.type == "image/gif" || file_link.type == "image/GIF" ) {
            if(file_link.size<10000000){
                validar = 0;
            } else {
                validar = 1;
            }
        } else {
            validar = 2;
        }     
    }

     if ( validar == 0 ) {
            datos.append("opcn","guardarLinks");
            datos.append("idproducto", $('#idproducto').val() );
            datos.append("opcion", $('#opcion').val() );
            datos.append( "apoyo_link", file_link );
            datos.append( "vct_link", $("#vct").val() );

        $.ajax({
            url:'controllers/producto.php',
            data: datos,
            cache: false,
            type: 'post',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var res0 = data.resultado;
                if(res0 == "TRUE"){
                    if ( $('#opcion').val() == 0 ) {
                        $('#crearLinks').attr('disabled', true);
                        $('#crearAudios').attr('disabled', false);

                        $("#img_link_recorrido1").attr( "src","../assets/products/images/links/upload_image.png");
                    }

                        reset( 'img_link_recorrido' );

                    notyfy({
                            text: 'Se han cargado satisfactoriamente los Links del Producto.',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                } else {
                    notyfy({
                            text: 'No se ha logrado guardar los Links del Producto.',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                }
            }
        });
    } else {
        if (validar == 1) {
            notyfy({
                text: 'Hay problemas con el tamaño de una de las imagenes del Link de Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
        if (validar == 2) {
            notyfy({
                text: 'Hay problemas con el formato de una de las imagenes del Link de Producto.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

} );

//FORMULARIO CREAR AUDIOS DE PRODUCTO
$( '#formulario_audios' ).submit( function( e ){
    e.preventDefault();

    var file_audio = ["", ""];
    var validar = 0;

    if ( $('#biblioteca').val() == -1 ) { //Si se ha seleccionado un metodo de carga de AudioLibro
        notyfy({
            text: 'Debe seleccionar un metodo de relación para cargar AudioLibros.',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    } else { 

        if ( $('#biblioteca').val() == 0 ) { //Si se creara un nuevo AudioLibro

            var inputFileAudio = document.getElementById("audioLibro");
            file_audio[0] = inputFileAudio.files[0] ? inputFileAudio.files[0] : '' ;
            var inputFileImage = document.getElementById("image_audio");
            file_audio[1] = inputFileImage.files[0] ? inputFileImage.files[0] : '' ;
            var datos = new FormData();

            for (var i = 0; i < 2; i++) {
                if ( file_audio[i].type != undefined ) {
                    if( file_audio[i].type == "audio/mp3" || file_audio[i].type == "audio/MP3" || file_audio[i].type == "image/jpeg" || file_audio[i].type == "image/jpg" || file_audio[i].type == "image/JPG" || file_audio[i].type == "image/JPEG" || file_audio[i].type == "image/png" || file_audio[i].type == "image/PNG" || file_audio[i].type == "image/gif" || file_audio[i].type == "image/GIF" ){
                        if(file_audio[i].size<50000000){
                            validar = 0;
                        } else {
                            validar = 1;
                            break;
                        }
                    } else {
                        validar = 2;
                        break;
                    }
                }
            }

            if ( validar == 0 ) {
                    datos.append("opcn","guardarAudio");
                    datos.append("idproducto", $('#idproducto').val() );
                    datos.append("descripcion", $('#descripcion').val() );
                    datos.append("name_vehiculo", nombre_vehiculo );
                    datos.append( "mp3_audio", file_audio[0] );
                    datos.append( "image_audio", file_audio[1] );

                $.ajax({
                    url:'controllers/producto.php',
                    data: datos,
                    cache: false,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        var res0 = data.resultado;
                        if(res0 == "TRUE"){
                            $('#crearAudios').attr('disabled', false);

                            reset( 'audioLibro' );
                            reset( 'image_audio' );
                            $("#image_audio1").attr( "src","../assets/products/images/upload_image.png");

                            notyfy({
                                    text: 'Se han cargado satisfactoriamente el AudioLibro del Producto.',
                                    type: 'success' // alert|error|success|information|warning|primary|confirm
                                });
                        } else {
                            notyfy({
                                    text: 'No se ha logrado guardar el AudioLibro del Producto.',
                                    type: 'error' // alert|error|success|information|warning|primary|confirm
                                });
                        }
                    }
                });
            } else {
                if (validar == 1) {
                    notyfy({
                        text: 'Hay problemas con el tamaño de uno del AudioLibro de Producto.',
                        type: 'warning' // alert|error|success|information|warning|primary|confirm
                    });
                }
                if (validar == 2) {
                    notyfy({
                        text: 'Hay problemas con el formato de uno del AudioLibro de Producto.',
                        type: 'warning' // alert|error|success|information|warning|primary|confirm
                    });
                }
            }

        } //END Si se creara un nuevo AudioLibro

        if ( $('#biblioteca').val() > 0 ) { //Si se relaciona un AudioLibro con Biblioteca
            var datos = new FormData();
            datos.append("opcn","AudioBiblioteca");
            datos.append("idproducto", $('#idproducto').val() );
            datos.append("idlibreria", $('#library_id').val() );
            $.ajax({
                url:'controllers/producto.php',
                data: datos,
                cache: false,
                type: 'post',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    var res0 = data.resultado;
                    if(res0 == "TRUE"){
                        $('#crearAudios').attr('disabled', false);

                        notyfy({
                                text: 'Se han cargado satisfactoriamente los AudioLibros del Producto.',
                                type: 'success' // alert|error|success|information|warning|primary|confirm
                            });
                    } else {
                        notyfy({
                                text: 'No se ha logrado guardar los AudioLibros del Producto.',
                                type: 'error' // alert|error|success|information|warning|primary|confirm
                            });
                    }
                }
            });
        }

    }

} );


//LIMPIA LOS CAMPOS Y BOTONES
function limpiar(){
    $('#crearProducto').attr('disabled', false);
    $('#crearImagenes').attr('disabled', true);
    $('#crearArchivos').attr('disabled', true);
    $('#crearVideos').attr('disabled', true);
    $('#crearAudios').attr('disabled', true);
    $('#crearLinks').attr('disabled', true);

    $('#idproducto').val('0');

    reset( 'image_profile' );
    $("#image_profile1").attr( "src","../assets/products/images/upload_image.png");
    reset( 'image_exterior' );
    $("#image_exterior1").attr( "src","../assets/products/images/upload_image.png");
    reset( 'image_interior' );
    $("#image_interior1").attr( "src","../assets/products/images/upload_image.png");
    reset( 'image_desempeno' );
    $("#image_desempeno1").attr( "src","../assets/products/images/upload_image.png");
    reset( 'image_tecnologia' );
    $("#image_tecnologia1").attr( "src","../assets/products/images/upload_image.png");
    reset( 'image_seguridad' );
    $("#image_seguridad1").attr( "src","../assets/products/images/upload_image.png");

    reset( 'ficha' );
    reset( 'accesorio' );
    reset( 'version' );
    reset( 'competencia' );

    reset( 'video_profile' );
    reset( 'video_recorrido' );

    reset( 'audioLibro' );
    reset( 'image_audio' );
    $("#image_audio1").attr( "src","../assets/products/images/upload_image.png");
    nombre_vehiculo=''; 

    $('#nuevoAudio').hide();
    $('#nuevaBiblioteca').hide();

    reset( 'img_link_recorrido' );
    $("#img_link_recorrido1").attr( "src","../assets/products/images/upload_image.png");

}

//RESETEA EL INPUT FILE
function reset( input ){
    var $el = $('#'+input);
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
}