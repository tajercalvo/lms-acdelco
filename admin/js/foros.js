$(document).ready(function() {
	$("#perfil").select2({
                    placeholder: "Seleccione un Perfil",
                    allowClear: true
          });
	$("#trayectoria").select2({
                    placeholder: "Seleccione una Trayectoria",
                    allowClear: true
          });
	$("#perfiledit").select2({
                    placeholder: "Seleccione un Perfil",
                    allowClear: true
          });
	$("#trayectoriaedit").select2({
                    placeholder: "Seleccione una Trayectoria",
                    allowClear: true
          });

	consultar_trayectorias()
	consultar_perfiles()
	consultar_hilo() 

});

function consultar_perfiles() {
	
		$.ajax({
		url: 'controllers/foros_tutor_detalle.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'consultar_perfiles'},
	})
		.done(function(data) {
		//console.log(data);

			 datos = '';
			for (var i = 0; i < data.length; i++) {
				 
				datos +='<option value = "'+data[i].perfil_id+'">'+data[i].perfil+'</option>'; 
			}
			$('#perfil').html(datos)
			$('#perfiledit').html(datos)

		})
		.fail(function() {
			console.log("error");
		})	
		
}



function consultar_trayectorias(){
	$.ajax({
		url: 'controllers/foros_tutor_detalle.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'consultar_trayectorias'},
	})
	.done(function(data) {
		//console.log(data);
 			datos = ''
            for (var i = 0; i < data.length; i++) {
                datos += '<option value="' + data[i].charge_id + '">' + data[i].charge+' </option>';
            }

             $('#trayectoria').html(datos);
             $('#trayectoriaedit').html(datos);

	})
	.fail(function() {
		console.log("error");
	})
	
}

$('#frm_hilo').submit(function(event) {
	/* Act on the event */
	event.preventDefault();
	//data = $(this).serializeArray()
	var data=[]
	data.push({name: 'titulo',value: $('#titulo').val() })
    data.push({name: 'id_perfiles',value: $('#perfil').val() })
    data.push({name: 'id_trayectorias',value: $('#trayectoria').val() })
    data.push({name: 'opcn',value: 'crear_hilo'})

	// console.log(data);
	// return;

	$.ajax({
		url: 'controllers/foros_tutor_detalle.php',
		type: 'POST',
		dataType: 'JSON',
		data: data,
	})
	.done(function(data) {
		//console.log(data);
		 $('#modal_crear').modal('hide');
		 $('#titulo').val('') 
		 $('#perfil').val('').trigger('change');
		 $('#trayectoria').val('').trigger('change');
		 consultar_hilo() 

	})
	.fail(function() {
		console.log("error");
	})	

});

function consultar_hilo(){
	$.ajax({
		url: 'controllers/foros_tutor_detalle.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'consultar_hilo'},
	})
	.done(function(data) {
		//console.log(data);
		renderForos(data)
	})
	.fail(function() {
		console.log("error");
	})
	
}

function renderForos(foros) {
    console.log( foros )
    var html = "";
    rol = $('#rol_id').data("value")
    console.log(rol)
    if (rol == 'admin') {

    }
    foros.forEach(function(key, idx) {
    	//console.log(key.image)
        var f = new Date(key.date_creation);
        var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
        var imagen = key.image != "" ? key.image : "default.jpg";
        var editar = rol == 'admin' ? '<button class="btn btn-success bt-xs" onclick="editar(`'+key.foro_id+'`)"><i class="fa fa-pencil"></i> Editar </button>' : '';
        var aux = `<div class="media innerAll inner-2x border-bottom margin-none">
                    <div class="pull-left media-object" style="width:100px">
                        <div class="text-center">
                            <a href="ver_perfil.php?back=usuarios&id=${ key.creador_id }" class="clearfix" target="_blank">
                                <img src="../assets/images/usuarios/${ key.image }" class="rounded-none" style="max-width: 70px"/>
                            </a>
                            <small class="text-small">Moderador</small>
                            
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="pull-right">creado: <small class="label label-default"> ${ fecha } </small></span>
                        <h5><a href="foros_tutor_detalle.php?foro_id=${ key.foro_id }" class="text-primary"> ${ key.first_name+" "+key.last_name }</a> <span>escribio:</span></h5>
                        <div class="row">
                            <div class="col-md-9">
                                <h4>${ key.foro_name }</h4>
                                 
                            </div>
                            <div class="col-md-3">
                                <a class="pull-left innerAll half " href="foros_tutor_detalle.php?foro_id=${ key.foro_id }">
                                    <img class="img-responsive" style="width: 150px;" id="imgModal_Lanzamiento" src="../assets/gallery/modal/default.jpg" title="${ key.foro_name }">
                                </a>
                            </div>
                        </div>
                        <small class="display-block text-muted">
                            <a href="foros_tutor_detalle.php?foro_id=${ key.foro_id }" class="btn btn-success"><i class="fa fa-angle-double-right"></i> Entrar al foro </a>
                          ${editar}
                           
                        </small>
                    </div>
                </div>`;
        html += aux;
    });
    $('#listado_post').html(html);
}


function editar(foro_id){
	$("#modal_editar").modal('show')
	//$('#tituloedit').val(foro_id)
	//$('#perfiledit').val( 11 ).trigger('change');
	$.ajax({
		url: 'controllers/foros_tutor_detalle.php',
		type: 'POST',
		dataType: 'JSON',
		data: {opcn: 'consult_hilo_forid',foro_id:foro_id },
	})
	.done(function(data) {
		//console.log('consulta')
		console.log(data);
		var trayectoria_id =[];
		var perfiledit = [];
		for (var i = 0; i < data.length; i++) {
			  perfiledit.push(data[i].perfil_id);
			  trayectoria_id.push(data[i].trayectoria_id);
			  
			 $('#tituloedit').val(data[i].foro_name);
	    	 $( '#foro_id' ).val(data[i].foro_id );
		}
		console.log(perfiledit);
		console.log(trayectoriaedit);
	    	 $('#perfiledit').val(perfiledit).trigger('change');
	    	 $('#trayectoriaedit').val(trayectoria_id).trigger('change');			
	})
	.fail(function() {
		console.log("error");
	})
}

$('#frm_edit_hilo').submit(function(e){
	e.preventDefault();
	var data = []
	    data.push({name: 'foro_id',value: $('#foro_id').val() })
	    data.push({name: 'tituloedit',value: $('#tituloedit').val() })
	    data.push({name: 'perfiledit',value: $('#perfiledit').val() })
    	data.push({name: 'trayectoriaedit',value: $('#trayectoriaedit').val() })
		data.push({name:'opcn', value:'editar_hilo'})
// 		console.log($(foro_id).val());
// return;
	$.ajax({
		url: 'controllers/foros_tutor_detalle.php',
		type: 'POST',
		dataType: 'JSON',
		data: data,
	})
	.done(function(data) {
		//console.log('editado')
		//console.log(data);
		$("#modal_editar").modal('hide')
		consultar_hilo()
	})
	.fail(function() {
		console.log("error");
	})

})





