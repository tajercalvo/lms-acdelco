//var stack = 1, bars = true, lines = false, steps = false;
var barChartOptions = {
  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
  scaleBeginAtZero: true,
  //Boolean - Whether grid lines are shown across the chart
  scaleShowGridLines: true,
  //String - Colour of the grid lines
  scaleGridLineColor: "rgba(0,0,0,.05)",
  //Number - Width of the grid lines
  scaleGridLineWidth: 1,
  //Boolean - Whether to show horizontal lines (except X axis)
  scaleShowHorizontalLines: true,
  //Boolean - Whether to show vertical lines (except Y axis)
  scaleShowVerticalLines: true,
  //Boolean - If there is a stroke on each bar
  barShowStroke: true,
  //Number - Pixel width of the bar stroke
  barStrokeWidth: 2,
  //Number - Spacing between each of the X value sets
  barValueSpacing: 5,
  //Number - Spacing between data sets within X values
  barDatasetSpacing: 1,
  //String - A legend template
  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
  //Boolean - whether to make the chart responsive
  responsive: true,
  maintainAspectRatio: true
};

var DataNames = [];

$(document).ready( function(){
    $('#p_start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#p_end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#z_start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#z_end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#c_start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#c_end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#s_start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#s_end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#u_start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $('#u_end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2017-01-01"
    });
    $(".select").select2({
        placeholder: "Seleccione una opción",
        allowClear: true
    });
    NProgress.configure({ parent: '.loader_s' });

    //selecciona la fecha en todos los campos de fecha
    $('.start_date_day').change( function(){
        $('.start_date_day').val( $(this).val() );
    } );
    $('.end_date_day').change( function(){
        $('.end_date_day').val( $(this).val() );
    } );
} );

$('#s_dealer_id').change( function(){
    NProgress.start();
    $("#headquarter_id").select2("val", "");
    var url = "controllers/rep_ventas.php"
    var data = new FormData();
    data.append( 'opcn', 'sel_sede' );
    data.append( 'sede', $(this).val() );
    $.ajax({
        url         : url,
        type        :'post',
        contentType : false,
        data        : data,
        processData : false,
        dataType    : 'html',
        cache       : false,
        async       : false
    } )
    .done( function( data ) {
        $('#headquarter_id').html( data );
        NProgress.done();
    } );
} );

$( "#consultaPais" ).on( "submit", function( event ) {
    event.preventDefault();
    NProgress.start();
    var datos = new FormData();
    datos.append( 'opcn', 'pais' );
    datos.append( 'start_date_day', $('#p_start_date_day').val() );
    datos.append( 'end_date_day', $('#p_end_date_day').val() );
    $.ajax({
        url           : "controllers/rep_ventas.php",
        data          : datos,
        type          : 'post',
        contentType   : false,
        processData   : false,
        dataType      : 'json',
        cache         : false,
        async         : false
    }).done( function( data ){
        // console.log( val_pais );
      //$("#data_char_1").html( val_pais );
      graficaPais( data.gr_pais, 'char_pais_1');
      grafica( data.gr_zona_fac, data.gr_zona_mat, 'char_pais_2', ['Facturación', 'Matriculas'], 'zone' );
      grafica( data.gr_concs_fac, data.gr_concs_mat, 'char_pais_3',['Facturación', 'Matriculas'], 'dealer' );
      grafica( data.gr_modelos_fac, data.gr_modelos_mat, 'char_pais_4',['Facturación', 'Matriculas'], 'Desc_KMAT' );
      graficaUnica( data.gr_genero, 'char_pais_5', 'gender', 'cantidad', true );
      graficaUnica( data.gr_edad, 'char_pais_6', 'texto', 'cantidad', false );
      graficaUnica( data.gr_agentes, 'char_pais_7', 'nombre', 'cantidad', true );

      NProgress.done();
    } );
});//fin consultaPais

$( "#consultaZona" ).on( "submit", function( event ) {
    NProgress.start();
    event.preventDefault();
    var datos = new FormData();
    datos.append( 'opcn', 'zona' );
    datos.append( 'start_date_day', $('#z_start_date_day').val() );
    datos.append( 'end_date_day', $('#z_end_date_day').val() );
    datos.append( 'zone_id', $('#zone_id').val() );
    $.ajax({
        url           : "controllers/rep_ventas.php",
        data          : datos,
        type          : 'post',
        contentType   : false,
        processData   : false,
        dataType      : 'json',
        cache         : false,
        async         : false
    }).done( function( data ){
        // console.log( data );
        grafica( data.gr_zona_fac, data.gr_zona_mat, 'char_zona_1',['Facturación', 'Matriculas'], 'zone' );
        grafica( data.gr_concs_fac, data.gr_concs_mat, 'char_zona_3',['Facturación', 'Matriculas'], 'dealer' );
        grafica( data.gr_modelos_fac, data.gr_modelos_mat, 'char_zona_4',['Facturación', 'Matriculas'], 'Desc_KMAT' );
        graficaUnica( data.gr_genero, 'char_zona_5', 'gender','Cantidad', true );
        graficaUnica( data.gr_edad, 'char_zona_6', 'texto','Cantidad', false );
        graficaUnica( data.gr_agentes, 'char_zona_7', 'nombre','Cantidad', true );
        NProgress.done();
    } );
});//fin consultaZona

$( "#consultaConsecionario" ).on( "submit", function( event ) {
    NProgress.start();
    event.preventDefault();
    var datos = new FormData();
    datos.append( 'opcn', 'concesionario' );
    datos.append( 'start_date_day', $('#c_start_date_day').val() );
    datos.append( 'end_date_day', $('#c_end_date_day').val() );
    datos.append( 'dealer_id', $('#c_dealer_id').val() );
    $.ajax({
        url           : "controllers/rep_ventas.php",
        data          : datos,
        type          : 'post',
        contentType   : false,
        processData   : false,
        dataType      : 'json',
        cache         : false,
        async         : false
    }).done( function( data ){
        // console.log( data );
        grafica( data.gr_concs_fac, data.gr_concs_mat, 'char_concs_1',['Facturación', 'Matriculas'], 'dealer' );
        grafica( data.gr_modelos_fac, data.gr_modelos_mat, 'char_concs_3',['Facturación', 'Matriculas'], 'Desc_KMAT' );
        graficaUnica( data.gr_genero, 'char_concs_5', 'gender', 'Cantidad', true );
        graficaUnica( data.gr_edad, 'char_concs_6', 'texto', 'Cantidad', false );
        graficaUnica( data.gr_agentes, 'char_concs_7', 'nombre', 'Cantidad', true );
        NProgress.done();
    } );
});//fin consultaConsecionario

$( "#consultaSede" ).on( "submit", function( event ) {
    NProgress.start();
    event.preventDefault();
    var headquarter = "";
    var datos = new FormData();
    if( $('#headquarter_id').val() && $('#headquarter_id').val().length > 0 ){
        headquarter = $('#headquarter_id').val().toString();
    }
    datos.append( 'opcn', 'sede' );
    datos.append( 'start_date_day', $('#s_start_date_day').val() );
    datos.append( 'end_date_day', $('#s_end_date_day').val() );
    datos.append( 'dealer_id', $('#s_dealer_id').val() );
    datos.append( 'headquarter_id', headquarter );
    $.ajax({
        url           : "controllers/rep_ventas.php",
        data          : datos,
        type          : 'post',
        contentType   : false,
        processData   : false,
        dataType      : 'json',
        cache         : false,
        async         : false
    }).done( function( data ){
        grafica( data.gr_concs_mat, [], 'char_sede_1', ['Matriculas','Facturación'], 'headquarter' );
        grafica( data.gr_modelos_mat, [], 'char_sede_2', ['Matriculas','Facturación'], 'Desc_KMAT' );
        graficaUnica( data.gr_genero, 'char_sede_3', 'gender', 'Cantidad', true );
        graficaUnica( data.gr_edad, 'char_sede_4', 'texto', 'Cantidad', false );
        graficaUnica( data.gr_agentes, 'char_sede_5', 'nombre', 'Cantidad', true );
        console.log( data.gr_agentes );
        NProgress.done();
    } );
});//fin consultaSede

$( "#consultaUsuario" ).on( "submit", function( event ) {
    NProgress.start();
    event.preventDefault();
    var datos = new FormData();
    datos.append( 'opcn', 'usuario' );
    datos.append( 'start_date_day', $('#u_start_date_day').val() );
    datos.append( 'end_date_day', $('#u_end_date_day').val() );
    datos.append( 'users', $('#identification').val() );
    $.ajax({
        url           : "controllers/rep_ventas.php",
        data          : datos,
        type          : 'post',
        contentType   : false,
        processData   : false,
        dataType      : 'json',
        cache         : false,
        async         : false
    }).done( function( data ){
        grafica( data.gr_modelos_mat, [], 'char_user_4', ['Matriculas','Facturación'], 'Desc_KMAT' );
        graficaUnica( data.gr_genero, 'char_user_2', 'gender', 'Cantidad', true );
        graficaUnica( data.gr_edad, 'char_user_3', 'texto', 'Cantidad', false );
        graficaUnica( data.gr_agentes, 'char_user_1', 'nombre', 'Cantidad', true );
        console.log( data.gr_agentes );
        NProgress.done();
    } );
});//fin consultaUsuario

//==========================================================================
//Funcion que genera una grafica con un solo item
//==========================================================================
function graficaPais( obj, id ){
    var data = {
        labels: ['Total'],
        datasets: [
            {
                label: obj[0].texto,
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [obj[0].cantidad]
            },
            {
                label: obj[1].texto,
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [obj[1].cantidad]
            }
        ]
    };

    barChartCanvas = $("#"+id).get(0).getContext("2d"); //id del canvas
    barChart = new Chart( barChartCanvas ); //instancia de la clase Chart
    data.datasets[0].fillColor = "#7c7c7c";
    data.datasets[0].strokeColor = "#7c7c7c";
    data.datasets[0].pointColor = "#7c7c7c";
    data.datasets[1].fillColor = "#d65050";
    data.datasets[1].strokeColor = "#d65050";
    data.datasets[1].pointColor = "#d65050";

    barChartOptions.datasetFill = false;
    barChart.Bar(data, barChartOptions);
}

//================================================================================
//Funcion que genera una grafica con 2 objetos de campos similares
//================================================================================
function grafica( obj1, obj2, id, label,name ){
    var nombres = [];
    var facturacion = [];
    var matriculas = [];
    for ( variable of obj1 ) {
        nombres.push( variable[name] );
        facturacion.push( variable.cantidad );
    }

    for ( variable of obj2 ) {
        for (var i = 0; i < nombres.length; i++) {
            if( nombres[i] == variable[name] ){
                matriculas.push( variable.cantidad );
            }
        }
    }

    var data = {
        labels: nombres,
        datasets: [
            {
                label: label[0],
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: facturacion
            },
            {
                label: label[1],
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: matriculas
            }
        ]
    };

    barChartCanvas = $("#"+id).get(0).getContext("2d"); //id del canvas
    barChart = new Chart( barChartCanvas ); //instancia de la clase Chart
    data.datasets[0].fillColor = "#7c7c7c";
    data.datasets[0].strokeColor = "#7c7c7c";
    data.datasets[0].pointColor = "#7c7c7c";
    data.datasets[1].fillColor = "#d65050";
    data.datasets[1].strokeColor = "#d65050";
    data.datasets[1].pointColor = "#d65050";

    barChartOptions.datasetFill = false;
    barChart.Bar(data, barChartOptions);

}

//================================================================================
//Funcion que genera una grafica con barras unicas
//================================================================================
function graficaUnica( obj, id, name, label, color ){
    var nombres = [];
    var cantidades = [];
    for ( variable of obj ) {
        nombres.push( variable[name] );
        cantidades.push( variable.cantidad );
    }

    var data = {
        labels: nombres,
        datasets: [
            {
                label: label,
                fillColor: "rgba(210, 214, 222, 1)",
                strokeColor: "rgba(210, 214, 222, 1)",
                pointColor: "rgba(210, 214, 222, 1)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: cantidades
            }
        ]
    };

    barChartCanvas = $("#"+id).get(0).getContext("2d"); //id del canvas
    barChart = new Chart( barChartCanvas ); //instancia de la clase Chart
    if( color ){
        data.datasets[0].fillColor = "#d65050";
        data.datasets[0].strokeColor = "#d65050";
        data.datasets[0].pointColor = "#d65050";
    }

    barChartOptions.datasetFill = false;
    barChart.Bar(data, barChartOptions);

}
