$(document).ready(function(){
	setTimeout(function(){
        $("#banner-club").owlCarousel({
            slideSpeed : 400,
            paginationSpeed : 400,
            navigation: false,
            autoPlay: 2000,
            items: 1,
            navigationText: ['Anterior', 'Siguiente']
        });
    },100);
});

function ProcesoRedencion(form){
    notyfy({
        text: 'Esta seguro de solicitar este producto?',
        type: 'confirm',
        dismissQueue: true,
        layout: 'top',
        buttons: ([{
            addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
            text: '<i></i> Aceptar',
            onClick: function ($notyfy) {
                $notyfy.close();
                console.log("retorna true");
                $( "#"+form ).submit();
                return true;
            }
        }, {
            addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
            text: '<i></i> Cancelar',
            onClick: function ($notyfy) {
                $notyfy.close();
                notyfy({
                    force: true,
                    text: '<strong>Se ha cancelado la selección y redención de este producto<strong>',
                    type: 'error',
                    layout: 'top'
                });
                console.log("retorna false");
                return false;
            }
        }])
    });
    event.preventDefault();
}
$(document).ready(function(){

    if ($('#ResReden').length){
        if($('#ResReden').val()=="1"){
            notyfy({
                text: 'Hemos recibido la solicitud de redención, estamos iniciando el proceso de aprobación, alistamiento y entrega',
                type: 'success' // alert|error|success|information|warning|primary|confirm
            });
        }else{
            notyfy({
                text: 'Ha ocurrido un error, no se ha logrado recibir la solicitud.',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }
});

function mostrar( mes, texto_grupo ){
	var grupo = data.recibidos.indexOf( texto_grupo );
	console.log( mes, grupo, texto_grupo );
	if( grupo >= 0 ){
		var num = data.datos[ data.meses[mes-1] ][ data.grupos[ grupo ] ].length;
		for (var i = 0; i < num; i++) {
			var archivo = data.datos[ data.meses[ mes-1 ] ][ data.grupos[ grupo ] ][i];
			var texto = data.url + data.meses[mes-1] +"/"+ data.grupos[ grupo ] +"/"+archivo;
			$('#imagenes_'+i).attr( { 'src':texto, "style":"width:100%;display:block"} );
			$('#link_'+i).attr( { 'href':texto, 'download': archivo } );
		}
	}
	$('#modalMostrarImagen').modal();

}

$('#cerrar_modal').click( function(){
	$('#modalMostrarImagen').modal('hide');
} );

var data = {
	url:"../assets/gallery/banners/ClubLideres/kilometros/",
	meses:["Enero","Febrero","Marzo","Abril"],
	recibidos:["GRUPO A","GRUPO B","GRUPO C","GRUPO D","GRUPO E"],
	grupos:["Grupo_A","Grupo_B","Grupo_C","Grupo_D","Grupo_E"],
	datos:{
		"Enero":{
			"Grupo_A":
			[
				"Tabla_Resultados_Enero_Grupo_A_1.jpg",
				"Tabla_Resultados_Enero_Grupo_A_2.jpg"
			],
			"Grupo_B":
			[
				"Tabla_Resultados_Enero_Grupo_B_1.jpg",
				"Tabla_Resultados_Enero_Grupo_B_2.jpg"
			],
			"Grupo_C":
			[
				"Tabla_Resultados_Enero_Grupo_C.jpg"
			],
			"Grupo_D":
			[
				"Tabla_Resultados_Enero_Grupo_D.jpg"
			],
			"Grupo_E":
			[
				"Tabla_Resultados_Enero_Grupo_E.jpg"
			]
		},
		"Febrero":{
			"Grupo_A":
			[
				"Tabla_Resultados_Febrero_Grupo_A_1.jpg",
				"Tabla_Resultados_Febrero_Grupo_A_2.jpg"
			],
			"Grupo_B":
			[
				"Tabla_Resultados_Febrero_Grupo_B_1.jpg",
				"Tabla_Resultados_Febrero_Grupo_B_2.jpg"
			],
			"Grupo_C":
			[
				"Tabla_Resultados_Febrero_Grupo_C.jpg"
			],
			"Grupo_D":
			[
				"Tabla_Resultados_Febrero_Grupo_D.jpg"
			],
			"Grupo_E":
			[
				"Tabla_Resultados_Febrero_Grupo_E.jpg"
			]
		},
		"Marzo":{
			"Grupo_A":
			[
				"Tabla_Resultados_Marzo_Grupo_A_1.jpg",
				"Tabla_Resultados_Marzo_Grupo_A_2.jpg"
			],
			"Grupo_B":
			[
				"Tabla_Resultados_Marzo_Grupo_B_1.jpg",
				"Tabla_Resultados_Marzo_Grupo_B_2.jpg"
			],
			"Grupo_C":
			[
				"Tabla_Resultados_Marzo_Grupo_C.jpg"
			],
			"Grupo_D":
			[
				"Tabla_Resultados_Marzo_Grupo_D.jpg"
			],
			"Grupo_E":
			[
				"Tabla_Resultados_Marzo_Grupo_E.jpg"
			]
		},
		"Abril":{
			"Grupo_A":
			[
				"Tabla_Resultados_Abril_Grupo_A_1.jpg",
				"Tabla_Resultados_Abril_Grupo_A_2.jpg"
			],
			"Grupo_B":
			[
				"Tabla_Resultados_Abril_Grupo_B_1.jpg",
				"Tabla_Resultados_Abril_Grupo_B_2.jpg"
			],
			"Grupo_C":
			[
				"Tabla_Resultados_Abril_Grupo_C_1.jpg",
				"Tabla_Resultados_Abril_Grupo_C_2.jpg"
			],
			"Grupo_D":
			[
				"Tabla_Resultados_Abril_Grupo_D.jpg"
			],
			"Grupo_E":
			[
				"Tabla_Resultados_Abril_Grupo_E.jpg"
			]
		}
	}
};
