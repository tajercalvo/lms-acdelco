$(document).ready(function(){

    // Select Placeholders
    $("#select_respuesta_correcta1").select2({
        placeholder: "Seleccione",
        allowClear: true
    });
    $("#select_respuesta_correcta2").select2({
        placeholder: "Seleccione",
        allowClear: true
    });
    $("#select_respuesta_correcta3").select2({
        placeholder: "Seleccione",
        allowClear: true
    });
    // Select Placeholders
    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#strategy_id").select2({
        placeholder: "Seleccione una estrategia",
        allowClear: true
    });
    $("#type").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#supplier_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#subject_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#specialty_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#ask1Over").mouseover(function(){
        $("#ask1").attr("type","text");
    });
    $("#ask2Over").mouseover(function(){
        $("#ask2").attr("type","text");
    });
    $("#ask1Over").mouseout(function(){
        $("#ask1").attr("type","password");
    });
    $("#ask2Over").mouseout(function(){
        $("#ask2").attr("type","password");
    });

    cargar_tabla_archivos(); // cargar la tabla con los archivos 'diapositivas de este curso'
});

$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/cursos_op_vct.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});

// Funcion para capturar los id de las URL se utiliza asi $.get("id_url") id evolvere el valor del id de la variable de la URl
    (function($) {
    $.get = function(key)   {
        key = key.replace(/[\[]/, '\\[');
        key = key.replace(/[\]]/, '\\]');
        var pattern = "[\\?&]" + key + "=([^&#]*)";
        var regex = new RegExp(pattern);
        var url = unescape(window.location.href);
        var results = regex.exec(url);
        if (results === null) {
            return null;
        } else {
            return results[1];
        }
    }
})(jQuery);


$('body').on('click', '.eliminar_archivo', function(){
    var coursefile_id = $(this).parents("tr").attr('data-id');
    var image = $(this).parents("tr").attr('data-image');
    var archivo_vct = $(this).parents("tr").attr('data-archivo');
    bootbox.confirm("Estas seguro de eliminar este archivo", function(result){// modal de confirmación
        if(result){
             var datos_form = [];
             datos_form.push({name: "course_id", value: $.get("id")}); //$.get("id") es una funcion global que captura el id de la url
             datos_form.push({name: "coursefile_id", value: coursefile_id});
             datos_form.push({name: "image", value: image});
             datos_form.push({name: "archivo_vct", value: archivo_vct});
             console.log(datos_form);
             uploadFile('', '', datos_form, 'eliminar_archivo', 'cursos_op_vct.php'); //uploadFile(archivo a cargar, imagen del archivo, datos formulario html, opcion que se pedira en el controlador.php);
        }
    });
    return false; // evita que se ejecute el href
});

$('body').on('click', '#crear_archivo', function(){
            $("#Modal_crear_archivo").modal("toggle");
            return false; // evita que se ejecute el href
});

$('body').on('click', '#crear_pregunta', function(){
            $("#row-respuestas").html('');
            $("#Modal_Crear_pregunta").modal("toggle");
            return false; // evita que se ejecute el href
});


// modal para editar un archivo
$( "#form_Editar_archivos" ).submit(function( event ) {
        if(!valida_campos()){ return false; }
        event.preventDefault();
        // imagen
        var imagen = document.getElementById("image");
        var imagen = imagen.files[0] ? imagen.files[0] : '' ;

        // archivo
        var archivos = document.getElementById("archivo");
        var file = archivos.files[0] ? archivos.files[0] : '' ;

        var datos_form = $(this).serializeArray();
        datos_form.push({name: "coursefile_id", value: coursefile_id});
        datos_form.push({name: "course_id", value: $.get("id")});
        datos_form.push({name: "archivo_vct", value: archivo_vct});
        datos_form.push({name: "image", value: image});
        uploadFile(file, imagen, datos_form, 'editar_archivos', 'cursos_op_vct.php'); //uploadFile(archivo a cargar, imagen del archivo, datos formulario html, opcion que se pedira en el controlador.php);
    });

function crear_select(variable, consecutivo) {
    $("#"+variable+consecutivo).select2({
                placeholder: "Seleccione",
                allowClear: true
            });
}


//(function($) {
    //variables globales
    consecutivo = 4; // se inicia en 3 teniendo en cuenta que en la modal crear preguntas se inician 3 select2 y imput type text
    $('#agregar_repuesta').click(function (e) {
            e.preventDefault();
            div = '<div class="col-md-2">';
            div += '<label class="control-label">Correcta: </label>';
            div += '<select style="width: 100%;" id="select_respuesta_correcta'+consecutivo+'" name="respuesta'+consecutivo+'[]">';
            div += '<option value="no" > NO</option>';
            div += '<option value="si" > SI</option>';
            div += '</select>';
            div += '</div>';
            div += '<div class="col-md-10">';
            div += '<label class="control-label">respuesta: </label>';
            div += '<textarea rows="4" class="form-control col-md-8" name="respuesta'+consecutivo+'[]" placeholder="Escribe la respuesta"></textarea>';
            div += '</div>';
            $('#row-respuestas').append(div);

            crear_select('select_respuesta_correcta', consecutivo);
            /*$("#select_respuesta_correcta"+consecutivo).select2({
                placeholder: "Seleccione",
                allowClear: true
            });*/
            consecutivo++;
    }); // end funcion agregar_repuesta

    consecutivo2 = 0;
    $('body').on('click', '.editar_archivos', function(e){ console.log(preguntas);
            e.preventDefault(); // evita que se ejecute el href
             //console.log( $(this).parents("tr").attr('data-id'));
             coursefile_id = $(this).parents("tr").attr('data-id'); // variabls globales para poderlas capturar en la modal
             image = $(this).parents("tr").attr('data-image'); // variabls globales para poderlas capturar en la modal
             archivo_vct = $(this).parents("tr").attr('data-archivo'); // variabls globales para poderlas capturar en la modal
             pregunta_id = $(this).parents("tr").attr('data-pregunta_id'); // variabls globales para poderlas capturar en la modal
             arreglo = [];
            $(this).parents("tr").find("td").each(function(){
                arreglo.push($(this).html()); // capturamos los campos de la fila
            });

            numero_antiguo = arreglo[0];
            nombre_antiguo = arreglo[1];
             //
             if(archivo_vct.indexOf(".preg") > -1){
                $('#row-editar-respuestas').html('');
                console.log('pegunta_id = '+pregunta_id);
                console.log('preguntas = '+preguntas);
                $("#Modal_editar_pregunta").modal("toggle");
                            $('#editar_numero_archivo').val(numero_antiguo);
                            $('#editar_nombre_archivo').val(nombre_antiguo);
                            var div = '';
                            var  seleccionado_si = '';
                            var  seleccionado_no = '';
                            if(preguntas[pregunta_id]){
                                        for (var i = 0; i < preguntas[pregunta_id].length; i++) {
                                                if(preguntas[pregunta_id][i].result == "si"){
                                                    seleccionado_si = 'selected';
                                                    seleccionado_no = '';
                                                }else{
                                                    seleccionado_si = '';
                                                    seleccionado_no = 'selected';
                                                }
                                                console.log(i);
                                                div += '<div class="col-md-2">';
                                                div += '<label class="control-label">Correcta: </label>';
                                                div += '<select style="width: 100%;" id="select_editar_respuesta_correcta'+consecutivo2+'" name="editar_respuesta'+consecutivo2+'[]">';
                                                div += '<option value="no" '+seleccionado_no+'> NO</option>';
                                                div += '<option value="si" '+seleccionado_si+'> SI</option>';
                                                div += '</select>';
                                                div += '</div>';
                                                div += '<div class="col-md-10">';
                                                div += '<label class="control-label">respuesta: </label>';
                                                div += '<textarea rows="4" class="form-control col-md-8" name="editar_respuesta'+consecutivo2+'[]" placeholder="Escribe la respuesta">'+preguntas[pregunta_id][i].answer+'</textarea>';
                                                div += '</div>';

                                                crear_select('select_editar_respuesta_correcta', consecutivo2);
                                               /* $("#select_editar_respuesta_correcta"+consecutivo2).select2({
                                                    placeholder: "Seleccione",
                                                    allowClear: true
                                                });*/
                                                consecutivo2++;
                                        }
                                    $('#row-editar-respuestas').html(div);
                            }

             }else{
                $("#ModalEditar").modal("toggle");
                $('#numero_archivo').val(arreglo[0]);
                $('#nombre_archivo').val(arreglo[1]);
             }
            return false; // evita que se ejecute el href
});

var preguntas = [];
function cargar_tabla_archivos(id = $.get("id")) {

        var datos = new FormData();
        //var id = $.get("id");
        datos.append( "opcn", "cargar_tabla_archivos" );
        if(id == null){
            datos.append( "course_id", 0 );
        }else{
            datos.append( "course_id", id );
        }

        $.ajax({
            url: 'controllers/cursos_op_vct.php',
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json'
        })
        .done( function( data ){

                    var contenido = '';
                    contenido += '<table class="table table-striped">';
                    contenido += '<thead>';
                    contenido += '<tr>';
                    contenido += '<th>Numero</th>';
                    contenido += '<th>Nombre</th>';
                    contenido += '<th>Imagen</th>';
                    contenido += '<th>Descargar</th>';
                     if($.get("opcn") == 'nuevo'){
                        contenido += '<th>  </th>';
                    }else{
                        contenido += '<th>Acción</th>';
                    }
                    contenido += '</tr>';
                    contenido += '</thead>';
                    contenido += '<tbody id="table_boddy">';

                for (var i = 0; i <  data.datos.length; i++) {
                        var tipo_Archivo = data.datos[i]['image'];
                        var imagen =  data.datos[i]['image'];
                        var ruta_imagen = '';

                    if(data.datos[i]['image'].indexOf("default_") > -1){
                            ruta_imagen = '../assets/fileVCT/'+imagen;
                        }else{
                             ruta_imagen = '../assets/fileVCT/VCT_'+id+'/'+imagen;
                        }

                    if(data.datos[i]['archivo'] == null ){
                        archivo = data.datos[i]['name'];
                        }else{
                            archivo = data.datos[i]['archivo'];
                        }

                        if(data.datos[i]['archivo'].indexOf(".preg") > -1){
                                preguntas[data.datos[i]['question_id']]=data.datos[i]['respuestas'];
                                //preguntas.push(data.datos[i]['respuestas']);
                                contenido += '<tr data-id = "'+data.datos[i]['coursefile_id']+'" data-image = "'+data.datos[i]['image']+'" data-archivo = "'+data.datos[i]['archivo']+'" data-pregunta_id = "'+data.datos[i]['question_id']+'"  >';
                        }else{
                                contenido += '<tr data-id = "'+data.datos[i]['coursefile_id']+'" data-image = "'+data.datos[i]['image']+'" data-archivo = "'+data.datos[i]['archivo']+'"  >';
                        }
                    contenido += '<td contenteditable="true" >'+data.datos[i]['file']+'</td>';
                    contenido += '<td>'+data.datos[i]['name']+'</td>';
                    contenido += '<td><img class="img-responsive" src="'+ruta_imagen+'"></td>';
                    contenido += '<td><a href="../assets/fileVCT/VCT_'+id+'/'+archivo+'" download="'+data.datos[i]['archivo']+'">Descargar</a> <br> <a href="../assets/fileVCT/VCT_'+id+'/'+archivo+'" target="_blank">Ver</a></td>';
                    if($.get("opcn") == 'nuevo'){
                        contenido += '<td>  </td>';
                    }else{
                        contenido += '<td> <a class="editar_archivos" href="#" onclick="return false;"><span class="label label-success"><i class="fa fa-pencil"></i></span></a> <br> <a class="eliminar_archivo" href="" onclick="return false;"><span class="label label-danger"><i class="fa fa-trash-o"></i></span></a> </td>';
                    }
                    contenido += '</tr>';

                } // fin for

                    contenido += '<tr>';
                    contenido += '<td>  </td>';
                    contenido += '<td>  </td>';
                    if($.get("opcn") == 'nuevo'){
                        contenido += '<td>  </td>';
                        contenido += '<td>  </td>';
                    }else{
                        contenido += '<td> <a id="crear_archivo" href="#"><span class="label label-success">Agregar archivo</span></a> </td>';
                        contenido += '<td> <a id="crear_pregunta" href="#"><span class="label label-success">Agregar pregunta</span></a> </td>';
                    }

                    contenido += '<td> </td>';
                    /*contenido += '<td> <a href="../assets/fileVCT/VCT_'+id+'.zip" download> Descargar .zip</a> </td>';*/
                    contenido += '<td>  </td>';
                    contenido += '</tr>';
                    contenido += '</tbody>';
                    contenido += '</table>';

                $('#tabla_archivos_vct').html(contenido);

        } );
}


//})(jQuery);



// modal para crear un archivos
$( "#form_crear_archivo" ).submit(function( event ) {
        event.preventDefault();
        // archivo
        var archivo = document.getElementById("crear_file");
        var archivo = archivo.files[0] ? archivo.files[0] : '' ;
        if( archivo == ''){ alert('Olvidaste seleccionar un archivo'); return false;}

        //Imagen
        var imagen = document.getElementById("crear_image_archivo");
        var imagen = imagen.files[0] ? imagen.files[0] : '' ;

        if(!valida_campos()){ return false; }
        var datos_form = $(this).serializeArray();
        datos_form.push({name: "course_id", value: $.get("id")});
        uploadFile(archivo, imagen, datos_form, 'crear_archivos', 'cursos_op_vct.php'); //uploadFile(archivo a cargar, imagen del archivo, datos formulario html, opcion que se pedira en el controlador.php);

    });

// Formulario para la creación de preguntas

$( "#form_crear_pregunta" ).submit(function( event ) {
        event.preventDefault();
        var datos_form = $(this).serializeArray();
        datos_form.push({name: "course_id", value: $.get("id")});
        datos_form.push({name: "opcn", value: 'crear_pregunta'});
        $.ajax({ url: 'controllers/cursos_op_vct.php',
        data: datos_form,
        type: 'post',
        dataType: "json",
        success: function(data) {
            if(data.error){
                notyfy({
                    text: data.mensaje,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });

            }else{
                cargar_tabla_archivos();
                $("#Modal_Crear_pregunta").modal("toggle");
                document.getElementById("form_crear_pregunta").reset();
                notyfy({
                    text: data.mensaje,
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
});

//formulario para editar las preguntas
$( "#form_editar_pregunta" ).submit(function( event ) {
        event.preventDefault();
        var datos_form = $(this).serializeArray();
        datos_form.push({name: "course_id", value: $.get("id")});
        datos_form.push({name: "opcn", value: 'editar_preguntas'});
        datos_form.push({name: 'pregunta_id', value: pregunta_id});
        $.ajax({ url: 'controllers/cursos_op_vct.php',
        data: datos_form,
        type: 'post',
        dataType: "json",
        success: function(data) {
            if(data.error){
                notyfy({
                    text: data.mensaje,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                cargar_tabla_archivos();
                document.getElementById("form_editar_pregunta").reset();
                $("#Modal_editar_pregunta").modal("toggle");
                notyfy({
                    text: data.mensaje,
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
});


// Formulario principal, contiene el archivo .zip
$( "#formulario_data" ).submit(function( event ) {
        if(!valida_campos()){ return false; }
        event.preventDefault();
        var datos = [];
        var archivos = document.getElementById("archivos");
        var file = archivos.files[0] ? archivos.files[0] : '' ;
        if(file == ''){
             bootbox.confirm("No seleccionaste material de apoyo, si este curso ya tiene material de apoyo no es necesario agregarlo nuevamente, desea continuar?", function(result){ // modal de confirmación
                if(result){
                    var datos_form = $("#formulario_data").serializeArray();
                    if($.get("opcn") == 'editar'){
                        datos_form.push({name: "opcn", value: 'editar'});
                    }else if($.get("opcn") == 'nuevo'){
                        datos_form.push({name: "opcn", value: 'crear'});
                    }else{
                        alert('url no valida'); return false;
                    }
                    uploadFile(file, '', datos_form, '', 'cursos_op_vct.php'); //uploadFile(archivo a cargar, imagen del archivo, datos formulario html, opcion que se pedira en el controlador.php);
                }
            });
         }else{
                var datos_form = $("#formulario_data").serializeArray();
                if($.get("opcn") == 'editar'){
                    datos_form.push({name: "opcn", value: 'editar'});
                }else if($.get("opcn") == 'nuevo'){
                    datos_form.push({name: "opcn", value: 'crear'});
                }else{
                    alert('url no valida'); return false;
                }
                uploadFile(file, '', datos_form, '', 'cursos_op_vct.php'); //uploadFile(archivo a cargar, imagen del archivo, datos formulario html, opcion que se pedira en el controlador.php);
         }


});

function valida_campos() {
    var valida = true;
    $('#description').val();
    $('#prerequisites').val();
    $('#target').val();

    if( $('#description').val().length == ''){
        $('#description').css( "border-color","red" );
        $('#description').focus();
        valida = false;
    }

    if( $('#prerequisites').val().length == ''){
        $('#prerequisites').css( "border-color","red" );
        $('#prerequisites').focus();
        valida = false;
    }

    if( $('#target').val().length == ''){
        $('#target').css( "border-color","red" );
        $('#target').focus();
        valida = false;
    }

    return valida;
}
$("#retornaElemento").click(function() {
    window.location="cursos.php";
});

function valida(){
    var valid = 0;
    $('#course').css( "border-color", "#efefef" );
    //$('#code').css( "border-color", "#efefef" );
    $('#description').css( "border-color", "#efefef" );
    $('#prerequisites').css( "border-color", "#efefef" );
    $('#target').css( "border-color", "#efefef" );

    var valid = 0;
    if($('#course').val() == ""){
        $('#course').css( "border-color", "#b94a48" );
        valid = 1;
    }
    /*if($('#code').val() == ""){
        $('#code').css( "border-color", "#b94a48" );
        valid = 1;
    }*/
    if($('#description').val() == ""){
        $('#description').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#prerequisites').val() == ""){
        $('#prerequisites').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#target').val() == ""){
        $('#target').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg){
    $.ajax({ url: 'controllers/cursos_op_vct.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res = data.resultado;
            if(res == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function cargaImagen(){
    $('#loading_imagen').removeClass('hide');
    try {
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type == "image/jpeg"){
            if(file.size<1500000){
                var data = new FormData();
                data.append('image_new',file);
                data.append('course_id',$('#idElemento').val());
                data.append('imgant',$('#imgant').val());
                data.append('opcn','CrearImagen');
                var url = "controllers/cursos_op_vct.php";
                    $.ajax({
                        url:url,
                        type:'post',
                        contentType:false,
                        data:data,
                        processData:false,
                        dataType: "json",
                        cache:false,
                        success: function(data) {
                            if(data.resultado=="SI"){
                                $("#ImgCurso").attr("src","../assets/images/distinciones/"+data.archivo);
                                notyfy({
                                    text: 'La imagen ha sido cambiada correctamente',
                                    type: 'success' // alert|error|success|information|warning|primary|confirm
                                });
                            }else{
                                notyfy({
                                    text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                                    type: 'error' // alert|error|success|information|warning|primary|confirm
                                });
                            }
                            $('#loading_imagen').addClass('hide');
                        }
                    });
            }else{
                notyfy({
                    text: 'La imagen NO ha sido cambiada, el tamaño es demasiado GRANDE',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            notyfy({
                text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }catch(err) {
        $('#loading_imagen').addClass('hide');
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function cargaFile(){
    $('#loading_file').removeClass('hide');
    try {
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        //console.log(file.type);
        if(file.type == "application/pdf"){
            if(file.size<1500000){
                var data = new FormData();
                data.append('image_new',file);
                data.append('course_id',$('#course_file').val());
                data.append('name',$('#name_file').val());
                data.append('opcn','CrearRecurso');
                var url = "controllers/course_detail.php?token="+$('#token_file').val();
                    $.ajax({
                        url:url,
                        type:'post',
                        contentType:false,
                        data:data,
                        processData:false,
                        dataType: "json",
                        cache:false,
                        success: function(data) {
                            if(data.resultado=="SI"){
                                notyfy({
                                    text: 'El archivo ha sido cargado correctamente, recarga la página para observarlo!',
                                    type: 'success' // alert|error|success|information|warning|primary|confirm
                                });
                            }else{
                                notyfy({
                                    text: 'El archivo NO ha sido cargado, revise la extensión del mismo sólo se acepta PDF',
                                    type: 'error' // alert|error|success|information|warning|primary|confirm
                                });
                            }
                            $('#loading_file').addClass('hide');
                        }
                    });
            }else{
                notyfy({
                    text: 'El archivo NO ha sido cargado, el tamaño es demasiado GRANDE; máximo 2 Megas',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            notyfy({
                text: 'El archivo NO ha sido cargado, revise la extensión del mismo sólo se acepta PDF',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }catch(err) {
        $('#loading_file').addClass('hide');
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}

$( "#registrarNota" ).click(function( event ) {
    var Nota = $('#reg_nota').val();
    var Modulo = $('#reg_module_id').val();
    var Course_id = $('#reg_course_id').val();
    var Fecha_nota = $('#reg_fecha').val();
    notyfy({
        text: 'Esta seguro de registrar la nota: '+Nota+' Puntos',
        type: 'confirm',
        dismissQueue: true,
        layout: 'top',
        buttons: ([{
            addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
            text: '<i></i> Aceptar',
            onClick: function ($notyfy) {
                $notyfy.close();
                var data = new FormData();
                data.append('Nota',Nota);
                data.append('Modulo_id',Modulo);
                data.append('opcn','RegistrarNota');
                var url = "controllers/cursos_op_vct.php";
                $.ajax({
                    url:url,
                    type:'post',
                    contentType:false,
                    data:data,
                    processData:false,
                    dataType: "json",
                    cache:false,
                    success: function(data) {
                        if(data.resultado=="SI"){
                            if(Nota>79){
                                notyfy({
                                    text: 'La nota ha sido registrada correctamente, de click aquí para descargar su certificado.<br><br>Certificado: <a target="_blank" class="btn btn-success" href="certificado.php?course_id='+Course_id+'&date='+Fecha_nota+'">DESCARGAR</a><br><br>También puede descargarlo en cualquier momento en su perfil, o en el panel inicial de información',
                                    type: 'primary' // alert|error|success|information|warning|primary|confirm
                                });
                            }else{
                                notyfy({
                                    text: 'La nota ha sido registrada correctamente, sin embargo los puntos obtenidos no alcanzán para superar el curso; debería intentar nuevamente superar el Objeto Virtual de Aprendizaje',
                                    type: 'information' // alert|error|success|information|warning|primary|confirm
                                });
                            }

                        }else{
                            notyfy({
                                text: 'La nota NO ha podido ser Registrada, confirme por favor su conexión a internet he intente nuevamente',
                                type: 'error' // alert|error|success|information|warning|primary|confirm
                            });
                        }
                    }
                });
            }
        }, {
            addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
            text: '<i></i> Cancelar',
            onClick: function ($notyfy) {
                $notyfy.close();
                notyfy({
                    force: true,
                    text: '<strong>Se ha cancelado la asignación<strong>',
                    type: 'error',
                    layout: 'top'
                });
            }
        }])
    });
    event.preventDefault();
});

$("#btnValidar").click(function(){
    if( validaPreguntas() ){
        var datos = new FormData();
        datos.append( "idAnswer1", $("#id_1").val() );
        datos.append( "idAnswer2", $("#id_2").val() );
        datos.append( "answer1", $("#ask1").val() );
        datos.append( "answer2", $("#ask2").val() );
        $.ajax({
            url:'course_ajax.php',
            type:'post',
            contentType:false,
            data:datos,
            processData:false,
            dataType: "json",
            cache:false,
            success: function(data){
                switch (data.resultado) {
                    case 0:
                        $("#contenedorSeguridad").html("Se ingresaron correctamente las preguntas de seguridad, por favor vuelva a dar clic en <strong>Inscribirme</strong>");
                        $("#inscripcion").show();
                        $("#security_question").hide();
                        $("#btnValidar").hide();
                    break;
                    case 1:
                        $('#ask1').css( "border-color", "#efefef" );
                        $('#ask2').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'La respuesta a la pregunta de seguridad no es correcta',
                            type: 'information' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 2:
                        $('#ask2').css( "border-color", "#efefef" );
                        $('#ask1').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'La respuesta a la pregunta de seguridad no es correcta',
                            type: 'information' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 3:
                        $('#ask1').css( "border-color", "#b94a48" );
                        $('#ask2').css( "border-color", "#b94a48" );
                    break;
                }//fin switch
            }
        });
    }//fin if
});

function validaPreguntas(){
    var valid = 0;
    var retornar = false;
    $('#ask1').css( "border-color", "#efefef" );
    $('#ask2').css( "border-color", "#efefef" );
    if( $('#ask1').val() == "" ){
        $('#ask1').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if( $('#ask2').val() == "" ){
        $('#ask2').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if( valid == 0 ){
        retornar = true;
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
    return retornar;
}//fin funcion validaPreguntas

$("#btnValidarNota").click(function(){
    if( validaPreguntas() ){
        var datos = new FormData();
        datos.append( "idAnswer1", $("#id_1").val() );
        datos.append( "idAnswer2", $("#id_2").val() );
        datos.append( "answer1", $("#ask1").val() );
        datos.append( "answer2", $("#ask2").val() );
        $.ajax({
            url:'course_ajax.php',
            type:'post',
            contentType:false,
            data:datos,
            processData:false,
            dataType: "json",
            cache:false,
            success: function(data){
                switch (data.resultado) {
                    case 0:
                        $("#contenedorSeguridad").html("Se ingresaron correctamente las preguntas de seguridad, por favor vuelva a dar clic en <strong>Registrar nota</strong>");
                        $("#registrarNota").show();
                        $("#registrarNotaSeguridad").hide();
                        $("#btnValidarNota").hide();
                    break;
                    case 1:
                        $('#ask1').css( "border-color", "#efefef" );
                        $('#ask2').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'Por favor verifíque la información que esta pendiente',
                            type: 'warning' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 2:
                        $('#ask2').css( "border-color", "#efefef" );
                        $('#ask1').css( "border-color", "#b94a48" );
                        notyfy({
                            text: 'Por favor verifíque la información que esta pendiente',
                            type: 'warning' // alert|error|success|information|warning|primary|confirm
                        });
                    break;
                    case 3:
                        $('#ask1').css( "border-color", "#b94a48" );
                        $('#ask2').css( "border-color", "#b94a48" );
                    break;
                }//fin switch
            }
        });
    }//fin if
});


function id(el){
  return document.getElementById(el);
}

//Inicio para subir o crear los archivos
function uploadFile(file, image, datos, opcn, url){

    //Valida el tamaño del archivo
    if(file.size>=1000000000){//if_2
        notyfy({
            text: 'El archivo excede el tamaño máximo permitido ( Tamaño permitido 100 Mb )',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return false;
    }//fin if

    if(image.size>=5000000){//if_2
        notyfy({
            text: 'La imagen excede el tamaño máximo permitido ( Tamaño permitido 5 Mb )',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return false;
    }//fin if

    $('.btn').prop("disabled", true);

  // fin
  file = file  == '' ? '' :file;
  image = image == '' ? '' :image;

  if(file+image != ''){
    notyfy({
        text: `Estamos procesando tu información, por favor espere <label id="status" class="control-label" ></label> <br>
        <div class="row">
          <div class="col-sm-4"></div>
          <div class="col-sm-4" style="height: 15px;"><div class="progress progress-mini"><div class="progress-bar progress-bar-primary" id="progressBar" style="width: 0px; border-radius: 5px;"></div></div></div>
          <div class="col-sm-4"></div>
        </div>`,
        type: 'information' // alert|error|success|information|warning|primary|confirm
    });
  }

  var formdata = new FormData();
  formdata.append("archivo", file);
  formdata.append("imagen", image);
  opcn == ''? '' : formdata.append('opcn', opcn);
  for (var i = 0; i < datos.length; i++) {
      formdata.append(datos[i].name,datos[i].value);
  }
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.addEventListener("error", errorHandler, false);
  ajax.addEventListener("abort", abortHandler, false);
  ajax.open("POST", "controllers/"+url);
  ajax.send(formdata);
}

function progressHandler(event){
    if($('body #progressBar').length > 0){ // validamos si fue creado el id de la barra de prpgreso
          //id("loaded_n_total").innerHTML = "Se han cargado "+event.loaded+" bytes de "+event.total;
          var percent = (event.loaded / event.total) * 100;
          id("progressBar").style.width = Math.round(percent)+'%';
          id("progressBar").text= Math.round(percent)+'%';
          // _("progressBar").value = Math.round(percent);
          $('body #status').html(Math.round(percent)+'% Cargado ');
          //id("status").innerHTML = Math.round(percent)+'% Cargando... por favor espere';
    }
}
function completeHandler(event){
    $( "input:file" ).val('');
    $('.notyfy_message').remove();
    $('.btn').prop("disabled", false);
    setTimeout($('.modal').modal('hide'), 2000);
    var datos = JSON.parse(event.target.responseText);
    if(datos.resultado == 'incompleto'){
        var no_tiene = '';
        for (var i = 0; i < datos.datos.length; i++) {
             no_tiene += datos.datos[i]+', ';
         }
         no_tiene = no_tiene.slice(0, -2);
         no_tiene += '.';
        notyfy({
                text: 'Para poder actualizar la información de este curso, debes cambiar el estado a inactivo <br> solo lo podras cambiarlo a activo cuando tenga '+ no_tiene,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });

    }else if(datos.resultado == 'SI'){
            cargar_tabla_archivos(datos.id); //cargamos la tabla con los nuevos archivos
            $('body #status').html('Carga finalizada correctamente');

            notyfy({
                text: 'Tu información ha sido cargada correctamente ',
                type: 'success' // alert|error|success|information|warning|primary|confirm
            });
            setTimeout(function() {$(".notyfy_container").fadeOut(5000); },3000);
    }else if(datos.error){
            notyfy({
                text: datos.mensaje,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
            setTimeout(function() {$(".notyfy_container").fadeOut(5000); },3000);
            // _("progressBar").style.width = "0";
            // _("progressBar").value = 0;
    }else{
        alert('Resultado inesperado');
    }

}
function errorHandler(event){
  id("status").innerHTML = "Upload Failed";
}
function abortHandler(event){
  id("status").innerHTML = "Upload Aborted";
}
//Fin funcion para cargar el archivo


// Selecciona todos los textarea con la clase 'contador'
var textareas = document.querySelectorAll('.contador');

// Itera sobre cada textarea y agrega el evento 'click' para mover el cursor a la posición 0
textareas.forEach(function(textarea) {
    textarea.addEventListener('click', function() {
        textarea.selectionStart = 0;  // Mueve el cursor al inicio
        textarea.selectionEnd = 0;    // Finaliza en la posición 0
    });
});


								