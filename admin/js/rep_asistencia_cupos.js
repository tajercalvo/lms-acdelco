$(document).ready(function(){
    // Select Placeholders
    /*$("#schedule_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });*/
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    var Timer_ImgLanza = setTimeout('CargarGraficas()', 1000);
});
function CargarGraficas(){
    $(".ElementID").each(function (index) {
        var Sche_id_val = $(this).val();
        var DatConce_min = [];
        var DatConce_max = [];
        var DatConce_ins = [];
        var DataNames = [];
        var _CtrlVar = 0;
        $(".ValSche"+Sche_id_val).each(function (index) {
            _CtrlVar = _CtrlVar + 1;
            //console.log("Concesionario:"+this.value);
            DatConce_min.push([_CtrlVar, $(this).attr('data-min') ]);
            DatConce_max.push([_CtrlVar, $(this).attr('data-max') ]);
            DatConce_ins.push([_CtrlVar, $(this).attr('data-ins') ]);
            DataNames.push([_CtrlVar, $(this).attr('data-deal') ]);
        });
        var ds = new Array();
        ds.push({
            label: "Mínimos",
            data:DatConce_min,
            bars: {order: 1}
        });
        ds.push({
            label: "Máximos",
            data:DatConce_max,
            bars: {order: 2}
        });
        ds.push({
            label: "Inscripciones",
            data:DatConce_ins,
            bars: {order: 3}
        });
        var data = ds;

        $.plot($("#chart_ordered_bars"+Sche_id_val), data, {
            bars: {
                show:true,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y.0",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames
            }
        });
    });
}