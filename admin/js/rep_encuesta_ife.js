$(document).ready(function(){
    // Select Placeholders
    $("#dealer_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });
    $("#zone_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });
    $("#specialty_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });
    $("#supplier_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    var Timer_ImgLanza = setTimeout('CargarGraficas()', 2000);
});

function CargarGraficas(){
    $(".ElementID").each(function (index) {
        var Quest_id_val = $(this).val();
        var dat_val = [];
        var dat_obj = [];
        var DataNames = [];
        var _CtrlVar = 0;
        $(".Question_Id"+Quest_id_val+"_Val").each(function (index) {
            _CtrlVar = _CtrlVar + 1;
            dat_val.push([ _CtrlVar , $(this).val() ]);
            var nameMes = $(this).attr('name');
            console.log(nameMes + nameMes.indexOf("2015") );
            if( (nameMes.indexOf("2015") == 0) || (nameMes.indexOf("2016 Ene") == 0) || (nameMes.indexOf("2016 Feb") == 0) || (nameMes.indexOf("2016 Mar") == 0) ){
                dat_obj.push([ _CtrlVar , 75]);
            }else{
                dat_obj.push([ _CtrlVar , 90]);
            }
            DataNames.push([ _CtrlVar, $(this).attr('name') ]);
        });
        var ds = new Array();
        ds.push({
            label: "Indice",
            data: dat_val,
            bars: {order: 1}
        });
        ds.push({
            label: "Objetivo",
            data: dat_obj,
            bars: {order: 2}
        });
        var data = ds;
        $.plot($("#GrafEnq_Gral_"+Quest_id_val), data, {
            bars: {
                show:false,
                barWidth: 0.2,
                fill:1
            },
            colors: ["#0829FF","#FF0000"],//#0829FF, "#FF0000","#FFFF00","#00FF00"
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y.0",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames
            }
        });
    });
}