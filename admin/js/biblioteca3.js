$(document).ready(function() {
  expandirRecomendados();
  expandirMasvisto();
  // animacion de entrada
  setTimeout(function(){
            $("#mibody").fadeIn(1000);
            $("#load").html("");
    }, 1000);
  CargarImagenes();
  $('#body').css('visibility', 'visible');
});

// select 2
    $('#compartir').select2({

        placeholder: 'Seleciona un usuario',
        ajax: {
          url: 'controllers/biblioteca3.php',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }

      });

//guardarMasTarde
function guardarMasTarde(idLibreria) {
  var data = new FormData();
        data.append('opcn','guardarLibreria');
        data.append('id',idLibreria);
        var url = "controllers/biblioteca3.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            // dataType: "text",
            async: true,
            cache:false,
            success: function(data) {
                  if (data == 'sessionOff'){
                    mensajeSinSession();
                  }else if(data.resultado == "SI"){
                      notyfy({
                          text: 'Guardado para ver más tarde',
                          type: 'success' // alert|error|success|information|warning|primary|confirm
                      });
                }else{
                  notyfy({
                    text: 'Ya guardaste esta multimedia anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
                }
                // $('.notyfy_message').parent().fadeOut(3000);
            }
        }).fail(function() {
          mensajeSinSession();
      });
}

// Modal guardados
function misguardados() {

  var data = new FormData();
        data.append('opcn','misguardados');
        var url = "controllers/biblioteca3.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "text",
            // dataType: "text",
            async: true,
            cache:false,
            success: function(data) {

               document.getElementById("vistos").innerHTML = "";
                if (data == 'sin_session') {
                    document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';
                    setTimeout(function() {
                    $(".gritter-item-wrapper").fadeOut(5000);
                },3000);
                }else{
                  document.getElementById("vistos").innerHTML = data;
                }
            }
        }).fail(function() {
          mensajeSinSession();
      });
}
// fin

// modal compartdidos
function misCompartidos() {
  var data = new FormData();
        data.append('opcn','misCompartidos');
        var url = "controllers/biblioteca3.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "text",
            // dataType: "text",
            async: true,
            cache:false,
            success: function(data) {

                document.getElementById("misCompartidos").innerHTML = "";
                if (data == 'sin_session') {
                    document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';
                    setTimeout(function() {
                    $(".gritter-item-wrapper").fadeOut(5000);
                },3000);
                }else{
                  document.getElementById("misCompartidos").innerHTML = data;
                }

            }
        }).fail(function() {
          mensajeSinSession();
      });
}
// fin

// modal compartdidos
$('#compartirCon').click(function(){
    var data = new FormData();
        data.append('opcn','compartirCon');
        data.append('usuario',$('#compartir').val());
        data.append('libraryid',$('#idarchivo').val());
        var url = "controllers/biblioteca3.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            // dataType: "text",
            async: true,
            cache:false,
            success: function(data) {
                if (data == 'sin_session') {
                    document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';
                    setTimeout(function() {
                    $(".gritter-item-wrapper").fadeOut(5000);
                },3000);
                }else if(data.resultado == "SI"){
                      notyfy({
                          text: 'Compartido',
                          type: 'success' // alert|error|success|information|warning|primary|confirm
                      });
                }else{
                  notyfy({
                    text: 'Ya compartiste esta multimedia anteriormente con este usuario, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
                }
            }
        }).fail(function() {
          mensajeSinSession();
      });
});

function compartirCon(idarchivo, nombre) {
  $('#idarchivo').val(idarchivo);
  $('#idnombrearchivo').val(nombre);
  }
// fin

(function ($) {
filtroPrimario = 1;
// Mantener contenido activo
$( "#menuPrincipal > div > .btn-colorgm").click(function() {

  $( ".btn-colorgm" ).removeClass( "active" );
  var id =$(this).attr("id");
  $( "#"+id ).addClass( "active" );

  // $('span.btn-colorgray').removeClass( "active" );
  // $('#rowUltimo > div > span.btn-colorgray:first').addClass( "active" );
  // $('#rowrecomendados > div > span.btn-colorgray:first').addClass( "active" );
  // $('#rowMasvisto > div > span.btn-colorgray:first').addClass( "active" );

  if (id == 'tab-Producto') {
      filtroPrimario = 1;
      consulta('consultartodos', '0', '1'); //opcn, consulta instantanea y segmento
    }else if(id == 'tab-HabTecnicas'){
      filtroPrimario = 2;
      consulta('consultartodos', '0', '2');
    }else if(id == 'tab-HabBlandas'){
      filtroPrimario = 3;
      consulta('consultartodos', '0', '3');
    }else if(id == 'tab-KnowHowChevr'){
      filtroPrimario = 4;
      consulta('consultartodos', '0', '4');
    }
    //console.log('filtroPrimario = '+filtroPrimario);
});

$( ".btn-colorgray" ).click(function() {

    idPadre = $(this).parent().parent().attr('id');
    $( "#"+idPadre+" div "+".btn-colorgray" ).removeClass( "active" );
    var id =$(this).attr("id");
    $( "#"+id ).addClass( "active" );

    id = id.split("-");
    id = id[0]+'-'+id[1];
    if (id == 'tab-todos') {
      consulta('consultartodos', '0', filtroPrimario);
    }else if (id == 'tab-video') {
      consulta('consultarvideos', '0', filtroPrimario);
    }else if (id == 'tab-audioLibro') {
      consulta('consultaaudiolibros', '0', filtroPrimario);
    }else if (id == 'tab-archivo') {
      consulta('consultaarchivos', '0', filtroPrimario);
      //console.log('filtro = consultaarchivos, segmento ='+filtroPrimario);
    }
});

})(jQuery);


// Expandir y contraer
// ultimo
function expandirUltimo() {
  clase = $('#expandirLoUltimo').attr('class');
    var res = clase.split("-");
    if(res[(res.length-1)] == 'up'){
      // $('#contenidoVertical').css("visibility","hidden");
      $('#expandirLoUltimo').removeClass('fa-angle-double-up');
      $('#expandirLoUltimo').addClass('fa-angle-double-down');
      $("#loUltimoh").slideUp(500);
    }else{
      $('#expandirLoUltimo').removeClass('fa-angle-double-down');
      $('#expandirLoUltimo').addClass('fa-angle-double-up');
      $("#loUltimoh").slideDown(1000);
    }
}
$('#expandirLoUltimo').click(function(){
    expandirUltimo();
});

$('#verMasUltimo').click(function(){
    expandirUltimo();
});

// recomendados
function expandirRecomendados() {
   clase = $('#expandirRecomendado').attr('class');
    var res = clase.split("-");
    if(res[(res.length-1)] == 'up'){
      $('#expandirRecomendado').removeClass('fa-angle-double-up');
      $('#expandirRecomendado').addClass('fa-angle-double-down');
      $("#recomendadoh").slideUp(500);
    }else{
      $('#expandirRecomendado').removeClass('fa-angle-double-down');
      $('#expandirRecomendado').addClass('fa-angle-double-up');
      $("#recomendadoh").slideDown(1000);
    }
}
$('#expandirRecomendado').click(function(){
   expandirRecomendados();
});

$('#verMasRecomendados').click(function(){
   expandirRecomendados();
});

// mas visto
function expandirMasvisto() {
  clase = $('#expandirMasvisto').attr('class');
    var res = clase.split("-");
    if(res[(res.length-1)] == 'up'){
      $('#expandirMasvisto').removeClass('fa-angle-double-up');
      $('#expandirMasvisto').addClass('fa-angle-double-down');
      $("#masvistoH").slideUp(500);
    }else{
      $('#expandirMasvisto').removeClass('fa-angle-double-down');
      $('#expandirMasvisto').addClass('fa-angle-double-up');
      $("#masvistoH").slideDown(1000);
    }
}

$('#expandirMasvisto').click(function(){
    expandirMasvisto();
});

$('#verMasVisto').click(function(){
    expandirMasvisto();
});
// FIn Expandir y contraer

//Fin Mantener contenido activo
function CargarImagenes()
{
    $(".post-carga").each(function(){
        $(this).attr('src', $(this).data('src')).load(function(){
            $(this).fadeIn(1000);
        });
    });
}

clic = 0;

function consulta(opcn, buscar, segmento){
      $('.fondoGMMasOscuro').css('visibility', 'hidden');
      $('.contenidoVertical').html('');
      $(".claseMostrar").html('');
      $('.nombreInterno').text('');
      $('.descripcionInterno').text('');

        CargarImagenes();
    // $('#contenidoVertical').html('<div id="cargando_pagina"><img style="width: 15px; " src="../assets/loading-course.gif"> </div>');
        var data = new FormData();
        data.append('opcn',opcn);
        if (buscar == 0) {
          data.append('segmento',segmento);
        }else{
          data.append('consultainstantanea',buscar);
        }

        var url = "controllers/biblioteca3.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            // dataType: "text",
            async: true,
            cache:false,
            success: function(data) {
              //console.log(data);
                  if (data == 'sessionOff'){
                    mensajeSinSession();
                    return false;
                  }
                   var contenido ='';
                   var archivo ='';
                   var filtro ='';
                   var imagen = '';
                   var data_toggle = '';
                   var segmento = '';
                   var nombre = '';
                   var descripcion = '';
                 for (var i = 0; i < data.length; i++) {

                                    if (data[i]['file'].indexOf("youtube") > -1) {
                                      var res = data[i]['file'].split("=");
                                      archivo = res[1];
                                      filtro = 'youtube';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_videosProducto';
                                    }else if(data[i]['file'].indexOf(".mp3") > -1){
                                      archivo =data[i]['file'];
                                      filtro = 'mp3';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_audiolibrosProducto';
                                    }else if(data[i]['file'].indexOf(".mp4") > -1){
                                      archivo =data[i]['file'];
                                      filtro = 'mp4';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_videosProducto';
                                    }else if(data[i]['file'].indexOf(".mov") > -1){
                                      archivo =data[i]['file'];
                                      filtro = 'mp4';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_videosProducto';
                                    }else if(data[i]['file'].indexOf(".pdf") > -1){
                                      archivo =data[i]['file'];
                                      filtro = 'pdf';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_archivosProducto';
                                    }else if(data[i]['file'].indexOf(".html") > -1){
                                      archivo =data[i]['file'];
                                      filtro = 'html';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_archivosProducto';
                                    }else if(data[i]['file'].indexOf(".pptx") > -1){
                                      archivo =data[i]['file'];
                                      filtro = 'pptx';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_archivosProducto';
                                    }else if(data[i]['file'].indexOf(".php") > -1){
                                      archivo =data[i]['file'];
                                      filtro = 'php';
                                      imagen = data[i]['image'];
                                      library_id = data[i]['library_id'];
                                      segmento = data[i]['segment_id'];
                                      nombre = data[i]['name'];
                                      descripcion = data[i]['description'].toLowerCase().replace('(','').replace(')','');
                                      // data_toggle = 'filtro_archivosProducto';
                                    }else{
                                      archivo = '';
                                      filtro = '';
                                      imagen = '';
                                      data_toggle = '';
                                      segmento = '';
                                    }
                                    //console.log(data[i]['name']);
                                    contenido += '<div class="widget widget-heading-simple widget-body-gray widget-offers">';
                                    contenido += '<div class="widget-body">';
                                    contenido += '<!-- Media item -->';
                                    contenido += '<div class="media">';
                                    contenido += '<a onclick="cambiarcontenido(\''+archivo+'\' , \''+filtro+'\', \''+imagen+'\', \''+library_id+'\', \''+segmento+'\', \''+nombre+'\', \''+descripcion+'\');" data-toggle="tab" href="#'+data_toggle+'"><div class="img cargador" ><img class="post-carga media-object pull-left thumb hidden-tablet hidden-phone" src="../assets/Biblioteca/m/'+imagen+'" style="width: 168px; height: 94px;"></div></a>';
                                    contenido += '<div class="media-body">';
                                    contenido += '<strong><h><a class="a" onclick="cambiarcontenido(\''+archivo+'\', \''+filtro+'\', \''+imagen+'\', \''+library_id+'\', \''+segmento+'\', \''+nombre+'\', \''+descripcion+'\');" data-toggle="tab" href="#'+data_toggle+'">'+data[i]['name'].toUpperCase().substring(0, 40)+'</a></h></strong>';
                                    contenido += '<p>'+descripcion.charAt(0).toUpperCase()+descripcion.slice(1, 100)+'</p>';
                                    contenido += '</div>' ;
                                    contenido += '<div class="row ">';
                                    contenido += '<div class="col-md-1"><a onclick="guardarMasTarde('+library_id+'); return false;" href="" class="glyphicons history single-icon agregar btn-white" data-placement="left"><i></i> </a></div>';
                                    contenido += '<div class="col-md-1"><a onclick="compartirCon('+library_id+', \''+nombre+'\'); return false;" href="#" class="glyphicons share_alt single-icon agregar btn-white" data-toggle="modal" data-target="#modalCompartirCon"><i></i> </a></div>';
                                    contenido += '<div class="col-md-1"><a href="../assets/Biblioteca/'+archivo+'" download="'+nombre+'" class="glyphicons cloud-upload single-icon agregar btn-white" data-placement="left"><i></i> </a></div>';
                                    contenido += '<div class="col-md-9"></div>';
                                    //contenido += '<button class="btn btn-primary btn-xs"><i class="fa fa-fw icon-refresh-star-fill"></i></button>';
                                    //contenido += '<button class="btn btn-primary btn-xs"><i class="fa fa-fw icon-manager"></i></button>';
                                    //contenido += '<button class="btn btn-primary btn-xs"><i class="fa fa-cloud-download"></i><a href="../assets/Biblioteca/'+data[i]['file']+'" download>.</button>';
                                    contenido += '</div>';
                                    contenido += '</div>';
                                    contenido += '</div>';
                                    contenido += '</div>';
                                    contenido += '</div>';
                                    //Cambia el contenido interno de acuerdo al primer archivo que se lista

                                       if (i==0) {
                                        cambiarcontenido(archivo, filtro, imagen, library_id, segmento, nombre, descripcion);
                                       }

                }// FIN for

                // $('#contenidoVerticalLoUltimo').html(contenido);
                // $('#contenidoVerticalRecomendados').html(contenido);
                // $('#contenidoVerticalMasVisto').html(contenido);
                // $('#contenidoVerticalLoUltimo').hide().fadeIn();

                $('.contenidoVertical').html(contenido);
                $('.contenidoVertical').hide().fadeIn();
                CargarImagenes();
            }
        }).fail(function() {
          mensajeSinSession();

      });
}

// $( "#txtSearch" ).keyup(function() {
//   autoplay = 1;
//   console.log('buscar');
//   consulta('consultainstantanea', $( "#txtSearch" ).val(), 0);
// });
// evento al pulsar enter
$("#txtSearch").keypress(function(e) {
       if(e.which == 13) {
          consulta('consultainstantanea', $( "#txtSearch" ).val(), 0);
          return false;
       }
    });

$('#btnEnviarComentario').click(function(){
      var txt_comentario = $('#txt_comentario').val();
      if (txt_comentario == '') {
        $('#txt_comentario').css('border', '2px solid #d65050');
        $( "#txt_comentario" ).focus();
        return false;
      }

      var library_id = $('#library_id').val();

      var NameUsuario = $('#NameUsuario').val();
      var dealer = $('#dealer').val();
      var imagen = $('#imagen').val();


      var usuarioID = $('#user_id').val();
      var f = new Date();
      var id_comentario_usuario = usuarioID +""+f.getTime();

       var contenido ='';
       contenido += '<div id="comentario_'+id_comentario_usuario+'" class="timeline-top-info content-filled border-bottom">';
       contenido += '<a class="pull-left" href="#"><img src="../assets/images/usuarios/'+imagen+'" alt="photo" class="media-object" width="35"></a>';
       contenido += '<div class="media-body"> <strong> '+dealer+'</strong>';
       contenido += '<a class="a" href="ver_perfil.php?back=usuarios&amp;id='+usuarioID+'" target="_blank" > '+NameUsuario+'</a>';
       contenido += '<p id="P'+id_comentario_usuario+'" class="comentarios" style="color: white">'+txt_comentario+' </p>  ';
       contenido += '<div class="timeline-bottom" id="statusComentario_id'+id_comentario_usuario+'"><i class="fa fa-clock-o"></i> Enviando comentario</div>';
       contenido += '</div>';
       contenido += '</div>';
      $('#comentarios').append(contenido);
      $('#txt_comentario').val('');

        var data = new FormData();
        data.append('opcn','crearComentario');
        data.append('library_id',library_id);
        data.append('txt_comentario',txt_comentario);
        data.append('id_comentario_usuario',id_comentario_usuario);

        var url = "controllers/biblioteca3.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            // dataType: "text",
            async: true,
            cache:false,
            success: function(data) {
                  if (data == 'sessionOff'){
                    mensajeSinSession();
                    return false;
                  }
                  $('#P'+id_comentario_usuario).append('<span class="label label-danger"><a href="#" onclick="eliminarComentario(\''+data['id']+'\', \''+data['id_comentario_usuario']+'\'); return false;" data-toggle="modal" style="color: white;"><i class="fa fa-trash-o"></i></a></span>');
                  $('#statusComentario_id'+id_comentario_usuario).html('<i class="fa fa-clock-o"></i> '+data['fecha']);
            }
        }).fail(function() {
          mensajeSinSession();

      });

});


function mensajeSinSession(){
         setTimeout(function() {

                    $(".notyfy_container").fadeOut(4000);

              },4000);

             //document.getElementById("vistos").innerHTML = '<span class="label label-danger">Realizando la consulta, por favor espere…</span>';
             document.getElementById("vistos").innerHTML = '';
             document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';

             setTimeout(function() {

                    $(".gritter-item-wrapper").fadeOut(5000);

              },3000);
}

function activarTaba(tabPrincipal, tabSecundario) {
  var tab1 = cadena.charAt(tabPrincipal.length-1);
  var tab2 = '-'+cadena.charAt(tabSecundario.length-1);
  var arreglo = [tab1, tab2];
  return arreglo;
}

function cambiarcontenido(archivo, filtro, imagen, library_id, segmento, nombre, descripcion){
  //console.log('archivo '+archivo+' Filtro '+filtro+' Imagen '+imagen+' id '+library_id+' segmento '+segmento+' Nombre '+nombre+' Descripcion '+descripcion);
  $('.nombreInterno').text(nombre.toUpperCase());
  $('.descripcionInterno').text(descripcion.charAt(0).toUpperCase() + descripcion.toLowerCase().replace('(','').replace(')','').slice(1, 100));
  $('.a-guardar').html('<a onclick="guardarMasTarde('+library_id+'); return false;" href="" class="glyphicons history single-icon agregar btn-white" data-placement="left"><i></i> </a>');
  $('.a-compartir').html('<a onclick="compartirCon(\''+library_id+'\',\''+nombre+'\' ); return false;" href="#" class="glyphicons share_alt single-icon agregar btn-white" data-toggle="modal" data-target="#modalCompartirCon"><i></i> </a>');
  $('.a-descargar').html('<a href="../assets/Biblioteca/'+archivo+'" download="'+nombre+'" class="glyphicons cloud-upload single-icon agregar btn-white" data-placement="left"><i></i> </a>');

       if (segmento == 1) {
        tabPrincipal = 'tab-Producto';
        filtroPrimario = 1;
       }else if(segmento == 2){
        tabPrincipal = 'tab-HabTecnicas';
        filtroPrimario = 2;
       }else if(segmento == 3){
        tabPrincipal = 'tab-HabBlandas';
        filtroPrimario = 3;
       }else if(segmento == 4){
        tabPrincipal = 'tab-KnowHowChevr';
        filtroPrimario = 4;
       }

      ViewLibary(library_id); //guardar que el usuario vio este elemento

     if (filtro == 'youtube') {
              $(".claseMostrar").html('<iframe width="518" height="365" src="https://www.youtube.com/embed/'+archivo+'?autoplay=0" allowfullscreen></iframe>');
              $('.claseMostrar').hide().fadeIn(1000);
              tabSecundario = 'tab-video';
            }else if (filtro == 'pdf'){
                $(".claseMostrar").html('<a target="_blank" href="../assets/Biblioteca/'+archivo+'"><img class="img-responsive" align="auto" src="../assets/Biblioteca/m/'+imagen+'" class="img-responsive" ></a>');
                $('.claseMostrar').hide().fadeIn(1000);
                tabSecundario = 'tab-archivo';
            }else if (filtro == 'pptx'){
                $(".claseMostrar").html('<a target="_blank" href="../assets/Biblioteca/'+archivo+'"><img class="img-responsive" align="auto" src="../assets/Biblioteca/m/'+imagen+'" class="img-responsive" ></a>');
                $('.claseMostrar').hide().fadeIn(1000);
                tabSecundario = 'tab-archivo';
            }else if (filtro == 'mp3'){
                  var div ='';
                  div += '<img align="middle" src="../assets/Biblioteca/m/'+imagen+'" class="img-responsive">';
                  div += '<audio src="../assets/Biblioteca/'+archivo+'" controls class="img-responsive"></audio>';
                  $(".claseMostrar").html(div);
                  $('.claseMostrar').hide().fadeIn(1000);
                  tabSecundario = 'tab-audioLibro';
            }else if (filtro == 'mp4'){
                $(".claseMostrar").html('<video data-library_id='+library_id+' class="img-responsive" controls ><source src="../assets/Biblioteca/'+archivo+'" type="video/mp4">Your browser does not support the video tag.</video>');
                $('.claseMostrar').hide().fadeIn(1000);
                vio_video()
                tabSecundario = 'tab-video';
            }else if (filtro == 'html'){
                $(".claseMostrar").html('<a target="_blank" href="'+archivo+'"><img class="img-responsive" align="auto" src="../assets/Biblioteca/m/'+imagen+'" class="img-responsive" ></a>');
                $('.claseMostrar').hide().fadeIn(1000);
                tabSecundario = 'tab-archivo';
            }else if (filtro == 'php'){
              console.log('url = '+archivo);
                $(".claseMostrar").html('<a target="_blank" href="'+archivo+'"><img class="img-responsive" align="auto" src="../assets/Biblioteca/m/'+imagen+'" class="img-responsive" ></a>');
                $('.claseMostrar').hide().fadeIn(1000);
                tabSecundario = 'tab-archivo';
            }
            $( ".btn-colorgm" ).removeClass( "active" );
            $( "span.btn-colorgray" ).removeClass( "active" );
            $( "#"+tabSecundario ).addClass( "active" );
            $( "#"+tabSecundario+'-Masvisto' ).addClass( "active" );
            $( "#"+tabSecundario+'-recomendados' ).addClass( "active" );
            $( "#"+tabPrincipal ).addClass( "active" );
            $('.fondoGMMasOscuro').css('visibility', 'visible');
}

// Selec que estan dentro de form para cargar o actualizar lso archivos
$(document).ready(function(){
    // Select Placeholders
    $("#type_library").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#charge_id").select2({
        placeholder: "Seleccione el (los) cargos",
        allowClear: true
    });

    $("#course_id").select2({
        placeholder: "Seleccione el (los) cursos",
        allowClear: true
    });

    $("#dealer_id").select2({
        placeholder: "Seleccione el (los) concesionarios",
        allowClear: true
    });
    $("#rol_id").select2({
        placeholder: "Seleccione el perfil",
        allowClear: true
    });
    $("#librarylibrary_id").select2({
        placeholder: "Seleccione el (los) documentos",
        allowClear: true
    });
    $("#private").select2({
        placeholder: "Seleccione el la seguridad",
        allowClear: true
    });

    $("#url").prop('disabled', false); //input activado
$("#file1").prop('disabled', true); // input inactivado

});
//Fin select

// Inicio Funcion para seleaccionar un archivo o una URL
var valida_input = 0;

$('#checkbox').click(function  input (){
  if(valida_input % 2 == 0) {
$("#url").prop('disabled', true);
$("#url").val("");
$("#url").attr("placeholder", "URL desactivada, seleccione archivo");
$("#file1").prop('disabled', false);
  }else{
$("#url").prop('disabled', false);
$("#file1").prop('disabled', true);
$("#file1").val("");
$("#url").val("");
$("#url").attr("placeholder", "URL Ej: https://www.youtube.com");
  }
valida_input = valida_input+1;
});

// Funcion para recibir los likes de los usuarios
function LikeLibrary(library_id){

    var data = new FormData();
    data.append('library_id',library_id);
    data.append('opcn','LikeLibrary');
    var url = "controllers/biblioteca3.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){

                   var currentFile = "";
                        // Check for audio element support.
                        if (window.HTMLAudioElement) {

                            try {
                                var oAudio = document.getElementById('myaudio');
                                var btn = document.getElementById('play');
                                var audioURL = document.getElementById('audiofile');


                                if (audioURL.value !== currentFile) {
                                    oAudio.src = audioURL.value;
                                    currentFile = audioURL.value;
                                }


                                if (oAudio.paused) {
                                    oAudio.play();
                                    btn.textContent = "Pause";
                                    oAudio.currentTime = 0;
                                }
                                else {
                                    oAudio.pause();
                                    btn.textContent = "Play";
                                    oAudio.currentTime = 0;
                                }
                            }
                            catch (e) {

                                 if(window.console && console.error("Error:" + e));
                            }
                        }
                var Cantlikes = parseInt($('#CantLikes'+library_id).text());
                $('#CantLikes'+library_id).text(Cantlikes+1);
            }else{

                notyfy({
                    text: 'Ya votaste por esta multimedia anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }

             setTimeout(function() {

                    $(".notyfy_container").fadeOut(4000);

              },4000);
        }
    })
    .fail(function() {

        document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';
        //$(".gritter-item-wrapper").slideDown(1000);


        setTimeout(function() {

                    $(".gritter-item-wrapper").fadeOut(5000);

              },3000);

    })
    //.done(function() { document.getElementById("estado_conexion_ajax").innerHTML = '';})
}
// Fin funcion like

//Inicio Funcion para capturar quien vio o descargo el archivo
function ViewLibary(library_id){
    tiempo_video = 0;
    num = 0;

    var data = new FormData();
    data.append('library_id',library_id);
    data.append('opcn','ViewLibary');
    var url = "controllers/biblioteca3.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            // if(data.resultado=="SI"){

            //     num = 1;

            //      console.log('num es igual a '+num+' en ajax');
            //     var Cantviews = parseInt($('#CantViews'+library_id).text());
            //     $('#CantViews'+library_id).text(Cantviews+1);

            // }
        }
    })
    .fail(function() {
      console.log('error ajax');
      // document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';
      // setTimeout(function() {

      //               $(".gritter-item-wrapper").fadeOut(5000);

      //         },3000);

    })

        setTimeout(function() {

                    $(".gritter-item-wrapper").fadeOut(5000);

              },3000);

              console.log(num);

    if(num == 1){
      return true;
    }else{
      return false;
    }

}

//Fin funcion ver libreria
//Inicio funcion, para actualizar los atributos del arhchivo
function UpdateLibrary(){

  if (validar_conexion()) {

    var data = new FormData();
  data.append('library_id', $('#library_id').val());
  data.append('opcn','editar');
  data.append('name',$('#name').val());
  data.append('description',$('#description').val());
  data.append('keywords',$('#keywords').val());
  data.append('type_library',$('select[name=type_library]').val());
  data.append('status_id',$('select[name=status_id]').val());
  data.append('charge_id', $('#charge_id').val());
  data.append('course_id', $('#course_id').val());
  data.append('dealer_id', $('#dealer_id').val());
  data.append('rol_id', $('#rol_id').val());
  data.append('librarylibrary_id', $('#librarylibrary_id').val());
  data.append('private', $('#private').val());


    var url = "controllers/biblioteca3.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){

              notyfy({
                    text: 'Archivo actualizado correctamente ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });

            }else{
                notyfy({
                    text: 'A ocurrido un error, intenta nuevamente',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    })
.fail(function() { document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente </p></div></div></div>'; })


  }

}
//Fin ACtualizar archivo


//Inicio para subir o crear los archivos
function _(el){
  return document.getElementById(el);
}

function uploadFile(){

  //Validar chaeck box
  if($('#checkbox').prop('checked')==false) {
    console.log('No esta seleccionado');

  var comprobar = $('#file1').val().length * $('#image').val().length * $('#name').val().length * $('#description').val().length * $('#keywords').val().length;

  }else{

    var comprobar = $('#url').val().length * $('#image').val().length * $('#name').val().length * $('#description').val().length * $('#keywords').val().length;
    var tamano_file = 0;
  }

  if(comprobar>0){
  var file = _("file1").files[0];
  var image = _("image").files[0];

  //Limitando el tamaño del archivo en bytes
     /*$('.input-file').change(function (){
     var sizeByte = this.files[0].size;
     var siezekiloByte = parseInt(sizeByte / 1024);

     if(siezekiloByte > $(this).attr('size')){
         alert('El tamaño supera el limite permitido');
         $(this).val('');
     }
   });*/
  // fin

  if(tamano_file>=100000000){
            notyfy({
                text: 'El archivo excede el tamaño máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }else{

          //Inicio renombrando el archivo para evitar puntos ( . ) adicionales al inertar en la base de datos
          var fileName = document.getElementById('file1').files[0].name;
          arrayFileName = fileName.split(".");
          newFileName ="";

          for (var i = 0; i < arrayFileName.length; i++) {
            if (i == (arrayFileName.length-1)) {
              newFileName +='.';
            }
             newFileName += arrayFileName[i];
          }
          //console.log(newFileName);
          //Fin enombrando el archivo para evitar puntos ( . ) adicionales al inertar en la base de datos

  var formdata = new FormData();
  formdata.append("file1", file);
  formdata.append("image", image);
  formdata.append('name',$('#name').val());
  formdata.append('name_file',newFileName);
  formdata.append('description',$('#description').val());
  formdata.append('keywords',$('#keywords').val());
  formdata.append('type_library',$('select[name=type_library]').val());
  formdata.append('status_id',$('select[name=status_id]').val());
  formdata.append('opcn',$('#opcn').val());
  formdata.append('charge_id', $('#charge_id').val());
  formdata.append('course_id', $('#course_id').val());
  formdata.append('dealer_id', $('#dealer_id').val());
  formdata.append('rol_id', $('#rol_id').val());
  formdata.append('librarylibrary_id', $('#librarylibrary_id').val());
  formdata.append('private', $('#private').val());
  formdata.append('url', $('#url').val());

  var ajax = new XMLHttpRequest();

  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  if($('#checkbox').prop('checked')==false) {
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.addEventListener("error", errorHandler, false);
  ajax.addEventListener("abort", abortHandler, false);
  }

  ajax.open("POST", "controllers/biblioteca3.php");
  ajax.send(formdata);

}
}
else{

  notyfy({
      text: 'Completa la informacion antes de subir un archivo',
      type: 'warning'
  });

  return false;

}

}

function progressHandler(event){
  _("loaded_n_total").innerHTML = "Se han cargado "+event.loaded+" bytes de "+event.total;
  var percent = (event.loaded / event.total) * 100;
  _("progressBar").style.width = Math.round(percent)+'%';
  // _("progressBar").value = Math.round(percent);
  _("status").innerHTML = Math.round(percent)+'% Cargando... por favor espere'+'"';
}
function completeHandler(event){
  _("status").innerHTML = event.target.responseText;
  setTimeout(_("progressBar").style.width = "0", 3000);

   notyfy({
                    text: 'Archivo cargado correctamente ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
   $('#Modalprogress_bar').modal('hide');
   $( "#txtSearch" ).focus();
  // _("progressBar").style.width = "0";
  // _("progressBar").value = 0;
}
function errorHandler(event){
  _("status").innerHTML = "Upload Failed";
}
function abortHandler(event){
  _("status").innerHTML = "Upload Aborted";
}
//Fin funcion para cargar el archivo


// validar si vio video
var tiempo_video = 0;
function vio_video() {

    var v = document.querySelector(".claseMostrar video");
    v.addEventListener("timeupdate",function(ev){
       console.log(v.currentTime);
       tiempo_video = v.currentTime;
     },true);

    $("video").on('ended', function(){

      var library_id = $(this).data('library_id');
      var data = {opcn: 'finis_video', library_id: library_id, tiempo_video:tiempo_video };

      $.ajax({
        url: 'controllers/biblioteca3.php',
        type: 'post',
        dataType: 'json',
        data: data,
      })
      .done(function() {
        console.log("success");
        notyfy({
              text: 'Video finalizado',
              type: 'success' // alert|error|success|information|warning|primary|confirm
          });
      })
      .fail(function() {
        console.log("error");
      })
    });

    $("video").on('play', function(){

      var library_id = $(this).data('library_id');
      var data = {opcn: 'play_video', library_id: library_id };

      $.ajax({
        url: 'controllers/biblioteca3.php',
        type: 'post',
        dataType: 'json',
        data: data,
      })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })

    });


}