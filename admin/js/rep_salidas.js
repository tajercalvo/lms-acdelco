$(document).ready(function(){
    // Select Placeholders
    $("#dealer_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2012-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2012-01-01"
    });
});

$(document).ready(function() {
    // By suppling no content attribute, the library uses each elements title attribute by default
    $('a[href][title]').qtip({
        content: {
            text: false // Use each elements title attribute
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        } // Give it some style
    });
    // NOTE: You can even omit all options and simply replace the regular title tooltips like so:
    // $('#content a[href]').qtip();
});
