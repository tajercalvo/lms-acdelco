$(document).ready(function(){
    $("#dealer_id").select2({
        placeholder: "Seleccione la (las) empresas",
        allowClear: true
    });

    $("#estado").select2({
      placeholder: "Seleccione un estado",
      allowClear: true
  });

  $("#despliegue_mensual").select2({
    placeholder: "Seleccionar despliegue",
    allowClear: true
  });
  
  $('#fecha_inicio').bdatepicker({
    format: "yyyy-mm-dd",
    startDate: "2015-01-01"
  });

  $('#fecha_fin').bdatepicker({
    format: "yyyy-mm-dd",
    startDate: "2015-01-01"
  });
});

//variable globales
encuesta_id = 0;
array_preguntas = [];
opcen = '';
$('.editar').click(function (e) { 
  e.preventDefault();
  opcn = 'editar';
  array_preguntas = [];
  encuesta_id = $(this).data('encuesta_id');
  $.ajax({
    url: 'controllers/encuestas_pendientes.php',
    type: 'POST',
    dataType: 'JSON',
    data: {opcn: 'getEncuesta', encuesta_id: encuesta_id }
  })
  .done(function(  data ) {

    var dealer_id = [];
    data.empresas.forEach(e => {
      dealer_id.push(e.dealer_id)
    });
    $("#dealer_id").val(dealer_id).trigger('change');

    array_preguntas = data.preguntas;
    pintar_tabla_preguntas( data.preguntas )

    $("#estado").val(data.status_id).trigger('change');
    $("#despliegue_mensual").val(data.despliegue_mensual).trigger('change');
    $("#encuesta").val(data.encuesta)
    $("#fecha_inicio").val(data.fecha_inicio)
    $("#fecha_fin").val(data.fecha_fin)
  })
  .fail(function() {
    console.log('error');
  });
});

function pintar_tabla_preguntas( json_tabla ) {
  var tabla = "";
  json_tabla.forEach( function(e, i) {
    tabla +=`<tr>
              <td>`+e.encuesta_pregunta+`</td>
              <td><span data-array="`+i+`" data-encuesta_pregunta_id='`+e.encuesta_pregunta_id+`' class="eliminar_pregunta label label-danger"><i class="fa fa-trash-o"></i></span></td>
            </tr>`
    });
  $('#mitabla').html(tabla);
}

$('.agregar_pregunta').click(function (e) { 
  e.preventDefault();
  console.log('Click');
  var descripcion = $('#description').val().trim();
  if( descripcion == ''){alert('La pregunta está vacía'); return false;} 
  $('#description').val('');
  $('#description').focus();
  array_preguntas.push({encuesta_pregunta: descripcion})
  pintar_tabla_preguntas(array_preguntas);
});

$('body').on('click', '.eliminar_pregunta', function (e) {
  e.preventDefault();
  console.log('sisi')
  var pos = $(this).data('array');
  array_preguntas.splice(pos, 1);
  $('#description').val('');
  pintar_tabla_preguntas(array_preguntas);
});

$('#enviar').click(function (e) { 
  e.preventDefault();
  var encuesta = $('#encuesta').val().trim();
  if( encuesta == ''){alert('Campo encuesta está vacío'); return false;}
  
  var despliegue_mensual = $('#despliegue_mensual').val();
  if( despliegue_mensual == ''){alert('Campo despliegue está vacío'); return false;}
  if( despliegue_mensual == 'NO'){
    var fecha_inicio = $('#fecha_inicio').val();
    var fecha_fin = $('#fecha_fin').val();
    if( fecha_inicio == ''){alert('Fecha inicio está vacío'); return false;}
    if( fecha_fin == ''){alert('Fecha fin está vacío'); return false;}
  }
  
  var dealer_id = $('#dealer_id').val();
  if( dealer_id === null){alert('Escoja al menos una empresa'); return false;}

  var estado = $('#estado').val();

  encuesta_id
  array_preguntas
  data = {opcn: opcn, encuesta: encuesta, dealer_id: dealer_id, despliegue_mensual: despliegue_mensual, fecha_inicio: fecha_inicio, fecha_fin: fecha_fin, estado: estado, encuesta_id: encuesta_id, array_preguntas: array_preguntas}
  $.ajax({
    url: 'controllers/encuestas_pendientes.php',
    type: 'POST',
    dataType: 'JSON',
    data: data
  })
  .done(function(  data ) {
    console.log(data);
    if(!data.error){
      alert(data.msj)
      location.reload();
    }else{
      alert(data.msj)
    }
  })
  .fail(function() {
    console.log('error');
  });
});

$('#despliegue_mensual').change(function (e) { 
  e.preventDefault();
  if($(this).val() == 'SI'){
    $('#fechas').css('display', 'none');
  }else{
    $('#fechas').css('display', 'block');
  }
});

$('#agregar').click(function (e) { 
  e.preventDefault();
  opcn = 'crear';
  array_preguntas = [];
  $('#mitabla').html('');
  $("#dealer_id").val({}).trigger('change');
  $('#description').val('');
  $('#fecha_inicio').val('');
  $('#fecha_fin').val('');
  $('#estado').val('1').trigger('change');
  $('#encuesta').val('');
});

