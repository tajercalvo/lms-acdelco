$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/preguntas_r.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});
$(document).ready(function(){
    // Select Placeholders
    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#type").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#type_response").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#image_new').setImagen('ImgPregunta');
});

$( "#formulario_data" ).submit(function( event ) {
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
    event.preventDefault();
});

$("#retornaElemento").click(function() {
    window.location="preguntas_r.php";
});

function valida(){
    //event.preventDefault();
    var valid = 0;
    $('#code').css( "border-color", "#efefef" );
    $('#question').css( "border-color", "#efefef" );
    $('#answer1').css( "border-color", "#efefef" );
    $('#answer2').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#code').val() == ""){
        $('#code').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#question').val() == ""){
        $('#question').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#type_response').val()=="1"){
        if($('#answer1').val() == ""){
            $('#answer1').css( "border-color", "#b94a48" );
            valid = 1;
        }
        if($('#answer2').val() == ""){
            $('#answer2').css( "border-color", "#b94a48" );
            valid = 1;
        }
    }
    try {
        if($('#image_new').val()!=""){
            var inputFileImage = document.getElementById("image_new");
            var file = inputFileImage.files[0];
            console.log(file);
            if(file.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }

        if($('#ImgResp1_image_new').val()!=""){
            var inputFileImage1 = document.getElementById("ImgResp1_image_new");
            var file1 = inputFileImage1.files[0];
            console.log(file1);
            if(file1.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file1.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp2_image_new').val()!=""){
            var inputFileImage2 = document.getElementById("ImgResp2_image_new");
            var file2 = inputFileImage2.files[0];
            console.log(file2);
            if(file2.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file2.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp3_image_new').val()!=""){
            var inputFileImage3 = document.getElementById("ImgResp3_image_new");
            var file3 = inputFileImage3.files[0];
            console.log(file3);
            if(file3.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file3.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp4_image_new').val()!=""){
            var inputFileImage4 = document.getElementById("ImgResp4_image_new");
            var file4 = inputFileImage4.files[0];
            console.log(file4);
            if(file4.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file4.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp5_image_new').val()!=""){
            var inputFileImage5 = document.getElementById("ImgResp5_image_new");
            var file5 = inputFileImage5.files[0];
            console.log(file5);
            if(file5.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file5.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp6_image_new').val()!=""){
            var inputFileImage6 = document.getElementById("ImgResp6_image_new");
            var file6 = inputFileImage6.files[0];
            console.log(file6);
            if(file6.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file6.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp7_image_new').val()!=""){
            var inputFileImage7 = document.getElementById("ImgResp7_image_new");
            var file7 = inputFileImage7.files[0];
            console.log(file7);
            if(file7.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file7.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp8_image_new').val()!=""){
            var inputFileImage8 = document.getElementById("ImgResp8_image_new");
            var file8 = inputFileImage8.files[0];
            console.log(file8);
            if(file8.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file8.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp9_image_new').val()!=""){
            var inputFileImage9 = document.getElementById("ImgResp9_image_new");
            var file9 = inputFileImage9.files[0];
            console.log(file9);
            if(file9.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file9.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
        if($('#ImgResp10_image_new').val()!=""){
            var inputFileImage10 = document.getElementById("ImgResp10_image_new");
            var file10 = inputFileImage10.files[0];
            console.log(file10);
            if(file10.type != "image/jpeg"){
                valid = 1;
                notyfy({
                    text: 'La imagen seleccionada no es jpg, único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            if(file10.size>=1500001){
                valid = 1;
                notyfy({
                    text: 'La imagen excede el peso máximo permitido (1.3 Mb) o no es jpg único formato válido',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    }catch(err) {
        console.log("Error en validación de imagenes");
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
    //return false;
}

/*function Operacion(msg){
    $.ajax({ url: 'controllers/preguntas_r.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}*/
function Operacion(msg){
    try {

        var formElement = document.getElementById("formulario_data");
        console.log( formElement );
        var data = new FormData(formElement);

        if($('#image_new').val()!=""){
            var inputFileImage = document.getElementById("image_new");
            var file = inputFileImage.files[0];
            data.append('image_new',file);
        }
        if($('#ImgResp1_image_new').val()!=""){
            var inputFileImage1 = document.getElementById("ImgResp1_image_new");
            var file1 = inputFileImage1.files[0];
            data.append('ImgResp1_image_new',file1);
        }
        if($('#ImgResp2_image_new').val()!=""){
            var inputFileImage2 = document.getElementById("ImgResp2_image_new");
            var file2 = inputFileImage2.files[0];
            data.append('ImgResp2_image_new',file2);
        }
        if($('#ImgResp3_image_new').val()!=""){
            var inputFileImage3 = document.getElementById("ImgResp3_image_new");
            var file3 = inputFileImage3.files[0];
            data.append('ImgResp3_image_new',file3);
        }
        if($('#ImgResp4_image_new').val()!=""){
            var inputFileImage4 = document.getElementById("ImgResp4_image_new");
            var file4 = inputFileImage4.files[0];
            data.append('ImgResp4_image_new',file4);
        }
        if($('#ImgResp5_image_new').val()!=""){
            var inputFileImage5 = document.getElementById("ImgResp5_image_new");
            var file5 = inputFileImage5.files[0];
            data.append('ImgResp5_image_new',file5);
        }
        if($('#ImgResp6_image_new').val()!=""){
            var inputFileImage6 = document.getElementById("ImgResp6_image_new");
            var file6 = inputFileImage6.files[0];
            data.append('ImgResp6_image_new',file6);
        }
        if($('#ImgResp7_image_new').val()!=""){
            var inputFileImage7 = document.getElementById("ImgResp7_image_new");
            var file7 = inputFileImage7.files[0];
            data.append('ImgResp7_image_new',file7);
        }
        if($('#ImgResp8_image_new').val()!=""){
            var inputFileImage8 = document.getElementById("ImgResp8_image_new");
            var file8 = inputFileImage8.files[0];
            data.append('ImgResp8_image_new',file8);
        }
        if($('#ImgResp9_image_new').val()!=""){
            var inputFileImage9 = document.getElementById("ImgResp9_image_new");
            var file9 = inputFileImage9.files[0];
            data.append('ImgResp9_image_new',file9);
        }
        if($('#ImgResp10_image_new').val()!=""){
            var inputFileImage10 = document.getElementById("ImgResp10_image_new");
            var file10 = inputFileImage10.files[0];
            data.append('ImgResp10_image_new',file10);
        }

        var url = "controllers/preguntas_r.php";
            $.ajax({
                url:url,
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                cache:false,
                success: function(data) {
                    if(data.resultado=="SI"){
                        //$("#ImgPregunta").attr("src","../assets/images/usuarios/"+data.archivo);
                        notyfy({
                            text: 'La información ha sido '+msg+' correctamente',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                    }else{
                        notyfy({
                            text: 'No se ha logrado '+msg+' la información, por favor confirme la información y realicela nuevamente',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                    }
                }
            });
    }catch(err) {
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}
