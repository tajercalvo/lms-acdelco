$(document).ready(function(){

    // Select Placeholders
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    
        // No permitir escribir letras en el campo de las fechas
     $("#start_date_day").keypress(function(tecla) {
            if(tecla.charCode < 48 || tecla.charCode > 57) return false;
        });

        $("#end_date_day").keypress(function(tecla) {
            if(tecla.charCode < 48 || tecla.charCode > 57) return false;
        });

        $('#wrapper').css("visibility", 'visible');

    });
    // FIn no permitir escribir letras en el campo de las fechas

    
 $( "#formulario_usuarios" ).submit(function( event ) {
        return false;
     //event.preventDefault();
 });


$('#btn_consultar').click(function(){
    
    if (validacion()) {

                //Inahibilitando el boton mientras se hace la consulta
                $('#btn_consultar').attr("disabled", true);

                //Lipiando mensaje si no hay registros
                $( "#mensaje_si_vacio" ).html('');
                
                //Limpiando los tablas del asesor comercial
                $('#well_tablas').css("visibility", 'hidden');
                $("#Interacciones_todos tbody").html('');
                $("#Interacciones_unicos tbody").html('');
                

                //Mostrando mensaje descargando
                $( "#mensaje_descargando" ).html('<span class="label label-success">Consultando...</span><img style="width: 15px; " src="../assets/loading.gif">');
              
                var data = new FormData();
                data.append('start_date_day',$('#start_date_day').val());
                data.append('end_date_day',$('#end_date_day').val());
                data.append('opcn','consulta');
                var url = "controllers/rep_metricas_lideres.php";
                $.ajax({
                    url:url,
                    type:'post',
                    contentType:false,
                    data:data,
                    processData:false,
                    dataType: "json",
                    async: true,
                    cache:false,
                    success: function(data) {

                        //Ocultando mensanje consultando
                        $( "#mensaje_descargando" ).html('');

                        //Mostrando el div well que contiene las tablas
                        $('#well_tablas').css("visibility", 'visible');

                        //Habilitando nuevamente el boton de para consultar
                        $('#btn_consultar').attr("disabled", false);
                        
                        // contando las posiciones del array mejor asesor pais
                        var contador_ac=0;
                        for(var i in  data['interacciones_por_seccion']) {
                            contador_ac = contador_ac + 1;
                            break;    
                        }
                            // si existe mas de un resultado, modifica los datos
                            if (contador_ac > 0) {

                                    //TABLAL todos los usuario
                                    for(var i in  data['interacciones_por_seccion']) {

                                                            $('#Interacciones_todos  tbody:last-child').append('<tr> <td> '+data['interacciones_por_seccion'][i]['modulo']+'</td> <td> '+data['interacciones_por_seccion'][i]['cantidad']+'</td> </tr>');
                                                            
                                                    }
                                    //TABLAL MEJOR ASESOR POR CONCESIONARIO
                                    for(var i in  data['usuarios_unicos_por_seccion']) {
   
                                                    $('#Interacciones_unicos  tbody:last-child').append('<tr> <td> '+data['usuarios_unicos_por_seccion'][i]['modulo']+'</td> <td> '+data['usuarios_unicos_por_seccion'][i]['usuarios']+'</td>  </tr>');       
                                            }

                                    //FIN //INICIO MOSTRANDO ELEMENTOS ASESORES COMERCIALES

                            }else{


                                //Mensaje de color verde dentro del well
                                $('#well_tablas').css("visibility", 'hidden');
                                $( "#mensaje_si_vacio" ).html('<span class="label label-success">Consulta sin resultado</span>');
                                //console.log('sin resultados');
                            }

                    } // fin function data


                })
                .fail(function() { 
                  
                  $('#btn_consultar').attr("disabled", false);
                  $( "#mensaje_descargando" ).html('');
                  $( "#Atencion" ).html('<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>');
                  setTimeout(function() {

                                $(".gritter-item-wrapper").fadeOut(5000); 

                          },3000); 
                  
                })

                    setTimeout(function() {

                                $(".gritter-item-wrapper").fadeOut(5000); 

                          },3000); 
    }// Fin if(validacion)
});




function validacion() {

        inicio= new Date($('#start_date_day').val());
        finalq= new Date($('#end_date_day').val());
        if(inicio>finalq){

                    bootbox.alert("La fecha inicial no puede ser menor que la fecha final", function(result) 
                {
                    
                });
                    return false;
        }

      	s_date = document.getElementById("start_date_day").value;
      	e_date = document.getElementById("end_date_day").value;

        $('#start_date_day').css( "border-color", "#ffffff" );
        $('#end_date_day').css( "border-color", "#ffffff" );
       
    	if( s_date == '' & e_date == '') {
    	// alert('Complete la informacion antes de consultarla');
        $('#start_date_day').css( "border-color", "#b94a48" );
        $('#end_date_day').css( "border-color", "#b94a48");
        $( "#start_date_day" ).focus();
        return false;
        } 
         else if($('#start_date_day').val() == ""){
            $('#start_date_day').css( "border-color", "#b94a48" );
            $( "#start_date_day" ).focus();
        return false;
        } else if($('#end_date_day').val() == ""){
            $('#end_date_day').css( "border-color", "#b94a48" );
            $( "#end_date_day" ).focus();
        return false;
        } 
        return true;
    }


