$(document).ready(function(){
    // Select Placeholders
    $("#dealer_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2012-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2012-01-01"
    });
});

$(document).ready(function() {
    // By suppling no content attribute, the library uses each elements title attribute by default
    $('a[href][title]').qtip({
        content: {
            text: false // Use each elements title attribute
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        } // Give it some style
    });

    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

});

//SE CAPTURA EL ESTADO DE APROBACION
$('#status_id').change( function(){

    var status_id = $(this).val();

    if (status_id == 2) {
        $('#observaciones').show();
    } else{
        $('#observaciones').hide();
    }

} );


//SE CAPTURAN LOS DATOS DE LA MODAL PARA CREAR LA LOGISTICA
$( "#modal_form_Logistica" ).submit(function( event ) {
    if( valida() ){
        event.preventDefault();

        var hora_inicio = hora_fin = horas = concesionario_id = elementos = caso_especial = 0;
        var hora_minima = 4;
        var inputFile = file = file_edit = "";

        var datos = new FormData();

        datos.append("opcn","crear_feeding");
        datos.append("schedule_id", $('#schedule_id').val() );
        datos.append("cost_dom", $('#cost_dom').val() );
        datos.append("status_id", $('#status_id').val() );
        datos.append("notes", $('textarea#description').val() );

        horas = $('#horas_curso').val();
        hora_inicio = $('#horas_inicio').val();
        hora_fin = $('#horas_fin').val();
        concesionario_id = $('#dealer_id').val();
        elementos = horas/hora_minima;

        caso_especial =  casos_especiales(concesionario_id, hora_inicio, hora_fin);

        for (var i = 1; i <= elementos; i++) {

            inputFile = document.getElementById("factura_ref"+i);
            file = inputFile.files[0];
            if (file == undefined) {
                file = "";
                file_edit = $('#factura_edit_ref'+i).val();
            }

            datos.append("cost_"+i, $('#cost_'+i).val() );
            datos.append( "file_"+i, file );
            datos.append( "file_edit_"+i, file_edit );

            if (caso_especial) { i++; } //Si existo algun caso de alimentacion especial
        }

        datos.append( "datos_dinamicos", (i-1) );
        
        $.ajax({ 
            url: 'controllers/rep_logistica.php',
            type: 'post',
            data: datos,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var res0 = data.resultado;
                console.log(res0);
                if(res0 == "TRUE"){
                    $('#ModalLogistica').modal('hide');
                    alert("La operación ha sido completada satisfactoriamente");
                    setTimeout(function(){ location.reload(true) }, 500);
                }else{
                    $('#ModalLogistica').modal('hide');
                    alert("No se ha logrado cargar la información, por favor confirme su conexión y realicela nuevamente");
                    setTimeout(function(){ location.reload(true) }, 500);
                }
            }
        });

    } else{
    return false;
    }
});


//DISPARA LA MODAL Y CAPTURA EL ID DE LA SESION
function costo_logistica( schedule_id, course_id, inscritos, dealer_id ){

    limpiar();
    $('#aprobacion').hide(); 
    $('#observaciones').hide();
    $('#schedule_id').val( schedule_id ); //Input oculto para capturar el schedule_id 
    $('#inscritos').val( inscritos );

    data = { 'opcn':'horas', 'schedule_id': schedule_id, 'course_id': course_id }
    //Se consulta la duracion horaria del curso y habilitan campos
    $.ajax({ 
        url: 'controllers/rep_logistica.php',
        data: data,
        type: 'post',
        dataType: "json",
        success: function(data) {
            var dia = horas = elementos = 0;
            var hora_minima = 4;
            var tipo_alimentacion = newtr = caso_especial = hora_inicio = hora_fin = "";

            horas = data.duration_time;
            hora_inicio = data.start_date.split(" ");
            hora_fin = data.end_date.split(" ");
            
            $('#horas_curso').val(horas);
            $('#horas_inicio').val(hora_inicio[1]);
            $('#horas_fin').val(hora_fin[1]);
            $('#dealer_id').val(data.dealer_id);
            elementos = horas/hora_minima;

            caso_especial =  casos_especiales(data.dealer_id, hora_inicio[1], hora_fin[1]);

            for (var i = 1; i <= elementos; i++) {

                dia = Math.ceil( i/2 );

                if (i % 2 == 0){
                    tipo_alimentacion = "Almuerzo Dia "+dia;
                }else { 
                    tipo_alimentacion = "Refrigerio Dia "+dia;
                }

                newtr += '<tr><td></td>';
                newtr += '<td style="vertical-align: unset;"> '+tipo_alimentacion+' </td>';
                newtr += '<td style="vertical-align: unset;"><input  class="form-control" id="cost_'+i+'" name="cost_'+i+'" value="" /></td>';
                newtr += '<td style="vertical-align: unset;">';
                newtr += '<div class="fileupload fileupload-new margin-none" data-provides="fileupload">';
                newtr += '<input type="file" class="text-primary" id="factura_ref'+i+'" name="factura_ref'+i+'" />';
                newtr += '<span><a class="link1" id="factura_link_ref'+i+'" href="" ></a></span> ';
                newtr += '<input type="hidden" id="factura_edit_ref'+i+'" value="" name="factura_edit_ref'+i+'"> </div>';
                newtr += '<label id="mensaje_ref'+i+'" class="control-label" style="color: red;font-size: 10px;"></label>';
                newtr += '</td>';
                newtr = newtr + '<td></td></tr>';

                if (caso_especial) { i++; } //Si existo algun caso de alimentacion especial

            }

            $('#ContenidoTabla').html(newtr);
        }

    });

    //Cargar datos de BD
    setTimeout(function(){ cargar_datos(schedule_id) }, 550);
    $('#ModalLogistica').modal(); //Despliega el modal
}

//Cargar datos de BD
function cargar_datos(schedule_id){
    datos = { 'opcn':'cargar_feeding', 'schedule_id': schedule_id }
    $.ajax({ 
            url: 'controllers/rep_logistica.php',
            data: datos,
            type: 'post',
            dataType: "json",
            success: function(data) {
                var resultado = data.schedule_id;
                var horas = elementos = caso_especial = 0;
                var x = 1;
                var hora_minima = 4;

                if (resultado != "0") {

                    horas = $('#horas_curso').val();
                    elementos = horas/hora_minima;

                    caso_especial =  casos_especiales(data.dealer_id, hora_inicio[1], hora_fin[1]);

                    for (var i = 1; i <= elementos; i++) {
                        j = i-x;
                        $('#cost_'+i).val(data.Detalle_fedding[j].costo);
                        $('#factura_edit_ref'+i).val(data.Detalle_fedding[j].file);
                        $('a#factura_link_ref'+i).text(data.Detalle_fedding[j].file);
                        $('a#factura_link_ref'+i).attr('href','rep_logistica.php?file='+data.Detalle_fedding[j].file);

                        if (caso_especial) { i++; x++; } //Si existo algun caso de alimentacion especial
                    }


                    var linea_html = "";
                    $('#cost_dom').val(data.domicile);
                    $('#aprobacion').show();
                    linea_html += "<option value='0' > Pendiente </option>";//Valor default para comprobar la seleccion del select
                    linea_html += "<option value='1' > Aprobado </option>";
                    linea_html += "<option value='2' > Rechazado </option>";
                    $("#status_id").html(linea_html);
                    $("#status_id").select2().val(data.status_id).trigger("change");//Cambia el valor del select2
                    
                    if( data.status_id == 1 ){
                        $('#BtnCreacion').attr('disabled', true);
                        $('#status_id').prop('disabled', true);
                    }
                    if( data.status_id == 2 ) {
                        $('#observaciones').show();
                        $('textarea#description').val(data.notes);
                    }
                }
            }
        });
}

function casos_especiales( dealer_id, hora_inicio, hora_fin){
    //console.log("Dealer:"+dealer_id+" Inicio:"+hora_inicio+" Fin:"+hora_fin)
    if ( dealer_id == 17 ) { //Caso especial de Codiesel
        if ( (hora_inicio == "13:00:00" && hora_fin == "19:00:00") || (hora_inicio == "13:00:00" && hora_fin == "17:00:00") ) {
            return true;
        } else {
            return false;
        }
    }
}

function limpiar(){

    $('#ContenidoTabla').html("");

    $('#mensaje_suma').text("");
    $('#cost_dom').val("");
    $('#status_id').select2().val(null).trigger('change');
    $('textarea#description').val(""); 

    $('#BtnCreacion').attr('disabled', false);
    $('#status_id').prop('disabled', false);

    $('#schedule_id').val(""); //Input oculto para capturar el schedule_id 
    $('#inscritos').val("");
    $('#horas_inicio').val("");
    $('#horas_fin').val("");
    $('#dealer_id').val("");
}

function valida(){

    var valid = recorrido = suma_costos = domicilio = caso_especial = 0;
    var inscritos = costo_max = inputFile = file = "";
    var hora_minima = 4;
    var max_domicilio = 10000; //Valor maximo por dia de domicilio

    var horas = $('#horas_curso').val();
    var hora_inicio = $('#horas_inicio').val();
    var hora_fin = $('#horas_fin').val();
    var concesionario_id = $('#dealer_id').val();
    inscritos = parseInt($('#inscritos').val());
    inscritos += 1; //Alimento de Instructor

    recorrido = horas/hora_minima;

    //Limpian campos
    for (var i = 1; i <= recorrido; i++) {
        $('#cost_'+i).css( "border-color", "#efefef" );
        $('#factura_ref'+i).css( "border-color", "#efefef" );
        $('#mensaje_ref'+i).text("");
    }
    $('#cost_dom').css( "border-color", "#efefef" );
    $('#mensaje_suma').text("");



    //Valida domicilio
    if( $('#cost_dom').val() != "" ){
        if( !(/^[0-9]{0,8}$/.test( $('#cost_dom').val() )) ) { //Validacion campo de numeros
            $('#cost_dom').css( "border-color", "#b94a48" );
            valid = 1;
        } else {

            max_domicilio = recorrido*max_domicilio;

            if ( $('#cost_dom').val() > max_domicilio ) {
                valid = 1;
                $('#cost_dom').css( "border-color", "#b94a48" );
                $('#mensaje_suma').text("Por favor verifíque la información pendiente: El costo de domicilio excede el valor máximo permitido por día($20000).");
            } else {
                domicilio = $('#cost_dom').val(); 
            }
        }
    }

    //Validar Estado de Aprobacio 
    if( $('#status_id').val() != null ){
        if( $('#status_id').val() == '3' ){
            valid = 1;
            $('#mensaje_suma').text("Por favor complete la información pendiente: Estado de Aprobación.");         
        }
    }

    //Valida los campos dinamicos
    if ( valid == 0 ) {

        caso_especial =  casos_especiales(concesionario_id, hora_inicio, hora_fin);

        for (var i = 1; i <= recorrido; i++) {

            //Validacion cajas de texto
            if( $('#cost_'+i).val() == "" ){
                $('#cost_'+i).css( "border-color", "#b94a48" );
                valid = 1;
            } else if( !(/^[0-9]{0,8}$/.test( $('#cost_'+i).val() )) ) { //Validacion campo de numeros
                $('#cost_'+i).css( "border-color", "#b94a48" );
                valid = 1;
            } else {

                if (i % 2 == 0){ 
                    costo_max = ( (inscritos) * $('#costo_unit_alm').val() ) + parseInt(domicilio);
                }else { 
                    costo_max = ( (inscritos) * $('#costo_unit_ref').val() ) + parseInt(domicilio);
                }

                if ( $('#cost_'+i).val() > costo_max ) {
                    $('#cost_'+i).css( "border-color", "#b94a48" );
                    $('#mensaje_suma').text("Por favor verifíque la información pendiente: El costo de logística excede el valor máximo permitido.");
                    valid = 1;
                }
            }

            //Validacion de Carga de Archivos
            inputFile = ""; file = "";
            inputFile = document.getElementById("factura_ref"+i);
            file = inputFile.files[0];

            if( file == undefined && $('#factura_edit_ref'+i).val()=="" ){ //Valida nuevo registro
                valid = 1;
                $('#factura_ref'+i).css( "border-color", "#b94a48" );
                $('#mensaje_ref'+i).text("Por favor complete la información que esta pendiente");
            } else {
                if ( file != undefined ) { //Valida el nuevo/actualizacion de registro
                    if(file.type == "application/pdf" || file.type == "application/zip"){
                        if(file.size>2500000){
                            $('#factura_ref'+i).css( "border-color", "#b94a48" );
                            valid = 1;
                            $('#mensaje_ref'+i).text("Por favor verifíque la información que esta pendiente: Hay problemas con el tamaño del archivo");
                        }
                    }else{
                        $('#factura_ref'+i).css( "border-color", "#b94a48" );
                        valid = 1;
                        $('#mensaje_ref'+i).text("Por favor verifíque la información que esta pendiente: Hay problemas con el formato del archivo");
                    }
                }
            }

            if (caso_especial) { i++; } //Si existo algun caso de alimentacion especial

        }

    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    } else{
        return false;
    }

}