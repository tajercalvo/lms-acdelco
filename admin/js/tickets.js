$(document).ready( function(){
    $('#tema').select2({
        placeholder: "Seleccione un tema para reportar",
        allowClear: true
    });
    $('#prioridad').select2({
        placeholder: "Seleccione la prioridad",
        allowClear: true
    });
} );

function abrirModal( id ){
    $('#numeroTicketReabrir').text( id );
    $("#modalReabrirTicket").modal();
}

$("#btnReabrirTicket").click( function(){
    console.log("ingreso al ticket");
    var id = $("#numeroTicketReabrir").text();
    var datos = new FormData();
    var url = "controllers/tickets.php";
    datos.append('opcn', 'reabrir');
    datos.append( 'ticket_id', id);
    datos.append('faq_text_r' , $("#faq_text_r").val() );
    if($("#faq_img_r")[0].files[0]){
        var archivo = $("#faq_img_r").val();
        datos.append( "faq_img_r", $("#faq_img_r")[0].files[0], archivo.replace("C:\\fakepath\\","") );
    }
    $('#btnReabrirTicket').attr('disabled');
    $('#btnReabrirTicket').addClass('disabled');
    $.ajax( {
        url         : url,
        type        :'post',
        contentType : false,
        data        : datos,
        processData : false,
        dataType    : "json",
        cache       : false,
        async       : false
    } )
    .done( function(data) {
        if( data.error == "no" ){
            $("#estado_"+id).html("<label class='label label-info'>Reabierto</label>");
            $("#reabrir_"+id).text('');
            $("#modalReabrirTicket").modal('hide');
            $("#limpiarReabrir").trigger('click');
        }else if( data.error == "si" ){
            notyfy( {
                text: 'No se pudo cambiar el estado del ticket',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            } );
        }
    } )
    .fail( function(){
        notyfy({
            text: 'Error intentando conectar con el servidor, verifique su sesión de usuario o conexión de internet',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    } )
    .always( function(){
        $('#btnReabrirTicket').removeAttr('disabled');
        $('#btnReabrirTicket').removeClass('disabled');
    } );;
} );

$( "#formu_FAQ" ).submit( function( event ){
    event.preventDefault();
    var archivo = "";
    if( validar() ){
        $('#btn-enviar').attr('disabled');
        $('#btn-enviar').addClass('disabled');
        var datos = new FormData();
        datos.append( "tema", $("select[id=tema]").val() );
        datos.append( "prioridad", $("select[id=prioridad]").val() );
        datos.append( "faq_text", $("#faq_text").val() );
        if( $("#faq_img").val() !== "" && $("#faq_img").val() !== undefined ){
            archivo = $("#faq_img").val();
            datos.append( "faq_img",$("#faq_img")[0].files[0], archivo.replace("C:\\fakepath\\","") );
        }
        datos.append( "opcn", "crear" );
        //console.log(fileInputElement.files[0]);
        $.ajax({
            url             : "controllers/tickets.php",
            processData     : false,
            type            : "post",
            contentType     : false,
            data            : datos,
            cache           : false,
            async           : false
        })
        .done( function( data ){
            datos = JSON.parse(data);
            if( datos.error == 'no' ){
                $( "#cerrarModalFAQ" ).trigger( "click" );
                $( "#FAQMTicket" ).trigger( "click" );
                $( '#limpiar' ).trigger( "click" );
                $( "#faq_img" ).val("");
                $( "#modal-content-FAQ" ).html( datos.ticket );
            }else{
                notyfy({
                    text: datos.description,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        } )
        .fail( function( data ){
            notyfy({
                text: 'Error intentando conectar con el servidor, verifique su sesión de usuario o conexión de internet',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        } )
        .always( function(){
            $('#btn-enviar').removeAttr('disabled');
            $('#btn-enviar').removeClass('disabled');
        } );
    }else{
        notyfy({
            text: 'No ha ingresado un texto adecuado para reportar un problema',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
    //console.log("error ",event);
    return false;
});

/*
Andres Vega
12 Nov 2016
Funcion para validar que los campos sean correctamente seleccionados
*/
function validar(){
    var valid = 0;
    var estado = false;
    $('#faq_text').css( "border-color", "#efefef" );
    if($('#faq_text').val() == ""){
        $('#faq_text').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if( valid == 1 ){
        console.log( "esta vacio el texto" );
        notyfy({
            text: 'No ha ingresado un texto adecuado para reportar un problema',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }else{
        estado = true;
    }
    return estado;
}//fin funcion validar

$("#actualizarTablaTicket").click( function(){
    var datos = new FormData();
    datos.append( 'opcn','tablaTickets');
    $.ajax({
        url             : "controllers/tickets.php",
        processData     : false,
        type            : "post",
        contentType     : false,
        data            : datos,
        cache           : false,
        dataType        : 'html'
    })
    .done( function( data ){
        $("#campoTablaTickets").html(data);
    } )
    .fail( function( data ){
        notyfy({
            text: 'No se ha sido posible cargar los tickets creados, por favor intente nuevamente ',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    } )
} );
