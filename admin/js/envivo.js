$( "#form_CrearGaleria" ).submit(function( event ) {
    if(valida()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

$(document).ready(function(){
    if ($('#cartoonVideo').length){
        /* Get iframe src attribute value i.e. YouTube video url
        and store it in a variable */
        var url = $("#cartoonVideo").attr('src');

        /* Assign empty url value to the iframe src attribute when
        modal hide, which stop the video playing */
        $("#myModal").on('hide.bs.modal', function(){
            $("#cartoonVideo").attr('src', '');
        });

        /* Assign the initially stored url back to the iframe src
        attribute when modal is displayed again */
        $("#myModal").on('show.bs.modal', function(){
            $("#cartoonVideo").attr('src', url);
        });
    }

    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

    $("#estado2").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

     $('a[href][title]').qtip({
        content: {
            text: false
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        },
        position: {
            target: 'mouse'
        }
    });

});

// $(document).ready(function(){
//     // Select Placeholders
//     $("#estado").select2({
//         placeholder: "Seleccione un estado",
//         allowClear: true
//     });
// });

// $(document).ready(function() {

// });

function valida(){
    var valid = 0;
    $('#media_detail').css( "border-color", "#efefef" );
    $('#description').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#media_detail').val() == ""){
        $('#media_detail').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description').val() == ""){
        $('#description').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new').val()==""){
        valid=1;
        notyfy({
                text: 'Debe seleccionar una imagen',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
    }else{
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

// Mi editar
function CargaDatosEditar_Vid(media_id, image, media, description, video, estado){
    console.log('dio click');
    $('#media_detail_id').val(media_id);
    $('#imgant').val(image);
    $('#media_detail_ed').val(media);
    $('#description_ed').val(description);
    $('#video_ed').val(video);
    if(estado=="1"){
        $('#EstadoActual').html('Activo');
    }else{
        $('#EstadoActual').html('Inactivo');
    }
    //$('#estado').val(video);
    

}

$( "#form_EditarGaleria" ).submit(function( event ) {
    if(validaEdt()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

function validaEdt(){
    var valid = 0;
    $('#media_ed').css( "border-color", "#efefef" );
    $('#description_ed').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#media_ed').val() == ""){
        $('#media_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description_ed').val() == ""){
        $('#description_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new_ed').val()!=""){
        var inputFileImage = document.getElementById("image_new_ed");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function LikeGalery(media_detail_id){

    var data = new FormData();
    data.append('media_detail_id',media_detail_id);
    data.append('opcn','LikeGalery');
    var url = "controllers/envivo.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){

            	 var currentFile = "";
						            // Check for audio element support.
						            if (window.HTMLAudioElement) {

						                try {
						                    var oAudio = document.getElementById('myaudio');
						                    var btn = document.getElementById('play'); 
						                    var audioURL = document.getElementById('audiofile'); 

						                    
						                    if (audioURL.value !== currentFile) {
						                        oAudio.src = audioURL.value;
						                        currentFile = audioURL.value;                       
						                    }

						                    
						                    if (oAudio.paused) {
						                        oAudio.play();
						                        btn.textContent = "Pause";
						                        oAudio.currentTime = 0;
						                    }
						                    else {
						                        oAudio.pause();
						                        btn.textContent = "Play";
						                        oAudio.currentTime = 0;
						                    }
						                }
						                catch (e) {
						                    
						                     if(window.console && console.error("Error:" + e));
						                }
						            }

                notyfy({
                    text: 'Muchas gracias, hemos recibido tu like! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes'+media_detail_id).text());
                $('#CantLikes'+media_detail_id).text(Cantlikes+1);
            }else{
                notyfy({
                    text: 'Ya votaste por esta multimedia anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function streaming_views(media_detail_id){
    // console.log(media_detail_id);
    var data = new FormData();
    data.append('media_detail_id',media_detail_id);
    data.append('opcn','streaming_views');
    var url = "controllers/envivo.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                var Cantlikes = parseInt($('#CantViews'+media_detail_id).text());
                $('#CantViews'+media_detail_id).text(Cantlikes+1);
                 console.log(media_detail_id);
            }
        }
    });
}
