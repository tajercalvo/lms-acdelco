$(document).ready(function () {
  scrool_alfinal();
  msj_id = $('#msjId').html();
  var alto = $('.content').height();
  $('#content-dia-user').css('height', alto + 'px');
  if ($('#finAlumnoTransmisor').length == 1) {
    iniciarAlumno()
  }
});


function scrool_alfinal() {
  var div = document.getElementById('ventana-chat');
  div.scrollTop = '999999';
}

$('.mini-diap').click(function (e) {
  $('.mini-diap').removeClass('active green');
  $(this).addClass('active');
  diapo_actu = $(this).data('diapositiva');
  var question_id = $(this).data('question_id');

  // inicio
  var image = $(this).attr('data-image');
  var ruta = $(this).attr('data-ruta');
  var archivo = $(this).attr('data-archivo');
  pintar_diapositiva(archivo, ruta, image, question_id)
  // fin

  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?cambiardiapositiva",
    dataType: "json",
    data: { opcn: 'cambiardiapositiva', diapositiva: diapo_actu },
  }).done(function (data) {
    $('#diapo_' + data.id).addClass('active green');
  })
    .fail(function () {
      console.log("error");
    })
});

function pintar_diapositiva(archivo, ruta, image, question_id = 0) {
  var diapositiva_actual = '';
  if (archivo.indexOf(".mp4") > -1) {
    diapositiva_actual = '<video style="width: 100%;" class="img-responsive" controls autoplay>';
    diapositiva_actual += '<source src="' + ruta + archivo + '" type="video/mp4">';
    diapositiva_actual += 'Your browser does not support the video tag.';
    diapositiva_actual += '</video>';
  } else if (archivo.indexOf(".pdf") > -1) {
    diapositiva_actual = '<embed src="' + ruta + archivo + '" style="height:600px; width:100%;">';
  } else if (archivo.indexOf(".preg") > -1) {
    $.ajax({
      type: "post",
      url: "controllers/aulavirtual.php?cargarPregunta",
      dataType: "html",
      data: { opcn: 'cargarPregunta', question_id: question_id },
    }).done(function (data) {
      diapositiva_actual = data
      if (!comPantalla) {
        $('#diapositiva_actual').css('display', 'none');
        $('#diapositiva_actual').html(diapositiva_actual);
        $('#diapositiva_actual').fadeIn();
      } else {
        $('#diapositiva_actual').html('');
      }
    })
      .fail(function (data) {
      })

  } else {
    diapositiva_actual += '<img style="width: 100%;" class="img-responsive" src="' + ruta + image + '" >';
  }

  if (!comPantalla) {
    $('#diapositiva_actual').css('display', 'none');
    $('#diapositiva_actual').html(diapositiva_actual);
    $('#diapositiva_actual').fadeIn();
  } else {
    $('#diapositiva_actual').html('');
  }
}

$('.ver').click(function (e) {
  e.stopPropagation();
});

$('#frm_chat').submit(function (e) {
  e.preventDefault();
  var msj = $('#mi-msj').val()
  if (msj.trim() == '') {
    return;
  }

  var foto = $('#mi-foto').attr('src')
  var f = new Date();
  var idmsj = "msj_" + f.getTime();
  var msj_div = `<div class="direct-chat-msg right">
                <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">Yo</span>
                <span id="`+ idmsj + `" class="direct-chat-timestamp pull-left"><i class="fa fa-clock-o"></i></span>
                </div>
                <!-- /.direct-chat-info -->
                <img class="direct-chat-img" src="`+ foto + `" alt="img">
                <!-- /.direct-chat-img -->
                <div class="direct-chat-text">
                `+ msj + `
                </div>
            </div>`;
  $('#ventana-chat').append(msj_div);
  scrool_alfinal()
  $('#mi-msj').val('')
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?nuevomsj",
    dataType: "json",
    data: { opcn: 'nuevomsj', msj: msj, idmsj: idmsj },
  }).done(function (data) {
    if (!data.error) {
      var x = document.getElementById("sonido_chat");
      x.play();
      $('#' + data.idmsj).html(data.time + ' <i class="fa fa-check text-info"></i>');
    } else {
      var x = document.getElementById("sonido_chat");
      x.play();
      $('#' + idmsj).html('<i class="fa fa-close text-danger"></i>');
    }
  })
    .fail(function (data) {
      var x = document.getElementById("sonido_chat");
      x.play();
      $('#' + idmsj).html('<i class="fa fa-close text-danger"></i>');
    })
});

scrool_chat = 0;
mensajes_nuevos = 0;
setInterval(() => {
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?msjNuevos",
    dataType: "json",
    data: { opcn: 'msjNuevos', msj_id: msj_id },
  }).done(function (data) {
    if (data.msj_id <= msj_id) { return; false; }

    msj_id = data.msj_id;
    var div_msj = '';
    var cant_mensajes_a = $('.direct-chat-msg').length
    data.msj.forEach(e => {
      div_msj += `<div class="direct-chat-msg left ">
                            <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">`+ e.usuario + `</span>
                            <span class="direct-chat-timestamp pull-right">`+ e.time + `</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="../assets/images/usuarios/`+ e.imagen + `" alt="img">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                            `+ e.comment_text + `
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>`
    });
    $('#ventana-chat').append(div_msj);
    var cant_mensajes_d = $('.direct-chat-msg').length

    if (scrool_chat == $('#ventana-chat').scrollTop()) {
      scrool_alfinal();
    } else {
      if (cant_mensajes_d > cant_mensajes_a) {
        mensajes_nuevos = cant_mensajes_d - cant_mensajes_a + mensajes_nuevos;
        $('#mensajes_nuevos').html('Tienes ' + mensajes_nuevos + ' mensajes sin leer <img src="../assets/Biblioteca/FlechaAbajo.gif" style="margin-left: 1%; width: 11px;">');
        $('#sonido_mensajes_nuevos').trigger('play');
      }
    }
  })
    .fail(function () {
      console.log("error");
    })

}, 2000);

$('#mensajes_nuevos').click(function (e) {
  e.preventDefault();
  scrool_alfinal()
});
$('#ventana-chat').on('scroll', function () {
  let element = document.getElementById("ventana-chat");

  if (element.offsetHeight + element.scrollTop >= element.scrollHeight) {
    $('#mensajes_nuevos').html('');
    scrool_chat = $('#ventana-chat').scrollTop();
    mensajes_nuevos = 0;
  }
});
// $('#ventana-chat').on('scroll', function () {
//   scrool_chat == $('#ventana-chat').scrollTop()
//   console.log($('#ventana-chat').scrollTop()+' ID')
//   console.log(scrool_chat+'variable')

// });

setInterval(() => {
  diapsiitva_Actual();
}, 2000);

diapo_actu = '';
function diapsiitva_Actual() {
  // if ($('#instructor').html() == 'si') {
  //     return;
  // }
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?diapositivaActual",
    dataType: "json",
    data: { opcn: 'diapositivaActual' },
  }).done(function (data) {
    if (data.length == 0) {
      return false;
    } else {
      if (diapo_actu == data.coursefile_id) { return; }
      diapo_actu = data.coursefile_id;
      pintar_diapositiva(data.archivo, data.ruta, data.image, data.question_id)
    }
  })
    .fail(function () {
      console.log("error");
    })
}


// Transmision
$(document).ready(function () {

  if ($('#valida_alumno').length == 1 && $('#finAlumnoTransmisor').length == 0) {
    recibir();
  }
});


// Transmisión
//apiKey = "46812004";
//Secret = df3f78189b36c4001a6a1537d9b126e344274d6a
//sessionId = "2_MX40NjgxMjAwNH5-MTU5MzA5OTc5ODk5MH5xdkNuaDNnYVhHYzBFblV2dUtkbTVRL0p-fg";
//token = "T1==cGFydG5lcl9pZD00NjgxMjAwNCZzaWc9NjM3NmI2MDBiN2RlNTNhNWI3NTVmNTY4NjQ5ODFkMjU0MTkyODQ3MjpzZXNzaW9uX2lkPTJfTVg0ME5qZ3hNakF3Tkg1LU1UVTVNekE1T1RjNU9EazVNSDV4ZGtOdWFETm5ZVmhIWXpCRmJsVjJkVXRrYlRWUkwwcC1mZyZjcmVhdGVfdGltZT0xNTkzMDk5ODE4Jm5vbmNlPTAuNjExODcxOTMwNDM5NDU4NCZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTk1NjkxODExJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9";

var apiKey = '46813974';
sessionId = $('#sessionid').val();
token = $('#token').val();

//compartir pantalla
extensionId = 'ijgfggkmnahehfeimghpomadjmmkiphd';
var ffWhitelistVersion; // = '36';

// Handling all of our errors here by alerting them
function handleError(error) {
  if (error) {
    alert(error.message);
  }
}

estado = 1;
function iniciar() {
  if (estado == 1) {
    ini()
    estado = 2;
  } else {
    location.reload();
  }
}

$('#finAlumnoTransmisor').click(function (e) {
  e.preventDefault();
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?finAlumnoTransmisor",
    dataType: "json",
    data: { opcn: 'finAlumnoTransmisor' },
  }).done(function (data) {
    if (!data.error) {
      location.reload();
    }
  }).fail(function () {
    console.log("error");
  })

});


comPantalla = false;
function iniciarAlumno() {
  console.log('inicia alumno a transmitir')
  $('#iniciar').css('display', 'none');
  $('#img-profesor').css('display', 'none');
  $('#finAlumnoTransmisor').css('visibility', 'visible');
  $('#levantarMano').css('visibility', 'hidden');
  $('#profesor').css('display', 'block');
  session = OT.initSession(apiKey, sessionId);
  // Subscribe to a newly created stream
  //anterior
  session.on('streamCreated', function (event) {
    if (event.stream.videoType === 'screen') {
      comPantalla = true;
      document.getElementById("diapositiva_actual").style.display = "none";
      var subOptions = {
        insertMode: 'append',
        width: '930',
        height: '523'
      };
      subscriber = session.subscribe(event.stream, 'screen-preview', subOptions);
    } else {
      session.subscribe(event.stream, 'profesor', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      }, handleError);
    }
  });

  // Create a publisher
  var publisher = OT.initPublisher('profesor', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);

  session.on("streamDestroyed", function (event) {
    $('#diapositiva_actual').css('display', 'block')
  });

  // Connect to the session
  session.connect(token, function (error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (error) {
      handleError(error);
    } else {
      session.publish(publisher, handleError);
    }
  });
}

function recibir() {
  session = OT.initSession(apiKey, sessionId);

  session.on('streamCreated', function (event) {
    if (event.stream.videoType === 'screen') {
      comPantalla = true;
      document.getElementById("diapositiva_actual").style.display = "none";

      var subOptions = {
        insertMode: 'append',
        width: '930',
        height: '523'
      };
      subscriber = session.subscribe(event.stream, 'screen-preview', subOptions);
    } else {
      session.subscribe(event.stream, 'profesor', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      }, handleError);
    }
  });

  session.on("streamDestroyed", function (event) {
    $('#diapositiva_actual').css('display', 'block')
  });

  session.connect(token, function (error) {
    if (error) {
      handleError(error);
    }
  });
}

function ini() {

  if ($('#profesor').length == 0 || $('#alumnoTransmisor').length == 0) {
    $('#img-profesor').css('display', 'none');
    $('#profesor').css('display', 'block');
    $('#iniciar').html('<i class="text-danger fa fa-stop" aria-hidden="true">');
    session = OT.initSession(apiKey, sessionId);

    // Subscribe to a newly created stream
    session.on('streamCreated', function (event) {
      session.subscribe(event.stream, 'profesor', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      }, handleError);
    });

    // Create a publisher
    var publisher = OT.initPublisher('profesor', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    }, handleError);

    // Connect to the session
    session.connect(token, function (error) {
      // If the connection is successful, initialize a publisher and publish to the session
      if (error) {
        handleError(error);
      } else {
        session.publish(publisher, handleError);
      }
    });
  }

}
//FIn transmision

//Compartir pantalla
function compartirPantalla() {
  session = OT.initSession(apiKey, sessionId);
  // OT.checkScreenSharingCapability(function(response) {
  //   if(!response.supported || response.extensionRegistered === false) {
  //     // This browser does not support screen sharing
  //   }
  // });
  OT.checkScreenSharingCapability(function (response) {
    if (!response.supported || response.extensionRegistered === false) {
      // This browser does not support screen sharing.
    } else if (response.extensionInstalled === false) {
      // Prompt to install the extension.
    } else {
      var screenPublisherElement = document.createElement('div');
      // Screen sharing is available. Publish the screen.
      var publisher = OT.initPublisher(screenPublisherElement,
        //var publisher = OT.initPublisher('screen-preview',
        { videoSource: 'screen' },
        function (error) {
          if (error) {
            // Look at error.message to see what went wrong.
          } else {
            session.publish(publisher, function (error) {
              if (error) {
                // Look error.message to see what went wrong.
              }
            });
          }
        }
      );
    }
  });
}
//Fin compartir pantalla

//levantar la mano

//Para el instructor
setInterval(() => {
  if ($('#profesor').length == 1) {
    quienLevanto();
  }
}, 3000);

function quienLevanto() {
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?quienLevanto",
    dataType: "json",
    data: { opcn: 'quienLevanto' },
  }).done(function (data) {
    var html = '';
    var stilo = '';
    data.forEach(e => {
      if (e.status_id == 1) { stilo = 'style="background: #4caf50;"' }

      var slash = "";
      if (e.silenciar == 1) {
        slash = "-slash";
      }
      html += `<li ` + stilo + `>
                  <a href="#" data-alumnoId="`+ e.user_id_av + `">
                    <i class="quitar_alumno fa fa-close text-red"></i>
                      <span class="autorizarAlumno">`+ e.usuario + `</span>
                      <i class="silenciar fa fa-microphone`+ slash + ` text-red"></i>
                  </a>
                </li>`
    });
    if (data.length > 0) {
      $('#cuantosLevantaron').html(data.length);
    } else {
      $('#cuantosLevantaron').html('');
    }
    $('#listaManoLevantada').html(html);
  })
    .fail(function () {
      console.log("error");
    })
}


$('body').on('click', '.silenciar', function (e) {
  e.preventDefault();
  var alumnoId = $(this).parents('a').attr('data-alumnoId');

  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?silenciar",
    dataType: "json",
    data: { opcn: 'silenciar', user_id: alumnoId },
  }).done(function (data) {
    //console.log(data)
  })
    .fail(function () {
      console.log("error");
    })
});

$('body').on('click', '.autorizarAlumno', function (e) {
  e.preventDefault();
  var alumnoId = $(this).parents('a').attr('data-alumnoId');
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?autorizarAlumno",
    dataType: "json",
    data: { opcn: 'autorizarAlumno', user_id: alumnoId },
  }).done(function (data) {
    //console.log(data)
  })
    .fail(function () {
      console.log("error");
    })
});

$('body').on('click', '.quitar_alumno', function (e) {
  e.preventDefault();
  var alumnoId = $(this).parents('a').attr('data-alumnoId');
  $.ajax({
    type: 'post',
    url: "controllers/aulavirtual.php?quitar_alumno",
    dataType: 'json',
    data: { opcn: 'quitar_alumno', user_id: alumnoId },
  }).done(function (data) {
    //console.log(data)
  })
    .fail(function () {
      console.log('error');
    })
});


$('#quitarNotificaciones').click(function (e) {
  e.preventDefault();
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?quitarNotificaciones",
    dataType: "json",
    data: { opcn: 'quitarNotificaciones' },
  }).done(function (data) {
  })
    .fail(function () {
      console.log("error");
    })
});

//para el estudiante
$('#levantarMano').click(function (e) {

  if (!confirm("Se accederá a su cámara y micrófono.")) { return false; }
  e.preventDefault();

  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?levantarMano",
    dataType: "json",
    data: { opcn: 'levantarMano' },
  }).done(function (data) {
    $('#levantarMano').html('<i class="text-green fa fa-hand-paper-o"></i>');
  })
    .fail(function () {
      console.log("error");
    })
});


setInterval(() => {
  validarAutorizacion()
}, 2000);
//Valida si fue autorizado para transmitir
function validarAutorizacion() {

  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?validarAutorizacion",
    dataType: "json",
    data: { opcn: 'validarAutorizacion' },
  }).done(function (data) {

    if (data.silenciar == 1 && $(".OT_publisher .OT_active").length == 0) {
      $(".OT_publisher .OT_mute").trigger("click");
    }

    if (data.silenciar == 0 && $(".OT_publisher .OT_active").length == 1) {
      $(".OT_publisher .OT_mute").trigger("click");
    }

    if (!data.error) {
      if ($('#finAlumnoTransmisor').length == 1) { return; }
      location.reload();
    } else {
      if (data.status_id == 2) {
        $('#levantarMano').html('<i class="text-green fa fa-hand-paper-o"></i>');
      } else {
        $('#levantarMano').html('<i class="text-danger fa fa-hand-paper-o"></i>');
        if ($('#finAlumnoTransmisor').length == 1) {
          location.reload();
        }
      }

    }
  }).fail(function () {
    console.log("error");
  })
}
// fin levantar la mano

$('body').on('submit', '#frm-responder', function (e) {
  e.preventDefault();
  var datos = $(this).serializeArray();

  datos.push({ name: "opcn", value: "responderPreguntas" })
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?responderPreguntas",
    dataType: "json",
    data: datos,
  }).done(function (data) {
    alert(data.msj)
    $('#diapositiva_actual').html('');
  }).fail(function () {
    console.log("error");
  })

});

//Usuarios conectados

setInterval(() => {
  $.ajax({
    type: "post",
    url: "controllers/aulavirtual.php?usuariosConectados",
    dataType: "json",
    data: { opcn: 'conectados' },
  }).done(function (data) {
    var div_conect = '';
    $('#cant_conectados').html(data.length);
    data.forEach(e => {
      div_conect += `<li>
                          <a href="javascript:void(0)">
                          <img class="direct-chat-img" src="../assets/images/usuarios/`+ e.imagen + `" alt="img">
                          <div class="menu-info">
                              <h4 class="control-sidebar-subheading">`+ e.usuario + `</h4>
                              <p> <i class="fa fa-circle text-green"></i> En linea</p>
                          </div>
                          </a>
                      </li>`
      $('#usuarios_conectados').html(div_conect);
    });
  })
    .fail(function () {
      console.log("error");
    })

}, 2000);

$("#finalizar").click(function () {
  if (!confirm('¿Estas seguro de finalizar el curso?')) { return false; }
  $(this).prop("disabled", true);
  var datos = {};
  datos.schedule_id = $.getURL("schedule") || 0;
  datos.module_id = $.getURL("id") || 0;
  datos.opcn = "finalizar_curso";
  $.ajax({
    url: "controllers/aulavirtual.php?opcn=finalizarcurso",
    data: datos,
    type: "post",
    dataType: "json"
  })
    .done(function (data) {
      if (data.error) {
        alert(data.msj)
      } else {
        alert(data.msj)
        window.location = "index.php";
      }
    })
    .always(function () {
      $('#finalizar').prop("disabled", false);
    });
});

//Funcion para capturar las variables de la URL, se utiliza asÃ­ var id_url = $.get("id");
(function ($) {
  $.getURL = function (key) {
    key = key.replace(/[\[]/, '\\[');
    key = key.replace(/[\]]/, '\\]');
    var pattern = "[\\?&]" + key + "=([^&#]*)";
    var regex = new RegExp(pattern);
    var url = unescape(window.location.href);
    var results = regex.exec(url);
    if (results === null) {
      return null;
    } else {
      return results[1];
    }
  }
})(jQuery);