$(document).ready(function(){
    $('#cursosF').select2({
        placeholder: "Seleccione un curso",
        allowClear: true
    });
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
});
