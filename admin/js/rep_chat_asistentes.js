
$(document).ready(function(){
        consultar_broad();
         

});

        $("#conference_id").change(function(){
            $("#chat").css("display","none");
        });

        $("#descargar").click(function(event) {
            $("#datos_tabla").val( $("<div>").append( $("#asistentes2").eq(0).clone()).html());//Clona la tabla para envialo a Fichero.excel para descargar
            $("#FormularioExportacion").submit(); 
        });
   
  
//consulta el nombre y id del broadcast
function consultar_broad(){

var data = {opcn:'consultar_Broad'};

         $.ajax({
             url: 'controllers/rep_asistentes_broadcast.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             //console.log(data);
            var datos = "";
            for (var i = 0; i < data.length; i++) {
              datos += '<option value="'+data[i].conference_id+'">'+data[i].conference+' - '+data[i].conference_id+' </option>';
             
             }
                $('#conference_id').html(datos)
               
                $("#conference_id").select2({
                    placeholder: "Seleccione una sesión",
                    allowClear: true
                 });
         })
         .fail(function() {
             console.log("error");
         })
}
conference_id = '';
//-----------------------Imprime los datos en la tabla----------------------//
$('#frm').submit(function(e) {
    e.preventDefault();
     conference_id = $('#conference_id').val()

      //mensaje_chat(conference_id)
    $('#consultando').html('<img style="width: 15px; " src="../assets/loading.gif">');

    var data = {opcn:'consultar_asistantes', conference_id: conference_id};
    $.ajax({
             url: 'controllers/rep_asistentes_broadcast.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             //console.log(data);
             var xnum = ''
             xnum = data.length
             $('#consultando').html('');
             $("#chat").css("display","block");
              table = '';
             if (data.mensaje) {
                alert(data.mensaje);
             }
            for (var i = 0; i < data.length; i++) {
  
                table +='<tr>';
                table +='<td>'+data[i].identification+'</td>';
                table +='<td>'+data[i].apellidos+' '+data[i].nombres+'</td>';
                table +='<td>'+data[i].concesionario+'</td>';
                table +='<td>'+data[i].sede+'</td>';
                table +='<td>'+data[i].trayectorias+'</td>';
                table +='<td>'+data[i].perfil+'</td>';
                table +='</tr>';
             
             }
              
             $('#asistentes tbody').html(table);
               // table.destroy();
             $('#asistentes').DataTable({
                     
                    "language": {
                      "sProcessing": "Procesando...",
                      "sLengthMenu": "",
                      "sZeroRecords": "No se encontraron resultados",
                      "sEmptyTable": "Ning¨²n dato disponible en esta tabla",
                      "sInfo": "Mostrando registros del Inicio al Final de un total de TOTAL registros",
                      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered": "(de MAX registros)",
                      "sInfoPostFix": "",
                      "sSearch": "Buscar:",
                      "sUrl": "",
                      "sInfoThousands": ",",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                      "sFirst": "Primero",
                      "sLast": "0‰3ltimo",
                      "sNext": "Siguiente",
                      "sPrevious": "Anterior"
                      },
                      "oAria": {
                      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                      }
                      ,
                      dom: 'lBfrtip',
                      pageLength: 12,
                      buttons: [
                      {extend : 'excelHtml5',
                      text: '<i class="las la-file-excel">EXCEL</i>',
                      filename: 'Usuarios_webroom',
                      },
                      {extend : 'pdfHtml5',
                      text: '<i class="las la-file-pdf">PDF</i>',
                      //orientation: 'landscape',
                      filename: 'clientes_autotrain',
                      exportOptions: {
                       columns: [0,1,2,3,4,5],
                      }
                      },
                      {extend: 'print',text: '<i class="las la-print">IMPRIMIR</i>', filename: 'clientes_autotrain'}
                      ]
                     
             });//convierte en datatable;//convierte en datatable
             $('#num_asistentes').html(xnum+' Participantes');
               mensaje_chat()
               

         })
         .fail(function() {
             $('#consultando').html('');
             console.log("error");
         })
});

function mensaje_chat(){
  
    
  $.ajax({
        url: 'controllers/rep_asistentes_broadcast.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn: 'mensaje_chat',conference_id},
    })
  .done(function(data) {
     var xnumchat = ''
     xnumchat = data.length
      table = ''
     
          
      for (var i = 0; i < data.length; i++) {
        table+='<tr>'
        table+='<td>'+data[i].Comentario+'</td>'
        table+='<td>'+data[i].Usuario+'</td>'
        table+='<td>'+data[i].Empresa+'</td>'
        table+='<td>'+data[i].date_creation+'</td>'
        table+='<td>'+data[i].Conferencia+'</td>'
        
        table+='</tr>'
    }
    $('#tabl_chat tbody').html(table)
   // table.destroy();
      $('#tabl_chat').DataTable({
                     
                    "language": {
                      "sProcessing": "Procesando...",
                      "sLengthMenu": "",
                      "sZeroRecords": "No se encontraron resultados",
                      "sEmptyTable": "Ning¨²n dato disponible en esta tabla",
                      "sInfo": "Mostrando registros del Inicio al Final de un total de TOTAL registros",
                      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                      "sInfoFiltered": "(de MAX registros)",
                      "sInfoPostFix": "",
                      "sSearch": "Buscar:",
                      "sUrl": "",
                      "sInfoThousands": ",",
                      "sLoadingRecords": "Cargando...",
                      "oPaginate": {
                      "sFirst": "Primero",
                      "sLast": "0‰3ltimo",
                      "sNext": "Siguiente",
                      "sPrevious": "Anterior"
                      },
                      "oAria": {
                      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                      }
                      }
                      ,
                      dom: 'lBfrtip',
                      pageLength: 12,
                      buttons: [
                      {extend : 'excelHtml5',
                      text: '<i class="las la-file-excel">EXCEL</i>',
                      filename: 'Usuarios_webroom',
                      },
                      {extend : 'pdfHtml5',
                      text: '<i class="las la-file-pdf">PDF</i>',
                      //orientation: 'landscape',
                      filename: 'clientes_autotrain',
                      exportOptions: {
                       columns: [0,1,2,3,4,5],
                      }
                      },
                      {extend: 'print',text: '<i class="las la-print">IMPRIMIR</i>', filename: 'clientes_autotrain'}
                      ]
                     
             });//convierte en datatable;//convierte en datatable
           
        
      $('#num_mensajes').html('Se han registrado: ['+xnumchat+'] Mensajes');
           

  })
  .fail(function() {
    console.log("error");
  })

}
  


