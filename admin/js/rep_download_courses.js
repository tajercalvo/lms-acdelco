$(document).ready(function(){
        consultar_views();

});

        $("#library_id").change(function(){
        $('#asistentes tbody').html("");//limpia la tabla
        $('#respuesta').html("");// limpia el campo de no hay resp
        $("#descargar").css("display","none");
        });


        $("#descargar").click(function(event) {
        $("#datos_tabla").val( $("<div>").append( $("#asistentes").eq(0).clone()).html());
        $("#FormularioExportacion").submit();

    });

//consulta y llena el select con el nombre y id del broadcast
function consultar_views(){

var data = {opcn:'consultar_views'};

         $.ajax({
             url: 'controllers/rep_download_courses.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             console.log(data);
            var datos = "";
            for (var i = 0; i < data.length; i++) {
              datos += '<option value="'+data[i].course_id+'">'+data[i].course_id+' - '+data[i].course+' </option>';

             }

             $('#library_id').html(datos)

                $("#library_id").select2({
                    placeholder: "Seleccione una sesión",
                    allowClear: true
                 });
         })
         .fail(function() {
             console.log("error");
         })
}

$('#frm').submit(function(e) {
    e.preventDefault();
    var library_id = $('#library_id').val()

    $('#consultando').html('<img style="width: 15px; " src="../assets/loading.gif">');

    var data = {opcn:'consultar_libreria', library_id: library_id};
    $.ajax({
             url: 'controllers/rep_download_courses.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             console.log(data);
             $('#consultando').html('');//imagen gif se quite
             $("#descargar").css("display","block");
             var table = "";
        if (data.length !== 0) {
            for (var i = 0; i < data.length; i++) {

                table +='<tr>';
                table +='<td>'+data[i].identificacion+'</td>';
                table +='<td>'+data[i].nombre.toUpperCase()+' '+data[i].apellido.toUpperCase()+'</td>';
                table +='<td>'+data[i].concesionario+'</td>';
                table +='<td>'+data[i].sede+'</td>';
                table +='<td>'+data[i].zona+'</td>';
                table +='<td>'+data[i].name+'</td>';
                table +='<td>'+data[i].date_creation+'</td>';
                table +='<td>'+data[i].descargas+'</td>';
                table +='</tr>';

             }
        } else{
                alert('ESTA CONSULTA NO CONTIENE DATOS');
                $('#respuesta').html('ESTA CONSULTA NO CONTIENE DATOS');
                $("#descargar").css("display","none");
        }

             $('#asistentes tbody').html(table);

         })
         .fail(function() {
             $('#consultando').html('');
             console.log("error");
         })
});





