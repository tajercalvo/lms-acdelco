var stack = 0, bars = false, lines = true, steps = false;
var dataChar = [];
var dataChar_ot = [];

$(document).ready(function(){
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
});


function plotCargaPies(){
    $(".HorasXProveedor").each(function (index,element) {
        dataChar.push({ label: $(element).attr("name")+" : "+$(element).val(), data: $(element).val() });
    });

    $(".SessionesXProveedor").each(function (index,element) {
        dataChar_ot.push({ label: $(element).attr("name")+" : "+$(element).val(), data: $(element).val() });
    });

    var data = [],
            series = Math.floor(Math.random() * 6) + 3;

        for (var i = 0; i < series; i++) {
            data[i] = {
                label: "Series" + (i + 1),
                data: Math.floor(Math.random() * 100) + 1
            }
        }

        $.plot('#donut-horas', dataChar, {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.2,
                    label: {
                        show: true,
                        radius: 1,
                        formatter: function(label, series){
                            return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                        },
                        background: { opacity: 0.8 }
                    }
                }
            },
            shadowSize:1,
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y.0",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            }
        });

        $.plot('#donut-sesiones', dataChar_ot, {
            series: {
                pie: {
                    show: true,
                    innerRadius: 0.2,
                    label: {
                        show: true,
                        radius: 1,
                        formatter: function(label, series){
                            return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                        },
                        background: { opacity: 0.8 }
                    }
                }
            },
            shadowSize:1,
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y.0",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            }
        });

    /*$.plot('#donut-horas', dataChar, {
        series: {
            pie: {
                innerRadius: 0.2,
                show: true,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
        },
        legend: {
            show: false
        },
        shadowSize:1,
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });*/

    /*$.plot($("#donut-horas"), dataChar,
    {
        series: {
            pie: {
                innerRadius: 0.2,
                show: true,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
        },
        shadowSize:1,
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });*/
}

function plotPAC(){
    Data_Target = [];
    Data_PAC_Mes = [];

    Data_PAC_Posp = [];
    Data_PAC_Posr = [];

    DataNames_Mes = [];
    var _CtrlVar = 0;
    $(".PACEvolucion").each(function (index) {
        Data_Target.push([_CtrlVar, 95]);
        Data_PAC_Mes.push([_CtrlVar, $(this).attr('dat-pac')]);
        DataNames_Mes.push([_CtrlVar, $(this).attr('dat-fec')]);

        Data_PAC_Posp.push([_CtrlVar, $(this).attr('dat-posp')]);
        Data_PAC_Posr.push([_CtrlVar, $(this).attr('dat-posr')]);

        _CtrlVar = _CtrlVar + 1;
    });
    var ds = new Array();
        ds.push({
            label: "Objetivo",
            data: Data_Target,
            bars: {order: 1}
        });
        ds.push({
            label: "Concesionario",
            data: Data_PAC_Mes,
            bars: {order: 2}
        });
        ds.push({
            label: "En el País",
            data: Data_PAC_Posp,
            bars: {order: 3}
        });
        ds.push({
            label: "En la Región",
            data: Data_PAC_Posr,
            bars: {order: 4}
        });
    var data = ds;

    $.plot($("#ChartPAC"), data, {
        bars: {
            show: false,
            barWidth: 0.2,
            fill:1
        },
        grid: {
            hoverable: true,
            clickable: false,
            borderWidth: 1
        },
        legend: {
            labelBoxBorderColor: "none",
            backgroundColor: "#FFFFFF",
            backgroundOpacity: 0.3,
            position: "ne"
        },
        series: {
            shadowSize: 1,
            grow: {active:false}
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        },
        xaxis: {
            ticks: DataNames_Mes,
            tickOptions:{
                angle: -60
            }
        }
    });
    CambiarEtiquetas();
}
function CambiarEtiquetas(){
    $(".flot-tick-label").each(function (index) {
        $( this ).css( "-webkit-transform", "rotate(-45deg)" );
        $( this ).css( "transform", "rotate(-45deg)" );
        $( this ).css( "-ms-transform", "rotate(-45deg)" );
        $( this ).css( "-moz-transform", "rotate(-45deg)" );
        $( this ).css( "-o-transform", "rotate(-45deg)" );
        $( this ).css( "font-size", "8px" );
    });
}
