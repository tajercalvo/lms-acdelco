var contador = 0;
var intervalo;

$(document).ready( function(){

    $('#zonas').select2({
        placeholder: "Seleccione una zona",
        allowClear: true
    });
    $('#concesionarios').select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
    $('#sedes').select2({
        placeholder: "Seleccione una sede",
        allowClear: true
    });
    $('#cargos').select2({
        placeholder: "Seleccione un cargo",
        allowClear: true
    });
    $('#contratacion').select2({
        placeholder: "Seleccione un cargo",
        allowClear: true
    });
    $('#tipo_usuario').select2({
        placeholder: "Seleccione un cargo",
        allowClear: true
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

    NProgress.configure({ parent: '.loader_s' });
} );

//=========================================================
//Consulta las sedes de acuerdo a los concesionarios
//=========================================================
$('#concesionarios').change( function(){
    var concesionarios = $(this).val() || "";
    $("#sedes").select2("val", "");
    var url = "controllers/analytics.php"
    var data = new FormData();
    data.append( 'opcn', 'sel_sede' );
    data.append( 'concesionarios', concesionarios );
    $.ajax({
        url         : url,
        type        :'post',
        contentType : false,
        data        : data,
        processData : false,
        dataType    : 'html',
        cache       : false,
        async       : false
    } )
    .done( function( data ) {
        $('#dataSedes').html( data );
        if( $("#sedes").attr('disabled') ){
            $("#sedes").removeAttr('disabled');
        }
        // NProgress.done();
    } );
} );

//=========================================================
//Consulta los concesionarios de acuerdo a las zonas seleccionadas
//=========================================================
$('#zonas').change( function(){
    var zonas = $(this).val() || "";
    // console.log(zonas);
    $("#concesionarios").select2("val", "");
    var url = "controllers/analytics.php"
    var data = new FormData();
    data.append( 'opcn', 'sel_Concs' );
    data.append( 'zonas', zonas );
    $.ajax({
        url         : url,
        type        :'post',
        contentType : false,
        data        : data,
        processData : false,
        dataType    : 'html',
        cache       : false,
        async       : false
    } )
    .done( function( data ) {
        $('#dataConcs').html( data );
        // NProgress.done();
    } );
} );

$('#form_analytics').submit( function( event ){
    event.preventDefault();
    if( validar() ){
        NProgress.start();

        $('#consultaAnalytics').attr('disabled',true);
        var url = "controllers/analytics.php";
        var datos = new FormData();
        contar( 'contador' );

        datos.append( "zonas", $( '#zonas' ).val() || "" );
        datos.append( "concesionarios", $( '#concesionarios' ).val() || "" );
        datos.append( "sedes", $( '#sedes' ).val() || "" );
        datos.append( "cargos", $( '#cargos' ).val() || "" );
        datos.append( "es_activo", $( '#es_activo' ).prop('checked') ? "SI" : "NO" );
        datos.append( "es_inactivo", $( '#es_inactivo' ).prop('checked') ? "SI" : "NO" );
        datos.append( "femenino", $( '#femenino' ).prop('checked') ? "SI" : "NO" );
        datos.append( "masculino", $( '#masculino' ).prop('checked') ? "SI" : "NO" );
        datos.append( "start_date_day", $( '#start_date_day' ).val() );
        datos.append( "end_date_day", $( '#end_date_day' ).val() );
        datos.append( "contratacion", $( '#contratacion' ).val() );
        datos.append( "tipo_usuario", $( '#tipo_usuario' ).val() );
        datos.append( "opcn", "datos" );

        $.ajax({
            url         : url,
            type        :'post',
            contentType : false,
            data        : datos,
            processData : false,
            dataType    : 'json',
            cache       : false,
            async       : true
        })
        .done( function( data ){
            NProgress.done();
            detener();
            $('#resultado').html( data.length );
            $('#consultaAnalytics').attr( 'disabled', false );
            console.table( data );
        } )
        .always( function(){
            NProgress.done();
            detener();
            $('#consultaAnalytics').attr( 'disabled', false );
        } );
    }//fin validar
} );

function validar(){
    var valid = 0;
    var estado = false;

    $('#start_date_day').css( "border-color", "#efefef" );
    $('#end_date_day').css( "border-color", "#efefef" );

    if($('#start_date_day').val() == ""){
      valid = 1;
      $('#start_date_day').css( "border-color", "#b94a48" );
    }
    if($('#end_date_day').val() == ""){
      valid = 1;
      $('#end_date_day').css( "border-color", "#b94a48" );
    }
    if($('#end_date_day').val() < $('#start_date_day').val() ){
      valid = 1;
      $('#end_date_day').css( "border-color", "#b94a48" );
      $('#start_date_day').css( "border-color", "#b94a48" );
      notyfy({
          text: 'La fecha final no puede ser menor que la fecha inicial',
          type: 'error' // alert|error|success|information|warning|primary|confirm
      });
    }
    if( valid == 0 ){
        estado = true ;
    }

    return estado;
}

function contar( div ){
    intervalo = setInterval( function(){
        contador++;
        $('#'+div).html( contador );
    }, 1000 );
}

function detener(){
    contador = 0;
    clearInterval( intervalo );
}
