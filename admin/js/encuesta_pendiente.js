$(document).ready(function () {
  encuesta_id = $('#encuesta_id').val()
  cant_preg = $('#cant_preg').val()
});

respuestas = [];
$('.respuesta').click(function (e) { 
  var encuesta_pregunta_id = $(this).parents('div').data('encuesta_pregunta_id')
  var pos = $(this).parents('div').data('pos')
  respuestas[pos] = {encuesta_pregunta_id: encuesta_pregunta_id, puntaje: $(this).val()};
});

$('#enviar').click(function (e) {
  e.preventDefault();
  
  var contador = 0;
  $( ".respuesta" ).each(function( index, e ) {
      if($(this).is(':checked')){
          contador = contador + 1;
      }
  });
  
  if( contador < cant_preg ){
    alert('Te faltan preguntras sin responder');
    return false;
  }

  $( "#enviar" ).prop( "disabled", true );

  $.ajax({
    url: 'controllers/encuestas_pendientes.php',
    type: 'POST',
    dataType: 'JSON',
    data: {opcn: 'guardarEncuesta', encuesta_id:encuesta_id, datos:respuestas}
  })
  .done(function(  data ) {
    if(!data.error){
      notyfy({
        text: data.msj,
        type: 'success' // alert|error|success|information|warning|primary|confirm
      });
      setTimeout(function(){ location.reload() }, 2000);
    }else{
      notyfy({
        text: data.msj,
        type: 'error' // alert|error|success|information|warning|primary|confirm
      });
    }
  })
  .fail(function() {
    console.log('error');
  });
});