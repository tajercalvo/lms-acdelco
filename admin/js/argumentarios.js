$( "#form_CrearGaleria" ).submit(function( event ) {
    if(valida()){
        return true;
    }else{
        return false;
    }
    event.preventDefault();
    return false;
});

/*
se agregan atributos cuando carga la imagen
*/
$(document).ready(function(){
    // Select Placeholders
    $("#argument_cat_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#argument_cat_id_ed").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#media_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#media_idv").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#media_id_ed").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#media_idv_ed").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

    $("#librarylibrary_id").select2({
        placeholder: "Seleccione el (los) documentos",
        allowClear: true
    });
    

});

$(document).ready(function() {
    $('a[href][title]').qtip({
        content: {
            text: false
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        },
        position: {
            target: 'mouse'
        }
    });
});
$(document).ready(function() {
    $(".RespFaq").each(function (index,element) {
        $(element).hide();
    });
});
function MuestraResp(argument_faq_id){
    $(".RespFaq").each(function (index,element) {
        $(element).hide();
    });
    $('#RespFaq'+argument_faq_id).show(300);
}

// Modal confirmacion

    $('#enviar').click(function()
    {
        // $('#aprob'+27).css("visibility","hidden");
        // $('#aprob').css("visibility","visible");
        // $('#aprob').css("visibility","hidden");
        
        bootbox.confirm("Estas seguro de enviar este comentario?", function(result) 
        {
           
           if (result) {
             var data = new FormData();
    data.append('argument_id',$('#argument_id').val());
    data.append('good_practices', $('#good_practices').val());
    data.append('opcn','Good_practice');
    var url = "controllers/argumentarios.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                notyfy({
                    text: 'Muchas gracias, hemos recibido tu comentario, podrás verlo una vez sea aprobado por un administrador',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                // var Cantlikes = parseInt($('#CantLikes'+news_id).text());
                // $('#CantLikes'+news_id).text(Cantlikes+1);
            }
        }
    });
           }else{
             notyfy({
                text: 'has cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            
           }

        });
    });

    //    $('#enviar').click(function()
    // {
    //     $("tr:hidden#target").css("visibility","visible");
    // });

/*function valida(){
    var valid = 0;
    $('#argument').css( "border-color", "#efefef" );
    if($('#argument').val() == ""){
        $('#argument').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description_principal').val() == ""){
        $('#description_principal').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new').val()==""){
        valid=1;
        notyfy({
                text: 'Debe seleccionar una imagen Principal',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
    }else{
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if($('#file_pdf').val()==""){
        valid=1;
        notyfy({
                text: 'Debe seleccionar un archivo PDF para la ficha técnica',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
    }else{
        var inputFileImage1 = document.getElementById("pdf_new");
        var file1 = inputFileImage1.files[0];
        if(file1.type != "application/pdf" && file1.type != "application/zip"){
            valid = 1;
            notyfy({
                text: 'El archivo seleccionado no es PDF, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file1.size>=10000001){
            valid = 1;
            notyfy({
                text: 'El archivo PDF excede el peso máximo permitido (1 Mb) o no es un pdf único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}
*/

$( "#form_EditarGaleria" ).submit(function( event ) {
    if(valida()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

/*function validaEdt(){
    var valid = 0;
    $('#argument_ed').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#argument_ed').val() == ""){
        $('#argument_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new_ed').val() != ""){
        var inputFileImage = document.getElementById("image_new_ed");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}
*/
function LikeGalery(news_id){
    var data = new FormData();
    data.append('news_id',news_id);
    data.append('opcn','LikeGalery');
    var url = "controllers/argumentarios.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                notyfy({
                    text: 'Muchas gracias, hemos recibido tu like! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes'+news_id).text());
                $('#CantLikes'+news_id).text(Cantlikes+1);
            }else{
                notyfy({
                    text: 'Ya votaste por esta multimedia anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}



function aprobar(news_id){
    bootbox.confirm("Estas seguro de enviar este comentario?", function(result) 
        {
            if (result) {

    var data = new FormData();
    data.append('news_id',news_id);
    data.append('opcn','aprobar');
    var url = "controllers/argumentarios.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){

                notyfy({
                    text: 'Has aprobado este comentario',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                
                // $('#aprob'+news_id).css("visibility","hidden");
                // $('#elim'+news_id).css("visibility","hidden");
                $('#aprob'+news_id).remove();
                // $('#elim'+news_id).remove();
            }
         }
        });
    }else{
             notyfy({
                text: 'has cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
         }
    });
}

function eliminar(news_id){
    bootbox.confirm("Estas seguro de eliminar este comentario?", function(result) 
        {
            if (result) {

    var data = new FormData();
    data.append('news_id',news_id);
    data.append('opcn','eliminar');
    var url = "controllers/argumentarios.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){

                notyfy({
                    text: 'Has eliminado este comentario',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                
                // $('#aprob'+news_id).css("visibility","hidden");
                // $('#elim'+news_id).css("visibility","hidden");
                // $('#aprob'+news_id).remove();
                $('#elim'+news_id).remove();
            }
         }
        });
    }else{
             notyfy({
                text: 'has cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
         }
    });
}


/*
Andres Vega 02/08/2016
funcion para validar que se envien correctamente los datos de material de apoyo
@ titulo del archivo
@ archivo adjunto
*/
function validarAdjunto(){
  var valid = 0;
  $('#tittle_file').css( "border-color", "#efefef" );
  //valida la caja de texto
  if($('#tittle_file').val() == ""){
    valid = 1;
    $('#tittle_file').css( "border-color", "#b94a48" );
  }

  //valida el archivo de texto
  if($("#pdf_adjunto").val() == ""){
    valid = 1;
    notyfy({
      text: 'Debe seleccionar un archivo PDF',
      type: 'error' // alert|error|success|information|warning|primary|confirm
    });
  }else{// else_1
    var inputFilePDF = document.getElementById("pdf_adjunto");
    var adjunto = inputFilePDF.files[0];

    //valida que el archivo adjunto se cargue con el formato correcto
    if(adjunto.type != "application/pdf" && adjunto.type != "application/zip"){//if_1
        valid = 1;
        notyfy({
            text: 'El archivo seleccionado no es PDF, único formato válido',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }//fin if_1

    //Valida el tamaño del archivo
    if(adjunto.size>=10000001){//if_2
        valid = 1;
        notyfy({
            text: 'El archivo PDF excede el peso máximo permitido (1 Mb) o no es un pdf único formato válido',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }//fin if_2

    if(valid == 0){//if_3
        if(confirm('¿Esta seguro de Guardar esta información?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }//fin if_3

  }//fin else_1

}//fin funcion validar

/*
Andres Vega 02/08/2016
funcion JQuery para validar y guardar un archivo adjunto PDF como material de apoyo
*/
$("#form_MaterialApoyo").submit(function(event){
  if(validarAdjunto()){
    return true;
  }else{
    return false;
  }
  event.preventDefault();
});//fin metodo JQuery

$("#form_CreateFaq").submit(function(event){
  if(validarFaq()){
    return true;
  }else{
    return false;
  }
  event.preventDefault();
});

function validarFaq(){
  var valid = 0;
  $('#faq').css( "border-color", "#efefef" );
  $('#faq_answer').css( "border-color", "#efefef" );
  //valida la caja de texto
  if($('#faq').val() == ""){
    valid = 1;
    $('#faq').css( "border-color", "#b94a48" );
  }
  if($('#faq_answer').val() == ""){
    valid = 1;
    $('#faq_answer').css( "border-color", "#b94a48" );
  }
    if(valid == 0){//if_3
        if(confirm('¿Esta seguro de Guardar esta información?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

$(window).on('load', function(){
  setTimeout(function(){
      $("#banner-home").owlCarousel({
          slideSpeed : 500,
          paginationSpeed : 600,
          navigation: false,
          autoPlay: 5000,
          items: 1,
          navigationText: ['Anterior', 'Siguiente']
      });
  },100);
});
