(function () {
  var total_disponible = 0;
  $(document).ready(function () {
    $("select").select2({
      placeholder: "Seleccionar",
      allowClear: true,
    });

    total_disponible = 100 - $("#total_nota").val();
  });

  $("#tipo").change(function (e) {
    e.preventDefault();
    console.log($(this).val());
    if ($(this).val() == "Evaluacion") {
      $("#div_evaluacion").css("display", "block");
    } else {
      $("#div_evaluacion").css("display", "none");
    }
  });

  $("#porcentaje").on("input", function () {
    this.value = this.value.replace(/[^0-9]/g, "");
  });
  $("#porcentaje").keyup(function (e) {
    // console.log($(this).val())
    // console.log(total_nota)
    if ($(this).val() > total_disponible) {
      $(this).val(total_disponible);
    }
  });

  $("#frm").submit(function (e) {
    e.preventDefault();

    var formData = new FormData();
    formData.append("opcn", "crear");
    formData.append("activity", $("#activity").val());
    formData.append("orden", $("#orden").val());
    formData.append("tipo", $("#tipo").val());
    formData.append("resultado", $("#resultado").val());
    formData.append("porcentaje", $("#porcentaje").val());
    formData.append("adjuntos", $("#adjuntos").val());
    formData.append("review_id", $("#review_id").val());
    formData.append("instrucciones", $("#instrucciones").val());
    formData.append("video", $("#video").val());

    if ($("#activity").val() == "") {
      alert("Agregar el nombre de la actividad");
      return false;
    }
    if ($("#orden").val() == "") {
      alert("Agrega un orden");
      return false;
    }

    if ($("#porcentaje").val() == "") {
      alert("Agrega el porcentaje");
      return false;
    }

    if ($("#tipo").val() == "Evaluación" && $("#review_id").val() === null) {
      alert("Agrega una evaluación");
      return false;
    }

    var archivo = document.getElementById("adjunto");
    // console.log(archivo.files[0]);

    if ($("#instrucciones").val() == "") {
      alert("Agrega las intrucciones");
      return false;
    }

    if (archivo.files[0] === undefined) {
      if (
        !confirm(
          "No has adjuntado ningún archivo a esta actividad, \n ¿desea continuar?"
        )
      ) {
        return false;
      }
    } else {
      formData.append("adjunto", archivo.files[0]);
    }

    $(".btn").attr("disabled", true);
    $.ajax({
      url: "controllers/ac_modulo.php",
      type: "post",
      dataType: "json",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
    }).done(function (data) {
      $(".btn").attr("disabled", false);
      if (data.error) {
        notyfy({
          text: data.msj,
          type: "error", // alert|error|success|information|warning|primary|confirm
        });
      } else {
        notyfy({
          text: data.msj,
          type: "success", // alert|error|success|information|warning|primary|confirm
        });
        location.reload();
      }
    });
  });

  $(".borrar").click(function (e) {
    e.preventDefault();
    var activity_id = $(this).data("activity_id");
    if (!confirm("¿Estas seguro de borrar esta actividad?")) {
      return;
    }
    $.ajax({
      url: "controllers/ac_modulo.php",
      type: "POST",
      dataType: "JSON",
      data: { opcn: "borrar", activity_id: activity_id },
    })
      .done(function (data) {
        if (data.error) {
          notyfy({
            text: data.msj,
            type: "error", // alert|error|success|information|warning|primary|confirm
          });
        } else {
          notyfy({
            text: data.msj,
            type: "success", // alert|error|success|information|warning|primary|confirm
          });
          location.reload();
        }
      })
      .fail(function () {
        console.log("error");
      });
  });
})();
