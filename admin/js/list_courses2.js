$(document).ready(function() {

    if( $('#cursoInvitados').val() ){
        console.log("ingreso a cursoInvitados");
        consulta('buscar', '' ,$( "#cursoInvitados" ).val());
    }else{
        console.log("ingreso a consultarCursos");
        consulta('consultarcursos', 1, ''); // consulta habilidades tecnicas
    }
  // animacion de entrada
  $('#body').css('visibility', 'visible');
  expandir_en_proceso()
  expandirRecomendados()
  expandirMasvisto()
});

(function ($) { // Encapsulamos las funcionesp para que las variables que se declaren globales solo queden dentro de esta funcion anonima
    // Mantener contenido activo
    $( "#menuPrincipal > div > .btn-colorgm").click(function() {
      var id = $(this).attr("data-specialty_id");// Capturamos el id de la especialidad del curso que se va a consultar
      $( ".btn-colorgm" ).removeClass( "active" ); // le quitamos a todos la clase active que consiste en poner el fondo de color amarillo
      $(this).addClass( "active" ); // le agregamos la clase active al elemnto que se le dio clic para dejarle el fondo amarillo
      consulta('consultarcursos', id, ''); //opcn, consulta instantanea y segmento
    });

    $( ".btn-colorgray" ).click(function() {
        idPadre = $(this).parent().parent().attr('id');
        $( "#"+idPadre+" div "+".btn-colorgray" ).removeClass( "active" );
        var id =$(this).attr("id");
        $( "#"+id ).addClass( "active" );
    });

})(jQuery);

// Expandir y contraer
// ultimo
function expandir_en_proceso() {
  clase = $('#en_proceso').attr('class');
    var res = clase.split("-");
    if(res[(res.length-1)] == 'up'){
      // $('#contenidoVertical').css("visibility","hidden");
      $('#en_proceso').removeClass('fa-angle-double-up');
      $('#en_proceso').addClass('fa-angle-double-down');
      $("#icono_en_proceso").slideUp(500);
    }else{
      $('#en_proceso').removeClass('fa-angle-double-down');
      $('#en_proceso').addClass('fa-angle-double-up');
      $("#icono_en_proceso").slideDown(1000);
    }
}
$('#en_proceso').click(function(){
    expandir_en_proceso();
    
});

$('#verMasUltimo').click(function(){
    expandir_en_proceso();


});

// recomendados
function expandirRecomendados() {
   clase = $('#expandirRecomendado').attr('class');
    var res = clase.split("-");
    if(res[(res.length-1)] == 'up'){
      $('#expandirRecomendado').removeClass('fa-angle-double-up');
      $('#expandirRecomendado').addClass('fa-angle-double-down');
      $("#recomendadoh").slideUp(500);
    }else{
      $('#expandirRecomendado').removeClass('fa-angle-double-down');
      $('#expandirRecomendado').addClass('fa-angle-double-up');
      $("#recomendadoh").slideDown(1000);
    }
}
$('#expandirRecomendado').click(function(){
   expandirRecomendados();
});

$('#verMasRecomendados').click(function(){
   expandirRecomendados();
});

// mas visto
function expandirMasvisto() {
  clase = $('#expandirMasvisto').attr('class');
    var res = clase.split("-");
    if(res[(res.length-1)] == 'up'){
      $('#expandirMasvisto').removeClass('fa-angle-double-up');
      $('#expandirMasvisto').addClass('fa-angle-double-down');
      $("#masvistoH").slideUp(500);
    }else{
      $('#expandirMasvisto').removeClass('fa-angle-double-down');
      $('#expandirMasvisto').addClass('fa-angle-double-up');
      $("#masvistoH").slideDown(1000);
    }
}

$('#expandirMasvisto').click(function(){
    expandirMasvisto();
});

$('#verMasVisto').click(function(){
    expandirMasvisto();
});
// FIn Expandir y contraer

function consulta(opcn, specialty_id, texto_buscar){

        $('.contenidoVertical').html('<span class="label label-success">Consultando cursos, por favor espere... <img style="width: 15px; " src="../assets/Radio.gif"></span>');
        $('.Mostrar').html('');

        var data = new FormData();
        data.append('opcn',opcn);
        if(specialty_id == ''){
            data.append('texto_buscar',texto_buscar);
        }else{
            data.append('specialty_id',specialty_id);
        }

        var url = "controllers/list_courses2.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            // dataType: "text",
            async: true,
            cache:false,
            success: function(data) {
              if(data.error){
                  notyfy({
                        text: data.mensaje,
                        type: 'error' // alert|error|success|information|warning|primary|confirm
                    });
              }else{
                    if(specialty_id == ''){
                        pintar_contenido(data.encontrados, 'class', data.token, data._valSco, 'Inscribirme');
                    }else{
                        if(data.finalizados.length > 0){
                            cambiarcontenido(data.finalizados[0].image, data.finalizados[0].course, data.finalizados[0].description, 'Mostrar-finalizados', data.token, data._valSco, data.finalizados[0].course_id);
                          }

                        if(data.en_proceso.length > 0){
                            cambiarcontenido(data.en_proceso[0].image, data.en_proceso[0].course, data.en_proceso[0].description, 'Mostrar-en_proceso', data.token, data._valSco, data.en_proceso[0].course_id);
                          }

                          if(data.disponible.length > 0){
                            cambiarcontenido(data.disponible[0].image, data.disponible[0].course, data.disponible[0].description, 'Mostrar-disponible', data.token, data._valSco, data.disponible[0].course_id);
                          }

                        pintar_contenido(data.en_proceso, 1, data.token, data._valSco, 'Continuar');
                        pintar_contenido(data.disponible, 2, data.token, data._valSco, 'Inscribirme');
                        pintar_contenido(data.finalizados, 3, data.token, data._valSco, 'Repasar');
                    }
              }
            } // end function data
        }).fail(function() {
          console.log('falló ajax');
      });
}

function pintar_contenido(data, id, token, valSco, accion){
      var contenido = '';
      for (var i = 0; i < data.length; i++) {

                    contenido +='<div class="widget widget-heading-simple widget-body-gray widget-offers">';
                    contenido +='<div class="widget-body">';
                    contenido +='<div class="media datos" data-image="'+data[i].image+'" data-course="'+data[i].course+'" data-description="'+data[i].description+'" data-token="'+token+'" data-valSco="'+valSco+'" data-course_id="'+data[i].course_id+'">';
                    contenido +='<a href="#valor"><div class="img" ><img class="post-carga media-object pull-left thumb hidden-tablet hidden-phone" src="../assets/images/distinciones/'+data[i].image+'"></div></a>';
                    contenido +='<div class="media-body">';
                    contenido +='<strong><h><a class="a" return false;" href="">'+data[i].course+'</a></h></strong>';
                    contenido += '<br><span class="label label-'+data[i].label+'" style="border-radius: 10px;">'+data[i].disposicion+'</span>';
                    contenido +='</div>';
                    contenido +='</div>';
                    contenido +='<p class="text-small margin-none"><a href="course_detail.php?token='+token+'&_valSco='+valSco+'&opcn=ver&id='+data[i].course_id+'" target="_blank"><i class="fa fa-fw fa-youtube-play"></i> <strong>'+accion+'</strong></a></p>';
                    contenido +='</div>';
                    contenido +='</div>';
      }// FIN for

      if(id == 'class'){
            $('.contenidoVertical').html(contenido);
            $('.contenidoVertical').hide().fadeIn();
        }else{
            $('#contenidoVertical_'+id).html(contenido);
            $('#contenidoVertical_'+id).hide().fadeIn();
        }
}

// evento al pulsar enter
$("#txtSearch").keypress(function(e) {
   if(e.which == 13) {
      consulta('buscar', '' ,$( "#txtSearch" ).val());
      return false;
   }
});

$('body').on('click', '.datos', function(){
        var toggle = $(this).parent().parent().parent().attr('data-toggle')
        var imagen =  $(this).attr('data-image');
        var course =  $(this).attr('data-course');
        var description =  $(this).attr('data-description');
        var token =  $(this).attr('data-token');
        var valSco =  $(this).attr('data-valSco');
        var course_id =  $(this).attr('data-course_id');
        cambiarcontenido(imagen, course, description, toggle, token, valSco, course_id);
        return false;
    });

function cambiarcontenido(imagen, course, description, toggle, token, valSco, course_id){
        div = '';
        div += '<a href="course_detail.php?token='+token+'&_valSco='+valSco+'&opcn=ver&id='+course_id+'" target="_blank"><img class="img-responsive" src="../assets/images/distinciones/'+imagen+'" alt="imagen"></a>';
        /*div += '<div class="row fondoGMMasOscuro" style="height: 40px; color: #FFF;">';
        div += '<div class="col-md-9"></div>';
        div += '</div>';*/
        div += '<br><div class="row">';
        div += '<div class="col-md-12">'+course+'</div>';
        div += '<div class="col-md-12">'+description+'</div>';
        div += '</div>';
        $('#'+toggle).html(div);
}
