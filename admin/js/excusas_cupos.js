var data_tables;

function data_table() {
    data_tables = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": `controllers/excusas_cupos.php?action=getElementsAjax&mes=${$('#mes').val()}&anio=${$('#anio').val()}&dealer=${$('#concesionario_l').val()}`,
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [
            [0, "asc"]
        ],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate": {
                "sNext": "Siguiente",
                "sPrevious": "Anterior",
                "sFirst": "Inicio",
                "sLast": "Fin"
            }
        }
    });
}

$("#crearExcusa").click(function() {
    var tipo = $('#tipoU').val();
    var url = "op_excusa_cupo.php?opcn=nuevo";
    // var mensaje = 'No puedes cargar una excusa despues de la fecha establecida para hacerlo, fecha limite 27 de cada mes';
    //1 = Admin | 0 = lider, user
    // var d = new Date();
    // var dia = d.getDate();
    // console.log("tipo", tipo, "dia", dia);
    window.location = url;
    // if (tipo == 1) {
    //     window.location = url;
    // } else {
    //     if (dia > 27) {
    //         notyfy({
    //             text: mensaje,
    //             type: 'warning' // alert|error|success|information|warning|primary|confirm
    //         });
    //     } else {
    //         window.location = url;
    //     }
    // }
});

// op_excusa_cupo.php
$(document).ready(function() {
    $("#schedule_id").select2({
        placeholder: "Seleccione un curso",
        minimumInputLength: 1,
        allowClear: true,
        ajax: {
            //How long the user has to pause their typing before sending the next request
            quietMillis: 150,
            url: 'controllers/excusas_cupos.php',
            type: 'post',
            dataType: 'json',
            data: function( params ) {
                console.log("parametros", params);
                return {
                    searchTerm: params.term,
                    opcn: "consulta_schedule"
                };
            },
            delay: 250,
            processResults: function(data) {
                return { results: data };
            }
        }
    });

    $("#dealer_id").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });

    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

    // $("#aplica_para").select2({
    //     placeholder: "Seleccione donde se aplicara la excusa",
    //     allowClear: true
    // });

    // $("#type").select2({
    //     placeholder: "Seleccione el tipo de excusa",
    //     allowClear: true
    // });

    console.log($.getURL('opcn'));
    if ($.getURL('opcn') == "nuevo") {
        $("#user_id").select2({
            placeholder: "Seleccione un usuario",
            minimumInputLength: 2,
            allowClear: true,
            ajax: {
                //How long the user has to pause their typing before sending the next request
                quietMillis: 150,
                url: 'controllers/excusas_cupos.php',
                type: 'POST',
                dataType: 'json',
                data: function(term, page) {
                    //console.log("Valor:"+term+" y Page:"+page);
                    return {
                        searchTerm: term,
                        opcn: "getUsuarios"
                    };
                },
                delay: 250,
                results: function(data) {
                    return { results: data };
                }
            }
        });
    } else if ($.getURL('opcn') == "editar") {
        $("#user_id").select2({
            placeholder: "Seleccione un usuario",
            allowClear: true
        });
    }

}); // fin op_excusa_cupo.php

// excusas_cupos.php
$(document).ready(function() {

    $("#mes").select2({
        placeholder: "Seleccione un mes",
        allowClear: true
    });

    $("#anio").select2({
        placeholder: "Seleccione el año",
        allowClear: true
    });

    $("#concesionario_l").select2({
        placeholder: "Seleccione el tipo de excusa",
        allowClear: true
    });

    data_table();
    // CambiaUsuario();
});

$('#mes').change(function() {
    data_tables.fnSettings().sAjaxSource = `controllers/excusas_cupos.php?action=getElementsAjax&mes=${$('#mes').val()}&anio=${$('#anio').val()}&dealer=${$('#concesionario_l').val()}`;
    data_tables._fnAjaxUpdate();
});

$('#anio').change(function() {
    data_tables.fnSettings().sAjaxSource = `controllers/excusas_cupos.php?action=getElementsAjax&mes=${$('#mes').val()}&anio=${$('#anio').val()}&dealer=${$('#concesionario_l').val()}`;
    data_tables._fnAjaxUpdate();
});

$('#concesionario_l').change(function() {
    data_tables.fnSettings().sAjaxSource = `controllers/excusas_cupos.php?action=getElementsAjax&mes=${$('#mes').val()}&anio=${$('#anio').val()}&dealer=${$('#concesionario_l').val()}`;
    data_tables._fnAjaxUpdate();
});

$("#formulario_data").submit(function(event) {
    event.preventDefault();
    var lista = $(this).serializeArray();
    console.log(lista);
    if (valida()) {
        if ($('#opcn').val() == "crear") {
            Operacion('Crear', 'crear');
        } else if ($('#opcn').val() == "editar") {
            if ($('#excuse_status_id').val() == 3) {
                // alert("rechazado");
                $('#btnReabrirExcusax').trigger('click');
            } else {
                Operacion('Editar', 'editar');
            }
        } else {
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
});

$('#btnRechazarExcusa').click(function() {
    $('#cerrarBtnRExcusa').trigger('click');
    $('#limpiarReabrir').trigger('click');
    Operacion('Editar', 'editar');
});

$("#retornaElemento").click(function() {
    window.location = "excusas_cupos.php";
});

function valida(opcion) {
    var valid = 0;
    if ($('#schedule_id').val() == null) {
        valid = 1;
    }
    if ($('#opcn').val() == "editar") {
        if ($('#status_id').val() != 3) {
            if ($('#aplica_para').val() == "") {
                valid = 2;
            }
        }
    }
    // console.log( valid );
    if (valid == 0 || $('#status_id').val() == 3) {
        if (confirm('¿Esta seguro de ejecutar esta instrucción?')) {
            return true;
        } else {
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    } else {

        if (valid == 2) {
            notyfy({
                text: 'Por favor verifíque la información que esta pendiente, Es necesario seleccionar donde se aplicara la excusa.',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        } else {
            notyfy({
                text: 'Por favor verifíque la información que esta pendiente, Es necesario seleccionar la sesión y el archivo de la excusa',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
}

function Operacion(msg, opcion) {
    try {
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0] ? inputFileImage.files[0] : '';

        if ((file.type && file.type == "application/pdf") || opcion == 'editar') {
            if ((file && file.size <= 5000000) || opcion == 'editar' ) {
                var data = new FormData();
                var image_new = file;
                data.append('excusa', image_new);
                data.append('type', $('#type').val());
                data.append('description', $('#description').val());
                data.append('schedule_id', $('#schedule_id').val());
                data.append('num_invitation', $('#num_invitation').val());
                data.append('opcn', opcion);
                data.append('dealer_id', $('#dealer_id').val());
                data.append('status_id', $('#status_id').val());
                if (opcion == 'editar') {
                    data.append('aplica_para', $('#aplica_para').val());
                    if ($('#status_id').val() == 3) {
                        data.append('excusa_text_r', $('#excusa_text_r').val());
                    }
                    data.append('excuse_dealer_id', $('#excuse_dealer_id').val());
                }
                var url = "controllers/excusas_cupos.php";
                $.ajax({
                    url: url,
                    type: 'post',
                    contentType: false,
                    data: data,
                    processData: false,
                    dataType: "json",
                    cache: false,
                    success: function(data) {
                        console.log(data);

                        if (!data.error) {
                            if (data.archivo && data.archivo != "") {
                                $('#pdf_view').attr('src', '../assets/excusas_dealer/' + data.archivo);
                            }
                            notyfy({
                                text: data.message,
                                type: 'success' // alert|error|success|information|warning|primary|confirm
                            });
                            $('#formulario_data')[0].reset();
                        } else {
                            notyfy({
                                text: data.message,
                                type: 'error' // alert|error|success|information|warning|primary|confirm
                            });
                        }

                        $('#loading_imagen').addClass('hide');
                    }
                });
            } else {
                notyfy({
                    text: 'No se ha logrado subir la información, el tamaño del archivo es demasiado GRANDE',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        } else {
            notyfy({
                text: 'No se ha logrado subir la información, revise la extensión del archivo sólo se acepta PDF',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    } catch (err) {
        notyfy({
            text: "error: " + err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}

// function CambiaUsuario() {
//     $.post('controllers/excusas_cupos.php', { opcn: 'GetSchedule', usuarioId: $('#user_id').val(), excuse_id: $('#excuse_id').val() }, function(data) {
//         $("#schedule_id").html(data);
//         $("#schedule_id").select2({
//             placeholder: "Seleccione una programación",
//             allowClear: true
//         });
//     });
// }

$('#type').change(function() {
    if ($(this).val() == "otro") {
        $('#description').attr("required", "required");
    } else {
        $('#description').removeAttr("required");
    }
});

$('#schedule_id').change(function() {
    var datos = {
        opcn: 'getScheduleDealers',
        schedule_id: $(this).val()
    };
    $.ajax({
            url: 'controllers/excusas_cupos.php',
            type: 'post',
            data: datos,
            dataType: 'json'
        })
        .done(function(data) {
            var listaDealers = '<option value=""></option>';
            if (!data.error) {
                data.dealers.forEach(function(key, idx) {
                    listaDealers += `<option value="${key.dealer_id}">${ key.dealer }</option>`;
                });
            } else {
                notyfy({
                    text: data.message,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
            $('#dealer_id').html(listaDealers);
        });
});
