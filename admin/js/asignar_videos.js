$(document).ready(function() {
    init();
    $('#toggleActivar').bootstrapToggle()
});

function init() {

  $.ajax({
    url: 'controllers/asignar_videos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {opcn: 'init'},
  })
  .done(function( data ) {
    console.log(data)
    var tabla = `<thead>
                    <tr>
                      <th>
                        <div style="border-right: 1px solid #d65050">IMAGEN</div>
                      </th>
                      <th>
                        <div style="border-right: 1px solid #d65050">
                        Descripción
                        </div>
                      </th>
                      <th>
                        <div style="border-right: 1px solid #d65050">
                        Creador y fecha de creación
                        </div>
                      </th>
                      <th>
                        <div>
                        Opciones
                        </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>`;
    data.forEach( function(e, i) {
    var accesousuario = `<button type="button" id="btn_accesoUsuario" data-toggle="modal" data-target="#accesoUsuario" class="accesoUsuario btn btn-warning btn-xs  " data-schedule_id = "`+e.schedule_id+`"><i class="fa fa-unlock"></i></button>`

        tabla += `<tr>
                  <td align="center" > <a href="../assets/Biblioteca_Cursos_VCT/`+e.file+`" target="_blank"><img src="../assets/Biblioteca_Cursos_VCT/imagenes/`+e.image+`" style="width: 50%;"></a></td>
                  <td align="center" > <span class="text-uppercase" style="font-size: 12px;"></strong>`+e.name+`</strong></span><br><span style="font-size: 11px;">`+e.description+`</span><span id = "vct_id" data-schedule_id = "`+e.schedule_id+`"></span><br>`+e.estado+`</td>
                  <td align="center" >Por: `+e.first_name+` `+e.last_name+` el `+e.date_creation+`</td>
                  <td align="center" > <a style="color: red; text-decoration: underline;"><p class="btn"  style="font-size: 12px;" onclick="eliminar(`+e.library_id+`, '`+e.image+`', '`+e.file+`')";>ELIMINAR</p></a> <a style="color: black; text-decoration: underline; font-size: 12px;" href="#Modalprogress_bar" data-toggle="modal"> <p data-library_id="`+e.library_id+`" class="editar">EDITAR</p></a> <p data-library_id="`+e.library_id+`">`+accesousuario+`</p></td>
                </tr>`
    });
    tabla += '</tbody>';
    $('#tabla').html(tabla);
    $('#cant_registros').html(data.length);

  })
  .fail(function() {
    console.log("error");
  });
}

opcn = '';
$('#agregar').click(function(event) {
  opcn = 'crear';
  $('form').trigger("reset");

});

library_id = '';
$('body').on('click', '.editar', function(event) {
  event.preventDefault();
  opcn = 'editar';
  library_id =$(this).data('library_id')
  console.log(library_id);

  $.ajax({
    url: 'controllers/asignar_videos.php',
    type: 'POST',
    dataType: 'JSON',
    data: {opcn: 'consultar_libreria', library_id:library_id},
  })
  .done(function(  data ) {
    console.log(data)
    cargos = [];
    data.cargos.forEach( function(e, i) {
      cargos.push(e.charge_id);
    });
    $("#charge_id").val(cargos).trigger('change');

    dealer = [];
    data.concesionarios.forEach( function(e, i) {
      dealer.push(e.dealer_id);
    });
    $("#dealer_id").val(dealer).trigger('change');

    roles = [];
    data.roles.forEach( function(e, i) {
      roles.push(e.rol_id);
    });
    $("#rol_id").val(roles).trigger('change');

    $('#name').val( data.libreria.name );
    $('#description').val( data.libreria.description );

    $("#segment_id").val(data.libreria.specialty_id).trigger('change');
    $("#status_id").val(data.libreria.status_id).trigger('change');
    $("#schedule_id").val(data.libreria.schedule_id).trigger('change');

    $('#status').html('')
    //oculta la modal y la notificacion despues de 3 segundos Freddy mendoza/05/07/2020
    
    setTimeout(function(){ $('.notyfy_message').click(); }, 3000);

  })
  .fail(function() {
    console.log("error");
  });

});
function eliminar( library_id, image, file ){
  var r = confirm("¿Está seguro de eliminar el resultado de esta evaluación?");
  if (r) {
      $.ajax({
         url: 'controllers/asignar_videos.php',
      type: 'POST',
      dataType: 'JSON',
      data: {opcn: 'eliminar', library_id:library_id, image: image, file: file},
      })
      .done(function( data ) {
        notyfy({
            text: data.msj,
            type: data.type
        });
        init();
      })
      .fail(function() {
        console.log("error");
      });

  }else{
    return false;
  }
};

// Selec que estan dentro de form para cargar o actualizar lso archivos
$(document).ready(function(){
    // Select Placeholders
    $("#segment_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });


    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#charge_id").select2({
        placeholder: "Seleccione la (las) trayectorias",
        allowClear: true
    });

    $("#dealer_id").select2({
        placeholder: "Seleccione la (las) empresas",
        allowClear: true
    });
    $("#rol_id").select2({
        placeholder: "Seleccione el perfil",
        allowClear: true
    });
    $("#schedule_id").select2({
      placeholder: "SELECCIONE UNA OPCION",
      allowClear: true
    });
});
//Fin select


// Inicio Funcion para seleaccionar un archivo o una URL
url_archivo = false;
$('#checkbox').click(function  input (){
  if($("#checkbox").is(':checked')) {
    url_archivo = true;
    $('#archivo_Biblioteca_Cursos_VCT').css('display', 'none');
    $('#video_youtube').css('display', 'block');
  } else {

    url_archivo = false;
    $('#archivo_Biblioteca_Cursos_VCT').css('display', 'block');
    $('#video_youtube').css('display', 'none');
  }
});
// Fin Funcion para seleaccionar un archivo o una URL

//Inicio para subir o crear los archivos
function _(el){
  return document.getElementById(el);
}

function uploadFile(){

    if( $('#name').val() == '' ){
      notyfy({
          text: 'Ingresa un nombre',
          type: 'warning'
      });
      return;
    }

    if( $('#description').val() == '' ){
      notyfy({
          text: 'Ingresa una descripción',
          type: 'warning'
      });
      return;
    }
    if( $('#schedule_id').val() == '' ){
      notyfy({
          text: 'Ingresa un Curso VCT',
          type: 'warning'
      });
      return;
    }

    if( opcn == 'editar' ){
        if( $('#image').val() == '' ){
              var confirma = confirm("No has seleccionado una imagen, no se modificará la actual, desea continuar?");
              if( confirma ){
              }else{
                return;
              }
            }

        if( $('#file1').val() == '' ){
              var confirma = confirm("No has seleccionado un archivo, no se modificará el actual, desea continuar?");
              if( confirma ){
              }else{
                return;
              }
            }

    }else{

          if( $('#image').val() == '' ){
              notyfy({
                  text: 'Selecciona una imagen para tu archivo',
                  type: 'warning'
              });
              return;
            }

        if( url_archivo ) {
              if( $('#url').length == 0 ){
                notyfy({
                    text: 'Escribe una url para tu archivo',
                    type: 'warning'
                });
                return;
              }
        }else{
            if( $('#file1').val() == '' ){
              notyfy({
                  text: 'Selecciona un archivo',
                  type: 'warning'
              });
              return;
            }
        }
    }

  var image = _("image").files[0];

            // notyfy({
            //     text: 'El archivo excede el tamaño máximo permitido (1 Mb) o no es jpg único formato válido',
            //     type: 'error' // alert|error|success|information|warning|primary|confirm
            // });

          if( !url_archivo ){
          //console.log('sisisi')
                //Inicio renombrando el archivo para evitar puntos ( . ) adicionales al inertar en la base de datos
              //   var fileName = document.getElementById('file1').files[0].name;
              //   arrayFileName = fileName.split(".");
              //   newFileName ="";

              //   for (var i = 0; i < arrayFileName.length; i++) {
              //     if (i == (arrayFileName.length-1)) {
              //       newFileName +='.';
              //     }
              //      newFileName += arrayFileName[i];
              //   }

              // }else{
                 newFileName = $('#url').val();
              }

      var formdata = new FormData();
      if(!url_archivo){
        formdata.append("file1", _("file1").files[0]);
      }
      formdata.append('opcn', opcn);
      formdata.append("image", image);
      formdata.append('name',$('#name').val());
      formdata.append('name_file',newFileName);
      formdata.append('description',$('#description').val());
      formdata.append('segment_id',$('#segment_id').val());
      formdata.append('status_id',$('select[name=status_id]').val());
      formdata.append('schedule_id',$('#schedule_id').val());
      formdata.append('url', $('#url').val());
      formdata.append('library_id', library_id);
  
      var ajax = new XMLHttpRequest();

      ajax.upload.addEventListener("progress", progressHandler, false);
      ajax.addEventListener("load", completeHandler, false);
      if(!url_archivo) {
        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.addEventListener("abort", abortHandler, false);
      }

      ajax.open("POST", "controllers/asignar_videos.php");
      ajax.send(formdata);
}

function progressHandler(event){
  var percent = (event.loaded / event.total) * 100;
  _("progressBar").style.width = Math.round(percent)+'%';
  _("status").innerHTML = Math.round(percent)+'% Cargando... por favor espere';
}
function completeHandler(event){
  var datos = JSON.parse(event.target.responseText);
  console.log(datos)
  _("status").innerHTML = datos.msj+'<br>'+datos.cargos+'<br>'+datos.concesionarios+'<br>'+datos.roles

  setTimeout(_("progressBar").style.width = "0", 3000);

   notyfy({
                    text: 'Archivo cargado correctamente ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
  init();
  //oculta la modal y la notificacion despues de 3 segundos Freddy mendoza/05/07/2020
  $('#Modalprogress_bar').modal('hide')
  setTimeout(function(){ $('.notyfy_message').click(); }, 3000);
}
function errorHandler(event){
  _("status").innerHTML = "Upload Failed";
}
function abortHandler(event){
  _("status").innerHTML = "Upload Aborted";
}
//Fin funcion para cargar el archivo
var vct_id = '';


$('body').on('click', '#btn_accesoUsuario', function(event)  {
   atras()
  $('#primero').css('display','none')
  $('#segundo').css('display','block')
   $('#busqueda').val('');
 vct_id = $(this).data('schedule_id');
  $.ajax({
          url: 'controllers/asignar_videos.php',
          type: 'POST',
          dataType: 'JSON',
          data: {opcn: 'consultar_Usuarios',vct_id: vct_id},
        })
        .done(function(data) {
          console.log(data);
              var table = ''
          
         for (var i = 0; i < data.length; i++) {
          // console.log(data[i].invitados)
            
                // console.log(data[i].nombre)
               

            
                                table+='<tr>';
                                table+='<td>'+data[i].identification+'</td>';
                                table+='<td data-user_id = "'+data[i].user_id+'">'+data[i].nombre+'</td>';
                                table+='<td><div class="col-md-6"><i class="fa fa-circle" aria-hidden="true" style="font-size: 20px;"></i> <span>'+data[i].date_creation+'</span><br><strong>'+data[i].dias+'</strong> </div> <div class="col-md-6"><input type="checkbox" '+data[i].checked+'  onchange="activarMenu('+i+')" data-user_id = "'+data[i].user_id+'" data-schedule_id="'+vct_id+'" data-toggle="toggle" data-on="SI" data-off="No" data-onstyle="success" data-offstyle="danger" id="toggleActivar'+i+'"></div></td>';
                                table+='</tr>';
            
       
            } 

            $('#asignar_usuario tbody').html(table)
            // $('#toggleActivar').bootstrapToggle('destroy')
            // for (var i = 0; i < data.length; i++) {
            //   $('#toggleActivar'+i).bootstrapToggle()

            // }


        })

});


function activarMenu(i){
  //console.log(i)
  opcion    = $('#toggleActivar'+i).prop('checked')
  user_id   = $('#toggleActivar'+i).data('user_id')
  schedule_id   = $('#toggleActivar'+i).data('schedule_id')
$('#toggleActivar').bootstrapToggle('destroy')
  console.log(user_id +' '+ opcion +' '+schedule_id)

    $.ajax({
      url: 'controllers/asignar_videos.php',
      type: 'POST',
      dataType: 'JSON',
      data: {opcn: 'asignar_video',opcion:opcion,user_id:user_id,schedule_id:schedule_id},
    })
    .done(function(data) {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
      
}

function atras(){
    $('#atras').click(function(event) {
      $('#primero').css('display','block')
      $('#segundo').css('display','none')
  });
}


$("#atras").hover(function(){
     $(this).css({'background': '#b33f3f', 'color': '#fffefe', 'border': '2px solid #c52f2b', 'border-radius': '69px', 'padding': '0px 15px 0px 15px', 'font-size': '24px', 'margin-bottom': '3px', });
    }, function(){
    $(this).css({'background': 'white', 'color': 'black', 'border': '2px solid #c52f2b', 'border-radius': '69px', 'padding': '0px 15px 0px 15px', 'font-size': '24px', 'margin-bottom': '3px', });
  });


$('#busqueda').keyup(function(event) {
  // event.preventDefault;
    var datos = $(this).val()
    //console.log(datos+' '+vct_id)
    $.ajax({
        url: 'controllers/asignar_videos.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn: 'busqueda',datos:datos,vct_id:vct_id},
      })
      .done(function(data) {
        console.log(data);
       var table = '';
        for (var i = 0; i < data.length; i++) {
           table+='<tr>';
           table+='<td>'+data[i].identification+'</td>';
           table+='<td data-user_id = "'+data[i].user_id+'">'+data[i].nombre+'</td>';
           table+='<td><div class="col-md-6"><i class="fa fa-circle" aria-hidden="true" style="font-size: 20px;"></i></div> <div class="col-md-6"><input type="checkbox" '+data[i].checked+'  onchange="activarMenu('+i+')" data-user_id = "'+data[i].user_id+'" data-schedule_id="'+vct_id+'" data-toggle="toggle" data-on="SI" data-off="No" data-onstyle="success" data-offstyle="danger" id="toggleActivar'+i+'"></div></td>';
           table+='</tr>';

        }
          $('#asignar_usuario tbody').html(table)
          
              // $('#toggleActivar').bootstrapToggle('destroy')
              // for (var i = 0; i < data.length; i++) {
              //   $('#toggleActivar'+i).bootstrapToggle()

              // }
      })
      .fail(function() {
        console.log("error");
      })

});

