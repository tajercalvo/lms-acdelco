/*
Cristian
Semana del 24 de septiembre
Se agrega la funcion de la tabla (datatable)
*/
$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/banners.php?opcn=tabla",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            },
        }
    });
});
/*
Andres Vega
17/08/2016
Se agrega el efecto de select2 a las listas seleccionadas por los selectores de JQuery
*/
$(document).ready(function(){
  $("#banner_type").select2({
    placeholder: "Seleccione un banner",
    allowClear: true
  });
  $("#status_id").select2({
    placeholder: "Seleccione uno",
    allowClear: true
  });
  $("#cargos").select2({
	    placeholder: "Seleccione las trayectorias",
	    allowClear: true
	});
  $("#concesionarios").select2({
	    placeholder: "Seleccione las empresas",
	    allowClear: true
	});
  $("#gender").select2({
    placeholder: "Seleccione un genero",
    allowClear: true
  });
  $('#start_date_day').bdatepicker({
      format: "yyyy-mm-dd",
      startDate: "2015-01-01"
  });
  $('#start_date_day').bdatepicker({
      format: "yyyy-mm-dd",
      startDate: "2015-01-01"
  });
  $('#start_date').bdatepicker({
      format: "yyyy-mm-dd",
      startDate: "2015-01-01",
      minDate: 0
  });
  $('#end_date').bdatepicker({
      format: "yyyy-mm-dd",
      startDate: "2015-01-01",
      minDate: 0
  });
  /*
  Cristian
  Semana del 24 de septiembre
  Se comenta datatable que y estaba se deja por si se llega a necesitar
  */
  //
  // var datos = {'opcn':'tabla'};
  // $("#TableData").DataTable({
  //     "bProcessing": true,
  //     "bServerSide": true,
  //     "sAjaxSource": "controllers/banners.php?opcn=tabla",
  //     "sPaginationType": "bootstrap",
  //     "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
  //     "aaSorting": [[ 0, "asc" ]],
  //     "sScrollX": "100%",
  //     "sScrollXInner": "100%",
  //     "bScrollCollapse": true,
  //     "oLanguage": {
  //         "sLengthMenu": "Mostrando _MENU_ registros por pagina",
  //         "sZeroRecords": "No hay registros",
  //         "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
  //         "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
  //         "sInfoFiltered": "",
  //         "sEmptyTable": "No hay registros",
  //         "sSearch": "Buscar",
  //         "oPaginate":{
  //             "sNext" : "Siguiente",
  //             "sPrevious" : "Anterior",
  //             "sFirst" : "Inicio",
  //             "sLast" : "Fin"
  //         }
  //     }
  // } );//fin DataTable

});//fin select 2

$( "#form_CrearBanner" ).submit(function( event ) {
    if(!valida()){
      event.preventDefault();
    }
});

/*
Funcion que valida los campos de texto y fecha en el formulario para ingresar un nuevo Banner
*/
function valida(){
    var valid = 0;
    $('#banner_name').css( "border-color", "#efefef" );
    if($('#banner_name').val() == ""){
        $('#banner_name').css( "border-color", "#b94a48" );
        valid = 1;
    }
    $('#start_date').css( "border-color", "#efefef" );
    if($('#start_date').val() == ""){
        $('#start_date').css( "border-color", "#b94a48" );
        valid = 1;
    }
    $('#end_date').css( "border-color", "#efefef" );
    if($('#end_date').val() == ""){
        $('#end_date').css( "border-color", "#b94a48" );
        valid = 1;
    }
    $('#banner_url').css( "border-color", "#efefef" );
    if($('#banner_url').val() == ""){
        $('#banner_url').css( "border-color", "#b94a48" );
        valid = 1;
    }
    $('#file_name').css( "border-color", "#efefef" );
    if($('#file_name').val() == ""){
        $('#file_name').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}//fin funcion validar

function operacion(form,msg){
    $.ajax({ url: 'controllers/banners.php',
        data: $('#'+form).serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}//fin funcion operacion


/*
Agrega la funcion de datepicker a un input de texto
*/
/*$(function(){
  $('#start_date').datepicker({
      format: "yyyy-mm-dd",
      startDate: "2015-01-01",
      minDate: 0
  });
  $('#end_date').datepicker({
      format: "yyyy-mm-dd",
      startDate: "2015-01-01",
      minDate: 0
  });
});//fin funcion datepicker
*/
function op_valida(){
    var validar = 0;
    $('#banner_name').css( "border-color", "#efefef" );
    if($('#banner_name').val() == ""){
        $('#banner_name').css( "border-color", "#b94a48" );
        validar = 1;
    }
    $('#start_date').css( "border-color", "#efefef" );
    if($('#start_date').val() == ""){
        $('#start_date').css( "border-color", "#b94a48" );
        validar = 1;
    }
    $('#end_date').css( "border-color", "#efefef" );
    if($('#end_date').val() == ""){
        $('#end_date').css( "border-color", "#b94a48" );
        validar = 1;
    }
    $('#banner_url').css( "border-color", "#efefef" );
    if($('#banner_url').val() == ""){
        $('#banner_url').css( "border-color", "#b94a48" );
        validar = 1;
    }
    if(validar == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

$("#formActualizarData").submit(function( event ){
  if(!op_valida()){
    event.preventDefault();
  }else{
    console.log("todo bien");
  }
});

/*
Andres Vega
23/08/2016
Funcion para retornar a la pagina de banners cuando se cancele una edisión de un registro
*/
$("#retornaElemento").click(function() {
    window.location="banners.php";
});
