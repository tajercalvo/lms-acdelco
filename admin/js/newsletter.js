$(document).ready(function(){
  $("#s_persona").select2({
    placeholder: "Seleccione una persona",
    allowClear: true
  });
  $("#s_concesionario").select2({
    placeholder: "Seleccione un concesionario",
    allowClear: true
  });
  $("#s_cargos").select2({
    placeholder: "Seleccione un cargo",
    allowClear: true
  });
  $("#s_genero").select2({
    placeholder: "Seleccione un genero",
    allowClear: true
  });
  $("#s_rol").select2({
    placeholder: "Seleccione un rol",
    allowClear: true
  });
});
/*
carga la plantilla del correo seleccionado
*/
$("#redactar").click(function(){
  var url = "newsletter-ajax.php";
  //console.info("listo todo",temaSel);
  $.post(url,{tema:1},function(datos,status,xhr){
    if(status == 'success'){
      $("#contenedorCorreo").html(datos);
    }
  });
});

/*
Carga la lista seleccionada segun se selecciona en el grupo
*/
function agregarLista(){
  var valorS= $("#grupo option:selected").val();
  var opcionL = 'lista';
  $.post("newsletter-ajax.php",{opcion:opcionL,valor:valorS},function(datos,status,xhr){
    if(status == 'success'){
      $("#destinatario").html(datos);
    }//fin if
  });
}
/*
deja previsualizar la imagen seleccionada para despues enviar en el correo
##Se coloco directamente en el archivo newsletter-ajax.php##
*/
$("#imagen-mail").on('change', function () {
   if (typeof (FileReader) != "undefined") {
       var image_holder = $("#contentImagen");
       image_holder.empty();
       var reader = new FileReader();
       reader.onload = function (e) {
           $("<img />", {
               "src": e.target.result,
               "style":"width: 100%"
           }).appendTo(image_holder);
       };
       image_holder.show();
       reader.readAsDataURL($(this)[0].files[0]);
   } else {
       alert("This browser does not support FileReader.");
   }
});
//valida que el campo de destinatario no este vacio
function valida(){
  var valid = 0;
  $('#s_persona').css( "border-color", "#efefef" );
  if($('#s_persona').val() == '' || $('#s_persona').val() == null){
      $('#s_persona').css( "border-color", "#b94a48" );
      valid++;
  }
  $('#s_concesionario').css( "border-color", "#efefef" );
  if($('#s_concesionario').val() == '' || $('#s_concesionario').val() == null){
      $('#s_concesionario').css( "border-color", "#b94a48" );
      valid++;
  }
  $('#s_cargos').css( "border-color", "#efefef" );
  if($('#s_cargos').val() == '' || $('#s_cargos').val() == null){
      $('#s_cargos').css( "border-color", "#b94a48" );
      valid++;
  }
  $('#s_genero').css( "border-color", "#efefef" );
  if($('#s_genero').val() == '' || $('#s_genero').val() == null){
      $('#s_genero').css( "border-color", "#b94a48" );
      valid++;
  }
  $('#s_rol').css( "border-color", "#efefef" );
  if($('#s_rol').val() == '' || $('#s_rol').val() == null){
      $('#s_rol').css( "border-color", "#b94a48" );
      valid++;
  }
  return valid;
}

$("#formCorreo").submit(function(event){
  console.log("si entro al submit", valida());
  if(valida() > 4){
    notyfy({
        text: 'No se ha seleccionado un destinatario',
        type: 'error' // alert|error|success|information|warning|primary|confirm
    });
    return false;
    event.preventDefault();
  }
});
/*
*/
function mostrar(id){
  var url = "newsletter-ajax.php";
  $.post("newsletter-ajax.php",{opcion:"mostrar",idL:id},function(datos,status,hxr){
    console.info("si entro");
    $("#contenedorCorreo").html(datos);
    $("#destinatario").select2({
      placeholder: "Seleccione un estado",
      allowClear: true
    });
  });
}//fin funcion mostrar
/*
Agrega un nombre al campo de archivo adjunto
*/
$("#inputFile").change(function(){
  var nombreFile= $("#inputFile").val();
  var longitud = nombreFile.length;
  console.log(nombreFile,longitud);
  $("#textoFile").text(nombreFile.substring(12,longitud));
});
