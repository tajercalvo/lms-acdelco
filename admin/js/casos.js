var oMemTable;
$(document).ready(function(){
    $('#modal-sentinel').modal('show');
    data_table();

    $("#vin").keyup(function() {
        if($('#vin').val().length != 17){
            notyfy({
                text: 'El Número de Vin cargado no es correcto, debe contener el número de digitos necesario para ser procesado',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    });
    $("#cat_id").keyup(function() {
        if($('#cat_id').val().length != 17){
            notyfy({
                text: 'El Número de Ticket CAT cargado no es correcto, debe contener el número de digitos necesario para ser procesado',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    });
    $('#nserie').attr('readonly', true);
    // Select Placeholders

    if($('#sentinel_model_id_s').length){
        $("#sentinel_model_id_s").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
    }
    if($('#dealer_id_s').length){
        $("#dealer_id_s").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
    }
    if($('#status_id_s').length){
        $("#status_id_s").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
    }
    if($('#sub_status_id_s').length){
        $("#sub_status_id_s").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
    }
    if($('#sentinel_service_id_s').length){
        $("#sentinel_service_id_s").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
    }
    if($('#sentinel_system_id_s').length){
        $("#sentinel_system_id_s").select2({
            placeholder: "Seleccione uno",
            allowClear: true
        });
        $('#date_start_s').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_end_s').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_escalado_start').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_escalado_end').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
    }


    if($('#status_id').length && $('#status_id').attr('type')!='hidden'){
        $("#status_id").select2({
            placeholder: "Seleccione un estado",
            allowClear: true
        });
    }
    if($('#substatus_id').length){
        $("#substatus_id").select2({
            placeholder: "Seleccione un estado",
            allowClear: true
        });
    }
    if($('#status_id_new').attr('type') != 'hidden'){
        $("#status_id_new").select2({
            placeholder: "Seleccione un estado",
            allowClear: true
        });
    }else{
        $('#date_request').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_delivered').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_scrap').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
    }
    $("#sentinel_model_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#sentinel_application_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#car_version").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#date_sentinel').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#date_requ_cdg').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#date_start_cdg').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

     $('#date_analysis_cdg').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

      $('#date_retirement_cdg').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

       $('#date_destruction_cdg').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

    if($('#date_ot').length){
        if($('#date_ot').val().length==0){
            $('#date_ot').prop('disabled', true);
        }else{
            $('#date_ot').bdatepicker({
                format: "yyyy-mm-dd",
                endDate: $('#date_sentinel').val()
            });
        }
    }
    $('#garranty_start').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2000-01-01"
    });
    $("#sentinel_service_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#transmission").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#configuration").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#sentinel_system_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#sentinel_check_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    /*$("#lubricant_mot").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#lubricant_tra").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#lubricant_dif").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });*/
    if($('#date_ot_1').length){
        $('#date_ot_1').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_ot_2').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_ot_3').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_ot_4').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_ot_5').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
    }
    if($('#date_qa_1').length){
        $('#date_qa_1').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_qa_2').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
        $('#date_qa_3').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2000-01-01"
        });
    }
});
$("#descargar").click(function() {

    //document.getElementById("mensaje_descargando").innerHTML = '<span class="label label-success">Descargando...</span>';
    $( "#mensaje_descargando" ).html('<span class="label label-success">Descargando...</span><img style="width: 15px; " src="../assets/loading.gif">');
    retorna = 0;

    var data = new FormData();
    data.append('opcn','Descargar');
    var url = "controllers/casos.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        //dataType: "json",
        dataType: "text",
        async: true,
        cache:false,
        success: function(data) {

            if (data == 'sin_session') {
                    //document.getElementById("mensaje_descargando").innerHTML = '';
                    $( "#Atencion" ).html('<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>');
                    //document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';
                    setTimeout(function() {

                    $(".gritter-item-wrapper").fadeOut(5000);

                    },4000);

              }else{

                    retorna = 1;

                        setTimeout(function() {
                            $("#datos_tabla").val(data);
                            $("#FormularioExportacion").submit();
                            $( "#mensaje_descargando" ).html('');
                            //document.getElementById("mensaje_descargando").innerHTML = '';

                        },1000);
            }// Fin else
        }// Fin
    })
    .fail(function() {
              retorna = 0;

              setTimeout(function() {

                    $(".notyfy_container").fadeOut(4000);

              },4000);

              // setTimeout(function() {
                   $( "#mensaje_descargando" ).html('');

              //           },4000);
              $( "#Atencion" ).html('<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>');


             setTimeout(function() {

                    $(".gritter-item-wrapper").fadeOut(5000);

              },4000);
      })

    //setTimeout(oculta_modal(), 5000);

    console.log(retorna);

    if (retorna == 0) {
      return true;
    }else{
      return false;
    }

});
//=========================================================
//realizar la actualizacion de los registros segun los filtros
//=========================================================
$('#filtrar_casos').submit(function( e ){
    e.preventDefault();
    var url = "controllers/casos.php?action=getElementsAjax";
    var datos = $(this).serializeArray();
    datos.forEach( function( key, idx ){
        url += `&${key.name}=${key.value}`;
    } );
    oMemTable.fnSettings().sAjaxSource = url;
    oMemTable._fnAjaxUpdate();
});//fin filtrar_casos.submit

function data_table(){
    oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/casos.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "desc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });//fin
}

function CambiarFecha(){
    $('#date_ot').prop('disabled', false);
    $('#date_ot').bdatepicker({
        format: "yyyy-mm-dd",
        endDate: $('#date_sentinel').val()
    });
}
function CambiaSerie(){
    if($('#sentinel_system_id').val() == "10"){
        $('#nserie').attr('readonly', false);
    }else{
        $('#nserie').val('');
        $('#nserie').attr('readonly', true);
    }
}
$( "#formulario_data" ).submit(function( event ) {
    if(valida()){
        if($('#opcn').val()=="crear"){
            $('#crearElemento').hide();
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            $('#editarElemento').hide();
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
    event.preventDefault();
});
$("#retornaElemento").click(function() {
    window.location="casos.php";
});

function valida(){
    var valid = 0;
    var CamposErr = "";
    $('#date_sentinel').css( "border-color", "#efefef" );
    $('#nro_ot').css( "border-color", "#efefef" );
    $('#date_ot').css( "border-color", "#efefef" );
    $('#vin').css( "border-color", "#efefef" );
    $('#km').css( "border-color", "#efefef" );
    $('#garranty_start').css( "border-color", "#efefef" );
    $('#customer_complaint').css( "border-color", "#efefef" );
    $('#diagnostic').css( "border-color", "#efefef" );
    $('#cause_failure').css( "border-color", "#efefef" );
    $('#correction').css( "border-color", "#efefef" );
    $('#parts_description').css( "border-color", "#efefef" );
    $('#cost_workforce').css( "border-color", "#efefef" );
    $('#cost_parts').css( "border-color", "#efefef" );
    $('#total').css( "border-color", "#efefef" );
    $('#cat_id').css( "border-color", "#efefef" );

    var valid = 0;

    if($('#date_sentinel').val() == ""){
        $('#date_sentinel').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Fecha del sentinel<br>";
    }
    if($('#nro_ot').val() == ""){
        $('#nro_ot').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "# OT<br>";
    }
    if($('#date_ot').val() == ""){
        $('#date_ot').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Fecha del OT<br>";
    }
    if($('#vin').val().length != 17){
        $('#vin').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "# de Vin<br>";
    }
    if($('#cat_id').val().length != 17){
        $('#cat_id').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "# de Caso en el CAT<br>";
    }
    if($('#km').val() == ""){
        $('#km').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Kilometraje<br>";
    }
    if($('#garranty_start').val() == ""){
        $('#garranty_start').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Inicio de la garantía<br>";
    }
    if($('#customer_complaint').val() == ""){
        $('#customer_complaint').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Queja del consumidor<br>";
    }
    if($('#diagnostic').val() == ""){
        $('#diagnostic').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Diagnostico<br>";
    }
    if($('#cause_failure').val() == ""){
        $('#cause_failure').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Causa de la falla<br>";
    }
    if($('#correction').val() == ""){
        $('#correction').css( "border-color", "#b94a48" );
        valid = 1;
        CamposErr = CamposErr + "Corrección<br>";
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente<br><br>'+CamposErr,
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}
var contador = 0;
function Operacion(msg){
    if(contador == 1){console.log('sisis'); return false;}
    contador = 1;

    var data = $('#formulario_data').serialize();

     var comentario = '';
     var id_comentario = '';
     if(msg == 'Actualizar'){
        id_comentario1 = $('#comments_1').data("id");
        id_comentario2 = $('#comments_2').data("id");
        id_comentario3 = $('#comments_3').data("id");
        id_comentario4 = $('#comments_4').data("id");
        id_comentario5 = $('#comments_5').data("id");

        data+= data+'&id_com1='+ id_comentario1+'&id_com2='+ id_comentario2+'&id_com3='+ id_comentario3+'&id_com4='+ id_comentario4+'&id_com5='+ id_comentario5;
        // id_comentario2 = $('#comments_2').data("id");
        // id_comentario3 = $('#comments_3').data("id");
        // id_comentario4 = $('#comments_4').data("id");
        // id_comentario5 = $('#comments_5').data("id");
     }

    // if (msg == 'Actualizar') {
    //     for (var i = 1; i < 6; i++) {
    //         comentario = $('#comments_'+i).text();
    //         id_comentario = $('#comments_'+i).data("id");

    //         data+= data+'&co'+i+'='+ comentario;
    //         data+= data+'&id_co'+i+'='+ id_comentario;
    //         console.log(id_comentario) ;

    //     }
    // }
    console.log(data);
    $.ajax({ url: 'controllers/casos.php',
        data: data,
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                  notyfy({
                      text: 'Completa la informacion antes de subir un archivo',
                      type: 'warning'
                  });
                alert("El caso ha sido actualizado correctamente");

                if (data.idRegistro) {
                    $('#idElemento').val(data.idRegistro);
                }

                $('#imgant').val("default.jpg");
                if($('#image_new').val()!=""){
                    cargaImagen();
                }
                if($('#archivo_new').val()!=""){
                    cargaArchivo();
                }
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, La sesión ha terminado, vuelva a ingresar o confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function cargaImagen(){
    $('#loading_imagen').removeClass('hide');
    try {
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type == "image/jpeg"){
            if(file.size<1500000){
                var data = new FormData();
                data.append('image_new',file);
                data.append('id',$('#idElemento').val());
                data.append('imgant',$('#imgant').val());
                data.append('opcn','CrearImagen');
                var url = "controllers/casos.php";
                    $.ajax({
                        url:url,
                        type:'post',
                        contentType:false,
                        data:data,
                        processData:false,
                        dataType: "json",
                        cache:false,
                        success: function(data) {
                            if(data.resultado=="SI"){
                                $("#ImagenReg").attr("src","../assets/images/sentinel/"+data.archivo);
                                notyfy({
                                    text: 'La imagen del vehículo ha sido cambiada correctamente',
                                    type: 'success' // alert|error|success|information|warning|primary|confirm
                                });
                            }else{
                                notyfy({
                                    text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                                    type: 'error' // alert|error|success|information|warning|primary|confirm
                                });
                            }
                            $('#loading_imagen').addClass('hide');
                        }
                    });
            }else{
                notyfy({
                    text: 'La imagen NO ha sido cambiada, el tamaño es demasiado GRANDE',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            notyfy({
                text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }catch(err) {
        $('#loading_imagen').addClass('hide');
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function cargaArchivo(){
    $('#loading_imagen').removeClass('hide');
    try {
        var inputFileImage = document.getElementById("archivo_new");
        var file = inputFileImage.files[0];
        if(file.size<1500000){
            var data = new FormData();
            data.append('archivo_new',file);
            data.append('id',$('#idElemento').val());
            data.append('opcn','CrearArchivo');
            var url = "controllers/casos.php";
                $.ajax({
                    url:url,
                    type:'post',
                    contentType:false,
                    data:data,
                    processData:false,
                    dataType: "json",
                    cache:false,
                    success: function(data) {
                        if(data.resultado=="SI"){
                            notyfy({
                                text: 'El archivo con la información Sentinel ha sido cargado correctamente',
                                type: 'success' // alert|error|success|information|warning|primary|confirm
                            });
                        }else{
                            notyfy({
                                text: 'El archivo con la información del sentinel NO ha sido cambiada, revise la extensión del archivo sólo se acepta ZIP, JPG o PDF',
                                type: 'error' // alert|error|success|information|warning|primary|confirm
                            });
                        }
                        $('#loading_imagen').addClass('hide');
                    }
                });
        }else{
            notyfy({
                text: 'La imagen NO ha sido cambiada, el tamaño es demasiado GRANDE',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }catch(err) {
        $('#loading_imagen').addClass('hide');
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}

// Capturar el id de la url
// se pide así $.get("idurl")
(function($) {
    $.get = function(key)   {
        key = key.replace(/[\[]/, '\\[');
        key = key.replace(/[\]]/, '\\]');
        var pattern = "[\\?&]" + key + "=([^&#]*)";
        var regex = new RegExp(pattern);
        var url = unescape(window.location.href);
        var results = regex.exec(url);
        if (results === null) {
            return null;
        } else {
            return results[1];
        }
    }
})(jQuery);
