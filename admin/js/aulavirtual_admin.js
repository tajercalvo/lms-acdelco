$(document).ready(function () {
    NProgress.configure({ parent: '.loader_s' });
    Init();

    $('#privada').select2({});
});

$('#cargos').select2({
    ajax: {
    url: "controllers/aulavirtual_admin.php?opcn=cargos",
      dataType: 'json',
      type: 'POST'
      // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
  });

$('.js-data-example-ajax').select2({
    ajax: {
    url: "controllers/aulavirtual_admin.php",
      dataType: 'json',
      type: 'POST'
      // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
  });

$('.openClose').click(function (e) { 
    e.preventDefault();
    $('#div_crear_editar').toggle();
    $('#form_Crear')[0].reset();
    $('#BtnCreacion').text('Crear');
    $('.js-data-example-ajax').html(`<option value=""></option>`);
});

function editar(con, id_broadcast, token, session_id, privada ) {
    $('#div_crear_editar').css('display', 'block');
    $('#wrapper').animate({scrollTop:0}, 1250);
    $('.btn').prop("disabled", true);
    $("#conference").val(con);
    $('#BtnCreacion').text('Editar');
    $("#token").val(token);
    $("#session_id").val(session_id);
    $('#privada').val(privada).trigger('change')

    opcn = 'editar';
    conference_id = id_broadcast;
    $.ajax({
        url: "controllers/aulavirtual_admin.php",
        data: { opcn: 'buscar_cargos_conferencias', id_broadcast: id_broadcast },
        type: 'POST',
        dataType: 'json'
    })
        .done(function (data) {
            console.log(data);
            html = "";
            data.teacher_id.forEach(function (key, idx) {
                aux = `<option selected="selected" value="${key.user_id}">${key.nombre}</option>`;
                html += aux;
            });
            $('#teacher_id').html(html);

            html = "";
            data.teacher_aux.forEach(function (key, idx) {
                aux = `<option selected="selected" value="${key.user_id}">${key.nombre}</option>`;
                html += aux;
            });
            $('#auxiliares').html(html);

            html = "";
            data.cargos.forEach(function (key, idx) {
                aux = `<option selected="selected" value="${key.charge_id}">${key.cargo}</option>`;
                html += aux;
            });
            $('#cargos').html(html);

            $('.btn').prop("disabled", false);

        })
        .fail(function () {
            console.log("error");
        });

}


var texto = '',
    anio = '',
    foros = [];

function abrirModal(conference_id) {
    $('#conference_id').val(conference_id);
    console.log(conference_id);
    $('#ModalPresentacion').modal();
}

function preguntas(conference_id) {
    $('#conference_id').val(conference_id);
    $('#modalpreguntas').modal();
    $('#tabla_respuestas').html('');
    $('#tabla_respuestas').html('');
    $('#pregunta').html('');
    $.ajax({
        url: "controllers/aulavirtual_admin.php?get_preguntas",
        type: 'POST',
        dataType: 'JSON',
        data: { opcn: 'get_preguntas', conference_id: conference_id }
    })
        .done(function (data) {
            console.log(data)
            let tabla = '';
            data.forEach(function (e, i) {
                tabla += `<tr class"pregunta activa">
                        <td>`+ e.file + `</td>
                        <td>`+ e.name + `</td>
                        <td style="text-align: center;">
                            <span style="cursor: pointer;" onclick="eliminar_pregunta(`+ e.course_id + `, `+ e.coursefile_id + `, `+ e.question_id + `);" class="text-primary"><i class="fa fa-trash-o"></i></span>
                            <span style="cursor: pointer;" onclick="ver_respuestas(`+ e.question_id + `)" class="pregactiva text-success"><i class="fa fa-eye" ></i></span>
                        </td>
                    </tr>`
            });
            $('#tabla_preguntas').html(tabla);
        })
        .fail(function () {
            console.log('error');
        });
}

$('body').on('click', '.pregactiva', function () {
    $("#tabla_preguntas tr").css("background-color", "white");
    $(this).parents('tr').css("background-color", "#f2f2f2");
    console.log('sisis')
});

let question_id = ''
function ver_respuestas(question) {
    question_id = question
    $('#tabla_respuestas').html('');
    $.ajax({
        url: "controllers/aulavirtual_admin.php?get_respuestas",
        type: 'POST',
        dataType: 'JSON',
        data: { opcn: 'get_respuestas', question_id: question }
    })
        .done(function (data) {
            console.log(data)
            let tabla = '';
            data.forEach(function (e, i) {
                tabla += `<tr>
                            <td>`+ e.answer + `</td>
                            <td style="text-align: center;">
                                <span style="cursor: pointer;" onclick="eliminar_respuestas(`+ e.id + `);" class="text-primary"><i class="fa fa-trash-o"></i></span>
                            </td>
                        </tr>`
            });
            $('#tabla_respuestas').html(tabla);
        })
        .fail(function () {
            console.log('error');
        });
}

function renderForos(foros, rol) {
    console.log(foros)
    var html = "";
    foros.forEach(function (key, idx) {
        var f = new Date(key.date_creation);
        var fecha = f.getDate() + "-" + (f.getMonth() + 1) + "-" + f.getFullYear();
        var imagen = key.image != "" ? key.image : "default.jpg";
        var cargar_presentacion = rol == 'admin' ? '<button class="btn btn-default" onclick="abrirModal( ' + key.conference_id + ' )"><i class="fa fa-upload"></i> Presentación </button> <button class="btn btn-default" onclick="preguntas( ' + key.conference_id + ' )"><i class="fa fa-list"></i> Preguntas </button> <button class="btn btn-success bt-xs" onclick="editar( `' + key.conference + '`,`' + key.conference_id + '`,`' + key.token + '`,`' + key.session_id + '`,`' + key.privada + '`   )"><i class="fa fa-pencil"></i> Editar </button> <a href="aulavirtual_admin.php?opcn=descargar&schedule_id='+key.conference_id+'" class="btn btn-primary"><i class="fa fa-download"></i> Descargar </a>' : '';
        var aux = `<div class="media innerAll inner-2x border-bottom margin-none">
                    <div class="pull-left media-object" style="width:100px">
                        <div class="text-center">
                            <a href="ver_perfil.php?back=usuarios&id=${ key.creator}" class="clearfix">
                                <img src="../assets/images/usuarios/${ key.image_usr}" class="rounded-none" style="max-width: 70px"/>
                            </a>
                            <small class="text-small">Moderador</small>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="pull-right">creado: <small class="label label-default"> ${ fecha} </small></span>
                        <h5><a href="view_foros.php?id=${ key.conference_id}" class="text-primary"> ${key.first_name + " " + key.last_name}</a> <span>escribio:</span></h5>
                        <div class="row">
                            <div class="col-md-9">
                                <h4>${ key.conference}</h4>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <small class="display-block text-muted">
                            <a href="aulavirtual.php?schedule=${ key.conference_id}" class="btn btn-success"><i class="fa fa-angle-double-right"></i> Ir </a>
                            ${ cargar_presentacion}
                        </small>
                    </div>
                </div>`;
        html += aux;
    });
    $('#listado_post').html(html);
}

function getForos() {
    $.ajax({
        url: "controllers/aulavirtual_admin.php",
        type: 'get',
        data: { opcn: 'listado', texto: texto, fecha: anio },
        dataType: "json",
    })
        .done(function (data) {
            if (!data.error) {
                foros = data.data;
                renderForos(foros, data.rol);
            }
        });
}
// funcion que valida si están vacios los campos de la modal para crear una conferencia
function valida() {
    var valid = 0;
    $('#conference').css("border-color", "#efefef");
    $('#conference_description').css("border-color", "#efefef");
    $('#position').css("border-color", "#efefef");

    var valid = 0;
    if ($('#conference').val() == "") {
        $('#conference').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#conference_description').val() == "") {
        $('#conference_description').css("border-color", "#b94a48");
        valid = 1;
    }

    if ($('#position').val() == "") {
        $('#position').css("border-color", "#b94a48");
        valid = 1;
    }

    if (valid == 0) {

        if (confirm('¿Esta seguro de ejecutar esta instrucción?')) {
            return true;
        } else {
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            setTimeout(function () { $(".notyfy_container").fadeOut(5000); }, 3000);
            return false;
        }
    } else {
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
    setTimeout(function () { $(".notyfy_container").fadeOut(5000); }, 3000);
}

function uploadFile(file, image, datos, opcn, url) {

    //Valida el tamaño del archivo
    if (file.size >= 1000000000) { //if_2
        notyfy({
            text: 'El archivo excede el tamaño máximo permitido (Tamaño permitido 100 Mb)',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return false;
    } //fin if

    if (image.size >= 5000000) { //if_2
        notyfy({
            text: 'La imagen excede el tamaño máximo permitido (Tamaño permitido 5 Mb)',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return false;
    } //fin if

    $('.btn').prop("disabled", true);

    // fin
    file = file == '' ? '' : file;
    image = image == '' ? '' : image;

    if (file + image != '') {
        notyfy({
            text: `Estamos procesando tu información, por favor espere <label id="status" class="control-label" ></label> <br>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4"><div class="progress progress-mini"><div class="progress-bar progress-bar-primary" id="progressBar" style="width: 0px; border-radius: 5px;"></div></div></div>
                    <div class="col-sm-4"></div>
                </div>`,
            type: 'information' // alert|error|success|information|warning|primary|confirm
        });
    }

    var formdata = new FormData();
    formdata.append("archivo", file);
    formdata.append("imagen", image);
    opcn == '' ? '' : formdata.append('opcn', opcn);
    for (var i = 0; i < datos.length; i++) {
        formdata.append(datos[i].name, datos[i].value);
    }
    var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", "controllers/" + url);
    ajax.send(formdata);
}

function Init() {
    $('#filtro_anio').select2({
        placeholder: "Seleccione un año de busqueda",
        allowClear: true,
        data: [{ id: '2015', text: '2015' }, { id: '2016', text: '2016' }, { id: '2017', text: '2017' }, { id: '2018', text: '2018' }]
    });

    $('#tipo_cargue').select2({
        placeholder: "Seleccione un año de busqueda",
        allowClear: true
    });

    $('#filtro_anio').change(function () {
        anio = $(this).val();
        getForos();
    });

    getForos();

    $("#category").select2({
        placeholder: "Seleccione la categoria",
        allowClear: true
    });
    //agrega el evento escucha para filtrar por texto las comunicaciones

    //formulario para crear una comunicacion

} //fin funcion Init

$('#filtro').keyup(function () {
    texto = $(this).val();
    if (texto.length == 0 || texto.length > 2) {
        getForos(texto);
    }
});

opcn = 'crear';
conference_id = '';
$('#form_Crear').submit(function (e) {
    e.preventDefault();
    if (valida()) {

        $( ".btn" ).prop( "disabled", true );
        var datos_form = $(this).serializeArray();
        datos_form.push({ name: "opcn", value: opcn });
        datos_form.push({ name: "conference_id", value: conference_id });

        //console.log(datos_form); return;

        $.ajax({
            url: "controllers/aulavirtual_admin.php",
            type: 'post',
            data: datos_form,
            dataType: "json",
        })
            .done(function (data) {
                $( ".btn" ).prop( "disabled", false );
                $('#form_Crear')[0].reset();
                if (!data.error) {
                    $('#ModalCrearNueva').modal('hide');
                    $('#BtnReset').trigger('click');
                    notyfy({
                        text: "Se ha creado correctamente la comunicación",
                        type: 'success' // alert|error|success|information|warning|primary|confirm
                    });
                    getForos();
                } else {
                    notyfy({
                        text: data.message,
                        type: 'warning' // alert|error|success|information|warning|primary|confirm
                    });
                }
            });
    } //fin if
});

$('#form_presentacion').submit(function (e) {
    e.preventDefault();
    NProgress.start();
    console.log($('#imagenes')[0].files);
    var datos = new FormData();
    var inputFileImage = document.getElementById("imagenes");
    var cantidad = inputFileImage.files.length;
    //for (var i = 0; i < cantidad; i++) {
    datos.append("imagen", inputFileImage.files[0]);
    //}
    datos.append('opcn', 'imagenes');
    datos.append('conference_id', $('#conference_id').val());
    datos.append('tipo_cargue', $('#tipo_cargue').val());
    $.ajax({
        url: 'controllers/aulavirtual_admin.php',
        dataType: 'json',
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    }).done(function (data) {
        NProgress.done();
        if (!data.error) {
            $('#ModalPresentacion').modal('hide');
            self.renderImagen(data.imagenes);
            if ($('#tipo').val() == 'instructor') {
                $('#lista_diapositivas').html(self.carreteImagenes(data.imagenes));
            }
            $('#resetFiles').trigger('click');
        }
    }).always(function () {
        NProgress.done();
    });
});

$('#frm_pregunta').submit(function (e) { 
    e.preventDefault();
    let  conference_id = $('#conference_id').val()
    let  pregunta = $('#pregunta').val().trim()
    let  orden = $('#orden').val()

    if(orden == ""){ alert('Campo orden vacío'); return false; }
    if(pregunta == ""){ alert('Campo pregunta vacío'); return false; }

    $.ajax({
        url: 'controllers/aulavirtual_admin.php?crear_pregunta',
        type: 'POST',
        dataType: 'JSON',
        data: { opcn:'crear_pregunta', pregunta:pregunta, conference_id:conference_id, orden: orden }
    })
    .done(function (data) {
        if(!data.error){
            notyfy({
                text: data.msj,
                type: 'success' // alert|error|success|information|warning|primary|confirm
            });
            $('#pregunta').val('')
            $('#orden').val('')
            $('#tabla_respuestas').html('');
            preguntas(data.conference_id)
        }else{
            notyfy({
                text: data.msj,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    })
    .fail(function () {
        console.log('error');
    });
});

$('#frm_respuesta').submit(function (e) { 
    e.preventDefault();
    let  conference_id = $('#conference_id').val()
    //let  pregunta = $('#pregunta').val()
    let  respuesta = $('#respuesta').val()

    if(conference_id == ""){ alert('conference_id, Vacío'); return false; }
    if(question_id == ""){ alert('Por favor escoja la pregunta a la que se le asignará la respuesta'); return false; }
    if(respuesta == ""){ alert('Campo respuesta vacío'); return false; }
    
    $.ajax({
        url: 'controllers/aulavirtual_admin.php',
        type: 'POST',
        dataType: 'JSON',
        data: { opcn:'crear_respuesta', respuesta:respuesta, question_id:question_id, conference_id:conference_id }
    })
    .done(function (data) {
        if(!data.error){
            $('#respuesta').val('')
            notyfy({
                text: data.msj,
                type: 'success' // alert|error|success|information|warning|primary|confirm
            });
            ver_respuestas(data.question_id)
        }else{
            notyfy({
                text: data.msj,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    })
    .fail(function () {
        console.log('error');
    });
});

function eliminar_pregunta(course_id, coursefile_id, question_id) {
    $.ajax({
        url: 'controllers/aulavirtual_admin.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn:'eliminar_pregunta', coursefile_id:coursefile_id, question_id:question_id, course_id:course_id}
    })
    .done(function (data) {
        console.log(data)

        preguntas($('#conference_id').val())
    })
    .fail(function () {
        console.log('error');
    });
}