$(function () {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/programaciones.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[2, "desc"]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate": {
                "sNext": "Siguiente",
                "sPrevious": "Anterior",
                "sFirst": "Inicio",
                "sLast": "Fin"
            }
        }
    });
});

$('.js-data-example-ajax').select2({
    ajax: {
        url: "controllers/aulavirtual_admin.php",
        dataType: 'json',
        type: 'POST'
        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
    }
});

$(document).ready(function () {
    Consulta_DataInicial();
    $('#start_date_day').change(function () {
        $('#end_date_day').val($('#start_date_day').val());
    });
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#start_date_time').timepicker({
        minuteStep: 15,
        template: 'modal',
        showSeconds: true,
        showMeridian: false,
        modalBackdrop: true,
        defaultTime: '08:00:00',
        showInputs: false
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_time').timepicker({
        minuteStep: 15,
        template: 'modal',
        showSeconds: true,
        showMeridian: false,
        modalBackdrop: true,
        defaultTime: '17:00:00',
        showInputs: false
    });
    $('#max_inscription_date').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#living_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#module_id").select2({
        placeholder: "Seleccione un módulo",
        allowClear: true
    });
    $("#course_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#teacher_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#start_date_search').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_search').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $("#status_id_search").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#course_id_search").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#teacher_id_search").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#living_id_search").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});

function CambioProg(campo) {
    var CampoAct = $('#Cambio_Prog').val() + ', ' + campo;
    $('#Cambio_Prog').val(CampoAct);
    event.preventDefault();
}

$("#formulario_data").submit(function (event) {
    if (valida()) {
        if ($('#opcn').val() == "crear") {
            Operacion('Crear');
        } else if ($('#opcn').val() == "editar") {
            Operacion('Actualizar');
        } else {
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
    event.preventDefault();
});

$("#frmToken").submit(function (event) {

    var session_id = $("#session_id").val();
    var token = $("#token").val();
    var opcn = "asignar_token";
    $.ajax({
        type: 'post',
        url: 'controllers/programaciones.php',
        dataType: 'json',
        data: { opcn: opcn, session_id: session_id, token: token, schedule_id: schedule_id }
    }).done(function (data) {
        notyfy({
            text: data.msj,
            type: data.type // alert|error|success|information|warning|primary|confirm
        });
    })
        .fail(function () {
            console.log('error');
        })
    event.preventDefault();
});

$("#retornaElemento").click(function () {
    window.location = "programaciones.php";
});

function valida() {
    var valid = 0;
    $('#start_date_day').css("border-color", "#efefef");
    $('#start_date_time').css("border-color", "#efefef");
    $('#end_date_day').css("border-color", "#efefef");
    $('#end_date_time').css("border-color", "#efefef");
    $('#max_inscription_date').css("border-color", "#efefef");
    $('#min_size').css("border-color", "#efefef");
    $('#max_size').css("border-color", "#efefef");
    $('#waiting_size').css("border-color", "#efefef");
    var valid = 0;
    if ($('#start_date_day').val() == "") {
        $('#start_date_day').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#start_date_time').val() == "") {
        $('#start_date_time').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#end_date_day').val() == "") {
        $('#end_date_day').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#end_date_time').val() == "") {
        $('#end_date_time').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#max_inscription_date').val() == "") {
        $('#max_inscription_date').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#min_size').val() == "") {
        $('#min_size').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#max_size').val() == "") {
        $('#max_size').css("border-color", "#b94a48");
        valid = 1;
    }
    if ($('#waiting_size').val() == "") {
        $('#waiting_size').css("border-color", "#b94a48");
        valid = 1;
    }
    if (valid == 0) {
        if (confirm('¿Esta seguro de ejecutar esta instrucción?')) {
            return true;
        } else {
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    } else {
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg) {
    $.ajax({
        url: 'controllers/programaciones.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function (data) {
            var res0 = data.resultado;
            if (res0 == "SI") {
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            } else if (!res0) {

                notyfy({
                    text: data.msj,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            } else {
                notyfy({
                    text: 'No se ha logrado ' + msg + ' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
        }
    }
    });
}

function Consulta_DataInicial() {
    if ($('#living_id').length) {

        //Verifica si ya existe la programación
        var Schedule_id_sel = "0";
        if ($('#idElemento').length) {
            Schedule_id_sel = $('#idElemento').val();
        }
        //Verifica si ya existe la programación
        //Busca el número máximo de participantes por living
        var data = new FormData();
        data.append('living_id', $('#living_id').val());
        data.append('course_id', $('#course_id').val());
        data.append('opcn', 'Cupos');
        var url = "controllers/programaciones.php";
        $.ajax({
            url: url,
            type: 'post',
            contentType: false,
            data: data,
            processData: false,
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data.resultado == "SI") {
                    if ($('#max_size').val() == "0") {
                        $('#max_size').val(data.CuposMax);
                        $('#min_size').val(data.CuposMin);
                        $('#waiting_size').val(data.CuposWait);
                    }
                    $('#city_id').val(data.city_id);
                    consulta_Modulos();
                    //ConsultaDisponibilidad($('#living_id').val(), $('#course_id').val(), $('#max_size').val(), $('#min_size').val(), data.city_id, Schedule_id_sel);
                }
            }
        });
        //Busca el número máximo de participantes por living

    }
}

function consulta_Modulos() {
    var Schedule_id_sel = "0";
    if ($('#idElemento').length) {
        Schedule_id_sel = $('#idElemento').val();
    }

    var data = new FormData();
    data.append('living_id', $('#living_id').val());
    data.append('course_id', $('#course_id').val());
    data.append('opcn', 'Cupos');
    var url = "controllers/programaciones.php";
    $.ajax({
        url: url,
        type: 'post',
        contentType: false,
        data: data,
        processData: false,
        dataType: "json",
        cache: false,
        success: function (data) {
            if (data.resultado == "SI") {
                $('#module_id').select2('destroy');
                var sel_mo = ``;
                data.modulos.forEach(function (e, i) {
                    sel_mo += `<option value="` + e.module_id + `" ` + e.selected + `>` + e.module + `</option>`
                });
                $('#module_id').html(sel_mo);
                //$("#module_id").select2("val", "");
                $("#module_id").select2({
                    placeholder: "Seleccione un módulo",
                    allowClear: true
                });
                $('#detProgramacion').html("");
                ConsultaDisponibilidad($('#living_id').val(), $('#course_id').val(), $('#max_size').val(), $('#min_size').val(), $('#city_id').val(), Schedule_id_sel, $('#module_id').val());
            }
        }
    });
}

function Consulta_DataCambio(mensaje) {
    if ($('#living_id').length) {
        CambioProg(mensaje);
        $('#loading_programacion').removeClass('hide');
        //Verifica si ya existe la programación
        var Schedule_id_sel = "0";
        if ($('#idElemento').length) {
            Schedule_id_sel = $('#idElemento').val();
        }
        //Verifica si ya existe la programación
        //Busca el número máximo de participantes por living
        var data = new FormData();
        data.append('living_id', $('#living_id').val());
        data.append('course_id', $('#course_id').val());
        data.append('opcn', 'Cupos');
        var url = "controllers/programaciones.php";
        $.ajax({
            url: url,
            type: 'post',
            contentType: false,
            data: data,
            processData: false,
            dataType: "json",
            cache: false,
            success: function (data) {
                if (data.resultado == "SI") {
                    $('#max_size').val(data.CuposMax);
                    $('#min_size').val(data.CuposMin);
                    $('#waiting_size').val(data.CuposWait);
                    $('#city_id').val(data.city_id);
                    ConsultaDisponibilidad($('#living_id').val(), $('#course_id').val(), data.CuposMax, data.CuposMin, data.city_id, Schedule_id_sel, $('#module_id').val());
                    console.log(data.modulos)

                    //var sel_mo = `<option value=""></option>`;
                    //data.modulos.forEach(function (e, i) {
                    //sel_mo += `<option value="` + e.module_id + `">` + e.module + `</option>`
                    //});
                    //$('#module_id').html(sel_mo);
                }
            }
        });

        $('#loading_programacion').addClass('hide');
        //Busca el número máximo de participantes por living
    }
    event.preventDefault();
}

function Consulta_CambioCupos(mensaje) {
    $('#loading_programacion').removeClass('hide');
    CambioProg(mensaje);
    //Verifica si ya existe la programación
    var Schedule_id_sel = "0";
    if ($('#idElemento').length) {
        Schedule_id_sel = $('#idElemento').val();
    }
    //Verifica si ya existe la programación
    if ($('#living_id').length) {
        ConsultaDisponibilidad($('#living_id').val(), $('#course_id').val(), $('#max_size').val(), $('#min_size').val(), $('#city_id').val(), Schedule_id_sel, $('#module_id').val());
    }
    $('#loading_programacion').addClass('hide');
}

function ConsultaDisponibilidad(living_id, course_id, max_size, min_size, city_id, Schedule_id_sel, module_id) {
    $('#detProgramacion').load('det_programacion.php', { living_id: living_id, course_id: course_id, max_size: max_size, min_size: min_size, city_id: city_id, Schedule_id_sel: Schedule_id_sel, module_id: module_id }, function (response, status, xhr) {
        if (status == "error") {
            var msg = "Ha ocurrido un error: ";
            notyfy({
                text: msg + xhr.status + " " + xhr.statusText,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        } else {
            EstableceTitulos();
        }
    });
}

function EstableceTitulos() {
    // By suppling no content attribute, the library uses each elements title attribute by default
    $('a[href][title]').qtip({
        content: {
            text: false // Use each elements title attribute
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background: '#A2D959',
            name: 'green'
        } // Give it some style
    });
    // NOTE: You can even omit all options and simply replace the regular title tooltips like so:
    // $('#content a[href]').qtip();
}

// juan Carlos Villar
schedule_id = '';
$("body").on('click', '.actividades', function () {
    var module_id = $(this).data('module_id');
    var schedule_id = $(this).data('schedule_id');
    $('#tabla_actividades tbody').html('');
    $.ajax({
        url: 'controllers/programaciones.php',
        type: 'POST',
        dataType: 'JSON',
        data: { opcn: 'getModulesActivities', module_id: module_id, schedule_id: schedule_id }
    })
        .done(function (data) {

            tabla = '';
            data.forEach(function (e, i) {
                tabla += `<tr data-activity_schedule_id='` + e.activity_schedule_id + `'  data-schedule_id='` + e.schedule_id + `' data-activity_id=` + e.activity_id + ` class='fila'>
                            <td>`+ e.activity + `</td>
                            <td><input class="inicio fecha form-control" type="text" value="`+ e.desde + `"/></td>
                            <td><input class="fin fecha form-control" type="text" value="`+ e.hasta + `"/></td>
                        </tr>`
            });
            if (tabla == '') { tabla = 'Módulo sin actividades' }else{
                $('#tabla_actividades tbody').html(tabla);
                $('.fecha').bdatepicker({
                    format: "yyyy-mm-dd",
                    startDate: "2015-01-01"
                });
            }
        })
        .fail(function () {
            console.log('error');
        });
});

$("body").on('click', '.tokens', function () {
    schedule_id = $(this).data('schedule_id');
    $.ajax({
        url: 'controllers/programaciones.php',
        type: 'POST',
        dataType: 'JSON',
        data: { opcn: 'getToken', schedule_id: schedule_id }
    })
        .done(function (data) {
            $("#session_id").val(data.session_id);
            $("#token").val(data.token);
        })
        .fail(function () {
            console.log('error');
        });
});

$('#btn_fech_act').click(function (e) {
    e.preventDefault();
    var datos = [];
    var valida = true;
    $(".fila").each(function (i) {
        if ($(this).find('.inicio').val().trim() == '') { valida = false; $(this).find('.inicio').css('border', '1px solid red'); }
        if ($(this).find('.fin').val().trim() == '') { valida = false; $(this).find('.fin').css('border', '1px solid red'); }
        var activity_id = $(this).data('activity_id');
        var activity_schedule_id = $(this).data('activity_schedule_id');
        var schedule_id = $(this).data('schedule_id');
        datos.push({ desde: $(this).find('.inicio').val(), hasta: $(this).find('.fin').val(), activity_id: activity_id, activity_schedule_id: activity_schedule_id, schedule_id: schedule_id })
    });

    //console.log(valida)
    // console.log(datos)
    
    if (!valida) { alert('Por favor rellene los campos en rojo'); return false; }
    $(".btn").prop("disabled", true);
    $.ajax({
        url: 'controllers/programaciones.php',
        type: 'POST',
        dataType: 'JSON',
        data: { opcn: 'programar_actividades', datos: datos }
    })
        .done(function (data) {
            if (!data.error) {
                location.reload()
            } else {
                $(".btn").prop("disabled", false);
            }
        })
        .fail(function () {
            console.log('error');
        });
});