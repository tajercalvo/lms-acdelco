/*var Opcn1 = false;
var Opcn2 = false;
var Opcn3 = false;
var Opcn4 = false;
var Opcn5 = false;
var Opcn6 = false;

$(document).ready(function(){
    $('#PanelPreguntas').hide();
    $('#PanelBotones').hide();

    $('#LI_Sel1').hide();
    $('#LI_Sel2').hide();
    $('#LI_Sel3').hide();
    $('#LI_Sel4').hide();
    $('#LI_Sel5').hide();
    $('#LI_Sel6').hide();

    $('#tab1-1').hide();
    $('#tab2-1').hide();
    $('#tab3-1').hide();
    $('#tab4-1').hide();
    $('#tab5-1').hide();
    $('#tab6-1').hide();

    $('#AVN').change(function() {
        console.log("clic");
        if($('#AVN').is(':checked')){
            $('#LI_Sel1').show();
            $('#tab1-1').show();
            Opcn1 = true;
            $('#PanelPreguntas').show("fast",function(){
                $('#PanelBotones').show();
                $('#LI_Sel1').addClass(function( index, currentClass ){
                    $('#LI_Sel2').removeClass("active");
                    $('#tab2-1').removeClass("active");
                    $('#LI_Sel3').removeClass("active");
                    $('#tab3-1').removeClass("active");
                    $('#LI_Sel4').removeClass("active");
                    $('#tab4-1').removeClass("active");
                    $('#LI_Sel5').removeClass("active");
                    $('#tab5-1').removeClass("active");
                    $('#LI_Sel6').removeClass("active");
                    $('#tab6-1').removeClass("active");

                    $('#tab1-1').addClass("active");
                    return "active";
                });
            });
        }else{
            $('#LI_Sel1').hide();
            $('#tab1-1').hide();
            Opcn1 = false;
            $('#LI_Sel1').removeClass("active");
            $('#tab1-1').removeClass("active");
            ConfirmaPanel();
        }
    });
    $('#ACC').change(function() {
        if($('#ACC').is(':checked')){
            $('#LI_Sel2').show();
            $('#tab2-1').show();
            Opcn2 = true;
            $('#PanelPreguntas').show("fast",function(){
                $('#PanelBotones').show();
                $('#LI_Sel2').addClass(function( index, currentClass ){
                    $('#LI_Sel1').removeClass("active");
                    $('#tab1-1').removeClass("active");
                    $('#LI_Sel3').removeClass("active");
                    $('#tab3-1').removeClass("active");
                    $('#LI_Sel4').removeClass("active");
                    $('#tab4-1').removeClass("active");
                    $('#LI_Sel5').removeClass("active");
                    $('#tab5-1').removeClass("active");
                    $('#LI_Sel6').removeClass("active");
                    $('#tab6-1').removeClass("active");
                    
                    $('#tab2-1').addClass("active");
                    return "active";
                });
            });
        }else{
            $('#LI_Sel2').hide();
            $('#tab2-1').hide();
            Opcn2 = false;
            $('#LI_Sel2').removeClass("active");
            $('#tab2-1').removeClass("active");
            ConfirmaPanel();
        }
    });
    $('#CID').change(function() {
        if($('#CID').is(':checked')){
            $('#LI_Sel3').show();
            $('#tab3-1').show();
            Opcn3 = true;
            $('#PanelPreguntas').show("fast",function(){
                $('#PanelBotones').show();
                $('#LI_Sel3').addClass(function( index, currentClass ){
                    $('#LI_Sel2').removeClass("active");
                    $('#tab2-1').removeClass("active");
                    $('#LI_Sel1').removeClass("active");
                    $('#tab1-1').removeClass("active");
                    $('#LI_Sel4').removeClass("active");
                    $('#tab4-1').removeClass("active");
                    $('#LI_Sel5').removeClass("active");
                    $('#tab5-1').removeClass("active");
                    $('#LI_Sel6').removeClass("active");
                    $('#tab6-1').removeClass("active");
                    
                    $('#tab3-1').addClass("active");
                    return "active";
                });
            });
        }else{
            $('#LI_Sel3').hide();
            $('#tab3-1').hide();
            Opcn3 = false;
            $('#LI_Sel3').removeClass("active");
            $('#tab3-1').removeClass("active");
            ConfirmaPanel();
        }
    });
    $('#ARA').change(function() {
        if($('#ARA').is(':checked')){
            $('#LI_Sel4').show();
            $('#tab4-1').show();
            Opcn4 = true;
            $('#PanelPreguntas').show("fast",function(){
                $('#PanelBotones').show();
                $('#LI_Sel4').addClass(function( index, currentClass ){
                    $('#LI_Sel2').removeClass("active");
                    $('#tab2-1').removeClass("active");
                    $('#LI_Sel3').removeClass("active");
                    $('#tab3-1').removeClass("active");
                    $('#LI_Sel1').removeClass("active");
                    $('#tab1-1').removeClass("active");
                    $('#LI_Sel5').removeClass("active");
                    $('#tab5-1').removeClass("active");
                    $('#LI_Sel6').removeClass("active");
                    $('#tab6-1').removeClass("active");
                    
                    $('#tab4-1').addClass("active");
                    return "active";
                });
            });
        }else{
            $('#LI_Sel4').hide();
            $('#tab4-1').hide();
            Opcn4 = false;
            $('#LI_Sel4').removeClass("active");
            $('#tab4-1').removeClass("active");
            ConfirmaPanel();
        }
    });
    $('#TIG').change(function() {
        if($('#TIG').is(':checked')){
            $('#LI_Sel5').show();
            $('#tab5-1').show();
            Opcn5 = true;
            $('#PanelPreguntas').show("fast",function(){
                $('#PanelBotones').show();
                $('#LI_Sel5').addClass(function( index, currentClass ){
                    $('#LI_Sel2').removeClass("active");
                    $('#tab2-1').removeClass("active");
                    $('#LI_Sel3').removeClass("active");
                    $('#tab3-1').removeClass("active");
                    $('#LI_Sel4').removeClass("active");
                    $('#tab4-1').removeClass("active");
                    $('#LI_Sel1').removeClass("active");
                    $('#tab1-1').removeClass("active");
                    $('#LI_Sel6').removeClass("active");
                    $('#tab6-1').removeClass("active");
                    
                    $('#tab5-1').addClass("active");
                    return "active";
                });
            });
        }else{
            $('#LI_Sel5').hide();
            $('#tab5-1').hide();
            Opcn5 = false;
            $('#LI_Sel5').removeClass("active");
            $('#tab5-1').removeClass("active");
            ConfirmaPanel();
        }
    });
    $('#TID').change(function() {
        if($('#TID').is(':checked')){
            $('#LI_Sel6').show();
            $('#tab6-1').show();
            Opcn6 = true;
            $('#PanelPreguntas').show("fast",function(){
                $('#PanelBotones').show();
                $('#LI_Sel6').addClass(function( index, currentClass ){
                    $('#LI_Sel2').removeClass("active");
                    $('#tab2-1').removeClass("active");
                    $('#LI_Sel3').removeClass("active");
                    $('#tab3-1').removeClass("active");
                    $('#LI_Sel4').removeClass("active");
                    $('#tab4-1').removeClass("active");
                    $('#LI_Sel5').removeClass("active");
                    $('#tab5-1').removeClass("active");
                    $('#LI_Sel1').removeClass("active");
                    $('#tab1-1').removeClass("active");
                    
                    $('#tab6-1').addClass("active");
                    return "active";
                });
            });
        }else{
            $('#LI_Sel6').hide();
            $('#tab6-1').hide();
            Opcn6 = false;
            $('#LI_Sel6').removeClass("active");
            $('#tab6-1').removeClass("active");
            ConfirmaPanel();
        }
    });
});

function ConfirmaPanel(){
    if(Opcn1||Opcn2||Opcn3||Opcn4||Opcn5||Opcn6){
        $('#PanelPreguntas').show();
        $('#PanelBotones').show();
        if(Opcn1){
            $('#LI_Sel1').addClass("active");
            $('#tab1-1').addClass("active");
        }else if(Opcn2){
            $('#LI_Sel2').addClass("active");
            $('#tab2-1').addClass("active");
        }else if(Opcn3){
            $('#LI_Sel3').addClass("active");
            $('#tab3-1').addClass("active");
        }else if(Opcn4){
            $('#LI_Sel4').addClass("active");
            $('#tab4-1').addClass("active");
        }else if(Opcn5){
            $('#LI_Sel5').addClass("active");
            $('#tab5-1').addClass("active");
        }else if(Opcn6){
            $('#LI_Sel6').addClass("active");
            $('#tab6-1').addClass("active");
        }
    }else{
        $('#PanelPreguntas').hide();
        $('#PanelBotones').hide();
        $('#LI_Sel1').removeClass("active");
        $('#tab1-1').removeClass("active");
        $('#LI_Sel2').removeClass("active");
        $('#tab2-1').removeClass("active");
        $('#LI_Sel3').removeClass("active");
        $('#tab3-1').removeClass("active");
        $('#LI_Sel4').removeClass("active");
        $('#tab4-1').removeClass("active");
        $('#LI_Sel5').removeClass("active");
        $('#tab5-1').removeClass("active");
        $('#LI_Sel6').removeClass("active");
        $('#tab6-1').removeClass("active");
    }
}*/

$(document).ready(function(){
    $( "#FormularioParte1" ).submit(function( event ) {
        if( !$('#AVN').is(':checked') && !$('#ACC').is(':checked') && !$('#CID').is(':checked') && !$('#ARA').is(':checked') && !$('#TIG').is(':checked') && !$('#TID').is(':checked') ){
            notyfy({
                text: 'Debe selecionar al menos un cargo',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }else{
            if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
                return true;
            }else{
                notyfy({
                    text: 'Ha cancelado la operación, puede dar clic en Siguiente en cualquier momento nuevamente',
                    type: 'information' // alert|error|success|information|warning|primary|confirm
                });
                return false;
            }
        }
        event.preventDefault();
    });

    $( "#FormularioParte2" ).submit(function( event ) {
        var _val_trayectoria = "";
        var_Completo = 0;
        if ($('#AVN').is(':checked')){
            var ctr_1 = 0;
            if($('#AVN1').is(':checked')){ ctr_1++; }
            if($('#AVN2').is(':checked')){ ctr_1++; }
            if($('#AVN3').is(':checked')){ ctr_1++; }
            if($('#AVN4').is(':checked')){ ctr_1++; }
            if($('#AVN5').is(':checked')){ ctr_1++; }
            if($('#AVN6').is(':checked')){ ctr_1++; }
            if(ctr_1<3){
                var_Completo = 1;
                _val_trayectoria = "Asesor V. Nuevos y Selective Channel";
            }
        }
        if ($('#ACC').is(':checked')){
            var ctr_1 = 0;
            if($('#ACC1').is(':checked')){ ctr_1++; }
            if($('#ACC2').is(':checked')){ ctr_1++; }
            if($('#ACC3').is(':checked')){ ctr_1++; }
            if($('#ACC4').is(':checked')){ ctr_1++; }
            if($('#ACC5').is(':checked')){ ctr_1++; }
            if(ctr_1<3){
                var_Completo = 1;
                _val_trayectoria = "Agente Call Center";
            }
        }
        if ($('#CID').is(':checked')){
            var ctr_1 = 0;
            if($('#CID1').is(':checked')){ ctr_1++; }
            if($('#CID2').is(':checked')){ ctr_1++; }
            if($('#CID3').is(':checked')){ ctr_1++; }
            if($('#CID4').is(':checked')){ ctr_1++; }
            if($('#CID5').is(':checked')){ ctr_1++; }
            if($('#CID6').is(':checked')){ ctr_1++; }
            if(ctr_1<3){
                var_Completo = 1;
                _val_trayectoria = "Consultor Integral Diésel (CID)";
            }
            var ctr_2 = 0;
            if($('#CID7').is(':checked')){ ctr_2++; }
            if($('#CID8').is(':checked')){ ctr_2++; }
            if($('#CID9').is(':checked')){ ctr_2++; }
            if(ctr_2<1){
                var_Completo = 1;
                _val_trayectoria = "Consultor Integral Diésel (CID)";
            }
        }
        if ($('#ARA').is(':checked')){
            var ctr_1 = 0;
            if($('#ARA1').is(':checked')){ ctr_1++; }
            if($('#ARA2').is(':checked')){ ctr_1++; }
            if($('#ARA3').is(':checked')){ ctr_1++; }
            if($('#ARA4').is(':checked')){ ctr_1++; }
            if($('#ARA5').is(':checked')){ ctr_1++; }
            if(ctr_1<3){
                var_Completo = 1;
                _val_trayectoria = "Asesor de Repuestos y accesorios";
            }
        }
        if ($('#TIG').is(':checked')){
            var ctr_1 = 0;
            if($('#TIG1').is(':checked')){ ctr_1++; }
            if($('#TIG2').is(':checked')){ ctr_1++; }
            if($('#TIG3').is(':checked')){ ctr_1++; }
            if($('#TIG4').is(':checked')){ ctr_1++; }
            if($('#TIG5').is(':checked')){ ctr_1++; }
            if(ctr_1<3){
                var_Completo = 1;
                _val_trayectoria = "Técnico Integral Gasolina (TIG)";
            }
            var ctr_2 = 0;
            if($('#TIG7').is(':checked')){ ctr_2++; }
            if($('#TIG8').is(':checked')){ ctr_2++; }
            if($('#TIG9').is(':checked')){ ctr_2++; }
            if($('#TIG10').is(':checked')){ ctr_2++; }
            if($('#TIG11').is(':checked')){ ctr_2++; }
            if($('#TIG12').is(':checked')){ ctr_2++; }
            if(ctr_2<1){
                var_Completo = 1;
                _val_trayectoria = "Técnico Integral Gasolina (TIG)";
            }
        }
        if ($('#TID').is(':checked')){
            var ctr_1 = 0;
            if($('#TID1').is(':checked')){ ctr_1++; }
            if($('#TID2').is(':checked')){ ctr_1++; }
            if($('#TID3').is(':checked')){ ctr_1++; }
            if($('#TID4').is(':checked')){ ctr_1++; }
            if($('#TID5').is(':checked')){ ctr_1++; }
            if(ctr_1<3){
                var_Completo = 1;
                _val_trayectoria = "Técnico Integral Diésel (TID)";
            }
            var ctr_2 = 0;
            if($('#TID7').is(':checked')){ ctr_2++; }
            if($('#TID8').is(':checked')){ ctr_2++; }
            if($('#TID9').is(':checked')){ ctr_2++; }
            if($('#TID10').is(':checked')){ ctr_2++; }
            if($('#TID11').is(':checked')){ ctr_2++; }
            if(ctr_2<1){
                var_Completo = 1;
                _val_trayectoria = "Técnico Integral Diésel (TID)";
            }
        }
        if(var_Completo == 0){
            if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
                return true;
            }else{
                notyfy({
                    text: 'Ha cancelado la operación, puede dar clic en Siguiente en cualquier momento nuevamente',
                    type: 'information' // alert|error|success|information|warning|primary|confirm
                });
                return false;
            }
        }else{
            notyfy({
                    text: 'Debe seleccionar las respuestas de acuerdo a lo indicado, la información se encuentra incompleta; revise la pestaña de la trayectoria:'+_val_trayectoria,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
                return false;
        }
        
        event.preventDefault();
    });
});