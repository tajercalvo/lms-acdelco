$(document).ready(function(){

   $("#zone").change(function () {

            $('#consultando').html('<span class="label label-success">Realizando consulta, por favor espere...</span><img style="width: 15px; " src="../assets/loading-course.gif">');

           $("#zone option:selected").each(function () {
            //captura el id del value del select 2 que se va a enviar
            var zona=$(this).val();
            $.post("controllers/rep_indnavegacion.php", { zona: zona, opcn:"opcn" }, function(data){
            $("#dealer").html(data);
            $('#consultando').html("");

            });
        });

   });

   $("#dealer").change(function () {
            $('#consultando').html('<span class="label label-success">Realizando consulta, por favor espere...</span><img style="width: 15px; " src="../assets/loading-course.gif">');
           $("#dealer option:selected").each(function () {
            //captura el id del value del select 2 que se va a enviar
            var dealer=$(this).val();
            $.post("controllers/rep_indnavegacion.php", { dealer: dealer, opcn:"opcn" }, function(data){
            $("#headquarters").html(data);
            $('#consultando').html('');
            });
        });
   });

   $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
  
      $("#zone").select2({
        placeholder: "Seleccione una zona",
        allowClear: true
    });
      $("#dealer").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
      $("#headquarters").select2({
        placeholder: "Seleccione una sede",
        allowClear: true
    });

   setTimeout(function(){
    // $("#mibody").css("visibility","visible");
            $("#mibody").fadeIn();
    $("#load").html("");
    }, 2000);
});

// $("#descargar_excel").click(function(event) {

//        $("#datos_a_enviar").val( $("<div>").append( $("#tab_Navegacion").eq(0).clone()).html());
//         $("#FormularioExportacion").submit();
//          //console.log( $("#datos_a_enviar").val());
//     });

// $("#descargar_TODOS").click(function(event) {
//        $("#datos_a_enviar_tab_TODOS").val( $("<div>").append( $("#tab_TODOS").eq(0).clone()).html());
//         $("#FormularioExportacion_TODOS").submit();
//          //console.log( $("#datos_a_enviar").val());
//     });

// $("#descargar_excel_tab_GMAcademy").click(function(event) {

//        $("#datos_a_enviar_tab_GMAcademy").val( $("<div>").append( $("#tab_GMAcademy").eq(0).clone()).html());
//         $("#FormularioExportacion_tab_GMAcademy").submit();
//         // console.log( $("#datos_a_enviar").val());
//     });


$( "#btn_consulta" ).click(function() {


    if (validacion()) {
             $("#consultando").html('');// COnsulta sin resultado
            $("#contenido_tablas").css("visibility","hidden");
            $("#btn_consulta").html('<img style="width: 12px; " src="../assets/loading-course.gif"> Consultar ');
            $( "#btn_consulta" ).prop( "disabled", true );
            $('#ContenidoTabla').html('');
            // $('#consultando').html('<span class="label label-success">Consultando...</span><img style="width: 15px; " src="../assets/loading-course.gif">');

            var data = new FormData();
            data.append('opcn','opcn');
            //data.append('idconsulta',idconsulta);
            data.append('start_date_day',$("#start_date_day").val());
            data.append('end_date_day',$("#end_date_day").val());
            data.append('Zona',$("#zone").val());
            data.append('Concesionario',$("#dealer").val());
            data.append('Sede',$("#headquarters").val());
            data.append('Cargo',$("#charge").val());
            $.ajax({
                url:"controllers/rep_intensidad_horaria.php",
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                cache:false,
                success: function(data) {
                      var cantidadaDatos = data.length;
                      $("#btn_consulta").html('<i class="fa fa-check-circle"></i> Consultar');
                      $( "#btn_consulta" ).prop( "disabled", false );
                      if (cantidadaDatos == 0) {
                        $("#consultando").html('<span class="label label-warning">Consulta sin resultados</span>');
                        return false;
                      }else{
                          $("#contenido_tablas").css("visibility","visible");
                          var tabla='';
                          tabla += '<div class="form-inline separator bottom small">Total de inscritos: '+cantidadaDatos+'</div>';
                          tabla += '<table  class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable ui-sortable">';
                          tabla += '<thead>';
                          tabla += '<tr>';
                          tabla += '<th data-hide="phone,tablet">CONCESIONARIO</th>';
                          tabla += '<th data-hide="phone,tablet">SEDE</th>';
                          tabla += '<th data-hide="phone,tablet">CANTIDAD CURSOS</th>';
                          tabla += '<th data-hide="phone,tablet">ASISTENTES</th>';
                          tabla += '<th data-hide="phone,tablet">HORAS DE IMPARTICIÓN</th>';
                          tabla += '<th data-hide="phone,tablet">CUPOS</th>';
                          tabla += '</tr>';
                          tabla += '</thead>';
                          tabla += '<tbody>';
                          for (var i = 0; i < data.length; i++) {
                          tabla += '<tr>';
                          tabla +='<td>'+data[i]['concecionairo']+'</td>';
                          tabla += '<td>'+data[i]['sede']+'</td>';
                          tabla += '<td>pendiente</td>';
                          tabla += '<td>'+data[i]['asistentes']+'</td>';
                          tabla += '<td>'+data[i]['Horas_de_imparticon']+'</td>';
                          tabla += '<td>'+data[i]['invitados']+'</td>';
                          tabla += '</tr>';
                          }
                          tabla += '</tbody>';
                          tabla += '</table>';
                          $('#ContenidoTabla').html(tabla);
                      }
                } // Fin function data
            })// Fin ajax
            .fail(function() {
                $( "#btn_consulta" ).prop( "disabled", false );
                // $("#consultando").html('<span class="label label-danger">Falló la conexión con el servidor, verifica que tengas conexión a internet e intenta nuevamente</span>');
                 document.getElementById("Atencion").innerHTML = '<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>';
                 setTimeout(function() {

                    $(".gritter-item-wrapper").fadeOut(5000); 

              },3000);

              });
    }// Fin si valida

});
// Fin funcion click

function validacion() {

    s_date = document.getElementById("start_date_day").value;
    e_date = document.getElementById("end_date_day").value;

    $('#start_date_day').css( "border-color", "#ffffff" );
    $('#end_date_day').css( "border-color", "#ffffff" );
    //$('#identificacion').css( "border-color", "#ffffff" );

     inicio= new Date($('#start_date_day').val());
        finalq= new Date($('#end_date_day').val());
        if(inicio>finalq){

                    bootbox.alert("La fecha inicial no puede ser mayor que la fecha final", function(result)
                {

                });
                    return false;
        }


    if( s_date == '' & e_date == '') {
        $('#start_date_day').css( "border-color", "#b94a48" );
        $('#end_date_day').css( "border-color", "#b94a48");
        //$('#identificacion').css( "border-color", "#b94a48" );
        $( "#start_date_day" ).focus();
    return false;
    } else if( e_date == '') {
        $('#end_date_day').css( "border-color", "#b94a48" );
        $( "#end_date_day" ).focus();
        return false;
    } else if( s_date == '') {
        // alert('Complete la informacion antes de consultarla');
        $('#start_date_day').css( "border-color", "#b94a48" );
        //$('#identificacion').css( "border-color", "#b94a48" );
        $( "#start_date_day" ).focus();
        return false;
    }else{
        return true;
    }

}//fin funcion validacion
