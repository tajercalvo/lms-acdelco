var stack = 0, bars = false, lines = true, steps = false;
var Data_1          = [];
var DataNames_Mes   = [];
DataNames_Mes.push([0, 'Ene']);
DataNames_Mes.push([1, 'Feb']);
DataNames_Mes.push([2, 'Mar']);
DataNames_Mes.push([3, 'Abr']);
DataNames_Mes.push([4, 'May']);
DataNames_Mes.push([5, 'Jun']);
DataNames_Mes.push([6, 'Jul']);
DataNames_Mes.push([7, 'Ago']);
DataNames_Mes.push([8, 'Sep']);
DataNames_Mes.push([9, 'Oct']);
DataNames_Mes.push([10, 'Nov']);
DataNames_Mes.push([11, 'Dic']);


(function($)
{
    $('#ano_id_end').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#zone_id').select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $('#dealer_id').select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $('#model').select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#course_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    setTimeout(function(){
            CargarDatos('Graf1','char_1');
            CargarDatos('Graf2','char_2');
            CargarDatos('Graf3','char_3');
            CargarDatos('Graf4','char_4');
            CargarDatos('Graf5','char_5');
        }, 100);
})(jQuery);

function CambiarEtiquetas(){
    $(".flot-tick-label").each(function (index) {
        $( this ).css( "-webkit-transform", "rotate(-45deg)" );
        $( this ).css( "transform", "rotate(-45deg)" );
        $( this ).css( "-ms-transform", "rotate(-45deg)" );
        $( this ).css( "-moz-transform", "rotate(-45deg)" );
        $( this ).css( "-o-transform", "rotate(-45deg)" );
        $( this ).css( "font-size", "8px" );
    });
}

function CargarDatos(nomGraf,nomChar){
    Data_1 = [];
    var ds = new Array();
    $("."+nomGraf).each(function (index) {
        Data_1 = [];
        Data_1.push([0, $(this).attr('dat-1')]);
        Data_1.push([1, $(this).attr('dat-2')]);
        Data_1.push([2, $(this).attr('dat-3')]);
        Data_1.push([3, $(this).attr('dat-4')]);
        Data_1.push([4, $(this).attr('dat-5')]);
        Data_1.push([5, $(this).attr('dat-6')]);
        Data_1.push([6, $(this).attr('dat-7')]);
        Data_1.push([7, $(this).attr('dat-8')]);
        Data_1.push([8, $(this).attr('dat-9')]);
        Data_1.push([9, $(this).attr('dat-10')]);
        Data_1.push([10, $(this).attr('dat-11')]);
        Data_1.push([11, $(this).attr('dat-12')]);
        ds.push({
            label: $(this).val(),
            data: Data_1,
            bars: {order: 1}
        });
    });
    var data = ds;
        $.plot($("#"+nomChar), data, {
            bars: {
                show: false,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames_Mes,
                tickOptions:{ 
                    angle: -60
                }
            }
        });
    CambiarEtiquetas();
}