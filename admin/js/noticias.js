$( "#form_CrearGaleria" ).submit(function( event ) {
    if(valida()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

$(document).ready(function(){
    // Select Placeholders
    $("#media_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#media_id_ed").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#video_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#video_id_ed").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});

$(document).ready(function() {
    $('a[href][title]').qtip({
        content: {
            text: false
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        },
        position: {
            target: 'mouse'
        }
    });
});

function valida(){
    var valid = 0;
    $('#news').css( "border-color", "#efefef" );
    $('#description').css( "border-color", "#efefef" );
    if($('#news').val() == ""){
        $('#news').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#category').val() == ""){
        $('#category').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description').val() == ""){
        $('#description').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new').val()==""){
        valid=1;
        notyfy({
                text: 'Debe seleccionar una imagen',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
    }else{
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

$( "#form_EditarGaleria" ).submit(function( event ) {
    if(validaEdt()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

function validaEdt(){
    var valid = 0;
    $('#news_ed').css( "border-color", "#efefef" );
    $('#description_ed').css( "border-color", "#efefef" );
    if($('#news_ed').val() == ""){
        $('#news_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#category_ed').val() == ""){
        $('#category_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description_ed').val() == ""){
        $('#description_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new_ed').val()!=""){
        var inputFileImage = document.getElementById("image_new_ed");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}


function LikeGalery(news_id){
    var data = new FormData();
    data.append('news_id',news_id);
    data.append('opcn','LikeGalery');
    var url = "controllers/noticias.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                notyfy({
                    text: 'Muchas gracias, hemos recibido tu like! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes'+news_id).text());
                $('#CantLikes'+news_id).text(Cantlikes+1);
            }else{
                notyfy({
                    text: 'Ya votaste por esta multimedia anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function ViewGalery(media_detail_id){
    //alert("otro");
    var data = new FormData();
    data.append('media_detail_id',media_detail_id);
    data.append('opcn','ViewGalery');
    var url = "controllers/multimedias_detail.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                var Cantlikes = parseInt($('#CantViews'+media_detail_id).text());
                $('#CantViews'+media_detail_id).text(Cantlikes+1);
            }
        }
    });
}

$('#imagenNoticia').click( function(){
    $('#ModalImagen').modal();
} );
