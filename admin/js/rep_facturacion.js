$( document ).ready( function(){
    $('#zonas').select2({
        placeholder: "Seleccione una zona",
        allowClear: true
    });
    $('#concesionarios').select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
    $('#sedes').select2({
        placeholder: "Seleccione una sede",
        allowClear: true
    });
    $('#cargos').select2({
        placeholder: "Seleccione un cargo",
        allowClear: true
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    NProgress.configure({ parent: '.loader_s' });
} );

$( '#form_facturacion' ).submit( function( e ){
    e.preventDefault();
    NProgress.start();//inicia el loader
    var datos = $(this).serialize();
    $.ajax({
        url:'controllers/rep_facturacion.php',
        data: datos,
        type: 'POST',
        dataType: 'JSON'
    })
    .done( function( data ){
        $( '#tabla_IBT' ).html( data.IBT );
        $( '#tabla_VCT' ).html( data.VCT );
        $( '#tabla_WBT' ).html( data.WBT );
        $( '#tabla_OJT' ).html( data.OJT );
        
        generarGrafica( data.datos_tablas.cursos, "Total de Cursos", "g_cursos" );
        generarGrafica( data.datos_tablas.horas, "Horas de Impartición", "g_horas" );
        generarGrafica( data.datos_tablas.asistentes, "Asistencia", "g_asistentes" );
        generarGrafica( data.datos_tablas.capacitacion, "Total de Horas Capacitación", "g_capacitacion" );

    } )
    .always( function(){  NProgress.done();/*finaliza el loader*/ } );
} );

function generarGrafica( obj, titulo, id ){
    var pieOptions = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,
        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 0, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
      };
    
    var pieChartCanvas = $("#"+id).get(0).getContext("2d");
    $('#'+id+"_titulo").html(titulo);
    var pieChart = new Chart(pieChartCanvas);
    // var colores = [ "#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de" ];
    var lista = [];
    lista.push( { value: obj.blandas, color: "#f56954", highlight: "#f56954", label: "Habilidades Blandas" } );
    lista.push( { value: obj.tecnicas, color: "#00a65a", highlight: "#00a65a", label: "Habilidades Tecnicas" } );
    lista.push( { value: obj.marca, color: "#f39c12", highlight: "#f39c12", label: "Procesos de Marca" } );
    lista.push( { value: obj.producto, color: "#3c8dbc", highlight: "#3c8dbc", label: "Productos" } );

    pieChart.Doughnut(lista, pieOptions);
}

