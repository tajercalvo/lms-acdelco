$(document).ready(function(){
    // Select Placeholders
    $("#charge_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2012-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2012-01-01"
    });
});
