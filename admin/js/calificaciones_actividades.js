$(function() {
    $(document).ready(function () {
        $("#schedule_id").select2({
            placeholder: "Seleccione una trayectoria",
            allowClear: true
        });
        $('#fecha_inicial').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2014-01-01"
        });
        $('#fecha_final').bdatepicker({
            format: "yyyy-mm-dd",
            startDate: "2014-01-01"
        });
    });
    
    
    $('#frm_sesiones').submit(function (e) {
        e.preventDefault();
    
        fecha_inicial = $('#fecha_inicial').val();
        fecha_final = $('#fecha_final').val();
        var fechaInicio = new Date(fecha_inicial).getTime();
        var fechaFin = new Date(fecha_final).getTime();
        var diff = fechaFin - fechaInicio;
        console.log(diff / (1000 * 60 * 60 * 24));
        if( diff / (1000 * 60 * 60 * 24) > 31 ){
            alert('La fecha no puede ser mayor a un mes');
            return false; 
        }
        
        //$('#consultando').html('<img style="width: 15px; " src="../assets/loading.gif">');
        $('#tabla tbody').html('');
        var data = { opcn: 'getCoursesTeacher', schedule_id: $('#schedule_id').val(), fecha_inicial:fecha_inicial, fecha_final:fecha_final };
        $.ajax({
            url: 'controllers/calificaciones_actividades.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
        })
            .done(function (data) {
                console.log(data);
                select = '';
                data.forEach(function (e, i) {
                    select +=`<option value="`+e.schedule_id+`" >Sesión: `+e.schedule_id+` - Curso: `+e.course+` - Sesión: `+e.module+`</option>`
                });
                $('#schedule_id').html(select) 
                $('#schedule_id').select2('destroy');
                $("#schedule_id").select2({
                    placeholder: "Seleccione una trayectoria",
                    allowClear: true
                });
            })
            .fail(function () {
                $('#consultando').html('');
                console.log("error");
            })
    });
    
    var notas = '';
    $('#frm_actividades').submit(function (e) {
        e.preventDefault();    
        getAcitivitieUser();
    });

    function getAcitivitieUser() {
        var data = { opcn: 'getAcitivitieUser', schedule_id: $('#schedule_id').val() };
        $.ajax({
            url: 'controllers/calificaciones_actividades.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
        })
            .done(function (data) {
                notas = data;
                tabla = '';
                data.forEach(function (e, i) {
                    if(e.teacher_id == 0){
                        estado = 'Pendiente';
                    } else {
                        estado = 'Calificada';
                    }
                
                    // Verificación del archivo adjunto directamente en JavaScript
                    let archivo = '';
                    if (e.archivo && e.archivo.trim() !== '') {
                        archivo = `<a href="../assets/actividades_usuarios/`+e.archivo+`" target="_blank">Archivo</a><br><span>`+e.comentarios+`</span>`;
                    } else {
                        archivo = `<span style="color:red";>Sin adjunto</span><br><span>`+e.comentarios+`</span>`;
                    }
                
                    // Construcción de la fila
                    tabla += `<tr data-pos="`+i+`">
                                <td>`+e.activity+`</td>
                                <td>`+e.date_edition+`</td>
                                <td>`+e.tipo+`</td>
                                <td>`+e.porcentaje+`%</td>
                                <td>`+e.identification+`</td>
                                <td>`+e.first_name+` `+e.last_name+`</td>
                                <td>`+archivo+`</td> <!-- Aquí se muestra el archivo o el mensaje "Sin adjunto" -->
                                <td>`+estado+`<br>
                                    <span id="comen_`+i+`" ><a class="comentarios" href="javascript:void(0);" data-toggle="tooltip" title="`+e.coments_teacher+`">Comentarios</a></span>
                                </td>
                                <td>
                                    <input class="nota form-control" type="text" autocomplete="off" value="`+e.resultado+`" onclick="clearAndSetCursorPosition(this)" />
                                </td>
                              </tr>`;
                });
                $('#tabla tbody').html(tabla);
                
            })
            .fail(function () {
                console.log("error");
            })
    }
    
    $('body').on('input', '.nota', function () {
        this.value = this.value.replace(/[^0-9]/g,'');
        if(this.value > 100){
            $(this).val(100);
        }
    });

    $("#myModal").on('hidden.bs.modal', function () {
        var comentario = $('#comemtario_modal').val().trim();
        notas[pos_comentario].coments_teacher = comentario;
        $('#comen_'+pos_comentario).html('<a class="comentarios" href="javascript:void(0);" data-toggle="tooltip" title="'+comentario+'">Comentarios</a>');
    });

    $('body').on('click', '.comentarios', function (e) {
        e.preventDefault();
        $('#myModal').modal('toggle');
        pos_comentario = $(this).parents('tr').data('pos');
        var coments_teacher = notas[pos_comentario].coments_teacher
        $('#comemtario_modal').val(coments_teacher);
    });

    
    $('body').on('change', '.nota', function () {
        var pos = $(this).parents('tr').data('pos');
        console.log(pos)
        notas[pos].resultado = $(this).val()
    });

    $('#btn-calificar').click(function (e) { 
        e.preventDefault();
        $.ajax({
            url: 'controllers/calificaciones_actividades.php',
            type: 'POST',
            dataType: 'JSON',
            data: {opcn:'calificar', notas:notas}
        })
        .done(function (data) {
            if (data.error) {
                notyfy({
                    text: data.msj,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            } else {
                notyfy({
                    text: data.msj,
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                getAcitivitieUser()
            }
        })
        .fail(function () {
            console.log('error');
        });
    });


    $(document).ajaxStart(function () {
        $( ".btn" ).prop( "disabled", true );
        $('.btn').prepend('<i class="fa fa-refresh fa-spin"></i> ');
      })
      $(document).ajaxComplete(function () {
        $( ".btn" ).prop( "disabled", false );
        $('.fa-refresh.fa-spin').replaceWith('');
      })
    
});
function clearAndSetCursorPosition(inputElement) {
    // Borra el contenido del input
    inputElement.value = "";
    // Establece el cursor en la posición 0
    inputElement.setSelectionRange(0, 0);
    // Enfoca el input
    inputElement.focus();
}