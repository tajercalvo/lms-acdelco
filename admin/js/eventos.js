$(document).ready(function() {
	if ($('#calendar_eventos_asesor').length){
		$('#calendar_eventos_asesor').fullCalendar({
			//lang: 'es',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: false,
			droppable: false,
			events: rootPath + "/admin/controllers/eventos.php?opcn=consultar",
			loading: function(bool) {
				if (bool) $('#loading').show();
				else $('#loading').hide();
			},
		    eventRender: function(event, element) {
		        element.qtip({
		            //content: event.description,
					content: {
						text: event.description,
						title: {
							text: event.title
						}
					},
	        		style: { classes: 'qtip-rounded qtip-'+event.color },
	        		hide: {
				        fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
				    },
				    position: {
				        my: 'bottom center',
				        at: 'top center'
				    }
		        });
		    }
		});
	}
	$("#media_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#date_event').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-07-01"
    });
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
	$('a[href][title]').qtip({
        content: {
            text: false
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        },
        position: {
            target: 'mouse'
        }
    });
});