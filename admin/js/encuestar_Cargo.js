$(document).ready(function(){
    $('#loadingRespuesta').addClass('hide');
});

/*$( "#formulario_respuestas" ).submit(function( event ) {
    //Variables
        $('#loadingRespuesta').removeClass('hide');
        var valid = 0;
        var valid_1_ct = 0;
        var valid_2_ct = 0;
        var valid_3_ct = 0;
        var valid_4_ct = 0;
        var valid_5_ct = 0;
        var valid_6_ct = 0;
        var valid_7_ct = 0;
        var valid_8_ct = 0;
        var valid_9_ct = 0;
        var val_rep104 = 0;
        var val_rep105 = 0;
        var val_rep106 = 0;
        var val_rep107 = 0;
        var val_rep108 = 0;
        var val_rep109 = 0;
        var val_rep110 = 0;
        var val_rep111 = 0;
        var val_rep112 = 0;
        var _val_resp104 = 0;
        var _val_resp105 = 0;
        var _val_resp106 = 0;
        var _val_resp107 = 0;
        var _val_resp108 = 0;
        var _val_resp109 = 0;
        var _val_resp110 = 0;
        var _val_resp111 = 0;
        var _val_resp112 = 0;
    //Variables
    //Validaciones
        $('#104').css( "border-color", "#efefef" );
        if($('#104').val() == ""){
            $('#104').css( "border-color", "#b94a48" );
            valid = 1;
        }
        $('#106').css( "border-color", "#efefef" );
        if($('#106').val() == ""){
            $('#106').css( "border-color", "#b94a48" );
            valid = 1;
        }
        $('#107').css( "border-color", "#efefef" );
        if($('#107').val() == ""){
            $('#107').css( "border-color", "#b94a48" );
            valid = 1;
        }
        $('#109').css( "border-color", "#efefef" );
        if($('#109').val() == ""){
            $('#109').css( "border-color", "#b94a48" );
            valid = 1;
        }
        $('#110').css( "border-color", "#efefef" );
        if($('#110').val() == ""){
            $('#110').css( "border-color", "#b94a48" );
            valid = 1;
        }
        $('#111').css( "border-color", "#efefef" );
        if($('#111').val() == ""){
            $('#111').css( "border-color", "#b94a48" );
            valid = 1;
        }
        $('#112').css( "border-color", "#efefef" );
        if($('#112').val() == ""){
            $('#112').css( "border-color", "#b94a48" );
            valid = 1;
        }

        $(".Quest105").each(function (index,element) {
            if($(this).is(':checked')){
                valid_1_ct = 1;
                val_rep105 = $(this).val();
                _val_resp105 = $(this).attr('data-vlr');
                console.log("::"+val_rep105+"::"+_val_resp105);
            }
        });
        $(".Quest108").each(function (index,element) {
            if($(this).is(':checked')){
                valid_2_ct = 1;
                val_rep108 = $(this).val();
                _val_resp108 = $(this).attr('data-Vlr');
                console.log("::"+val_rep108+"::"+_val_resp108);
            }
        });
        

        if(valid_1_ct==0 || valid_2_ct==0 ){
            valid = 1;
        }
        if(_val_resp105==0 || _val_resp108==0 ){
            valid = 1;
        }
    //Validaciones

    if(valid == 0){
        if(confirm('¿Esta seguro de contestar la encuesta con estas respuestas?')){
            var data = new FormData();
            data.append('val_rep104',$('#104').val());
            data.append('val_rep106',$('#106').val());
            data.append('val_rep107',$('#107').val());
            data.append('val_rep109',$('#109').val());
            data.append('val_rep110',$('#110').val());
            data.append('val_rep111',$('#111').val());
            data.append('val_rep112',$('#112').val());
            data.append('val_rep105',val_rep105);
            data.append('val_rep108',val_rep108);
            data.append('_val_resp105',_val_resp105);
            data.append('_val_resp108',_val_resp108);

            data.append('opcn','GuardaEncuesta');
            var url = "controllers/encuestar_Cargo.php";
            $.ajax({
                url:url,
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                cache:false,
                success: function(data) {
                    $('#loadingRespuesta').addClass('hide');
                    if(data.resultado=="SI"){
                        notyfy({
                            text: 'Muchas gracias tu encuesta ha sido procesada',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                        $('#ContenidoEnc').html('<div class="innerAll"><h4 class="innerTB margin-none half center">Muchas Gracias por su tiempo. Reiteramos que la prioridad de Chevrolet y GMAcademy es hacer clientes felices.</h4><div class="separator bottom"></div><div class="row">Es posible que tenga encuestas pendientes por responder, las cuales irán iendo una a una hasta finalizarlas por completo.<div id="BtnIniciar" class="innerAll text-center"><a href="index.php" class="btn btn-success">Aceptar</a></div></div></div>');
                    }else{
                        notyfy({
                            text: 'No se ha podido almacenar tu encuesta, comprueba tu conexión a internet',
                            type: 'warning' // alert|error|success|information|warning|primary|confirm
                        });
                    }
                }
            });
        }else{
            $('#loadingRespuesta').addClass('hide');
            notyfy({
                text: 'ha cancelado el envío de las respuestas para esta encuesta, puede intentar nuevamente dando clic en Responder!',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
        }
    }else{
        $('#loadingRespuesta').addClass('hide');
        notyfy({
            text: 'Hay preguntas sin responder, por favor verifíque las respuesas e intente nuevamente',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
    event.preventDefault();
});*/

$( "#formulario_respuestas" ).submit(function( event ) {
    $('#loadingRespuesta').removeClass('hide');
    var ProcesaTodo = 0;
    var MsgError = "Preguntas que aún no ha diligenciado:<br>";
    $(".Data_Radio").each(function (index,element) {
        var DataRadio_Ctrl = 0;
        var IdRadio = $(this).val();
        $(".Quest"+IdRadio).each(function (index,element) {
            if($(this).is(':checked')){
                DataRadio_Ctrl = 1;
            }
        });
        if(DataRadio_Ctrl==0){
            ProcesaTodo = 1;
            MsgError = MsgError + "Pregunta: "+ $(this).attr('data-lbl')+"<br>";
        }
    });
    $(".CampText").each(function (index,element) {
        if($(this).val()==""){
            ProcesaTodo = 1;
            MsgError = MsgError + "Pregunta: "+ $(this).attr('data-lbl')+"<br>";
        }
    });

    if(ProcesaTodo==1){
        notyfy({
            text: MsgError,
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }else{
        notyfy({
            text: 'Esta seguro de enviar estas respuestas?',
            type: 'confirm',
            dismissQueue: true,
            layout: 'top',
            buttons: ([{
                addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
                text: '<i></i> Aceptar',
                onClick: function ($notyfy) {
                    $notyfy.close();
                        $.ajax({ url: 'controllers/encuestar_Cargo.php',
                            data: $('#formulario_respuestas').serialize(),
                            type: 'post',
                            dataType: "json",
                            success: function(data) {
                                $('#loadingRespuesta').addClass('hide');
                                var res0 = data.resultado;
                                if(res0 == "SI"){
                                    notyfy({
                                        text: 'Hemos recibido la información de la encuesta, muchas gracias',
                                        type: 'success' // alert|error|success|information|warning|primary|confirm
                                    });
                                    $('#ContenidoEnc').html('<div class="innerAll"><h4 class="innerTB margin-none half center">Muchas Gracias por su tiempo. Reiteramos que la prioridad de Chevrolet y GMAcademy es hacer clientes felices.</h4><div class="separator bottom"></div><div class="row">Es posible que tenga encuestas pendientes por responder, las cuales irán iendo una a una hasta finalizarlas por completo.<div id="BtnIniciar" class="innerAll text-center"><a href="index.php" class="btn btn-success">Aceptar</a></div></div></div>');
                                }else{
                                    notyfy({
                                        text: 'No se ha logrado recibir la información, por favor confirme su conexión a internet y envíela nuevamente',
                                        type: 'error' // alert|error|success|information|warning|primary|confirm
                                    });
                                }
                            }
                        });
                }
            }, {
                addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
                text: '<i></i> Cancelar',
                onClick: function ($notyfy) {
                    $notyfy.close();
                    notyfy({
                        force: true,
                        text: '<strong>Ha cancelado el envío de la encuesta<strong>',
                        type: 'error',
                        layout: 'top'
                    });
                }
            }])
        });
    }
    event.preventDefault();
    $('#loadingRespuesta').addClass('hide');
});