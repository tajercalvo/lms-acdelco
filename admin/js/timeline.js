//Calendario
$(document).ready(function(){
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
});


function validacion() {

  	s_date = document.getElementById("start_date_day").value;
  	e_date = document.getElementById("end_date_day").value;
  	id = document.getElementById("identificacion").value;

    $('#start_date_day').css( "border-color", "#ffffff" );
    $('#end_date_day').css( "border-color", "#ffffff" );
    $('#identificacion').css( "border-color", "#ffffff" );


	if( s_date == '' & e_date == '' & id == '') {
	// alert('Complete la informacion antes de consultarla');
    $('#start_date_day').css( "border-color", "#b94a48" );
    $('#end_date_day').css( "border-color", "#b94a48");
    $('#identificacion').css( "border-color", "#b94a48" );
    $( "#start_date_day" ).focus();
    return false;
    } else if( s_date == '' & e_date == '') {
    // alert('Complete la informacion antes de consultarla');
    $('#start_date_day').css( "border-color", "#b94a48" );
    $('#end_date_day').css( "border-color", "#b94a48" );
    $( "#start_date_day" ).focus();
    return false;
    } else if( s_date == '' & id == '') {
    // alert('Complete la informacion antes de consultarla');
    $('#start_date_day').css( "border-color", "#b94a48" );
    $('#identificacion').css( "border-color", "#b94a48" );
    $( "#start_date_day" ).focus();
    return false;
    } else if( e_date == '' & id == '') {
    $('#end_date_day').css( "border-color", "#b94a48" );
    $('#identificacion').css( "border-color", "#b94a48" );
    $( "#end_date_day" ).focus();
    return false;
    } 
     else if($('#start_date_day').val() == ""){
        $('#start_date_day').css( "border-color", "#b94a48" );
        $( "#start_date_day" ).focus();
    return false;
    } else if($('#end_date_day').val() == ""){
        $('#end_date_day').css( "border-color", "#b94a48" );
        $( "#end_date_day" ).focus();
    return false;
    } else if($('#identificacion').val() == ""){
        $('#identificacion').css( "border-color", "#b94a48" );
        $( "#identificacion" ).focus();
    return false;
    } 
    return true;
    }

//No permitir escribir caracteres en las cajas de texto
$(document).ready(function() {
    $("#identificacion").keypress(function(tecla) {
        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
    });
});
jQuery(document).ready(function() {
    $("#end_date_day").keypress(function(tecla) {
        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
    });
});
$(document).ready(function() {
    $("#start_date_day").keypress(function(tecla) {
        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
    });
});

function confirmar(){
    var r = confirm("¿Está seguro de eliminar el resultado de esta evaluación?");
    if (r) {
        return true;
    }else{
        return false;
    }
};


// $("#identificacio").keypress(function (key) {
//             window.console.log(key.charCode)
//             if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
//                 && (key.charCode < 65 || key.charCode > 90) //letras minusculas
//                 && (key.charCode != 45) //retroceso
//                 && (key.charCode != 241) //ñ
//                  && (key.charCode != 209) //Ñ
//                  && (key.charCode != 32) //espacio
//                  && (key.charCode != 225) //á
//                  && (key.charCode != 233) //é
//                  && (key.charCode != 237) //í
//                  && (key.charCode != 243) //ó
//                  && (key.charCode != 250) //ú
//                  && (key.charCode != 193) //Á
//                  && (key.charCode != 201) //É
//                  && (key.charCode != 205) //Í
//                  && (key.charCode != 211) //Ó
//                  && (key.charCode != 218) //Ú
 
//                 )
//                 return false;
//         });
