$(document).ready(function(){
    get_menu();
    // alert();
});

function get_menu(){

var data = {opcn:'get_menu'};

         $.ajax({
             url: 'controllers/admin_menu.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
            var html = '';
            data.menu.forEach( function(e, i) {
                html += `<tr data-id="`+e.menu_id+`" data-padre="`+e.padre+`" data-hijo="`+e.hijo+`">
                        <td>`+(i+1)+`</td>
                        <td>`+e.menu_id+`</td>
                        <td>`+e.padre+`</td>
                        <td>`+e.hijo+`</td>
                        <td><span class="editar btn btn-action glyphicons pencil btn-success"><i></i></span></td>
                        <td><label class="switch">
                          <input class="checkeado" type="checkbox" `+e.estado+`>
                          <span class="slider round"></span>
                        </label>
                        </td>
                        </tr>`;
            });
            $('#tabla tbody').html(html);
         })
         .fail(function() {
             console.log("error");
         })
}


$('body').on('click', '.checkeado', function(event) {
    var menu_id = $(this).parents('tr').data('id');
    console.log('menu_id')
        console.log(menu_id)
    if( $(this).is(':checked') ) {
        act_menu(menu_id, 1)
    }else{
        act_menu(menu_id, 2)
    }
});

function act_menu(menu_id, status_id) {
    $.ajax({
        url: 'controllers/admin_menu.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn: 'act_menu', status_id:status_id, menu_id:menu_id },
    })
    .done(function() {
        console.log("success");
    })
    .fail(function() {
        console.log("error");
    })
}

permiso_menu_id = '';
$('body').on('click', '.editar', function(event) {
    permiso_menu_id = $(this).parents('tr').data('id');
    $('#tabla').toggle();
    // $('#tabla_permisos').toggle();
    $('#volver').toggle();
    console.log($(this).parents('tr').data('hijo'))
    console.log($(this).parents('tr').data('padre'))

    var modulo = $(this).parents('tr').data('hijo');
    if( modulo == 'NO' ){
        modulo = $(this).parents('tr').data('padre');
    }else{
        modulo = $(this).parents('tr').data('hijo')
    }
    $('#titulo_modulo').html(modulo);

         $.ajax({
             url: 'controllers/admin_menu.php',
             type: 'POST',
             dataType: 'JSON',
             data: {opcn:'get_permisos', menu_id:permiso_menu_id},
         })
         .done(function(data) {
            var html = '';
            data.menu.forEach( function(e, i) {
                html += `<tr data-id="`+e.rol_id+`">
                        <td>`+(i+1)+`</td>
                        <td>`+e.rol+`</td>
                        <td><label class="switch">
                          <input class="checkeado_p" type="checkbox" `+e.estado+`>
                          <span class="slider round"></span>
                        </label>
                        </td>
                        </tr>`;
            });
            $('#tabla_permisos tbody').html(html);
            $('#tabla_permisos').toggle();
         })
         .fail(function() {
             console.log("error");
         });
});

$('#volver').click(function(event) {
    $('#tabla').toggle();
    $('#tabla_permisos').toggle();
    $('#volver').toggle();
});

rol_id = '';
$('body').on('click', '.checkeado_p', function(event) {
    rol_id = $(this).parents('tr').data('id');
        console.log('menu_id')
        console.log(permiso_menu_id)
    if( $(this).is(':checked') ) {
        act_perm(permiso_menu_id, rol_id, 1)
    }else{
        act_perm(permiso_menu_id, rol_id, 2)
    }
});

function act_perm(menu_id, rol_id, status_id) {
    $.ajax({
        url: 'controllers/admin_menu.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn: 'act_perm', status_id:status_id, menu_id:menu_id, rol_id:rol_id },
    })
    .done(function() {
        console.log("success");
    })
    .fail(function() {
        console.log("error");
    })
}