$(document).ready(function(){
    $('#course_id').select2({
        placeholder: "Seleccione un curso",
        allowClear: true
    });
    
    // Select Placeholders
    $('#start_date').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
});


function validar() {
    console.log('Console');
    let start_date = new Date($('#start_date').val()); 
    let end_date = new Date($('#end_date').val());
    
        let diasdif= end_date.getTime()-start_date.getTime();
        let contdias = Math.round(diasdif/(1000*60*60*24));
        
        if( contdias > 31){
            alert('Seleccione una fecha no mayor a 30 días')
            return false;
        }else{
            return true;
        }
    
}
