$(document).ready(function(){
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#trayectoria').select2({
        placeholder: "Seleccione una trayectoria",
        allowClear: true
    });
    CargarDatos();
    CargarArgumentario();
});

function CargarDatos(){
    var DataVisitas = [];
    var DataVisitantes = [];
    var DataTiempo = [];
    var DataPromedio = [];
    var DataNames = [];
    var _CtrlVar = 0;
    $(".seccion_dat").each(function (index) {
        _CtrlVar = _CtrlVar + 1;
        DataVisitas.push([_CtrlVar, $(this).attr('data-Vis')]);
        DataVisitantes.push([_CtrlVar, $(this).attr('data-Usr')]);
        DataNames.push([_CtrlVar, $(this).val()]);
        DataTiempo.push([_CtrlVar, $(this).attr('data-TimeT')]);
        DataPromedio.push([_CtrlVar, $(this).attr('data-Prom')]);
    });
        var ds = new Array();
        ds.push({
            label: "Visitas",
            data:DataVisitas,
            bars: {order: 1}
        });
        ds.push({
            label: "Visitantes",
            data:DataVisitantes,
            bars: {order: 2}
        });
        var data = ds;

        var ds1 = new Array();
        ds1.push({
            label: "Tiempo Total",
            data:DataTiempo,
            bars: {order: 1}
        });
        ds1.push({
            label: "Tiempo Promedio",
            data:DataPromedio,
            bars: {order: 2}
        });
        var data1 = ds1;

        $.plot($("#charVisitas"), data, {
            bars: {
                show:true,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y.0",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames,
                tickOptions:{
                    angle: -60
                }
            }
        });

        $.plot($("#charTiempos"), data1, {
            bars: {
                show:true,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y.0",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames,
                tickOptions:{
                    angle: -60
                }
            }
        });
    CambiarEtiquetas();
}

function CargarArgumentario(){
    var DataVisitas = [];
    var DataVisitantes = [];
    var DataTiempo = [];
    var DataPromedio = [];
    var DataNames = [];
    var Datalikes = [];
    var _CtrlVar = 0;
    $(".Agument_dat").each(function (index) {
        _CtrlVar = _CtrlVar + 1;
        DataVisitas.push([_CtrlVar, $(this).attr('data-Vis')]);
        DataVisitantes.push([_CtrlVar, $(this).attr('data-Usr')]);
        DataNames.push([_CtrlVar, $(this).val()]);
        DataTiempo.push([_CtrlVar, $(this).attr('data-TimeT')]);
        DataPromedio.push([_CtrlVar, $(this).attr('data-Prom')]);
        Datalikes.push([_CtrlVar, $(this).attr('data-likes')]);
    });
        var ds = new Array();
        if($('#opcn_Visitas').is(':checked')){
            ds.push({
                label: "Visitas",
                data:DataVisitas,
                bars: {order: 1}
            });
        }
        if($('#opcn_Visitantes').is(':checked')){
            ds.push({
                label: "Visitantes",
                data:DataVisitantes,
                bars: {order: 2}
            });
        }
        if($('#opcn_Tiempo').is(':checked')){
            ds.push({
                label: "Tiempo Total",
                data:DataTiempo,
                bars: {order: 1}
            });
        }
        if($('#opcn_Promedio').is(':checked')){
            ds.push({
                label: "Tiempo Promedio",
                data:DataPromedio,
                bars: {order: 2}
            });
        }
        if($('#opcn_Likes').is(':checked')){
            ds.push({
                label: "Likes",
                data:Datalikes,
                bars: {order: 2}
            });
        }
        var data = ds;

        $.plot($("#charArgumentarios"), data, {
            bars: {
                show:true,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames,
                tickOptions:{
                    angle: -60
                }
            }
        });
    CambiarEtiquetas();
}

function CambiarEtiquetas(){
    $(".flot-tick-label").each(function (index) {
        $( this ).css( "-webkit-transform", "rotate(-45deg)" );
        $( this ).css( "transform", "rotate(-45deg)" );
        $( this ).css( "-ms-transform", "rotate(-45deg)" );
        $( this ).css( "-moz-transform", "rotate(-45deg)" );
        $( this ).css( "-o-transform", "rotate(-45deg)" );
        $( this ).css( "font-size", "8px" );
    });
}

$(document).ready(function() {
    $('a[href][title]').qtip({
        content: {
            text: false
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        },
        position: {
            target: 'mouse'
        }
    });
});

function mostrar( clase ){
    // console.log('tiene clase? ', $('.'+clase).hasClass('hide') );

    if( $('.'+clase).hasClass('hide') ){
        $('.'+clase).removeClass('hide');
    }else{
        $('.'+clase).addClass('hide');
    }
}
