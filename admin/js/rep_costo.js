var Data_Ano1_Mes = [];
var Data_Ano2_Mes = [];
var DataNames_Mes = [];

(function($)
{
    $('#ano').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#mes').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#BtnProcesaDatos').click(function() {
        ProcesarDatos();
        /*notyfy({
            text: 'Esta seguro de procesar este periodo, si ya hay datos para el mismo se reescribirán y no podrá recuperarlos',
            type: 'confirm',
            dismissQueue: true,
            layout: 'top',
            buttons: ([{
                addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
                text: '<i></i> Aceptar',
                onClick: function ($notyfy) {
                    $notyfy.close();
                    ProcesarDatos();
                }
            }, {
                addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
                text: '<i></i> Cancelar',
                onClick: function ($notyfy) {
                    $notyfy.close();
                    notyfy({
                        force: true,
                        text: '<strong>Se ha cancelado el proceso<strong>',
                        type: 'error',
                        layout: 'top'
                    });
                }
            }])
        });*/
    });
    /*$('#consulta').submit(function(event) {
       event.preventDefault();
       console.log($(this).serializeArray());
    });*/
    /*setTimeout(function(){
            CargarDatos();
        }, 100);*/
})(jQuery);

function CargarDatos(){
    Data_Ano1_Mes = [];
    Data_Ano2_Mes = [];
    DataNames_Mes = [];
    var _CtrlVar = 0;
    $(".GrafAnoMes").each(function (index) {
        Data_Ano1_Mes.push([_CtrlVar, $(this).attr('dat-anoa')]);
        Data_Ano2_Mes.push([_CtrlVar, $(this).attr('dat-anof')]);
        DataNames_Mes.push([_CtrlVar, $(this).attr('dat-mes')]);
        _CtrlVar = _CtrlVar + 1;
    });
    var ds = new Array();
        if($('#opcn_Ano1').is(':checked')){
            ds.push({
                label: $('#Ano1_sel').val(),
                data: Data_Ano1_Mes,
                bars: {order: 1}
            });
        }
        if($('#opcn_Ano2').is(':checked')){
            ds.push({
                label: $('#Ano2_sel').val(),
                data: Data_Ano2_Mes,
                bars: {order: 2}
            });
        }
    var data = ds;

        $.plot($("#charSentinel"), data, {
            bars: {
                show: false,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames_Mes,
                tickOptions:{
                    angle: -60
                }
            }
        });
    CambiarEtiquetas();
}

function CambiarEtiquetas(){
    $(".flot-tick-label").each(function (index) {
        $( this ).css( "-webkit-transform", "rotate(-45deg)" );
        $( this ).css( "transform", "rotate(-45deg)" );
        $( this ).css( "-ms-transform", "rotate(-45deg)" );
        $( this ).css( "-moz-transform", "rotate(-45deg)" );
        $( this ).css( "-o-transform", "rotate(-45deg)" );
        $( this ).css( "font-size", "8px" );
    });
}


function ProcesarDatos(){
    $('#loading_efectividad').removeClass('hide');
    var data = new FormData();
    data.append('ano_id_end',$('#ano_id_end').val());
    data.append('mes_id_end',$('#mes_id_end').val());
    data.append('opcn','proc');
    var url = "controllers/sentinel_efectividad.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                alert('Hemos procesado el Mes del año correspondiente, ya puede consultar la información');
                notyfy({
                    text: 'Hemos procesado el Mes del año correspondiente, ya puede consultar la información',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                $('#loading_efectividad').addClass('hide');
            }else{
                notyfy({
                    text: 'El servidor se encuentra ocupado para procesar la solicitud, por favor intente de nuevo mas tarde',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
                $('#loading_efectividad').addClass('hide');
            }
        }
    });
}
