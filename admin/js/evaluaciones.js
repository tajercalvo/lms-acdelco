var arr_agrup = [];
var review_id = 0;
var questions = [];
var preguntasSel = [];
//===========================================================================
// consulta los datos de la tabla de evaluaciones
//===========================================================================
$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/evaluaciones.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});

//===========================================================================
// Inicia los scripts al cargar totalmente la pagina
//===========================================================================
$(document).ready(function(){
    review_id = $('#review_id').val();
    // review_id = 2
    CambiaTipo();
    // Select Placeholders
    $("#cursos").select2({
        placeholder: "Seleccione el (los) cursos",
        allowClear: true
    });
    $("#cargos").select2({
        placeholder: "Seleccione la (las) trayectorias",
        allowClear: true
    });
    $("#type").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

    //consulta las agrupaciones creadas para un review_id
    consultarAgrupacion( review_id );
    getQuestions( review_id );
    getPreguntasSel( review_id );
});

//==========================================================================
// Crea o edita los datos de una evaluacion
//==========================================================================
$( "#formulario_data" ).submit(function( event ) {
    if(valida()){

        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }//fin if-else

    }// fin valida
    event.preventDefault();
});//fin submit => #formulario_data

//===========================================================================
// Retorna a la pagina anterior
//===========================================================================
$("#retornaElemento").click(function() {
    window.location="evaluaciones.php";
});

$('#agregarPregunta').submit( function( e ){
    e.preventDefault();
    opciones = $(this).serializeArray();
    console.log(  );
    datos = {
        'opcn':'agregarPregunta'
    };
    //agrega los datos para almacenar la pregunta
    opciones.forEach( function( key, idx ){
        console.log( key );
        if( key.name == 'value' ){
            if( key.value == 0 || key.value == "" ){
                notyfy({
                    text: 'El valor de la pregunta no puede ser 0 o estar en blanco',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
                return;
            }
        }//fin key.name == value

        datos[key.name] = key.value;

    } );//fin forEach

    $.ajax({
        url: 'controllers/evaluaciones.php',
        data: datos,
        type: 'post',
        dataType: "json",
        success: function(data) {

            if( data.resultado == "SI" ){
                preguntasSel.push( data.reviews_questions );
                tabla = renderPreguntasSel( preguntasSel );
                $('#list_preguntasSel').html( tabla );
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: data.message,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });//fin envio datos ajax

} );//fin ('#agregarPregunta').submit

//===========================================================================
// Confirma que los datos sean correctos
//===========================================================================
function valida(){
    var valid = 0;
    var texto = 'Por favor verifíque la información que esta pendiente';
    $('#code').css( "border-color", "#efefef" );
    $('#review').css( "border-color", "#efefef" );

    var valid = 0;
    if($('#code').val() == "" ){
        $('#code').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if( $('#code').val().indexOf( " " ) > 0 ){
        $('#code').css( "border-color", "#b94a48" );
        texto = 'El campo de código no puede tener espacios, recomendamos separar las palabras con un guion ( - ) o guion bajo ( _ )';
        valid = 1;
    }
    if($('#review').val() == ""){
        $('#review').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: texto,
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}//fin valida

//===========================================================================
// Realiza la peticion ajax
//===========================================================================
function Operacion(msg){
    $.ajax({ url: 'controllers/evaluaciones.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}//fin operacion

//===========================================================================
// Cambia el tipo de evaluacion
//===========================================================================
function CambiaTipo(){
    event.preventDefault();
    if($('#type').val()=="1"){
        $('#VistaCargos').show();
        $('#VistaCursos').hide();
    }else{
        $('#VistaCargos').hide();
        $('#VistaCursos').show();
    }
}

//===========================================================================
// Consulta los datos de agrupaciones creadas para cada evaluacion
//===========================================================================
function consultarAgrupacion( review_id ){
    $.ajax({
        url:'controllers/evaluaciones.php?opcn=agrupacion&review_id='+review_id,
        type:'get',
        dataType: "json",
        success: function( data ){
            // console.log( data );
            if( data && data.resultado && data.resultado == "SI" ){
                arr_agrup = data.agrupaciones
                tabla = renderAgrupaciones(data.agrupaciones);
                $('.lista_agrupaciones').html( tabla );
                agregarSelectAgrup( '#select_agrup', '' );
                listarAgrupacionesDataList()
            }
        }
    });
}//fin consulta agrupaciones

//===========================================================================
// Pinta las agrupaciones en una tabla
//===========================================================================
function renderAgrupaciones( agrupaciones ){
    html = "";
    console.log( "ingreso a renderAgrupaciones" );
    agrupaciones.forEach( function( key, index ){
        html += `<tr class='linea_${key.review_dist_id}'>
            <td>${index+1}</td>
            <td>${key.agrup}</td>
            <td>${key.obligatorias}</td>
            <td>
                <label class='btn label label-success' onclick='editarAgrupacion( ${index} )'>editar</label>
                <label class='btn label label-danger' onclick='eliminarAgrupacion( ${index} )'>eliminar</label>
            </td>
        <tr>`;
    } );
    html += `<tr class=''>
        <td> - </td>
        <td><input class='form-control' type="text" placeholder="nuevo texto" id="agrup_new"></td>
        <td><input class='form-control' type="number" placeholder="numero preguntas" id="obligatorias_new"></td>
        <td><label class='btn label label-warning' onclick='nuevaAgrupacion()'>guardar</label></td>
    <tr>`;
    return html;
}//fin renderAgrupaciones

//===========================================================================
// Edita la agrupacion seleccionada
//===========================================================================
function editarAgrupacion( index ){
    console.log( arr_agrup[ index ] );
    key = arr_agrup[ index ];
    tr = `<td>${index+1}</td>
        <td><input class='form-control' type='text' value='${key.agrup}' id='new_agrup'></td>
        <td><input class='form-control' type='number' value='${key.obligatorias}' id='new_obligatorias'></td>
        <td>
            <label class='btn label label-warning' onclick='guardarNAgrupacion( ${index} )'>guardar</label>
            <label class='btn label label-primary' onclick='cancelar()'>cancelar</label>
        </td>`;
    $('.linea_'+key.review_dist_id).html( tr );
}

//===========================================================================
// Guarda los datos editados de una agrupacion
//===========================================================================
function guardarNAgrupacion( index ){
    key = arr_agrup[ index ];
    suma = 0;
    preg = parseInt( $( '#cant_question' ).val() );
    key.agrup = $('#new_agrup').val();
    key.obligatorias = $('#new_obligatorias').val();

    if( parseInt( $('#new_obligatorias').val() ) <= 0 || $('#new_obligatorias').val() == '' ){
        notyfy({
            text: 'No es un número válido',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return;
    }

    if( $('#new_agrup').val() == '' ){
        notyfy({
            text: 'El texto no puede estar en blanco',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return;
    }

    for ( a of arr_agrup ) {
        suma += parseInt(a.obligatorias);
    }

    if( suma > preg ){
        notyfy({
            text: 'El número de preguntas de las agrupaciones son mayores a la cantidad total de preguntas de la evaluación',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return;
    }

    //peticion ajax
    $.ajax({
        url:'controllers/evaluaciones.php',
        type:'post',
        data:{'opcn':'editarAgrupacion','datos':key },
        dataType: "json",
        success: function( data ){
            // console.log( data );
            if( data.resultado == "SI" ){
                $('.lista_agrupaciones').html( renderAgrupaciones( arr_agrup ) );
                agregarSelectAgrup( '#select_agrup', '' );
                listarAgrupacionesDataList()
            }
        }
    });//fin $.ajax
}//fin guardarNAgrupacion

//===========================================================================
// Guarda los datos de una nueva agrupacion
//===========================================================================
function nuevaAgrupacion(){
    preg = parseInt( $( '#cant_question' ).val() );
    suma = 0;
    for ( a of arr_agrup ) {
        suma += parseInt(a.obligatorias);
    }

    if( parseInt( $('#obligatorias_new').val() ) <= 0 || $('#obligatorias_new').val() == '' ){
        notyfy({
            text: 'No es un número válido',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return;
    }

    if( $('#agrup_new').val() == '' ){
        notyfy({
            text: 'El texto no puede estar en blanco',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return;
    }

    suma += parseInt( $('#obligatorias_new').val() );

    if( suma > preg ){
        notyfy({
            text: 'El número de preguntas de las agrupaciones son mayores a la cantidad total de preguntas de la evaluación',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return;
    }

    if( $('#agrup_new').val().indexOf(' ') > 0 ){
        agrupacion = $('#agrup_new').val().replace( ' ', '_' );
    }else{
        agrupacion = $('#agrup_new').val();
    }

    datos = {
        'opcn':'nuevaAgrupacion',
        'review_id': review_id,
        'agrup': agrupacion,
        'obligatorias': $('#obligatorias_new').val()
    };
    $.ajax({
        url: 'controllers/evaluaciones.php',
        type: 'post',
        data: datos,
        dataType: "json",
        success: function( data ){
            // console.log( data );
            if( data.resultado == "SI" ){
                arr_agrup = data.agrupaciones
                tabla = renderAgrupaciones(data.agrupaciones);
                $('.lista_agrupaciones').html( tabla );
                agregarSelectAgrup( '#select_agrup', '' );
                listarAgrupacionesDataList()
            }
            if( data.resultado == "NO" ){
                notyfy({
                    text: 'No ha sido posible crear la agrupación, recargue la página e intente nuevamente',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}//fin nuevaAgrupacion

//===========================================================================
// Elimina una agrupacion especifica
//===========================================================================
function eliminarAgrupacion( index ){
    datos = {
        'opcn':'eliminarAgrupacion',
        'review_dist_id': arr_agrup[index].review_dist_id
    };
    $.ajax({
        url: 'controllers/evaluaciones.php',
        type: 'post',
        data: datos,
        dataType: "json",
        success: function( data ){
            // console.log( data );
            if( data.resultado == "SI" ){
                arr_agrup.splice( index, 1 );
                tabla = renderAgrupaciones( arr_agrup );
                $('.lista_agrupaciones').html( tabla );
                agregarSelectAgrup( '#select_agrup', '' );
                listarAgrupacionesDataList()
            }
        }
    });
}//fin eliminarAgrupacion

function cancelar(){
    tabla = renderAgrupaciones( arr_agrup );
    $('.lista_agrupaciones').html( tabla );
}

function listarAgrupacionesDataList(){
    aux = "";
    for ( list of arr_agrup) {
        aux += `<option value="${list.agrup}">${list.agrup}</option>`;
    }
    $('#lista_desp_agrup').html( aux );
}

//=========================================================================
// Agrega las opciones de agrupacion al select correspondiente
//=========================================================================
function agregarSelectAgrup( selector, seleccionado ){
    aux = "";
    for (vari of arr_agrup) {
        seleccion = vari.agrup == seleccionado ? "selected" : "";
        aux += `<option value="${vari.agrup}" ${seleccion}>${vari.agrup}</option>`;
    }
    $(selector).html( aux );
    $(selector).select2({
        placeholder: "Seleccione la agrupación",
        allowClear: true
    });
}

//=========================================================================
// Obtiene todas las preguntas activas para una evaluación
//=========================================================================
function getQuestions( review_id ){
    console.log( "entro a getQuestions", review_id );
    $.ajax({
        url:'controllers/evaluaciones.php?opcn=questions&review_id='+review_id,
        type:'get',
        dataType: "json",
        success: function( data ){
            // console.log( "llego la info", data );
            if( data.resultado == "SI" ){
                questions = data.questions;
                tabla = renderQuestions(data.questions);
                $('#question_id').html( tabla );
                $("#question_id").select2({
                    placeholder: "Seleccione un estado",
                    allowClear: true
                });
            }
        }
    });
}//fin getQuestions

//=========================================================================
// Renderiza en html la lista del select con las opciones = questions
//=========================================================================
function renderQuestions( data ){
    html = "";
    tipo = "";
    for ( v of data ) {
        switch ( v.question_id ) {
            case 1: tipo = "Evaluación Abierta"; break;
            case 2: tipo = "Encuesta Abierta"; break;
            case 3: tipo = "Evaluación Curso"; break;
            default: tipo = "Encuesta Curso";
        }
        html += `<option value="${v.question_id}">${v.code} | ${v.question} | ${tipo} </option>`;
    }
    return html;
}//fin renderQuestions

//=========================================================================
// Obtiene las preguntas seleccionadas para la evaluación
//=========================================================================
function getPreguntasSel( review_id ){
    $.ajax({
        url:'controllers/evaluaciones.php?opcn=preguntasSel&review_id='+review_id,
        type:'get',
        dataType: "json",
        success: function( data ){
            // console.log( data );
            if( data && data.resultado && data.resultado == "SI" ){
                preguntasSel = data.preguntasSel
                tabla = renderPreguntasSel( preguntasSel );
                $('#list_preguntasSel').html( tabla );
            }
        }
    });
}//fin getPreguntasSel

//=========================================================================
// renderiza las preguntas seleccionadas de una evaluación
//=========================================================================
function renderPreguntasSel( preguntasSeleccionadas ){
    html = '';
    preguntasSeleccionadas.forEach( function( key, idx ){
        // console.log( "key", key, "idx", idx );
        if( key.YaRespondida == "NO" ){
            if( key.status_id == 1 ){
                boton = `<span class="btn label label-warning" onclick="inactivarPregunta( ${key.reviews_questions_id} )">Inactivar</span>`;
            }else{
                boton = `<span class="btn label label-success" onclick="activarPregunta( ${key.reviews_questions_id} )">Activar</span>`;
            }
            editar = `<span class="btn label label-primary" onclick="editarPregunta( ${ idx } )">Editar</span>`;
        }else if( key.YaRespondida == "SI" ){
            boton = `<span class="label label-info">Ya han respondido</span>`;
            editar = "";
        }
        html += `<tr id="pregunta_${key.reviews_questions_id}">
            <td> ${key.code} | ${key.question} </td>
            <td>${key.first_name} ${key.last_name}</td>
            <td>${key.value}</td>
            <td>${key.agrup}</td>
            <td>
                <div id="boton_${key.reviews_questions_id}">${ boton }</div>
                <div>${ editar }</div>
            </td>
        </tr>`;
    } );
    return html;
}//fin renderPreguntasSel

//=========================================================================
// Inactiva una pregunta seleccionada para una evaluación
//=========================================================================
function inactivarPregunta( reviews_questions_id ){
    datos = {
        'reviews_questions_id':reviews_questions_id,
        'status_id':2,
        'opcn':'estadoPregunta'
    };
    $.ajax({
        url:'controllers/evaluaciones.php',
        type:'post',
        data: datos,
        dataType: "json",
        success: function( data ){
            if( data && data.resultado == "SI" ){
                $( '#boton_'+reviews_questions_id ).html( `<span class="btn label label-success" onclick="activarPregunta( ${reviews_questions_id} )">Activar</span>` );
                preguntasSel.forEach( function( key, idx ){
                    if( key.reviews_questions_id == reviews_questions_id ){
                        preguntasSel[idx].status_id = 2;
                    }
                } );
            }else{
                notyfy({
                    text: 'No ha sido posible inactivar la pregunta, recargue la página e intente nuevamente',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
    console.log( "inactivarPregunta", reviews_questions_id );
}//fin inactivarPregunta

//=========================================================================
// Obtiene las preguntas seleccionadas para la evaluación
//=========================================================================
function activarPregunta( reviews_questions_id ){
    datos = {
        'reviews_questions_id':reviews_questions_id,
        'status_id':1,
        'opcn':'estadoPregunta'
    };
    $.ajax({
        url:'controllers/evaluaciones.php',
        type:'post',
        data: datos,
        dataType: "json",
        success: function( data ){
            if( data && data.resultado == "SI" ){
                $( '#boton_'+reviews_questions_id ).html( `<span class="btn label label-warning" onclick="inactivarPregunta( ${reviews_questions_id} )">Inactivar</span>` );
                preguntasSel.forEach( function( key, idx ){
                    if( key.reviews_questions_id == reviews_questions_id ){
                        preguntasSel[idx].status_id = 1;
                    }
                } );
            }else{
                notyfy({
                    text: 'No ha sido posible activar la pregunta, recargue la página e intente nuevamente',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
    console.log( "activarPregunta", reviews_questions_id );
}//fin activarPregunta

//=========================================================================
// Modifica la pregunta seleccionada para permitir editarla
//=========================================================================
function editarPregunta( idx ){
    console.log( "editarPregunta", idx, preguntasSel[ idx ] );
    preg = preguntasSel[ idx ];
    l_options = "";
    for ( qst of questions ) {
        l_options += `<option value="${ qst.question_id }" ${ qst.question_id == preg.question_id ? 'selected' : '' }>${ qst.question }</option>`;
    }
    l_agrups = "";
    for ( agr of arr_agrup ) {
        l_agrups += `<option value="${ agr.agrup }" ${ agr.agrup == preg.agrup ? 'selected' : '' }>${ agr.agrup }</option>`;
    }
    html = `<td>
            <select id="edit_preg" style="width:70%"></select>
        </td>
        <td>${preg.first_name} ${preg.last_name}</td>
        <td>
            <input class="form-control" id="edit_value" value="${preg.value}">
        </td>
        <td>
            <select id="edit_agrup" style="width:100%"></select>
        </td>
        <td>
            <div id="">
                <span class="btn label label-warning" onclick="guardarCambios( ${ preg.reviews_questions_id } )">Guardar</span>
            </div>
            <div>
                <span class="btn label label-default" onclick="cancelarCambios( ${ idx } )">Cancelar</span>
            </div>
        </td>`;
    $('#pregunta_'+preg.reviews_questions_id ).html( html );
    $('#edit_agrup' ).html( l_agrups );
    $('#edit_preg' ).html( l_options );
    $("#edit_preg").select2({
        placeholder: "Seleccione una pregunta",
        allowClear: true
    });
    $("#edit_agrup").select2({
        placeholder: "Seleccione una agrupacion",
        allowClear: true
    });
}

//=========================================================================
// Guarda los cambios hechos en la base de datos
//=========================================================================
function guardarCambios( reviews_questions_id ){
    datos = {
        'opcn':'editarPreguntaSel',
        'reviews_questions_id':reviews_questions_id,
        'agrup': $('#edit_agrup').val(),
        'value': $('#edit_value').val(),
        'question_id': $('#edit_preg').val()
    };
    $.ajax({
        url:'controllers/evaluaciones.php',
        type:'post',
        data: datos,
        dataType: "json",
        success: function( data ){
            if( data && data.resultado == "SI" ){
                preguntasSel.forEach( function( key, idx ){
                    if( key.reviews_questions_id == reviews_questions_id ){
                        preguntasSel[idx] = data.reviews_questions;
                    }
                } );
                tabla = renderPreguntasSel( preguntasSel );
                $('#list_preguntasSel').html( tabla );
            }else{
                notyfy({
                    text: 'No ha sido posible modificar la pregunta, recargue la página e intente nuevamente',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
    console.log( "se guardan cambios en", datos );
}//fin guardarCambios

//=========================================================================
// Cancela la edicion de un campo
//=========================================================================
function cancelarCambios( idx ){
    console.log( "se cancelan cambios en", idx );
    html = "";
    key = preguntasSel[ idx ];
    if( key.YaRespondida == "NO" ){
        if( key.status_id == 1 ){
            boton = `<span class="btn label label-warning" onclick="inactivarPregunta( ${key.reviews_questions_id} )">Inactivar</span>`;
        }else{
            boton = `<span class="btn label label-success" onclick="activarPregunta( ${key.reviews_questions_id} )">Activar</span>`;
        }
        editar = `<span class="btn label label-primary" onclick="editarPregunta( ${ idx } )">Editar</span>`;
    }else if( key.YaRespondida == "SI" ){
        boton = `<span class="label label-info">Ya han respondido</span>`;
        editar = "";
    }
    html += `<td> ${key.code} | ${key.question} </td>
        <td>${key.first_name} ${key.last_name}</td>
        <td>${key.value}</td>
        <td>${key.agrup}</td>
        <td>
            <div id="boton_${key.reviews_questions_id}">${ boton }</div>
            <div>${ editar }</div>
        </td>`;
        $( '#pregunta_'+key.reviews_questions_id ).html( html );
}
