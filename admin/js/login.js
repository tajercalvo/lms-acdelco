$(document).ready(function(){
	$('#Error_gral').css( "border-color", "#b94a48" );
	$('#Error_gral').hide();
});
function validateForm(){
	$('#Error_gral').hide();
	$('#username').css( "border-color", "#efefef" );
	$('#password').css( "border-color", "#efefef" );
	var valid = 0;
	if($('#username').val() == ""){
		$('#username').css( "border-color", "#b94a48" );
		valid = 1;
	}else if($('#password').val() == ""){
		$('#password').css( "border-color", "#b94a48" );
		valid = 1;
	}
	if(valid == 0){
		return true;
	}
	else{
		return false;
	}
}