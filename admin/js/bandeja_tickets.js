( function(){
    angular.module( 'ludusApp', ['ngRoute' ] )
    .controller( 'bandejaTicketsCtrl', [ '$scope', 'Tickets', function( $scope, Tickets ){

        var b = this;
        $scope.pagina   = 1;
        $scope.seleccion= 0;
        $scope.imagen = "imagen.jpg";
        b.seccion       = "Bandeja Entrada";
        b.respuesta     = {
            ticket          : 0,
            textoRespuesta  : "",
            imagen          : {}
        };
        b.objFiltro = {
            nombre : "",
            fecha : ""
        };
        b.nuevosCheck   = true;
        b.cerradosCheck = false;
        b.ticket        = Tickets;
        b.ticket_sel    = {};
        b.mPendientes   = "active";
        b.imagen        = "";

        b.bandeja = function( menu, activate ){
            b.mPendientes   = "";
            b.mBandeja      = "";
            b.mCerrados     = "";
            b.mReabiertos   = "";
            b.seccion       = menu;
            b[activate]     = "active";
            $scope.pagina   = 1;
            b.cargarDatos( $scope.pagina, b.seccion );
        }

        b.cargarDatos = function( pagina, seccion ){
            b.ticket.loadTickets( pagina, seccion ).then( function( data ){
                b.ticket_sel = b.ticket.tickets[0];
                //console.log("tickets ", b.ticket );
            } );
        }

        b.abrirModal = function( id ){
            b.respuesta.ticket = id;
            $('#modalCerrarTicket').modal();
        }

        b.cerrarTicket = function(){
            b.ticket.cerrarTicket( b.respuesta ).then( function( data ){
                console.log( "data", data );
                if( data == "si" ){
                    $('#modalCerrarTicket').modal('hide');
                    $( '#limpiar' ).trigger( "click" );
                    b.respuesta     = {
                        ticket          : 0,
                        textoRespuesta  : "",
                        imagen          : {}
                    };
                    b.cargarDatos( $scope.pagina, b.seccion );
                }
            } );
        }//fin cerrarTicket

        b.abrirModalImagen = function( image ){
            b.imagen = image;
            $('#ModalImagen').modal();
        }

        b.filtrar = function(){
            if( b.objFiltro.nombre == "" && b.objFiltro.fecha == "" ){
                b.cargarDatos( $scope.pagina, b.seccion );
            }else{
                b.ticket.filtrarTickets( b.objFiltro ).then( function( data ){
                    b.ticket_sel = b.ticket.tickets[0];
                } );
                b.objFiltro = {
                    nombre : "",
                    fecha : ""
                };
            }
        }

        b.cambiarSeleccion = function( id ){
            b.ticket_sel = b.ticket.tickets[id];
        }

        b.siguiente = function(){
            $scope.pagina++;
            b.cargarDatos( $scope.pagina, b.seccion );
        }

        b.anterior = function(){
            var num_anterior = $scope.pagina - 1;
            if( num_anterior > 0 ){
                $scope.pagina = num_anterior ;
                b.cargarDatos( $scope.pagina, b.seccion );
            }
        }

        b.cargarDatos( $scope.pagina, b.seccion );

    } ] )
    .factory( 'Tickets', [ '$http', '$q', function( $http, $q ){
        var self = {
            cargando    : false,
            num_tickets : 0,
            pendientes  : 0,
            reabiertos  : 0,
            totalPaginas: 0,
            totalBandeja: 0,
            tickets     : {}
        };

        self.loadTickets = function( pagina, seccion ){
            self.cargando = true;
            var q = $q.defer();
            $http.get("controllers/tickets.php?opcn=tickets&pagina="+pagina+"&seccion="+seccion).then( function( data ){
                //console.log("data", data);
                self.num_tickets    = data.data.numero_tickets;
                self.pendientes     = data.data.pendientes;
                self.reabiertos     = data.data.reabiertos;
                self.tickets        = data.data.tickets;
                self.totalBandeja   = data.data.total_bandeja;
                self.cargando       = false;
                q.resolve();
            } );
            return q.promise;
        }
        self.filtrarTickets = function( filtro ){
            self.cargando = true;
            var q = $q.defer();
            $http.get("controllers/tickets.php?opcn=filtro&fecha="+filtro.fecha+"&dato="+filtro.nombre).then( function( data ){
                //console.log("data", data);
                self.num_tickets    = data.data.numero_tickets;
                self.pendientes     = data.data.pendientes;
                self.reabiertos     = data.data.reabiertos;
                self.tickets        = data.data.tickets;
                self.totalBandeja   = data.data.total_bandeja;
                self.cargando       = false;
                q.resolve();
            } );
            return q.promise;
        }

        self.cerrarTicket = function( ticket ){
            self.cargando = true;
            var q = $q.defer();
            var fdata = new FormData();
            var header_obj = {
                headers: { 'Content-type' : undefined },
                transformRequest : angular.indentity
            }
            fdata.append('ticket',ticket.ticket);
            fdata.append('textoRespuesta',ticket.textoRespuesta);
            fdata.append('imagen',ticket.imagen);
            $http.post( 'controllers/tickets.php?opcn=marcar', fdata, header_obj ).then( function( data ){
                self.cargando = false;
                var resultado = "";
                if( data.data.resultado == "si" ){
                    resultado = "si";
                }
                q.resolve( resultado );
            } );
            return q.promise;
        }

        return self;
    } ] )

    .directive( 'uploaderModel', [ '$parse', function( $parse ){
        return {
            restrict : 'A',
            link: function( scope, iElement, iAttrs ){
                iElement.on( 'change', function( e ){
                    $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0] );
                } );
            }
        };
    } ] )

    .filter( 'mensajecorto', function(){
        return function( mensaje ){
            if( mensaje ){
                if( mensaje.length > 70 ){
                    return mensaje.substr( 0, 70 ) + "...";
                }else{
                    return mensaje;
                }
            }
        }//fin funcion para recortar un mensaje si es muy largo
    } )
} )();

$(document).ready( function(){
    // $('.textarea').wysihtml5();
    console.log("todo listo");
    $('#fechaFiltro').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
} );
