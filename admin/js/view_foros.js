$("#btn_inscription").hide();
var CameraMov;
var SAMPLE_SERVER_BASE_URL = 'https://www.gmacademy.co/assets/vct/';
if ($('#sessionId').length) {
    sessionId = $('#sessionId').val();
    var apiKey = '46054122';
    //sessionId = '1_MX40NjA1NDEyMn5-MTU1MDAxNjczMzczOH5id05PUVpDZTZyLzVPeklSOTh6cjlrQjl-fg';
    // token = 'T1==cGFydG5lcl9pZD00NjA1NDEyMiZzaWc9Zjk5MTMyMmE5MGQ3YTA2MTExNzYxNWY5ZmVkZDMwZmI4NmZhMzI4ZDpzZXNzaW9uX2lkPTFfTVg0ME5qQTFOREV5TW41LU1UVTFNREF4Tmpjek16Y3pPSDVpZDA1UFVWcERaVFp5THpWUGVrbFNPVGg2Y2psclFqbC1mZyZjcmVhdGVfdGltZT0xNTUwMDE2NzQzJm5vbmNlPTAuNDQ2NzYxMzc3MDE2MDg1NjUmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTU1MjYwNTE0MiZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==';
    token = '';

    if( $('#token').length ){
        token = $('#token').val();
    }
    // Replace this with the ID for your Chrome screen-sharing extension, which you can
    // get at chrome://extensions/:
    var extensionId = 'ijgfggkmnahehfeimghpomadjmmkiphd';
    //06a080e7f6e1baf9badbb5cfbabecb28bec6666a
    // If you register your domain with the the Firefox screen-sharing whitelist, instead of using
    // a Firefox screen-sharing extension, set this to the Firefox version number, such as 36, in which
    // your domain was added to the whitelist:
    var ffWhitelistVersion; // = '36';
    var session = OT.initSession(apiKey, sessionId);
    session.connect(token, function(error) {
        if (error) {
            alert('Error connecting to session: ' + error.message);
            return;
        }
        if ($('#teacher_id').length) {
            //console.log("inicio transmisión");
            // publish a stream using the camera and microphone:
            var publisher = OT.initPublisher('camera-publisher', {
                width: '100%',
                height: '200px'
            });
            session.publish(publisher);
        }
    });
    session.on('streamCreated', function(event) {
        if (event.stream.videoType === 'screen') {
            // This is a screen-sharing stream published by another client
            var subOptions = {
                insertMode: "append",
                width: '100%',
                height: '600px'
            };
            session.subscribe(event.stream, 'screen-subscriber', subOptions);
            $('#camera-subscriber').css('top', '-230px');
            $('#camera-subscriber').css('left', '-27px');
        } else {
            // This is a stream published by another client using the camera and microphone
            session.subscribe(event.stream, 'camera-subscriber', {
                insertMode: "append",
                width: '100%',
                height: '200px'
            });
            // CameraMov = setInterval(CambiaDrag,2000);
        }
    });
    // For Google Chrome only, register your extension by ID,
    // You can find it at chrome://extensions once the extension is installed
    OT.registerScreenSharingExtension('chrome', extensionId, 2);

    function screenshare() {
        OT.checkScreenSharingCapability(function(response) {
            //console.info(response);
            if (!response.supported || response.extensionRegistered === false) {
                alert('This browser does not support screen sharing.');
            } else if (response.extensionInstalled === false &&
                (response.extensionRequired || !ffWhitelistVersion)) {
                alert('Please install the screen-sharing extension and load this page over HTTPS.');
            } else if (ffWhitelistVersion && navigator.userAgent.match(/Firefox/) &&
                navigator.userAgent.match(/Firefox\/(\d+)/)[1] < ffWhitelistVersion) {
                alert('For screen sharing, please update your version of Firefox to ' +
                    ffWhitelistVersion + '.');
            } else {
                // Screen sharing is available. Publish the screen.
                // Create an element, but do not display it in the HTML DOM:
                var screenContainerElement = document.createElement('div');
                var screenSharingPublisher = OT.initPublisher(
                    screenContainerElement, { videoSource: 'screen' },
                    function(error) {
                        if (error) {
                            alert('Something went wrong: ' + error.message);
                        } else {
                            console.log("Aqui deberia publicar");
                            session.publish(
                                screenSharingPublisher,
                                function(error) {
                                    if (error) {
                                        alert('Something went wrong: ' + error.message);
                                    }
                                });
                        }
                    });
            }
        });
        // CameraMov = setInterval(CambiaDrag,2000);
    }
}

$('#btn_compartir').click(function() {
    screenshare();
});
/*Para transmisión*/

$('#finalizar_trasmision').click(function() {
     $.ajax({
        url: 'controllers/foros_ventas.php',
        data: { opcn: 'finalizar_trasmision', conference_id: $.getURL('id') },
        dataType: 'JSON',
        type: 'POST'
    })
     .done(function(data) {
       //console.log(data);
        alert("transmision finalizada");

        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
});
/**
*
*
*
Diapositivas
*
*
*
**/
//=================================================================
// Modulo para asistentes de foros
//=================================================================
var intervalo = 0,
    presentacion = [],
    imagenes = '',
    _actual = 0;

function consultarAsistencia() {

    $.ajax({
        url: 'controllers/foros_ventas.php',
        data: { opcn: 'assist', conference_id: $.getURL('id') },
        dataType: 'JSON',
        type: 'POST'
    }).done(function(data) {

        if (data && data.asistentes > 0) {
            $("#Asistentes").html("[ " + data.asistentes + " Asistentes ]");
        }
    });
}
//==========================================================================
// consulta las imagenes seleccionadas para un foro
//==========================================================================
var cant_img = 0;
function consultarImagenes(var_conference_id) {

    $.ajax({
            url: 'controllers/foros_ventas.php',
            data: { opcn: 'consultarImagenes', conference_id: var_conference_id },
            dataType: 'JSON',
            type: 'POST'
        })
        .done(function(data) {
            if (!data.error) {
                var _actual = -1;
                // imagenes = data.imagenes;
                presentacion = data.imagenes;

                cant_img = presentacion.length;

                presentacion.forEach(function(key, idx) {
                    if (key.idx == 1) {
                        _actual = idx;
                    }
                });

                if (_actual >= 0) {
                    sig_ant = _actual;
                    if( data.estado == 1 ){
                        renderImagen(_actual);
                    }

                    if ($('#tipo').val() == 'instructor' || data.estado == 9 ) {
                        $('#lista_diapositivas').html(carreteImagenes(data.imagenes));
                    }
                }
            }
        });
} //fin consultarImagenes
//==========================================================================
// actualiza la imagen seleccionada
//==========================================================================
id_actual = 0;
function renderImagen(indice) {
    var imagen_sel = presentacion[indice];

    if( id_actual == indice){ return false;}else{
        id_actual = indice;
    }
    if ( imagen_sel.imagen.indexOf(".jpg") > -1 ) {
        $('#imagen_presentacion_act').html('<img id="imagen_presentacion_act" src="../assets/FilesForos/'+imagen_sel.imagen+'" alt="presentación" class="img-responsive">');
    } else if ( imagen_sel.imagen.indexOf(".mp4") > -1 ) {
         $('#imagen_presentacion_act').html('<video width="100%" controls ><source src="../assets/FilesForos/'+imagen_sel.imagen+'" type="video/mp4" ></video>')
    }
    // $('#imagen_presentacion_act').attr('src', "../assets/FilesForos/" + imagen_sel.imagen);
    $('#imagen_' + imagen_sel.conference_file_id).addClass('active');
} //fin renderImagen
//==========================================================================
// Renderiza las imagenes seleccionadas para el foro
//==========================================================================
function carreteImagenes(obj) {

    lista_diapositivas = "";
    obj.forEach(function(key, idx) {

         if (key.imagen.indexOf(".mp4")> -1 ) {
            var activo = key.idx == 1 ? "active" : "";
        lista_diapositivas += `<a class="diapositivas carrusel_imagenes ${activo}" id="imagen_${key.conference_file_id}" onclick=' seleccionarImagen( ${ idx } ) '>
                                    <img class="img-responsive" src="../assets/FilesForos/Video.jpg" style="width: 200px;height: 113px;" >
                                </a>`;
            }else{
                var activo = key.idx == 1 ? "active" : "";
                lista_diapositivas += `<a class="diapositivas carrusel_imagenes ${activo}" id="imagen_${key.conference_file_id}" onclick=' seleccionarImagen( ${ idx } ) '>
                                         <img class="img-responsive" src="../assets/FilesForos/${key.imagen}" style="width: 200px;height: 113px;" >
                                     </a>`;
            }
    });
    return lista_diapositivas;
} //fin carreteImagenes
//==========================================================================
// Actualiza la imagen seleccionada
//==========================================================================
var sig_ant = 0;
function seleccionarImagen(idx) {


    sig_ant = idx;

    var imagen = presentacion[idx];

    if ( imagen.imagen.indexOf(".jpg") > -1 ) {
        $('#imagen_presentacion_act').html('<img id="imagen_presentacion_act" src="../assets/FilesForos/'+imagen.imagen+'" alt="presentación" class="img-responsive">');
    } else if ( imagen.imagen.indexOf(".mp4") > -1 ) {
         $('#imagen_presentacion_act').html('<video width="100%" controls autoplay><source src="../assets/FilesForos/'+imagen.imagen+'" type="video/mp4" ></video>')
    }


    $('.carrusel_imagenes').removeClass('active');
    $('#imagen_' + imagen.conference_file_id).addClass('active');

    if( $('#tipo').val() == 'alumno' ){  return false;}

    $.ajax({
            url: 'controllers/foros_ventas.php',
            data: { opcn: 'cambiarImagenIdx', conference_file_id: imagen.conference_file_id, conference_id: imagen.conference_id },
            type: 'post',
            dataType: 'json'
        })
        .done(function(data) {
            $('#imagen_presentacion_act').attr('src', "../assets/FilesForos/" + imagen.imagen);
        });
} //fin seleccionarImagen

    document.onkeydown = onTecla;
    function onTecla(e){
        var num = e?e.keyCode:event.keyCode;

        if(num == 39){

            if( sig_ant == (cant_img - 1 ) ){return false;}
            sig_ant = sig_ant + 1;
            seleccionarImagen( sig_ant);
        }

        if(num == 37){

            if( sig_ant == 0 ){return false;}
            sig_ant = sig_ant - 1;
            seleccionarImagen( sig_ant);
        }

    }

function editarHilo() {
    $('#form_Editar').submit(function(e) {
        e.preventDefault();
        var datos = $(this).serialize();
        $.ajax({
                url: 'controllers/foros_ventas.php',
                dataType: 'json',
                data: datos,
                type: 'POST'
            })
            .done(function(data) {
                $('#ModalEditar').modal('hide');
                if (!data.error) {
                    notyfy({
                        text: 'Se modifico correctamente la comunicación',
                        type: 'success' // alert|error|success|information|warning|primary|confirm
                    });
                }
            });
    });
}

function iniciarSelects() {
    $('#cargos').select2({ placeholder: "Seleccione los cargos", allowClear: true });
    $('#estado').select2({ placeholder: "Seleccione un estado", allowClear: true });
    $('#category').select2({ placeholder: "Seleccione la categoria", allowClear: true });
    $('#tipo_cargue').select2({ placeholder: "Seleccione el tipo de cargue", allowClear: true });
}
//==========================================================================
// Carga las imagenes para ser usadas en la plataforma
//==========================================================================
function Init() {

    // NProgress.configure({ parent: '.loader_s' });
    consultarImagenes($.getURL('id'));
    // cargarImagenes();
    iniciarSelects();
    editarHilo();

    if ($('#tipo').val() == 'instructor') {
        setInterval(consultarAsistencia, 15000);
    } else {
        // console.log("ingreso como asistente");
        intervalo = setInterval(function() {
            consultarImagenes($.getURL('id'));
        }, 4000);
        setInterval(consultarAsistencia, 600000);
    }


    //Inicia la transmision
    // iniciar();

    //Guarda las imagenes seleccionadas en el servidor
    // nuevaGaleria();

} //fin Init


/**
*
*
*
CHAT
*
*
*
**/

var Timer_Chat;
var Timer_Asist;

$(document).ready(function() {
    Init();
    if ($("#ElDeBajar").length > 0) {
        $("#ElDeBajar").animate({ scrollTop: $('#ElDeBajar')[0].scrollHeight }, 1000);
    }
    $.ajaxSetup({ "cache": false });
    Timer_Chat = setInterval(CargaNuevosReply, 40000);
    // Timer_Asist = setTimeout(CargaAsistentes,50000);
    $("#botonChat").click(function() {
        if ($('#comentario').val() != "") {
            var datos = $("#FormularioComentarios").serialize();
            $.ajax({
                url: "controllers/foros_ventas.php",
                data: datos,
                type: "post",
                dataType: "json",
                success: function(data) {
                    var res0 = data.resultado;
                    var MaxIdN = data.MaxId;
                    //$("#MaxIdReply").val(MaxIdN);
                    var Comentario_Text = $('#comentario').val();
                    $('#comentario').val('');
                    $('#MensajesForo').append('<div class="media border-bottom margin-none"><div class="media-body"><small class="label label-default">Escribiste:</small> <span> ' + Comentario_Text + '</span><br></div></div>');
                    $('#MensajesForo').append('<input type="hidden" class="MaxIdReply" name="MaxIdReply" id="MaxIdReply" value="' + MaxIdN + '">');
                    $("#ElDeBajar").animate({ scrollTop: $('#ElDeBajar')[0].scrollHeight }, 1000);
                }
            });
        } else {
            notyfy({
                text: 'Para enviar un mensaje debe escribirlo!',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    });
});

function ProcesoPost(conference_reply_id, status_id) {
    var data = new FormData();
    data.append('conference_reply_id', conference_reply_id);
    data.append('status_id', status_id);
    data.append('opcn', 'ProcesoAct');
    var url = "controllers/foros_ventas.php";
    $.ajax({
        url: url,
        type: 'post',
        contentType: false,
        data: data,
        processData: false,
        dataType: "json",
        cache: false,
        success: function(data) {
            if (data.resultado == "SI") {
                notyfy({
                    text: 'Se ha realizado la activación del post, refresca la página y lo verás listo! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes' + conference_reply_id).text());
                $('#CantLikes' + conference_reply_id).text(Cantlikes + 1);
            } else {
                notyfy({
                    text: 'No se ha podido llevar a cabo la activación, intenta nuevamente',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function CargaNuevosReply() {
    // clearInterval(Timer_Chat);

    if ($('#MaxIdReply').length) {
        var conference_id = $('#conference_id').val();
        //var MaxIdReply = $('#MaxIdReply').val();
        var MaxIdReply = $(".MaxIdReply").last().val(); //id del ultimo comentario en el chat
        $.ajax({
                url: 'controllers/foros_ventas.php',
                type: 'post',
                dataType: 'json',
                data: { opcn: 'comments', conference_id: conference_id, MaxIdReply: MaxIdReply }
            })
            .done(function(datos) {
                // console.info( datos );
                if (datos && datos.mensajes != "") {
                    $("#MensajesForo").append(datos.mensajes);
                    $("#ElDeBajar").animate({ scrollTop: $('#ElDeBajar')[0].scrollHeight }, 1000);
                }

            });
        // Timer_Chat = setTimeout(CargaNuevosReply,3000);
    }
}

$('#btnEnSala').click(function() {
    // var key = e.which;
    var numEnSala = $('#enSala').val();
    var conference_id = $('#conference_id').val();
    // $(this).prop('disabled', true);
    $.post('controllers/foros_ventas.php', { opcn: 'suma', conference_id: conference_id, numEnSala: numEnSala }, function(datos, status, xhr) {
        if (status == 'success' && datos != '') {
            $("#Asistentes").html("[ Cargando ]");
            //$("#ElDeBajar").animate({ scrollTop: $('#ElDeBajar')[0].scrollHeight}, 1000);
        }
        // $('#enSala').prop('disabled', false);
    });
});