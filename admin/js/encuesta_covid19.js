$(document).ready(function() {
	
	 $("#mot_salida").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
	
});

$('#mot_salida').change(function(event) {
	/* Act on the event */
	if( $('#mot_salida').val() == 'Otros' ){
    	$('#opc_otros').prop({ 'required': 'required' })
    	$('.otros').css('border-color', '#ab2b3e');
    	//alert('si es otro')
    }else{
    	$('.otros').css('border-color', '');
    	$("#opc_otros").removeAttr("required")
    }
});

 // $("#submit").on("click", function() {
 
 //  		 ContestarEvaluacion();
 //        var condiciones = $("#check13").is(":checked");
 //        if (!condiciones) {
 //            // alert("Debe aceptar las condiciones");
          
 //        }
 //    });

  $("#atras").on("click", function() {
 	     $('.sesion1').css({display: 'block'});
         $('#sesion2').css({display: 'none'});
  		 $("#atras").css('display', 'none');
  		 $('#Siguiente').css('display','block')
  		 $('#submit').css('display', 'none');
    });

$('#Siguiente').click(function(event) {
	event.preventDefault();

	 
	if( $('#mot_salida').val() == ''){
      notyfy({
          text: 'Seleccione el Motivo de la Salida',
          type: 'warning'
      });
      return;
    }
    
    var CantCorrectas = 0;
	var valida1 = 0;
	var valida2 = 0;
	var valida3 = 0;
	var valida4 = 0;
	var valida5 = 0;
	var valida6 = 0;
	var valida7 = 0;
	var valida8 = 0;
	var valida9 = 0;
	var valida10 = 0;
	var valida11 = 0;
	var valida12 = 0;
	var valida13 = 0;


	if($('#opc_1_1').prop('checked')){
		valida13 = 1;		
	}else if($('#opc_1_2').prop('checked')){
		valida13 = 1;
	}else if($('#opc_1_3').prop('checked')){
		valida13 = 1;
	}else if($('#opc_1_4').prop('checked')){
		valida13 = 1;
	}else if($('#opc_1_5').prop('checked')){
		valida13 = 1;
	}

	if($('#check1_1').prop('checked')){
		valida1 = 1;		
	}else if($('#check1_2').prop('checked')){
		valida1 = 1;
	}

	if($('#check2_1').prop('checked')){
		valida2 = 1;
	}else if($('#check2_2').prop('checked')){
		valida2 = 1;
		CantCorrectas++;
	}

	if($('#check3_1').prop('checked')){
		valida3 = 1;
	}else if($('#check3_2').prop('checked')){
		valida3 = 1;
		CantCorrectas++;
	}

	if($('#check4_1').prop('checked')){
		valida4 = 1;
	}else if($('#check4_2').prop('checked')){
		valida4 = 1;
		
	}

	if($('#check5_1').prop('checked')){
		valida5 = 1;
	}else if($('#check5_2').prop('checked')){
		valida5 = 1;
		
	}

	if($('#check6_1').prop('checked')){
		valida6 = 1;
	}else if($('#check6_2').prop('checked')){
		valida6 = 1;
		
	}

	if($('#check7_1').prop('checked')){
		valida7 = 1;
	}else if($('#check7_2').prop('checked')){
		valida7 = 1;
		
	}

   if($('#check8_1').prop('checked')){
		valida8 = 1;
	}else if($('#check8_2').prop('checked')){
		valida8 = 1;
		
	}

	if($('#check9_1').prop('checked')){
		valida9 = 1;
	}else if($('#check9_2').prop('checked')){
		valida9 = 1;
		
	}
	if($('#check10_1').prop('checked')){
		valida10 = 1;
	}else if($('#check10_2').prop('checked')){
		valida10 = 1;
		
	}
	if($('#check11_1').prop('checked')){
		valida11 = 1;
	}else if($('#check11_2').prop('checked')){
		valida11 = 1;
		
	}
	if($('#check12_1').prop('checked')){
		valida12 = 1;
	}else if($('#check12_2').prop('checked')){
		valida12 = 1;
		
	}

	
	if(valida1>0 && valida2>0 && valida3>0 && valida4>0 && valida5>0 && valida6>0 && valida7>0 && valida8>0 && valida9>0 && valida10>0 && valida11>0 && valida12>0 && valida13>0){
		
		 $('.sesion1').css({display: 'none'});
         $('#sesion2').css({display: 'block'});
        
		 $('#submit').css('display', 'block');
		 $('#atras').css('display', 'block');
		 $('#Siguiente').css('display','none')
	}else{
		alert('No ha respondido todas las preguntas, por favor verifíque e intente nuevamente.')
		return;
	}


	
});


$('#frm_covid').submit(function(event) {
	event.preventDefault();

	   var condiciones = $("#check13").is(":checked");
        if (!condiciones) {
            alert("Debe aceptar las condiciones");
         	return;
        }

     info = $(this).serializeArray();
	 info.push({name:'opcn',value:'Insertar'})

	//console.log(datos)
	$.ajax({
		url: 'controllers/encuesta_covid19.php',
		type: 'POST',
		dataType: 'JSON',
		data: info,
	})
	.done(function() {
		console.log("success");
		alert('Encuesta Enviada');
		location.reload();
	})
	
});



