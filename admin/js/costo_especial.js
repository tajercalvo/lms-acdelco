$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/costo_especial.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "sProcessing": "procesando información...",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});

$(document).ready(function(){
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });

    $("#dealer_id").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });

    $("#tipo").select2({
        placeholder: "Seleccione una sesion",
        allowClear: true
    });

    $("#schedule_id").select2({
        placeholder: "Seleccione una sesion",
        minimumInputLength: 1,            
        allowClear: true,
        ajax: {
            quietMillis: 150,
            url: 'controllers/costo_especial.php',
            type:'POST',
            dataType: 'json',
            data: function (term, page) {
                //console.log("Valor:"+term+" y Page:"+page);
                return {
                    fecha_inicio: $("#start_date_day").val(),
                    fecha_fin: $("#end_date_day").val(),
                    searchTerm: term,
                    opcn: "consulta_sesion"
                };
            },
            delay: 250,
            results: function (data) {
                return { results: data };
            }
        }
    }); 


});

//CARGA LOS CONCESIONARIOS POR SESION EN LA LISTA DESPLEGABLE
$('#schedule_id').change( function(){

    $("#dealer_id").select2("val", "");
    var schedule_id = $(this).val();

    data = { 
        'opcn':'sel_dealer',
        'schedule_id': $(this).val() 
    }
    //console.log(data);
    cargar_datos_dealer( data );

} );

function cargar_datos_dealer( datos ){

    data = datos;

    $.ajax( {
        url         :'controllers/costo_especial.php',
        type        :'POST',
        data        : data,
        dataType    : 'json'
    } )
    .done( function(data) {

        var cantidadaDatos = data.length;
        var linea_html = "";
        if (cantidadaDatos == 0) {
            linea_html += "<option value='0'>No existe concesionarios para esta sesion</option>";
            $("#dealer_id").html(linea_html);
        }
        else{
           $.each(data, function (key, val) {
                linea_html += "<option value='"+val.dealer_id+"' >"+val.dealer+"</option>";
           });       
           $("#dealer_id").html(linea_html);
        }     
       
    });

}

$('#tipo').change( function(){

    var tipo_ajuste = $(this).val();

    if ( tipo_ajuste == 1) { 
        $( "#sesiones" ).show(); 
        $( "#fecha_final" ).show(); 
        $('label[for="start_date_day"]').text("Fecha Inicial Sesion:");

        $("#dealer_id").select2('data', {id: 0, text: '' });
        $("#schedule_id").select2('data', {id: 0, text: '' });

    }
    if ( tipo_ajuste == 2) { 
        $( "#sesiones" ).hide(); 
        $( "#fecha_final" ).hide(); 
        $('label[for="start_date_day"]').text("Fecha Ajuste Mes:");

        $("#dealer_id").select2("val", "");

        data = { 'opcn':'sel_all_dealer' }
        cargar_datos_dealer( data );
    }   

} );


$( "#formulario_data" ).submit(function( event ) {
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('crear');
        }if($('#opcn').val()=="editar"){
            Operacion('editar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        
    }
    event.preventDefault();
});

function Operacion(msg){
    $.ajax({ url: 'controllers/costo_especial.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                setTimeout(function(){ location.reload(true) }, 500);
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function valida(){
    var valid = 0;
    $('#start_date_day').css( "border-color", "#efefef" );
    $('#end_date_day').css( "border-color", "#efefef" );

    if($('#start_date_day').val() == ""){
        $('#start_date_day').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#end_date_day').val() == "" && $('#tipo').val() == 1){
        $('#end_date_day').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#tarifa').val() == ""){
        $('#tarifa').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function cargar_datos( id_extra_cost ){
    
    datos = { 'opcn':'consulta_costo_especial', 'id_extra_cost': id_extra_cost }
    limpiar();

    $('#opcn').val("editar");
    $('#id_cost').val(id_extra_cost);

    $.ajax( {
        url         :'controllers/costo_especial.php',
        type        :'POST',
        data        : datos,
        dataType    : 'json'
    } )
    .done( function(data) {

        if ( data.type == 1 ) {
            $( "#sesiones" ).show(); 
            $( "#fecha_final" ).show(); 
            $('label[for="start_date_day"]').text("Fecha Inicial Sesion:");

            $("#tipo").select2("val", "1");
            $("#schedule_id").select2('data', {id: data.schedule_id, text: (data.headquarter+' | '+data.start_date+' | '+data.newcode+' | '+data.course) });
            
            $('#start_date_day').val(data.start_date);
            $('#end_date_day').val(data.end_date);
            $('#tarifa').val(data.cost_total);

            opcion = { 'opcn':'sel_dealer','schedule_id': data.schedule_id }
            cargar_datos_dealer( opcion );
            setTimeout(function(){ $("#dealer_id").select2("val", data.dealer_id); }, 400);
        }
        if ( data.type == 2 ) {
            $( "#sesiones" ).hide(); 
            $( "#fecha_final" ).hide(); 
            $('label[for="start_date_day"]').text("Fecha Ajuste Mes:");

            $("#tipo").select2("val", "2");
            $('#start_date_day').val(data.date_cost);
            $('#tarifa').val(data.cost_total);

            opcion = { 'opcn':'sel_all_dealer' }
            cargar_datos_dealer( opcion );
            setTimeout(function(){ $("#dealer_id").select2("val", data.dealer_id); }, 400);
        }    
       
    });
}

function limpiar(){

    $( "#sesiones" ).show(); 
    $( "#fecha_final" ).show(); 
    $('label[for="start_date_day"]').text("Fecha Inicial Sesion:");

    $('#start_date_day').val('');
    $('#end_date_day').val('');
    $('#tarifa').val('');

    $("#dealer_id").select2('data', {id: 0, text: '' });
    $("#schedule_id").select2('data', {id: 0, text: '' });
    $("#tipo").select2("val", "1");

    $('#opcn').val("crear");
    $('#id_cost').val("0");
}




