var colores = [ "#1552c1", "#f39c12",'#128625', "#FF6384", "#36A2EB", "#FFCE56" ];
var grafica_todos_cursos = null;
var grafica_todos_imparticion = null;
var grafica_todos_inscritos = null;
var grafica_todos_horas_cap = null;
var grafica_vct_cursos = null;
var grafica_vct_imparticion = null;
var grafica_vct_inscritos = null;
var grafica_vct_horas_cap = null;
var grafica_ibt_cursos = null;
var grafica_ibt_imparticion = null;
var grafica_ibt_inscritos = null;
var grafica_ibt_horas_cap = null;
var grafica_ojt_cursos = null;
var grafica_ojt_imparticion = null;
var grafica_ojt_inscritos = null;
var grafica_ojt_horas_cap = null;
var grafica_wbt_cursos = null;
var grafica_wbt_imparticion = null;
var grafica_wbt_inscritos = null;
var grafica_wbt_horas_cap = null;
$(document).ready( function(){
    NProgress.configure({ parent: '.loader_s' });
    $('#start_date').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#concesionarios').select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
} );

function setURL(){
    var dealer_id = $('#concesionarios').val();
    console.log( dealer_id );
    dealer_id = dealer_id && dealer_id != "" ? dealer_id.join() : 0;
    var url = `rep_intensidad_h_excel.php?opcn=descargar&start_date=${ $('#start_date').val() }&end_date=${ $('#end_date').val() }&dealer_id=${dealer_id}`;
    console.log( url );
    $('#btn_descarga a').attr('href', url );
}

$( '#form_repIntHor' ).submit( function( e ){
    e.preventDefault();
    NProgress.start();
    $('#consultaIntensidadH').prop('disabled',true);
    var datos_prev = $(this).serializeArray();
    var datos = $(this).serialize();
    console.log( datos_prev );
    $.ajax( {
        url: 'controllers/rep_intensidad_h.php',
        data: datos,
        type: 'post',
        dataType: 'json'
    } )
    .done( function( data ){

        if( grafica_todos_cursos ){
            reiniciarGraficas();
        }
        // console.table( data );
        console.table( agruparCursos(data, '') );
        var titulos = ["HB. Habilidades Blandas","HT. Habilidades Técnicas","Producto","Marca"];
        // ========================================================
        // Grafica para todos
        // ========================================================
        var datos_todos = agruparCursos(data, '');
        var render_todos = renderTabla( datos_todos );
        var datos_todos_cursos = [datos_todos.blandas.cursos ,datos_todos.tecnicas.cursos ,datos_todos.producto.cursos ,datos_todos.marca.cursos];
        var datos_todos_imparticion = [datos_todos.blandas.imparticion ,datos_todos.tecnicas.imparticion ,datos_todos.producto.imparticion ,datos_todos.marca.imparticion];
        var datos_todos_inscritos = [datos_todos.blandas.inscritos ,datos_todos.tecnicas.inscritos ,datos_todos.producto.inscritos ,datos_todos.marca.inscritos];
        var datos_todos_horas_cap = [datos_todos.blandas.horas_cap ,datos_todos.tecnicas.horas_cap ,datos_todos.producto.horas_cap ,datos_todos.marca.horas_cap];
        grafica_todos_cursos = renderGrafica( 'graficaCursosTodos', datos_todos_cursos, titulos, "Cursos por habilidades" );
        grafica_todos_imparticion = renderGrafica( 'graficaImparticionTodos', datos_todos_imparticion, titulos, "Impartición por habilidades" );
        grafica_todos_inscritos = renderGrafica( 'graficaAsistentesTodos', datos_todos_inscritos, titulos, "Asistentes por habilidades" );
        grafica_todos_horas_cap = renderGrafica( 'graficaHorasCapTodos', datos_todos_horas_cap, titulos, "Horas de capacitación por habilidades" );
        $('#tablaTodos').html( render_todos );

        // ========================================================
        // Grafica VCT
        // ========================================================
        var datos_vct = agruparCursos(data, 'VCT')
        var d_vct = renderTabla( datos_vct );
        var datos_vct_cursos = [datos_vct.blandas.cursos ,datos_vct.tecnicas.cursos ,datos_vct.producto.cursos ,datos_vct.marca.cursos];
        var datos_vct_imparticion = [datos_vct.blandas.imparticion ,datos_vct.tecnicas.imparticion ,datos_vct.producto.imparticion ,datos_vct.marca.imparticion];
        var datos_vct_inscritos = [datos_vct.blandas.inscritos ,datos_vct.tecnicas.inscritos ,datos_vct.producto.inscritos ,datos_vct.marca.inscritos];
        var datos_vct_horas_cap = [datos_vct.blandas.horas_cap ,datos_vct.tecnicas.horas_cap ,datos_vct.producto.horas_cap ,datos_vct.marca.horas_cap];
        grafica_vct_cursos = renderGrafica( 'graficaCursosVCT', datos_vct_cursos, titulos, "Cursos por habilidades" );
        grafica_vct_imparticion = renderGrafica( 'graficaImparticionVCT', datos_vct_imparticion, titulos, "Impartición por habilidades" );
        grafica_vct_inscritos = renderGrafica( 'graficaAsistentesVCT', datos_vct_inscritos, titulos, "Asistentes por habilidades" );
        grafica_vct_horas_cap = renderGrafica( 'graficaHorasCapVCT', datos_vct_horas_cap, titulos, "Horas de capacitación por habilidades" );
        $('#tablaVCT').html( d_vct );


        // ========================================================
        // Grafica IBT
        // ========================================================
        var datos_ibt = agruparCursos(data, 'IBT')
        var d_ibt = renderTabla( datos_ibt );
        var datos_ibt_cursos = [datos_ibt.blandas.cursos ,datos_ibt.tecnicas.cursos ,datos_ibt.producto.cursos ,datos_ibt.marca.cursos];
        var datos_ibt_imparticion = [datos_ibt.blandas.imparticion ,datos_ibt.tecnicas.imparticion ,datos_ibt.producto.imparticion ,datos_ibt.marca.imparticion];
        var datos_ibt_inscritos = [datos_ibt.blandas.inscritos ,datos_ibt.tecnicas.inscritos ,datos_ibt.producto.inscritos ,datos_ibt.marca.inscritos];
        var datos_ibt_horas_cap = [datos_ibt.blandas.horas_cap ,datos_ibt.tecnicas.horas_cap ,datos_ibt.producto.horas_cap ,datos_ibt.marca.horas_cap];
        grafica_ibt_cursos = renderGrafica( 'graficaCursosIBT', datos_ibt_cursos, titulos, "Cursos por habilidades" );
        grafica_ibt_imparticion = renderGrafica( 'graficaImparticionIBT', datos_ibt_imparticion, titulos, "Impartición por habilidades" );
        grafica_ibt_inscritos = renderGrafica( 'graficaAsistentesIBT', datos_ibt_inscritos, titulos, "Asistentes por habilidades" );
        grafica_ibt_horas_cap = renderGrafica( 'graficaHorasCapIBT', datos_ibt_horas_cap, titulos, "Horas de capacitación por habilidades" );
        $('#tablaIBT').html( d_ibt );


        // ========================================================
        // Grafica OJT
        // ========================================================
        var datos_ojt = agruparCursos(data, 'OJT')
        var d_ojt = renderTabla( datos_ojt );
        var datos_ojt_cursos = [datos_ojt.blandas.cursos ,datos_ojt.tecnicas.cursos ,datos_ojt.producto.cursos ,datos_ojt.marca.cursos];
        var datos_ojt_imparticion = [datos_ojt.blandas.imparticion ,datos_ojt.tecnicas.imparticion ,datos_ojt.producto.imparticion ,datos_ojt.marca.imparticion];
        var datos_ojt_inscritos = [datos_ojt.blandas.inscritos ,datos_ojt.tecnicas.inscritos ,datos_ojt.producto.inscritos ,datos_ojt.marca.inscritos];
        var datos_ojt_horas_cap = [datos_ojt.blandas.horas_cap ,datos_ojt.tecnicas.horas_cap ,datos_ojt.producto.horas_cap ,datos_ojt.marca.horas_cap];
        grafica_ojt_cursos = renderGrafica( 'graficaCursosOJT', datos_ojt_cursos, titulos, "Cursos por habilidades" );
        grafica_ojt_imparticion = renderGrafica( 'graficaImparticionOJT', datos_ojt_imparticion, titulos, "Impartición por habilidades" );
        grafica_ojt_inscritos = renderGrafica( 'graficaAsistentesOJT', datos_ojt_inscritos, titulos, "Asistentes por habilidades" );
        grafica_ojt_horas_cap = renderGrafica( 'graficaHorasCapOJT', datos_ojt_horas_cap, titulos, "Horas de capacitación por habilidades" );
        $('#tablaOJT').html( d_ojt );

        // ========================================================
        // Grafica WBT
        // ========================================================
        var datos_wbt = agruparCursos(data, 'WBT')
        var d_wbt = renderTabla( datos_wbt );
        var datos_wbt_cursos = [datos_wbt.blandas.cursos ,datos_wbt.tecnicas.cursos ,datos_wbt.producto.cursos ,datos_wbt.marca.cursos];
        var datos_wbt_imparticion = [datos_wbt.blandas.imparticion ,datos_wbt.tecnicas.imparticion ,datos_wbt.producto.imparticion ,datos_wbt.marca.imparticion];
        var datos_wbt_inscritos = [datos_wbt.blandas.inscritos ,datos_wbt.tecnicas.inscritos ,datos_wbt.producto.inscritos ,datos_wbt.marca.inscritos];
        var datos_wbt_horas_cap = [datos_wbt.blandas.horas_cap ,datos_wbt.tecnicas.horas_cap ,datos_wbt.producto.horas_cap ,datos_wbt.marca.horas_cap];
        grafica_wbt_cursos = renderGrafica( 'graficaCursosWBT', datos_wbt_cursos, titulos, "Cursos por habilidades" );
        grafica_wbt_imparticion = renderGrafica( 'graficaImparticionWBT', datos_wbt_imparticion, titulos, "Impartición por habilidades" );
        grafica_wbt_inscritos = renderGrafica( 'graficaAsistentesWBT', datos_wbt_inscritos, titulos, "Asistentes por habilidades" );
        grafica_wbt_horas_cap = renderGrafica( 'graficaHorasCapWBT', datos_wbt_horas_cap, titulos, "Horas de capacitación por habilidades" );
        $('#tablaWBT').html( d_wbt );

        NProgress.done();
        setURL();
        $('#btn_descarga').show();
        $('#consultaIntensidadH').prop('disabled',false);
        $('._well').show();
    } );
} );

function agruparCursos( data, tipo ){
    var tabla = {
        blandas: { cursos: 0, imparticion: 0, inscritos: 0, horas_cap: 0 },// habilidades blandas
        tecnicas: { cursos: 0, imparticion: 0, inscritos: 0, horas_cap: 0 },// habilidades tecnicas
        marca: { cursos: 0, imparticion: 0, inscritos: 0, horas_cap: 0 },// procesos de marca
        producto: { cursos: 0, imparticion: 0, inscritos: 0, horas_cap: 0 },// producto
        total: { cursos: 0, imparticion: 0, inscritos: 0, horas_cap: 0 }// total
    };
    data.map( function( key, idx ){
        var filtro = tipo == "" ? key.course_type : tipo;
        var inscritos = parseInt( key.inscritos );
        var imparticion = parseInt( key.duration_time );
        var horas_cap = parseInt( key.horas_cap );
        if( key.course_type == filtro ){
            switch ( key.specialty_id ) {
                case "1":
                    tabla.tecnicas.cursos ++;
                    tabla.tecnicas.imparticion += imparticion;
                    tabla.tecnicas.inscritos += inscritos;
                    tabla.tecnicas.horas_cap += horas_cap;
                    tabla.total.cursos ++;
                    tabla.total.imparticion += imparticion;
                    tabla.total.inscritos += inscritos;
                    tabla.total.horas_cap += horas_cap;
                break;
                case "2":
                    tabla.blandas.cursos ++;
                    tabla.blandas.imparticion += imparticion;
                    tabla.blandas.inscritos += inscritos;
                    tabla.blandas.horas_cap += horas_cap;
                    tabla.total.cursos ++;
                    tabla.total.imparticion += imparticion;
                    tabla.total.inscritos += inscritos;
                    tabla.total.horas_cap += horas_cap;
                break;
                case "3":
                    tabla.marca.cursos ++;
                    tabla.marca.imparticion += imparticion;
                    tabla.marca.inscritos += inscritos;
                    tabla.marca.horas_cap += horas_cap;
                    tabla.total.cursos ++;
                    tabla.total.imparticion += imparticion;
                    tabla.total.inscritos += inscritos;
                    tabla.total.horas_cap += horas_cap;
                break;
                case "4":
                    tabla.producto.cursos ++;
                    tabla.producto.imparticion += imparticion;
                    tabla.producto.inscritos += inscritos;
                    tabla.producto.horas_cap += horas_cap;
                    tabla.total.cursos ++;
                    tabla.total.imparticion += imparticion;
                    tabla.total.inscritos += inscritos;
                    tabla.total.horas_cap += horas_cap;
                break;
            }// Fin switch
        }
    } );
    return tabla;
}// Fin agruparCursos

function renderTabla( data ){
    var html = `<table class="footable table table-striped table-primary">
        <thead>
            <tr class="text-center">
                <th>ESPECIALIDAD</th>
                <th>CURSOS</th>
                <th>H. IMPARTICIÓN</th>
                <th>ASISTENTES</th>
                <th>H. CAPACITACIÓN</th>
            </tr>
        </thead>
        <tbody>
            <tr class="text-center">
                <th>HB. HABILIDADES BLANDAS</th>
                <td>${ data.blandas.cursos }</td>
                <td>${ data.blandas.imparticion }</td>
                <td>${ data.blandas.inscritos }</td>
                <td>${ data.blandas.horas_cap }</td>
            </tr>
            <tr class="text-center">
                <th>HT HABILIDADES TÉCNICAS</th>
                <td>${ data.tecnicas.cursos }</td>
                <td>${ data.tecnicas.imparticion }</td>
                <td>${ data.tecnicas.inscritos }</td>
                <td>${ data.tecnicas.horas_cap }</td>
            </tr>
            <tr class="text-center">
                <th>PROCESOS DE MARCA</th>
                <td>${ data.marca.cursos }</td>
                <td>${ data.marca.imparticion }</td>
                <td>${ data.marca.inscritos }</td>
                <td>${ data.marca.horas_cap }</td>
            </tr>
            <tr class="text-center">
                <th>PRODUCTO</th>
                <td>${ data.producto.cursos }</td>
                <td>${ data.producto.imparticion }</td>
                <td>${ data.producto.inscritos }</td>
                <td>${ data.producto.horas_cap }</td>
            </tr>
            <tr class="text-center">
                <th>TOTAL</th>
                <td>${ data.total.cursos }</td>
                <td>${ data.total.imparticion }</td>
                <td>${ data.total.inscritos }</td>
                <td>${ data.total.horas_cap }</td>
            </tr>
        </tbody>
    </table>`;
    return html;
}// renderTabla

function renderGrafica( idx, dataGf, titulos, texto ){
    console.log( dataGf, titulos );
    var ctx = document.getElementById( idx ).getContext('2d');
    var config = {
        type: 'pie',
        data: {
            datasets: [ {
                data: dataGf,
                label: texto,
                backgroundColor: colores,
                borderColor: colores,
            } ],
            labels: titulos
        },
        responsive: true,
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: texto
        },
    };
    return new Chart(ctx, config);
}

function reiniciarGraficas(){
    grafica_todos_cursos.destroy();
    grafica_todos_imparticion.destroy();
    grafica_todos_inscritos.destroy();
    grafica_todos_horas_cap.destroy();
    grafica_vct_cursos.destroy();
    grafica_vct_imparticion.destroy();
    grafica_vct_inscritos.destroy();
    grafica_vct_horas_cap.destroy();
    grafica_ibt_cursos.destroy();
    grafica_ibt_imparticion.destroy();
    grafica_ibt_inscritos.destroy();
    grafica_ibt_horas_cap.destroy();
    grafica_ojt_cursos.destroy();
    grafica_ojt_imparticion.destroy();
    grafica_ojt_inscritos.destroy();
    grafica_ojt_horas_cap.destroy();
    grafica_wbt_cursos.destroy();
    grafica_wbt_imparticion.destroy();
    grafica_wbt_inscritos.destroy();
    grafica_wbt_horas_cap.destroy();
}
