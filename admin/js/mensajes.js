$(document).ready(function(){
    iniciar();
});

function iniciar(){
	// Select Placeholders
    $("#user_msg_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $('.UsrMsg').click(function() {
    	$(".UsrMsg").each(function (index) {
    		$(this).removeClass('active');
    	});
    	$('#user_newMsg').val($(this).attr('data-id'));
    	$(this).addClass('active');
    	CargarMsg($(this).attr('data-id'),$(this).attr('data-name'));
	});
	$('.MsgNvo').click(function() {
    	$(".UsrMsg").each(function (index) {
    		$(this).removeClass('active');
    	});
    	$('#user_newMsg').val($('#user_msg_id').val());
    	$(this).addClass('active');
    	CargarMsg($('#user_msg_id').val(),'Nuevo Mensaje');
	});
}

function Act_FormularioData(){
    $('#BtnMsgEnviar').hide();
	var value = $('#MensajeTxt').val();
	value = value.replace(/\r?\n/g, "<br>");
	$('#MensajeTxt').val(value);
    EnviaMensaje();
    event.preventDefault();
}
function EnviaMensaje(){
    $.ajax({ url: 'controllers/mensajes.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            $('#MensajeTxt').val('');
            CargarMsg($('#user_newMsg').val(),$('#name_user_newMsg').val());
            CargarUsr();
            $('#BtnMsgEnviar').show();
        }
    });
}

function CargarMsg(Usr,Name){
	$('#Loading_Msg').removeClass('hide');
	$('#SeccionMensajes').load('detail_msg.php', {idUsr:Usr, namePer:Name}, function(response, status, xhr){
		if ( status == "error" ) {
		    var msg = "Sorry but there was an error: ";
		    console.log( msg + xhr.status + " " + xhr.statusText );
		}
		$('#Loading_Msg').addClass('hide');
	});
}

function CargarUsr(){
	$('#Loading_Msg_Usr').removeClass('hide');
	$('#ListadoRemitentes').load('detail_msg_usr.php', function(response, status, xhr){
		if ( status == "error" ) {
		    var msg = "Sorry but there was an error: ";
		    console.log( msg + xhr.status + " " + xhr.statusText );
		}
		$('#Loading_Msg_Usr').addClass('hide');
		iniciar();
	});
}