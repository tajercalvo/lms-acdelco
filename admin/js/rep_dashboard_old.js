var dataEmp = [];
var d1 = [];
var d2 = [];
var stack = 0, bars = false, lines = true, steps = false;
var dataChar = [];
var dataChar_ot = [];
var dataChar_oc = [];

var concesionarios = ["ANDAR","AUTODENAR","AUTOGRANDE","AUTOLARTE","AUTOLITORAL","AUTOMARCALI","AUTONIZA","AUTOPACIFICO","AUTOSUPERIOR","AYURA MOTOR","CAESCA","CAMINOS","CAMPESA","CASA RESTREPO","CENTRODIESEL","CODIESEL","COLTOLIMA","CONTINAUTOS","COUNTRY MOTORS","DIESEL ANDINO","DISAUTOS","INTERNACIONAL DE VEHICULOS","LLANO GRANDE","MARAUTOS","RIO GRANDE","SAN JORGE","VEHICOSTA","YANACONAS","TALLERES DEL NORTE"];



var num_meses = 0;
$(document).ready(function(){
    // Select Placeholders
    $('#ano_id_start').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#mes_id_start').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#ano_id_end').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#mes_id_end').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    num_meses = $('#cant_meses').val();
});
$(function () {
    for (var i = 0; i <= 10; i += 1)
        d1.push([concesionarios[i], parseInt(Math.random() * 30)]);
    for (var i = 0; i <= 29; i += 1)
        d2.push(["Mes "+i, parseInt(Math.random() * 30)]);
    dataEmp.push({ label: "Promedio Cursos", data:d1 });
    
	plotWithOptions();
    plotAvConcesionario();*/
    plotCargaPies();
});

function plotAvConcesionario(){
    $.plot($("#placeholder-AvConcesionario"), dataEmp, {
        series: {
            lines: { show: false, fill: true, steps: false },
            bars: { show: true, barWidth: 0.6 },
            points: { show: true }
        },
        grid: { hoverable:true, clickable:false },
        yaxis: { min: 0 },
        xaxis: { 
            /*ticks:11, tickDecimals: 0*/
            mode: "categories",
            ticklength: 0
        },
        /*colors: [],*/
        shadowSize:1,
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            }
        }
    });
}
function plotCargaPies(){
    dataChar.push({ label: "IBT", data: 301 });
    dataChar.push({ label: "WBT", data: 273 });

    dataChar_ot.push({ label: "No Ejecutado", data: 10 });
    dataChar_ot.push({ label: "Ejecutado", data: 3 });
    dataChar_oc.push({ label: "Disponibles", data: 72 });
    dataChar_oc.push({ label: "Ocupados", data: 43 });

    var plot1 = $.plot($("#donut-tipocurso"), dataChar, 
    {
        series: {
            pie: { 
                innerRadius: 0.2,
                show: true,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
        },
        colors: ["#609450","#5CC7DD","#424242"],
        shadowSize:1,
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            }
        }
    });

    plot1.draw();

    $.plot($("#donut-cursoprog"), dataChar_ot, 
    {
        series: {
            pie: { 
                innerRadius: 0.2,
                show: true,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
        },
        colors: ["#BD362F","#0EBFF2","#424242"]
    });
    $.plot($("#donut-ocupacion"), dataChar_oc, 
    {
        series: {
            pie: { 
                innerRadius: 0.2,
                show: true,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
        },
        colors: ["#d65050","#F4F4F4","#424242"]
    });
}

function CambiaGrafica(id, opcion){
	if(opcion == "lineal"){
        plotWithOptions_All(id,true,false,false)
	}else if(opcion == "barras"){
        plotWithOptions_All(id,false,false,true)
	}else if(opcion == "puntos"){
        plotWithOptions_All(id,false,true,false)
	}
}

function plotWithOptions_Id(id) {
    $('#loadingAvanceXCargo'+id).removeClass('hide');
    console.log($('#loadingAvanceXCargo'+id));
    var Charge_id = id;
    var ArrayMes = $("#intervalos_fechas").val();
    var datos_env = new FormData();
    datos_env.append('Charge_id',Charge_id);
    datos_env.append('ArrayMes',ArrayMes);
    datos_env.append('opcn','ConsultaAvanceCargo');
    var url = "controllers/dashboard.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:datos_env,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data_ret) {
            plotDataAjax(id,data_ret);
        }
    });
}

function plotDataAjax(id,data_charge){
    $.plot($("#placeholder-"+id), data_charge, {
        series: {
            lines: { show: lines, fill: true, steps: steps },
            bars: { show: bars, barWidth: 0.6 },
            points: { show: true }
        },
        grid: { hoverable:true, clickable:false },
        yaxis: { min: 0 },
        xaxis: { 
            /*ticks:11, tickDecimals: 0*/
            mode: "categories",
            ticklength: 0
        },
        /*colors: [],*/
        shadowSize:1,
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            }
        }
    });
    $('#loadingAvanceXCargo'+id).addClass('hide');
}
function plotWithOptions() {
    var ctrl = 0;
    var idPlot = 0;
    $(".LineChart").each(function (index) {
        if(ctrl==0){
            idPlot = $(this).attr('id');
            idPlot = idPlot.replace("placeholder-", "");
            if(idPlot>0){
                plotWithOptions_Id(idPlot);
            }
            ctrl = 1;
        }
    });
}
function plotWithOptions_All(id,linesv,stepsv,barsv) {
    $.plot($("#placeholder-"+id), dataEmp, {
        series: {
            lines: { show: linesv, fill: true, steps: stepsv },
            bars: { show: barsv, barWidth: 0.6 },
            points: { show: true }
        },
        grid: { hoverable:true, clickable:false },
        yaxis: { min: 0 },
        xaxis: { 
            /*ticks:11, tickDecimals: 0*/
            mode: "categories",
            ticklength: 0
        },
        /*colors: [],*/
        shadowSize:1,
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            }
        }
    });
}
