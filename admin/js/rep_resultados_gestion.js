$(document).ready( function(){
    $('#start_date').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
} );

$('#form_repGestion').submit( function( e ){
    e.preventDefault();
    var datos = {
        opcn: 'consulta',
        start_date : $('#start_date').val(),
        end_date : $('#end_date').val()
    };
    $.ajax({
        url: 'controllers/rep_resultados_gestion.php',
        data: datos,
        type: 'post',
        dataType: 'json'
    })
    .done( function( data ){
        if( !data.error ){
            // console.table( data.data );
            var tabla = renderTabla( data.data );
            $('#espacio_tabla').html( tabla );
            if( data.data.length > 0 ){
                $('#btn_descarga').show();
                $('#ref_descarga').attr('href',`rep_resultados_gestion_excel.php?opcn=descarga&start_date=${$('#start_date').val()}&end_date=${$('#end_date').val()}`);
            }else{
                $('#btn_descarga').hide();
                $('#ref_descarga').attr('href','');
            }
        }
    } );
} );

function renderTabla( data ){
    var tabla = `<table class='display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable ui-sortable'>
        <thead>
            <tr>
                <th class="text-center">CONCESIONARIO</th>
                <th class="text-center">CATEGORIA</th>
                <th class="text-center">CUPOS</th>
                <th class="text-center">ASISTENTES</th>
                <th class="text-center">EXCUSAS</th>
                <th class="text-center">ASISTENCIA + EXCUSAS</th>
                <th class="text-center">%</th>
                <th class="text-center">CUMPLIMIENTO</th>
            </tr>
        </thead>
        <tbody>`;
        data.forEach( function( key, idx ){
            var real_asis = key.asistentes + key.excusas;
            if ( key.cupos > 0 ) {
                var porcentaje = parseInt( key.cupos ) > 0 ? (real_asis / parseInt( key.cupos )) * 100 : 0;
            }else{
                var porcentaje = 100.00;
            }
            var text_cumpli = "";
            var limite = key.anio <= 2018 && key.mes < 6 ? 100 : 90;
            if( porcentaje < limite ){
                if ( key.cupos > 0 ) {
                    text_cumpli += "<span class='btn btn-danger btn-xs'>No cumplió</span>";
                }else{
                    text_cumpli += "<span class='btn btn-success btn-xs'>Cumplió</span>";
                }
            }else if( porcentaje >= limite && porcentaje <= 100 ){
                text_cumpli += "<span class='btn btn-success btn-xs'>Cumplió</span>";
            }else{
                text_cumpli += "<span class='btn btn-warning btn-xs'>Cumplió</span>";
            }
            var row = `<tr>
                <td class="text-center">${ key.dealer }</td>
                <td class="text-center">${ key.category }</td>
                <td class="text-center">${ key.cupos }</td>
                <td class="text-center">${ key.asistentes }</td>
                <td class="text-center">${ key.excusas }</td>
                <td class="text-center">${ real_asis }</td>
                <td class="text-center">${ porcentaje.toFixed(2) } %</td>
                <td class="text-center">${ text_cumpli }</td>
            </tr>`;
            tabla += row;
        } );
    tabla += `</tbody>
        </table>`;
    return tabla;
}
