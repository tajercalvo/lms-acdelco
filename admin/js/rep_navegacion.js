$(document).ready(function(){
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
     $("#charge").select2({
        placeholder: "Seleccione una trayectoria",
        allowClear: true
    });
      $("#dealer").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
   
});

$("#descargar_excel").click(function(event) {

       $("#datos_tabla").val( $("<div>").append( $("#tab_Navegacion").eq(0).clone()).html());
        $("#FormularioExportacion").submit();
         //console.log( $("#datos_a_enviar").val());
    });

$("#descargar_TODOS").click(function(event) {
       $("#datos_tabla2").val( $("<div>").append( $("#tab_TODOS").eq(0).clone()).html());
        $("#FormularioExportacion_TODOS").submit();
         //console.log( $("#datos_a_enviar").val());
    });

$("#descargar_excel_tab_GMAcademy").click(function(event) {

       $("#datos_tabla3").val( $("<div>").append( $("#tab_GMAcademy").eq(0).clone()).html());
        $("#FormularioExportacion_tab_GMAcademy").submit();
        // console.log( $("#datos_a_enviar").val());
    });


$( "#descargar" ).click(function() {

    acumula = 0;
    cien = 0;
    sum = 0;

    document.getElementById("miTabla_descarga").innerHTML = '';
    $("#barra_progreso").width(0);
    $("#descargando_reporte").text('');
    

    //id_descarga = $( "#cursosF" ).val();

    //$("#cargando").css("visibility","visible");
    //$("#cargando").text("Realizando la consulta, por favor espere…");

    var data = new FormData();
    data.append('opcn','descargar');    
    data.append('start_date_day',$("#start_date_day").val());
    data.append('end_date_day',$("#end_date_day").val());
    
    $.ajax({
        url:"controllers/rep_navegacion.php",
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "text",
        //dataType: "json",
        cache:false,
        success: function(data) {
  
             if(data==""){
            
                    //$("#cargando").text("consulta si resultados");

                 
                }else{

                        $("#progreso_visible").css("visibility","visible");
                        // convertimos el texto que biene de php en un array, la letra p es el delimitador del array
                        res = data.split("::");

                        console.log('Resultado total '+(res.length-1));

                        //Contamos el array y le restamos 1 por que el ultimo array biene vacio, asi obtenemos la cantidad
                        // de cursos a concultar
                        registros = res.length-1;

                        // Dividimos la cantidad de registros etre 100 para asignarlo por parte a la barra de progreso
                        cien = 100 /registros;

                        //Recorremos el array para obtener el siguiente resultado a consultar
                        for(var i in res) {

                                console.log(res[i]);
                                descargar_siguiente(res[i]);
                        } 

                            // else{

                            //       $('#miTabla_descarga').append(data);
                            //       $("#cargando").css("visibility","hidden");

                            // }
                } //Fin Else
        } // Fin function dta
    }); // FIn ajax
    
}); // Fin  FUnction click

tabla = '';
function descargar_siguiente(id){

    var data = new FormData();
    data.append('opcn','');
    data.append('charge_id',id);
    data.append('start_date_day',$("#start_date_day").val());
    data.append('end_date_day',$("#end_date_day").val());

    $.ajax({
        url:"controllers/rep_navegacion.php",
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "text",
        cache:false,
        success: function(data) {

                    tabla = tabla + data;
                    
                    // $('#miTabla_descarga').append(data);
                    console.log("progress"+(acumula=acumula+cien));
                    $("#barra_progreso").width(acumula+'%');
                    console.log('Progreso igual a '+acumula);

                    sum++;

                    if (sum >= registros) {
                        $("#descargando_reporte").text('Descarga finalizada, por favor espere…');
                        //$("#cargando").css("visibility","hidden");
                        // $("#cargando").text("FIN descarga");
                        //$("#progreso_visible").remove();
                        $("#progreso_visible").css("visibility","hidden");
                    }else{

                         $("#descargando_reporte").text('Descargando '+ sum + ' de ' + registros );
                         
                    }

                    if (sum > registros) {
                        $("#descargando_reporte").text('');
                        $('#js-tabla tr:last').after(tabla);
                        console.log(tabla);
                        $("#datos_tabla").val( $("<div>").append( $("#js-tabla").eq(0).clone()).html());
                        $("#FormularioExportacion").submit();

                        //$("#datos_tabla").val(tabla);
                        //$("#FormularioExportacion").submit();
                        $("#barra_progreso").width(0);
                    }

                    
        } // Fin function data 

    }); //Fin ajax

}// Fin Function descargar reporte

function validacion() {

    s_date = document.getElementById("start_date_day").value;
    e_date = document.getElementById("end_date_day").value;
   
    $('#start_date_day').css( "border-color", "#ffffff" );
    $('#end_date_day').css( "border-color", "#ffffff" );
    //$('#identificacion').css( "border-color", "#ffffff" );


    if( s_date == '' & e_date == '') {
    $('#start_date_day').css( "border-color", "#b94a48" );
    $('#end_date_day').css( "border-color", "#b94a48");
    //$('#identificacion').css( "border-color", "#b94a48" );
    $( "#start_date_day" ).focus();
    return false;
    } else if( e_date == '') {
    $('#end_date_day').css( "border-color", "#b94a48" );
    $( "#end_date_day" ).focus();
    return false;
    } else if( s_date == '') {
    // alert('Complete la informacion antes de consultarla');
    $('#start_date_day').css( "border-color", "#b94a48" );
    //$('#identificacion').css( "border-color", "#b94a48" );
    $( "#start_date_day" ).focus();
    return false;
    }else{
        return true;
    }
    
    }

