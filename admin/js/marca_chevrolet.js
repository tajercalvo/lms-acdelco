$(document).ready(function(){
	$("#galerias").select2({
	    placeholder: "Seleccione la (las) galería(s)",
	    allowClear: true
	});
    $( "#form_EditarGaleria" ).submit(function( event ) {
        if(validaEdt()){
            return true;
        }else{
            return false;
        }
        return false;
        event.preventDefault();
    });
});
function validaEdt(){
    var valid = 0;
    $('#galerias').css( "border-color", "#efefef" );

    if($('#galerias').val()==null){
        $('#galerias').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Debe seleccionar al menos una galería para la asignación',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function LikeGalery(){
    var data = new FormData();
    data.append('opcn','LikeGalery');
    var url = "controllers/marca_chevrolet.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                notyfy({
                    text: 'Muchas gracias, hemos recibido tu like! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes').text());
                $('#CantLikes').text(Cantlikes+1);
            }else{
                notyfy({
                    text: 'Ya votaste por esta multimedia anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function ViewGalery(){
    var data = new FormData();
    data.append('opcn','ViewGalery');
    var url = "controllers/marca_chevrolet.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                var CantViews = parseInt($('#CantViews').text());
                $('#CantViews').text(CantViews+1);
            }
        }
    });
}

var CantPlay =0;
$(document).ready(function(){
    $("#video").on('play',function(){
        if(CantPlay==0){
            CantPlay++;
            ViewGalery();
        }
    });
});
