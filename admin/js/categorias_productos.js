$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/categorias_productos.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});
$(document).ready(function(){
    // Select Placeholders
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

    $("#select_crear_estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});



$( "#form_crear_categoria" ).submit(function( event ) {
        event.preventDefault();
        var datos_form = $(this).serializeArray();
        datos_form.push({name: "opcn", value: 'crear'});
        $.ajax({
                data:  datos_form,
                url:   'controllers/categorias_productos.php',
                type:  'post',
                success:  function (response) {
                    notyfy({
                        text: 'La operación ha sido completada satisfactoriamente',
                        type: 'success' // alert|error|success|information|warning|primary|confirm
                    });
                    $("#ModalCrear").modal("toggle");
                    $("#TableData_filter > label > input").val( $("#txt_crearEspecialidad").val());
                }
        });
});

function valida_campos() {
    var valid = 0;
    $('#txt_Especialidad').css( "border-color", "#efefef" );
    $('#estado').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#txt_Especialidad').val() == ""){
        $('#txt_Especialidad').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#estado').val() == ""){
        $('#estado').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

// modal para editar un archivo
$( "#form_Editar_archivos" ).submit(function( event ) {
        event.preventDefault();
        if(!valida_campos()){ return false; }
        var datos_form = $(this).serializeArray();
        datos_form.push({name: "id_Especialidad", value: id_Especialidad});
        datos_form.push({name: "opcn", value: 'editar'});
        $.ajax({
                data:  datos_form,
                url:   'controllers/categorias_productos.php',
                type:  'post',
                success:  function (response) {
                    notyfy({
                        text: 'La operación ha sido completada satisfactoriamente',
                        type: 'success' // alert|error|success|information|warning|primary|confirm
                    });
                    $("#ModalEditar").modal("toggle");
                    $("#TableData_filter > label > input").val( $("#txt_Especialidad").val());
                }
        });
    });

/*$('#agregar_categoria').clic(function function_name(e) {
    e.preventDefault;
    $("#ModalCrear").modal("toggle");
});*/
$("#agregar_categoria").click(function(e){
    e.preventDefault;
    $("#ModalCrear").modal("toggle");
});

// funcion para editar los archivos con una modal
$('body').on('click', '.editar_archivos', function(e){
            e.preventDefault; // evita que se ejecute el href
            $("#ModalEditar").modal("toggle");
             //console.log( $(this).parents("tr").attr('data-id'));
             arreglo = [];
            $(this).parents("tr").find("td").each(function(){
                arreglo.push($(this).html()); // capturamos los campos de la fila
            });
            estado = $(this).attr('data-estado');
            var color = estado == 'Activo'? 'green': 'red' ;
            $("#indicador_estado").html('Estaco actual <strong style="color: '+color+'">'+estado+'</strong>');
            $('#txt_Especialidad').val(arreglo[0]);
            id_Especialidad = $(this).attr('data-id');
            return false;
});

function valida(){
    var valid = 0;
    $('#nombre').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#nombre').val() == ""){
        $('#nombre').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg){
    $.ajax({ url: 'controllers/categorias_productos.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}