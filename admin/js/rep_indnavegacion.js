var areaChartOptions = {
  //Boolean - If we should show the scale at all
  showScale: true,
  //Boolean - Whether grid lines are shown across the chart
  scaleShowGridLines: false,
  //String - Colour of the grid lines
  scaleGridLineColor: "rgba(0,0,0,.05)",
  //Number - Width of the grid lines
  scaleGridLineWidth: 1,
  //Boolean - Whether to show horizontal lines (except X axis)
  scaleShowHorizontalLines: true,
  //Boolean - Whether to show vertical lines (except Y axis)
  scaleShowVerticalLines: true,
  //Boolean - Whether the line is curved between points
  bezierCurve: true,
  //Number - Tension of the bezier curve between points
  bezierCurveTension: 0.3,
  //Boolean - Whether to show a dot for each point
  pointDot: false,
  //Number - Radius of each point dot in pixels
  pointDotRadius: 4,
  //Number - Pixel width of point dot stroke
  pointDotStrokeWidth: 1,
  //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
  pointHitDetectionRadius: 20,
  //Boolean - Whether to show a stroke for datasets
  datasetStroke: true,
  //Number - Pixel width of dataset stroke
  datasetStrokeWidth: 2,
  //Boolean - Whether to fill the dataset with a color
  datasetFill: true,
  //String - A legend template
  legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
  //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
  maintainAspectRatio: true,
  //Boolean - whether to make the chart responsive to window resizing
  responsive: true
};

$(document).ready(function(){

   $("#zone").change(function () {

            $('#consultando').html('<span class="label label-success">Realizando consulta, por favor espere...</span><img style="width: 15px; " src="../assets/loading-course.gif">');

           $("#zone option:selected").each(function () {
            //captura el id del value del select 2 que se va a enviar
            var zona=$(this).val();
            $.post("controllers/rep_indnavegacion.php", { zona: zona, opcn:"opcn" }, function(data){
            $("#dealer").html(data);
            $('#consultando').html("");

            });
        });

   });

   $("#dealer").change(function () {
            $('#consultando').html('<span class="label label-success">Realizando consulta, por favor espere...</span><img style="width: 15px; " src="../assets/loading-course.gif">');
           $("#dealer option:selected").each(function () {
            //captura el id del value del select 2 que se va a enviar
            var dealer=$(this).val();
            $.post("controllers/rep_indnavegacion.php", { dealer: dealer, opcn:"opcn" }, function(data){
            $("#headquarters").html(data);
            $('#consultando').html('');
            });
        });
   });

   $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
     $("#charge").select2({
        placeholder: "Seleccione un cargo",
        allowClear: true
    });
      $("#zone").select2({
        placeholder: "Seleccione una zona",
        allowClear: true
    });
      $("#dealer").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
      $("#headquarters").select2({
        placeholder: "Seleccione una sede",
        allowClear: true
    });

   $("#mibody").css("visibility","visible");
   $("#cargando_pagina").html("");

});


$("#descargar_excel").click(function(event) {

       $("#datos_a_enviar").val( $("<div>").append( $("#tab_Navegacion").eq(0).clone()).html());
        $("#FormularioExportacion").submit();
         //console.log( $("#datos_a_enviar").val());
    });

$("#descargar_TODOS").click(function(event) {
       $("#datos_a_enviar_tab_TODOS").val( $("<div>").append( $("#tab_TODOS").eq(0).clone()).html());
        $("#FormularioExportacion_TODOS").submit();
         //console.log( $("#datos_a_enviar").val());
    });

$("#descargar_excel_tab_GMAcademy").click(function(event) {

       $("#datos_a_enviar_tab_GMAcademy").val( $("<div>").append( $("#tab_GMAcademy").eq(0).clone()).html());
        $("#FormularioExportacion_tab_GMAcademy").submit();
        // console.log( $("#datos_a_enviar").val());
    });


$( "#btn_consulta" ).click(function() {


    if (validacion()) {

            $("#contenido_tablas").css("visibility","hidden");
            $( "#btn_consulta" ).prop( "disabled", true );

            //$('#miTablaaa').append('');

            //Borra el contenido de la tabla
            $( "#tabla_visitantes1" ).html('');
            $( "#tabla_visitantes2" ).html('');

            $('#consultando').html('<span class="label label-success">Realizando consulta, por favor espere...</span><img style="width: 15px; " src="../assets/loading-course.gif">');


            //$("#cargando").text("Realizando la consulta, por favor espere…");

            var data = new FormData();
            data.append('opcn','opcn');
            //data.append('idconsulta',idconsulta);
            data.append('start_date_day',$("#start_date_day").val());
            data.append('end_date_day',$("#end_date_day").val());
            data.append('Zona',$("#zone").val());
            data.append('Concesionario',$("#dealer").val());
            data.append('Sede',$("#headquarters").val());
            data.append('Cargo',$("#charge").val());


            $.ajax({
                url:"controllers/rep_indnavegacion.php",
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                //dataType: "json",
                cache:false,
                success: function(data) {

                    //SI data biene vacio va y busca el siguiente id del curso
                  // document.getElementById("miTablaaa").innerHTML = data;
                    if(data.vacio == "si" ){
                        $('#modal').modal('toggle');

                        $("#contenido_tablas").css("visibility","hidden");
                        $( "#btn_consulta" ).prop( "disabled", false );
                        $("#consultando").html('<span class="label label-warning">Consulta sin resultados</span>');

                         // $("#cargando").css("visibility","hidden");

                        }else{
                            $('#modal').modal('toggle');

                            var data_1 = {
                                visitantes:[],
                                cedulas:[],
                                navegantes:[]
                            };
                            var data_2 = {
                                visitantes:[],
                                cedulas:[]
                            };

                            var meses = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
                            var titulos = [];

                            for (var i = 0; i < data.data.length; i++) {
                                titulos.push( meses[ ( data.data[i].mes - 1 ) ]+"-"+data.data[i].ano );
                            }

                            for (var i = 0; i < data.data.length; i++) {

                                data_1.visitantes.push(data.data[i].visitantes_unicos);
                                data_1.cedulas.push(data.data[i].cant_cedulas);
                                data_1.navegantes.push( Math.round(data.data[i].visitantes_unicos / data.data[i].cant_cedulas * 100,1) );

                                data_2.visitantes.push(data.data[i].visitantes_unicos);
                                data_2.cedulas.push( Math.round(data.data[i].visitas / data.data[i].visitantes_unicos,1) );
                            }

                            var dataSet1 = {
                              labels: titulos,
                              datasets: [
                                {
                                  label: "Visitantes únicos",
                                  fillColor: "rgba(210, 214, 222, 1)",
                                  strokeColor: "rgba(210, 214, 222, 1)",
                                  pointColor: "rgba(210, 214, 222, 1)",
                                  pointStrokeColor: "#c1c7d1",
                                  pointHighlightFill: "#fff",
                                  pointHighlightStroke: "rgba(220,220,220,1)",
                                  data: data_1.visitantes
                                },
                                {
                                  label: "Total cédulas",
                                  fillColor: "rgba(60,141,188,0.9)",
                                  strokeColor: "rgba(60,141,188,0.8)",
                                  pointColor: "#3b8bba",
                                  pointStrokeColor: "rgba(60,141,188,1)",
                                  pointHighlightFill: "#fff",
                                  pointHighlightStroke: "rgba(60,141,188,1)",
                                  data: data_1.cedulas
                                },
                                {
                                  label: "% navegantes",
                                  fillColor: "rgba(160,141,188,0.9)",
                                  strokeColor: "rgba(160,141,188,0.8)",
                                  pointColor: "#ffffff",
                                  pointStrokeColor: "rgba(160,141,188,1)",
                                  pointHighlightFill: "#aaa",
                                  pointHighlightStroke: "rgba(160,141,188,1)",
                                  data: data_1.navegantes
                                }
                              ]
                            };
                            var dataSet2 = {
                              labels: titulos,
                              datasets: [
                                {
                                  label: "Visitantes únicos",
                                  fillColor: "rgba(210, 214, 222, 1)",
                                  strokeColor: "rgba(210, 214, 222, 1)",
                                  pointColor: "rgba(210, 214, 222, 1)",
                                  pointStrokeColor: "#c1c7d1",
                                  pointHighlightFill: "#fff",
                                  pointHighlightStroke: "rgba(220,220,220,1)",
                                  data: data_2.visitantes
                                },
                                {
                                  label: "visitas x cédulas",
                                  fillColor: "rgba(60,141,188,0.9)",
                                  strokeColor: "rgba(60,141,188,0.8)",
                                  pointColor: "#3b8bba",
                                  pointStrokeColor: "rgba(60,141,188,1)",
                                  pointHighlightFill: "#fff",
                                  pointHighlightStroke: "rgba(60,141,188,1)",
                                  data: data_2.cedulas
                                }
                              ]
                            };

                            var lineChartCanvas1 = $("#lineChart1").get(0).getContext("2d");
                            var lineChartCanvas2 = $("#lineChart2").get(0).getContext("2d");

                            var lineChart1 = new Chart(lineChartCanvas1);
                            var lineChart2 = new Chart(lineChartCanvas2);

                            var lineChartOptions = areaChartOptions;
                            lineChartOptions.datasetFill = false;

                            lineChart1.Line(dataSet1, lineChartOptions);
                            lineChart2.Line(dataSet2, lineChartOptions);

                            $("#contenido_tablas").css("visibility","visible");
                            $( "#btn_consulta" ).prop( "disabled", false );

                            $('#tabla_visitantes1').html(data.tabla1);
                            $('#tabla_visitantes2').html(data.tabla2);
                            $('#consultando').html('');

                    } // Fin else
                } // Fin function data
            })// Fin ajax
            .fail(function() {
                $( "#btn_consulta" ).prop( "disabled", false );
                $("#consultando").html('<span class="label label-danger">Falló la conexión con el servidor, verifica que tengas conexión a internet e intenta nuevamente</span>');

              });
    }// Fin si valida

});
// Fin funcion click

function validacion() {

    s_date = document.getElementById("start_date_day").value;
    e_date = document.getElementById("end_date_day").value;

    $('#start_date_day').css( "border-color", "#ffffff" );
    $('#end_date_day').css( "border-color", "#ffffff" );
    //$('#identificacion').css( "border-color", "#ffffff" );

     inicio= new Date($('#start_date_day').val());
        finalq= new Date($('#end_date_day').val());
        if(inicio>finalq){

                    bootbox.alert("La fecha inicial no puede ser mayor que la fecha final", function(result)
                {

                });
                    return false;
        }


    if( s_date == '' & e_date == '') {
        $('#start_date_day').css( "border-color", "#b94a48" );
        $('#end_date_day').css( "border-color", "#b94a48");
        //$('#identificacion').css( "border-color", "#b94a48" );
        $( "#start_date_day" ).focus();
    return false;
    } else if( e_date == '') {
        $('#end_date_day').css( "border-color", "#b94a48" );
        $( "#end_date_day" ).focus();
        return false;
    } else if( s_date == '') {
        // alert('Complete la informacion antes de consultarla');
        $('#start_date_day').css( "border-color", "#b94a48" );
        //$('#identificacion').css( "border-color", "#b94a48" );
        $( "#start_date_day" ).focus();
        return false;
    }else{
        return true;
    }

}//fin funcion validacion
