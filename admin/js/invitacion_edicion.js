$(document).ready(function(){
    // Select Placeholders
    //
    $("#schedule_id").select2({
        placeholder: "Seleccione una sesion",
        allowClear: true
    });
    $("#new_schedule_id").select2({
        placeholder: "Seleccione una sesion",
        allowClear: true
    });

    $("#course_id").select2({
        placeholder: "Seleccione un curso",
        minimumInputLength: 1,
        allowClear: true,
        ajax: {
            //How long the user has to pause their typing before sending the next request
            quietMillis: 150,
            url: 'controllers/invitacion_edicion.php',
            type:'POST',
            dataType: 'json',
            data: function (term, page) {
                //console.log("Valor:"+term+" y Page:"+page);
                return {
                    searchTerm: term,
                    opcn: "consulta_curso"
                };
            },
            delay: 250,
            results: function (data) {
                return { results: data };
            }
        }
    });


});

// *** VARIABLES GLOBALES ***
var datos_tabla='';
var user_id_seleccionado;
// *** ***

//CARGA LAS SESIONES DE CADA CURSO EN LA LISTA DESPLEGABLE
$('#course_id').change( function(){

    $("#schedule_id").select2("val", "");
    var course_id = $(this).val();

    data = {
        'opcn':'sel_curso',
        'curso': $(this).val()
    }

    $.ajax( {
        url         :'controllers/invitacion_edicion.php',
        type        :'POST',
        data        : data,
        dataType    : 'json'
    } )
    .done( function(data) {

        var cantidadaDatos = data.length;
        var linea_html = "";
        if (cantidadaDatos == 0) {
            linea_html += "<option value='0'>No existe sesiones programadas</option>";
            $("#schedule_id").html(linea_html);
        }
        else{
           $.each(data, function (key, val) {
                linea_html += "<option value='"+val.schedule_id+"' >"+val.start_date+' | INSTRUCTOR: '+val.first_name+' '+val.last_name+' | LUGAR: '+val.living+"</option>";
           });
           $("#schedule_id").html(linea_html);
        }

       //console.log(data);

    });

} );

//CARGA LA BUSQUEDA DESEADA EN LA TABLA DE RESULTADOS
$(".val-trigger").on('input', function( e ) {

  var filtro = ( $(e.target).val() ).toUpperCase();
  var patt = new RegExp(filtro);
  var busqueda = 0;

    var tabla='';
      tabla += '<table  class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">';
      tabla += '<thead>';
      tabla += '<tr>';
      tabla += '<th data-hide="phone,tablet">#</th>';
      tabla += '<th data-hide="phone,tablet">FOTO</th>';
      tabla += '<th data-hide="phone,tablet">USUARIO</th>';
      tabla += '<th data-hide="phone,tablet">IDENTIFICACION</th>';
      tabla += '<th data-hide="phone,tablet">EMPRESA</th>';
      tabla += '<th data-hide="phone,tablet">ESTADO</th>';
      tabla += '<th data-hide="phone,tablet">EDICION</th>';
      tabla += '</tr>';
      tabla += '</thead>';
      tabla += '<tbody>';

      for (var i = 0; i < datos_tabla.length; i++) {
        busqueda = 0;
        if( patt.test(datos_tabla[i]['first_name']+' '+datos_tabla[i]['last_name']) == true ){
            busqueda = 1;
        } else if ( patt.test(datos_tabla[i]['identification']) == true ){
            busqueda = 1;
        } else if ( patt.test(datos_tabla[i]['dealer']) == true ){
            busqueda = 1;
        }

        if ( busqueda == 1 ) {
            var estado = "";
            if( datos_tabla[i]['status_id'] == 1 ){
                estado = "Invitado";
            } else if ( datos_tabla[i]['status_id'] == 2 ){
                estado = "Asistio";
            } else if ( datos_tabla[i]['status_id'] == 3 ){
                estado = "No Asistio";
            }
                tabla += '<tr>';
                tabla +='<td>'+(i+1)+'</td>';
                tabla += '<td><img src="../assets/images/usuarios/'+datos_tabla[i]['image']+'" style="width: 40px;" alt="image"></td>';
                tabla += '<td>'+datos_tabla[i]['first_name']+' '+datos_tabla[i]['last_name']+'</td>';
                tabla += '<td>'+datos_tabla[i]['identification']+'</td>';
                tabla += '<td>'+datos_tabla[i]['dealer']+'</td>';
                tabla += '<td>'+estado+'</td>';//onclick='ir(\"" + idHisto + "\",\"" + adm +"\")'>
                tabla += '<td style="text-align:center;"><a onclick="asignar('+datos_tabla[i]['invitation_id']+', '+datos_tabla[i]['schedule_id']+', '+datos_tabla[i]['dealer_id']+', '+datos_tabla[i]['course_id']+', '+datos_tabla[i]['user_id']+', \''+datos_tabla[i]['first_name']+' '+datos_tabla[i]['last_name']+'\' );" class="btn btn-action glyphicons pencil btn-success"><i></i></a></td>';
                tabla += '</tr>';
        }
      }

       tabla += '</tbody>';
       tabla += '</table>';

    $('#ContenidoTabla').html(tabla);

});

$("#filter").change(function() {
  console.log("triggered!");
});

//FORMULARIO DE LISTAS CURSO Y SESION
$('#form_consulta').submit( function( e ){

    e.preventDefault();
    $('#ContenidoTabla').html('');

    if( $("#course_id").val() == 0 ){
        notyfy({
          text: 'Debe seleccionar un Curso',
          type: 'error'
        });
    } else if( $("#schedule_id").val() == 0 ){
        notyfy({
          text: 'Debe seleccionar una Sesión',
          type: 'error'
        });
    }
    else{
        data = {
            'opcn':'invitados_schedule',
            'course_id': $('#course_id').val(),
            'schedule_id': $('#schedule_id').val()
        }

        $.ajax( {
            url         :'controllers/invitacion_edicion.php',
            type        :'POST',
            data        : data,
            dataType    : 'json'
        } )
        .done( function(data) {
            var cantidadaDatos = data.usuarios.length;
            nuevas_sesiones( data.sesiones );
            if ( cantidadaDatos == 0 ) {
                $('.val-trigger').css( "display", "none" );
                $('#cantidad_datos').text("Total de invitados: 0");
                // console.log("Esto esta vacio");
                return false;
            }
            else{
                $('.val-trigger').css( "display", "block" );
                $('#cantidad_datos').text("Total de invitados: "+cantidadaDatos);
                generar_tabla( data.usuarios, 0 );

            }
           //console.log(data);
        });

    }

} );


//FORMULARIO DE NUEVA SESION
$('#form_new_schedule').submit( function( e ){

    e.preventDefault();
    /*console.log("Nueva sesion:");
    console.log( $('#new_schedule_id').val() );
    console.log("Sesion:");
    console.log( $('#schedule_id').val() );*/

    if( $("#new_schedule_id").val() == 0 ){
        notyfy({
          text: 'Debe seleccionar una Nueva Sesion',
          type: 'error'
        });
    }
    else {

        datos = {
            'opcn':'modificar_sesion',
            'invitation_id': $('#invitation_id').val(),
            'new_schedule_id': $('#new_schedule_id').val(),
            'schedule_id': $('#schedule_id').val(),
            'user_id': $('#user_id').val(),
            'dealer_id': $('#dealer_id').val(),
            'course_id': $('#course_id').val()
        }
        $.ajax( {
                url         :'controllers/invitacion_edicion.php',
                type        :'POST',
                data        : datos,
                dataType    : 'json'
            } )
            .done( function(data) {
                var res0 = data.resultado;
                if(res0 == "TRUE"){
                    generar_tabla( datos_tabla, user_id_seleccionado );
                    $('#confirmar_sesion').attr("disabled", true);
                    $("#nombre").val("");
                    notyfy({
                        text: 'La operación ha sido completada satisfactoriamente',
                        type: 'success' // alert|error|success|information|warning|primary|confirm
                    });
                }else{
                    if(res0 == "FALSE1"){
                        notyfy({
                            text: 'No se ha logrado editar la invitación. Ya existe una invitación creada en la nueva sesion.',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                    }
                    else{
                        notyfy({
                            text: 'No se ha logrado editar la invitación, por favor confirme su conexión y realicela nuevamente',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                    }
                }
            });
    }

} );

// IMPRIMIR TABLA DE RESULTADOS
function generar_tabla ( obj, user_id_select ){

    data = obj;//Datos de la tabla

    if ( user_id_select > 0) {
        for (var i = 0; i < data.length; i++) {
            if( data[i]['user_id'] == user_id_select ){
                data.splice(i, 1);
            }
        }
    }

        var tabla='';
          tabla += '<table  class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">';
          tabla += '<thead>';
          tabla += '<tr>';
          tabla += '<th data-hide="phone,tablet">#</th>';
          tabla += '<th data-hide="phone,tablet">FOTO</th>';
          tabla += '<th data-hide="phone,tablet">USUARIO</th>';
          tabla += '<th data-hide="phone,tablet">IDENTIFICACION</th>';
          tabla += '<th data-hide="phone,tablet">EMPRESA</th>';
          tabla += '<th data-hide="phone,tablet">ESTADO</th>';
          tabla += '<th data-hide="phone,tablet">EDICION</th>';
          tabla += '</tr>';
          tabla += '</thead>';
          tabla += '<tbody>';
          for (var i = 0; i < data.length; i++) {
              console.log( data );
            var estado = "";
            if( data[i]['status_id'] == 1 ){
                estado = "Invitado";
            } else if ( data[i]['status_id'] == 2 ){
                estado = "Asistio";
            } else if ( data[i]['status_id'] == 3 ){
                estado = "No Asistio";
            }
            tabla += '<tr>';
            tabla +='<td>'+(i+1)+'</td>';
            tabla += '<td><img src="../assets/images/usuarios/'+data[i]['image']+'" style="width: 40px;" alt="image"></td>';
            tabla += '<td>'+data[i]['first_name']+' '+data[i]['last_name']+'</td>';
            tabla += '<td>'+data[i]['identification']+'</td>';
            tabla += '<td>'+data[i]['dealer']+'</td>';
            tabla += '<td>'+estado+'</td>';//onclick='ir(\"" + idHisto + "\",\"" + adm +"\")'> Enviar datos con JSON.stringify( data.[i] )
            tabla += '<td style="text-align:center;"><a onclick="asignar('+data[i]['invitation_id']+', '+data[i]['schedule_id']+', '+data[i]['dealer_id']+', '+data[i]['course_id']+', '+data[i]['user_id']+', \''+data[i]['first_name']+' '+data[i]['last_name']+'\' );" class="btn btn-action glyphicons pencil btn-success"><i></i></a></td>';
            tabla += '</tr>';
          }
           tabla += '</tbody>';
           tabla += '</table>';
        $('#ContenidoTabla').html(tabla);


}

//CARGA LAS NUEVAS SESIONES PARA CADA USUARIO EN LA LISTA DESPLEGABLE
function nuevas_sesiones( sesiones ){

    linea_html = "";

    if ( sesiones.length == 0) {
        console.log("Sesion nueva vacia");
        linea_html += "<option value='0'>No existe mas sesiones programadas</option>";

    }
    else{
       sesiones.forEach( function (value, idx ) {
            linea_html += "<option value='"+value.schedule_id+"'>"+value.start_date+' | INSTRUCTOR: '+value.first_name+' '+value.last_name+' | LUGAR: '+value.living+"</option>";
       });
    }

    $("#new_schedule_id").html(linea_html);

}


//AGREGA DATOS Y COMPONENTES EN EL FORMULARIO NUEVA SESION
function asignar( invitation_id, schedule_id, dealer_id, course_id, user_id, name ){ // recibir obj
    // // asigna datos de la invitacion en la nueva sesion
    //seleccionado = obj;
    user_id_seleccionado = user_id;
    $('#confirmar_sesion').removeAttr("disabled");
    $("#invitation_id").val(invitation_id);
    $("#schedule_id").val(schedule_id);
    $("#user_id").val(user_id);
    $("#dealer_id").val(dealer_id);
    $("#course_id").val(course_id);
    $("#nombre").val(name);
}
