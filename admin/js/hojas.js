$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/hojas.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});
$(document).ready(function(){
    // Select Placeholders
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $('#fec_fulfillment').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "1960-01-01"
    });
});

$( "#formulario_data" ).submit(function( event ) {
    if(validarHoja()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
    event.preventDefault();
});

$("#retornaElemento").click(function() {
    window.location="hojas.php";
});

function valida(){
    var valid = 0;
    $('#nombre').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#nombre').val() == ""){
        $('#nombre').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg){
    $.ajax({ url: 'controllers/hojas.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

/*
Andres Vega
04/08/2016
funcion que evalua los datos enviados en el formulario de Hoja de Ruta
true: si se ingresaron correctamente todos los datos
false: si no se ha ingresado al menos uno de los campos
*/
function validarHoja(){
  var valid=0;
  $('textarea').css( "border-color", "#efefef" );
  if($("#description").val()==""){
    $('#description').css( "border-color", "#b94a48" );
    valid=1;
  }else if($("#target").val()==""){
    $('#target').css( "border-color", "#b94a48" );
    valid=1;
  }else if($("#action").val()==""){
    $('#action').css( "border-color", "#b94a48" );
    valid=1;
  }else if($("#fec_fulfillment").val()=="0000-00-00 00:00:00"){
    $('#fec_fulfillment').css( "border-color", "#b94a48" );
    valid=1;
  }
  if(valid==0){
    /*notyfy({
      text: 'La operación ha sido completada satisfactoriamente',
      type: 'success' // alert|error|success|information|warning|primary|confirm
    });*/
    return true;
  }else{
    notyfy({
      text: 'Por favor verifíque la información que esta pendiente',
      type: 'warning' // alert|error|success|information|warning|primary|confirm
    });
    return false;
  }

}//fin funcion validarHoja
