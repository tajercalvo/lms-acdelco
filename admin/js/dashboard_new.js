( function( d, w, undefined, $ ){
    ( function(){
        return graf = {
            total_usuarios_con_trayectorias: 0,
            sabanaDatos:[],
            concesionarios:[],
            concesionarioSel: [],
            usuarios_trayect: 0,
            fechaSel:[],
            promedio_pais: 0,
            _general: null, _tecticas: null, _blandas: null, _producto: null, _marca: null,
            PromZona: null,
            _h_general: null, _h_tecticas: null, _h_blandas: null, _h_producto: null, _h_marca: null,
            colores: [ "#1552c1", "#f39c12", "#FF6384", "#36A2EB", "#FFCE56", '#128625' ],
            /* =========================================================
            Ejecuta funciones que activan eventos especificos
            ========================================================= */
            ejecutarFunciones: function(){
                var self = this;
                $(".h_ife").change( function(){
                    $('#btn_ife').trigger('click');
                } );
                $(".hab_ccs").change( function(){
                    // console.log("se ejecuto el boton");
                    $('#btn_hab_ccs').trigger('click');
                } );
                //=================================================================
                // Modifica la grafica de cupos vs inscritos segun el filtro
                //=================================================================
                $('#form_ife').submit( function( e ){
                    e.preventDefault();
                    opcn = $(this).serializeArray()[0].value;

                    self.renderNotasEncuestas( self.encuestas, opcn );
                } );
                //=================================================================
                // Modifica la grafica de promedio de notas por concesionarios
                //=================================================================
                $('#form_hab_ccs').submit( function( e ){
                    e.preventDefault();
                    opcn = $(this).serializeArray()[0].value;
                    self.renderPromedioZona( opcn );
                } );
            },
            /* =========================================================
            inicializa todos los procesos generados con sus funciones
            ========================================================= */
            Init: function(){
                var self = this;
                //inicializa el script para loader
                NProgress.configure({ parent: '.loader_s' });

                self.iniciarSelects();
                self.ejecutarDashboard();
                self.ejecutarFunciones();

            },
            /* =========================================================
            inicializa los selects y sus acciones
            ========================================================= */
            iniciarSelects: function(){
                var self = this;
                $('#fecha').select2({
                    placeholder: "Seleccione un mes o un Quarter",
                    allowClear: true
                });
                $('#zonas').select2({
                    placeholder: "Seleccione una zona",
                    allowClear: true
                });
                $('#concesionarios').select2({
                    placeholder: "Seleccione un concesionario",
                    allowClear: true
                });

                $('#zonas').change( function(){
                    var zonas = $(this).val() || "";
                    $("#concesionarios").select2("val", "");
                    var _url = "controllers/dashboard_new.php"

                    var datos = new FormData();
                    datos.append( 'opcn', 'sel_Concs' );
                    datos.append( 'zonas', zonas );

                    $.ajax({
                        url         : _url,
                        type        :'post',
                        contentType : false,
                        data        : datos,
                        processData : false,
                        dataType    : 'html',
                        cache       : false,
                        async       : false
                    } )
                    .done( function( data ) {
                        $('#dataConcs').html( data );
                    } );
                } );
            },
            /* =========================================================
            // Hace una validacion previa a los datos enviados por el formulario
            ========================================================= */
            validar:function(){
                var valid = 0;
                var estado = false;

                $('#fecha').css( "border-color", "#efefef" );

                if(!$('#fecha').val() || $('#fecha').val() == ""){
                  valid = 1;
                  $('#fecha').css( "border-color", "#b94a48" );
                  notyfy({
                      text: 'La fecha es necesaria para realizar la consulta',
                      type: 'warning' // alert|error|success|information|warning|primary|confirm
                  });
                }
                if(!$('#zonas').val() || $('#zonas').val() == ""){
                  valid = 1;
                  $('#zonas').css( "border-color", "#b94a48" );
                  notyfy({
                      text: 'Selecciona una zona por favor',
                      type: 'warning' // alert|error|success|information|warning|primary|confirm
                  });
                }
                if( valid == 0 ){
                    estado = true ;
                }

                return estado;
            },
            /* =========================================================
            Agrupa los concesionarios
            ========================================================= */
            agruparConcesionarios: function( data ){
                var self = this;
                var ccs = [];
                data.map( function( key, idx ){
                    ccs.push( key );
                    ccs[idx].max_size = 0;
                    ccs[idx].promedio = 0;
                    ccs[idx].aprobo = 0;
                    ccs[idx].reprobo = 0;
                    ccs[idx].excusas = 0;
                    ccs[idx].invitados = 0;
                    ccs[idx].inscritos = 0;
                    self.sdatos_pais.map( function( k, i ){
                        if( key.dealer_id == k.dealer_id ){
                            ccs[idx].max_size += parseInt(k.max_size);
                            ccs[idx].promedio += parseInt(k.promedio);
                            ccs[idx].aprobo += parseInt(k.aprobo);
                            ccs[idx].reprobo += parseInt(k.reprobo);
                            ccs[idx].excusas += parseInt(k.excusas) + parseInt( k.excusas_cupos );
                            ccs[idx].invitados += parseInt(k.inscritos);
                            ccs[idx].inscritos += parseInt(k.asistentes);
                        }
                    } );
                } );
                return ccs;
            },
            /* =========================================================
            Agrupa los concesionarios segun el filtro
            ========================================================= */
            agruparConcesionariosFiltro: function( filtro ){
                var self = this;
                var ccs = [];
                self.concesionarios.map( function( key, idx ){
                    ccs.push( key );
                    ccs[idx].max_size = 0;
                    ccs[idx].promedio = 0;
                    ccs[idx].aprobo = 0;
                    ccs[idx].reprobo = 0;
                    ccs[idx].excusas = 0;
                    ccs[idx].invitados = 0;
                    ccs[idx].inscritos = 0;
                    self.sdatos_pais.map( function( k, i ){
                        if( key.dealer_id == k.dealer_id && k.specialty_id == filtro ){
                            ccs[idx].max_size += parseInt(k.max_size);
                            ccs[idx].promedio += parseInt(k.promedio);
                            ccs[idx].aprobo += parseInt(k.aprobo);
                            ccs[idx].reprobo += parseInt(k.reprobo);
                            ccs[idx].excusas += parseInt(k.excusas) + parseInt( k.excusas_cupos );
                            ccs[idx].invitados += parseInt(k.inscritos);
                            ccs[idx].inscritos += parseInt(k.asistentes);
                        }
                    } );
                } );
                return ccs;
            },
            /* =========================================================
            Renderiza la tabla de comparativo de los concesionarios segun sus notas
            ========================================================= */
            renderTablaComparacion: function( tabla ){
                html = '';
                datos = {
                    "A":{ 'categoria':"A", 'puntaje':'>=95', 'puntos':4, 'concesionarios':0, 'color':"" },
                    "B":{ 'categoria':"B", 'puntaje':'de 87,56 a 94,99', 'puntos':2, 'concesionarios':0, 'color':"" },
                    "C":{ 'categoria':"C", 'puntaje':'de 80 a 87,55', 'puntos':1, 'concesionarios':0, 'color':"" },
                    "D":{ 'categoria':"D", 'puntaje':'de 0,00 a 79,99', 'puntos':0, 'concesionarios':0, 'color':"" }
                };
                tabla.forEach( function( key, idx ){
                    var nota = 0;
                    if( parseInt( key.max_size ) > 0 ){
                        nota = parseInt(key.promedio) / parseInt( key.max_size );
                    }
                    if( nota >= 95 ){ datos['A'].concesionarios ++; }
                    if( nota > 87.55 && nota < 95 ){ datos['B'].concesionarios ++; }
                    if( nota >= 80 && nota <= 87.55 ){ datos['C'].concesionarios ++; }
                    if( nota < 80 && nota > 0 ){ datos['D'].concesionarios ++; }
                } );

                if( datos['A'].concesionarios > 0 ){
                    html += `<tr>
                        <td class="text-center" style="background-color:#00a65a;color:#fff">${ datos['A'].concesionarios }</td>
                        <td class="text-center" style="background-color:#00a65a;color:#fff">A</td>
                        <td class="text-center">${ datos['A'].puntaje }</td>
                        <td class="text-center">${ datos['A'].puntos }</td>
                    </tr>`;
                }
                if( datos['B'].concesionarios > 0 ){
                    html += `<tr>
                        <td class="text-center" style="background-color:#0058a3;color:#fff">${ datos['B'].concesionarios }</td>
                        <td class="text-center" style="background-color:#0058a3;color:#fff">B</td>
                        <td class="text-center">${ datos['B'].puntaje }</td>
                        <td class="text-center">${ datos['B'].puntos }</td>
                    </tr>`;
                }
                if( datos['C'].concesionarios > 0 ){
                    html += `<tr>
                        <td class="text-center" style="background-color:#f39c12;color:#fff">${ datos['C'].concesionarios }</td>
                        <td class="text-center" style="background-color:#f39c12;color:#fff">C</td>
                        <td class="text-center">${ datos['C'].puntaje }</td>
                        <td class="text-center">${ datos['C'].puntos }</td>
                    </tr>`;
                }
                if( datos['D'].concesionarios > 0 ){
                    html += `<tr>
                        <td class="text-center" style="background-color:#dd4b39;color:#fff">${ datos['D'].concesionarios }</td>
                        <td class="text-center" style="background-color:#dd4b39;color:#fff">D</td>
                        <td class="text-center">${ datos['D'].puntaje }</td>
                        <td class="text-center">${ datos['D'].puntos }</td>
                    </tr>`;
                }
                return html;

            },
            //=================================================================
            // retorna un objeto para validar los datos y calcular los quarters
            //=================================================================
            calcularQuarters: function( obj ){
                var self = this;
                var fecha = new Date();
                quarters = { q1:{ cumplimiento:0, aprobado:0, num: 0 }, q2:{ cumplimiento:0, aprobado:0, num: 0 }, q3:{ cumplimiento:0, aprobado:0, num: 0 }, q4:{ cumplimiento:0, aprobado:0, num: 0 } };
                obj.forEach( function( key, idx ){
                    console.log( key, self.fechaSel );
                    var asistentes = parseInt(key.asistentes);
                    var excusas = parseInt(key.excusas);
                    var cupos = parseInt(key.cupos);

                    // Renderiza datos si el mes fue seleccionado
                    if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {

                        // si no se le asignaron cupos, cuenta como cumplimiento
                        if ( cupos == 0 ) {
                            cumplimiento = 100;
                            check = "fa-clock-o";
                        }else{
                            cumplimiento = parseInt( ( ( asistentes + excusas ) / cupos ) * 100 );
                            check = cumplimiento >= 100 ? "fa-check" : "fa-times";
                        }
                        $('#cump_mes_'+key.mes).html( `<strong>${cumplimiento}<i class="fa ${ check }"></i></strong>` );
                        $('#cupos_mes_'+key.mes).html( `<small>${cupos}</small>` );
                        $('#asistentes_mes_'+key.mes).html( `<small>${asistentes}</small>` );
                        $('#excusas_mes_'+key.mes).html( `<small>${excusas}</small>` );

                        // calcula por mes
                        switch( parseInt(key.mes) ){
                            case 1:
                                quarters.q1.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q1.num ++;
                                }
                                quarters.q1.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 2:
                                quarters.q1.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q1.num ++;
                                }
                                quarters.q1.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 3:
                                quarters.q1.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q1.num ++;
                                }
                                quarters.q1.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 4:
                                quarters.q2.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q2.num ++;
                                }
                                quarters.q2.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 5:
                                quarters.q2.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q2.num ++;
                                }
                                quarters.q2.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 6:
                                quarters.q2.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q2.num ++;
                                }
                                quarters.q2.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 7:
                                quarters.q3.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q3.num ++;
                                }
                                quarters.q3.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 8:
                                quarters.q3.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q3.num ++;
                                }
                                quarters.q3.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 9:
                                quarters.q3.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q3.num ++;
                                }
                                quarters.q3.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 10:
                                quarters.q4.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q4.num ++;
                                }
                                quarters.q4.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 11:
                                quarters.q4.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q4.num ++;
                                }
                                quarters.q4.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                            case 12:
                                quarters.q4.cumplimiento += cumplimiento;
                                if ( cupos > 0 || self.fechaSel.indexOf(""+key.mes) >= 0 ) {
                                    quarters.q4.num ++;
                                }
                                quarters.q4.aprobado += ( cumplimiento >= 100 || ( cupos == 0 && self.fechaSel.indexOf(""+key.mes) >= 0 ) ) ? 1 : 0;
                            break;
                        }//fin switch

                    }// fin if cupos > 0

                } );//fin forEach
                return quarters;
            },//fin calcularQuarters
            //=================================================================
            // renderiza las tablas de las trayectorias y sus usuarios
            //=================================================================
            renderTrayectorias: function( data, cat ){
                var self = this;
                var tabla1 = "";
                var tabla2 = "";
                var total  = 0;
                var tablas = {};
                // console.log( "trayectorias", data );
                cat.forEach( function( cate, indx ){
                    tablas[ cate.category ] = { 'categoria': cate.category, 'tabla': '' };
                    tablas[ cate.category ].tabla += `<table class='table table-primary'>
                        <caption><h4>${ cate.category }</h4></caption>
                        <tr>
                            <th>Trayectoria</th>
                            <th>#</th>
                        </t>`;
                    data.forEach( function( k, i ){
                        if ( k.category_id == cate.category_id ) {
                            tablas[ cate.category ].tabla += `<tr>
                                <td>${ k.charge }</td>
                                <td>${ k.usuarios }</td>
                            </tr>`;
                        }
                    } );
                    tablas[ cate.category ].tabla += `</table>`;
                } );
                data.forEach( function( key, idx ){
                    //excluye el cargo de Voluntarios
                    if( key.charge_id != 123 ){
                        total += parseInt(key.usuarios);

                    }
                } );

                return { "tabla1":tablas, "tabla2":tabla2, "total":total };
            },//fin renderTrayectorias
            //=================================================================
            // renderiza una gráfica que muestra la participación
            //=================================================================
            renderParticipacion: function( data, usuarios ){
                // console.log( usuarios );
                var self = this;
                var etiquetas = ['Cupos','Inscritos','Asistentes','Aprobados','Reprobados'];
                var generalF = [0,0,0,0,0];
                var gTecnica = [0,0,0,0,0];
                var gBlandas = [0,0,0,0,0];
                var gProducto = [0,0,0,0,0];
                var gMarca = [0,0,0,0,0];

                if( self._general ){
                    self._general.destroy();
                    self._tecticas.destroy();
                    self._blandas.destroy();
                    self._producto.destroy();
                    self._marca.destroy();
                }
                // console.log( data );
                if( data ){
                    data.map( function( key, idx ){
                        if( self.concesionarioSel == '' || self.concesionarioSel.indexOf( ''+key.dealer_id ) >= 0 ){

                            generalF[0] += parseInt(key.max_size);
                            generalF[1] += parseInt(key.inscritos);
                            generalF[2] += parseInt(key.asistentes);// personas inscritas
                            generalF[3] += parseInt(key.aprobo);
                            generalF[4] += parseInt(key.reprobo);

                            switch ( key.specialty_id ) {
                                case '1':

                                    gTecnica[0] += parseInt(key.max_size);
                                    gTecnica[1] += parseInt(key.inscritos);
                                    gTecnica[2] += parseInt(key.asistentes);
                                    gTecnica[3] += parseInt(key.aprobo);
                                    gTecnica[4] += parseInt(key.reprobo);
                                break;
                                case '2':

                                    gBlandas[0] += parseInt(key.max_size);
                                    gBlandas[1] += parseInt(key.inscritos);
                                    gBlandas[2] += parseInt(key.asistentes);
                                    gBlandas[3] += parseInt(key.aprobo);
                                    gBlandas[4] += parseInt(key.reprobo);
                                break;
                                case '3':

                                    gProducto[0] += parseInt(key.max_size);
                                    gProducto[1] += parseInt(key.inscritos);
                                    gProducto[2] += parseInt(key.asistentes);
                                    gProducto[3] += parseInt(key.aprobo);
                                    gProducto[4] += parseInt(key.reprobo);
                                break;
                                case '4':

                                    gMarca[0] += parseInt(key.max_size);
                                    gMarca[1] += parseInt(key.inscritos);
                                    gMarca[2] += parseInt(key.asistentes);
                                    gMarca[3] += parseInt(key.aprobo);
                                    gMarca[4] += parseInt(key.reprobo);
                                break;
                            }
                        }
                    } );
                }

                self._general = self.graficaFunnel( 'gfFunnelParticipacion', generalF, etiquetas , 'General');
                self._tecticas = self.graficaFunnel( 'gfFunnelParticipacion_HTecticas', gTecnica, etiquetas, 'Producto' );
                self._blandas = self.graficaFunnel( 'gfFunnelParticipacion_HBlandas', gBlandas, etiquetas, 'Habilidades Técnicas' );
                self._producto = self.graficaFunnel( 'gfFunnelParticipacion_Producto', gProducto, etiquetas, 'Habilidades Blandas' );
                self._marca = self.graficaFunnel( 'gfFunnelParticipacion_Marca', gMarca, etiquetas, 'Procesos de marca' );
            },//fin renderParticipacion
            //=================================================================
            // Renderiza la tabla de comparación por zona
            //=================================================================
            renderPromedioZona: function( filtro ){
                var self = this;
                var labels = [];
                var datos = [];
                var colores = [];
                var aux = [];
                var pais = [];
                var color_pais = [];
                var promedioZona = 0;

                if( self.PromZona ){
                    self.PromZona.destroy();
                }
                // console.log( data );
                if( filtro == 'all' ){
                    var data = self.agruparConcesionarios( self.concesionarios );
                    // console.table( data );
                    if( data ){
                        data.map( function( key, idx ){
                            if( key.max_size > 0 ){
                                var resultado = (key.promedio / key.max_size );
                                promedioZona += (key.promedio / key.max_size );
                                datos.push( resultado.toFixed(1) );
                                labels.push( key.dealer );
                                colores.push( self.concesionarioSel.indexOf( key.dealer_id ) >= 0 ? "#f39c12" : "#1552c1" );
                            }
                        } );
                        promedioZona = ( promedioZona / data.length ).toFixed(1);
                        for (var i = 0; i < data.length; i++) {
                            aux.push( promedioZona );
                            pais.push( self.promedio_pais.toFixed(1) );
                            color_pais.push( '#128625' );
                        }
                    }
                }else{
                    var data = self.agruparConcesionariosFiltro( filtro );
                    data.map( function( key, idx ){
                        if( key.max_size > 0 ){
                            var resultado = (key.promedio / key.max_size );
                            promedioZona += (key.promedio / key.max_size );
                            datos.push( resultado.toFixed(1) );
                            labels.push( key.dealer );
                            colores.push( self.concesionarioSel.indexOf( key.dealer_id ) >= 0 ? "#FF6384" : "#1552c1" );
                        }
                    } );
                    promedioZona = ( promedioZona / data.length ).toFixed(1);
                    for (var i = 0; i < data.length; i++) {
                        aux.push( promedioZona );
                        pais.push( self.promedio_pais.toFixed(1) );
                        color_pais.push( '#128625' );
                    }
                }


                self.PromZona = self.graficaBarras( 'promNotasCcs', datos, colores, labels, aux, pais, color_pais );

            },//fin renderPromedioZona
            //=================================================================
            // Renderiza el ranking de estudiantes, por cursos aprobados
            //=================================================================
            renderRankingEstudiantesCursos: function( data ){
                var self = this;
                var html = '';
                if( data ){
                    data.forEach( function( key, idx ){
                        html += `<tr class="text-center">
                                <td>${ idx + 1 }</td>
                                <td>${ key.first_name+" "+key.last_name } <small style="color: silver;">(${ key.dealer })</small></td>
                                <td>${ key.cursos+" / "+parseInt(key.promedio).toFixed(1) }</td>
                            </tr>`;
                    } );
                }
                $('#rankingEstudiantesC').html( html );

            },//fin renderRankingEstudiantesCursos
            //=================================================================
            // Renderiza el ranking de estudiantes, por cursos aprobados
            //=================================================================
            renderRankingEstudiantesNotas: function( data ){
                var self = this;
                var html = '';
                if( data ){
                    data.forEach( function( key, idx ){
                        var segundos = parseFloat( key.tiempo );
                        var d = new Date( segundos * 1000 );
                        html += `<tr class="text-center">
                                <td>${ idx + 1 }</td>
                                <td>${ key.first_name+" "+key.last_name } <small style="color: silver;">(${ key.dealer })</small></td>
                                <td>${ parseFloat(key.promedio).toFixed(1)+" / "+d.getMinutes()+":"+d.getSeconds()+" / "+key.cursos }</td>
                            </tr>`;
                    } );
                }
                $('#rankingAlumn').html( html );

            },//fin renderRankingEstudiantesNotas
            //=================================================================
            // Renderiza las horas de capacitacion/laborales según especialidad
            //=================================================================
            renderHorasCapacitacion: function( data ){
                var self = this;
                var etiquetas = ['H. laborales','H. formación'];
                var generalF = { laborales:0, capacitacion:0, inscritos:0, usuarios: [] };
                var gTecnica = { laborales:0, capacitacion:0, inscritos:0, usuarios: [] };
                var gBlandas = { laborales:0, capacitacion:0, inscritos:0, usuarios: [] };
                var gProducto = { laborales:0, capacitacion:0, inscritos:0, usuarios: [] };
                var gMarca = { laborales:0, capacitacion:0, inscritos:0, usuarios: [] };
                var mes = { "_1": 184, "_2": 176, "_3": 172, "_4": 184, "_5": 184, "_6": 172,
                    "_7": 176, "_8": 184, "_9": 180, "_10": 192, "_11": 176, "_12": 168 };

                if( data ){
                    // console.log( "data horas", data );
                    data.map( function( key, idx ){
                        if( self.concesionarioSel == '' || self.concesionarioSel.indexOf( ''+key.dealer_id ) >= 0 ){

                            // asigno los usuarios unicos
                            key.usuarios.forEach( function( k, i ) {
                                if (  generalF.usuarios.indexOf( key.mes+"_"+k.user_id ) < 0 ) {
                                    generalF.usuarios.push( key.mes+"_"+k.user_id );
                                }
                            } );
                            generalF.capacitacion += parseInt(key.duration_time) * key.inscritos; // OK

                            switch ( key.specialty_id ) {
                                case '1':

                                    key.usuarios.forEach( function( k, i ) {
                                        if (  gTecnica.usuarios.indexOf( key.mes+"_"+k.user_id ) < 0 ) {
                                            gTecnica.usuarios.push( key.mes+"_"+k.user_id );
                                        }
                                    } );
                                    gTecnica.capacitacion += parseInt(key.duration_time) * key.inscritos; // OK


                                break;
                                case '2':
                                    key.usuarios.forEach( function( k, i ) {
                                        if (  gBlandas.usuarios.indexOf( key.mes+"_"+k.user_id ) < 0 ) {
                                            gBlandas.usuarios.push( key.mes+"_"+k.user_id );
                                        }
                                    } );
                                    gBlandas.capacitacion += parseInt(key.duration_time) * key.inscritos; // OK

                                break;
                                case '3':
                                    key.usuarios.forEach( function( k, i ) {
                                        if (  gProducto.usuarios.indexOf( key.mes+"_"+k.user_id ) < 0 ) {
                                            gProducto.usuarios.push( key.mes+"_"+k.user_id );
                                        }
                                    } );
                                    gProducto.capacitacion += parseInt(key.duration_time) * key.inscritos; // OK

                                break;
                                case '4':
                                    key.usuarios.forEach( function( k, i ) {
                                        if (  gMarca.usuarios.indexOf( key.mes+"_"+k.user_id ) < 0 ) {
                                            gMarca.usuarios.push( key.mes+"_"+k.user_id );
                                        }
                                    } );
                                    gMarca.capacitacion += parseInt(key.duration_time) * key.inscritos; // OK

                                break;
                            }//Fin switch
                        } //fin if
                    } );
                    // ========================================================
                    // Horas Laborales
                    // ========================================================
                     generalF.usuarios.forEach( function( key, idx ){
                         var aux = key.split( '_' );
                         generalF.laborales += mes["_"+aux[0]];
                     } );
                     gTecnica.usuarios.forEach( function( key, idx ){
                         var aux = key.split( '_' );
                         gTecnica.laborales += mes["_"+aux[0]];
                     } );
                     gBlandas.usuarios.forEach( function( key, idx ){
                         var aux = key.split( '_' );
                         gBlandas.laborales += mes["_"+aux[0]];
                     } );
                     gProducto.usuarios.forEach( function( key, idx ){
                         var aux = key.split( '_' );
                         gProducto.laborales += mes["_"+aux[0]];
                     } );
                     gMarca.usuarios.forEach( function( key, idx ){
                         var aux = key.split( '_' );
                         gMarca.laborales += mes["_"+aux[0]];
                     } );

                    // render Horas
                    $('#h_lab').html( new Intl.NumberFormat("de-DE").format( generalF.usuarios.length ) );
                    $('#h_cap').html( new Intl.NumberFormat("de-DE").format( generalF.capacitacion ) );
                    var _aux_ = generalF.capacitacion > 0 && generalF.usuarios.length > 0 ?  generalF.capacitacion / generalF.usuarios.length : 0;
                    $('#p_hor').html( _aux_.toFixed(1) );
                    // render Horas Tecnica
                    $('#h_t_lab').html( new Intl.NumberFormat("de-DE").format( gTecnica.usuarios.length ) );
                    $('#h_t_cap').html( new Intl.NumberFormat("de-DE").format( gTecnica.capacitacion ) );
                    var _aux_ = gTecnica.capacitacion > 0 && gTecnica.usuarios.length > 0 ?  gTecnica.capacitacion / gTecnica.usuarios.length : 0;
                    $('#p_t_hor').html( _aux_.toFixed(1) );
                    // render Horas Blandas
                    $('#h_b_lab').html( new Intl.NumberFormat("de-DE").format( gBlandas.usuarios.length ) );
                    $('#h_b_cap').html( new Intl.NumberFormat("de-DE").format( gBlandas.capacitacion ) );
                    var _aux_ = gBlandas.capacitacion > 0 && gBlandas.usuarios.length > 0 ?  gBlandas.capacitacion / gBlandas.usuarios.length : 0;
                    $('#p_b_hor').html( _aux_.toFixed(1) );
                    // render Horas Producto
                    $('#h_p_lab').html( new Intl.NumberFormat("de-DE").format( gProducto.usuarios.length ) );
                    $('#h_p_cap').html( new Intl.NumberFormat("de-DE").format( gProducto.capacitacion ) );
                    var _aux_ = gProducto.capacitacion > 0 && gProducto.usuarios.length > 0 ?  gProducto.capacitacion / gProducto.usuarios.length : 0;
                    $('#p_p_hor').html( _aux_.toFixed(1) );
                    // render Horas Marca
                    $('#h_m_lab').html( new Intl.NumberFormat("de-DE").format( gMarca.usuarios.length ) );
                    $('#h_m_cap').html( new Intl.NumberFormat("de-DE").format( gMarca.capacitacion ) );
                    var _aux_ = gMarca.capacitacion > 0 && gMarca.usuarios.length > 0 ?  gMarca.capacitacion / gMarca.usuarios.length : 0;
                    $('#p_m_hor').html( _aux_.toFixed(1) );

                }

            },//fin renderHorasCapacitacion
            //=================================================================
            // Genera una grafica tipo funnel
            //=================================================================
            graficaFunnel: function( idx, data, labels, titulo ){
                var self = this;
                var ctx = document.getElementById( idx ).getContext('2d');
                // Copio el array de labels
                var newLabels = labels.slice();

                // asigno valor a los labels
                data.forEach( function( key, idx ){
                    newLabels[idx] += ': '+key;
                } );

                var areaChartData = {
                  labels  : newLabels,
                  datasets: [
                    {
                        data: data,
                        backgroundColor: self.colores.slice(0,5),
                        hoverBackgroundColor: self.colores.slice(0,5),
                    }
                  ]
                }

                var config = {
                    showAllTooltips: false,
                    type: 'horizontalBar',
                    data: areaChartData,
                    options: {
                        legend: {
                            display: false,
                            text: titulo,
                        },
                        title: {
                          display: false,
                          text: titulo
                        },
                        sort: 'desc',
                    }
                };
                return new Chart(ctx, config);
            },
            //=================================================================
            // Genera una grafica tipo funnel
            //=================================================================
            graficaBarras: function( idx, data, colores, labels, promZona, promPais, colorPais ){
                var self = this;
                var ctx = document.getElementById( idx ).getContext('2d');
                var config = {
                    type: 'bar',
                    data: {
                        datasets: [
                        {
                            label: "Promedio Zona",
                            data: promZona,
                            type: 'line'
                        },
                        {
                            label: 'Promedio Nota',
                            data: data,
                            backgroundColor: colores,
                            hoverBackgroundColor: colores,
                        },
                        {
                            label: "Promedio País",
                            data: promPais,
                            type: 'line',
                            hoverBackgroundColor: colorPais[0],
                            borderColor: colorPais[0],
                            fill: false
                        }],
                        labels: labels
                    },
                    options: {
                        sort: 'desc',
                        display: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                }
                            }]
                        }
                    }
                };
                return new Chart(ctx, config);
            },
            //=================================================================
            // Calcula el promedio de las notas de las encuestas
            //=================================================================
            renderNotasEncuestas: function( data, filtro ){
                // console.log( "data entregada ",data, "filtro", filtro );
                var tipos = ["IBT","OJT","VCT"]; // ,"WBT"
                var satisfaccion_GMAcademy_35 = 0;
                var instructor_36 = 0;
                var ev_curso_37 = 0;
                var encuestas = 0;
                var min_35 = 0, max_35 = 0, min_36 = 0, max_36 = 0, min_37 = 0, max_37 = 0;
                switch( filtro ){
                    case 'todos':
                        for ( v of tipos ) {
                            encuestas += data[ v ].encuestas;
                            min_35 += data[ v ].pregunta_35.min;
                            max_35 += data[ v ].pregunta_35.max;
                            min_36 += data[ v ].pregunta_36.min;
                            max_36 += data[ v ].pregunta_36.max;
                            min_37 += data[ v ].pregunta_37.min;
                            max_37 += data[ v ].pregunta_37.max;
                        }
                        if( encuestas > 0 ){
                            satisfaccion_GMAcademy_35 = ( ( ( max_35 - min_35 ) / encuestas ) * 100 ).toFixed(1);
                            instructor_36 = ( ( ( max_36 - min_36 ) / encuestas ) * 100 ).toFixed(1);
                            ev_curso_37 = ( ( ( max_37 - min_37 ) / encuestas ) * 100 ).toFixed(1);
                        }
                    break;
                    default:
                        if( data[ filtro ].encuestas > 0 ){
                                satisfaccion_GMAcademy_35 = ( ( ( data[ filtro ].pregunta_35.max - data[ filtro ].pregunta_35.min ) / data[ filtro ].encuestas ) * 100 ).toFixed(1);
                                instructor_36 = ( ( ( data[ filtro ].pregunta_36.max - data[ filtro ].pregunta_36.min ) / data[ filtro ].encuestas ) * 100 ).toFixed(1);
                                ev_curso_37 = ( ( ( data[ filtro ].pregunta_37.max - data[ filtro ].pregunta_37.min ) / data[ filtro ].encuestas ) * 100 ).toFixed(1);
                        }
                    break;

                }//fin switch

                $('#satisfaccion_gral').html( satisfaccion_GMAcademy_35 );
                $('#instruc_facilitador').html( instructor_36 );
                $('#contenido_curso').html( ev_curso_37 );

            },//fin renderNotasEncuestas
            /* =========================================================
            Ejecuta la consulta de los datos para desplegar las graficas
            ========================================================= */
            ejecutarDashboard: function(){
                var self = this;
                $( '#form_dashboard' ).submit( function( e ){
                    e.preventDefault();

                    if( self.validar() ){
                        NProgress.start();
                        var datos = {};
                        $('#consultaAnalytics').attr('disabled',true);
                        var url = "controllers/dashboard_new.php";
                        self.concesionarioSel = $( '#concesionarios' ).val() || [];
                        self.fechaSel = $( '#fecha' ).val() || [];

                        datos.zonas = $( '#zonas' ).val() ? $( '#zonas' ).val().join() : "" ;
                        datos.concesionarios = $( '#concesionarios' ).val() ? $( '#concesionarios' ).val().join() : '' ;
                        datos.fecha = $( '#fecha' ).val().join() ;
                        datos.opcn = "datos";

                        console.info( "datos para enviar",datos );

                        $.ajax({
                            url         : url,
                            type        : 'post',
                            data        : datos,
                            dataType    : 'json'
                        })
                        .done( function( data ){
                            // console.table( data.quarters );
                            // console.log( data );
                            self.sdatos         = data.sdatos;
                            self.sdatos_pais    = data.sdatos_pais;
                            self.usuarios_trayect    = data.usuarios_trayect;
                            self.encuestas      = data.encuestas;
                            self.promedio_pais  = data.pais;
                            self.total_usuarios_con_trayectorias = 0;

                            //row 1 -> col 1
                            //PAC
                            self.concesionarios = self.agruparConcesionarios( data.concesionarios );

                            //row 1
                            //Historico
                            var quarters = self.calcularQuarters( data.quarters );
                            var _q_1 = quarters.q1.aprobado > 1 ? "fa-check" : "fa-times";
                            $("#q1").html( `${ quarters.q1.num > 0 ? (quarters.q1.cumplimiento / quarters.q1.num ).toFixed(1) : 0 } <i class="fa ${_q_1}"></i>` );
                            var _q_2 = quarters.q2.aprobado > 1 ? "fa-check" : "fa-times";
                            $("#q2").html( `${ quarters.q2.num > 0 ? (quarters.q2.cumplimiento / quarters.q2.num ).toFixed(1) : 0 } <i class="fa ${_q_2}"></i>` );
                            var _q_3 = quarters.q3.aprobado > 1 ? "fa-check" : "fa-times";
                            $("#q3").html( `${ quarters.q3.num > 0 ? (quarters.q3.cumplimiento / quarters.q3.num ).toFixed(1) : 0 } <i class="fa ${_q_3}"></i>` );
                            var _q_4 = quarters.q4.aprobado > 1 ? "fa-check" : "fa-times";
                            $("#q4").html( `${ quarters.q4.num > 0 ? (quarters.q4.cumplimiento / quarters.q4.num ).toFixed(1) : 0 } <i class="fa ${_q_4}"></i>` );

                            //row 3
                            //Población
                            var trayectorias = self.renderTrayectorias( data.trayectorias, data.categorias );
                            self.total_usuarios_con_trayectorias = trayectorias.total;

                            console.log( "trayectorias", trayectorias );
                            $('#tabla1').html('');
                            $('#tabla2').html('');
                            $('#tabla1').append( trayectorias.tabla1['Alta Gerencia'].tabla );
                            $('#tabla1').append( trayectorias.tabla1['Gerencia Media'].tabla );
                            $('#tabla2').append( trayectorias.tabla1['Productivo'].tabla );
                            $('#tabla2').append( trayectorias.tabla1['Voluntario'].tabla );
                            $('#total_trayectorias_usr').html( trayectorias.total );
                            // $('#total_usuarios_trayectorias').html( data.trayectoriaUsuarios );
                            $('#total_usuarios').html( data.usuarios );

                            //row 4
                            //Participacion
                            self.renderParticipacion( self.sdatos, self.total_usuarios_con_trayectorias );

                            // row 5
                            // horas capacitacion
                            self.renderHorasCapacitacion( data.sdatos );

                            //row 6
                            // promedio concesionarios
                            self.renderPromedioZona( 'all' );

                            // row 7
                            //encuestas segmentadas por metodologias
                            self.renderNotasEncuestas( self.encuestas, 'todos');

                            // row 8 -> 1
                            //ranking asesores con mas cursos APROBADOS
                            self.renderRankingEstudiantesCursos( data.rankingCursados );
                            // row 8 -> 2
                            //ranking asesores con mejor promedio
                            self.renderRankingEstudiantesNotas( data.rankingEstudiantes );

                        } )
                        .always( function(){
                            NProgress.done();
                            $('#consultaAnalytics').removeAttr('disabled');
                        } );//fin ajax

                    }//fin if validar

                } );//fin submit
            }

        };
    } )();
    graf.Init();
} )( document, window, undefined, jQuery );
