$(document).ready(function(){
    $("#dealer_id").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
    $("#course_id").select2({
        placeholder: "Seleccione un curso",
        allowClear: true
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
});
