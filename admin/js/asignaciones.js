$(document).on("ready", function () {
  confirmaSeleccionados();
  if ($("#calendar_asignaciones").length) {
    $("#calendar_asignaciones").fullCalendar({
      //lang: 'es',
      header: {
        left: "prev,next today",
        center: "title",
        right: "month,agendaWeek,agendaDay",
      },
      editable: false,
      droppable: false,
      events: rootPath + "/admin/ajax/calendar_asignaciones.php?opcn=asig",
      loading: function (bool) {
        if (bool) $("#loading").show();
        else $("#loading").hide();
      },
      eventRender: function (event, element) {
        element.qtip({
          //content: event.description,
          content: {
            text: event.description,
            title: {
              text: event.title,
            },
          },
          style: { classes: "qtip-rounded qtip-" + event.color },
          hide: {
            fixed: true, // Helps to prevent the tooltip from hiding ocassionally when tracking!
          },
          position: {
            my: "bottom center",
            at: "top center",
          },
        });
      },
    });
  }
  if ($("#calendar_dash_ins").length) {
    $("#calendar_dash_ins").fullCalendar({
      //lang: 'es',
      header: {
        left: "prev,next today",
        center: "title",
        right: "month,agendaWeek,agendaDay",
      },
      editable: false,
      droppable: false,
      events: rootPath + "/admin/ajax/calendar_asignaciones.php?opcn=ins",
      loading: function (bool) {
        if (bool) $("#loading").show();
        else $("#loading").hide();
      },
      eventRender: function (event, element) {
        element.qtip({
          //content: event.description,
          content: {
            text: event.description,
            title: {
              text: event.title,
            },
          },
          style: { classes: "qtip-rounded qtip-" + event.color },
          hide: {
            fixed: true, // Helps to prevent the tooltip from hiding ocassionally when tracking!
          },
          position: {
            my: "bottom center",
            at: "top center",
          },
        });
      },
    });
  }
  $("#EnvResults").click(function () {
    notyfy({
      text: "Esta seguro de almacenar la asignación",
      type: "confirm",
      dismissQueue: true,
      layout: "top",
      buttons: [
        {
          addClass: "btn btn-success btn-small btn-icon glyphicons ok_2",
          text: "<i></i> Aceptar",
          onClick: function ($notyfy) {
            $notyfy.close();
            GuardarAsignacion();
          },
        },
        {
          addClass: "btn btn-danger btn-small btn-icon glyphicons remove_2",
          text: "<i></i> Cancelar",
          onClick: function ($notyfy) {
            $notyfy.close();
            notyfy({
              force: true,
              text: "<strong>Se ha cancelado la asignación<strong>",
              type: "error",
              layout: "top",
            });
          },
        },
      ],
    });
  });
  if (parseInt($("#Cant_cap").text()) <= 0) {
    desactiva_ck();
  }
  if ($("#dealer_id").attr("type") != "hidden") {
    $("#dealer_id").select2({
      placeholder: "Seleccione un estado",
      allowClear: true,
    });
  }
  $("#busqueda_pal").keyup(function () {
    var PalabraBusqueda = $("#busqueda_pal").val().toUpperCase();
    $(".reg_busqueda").each(function (index, element) {
      if (PalabraBusqueda != "") {
        var ElementText = $(element).text().toUpperCase();
        var index_del = ElementText.indexOf(PalabraBusqueda);
        if (index_del > 0) {
          $(element).show();
        } else {
          $(element).hide();
        }
      } else {
        $(element).show();
      }
    });
  });
  ValInicial();

  $("#boton_filtro").click(function () {
    var datos = {};
    datos.dealer_id = $("#dealer_id").val();
    datos.filtro = $("#busqueda_pal").val();
    datos.schedule_id = $("#schedule_id").val();
    datos.usuarios = listarUsuarios();
    datos.opcn = "buscar";
    //console.log( datos );
    $.post(
      "controllers/asignaciones.php",
      datos,
      function (data) {
        $("#tablaUsuarios").append(data);
        $(".nuevos").bootstrapSwitch();
        if (parseInt($("#Cant_cap").text()) > 0) {
          activa_ck();
          activa_ck_sel();
        }
        $(".make-switch").removeClass("nuevos");
      },
      "html"
    );
  });
});
//====================================================================================
//Funciones globales
//====================================================================================
function listarUsuarios() {
  var elementos = document.getElementsByName("id");
  var usr = "0";
  for (var i = 0; i < elementos.length; i++) {
    usr += "," + elementos[i].value;
  }
  return usr;
}
function ValInicial() {
  var CantidadCapacidad = parseInt($("#Cant_cap").text());
  var CantidadSeleccionada = parseInt($("#Cant_sel").text());
  if (CantidadCapacidad > 0) {
    activa_ck();
  }
  if (CantidadSeleccionada > 0) {
    activa_ck_sel();
  }
}
var DataSeleccionados = [];

function confirmaSeleccionados() {
  $(".make-switch").each(function (index, element) {
    var id_el = $(element).prop("id");
    var nom_check = id_el.replace("Sw_", "");
    var CodAsg = nom_check.replace("ChkAsig", "");
    var CodAsg_agr = parseInt(CodAsg);
    if ($("#" + nom_check).prop("checked")) {
      DataSeleccionados[DataSeleccionados.length] = CodAsg_agr;
    }
  });
}
function activa_ck_sel() {
  $(".make-switch").each(function (index, element) {
    var idSw = $(element).attr("id");
    var idEl = idSw.substr(3);
    if ($("#" + idEl).prop("checked") != false) {
      $("#" + idSw).bootstrapSwitch("setActive", true);
      $("#" + idEl).attr("disabled", false);
    } else {
      //$('#'+idSw).bootstrapSwitch('setActive', false);
      //$("#"+idEl).attr("disabled", true);
    }
  });
}
function CheckAsigna(control, user_id) {
  if ($(control).prop("checked") == false) {
    //console.log($("#Cant_cap").text());
    if (parseInt($("#Cant_cap").text()) > 0) {
      activa_ck();
    }

    var cant_Activos = 0;
    $(".make-switch").each(function (index, element) {
      var idSw = $(element).attr("id");
      var idEl = idSw.substr(3);
      if ($("#" + idEl).prop("checked") != false) {
        cant_Activos = cant_Activos + 1;
      }
    });

    var cantidadOriginal = parseInt($("#Cant_capOriginal").val());
    var cantidad_capac = cantidadOriginal - cant_Activos;

    $("#Cant_sel").text(cant_Activos);
    $("#Cant_cap").text(cantidad_capac);

    var index_del = DataSeleccionados.indexOf(user_id);
    if (index_del > -1) {
      DataSeleccionados.splice(index_del, 1);
    }
    if (parseInt($("#Cant_cap").text()) <= 0) {
      desactiva_ck();
    } else {
      activa_ck();
    }
  }
  if ($(control).prop("checked")) {
    if (parseInt($("#Cant_cap").text()) <= 1) {
      desactiva_ck();
    }

    var cant_Activos = 0;
    $(".make-switch").each(function (index, element) {
      var idSw = $(element).attr("id");
      var idEl = idSw.substr(3);
      if ($("#" + idEl).prop("checked") != false) {
        cant_Activos = cant_Activos + 1;
      }
    });

    var cantidadOriginal = parseInt($("#Cant_capOriginal").val());
    var cantidad_capac = cantidadOriginal - cant_Activos;
    $("#Cant_sel").text(cant_Activos);
    $("#Cant_cap").text(cantidad_capac);

    DataSeleccionados[DataSeleccionados.length] = user_id;
  }
}
function desactiva_ck_ajax() {
  $(".make-switch").each(function (index, element) {
    var idSw = $(element).attr("id");
    var idEl = idSw.substr(3);
    if ($("#" + idEl).prop("checked") == false) {
      $("#" + idSw).bootstrapSwitch("setActive", false);
      $("#" + idEl).attr("disabled", true);
    }
  });
}
function desactiva_ck() {
  notyfy({
    text: "Ha llegado al número máximo de usuarios que puede asignar según el cupo del curso",
    type: "information", // alert|error|success|information|warning|primary|confirm
  });
  $(".make-switch").each(function (index, element) {
    var idSw = $(element).attr("id");
    var idEl = idSw.substr(3);
    if ($("#" + idEl).prop("checked") == false) {
      $("#" + idSw).bootstrapSwitch("setActive", false);
      $("#" + idEl).attr("disabled", true);
    }
  });
}
function activa_ck() {
  $(".make-switch").each(function (index, element) {
    var idSw = $(element).attr("id");
    var idEl = idSw.substr(3);
    if ($("#" + idEl).attr("disabled") == "disabled") {
      $("#" + idSw).bootstrapSwitch("setActive", true);
      $("#" + idEl).attr("disabled", false);
    }
  });
}
function GuardarAsignacion() {
  $("#loading_asignacion").removeClass("hide");
  $("#EnvResults").prop("disabled", true);
  var data = new FormData();
  data.append("DataSeleccionados", DataSeleccionados.join(","));
  data.append("schedule_id", $("#schedule_id").val());
  data.append("dealer_id", $("#dealer_id").val());
  data.append("opcn", "GuardarSchedule");
  var url = "controllers/asignaciones.php";
  $.ajax({
    url: url,
    type: "post",
    contentType: false,
    data: data,
    processData: false,
    dataType: "json",
    cache: false,
    success: function (data) {
      $("#EnvResults").prop("disabled", false);
      if (!data.error) {
        notyfy({
          text: "La asignación ha sido almacenada correctamente",
          type: "success", // alert|error|success|information|warning|primary|confirm
        });
      } else {
        notyfy({
          text: data.message,
          type: data.tipo, // alert|error|success|information|warning|primary|confirm
        });
      }
      $("#loading_asignacion").addClass("hide");
    },
  });
}
$(document).ready(function () {
  // By suppling no content attribute, the library uses each elements title attribute by default
  $("a[href][title]").qtip({
    content: {
      text: false, // Use each elements title attribute
    },
    style: {
      tip: "leftMiddle",
      color: "white",
      background: "#A2D959",
      name: "green",
    }, // Give it some style
  });
  // NOTE: You can even omit all options and simply replace the regular title tooltips like so:
  // $('#content a[href]').qtip();
});

// cerrar curso MFR
$("#Cerrar_MFR").click(function () {
  if (!confirm("¿Estas seguro de finalizar el curso?")) {
    return false;
  }
  var datos = {};
  datos.schedule_id = $.getURL("schedule_id") || 0;
  datos.opcn = "finalizar_curso_MFR";
  $.ajax({
    url: "controllers/asignaciones.php?opcn=finalizar_curso_MFR",
    data: datos,
    type: "post",
    dataType: "json",
  })
    .done(function (data) {
      console.log(data);
      if (data.Error) {
        alert(data.Msj);
      } else {
        alert(data.Msj);
      }
    })
    .always(function () {
      $("#finalizar").prop("disabled", false);
    });
});
