$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/evaluaciones_r.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});
$(document).ready(function(){
    // Select Placeholders
    $("#cursos").select2({
        placeholder: "Seleccione el (los) cursos",
        allowClear: true
    });
    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#question_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});

$( "#formulario_data" ).submit(function( event ) {
    console.log( "ingreso al formulario" );
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            console.log("Editar el formulario");
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
    event.preventDefault();
});

$("#retornaElemento").click(function() {
    window.location="evaluaciones_r.php";
});

function valida(){
    var valid = 0;
    $('#code').css( "border-color", "#efefef" );
    $('#review').css( "border-color", "#efefef" );

    var valid = 0;
    if($('#code').val() == ""){
        $('#code').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#review').val() == ""){
        $('#review').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg){
    // console.log( $('#formulario_data').serialize() );
    $.ajax({ url: 'controllers/evaluaciones_r.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

/*
script para editar las preguntas en una evaluacion

@ idQuestion:   parametro que almacena el id de la pregunta que se desea
@ valorActual:  es la pregunta que esta actualmente y que se va a cambiar
@ idReview:     es el id de la evaluacion que se va a actualizar
*/
function CambiarAEditar(idQuestion, valorActual, idReview){
  //funcion ajax para consultar las pregunta
    $('#Desc'+idQuestion).load('controllers/evaluaciones_r.php', {opcn:'ConsultarPreguntas',idQuestion:idQuestion}, function(response, status, xhr){
        if ( status == "error" ) {
            var msg = "Ha ocurrido un error: ";
            notyfy({
                text: msg + xhr.status + " " + xhr.statusText,
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
            //fin if status == 'error'
        }else{
            $("#preg"+idQuestion).select2({
                placeholder: "Seleccione una pregunta",
                allowClear: true
            });
            //funcion JQuery para reemplazar el boton "EDITAR" pro "GUARDAR"
            $("#BtnAccion_"+idQuestion).html('<a class="btn-information" href="op_evaluacion.php" onclick="GuardarPregCambiada('+idQuestion+','+idReview+'); return false;">Guardar</a>');
        }//fin else : status == 'success'
    });
    $('#Val'+idQuestion).html('<input class="form-control" id="value'+idQuestion+'" name="value" type="text" placeholder="Peso de la pregunta Ej 10" value="'+valorActual+'" />');
}//fin funcion CambiarAEditar


/*
script para guardar mediante AJAX las nuevas preguntas, modificadas en las evaluaciones

@ idQuestion: id de la pregunta que se va a guardar en la evaluacion
@ idReview: id de la evaluacion
*/
function GuardarPregCambiada(idQuestion,idReview){
    var pregSel = $("#preg"+idQuestion).val();
    var valueSel = $("#value"+idQuestion).val();
    var data = new FormData();
    data.append('idQuestion',idQuestion);//id de la anterior pregunta
    data.append('idReview',idReview);//id de la evaluacion a editar
    data.append('pregSel',pregSel);//valor de la nueva pregunta
    data.append('ValueSel',valueSel);//valor seleccionado para la nota
    data.append('opcn','GuardarPreguntas');//opcion a ejecutar dentro del controlador
    var url = "controllers/evaluaciones_r.php";


        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            cache:false,
            success: function(data) {
                if(data.resultado=="SI"){
                    notyfy({
                        text: 'La pregunta ha sido cambiada correctamente',
                        type: 'success' // alert|error|success|information|warning|primary|confirm
                    });
                    //reemplazo los valores y los ID anteriores por los nuevos
                    $("#Desc"+idQuestion).html($("#preg"+idQuestion+" option:selected").text());
                    $("#Desc"+idQuestion).attr("id","Desc"+pregSel);//reemplaza el anterior ID por el nuevo ID, en la pregunta
                    $("#Val"+idQuestion).html(valueSel);
                    $("#Val"+idQuestion).attr("id","Val"+pregSel);//reemplaza el anterior ID por el nuevo ID, en el valor
                    $("#BtnAccion_"+idQuestion).attr("id","BtnAccion_"+pregSel);

                    $("#BtnAccion_"+pregSel).html('<a class="btn-inverse" href="op_evaluacion.php" id="editar_'+pregSel+'" onclick="CambiarAEditar('+pregSel+','+valueSel+','+idReview+'); return false;">Editar</a>');
//'<a class="btn-inverse" href="op_evaluacion.php" id="editar_'++'" onclick="CambiarAEditar('+$("#preg"+idQuestion).val()+','+$("#value"+idQuestion).val()+','+idReview+')" return false>Editar</a>'
                }else{
                    notyfy({
                        text: 'La pregunta NO ha sido cambiada, revise que no hayan usuarios que ya la han respondido anteriormente, o que ya se encuentre asignada a esta evaluación',
                        type: 'error' // alert|error|success|information|warning|primary|confirm
                    });

                }
                //$('#loading_imagen').addClass('hide');
            }//fin funcion : success

        });//fin funcion ajax JQuery

}//fin metodo GuardarPregCambiada
