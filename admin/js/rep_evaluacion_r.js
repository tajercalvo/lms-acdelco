$(document).ready(function(){
    // Select Placeholders
    /*$("#schedule_id").select2({
        placeholder: "Seleccione una sesión",
        allowClear: true
    });*/
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $("#review_id").select2({
        placeholder: "Seleccione una evaluación",
        allowClear: true
    });
    //var Timer_ImgLanza = setTimeout('CargarGraficas()', 1000);
    if($('#GrafGral').length){
        CargarGraficas();
        CargarGraficasDet();
    }

$("#mibody").css("visibility","visible");
   $("#cargando_pagina").html("");

});


function Cargarbody(){
$("#mibody").css("visibility","visible");
   $("#cargando_pagina").html("");

}

Cargarbody();

function CargarGraficas(){
    var DataNames = [];
    var DatGralInfo = [];
    DataNames.push([0, "Resp 1" ]);
    DataNames.push([1, "Resp 2" ]);
    DataNames.push([2, "Resp 3" ]);
    DataNames.push([3, "Resp 4" ]);
    DataNames.push([4, "Resp 5" ]);
    DataNames.push([5, "Resp 6" ]);
    DataNames.push([6, "Resp 7" ]);
    DataNames.push([7, "Resp 8" ]);
    DataNames.push([8, "Resp 9" ]);
    DataNames.push([9, "Resp 10" ]);
    DatGralInfo.push([0, $('#TotValues').attr('dat-1') ]);
    DatGralInfo.push([1, $('#TotValues').attr('dat-2') ]);
    DatGralInfo.push([2, $('#TotValues').attr('dat-3') ]);
    DatGralInfo.push([3, $('#TotValues').attr('dat-4') ]);
    DatGralInfo.push([4, $('#TotValues').attr('dat-5') ]);
    DatGralInfo.push([5, $('#TotValues').attr('dat-6') ]);
    DatGralInfo.push([6, $('#TotValues').attr('dat-7') ]);
    DatGralInfo.push([7, $('#TotValues').attr('dat-8') ]);
    DatGralInfo.push([8, $('#TotValues').attr('dat-9') ]);
    DatGralInfo.push([9, $('#TotValues').attr('dat-10') ]);
    var ds = new Array();
    ds.push({
        label: "Cantidad",
        data: DatGralInfo,
        bars: {order: 1}
    });
    var data = ds;
    $.plot($("#GrafGral"), data, {
        bars: {
            show:true,
            barWidth: 0.2,
            fill:1
        },
        grid: {
            hoverable: true,
            clickable: false,
            borderWidth: 1
        },
        legend: {
            labelBoxBorderColor: "none",
            backgroundColor: "#FFFFFF",
            backgroundOpacity: 0.3,
            position: "ne"
        },
        series: {
            shadowSize: 1,
            grow: {active:false}
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        },
        xaxis: {
            ticks: DataNames
        }
    });
}

//IINCIO PARA DESCARGAR TABLA REDIRECIONANDO A OTRA PAGINA
$("#descargar").click(function(){

     $('#review_id').css( "border-color", "#ffffff" );
        $('#start_date_day').css( "border-color", "#ffffff" );
        $('#end_date_day').css( "border-color", "#ffffff" );

        inicio= new Date($('#start_date_day').val());
        finalq= new Date($('#end_date_day').val());
        if(inicio>finalq){
            bootbox.alert("La fecha inicial no puede ser menor que la fecha final", function(result){

            });
            return false;
        }


        var  review_id = $('#review_id').val();
        var start_date_day = $('#start_date_day').val();
        var end_date_day = $('#end_date_day').val();

     if (review_id == '' || start_date_day == '' || end_date_day == '') {

        bootbox.alert("Por favor completa la información antes de realizar la descarga, escoge una fecha inicial, una fecha final y una encuesta", function(result) {
            //Codigo...
            });

        $('#review_id').css( "border-color", "#b94a48" );
        $('#start_date_day').css( "border-color", "#b94a48");
        $('#end_date_day').css( "border-color", "#b94a48" );

    }else{

        var url = "rep_encuesta_excel.php?start_date_day=" + start_date_day + "&end_date_day=" + end_date_day + "&" + "&review_id=" + review_id + "&";
        //WINDOWS.OPEN, PARA REDIRECIONAR A OTRA PAGINA CON TARGET BLANK
        window.open(url, '_blank');
    }
});
