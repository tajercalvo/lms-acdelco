$(document).ready(function(){
    // Select Placeholders
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    
    $('#wrapper').css("visibility", 'visible');

$( "#formulario_usuarios" ).submit(function( event ) {
    if(validacion()){
    	console.log('SI entro al si');
    	return true;
    }
    console.log('No entro al si');
    return false;
    //event.preventDefault();
});


$('#btn_consultar').click(function(){
    
    if (validacion()) {

                //Inahibilitando el boton mientras se hace la consulta
                $('#btn_consultar').attr("disabled", true);

                // limpiando el mensaje de color verde, cuando la consulta viene vacia
                $( "#tablas_Asesores_Comerciales" ).html('');
                $( "#tablas_Asesores_Tecnico" ).html('');
                

                //Limpiando los tablas del asesor comercial
                $("#tabla_Asesor_comercial_mp tbody").html('');
                $("#tabla_Asesor_comercial_mz tbody").html('');
                $("#tabla_Asesor_comercial_mc tbody").html('');
                $("#imagen_mp tbody").html('');
                $("#h4_nombre_asesor").text('');
                $('#well_asesor_comercial').css("visibility", 'hidden');

                //Limpiando los datos del asesor ctecnico
                $("#tabla_Asesor_tecnico_mp tbody").html('');
                $("#tabla_Asesor_Tecnico_mz tbody").html('');
                $("#tabla_Asesor_Tecnico_mc tbody").html('');
                $("#imagen_Tecnico_mp tbody").html('');
                $("#h4_nombre_Tecnico").text('');
                $('#well_asesor_Tecnico').css("visibility", 'hidden');

                //Mostrando mensaje descargando
                $( "#mensaje_descargando" ).html('<span class="label label-success">Consultando...</span><img style="width: 15px; " src="../assets/loading.gif">');
              
                var data = new FormData();
                data.append('start_date_day',$('#start_date_day').val());
                data.append('end_date_day',$('#end_date_day').val());
                data.append('opcn','consulta');
                var url = "controllers/rep_mejor.php";
                $.ajax({
                    url:url,
                    type:'post',
                    contentType:false,
                    data:data,
                    processData:false,
                    dataType: "json",
                    async: true,
                    cache:false,
                    success: function(data) {

                        //Ocultando mensanje consultando
                        $( "#mensaje_descargando" ).html('');

                        //Mostrando el div well que contiene las tablas
                        $('#well_asesor_comercial').css("visibility", 'visible');
                        $('#well_asesor_Tecnico').css("visibility", 'visible');

                        //Habilitando nuevamente el boton de para consultar
                        $('#btn_consultar').attr("disabled", false);
                        
                        // contando las posiciones del array mejor asesor pais
                        var contador_ac=0;
                        for(var i in  data['Asesor_Comercial_mp']) {
                            contador_ac = contador_ac + 1;
                            break;    
                        }
                            // si existe mas de un resultado, modifica los datos
                            if (contador_ac > 0) {

                                    //INICIO MOSTRANDO ELEMENTOS ASESORES COMERCIALES
                                    $('#h4_nombre_asesor').text(data['Asesor_Comercial_mp'][0]['first_name']+' '+data['Asesor_Comercial_mp'][0]['last_name']);
                                    $('#imagen_mp').html('<img id class="img-responsive" src="../assets/images/usuarios/'+data['Asesor_Comercial_mp'][0]['image']+' " style="width: 74px;">');

                                    $('#tabla_Asesor_comercial_mp  tbody:last-child').append('<tr> <td>'+data['Asesor_Comercial_mp'][0]['mejores']+'</td><td> '+data['Asesor_Comercial_mp'][0]['cantidad_curso']+'</td> <td>'+data['Asesor_Comercial_mp'][0]['promedio']+'</td> <td>'+data['Asesor_Comercial_mp'][0]['ventas']+'</td> </tr>');

                                    //TABLAL MEJOR ASESOR POR ZONA
                                    for(var i in  data['Asesor_Comercial_mz']) {

                                                            $('#tabla_Asesor_comercial_mz  tbody:last-child').append('<tr> <td>'+data['Asesor_Comercial_mz'][i]['mejores']+'<br>'+data['Asesor_Comercial_mz'][i]['zone']+'</td> <td> '+data['Asesor_Comercial_mz'][i]['first_name']+'<br>'+data['Asesor_Comercial_mz'][i]['identification']+'</td> <td> '+data['Asesor_Comercial_mz'][i]['cantidad_curso']+'</td> <td>'+data['Asesor_Comercial_mz'][i]['promedio']+'</td> <td>'+data['Asesor_Comercial_mz'][i]['ventas']+'</td> </tr>');
                                                            
                                                    }
                                    //TABLAL MEJOR ASESOR POR CONCESIONARIO
                                    for(var i in  data['Asesor_Comercial_mc']) {

                                                    
                                                    $('#tabla_Asesor_comercial_mc  tbody:last-child').append('<tr> <td>'+data['Asesor_Comercial_mc'][i]['mejores']+'<br>'+data['Asesor_Comercial_mc'][i]['dealer']+'</td> <td> '+data['Asesor_Comercial_mc'][i]['first_name']+'<br>'+data['Asesor_Comercial_mc'][i]['identification']+'</td> <td> '+data['Asesor_Comercial_mc'][i]['cantidad_curso']+'</td> <td>'+data['Asesor_Comercial_mc'][i]['promedio']+'</td> <td>'+data['Asesor_Comercial_mc'][i]['ventas']+'</td> </tr>');
                                                    
                                            }

                                    //FIN //INICIO MOSTRANDO ELEMENTOS ASESORES COMERCIALES

                            }else{


                                //Mensaje de color verde dentro del well
                                $( "#tablas_Asesores_Comerciales" ).html('<span class="label label-success">Asesores Comerciales sin resultado</span>');
                                //console.log('sin resultados');
                            }


                        //Inicio Asesores Tecnico
                       var contador_at = 0;

                        for(var i in  data['Asesor_Tecnico_mp']) {
                            contador_at = contador_at + 1;
                            break;    
                        }

                            if (contador_at > 0) {

                                    //INICIO MOSTRANDO ELEMENTOS ASESORES TECNICOS
                                    $('#h4_nombre_Tecnico').text(data['Asesor_Tecnico_mp'][0]['first_name']+' '+data['Asesor_Tecnico_mp'][0]['last_name']);
                                    $('#imagen_Tecnico_mp').html('<img id class="img-responsive" src="../assets/images/usuarios/'+data['Asesor_Tecnico_mp'][0]['image']+' " style="width: 74px;">');

                                    $('#tabla_Asesor_tecnico_mp  tbody:last-child').append('<tr> <td> '+data['Asesor_Tecnico_mp'][0]['cantidad_curso']+'</td> <td>'+data['Asesor_Tecnico_mp'][0]['promedio']+'</td> <td>'+Math.round(data['Asesor_Tecnico_mp'][0]['academico'])+'</td> </tr>');

                                    //TABLAL MEJOR ASESOR POR ZONA
                                    for(var i in  data['Asesor_Tecnico_mz']) {

                                                            $('#tabla_Asesor_Tecnico_mz  tbody:last-child').append('<tr> <td>'+data['Asesor_Tecnico_mz'][i]['zone']+'<br>'+Math.round(data['Asesor_Tecnico_mz'][i]['academico']) +'</td> <td> '+data['Asesor_Tecnico_mz'][i]['first_name']+'<br>'+data['Asesor_Tecnico_mz'][i]['identification']+'</td> <td> '+data['Asesor_Tecnico_mz'][i]['cantidad_curso']+'</td> <td>'+data['Asesor_Tecnico_mz'][i]['promedio']+'</td>   </tr>');
                                                            
                                                    }
                                    //TABLAL MEJOR ASESOR POR CONCESIONARIO
                                    for(var i in  data['Asesor_Tecnico_mc']) {
                                                    
                                                            $('#tabla_Asesor_Tecnico_mc  tbody:last-child').append('<tr> <td>'+data['Asesor_Tecnico_mc'][i]['dealer']+'<br>'+Math.round(data['Asesor_Tecnico_mc'][i]['academico'])+'</td> <td> '+data['Asesor_Tecnico_mc'][i]['first_name']+'<br>'+data['Asesor_Tecnico_mc'][i]['identification']+'</td> <td> '+data['Asesor_Tecnico_mc'][i]['cantidad_curso']+'</td> <td>'+data['Asesor_Tecnico_mc'][i]['promedio']+'</td> </tr>');
                                                    
                                            }

                                     //FIN //INICIO MOSTRANDO ELEMENTOS ASESORES COMERCIALES


                            }else{
                                //$( "#tablas_Asesores_Comerciales" ).html('<span class="label label-success">Asesores Comerciales sin resultado</span>');
                                $( "#tablas_Asesores_Tecnico" ).html('<span class="label label-success">Asesores Técnicos sin resultado</span>');
                            }


                    } // fin function data


                })
                .fail(function() { 
                  
                  $('#btn_consultar').attr("disabled", false);
                  $( "#mensaje_descargando" ).html('');
                  $( "#Atencion" ).html('<div id="gritter-notice-wrapper"><div id="gritter-item-7" class="gritter-item-wrapper gritter-primary"><div class="gritter-top"></div><div class="gritter-item"><span class="gritter-title">Atención</span><p>has perdido tu conexion con el servidor, verifica que estes conectado a internet e intenta nuevamente, por favor valida haciendo clic aquí <a href="http://www.gmacademy.co" target="_blank">Clic aqui para iniciar sesion</a> </p></div></div></div>');
                  setTimeout(function() {

                                $(".gritter-item-wrapper").fadeOut(5000); 

                          },3000); 
                  
                })

                    setTimeout(function() {

                                $(".gritter-item-wrapper").fadeOut(5000); 

                          },3000); 
    }// Fin if(validacion)
});

 $("#start_date_day").keypress(function(tecla) {
        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
    });

    $("#end_date_day").keypress(function(tecla) {
        if(tecla.charCode < 48 || tecla.charCode > 57) return false;
    });

});



function validacion() {

        inicio= new Date($('#start_date_day').val());
        finalq= new Date($('#end_date_day').val());
        if(inicio>finalq){

                    bootbox.alert("La fecha inicial no puede ser menor que la fecha final", function(result) 
                {
                    
                });
                    return false;
        }

      	s_date = document.getElementById("start_date_day").value;
      	e_date = document.getElementById("end_date_day").value;

        $('#start_date_day').css( "border-color", "#ffffff" );
        $('#end_date_day').css( "border-color", "#ffffff" );
       
    	if( s_date == '' & e_date == '') {
    	// alert('Complete la informacion antes de consultarla');
        $('#start_date_day').css( "border-color", "#b94a48" );
        $('#end_date_day').css( "border-color", "#b94a48");
        $( "#start_date_day" ).focus();
        return false;
        } 
         else if($('#start_date_day').val() == ""){
            $('#start_date_day').css( "border-color", "#b94a48" );
            $( "#start_date_day" ).focus();
        return false;
        } else if($('#end_date_day').val() == ""){
            $('#end_date_day').css( "border-color", "#b94a48" );
            $( "#end_date_day" ).focus();
        return false;
        } 
        return true;
    }


