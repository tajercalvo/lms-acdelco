$(document).ready(function(){
    // Select Placeholders
    $("#cargos").select2({
        placeholder: "Seleccione una trayectoria",
        allowClear: true
    });
    // Select Placeholders
    $('#start_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
    $('#end_date_day').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "2015-01-01"
    });
});