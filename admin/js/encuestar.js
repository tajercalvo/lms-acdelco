$(document).ready(function(){
    $('#loadingRespuesta').addClass('hide');
});

/*$( "#formulario_respuestas" ).submit(function( event ) {
    //Variables
        $('#loadingRespuesta').removeClass('hide');
        var valid = 0;
        var valid_1_ct = 0;
        var valid_2_ct = 0;
        var valid_3_ct = 0;
        var valid_4_ct = 0;
        var valid_5_ct = 0;
        var valid_6_ct = 0;
        var valid_7_ct = 0;
        var valid_8_ct = 0;
        var valid_9_ct = 0;
        var val_rep35 = 0;
        var val_rep36 = 0;
        var val_rep37 = 0;
        var val_rep38 = 0;
        var val_rep39 = 0;
        var val_rep40 = 0;
        var val_rep41 = 0;
        var val_rep42 = 0;
        var val_rep43 = 0;
        var _val_resp35 = 0;
        var _val_resp36 = 0;
        var _val_resp37 = 0;
        var _val_resp38 = 0;
        var _val_resp39 = 0;
        var _val_resp40 = 0;
        var _val_resp41 = 0;
        var _val_resp42 = 0;
        var _val_resp43 = 0;
    //Variables
    //Validaciones
        $('#44').css( "border-color", "#efefef" );
        if($('#44').val() == ""){
            $('#44').css( "border-color", "#b94a48" );
            valid = 1;
        }
        $(".Quest35").each(function (index,element) {
            if($(this).is(':checked')){
                valid_1_ct = 1;
                val_rep35 = $(this).val();
                _val_resp35 = $(this).attr('data-vlr');
                console.log("::"+val_rep35+"::"+_val_resp35);
            }
        });
        $(".Quest36").each(function (index,element) {
            if($(this).is(':checked')){
                valid_2_ct = 1;
                val_rep36 = $(this).val();
                _val_resp36 = $(this).attr('data-Vlr');
                console.log("::"+val_rep36+"::"+_val_resp36);
            }
        });
        $(".Quest37").each(function (index,element) {
            if($(this).is(':checked')){
                valid_3_ct = 1;
                val_rep37 = $(this).val();
                _val_resp37 = $(this).attr('data-Vlr');
                console.log("::"+val_rep37+"::"+_val_resp37);
            }
        });
        $(".Quest38").each(function (index,element) {
            if($(this).is(':checked')){
                valid_4_ct = 1;
                val_rep38 = $(this).val();
                _val_resp38 = $(this).attr('data-Vlr');
                console.log("::"+val_rep38+"::"+_val_resp38);
            }
        });
        $(".Quest39").each(function (index,element) {
            if($(this).is(':checked')){
                valid_5_ct = 1;
                val_rep39 = $(this).val();
                _val_resp39 = $(this).attr('data-Vlr');
                console.log("::"+val_rep39+"::"+_val_resp39);
            }
        });
        $(".Quest40").each(function (index,element) {
            if($(this).is(':checked')){
                valid_6_ct = 1;
                val_rep40 = $(this).val();
                _val_resp40 = $(this).attr('data-Vlr');
                console.log("::"+val_rep40+"::"+_val_resp40);
            }
        });
        $(".Quest41").each(function (index,element) {
            if($(this).is(':checked')){
                valid_7_ct = 1;
                val_rep41 = $(this).val();
                _val_resp41 = $(this).attr('data-Vlr');
                console.log("::"+val_rep41+"::"+_val_resp41);
            }
        });
        $(".Quest42").each(function (index,element) {
            if($(this).is(':checked')){
                valid_8_ct = 1;
                val_rep42 = $(this).val();
                _val_resp42 = $(this).attr('data-Vlr');
                console.log("::"+val_rep42+"::"+_val_resp42);
            }
        });
        $(".Quest43").each(function (index,element) {
            if($(this).is(':checked')){
                valid_9_ct = 1;
                val_rep43 = $(this).val();
                _val_resp43 = $(this).attr('data-Vlr');
                console.log("::"+val_rep43+"::"+_val_resp43);
            }
        });

        if(valid_1_ct==0 || valid_2_ct==0 || valid_3_ct==0 || valid_4_ct==0 || valid_5_ct==0 || valid_6_ct==0 || valid_7_ct==0 || valid_8_ct==0 || valid_9_ct==0){
            valid = 1;
        }
        if(_val_resp35==0 || _val_resp36==0 || _val_resp37==0 || _val_resp38==0 || _val_resp39==0 || _val_resp40==0 || _val_resp41==0 || _val_resp42==0 || _val_resp43==0){
            valid = 1;
        }
    //Validaciones

    if(valid == 0){
        if(confirm('¿Esta seguro de contestar la encuesta con estas respuestas?')){
            var data = new FormData();
            data.append('val_rep35',val_rep35);
            data.append('val_rep36',val_rep36);
            data.append('val_rep37',val_rep37);
            data.append('val_rep38',val_rep38);
            data.append('val_rep39',val_rep39);
            data.append('val_rep40',val_rep40);
            data.append('val_rep41',val_rep41);
            data.append('val_rep42',val_rep42);
            data.append('val_rep43',val_rep43);
            data.append('_val_resp35',_val_resp35);
            data.append('_val_resp36',_val_resp36);
            data.append('_val_resp37',_val_resp37);
            data.append('_val_resp38',_val_resp38);
            data.append('_val_resp39',_val_resp39);
            data.append('_val_resp40',_val_resp40);
            data.append('_val_resp41',_val_resp41);
            data.append('_val_resp42',_val_resp42);
            data.append('_val_resp43',_val_resp43);
            data.append('val_rep44',$('#44').val());
            data.append('FechaDebio',$('#Fec_Debio').val());
            data.append('opcn','GuardaEncuesta');
            var url = "controllers/encuestar.php";
            $.ajax({
                url:url,
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                cache:false,
                success: function(data) {
                    $('#loadingRespuesta').addClass('hide');
                    if(data.resultado=="SI"){
                        notyfy({
                            text: 'Muchas gracias tu encuesta ha sido procesada',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                        $('#ContenidoEnc').html('<div class="innerAll"><h4 class="innerTB margin-none half center">Muchas Gracias por su tiempo. Reiteramos que la prioridad de Chevrolet y GMAcademy es hacer clientes felices.</h4><div class="separator bottom"></div><div class="row">Es posible que tenga encuestas pendientes por responder, las cuales irán iendo una a una hasta finalizarlas por completo; recuerde que tiene 5 puntos en sus calificaciones disponibles por cada encuesta que responda en las siguientes 48 horas luego de la actividad formativa.<div id="BtnIniciar" class="innerAll text-center"><a href="index.php" class="btn btn-success">Aceptar</a></div></div></div>');
                    }else{
                        notyfy({
                            text: 'No se ha podido almacenar tu encuesta, comprueba tu conexión a internet',
                            type: 'warning' // alert|error|success|information|warning|primary|confirm
                        });
                    }
                }
            });
        }else{
            $('#loadingRespuesta').addClass('hide');
            notyfy({
                text: 'ha cancelado el envío de las respuestas para esta encuesta, puede intentar nuevamente dando clic en Responder!',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
        }
    }else{
        $('#loadingRespuesta').addClass('hide');
        notyfy({
            text: 'Hay preguntas sin responder, por favor verifíque las respuesas e intente nuevamente',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
    event.preventDefault();
});*/


$( "#formulario_respuestas" ).submit(function( event ) {
    $('#loadingRespuesta').removeClass('hide');
    var ProcesaTodo = 0;
    var MsgError = "Preguntas que aún no ha diligenciado:<br>";
    $(".Data_Radio").each(function (index,element) {
        var DataRadio_Ctrl = 0;
        var IdRadio = $(this).val();
        $(".Quest"+IdRadio).each(function (index,element) {
            if($(this).is(':checked')){
                DataRadio_Ctrl = 1;
            }
        });
        if(DataRadio_Ctrl==0){
            ProcesaTodo = 1;
            MsgError = MsgError + "Pregunta: "+ $(this).attr('data-lbl')+"<br>";
        }
    });
    /*$(".CampText").each(function (index,element) {
        if($(this).val()==""){
            ProcesaTodo = 1;
            MsgError = MsgError + "Pregunta: "+ $(this).attr('data-lbl')+"<br>";
        }
    });*/

    if(ProcesaTodo==1){
        notyfy({
            text: MsgError,
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }else{
        notyfy({
            text: 'Esta seguro de enviar estas respuestas?',
            type: 'confirm',
            dismissQueue: true,
            layout: 'top',
            buttons: ([{
                addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
                text: '<i></i> Aceptar',
                onClick: function ($notyfy) {
                    $notyfy.close();
                        $.ajax({ url: 'controllers/encuestar.php',
                            data: $('#formulario_respuestas').serialize(),
                            type: 'post',
                            dataType: "json",
                            success: function(data) {
                                $('#loadingRespuesta').addClass('hide');
                                var res0 = data.resultado;
                                if(res0 == "SI"){
                                    notyfy({
                                        text: 'Hemos recibido la información de la encuesta, muchas gracias',
                                        type: 'success' // alert|error|success|information|warning|primary|confirm
                                    });
                                    $('#ContenidoEnc').html('<div class="innerAll"><h4 class="innerTB margin-none half center">Muchas Gracias por su tiempo. Reiteramos que la prioridad de Chevrolet y GMAcademy es hacer clientes felices.</h4><div class="separator bottom"></div><div class="row">Es posible que tenga encuestas pendientes por responder, las cuales irán iendo una a una hasta finalizarlas por completo.<div id="BtnIniciar" class="innerAll text-center"><a href="index.php" class="btn btn-success">Aceptar</a></div></div></div>');
                                }else{
                                    notyfy({
                                        text: 'No se ha logrado recibir la información, por favor confirme su conexión a internet y envíela nuevamente',
                                        type: 'error' // alert|error|success|information|warning|primary|confirm
                                    });
                                }
                            }
                        });
                }
            }, {
                addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
                text: '<i></i> Cancelar',
                onClick: function ($notyfy) {
                    $notyfy.close();
                    notyfy({
                        force: true,
                        text: '<strong>Ha cancelado el envío de la encuesta<strong>',
                        type: 'error',
                        layout: 'top'
                    });
                }
            }])
        });
    }
    event.preventDefault();
    $('#loadingRespuesta').addClass('hide');
});


