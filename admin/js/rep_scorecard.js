var stack = 0, bars = false, lines = true, steps = false;
var dataCharEs_act = [];
var dataCharEs_end = [];
var dataCharSub_act = [];
var dataCharSub_end = [];
var Data_Ano1 = [];
var Data_Ano2 = [];
var DataNames = [];

var dataCharEs_act_Es = [];
var dataCharEs_end_Es = [];
var dataCharSub_act_Es = [];
var dataCharSub_end_Es = [];

var dataCharTipo_act = [];
var dataCharTipo_end = [];
var dataCharSistema_act = [];
var dataCharSistema_end = [];

var Data_Ano1_Mes = [];
var Data_Ano2_Mes = [];
var DataNames_Mes = [];
(function($)
{
    $('#ano_id_end').select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    setTimeout(function(){
            CargarDatos_SentinelXConcesionario();
        }, 100);
    setTimeout(function(){
            CargarDatos_SentinelXMes();
        }, 100);
    setTimeout(function(){
            Cargar_CharEstados();
        }, 100);
    setTimeout(function(){
            Cargar_CharSubEstados();
        }, 100);
    setTimeout(function(){
            Cargar_CharEstados_Es();
        }, 100);
    setTimeout(function(){
            Cargar_CharSubEstados_Es();
        }, 100);
    setTimeout(function(){
            Cargar_CharTipos();
        }, 100);
    setTimeout(function(){
            Cargar_CharSistemas();
        }, 100);
    setTimeout(function(){
            QuitaBordes();
        }, 200);
})(jQuery);

/*$(document).ready(function(){
});*/

function CargarDatos_SentinelXConcesionario(){
    Data_Ano1 = [];
    Data_Ano2 = [];
    DataNames = [];
    var _CtrlVar = 0;
    $(".DatosAnos").each(function (index) {
        Data_Ano1.push([_CtrlVar, $(this).attr('dat-ano1')]);
        Data_Ano2.push([_CtrlVar, $(this).attr('dat-ano2')]);
        DataNames.push([_CtrlVar, $(this).attr('dat-conse')]);
        _CtrlVar = _CtrlVar + 1;
    });
    var ds = new Array();
        if($('#opcn_Ano1').is(':checked')){
            ds.push({
                label: $('#Ano1_sel').val(),
                data: Data_Ano1,
                bars: {order: 1}
            });
        }
        if($('#opcn_Ano2').is(':checked')){
            ds.push({
                label: $('#Ano2_sel').val(),
                data: Data_Ano2,
                bars: {order: 2}
            });
        }
    var data = ds;

        $.plot($("#charSentinelXConcesionarios"), data, {
            bars: {
                show: true,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                grow: {active:false},
                shadowSize: 1
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames,
                tickOptions:{ 
                    angle: -60
                }
            }
        });
    CambiarEtiquetas();
}
function CargarDatos_SentinelXMes(){
    Data_Ano1_Mes = [];
    Data_Ano2_Mes = [];
    DataNames_Mes = [];
    var _CtrlVar = 0;
    $(".DatMesAno").each(function (index) {
        Data_Ano1_Mes.push([_CtrlVar, $(this).attr('dat-ano1')]);
        Data_Ano2_Mes.push([_CtrlVar, $(this).attr('dat-ano2')]);
        DataNames_Mes.push([_CtrlVar, $(this).attr('dat-name')]);
        _CtrlVar = _CtrlVar + 1;
    });
    var ds = new Array();
        if($('#opcn_Ano1').is(':checked')){
            ds.push({
                label: $('#Ano1_sel').val(),
                data: Data_Ano1_Mes,
                bars: {order: 1}
            });
        }
        if($('#opcn_Ano2').is(':checked')){
            ds.push({
                label: $('#Ano2_sel').val(),
                data: Data_Ano2_Mes,
                bars: {order: 2}
            });
        }
    var data = ds;

        $.plot($("#charSentinelXMes"), data, {
            bars: {
                show: false,
                barWidth: 0.2,
                fill:1
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 1
            },
            legend: {
                labelBoxBorderColor: "none",
                backgroundColor: "#FFFFFF",
                backgroundOpacity: 0.3,
                position: "ne"
            },
            series: {
                shadowSize: 1,
                grow: {active:false}
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : %y",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            xaxis: {
                ticks: DataNames_Mes,
                tickOptions:{ 
                    angle: -60
                }
            }
        });
    CambiarEtiquetas();
}

function CambiarEtiquetas(){
    $(".flot-tick-label").each(function (index) {
        $( this ).css( "-webkit-transform", "rotate(-45deg)" );
        $( this ).css( "transform", "rotate(-45deg)" );
        $( this ).css( "-ms-transform", "rotate(-45deg)" );
        $( this ).css( "-moz-transform", "rotate(-45deg)" );
        $( this ).css( "-o-transform", "rotate(-45deg)" );
        $( this ).css( "font-size", "8px" );
    });
}

function Cargar_CharEstados(){
    $(".legendColorBox").children("div").each(function (index,element) {
        $(element).css("border","0");
    });
    $(".EstadoAct_Val").each(function (index,element) {
        dataCharEs_act.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $(".EstadoEnd_Val").each(function (index,element) {
        dataCharEs_end.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $.plot('#charEstadosAnoAct', dataCharEs_act, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
    /*$.plot('#charEstadosAnoAct', dataCharEs_act, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
        },
        grid: { 
            hoverable:true 
        },
        shadowSize:1,
        tooltip: true,
        tooltipOpts: {
            content: "%s : %y",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });*/
    $.plot('#charEstadosAnoEnd', dataCharEs_end, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
}

function Cargar_CharSubEstados(){
    $(".SubEstadoAct_Val").each(function (index,element) {
        dataCharSub_act.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $(".SubEstadoEnd_Val").each(function (index,element) {
        dataCharSub_end.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $.plot('#charSubEstadosAnoAct', dataCharSub_act, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
    $.plot('#charSubEstadosAnoEnd', dataCharSub_end, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
}

function Cargar_CharTipos(){
    $(".TipoAct_Val").each(function (index,element) {
        dataCharTipo_act.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $(".TipoEnd_Val").each(function (index,element) {
        dataCharTipo_end.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $.plot('#charTiposAnoAct', dataCharTipo_act, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
    $.plot('#charTiposAnoEnd', dataCharTipo_end, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
}

function Cargar_CharSistemas(){
    $(".SistemaAct_Val").each(function (index,element) {
        dataCharSistema_act.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $(".SistemaEnd_Val").each(function (index,element) {
        dataCharSistema_end.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $.plot('#charSistemasAnoAct', dataCharSistema_act, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
    $.plot('#charSistemasAnoEnd', dataCharSistema_end, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
}

function Cargar_CharEstados_Es(){
    $(".Paso_EstadoAct_Val").each(function (index,element) {
        dataCharEs_act_Es.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $(".Paso_EstadoEnd_Val").each(function (index,element) {
        dataCharEs_end_Es.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $.plot('#Es_charEstadosAnoAct', dataCharEs_act_Es, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
    $.plot('#Es_charEstadosAnoEnd', dataCharEs_end_Es, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
}

function Cargar_CharSubEstados_Es(){
    $(".Esc_SubEstadoAct_Val").each(function (index,element) {
        dataCharSub_act_Es.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $(".Esc_SubEstadoEnd_Val").each(function (index,element) {
        dataCharSub_end_Es.push({ label: $(element).attr("dat-name")+" : "+$(element).val(), data: $(element).val() });
    });
    $.plot('#Es_charSubEstadosAnoAct', dataCharSub_act_Es, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
    $.plot('#Es_charSubEstadosAnoEnd', dataCharSub_end_Es, {
        series: {
            pie: {
                show: true,
                innerRadius: 0.2,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:black;z-index:10000 !important;">'+Math.round(series.percent)+'%</div>';
                    },
                    background: { opacity: 0.3 }
                }
            }
        },
        legend:{show:true},
        grid: { 
            hoverable:true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%s",
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        }
    });
}
function QuitaBordes(){
    $(".legendColorBox").children("div").each(function (index,element) {
        $(element).css("border","0");
    });
    $(".legendLabel").each(function (index,element) {
        $(element).css("text-align","left");
    });
}