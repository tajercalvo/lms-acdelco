$(document).ready(function(){
	$('#date_birthday').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "1915-01-01"
    });
    $('#date_startgm').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "1960-01-01"
    });
    $("#gender").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
     $("#pais").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
});

$( "#formulario_usuarios" ).submit(function( event ) {
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        
    }
    event.preventDefault();
});


function valida(){
    var valid = 0;
    $('#first_name').css( "border-color", "#efefef" );
    $('#last_name').css( "border-color", "#efefef" );
    $('#date_birthday').css( "border-color", "#efefef" );
    $('#date_startgm').css( "border-color", "#efefef" );
    $('#email').css( "border-color", "#efefef" );
    $('#phone').css( "border-color", "#efefef" );

    var valid = 0;
    if($('#first_name').val() == ""){
        $('#first_name').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#last_name').val() == ""){
        $('#last_name').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#date_birthday').val() == ""){
        $('#date_birthday').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#date_startgm').val() == ""){
        $('#date_startgm').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#email').val() == ""){
        $('#email').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#phone').val() == ""){
        $('#phone').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente. La identificación sólo admite valores numéricos',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}
function Operacion(msg){
    $.ajax({ url: 'controllers/ed_usuarios.php',
        data: $('#formulario_usuarios').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'El usuario ya se encuentra creado en otro CONCESIONARIO, comuníquese con el administrador para que le asigne dicho USUARIO',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}
