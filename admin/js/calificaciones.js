$(document).ready(function () {
    /*  $('#CalificarAsistencia').hide() */
    //  $('#EnvResults').hide()
    //  $('#btnAsistencia').hide()
     if ($('#userOJT_id').length) {
       $('#userOJT_id').select2({
         placeholder: 'Seleccione una sesión',
         allowClear: true
       })
       $('#formulario_CalifOJT').submit(function (event) {
         if (validaCalOJT()) {
           return true
         } else {
           return false
         }
         event.preventDefault()
       })
     }
     // Select Placeholders
     $('#fecha_inicial').bdatepicker({
       format: 'yyyy-mm-dd',
       startDate: '2014-01-01'
     })
     $('#fecha_final').bdatepicker({
       format: 'yyyy-mm-dd',
       startDate: '2014-01-01'
     })
     $('#schedule_id').select2({
       placeholder: 'Seleccione una sesión',
       allowClear: true
     })
   
     $('#fecha_inicial').change(function () {
       if (
         $('#fecha_inicial').val().length == 10 &&
         $('#fecha_final').val().length == 10
       ) {
         ConsultaSesion()
       }
     })
     $('#fecha_final').change(function () {
       if (
         $('#fecha_inicial').val().length == 10 &&
         $('#fecha_final').val().length == 10
       ) {
         ConsultaSesion()
       }
     })
     $('#formulario_search').submit(function (event) {
       if (valida()) {
         return true
       } else {
         return false
       }
       event.preventDefault()
     })
     $('#EnvResults').click(function () {
       notyfy({
         text: 'Esta seguro de almacenar la calificación y terminar el curso? verifique si el curso tiene actividades antes de proseguir',
         type: 'confirm',
         dismissQueue: true,
         layout: 'top',
         buttons: [
           {
             addClass: 'btn btn-success btn-small btn-icon glyphicons ok_2',
             text: '<i></i> Aceptar',
             onClick: function ($notyfy) {
               $notyfy.close()
               GuardarAsignacion()
               location.reload()
               $('#EnvResults').hide()
             }
           },
           {
             addClass: 'btn btn-danger btn-small btn-icon glyphicons remove_2',
             text: '<i></i> Cancelar',
             onClick: function ($notyfy) {
               $notyfy.close()
               notyfy({
                 force: true,
                 text: '<strong>Se ha cancelado la calificación<strong>',
                 type: 'error',
                 layout: 'top'
               })
             }
           }
         ]
       })
     })
     $('#CalificarAsistencia').click(function () {
       Swal.fire({
           title: '¿Está seguro de la acción?',
           text: 'Esta seguro de almacenar la calificación y terminar el curso? verifique si el curso tiene actividades y esten calificadas antes de proseguir?',
           icon: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Aceptar, Deseo finalizar el curso',
           cancelButtonText: 'No, Cancelar'
       }).then((result) => {
           if (result.isConfirmed) {
               GuardarAsistencia();
               $('#CalificarAsistencia').hide();
               location.reload();
           }
       });
   });
     validaActividades()
   })
   
   function ConsultaSesion () {
     var fec_ini = $('#fecha_inicial').val()
     var fec_fin = $('#fecha_final').val()
     $('#schedule_id').html('')
     $.post(
       'controllers/calificaciones.php',
       { opcn: 'sesiones', fec_ini: fec_ini, fec_fin: fec_fin },
       function (data) {
         $('#schedule_id').html(data)
         $('#schedule_id').select2({
           placeholder: 'Seleccione una sucursal',
           allowClear: true
         })
       }
     )
   }
   
   function GuardarAsignacion() {
    $('#loading_calificacion').removeClass('hide')
    $.ajax({
      url: 'controllers/calificaciones.php',
      data: $('#formulario_data').serialize(),
      type: 'post',
      dataType: 'json',
      success: function (data) {
        var res0 = data.resultado
        if (res0 == 'SI') {
          notyfy({
            text: 'La operación ha sido completada satisfactoriamente',
            type: 'success' // alert|error|success|information|warning|primary|confirm
          })
        } else {
          notyfy({
            text:
              'No se ha logrado ' +
              msg +
              ' la información, por favor confirme su conexión y realicela nuevamente',
            type: 'error' // alert|error|success|information|warning|primary|confirm
          })
        }
        $('#loading_calificacion').addClass('hide')
      }
    })
  }
   function valida () {
     var valid = 0
     if ($('#schedule_id').val() == null) {
       valid = 1
     }
     if (valid == 0) {
       if (confirm('¿Desea consultar para calificar esta sesión?')) {
         return true
       } else {
         notyfy({
           text: 'ha cancelado la operación',
           type: 'information' // alert|error|success|information|warning|primary|confirm
         })
         return false
       }
     } else {
       notyfy({
         text: 'Debe seleccionar una sesión para poder realizar la calificación',
         type: 'warning' // alert|error|success|information|warning|primary|confirm
       })
     }
   }
   
   function validaCalOJT () {
     var valid = 0
     $('#CalifOJT').css('border-color', '#efefef')
     var valid = 0
     if ($('#CalifOJT').val() == '') {
       $('#CalifOJT').css('border-color', '#b94a48')
       valid = 1
     }
     if ($('#CalifOJT').val() > 100) {
       $('#CalifOJT').css('border-color', '#b94a48')
       valid = 1
     }
     if (valid == 0) {
       if (confirm('¿Desea crear esta calificación?')) {
         return true
       } else {
         notyfy({
           text: 'ha cancelado la operación',
           type: 'information' // alert|error|success|information|warning|primary|confirm
         })
         return false
       }
     } else {
       notyfy({
         text: 'Por favor revise la información antes de calificar',
         type: 'warning' // alert|error|success|information|warning|primary|confirm
       })
     }
   }
   function CheckAsigna(control, user_id) {
     if ($(control).prop('checked') == false) {
       $('#Calif' + user_id).val();
       $('#Calif' + user_id).attr('disabled', true);
       $(control).val('NO');
     }
     if ($(control).prop('checked')) {
       $('#Calif' + user_id).attr('disabled', false);
       $(control).val('SI');
     }
   }
   
   
   
   
   //se califica la asistencia para ILT con actividades
   $('#btnAsistencia').click(function (e) {
    $('#loading_calificacion').removeClass('hide')
    $.ajax({
      url: 'controllers/calificaciones.php',
      data: $('#formulario_data').serialize(),
      type: 'post',
      dataType: 'json',
      success: function (data) {
        var res0 = data.resultado
        if (res0 == 'SI') {
          notyfy({
            text: 'La operación ha sido completada satisfactoriamente',
            type: 'success' // alert|error|success|information|warning|primary|confirm
          })
        } else {
          notyfy({
            text:
              'No se ha logrado ' +
              msg +
              ' la información, por favor confirme su conexión y realicela nuevamente',
            type: 'error' // alert|error|success|information|warning|primary|confirm
          })
        }
        $('#loading_calificacion').addClass('hide')
      }
    })
   });
   
   
   function GuardarAsistencia () {
     let data = $('#formulario_data').serializeArray()
     data.push({
       name: 'opcn',
       value: 'asistencia'
     })
     $.ajax({
       url: 'controllers/calificaciones.php',
       data: data,
       type: 'post',
       dataType: 'json',
       success: function (data) {
         var res0 = data.resultado
         if (res0 == 'SI') {
           notyfy({
             text: 'La operación ha sido completada satisfactoriamente',
             type: 'success'
           })
         } else {
           notyfy({
             text:
               'No se ha logrado ' +
               msg +
               ' la información, por favor confirme su conexión y realicela nuevamente',
             type: 'error'
           })
         }
         $('#loading_calificacion').addClass('hide')
       }
     })
   }
   
   function validaActividades () {
     $(document).ready(function () {
       $('#ConsultaRepLog').click(function (event) {
         event.preventDefault()
         $('#formulario_data').show()
         let tipoValor = $('#type').val()
         let valorActi = $('#acti').val()
         let finallyValue = $('#finally').val()
         let final = $('#calificacion_curso').val()
         let btnAsistencia = $('#btnAsistencia')
       /*   let btnCalificarAsistencia = $('#CalificarAsistencia') */
   
      //    if (finallyValue == 0) {
      //      $('#EnvResults').hide()
      //      btnAsistencia.hide()
      //  /*     btnCalificarAsistencia.hide() */
      //    } else {
      //      if (
      //        tipoValor === 'VCT' ||
      //        tipoValor === 'MFR' ||
      //        tipoValor === 'WBT' ||
      //        tipoValor === 'OJT'
      //      ) {
      //        $('#EnvResults').show().prop('disabled', false)
      //        btnAsistencia.hide()
      //       /*  btnCalificarAsistencia.hide() */
      //      } else if (tipoValor === 'ILT' && valorActi > 0) {
      //        $('#EnvResults').hide()
      //        if (final == 2) {
      //          btnAsistencia.hide()
      //        /*   btnCalificarAsistencia.hide() */
      //        } else if (final == 1) {
      //          btnAsistencia.hide()
      //         /*  btnCalificarAsistencia.show().prop('disabled', false) */
      //        } else {
      //          btnAsistencia.show().prop('disabled', false)
      //          /* btnCalificarAsistencia.hide() */
      //        }
      //      } else {
      //        btnAsistencia.hide()
      //       /*  btnCalificarAsistencia.hide() */
      //        $('#EnvResults').show().prop('disabled', false)
      //      }
      //    }
       })
     })
   }
   
  
  
  
  
  
  
  