(function (){
    
    $(document).ready(function () {
        module_result_usr_id = $('#module_result_usr_id').html();
    });
    
    $('#frm').submit(function (e) { 
        e.preventDefault();
        if(module_result_usr_id == 0){
            alert('No puedes subir esta actividad, comunicate con el administrador');
            return false;
        }
        var formData = new FormData();
        formData.append("opcn", "responder_actividad");
        formData.append("comentarios", $('#comentarios').val());
        formData.append("module_result_usr_id", module_result_usr_id);
        if( $('#archivo_actual').length ){
            formData.append("archivo_actual", $('#archivo_actual').data('archivo_actual'));
        }
        
        var archivo = document.getElementById("adjunto");
    
        if (archivo.files[0] === undefined) {
            
            if(!confirm('No has adjuntado ningun archivo a esta actividad, \n ¿desea continuar?')){
                return false;
            }
         }else{
            formData.append("adjunto", archivo.files[0]);
         }
         
        $('.btn').attr('disabled', true);
        $('#animacion').toggle();
        $.ajax({
            url: 'controllers/op_actividades.php',
            type: "post",
            dataType: "json",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
            .done(function(data){
                $('#animacion').toggle();
                if(data.error){
                    notyfy({
                        text: data.msj,
                        type: 'error' // alert|error|success|information|warning|primary|confirm
                    });
                    $('.btn').attr('disabled', false);
                }else{
                    alert(data.msj)
                    window.location="actividades.php";
                }
    
            });
    });
})();
// Captura el textarea por su ID
var textarea = document.getElementById('comentarios');

// Función para mover el cursor a la posición 0 al hacer clic
textarea.addEventListener('click', function () {
    textarea.selectionStart = 0;  // Coloca el cursor en la posición 0
    textarea.selectionEnd = 0;    // También lo finaliza en la posición 0
});