$(document).ready(function(){
	// Select Placeholders
	$("#review_id").select2({
		placeholder: "Seleccione una sesión",
		allowClear: true
	});
	$("#review_id_r").select2({
		placeholder: "Seleccione una sesión",
		allowClear: true
	});
});

function confirmar(){
	var r = confirm("¿Está seguro de eliminar el resultado de esta evaluación?");
	if (r) {
		return true;
	}else{
		return false;
	}
}

function eliminar( id ){
	if( confirmar() ){
		$.ajax( {
			url: 'controllers/rep_evaluaciones_incompletas.php',
			data: { 'opcn':'eliminar', 'reviews_answer_id': id },
			dataType: 'json',
			type: 'post'
		} )
		.done( function( data ){
			if( !data.error ){
				if( data.eliminado == "SI" ){
					$( '#rev_'+id ).remove();
					notyfy({
		                text: 'Se reinicio correctamente la evaluación',
		                type: 'success' // alert|error|success|information|warning|primary|confirm
		            });
				}
			}else{
				notyfy({
	                text: 'No se ha logrado completar la operación',
	                type: 'error' // alert|error|success|information|warning|primary|confirm
	            });
			}
		} );
	}//fin confirmar
}//fin eliminar

$( '#form_evaluaciones' ).submit( function( e ){
	e.preventDefault();
	$("#review_id_r").select2("val", "");
	$.ajax( {
		url: 'controllers/rep_evaluaciones_incompletas.php',
		data: { 'opcn':'listado', 'review_id': $('#review_id').val() },
		dataType: 'json',
		type: 'get'
	} )
	.done( function( data ){
		console.log( data );
		var _html = '';
		console.log( data.registros, data.registros.length );
		if( !data.error ){
			if( data.registros.length > 0 ){
				data.registros.map( function( key, idx ){
					_html += `<tr id="rev_${ key.reviews_answer_id }" >
						<td>${ key.review }</td>
						<td>${ key.first_name+" "+key.last_name }</td>
						<td>${ key.identification }</td>
						<td>${ key.headquarter }</td>
						<td>${ key.dealer }</td>
						<td>${ key.score }</td>
						<td>${ key.date_creation }</td>
						<td>${ key.time_response }</td>
						<td>
							<button class="btn btn-success btn-xs" onclick="eliminar( ${ key.reviews_answer_id } )">Eliminar</button>
						</td>
					</tr>`;
				} );

			}else{
				notyfy({
	                text: 'No hay registros para esta evaluación',
	                type: 'warning' // alert|error|success|information|warning|primary|confirm
	            });
			}

			$('#evaluaciones').html( _html );
		}
	} );
} );

function eliminarR( id ){
	if( confirmar() ){
		$.ajax( {
			url: 'controllers/rep_evaluaciones_incompletas.php',
			data: { 'opcn':'eliminar_r', 'reviews_answer_id': id },
			dataType: 'json',
			type: 'post'
		} )
		.done( function( data ){
			if( !data.error ){
				if( data.eliminado == "SI" ){
					$( '#rev_r_'+id ).remove();
					notyfy({
		                text: 'Se reinicio correctamente la evaluación',
		                type: 'success' // alert|error|success|information|warning|primary|confirm
		            });
				}
			}else{
				notyfy({
	                text: 'No se ha logrado completar la operación',
	                type: 'error' // alert|error|success|information|warning|primary|confirm
	            });
			}
		} );
	}//fin confirmar
}//fin eliminar

$( '#form_evaluaciones_r' ).submit( function( e ){
	e.preventDefault();
	$("#review_id").select2("val", "");
	$.ajax( {
		url: 'controllers/rep_evaluaciones_incompletas.php',
		data: { 'opcn':'listado_r', 'review_id': $('#review_id_r').val() },
		dataType: 'json',
		type: 'get'
	} )
	.done( function( data ){
		console.log( data );
		var _html = '';
		if( !data.error ){
			if( data.registros.length > 0 ){
				data.registros.map( function( key, idx ){
					_html += `<tr id="rev_r_${ key.reviews_answer_id }" >
						<td>${ key.review }</td>
						<td>${ key.first_name+" "+key.last_name }</td>
						<td>${ key.identification }</td>
						<td>${ key.headquarter }</td>
						<td>${ key.dealer }</td>
						<td>${ key.score }</td>
						<td>${ key.date_creation }</td>
						<td>${ key.time_response }</td>
						<td>
							<button class="btn btn-success btn-xs" onclick="eliminarR( ${ key.reviews_answer_id } )">Eliminar</button>
						</td>
					</tr>`;
				} );

			}else{
				notyfy({
	                text: 'No hay registros para esta evaluación',
	                type: 'warning' // alert|error|success|information|warning|primary|confirm
	            });
			}

			$('#evaluaciones').html( _html );
		}
	} );
} );
