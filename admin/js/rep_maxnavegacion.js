$(document).ready(function(){
// Select Placeholders
  $("#dealer_id").select2({
    placeholder: "Seleccione una sesión",
    allowClear: true
  });
  $('#start_date_day').bdatepicker({
    format: "yyyy-mm-dd",
    startDate: "2012-01-01"
  });
  $('#end_date_day').bdatepicker({
    format: "yyyy-mm-dd",
    startDate: "2012-01-01"
  });
});
$(document).ready(function() {
  var oMemTable = $('#TablaNavegacion').dataTable({
        "bProcessing": false,
        "bServerSide": false,
        //"sAjaxSource": "controllers/asignaturas.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});

