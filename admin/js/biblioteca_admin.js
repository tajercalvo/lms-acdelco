$(document).ready(function() {
    init();
});

// Función para validar si el enlace es de YouTube
function esLinkYoutube(url) {
  const regexYoutube = /^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/;
  return regexYoutube.test(url);
}

function init() {

  $.ajax({
    url: 'controllers/biblioteca_admin.php',
    type: 'POST',
    dataType: 'JSON',
    data: {opcn: 'init'},
  })
  .done(function( data ) {
    var tabla = `<thead>
                    <tr>
                      <th>
                        <div style="border-right: 1px solid #d65050">IMAGEN</div>
                      </th>
                      <th>
                        <div style="border-right: 1px solid #d65050">
                        Descripción
                        </div>
                      </th>
                      <th>
                        <div style="border-right: 1px solid #d65050">
                        Creador y fecha de creación
                        </div>
                      </th>
                      <th>
                        <div>
                        Opciones
                        </div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>`;
    data.forEach( function(e, i) {

      // Aquí se construye el link según sea un archivo local o un enlace de YouTube
      let linkHTML = '';
      if (esLinkYoutube(e.file)) {
        linkHTML = `<a href="${e.file}" target="_blank">
                      <img src="../assets/Biblioteca/m/${e.image}" style="width: 50%;">
                    </a>`;
      } else if (e.file && e.file !== "") {
        linkHTML = `<a href="../assets/Biblioteca/${e.file}" target="_blank">
                      <img src="../assets/Biblioteca/m/${e.image}" style="width: 50%;">
                    </a>`;
      } else {
        linkHTML = `<span>No se encontró el archivo o el enlace no es válido</span>`;
      }
        tabla += `<tr>
                  <td align="center">${linkHTML}</td>
                  <td align="center" > <span class="text-uppercase" style="font-size: 12px;"></strong>`+e.name+`</strong></span><br><span style="font-size: 11px;">`+e.description+`</span><br>`+e.estado+`</td>
                  <td align="center" >Por: `+e.first_name+` `+e.last_name+`el `+e.date_creation+`</td>
                  <td align="center" > <a style="color: red; text-decoration: underline;"><p class="btn"  style="font-size: 12px;" onclick="eliminar(`+e.library_id+`, '`+e.image+`', '`+e.file+`')";>ELIMINAR</p></a> <a style="color: black; text-decoration: underline; font-size: 12px;" href="#Modalprogress_bar" data-toggle="modal"> <p data-library_id="`+e.library_id+`" class="editar">EDITAR</p></a></td>
                </tr>`
    });
    tabla += '</tbody>';
    $('#tabla').html(tabla);
    $('#cant_registros').html(data.length);

  })
  .fail(function() {
    console.log("error");
  });
}

opcn = '';
$('#agregar').click(function(event) {
  opcn = 'crear';
  $('form').trigger("reset");
  $("#charge_id").val([]).trigger('change');
  $("#dealer_id").val([]).trigger('change');
  $("#rol_id").val([]).trigger('change');

});

library_id = '';
$('body').on('click', '.editar', function(event) {
  event.preventDefault();
  opcn = 'editar';
  library_id =$(this).data('library_id')
  console.log(library_id);

  $.ajax({
    url: 'controllers/biblioteca_admin.php',
    type: 'POST',
    dataType: 'JSON',
    data: {opcn: 'consultar_libreria', library_id:library_id},
  })
  .done(function(  data ) {
    cargos = [];
    data.cargos.forEach( function(e, i) {
      cargos.push(e.charge_id);
    });
    $("#charge_id").val(cargos).trigger('change');

    dealer = [];
    data.concesionarios.forEach( function(e, i) {
      dealer.push(e.dealer_id);
    });
    $("#dealer_id").val(dealer).trigger('change');

    roles = [];
    data.roles.forEach( function(e, i) {
      roles.push(e.rol_id);
    });
    $("#rol_id").val(roles).trigger('change');

    $('#name').val( data.libreria.name );
    $('#description').val( data.libreria.description );

    $("#segment_id").val(data.libreria.specialty_id).trigger('change');
    $("#status_id").val(data.libreria.status_id).trigger('change');

    $('#status').html('')
    //oculta la modal y la notificacion despues de 3 segundos Freddy mendoza/05/07/2020
    //$('#Modalprogress_bar').modal('hide')
    setTimeout(function(){ $('.notyfy_message').click(); }, 3000);

  })
  .fail(function() {
    console.log("error");
  });

});
function eliminar( library_id, image, file ){
  var r = confirm("¿Está seguro de eliminar el resultado de esta evaluación?");
  if (r) {
      $.ajax({
         url: 'controllers/biblioteca_admin.php',
      type: 'POST',
      dataType: 'JSON',
      data: {opcn: 'eliminar', library_id:library_id, image: image, file: file},
      })
      .done(function( data ) {
        notyfy({
            text: data.msj,
            type: data.type
        });
        init();
      })
      .fail(function() {
        console.log("error");
      });

  }else{
    return false;
  }
};

// Selec que estan dentro de form para cargar o actualizar lso archivos
$(document).ready(function(){
    // Select Placeholders
    $("#segment_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });

    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#charge_id").select2({
        placeholder: "Seleccione la (las) trayectorias",
        allowClear: true
    });

    $("#dealer_id").select2({
        placeholder: "Seleccione la (las) empresas",
        allowClear: true
    });
    $("#rol_id").select2({
        placeholder: "Seleccione el perfil",
        allowClear: true
    });
});
//Fin select


// Inicio Funcion para seleaccionar un archivo o una URL
url_archivo = false;
$('#checkbox').click(function  input (){
  if($("#checkbox").is(':checked')) {
    url_archivo = true;
    $('#archivo_biblioteca').css('display', 'none');
    $('#video_youtube').css('display', 'block');
  } else {

    url_archivo = false;
    $('#archivo_biblioteca').css('display', 'block');
    $('#video_youtube').css('display', 'none');
  }
});
// Fin Funcion para seleaccionar un archivo o una URL

//Inicio para subir o crear los archivos
function _(el){
  return document.getElementById(el);
}

function uploadFile(){
    newFileName = $('#url').val();

    if( $('#name').val() == '' ){
      notyfy({
          text: 'Ingresa un nombre',
          type: 'warning'
      });
      return;
    }

    if( $('#description').val() == '' ){
      notyfy({
          text: 'Ingresa una descripción',
          type: 'warning'
      });
      return;
    }


    if( opcn == 'editar' ){
        if( $('#image').val() == '' ){
              var confirma = confirm("No has seleccionado una imagen, no se modificará la actual, desea continuar?");
              if( confirma ){
              }else{
                return;
              }
            }

        if( $('#file1').val() == '' ){
              var confirma = confirm("No has seleccionado un archivo, no se modificará el actual, desea continuar?");
              if( confirma ){
              }else{
                return;
              }
            }

    }else{

          if( $('#image').val() == '' ){
              notyfy({
                  text: 'Selecciona una imagen para tu archivo',
                  type: 'warning'
              });
              return;
            }

        if( url_archivo ) {
              if( $('#url').length == 0 ){
                notyfy({
                    text: 'Escribe una url para tu archivo',
                    type: 'warning'
                });
                return;
              }
        }else{
            if( $('#file1').val() == '' ){
              notyfy({
                  text: 'Selecciona un archivo',
                  type: 'warning'
              });
              return;
            }
        }
    }

  var image = _("image").files[0];

            // notyfy({
            //     text: 'El archivo excede el tamaño máximo permitido (1 Mb) o no es jpg único formato válido',
            //     type: 'error' // alert|error|success|information|warning|primary|confirm
            // });

          if( !url_archivo ){
          //console.log('sisisi')
                //Inicio renombrando el archivo para evitar puntos ( . ) adicionales al inertar en la base de datos
              //   var fileName = document.getElementById('file1').files[0].name;
              //   arrayFileName = fileName.split(".");
              //   newFileName ="";

              //   for (var i = 0; i < arrayFileName.length; i++) {
              //     if (i == (arrayFileName.length-1)) {
              //       newFileName +='.';
              //     }
              //      newFileName += arrayFileName[i];
              //   }

              // }else{
                 newFileName = $('#url').val();
              }

      var formdata = new FormData();
      if(!url_archivo){
        formdata.append("file1", _("file1").files[0]);
      }
      formdata.append("image", image);
      formdata.append('name',$('#name').val());
      formdata.append('name_file',newFileName);
      formdata.append('description',$('#description').val());
      formdata.append('segment_id',$('#segment_id').val());
      formdata.append('status_id',$('select[name=status_id]').val());
      formdata.append('opcn', opcn);
      formdata.append('charge_id', $('#charge_id').val());
      formdata.append('dealer_id', $('#dealer_id').val());
      formdata.append('rol_id', $('#rol_id').val());
      formdata.append('url', $('#url').val());
      formdata.append('library_id', library_id);
      var ajax = new XMLHttpRequest();

      ajax.upload.addEventListener("progress", progressHandler, false);
      ajax.addEventListener("load", completeHandler, false);
      if(!url_archivo) {
        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.addEventListener("abort", abortHandler, false);
      }

      ajax.open("POST", "controllers/biblioteca_admin.php");
      ajax.send(formdata);
}

function progressHandler(event){
  var percent = (event.loaded / event.total) * 100;
  _("progressBar").style.width = Math.round(percent)+'%';
  _("status").innerHTML = Math.round(percent)+'% Cargando... por favor espere';
}
function completeHandler(event){
  var datos = JSON.parse(event.target.responseText);
  console.log(datos)
  _("status").innerHTML = datos.msj+'<br>'+datos.cargos+'<br>'+datos.concesionarios+'<br>'+datos.roles

  setTimeout(_("progressBar").style.width = "0", 3000);

   notyfy({
                    text: 'Archivo cargado correctamente ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
  init();
  //oculta la modal y la notificacion despues de 3 segundos Freddy mendoza/05/07/2020
  $('#Modalprogress_bar').modal('hide')
  setTimeout(function(){ $('.notyfy_message').click(); }, 3000);
}
function errorHandler(event){
  _("status").innerHTML = "Upload Failed";
}
function abortHandler(event){
  _("status").innerHTML = "Upload Aborted";
}
//Fin funcion para cargar el archivo