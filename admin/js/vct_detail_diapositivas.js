$(document).ready(function(){
            //alert();
            instructor = 'no';
            course_id = ''; // iniciamos la variable instructor en no, para que la busqueda por ajax de la diapositiva actual, no se inicie hasta que instructor sea igual a si;
            get_diapositivas(); // funcion que le mostrara al instructor el listado de las diapositivas o imagenes en el scroll horizontal, y tambien valida la variable instrutor
            setInterval('cambiardiapositiva_actual()',5000); // funcion que le cambiara al estudiante la diapotiva qo archivo que actualmente está viendo

            //funcion para el chat
            clic = false;
            setInterval("cargarComentarios2()",6000);
            // fin

            //Funcion que calcula tiempo
            setInterval("mostrarHora('mostrarTiempo')",1000);

            //Escoger un solo checkbox
            $('input[type=checkbox]').live('click', function(){
                var parent = $(this).parent().attr('id');
                $('#'+parent+' input[type=checkbox]').removeAttr('checked');
                $(this).attr('checked', 'checked');
            });

            $('.checkbox').live('click', function(){
                var parent = $(this).parent().attr('id');
                $('#'+parent+' input[type=checkbox]').removeAttr('checked');
                $(this).attr('checked', 'checked');
            });
        });

        //Cargar mensajes cada cierto tiempo
function cargarComentarios2() {
    //console.log(clic);
    //Validar si se dio clic en l pestaña comunicaciones
    if (clic) {
      var vct = $("#idvct").val();
      //console.log('consultar id '+ultimo_mendaje_id_BD);
  var data = new FormData();
    data.append('idvcts',vct);
    data.append('lastId',ultimo_mendaje_id_BD);
    data.append('opcn','comments2');
    var url = "vct_detail_ajax2.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        async: true,
        cache:false,
        success: function(data) {
         //console.log('si ajax');
          //Capturamos el ultimo id del mensaje que biene de la base de datos para compararlo con el ultimo que se consulto, si son iguales no se hace nada
          var  cantidad = $(data).length - 1;

          if (cantidad >= 0) {
                          ultimo_mendaje_id_BD2 = data[cantidad]['comments_id'];
                          if (ultimo_mendaje_id_BD == ultimo_mendaje_id_BD2 ) { //console.log('NO hay mensaje snuevos');
                            return false;
                          }
                          ultimo_mendaje_id_BD = ultimo_mendaje_id_BD2;
                          for(var i in  data) {
                                                var mensaje = '<li class="row row-merge border-none" id="'+data[i]['comments_id']+'">';
                                              mensaje = mensaje +'<div class="col-md-12">';
                                                mensaje = mensaje +'<div class="innerAll padding-none">';
                                                 mensaje = mensaje +' <div class="media">';
                                                   mensaje = mensaje +' <a href="#" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/'+data[i]['image']+'" alt="Image" style="width: 25px;"/></a>';
                                                    mensaje = mensaje +'<div class="media-body">';
                                                      mensaje = mensaje +'<div><small class="pull-right" style ="color:white;"><i class="fa fa-fw icon-envelope-fill-1 text-primary"></i>'+data[i]['date_comment']+'<em></em></small></div>';
                                                      mensaje = mensaje +'<strong class="text-primary">'+data[i]['first_name']+' '+data[i]['last_name']+'</strong><br/>';
                                                      mensaje = mensaje +'<small style ="color:white;"> '+data[i]['comment_text']+'</small>';
                                                    mensaje = mensaje +'</div>';
                                                  mensaje = mensaje +'</div>';
                                                mensaje = mensaje +'</div>';
                                              mensaje = mensaje +'</div>';
                                            mensaje = mensaje +'</li>';

                                            $("#cajaComentarios").append(mensaje);
                                            var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
                                            $("#cajaComentarios").scrollTop(alturaScroll);
                                            if ($("#cajaComentarios li").length > 20) {
                                                $("#cajaComentarios li").first().remove();// elminamos el primer mensaje de la lista para solo mantener siempre la misma cantidad y no poner lento el chat
                                              }
                                        }

                      }else{
                        //console.log('Sin mensajes nuevos');
                        }// Fin cantidad mayor a 0
        }// Fin function data
    });
  }// Fin If clic, comunicaciones
  else{return false;}


}

// Cargar los mensajes al hacer clic en la pestaña comunicaciones o cuando se le de clic en el boton actualizar chat
$("#list_chat").click(function(){
  Cargar_chat();
});



//Carga por primera vez o actualiza la ventana de chat
function Cargar_chat() {

  $('#cajaComentarios').html('');
  $('#cargando_mensajes').html('<span class="label label-success">Consultando mensajes, por favor espere... <img style="width: 15px; " src="../assets/Radio.gif"></span>');
  var vct = $("#idvct").val();

  var data = new FormData();
    data.append('idvcts',vct);
    data.append('opcn','comments');
    var url = "vct_detail_ajax2.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        async: true,
        cache:false,
        success: function(data) {
                //console.log($(data.mensajes).length);
                nombre = data.datos_usuario['first_name']+' '+data.datos_usuario['last_name'];
                id_usuario = data.datos_usuario['user_id']
          //Capturo la ultima posicion del array
          var  cantidad = $(data.mensajes).length;
          if(cantidad>=1){cantidad = $(data.mensajes).length - 1}
            else{
                $('#cargando_mensajes').html('<div class="btn-group btn-group-xs pull-right"><button class="btn btn-primary" onclick="Cargar_chat()"><i class="fa fa-refresh " ></i> Actualizar chat</button></div>');
                //console.log('chat sin mensajes');
                return false
                };
          //console.log(cantidad);

          //Se captura el ultimo id de mensaje que biene de la base d edatos, para despues buscar los siguientes
          ultimo_mendaje_id_BD = data.mensajes[cantidad]['comments_id'];
          clic = true;

          //console.log('Ultimo id mensaje primera carga '+ultimo_mendaje_id_BD);
          //inicio mostrar los chats
          for(var i in  data.mensajes) {
            //Validar si el id de la base que biene de la de datos es igual al que tiene la seccion iniciada, para cambiar la clase del estado del mensaje, con el icono de un chulito o una carta
            if (data.mensajes[i]['user_id'] == id_usuario ) { var estadomensaje = '<i class="fa fa-fw icon-checkmark-thick text-primary"></i>';}else{ estadomensaje = '<i class="fa fa-fw icon-envelope-fill-1 text-primary"></i>'; }

                                var mensaje = '<li class="row row-merge border-none" id="'+data.mensajes[i]['comments_id']+'">';
                              mensaje = mensaje +'<div class="col-md-12">';
                                mensaje = mensaje +'<div class="innerAll padding-none">';
                                 mensaje = mensaje +' <div class="media">';
                                   mensaje = mensaje +' <a href="#" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/'+data.mensajes[i]['image']+'" style="width: 25px;"/></a>';
                                    mensaje = mensaje +'<div class="media-body">';
                                      mensaje = mensaje +'<div><small class="pull-right" style ="color:white;">'+estadomensaje+data.mensajes[i]['date_comment']+'<em></em></small></div>';
                                      mensaje = mensaje +'<strong class="text-primary">'+data.mensajes[i]['first_name']+' '+data.mensajes[i]['last_name']+'</strong><br/>';
                                      mensaje = mensaje +'<small style ="color:white;"> '+data.mensajes[i]['comment_text']+'</small>';
                                    mensaje = mensaje +'</div>';
                                  mensaje = mensaje +'</div>';
                                mensaje = mensaje +'</div>';
                              mensaje = mensaje +'</div>';
                            mensaje = mensaje +'</li>';

                            $("#cajaComentarios").append(mensaje);
                        }// Fin for

                        var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
                        $("#cajaComentarios").scrollTop(alturaScroll);
                        $('#cargando_mensajes').html('<div class="btn-group btn-group-xs pull-right"><button class="btn btn-primary" onclick="Cargar_chat()"><i class="fa fa-refresh " ></i> Actualizar chat</button></div>');

            //}
        }
    });
}

// al presionar enter dentro de la caja de texto del chat
$("#texto_comentario").keypress(function(e) {
       if(e.which == 13) {
          enviarMensaje();
          return false;
       }
    });

    //funcion qu eenvia los mensajes
function enviarMensaje(){

      //var chat = $('#texto_comentario').val().length;
      var chat = $("#texto_comentario").val();
      if (chat!='') {
      var usuarioID = id_usuario;
      var datos = $("#comentario").serialize();
      //nombre = $("#first_name").text()+' '+$("#last_name").text();

      var id = $("#cajaComentarios li").last().attr("id");//id del ultimo comentario en el chat
      var f = new Date();
      id_msj_usuario = usuarioID +""+f.getTime();
      datos = datos + '&id_msj_usuario='+id_msj_usuario;

    //console.log("datos = "+datos);

       var mensaje = '<li class="row row-merge border-none" id="'+id_msj_usuario+'">';
          mensaje = mensaje +'<div class="col-md-12">';
            mensaje = mensaje +'<div class="innerAll padding-none">';
             mensaje = mensaje +' <div class="media">';
               mensaje = mensaje +' <!--<a href="#" class="thumb pull-left visible-md visible-lg" target="_blank"><img src="../assets/images/usuarios/" alt="Image" style="width: 50px;"/></a>-->';
                mensaje = mensaje +'<div class="media-body">';
                  mensaje = mensaje +'<div id=estado'+id_msj_usuario+'><small class="pull-right" style ="color:gray;"><i class="fa fa-fw icon-refresh-location-fill text-primary"></i> Enviando<em></em></small></div>';
                  mensaje = mensaje +'<strong class="text-primary">'+nombre+'</strong><br/>';
                  mensaje = mensaje +'<small style ="color:white;"> '+chat+'</small>';
                mensaje = mensaje +'</div>';
              mensaje = mensaje +'</div>';
            mensaje = mensaje +'</div>';
          mensaje = mensaje +'</div>';
        mensaje = mensaje +'</li>';

        $("#cajaComentarios").append(mensaje);
        mensaje = '';
        //$("#cajaComentarios").animate({ scrollTop: $('#cajaComentarios')[0].scrollHeight}, 1000);
        var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
        $("#cajaComentarios").scrollTop(alturaScroll);
        $('#texto_comentario').val('');

          $.ajax({
            url     : "controllers/vct_detail.php",
            data    : datos,
            type    : "post",
            dataType: "json",
            async: true,
            success: function(data){
              //var res0 = data.resultado;
                  if (data.resultado == "SI") {

                      //console.log('mensaje enviado');
                      $('#estado'+data.id_comments_users).html('<small class="pull-right text-primary-light" style ="color:white;"><i class="fa fa-fw icon-checkmark-thick text-primary"></i> '+data.fecha+' <em></em></small>');
                      $('audio')[0].play();
                      //var res0 = data.resultado;
                      var alturaScroll = $("#cajaComentarios").prop("scrollHeight");
                      $("#cajaComentarios").scrollTop(alturaScroll);
                      if ($("#cajaComentarios li").length > 20) {
                        $("#cajaComentarios li").first().remove();// elminamos el primer mensaje de la lista para solo mantener siempre la misma cantidad y no poner lento el chat
                      }

                      //$( "#cajaComentarios li" ).first().removeClass( "row row-merge border-none" );
                      //$("#cajaComentarios").animate({ scrollTop: $('#cajaComentarios')[0].scrollHeight}, 1000);
                  }else{
                    $('#estado'+id_msj_usuario).html('<small class="pull-right text-primary-light"><i class="fa fa-fw icon-delete-symbol text-primary"></i> <em></em></small>');
                  }
            }
          })
          .fail(function() {
             $('#estado'+id_msj_usuario).html('<small class="pull-right text-primary-light"><i class="fa fa-fw icon-delete-symbol text-primary"></i> <em></em></small>');
      });
    }else{
      return false;
    }

}

        // Solo para el estudiante, funcion que cambia automaticamente la imagen central
        var id_diapsitiva_Actual = 0; // variable que llevara el id de la diapositiva que actualmente se está viendo, en la base de datos en la tabla ludus_diapositiva_siguiente se cinsulta el ultimo id insrtado
        function cambiardiapositiva_actual(){
            if(instructor == 'si'){ return false;}
            $('#row_lista_diapositivas').remove();
            var data = new FormData();
            data.append('opcn','get_diapositiva_actual');
            data.append('schedule_id', $.get("schedule"));
            $.ajax({
                url: "controllers/vct_detail2.php",
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                async: true,
                cache:false,
                success: function(data) {
                        if (data.length > 0 ) {
                            if(id_diapsitiva_Actual == data[0].course_file_id){
                                return false;
                            }else{
                                id_diapsitiva_Actual = data[0].course_file_id; // validamos si el id de la diapositiva actual es diferente a la que está actualmente en para no volver a cargar el contenido
                            }
                            var diapositiva_actual = ''; // iniciamos la construccion del div que contendra la diapositiva actual
                            //console.log(data);
                            if (data[0].archivo.indexOf(".mp4") > -1) { // si el archivo contiene el texto .mp4
                                diapositiva_actual  = '<video class="img-responsive" controls autoplay>';
                                diapositiva_actual += '<source src="../assets/fileVCT/VCT_'+course_id+'/'+data[0].archivo+'" type="video/mp4">';
                                diapositiva_actual += 'Your browser does not support the video tag.';
                                diapositiva_actual += '</video>';
                             }else if (data[0].archivo.indexOf(".pdf") > -1){
                                diapositiva_actual = '<embed src="../assets/fileVCT/VCT_'+course_id+'/'+data[0].archivo+'" style="height:600px; width:100%;">';
                            }else if (data[0].archivo.indexOf(".preg") > -1){
                              var pregunta_id = data[0].question_id
                              var name = data[0].name
                              diapositiva_actual = '<div class="row"><div class="col-md-2"></div><div class="col-md-8">';
                              diapositiva_actual += '<h4 style="text-align: center;" id="pregunta">'+name+'</h4><div class="widget"><div class="widget-head"><h4 class="heading">Escoge una respuesta</h4></div><div class="widget-body innerAll inner-2x">';
                              diapositiva_actual += '<form id="formulario_data" method="post" enctype="multipart/form-data" autocomplete="off">';
                              for (var i = 0; i < preguntas[pregunta_id].length; i++) {
                                diapositiva_actual += ' <div id="product1"><input type="checkbox" value="'+preguntas[pregunta_id][i].id+'" id="product-1-'+i+'" name="check">  '+preguntas[pregunta_id][i].answer+'<br><br> ';
                              }
                              diapositiva_actual += '<input type="submit" value="Enviar" class="btn-xs btn-primary"> </form>';
                              diapositiva_actual += '</div></div></div></div><div class="col-md-2"></div></div>';

                            }else{
                                  diapositiva_actual = '<img class="img-responsive" src="../assets/fileVCT/VCT_'+course_id+'/'+data[0].archivo+'" class="img-responsive" >';
                              }

                            //$("#diapositiva_actual").fadeOut();
                            $('#diapositiva_actual').css('visibility','hidden').hide().fadeOut('slow');
                            $('#diapositiva_actual').html(diapositiva_actual); // agregamos al div el contenido dependiendo del formato que venga del archivo
                            $('#diapositiva_actual').css('visibility','visible').hide().fadeIn('slow');
                            //$("#diapositiva_actual").fadeIn();
                        }
                } // end function data
            }).fail(function() {
               //console.log('falló la funcion cambiardiapositiva_actual()');
            });

        }

        //FIN funcones para el chat

        // funciones que calculan tiempo
            /*
            Funcion que evalua el tiempo transcurrido y pendiente y cambia el estilo de acuerdo al resultado
            @tiempo= es el tiempo que se esta tomando para un estado, 15 min es el readye para ejecutar los cambios
            @idDiv= es el id del DIV donde se esta mostrando el tiempo transcurrido
            @etapa= es el momento que determina el estado del boton de inscripcion
              1. Es el tiempo entre la hora actual y la hora programada de la capacitacion si aun no a empezado
              2. Es el tiempo despues que empezo la capacitacion
            */
            function calcularTiempo(tiempo,idDiv,etapa){
                      //console.log(tiempo+":"+etapa+"|");
                      if(etapa == 1 && tiempo > 900000){
                        $("#btn_inscription").hide();
                      }
                      if(etapa == 1 && tiempo >= 0 && tiempo <= 300000){
                        $("#"+idDiv).attr("style","color: orange;");
                        $("#btn_inscription").show();
                      }
                      if(etapa == 1 && tiempo >= 300773 && tiempo <= 900000){
                        $("#"+idDiv).attr("style","color: orange;");
                        $("#btn_inscription").hide();
                      }
                      if(etapa == 2 && tiempo >= 0 && tiempo <= 900000){
                        $("#"+idDiv).attr("style","color: green;");
                        $("#btn_inscription").show();

                      }
                      if(etapa == 2 && tiempo > 900000){
                        if($("#btn_inscription").attr("href") != "#"){
                          $("#btn_inscription").attr({"href":"#modalVCT","data-toggle":"modal"});
                          $("#btn_inscription").show();
                        }
                      }
                    }//fin funcion calcularTiempo

            /*
            Funcion que genera un temporizador que mostrarHora
              - tiempo faltante para empezar un curso y asi mismo inscribirseCurso
              - si el curso ya inicio, el tiempo que ha transcurrido
            */
            function mostrarHora(idSeleccion){
                      var actual = new Date();
                      var d = new Date($("#fecha_hora").val());
                      var msMinute = 1000 * 60;
                      var msHour = msMinute * 60;
                      var msDay = msHour * 24;
                      var actualMS = actual.getTime();
                      var dMS = d.getTime();
                      var tiempoFinal = 0;
                      if (actualMS < dMS) {
                        var diferencia = dMS - actualMS;
                        calcularTiempo(diferencia,"mostrarTiempo",1);
                        var dias = Math.floor(diferencia / msDay);
                        diferencia = diferencia - (dias * msDay);
                        var horas = Math.floor(diferencia / msHour);
                        diferencia = diferencia - (horas * msHour);
                        var minutos = Math.floor(diferencia / msMinute);
                        diferencia = diferencia - (minutos * msMinute);
                        var segundos = Math.floor(diferencia / 1000);
                        tiempoFinal = dias+" dias, "+horas+":"+(minutos < 10 ? "0" : "")+minutos+":"+(segundos < 10 ? "0" : "")+segundos;
                      }else{
                        var diferencia = actualMS - dMS;
                        calcularTiempo(diferencia,"mostrarTiempo",2);
                        var dias = Math.floor(diferencia / msDay);
                        diferencia = diferencia - (dias * msDay);
                        var horas = Math.floor(diferencia / msHour);
                        diferencia = diferencia - (horas * msHour);
                        var minutos = Math.floor(diferencia / msMinute);
                        diferencia = diferencia - (minutos * msMinute);
                        var segundos = Math.floor(diferencia / 1000);
                        tiempoFinal = dias+" dias, "+horas+":"+(minutos < 10 ? "0" : "")+minutos+":"+(segundos < 10 ? "0" : "")+segundos;
                      }
                      document.getElementById(idSeleccion).innerHTML = tiempoFinal;
                    }//fin funcion mostrarHora

             // Fin funciones que calculan tiempo

             //funcion  para buscar usuarios
             $("#buscarUsuario").keyup(function(){
              var nombre = $("#buscarUsuario").val();
              var idsearch = $("#idsearch").val();
              var schedule_id = $("#schedule_id").val();
              cargarDatosAjax('asistentes','search',idsearch,nombre,schedule_id);
            });

        //trae el listado de las diapositivas disponibles si este usuario es el instructor para este sección
        var preguntas = [];
        function get_diapositivas(){

            $('#lista_diapositivas').html('');
            var data = new FormData();
            data.append('opcn','get_diapositivas');
            data.append('schedule_id', $.get("schedule")); // $.get("schedule") aptura el schdeule_id de la URL
            $.ajax({
                url: "controllers/vct_detail2.php",
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                async: true,
                cache:false,
                success: function(data) {
                    console.log(data);
                    if(data.datos.course_id != ''){
                        course_id = data.datos.course_id;
                        instructor = data.datos.instructor; // Una vez validamos si este usuario es el instructor, cambiamos la variable instrutor que se encuentra inicializada en el document ready
                    }

                    if(data.datos.instructor == 'si'){

                        $('#row_lista_diapositivas').css("visibility","visible");
                        $('#opcn_diapositivas').css("visibility","visible");
                        $("#editar_lista_diapositivas").attr("href", "op_vct.php?opcn=editar&id="+course_id);
                    }

                    if (data.archivos.length > 0) {

                        var lista_diapositivas = '';
                        for (var i = 0; i < data.archivos.length; i++) {
                            var imagen = data.archivos[i]['image'];
                            var id = data.archivos[i]['coursefile_id'];
                            var archivo = data.archivos[i]['archivo'];
                            var name = data.archivos[i]['name'];
                            var ruta_imagen = '';
                            //var course_id = data.archivos[i]['course_id'];

                            if(imagen.indexOf("default") > -1){ // validamos el nombre de la imagen
                                ruta = '../assets/fileVCT/'; // si contiene la pabra default buscamos en esta ruta
                            }else{
                                ruta = '../assets/fileVCT/VCT_'+course_id+'/'; // si no buscamos en otra ruta
                            }

                            //creamos el array con las respuestas
                            if(data.archivos[i]['archivo'].indexOf(".preg") > -1){
                                console.log('pregunta')
                                console.log(data.archivos[i]['question_id'])
                                preguntas[data.archivos[i]['question_id']] = data.archivos[i]['respuestas'];
                                //preguntas.push(data.datos[i]['respuestas']);
                                lista_diapositivas +='<a class="diapositivas" href="#" data-image = "'+imagen+'" data-ruta = "'+ruta+'" data-archivo = "'+archivo+'" data-id = "'+id+'" data-ruta-archivo = "'+id+'" data-pregunta_id = "'+data.archivos[i]['question_id']+'" data-name = "'+data.archivos[i]['name']+'" ><img class="img-responsive" src="'+ruta+imagen+'" style="width: 200px;height: 113px;" >'+data.archivos[i].name+'</a>';
                                //contenido += '<tr data-id = "'+data.datos[i]['coursefile_id']+'" data-image = "'+data.datos[i]['image']+'" data-archivo = "'+data.datos[i]['archivo']+'" data-pregunta_id = "'+data.datos[i]['question_id']+'"  >';
                            }else{
                              lista_diapositivas +='<a class="diapositivas" href="#" data-image = "'+imagen+'" data-ruta = "'+ruta+'" data-archivo = "'+archivo+'" data-id = "'+id+'" data-ruta-archivo = "'+id+'"><img class="img-responsive" src="'+ruta+imagen+'" style="width: 200px;height: 113px;" >'+data.archivos[i].name+'</a>';
                            }


                            $('#lista_diapositivas').html(lista_diapositivas);
                        }

                    }else{
                        //$('#row_lista_diapositivas').html("");
                    }
                }
            }).fail(function() {
                //console.log('falló get_diapositivas');
            });
        } // End function get_diapositivas

        // solo para el instructor, cuando se da clic a la imagen en miniatura, cambia la imagen central y envia por ajax a la base de datos la diapositiva que esta actualmente
        $('body').on('click', '.diapositivas', function(e){
          console.log("evento click diapositiva", e);
            $( ".diapositivas" ).removeClass( "active" );
            $(this).addClass("active");
            var image = $(this).attr('data-image');
            var ruta = $(this).attr('data-ruta');
            var archivo = $(this).attr('data-archivo');
            var id = $(this).attr('data-id');
            var diapositiva_actual = '';
            console.log(archivo);
            if (archivo.indexOf(".mp4") > -1) {

                diapositiva_actual  = '<video class="img-responsive" controls>';
                diapositiva_actual += '<source src="../assets/fileVCT/VCT_'+course_id+'/'+archivo+'" type="video/mp4">';
                diapositiva_actual += 'Your browser does not support the video tag.';
                diapositiva_actual += '</video>';
            }else if (archivo.indexOf(".pdf") > -1){
                //diapositiva_actual = '<embed src="../assets/fileVCT/VCT_'+course_id+'/'+archivo+'#toolbar=0"  style="height:600px; width:100%;">';
                diapositiva_actual = '<embed src="../assets/fileVCT/VCT_'+course_id+'/'+archivo+'"  style="height:600px; width:100%;">';
            }else if (archivo.indexOf(".preg") > -1){
                var pregunta_id = $(this).attr('data-pregunta_id');
                var name = $(this).attr('data-name');
                diapositiva_actual = '<div class="row"><div class="col-md-2"></div><div class="col-md-8">';
                diapositiva_actual += '<h4 style="text-align: center;" id="pregunta">'+name+'</h4><div class="widget"><div class="widget-head"><h4 class="heading">Escoge una respuesta</h4></div><div class="widget-body innerAll inner-2x">';
                diapositiva_actual += '<form id="formulario_data" method="post" enctype="multipart/form-data" autocomplete="off">';
                for (var i = 0; i < preguntas[pregunta_id].length; i++) {
                  diapositiva_actual += ' <div id="product1"><input type="checkbox" value="'+preguntas[pregunta_id][i].id+'" id="product-1-'+i+'" name="check">  '+preguntas[pregunta_id][i].answer+'<br><br> ';
                }
                diapositiva_actual += '<input type="submit" value="Enviar" class="btn-xs btn-primary"> </form>';
                diapositiva_actual += '</div></div></div></div><div class="col-md-2"></div></div>';

              }else{
                    diapositiva_actual += '<img class="img-responsive" src="'+ruta+image+'" class="img-responsive" >';
              }

                  $('#diapositiva_actual').css('visibility','hidden').hide().fadeOut('slow');
                  $('#diapositiva_actual').html(diapositiva_actual); // agregamos al div el contenido dependiendo del formato que venga del archivo
                  $('#diapositiva_actual').css('visibility','visible').hide().fadeIn('slow');

            var data = new FormData();
            data.append('opcn','set_diapositiva_actual');
            data.append('schedule_id', $.get("schedule"));
            data.append('archivo', archivo);
            data.append('id', id);
            $.ajax({
                url: "controllers/vct_detail2.php",
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                // dataType: "text",
                async: true,
                cache:false,
                success: function(data) {

                }
            }).fail(function() {
                console.log('falló petición');
            });

            return false;
        } ); // end click class diapositivas

    $('body').on('submit', '#formulario_data', function(e){
          e.preventDefault();
          //console.log('si');
          var data = new FormData();
          var datos_form = $(this).serializeArray();
          //console.log(datos_form);
          data.append('opcn','responder_pregunta');
          data.append(datos_form[0].name,datos_form[0].value);
          data.append('schedule_id', $.get("schedule"));
          //console.log(data);
        $.ajax({
            url:'controllers/vct_detail2.php',
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            cache:false,
            success: function(data) {
              console.log(data);
              if(data.error){
                notyfy({
                    text: data.mensaje,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
              }else{
                notyfy({
                    text: data.mensaje,
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
              }
            }// Fin function data
        });
    });

        $('#cargar_lista_diapostivas').click(function () {
            get_diapositivas();
            return false;
        });

        //Funcion para capturar las variables de la URL, se utiliza así var id_url = $.get("id");
        (function($) {
                $.get = function(key)   {
                    key = key.replace(/[\[]/, '\\[');
                    key = key.replace(/[\]]/, '\\]');
                    var pattern = "[\\?&]" + key + "=([^&#]*)";
                    var regex = new RegExp(pattern);
                    var url = unescape(window.location.href);
                    var results = regex.exec(url);
                    if (results === null) {
                        return null;
                    } else {
                        return results[1];
                    }
                }
            })(jQuery);
