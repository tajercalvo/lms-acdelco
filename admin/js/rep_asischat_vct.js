
$(document).ready(function(){
        consultar_broad();
         

});

        $("#schedule_id").change(function(){
            $("#chat").css("display","none");
        });

        $("#descargar").click(function(event) {
            $("#datos_tabla").val( $("<div>").append( $("#asistentes2").eq(0).clone()).html());//Clona la tabla para envialo a Fichero.excel para descargar
            $("#FormularioExportacion").submit(); 
        });
   
  
//consulta el nombre y id del broadcast
function consultar_broad(){

var data = {opcn:'consultar_Broad'};

         $.ajax({
             url: 'controllers/rep_asischat_vct.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             console.log(data);
            var datos = "";
            for (var i = 0; i < data.length; i++) {
              datos += '<option value="'+data[i].schedule_id+'">'+data[i].course+' - '+data[i].schedule_id+' </option>';
             
             }
                $('#schedule_id').html(datos)
               
                $("#schedule_id").select2({
                    placeholder: "Seleccione una sesión",
                    allowClear: true
                 });
         })
         .fail(function() {
             console.log("error");
         })
}
schedule_id = '';
//-----------------------Imprime los datos en la tabla----------------------//
$('#frm').submit(function(e) {
    e.preventDefault();
     schedule_id = $('#schedule_id').val()

      //mensaje_chat(schedule_id)
    $('#consultando').html('<img style="width: 15px; " src="../assets/loading.gif">');

    var data = {opcn:'consultar_asistantes', schedule_id: schedule_id};
   var contador = 0

   var tabla = $('#asistentes').dataTable({
     "destroy": true,
     "ajax":{

        url: 'controllers/rep_asischat_vct.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn:'consultar_asistantes', schedule_id: schedule_id},
        //dataSrc: ''
       },
         
      columns: [

      {data: 'identification',title: "IDENTIFICACIÓN"},
      {data: 'apellidos',title: "APELLIDOS"},
      {data: 'nombres',title: "NOMBRES"},
      {data: 'concesionario',title: "EMPRESA"},
      {data: 'sede',title: "SEDE"},
      {data: 'trayectorias',title: "TRAYECTORIAS"},
      {data: 'perfil',title: "PERFILES",

            "render": function ( data, type, row) {
                //console.log(row.xnum_part)
                $('#num_asistentes').html(row.xnum_part+' Participantes');
                return row.perfil
           
             },
      },
      
      ],
      "language": {
      "sProcessing": "Procesando...",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningun dato disponible en esta tabla",
      "oPaginate": {
      "sFirst": "Primero",
      "sLast": "0‰3ltimo",
      "sNext": "Siguiente",
      "sPrevious": "Anterior"
      },
      "oAria": {
      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
      }
      ,
      dom: 'lBfrtip',
      pageLength: 15,
      buttons: [
      {extend : 'excelHtml5',
      text: '<i class="las la-file-excel">EXCEL</i>',
      filename: 'clientes_autotrain',
      },
      {extend : 'pdfHtml5',
      text: '<i class="las la-file-pdf">PDF</i>',
      //orientation: 'landscape',
      filename: 'clientes_autotrain',
      exportOptions: {
      // columns: [0,1,2,3,4,5,6,7,8],
      }
      },
      {extend: 'print',text: '<i class="las la-print">IMPRIMIR</i>', filename: 'clientes_autotrain'}
      ]
        
    });
    // var xnum = '';
    //          xnum = data.length;
             $('#consultando').html('');
             $("#chat").css("display","block");
             // $('#num_asistentes').html(xnum+' Participantes');
            mensaje_chat()
});

function mensaje_chat(){
  
    // var xnumchat = ''
    //  xnumchat = data.length
 var tabla = $('#tabl_chat').dataTable({
     "destroy": true,
     "ajax":{

        url: 'controllers/rep_asischat_vct.php',
        type: 'POST',
        dataType: 'JSON',
        data: {opcn:'mensaje_chat', schedule_id: schedule_id},
        dataSrc: ''
       },
         
      columns: [
      //{data: 'num'},
      {data: 'Comentario',title: "Comentario"},
      {data: 'Usuario',title: "Usuario"},
      {data: 'Empresa',title: "Empresa"},
      {data: 'date_creation',title: "date_creation"},
      {data: null,title: "conferencia",
      "render": function ( data, type, row) {
                            console.log(row.cantidad)
                            xnumchat = row.cantidad
                            $('#num_mensajes').html('Se han registrado: ['+xnumchat+'] Mensajes');
                            return row.Conferencia
           
                        },
           }
      ],
      "language": {
      "sProcessing": "Procesando...",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningun dato disponible en esta tabla",
      "oPaginate": {
      "sFirst": "Primero",
      "sLast": "0‰3ltimo",
      "sNext": "Siguiente",
      "sPrevious": "Anterior"
      },
      "oAria": {
      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
      }
      ,
      dom: 'lBfrtip',
      pageLength: 15,
      buttons: [
      {extend : 'excelHtml5',
      text: '<i class="las la-file-excel">EXCEL</i>',
      filename: 'clientes_autotrain',
      },
      {extend : 'pdfHtml5',
      text: '<i class="las la-file-pdf">PDF</i>',
      //orientation: 'landscape',
      filename: 'clientes_autotrain',
      exportOptions: {
      // columns: [0,1,2,3,4,5,6,7,8],
      }
      },
      {extend: 'print',text: '<i class="las la-print">IMPRIMIR</i>', filename: 'clientes_autotrain'}
      ]
        
    });
      //$('#num_mensajes').html('Se han registrado: ['+xnumchat+'] Mensajes');

}
  


