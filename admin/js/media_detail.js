$( "#form_CrearGaleria" ).submit(function( event ) {
    if(valida()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

function DatosVideo(var_src, var_title){
    var TiempoVideo = setTimeout(function(){ Carga_DatosVideo(var_src, var_title); }, 2000);
}

function Carga_DatosVideo(var_src, var_title){
    $("#TituloVideo").html(var_title);
    $("#cartoonVideo").attr('src', var_src);
    var video = document.getElementById("cartoonVideo");
    video.play();
    console.log(var_src);
}

$(document).ready(function(){
    if ($('#cartoonVideo').length){
        /* Get iframe src attribute value i.e. YouTube video url
        and store it in a variable */
        var url = $("#cartoonVideo").attr('src');

        /* Assign empty url value to the iframe src attribute when
        modal hide, which stop the video playing */
        $("#myModal").on('hide.bs.modal', function(){
            $("#cartoonVideo").attr('src', '');
        });

        /* Assign the initially stored url back to the iframe src
        attribute when modal is displayed again */
        $("#myModal").on('show.bs.modal', function(){
            $("#cartoonVideo").attr('src', url);
        });
    }
});

$(document).ready(function(){
    // Select Placeholders
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});

$(document).ready(function() {
    $('a[href][title]').qtip({
        content: {
            text: false
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        },
        position: {
            target: 'mouse'
        }
    });
});

function valida(){
    var valid = 0;
    $('#media_detail').css( "border-color", "#efefef" );
    $('#description').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#media_detail').val() == ""){
        $('#media_detail').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description').val() == ""){
        $('#description').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new').val()==""){
        valid=1;
        notyfy({
                text: 'Debe seleccionar una imagen',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
    }else{
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function CargaDatosEditar(media_id,image,media,description){
    console.log('dio click');
    $('#media_id').val(media_id);
    $('#imgant').val(image);
    $('#media_ed').val(media);
    $('#description_ed').val(description);
}

function CargaDatosEditar_Img(media_id,image,media,description){
    console.log('dio click');
    $('#media_detail_id').val(media_id);
    $('#imgant').val(image);
    $('#media_detail_ed').val(media);
    $('#description_ed').val(description);
}

function CargaDatosEditar_Vid(media_id,image,media,description,video){
    console.log('dio click');
    $('#media_detail_id').val(media_id);
    $('#imgant').val(image);
    $('#media_detail_ed').val(media);
    $('#description_ed').val(description);
    $('#video_ed').val(video);
}

$( "#form_EditarGaleria" ).submit(function( event ) {
    if(validaEdt()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

function validaEdt(){
    var valid = 0;
    $('#media_ed').css( "border-color", "#efefef" );
    $('#description_ed').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#media_ed').val() == ""){
        $('#media_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description_ed').val() == ""){
        $('#description_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#image_new_ed').val()!=""){
        var inputFileImage = document.getElementById("image_new_ed");
        var file = inputFileImage.files[0];
        if(file.type != "image/jpeg"){
            valid = 1;
            notyfy({
                text: 'La imagen seleccionada no es jpg, único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
        if(file.size>=1500001){
            valid = 1;
            notyfy({
                text: 'La imagen excede el peso máximo permitido (1 Mb) o no es jpg único formato válido',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function LikeGalery(media_detail_id){
    var data = new FormData();
    data.append('media_detail_id',media_detail_id);
    data.append('opcn','LikeGalery');
    var url = "controllers/multimedias_detail.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                notyfy({
                    text: 'Muchas gracias, hemos recibido tu like! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes'+media_detail_id).text());
                $('#CantLikes'+media_detail_id).text(Cantlikes+1);
            }else{
                notyfy({
                    text: 'Ya votaste por esta multimedia anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}


//
function ViewGalery(media_detail_id){
    //alert("otro");
    var data = new FormData();
    data.append('media_detail_id',media_detail_id);
    data.append('opcn','ViewGalery');
    var url = "controllers/multimedias_detail.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                var Cantlikes = parseInt($('#CantViews'+media_detail_id).text());
                $('#CantViews'+media_detail_id).text(Cantlikes+1);
            }
        }
    });
}
