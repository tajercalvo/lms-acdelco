/*Comentarios para la cortinilla
    var Timer_ImgLanza = setTimeout('Init_Lanzamiento()', 6500);
    var ImgLanza = ['Cortinilla_arte-01.png','Cortinilla_arte-02.png','Cortinilla_arte-03.png'];
    function Init_Lanzamiento(){
        var ImgLanzar = $('#ValImgSel_Lanzamiento').val();
        if(ImgLanzar==0){
            $('#ValImgSel_Lanzamiento').val(1);
            $("#imgModal_Lanzamiento").attr("src","../assets/gallery/modal/"+ImgLanza[1]);
        }else if(ImgLanzar==1){
            $('#ValImgSel_Lanzamiento').val(2);
            $("#imgModal_Lanzamiento").attr("src","../assets/gallery/modal/"+ImgLanza[2]);
        }else{
            $('#ValImgSel_Lanzamiento').val(0);
            $("#imgModal_Lanzamiento").attr("src","../assets/gallery/modal/"+ImgLanza[0]);
        }
        if( $("#modal-lanzamiento").attr('class') == "modal fade in" ){
            Timer_ImgLanza = setTimeout('Init_Lanzamiento()', 6500);
            console.log('Lanzando imagen cortina');
        }
    }
Comentarios para la cortinilla*/
var VerMasVarControl = 0;
$(document).ready(function () {
  if ("#modal-streaming".length > 0) {
    $("#modal-streaming").modal("show");
  }
  $("#modal-lanzamiento").modal("show");
  $("#VerMasIndex").hide();
  $("#modal-lanzamiento").modal("show");
  if ($("#calendar_actual").length) {
    $("#calendar_actual").fullCalendar({
      eventClick: function (params) {
        //validarInscripcion( params );
        return validarInscripcion(params);
      },
      lang: "es",
      header: {
        left: "prev,next ",
        center: "title",
        right: "",
      },
      editable: false,
      droppable: false,
      events: rootPath + "/admin/ajax/calendar_asignaciones.php?opcn=usr",
      eventRender: function (event, element) {
        element.qtip({
          //content: event.description,
          content: {
            text: event.description,
            title: {
              text: event.title,
            },
          },
          style: { classes: "qtip-rounded qtip-" + event.color },
          hide: {
            fixed: true, // Helps to prevent the tooltip from hiding ocassionally when tracking!
          },
          position: {
            my: "bottom center",
            at: "top center",
          },
        });
      },
    });
  }
});

function validarInscripcion(params) {
  console.log(params);
  var time_var = new Date();
  var tiempo = params.start - time_var;
  var res_tiempo = params.end - time_var;
  // console.log( params );
  var ir = false;
  // console.log( "comparacion ",time_var > params.end, "time_var", time_var, "params.end", params.end );
  if (params.tipo == "circular" || params.url.include("actividades.php")) {
    ir = true;
  } else {
    // if( time_var > params.end ){
    //     notyfy({
    //         text: 'La capacitación ha finalizado, por favor valida las próximas capacitaciones con tu líder GMAcademy. ',
    //         type: 'warning' // alert|error|success|information|warning|primary|confirm
    //     });
    // }else
    if (tiempo < 300000) {
      var cadena = params.url; // vct_detail.php?id=772&schedule=3800
      var aux = cadena.split("?"); //['vct_detail.php','id=772&schedule=3800']
      var pares = aux[1].split("&"); //['id=772','schedule=3800']
      var data = new FormData();
      var url = "controllers/vct_detail.php";
      for (var i = 0; i < pares.length; i++) {
        var parametro = pares[i].split("=");
        data.append(parametro[0], parametro[1]);
      }
      var tiempo_enviar = tiempo > 0 ? tiempo : tiempo * -1;
      data.append("tiempo", tiempo_enviar);
      data.append("opcn", "inscripcion");

      /*if( ir ){*/
      $.ajax({
        url: url,
        type: "post",
        contentType: false,
        data: data,
        processData: false,
        dataType: "json",
        cache: false,
        async: false,
      })
        .done(function (data) {
          if (data.resultado == "si") {
            ir = true;
          } else {
            notyfy({
              text: data.mensaje,
              type: "error", // alert|error|success|information|warning|primary|confirm
            });
          }
        })
        .fail(function () {
          notyfy({
            text: "Error intentando conectar con el servidor, verifique su sesión de usuario o conexión de internet, ",
            type: "error", // alert|error|success|information|warning|primary|confirm
          });
        });
      //}//fin validacion de tiempo antes del curso
    } else {
      notyfy({
        text: " Solo podrás ingresar al curso 5 minutos antes de que inicie ",
        type: "primary", // alert|error|success|information|warning|primary|confirm
      });
    }
  } // fin params.tipo curso
  return ir;
}

function VerMas_Mostrar() {
  if (VerMasVarControl == 0) {
    $("#VerMasIndex").show();
    $("#btnVerMas_Index").html("Ver menos...");
    VerMasVarControl = 1;
  } else {
    $("#VerMasIndex").hide();
    $("#btnVerMas_Index").html("Ver más...");
    VerMasVarControl = 0;
  }
}
$(function () {
  var oMemTable_T = $("#Table_DataTomados").dataTable({
    sPaginationType: "bootstrap",
    sDom: "<'row separator bottom'>",
    aaSorting: [[0, "asc"]],
    sScrollX: "100%",
    sScrollXInner: "100%",
    bScrollCollapse: true,
    oLanguage: {
      sLengthMenu: "Mostrando _MENU_ registros por pagina",
      sZeroRecords: "No hay registros",
      sInfo: "Mostrando _START_ a _END_ de _TOTAL_ registros",
      sInfoEmpty: "Mostrando 0 to 0 of 0 registros",
      sInfoFiltered: "",
      sEmptyTable: "No hay registros",
      sSearch: "Buscar",
      oPaginate: {
        sNext: "Siguiente",
        sPrevious: "Anterior",
        sFirst: "Inicio",
        sLast: "Fin",
      },
    },
  });
  $("#dateRangeFrom").bdatepicker({
    format: "yyyy-mm-dd",
    startDate: "1915-01-01",
  });
  $("#dateRangeTo").bdatepicker({
    format: "yyyy-mm-dd",
    startDate: "1915-01-01",
  });
});

(function ($) {
  $(window).on("load", function () {
    setTimeout(function () {
      $("#news-featured-2").owlCarousel({
        slideSpeed: 500,
        paginationSpeed: 600,
        navigation: true,
        autoPlay: 5000,
        items: 4,
        itemsCustom: [
          [0, 1],
          [450, 1],
          [600, 1],
          [700, 1],
          [880, 2],
          [1100, 2],
          [1200, 3],
          [1400, 4],
          [1600, 4],
        ],
        navigationText: ["Anterior", "Siguiente"],
      });
      $("#LoadingDisponibles").addClass("hide");
    }, 100);
    setTimeout(function () {
      $("#news-featured-2-1").owlCarousel({
        slideSpeed: 500,
        paginationSpeed: 600,
        navigation: true,
        autoPlay: 5000,
        items: 4,
        itemsCustom: [
          [0, 1],
          [450, 1],
          [600, 1],
          [700, 1],
          [880, 2],
          [1100, 2],
          [1200, 3],
          [1400, 4],
          [1600, 4],
        ],
        navigationText: ["Anterior", "Siguiente"],
      });
      $("#LoadingInscritos").addClass("hide");
    }, 100);
    setTimeout(function () {
      $("#banner-home").owlCarousel({
        slideSpeed: 500,
        paginationSpeed: 600,
        navigation: false,
        autoPlay: 5000,
        items: 1,
        navigationText: ["Anterior", "Siguiente"],
      });
    }, 100);
  });
})(jQuery);
/*
23/11/2016
Andres Vega
Funcion para almacenar mediante AJAX el registro de un click a un banner
*/

function stop_vid() {
  $("video")[0].pause();
}
function clickBanner(id) {
  $.ajax({
    url: "controllers/banners.php",
    data: { opcn: "click", id: id },
    type: "post",
  });
} //fin funcion clickBanner

/*Funciones para streaming*/
/*var Interval_contador;
    var apiKey  = '45696832',
    sessionId   = '1_MX40NTY5NjgzMn5-MTQ4ODQwMTk3NzYxOH5MSnJLV1Y5SjNKbGh5RXFGazVIRkFBdVp-fg',
    token       = 'T1==cGFydG5lcl9pZD00NTY5NjgzMiZzaWc9Y2M2ODhiMzRjZGU0ZTFkODdjMDM2OTZhZjQ2NGU5MDM3YTdlNTE2NTpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOamd6TW41LU1UUTRPRFF3TVRrM056WXhPSDVNU25KTFYxWTVTak5LYkdoNVJYRkdhelZJUmtGQmRWcC1mZyZjcmVhdGVfdGltZT0xNDg4NDAxOTkzJm5vbmNlPTAuMzEyODc0MTc4NDc0OTIyNyZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDg4NDg4Mzky';
    var extensionId = 'ijgfggkmnahehfeimghpomadjmmkiphd';
    var ffWhitelistVersion; // = '36';
    var session = OT.initSession(apiKey, sessionId);
    session.connect(token, function(error) {
      if (error) {
        //alert('Error connecting to session: ' + error.message);
        return;
      }
      if($('#emisor_id').length){
        $('#imagenPreview').hide();
        var publisher = OT.initPublisher('camera-publisher',{
          width: '720px',
          height: '500px',
          insertMode: "append",
          name: 'Transmisión Jaime Gil',
          left: '86px'
        });
        session.publish(publisher);
        resizeTam();
      }
    });
    session.on('streamCreated', function(event) {
        $('#imagenPreview').hide();
        session.subscribe(event.stream, 'camera-subscriber', {
          width: '720px',
          height: '500px',
          insertMode: "append",
          name: 'Transmisión Jaime Gil',
          left: '86px'
        });
        resizeTam();
    });
    function resizeTam(){
        $(".OT_root").each(function (index,element) {
            $(element).css('left','85px');
            Interval_contador = setInterval(CuentaConectados,3000);
        });
    }
    function CuentaConectados(){
        var data = new FormData();
        data.append('opcn','CantConectados');
        var url = "controllers/index_streaming.php";
        $.ajax({
            url:url,
            type:'post',
            contentType:false,
            data:data,
            processData:false,
            dataType: "json",
            cache:false,
            success: function(data) {
                var Cantidad_Conec = data.cantidad;
                $('#ContadorVisitantes').html('Visitantes: '+Cantidad_Conec);
            }
        });
    }*/
/*Funciones para streaming*/
