$(document).ready(function(){
    // Select Placeholders
    $("#question_id1").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#question_id2").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#question_id3").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#question_id4").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#question_id5").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
   setTimeout(function(){ $('#answer1').val(''); }, 1000);
});


$( "#formulario_data" ).submit(function( event ) {
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }
    event.preventDefault();
});

$("#retornaElemento").click(function() {
    window.location="modulos.php";
});

function valida(){
    var valid = 0;
    $('#question_id1').css( "border-color", "#efefef" );
    $('#question_id2').css( "border-color", "#efefef" );
    $('#question_id3').css( "border-color", "#efefef" );
    $('#question_id4').css( "border-color", "#efefef" );
    $('#question_id5').css( "border-color", "#efefef" );

    $('#answer1').css( "border-color", "#efefef" );
    $('#answer2').css( "border-color", "#efefef" );
    $('#answer3').css( "border-color", "#efefef" );
    $('#answer4').css( "border-color", "#efefef" );
    $('#answer5').css( "border-color", "#efefef" );

    if($('#answer1').val() == ""){
        $('#answer1').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#answer2').val() == ""){
        $('#answer2').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#answer3').val() == ""){
        $('#answer3').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#answer4').val() == ""){
        $('#answer4').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#answer5').val() == ""){
        $('#answer5').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if($('#question_id1').val() == "0"){
        $('#question_id1').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#question_id2').val() == "0"){
        $('#question_id2').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#question_id3').val() == "0"){
        $('#question_id3').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#question_id4').val() == "0"){
        $('#question_id4').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#question_id5').val() == "0"){
        $('#question_id5').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente, debe seleccionar y completar todas las preguntas y respuestas',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg){
    $.ajax({ url: 'controllers/security_questions.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;

            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}
