$( "#formulario" ).submit(function( event ) {
    if(valida()){
        Actualizar();
    }
    event.preventDefault();
});


function valida(){
    var valid = 0;
    $('#contrasena').css( "border-color", "#efefef" );
    $('#re_contrasena').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#contrasena').val() == ""){
        $('#contrasena').css( "border-color", "#b94a48" );
        valid = 1;
    }else if($('#re_contrasena').val() != $('#contrasena').val()){
        $('#re_contrasena').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de almacenar esta información?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Actualizar(){
    $.ajax({ url: 'controllers/usuarios.php',
        data: $('#formulario').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'Su contraseña se ha cambiado satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado actualizar la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}