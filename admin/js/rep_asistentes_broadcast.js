  //-----------------------------//
 // Creado por: Fredy Mendoza   //
//-----------------------------//
$(document).ready(function(){
        consultar_broad();

});

        $("#conference_id").change(function(){
        $("#descargar").css("display","none");
        });

        $("#descargar").click(function(event) {
        $("#datos_tabla").val( $("<div>").append( $("#asistentes2").eq(0).clone()).html());//Clona la tabla para envialo a Fichero.excel para descargar
        $("#FormularioExportacion").submit(); 
        });
   
//consulta el nombre y id del broadcast
function consultar_broad(){

var data = {opcn:'consultar_Broad'};

         $.ajax({
             url: 'controllers/rep_asistentes_broadcast.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             console.log(data);
            var datos = "";
            for (var i = 0; i < data.length; i++) {
              datos += '<option value="'+data[i].conference_id+'">'+data[i].conference+' - '+data[i].conference_id+' </option>';
             
             }

             $('#conference_id').html(datos)

                $("#conference_id").select2({
                    placeholder: "Seleccione una sesión",
                    allowClear: true
                 });
         })
         .fail(function() {
             console.log("error");
         })
}
//-----------------------Imprime los datos en la tabla----------------------//
$('#frm').submit(function(e) {
    e.preventDefault();
    var conference_id = $('#conference_id').val()

    $('#consultando').html('<img style="width: 15px; " src="../assets/loading.gif">');

    var data = {opcn:'consultar_asistantes', conference_id: conference_id};
    $.ajax({
             url: 'controllers/rep_asistentes_broadcast.php',
             type: 'POST',
             dataType: 'JSON',
             data: data,
         })
         .done(function(data) {
             console.log(data);
             var xnum = ''
             xnum = data.length
             $('#consultando').html('');
             $("#descargar").css("display","block");
             var table = "";
            for (var i = 0; i < data.length; i++) {
  
                table +='<tr>';
                table +='<td>'+data[i].identification+'</td>';
                table +='<td>'+data[i].apellidos+' '+data[i].nombres+'</td>';
                table +='<td>'+data[i].concesionario+'</td>';
                table +='<td>'+data[i].sede+'</td>';
                table +='<td>'+data[i].trayectorias+'</td>';
                table +='<td>'+data[i].local_charge+'</td>';
                table +='</tr>';
             
             }

             $('#asistentes tbody').html(table);
             $('#asistentes2 tbody').html(table);
             $('#asistentes').DataTable();//convierte en datatable
             $('#num_asistentes').html(xnum+' Participantes');

         })
         .fail(function() {
             $('#consultando').html('');
             console.log("error");
         })
});


  


