$( "#form_CrearGaleria" ).submit(function( event ) {
    if(valida()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});
var Timer_Chat;
$(document).ready(function(){
    $("#ElDeBajar").animate({ scrollTop: $('#ElDeBajar')[0].scrollHeight}, 1000);
    $.ajaxSetup({"cache":false});
    Timer_Chat = setTimeout(CargaNuevosReply,3000);
    $("#botonChat").click(function(){
        if($('#comentario').val()!=""){
            var datos  = $("#FormularioComentarios").serialize();
            $.ajax({
                url     : "controllers/foros_tutor.php",
                data    : datos,
                type    : "post",
                dataType: "json",
                success: function(data){
                    var res0 = data.resultado;
                    var MaxIdN = data.MaxId;
                    //$("#MaxIdReply").val(MaxIdN);
                    var Comentario_Text = $('#comentario').val();
                    $('#comentario').val('');
                    $('#MensajesForo').append('<div class="media border-bottom margin-none"><div class="media-body"><small class="label label-default">Escribiste:</small> <span> '+Comentario_Text+'</span><br></div></div>');
                    $('#MensajesForo').append('<input type="hidden" class="MaxIdReply" name="MaxIdReply" id="MaxIdReply" value="'+MaxIdN+'">');
                    $("#ElDeBajar").animate({ scrollTop: $('#ElDeBajar')[0].scrollHeight}, 1000);
                }
            });
        }else{
            notyfy({
                text: 'Para enviar un mensaje debe escribirlo!',
                type: 'warning' // alert|error|success|information|warning|primary|confirm
            });
        }
    });
});

$(document).ready(function(){
    // Select Placeholders
    $("#category").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#cargos").select2({
        placeholder: "Seleccione la (las) trayectorias",
        allowClear: true
    });
    $("#category_ed").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#cargos_ed").select2({
        placeholder: "Seleccione la (las) trayectorias",
        allowClear: true
    });
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
});

$(document).ready(function() {
    $('a[href][title]').qtip({
        content: {
            text: false
        },
        style: {
            tip: 'leftMiddle',
            color: 'white',
            background:'#A2D959',
            name: 'green'
        },
        position: {
            target: 'mouse'
        }
    });
});

function valida(){
    var valid = 0;
    $('#forum').css( "border-color", "#efefef" );
    $('#description').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#forum').val() == ""){
        $('#forum').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#description').val() == ""){
        $('#description').css( "border-color", "#b94a48" );
        valid = 1;
    }
    
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

$( "#form_EditarGaleria" ).submit(function( event ) {
    if(validaEdt()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

$( "#form_reply" ).submit(function( event ) {
    if(ValidaReply()){
        return true;
    }else{
        return false;
    }
    return false;
    event.preventDefault();
});

/*function ValidaReply(){
    var valid = 0;
    $('#replaycomment').css( "border-color", "#efefef" );
    if($('#replaycomment').val() == ""){
        $('#replaycomment').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente, debe incluir información en el comentario',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}*/

/*function validaEdt(){
    var valid = 0;
    $('#forum_ed').css( "border-color", "#efefef" );
    $('#forum_description_ed').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#forum_ed').val() == ""){
        $('#forum_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#forum_description_ed').val() == ""){
        $('#forum_description_ed').css( "border-color", "#b94a48" );
        valid = 1;
    }
    
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}*/

function LikeReply(forum_reply_id){
    var data = new FormData();
    data.append('forum_reply_id',forum_reply_id);
    data.append('opcn','LikeReply');
    var url = "controllers/foros_tutor.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                notyfy({
                    text: 'Muchas gracias, hemos recibido tu like! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes'+forum_reply_id).text());
                $('#CantLikes'+forum_reply_id).text(Cantlikes+1);
            }else{
                notyfy({
                    text: 'Ya votaste por este POST anteriormente, muchas gracias',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function ProcesoPost(forum_reply_id,status_id){
    var data = new FormData();
    data.append('forum_reply_id',forum_reply_id);
    data.append('status_id',status_id);
    data.append('opcn','ProcesoAct');
    var url = "controllers/foros_tutor.php";
    $.ajax({
        url:url,
        type:'post',
        contentType:false,
        data:data,
        processData:false,
        dataType: "json",
        cache:false,
        success: function(data) {
            if(data.resultado=="SI"){
                notyfy({
                    text: 'Se ha realizado la activación del post, refresca la página y lo verás listo! ',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                var Cantlikes = parseInt($('#CantLikes'+forum_reply_id).text());
                $('#CantLikes'+forum_reply_id).text(Cantlikes+1);
            }else{
                notyfy({
                    text: 'No se ha podido llevar a cabo la activación, intenta nuevamente',
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

function CargaNuevosReply(){
    clearInterval(Timer_Chat);
    if ($('#MaxIdReply').length){
        var forum_id = $('#forum_id').val();
        //var MaxIdReply = $('#MaxIdReply').val();
        var MaxIdReply = $(".MaxIdReply").last().val();//id del ultimo comentario en el chat
        $.post('controllers/foros_tutor.php',{opcn:'comments',forum_id:forum_id,MaxIdReply:MaxIdReply},function(datos,status,xhr){
            if(status== 'success' && datos != ''){
                $("#MensajesForo").append(datos);
                $("#ElDeBajar").animate({ scrollTop: $('#ElDeBajar')[0].scrollHeight}, 1000);
            }
        });
        Timer_Chat = setTimeout(CargaNuevosReply,1500);
    }
}





/*Para transmisión*/
function CambiaDrag(){
  clearInterval(CameraMov);
  if($('#camera-publisher').length){
    $('#camera-publisher').draggable();
    //$('#camera-publisher').css('width','180px');
    //$('#camera-publisher').css('height','135px');
  }
  if($('#camera-subscriber').length){
    $('#camera-subscriber').draggable();
    //$('#camera-subscriber').css('width','180px');
    //$('#camera-subscriber').css('height','135px');
  }
  console.log("camera draggablle");
}
    $("#btn_inscription").hide();
    var CameraMov;
    var SAMPLE_SERVER_BASE_URL = 'https://www.gmacademy.co/assets/vct/'
    if($('#sessionId').length){
        var apiKey    = '45696832',
        sessionId = $('#sessionId').val();//'1_MX40NTY5NjgzMn5-MTQ4NzI2MzkzMzA2M35NZ3FoNzVsMVVnSTVPUitHRlVUbFcxTjF-fg',
        token     = $('#token').val();//'T1==cGFydG5lcl9pZD00NTY5NjgzMiZzaWc9ZWQxMDVmOTA1YmFlOGJmNjNiMTJmYTk2OTk3ZDk1NDlhMWZhNmIzZDpzZXNzaW9uX2lkPTFfTVg0ME5UWTVOamd6TW41LU1UUTROekkyTXprek16QTJNMzVOWjNGb056VnNNVlZuU1RWUFVpdEhSbFZVYkZjeFRqRi1mZyZjcmVhdGVfdGltZT0xNDg3MjYzOTUzJm5vbmNlPTAuMzQ0NTc4MTczMTgyNTgwNTUmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTQ4NzI4NTU1Mg==';
        // Replace this with the ID for your Chrome screen-sharing extension, which you can
        // get at chrome://extensions/:
        var extensionId = 'ijgfggkmnahehfeimghpomadjmmkiphd';
        // If you register your domain with the the Firefox screen-sharing whitelist, instead of using
        // a Firefox screen-sharing extension, set this to the Firefox version number, such as 36, in which
        // your domain was added to the whitelist:
        var ffWhitelistVersion; // = '36';
        var session = OT.initSession(apiKey, sessionId);
        session.connect(token, function(error) {
          if (error) {
            alert('Error connecting to session: ' + error.message);
            return;
          }
          if($('#teacher_id').length){
            // publish a stream using the camera and microphone:
            var publisher = OT.initPublisher('camera-publisher',{
              width: '180px',
              height: '135px'
            });
            session.publish(publisher);
            document.getElementById('shareBtn').disabled = false;
            CameraMov = setInterval(CambiaDrag,2000);
            
          }
        });
        session.on('streamCreated', function(event) {
          if (event.stream.videoType === 'screen') {
            // This is a screen-sharing stream published by another client
            var subOptions = {
              width: event.stream.videoDimensions.width / 2,
              height: event.stream.videoDimensions.height / 2
            };
            session.subscribe(event.stream, 'screen-subscriber', subOptions);
            $('#camera-subscriber').css('top','-230px');
            $('#camera-subscriber').css('left','-27px');
            CameraMov = setInterval(CambiaDrag,2000);
          } else {
            // This is a stream published by another client using the camera and microphone
            session.subscribe(event.stream, 'camera-subscriber', {
                insertMode: "append",
                width: '180px',
                height: '135px'
            });
            CameraMov = setInterval(CambiaDrag,2000);
          }
        });
        // For Google Chrome only, register your extension by ID,
        // You can find it at chrome://extensions once the extension is installed
        OT.registerScreenSharingExtension('chrome', extensionId, 2);
        function screenshare() {
          OT.checkScreenSharingCapability(function(response) {
            console.info(response);
            if (!response.supported || response.extensionRegistered === false) {
              alert('This browser does not support screen sharing.');
            } else if (response.extensionInstalled === false
                && (response.extensionRequired || !ffWhitelistVersion)) {
              alert('Please install the screen-sharing extension and load this page over HTTPS.');
            } else if (ffWhitelistVersion && navigator.userAgent.match(/Firefox/)
              && navigator.userAgent.match(/Firefox\/(\d+)/)[1] < ffWhitelistVersion) {
                alert('For screen sharing, please update your version of Firefox to '
                  + ffWhitelistVersion + '.');
            } else {
              // Screen sharing is available. Publish the screen.
              // Create an element, but do not display it in the HTML DOM:
              var screenContainerElement = document.createElement('div');
              var screenSharingPublisher = OT.initPublisher(
                screenContainerElement,
                { videoSource : 'screen' },
                function(error) {
                  if (error) {
                    alert('Something went wrong: ' + error.message);
                  } else {
                    session.publish(
                      screenSharingPublisher,
                      function(error) {
                        if (error) {
                          alert('Something went wrong: ' + error.message);
                        }
                      });
                  }
                });
              }
            });
          CameraMov = setInterval(CambiaDrag,2000);
        }
    }
/*Para transmisión*/