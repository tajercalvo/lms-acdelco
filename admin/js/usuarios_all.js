$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/usuarios_all.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});
$(document).ready(function(){
    $("#charge_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#rol_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#status_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#headquarter_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });

    $("#dealer_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#type_user_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#action").select2({
        placeholder: "Seleccione el perfil",
        allowClear: true
    });
    $("#headquarter_id_m").select2({
        placeholder: "Seleccione el perfil",
        allowClear: true
    });
    $("#cargar_headquarter").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#no_time_ev").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
});

function validarAdjunto(archivo){
  var extenciones = new Array(".csv",".xls");
  var resultado = false;
  if(!archivo){
    notyfy({
        text: 'No se ha seleccionado ningún archivo',
        type: 'error' // alert|error|success|information|warning|primary|confirm
    });
  }else{
    var extencion = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    console.log(extencion);
    for (var i = 0; i < extenciones.length; i++) {
      if(extenciones[i] == extencion){
        resultado = true;
        break;
      }//fin if
    }//fin for
    if(!resultado){
      notyfy({
          text: 'No se ha seleccionado ningún archivo en formato separado por comas (.csv)',
          type: 'error' // alert|error|success|information|warning|primary|confirm
      });
    }
  }//fin else
  return resultado;
}//fin funcion validar
$("#form_CargarCSV").submit(function(event) {
  var archivo = $("#archivo_csv").val();
  var headquarter = $("#cargar_headquarter").val();
  console.log(headquarter);
  if(validarAdjunto(archivo)){
    var datos = new FormData();
    datos.append("archivo_csv",$("#archivo_csv")[0].files[0]);
    datos.append("opcn","cargar");
    datos.append("cargar_headquarter",headquarter);
    $.ajax({
      type:'post',
      dataType: 'html',
      url: 'usuarios_ajax.php',
      contentType: false,
      data: datos,
      processData: false
    }).done(function(respuesta){
      $("#cerrarForm").trigger("click");
      $("#div_resultado").html(respuesta);
      $("#btn_resultado").trigger("click");
    });
  }
  event.preventDefault();
});
