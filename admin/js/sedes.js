$(function() {
    var oMemTable = $('#TableData').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "controllers/sedes.php?action=getElementsAjax",
        "sPaginationType": "bootstrap",
        "sDom": "<'row separator bottom'<'col-md-5'T><'col-md-3'l><'col-md-4'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "aaSorting": [[ 0, "asc" ]],
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "bScrollCollapse": true,
        "oLanguage": {
            "sLengthMenu": "Mostrando _MENU_ registros por pagina",
            "sZeroRecords": "No hay registros",
            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 to 0 of 0 registros",
            "sInfoFiltered": "",
            "sEmptyTable": "No hay registros",
            "sSearch": "Buscar",
            "sProcessing": "procesando información...",
            "oPaginate":{
                "sNext" : "Siguiente",
                "sPrevious" : "Anterior",
                "sFirst" : "Inicio",
                "sLast" : "Fin"
            }
        }
    });
});
$(document).ready(function(){
    // Select Placeholders
    $("#status_id").select2({
        placeholder: "Seleccione un estado",
        allowClear: true
    });
    $("#area_id").select2({
        placeholder: "Seleccione un area",
        allowClear: true
    });
    $("#dealer_id").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
    });
    $("#type_headquarter").select2({
        placeholder: "Seleccione un tipo de sede",
        allowClear: true
    });
    $("#category_id").select2({
        placeholder: "Seleccione una categoria",
        allowClear: false
    });
});

$( "#formulario_data" ).submit(function( event ) {
    event.preventDefault();
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }

    }
});

$("#retornaElemento").click(function() {
    window.location="sedes.php";
});

function valida(){
    var valid = 0;
    $('#headquarter').css( "border-color", "#efefef" );
    $('#bac').css( "border-color", "#efefef" );
    $('#address1').css( "border-color", "#efefef" );
    $('#phone1').css( "border-color", "#efefef" );
    $('#email1').css( "border-color", "#efefef" );
    var valid = 0;
    if($('#headquarter').val() == ""){
        $('#headquarter').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#bac').val() == ""){
        $('#bac').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#address1').val() == ""){
        $('#address1').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#phone1').val() == ""){
        $('#phone1').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#email1').val() == ""){
        $('#email1').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}

function Operacion(msg){
    $.ajax({ url: 'controllers/sedes.php',
        data: $('#formulario_data').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.resultado;
            if(res0 == "SI"){
                notyfy({
                    text: 'La operación ha sido completada satisfactoriamente',
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: 'No se ha logrado '+msg+' la información, por favor confirme su conexión y realicela nuevamente',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}
