$(document).ready(function(){
    $("#identification").keyup(function() {
        var PalabraBusqueda = $("#identification").val().toUpperCase();
        if(PalabraBusqueda.length>4){
            var data = new FormData();
            data.append('usr_iden',PalabraBusqueda);
            data.append('opcn','BuscarUsuario');
            var url = "controllers/usuarios_all.php";
            $.ajax({
                url:url,
                type:'post',
                contentType:false,
                data:data,
                processData:false,
                dataType: "json",
                cache:false,
                success: function(data) {
                    if(data.resultado=="SI"){
                        notyfy({
                            text: 'El usuario que esta intentando crear ya existe en Ludus, a continuación se muestra parte de su información; no intente crearlo con una identificación diferente a la del usuario pues tendrá problemas con los certificados y su historial de notas.<br><br>Nombres: '+data.nombres+'<br>Concesionario Actual: '+data.concesionario,
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                    }
                }
            });
        }
    });
	$("#roles").select2({
	    placeholder: "Seleccione el perfil",
	    allowClear: true
	});
    $("#education").select2({
	    placeholder: "Seleccione el nivel educativo",
	    allowClear: true
	});
	$("#cargos").select2({
	    placeholder: "Seleccione la trayectoria",
	    allowClear: true
	});
  $("#type").select2({
	    placeholder: "Seleccione el tipo de usuario",
	    allowClear: true
	});
  $("#zone_id").select2({
	    placeholder: "Seleccione la zona",
	    allowClear: true
	});
	$('#date_birthday').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "1915-01-01"
    });
    $("#status_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#headquarter_id").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $('#date_startgm').bdatepicker({
        format: "yyyy-mm-dd",
        startDate: "1960-01-01"
    });
    $("#linking").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#gender").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });
    $("#pais").select2({
        placeholder: "Seleccione uno",
        allowClear: true
    });

});

$( "#formulario_usuarios" ).submit(function( event ) {
    event.preventDefault();
    if(valida()){
        if($('#opcn').val()=="crear"){
            Operacion('Crear');
        }else if($('#opcn').val()=="editar"){
            Operacion('Actualizar');
        }else{
            notyfy({
                text: 'No se ha logrado completar la operación',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }
});

function valida(){
    var valid = 0;
    $('#first_name').css( "border-color", "#efefef" );
    $('#last_name').css( "border-color", "#efefef" );
    $('#identification').css( "border-color", "#efefef" );
    $('#local_charge').css( "border-color", "#efefef" );
    $('#email').css( "border-color", "#efefef" );
    $('#phone').css( "border-color", "#efefef" );
    $('#date_birthday').css( "border-color", "#efefef" );

    var valid = 0;
    if($('#first_name').val() == ""){
        $('#first_name').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#last_name').val() == ""){
        $('#last_name').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#identification').val() == ""){
        $('#identification').css( "border-color", "#b94a48" );
        valid = 1;
    }

    if($('#local_charge').val() == ""){
        $('#local_charge').css( "border-color", "#b94a48" );
        valid = 1;
    }
    if($('#email').val() == ""){
        $('#email').css( "border-color", "#b94a48" );
        valid = 1;
    }
    
    if(valid == 0){
        if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            return true;
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
            return false;
        }
    }else{
        notyfy({
            text: 'Por favor verifíque la información que esta pendiente',
            type: 'warning' // alert|error|success|information|warning|primary|confirm
        });
    }
}
function Operacion(msg){
    //cargaImagen();
    $.ajax({ url: 'controllers/usuarios_all.php',
        data: $('#formulario_usuarios').serialize(),
        type: 'post',
        dataType: "json",
        success: function(data) {
            var res0 = data.error;
            console.log( res0 );
            if( !res0 ){
                notyfy({
                    text: data.mensaje,
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
            }else{
                notyfy({
                    text: data.mensaje,
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }
    });
}

/*$( "#formulario_usuarios" ).submit(function( event ) {
    if(confirm('¿Esta seguro de ejecutar esta instrucción?')){
            $.ajax({ url: 'controllers/usuarios_all.php',
                data: $('#formulario_usuarios').serialize(),
                type: 'post',
                dataType: "json",
                success: function(data) {
                    var res0 = data.resultado;
                    if(res0 == "SI"){
                        notyfy({
                            text: 'La operación ha sido completada satisfactoriamente',
                            type: 'success' // alert|error|success|information|warning|primary|confirm
                        });
                    }else{
                        notyfy({
                            text: 'El usuario ya se encuentra creado en otro CONCESIONARIO, se ha enviado una notificación al administrador para que tenga en cuenta dicho cambio',
                            type: 'error' // alert|error|success|information|warning|primary|confirm
                        });
                    }
                }
            });
        }else{
            notyfy({
                text: 'ha cancelado la operación',
                type: 'information' // alert|error|success|information|warning|primary|confirm
            });
        }
    event.preventDefault();
});*/
function cargaImagen(){
    $('#loading_imagen').removeClass('hide');
    // console.log("cargando imagen");
    try {
        var inputFileImage = document.getElementById("image_new");
        var file = inputFileImage.files[0];
        if(file.type == "image/jpeg"){
            if(file.size<1500000){
                var data = new FormData();
                data.append('image_new',file);
                data.append('user_id',$('#user_id').val());
                data.append('imgant',$('#imgant').val());
                data.append('opcn','CrearImagen');
                var url = "controllers/usuarios_all.php";
                    $.ajax({
                        url:url,
                        type:'post',
                        contentType:false,
                        data:data,
                        processData:false,
                        dataType: "json",
                        cache:false,
                        success: function(data) {
                            if(data.resultado=="SI"){
                                $("#ImgUsuario").attr("src","../assets/images/usuarios/"+data.archivo);
                                notyfy({
                                    text: 'La imagen ha sido cambiada correctamente',
                                    type: 'success' // alert|error|success|information|warning|primary|confirm
                                });
                            }else{
                                notyfy({
                                    text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                                    type: 'error' // alert|error|success|information|warning|primary|confirm
                                });
                            }
                            $('#loading_imagen').addClass('hide');
                        }
                    });
            }else{
                notyfy({
                    text: 'La imagen NO ha sido cambiada, el tamaño es demasiado GRANDE',
                    type: 'error' // alert|error|success|information|warning|primary|confirm
                });
            }
        }else{
            notyfy({
                text: 'La imagen NO ha sido cambiada, revise la extensión del archivo sólo se acepta JPG',
                type: 'error' // alert|error|success|information|warning|primary|confirm
            });
        }
    }catch( err ) {
        $('#loading_imagen').addClass('hide');
        notyfy({
            text: err.message,
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
    }
}//fin function cargaImagen

//=====================================================================
//Funcion que activa la evaluacion al usuario en cualquier momento
//=====================================================================
function activarNoTime(){
    $('#btn_activar_nte').attr( 'disabled', 'disabled' );
    datos = {
        'opcn': 'activar',
        'user_id': $('#user_id').val()
    };
    $.ajax({
        url: 'controllers/perfil.php',
        type: 'post',
        data: datos,
        dataType: "json",
        success: function( data ) {
            $('#btn_activar_nte').removeAttr( 'disabled' );
            if( data.error ){
                notyfy({
                    text: data.message,
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
                return;
            }else{
                boton = `<button type="button" id="btn_inactivar_nte" class="btn btn-primary" onclick="inactivarNoTime( ${data.review_notime_id} )">Inactivar</button>`;
                $('#review_notime').html( boton );
            }
        }
    });
}
//=====================================================================
//Funcion que inactiva la opcion de permitir la evaluacion en cualquier fecha
//=====================================================================
function inactivarNoTime( review_notime_id ){
    $('#btn_inactivar_nte').attr( 'disabled', 'disabled' );
    datos = {
        'opcn': 'inactivar',
        'user_id': $('#user_id').val(),
        'review_notime_id': review_notime_id
    };
    $.ajax({
        url: 'controllers/perfil.php',
        type: 'post',
        data: datos,
        dataType: "json",
        success: function( data ) {
            $('#btn_inactivar_nte').removeAttr( 'disabled' );
            if( data.error ){
                notyfy({
                    text: data.message,
                    type: 'warning' // alert|error|success|information|warning|primary|confirm
                });
                return;
            }else{
                boton = `<button type="button" id="btn_activar_nte" class="btn btn-success" onclick="activarNoTime()">Activar</button>`;
                $('#review_notime').html( boton );
                notyfy({
                    text: data.message,
                    type: 'success' // alert|error|success|information|warning|primary|confirm
                });
                return;
            }
        }
    });
}
