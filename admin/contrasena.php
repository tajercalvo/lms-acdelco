<?php include('src/seguridad.php'); ?>
<?php 
$idUsuario_log = $_SESSION['idUsuario'];
include('controllers/perfil.php');
$location = 'configuracion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons keys"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/contrsena.php">Cambio de Contraseña</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Cambiar mi contraseña &nbsp;<i class="fa fa-fw fa-key text-muted"></i></h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray widget-employees">
	<div class="widget-body padding-none">		
		<div class="row">
			<div class="col-md-12 detailsWrapper">
				<div class="ajax-loading hide">
					<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
				</div>
				<div class="innerAll">
					<div class="title">
						<div class="row">
							<div class="col-md-8">
								<h3 class="text-primary"><?php echo($datosUsuario['first_name'].' '.$datosUsuario['last_name']); ?></h3>
							</div>
							<div class="col-md-4 text-right">
								
							</div>
						</div>
					</div>
					<hr/>
					<div class="body">
						<div class="row">
							<div class="col-md-3 overflow-hidden">
								<!-- Profile Photo -->
								<!-- <div>
								<img src="../assets/images/usuarios/logouser.png"<?php echo($_SESSION['imagen']); ?> style="width: 200px;" alt="<?php echo($datosUsuario['nombres'].' '.$datosUsuario['apellidos']); ?>" />
								</div> -->
								<div>
										<img src="../assets/images/usuarios/logouser.png" <?php echo($_SESSION['imagen']); ?> 
										style="width: 200px;" 
										alt="<?php echo isset($datosUsuario['nombres']) ? $datosUsuario['nombres'] : ''; ?> 
													<?php echo isset($datosUsuario['apellidos']) ? $datosUsuario['apellidos'] : ''; ?>" />
								</div>

								<div class="separator bottom"></div>
								<!-- // Profile Photo END -->
								<ul class="icons-ul separator bottom">
									<li><i class="fa fa-envelope fa fa-li fa-fw"></i> @: <?php echo($datosUsuario['email']); ?></li>
									<li><i class="fa fa-phone fa fa-li fa-fw"></i> Tel: <?php echo($datosUsuario['phone']); ?></li>
									<li><i class="fa fa-skype fa fa-li fa-fw"></i> <?php echo($datosUsuario['headquarter']); ?> </li>
								</ul>
							</div>
							<div class="col-md-3 overflow-hidden">
								<h5 class="strong">Cambiar contraseña:</h5>
								<form action="../usuarios/" method="post" id="formulario">
									<div class="row">
										<div class="col-md-12">
											<label class="control-label" for="contrasena">Contraseña:</label>
											<input class="form-control" id="contrasena" name="contrasena" type="password" placeholder="**********" />
											<input type="hidden" id="opcn" name="opcn" value="contrasena"/>
											<input type="hidden" id="idUsuario" name="idUsuario" value="<?php echo($_SESSION['idUsuario']); ?>"/>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<label class="control-label" for="re_contrasena">Reescribir:</label>
											<input class="form-control" id="re_contrasena" name="re_contrasena" type="password" placeholder="**********" />
										</div>
									</div>
									<div class="separator"></div>
									<div class="form-actions">
										<button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Cambiar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/contrasena.js"></script>
</body>
</html>