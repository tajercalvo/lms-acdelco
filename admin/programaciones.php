<?php include('src/seguridad.php'); ?>
<?php include('controllers/programaciones.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<style>
	.modal-lg {
		width: 80% !important;
	}

	/* .modal{
		z-index: 10000;
	} */
</style>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons calendar"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/programaciones.php">Configuración Programación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Programación </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-9">
									<h5 style="text-align: justify; ">A continuación encontrará las opciones necesarias para administrar el calendario de cursos de la plataforma; tenga en cuenta que una vez creado un elemento de lista, este no podrá ser eliminado. Únicamente podrá inactivarse para que no aparezca en el sistema, pero en este listado aparecerá como inactivo. La programación tiene diferentes estados que deben tenerse en cuenta.</h5>
									<h5><a href="programaciones_excel.php" target="_blank" class="glyphicons no-js download_alt"><i></i>Descargar</a>
										<h5>
								</div>
								<div class="col-md-1">
								</div>
								<div class="col-md-2">
									<?php if ($_SESSION['max_rol'] >= 5) { ?><h5><a href="op_programacion.php?opcn=nuevo" class="glyphicons no-js circle_plus"><i></i>Programar</a>
											<h5><?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Sesiones programadas</h4>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body">
							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de sesiones: <?php echo ($cantidad_datos); ?>
							</div>
							<!-- // Total elements END -->
							<!-- Table elements-->
							<table id="TableData" class="display table table-striped table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<!--<th data-hide="phone,tablet">CODIGO</th>-->
										<th data-hide="phone,tablet">CURSO</th>
										<th data-hide="phone,tablet">INSTRUCTOR</th>
										<th data-class="expand">INICIO <br><i>(YYYY-MM-DD)</i></th>
										<th data-hide="phone,tablet">INSCRIPCIÓN <br><i>(YYYY-MM-DD)</i></th>
										<th data-hide="phone,tablet">EVALUACIÓN <br><i>(YYYY-MM-DD)</i></th>
										<th data-hide="phone,tablet">LUGAR</th>
										<th data-hide="phone,tablet">MIN</th>
										<th data-hide="phone,tablet">MAX</th>
										<th data-hide="phone,tablet">INS</th>
										<th data-hide="phone,tablet">TIEMPO</th>
										<th data-hide="phone,tablet">ESTADO</th>
										<th data-hide="phone,tablet"> - </th>
									</tr>
								</thead>
								<!-- // Table heading END -->
							</table>
							<!-- // Table elements END -->
						</div>
					</div>
					<!-- Nuevo ROW-->
					<div class="row row-app">
						<p class="separator text-center"><i class="icon-filter icon-2x"></i></p>
						<!-- Filters -->
						<form action="programaciones.php" method="post">
							<input type="hidden" id="opcn" name="opcn" value="filtrar">
							<div class="widget widget-heading-simple widget-body-gray">
								<div class="widget-body">
									<!-- Row -->
									<div class="row">
										<div class="col-md-3">
											<label class="control-label" for="course_id_search">Curso:</label>
											<select style="width: 100%;" id="course_id_search" name="course_id_search">
												<option value="0">Todos los cursos...</option>
												<?php foreach ($listadosCursos as $key => $Data_Cursos) { ?>
													<option value="<?php echo ($Data_Cursos['course_id']); ?>" <?php if (isset($_SESSION['course_id_search_prfl']) && ($_SESSION['course_id_search_prfl'] == $Data_Cursos['course_id'])) { ?>selected="selected" <?php } ?>><?php echo ($Data_Cursos['course']); ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="teacher_id_search">Instructor:</label>
											<select style="width: 100%;" id="teacher_id_search" name="teacher_id_search">
												<option value="0">Todos los instructores...</option>
												<?php foreach ($listadosProfesores as $key => $Data_Intructores) { ?>
													<option value="<?php echo ($Data_Intructores['user_id']); ?>" <?php if (isset($_SESSION['teacher_id_search_prfl']) && ($_SESSION['teacher_id_search_prfl'] == $Data_Intructores['user_id'])) { ?>selected="selected" <?php } ?>><?php echo ($Data_Intructores['first_name'] . ' ' . $Data_Intructores['last_name']); ?></option>
												<?php } ?>
											</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="status_id_search">Estado:</label>
											<select style="width: 100%;" id="status_id_search" name="status_id_search">
												<option value="0" <?php if (isset($_SESSION['status_id_search_prfl']) && ($_SESSION['status_id_search_prfl'] == "0")) { ?>selected="selected" <?php } ?>>Todos...</option>
												<option value="1" <?php if (isset($_SESSION['status_id_search_prfl']) && ($_SESSION['status_id_search_prfl'] == "1")) { ?>selected="selected" <?php } ?>>Activos</option>
												<option value="2" <?php if (isset($_SESSION['status_id_search_prfl']) && ($_SESSION['status_id_search_prfl'] == "2")) { ?>selected="selected" <?php } ?>>Inactivos</option>
											</select>
										</div>
										<div class="col-md-3">
											<label class="control-label" for="living_id_search">Salón:</label>
											<select style="width: 100%;" id="living_id_search" name="living_id_search">
												<option value="0">Todos los salones...</option>
												<?php foreach ($listadosSalones as $key => $Data_Salones) { ?>
													<option value="<?php echo ($Data_Salones['living_id']); ?>" <?php if (isset($_SESSION['living_id_search_prfl']) && ($_SESSION['living_id_search_prfl'] == $Data_Salones['living_id'])) { ?>selected="selected" <?php } ?>><?php echo ($Data_Salones['living']); ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<!-- Row END-->
									<div class="clearfix"><br></div>
									<!-- Row -->
									<div class="row">
										<div class="col-md-3">
											<label class="col-md-5 control-label" for="start_date_search" style="padding-top:8px;">Desde:</label>
											<div class="col-md-7 input-group date">
												<input class="form-control" type="text" id="start_date_search" name="start_date_search" placeholder="Desde el" <?php if (isset($_SESSION['start_date_search_prfl'])) { ?>value="<?php echo $_SESSION['start_date_search_prfl']; ?>" <?php } ?> />
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<div class="col-md-3">
											<label class="col-md-5 control-label" for="end_date_search" style="padding-top:8px;">Hasta:</label>
											<div class="col-md-7 input-group date">
												<input class="form-control" type="text" id="end_date_search" name="end_date_search" placeholder="Hasta el" <?php if (isset($_SESSION['end_date_search_prfl'])) { ?>value="<?php echo $_SESSION['end_date_search_prfl']; ?>" <?php } ?> />
												<span class="input-group-addon">
													<i class="fa fa-th"></i>
												</span>
											</div>
										</div>
										<div class="col-md-6" style="text-align: right;">
											<button type="submit" class="btn btn-primary">Buscar <i class="icon-search"></i></button>
										</div>
									</div>
									<!-- Row END-->
								</div>
							</div>
						</form>
						<!-- // Filters END -->
					</div>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<!-- Modal actividades-->
					<div id="myModal" class="modal fade" role="dialog">
						<div class="modal-dialog modal-lg">

							<!-- Modal content de programacion de las actividades-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Actividades para esta sesión</h4>
								</div>
								<div class="modal-body">
									<table class="table table-bordered" id="tabla_actividades">
										<thead>
											<tr>
												<th>Actividad</th>
												<th>Fecha inicio</th>
												<th>Fecha fin</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
								<div class="modal-footer">
									<button id="btn_fech_act" type="button" class="btn btn-success">Programar</button>
								</div>
							</div>

						</div>
					</div>
					<!--/modal  -->
					<!-- Modal token-->
					<div id="modalToken" class="modal fade" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Establecer sesion_id y Token para la trasmisión de los VCT</h4>
								</div>
								<div class="modal-body">
									<form id="frmToken">
										<div class="form-group">
											<label for="session_id">Sesion ID</label>
											<input type="text" class="form-control" id="session_id">
											<!-- <small id="session_id" name="session_id" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
										</div>
										<div class="form-group">
											<label for="token">Token</label>
											<textarea id="token" name="token" class="col-md-12 form-control" rows="5"></textarea>
										</div>
										<button type="submit" class="btn btn-success">Enviar</button>
									</form>
								</div>
								<div class="modal-footer">
								</div>
							</div>

						</div>
					</div>
					<!--/modal  -->
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/programaciones.js?var=<?php echo rand(0, 999); ?>"></script>
</body>

</html>