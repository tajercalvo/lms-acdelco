<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}
}
$location = 'home';
$CalendarData = true;
?>
<?php include('controllers/libro.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/encuesta.css" />
</head>
<body class="document-body ">


	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>



				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Detalle de Libro</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- Camino de Hormigas -->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">
								<div class="col-md-12 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
									<div class="innerAll">
										<div class="body">
											<div class="row padding">
												<?php if($_GET['id']!="3" && $_GET['id']!="4"){?>
												<div class="col-md-1">
													<!--<a href="#" class="glyphicons hand_up" onclick="return false;"><i></i> Arrastra las esquinas</a>-->
												</div>
												<?php } ?>
												<div class="<?php if($_GET['id']!="3"){?>col-md-10<?php }else{ ?>col-md-12<?php } ?>">
													<div id="flipbook">
														<?php
														$var_cont = 1;
														foreach ($datoLibro as $key => $value) {
															if($var_cont%2==0){?>
															<div>
																<img src="../assets/libros/<?php echo($value['image']); ?>" class="img-responsive">
																<div class="row">
																	<div class="col-md-3 padding-none border-none margin-none">
																		<a href="#" class="text-success glyphicons hand_up animated flash" onclick="return false;">Arrastra aquí<i></i></a>
																	</div>
																	<div class="col-md-9"></div>
																</div>
															</div>
															<?php }else{ ?>
															<div>
																<img src="../assets/libros/<?php echo($value['image']); ?>" class="img-responsive">
																<div class="row">
																	<div class="col-md-9"></div>
																	<div class="col-md-3 padding-none border-none margin-none">
																		<a href="#" class="text-success glyphicons hand_up animated flash" onclick="return false;">Arrastra aquí<i></i></a>
																	</div>
																</div>
															</div>
															<?php } ?>
														<?php $var_cont++;} ?>
													</div>
												</div>
												<?php if($_GET['id']!="3" && $_GET['id']!="4"){?>
													<div class="col-md-1">
														<!--<a href="#" class="glyphicons hand_up" onclick="return false;"><i></i> Arrastra las esquinas</a>-->
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
<script type="text/javascript">
function loadApp() {
	<?php if($_GET['id']=="3"){?>
		$("#flipbook").turn({
			width: 1450,
			height: 600,
			autoCenter: true
		});
	<?php }else if($_GET['id']=="4"){ ?>
		$("#flipbook").turn({
			width: 1450,
			height: 500,
			autoCenter: true
		});
	<?php }else{?>
		$("#flipbook").turn({
			width: 1100,
			height: 600,
			autoCenter: true
		});
	<?php } ?>
	// Create the flipbook
	/*$('.flipbook').turn({
			// Width
			width:922,
			// Height
			height:600,
			// Elevation
			elevation: 50,
			// Enable gradients
			gradients: true,
			// Auto center this flipbook
			autoCenter: true
	});*/
}
// Load the HTML4 version if there's not CSS transform
yepnope({
	test : Modernizr.csstransforms,
	yep: ['js/turnjs4/lib/turn.js'],
	nope: ['js/turnjs4/lib/turn.html4.min.js'],
	both: ['js/turnjs4/samples/basic/css/basic.css'],
	complete: loadApp
});
</script>
</body>
</html>
