<?php include('src/seguridad.php'); ?>
<?php 
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/club_lideres.php');
$location = 'club';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="club_lideres.php" class="glyphicons cup"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Club de líderes</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
				<!-- Sección club de líderes -->


<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
	<div class="widget-body padding-none border-none">
		<div class="row">
			<div class="col-md-1 detailsWrapper">
			</div>
			<div class="col-md-10 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
			<img align="center" src="../assets/gallery/banners/ClubLideres/bnclub_top.jpg" alt="" class="img-responsive padding-none border-none" />
				<div class="innerAll">
					<div class="body">
						<div class="row padding-none">
							
						</div>
						<div class="row padding-none">
							<div class="col-md-12">
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-1 center">
									</div>
									<div class="col-md-10">
										<p style="font-size: 18px;"><b>¡TE INVITAMOS A VER LA SIGUIENTE INFOGRAFÍA!</b></p>
									</div>
									<div class="col-md-1 center">
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-1 center">
									</div>
									<div class="col-md-10 center">
										<div id="banner-club" class="owl-carousel owl-theme">
											<div class="item">
												<img src="../assets/gallery/banners/ClubLideres/CL1.jpg" alt="Paso 1" class="img-responsive padding-none border-none"/>
											</div>
											<div class="item">
												<img src="../assets/gallery/banners/ClubLideres/CL2.jpg" alt="Paso 2" class="img-responsive padding-none border-none"/>
											</div>
											<div class="item">
												<img src="../assets/gallery/banners/ClubLideres/CL3.jpg" alt="Paso 3" class="img-responsive padding-none border-none"/>
											</div>
											<div class="item">
												<img src="../assets/gallery/banners/ClubLideres/CL4.jpg" alt="Paso 4" class="img-responsive padding-none border-none"/>
											</div>
											<div class="item">
												<img src="../assets/gallery/banners/ClubLideres/CL5.jpg" alt="Paso 5" class="img-responsive padding-none border-none"/>
											</div>
											<div class="item">
												<img src="../assets/gallery/banners/ClubLideres/CL6.jpg" alt="Paso 6" class="img-responsive padding-none border-none"/>
											</div>
											
										</div>
									</div>
									<div class="col-md-1 center">
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-1 center">
									</div>
									<div class="col-md-10 center">
										<a href="../assets/gallery/banners/ClubLideres/PresentacionFinalClubLideres.pptx">
											<img src="../assets/gallery/banners/ClubLideres/bnclub.jpg" alt="" class="img-responsive padding-none border-none" />
										</a>
									</div>
									<div class="col-md-1 center">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

				<!-- Sección club de líderes -->
				</div>	
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/clublideres.js"></script>
</body>
</html>