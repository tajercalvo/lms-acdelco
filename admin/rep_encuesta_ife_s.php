<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_encuesta_ife_s.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_encuesta_ife_s.php">Reporte de Encuestas IFE</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Encuestas IFE</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de las encuestas y el rango de fechas.</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<!--<h5><a href="rep_encuesta_ife_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>&specialty_id=<?php echo($_POST['specialty_id']); ?>&supplier_id=<?php echo($_POST['supplier_id']); ?>&dealer_id=<?php echo($_POST['dealer_id']); ?>&zone_id=<?php echo($_POST['zone_id']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>-->
				<?php } ?>
			</div>
		</div>
		<form action="rep_encuesta_ife_s.php" method="post" id="formulario">
			<div class="row">
				<div class="col-md-6">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="specialty_id" style="padding-top:8px;">Especialidad:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="specialty_id" name="specialty_id" >
									<option value="0" <?php if(isset($_POST['specialty_id']) && ($_POST['specialty_id']==0) ){ ?>selected="selected"<?php } ?>>Todas</option>
								<?php foreach ($datosEspecialidades as $key => $Data_Cursos) { ?>
									<option value="<?php echo($Data_Cursos['specialty_id']); ?>" <?php if(isset($_POST['specialty_id']) && ($_POST['specialty_id']==$Data_Cursos['specialty_id']) ){ ?>selected="selected"<?php } ?>><?php echo($Data_Cursos['specialty']); ?></option>
								<?php } ?>
							</select>
						</div>
						
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-6">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="supplier_id" style="padding-top:8px;">Proveedor:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="supplier_id" name="supplier_id" >
									<option value="0" <?php if(isset($_POST['supplier_id']) && ($_POST['supplier_id']==0) ){ ?>selected="selected"<?php } ?>>Todos</option>
								<?php foreach ($datosProveedores as $key => $Data_Zona) { ?>
									<option value="<?php echo($Data_Zona['supplier_id']); ?>" <?php if(isset($_POST['supplier_id']) && ($_POST['supplier_id']==$Data_Zona['supplier_id']) ){ ?>selected="selected"<?php } ?>><?php echo($Data_Zona['supplier']); ?></option>
								<?php } ?>
							</select>
						</div>
						
					</div>
					<!-- // Group END -->
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-6">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="dealer_id" style="padding-top:8px;">Concesionario:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="dealer_id" name="dealer_id" >
									<option value="0" <?php if(isset($_POST['dealer_id']) && ($_POST['dealer_id']==0) ){ ?>selected="selected"<?php } ?>>Todos</option>
								<?php foreach ($datosConcesionarios as $key => $Data_Cursos) { ?>
									<option value="<?php echo($Data_Cursos['dealer_id']); ?>" <?php if(isset($_POST['dealer_id']) && ($_POST['dealer_id']==$Data_Cursos['dealer_id']) ){ ?>selected="selected"<?php } ?>><?php echo($Data_Cursos['dealer']); ?></option>
								<?php } ?>
							</select>
						</div>
						
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-6">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="zone_id" style="padding-top:8px;">Zona:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="zone_id" name="zone_id" >
									<option value="0" <?php if(isset($_POST['zone_id']) && ($_POST['zone_id']==0) ){ ?>selected="selected"<?php } ?>>Todas</option>
								<?php foreach ($datosZonas as $key => $Data_Zona) { ?>
									<option value="<?php echo($Data_Zona['zone_id']); ?>" <?php if(isset($_POST['zone_id']) && ($_POST['zone_id']==$Data_Zona['zone_id']) ){ ?>selected="selected"<?php } ?>><?php echo($Data_Zona['zone']); ?></option>
								<?php } ?>
							</select>
						</div>
						
					</div>
					<!-- // Group END -->
				</div>
			</div>
			<br>
			<div class="row">
			<div class="col-md-6">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="dealer_id" style="padding-top:8px;">Encuesta:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="review_id" name="review_id" >
									<option value="0" <?php if(isset($_POST['review_id']) && ($_POST['review_id']==0) ){ ?>selected="selected"<?php } ?>>Selecione una encuesta</option>
								<?php foreach ($datosEncuestas as $key => $Data_Encuesta) { ?>
									<option value="<?php echo($Data_Encuesta['review_id']); ?>" <?php if(isset($_POST['review_id']) && ($_POST['review_id']==$Data_Encuesta['review_id']) ){ ?>selected="selected"<?php } ?>><?php echo($Data_Encuesta['review']); ?></option>
								<?php } ?>
							</select>
						</div>
						
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-6">
					<!-- Group -->

					<!-- // Group END -->
				</div>
				
			</div>
			<br>
			<div class="row">
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
						<div class="col-md-7 input-group date">
					    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-5 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final:</label>
						<div class="col-md-7 input-group date">
					    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
					<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
				</div>
			</div>
		</form>
	</div>
</div>

<?php if(isset($cantidad_datos) && ($cantidad_datos>0) ){ ?>
	<div class="well">
		<?php foreach ($datosCursos_all as $iID => $data) { ?>
			<table class="table table-invoice">
				<tbody>
					<tr>
						<td style="width: 50%;">
							<span class="text-center">&nbsp; <i class='fa fa-fw fa-question-circle'></i> <?php echo($data['question']); ?></span><br><br>
							<table class="table table-condensed table-vertical-center table-thead-simple">
						        <!-- Table heading -->
						        <thead>
						            <tr>
						            	<td data-hide="phone,tablet" align="center"><b>AÑO</b></td>
						                <td data-hide="phone,tablet" align="center"><b>MES</b></td>
						                <td data-hide="phone,tablet" align="center"><b>CANT</b></td>
						                <td data-hide="phone,tablet" align="center"><b>1/6</b></td>
						                <td data-hide="phone,tablet" align="center"><b>7/8</b></td>
						                <td data-hide="phone,tablet" align="center"><b>9/10</b></td>
						                <td data-hide="phone,tablet" align="center"><b>IFE</b></td>
						                <td data-hide="phone,tablet" align="center"><b>OBJ</b></td>
						            </tr>
						        </thead>
						        <!-- // Table heading END -->
						        <tbody>
							        <?php 
							        foreach ($data['Perio'] as $iID => $dataP) { 
							        	$totalPer = 0;
							        	foreach ($data['Resp'] as $key => $ValueResp) {
							        		if( ($ValueResp['ANO'] == $dataP['ANO']) && ($ValueResp['MES'] == $dataP['MES']) ){
							        			$totalPer = $totalPer + $ValueResp['cantidad'];
							        		}
							        	}
							        	?>
					                    <tr>
					                    	<td align="center"><?php echo($dataP['ANO']); ?></td>
					                        <td align="center"><?php echo(GetMes($dataP['MES'])); ?></td>
					                        <td align="center"><?php echo($totalPer); //echo($_Var1_6_val + $_Var7_8_val + $_Var9_10_val); ?></td>
					                        <td align="center">
						                        <?php  
						                        $_Var1_6_val = 0;
						                        foreach ($data['Resp'] as $key => $ValueResp) {
						                        	if( ($ValueResp['ANO'] == $dataP['ANO']) && ($ValueResp['MES'] == $dataP['MES']) && ContieneVal($ValueResp['answer'],"1,2,3,4,5,6")  ){
						                        		$_Var1_6_val = $_Var1_6_val + $ValueResp['cantidad'];
						                        	}
						                        }
						                        if($totalPer>0){
						                        	echo($_Var1_6_val." - ".number_format($_Var1_6_val/$totalPer *100,2)." %" );
						                        }else{
						                        	echo("0 - 0 %");
						                        }
						                        ?>
					                        </td>
					                        <td align="center">
					                        	<?php  
						                        $_Var7_8_val = 0;
						                        foreach ($data['Resp'] as $key => $ValueResp) {
						                        	if( ($ValueResp['ANO'] == $dataP['ANO']) && ($ValueResp['MES'] == $dataP['MES']) && ContieneVal($ValueResp['answer'],"7,8")  ){
						                        		$_Var7_8_val = $_Var7_8_val + $ValueResp['cantidad'];
						                        	}
						                        }
						                        if($totalPer>0){
						                        	echo($_Var7_8_val." - ".number_format($_Var7_8_val/$totalPer *100,2)." %" );
						                        }else{
						                        	echo("0 - 0 %");
						                        }
						                        ?>
					                        </td>
					                        <td align="center">
					                        	<?php  
						                        $_Var9_10_val = 0;
						                        foreach ($data['Resp'] as $key => $ValueResp) {
						                        	if( ($ValueResp['ANO'] == $dataP['ANO']) && ($ValueResp['MES'] == $dataP['MES']) && ContieneVal($ValueResp['answer'],"9,10")  ){
						                        		$_Var9_10_val = $_Var9_10_val + $ValueResp['cantidad'];
						                        	}
						                        }
						                        if($totalPer>0){
						                        	echo($_Var9_10_val." - ".number_format($_Var9_10_val/$totalPer *100,2)." %" );
						                        }else{
						                        	echo("0 - 0 %");
						                        }
						                        ?>
					                        </td>
					                        <td align="center">
					                        <?php 
					                        if($totalPer>0){
					                        	$varPorcentaje = number_format($_Var9_10_val/$totalPer *100,2) - number_format($_Var1_6_val/$totalPer *100,2);
					                        	echo( number_format($_Var9_10_val/$totalPer *100,2) - number_format($_Var1_6_val/$totalPer *100,2) ." %" ); 
					                        }else{
					                        	echo(" 0 %"); 
					                        }
					                        ?>
					                        <input type="hidden" class="Question_Id<?php echo($data['question_id']); ?>_Val" name="<?php echo($dataP['ANO']); ?> <?php echo(GetMes($dataP['MES'])); ?>" value="<?php echo($varPorcentaje); ?>" />
					                        </td>
					                        <td align="center">
					                        <?php
					                        	if( strpos($dataP['ANO'], "2015") === false && strpos($dataP['ANO'].GetMes($dataP['MES']), "2016Ene") === false && strpos($dataP['ANO'].GetMes($dataP['MES']), "2016Feb") === false && strpos($dataP['ANO'].GetMes($dataP['MES']), "2016Mar") === false ){
					                        		echo("90 %");
					                        	}else{
					                        		echo("75 %");
					                        	} ?>
					                        </td>
					                    </tr>
					                <?php } ?>
						        </tbody>
						    </table>
						</td>
						<td style="width: 50%;">
							<span class="text-center">&nbsp; <i class='fa fa-fw fa-bar-chart-o'></i> Comportamiento Gráfico</span>
							<div id="GrafEnq_Gral_<?php echo($data['question_id']); ?>" class="flotchart-holder" style="height: 300px;"></div>
							<input type="hidden" class="ElementID" id="QuestionId_<?php echo($data['question_id']); ?>" name="QuestionId_<?php echo($data['question_id']); ?>" value="<?php echo($data['question_id']); ?>" />
							<!--<span class="text-center">&nbsp; <i class='fa fa-fw fa-bar-chart-o'></i> Comportamiento Detallado</span>
							<div id="GrafDetalle" class="flotchart-holder" style="height: 300px;"></div>-->
						</td>
					</tr>
				</tbody>
			</table>
		<?php } ?>
	</div>
<?php } ?>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_encuesta_ife_s.js"></script>
</body>
</html>