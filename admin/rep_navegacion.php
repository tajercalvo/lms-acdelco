<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_navegacion.php');
$location = 'reporting';
$locData = true;
$Asist = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons link"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_asistencia.php">Reporte Navegación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Navegación por Secciones </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Este es el reporte general de navegación en donde se encuentra la información de Personas Vs Secciones de la plataforma, es un reporte completo que tiene un nivel de entendimiento elevado y el recurso necesario para procesarlo también es elevado, sea muy consciente de su necesidad</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<h5><a href="rep_navegacion_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>&dealer= <?php echo($_POST['dealer']); ?>&charge= <?php echo($_POST['charge']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<form action="rep_navegacion.php" method="post" onsubmit="return validacion()">
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-3 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Hasta..." <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-3 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..." <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
					<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-3 control-label" style="padding-top:8px;">Trayectorias:</label>
						<div class="col-md-6 input-group date">
					    	<select style="width: 100%;" id="charge" name="charge">
					    	<option value=""></option>
							<?php foreach ($trayectorias as $key => $Data_ActiveCharge) { ?>
									<option value="<?php echo $Data_ActiveCharge['charge_id']; ?>" <?php if(isset($_POST['charge']) && ($_POST['charge']==$Data_ActiveCharge['charge_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $Data_ActiveCharge['charge']; ?></option>
								<?php } ?>
						</select>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-3 control-label" style="padding-top:8px;">Concesionario:</label>
						<div class="col-md-6 input-group date">
					    	<select style="width: 100%;" id="dealer" name="dealer">
					    	<option value=""></option>
							<?php  foreach ($concesionarios as $key => $value) { ?>
								<option value="<?php echo $value['dealer_id']; ?>" <?php if(isset($_POST['dealer']) && ($_POST['dealer']==$value['dealer_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $value['dealer']; ?></option>

							<?php  } ?>
						</select>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
					<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])){ if($cantidad_datos > 0){ ?>
	<div class="well">
		<table class="table table-invoice" >
			<tbody>
				<tr>
					<td class="center">
						<div class="col-md-11">
									<p class="lead">Navegación</p>
							</div>
							<div class="col-md-1">
								<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion">
									<p id="descargar_excel"><a href="#" class="glyphicons cloud-upload"><i></i></a></p>
									<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Navegación" />
									<input type="hidden" id="datos_tabla" name="datos_tabla" />
								</form>
							</div>

						
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple" id="tab_Navegacion">
							<thead>
								<tr>
									<th class="center">SECCIÓN</th>
									<th class="center">VISITAS</th>
									<th class="center">VISITANTES</th>
									<th class="center">TIEMPO TOTAL (seg)</th>
									<th class="center">PROMEDIO (seg)</th>
								</tr>
							</thead>
							<tbody id="cuerpo_tabla">
								<?php  
									$total_visitas = 0;
									$total_visitantes = 0;
									$total_tiempo = 0;
									foreach ($SeccionesDatos as $iID => $data) { 
										$Section = $data['section'];
										if($Section=='hojas.php'){ $Section = 'Hojas de Ruta'; }
										if($Section=='op_hoja.php'){ $Section = 'Operación Hojas de Ruta'; }
										$Section = str_replace('.php', '', $Section);
										$Section = str_replace('rep_', 'Reporte ', $Section);
										$Section = str_replace('op_', 'Operación ', $Section);
										$Section = str_replace('_detail', ' Detalle', $Section);
										$Section = str_replace('_', ' ', $Section);
										$Section = ucwords($Section);
										$total_visitas = $total_visitas + $data['cantidadVisitas'];
										$total_visitantes = $total_visitantes + $data['cantidadUsuarios'];
										$total_tiempo = $total_tiempo + $data['tiempoTotal'];
										?>
										<!-- Item -->
										<tr class="selectable">
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($Section); ?></td>										
											<input type="hidden" class="seccion_dat" id="seccion_dat" name="seccion_dat" value="<?php echo($Section); ?>" data-Vis="<?php echo($data['cantidadVisitas']); ?>" data-Usr="<?php echo($data['cantidadUsuarios']); ?>" data-TimeT="<?php echo(number_format($data['tiempoTotal']/60,2)); ?>" data-Prom="<?php echo(number_format($data['tiempoPromedio']/60,2)); ?>" />
											<td class="center important" style="padding: 2px; font-size: 80%;"><?php echo($data['cantidadVisitas']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($data['cantidadUsuarios']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(round($data['tiempoTotal'],3)); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo(round($data['tiempoPromedio'],3)); ?></td>
										</tr>
										<!-- // Item END -->
								<?php } ?>
										<tr class="selectable">
											<td class="center" style="padding: 2px; font-size: 90%;"><strong>RESUMEN</strong></td>
											<td class="center important" style="padding: 2px; font-size: 90%;"><strong><?php echo($total_visitas); ?></strong></td>
											<td class="center" style="padding: 2px; font-size: 90%;"><strong><?php echo($total_visitantes); ?> / <?php echo($VisitantesUn['cantidad']); ?></strong></td>
											<td class="center" style="padding: 2px; font-size: 90%;"><strong><?php echo(round($total_tiempo,3)); ?></strong></td>
											<td class="center" style="padding: 2px; font-size: 90%;"><strong><?php echo(round($total_tiempo/$total_visitantes,3)); ?></strong></td>
										</tr>
							</tbody>
						</table>
						<!-- // Table END -->
					</td>
				</tr>
			</tbody>
		</table>
	</div>
<?php } }?>


					<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day'])&& count($usuarios_iteracion) > 0){ ?>
					<div class="row row-app"></div>
					<div class="well">
					
						<table class="table table-invoice" >
			<tbody>
				<tr>
					<td class="center">
					<div class="col-md-11">
									<p class="lead">TODOS</p>
							</h5></div>
							<div class="col-md-1">
									<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion_TODOS">
									<p id="descargar_TODOS"><a href="#" class="glyphicons cloud-upload"><i></i></a></p>
									<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="TODOS" />
									<input type="hidden" id="datos_tabla2" name="datos_tabla" />
								</form>
							</div>
						<!-- <p class="lead">TODOS</p>  -->
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple" id="tab_TODOS">
							<thead>
								<tr>
									<th class="center">CONCESIONARIO</th>
									<th class="center">TOTAL USUARIOS </th>
									<th class="center">USUARIOS ÚNICOS CON ITERACCIÓN</th>
									<th class="center">PROMEDIO</th>
								</tr>
							</thead>
					
							<tbody >
									<?php if(count($usuarios_iteracion) > 0){ 
										//TODOS
										$total_usuarios_TODOS = 0;
										$USUARIOS_CON_ITERACCIÓN = 0;
										$Promedio_todos = 0;

										foreach ($usuarios_iteracion as $key => $value2) {

										//TODOS
										$total_usuarios_TODOS = $total_usuarios_TODOS + $value2['todos'];
										$USUARIOS_CON_ITERACCIÓN = $USUARIOS_CON_ITERACCIÓN + $value2['con_iteracion'];
										$Promedio_todos = $Promedio_todos + round(($value2['con_iteracion']/$value2['todos'])*100, 1);

										?>
										<!-- Item -->
										


										<tr class="selectable">
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value2['dealer']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value2['todos']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value2['con_iteracion']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo round(($value2['con_iteracion']/$value2['todos'])*100, 1); ?></td>
											
										</tr>


										<!-- // Item END -->	
										<?php } } ?>

										<tr class="selectable">
											<td class="center" style="padding: 2px; font-size: 90%;"><strong>RESUMEN</strong></td>
											<td class="center important" style="padding: 2px; font-size: 90%;"><strong><?php echo($total_usuarios_TODOS); ?></strong></td>
											<td class="center" style="padding: 2px; font-size: 90%;"><strong><?php echo($USUARIOS_CON_ITERACCIÓN); ?></strong></td>
											<td class="center" style="padding: 2px; font-size: 90%;"><strong><?php echo(round($Promedio_todos,1)); ?></strong></td>
										</tr>


							</tbody>
						</table>
						<!-- // Table END -->
					</td>
				</tr>
			</tbody>
		</table>
					
					</div>
					<?php }?>

						<?php if(isset($_POST['end_date_day'])&&isset($_POST['start_date_day']) && count($usuarios_iteracion_GM) > 0){//echo count($usuarios_iteracion_GM);?>
					<div class="row row-app"></div>
					<div class="well">
					
						<table class="table table-invoice" >
			<tbody>
				<tr>
					<td class="center">
						<div class="col-md-11">
									<p class="lead">Solo cargos GMAcademy</p>
							</div>
							<div class="col-md-1">
									<div class="col-md-1">
								<form action="ficheroExcel.php" method="post" target="_blank" id="FormularioExportacion_tab_GMAcademy">
									<p id="descargar_excel_tab_GMAcademy"><a href="#" class="glyphicons cloud-upload"><i></i></a></p>
									<input type="hidden" id="nombre_archivo" name="nombre_archivo" value="Solo cargos GMAcademy" />
									<input type="hidden" id="datos_tabla3" name="datos_tabla" />
								</form>
							</div>
							</div>
						
						<!-- // Table -->
						<table class="table table-condensed table-vertical-center table-thead-simple" id="tab_GMAcademy">
							<thead>
								<tr>
									<th class="center">CONCESIONARIO</th>
									<th class="center">TOTAL USUARIOS </th>
									<th class="center">USUARIOS ÚNICOS CON ITERACCIÓN</th>
									<th class="center">PROMEDIO</th>
								</tr>
							</thead>
					
							<tbody >
									<?php

										//TODOS_GMAcademy
										$total_usuarios_TODOS_GMAcademy = 0;
										$USUARIOS_CON_ITERACCIÓN_GMAcademy = 0;
										$Promedio_todos_GMAcademy = 0;


									 foreach ($usuarios_iteracion_GM as $key => $value) {

									 	//TODOS_GMAcademy
										$total_usuarios_TODOS_GMAcademy = $total_usuarios_TODOS_GMAcademy + $value['todos'];
										$USUARIOS_CON_ITERACCIÓN_GMAcademy = $USUARIOS_CON_ITERACCIÓN_GMAcademy + $value['con_iteracion'];
										$Promedio_todos_GMAcademy = $Promedio_todos_GMAcademy + round(($value['con_iteracion']/$value['todos'])*100, 1);


										?>
										<!-- Item -->
										<tr class="selectable">
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value['dealer']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value['todos']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo($value['con_iteracion']); ?></td>
											<td class="center" style="padding: 2px; font-size: 80%;"><?php echo round(($value['con_iteracion']/$value['todos'])*100, 1); ?></td>
										</tr>
										<!-- // Item END -->	
										<?php } ?>
										<tr class="selectable">
											<td class="center" style="padding: 2px; font-size: 90%;"><strong>RESUMEN</strong></td>
											<td class="center important" style="padding: 2px; font-size: 90%;"><strong><?php echo($total_usuarios_TODOS_GMAcademy); ?></strong></td>
											<td class="center" style="padding: 2px; font-size: 90%;"><strong><?php echo($USUARIOS_CON_ITERACCIÓN_GMAcademy); ?></strong></td>
											<td class="center" style="padding: 2px; font-size: 90%;"><strong><?php echo(round($Promedio_todos_GMAcademy,1)); ?></strong></td>
										</tr>
							</tbody>
						</table>
						<!-- // Table END -->
					</td>
				</tr>
			</tbody>
		</table>
					
					</div>
					<?php }?>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_navegacion.js"></script>
</body>
</html>