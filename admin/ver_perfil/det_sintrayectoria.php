<?php include('src/seguridad_in.php'); ?>
<!-- Products table -->
<table id="TableData" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
	<thead>
		<tr>
			<th style="width: 1%;" class="center">C&oacute;digo</th>
			<th>Nombre</th>
			<th style="width: 1%;" class="center">Tipo</th>
			<th class="center">Fecha</th>
			<th class="center">Resultado</th>
			<th class="center" style="width: 100px;">Certificado</th>
		</tr>
	</thead>
	<tbody>
		<?php if(isset($registros_sintr)){
			foreach ($registros_sintr as $key => $Data_Sintra) { ?>
		<tr class="selectable">
			<td class="important center"><?php echo $Data_Sintra['newcode']; ?></td>
			<td class=""><strong><?php echo $Data_Sintra['course']; ?></strong> Puntaje: <?php echo round($Data_Sintra['score'],0); ?></td>
			<td class="center"><?php echo($Data_Sintra['type']);//$Data_Traysel['date_creation']; ?></td>
			<td><?php echo($Data_Sintra['date_creation']);//$Data_Traysel['date_creation']; ?></td>
			<td class="center">
				<?php if($Data_Sintra['score'] >= 75){ ?>
					<span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
				<?php }else{ ?>
					<span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
				<?php } ?>
			</td>
			<td class="center">
				<?php if($Data_Sintra['score'] >= 75){ ?>
					<a href="certificado.php?course_id=<?php echo $Data_Sintra['course_id']; ?>&user_id=<?php echo $_GET['id']; ?>&date=<?php echo $Data_Sintra['date_creation']; ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-certificate"></i></a>
				<?php } ?>
			</td>
		</tr>
		<?php } } ?>
	</tbody>
</table>
<!-- // Products table END -->