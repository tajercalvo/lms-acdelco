<?php include('src/seguridad_in.php'); ?>
<div class="widget widget-tabs widget-tabs-gray">
	<div class="widget-head">
		<ul>
			<?php
			$idCharges_Auto = "";
			if (isset($_GET['id'])) {
				$idUser_Tray = $_GET['id'];
			} else {
				$idUser_Tray = $_SESSION['idUsuario'];
			}

			$cant_ele = 1;

			foreach ($charge_user as $key => $Data_Charges) { ?>
				<li <?php if ($cant_ele == 1) { ?>class="active" <?php } ?>><a href="#search-trayectoria<?php echo $cant_ele; ?>" data-toggle="tab"><?php echo $cant_ele; ?></a></li>
			<?php
				$cant_ele++;
			} ?>
			<li><a href="#search-sintrayectoria" data-toggle="tab">Sin Trayectoria</a></li>
		</ul>
	</div>
	<div class="widget-body">
		<div class="tab-content">
			<?php
			$cant_ele = 1;
			foreach ($charge_user as $key => $Data_Charges) {
				$idCharge_Tray = $Data_Charges['charge_id'];
				$idCharges_Auto .= $idCharge_Tray . ",";
			?>
				<div class="tab-pane <?php if ($cant_ele == 1) { ?>active<?php } ?>" id="search-trayectoria<?php echo $cant_ele; ?>">
					<div class="widget widget-heading-simple widget-body-simple text-right">
						<form class="input-group">
							<input type="text" class="form-control" placeholder="Buscar curso en esta trayectoria ... " />
							<span class="input-group-btn"><button type="submit" class="btn btn-inverse">Buscar</button></span>
						</form>
					</div>
					<?php include('controllers/det_trayectoria.php'); ?>
					<?php

					// NIVELES LMS ACDELCO
					// NIVEL 1 ESPECIALISTA
					// NIVEL 2 EXPERTO
					// NIVEL 3 TÉCNICO
					// NIVEL 4 PRINCIPIANTE

					$cant_Oro_Pos = "0";
					$cant_Oro_Tom = "0";
					$cant_Plata_Pos = "0";
					$cant_Plata_Tom = "0";
					$cant_Bronce_Pos = "0";
					$cant_Bronce_Tom = "0";
					$cant_Esp_Tom = "0";
					$cant_Esp_Pos = "0";
					/* ---------comun -----------------*/
					$cant_Comun_Tom = "0";
					$cant_Comun_Pos = "0";

					foreach ($registros_cant as $key => $DataTom) {
						if ($DataTom['level_id'] == "1") {
							$cant_Esp_Tom = $DataTom['cantidad'];
						} elseif ($DataTom['level_id'] == "2") {
							$cant_Oro_Tom = $DataTom['cantidad'];
						} elseif ($DataTom['level_id'] == "3") {
							$cant_Plata_Tom = $DataTom['cantidad'];
						} elseif ($DataTom['level_id'] == "4") {
							$cant_Bronce_Tom = $DataTom['cantidad'];
						} /* ---------levet para comun------------ */
						 elseif ($DataTom['level_id'] == "5") {
							$cant_Comun_Tom = $DataTom['cantidad'];
						}
					}

					foreach ($registros_clas as $key => $DataPos) {
						if ($DataPos['level_id'] == "1") {
							$cant_Esp_Pos = $DataPos['cantidad'];
						} elseif ($DataPos['level_id'] == "2") {
							$cant_Oro_Pos = $DataPos['cantidad'];
						} elseif ($DataPos['level_id'] == "3") {
							$cant_Plata_Pos = $DataPos['cantidad'];
						} elseif ($DataPos['level_id'] == "4") {
							$cant_Bronce_Pos = $DataPos['cantidad'];
						}/* cantidad de registros comun registros comun----------- */
						 elseif ($DataPos['level_id'] == "5") {
							$cant_Comun_Pos = $DataPos['cantidad'];
						}

					}

					$data_slide_oro = "0";
					$data_slide_plata = "0";
					$data_slide_bronce = "0";
					$data_slide_especialista = "0";
					/* data de comun variable */
					$data_slide_comun = "0";

					if ($cant_Esp_Pos > 0 && $cant_Esp_Tom > 0 && $cant_Esp_Pos >= $cant_Esp_Tom) {
						$data_slide_especialista = ($cant_Esp_Tom / $cant_Esp_Pos) * 100;
						$data_slide_especialista = round($data_slide_especialista, 0);
					}

					if ($cant_Oro_Pos > 0 && $cant_Oro_Tom > 0 && $cant_Oro_Pos >= $cant_Oro_Tom) {
						$data_slide_oro = ($cant_Oro_Tom / $cant_Oro_Pos) * 100;
						$data_slide_oro = round($data_slide_oro, 0);
					}
					if ($cant_Plata_Pos > 0 && $cant_Plata_Tom > 0 && $cant_Plata_Pos >= $cant_Plata_Tom) {
						$data_slide_plata = ($cant_Plata_Tom / $cant_Plata_Pos) * 100;
						$data_slide_plata = round($data_slide_plata, 0);
					}
					if ($cant_Bronce_Pos > 0 && $cant_Bronce_Tom > 0 && $cant_Bronce_Pos >= $cant_Bronce_Tom) {
						$data_slide_bronce = ($cant_Bronce_Tom / $cant_Bronce_Pos) * 100;
						$data_slide_bronce = round($data_slide_bronce, 0);
					}
					/* validamos el total de data para el comun-------------- */
					if ($cant_Comun_Pos > 0 && $cant_Comun_Tom > 0 && $cant_Comun_Pos >= $cant_Comun_Tom) {
						$data_slide_comun = ($cant_Comun_Tom / $cant_Comun_Pos) * 100;
						$data_slide_comun = round($data_slide_comun, 0);
					}


					/* Sumsmos la catidad de cada uno de actividades */
					$data_total_Tom = $cant_Oro_Tom + $cant_Plata_Tom + $cant_Bronce_Tom + $cant_Esp_Tom + $cant_Comun_Tom;
					$data_total_Pos = $cant_Oro_Pos + $cant_Plata_Pos + $cant_Bronce_Pos + $cant_Esp_Pos + $cant_Comun_Pos;
					$avanceTray = 0;
					if ($data_total_Tom > 0) {
						$avanceTray = round(($data_total_Tom / $data_total_Pos) * 100, 0);
					}
					?>
					<div class="widget widget-heading-simple widget-body-white margin-none">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-3 center">
									<div class="row">
										<input type="hidden" value="<?php echo ($avanceTray); ?>" name="ResTray<?php echo ($idCharge_Tray); ?>" id="ResTray<?php echo ($idCharge_Tray); ?>">
										<input type="hidden" value="<?php echo ($data_total_Tom); ?>" name="CursosAprobados<?php echo ($idCharge_Tray); ?>" id="CursosAprobados<?php echo ($idCharge_Tray); ?>">
										<img src="../assets/images/distinciones/principiante.png" style="width: 70px;" alt="junior"><br>
									</div>
									<div class="row">
										<strong class="text-warning">Principiante</strong><br>
										<?php echo ($cant_Bronce_Tom . '/' . $cant_Bronce_Pos); ?>
									</div>
									<div class="row">
										<div class="slider-range-min row form-horizontal">
											<div class="col-md-12" style="padding-top: 9px;">
												<div class="slider slider-inverse" data-slide="<?php echo ($data_slide_bronce); ?>"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 center">
									<div class="row">
										<img src="../assets/images/distinciones/tecnico.png" style="width: 70px;" alt="senior"><br>
									</div>
									<div class="row">
										<strong class="text-default">Técnico</strong><br>
										<?php echo ($cant_Plata_Tom . '/' . $cant_Plata_Pos); ?>
									</div>
									<div class="row">
										<div class="slider-range-min row form-horizontal">
											<div class="col-md-12" style="padding-top: 9px;">
												<div class="slider slider-default" data-slide="<?php echo ($data_slide_plata); ?>"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 center">
									<div class="row">
										<img src="../assets/images/distinciones/experto.png" style="width: 70px;" alt="master"><br>
									</div>
									<div class="row">
										<strong class="text-primary">Experto</strong><br>
										<?php echo ($cant_Oro_Tom . '/' . $cant_Oro_Pos); ?>
									</div>
									<div class="row">
										<div class="slider-range-min row form-horizontal">
											<div class="col-md-12" style="padding-top: 9px;">
												<div class="slider slider-primary" data-slide="<?php echo ($data_slide_oro); ?>"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-3 center">
									<div class="row">
										<img src="../assets/images/distinciones/especialista.png" style="width: 70px;" alt="master"><br>
									</div>
									<div class="row">
										<strong class="text-primary">Especialista</strong><br>
										<?php echo ($cant_Esp_Tom . '/' . $cant_Esp_Pos); ?>
									</div>
									<div class="row">
										<div class="slider-range-min row form-horizontal">
											<div class="col-md-12" style="padding-top: 9px;">
												<div class="slider slider-primary" data-slide="<?php echo ($data_slide_especialista); ?>"></div>
											</div>
										</div>
									</div>
								</div>

								<!-- NIVEL COMUN -->
								<div class="col-md-3 center">
									<div class="row">
										<img src="../assets/images/distinciones/comun.png" style="width: 70px;" alt="master"><br>
									</div>
									<div class="row">
										<strong class="text-primary">Comun</strong><br>
										<?php echo ($cant_Comun_Tom . '/' . $cant_Comun_Pos); ?>
									</div>
									<div class="row">
										<div class="slider-range-min row form-horizontal">
											<div class="col-md-12" style="padding-top: 9px;">
												<div class="slider slider-primary" data-slide="<?php echo ($data_slide_comun); ?>"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- FIN NIVEL COMUN -->




							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white margin-none">
						<div class="widget-body">
							<h5 class="text-uppercase strong separator bottom">Cursos (<?php echo ($Data_Charges['charge']); ?>)</h5>
							<?php
							include('ver_perfil/det_trayectoria.php');
							?>
						</div>
					</div>
				</div>
			<?php
				$cant_ele++;
			} ?>
			<div class="tab-pane" id="search-sintrayectoria">
				<div class="widget widget-heading-simple widget-body-simple text-right">
					<form class="input-group">
						<input type="text" class="form-control" placeholder="Buscar curso autodirigido ... " />
						<span class="input-group-btn"><button type="submit" class="btn btn-inverse">Buscar</button></span>
					</form>
				</div>
				<div class="widget widget-heading-simple widget-body-white margin-none">
					<div class="widget-body">
						<div class="row">
							<!-- imagen de isn trayectoria... -->
							<div class="col-md-12 center">
								<div class="row">
									<!-- <img src="../assets/images/distinciones/especialista.png" style="width: 70px;" alt="Oro"><br>
									<strong class="text-primary">Especialista</strong><br> -->
								</div>
							</div>
						</div>
						<h5 class="text-uppercase strong separator bottom">Cursos Sin trayectoria</h5>
						<?php
						$idCharges_Auto .= "0";
						include('ver_perfil/det_sintrayectoria.php');
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>