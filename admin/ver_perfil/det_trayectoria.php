<!-- Products table -->
<table id="TableData" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
	<thead>
		<tr>
			<th style="width: 1%;" class="center">C&oacute;digo</th>
			<th>Nombre</th>
			<th style="width: 1%;" class="center">Tipo</th>
			<th class="center">Nivel</th>
			<th class="center">Fecha</th>
			<th class="center">Resultado</th>
			<th class="center" style="width: 100px;">Certificado</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$var_CantNotas = 0;
		$var_SumNotas = 0;
		$var_PromNotas = 0;
		foreach ($registros_tray as $key => $Data_Traysel) {
			$var_CantNotas++;
			$var_SumNotas = $var_SumNotas + $Data_Traysel['score'];
		?>
			<tr class="selectable">
				<td class="important center"><?php echo $Data_Traysel['newcode']; ?></td>
				<td class=""><strong><?php echo $Data_Traysel['course']; ?></strong></td>
				<td class="center"><?php echo $Data_Traysel['type']; ?></td>
				<td class="center">
					<?php if ($Data_Traysel['level_id'] == "1") { ?>
						<img src="../assets/images/distinciones/especialista.png" style="width: 30px;" alt="ESPECIALISTA"><br><span class="text-warning" style="font-size: 10px;">ESPECIALISTA</span>
					<?php } elseif ($Data_Traysel['level_id'] == "2") { ?>
						<img src="../assets/images/distinciones/experto.png" style="width: 30px;" alt="EXPERTO"><br><span class="text-default" style="font-size: 10px;">EXPERTO</span>
					<?php } elseif ($Data_Traysel['level_id'] == "3") { ?>
						<img src="../assets/images/distinciones/tecnico.png" style="width: 30px;" alt="TÉCNICO"><br><span class="text-secundary" style="font-size: 10px;">TÉCNICO</span>
					<?php } elseif ($Data_Traysel['level_id'] == "4") { ?>
						<img src="../assets/images/distinciones/principiante.png" style="width: 30px;" alt="PRINCIPIANTE"><br><span class="text-primary" style="font-size: 10px;">PRINCIPIANTE</span>
					<?php }elseif($Data_Traysel['level_id'] == "5"){ ?>
						<img src="../assets/images/distinciones/comun.png" style="width: 30px;" alt="COMÚN"><br><span class="text-primary" style="font-size: 10px;">COMÚN</span>
						<?php } ?>
				</td>
				<td><?php echo ($Data_Traysel['date_creation']); //$Data_Traysel['date_creation']; 
						?></td>
				<td class="center">
					<?php if ($Data_Traysel['score'] >= 75) { ?>
						<strong class="text-success" style="font-size: 20px;"><?php echo round($Data_Traysel['score'], 0); ?> </strong><span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
					<?php } else { ?>
						<strong class="text-danger" style="font-size: 20px;"><?php echo round($Data_Traysel['score'], 0); ?> </strong><span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
					<?php } ?>
				</td>
				<td class="center">
					<?php if ($Data_Traysel['score'] >= 75) { ?>
						<a href="certificado.php?course_id=<?php echo $Data_Traysel['course_id']; ?>&date=<?php echo $Data_Traysel['date_creation']; ?>&user_id=<?php echo $_GET['id']; ?>" target="_blank" class="btn btn-sm btn-info"><i class="fa fa-certificate"></i></a>
					<?php } ?>
				</td>
			</tr>
		<?php }
		if ($var_SumNotas > 0) {
			$var_PromNotas = round($var_SumNotas / $var_CantNotas, 0);
		}
		?>
		<input type="hidden" value="<?php echo ($var_PromNotas); ?>" name="PromedioNotas<?php echo ($idCharge_Tray); ?>" id="PromedioNotas<?php echo ($idCharge_Tray); ?>">
	</tbody>
</table>
<!-- // Products table END -->