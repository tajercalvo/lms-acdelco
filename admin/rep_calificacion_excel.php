<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_calificacion.php');
}
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_DiasCalificacion.csv');
if(isset($_GET['start_date_day'])){
    echo utf8_decode("INICIA;FINALIZA;TAMANO;LUGAR;CÓDIGO;CURSO;PROVEEDOR;PROFESOR;IDENTIFICACIÓN;CALIFICACIÓN;DIAS;MES CURSO;MES CALIFICACIÓN;\n");
    if($cantidad_datos > 0){ 
        foreach ($datosCursos_all as $iID => $data) { 
            echo(utf8_decode($data['start_date'].";".$data['end_date'].";".$data['min_size']."-".$data['max_size'].";".$data['living'].";".$data['newcode'].";".$data['course'].";".$data['supplier'].";".$data['first_name'].' '.$data['last_name'].";".$data['identification'].";".$data['fecCalif'].";".$data['DiasCalif'].";".$data['montCourse'].";".$data['montCalif']."\n"));
        }
    }
}