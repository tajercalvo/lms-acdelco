<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['course_id'])){
    include('controllers/rep_pendientescurso.php');
}
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_PendientesCurso.xls");
if(isset($_GET['course_id'])){
    ?>
    <table>
        <!-- Table heading -->
        <thead>
            <tr>
                <th data-hide="phone,tablet">C&Oacute;DIGO</th>
                <th data-hide="phone,tablet">CURSO</th>
                <th data-hide="phone,tablet">TIPO</th>
                <th data-hide="phone,tablet">CONCESIONARIO</th>
                <th data-hide="phone,tablet">SEDE</th>
                <th data-hide="phone,tablet">ALUMNO</th>
                <th data-hide="phone,tablet">IDENTIFICACI&Oacute;N</th>
                <th data-hide="phone,tablet">CARGOS</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            <?php 
            if($cantidad_datos > 0){ 
                    foreach ($datosCursos_all as $iID => $data) { 
                        ?>
                <tr>
                    <td><?php echo(utf8_decode($data['newcode'])); ?></td>
                    <td><?php echo(utf8_decode($data['course'])); ?></td>
                    <td><?php echo($data['type']); ?></td>
                    <td><?php echo(utf8_decode($data['dealer'])); ?></td>
                    <td><?php echo(utf8_decode($data['headquarter'])); ?></td>
                    <td><?php echo(utf8_decode($data['first_name'].' '.$data['last_name'])); ?></td>
                    <td><?php echo($data['identification']); ?></td>
                    <td>
                        <? foreach ($data['cargos'] as $iID => $data_c) { 
                            echo $data_c['charge'].' - ';
                        } ?>
                    </td>
                </tr>
            <?php } } ?>
        </tbody>
    </table>
<?php } ?>