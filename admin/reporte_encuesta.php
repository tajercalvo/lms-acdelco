<?php include('src/seguridad.php'); ?>
<?php include('controllers/reporte_encuesta.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!-->
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_encuesta.php">Reporte de Encuestas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Encuestas</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-12">
									<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de las encuestas.</h5></br>
								</div>
							</div>
							<div class="separator bottom"></div>
							<div class="row">
								<form action="reporte_encuesta.php" method="post">

									<div class="col-md-10">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="encuesta_id" style="padding-top:8px;">Encuesta:</label>
											<div class="col-md-10 input-group date">
												<select style="width: 100%;" id="encuesta_id" name="encuesta_id">
													<?php foreach ($encuestas as $key => $val) { ?>
														<option value="<?php echo $val['encuesta_id']; ?>" <?php if (isset($_POST['encuesta_id']) && ($_POST['encuesta_id'] == $val['encuesta_id'])) { ?> selected="selected" <?php } ?>><?php echo $val['encuesta']; ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-1">
										<button type="submit" class="btn btn-success" name="opcn" value="getEncuestas"><i class="fa fa-search"></i> Consultar</button>
									</div>
									<div class="col-md-1">
										<button type="submit" class="btn btn-warning" name="opcn" value="descargar"><i class="fa fa-download"></i> Descargar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php if( isset($reporte)){ ?>
					<div class="box-body no-padding">
						<div class="row">
							<div class="col-md-12">
								<div style="height: auto;">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Identificación</th>
												<th>Nombre</th>
												<th>Sede</th>
												<th>Empresa</th>
												<th>Encuesta</th>
												<th>Pregunta</th>
												<th>Respuesta</th>
												<th>Fecha de respuesta</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($reporte as $key => $value) { ?>
												<tr>
													<td><?php echo $value['identification'] ?></td>
													<td><?php echo $value['first_name'] . ' ' . $value['last_name'] ?></td>
													<td><?php echo $value['headquarter'] ?></td>
													<td><?php echo $value['dealer'] ?></td>
													<td><?php echo $value['encuesta'] ?></td>
													<td><?php echo $value['encuesta_pregunta'] ?></td>
													<td><?php echo $value['puntaje'] ?></td>
													<td><?php echo $value['creacion'] ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<?php }?>
					<!-- // END Nuevo ROW-->
					<div class="separator bottom"></div>
					<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/reporte_encuesta.js"></script>
</body>

</html>