<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;

try {
    ob_start();
    include 'certificado_source.php';
    $content = ob_get_clean();

    $html2pdf = new Html2Pdf('L', 'A4', 'es', true, 'UTF-8', array(5, 10, 5, 5));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->pdf->SetAuthor('Ludus Learning Management System');
    $html2pdf->pdf->SetCreator('Ludus Learning Management System');
    $html2pdf->pdf->SetTitle('Certificado de Estudios - AutoTrain LMS');
    $html2pdf->pdf->SetSubject('Certificado de Estudios - AutoTrain LMS');
    $html2pdf->pdf->SetMargins(0, 0, 0, true);
    $html2pdf->pdf->setPrintHeader(false);
    $html2pdf->pdf->SetFooterMargin(0);
    $html2pdf->pdf->setPrintFooter(false);
    $html2pdf->pdf->SetAutoPageBreak(TRUE, 0);
    $html2pdf->pdf->SetLeftMargin(0);
    $html2pdf->pdf->SetTopMargin(0);
    
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('Certificado_ACDelco.pdf');
} catch (Html2PdfException $e) {
    $html2pdf->clean();
}



/* require_once('vendor/tecnickcom/tcpdf/tcpdf.php');
$pdf = new TCPDF('L', 'mm', 'A4', true, 'UTF-8');
$pdf->SetMargins(5, 10, 5, 5);
$pdf->AddPage();
$pdf->SetDisplayMode('fullpage');
$pdf->SetAuthor('Ludus Learning Management System');
$pdf->SetCreator('Ludus Learning Management System');
$pdf->SetTitle('Certificado de Estudios - AutoTrain LMS');
$pdf->SetSubject('Certificado de Estudios - AutoTrain LMS');
$pdf->SetMargins(0, 0, 0, true);
$pdf->setPrintHeader(false);
$pdf->SetFooterMargin(0);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->SetLeftMargin(0);
$pdf->SetTopMargin(0);
ob_start();
include('certificado_source.php');
$html = ob_get_clean();
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('archivo.pdf', 'I'); */
