<?php include('src/seguridad.php'); ?>
<?php include('controllers/preguntas.php');
$location = 'education';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons circle_question_mark"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/preguntas.php">Configuración Banco de Preguntas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Banco de Preguntas </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
										<h5><a href="preguntas.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">

							<?php if ( isset( $numero_resp ) && $numero_resp > 0 ): ?>
								<div class="alert alert-warning">
									<strong>Importante!</strong> Esta pregunta ya tiene respuestas vinculadas, no es posible editarla
								</div>
							<?php endif; ?>
							<br>
							<form id="formulario_data" method="POST" enctype="multipart/form-data" autocomplete="off">
								<!-- Row -->
								<div class="row innerLR">
									<div class="col-md-1"></div>
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="code" style="padding-top:8px;">Código:</label>
											<div class="col-md-8"><input class="form-control" id="code" name="code" type="text" placeholder="Código de la pregunta" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['code']; ?>"<?php } ?> /></div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="type" style="padding-top:8px;">Tipo de Pregunta:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="type" name="type">
													<option value="1" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="1"){ ?>selected="selected"<?php } ?>>Evaluación Abierta</option>
													<option value="2" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="2"){ ?>selected="selected"<?php } ?>>Encuesta Abierta</option>
													<option value="3" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="3"){ ?>selected="selected"<?php } ?>>Evaluación Curso</option>
													<option value="4" <?php if(isset($registroConfiguracion['type'])&&$registroConfiguracion['type']=="4"){ ?>selected="selected"<?php } ?>>Encuesta Curso</option>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-1"></div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
									<div class="col-md-1"></div>
									<!-- Column -->
									<div class="col-md-10">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-2 control-label" for="question" style="padding-top:8px;">Pregunta:</label>
											<div class="col-md-10"><input class="form-control" id="question" name="question" type="text" placeholder="Texto de la pregunta" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['question']; ?>"<?php } ?> /></div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-1"></div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
									<div class="col-md-1"></div>
									<!-- Column -->
									<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
										<input type="hidden" id="imgant" name="imgant" value="<?php echo($registroConfiguracion['source']);?>">
									<?php } ?>
									<div class="col-md-5">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
										  	<span class="btn btn-default btn-file">
										  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
										  		<span class="fileupload-exists">Cambiar Imagen</span>
											  	<input type="file" class="margin-none" id="image_new" name="image_new" />
											</span>
										  	<span class="fileupload-preview"></span>
										  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="type_response" style="padding-top:8px;">Tipo de Respuesta:</label>
											<div class="col-md-7">
												<select style="width: 100%;" id="type_response" name="type_response">
													<option value="1" <?= isset($registroConfiguracion['type_response']) && $registroConfiguracion['type_response']=="1" ? "selected":"" ?>>Selección Múltiple</option>
													<option value="2" <?= isset($registroConfiguracion['type_response']) && $registroConfiguracion['type_response']=="2" ? "selected":"" ?>>Respuesta Abierta</option>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-1">
										<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
											if($registroConfiguracion['source']!=""){ ?>
												<a href="../assets/gallery/source/<?php echo($registroConfiguracion['source']); ?>" target="_blank" >
												<img id="ImgPregunta" style="width:100px;" src="../assets/gallery/source/<?php echo($registroConfiguracion['source']); ?>" alt="<?php echo($registroConfiguracion['source']); ?>" class="img-responsive padding-none border-none" />
												</a>
										<?php } } ?>
									</div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
									<div class="col-md-1"></div>
									<!-- Column -->
									<div class="col-md-5">
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-5">
										<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')): ?>
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
												<div class="col-md-7">
													<select style="width: 100%;" id="status_id" name="status_id">
														<option value="1" <?= $registroConfiguracion['status_id']=="1" ? "selected": "" ?>>Activo</option>
														<option value="2" <?= $registroConfiguracion['status_id']=="2" ? "selected": "" ?>>Inactivo</option>
													</select>
												</div>
											</div>
											<!-- // Group END -->
										<?php endif ?>
									</div>
									<!-- // Column END -->
									<!-- Column -->
									<div class="col-md-1"></div>
									<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Row -->
								<div class="row innerLR">
								<!-- Column -->
									<div class="col-md-12">
										<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
											<!-- Table heading -->
											<thead>
												<tr>
													<th data-hide="phone,tablet" style="width: 60%;">RESPUESTA</th>
													<th data-hide="phone,tablet" style="width: 15%;">ACTUAL</th>
													<th data-hide="phone,tablet" style="width: 15%;">CAMBIAR IMAGEN</th>
													<th data-hide="phone,tablet" style="width: 10%;">CORRECTA?</th>
												</tr>
											</thead>
											<!-- // Table heading END -->
											<tbody>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer1" name="answer1" type="text" placeholder="Texto de la respuesta 1 (Obligatoria)" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer1; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer1!=""){?>
															<img id="ImgResp1" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer1); ?>" alt="<?php echo($img_answer1); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp1_ant" name="ImgResp1_ant" value="<?php echo($img_answer1); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp1_image_new" name="ImgResp1_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="1" <?php if(isset($RespSel)&&$RespSel=="1"){ ?>checked="checked" <?php } ?> >
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer2" name="answer2" type="text" placeholder="Texto de la respuesta 2 (Obligatoria)" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer2; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer2!=""){?>
															<img id="ImgResp2" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer2); ?>" alt="<?php echo($img_answer2); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp2_ant" name="ImgResp2_ant" value="<?php echo($img_answer2); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp2_image_new" name="ImgResp2_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="2" <?php if(isset($RespSel)&&$RespSel=="2"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer3" name="answer3" type="text" placeholder="Texto de la respuesta 3" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer3; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer3!=""){?>
															<img id="ImgResp3" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer3); ?>" alt="<?php echo($img_answer3); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp3_ant" name="ImgResp3_ant" value="<?php echo($img_answer3); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp3_image_new" name="ImgResp3_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="3" <?php if(isset($RespSel)&&$RespSel=="3"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer4" name="answer4" type="text" placeholder="Texto de la respuesta 4" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer4; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer4!=""){?>
															<img id="ImgResp4" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer4); ?>" alt="<?php echo($img_answer4); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp4_ant" name="ImgResp4_ant" value="<?php echo($img_answer4); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp4_image_new" name="ImgResp4_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="4" <?php if(isset($RespSel)&&$RespSel=="4"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer5" name="answer5" type="text" placeholder="Texto de la respuesta 5" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer5; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer5!=""){?>
															<img id="ImgResp5" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer5); ?>" alt="<?php echo($img_answer5); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp5_ant" name="ImgResp5_ant" value="<?php echo($img_answer5); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp5_image_new" name="ImgResp5_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="5" <?php if(isset($RespSel)&&$RespSel=="5"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer6" name="answer6" type="text" placeholder="Texto de la respuesta 6" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer6; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer6!=""){?>
															<img id="ImgResp6" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer6); ?>" alt="<?php echo($img_answer6); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp6_ant" name="ImgResp6_ant" value="<?php echo($img_answer6); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp6_image_new" name="ImgResp6_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="6" <?php if(isset($RespSel)&&$RespSel=="6"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer7" name="answer7" type="text" placeholder="Texto de la respuesta 7" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer7; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer7!=""){?>
															<img id="ImgResp7" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer7); ?>" alt="<?php echo($img_answer7); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp7_ant" name="ImgResp7_ant" value="<?php echo($img_answer7); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp7_image_new" name="ImgResp7_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="7" <?php if(isset($RespSel)&&$RespSel=="7"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer8" name="answer8" type="text" placeholder="Texto de la respuesta 8" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer8; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer8!=""){?>
															<img id="ImgResp8" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer8); ?>" alt="<?php echo($img_answer8); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp8_ant" name="ImgResp8_ant" value="<?php echo($img_answer8); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp8_image_new" name="ImgResp8_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="8" <?php if(isset($RespSel)&&$RespSel=="8"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer9" name="answer9" type="text" placeholder="Texto de la respuesta 9" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer9; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer9!=""){?>
															<img id="ImgResp9" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer9); ?>" alt="<?php echo($img_answer9); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp9_ant" name="ImgResp9_ant" value="<?php echo($img_answer9); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp9_image_new" name="ImgResp9_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="9" <?php if(isset($RespSel)&&$RespSel=="9"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>
												<tr class="reg_busqueda">
													<td align="center">
														<input class="form-control" id="answer10" name="answer10" type="text" placeholder="Texto de la respuesta 10" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $answer10; ?>"<?php } ?> />
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) {
															if($img_answer10!=""){?>
															<img id="ImgResp10" style="width:100px;" src="../assets/gallery/source/<?php echo($img_answer10); ?>" alt="<?php echo($img_answer10); ?>" class="img-responsive padding-none border-none" />
														<?php } } ?>
													</td>
													<td align="center">
														<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
															<input type="hidden" id="ImgResp10_ant" name="ImgResp10_ant" value="<?php echo($img_answer10); ?>">
														<?php } ?>
														<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
														  	<span class="btn btn-default btn-file">
														  		<span class="fileupload-new">Seleccionar Imagen (jpg JPG únicamente)</span>
														  		<span class="fileupload-exists">Cambiar Imagen</span>
															  	<input type="file" class="margin-none" id="ImgResp10_image_new" name="ImgResp10_image_new" />
															</span>
														  	<span class="fileupload-preview"></span>
														  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
														</div>
													</td>
													<td align="center">
														<input type="radio" name="RespSel" id="RespSel" value="10" <?php if(isset($RespSel)&&$RespSel=="10"){ ?>checked="checked" <?php } ?>>
													</td>
												</tr>

											</tbody>
										</table>
									</div>
								<!-- // Column END -->
								</div>
								<!-- // Row END -->
								<div class="separator"></div>
								<!-- Form actions -->
								<div class="form-actions">
									<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
										<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
										<input type="hidden" id="opcn" name="opcn" value="crear">
									<?php }elseif(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>

										<?php if ( isset( $numero_resp ) && $numero_resp == 0 ): ?>
											<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
											<input type="hidden" id="opcn" name="opcn" value="editar">
											<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
										<?php endif; ?>
									<?php } ?>
									<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
								</div>
								<!-- // Form actions END -->
							</form>
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/preguntas.js"></script>
</body>
</html>
