<?php
if(isset($_POST['tema'])){
  if($_POST['tema']==1){
    include_once('models/newsletter.php');
    $obj_newsl = new Newsletter();
    $listaConcesionarios = $obj_newsl->consultaConcesionarios();
    $listaPersonal = $obj_newsl->consultaPersonal();
    $listaCargos = $obj_newsl->consultaCargos();
    $listaRoles = $obj_newsl->consultaRoles();
    ?>
    <form method="post" enctype="multipart/form-data" id="formCorreo">
      <input type="hidden" name="opcn" value="enviar">
      <div class="innerAll"  style="overflow-y:scroll;height:600px;width:100%;">
        <div class="modal-header" id="mail">
          <div class="row modal-header" id="mail-header">
            <div class="col-md-6 form-group">
              <label for="s_persona">Persona</label>
              <select multiple="multiple" style="width: 100%;" name="s_persona[]" id="s_persona">
                <optgroup label="concesionarios" id="">
                  <?php foreach ($listaPersonal as $key => $keyPersona) {
                    ?>
                    <option value="<?php echo $keyPersona['user_id']; ?>"><?php echo $keyPersona['first_name']." ".$keyPersona['last_name']; ?></option>
                  <?php } ?>
                </optgroup>
              </select>
            </div>
            <div class="col-md-6 form-group">
              <label for="s_concesionario">concesionarios</label>
              <select multiple="multiple" style="width: 100%;" name="s_concesionario[]" id="s_concesionario">
                <optgroup label="concesionarios">
                    <?php foreach ($listaConcesionarios as $key => $Data_Dealers) { ?>
                      <option value="<?php echo $Data_Dealers['dealer_id']; ?>"><?php echo $Data_Dealers['dealer']; ?></option>
                    <?php } ?>
                  </optgroup>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label for="s_cargos">Cargos</label>
              <select multiple="multiple" style="width: 100%;" name="s_cargos[]" id="s_cargos">
                <optgroup label="Cargos">
                  <?php foreach ($listaCargos as $key => $keyCargos) { ?>
                    <option value="<?php echo $keyCargos['charge_id']; ?>"><?php echo $keyCargos['charge']; ?></option>
                  <?php } ?>
                </optgroup>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label for="s_genero">Género</label>
              <select style="width: 100%;" name="s_genero" id="s_genero">
                <option></option>
                <option value="1">Hombre</option>
                <option value="2">Mujer</option>
              </select>
            </div>
            <div class="col-md-4 form-group">
              <label for="s_rol">Rol</label>
              <select multiple="multiple" style="width: 100%;" name="s_rol[]" id="s_rol">
                <optgroup label="Roles">
                  <?php foreach ($listaRoles as $key => $keyRoles) { ?>
                    <option value="<?php echo $keyRoles['rol_id']; ?>"><?php echo $keyRoles['rol']; ?></option>
                  <?php } ?>
                </optgroup>
              </select>
            </div>
            <div class="col-md-9 form-group">
              <label for="asunto">Asunto</label>
              <input type="text" name="asunto" id="asunto" class="form-control">
            </div>
            <div class="col-md-3 form-group">
              <label for="">Adjunto</label>
                <label for="inputFile" class="form-control">
                <span id="textoFile"></span>
                </label>
              <input type="file" id="inputFile" name="inputFile" class="form-control" style="display: none;">
            </div>
          </div>
          <div id="mail-body" style="margin: 20px auto;padding:5px;width: 85%;max-width: 900px;justify-content: center;border: solid 1px silver;border-radius: 3px;">
              <div style="margin:0 auto; padding:5px 10px; display: flex;flex-wrap; wrap;">
                <div style="margin: 5px;width: 25%;min-width: 220px;">
                <label for="imagen-mail">
                  <div id="contentImagen">
                    <img src="../assets/images/upload-img.png" alt="upload" style="width: 100%">
                  </div>
                </label>
                <input type="file" name="imagen-mail" id="imagen-mail" style="display: none;">
                </div>
                <div class="" style="border: solid 1px #eee;margin: 5px;width: 70%;min-height: 200px;">
                  <textarea class="wysihtml5" name="textoEmail" id="textoEmail" style="width: 95%;min-height: 150px;border: none;" row="15" placeholder="Ingrese su comentario"></textarea>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <button type="submit" name="enviar" id="enviar" class="btn btn-primary">Enviar</button>
        </div>
      </div>
    </form><!-- fin formulario -->
    <script type="text/javascript">
    $(document).ready(function(){
      $("#s_persona").select2({
        placeholder: "Seleccione una persona",
        allowClear: true
      });
      $("#s_concesionario").select2({
        placeholder: "Seleccione un concesionario",
        allowClear: true
      });
      $("#s_cargos").select2({
        placeholder: "Seleccione un cargo",
        allowClear: true
      });
      $("#s_genero").select2({
        placeholder: "Seleccione un genero",
        allowClear: true
      });
      $("#s_rol").select2({
        placeholder: "Seleccione un rol",
        allowClear: true
      });
    });

    $("#imagen-mail").on('change', function () {
       if (typeof (FileReader) != "undefined") {
           var image_holder = $("#contentImagen");
           image_holder.empty();
           var reader = new FileReader();
           reader.onload = function (e) {
               $("<img />", {
                   "src": e.target.result,
                   "style":"width: 100%"
               }).appendTo(image_holder);
           }
           image_holder.show();
           reader.readAsDataURL($(this)[0].files[0]);
       } else {
           alert("This browser does not support FileReader.");
       }
    });
    /*
    Muestra el texto del archivo adjunto
    */
    $("#inputFile").change(function(){
      var nombreFile= $("#inputFile").val();
      var longitud = nombreFile.length;
      console.log(nombreFile,longitud);
      $("#textoFile").text(nombreFile.substring(12,longitud));
    });
    /*
    */
    function valida(){
      var valid = 0;
      $('#s_persona').css( "border-color", "#efefef" );
      if($('#s_persona').val() == '' || $('#s_persona').val() == null){
          $('#s_persona').css( "border-color", "#b94a48" );
          valid++;
      }
      $('#s_concesionario').css( "border-color", "#efefef" );
      if($('#s_concesionario').val() == '' || $('#s_concesionario').val() == null){
          $('#s_concesionario').css( "border-color", "#b94a48" );
          valid++;
      }
      $('#s_cargos').css( "border-color", "#efefef" );
      if($('#s_cargos').val() == '' || $('#s_cargos').val() == null){
          $('#s_cargos').css( "border-color", "#b94a48" );
          valid++;
      }
      $('#s_genero').css( "border-color", "#efefef" );
      if($('#s_genero').val() == '' || $('#s_genero').val() == null){
          $('#s_genero').css( "border-color", "#b94a48" );
          valid++;
      }
      $('#s_rol').css( "border-color", "#efefef" );
      if($('#s_rol').val() == '' || $('#s_rol').val() == null){
          $('#s_rol').css( "border-color", "#b94a48" );
          valid++;
      }
      return valid;
    }

    $("#formCorreo").submit(function(event){
      console.log("si entro al submit", valida());
      if(valida() > 4){
        notyfy({
            text: 'No se ha seleccionado un destinatario',
            type: 'error' // alert|error|success|information|warning|primary|confirm
        });
        return false;
        event.preventDefault();
      }
    });
    </script>
<?php
  unset($obj_newsl);
} //fin segundo if $_POST
}
/*
@ $_POST['opcn']: opcion que determina al ayuda auxiliar
- lista: actualiza por AJAX las distintas listas de destinatarios [Concesionarios,Cargos,Genero,Roles]
*/
if(isset($_POST['opcion'])){

  switch($_POST['opcion']){
    /*
    case :'mostrar'
    Muestra un correo ya enviado y que es seleccionado desde la bandeja de salida
    */
    case 'mostrar':
      include_once('models/newsletter.php');
      $obj_newsl = new Newsletter();
      $sel = $obj_newsl->registroSeleccionado($_POST['idL']);//trae los registros del correo seleccionado
      if(strstr($sel['reciever'],',')){
        $recieverSeleccionados = explode(',',$sel['reciever']);
      }else{
        $recieverSeleccionados = $sel['reciever'];
      }
      if(strstr($sel['dealer'],',')){
        $dealerSeleccionados = explode(',',$sel['dealer']);
      }else{
        $dealerSeleccionados = $sel['dealer'];
      }
      if(strstr($sel['gender'],',')){
        $genderSeleccionados = explode(',',$sel['gender']);
      }else{
        $genderSeleccionados = $sel['gender'];
      }
      if(strstr($sel['charge'],',')){
        $chargeSeleccionados = explode(',',$sel['charge']);
      }else{
        $chargeSeleccionados = $sel['charge'];
      }
      if(strstr($sel['rol'],',')){
        $rolSeleccionados = explode(',',$sel['rol']);
      }else{
        $rolSeleccionados = $sel['rol'];
      }
      $select_personas = "";
      $select_concesionarios = "";
      $select_cargos = "";
      $select_genero = "";
      $select_roles = "";
      //print_r($sel);
      //si la opcion es 0 = correos personales
        $listaPersonas = $obj_newsl->consultaPersonalUnico($sel['reciever']);
        $listaConcesionarios = $obj_newsl->consultaConcesionarios();
        $listaCargos = $obj_newsl->consultaCargos();
        $listaRoles = $obj_newsl->consultaRoles();
        //print_r($dealersSeleccionados);
        $select_personas .= '<optgroup label="Personas">';
        foreach ($listaPersonas as $key => $Data_Dealers) {
          $selected = "selected";
          $select_personas .= '<option value="'.$Data_Dealers['user_id'].'" '.$selected.'>'.$Data_Dealers['first_name'].' '.$Data_Dealers['last_name'].'</option>';
        }
        $select_personas .= '</optgroup>';
        //fin crear personal

        //print_r($dealersSeleccionados);
        $select_concesionarios .= '<optgroup label="concesionarios">';
        foreach ($listaConcesionarios as $key => $Data_Dealers) {
          $selected = "";
          if(is_array($dealerSeleccionados)){
            foreach ($dealerSeleccionados as $key2 => $Data_DealerUser) {
              if($Data_Dealers['dealer_id'] == $Data_DealerUser){
                $selected = "selected";
              }
            }
          }else{
            if($Data_Dealers['dealer_id'] == $dealerSeleccionados){
              $selected = "selected";
            }
          }
          $select_concesionarios .= '<option value="'.$Data_Dealers['dealer_id'].'" '.$selected.'>'.$Data_Dealers['dealer'].'</option>';
        }
        $select_concesionarios .= '</optgroup>';
        //fin edicion concesionarios

        $select_cargos .= '<optgroup label="Cargos">';
        foreach ($listaCargos as $key => $keyCargos) {
          $selected = "";
          if(is_array($chargeSeleccionados)){
            foreach ($chargeSeleccionados as $key2 => $Data_DealerUser) {
              if($keyCargos['charge_id'] == $Data_DealerUser){
                $selected = "selected";
              }
            }
          }else{
            if($keyCargos['charge_id'] == $chargeSeleccionados){
              $selected = "selected";
            }
          }
          $select_cargos .= '<option value="'.$keyCargos['charge_id'].'" '.$selected.'>'.$keyCargos['charge'].'</option>';
        }
        $select_cargos .= '</optgroup>';
          //fin edicion cargos
        $select_genero .= $genderSeleccionados == 1 ? '<option value="1" selected>Hombre</option>':'<option value="1">Hombre</option>';
        $select_genero .= $genderSeleccionados == 2 ? '<option value="2" selected>Mujer</option>':'<option value="2">Mujer</option>';
        //fin edicion genero

        $select_roles .= '<optgroup label="Roles">';
        foreach ($listaRoles as $key => $keyRoles) {
          $selected = "";
          if(is_array($rolSeleccionados)){
            foreach ($rolSeleccionados as $key2 => $Data_DealerUser) {
              if($keyRoles['rol_id'] == $Data_DealerUser){
                $selected = "selected";
              }
            }
          }else{
            if($keyRoles['rol_id'] == $rolSeleccionados){
              $selected = "selected";
            }
          }
          $select_roles .= '<option value="'.$keyRoles['rol_id'].'" '.$selected.'>'.$keyRoles['rol'].'</option>';
        }
        $select_roles .= '</optgroup>';

      ?>
      <form method="post" enctype="multipart/form-data" id="formCorreo">
        <input type="hidden" name="opcn" value="enviar">
        <div class="innerAll"  style="overflow-y:scroll;height:600px;width:100%;">
          <div class="modal-header" id="mail">
            <div class="row modal-header" id="mail-header">
              <div class="col-md-6 form-group">
                <label for="s_persona">Persona</label>
                <select multiple="multiple" style="width: 100%;" name="s_persona[]" id="s_persona">
                  <?php echo($select_personas); ?>
                </select>
              </div>
              <div class="col-md-6 form-group">
                <label for="s_concesionario">Concesionarios</label>
                <select multiple="multiple" style="width: 100%;" name="s_concesionario[]" id="s_concesionario">
                  <?php echo($select_concesionarios); ?>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label for="s_cargos">Cargos</label>
                <select multiple="multiple" style="width: 100%;" name="s_cargos[]" id="s_cargos">
                  <?php echo($select_cargos); ?>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label for="s_genero">Género</label>
                <select style="width: 100%;" name="s_genero" id="s_genero">
                  <option></option>
                  <?php echo $select_genero ?>
                </select>
              </div>
              <div class="col-md-4 form-group">
                <label for="s_rol">Rol</label>
                <select multiple="multiple" style="width: 100%;" name="s_rol[]" id="s_rol">
                  <?php echo $select_roles ?>
                </select>
              </div>
              <div class="col-md-9 form-group">
                <label for="asunto">Asunto</label>
                <input type="text" name="asunto" id="asunto" class="form-control" value="<?php echo($sel['affair']); ?>">
              </div>
              <div class="col-md-3 form-group">
                <label for="">Adjunto</label>
                  <label for="inputFile" class="form-control">
                  <span id="textoFile"></span>
                  </label>
                <input type="file" id="inputFile" name="inputFile" class="form-control" style="display: none;">
              </div>
            </div>
            <div id="mail-body" style="margin: 20px auto;padding:5px;width: 85%;max-width: 900px;justify-content: center;border: solid 1px silver;border-radius: 3px;">
                <div style="margin:0 auto; padding:5px 10px; display: flex;flex-wrap; wrap;">
                  <div style="margin: 5px;width: 25%;min-width: 220px;">
                  <label for="imagen-mail">
                    <div id="contentImagen">
                      <img src="<?php if($sel['image'] == ''){echo("../assets/images/upload-img.png");}else{echo("../assets/emails/".$sel['image']);}?>" alt="<?php echo($sel['image']); ?>" style="width: 100%">
                    </div>
                  </label>
                  <input type="file" name="imagen-mail" id="imagen-mail" style="display: none;">
                  </div>
                  <div class="" style="border: solid 1px #eee;margin: 5px;width: 70%;min-height: 200px;">
                    <textarea class="wysihtml5" name="textoEmail" id="textoEmail" style="width: 95%;min-height: 150px;border: none;" row="15" placeholder="Ingrese su comentario"><?php echo($sel['email_text']); ?></textarea>
                  </div>
                </div>
                <div class="">
                  <strong>Adjunto: </strong>
                  <a href="../assets/emails/<?php echo($sel['file']); ?>" target="_blank"><?php echo($sel['file']); ?></a>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="row">
            <button type="submit" name="enviar" id="enviar" class="btn btn-primary">Enviar</button>
          </div>
        </div>
      </form><!-- fin formulario -->
      <script type="text/javascript">
      $(document).ready(function(){
        $("#s_persona").select2({
          placeholder: "Seleccione una persona",
          allowClear: true
        });
        $("#s_concesionario").select2({
          placeholder: "Seleccione un concesionario",
          allowClear: true
        });
        $("#s_cargos").select2({
          placeholder: "Seleccione un cargo",
          allowClear: true
        });
        $("#s_genero").select2({
          placeholder: "Seleccione un genero",
          allowClear: true
        });
        $("#s_rol").select2({
          placeholder: "Seleccione un rol",
          allowClear: true
        });
      });

      $("#imagen-mail").on('change', function () {
         if (typeof (FileReader) != "undefined") {
             var image_holder = $("#contentImagen");
             image_holder.empty();
             var reader = new FileReader();
             reader.onload = function (e) {
                 $("<img />", {
                     "src": e.target.result,
                     "style":"width: 100%"
                 }).appendTo(image_holder);
             }
             image_holder.show();
             reader.readAsDataURL($(this)[0].files[0]);
         } else {
             alert("This browser does not support FileReader.");
         }
      });
      /*
      Muestra el texto del archivo adjunto
      */
      $("#inputFile").change(function(){
        var nombreFile= $("#inputFile").val();
        var longitud = nombreFile.length;
        console.log(nombreFile,longitud);
        $("#textoFile").text(nombreFile.substring(12,longitud));
      });
      /*
      */
      function valida(){
        var valid = 0;
        $('#s_persona').css( "border-color", "#efefef" );
        if($('#s_persona').val() == '' || $('#s_persona').val() == null){
            $('#s_persona').css( "border-color", "#b94a48" );
            valid++;
        }
        $('#s_concesionario').css( "border-color", "#efefef" );
        if($('#s_concesionario').val() == '' || $('#s_concesionario').val() == null){
            $('#s_concesionario').css( "border-color", "#b94a48" );
            valid++;
        }
        $('#s_cargos').css( "border-color", "#efefef" );
        if($('#s_cargos').val() == '' || $('#s_cargos').val() == null){
            $('#s_cargos').css( "border-color", "#b94a48" );
            valid++;
        }
        $('#s_genero').css( "border-color", "#efefef" );
        if($('#s_genero').val() == '' || $('#s_genero').val() == null){
            $('#s_genero').css( "border-color", "#b94a48" );
            valid++;
        }
        $('#s_rol').css( "border-color", "#efefef" );
        if($('#s_rol').val() == '' || $('#s_rol').val() == null){
            $('#s_rol').css( "border-color", "#b94a48" );
            valid++;
        }
        return valid;
      }

      $("#formCorreo").submit(function(event){
        console.log("si entro al submit", valida());
        if(valida() > 4){
          notyfy({
              text: 'No se ha seleccionado un destinatario',
              type: 'error' // alert|error|success|information|warning|primary|confirm
          });
          return false;
          event.preventDefault();
        }
      });
      </script>
      <?php
      unset($obj_newsl);
    break;
  }//fin switch $_POST['opcn']
}//fin opcn
/*
Fin if $_POST['opcn']
*/
?>
