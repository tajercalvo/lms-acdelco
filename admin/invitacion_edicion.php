<?php include('src/seguridad.php'); ?>
 <?php include('controllers/invitacion_edicion.php');
$location = 'inivitacion';


?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons group"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/invitacion_edicion.php">Edición de Invitación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Edición de Invitación </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="row">
	<div class="widget widget-heading-simple widget-body-gray">
		<div class="widget-body">
			<div class="row">
				<div class="col-md-10">
					<h5 style="text-align: justify; ">Aquí encuentra las opciones para configurar la invitación de un estudiante o varios a otra sesion de educación disponible.</h5></br>
				</div>
			</div>
			<div class="row">
				<form role="form" id="form_consulta">
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="course_id" style="padding-top:8px;">Curso:</label>
						<div class="col-md-10">
							<!-- Inicio select de Curso -->
							<!-- <select style="width: 100%;" id="course_id" name="course_id">
								<option value="0">Todos</option>
								<?php foreach ($cursos_datos as $key => $cursos_datos) { ?>
									<option value="<?php echo($cursos_datos['course_id']); ?>" <?php if(isset($_POST['course_id']) && $_POST['course_id']==$cursos_datos['course_id']){ ?>selected="selected"<?php } ?>><?php echo($cursos_datos['newcode'].' | '.$cursos_datos['type'].' | '.$cursos_datos['course']); ?></option>
								<?php } ?>
							</select> -->
							<input id="course_id" name="course_id" type="text" value="" style="width: 100%;"/>
							<!-- fin de select -->
						</div>
					</div>
					<!-- // Group END -->
				</div>
				,<br><br><br>
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="schedule_id" style="padding-top:8px;">Sesión:</label>
						<div class="col-md-10">
							<!-- Inicio select de Sesion -->
							<select style="width: 100%;" id="schedule_id" name="schedule_id" >
								<!-- <option value="0" selected> </option> -->
			                </select>
							<!-- fin de select -->
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
					<button type="submit" class="btn btn-success" id="ConsultaInvitacion"><i class="fa fa-check-circle"></i> Consultar</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-8">
		<div class="widget widget-heading-simple widget-body-white">
			<!-- Widget heading -->
			<div class="widget-head">
				<h4 class="heading glyphicons list"><i></i> Información: </h4>
			</div>
			<!-- // Widget heading END -->
			<div class="widget-body">
				<div id="cantidad_datos" class="col-md-5 form-inline separator bottom small"></div>
				<div class="col-md-3"></div>
				<div class="col-md-4" style="text-align: right;">
					<div class="val-trigger" style="display: none;">
						<label>Buscar <input type="text" id="filter" /></label>
					</div>
				</div>
				<!-- Table elements-->
				<div id="ContenidoTabla">
				</div>
				<!-- <table id="TableData" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
					
					<thead>
						<tr>
							<th data-hide="phone,tablet">#</th>
							<th data-hide="phone,tablet">FOTO</th>
							<th data-hide="phone,tablet">USUARIO</th>
							<th data-hide="phone,tablet">IDENTIFICACION</th>
							<th data-hide="phone,tablet">EMPRESA</th>
							<th data-hide="phone,tablet">EDICION</th>
						</tr>
					</thead>
					
					
				</table>
				 -->

			</div>
		</div>
	</div>

	<div class="col-md-4" style="margin-top: 70px;">
		<div class="widget  padding-none">
			<form role="form" id="form_new_schedule">
				<div class="widget-body padding-none">
					<h4 class="innerAll bg-primary text-white margin-bottom-none center">Cambio de Sesion:</h4>
					<br>
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-2">
									<label class="control-label" for="new_schedule_id">Nueva Sesión:</label>
								</div>
								<div class="col-md-10">
									<!-- Inicio select de Sesion -->
									<select style="width: 100%;" id="new_schedule_id" name="new_schedule_id" >
										<!-- <option value="0">Seleccione una Sesion</option> -->
					                </select>
									<!-- fin de select -->
								</div>
							</div>
						</div>
						<br>
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-2">
									<label class="control-label" for="user" style="padding-top:8px;">Usuario:</label>
								</div>
								<div class="col-md-10">
									<input class="form-control" id="nombre" name="nombre" type="text" placeholder="Usuario seleccionado" value="" readonly="readonly">
								</div>
				                <input type="hidden" id="invitation_id" name="invitation_id" value="">
				                <input type="hidden" id="schedule_id" name="schedule_id" value="">
				                <input type="hidden" id="user_id" name="user_id" value="">
				                <input type="hidden" id="dealer_id" name="dealer_id" value="">
				                <input type="hidden" id="course_id" name="course_id" value="">
							</div>
						</div>	
				</div>
				<br><br><br>
				<div class="widget-body padding-none center">
					<div class="form-group">
						<br>
				    	<button id="confirmar_sesion" type="submit" class="btn btn-primary" disabled><i class="fa fa-check-circle"></i> Confirmar </button>
				  	</div>
				</div>
			</form>
		</div>
	</div>

</div>



						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/invitacion_edicion.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>