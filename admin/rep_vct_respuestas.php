<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_vct_respuestas.php'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VCT - AutoTrain</title>
  <link rel="icon" type="image/png" href="https://acdelco.com.co/wp-content/uploads/2017/09/logo.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../assets/templateVct/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../assets/templateVct/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="../assets/templateVct/bower_components/Ionicons/css/ionicons.min.css"> -->
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="../assets/templateVct/bower_components/jvectormap/jquery-jvectormap.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/templateVct/dist/css/AdminLTE.min.css">
  <!-- Material Design -->
  <link rel="stylesheet" href="../assets/templateVct/dist/css/bootstrap-material-design.min.css">
  <link rel="stylesheet" href="../assets/templateVct/dist/css/ripples.min.css">
  <link rel="stylesheet" href="../assets/templateVct/dist/css/MaterialAdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <!-- <link rel="stylesheet" href="../assets/templateVct/dist/css/skins/all-md-skins.min.css"> -->
  <link rel="stylesheet" href="../assets/templateVct/dist/css/mi.css?version=<?php echo md5(time());?>">
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo" style="background: #3f51b5">
      <img style="width: 100%;" src="https://acdelco.com.co/wp-content/uploads/2017/09/logo.png" alt="">
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" style="background: -webkit-linear-gradient(left, #3f51b5, #3f51b5, #3f51b5);">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <a href="index.php">
              <i class="fa fa-mail-reply-all"></i>
            </a>
          </li>
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img id="mi-foto" src="../assets/images/usuarios/<?php echo $_SESSION['imagen']?>" class="user-image" alt="img">
              <span id="mi-nombre" class="hidden-xs"><?php echo $_SESSION['NameUsuario']?></span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a style="border-left: 1px solid rgb(255,255,255, 0.1);" href="#" data-toggle="control-sidebar"><i class="fa fa-bars"></i></a>
          </li> -->
        </ul>
      </div>

    </nav>
  </header>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin-left: 0px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small> <i class="text-green fa fa-dot-circle-o"></i> Usuarios conectados <strong><?php echo $datos['conectados']; ?></strong> | </small>
        <small> <i class="text-green fa fa-check-square-o"></i> Respondieron <strong><?php echo $datos['acertaron'] + $datos['fallaron']; ?></strong> | </small>
        <small> <i class="text-red fa fa-close"></i> Faltantes <strong><?php echo ($datos['conectados'] - count($datos['res'])); ?></strong> | </small>
        <small> <i class="text-green fa fa-check-square"></i> Acertaron <strong><?php echo $datos['acertaron']; ?></strong> | </small>
        <small> <i class="text-red fa fa-close"></i> Fallaron <strong><?php echo $datos['fallaron']; ?></strong> | </small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $datos['pre']['name']; ?></h3>
              <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12">
                  <div style="height: auto;" >
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Identificación</th>
                        <th>Nombre</th>
                        <th>Respuesta</th>
                        <th>Acertó</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($datos['res'] as $key => $value){?>
                      <tr>
                        <td><?php echo $value['identification'] ?></td>
                        <td><?php echo $value['first_name'].' '.$value['last_name']?></td>
                        <td><?php echo $value['answer'] ?></td>
                        <td><?php echo $value['aproval'] ?></td>
                      </tr>
                      <?php }?>
                    </tbody>
                  </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /.row -->
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!--/col chat -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer" style="margin-left: 0px;">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong><a style="color: black;" href="https://autotrain.com.co" target="_blank">AutoTrain LMS &copy; 2020</a></strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs" style="background: #3f51b5;">
      <li style="border-right: 0.5px solid #721;"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-users"></i></a></li>
      <?php if ($DetallesProgramacion['teacher_id'] == $_SESSION['idUsuario']) { $display = 'block';}else{$display = 'none';} ?>
      <li><a style="display: <?php echo $display?>" href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-file-powerpoint-o"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div id="content-dia-user" class="tab-content" style="overflow-y: auto;">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <span class="control-sidebar-heading"><strong><?php echo count($registrados)?></strong>  Usuarios Conectados</span>
        <ul class="control-sidebar-menu">
            <?php foreach ($registrados as $key => $value) {?>
            <li>
                <a href="javascript:void(0)">
                <img class="direct-chat-img" src="../assets/images/usuarios/<?php echo $value['image'] ?>" alt="img">
                <div class="menu-info">
                    <h4 class="control-sidebar-subheading"><?php echo $value['first_name'].' '.$value['last_name'];?></h4>
                    <p><?php echo $value['dealer'] ?></p>
                </div>
                </a>
            </li>
            <?php }?>
        </ul>
        <!-- /.control-sidebar-menu -->
      </div>
      <!-- /.tab-pane -->

      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <h3 class="control-sidebar-heading">Diapositivas</h3>
        <!-- diapositivas -->
        <ul class="products-list product-list-in-box">
            <?php foreach ($diapositivas as $key => $value) {?>
                <li  class="item mini-diap" id="diapo_<?php echo $value['coursefile_id'] ?>" data-diapositiva="<?php echo $value['coursefile_id'] ?>" data-image="<?php echo $value['image'] ?>" data-archivo="<?php echo $value['archivo'] ?>" data-ruta="../assets/fileVCT/VCT_<?php echo $value['course_id'].'/' ?>" data-question_id="<?php echo $value['question_id'] ?>">
                    <div class="product-img">
                    <img src="<?php echo $value['imagen'] ?>" alt="img">
                    </div>
                    <div class="product-info">
                    <span class="product-title"><?php echo $value['file'] ?> - <a class="ver" href="../assets/fileVCT/VCT_<?php echo $value['course_id'] ?>/<?php echo $value['image'] ?>" target="_blank">Ver</a></span>
                    <span class="product-description"><?php echo $value['name'] ?></span>
                    </div>
                </li>
            <?php }?>

        </ul>
        <!-- /diapositivas -->
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../assets/templateVct/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../assets/templateVct/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Material Design -->
<script src="../assets/templateVct/dist/js/material.min.js"></script>
<script src="../assets/templateVct/dist/js/ripples.min.js"></script>
<script>
    $.material.init();
</script>
<script src="../assets/templateVct/dist/js/adminlte.min.js"></script>
<!-- <script src="//static.opentok.com/v2/js/opentok.js"></script>
<script src="js/virtualclass.js?version=<?php echo md5(time());?>"></script> -->
</body>
</html>