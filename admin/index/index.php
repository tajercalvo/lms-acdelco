<?php
include('models/usuarios.php');

$usuario_Class = new Usuarios();

$idUsr = 0;

if(isset($_SESSION['idUsuario'])){
	$idUsr = $_SESSION['idUsuario'];
}

$data_user = $usuario_Class->consultaDatosUsuario($idUsr);
$charge_user = $usuario_Class->consultaDatosCargos($idUsr);
$rol_user = $usuario_Class->consultaDatosRoles($idUsr);
/*$datosUsuario = $usuario_Class->consultaDatosUsuario($IdUsuario);
$charge_user = $usuario_Class->consultaDatosCargos($IdUsuario);
$rol_user = $usuario_Class->consultaDatosRoles($IdUsuario);*/

/*$var_Publicidad = 1;
//$act_Publicidad = $usuario_Class->crea_Publicidad(5);
$TieneTecnico = 0;
$TieneTecnico = Fcn_TieneCargo($charge_user,31);
$TieneTecnico = $TieneTecnico + Fcn_TieneCargo($charge_user,32);
if($TieneTecnico>0){
	$var_Publicidad = $usuario_Class->consulta_Publicidad(10);
	if($var_Publicidad<=0){
		$act_Publicidad = $usuario_Class->crea_Publicidad(10);
	}
	$var_Publicidad = 0;
}*/
$var_Publicidad = 1;
//$act_Publicidad = $usuario_Class->crea_Publicidad(5);
$var_Publicidad = $usuario_Class->consulta_Publicidad(12);
if($var_Publicidad<=0){
	$act_Publicidad = $usuario_Class->crea_Publicidad(12);
}

$cantidad_datos = $usuario_Class->consultaCantidadIndex();
$datosUsuarios = $usuario_Class->consultaDatosUsuariosIndex();

$Datos_CursosTomados = $usuario_Class->consultaTomados();
$Cant_CursosTomados = count($Datos_CursosTomados);

$Datos_CA_Inscrito = $usuario_Class->consulta_CA_Inscrito();
$CantDatos_CA_Inscrito = count($Datos_CA_Inscrito);
$Datos_CT_Inscrito = $usuario_Class->consulta_CT_Inscrito();
$CantDatos_CT_Inscrito = count($Datos_CT_Inscrito);

$Datos_CA_Finalizado = $usuario_Class->consulta_CA_Finalizado();
$CantDatos_CA_Finalizado = count($Datos_CA_Finalizado);
$Datos_CT_Finalizado = $usuario_Class->consulta_CT_Finalizado();
$CantDatos_CT_Finalizado = count($Datos_CT_Finalizado);

$Datos_CA_Disponible = $usuario_Class->consulta_CA_Disponible();
$CantDatos_CA_Disponible = count($Datos_CA_Disponible);
$Datos_CT_Disponible = $usuario_Class->consulta_CT_Disponible();
$CantDatos_CT_Disponible = count($Datos_CT_Disponible);

//Cursos que tiene disponibles para inscripción
$DatosDisponibleSinInsc = array_merge($Datos_CT_Disponible, $Datos_CA_Disponible);
//Cursos En los que se encuentra inscrito
$DatosDisponibleInsc = array_merge($Datos_CT_Inscrito, $Datos_CA_Inscrito);
//Cursos que ha finalizado y quiere repasar
$DatosFinalizadoInsc = array_merge($Datos_CT_Finalizado, $Datos_CA_Finalizado);

unset($usuario_Class);