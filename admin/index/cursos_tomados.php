<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head" style="padding-left: 15px;">
		<h4 class="heading glyphicons list"><i></i> Mis Cursos - Tomados (Hist&oacute;rico)</h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<?php /* ?>
		<!-- Total products & Sort by options -->
		<div class="form-inline separator bottom small">
			Total cursos: <?php echo($Cant_CursosTomados); ?>
		</div>
		<!-- // Total products & Sort by options END -->
		<!-- Filters -->
		<div class="filter-bar">
			<form class="form-inline">
				<!-- From -->
				<div class="form-group col-md-2 padding-none">
					<label>Desde:</label>
					<div class="input-group">
						<input type="text" name="dateRangeFrom" id="dateRangeFrom" class="form-control" value="" />
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
				<!-- // From END -->
				<!-- To -->
				<div class="form-group col-md-2 padding-none">
					<label>Hasta:</label>
					<div class="input-group">
						<input type="text" name="dateRangeTo" id="dateRangeTo" class="form-control" value="" />
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
				<!-- // To END -->
				<!-- Select -->
				<div class="form-group col-md-3 padding-none">
					<label class="label-control">Estado:</label>
					<div class="col-md-9 padding-none">
						<select name="status_id" id="status_id" class="form-control">
							<option>Aprobado</option>
							<option>No aprobado</option>
						</select>
					</div>
				</div>
				<!-- // Select END -->
				<div class="clearfix"></div>
			</form>
		</div><?php */ ?>
		<!-- // Filters END -->
		<!-- Products table -->
		<table id="Table_DataTomados" class="display table table-striped table-condensed table-primary table-vertical-center">
			<thead>
				<tr>
					<!--<th style="width: 25%;" class="center">C&oacute;digo</th>-->
					<th>Nombre</th>
					<th style="width: 5%;" class="center">Tipo</th>
					<th style="width: 5%;" class="center">Puntaje</th>
					<th style="width: 5%;" class="center">Fecha</th>
					<th style="width: 5%;" class="center">Aprobó?</th>
					<th style="width: 5%;" class="center"></th>
				</tr>
			</thead>
			<?php foreach ($Datos_CursosTomados as $iID => $DatosCursos_Tomados) { ?>
				<tbody>
					<!--<td class="strong center"><?php echo($DatosCursos_Tomados['code']); ?></td>-->
					<td class="center"><a href="course_detail.php?token=<?php echo(md5('GMAcademy')); ?>&_valSco=<?php echo(md5('GMColmotores')); ?>&opcn=ver&id=<?php echo($DatosCursos_Tomados['course_id']); ?>"><?php echo($DatosCursos_Tomados['course']); ?></a></td>
					<td class="strong center"><?php echo($DatosCursos_Tomados['type']); ?></td>
					<td class="center"><?php echo(number_format($DatosCursos_Tomados['score'],0)); ?></td>
					<td class="center"><?php echo($DatosCursos_Tomados['date_creation']); ?></td>
					<td class="center">
						<?php if($DatosCursos_Tomados['score'] >= 75){ ?>
							<span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
						<?php }else{ ?>
							<span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
						<?php } ?>
					</td>
					<?php if($DatosCursos_Tomados['score'] >= 75){ ?>
						<td class="center"><a href="certificado.php?course_id=<?php echo $DatosCursos_Tomados['course_id']; ?>&date=<?php echo $DatosCursos_Tomados['date_creation']; ?>" class="btn-action glyphicons certificate btn-info" target="_blank"><i></i></a></td>
					<?php }else{ ?>
						<td class="center btn-danger">
							<span class="btn-action btn-danger">Reprobó</span>
						</td>
					<?php } ?>
				</tbody>
			<?php } ?>
		</table>
		<!-- // Products table END -->
	</div>
</div>