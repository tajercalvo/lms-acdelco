<h3>Mis Cursos <span>Actuales</span></h3>
<div class="innerLR">
	<div class="row">
		<div class="col-md-12">
			<div class="widget widget-tabs widget-tabs-gray">
				<div class="widget-head">
					<ul>
						<li class="active"><a href="#courses-presen" data-toggle="tab">Cursos Presenciales</a></li>
						<li><a href="#courses-disponi" data-toggle="tab">WBT (Inscrito)</a></li>
						<li><a href="#courses-virtuales" data-toggle="tab">WBT (Todos)</a></li>
						<li><a href="#courses-autodiri" data-toggle="tab">Autodirigidos (Inscrito)</a></li>
						<li><a href="#courses-autodisp" data-toggle="tab">Autodirigidos (Todos)</a></li>
					</ul>
				</div>
				<div class="widget-body">
					<div class="tab-content">
						<div class="tab-pane active" id="courses-presen">
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<h5 class="text-uppercase strong separator bottom">Cursos encontrados</h5>
									<!-- calendar Layout -->
									<div data-component>
										<div id="calendar_actual"></div>
									</div>
									<!-- // calendar Layout END -->
									<hr class="separator" />
								</div>
							</div>
						</div>
						<div class="tab-pane" id="courses-disponi">
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<h5 class="text-uppercase strong separator bottom"><?php echo($CantDatos_CT_Inscrito); ?> Cursos Virtuales (Inscrito) - Mi trayectoria</h5>
									<table id="TableData" class="table table-striped table-bordered table-condensed table-white">
										<!-- Table heading -->
										<thead>
											<tr>
												<!--<th style="width: 10%;">CÓDIGO</th>-->
												<th>CURSO</th>
												<th>MODULO</th>
												<th style="width: 10%;">FECHA</th>
												<th style="width: 10%;">ESTADO</th>
												<th style="width: 10%;">PUNTAJE</th>
												<th style="width: 10%;">CERTIFICADO</th>
											</tr>
										</thead>
										<!-- // Table heading END -->
										<!-- Table body -->
										<tbody>
											<?php foreach ($Datos_CT_Inscrito as $iID => $DatosCursos_Tomados) { ?>
											<tr>
												<!--<td style="width: 10%;"><?php echo $DatosCursos_Tomados['code']; ?></td>-->
												<td><?php echo $DatosCursos_Tomados['course']; ?></td>
												<td><?php echo $DatosCursos_Tomados['module']; ?></td>
												<td style="width: 10%;">Fecha Inscripción</td>
												<td style="width: 10%;">Inscrito</td>
												<td style="width: 10%;">0</td>
												<td style="width: 10%;"><a href="certificado.php?course_id=<?php echo $DatosCursos_Tomados['course_id']; ?>" class="btn-action glyphicons certificate btn-success"><i></i></a></td>
											</tr>
											<?php } ?>
										</tbody>
										<!-- Table body -->
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="courses-virtuales">
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<h5 class="text-uppercase strong separator bottom"><?php echo($CantDatos_CT_Disponible); ?> Cursos Virtuales (Disponibles) - Mi trayectoria</h5>
									<table id="TableData" class="table table-striped table-bordered table-condensed table-white">
										<!-- Table heading -->
										<thead>
											<tr>
												<!--<th style="width: 10%;">CÓDIGO</th>-->
												<th>CURSO</th>
												<th>MODULO</th>
												<th style="width: 10%;">ESTADO</th>
												<th style="width: 10%;">INSCRIPCIÓN</th>
											</tr>
										</thead>
										<!-- // Table heading END -->
										<!-- Table body -->
										<tbody>
											<?php foreach ($Datos_CT_Disponible as $iID => $DatosCursos_Tomados) { ?>
											<tr>
												<!--<td style="width: 10%;"><?php echo $DatosCursos_Tomados['code']; ?></td>-->
												<td><?php echo $DatosCursos_Tomados['course']; ?></td>
												<td><?php echo $DatosCursos_Tomados['module']; ?></td>
												<td style="width: 10%;" class="center">No inscrito</td>
												<td style="width: 10%;" class="center"><a href="course_detail.php?opcn=inscribir&id=<?php echo $DatosCursos_Tomados['course_id']; ?>" class="btn-action glyphicons keynote btn-success"><i></i></a></td>
											</tr>
											<?php } ?>
										</tbody>
										<!-- Table body -->
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="courses-autodiri">
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<h5 class="text-uppercase strong separator bottom"><?php echo($CantDatos_CA_Inscrito); ?> Cursos Autodirigidos (Inscrito)</h5>
									<table id="TableData" class="table table-striped table-bordered table-condensed table-white">
										<!-- Table heading -->
										<thead>
											<tr>
												<!--<th style="width: 10%;">CÓDIGO</th>-->
												<th>CURSO</th>
												<th>MODULO</th>
												<th style="width: 10%;">FECHA</th>
												<th style="width: 10%;">ESTADO</th>
												<th style="width: 10%;">PUNTAJE</th>
												<th style="width: 10%;">CERTIFICADO</th>
											</tr>
										</thead>
										<!-- // Table heading END -->
										<!-- Table body -->
										<tbody>
											<?php foreach ($Datos_CA_Inscrito as $iID => $DatosCursos_Tomados) { ?>
											<tr>
												<!--<td style="width: 10%;"><?php echo $DatosCursos_Tomados['code']; ?></td>-->
												<td><?php echo $DatosCursos_Tomados['course']; ?></td>
												<td><?php echo $DatosCursos_Tomados['module']; ?></td>
												<td style="width: 10%;">Fecha Inscripción</td>
												<td style="width: 10%;">Inscrito</td>
												<td style="width: 10%;">0</td>
												<td style="width: 10%;"><a href="certificado.php?course_id=<?php echo $DatosCursos_Tomados['course_id']; ?>" class="btn-action glyphicons certificate btn-success"><i></i></a></td>
											</tr>
											<?php } ?>
										</tbody>
										<!-- Table body -->
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="courses-autodisp">
							<div class="widget widget-heading-simple widget-body-white margin-none">
								<div class="widget-body">
									<h5 class="text-uppercase strong separator bottom"><?php echo($CantDatos_CA_Disponible); ?> Cursos Autodirigidos (Disponibles)</h5>
									<table id="TableData" class="table table-striped table-bordered table-condensed table-white">
										<!-- Table heading -->
										<thead>
											<tr>
												<!--<th style="width: 10%;">CÓDIGO</th>-->
												<th>CURSO</th>
												<th>MODULO</th>
												<th style="width: 10%;">ESTADO</th>
												<th style="width: 10%;">INSCRIPCIÓN</th>
											</tr>
										</thead>
										<!-- // Table heading END -->
										<!-- Table body -->
										<tbody>
											<?php foreach ($Datos_CA_Disponible as $iID => $DatosCursos_Tomados) { ?>
											<tr>
												<!--<td style="width: 10%;"><?php echo $DatosCursos_Tomados['code']; ?></td>-->
												<td><?php echo $DatosCursos_Tomados['course']; ?></td>
												<td><?php echo $DatosCursos_Tomados['module']; ?></td>
												<td style="width: 10%;" class="center">No inscrito</td>
												<td style="width: 10%;" class="center"><a href="course_detail.php?opcn=inscribir&id=<?php echo $DatosCursos_Tomados['course_id']; ?>" class="btn-action glyphicons keynote btn-success"><i></i></a></td>
											</tr>
											<?php } ?>
										</tbody>
										<!-- Table body -->
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>