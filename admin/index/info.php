<style type="text/css" media="screen">
	.table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
  background-color: #ddd;

}

.pagination {
    text-align: center; /* Centrar los elementos */
    margin-top: 10px;
}

.pagination-btn {
    font-size: 14px; /* Tamaño de fuente más grande */
    padding: 8px 18px; /* Espacio adicional alrededor del texto */
    text-decoration: none; /* Quitar subrayado */
    color: #0058A3; /* Color de los enlaces */
    margin: 0 3px; /* Espacio entre los botones */
    border-radius: 5px; /* Bordes redondeados */
    border: 1px solid #0058A3; /* Borde alrededor del enlace */
    transition: background-color 0.3s ease; /* Transición suave al pasar el mouse */
}

.pagination-btn:hover {
    background-color: #0058A3; /* Fondo azul cuando se pasa el mouse */
    color: white; /* Cambiar el color del texto a blanco */
}

.pagination-btn.active {
    background-color: #0058A3; /* Color de fondo para la página activa */
    color: white; /* Color de texto para la página activa */
    pointer-events: none; /* Desactivar clic en la página activa */
}


</style>
<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
	<div class="widget-body padding-none border-none">
		<div class="row">
			<div class="col-md-12 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
				<div class="innerAll">
					<div class="body">
						<div class="row padding">
							<div class="col-md-12">
								<div id="banner-home" class="owl-carousel owl-theme">
									<?php foreach ($listado_BannerTop as $keys => $lbtop) {
										$dealerArray = explode(",", $lbtop['dealers_id']);
										foreach ($dealerArray as $key => $value) {
											if ($value == '') { ?>
												<div class="item">
													<a href="<?php echo ($lbtop['banner_url']); ?>" target="_blank">
														<!--  -->
														<img src="../assets/gallery/banners/<?php echo ($lbtop['file_name']); ?>" alt="<?php echo ($lbtop['banner_name']); ?>" class="img-responsive padding-none border-none" onclick="clickBanner(<?php echo ($lbtop['banner_id']); ?>)" />
													</a>
												</div>
											<?php } elseif ($value == $_SESSION['dealer_id']) { ?>
												<div class="item">
													<a href="<?php echo ($lbtop['banner_url']); ?>" target="_blank">
														<img src="../assets/gallery/banners/<?php echo ($lbtop['file_name']); ?>" alt="<?php echo ($lbtop['banner_name']); ?>" class="img-responsive padding-none border-none" onclick="clickBanner(<?php echo ($lbtop['banner_id']); ?>)" />
													</a>
												</div>
									<?php } //fin if
										} //fin segundo for
									} //fin primer foreach
									?>
								</div>
							</div>
						</div>
						<div class="row">
							
								<div class="row">
									<div class="col-md-1">
										<a href="ver_perfil.php"><img id="ImgUsuario" src="../assets/images/usuarios/logouser.png"<?php echo ($data_user['image']); ?> style="width: 80px;" alt="<?php echo ($data_user['first_name'] . ' ' . $data_user['last_name']); ?>" /></a>
									</div>
									<div class="col-md-10">
										<a href="ver_perfil.php"><strong><?php echo ($data_user['first_name'] . ' ' . $data_user['last_name']); ?></strong></a><br />
										<?php echo ($data_user['headquarter']); ?><br />
										<?php echo ($data_user['email']); ?><br />
										<!-- <img src="../assets/images/wall/CHEVROLET_ng.png" width="60px" alt="concesionario" /> -->
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
								<div class="container-fluid">	
								<div class="col-md-6">
									<h5 style="text-align: center;" class="text-uppercase strong text-primary"><i class="fa fa-calendar text-regular fa-fw"></i> Programación de Cursos</h5>
									<div data-component>
										<div id="calendar_actual"></div>
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
								<div class="col-md-6">
									<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Mis Trayectorias </h5>
									<div class="separator bottom"></div>
									<ul class="team">
										<?php
										$cant_ele = 1;
										//print_r($charge_user);
										foreach ($charge_user as $key => $Data_Charges) { ?>
											<li><span class="crt"><?php echo $cant_ele; ?></span><span class="strong"><?php echo ($Data_Charges['charge']); ?></span><span class="muted"><a href="trayectoria.php?charge=<?php echo ($Data_Charges['charge_id']); ?>" target="_blank"><?php echo ($Data_Charges['charge']); ?></a></span></li>
										<?php
											$cant_ele++;
										} ?>
									</ul>
								</div>
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
								<div class="row">
									<?php foreach ($listado_BannerHis as $key2 => $lbhis) {
										$hisArray = explode(",", $lbhis['dealers_id']);
										foreach ($hisArray as $key => $valuehis) {
											if ($valuehis == '') { ?>
												<div class="col-md-4" style="box-shadow: 1px 1px 10px #888 !important">
													<a href="<?php echo ($lbhis['banner_url']); ?>" target="_blank"><img src="../assets/gallery/banners/<?php echo ($lbhis['file_name']); ?>" alt="<?php echo ($lbhis['banner_name']); ?>" class="img-responsive padding-none border-none" onclick="clickBanner(<?php echo ($lbhis['banner_id']); ?>)" /></a>
												</div>
											<?php } elseif ($valuehis == $_SESSION['dealer_id']) { ?>
												<div class="col-md-4" style="box-shadow: 1px 1px 10px #888 !important">
													<a href="<?php echo ($lbhis['banner_url']); ?>" target="_blank"><img src="../assets/gallery/banners/<?php echo ($lbhis['file_name']); ?>" alt="<?php echo ($lbhis['banner_name']); ?>" class="img-responsive padding-none border-none" onclick="clickBanner(<?php echo ($lbhis['banner_id']); ?>)" /></a>
												</div>
									<?php	} //fin if
										} //fin segundo foreach
									} //fin primer foreach 
									?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="row">
									<?php foreach ($listado_BannerCal as $key3 => $lbcal) {
										$calArray = explode(",", $lbcal['dealers_id']);
										foreach ($calArray as $key => $valuecal) {
											if ($valuecal == '') { ?>
												<a href="<?php echo ($lbcal['banner_url']); ?>">
													<img style="box-shadow: 1px 1px 10px #888 !important" src="../assets/gallery/banners/<?php echo ($lbcal['file_name']); ?>" alt="<?php echo ($lbcal['banner_name']); ?>" class="img-responsive padding-none border-none" onclick="clickBanner(<?php echo ($lbcal['banner_id']); ?>)" />
												</a>
											<?php } elseif ($valuecal == $_SESSION['dealer_id']) { ?>
												<a href="<?php echo ($lbcal['banner_url']); ?>">
													<img style="box-shadow: 1px 1px 10px #888 !important" src="../assets/gallery/banners/<?php echo ($lbcal['file_name']); ?>" alt="<?php echo ($lbcal['banner_name']); ?>" class="img-responsive padding-none border-none" onclick="clickBanner(<?php echo ($lbcal['banner_id']); ?>)" />
												</a>
									<?php } //fin if
										} //fin segundo foreach
									} //fin primer foreach 
									?>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<!-- <h5 class="text-uppercase strong text-primary"><i class="fa fa-calendar text-regular fa-fw"></i> Programación de Cursos IBT - VCT</h5>
									<div data-component>
										<div id="calendar_actual"></div>
									</div> -->
								</div>
								<!--<div class="separator bottom"></div>
								<div class="row center">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<a href="foros_ventas.php"><img src="../assets/gallery/banners/comunicate.jpg" class="img-responsive" style="width: 400px;"></a>
									</div>
									<div class="col-md-3"></div>
								</div>-->
							</div>
							<div class="col-md-4">
								<!-- <a class="twitter-timeline" data-lang="es" data-width="100%" data-height="600" href="https://twitter.com/ChevroletCL">Chevrolet Chile</a>
								<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> -->
								<div id="fb-root"></div>
								<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v6.0"></script>
								
								<div class="fb-page" data-href="https://www.instagram.com/acdelco.colombia" data-tabs="timeline" data-width="400" data-height="700" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.instagram.com/acdelco.colombia" class="fb-xfbml-parse-ignore"><a href="https://www.instagram.com/acdelco.colombia">ACDelco Colombia</a></blockquote></div>
							</div>
						</div>
						<div class="separator bottom"></div>
						<div class="row">
							<div class="widget widget-heading-simple widget-body-white margin-none border-none">
								<!-- <div class="widget-head" style="padding-left: 15px;">
									<h4 class="heading glyphicons list"><i></i> </h4>
								</div> -->
								<div class="widget-body margin-none border-none">
									<span class="text-uppercase strong text-primary"><i class="fa fa-list-alt text-regular fa-fw"></i> Mis Cursos </span>
									<br>
									
									<?php
// Definir el número de registros por página
$registrosPorPagina = 10;

// Capturar la búsqueda si existe
$busqueda = isset($_GET['search']) ? $_GET['search'] : '';

// Filtrar los resultados de acuerdo con la búsqueda
$Datos_CursosTomados_Filtrados = array_filter($Datos_CursosTomados, function($curso) use ($busqueda) {
    return stripos($curso['newcode'], $busqueda) !== false ||  // Filtra por código del curso
           stripos($curso['course'], $busqueda) !== false;     // Filtra por nombre del curso
});

// Paginación
$pagina = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1; // Página actual
$totalRegistros = count($Datos_CursosTomados_Filtrados); // Total de registros filtrados
$totalPaginas = ceil($totalRegistros / $registrosPorPagina); // Número total de páginas
$indiceInicio = ($pagina - 1) * $registrosPorPagina; // Índice del primer registro de la página actual
$indiceFinal = min($indiceInicio + $registrosPorPagina, $totalRegistros); // Último índice en la página actual

// Obtener solo los registros correspondientes a la página actual
$Datos_PaginaActual = array_slice($Datos_CursosTomados_Filtrados, $indiceInicio, $registrosPorPagina);
?>



<div class="row">
    <div class="col-md-12">
        <form method="GET" action="">
            <input type="text" id="search-input" name="search" class="form-control" placeholder="Buscar cursos..." style="float: right; width: 250px;" value="<?php echo isset($_GET['search']) ? $_GET['search'] : ''; ?>">
						<!-- <label for="search-input" style="float: right; margin-left: 10px;">Buscar:&nbsp;</label> -->
        </form>
    </div>
</div>




<table id="Table_DataTomados" class="display table table-striped table-condensed table-hover table-primary table-vertical-center">
    <thead style="color: white;">
        <tr>
            <th style="width: 25%;" class="center">C&oacute;digo</th>
            <th>Nombre</th>
            <th style="width: 5%;" class="center">Tipo</th>
            <th style="width: 5%;" class="center">Nota</th>
            <th style="width: 5%;" class="center">Fecha</th>
            <th style="width: 5%;" class="center">Aprobó?</th>
            <th style="width: 5%;" class="center">Certificado</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($Datos_PaginaActual as $curso) :
				  // echo'<pre>';
					// print_r($curso);
					// echo'</pre>';
					?>
            <tr>
                <td class="strong left">
                    <a href="course_detail.php?token=<?php echo md5('GMAcademy'); ?>&_valSco=<?php echo md5('GMColmotores'); ?>&opcn=ver&id=<?php echo $curso['course_id']; ?>">
                        <?php echo $curso['newcode']; ?>
                    </a>
                </td>
                <td class="left"><?php echo $curso['course']; ?></td>
                <td class="strong center"><?php echo $curso['type']; ?></td>
                <td class="center"><?php echo number_format($curso['score'], 0); ?></td>
                <td class="center"><?php echo $curso['date_creation']; ?></td>
                <td class="center">
                    <?php if ($curso['score'] >= 70) { ?>
                        <span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
                    <?php } else { ?>
                        <span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
                    <?php } ?>
                </td>
                <?php if ($curso['score'] >= 70) { ?>
                    <td class="center">
                        <a href="certificado.php?course_id=<?php echo $curso['course_id']; ?>&date=<?php echo $curso['date_creation']; ?>" class="btn-action glyphicons certificate btn-info" target="_blank"><i></i></a>
                    </td>
                <?php } else { ?>
                    <td class="center btn-danger">
                        <span class="btn-action btn-danger">Reprobó</span>
                    </td>
                <?php } ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script>
    document.getElementById('search-input').addEventListener('keyup', function () {
        var input = this.value.toLowerCase();
        var rows = document.querySelectorAll('#Table_DataTomados tbody tr');

        rows.forEach(function (row) {
            var cells = row.querySelectorAll('td');
            var match = false;

            cells.forEach(function (cell) {
                if (cell.textContent.toLowerCase().includes(input)) {
                    match = true;
                }
            });

            if (match) {
                row.style.display = ''; // Mostrar la fila si coincide
            } else {
                row.style.display = 'none'; // Ocultar la fila si no coincide
            }
        });
    });
</script>

<br>

								
									<!-- Enlaces de paginación -->
							<div class="pagination" style="margin-top: 10px;">
									<?php if ($pagina > 1) : ?>
											<a href="?pagina=<?php echo ($pagina - 1); ?>&search=<?php echo $busqueda; ?>" class="pagination-btn">&laquo; Anterior</a>
									<?php endif; ?>

									<?php for ($i = 1; $i <= $totalPaginas; $i++) : ?>
											<a href="?pagina=<?php echo $i; ?>&search=<?php echo $busqueda; ?>" class="pagination-btn <?php echo ($i == $pagina) ? 'active' : ''; ?>"><?php echo $i; ?></a>
									<?php endfor; ?>

									<?php if ($pagina < $totalPaginas) : ?>
											<a href="?pagina=<?php echo ($pagina + 1); ?>&search=<?php echo $busqueda; ?>" class="pagination-btn">Siguiente &raquo;</a>
									<?php endif; ?>
							</div>




								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>