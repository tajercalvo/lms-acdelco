<div class="widget widget-heading-simple widget-body-gray widget-employees">
	<div class="widget-body padding-none">
		<div class="row">
			<div class="col-md-12 detailsWrapper">
				<div class="ajax-loading hide">
					<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
				</div>
				<div class="innerAll">
					<div class="title">
						<div class="row">
							<div class="col-md-6">
								<h3 class="text-primary">Información Personal</h3>
							</div>
							<div class="col-md-6 text-right">
								<h3 class="text-primary">Información Académica</h3>
							</div>
						</div>
					</div>
					<hr/>
					<div class="body">
						<div class="row padding">
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-12 overflow-hidden widget widget-body-white"><br>
										<h5 class="text-uppercase strong text-primary"><i class="fa fa-laptop text-regular fa-fw"></i> Mis Cursos Inscritos (<?php echo(count($DatosDisponibleInsc)); ?> Cursos) <a href="">[Ver Todos]</a></h5><br>
										<div class="ajax-loading" id="LoadingInscritos">
											<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
										</div>
										<div id="news-featured-2-1" class="owl-carousel owl-theme">
											<?php 
											$ct_row = 0;
											foreach ($DatosDisponibleInsc as $iID => $DatosCursos_Inscrito) { ?>
												<?php if($ct_row == 0){ ?><div class="item"><?php } ?>
													<?php $ct_row++; ?>
													<div class="box-generic padding-none margin-none overflow-hidden">
														<div class="relativeWrap overflow-hidden" style="height:175px">
															<img src="../assets/images/distinciones/<?php echo $DatosCursos_Inscrito['image']; ?>" alt="" class="img-responsive padding-none border-none" />
															<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.8) !important">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong><?php echo $DatosCursos_Inscrito['code']; ?>: <?php echo substr($DatosCursos_Inscrito['course'],0,35); ?>...</strong>
																		<p class="text-medium margin-none"><a href="course_detail.php?token=<?php echo(md5("GMAcademy")); ?>&_valSco=<?php echo(md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Inscrito['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Continuar</strong></a></p>
																	</div>
																</div>
															</div>
														</div>
														<div class="ribbon-wrapper">
															<div class="ribbon" style="background-color: <?php if( $DatosCursos_Inscrito['disposicion'] == "Autodirigidos"){ echo("red"); }else{ echo("green"); } ?>;font-size: 10px;"><?php echo($DatosCursos_Inscrito['disposicion']); ?></div>
														</div>
													</div>
												<?php if($ct_row == 2){ ?></div><?php $ct_row=0; } ?>
											<?php } ?>
											<?php if($ct_row > 0){ ?></div><?php } ?>
										</div>
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-12 overflow-hidden widget widget-body-white"><br>
										<h5 class="text-uppercase strong text-primary"><i class="fa fa-laptop text-regular fa-fw"></i> Cursos Virtuales Disponibles (<?php echo(count($DatosDisponibleSinInsc)); ?> Cursos)</h5><br>
										<div class="ajax-loading" id="LoadingDisponibles">
											<i class="fa fa-spinner fa fa-spin fa fa-4x"></i>
										</div>
										<div id="news-featured-2" class="owl-carousel owl-theme">
											<?php 
											$ct_row = 0;
											foreach ($DatosDisponibleSinInsc as $iID => $DatosCursos_Disponibles) { ?>
												<?php if($ct_row == 0){ ?><div class="item"><?php } ?>
													<?php $ct_row++; ?>
													<div class="box-generic padding-none margin-none overflow-hidden">
														<div class="relativeWrap overflow-hidden" style="height:175px">
															<img src="../assets/images/distinciones/<?php echo $DatosCursos_Disponibles['image']; ?>" alt="" class="img-responsive padding-none border-none" />
															<div class="fixed-bottom bg-inverse-faded" style="background:rgba(66,66,66,0.8) !important">
																<div class="media margin-none innerAll">
																	<div class="media-body text-white">
																		<strong><?php echo $DatosCursos_Disponibles['code']; ?>: <?php echo substr($DatosCursos_Disponibles['course'],0,35); ?>...</strong>
																		<p class="text-medium margin-none"><a href="course_detail.php?token=<?php echo(md5("GMAcademy")); ?>&_valSco=<?php echo(md5("GMColmotores")); ?>&opcn=ver&id=<?php echo $DatosCursos_Disponibles['course_id']; ?>"><i class="fa fa-fw fa-youtube-play"></i> <strong>Inscribirme</strong></a></p>
																	</div>
																</div>
															</div>
														</div>
														<div class="ribbon-wrapper">
															<div class="ribbon" style="background-color: <?php if( $DatosCursos_Disponibles['disposicion'] == "Autodirigidos"){ echo("red"); }else{ echo("green"); } ?>;font-size: 10px;"><?php echo($DatosCursos_Disponibles['disposicion']); ?></div>
														</div>
													</div>
												<?php if($ct_row == 2){ ?></div><?php $ct_row=0; } ?>
											<?php } ?>
											<?php if($ct_row > 0){ ?></div><?php } ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 overflow-hidden widget widget-body-white"><br>
								<div class="row">
									<div class="col-md-3">
										<img id="ImgUsuario" src="../assets/images/usuarios/<?php echo($data_user['image']); ?>" style="width: 80px;" alt="<?php echo($data_user['first_name'].' '.$data_user['last_name']); ?>" />
									</div>
									<div class="col-md-9">
										<strong><?php echo($data_user['first_name'].' '.$data_user['last_name']); ?></strong><br/>
										<?php echo($data_user['headquarter']); ?><br/>
											<?php echo($data_user['email']); ?><br/>
										<img src="../assets/images/wall/CHEVROLET_ng.png" width="60px" alt="concesionario"/>
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
								<div class="row">
									<h5 class="text-uppercase strong text-primary"><i class="fa fa-calendar text-regular fa-fw"></i> Programación de Cursos </h5>
									<div data-component>
										<div id="calendar_actual"></div>
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
								<div class="row">
									<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Mis Trayectorias Académicas </h5>
									<ul class="team">
										<?php 
										$cant_ele = 1;
										foreach ($charge_user as $key => $Data_Charges) { ?>
											<li><span class="crt"><?php echo $cant_ele; ?></span><span class="strong"><?php echo($Data_Charges['charge']); ?></span><span class="muted"><?php echo($Data_Charges['charge']); ?></span></li>
										<?php 
										$cant_ele++;
										} ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>