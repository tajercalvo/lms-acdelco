<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_logistica.php');
$location = 'reporting';
$locData = true;
$qtip = 'qtip';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<style type="text/css">
		a.link1::before {
		   position: relative;
		   cursor: pointer;
		}
	</style>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons fast_food"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_logistica.php">Reporte Logística</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte Logistica</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Este reporte contiene por concesionario la cantidad de prestamos de sala que ha realizado en un intervalo de tiempo. Una vez consultada puede descargarla para realizar operaciones localmente (En su computador).</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<h5><a href="rep_logistica_excel.php?dealer_id=<?php echo($_POST['dealer_id']); ?>&start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			</div>
		</div>
		<form action="rep_logistica.php" method="post">
			<div class="row">
				<div class="col-md-10">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="dealer_id" style="padding-top:8px;">Concesionario:</label>
						<div class="col-md-10">
							<select style="width: 100%;" id="dealer_id" name="dealer_id" >
								<?php if(isset($_SESSION['max_rol'])&&$_SESSION['max_rol']>=5){ ?>
									<option value="0">Todos los concesionarios</option>
								<?php } ?>
								<?php foreach ($datosCursos as $key => $Data_Cursos) { ?>
									<option value="<?php echo($Data_Cursos['dealer_id']); ?>" <?php if(isset($_POST['dealer_id'])&& $_POST['dealer_id']==$Data_Cursos['dealer_id']){ ?>selected="selected"<?php } ?> ><?php echo($Data_Cursos['dealer']); ?></option>
								<?php } ?>
							</select>
						</div>

					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
					<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-3 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
						<div class="col-md-9 input-group date">
					    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" autocomplete="off" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-3 control-label" for="end_date_day" style="padding-top:8px;">Fecha Final:</label>
						<div class="col-md-9 input-group date">
					    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" autocomplete="off" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="widget widget-heading-simple widget-body-white">
	<!-- Widget heading -->
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Información: <?php echo($cantidad_datos); ?> Prestamos de sala</h4>
	</div>
	<!-- // Widget heading END -->
	<div class="widget-body">
		<!-- Table elements-->
		<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
			<!-- Table heading -->
			<thead>
				<tr>
					<th data-hide="phone,tablet" style="width: 10%;">PROGRAMACION</th>
					<th data-hide="phone,tablet" style="width: 15%;">CODIGO</th>
					<th data-hide="phone,tablet" style="width: 15%;">CURSO</th>
					<th data-hide="phone,tablet" style="width: 10%;">INICIO</th>
					<th data-hide="phone,tablet" style="width: 10%;">FINALIZACION</th>
					<th data-hide="phone,tablet" style="width: 5%;">TAMAÑO</th>
					<th data-hide="phone,tablet" style="width: 15%;">LUGAR</th>
					<th data-hide="phone,tablet" style="width: 10%;">CONCESIONARIO</th>
					<th data-hide="phone,tablet" style="width: 10%;">SEDE</th>
					<th data-hide="phone,tablet" style="width: 5%;">INSCRITOS</th>
				</tr>
			</thead>
			<!-- // Table heading END -->
			<tbody>
				<?php
				if($cantidad_datos > 0){
					$NOW_data 	= date("Y-m-d")." ".(date("H")).":".date("i:s");
					foreach ($datosLogistica_all as $iID => $data) {

						?>
					<tr>
						<?php //Columna de schedule_id
							if( $NOW_data > $data['end_date'] && $data['type'] == "IBT" ){ ?>
                                <td><a onclick="costo_logistica('<?php echo $data['schedule_id']; ?>', '<?php echo $data['course_id']; ?>', '<?php echo $data['inscriptions']; ?>', '<?php echo $data['dealer_id']; ?>')" style="cursor: pointer;">
                                	<?php echo($data['schedule_id']); ?></a></td>
							<?php }else{ ?>
								<td><?php echo($data['schedule_id']); ?></td>
							<?php } 
						?>
						<?php //Columna de codigo
							$feeding = explode("-", $data['feeding']);
							if( $feeding[0] != "FALSE" && $feeding[1] == 1 && $data['type'] == "IBT" ){ ?>
								<td><div><?php echo($data['newcode']); ?></div>
									<label class="control-label" style="color: green;font-size: 10px;">Legalizado: $<?php echo($feeding[0]); ?></label> </td>
							<?php }else if( $feeding[0] != "FALSE" && $feeding[1] == 0 && $data['type'] == "IBT" ){ ?>
								<td><div><?php echo($data['newcode']); ?></div>
									<label class="control-label" style="color: red;font-size: 10px;">Pendiente por aprobar legalizado</label> </td>
							<?php }else if( $feeding[0] != "FALSE" && $feeding[1] == 2 && $data['type'] == "IBT" ){ ?>
									<td><div><?php echo($data['newcode']); ?></div>
										<label class="control-label" style="color: red;font-size: 10px;">Legalización Rechazada</label> </td>
							<?php }else{ ?>
								<td><?php echo($data['newcode']); ?></td>
							<?php } 
						?>
						<td><?php echo($data['course']); ?></td>
						<?php //Columna de fecha inicio
							if( $feeding[0] != "FALSE" && $feeding[1] == 1 && $data['type'] == "IBT" ){ ?>
								<td><span class="label text-success" style="font-size:13px;white-space:unset;"><?php echo($data['start_date']); ?></span></td>
							<?php }else if( $feeding[0] != "FALSE" && $feeding[1] == 0 && $data['type'] == "IBT" ){ ?>
								<td><span class="label text-danger" style="font-size:13px;white-space:unset;"><?php echo($data['start_date']); ?></span></td>
							<?php }else if( $feeding[0] != "FALSE" && $feeding[1] == 2 && $data['type'] == "IBT" ){ ?>
								<td><span class="label text-danger" style="font-size:13px;white-space:unset;"><?php echo($data['start_date']); ?></span></td>
							<?php }else{ ?>
								<td><?php echo($data['start_date']); ?></td>
							<?php } 
						?>
						<?php //Columna de fecha fin
							if( $feeding[0] != "FALSE" && $feeding[1] == 1 && $data['type'] == "IBT" ){ ?>
								<td><span class="label text-success" style="font-size:13px;white-space:unset;"><?php echo($data['end_date']); ?></span></td>
							<?php }else if( $feeding[0] != "FALSE" && $feeding[1] == 0 && $data['type'] == "IBT" ){ ?>
								<td><span class="label text-danger" style="font-size:13px;white-space:unset;"><?php echo($data['end_date']); ?></span></td>
							<?php }else if( $feeding[0] != "FALSE" && $feeding[1] == 2 && $data['type'] == "IBT" ){ ?>
								<td><span class="label text-danger" style="font-size:13px;white-space:unset;"><?php echo($data['end_date']); ?></span></td>
							<?php }else{ ?>
								<td><?php echo($data['end_date']); ?></td>
							<?php } 
						?>
						<td><?php echo($data['min_size'].' - '.$data['max_size']); ?></td>
						<td align="center"><?php echo($data['living']); ?></td>
						<td align="center"><?php echo($data['dealer']); ?></td>
						<td align="center"><?php echo($data['headquarter']); ?></td>
						<td align="center"><?php echo($data['inscriptions']); ?></td>
					</tr>
				<?php } } ?>
			</tbody>
		</table>
		<!-- // Table elements END -->
		<!-- Total elements
		<div class="form-inline separator bottom small">
			Total de inscritos: <strong class='text-primary'><?php echo($cantidad_datos); ?></strong> | Invitados: <strong class='text-primary'><?php echo($invitados); ?></strong> | Asistieron: <strong class='text-success'><?php echo($asistieron_tot); ?></strong> | No Asitieron: <strong class='text-danger'><?php echo($noasistieron_tot); ?> </strong> | Aprobaron: <strong class='text-success'><?php echo($aprobaron); ?></strong> | No Aprobaron: <strong class='text-danger'><?php echo($noaprobaron); ?></strong>
		</div>
		 // Total elements END -->
	</div>
</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">

					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>

<!-- MODAL - TABLE -->
<form action="" enctype="multipart/form-data" method="post" id="modal_form_Logistica"><br><br>
	<div class="modal fade" id="ModalLogistica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:30px;">
		<div class="modal-dialog" style="width: 55%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Cobros de Logistica</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Table -->
				<from>
			        <table id="Tabla_Alimentacion" class="table">
			            <thead align="center">
			                <tr>
			                    <th></th>
			                    <th style="text-align: center;">TIPO DE ALIMENTACIÓN</th>
			                    <th style="text-align: center;">COSTO</th>
			                    <th style="text-align: center;">FACTURA ADJUNTA</th>
			                    <th></th>
			                </tr>
			            </thead>
			            <tbody align="center" id="ContenidoTabla">
			                <tr></tr>
			            </tbody>
			        </table>
			    </from>
				<!-- END Table -->
				<!-- Row -->
				<div class="row" id="domicilio">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Costo Total Domicilio (Opcional): </label>
						<input type="text" id="cost_dom" name="cost_dom" class="form-control col-md-8" placeholder="Valor del domicilio" />
					</div>
					<div id="aprobacion" class="col-md-5">
						<label for="status_id" class="control-label">Estado de Aprobación: </label>
						<div class="col-md-9">
							<select style="width: 100%;" id="status_id" name="status_id">
							</select>
						</div>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="separator bottom"></div>
				<!-- Row -->
				<div class="row" id="observaciones">
					<div class="col-md-1">
					</div>
					<div class="col-md-8">
						<label class="control-label">Observaciones: </label>
						<textarea class="form-control" name="description" id="description" rows="2" cols="80"></textarea>
					</div>
					<div class="col-md-3">
					</div>
				</div>
				<!-- Row END-->

				<!-- Row Datos Ocultos -->
				<div class="row">
					<div class="col-md-12">
						<label id="mensaje_suma" class="control-label" style="color: red;font-size: 12px;"></label>
						<input type="hidden" id="schedule_id" value="" name="schedule_id">
						<input type="hidden" id="inscritos" value="" name="inscritos">
						<input type="hidden" id="opcn" value="crear" name="opcn">
						<input type="hidden" id="horas_curso" value="" name="horas_curso">
						<input type="hidden" id="horas_inicio" value="" name="horas_inicio">
						<input type="hidden" id="horas_fin" value="" name="horas_fin">
						<input type="hidden" id="dealer_id" value="" name="dealer_id">
						<input type="hidden" id="costo_unit_ref" value="<?php echo($refrigerio); ?>" name="costo_unit_ref">
						<input type="hidden" id="costo_unit_alm" value="<?php echo($almuerzo); ?>" name="costo_unit_alm">
					</div>
				</div>
				<!-- Row END--> 

				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnCreacion" class="btn btn-primary">Guardar Datos</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- END MODAL - TABLE -->

		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_logistica.js?varRand=<?php echo(rand(10,999999)); ?>"></script>
</body>
</html>
