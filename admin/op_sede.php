<?php include('src/seguridad.php'); ?>
<?php include('controllers/sedes.php');
$location = 'distribucion';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido General -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Estás aquí: </li>
					<li><a href="../admin/" class="glyphicons building"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/sedes.php">Configuración Sedes</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Configuración Sedes </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
								</div>
								<div class="col-md-2">
									<?php if(isset($_GET['back'])&&$_GET['back']=="inicio"){ ?>
										<h5><a href="../admin/" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php }else{ ?>
										<h5><a href="sedes.php" class="glyphicons no-js unshare" ><i></i>Regresar</a><h5>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="widget widget-heading-simple widget-body-white">
						<!-- Widget heading -->
						<div class="widget-head">
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?><h4 class="heading glyphicons list"><i></i> Agregar ítem</h4><?php } ?>
							<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')) { ?><h4 class="heading glyphicons list"><i></i> Editar ítem</h4><?php } ?>
						</div>
						<!-- // Widget heading END -->
						<div class="widget-body innerAll inner-2x">
							<form id="formulario_data" method="post" autocomplete="off">
								<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="headquarter" style="padding-top:8px;">Sede:</label>
												<div class="col-md-7">
													<input class="form-control" id="headquarter"
														name="headquarter" type="text"
														placeholder="Nombre de la sede" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>
														value="<?php echo $registroConfiguracion['headquarter']; ?>"<?php } ?> />
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="dealer_id" style="padding-top:8px;">Empresa:</label>
												<div class="col-md-7">
													<select style="width: 100%;" id="dealer_id" name="dealer_id">
													<?php foreach ($listadosConcesionarios as $key => $Data_listados) { ?>
														<option value="<?php echo($Data_listados['dealer_id']); ?>" <?php if(isset($registroConfiguracion['dealer_id']) &&$registroConfiguracion['dealer_id']==$Data_listados['dealer_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_listados['dealer']); ?></option>
													<?php } ?>
													</select>
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-2">
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="type_headquarter" style="padding-top:8px;">Tipo de Sede:</label>
												<div class="col-md-7">
													<select style="width: 100%;" id="type_headquarter" name="type_headquarter">
														<option value="Principal" <?php if(isset($registroConfiguracion['type_headquarter']) &&$registroConfiguracion['type_headquarter']=="Principal"){ ?>selected="selected"<?php } ?>>Principal</option>
														<option value="Sede" <?php if(isset($registroConfiguracion['type_headquarter']) &&$registroConfiguracion['type_headquarter']=="Sede"){ ?>selected="selected"<?php } ?>>Sede</option>
													</select>
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-2">
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="address1" style="padding-top:8px;">Dirección:</label>
												<div class="col-md-7"><input class="form-control" id="address1" name="address1" type="text" placeholder="Dirección de la sede" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['address1']; ?>"<?php } ?> /></div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="address2" style="padding-top:8px;">Dirección 2:</label>
												<div class="col-md-7"><input class="form-control" id="address2" name="address2" type="text" placeholder="Dirección de la sede" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['address2']; ?>"<?php } ?> /></div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-2">
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="phone1" style="padding-top:8px;">Teléfono:</label>
												<div class="col-md-7"><input class="form-control" id="phone1" name="phone1" type="text" placeholder="Teléfono de la sede" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['phone1']; ?>"<?php } ?> /></div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="phone2" style="padding-top:8px;">Teléfono 2:</label>
												<div class="col-md-7"><input class="form-control" id="phone2" name="phone2" type="text" placeholder="Teléfono de la sede" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['address2']; ?>"<?php } ?> /></div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-2">
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="email1" style="padding-top:8px;">e-Mail:</label>
												<div class="col-md-7"><input class="form-control" id="email1" name="email1" type="text" placeholder="eMail de la sede" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['email1']; ?>"<?php } ?> /></div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="email2" style="padding-top:8px;">e-Mail 2:</label>
												<div class="col-md-7"><input class="form-control" id="email2" name="email2" type="text" placeholder="eMail de la sede" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['email2']; ?>"<?php } ?> /></div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-2">
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<div class="row innerLR">
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="area_id" style="padding-top:8px;">Ciudad:</label>
												<div class="col-md-7">
													<select style="width: 100%;" id="area_id" name="area_id">
													<?php foreach ($listadosCiudades as $key => $Data_listados) { ?>
														<option value="<?php echo($Data_listados['area_id']); ?>" <?php if(isset($registroConfiguracion['area_id']) &&$registroConfiguracion['area_id']==$Data_listados['area_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_listados['area']); ?></option>
													<?php } ?>
													</select>
												</div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-5">
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="postal_code" style="padding-top:8px;">Código Postal:</label>
												<div class="col-md-7"><input class="form-control" id="postal_code" name="postal_code" type="text" placeholder="Código postal" <?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar' || $_GET['opcn']=='ver')) { ?>value="<?php echo $registroConfiguracion['postal_code']; ?>"<?php } ?> /></div>
											</div>
											<!-- // Group END -->
										</div>
										<!-- // Column END -->
										<!-- Column -->
										<div class="col-md-2">
										</div>
										<!-- // Column END -->
									</div>
									<!-- // Row END -->
									<br>
									<!-- Row -->
									<br>
									<div class="row innetLR">
										<!-- Column -->
										<div class="col-md-5">
											<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
											<!-- Group -->
											<div class="form-group">
												<label class="col-md-5 control-label" for="status_id" style="padding-top:8px;">Estado:</label>
												<div class="col-md-7">
													<select style="width: 100%;" id="status_id" name="status_id">
														<option value="1" <?php if($registroConfiguracion['status_id']=="1"){ ?>selected="selected"<?php } ?>>Activo</option>
														<option value="2" <?php if($registroConfiguracion['status_id']=="2"){ ?>selected="selected"<?php } ?>>Inactivo</option>
													</select>
												</div>
											</div>
											<!-- // Group END -->
											<?php } ?>
										</div>
										<!-- // Column END -->
									</div>
								<!-- // Row END -->
								<div class="separator"></div>
										<!-- Form actions -->
										<div class="form-actions">
											<?php if(isset($_GET['opcn'])&&($_GET['opcn']=='nuevo')) { ?>
												<button type="submit" class="btn btn-success" id="crearElemento"><i class="fa fa-check-circle"></i> Crear</button>
												<input type="hidden" id="opcn" name="opcn" value="crear">
											<?php }elseif(isset($_GET['opcn'])&&($_GET['opcn']=='editar')){ ?>
												<input type="hidden" id="idElemento" name="idElemento" value="<?php echo $_GET['id']; ?>">
												<input type="hidden" id="opcn" name="opcn" value="editar">
												<button type="submit" class="btn btn-success" id="editarElemento"><i class="fa fa-check-circle"></i> Editar</button>
											<?php } ?>
											<button type="button" class="btn btn-default" id="retornaElemento"><i class="fa fa-times"></i> Cancelar</button>
										</div>
										<!-- // Form actions END -->
							</form>
						</div>
					</div>
						<!-- Nuevo ROW-->
					<div class="row row-app">
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido General -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/sedes.js?varRand=<?= rand(10,999999);?>"></script>
</body>
</html>
