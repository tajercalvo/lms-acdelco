$(document).ready(function () {
    init();
});

function init() {
    $.ajax({
        type: "post",
        url: "../controllers/asistencia_tecnica.php",
        data: {opcn: 'init'},
        dataType: "json",
        success: function (data) {

            $('#chat_list').html(data.list_chat);
            $("#chat_list").animate({ scrollTop: $('#chat_list')[0].scrollHeight}, 1000);
        }
    });
}

setInterval(function(){ init() }, 3000);

$('#frm_enviar').submit(function (e) {
    e.preventDefault();
        var archivos = document.getElementById("archivo");
        var file = archivos.files[0] ? archivos.files[0] : '' ;
        console.log('archivo'+file)
    if( file != '' ){
      console.log('con archivo');
      var datos_form = $(this).serializeArray();
      $("#archivo").val('');
      subir_archivo(datos_form, file);
      return;
    }else{
      console.log('sin archivo')
    }


    var datos = $( this ).serializeArray();

    $('#message').val('');
    datos.push({name: "opcn", value: 'insertar'});
    $.ajax({
        type: "post",
        url: "../controllers/asistencia_tecnica.php",
        data: datos,
        dataType: "json",
        success: function (data) {
            init();
        }
    });

});

function cerrar_ticket() {
  $.ajax({
    url: "../controllers/asistencia_tecnica.php",
    type: 'POST',
    dataType: 'json',
    data: {opcn: 'cerrar_ticket'},
  })
  .done(function( data ) {
    console.log( data );
    $('#chat_list').html('');
  })
  .fail(function() {
    console.log("error");
  })

}

/*Aqui para cambiar de camara*/
/*
Copyright 2017 Google Inc.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

'use strict';

var videoElement = document.querySelector('video');
var audioSelect = document.querySelector('select#audioSource');
var videoSelect = document.querySelector('select#videoSource');

navigator.mediaDevices.enumerateDevices()
  .then(gotDevices).then(getStream).catch(handleError);

audioSelect.onchange = getStream;
videoSelect.onchange = getStream;

function gotDevices(deviceInfos) {
  for (var i = 0; i !== deviceInfos.length; ++i) {
    var deviceInfo = deviceInfos[i];
    var option = document.createElement('option');
    option.value = deviceInfo.deviceId;
    if (deviceInfo.kind === 'audioinput') {
      option.text = deviceInfo.label ||
        'microphone ' + (audioSelect.length + 1);
      audioSelect.appendChild(option);
    } else if (deviceInfo.kind === 'videoinput') {
      option.text = deviceInfo.label || 'camera ' +
        (videoSelect.length + 1);
      videoSelect.appendChild(option);
    } else {
      console.log('Found one other kind of source/device: ', deviceInfo);
    }
  }
}

function getStream() {
  if (window.stream) {
    window.stream.getTracks().forEach(function(track) {
      track.stop();
    });
  }

  var constraints = {
    audio: {
      deviceId: {exact: audioSelect.value}
    },
    video: {
      deviceId: {exact: videoSelect.value}
    }
  };

  navigator.mediaDevices.getUserMedia(constraints).
    then(gotStream).catch(handleError);
}

function gotStream(stream) {
  window.stream = stream; // make stream available to console
  videoElement.srcObject = stream;
}

function handleError(error) {
  console.log('Error: ', error);
}
