<?php 

@session_start();
// if( !isset($_SESSION['idUsuario']) ){
//   header("location: ../index.php");
// }
// @session_start();
// print_r($_SESSION);
// $_SESSION['NameUsuario']
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Asistencia técnica</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <!-- Material Design -->
    <link rel="stylesheet" href="dist/css/bootstrap-material-design.css">
    <link rel="stylesheet" href="dist/css/ripples.min.css">
    <link rel="stylesheet" href="dist/css/MaterialAdminLTE.css">
    <link rel="stylesheet" href="dist/css/skins/skin-md-blue.css">
    <link rel="stylesheet" href="dist/css/animate.css">
    <link rel="stylesheet" href="plugins/intro.js/introjs.css">

    <link rel="stylesheet" href="dist/css/style.css">
    <link rel="stylesheet" href="dist/css/modulo.css">
    <link rel="shortcut icon" href="../../assets/ludus.ico">

    <style>
    #mydiv {
          position: absolute;
          z-index: 9;
          background-color: #f1f1f1;
          border: 1px solid #d3d3d3;
          text-align: center;
      }

      #mydivheader {
          padding: 10px;
          cursor: move;
          z-index: 10;
          background-color: #1e477d;
          color: #fff;
      }
</style>


</head>

<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">
        <!-- ======================================================== -->
        <!-- Main Header -->
        <!-- ======================================================== -->
        <header class="main-header">

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation" style="margin-left: 0; text-align: center;">
                <span class="" data-step="1" data-intro="<span class='label label-danger round'>1</span> Aquí encontrará el nombre del curso actual.">
                    <h1 class="tituloCurso">
                        <strong id="titulo_curso">ASISTENCIA TECNICA</strong>
                    </h1>
                </span>
                <!-- ================================================ -->
                <!-- Sidebar toggle button-->
                <!-- ================================================ -->
                <a class="sidebar-toggle" style="course: pointer">
                    <span class="sr-only">Toggle navigation</span>
                    <img src="dist/img/LogoGMICA-02.png" alt="" style="width: 160px; margin: -11px 0;">
                </a>
                <!-- ================================================ -->
                <!-- END Sidebar toggle button -->
                <!-- ================================================ -->

                <!-- ================================================ -->
                <!-- Navbar Right Menu -->
                <!-- ================================================ -->
                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu" data-step="4" data-intro="<span class='label label-danger round'>4</span> Este es el control para activar o inactivar los audios de las diferentes presentaciones.">
                            <!-- Menu toggle button -->
                            <a class="dropdown-toggle" style="cursor: pointer" data-id="1" id="sonido">
                            </a>
                        </li>
                        <!-- ============================================================ -->
                        <!-- User Account Menu -->
                        <!-- ============================================================ -->
                        <li class="dropdown user user-menu" data-step="3" data-intro="<span class='label label-danger round'>3</span> El sistema debe saludarlo aquí, de lo contrario no continúe">
                            <!-- Menu Toggle Button -->
                            <a style="cursor: pointer" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img id="imgUser" src="../../assets/images/usuarios/<?php echo $_SESSION['imagen'];?>" class="user-image" style="background-color: white;" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs" id="nameUser"><?php echo $_SESSION['NameUsuario'];?></span>
                            </a>
                        </li>
                        <!-- ============================================================ -->
                        <!-- END User Account Menu -->
                        <!-- ============================================================ -->

                        <!-- ============================================================ -->
                        <!-- Boton de Salida -->
                        <!-- ============================================================ -->
                        <li data-step="7" data-intro="<span class='label label-danger round'>7</span> Para que la nota del curso quede registrada, deberá dar clic AQUI. Deberá estar seguro de registrar su salida cuando complete el módulo en la plataforma.">
                            <a style="cursor: pointer" title="Salir"><i class="fa fa-sign-out"></i></a>
                        </li>
                        <!-- ============================================================ -->
                        <!-- END Boton de Salida -->
                        <!-- ============================================================ -->

                        <!-- ============================================================ -->
                        <!-- Control Sidebar Toggle Button -->
                        <!-- ============================================================ -->
                        <!-- <li>
                          <a style="cursor: pointer" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li> -->
                        <!-- ============================================================ -->
                        <!-- END Control Sidebar Toggle Button -->
                        <!-- ============================================================ -->
                    </ul>
                </div>
                <!-- ================================================ -->
                <!-- END Navbar Right Menu -->
                <!-- ================================================ -->
            </nav>
        </header>
        <!-- ======================================================== -->
        <!-- END Main Header -->
        <!-- ======================================================== -->

        <!-- ======================================================== -->
        <!-- Content Wrapper. Contains page content -->
        <!-- ======================================================== -->
        <div class="content-wrapper" style="margin-left: 0 !important">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <small id="titulo_modulo">Asistencia personalizada  </small>
                    <!-- <small>Optional description</small> -->
                </h1>
                <!-- <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                    <li class="active">Here</li>
                </ol> -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="box box-warning direct-chat direct-chat-warning" style="width: 60%;" >
                    <div class="box-header with-border">
                      <h3 class="box-title">Chat</h3>
    
                      <div class="box-tools pull-right">
                        <!-- <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span> -->
                        <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle" style="color: #1E4781" >
                          <i class="fa fa-comments"></i></button>
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <!-- Conversations are loaded here -->
                      <div class="direct-chat-messages" style="height:400px" id="chat_list">
                        
                      </div>
                      <!--/.direct-chat-messages-->
    
                      <!-- Contacts are loaded here -->
                      <div class="direct-chat-contacts">
                        <ul class="contacts-list">
                          <li>
                            <a href="#">
                              <img class="contacts-list-img" src="dist/img/usuario1.jpg" alt="User Image">
    
                              <div class="contacts-list-info">
                                    <span class="contacts-list-name">
                                      Count Dracula
                                      <small class="contacts-list-date pull-right">2/28/2015</small>
                                    </span>
                                <span class="contacts-list-msg">How have you been? I was...</span>
                              </div>
                              <!-- /.contacts-list-info -->
                            </a>
                          </li>
                          <!-- End Contact Item -->
           
                          <li>
                            <a href="#">
                              <img class="contacts-list-img" src="dist/img/usuario2.jpg" alt="User Image">
    
                              <div class="contacts-list-info">
                                    <span class="contacts-list-name">
                                      Nadia Jolie
                                      <small class="contacts-list-date pull-right">2/20/2015</small>
                                    </span>
                                <span class="contacts-list-msg">I'll call you back at...</span>
                              </div>
                              <!-- /.contacts-list-info -->
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <img class="contacts-list-img" src="dist/img/usuario2.jpg" alt="User Image">
    
                              <div class="contacts-list-info">
                                    <span class="contacts-list-name">
                                      John K.
                                      <small class="contacts-list-date pull-right">1/27/2015</small>
                                    </span>
                                <span class="contacts-list-msg">Can I take a look at...</span>
                              </div>
                              <!-- /.contacts-list-info -->
                            </a>
                          </li>
                          <!-- End Contact Item -->
                          <li>
                            <a href="#">
                              <img class="contacts-list-img" src="dist/img/default.png" alt="User Image">
    
                              <div class="contacts-list-info">
                                    <span class="contacts-list-name">
                                      Kenneth M.
                                      <small class="contacts-list-date pull-right">1/4/2015</small>
                                    </span>
                                <span class="contacts-list-msg">Never mind I found...</span>
                              </div>
                              <!-- /.contacts-list-info -->
                            </a>
                          </li>
                          <!-- End Contact Item --><!-- End Contact Item -->
                          <li>
                            <a href="#">
                              <img class="contacts-list-img" src="dist/img/default.png" alt="User Image">
    
                              <div class="contacts-list-info">
                                    <span class="contacts-list-name">
                                      Kenneth M.
                                      <small class="contacts-list-date pull-right">1/4/2015</small>
                                    </span>
                                <span class="contacts-list-msg">Never mind I found...</span>
                              </div>
                              <!-- /.contacts-list-info -->
                            </a>
                          </li>
                          <!-- End Contact Item -->
                          <!-- End Contact Item -->
                          <li>
                            <a href="#">
                              <img class="contacts-list-img" src="dist/img/default.png" alt="User Image">
    
                              <div class="contacts-list-info">
                                    <span class="contacts-list-name">
                                      Kenneth M.
                                      <small class="contacts-list-date pull-right">1/4/2015</small>
                                    </span>
                                <span class="contacts-list-msg">Never mind I found...</span>
                              </div>
                              <!-- /.contacts-list-info -->
                            </a>
                          </li>
                          <!-- End Contact Item -->
                        </ul>
                        <!-- /.contatcts-list -->
                      </div>
                      <!-- /.direct-chat-pane -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                      <form method="post" id="frm_enviar">
                        <div class="input-group">
                          <div class="form-group is-empty">
                            <input type="text" name="message" id="message" placeholder="Escribe tu mensaje" class="form-control">
                          </div>
                              <span class="input-group-btn">
                                <button type="submit" class="btn btn-warning btn-flat">Enviar</button>
                              </span>
                        </div>
                      </form>
                    </div>
                    <!-- /.box-footer-->
                  </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- ======================================================== -->
        <!-- /.content-wrapper -->
        <!-- ======================================================== -->

        <!-- ======================================================== -->
        <!-- Main Footer -->
        <!-- ======================================================== -->
        <footer class="main-footer footer-fijo" style="margin-left: 0 !important; z-index: 10102;bottom: 0; position: fixed;">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                CAT
                <img src="dist/img/logo_autotrain.png" alt="" style="width: 40px; margin: -10px 10px">
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2018 <a href="http://www.luduscolombia.com" target="_blank">Ludus</a>.</strong>
        </footer>
        <!-- ======================================================== -->
        <!-- END Main Footer -->
        <!-- ======================================================== -->

        <div class="box box-default" style="width: 48%; height: 79%; position:absolute; right: 5px; top: 115px;" data-step="6" data-intro="<span class='label label-danger round'>6</span> En esta parte verá el avance del curso que lleva, debe estar pendiente cuando complete el 100% del mismo.">

            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 text-center" style="border-right: 1px solid #f4f4f4">
                        <div style="height: 500px;" id="emisor" ></div>
                    </div>
                </div>
            </div>

            <div class="row">
              <div id="mydiv">
                <!-- Include a header DIV with the same name as the draggable DIV, followed by "header" -->
                <div id="mydivheader">Arrasta para mover</div>
                <div style="height: 220px;" id="receptor"></div>
              </div>
            <!-- <div class="col-md-12 text-center" style="border-right: 1px solid #f4f4f4">
                        
                    </div> -->
            </div>

            
            <!-- <div class="box-body">
                <div class="row">
                    <div class="col-xs-6 text-center" style="border-right: 1px solid #f4f4f4">
                        <div style="height: 220px;" id="receptor"></div>
                    </div>
                </div>
            </div> -->

        </div>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Material Design -->
    <script src="dist/js/material.min.js"></script>
    <script src="dist/js/ripples.min.js"></script>
    <script src="plugins/knob/jquery.knob.js"></script>
    <script src="plugins/redux/redux.js"></script>
    <script src="plugins/intro.js/intro.js"></script>
    <script src="dist/js/materialize.min.js"></script>
    <script>
        $.material.init();
        $(".knob").knob({
            'change' : function (v) {
                console.log(v);
            }
        });
    </script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js?v=1"></script>
    <!-- streaming -->
    <script src="//static.opentok.com/v2/js/opentok.js"></script>
    <script src="dist/vct_detail2.js?varRand=<?php echo(rand(10,999999));?>"></script>
    <!-- streaming -->
    <script src="dist/app.js?varRand=<?php echo(rand(10,999999));?>"></script>
        <script>
        // Make the DIV element draggable:
dragElement(document.getElementById("mydiv"));

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV: 
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
</script>
    <!-- Modulos del curso -->
    <!-- <script src="dist/modulos.js?v=1"></script>
    <script src="dist/app.js?v=1"></script> -->

</body>
</html>
