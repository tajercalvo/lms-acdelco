<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_general.php'); ?>
<?php
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ReporteGeneral.xls");
?>

<table border="1">
	<thead>
		<tr>
			<th colspan="10">TRAYECTORIAS</th>

			<?php foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
				<th colspan="<?php echo (count($Data_TRY['Cursos']) + 16); ?>"><?php echo ($Data_TRY['charge']); ?></th>
			<?php } ?>
		</tr>
		<tr>
			<th rowspan="4">CONCESIONARIO</th>
			<th rowspan="4">SEDE</th>
			<th rowspan="4">CIUDAD</th>
			<th rowspan="4">ZONA</th>
			<th rowspan="4">APELLIDO</th>
			<th rowspan="4">NOMBRE</th>
			<th rowspan="4"><?php echo utf8_decode("IDENTIFICACIÓN"); ?></th>
			<th rowspan="4">CARGO</th>
			<th rowspan="4">EMAIL</th>
			<th rowspan="4">CELULAR</th>
			<?php $_VarCantCursos = 0; ?>
			<?php
			$td_comun	= '';
			$td_especialista	= '';
			$td_experto		= '';
			$td_principiante		= '';
			$td_tecnico		= '';
			$comun = 0;
			$especialista = 0;
			$experto = 0;
			$principiante = 0;
			$tecnico = 0;
			foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
				<?php foreach ($Data_TRY['Cursos'] as $key => $Data_CUR) { ?>
					<?php if ($Data_CUR['level'] == 'COMUN') {
						$comun++;
						$_VarCantCursos++;
					}
					if ($Data_CUR['level'] == 'ESPECIALISTA') {
						$especialista++;
						$_VarCantCursos++;
					}
					if ($Data_CUR['level'] == 'EXPERTO') {
						$experto++;
						$_VarCantCursos++;
					}
					if ($Data_CUR['level'] == 'PRINCIPIANTE') {
						$principiante++;
						$_VarCantCursos++;
					}
					if ($Data_CUR['level'] == 'TECNICO') {
						$tecnico++;
						$_VarCantCursos++;
					} ?>
					<!-- <th><?php echo ($Data_CUR['level']); ?></th> -->
				<?php } ?>
			<?php } ?>
			<th style="background-color: #b65525; " colspan="<?php echo $comun ?>">Común</th>
			<th style="background-color: #b65525; " rowspan="3" colspan="2">Total Común <?php echo $comun ?></th>
			<th style="background-color: #a5a9ac; " colspan="<?php echo $especialista ?>">Especialista</th>
			<th style="background-color: #a5a9ac; " rowspan="3" colspan="2">Total Especialista <?php echo $especialista ?></th>
			<th style="background-color: #ffff00; " colspan="<?php echo $experto ?>">Experto</th>
			<th style="background-color: #ffff00; " rowspan="3" colspan="2">Total Experto <?php echo $experto ?></th>
			<th style="background-color: #0158a3; " colspan="<?php echo $principiante ?>">Principiante</th>
			<th style="background-color: #0158a3; " rowspan="3" colspan="2">Total Principiante <?php echo $principiante ?></th>
			<th style="background-color: #069b06; " colspan="<?php echo $tecnico ?>">Técnico</th>
			<th style="background-color: #069b06; " rowspan="3" colspan="2">Total Técnico <?php echo $tecnico ?></th>
			<th rowspan="3" colspan="2">TOTAL CURSOS <?php echo utf8_decode($_VarCantCursos); ?></th>
		</tr>
		<tr>
			<?php foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
				<?php
				$td_comun	= '';
				$td_especialista	= '';
				$td_experto		= '';
				$td_principiante		= '';
				$td_tecnico		= '';
				foreach ($Data_TRY['Cursos'] as $key4 => $Data_CUR) {
					if ($Data_CUR['level'] == 'COMUN') {
						$td_comun .= '<th style="background-color: #b65525">' . utf8_decode($Data_CUR['course']) . '</th>';
					}
					if ($Data_CUR['level'] == 'ESPECIALISTA') {
						$td_especialista .= '<th style="background-color: #a5a9ac;">' . utf8_decode($Data_CUR['course']) . '</th>';
					}
					if ($Data_CUR['level'] == 'EXPERTO') {
						$td_experto .= '<th style="background-color: #ffff00">' . utf8_decode($Data_CUR['course']) . '</th>';
					}
					if ($Data_CUR['level'] == 'PRINCIPIANTE') {
						$td_principiante .= '<th style="background-color: #0158a3; ">' . utf8_decode($Data_CUR['course']) . '</th>';
					}
					if ($Data_CUR['level'] == 'TECNICO') {
						$td_tecnico .= '<th style="background-color: #069b06; ">' . utf8_decode($Data_CUR['course']) . '</th>';
					}
				?>
				<?php } ?>
			<?php }
			echo ($comun == 0) ? '<th style="background-color: #b65525;">Sin Cursos</th>' : $td_comun;
			echo ($especialista == 0) ? '<th style="background-color: #a5a9ac;">Sin Cursos</th>' : $td_especialista;
			echo ($experto == 0) ? '<th style="background-color: #ffff00;">Sin Cursos</th>' : $td_experto;
			echo ($principiante == 0) ? '<th style="background-color: #0158a3;">Sin Cursos</th>' : $td_principiante;
			echo ($tecnico == 0) ? '<th style="background-color: #069b06;">Sin Cursos</th>' : $td_tecnico;
			?>
		</tr>
		<tr>
			<?php foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
				<?php
				$td_comun	= '';
				$td_especialista	= '';
				$td_experto		= '';
				$td_principiante = '';
				$td_tecnico = '';
				foreach ($Data_TRY['Cursos'] as $key4 => $Data_CUR) {
					if ($Data_CUR['level'] == 'COMUN') {
						$td_comun .= '<th style="background-color: #b65525">' . utf8_decode($Data_CUR['type']) . '</th>';
					}
					if ($Data_CUR['level'] == 'ESPECIALISTA') {
						$td_especialista .= '<th style="background-color: #a5a9ac;">' . utf8_decode($Data_CUR['type']) . '</th>';
					}
					if ($Data_CUR['level'] == 'EXPERTO') {
						$td_experto .= '<th style="background-color: #ffff00">' . utf8_decode($Data_CUR['type']) . '</th>';
					}
					if ($Data_CUR['level'] == 'PRINCIPIANTE') {
						$td_principiante .= '<th style="background-color: #0158a3; ">' . utf8_decode($Data_CUR['type']) . '</th>';
					}
					if ($Data_CUR['level'] == 'TECNICO') {
						$td_tecnico .= '<th style="background-color: #069b06; ">' . utf8_decode($Data_CUR['type']) . '</th>';
					}
				?>
				<?php } ?>
			<?php }

			echo ($comun == 0) ? '<th style="background-color: #b65525;"></th>' : $td_comun;
			echo ($especialista == 0) ? '<th style="background-color: #a5a9ac;"></th>' : $td_especialista;
			echo ($experto == 0) ? '<th style="background-color: #ffff00;"></th>' : $td_experto;
			echo ($principiante == 0) ? '<th style="background-color: #0158a3;"></th>' : $td_principiante;
			echo ($tecnico == 0) ? '<th style="background-color: #069b06;"></th>' : $td_tecnico;
			?>
		</tr>
		<tr>

			<?php
			foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
				<?php

				$td_comun	= '';
				$td_especialista	= '';
				$td_experto		= '';
				$td_principiante = '';
				$td_tecnico = '';
				foreach ($Data_TRY['Cursos'] as $key5 => $Data_CUR) {
					if ($Data_CUR['level'] == 'COMUN') {
						$td_comun .= '<th style="background-color: #b65525">' . utf8_decode($Data_CUR['newcode']) . '</th>';
					}
					if ($Data_CUR['level'] == 'ESPECIALISTA') {
						$td_especialista .= '<th style="background-color: #a5a9ac;">' . utf8_decode($Data_CUR['newcode']) . '</th>';
					}
					if ($Data_CUR['level'] == 'EXPERTO') {
						$td_experto .= '<th style="background-color: #ffff00">' . utf8_decode($Data_CUR['newcode']) . '</th>';
					}
					if ($Data_CUR['level'] == 'PRINCIPIANTE') {
						$td_principiante .= '<th style="background-color: #0158a3; ">' . utf8_decode($Data_CUR['newcode']) . '</th>';
					}
					if ($Data_CUR['level'] == 'TECNICO') {
						$td_tecnico .= '<th style="background-color: #069b06">' . utf8_decode($Data_CUR['newcode']) . '</th>';
					}

				?>

				<?php } ?>
			<?php }
			echo (empty($td_comun)) ? "<th style='background-color: #b65525; '></th><th style='background-color: #b65525; '>Tomados</th><th style='background-color: #b65525; '>%</th>" : "$td_comun <th style='background-color: #b65525; '>Tomados</th><th style='background-color: #b65525; '>%</th>";
			echo (empty($td_especialista)) ? "<th style='background-color: #a5a9ac; '></th><th style='background-color: #a5a9ac; '>Tomados</th><th style='background-color: #a5a9ac; '>%</th>" : "$td_especialista <th style='background-color: #a5a9ac; '>Tomados</th><th style='background-color: #a5a9ac; '>%</th>";
			echo (empty($td_experto)) ? "<th style='background-color: #ffff00; '></th><th style='background-color: #ffff00; '>Tomados</th><th style='background-color: #ffff00; '>%</th>" : "$td_experto <th style='background-color: #ffff00; '>Tomados</th><th style='background-color: #ffff00; '>%</th>";
			echo (empty($td_principiante)) ? "<th style='background-color: #0158a3; '></th><th style='background-color: #0158a3; '>Tomados</th><th style='background-color: #0158a3; '>%</th>" : "$td_principiante <th style='background-color: #0158a3; '>Tomados</th><th style='background-color: #0158a3; '>%</th>";
			echo (empty($td_tecnico)) ? "<th style='background-color: #069b06; '></th><th style='background-color: #069b06; '>Tomados</th><th style='background-color: #069b06; '>%</th>" : "$td_tecnico <th style='background-color: #069b06; '>Tomados</th><th style='background-color: #069b06; '>%</th>";
			?>
			<th colspan="2">
				Cantidad Avance
			</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$cant_comun_tota = 0;
		$cant_especialista_tota = 0;
		$cant_experto_tota = 0;
		$cant_principiante_tota = 0;
		$cant_tecnico_tota = 0;

		$porcent_comun_total = 0;
		$porcent_especialista_total = 0;
		$porcent_experto_total = 0;
		$porcent_principiante_total = 0;
		$porcent_tecnico_total = 0;
		foreach ($UsuariosActivos as $key => $Data_USR) { ?>
			<tr>
				<td><?php echo utf8_decode($Data_USR['dealer']); ?></td>
				<td><?php echo utf8_decode($Data_USR['headquarter']); ?></td>
				<td><?php echo utf8_decode($Data_USR['area']); ?></td>
				<td><?php echo utf8_decode($Data_USR['zone']); ?></td>
				<td><?php echo utf8_decode($Data_USR['last_name']); ?></td>
				<td><?php echo utf8_decode($Data_USR['first_name']); ?></td>
				<td><?php echo utf8_decode($Data_USR['identification']); ?></td>
				<td><?php
						$_VarCantPass = 0;
						foreach ($Data_USR['Charges'] as $key => $Data_CHA) {
							echo ('- ' . $Data_CHA['charge'] . ' ');
						} ?></td>
				<td><?php echo utf8_decode($Data_USR['email']); ?></td>
				<td><?php echo utf8_decode($Data_USR['phone']); ?></td>
				<?php

				foreach ($TrayectoriasCursos as $key => $Data_TRY) {
					$ValTra = false;
					foreach ($Data_USR['Charges'] as $key => $Data_CHA) {
						if ($Data_CHA['charge_id'] == $Data_TRY['charge_id']) {
							$ValTra = true;
						}
					}
				?>
					<?php
					$td_comun	= '';
					$td_especialista	= '';
					$td_experto		= '';
					$td_principiante = '';
					$td_tecnico = '';

					$cant_comun = 0;
					$cant_especialista = 0;
					$cant_experto = 0;
					$cant_principiante = 0;
					$cant_tecnico = 0;

					$echos_comun = 0;
					$echos_especialista = 0;
					$echos_experto = 0;
					$echos_principiante = 0;
					$echos_tecnico = 0;

					foreach ($Data_TRY['Cursos'] as $key3 => $Data_CUR) {
						$result_Vlr = '';
						if (isset($Data_USR['Notas'])) {
							if ($ValTra) {
								foreach ($Data_USR['Notas'] as $key => $Data_NOT) {
									if ($Data_NOT['course_id'] == $Data_CUR['course_id']) {
										$result_Vlr = number_format($Data_NOT['avgscore'], 0);
									}
								}
							}
						}
						// if($result_Vlr!="" && $result_Vlr>79){
						// 	$_VarCantPass++;
						// }
						if ($Data_CUR['level'] == 'COMUN') {
							$cant_comun++;
							if (!empty($result_Vlr) and $result_Vlr > 79) {
								$echos_comun++;
							}
							$td_comun .= '<td>' . $result_Vlr . '</td>';
						}
						if ($Data_CUR['level'] == 'ESPECIALISTA') {
							$cant_especialista++;
							if (!empty($result_Vlr) and $result_Vlr > 79) {
								$echos_especialista++;
							}
							$td_especialista .= '<td>' . $result_Vlr . '</td>';
						}
						if ($Data_CUR['level'] == 'EXPERTO') {
							$cant_experto++;
							if (!empty($result_Vlr) and $result_Vlr > 79) {
								$echos_experto++;
							}
							$td_experto .= '<td>' . $result_Vlr . '</td>';
						}
						if ($Data_CUR['level'] == 'PRINCIPIANTE') {
							$cant_principiante++;
							if (!empty($result_Vlr) and $result_Vlr > 79) {
								$echos_principiante++;
							}
							$td_principiante .= '<td>' . $result_Vlr . '</td>';
						}
						if ($Data_CUR['level'] == 'TECNICO') {
							$cant_tecnico++;
							if (!empty($result_Vlr) and $result_Vlr > 79) {
								$echos_tecnico++;
							}
							$td_tecnico .= '<td>' . $result_Vlr . '</td>';
						}
					}
					$cant_comun_tota = $cant_comun_tota + $echos_comun;
					$cant_especialista_tota = $cant_especialista_tota + $echos_especialista;
					$cant_experto_tota = $cant_experto_tota + $echos_experto;
					$cant_principiante_tota +=  $echos_principiante;
					$cant_tecnico_tota += $echos_tecnico;
					$porcent_comun = 0;
					if ($echos_comun > 0) {
						$porcent_comun += $echos_comun * 100 / $cant_comun;
						$porcent_comun_total += $porcent_comun;
					}
					$porcent_especialista = 0;
					if ($echos_especialista > 0) {
						$porcent_especialista += $echos_especialista * 100 / $cant_especialista;
						$porcent_especialista_total += $porcent_especialista;
					}
					$porcent_experto = 0;
					if ($echos_experto > 0) {
						$porcent_experto += $echos_experto * 100 / $cant_experto;
						$porcent_experto_total += $porcent_experto;
					}
					$porcent_principiante = 0;
					if ($echos_principiante > 0) {
						$porcent_principiante += $echos_principiante * 100 / $cant_principiante;
						$porcent_principiante_total += $porcent_principiante;
					}
					$porcent_tecnico = 0;
					if ($echos_tecnico > 0) {
						$porcent_tecnico += $echos_tecnico * 100 / $cant_tecnico;
						$porcent_principiante_total += $porcent_tecnico;
					}
					echo "$td_comun <th>$echos_comun</th><th>" . round($porcent_comun, 2) . "%</th>";
					echo "$td_especialista <th>$echos_especialista</th><th>" . round($porcent_especialista, 2) . "%</th>";
					echo "$td_experto <th>$echos_experto</th><th>" . round($porcent_experto, 2) . "%</th>";
					echo "$td_principiante <th>$echos_principiante</th><th>" . round($porcent_principiante, 2) . "%</th>";
					echo "$td_tecnico <th>$echos_tecnico</th><th>" . round($porcent_tecnico, 2) . "%</th>";
					?>
				<?php } ?>
				<td><?php echo utf8_decode($echos_comun + $echos_especialista + $echos_experto + $echos_principiante + $echos_tecnico); ?></td>
				<td><?php if ($echos_comun > 0 || $echos_especialista > 0 || $echos_experto > 0 || $echos_principiante > 0 || $echos_tecnico > 0) {
							echo (round(number_format((($echos_comun + $echos_especialista + $echos_experto +  $echos_principiante + $echos_tecnico) / ($cant_comun + $cant_especialista + $cant_experto + $cant_principiante + $cant_tecnico)) * 100, 2)));
						} else {
							echo '0';
						}  ?> %</td>
			</tr>
		<?php } ?>
		<tr style="text-align: center;">
			<td colspan="<?php echo ($cant_comun) + 10 ?>"><?php echo count($UsuariosActivos); ?></td>
			<td style="background-color: #b65525"><?php echo $cant_comun_tota ?></td>
			<td style="background-color: #b65525"><?php if ($porcent_comun_total > 0) {
																							echo round($porcent_comun_total / count($UsuariosActivos), 2);
																						} else {
																							echo ("0");
																						} ?>%</td>
			<td colspan="<?php echo ($cant_especialista) ?>"></td>
			<td style="background-color: #a5a9ac;"><?php echo $cant_especialista_tota ?></td>
			<td style="background-color: #a5a9ac;"><?php if ($porcent_especialista_total > 0) {
																								echo round($porcent_especialista_total / count($UsuariosActivos), 2);
																							} else {
																								echo ("0");
																							} ?>%</td>
			<td colspan="<?php echo ($cant_experto) ?>"></td>
			<td style="background-color: #ffff00"><?php echo $cant_experto_tota ?></td>
			<td style="background-color: #ffff00"><?php if ($porcent_experto_total > 0) {
																							echo round($porcent_experto_total / count($UsuariosActivos), 2);
																						} else {
																							echo ("0");
																						}  ?>%</td>
			<td colspan="<?php echo ($cant_principiante) ?>"></td>
			<td style="background-color: #0158a3"><?php echo $cant_principiante_tota ?></td>
			<td style="background-color: #0158a3"><?php if ($porcent_principiante_total > 0) {
																							echo round($porcent_principiante_total / count($UsuariosActivos), 2);
																						} else {
																							echo ("0");
																						}  ?>%</td>

			<td colspan="<?php echo ($cant_tecnico) ?>"></td>
			<td style="background-color: #069b06"><?php echo $cant_tecnico_tota ?></td>
			<td style="background-color: #069b06"><?php if ($porcent_tecnico_total > 0) {
																							echo round($porcent_tecnico_total / count($UsuariosActivos), 2);
																						} else {
																							echo ("0");
																						}  ?>%</td>
			<td colspan="2"></td>
		</tr>
	<tbody>
</table>