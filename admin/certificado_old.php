<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
    // get the HTML
    ob_start();
    include('certificado_source.php');
    $content = ob_get_clean();
    //echo $content;
    // convert in PDF
    require_once(dirname(__FILE__).'/src/html2pdf/html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('L', 'A4', 'es', true, 'UTF-8', array(5, 10, 5, 5));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->pdf->SetAuthor('Ludus Learning Management System');
        $html2pdf->pdf->SetCreator('Ludus Learning Management System');
        $html2pdf->pdf->SetTitle('Certificado de Estudios - AutoTrain Academy');
        $html2pdf->pdf->SetSubject('Certificado de Estudios - AutoTrain Academy');
        $html2pdf->pdf->SetMargins(0, 0, 0, true);
        $html2pdf->pdf->setPrintHeader(false);
        $html2pdf->pdf->SetFooterMargin(0);
        $html2pdf->pdf->setPrintFooter(false);
        $html2pdf->pdf->SetAutoPageBreak(TRUE, 0);
        $html2pdf->pdf->SetLeftMargin(0);
        $html2pdf->pdf->SetTopMargin(0);

        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('Certificado_ACDelco.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
