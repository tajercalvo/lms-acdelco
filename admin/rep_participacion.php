<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_participacion.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_participacion.php">Reporte de participación</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Participación </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de los inscritos por cada curso en los programadas definidos entre 2 fechas.</h5></br>
								</div>
								<div class="col-md-2">
									<?php if($cantidad_datos > 0){ ?>
										<h5><a href="rep_participacion_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>&cursosF=<?php echo($_POST['cursosF']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									<?php } ?>
								</div>
							</div>
							<div class="row">
								<form action="rep_participacion.php" method="post">
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
											<div class="col-md-7 input-group date">
										    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-5">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Final:</label>
											<div class="col-md-7 input-group date">
										    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
									<div class="col-md-10">
										<div class="form-group">
											<label class="col-md-5 control-label" for="cursosF" style="padding-top:8px;">Curso :</label>
											<select style="width: 100%;" id="cursosF" name="cursosF">
												<option value="0">Todos los Cursos</option>
												<?php foreach ($listaCursos as $key => $valueCursos) { ?>
													<option value="<?php echo($valueCursos['course_id']); ?>"><?php echo($valueCursos['course']); ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php
					if( $cantidad_datos > 0 ){ ?>
						<div class="well">
							<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
								<thead>
									<tr>
										<th>CODIGO</th>
										<th>CURSO</th>
										<th>HORAS</th>
										<th>HABILIDAD</th>
										<th>TRAYECTORIA</th>
										<th>INSTRUCTOR</th>
										<th>INICIO</th>
										<th>FIN</th>
										<th>LUGAR</th>
										<th>MIN</th>
										<th>MAX</th>
										<th>INS</th>
										<th>ESTADO</th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($datosCursos_all as $key => $value) { ?>
									<tr>
										<td><?php echo( $value['newcode'] ); ?></td>
										<td><?php echo( $value['course'] ); ?></td>
										<td><?php echo( $value['duration_time'] ); ?></td>
										<td><?php echo( $value['specialty'] ); ?></td>
										<td>
										<?php
											if( isset( $value['trayectoria'] ) && count( $value['trayectoria'] ) > 0 ){
												$trayectoria = "";
												foreach ($value['trayectoria'] as $key => $valueT) {
													$trayectoria .= $valueT." - ";
												}
												echo( $trayectoria );
											}
										?>
										</td>
										<td><?php echo( $value['first_name']." ".$value['last_name'] ); ?></td>
										<td><?php echo( $value['start_date'] ); ?></td>
										<td><?php echo( $value['end_date'] ); ?></td>
										<td><?php echo( $value['living'] ); ?></td>
										<td><?php echo( $value['min_size'] ); ?></td>
										<td><?php echo( $value['max_size'] ); ?></td>
										<td><?php echo( $value['inscritos'] ); ?></td>
										<td>
										<?php
											switch ( $value['status_id'] ) {
												case '1': echo('Activo'); break;
												case '2': echo('Inactivo'); break;
											}
										?>
										</td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					<?php } ?>
				</div>
				<!-- // END Contenido proyectos -->
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_asistencia_total.js"></script>
</body>
</html>
