<?php include('src/seguridad.php'); ?>
<?php include('controllers/asignaciones.php');
$location = 'tutor';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">

		
		<?php $rol_id= ''; 
				foreach ($_SESSION['id_rol'] as $key => $value) {
				 
				 	if ($value['rol_id']==1 OR $value['rol_id']==2) {
				 		# code...
				 		$rol_id = 'admin';
				 	}
			  	
				}?>
				<div id="rol_id" data-value="<?php print_r($rol_id); ?>"></div>
	

	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons train"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/foros_tutor.php">Foro - AutoTrain</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Foro - AutoTrain</h2>
						
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->

					<!-- contenido interno -->
						<div class="innerAll spacing-x2">
							<!-- row -->
							<div class="row">
								<div class="col-md-9 col-sm-8">
									<div class="widget" id="listado_post">
										<!-- Post List -->
											
										
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<div class="widget ">
										<div class="innerAll half border-bottom bg-gray">
											<div class="input-group">
									      		<input type="text" class="form-control" placeholder="Search posts ...">
									      		<span class="input-group-btn">
									        		<button class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
									      		</span>
									    	</div>
										</div>
									</div>
								<?php if ($rol_id == 'admin') {?>
									<div class="widget">
										<!-- Heading -->
											<div class="innerAll half">
												<a id="btn_crear" href="#modal_crear" data-toggle="modal" class="btn btn-xs btn-primary"><i class="fa fa-plus fa-fw"></i> Nuevo Foro </a>
											</div>
									</div>
									<?php }  ?>
								</div>
							</div>
						</div>

					<!--FIN contenido interno -->


				

						<!-- Modal -->
					<div class="modal fade" id="modal_crear" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						  <div class="modal-dialog modal-dialog-centered" role="document">
						    <div class="modal-content" style="top: 30px">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLongTitle">Crear Nuevo Foro</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
							      <form id="frm_hilo">
							      	  <div class="modal-body">
								       	 <div class="row">
				                            <label class="col-md-4">Titulo del foro:</label>
				                              <div class="col-md-7">
				                                <div class="form-group has-feedback">
				                                  <input type="text" id="titulo" name="titulo" pattern=".{0}|.{3,}" class="form-control" placeholder="Escriba un Titulo" required="" title="5 caracteres mínimo" autocomplete="off">
				                                </div>
				                              </div>
				                              <div class="col-md-1"></div>
				                           </div>

				                           <div class="row">
				                           	 <label class="col-md-4">Perfil</label>
				                              <div class="col-md-7">
				                                <div class="form-group has-feedback">
				                                  	<select id="perfil" class="selec" name="perfil" style="width: 100%" multiple> 
													</select>
				                                </div>
				                              </div>
				                              <div class="col-md-1"></div>
				                           </div>

				                           <div class="row">
				                           	 <label class="col-md-4">Trayectoria</label>
				                              <div class="col-md-7">
				                                <div class="form-group has-feedback">
				                                    <select id="trayectoria" class="selec" name="trayectoria" style="width: 100%" multiple>
													</select>
				                                </div>
				                              </div>
				                              <div class="col-md-1"></div>
				                           </div>

								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								        <button type="submit" class="btn btn-primary save">Crear</button>
								      </div>
							      </form>
						    </div>
						  </div>
						</div>




						<!-- Modal_editar -->
					<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
						  <div class="modal-dialog modal-dialog-centered" role="document">
						    <div class="modal-content" style="top: 30px">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLongTitle">Crear Nuevo Foro</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
							      <form id="frm_edit_hilo">
							      	  <div class="modal-body">
								       	 <div class="row">
				                            <label class="col-md-4">Titulo del foro:</label>
				                              <div class="col-md-7">
				                                <div class="form-group has-feedback">
				                                  <input type="text" id="tituloedit" name="tituloedit" pattern=".{0}|.{3,}" class="form-control" placeholder="Escriba un Titulo" required="" title="5 caracteres mínimo" autocomplete="off">
				                                  <input type="text" name="foro_id" id="foro_id" style="display: none">
				                                </div>
				                              </div>
				                              <div class="col-md-1"></div>
				                           </div>

				                           <div class="row">
				                           	 <label class="col-md-4">Perfil</label>
				                              <div class="col-md-7">
				                                <div class="form-group has-feedback">
				                                  	<select id="perfiledit" class="selec" name="perfiledit" style="width: 100%" multiple> 
													</select>
				                                </div>
				                              </div>
				                              <div class="col-md-1"></div>
				                           </div>

				                           <div class="row">
				                           	 <label class="col-md-4">Trayectoria</label>
				                              <div class="col-md-7">
				                                <div class="form-group has-feedback">
				                                    <select id="trayectoriaedit" class="selec" name="trayectoriaedit" style="width: 100%" multiple>
													</select>
				                                </div>
				                              </div>
				                              <div class="col-md-1"></div>
				                           </div>

								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								        <button type="submit" class="btn btn-primary save">Editar</button>
								      </div>
							      </form>
						    </div>
						  </div>
						</div>



						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/foros.js"></script>
</body>
</html>