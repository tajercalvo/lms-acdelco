<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_mejor.php');
$location = 'reporting';
$locData = true;
//$Asist = true;
$DashBoard = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper" style="visibility: hidden;">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons cup"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_mejor.php">Ranking mejores asesores</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
						<div class="innerB">
							<h2 class="margin-none pull-left">Informe de los mejores asesores técnicos y comerciales</h2>
							<br><br>
						</div>
					<!-- // END heading -->
<!-- contenido filtros -->
	<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; "> </h5><br>
			</div>
			<div class="col-md-2">
				<?php if(isset($_POST['start_date_day'])){ ?>
					<!-- <h5><a href="rep_trayectorias_concesionarios_excel.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5> -->
				<?php } ?>
			
			</div>
		</div>
		<div class="row">
			<form action="rep_mejor.php" method="post" id="formulario_usuarios">
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Hasta..."<?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-5">
					<!-- Group -->
					<div class="form-group">
						<label class="col-md-2 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
						<div class="col-md-6 input-group date">
					    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..."<?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>>
					    	<span class="input-group-addon">
					    		<i class="fa fa-th"></i>
					    	</span>
						</div>
					</div>
					<!-- // Group END -->
				</div>
				<div class="col-md-2">
					<h1 class="btn btn-success" id="btn_consultar"><i class="fa fa-check-circle"></i> Consultar</h1>
					<div id="mensaje_descargando"> </div>
					<!-- <button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button> -->
				</div>
			</form>
		</div>
	</div>
</div>
<!-- // END contenido filtros -->

<div id="tablas_Asesores_Comerciales"></div>
<div class="well" id="well_asesor_comercial" style="visibility: hidden;">

				<div class="row border-top border-bottom innerLR">
				<div class="col-md-4">
					<div class="widget widget-heading-simple widget-body-white widget-flat">
					    <div class="widget-body ">
					        <div class="text-center">
					        	<i class="fa fa-trophy fa-3x text-primary innerB half"> ASESOR COMERCIAL</i>

					        	
					        	<h4 id="h4_nombre_asesor"></h4>
					        	<div id="imagen_mp"></div>
					        	<p class="margin-none"></p>
					        	<div class="clearfix"><br></div>
					
								<table class="table" id="tabla_Asesor_comercial_mp">
									<!-- Table heading -->
									<thead>
										<tr>
										  <th style="FONT-SIZE: 80%;">PUTAJE TOTAL Y ZONA</th>
										  <th style="FONT-SIZE: 80%;" class="center">CURSOS</th>
									      <th style="FONT-SIZE: 80%;">PROMEDIO NOTAS</th>
									      <th style="FONT-SIZE: 80%;">VENTAS</th>
									    </tr>	
									</thead>
									<!-- // Table heading END -->
									
									<!-- Table body -->
									<tbody>
											
									</tbody>
									<!-- // Table body END -->
								</table>

					        </div>
					    </div>
					</div>
				</div>
				<div class="col-md-4 padding-none border-right">
				<div class="innerAll">
						<h4 class="strong innerB half"><i class="fa fa-trophy text-primary icon-fixed-width"></i> Mejor Zona</h4>
							<table class="table" id="tabla_Asesor_comercial_mz">
								<!-- Table heading -->
									<thead>
										<tr>
										  <th style="FONT-SIZE: 80%;">PUTAJE TOTAL Y ZONA</th>
										  <th style="FONT-SIZE: 80%;" class="center">ESTUDIANTE </th>
										  <th style="FONT-SIZE: 80%;" class="center">CURSOS</th>
									      <th style="FONT-SIZE: 80%;">PROMEDIO NOTAS</th>
									      <th style="FONT-SIZE: 80%;">VENTAS</th>
									    </tr>	
									</thead>
									<!-- // Table heading END -->
									
									<!-- Table body -->
									<tbody>
											
									</tbody>
									<!-- // Table body END -->
								</table>
						<div class="separator bottom"></div>
						<!-- <a href=""><span class="text-underline">view all</span> <i class="fa fa-circle-arrow-right"></i></a> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="innerAll">
						<h4 class="strong innerB half"><i class="fa fa-trophy text-primary icon-fixed-width"></i> Mejor concesionario</h4>

						<table class="table" id="tabla_Asesor_comercial_mc">
		
								<!-- Table heading -->
								<thead>
									<tr>
									  <th style="FONT-SIZE: 80%;">PUTAJE TOTAL Y ZONA</th>
									  <th style="FONT-SIZE: 80%;" class="center">ESTUDIANTE </th>
									  <th style="FONT-SIZE: 80%;" class="center">CURSOS</th>
								      <th style="FONT-SIZE: 80%;">PROMEDIO NOTAS</th>
								      <th style="FONT-SIZE: 80%;">VENTAS</th>
								    </tr>	
								</thead>
								<!-- // Table heading END -->
								
								<!-- Table body -->
								<tbody>
									
								</tbody>
								<!-- // Table body END -->
							</table>
						<div class="separator bottom"></div>
						<!-- <a href=""><span class="text-underline">view all</span> <i class="fa fa-circle-arrow-right"></i></a> -->
					</div>
				</div>
			</div>

<!-- End well -->
</div>

<div id="tablas_Asesores_Tecnico"></div>
<div class="well" id="well_asesor_Tecnico" style="visibility: hidden;">

				<div class="row border-top border-bottom innerLR">
				<div class="col-md-4">
					<div class="widget widget-heading-simple widget-body-white widget-flat">
					    <div class="widget-body ">
					        <div class="text-center">
					        	<i class="fa fa-trophy fa-3x text-primary innerB half"> ASESOR TÉCNICO</i>
					        	
					        	<h4 id="h4_nombre_Tecnico"></h4>
					        	<div id="imagen_Tecnico_mp"></div>
					        	<p class="margin-none"></p>
					        	<div class="clearfix"><br></div>
					
								<table class="table" id="tabla_Asesor_tecnico_mp">
									<!-- Table heading -->
									<thead>
										<tr>
										  <th style="FONT-SIZE: 80%;">PUNTAJE TOTAL</th>
										  <th style="FONT-SIZE: 80%;" class="center">CURSOS</th>
									      <th style="FONT-SIZE: 80%;">PROMEDIO NOTAS</th>
									    </tr>	
									</thead>
									<!-- // Table heading END -->
									
									<!-- Table body -->
									<tbody>
											
									</tbody>
									<!-- // Table body END -->
								</table>

					        </div>
					    </div>
					</div>
				</div>
				<div class="col-md-4 padding-none border-right">
				<div class="innerAll">
						<h4 class="strong innerB half"><i class="fa fa-trophy text-primary icon-fixed-width"></i> Mejor Zona</h4>
							<table class="table" id="tabla_Asesor_Tecnico_mz">
								<!-- Table heading -->
									<thead>
										<tr>
										  <th style="FONT-SIZE: 80%;">PUNTAJE TOTAL Y ZONA</th>
										  <th style="FONT-SIZE: 80%;" class="center">ESTUDIANTE</th>
										  <th style="FONT-SIZE: 80%;" class="center">CURSOS</th>
									      <th style="FONT-SIZE: 80%;">PROMEDIO NOTAS</th>
									    </tr>	
									</thead>
									<!-- // Table heading END -->
									
									<!-- Table body -->
									<tbody>
											
									</tbody>
									<!-- // Table body END -->
								</table>
						<div class="separator bottom"></div>
						<!-- <a href=""><span class="text-underline">view all</span> <i class="fa fa-circle-arrow-right"></i></a> -->
					</div>
				</div>
				<div class="col-md-4">
					<div class="innerAll">
						<h4 class="strong innerB half"><i class="fa fa-trophy text-primary icon-fixed-width"></i> Mejor concesionario</h4>
						<table class="table" id="tabla_Asesor_Tecnico_mc">
		
								<!-- Table heading -->
								<thead>
									<tr>
									  <th style="FONT-SIZE: 80%;">PUNTAJE TOTAL Y CONCECIONARIO</th>
									  <th style="FONT-SIZE: 80%;" class="center">ESTUDIANTE</th>
									  <th style="FONT-SIZE: 80%;" class="center">CURSOS</th>
								      <th style="FONT-SIZE: 80%;">PROMEDIO NOTAS</th>
								    </tr>	
								</thead>
								<!-- // Table heading END -->
								
								<!-- Table body -->
								<tbody>
									
								</tbody>
								<!-- // Table body END -->
							</table>
						<div class="separator bottom"></div>
						<!-- <a href=""><span class="text-underline">view all</span> <i class="fa fa-circle-arrow-right"></i></a> -->
					</div>
				</div>
			</div>

<!-- End well -->
</div>

<!-- Fin tabla  -->
<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div> 
				</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_mejor.js"></script>
</body>
</html>