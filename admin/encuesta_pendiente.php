<?php include('src/seguridad.php'); ?>
<?php
if(!is_array($encuesta)){
	header("location: index.php");
}
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}

$location = 'biblioteca';
$script_select2v4 = 'select2v4';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/biblioteca.css?v=<?php echo md5(time())?>" media="screen" title="biblioteca">
</head>

<style type="text/css">
.radio{
	border-radius: 5px 30px 30px 5px;
	/* border-radius:  20px 2px 20px 2px; */
	padding-left: 5% !important;
	margin-left: 10px;
	margin-bottom: 1em;
    padding: 1em;
    background-color: #fff;
    box-shadow: 0 1px 5px 0 rgba(0,0,0,.2);
    -webkit-box-shadow: 0 1px 5px 0 rgba(0,0,0,.2);
    list-style: none;
}	

.pregunta{
	color: #d65050;
}
</style>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php" >Biblioteca</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<div style="text-align: center">
						<h2 class="text-uppercase" style="color: #d65050;letter-spacing: 3px;font-weight: bold;"><?php echo $encuesta['encuesta'];?></h2>
					</div>
					<!-- <hr style="border: 1px solid #efefef;"> -->
					<!--CONTENIDO BIBLIOTECA DETALLE-->
					<!-- <pre>
					<?php print_r($encuesta); ?>
					</pre> -->
					<input id="cant_preg" style="display: none;" type="text" value="<?php echo count($encuesta['preguntas']);?>">
					<input id="encuesta_id" style="display: none;" type="text" value="<?php echo $encuesta['encuesta_id'];?>">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6" style="border: 1px solid #efefef; border-radius: 5px; padding:2%;">
						<p>Estamos realizando una encuesta acerca de la percepción y calidad de los cursos impartidos por la Academia AutoTrain, por lo tanto, agradecemos su apoyo realizando la siguiente encuesta, es importante que tenga en cuenta que su respuesta es anónima.
							<br><br>
							Utilizando una escala de 5 puntos, donde 1 significa totalmente en desacuerdo y 5 Totalmente de acuerdo, por favor califique los siguientes aspectos:
						</p>
						<form>
							<?php foreach ($encuesta['preguntas'] as $key => $value) {?>
							<div class="form-group" data-encuesta="<?php echo ($key+1).') '.$value['encuesta_pregunta'] ?>" data-encuesta_pregunta_id="<?php echo $value['encuesta_pregunta_id'] ?>" data-pos="<?php echo $key ?>">
								<label class="pregunta"><?php echo ($key+1).') '.$value['encuesta_pregunta'] ?></label><br>
								<span class="radio">1<input class='respuesta' name="posicion<?php echo $key?>" type="radio" value="1"></span>
								<span class="radio">2<input class='respuesta' name="posicion<?php echo $key?>" type="radio" value="2"></span>
								<span class="radio">3<input class='respuesta' name="posicion<?php echo $key?>" type="radio" value="3"></span>
								<span class="radio">4<input class='respuesta' name="posicion<?php echo $key?>" type="radio" value="4"></span>
								<span class="radio">5<input class='respuesta' name="posicion<?php echo $key?>" type="radio" value="5"></span>
							</div>
							<br>
							<?php } ?>
							<button id="enviar" type="submit" class="btn btn-primary">Enviar</button>
							</form>
						</div>
						<div class="col-md-3"></div>
					</div>
					<!--CONTENIDO BIBLIOTECA DETALLE-->

						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- // Sidebar menu & content wrapper END -->
				<?php include('src/footer.php'); ?>
			</div>
			<!-- // Main Container Fluid END -->
			<?php include('src/global.php'); ?>
			<script src="js/encuesta_pendiente.js?v=<?php echo md5(time()) ?>"></script>
		</body>
		</html>