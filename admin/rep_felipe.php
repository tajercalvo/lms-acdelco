<?php
include('src/seguridad.php');
include_once('../config/database.php');
include_once('../config/config.php');
$query = "SELECT d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, m.module, m.newcode, c.course, c.newcode, MAX(mr.score) as score
			FROM ludus_modules_results_usr mr, ludus_modules m, ludus_courses c, ludus_users u, ludus_headquarters h, ludus_dealers d, ludus_charges_users cu
			WHERE mr.module_id = m.module_id AND m.course_id IN ( 686, 619, 612, 592, 503, 695, 607, 675, 590, 135 )
			AND mr.user_id = u.user_id
			AND m.course_id = c.course_id AND u.headquarter_id = h.headquarter_id
			AND h.dealer_id = d.dealer_id
			AND d.dealer_id IN ( 4, 8, 16, 19, 23, 27 ) 
			AND u.user_id = cu.user_id
			AND cu.charge_id IN ( 23, 35, 37, 130 ) 
			GROUP BY d.dealer, h.headquarter, u.first_name, u.last_name, u.identification, mr.user_id, mr.module_id, m.module, m.newcode, c.course, c.newcode
			ORDER BY d.dealer, h.headquarter, u.first_name, m.module";
			
$DataBase_Acciones = new Database();
$Rows_config = $DataBase_Acciones->SQL_SelectMultipleRows($query);
$cantidad_datos = count($Rows_config);
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_Felipe.csv');

    echo("CONCESIONARIO;SEDE;NOMBRES;APELLIDOS;IDENTIFICACION;MODULO;CODIGO;CURSO;CODIGO;PUNTAJE\n");
    if($cantidad_datos > 0){ 
        foreach ($Rows_config as $iID => $data) {
            echo(utf8_decode($data['dealer'].';'.$data['headquarter'].';'.$data['first_name'].';'.$data['last_name'].';'.$data['identification'].';'.$data['module'].';'.$data['newcode'].';'.$data['course'].';'.$data['newcode'].';'.$data['score'])."\n");
        }
    }

