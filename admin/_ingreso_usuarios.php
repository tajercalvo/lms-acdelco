<?php

include_once('../config/init_db.php');

$usuarios = [
    // ['nombre'=>'Luis Eduardo','apellido'=>'Hoyos','identificacion'=>'72260238','correo'=>'luiseduardohoyos@hotmail.com'],
    // ['nombre'=>'Johan','apellido'=>'Redondo','identificacion'=>'72006626','correo'=>'johandejesusr@gmail.com'],
    // ['nombre'=>'Hector Mario','apellido'=>'Correa','identificacion'=>'15370829','correo'=>'mariosoc32@gmail.com'],
    // ['nombre'=>'Carlos Alberto','apellido'=>'Rueda','identificacion'=>'72270227','correo'=>'carrnjsr_24@hotmail.com'],
    // ['nombre'=>'Mario','apellido'=>'Moron','identificacion'=>'13749277','correo'=>'mamopa3720@hotmail.com'],
    // ['nombre'=>'Alexander','apellido'=>'Guerrero','identificacion'=>'85473275','correo'=>'alexydary0915@gmail.com'],
    // ['nombre'=>'Ronal Stevens','apellido'=>'Jaime','identificacion'=>'1069053481','correo'=>'ronaljstevens91@hotmail.com'],
    // ['nombre'=>'Julio Alberto','apellido'=>'Acosta','identificacion'=>'72282285','correo'=>'jacosdavid@gmail.com'],
    // ['nombre'=>'Yoiner','apellido'=>'Quiñones','identificacion'=>'7558730','correo'=>'yoinerq@gmail.com'],
    // ['nombre'=>'Absalon','apellido'=>'Montaño','identificacion'=>'16224243','correo'=>'cruzadascalidecom@gmail.com'],
    ['nombre'=>'Alirio Miguel','apellido'=>'','identificacion'=>'16885300','correo'=>'alimez123@hotmail.com'],
    ['nombre'=>'Enrile Martinez','apellido'=>'Dippe','identificacion'=>'8854083','correo'=>'dippema@hotmail.com'],
    ['nombre'=>'Cristian ','apellido'=>'Mosquera','identificacion'=>'1020396201','correo'=>'cristian8621@gmail.com'],
    ['nombre'=>'Ruben','apellido'=>'Herrera','identificacion'=>'71241446','correo'=>'rubendho84@yahoo.es'],
    ['nombre'=>'Luis','apellido'=>'Portillo','identificacion'=>'87069427','correo'=>'betto3585@hotmail.com'],
    ['nombre'=>'Luis Carlos','apellido'=>'Pacheco','identificacion'=>'7921810','correo'=>'luiscpachecoc@hotmail.com'],
    ['nombre'=>'Cristian Felipe','apellido'=>'','identificacion'=>'94423031','correo'=>'cris-gomez@hotmail.com'],
    ['nombre'=>'Yefry','apellido'=>'Restrepo','identificacion'=>'14837304','correo'=>'jefry481@hotmail.com'],
    ['nombre'=>'Franki','apellido'=>'Greidy','identificacion'=>'1087027293','correo'=>'escobarfrankierazo@gmail.com'],
    ['nombre'=>'Elkin','apellido'=>'Cortes','identificacion'=>'16500100','correo'=>'elkin.cortesc@gmail.com'],
    ['nombre'=>'John Harold','apellido'=>'','identificacion'=>'16799682','correo'=>'jhfl32@hotmail.com'],
    ['nombre'=>'Oscar Fernando','apellido'=>'','identificacion'=>'13070164','correo'=>'ferchom34@gmail.com']
];

foreach ($usuarios as $key => $v) {

    DB::insert( 'ludus_users',[
        "last_name" => $v['nombre'],
        "first_name" => $v['apellido'],
        "image" => "default.png",
        "identification" => $v['identificacion'],
        "password" => DB::sqleval( "AES_ENCRYPT('".$v['identificacion']."','LdLmsDoorGM_2015*')" ),
        "email" => $v['correo'],
        "phone" => "",
        "mobile_phone" => "",
        "date_birthday" => "",
        "date_creation" => DB::sqleval("NOW()"),
        "date_inactive" => "",
        "date_session" => "",
        "headquarter_id" => 215,
        "status_id" => 1,
        "date_startgm" => DB::sqleval("NOW()"),
        "local_charge" => "",
        "linking" => "",
        "gender" => "",
        "type_user_id" => 1,
        "zone_id" => "",
        "education" => "",
        "editor" => 7187,
        "date_edition" => DB::sqleval("NOW()"),
        "confirmed" => 0
        ] );

    $id = DB::insertId();
    echo( "<br>id: $id <br>" );

    if( $id > 0 ){
        DB::insert('ludus_charges_users',[
            "charge_id" => 150,
            "user_id" => $id,
            "date_creation" => DB::sqleval("NOW()"),
            "status_id" => 1
        ]);
        DB::insert('ludus_roles_users',[
            "rol_id" => 15,
            "user_id" => $id,
            "date_creation" => DB::sqleval("NOW()"),
            "status_id" => 1
        ]);
        DB::insert('ludus_roles_users',[
            "rol_id" => 2,
            "user_id" => $id,
            "date_creation" => DB::sqleval("NOW()"),
            "status_id" => 1
        ]);

        DB::insert( 'ludus_headquarter_history',[
            "headquarter_id" => 150,
            "user_id" => $id,
            "date_creation" => DB::sqleval("NOW()"),
            "status_id" => 1,
            "editor" => 7187
            ] );
    }

}

?>
