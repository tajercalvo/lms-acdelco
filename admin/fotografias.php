<?php include('src/seguridad.php'); ?>
<?php 
$source = "img";
include('controllers/multimedias.php');
$location = 'tutor';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons picture"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/fotografias.php">Galería Fotográfica</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Galerías</h2>
						<div class="btn-group pull-right">
							<?php if($_SESSION['max_rol']>=6){ ?><h5><a href="#ModalCrearNueva" data-toggle="modal" class="glyphicons no-js circle_plus" ><i></i>Agregar Galería</a><h5><?php } ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<!-- Nuevo ROW-->
						<?php 
						$control = 0;
						foreach ($galeria_datos as $key => $Data_Gal) { 
							$control++;
							?>
						<?php if($control==1){ ?>
							<div class="row row-app"><?php } ?>
							<div class="col-md-2">
								<div class="box-generic padding-none margin-none overflow-hidden" style="border: 2px solid gold;">
									<div class="relativeWrap overflow-hidden" style="height:220px">
										<a href="fotografias_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>" class="pull-left">
											<img src="../assets/gallery/thumbs/<?php echo($Data_Gal['image']); ?>" alt="" class="img-responsive padding-none border-none" />
										</a>
										<!--<div class="fixed-bottom bg-inverse-faded">-->
										<div class="fixed-bottom" style="background: rgba(66,66,66,0.8) !important;">
											<div class="media margin-none innerAll">
												<a href="fotografias_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>" class="pull-left"><img src="../assets/images/usuarios/<?php echo($Data_Gal['image_usr']); ?>" style="width:35px" alt="<?php echo($Data_Gal['first_name'].' '.$Data_Gal['last_name']); ?>" /></a>
												<div class="media-body text-white">
													<h4><a href="fotografias_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>" class="pull-left" style="white-space:nowrap;overflow:hidden;word-wrap:break-word;"><?php echo(substr($Data_Gal['media'], 0, 20)); ?>..</a></h4><br>
													<p class="text-small margin-none"><?php echo(substr($Data_Gal['description'],0,30)); ?>.</p>
													<?php if($_SESSION['max_rol']>=6){ ?><p class="text-small margin-none"><a href="#ModalEditar" data-toggle="modal" onclick="CargaDatosEditar(<?php echo($Data_Gal['media_id']); ?>,'<?php echo($Data_Gal['image']); ?>','<?php echo($Data_Gal['media']); ?>','<?php echo($Data_Gal['description']); ?>','<?php echo($Data_Gal['status_id']); ?>');"><i class="fa fa-fw fa-pencil"></i> Editar </a></p><?php } ?>
													<!--<strong><i class="fa fa-fw fa-user-md"></i> <?php echo($Data_Gal['first_name'].' '.$Data_Gal['last_name']); ?> </strong>
													<p class="text-small margin-none"><?php echo($Data_Gal['email']); ?></p>-->
												</div>
											</div>
										</div>
									</div>
									<!--<div class="innerAll inner-2x border-bottom" style="padding:10px !important;">
										<h4><a href="fotografias_detail.php?opcn=verGal&id=<?php echo($Data_Gal['media_id']); ?>" class="pull-left" style="white-space:nowrap;overflow:hidden;word-wrap:break-word;"><?php echo(substr($Data_Gal['media'], 0, 30)); ?></a></h4><br>
										<p class="margin-none"><?php echo($Data_Gal['description']); ?>.</p>
									</div>-->
								</div>
							</div>
						<?php if($control==6){ 
							$control = 0;
							?>
							</div>
							<div class="separator bottom"></div>
						<?php } ?>
						<?php } ?>
					<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>


<!-- Modal -->
<form action="fotografias.php" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
	<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Crear Nueva Galería de Imágenes</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Titulo: </label>
						<input type="text" id="media" name="media" class="form-control col-md-8" placeholder="Título de la galería" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Descripción: </label>
						<input type="text" id="description" name="description" class="form-control col-md-8" placeholder="Descripción de la galería" />
					</div>
					<div class="col-md-1">
						<input type="hidden" id="type_gal" value="1" name="type_gal">
						<input type="hidden" id="id_gal" value="0" name="id_gal">
						<input type="hidden" id="opcn" value="crear" name="opcn">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-10">
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="image_new" name="image_new" />
							</span>
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->

<!-- Modal -->
<form action="fotografias.php" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
	<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Editar la Galería de Imágenes</h4>
				</div>
				<div class="modal-body">
					<div class="widget widget-heading-simple widget-body-gray">
			<div class="widget-body">
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-5">
						<label class="control-label">Titulo: </label>
						<input type="text" id="media_ed" name="media_ed" class="form-control col-md-8" placeholder="Título de la galería" />
					</div>
					<div class="col-md-5">
						<label class="control-label">Descripción: </label>
						<input type="text" id="description_ed" name="description_ed" class="form-control col-md-8" placeholder="Descripción de la galería" />
					</div>
					<div class="col-md-1">
						<input type="hidden" id="type_gal" value="1" name="type_gal">
						<input type="hidden" id="media_id" value="0" name="media_id">
						<input type="hidden" id="opcn" value="editar" name="opcn">
						<input type="hidden" id="imgant" value="0" name="imgant">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
				<!-- Row -->
				<div class="row">
					<div class="col-md-1">
					</div>
					<div class="col-md-4">
						<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
						  	<span class="btn btn-default btn-file">
						  		<span class="fileupload-new">Imágen (1024X678)</span>
						  		<span class="fileupload-exists">Cambiar Imagen</span>
							  	<input type="file" class="margin-none" id="image_new_ed" name="image_new_ed" />
							</span>
						  	<span class="fileupload-preview"></span>
						  	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
						</div>
					</div>
					<div class="col-md-1">
					</div>
					<div class="col-md-4">
						<label class="control-label">Estado: </label>
						<select style="width: 100%;" id="estado" name="estado">
							<option value="1">Activo</option>
							<option value="2">Inactivo</option>
						</select>
						<br>
						Estado Actual: <span id="EstadoActual">Activo</span>
					</div>
					<div class="col-md-1">
					</div>
				</div>
				<!-- Row END-->
				<div class="clearfix"><br></div>
			</div>
		</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- /.modal -->



		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/media.js"></script>
</body>
</html>