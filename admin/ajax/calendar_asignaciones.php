<?php
include_once('../models/asignaciones.php');
$fecha_INI = "2014-01-01";
$fecha_FIN = "2014-01-30";
if (isset($_GET['start'])) {
	$fecha_INI = date("Y-m-d", $_GET['start']);
}
if (isset($_GET['end'])) {
	$fecha_FIN = date("Y-m-d", $_GET['end']);
}
if (isset($_GET['opcn'])) {
	switch ($_GET['opcn']) {

			// ========================================================
			//
			// ========================================================
		case 'asig':
			$datosInvitaciones = Asignaciones::consultaAsignaciones($fecha_INI, $fecha_FIN);
			$datosUsuario_sched = Asignaciones::consultaDatosUsuario_sche($_SESSION['idUsuario']);
			$output = array();
			foreach ($datosInvitaciones as $iID => $data) {
				$description = "<strong>Inscripción hasta: " . $data['max_inscription_date'] . "</strong><br><strong>Ciudad: " . $data['area'] . "</strong><br><strong>Lugar:</strong> " . $data['living'] . "<br><strong>Cupos: " . ($data['max_size'] - $data['inscriptions']) . "</strong><br><strong>Curso:</strong> " . $data['course'] . "<br><strong>Módulo:</strong> " . $data['module'] . "<br><strong>Inicia:    </strong> " . substr($data['start_date'], 0, 10) . " " . substr($data['start_date'], 11, 8) . "<br><strong>Finaliza: </strong> " . substr($data['end_date'], 0, 10) . " " . substr($data['end_date'], 11, 8) . "<br><strong>Profesor:</strong> " . $data['first_name'] . " " . $data['last_name'] . "<br><strong class='text-danger'>Responsable: " . $data['fn_editor'] . " " . $data['ln_editor'] . "<br>Email: " . $data['email'] . "</strong>";
				if ($data['area'] == $datosUsuario_sched['area']) {
					$color = "green";
				} else {
					$color = "blue";
				}
				if (isset($data['cupos']) && $data['cupos'] == "SI") {
					$color = "#d65050";
				}
				$aItem = array(
					'id' => $data['schedule_id'],
					'title' => $data['area'] . ":" . $data['newcode'] . '|' . $data['course'],
					'start' => substr($data['start_date'], 0, 10) . "T" . substr($data['start_date'], 11, 8),
					'end' => substr($data['end_date'], 0, 10) . "T" . substr($data['end_date'], 11, 8),
					'url' => "op_asignaciones.php?schedule_id=" . $data['schedule_id'],
					'description' => $description,
					'color' => $color,
					'allDay' => false
				);
				$output[] = $aItem;
			}
			break;

			// ========================================================
			//
			// ========================================================
		case 'ins':
			$datosInvitaciones = Asignaciones::consultaAsignacionesIns($fecha_INI, $fecha_FIN);
			$output = array();
			foreach ($datosInvitaciones as $iID => $data) {
				$fecha_lista = substr($data['start_date'], 0, 4) . "" . substr($data['start_date'], 5, 2) . "" . substr($data['start_date'], 8, 2);
				$description = "<strong>Inscripción hasta: " . $data['max_inscription_date'] . "</strong><br><strong>Ciudad: " . $data['area'] . "</strong><br><strong>Lugar:</strong> " . $data['living'] . "<br><strong>Curso:</strong> " . $data['course'] . "<br><strong>Módulo:</strong> " . $data['module'] . "<br><strong>Inicia:    </strong> " . substr($data['start_date'], 0, 10) . " " . substr($data['start_date'], 11, 8) . "<br><strong>Finaliza: </strong> " . substr($data['end_date'], 0, 10) . " " . substr($data['end_date'], 11, 8) . "<br><strong>Profesor:</strong> " . $data['first_name'] . " " . $data['last_name'];
				if ($fecha_lista <= date("Ymd")) {
					$aItem = array(
						'id' => $data['schedule_id'],
						'title' => $data['area'] . ":" . $data['newcode'] . '|' . $data['course'],
						'start' => $data['start_date'],
						'end' => $data['end_date'],
						'url' => "calificaciones.php?schedule_id=" . $data['schedule_id'] . "&fecha_inicial=" . $data['start_date'] . "&fecha_final=" . $data['end_date'] . "&typeC=" . $data['type'],
						'description' => $description,
						'color' => 'green',
						'allDay' => false
					);
				} else {
					$aItem = array(
						'id' => $data['schedule_id'],
						'title' => $data['area'] . ":" . $data['course'],
						'start' => $data['start_date'],
						'end' => $data['end_date'],
						'url' => "calificaciones.php?schedule_id=" . $data['schedule_id'] . "&fecha_inicial=" . $data['start_date'] . "&fecha_final=" . $data['end_date'] . "&op=ver" . "&typeC=" . $data['type'],
						'description' => $description,
						'color' => 'blue',
						'allDay' => false
					);
				}
				$output[] = $aItem;
			}
			break;

			// ========================================================
			//
			// ========================================================
		case 'usr':
			$datosInvitaciones = Asignaciones::consultaMisAsignaciones($fecha_INI, $fecha_FIN);
			$datosUsuario_sched = Asignaciones::consultaDatosUsuario_sche($_SESSION['idUsuario']);
			$output = array();
			// echo '<pre>';
			// print_r($datosInvitaciones); return;
			foreach ($datosInvitaciones as $iID => $data) {
				/* print_r($data); */
				$nom_course = $data['course'];
				if (strlen($data['course']) > 60) {
					$nom_course = substr($data['course'], 0, 60) . '<br/>' . substr($data['course'], 60, 140);
				}

				if ($data['area'] == $datosUsuario_sched['area']) {
					$color = "green";
				} else {
					$color = "blue";
				}
				if ($data['type'] == 'VCT') {
					//Para cursos VCT
					$tipo_Actividad = !empty($data["tipo"]) && $data["tipo"] == "Evaluacion" ? "evaluar.php?review_id=".$data["review_id"]."&activity_schedule_id=".$data["activity_schedule_id"]."" : "op_actividades.php?id=" . $data["activity_schedule_id"];
					$description = empty($data["activity_schedule_id"]) ? "<img src='../assets/images/webcam.png' width='20px'><br><br><strong>Lugar: </strong> Virtual Classroom<br><strong>Curso: </strong>" . $nom_course . "<br><strong>Módulo: </strong>" . $data['module'] . "<br><strong>Inicia: </strong> " . substr($data['start_date'], 0, 10) . " " . substr($data['start_date'], 11, 8) . "<br><strong>Finaliza: </strong> " . substr($data['end_date'], 0, 10) . " " . substr($data['end_date'], 11, 8) . "<br><strong>Profesor:</strong> " . $data['first_name'] . " " . $data['last_name'] : "<strong>Lugar: </strong> Virtual Classroom<br><strong>Programación: </strong>" . $data["schedule_id"] . "<br><strong><strong>Curso: </strong>" . $nom_course . "<br><strong>Módulo: </strong>" . $data['module'] . "<br><strong>Inicia: </strong> " . substr($data['start_date'], 0, 10) . " " . substr($data['start_date'], 11, 8) . "<br><strong>Finaliza: </strong> " . substr($data['end_date'], 0, 10) . " " . substr($data['end_date'], 11, 8) . "<br><strong>Profesor:</strong> " . $data['first_name'] . " " . $data['last_name'];
					$color = empty($data["activity_schedule_id"]) ? "orange" : "green";
					$urlIr = !empty($data["activity_schedule_id"]) ? $tipo_Actividad : "virtualclass.php?token=" . md5('GMAcademy') . "&_valSco=" . md5('GMColmotores') . "&opcn=ver&id=" . $data['course_id'] . "&schedule=" . $data['schedule_id'];
				} else if ($data['type'] == 'OJT') {
					//Para cursos OJT
					$description = "<img src='../assets/images/ojt.png' width='20px'><br><br><strong>Ciudad: " . $data['city'] . "</strong><br><strong>Lugar: </strong> " . substr($data['living'], 0, 60) . "<br><strong>Curso: </strong>" . $nom_course . "<br><strong>Módulo: </strong>" . $data['module'] . "<br><strong>Disponibilidad: </strong> " . substr($data['start_date'], 0, 10) . " " . substr($data['start_date'], 11, 8) . "<br><strong>Finaliza: </strong> " . substr($data['end_date'], 0, 10) . " " . substr($data['end_date'], 11, 8) . "<br><strong>Profesor:</strong> " . $data['first_name'] . " " . $data['last_name'];
					$urlIr = "course_detail.php?token=" . md5('GMAcademy') . "&_valSco=" . md5('GMColmotores') . "&opcn=ver&id=" . $data['course_id'];
				} else if ($data['type'] == 'MFR') {
					//Para cursos MFR
					$description = "<strong>Lugar: </strong> Virtual Classroom<br><strong>Curso: </strong>" . $nom_course . "<br><strong>Módulo: </strong>" . $data['module'] . "<br><strong>Inicia: </strong> " . substr($data['start_date'], 0, 10) . " " . substr($data['start_date'], 11, 8) . "<br><strong>Finaliza: </strong> " . substr($data['end_date'], 0, 10) . " " . substr($data['end_date'], 11, 8) . "<br><strong>Profesor:</strong> " . $data['first_name'] . " " . $data['last_name'];
					$color = "green";
					$urlIr = (!empty($data["activity_schedule_id"])) ? "op_actividades.php?id=" . $data["activity_schedule_id"] : "actividades.php?curso=" . $data['course_id'] . "&modulo=" . $data['module_id'];
				} else {
					//Para cursos IBT
					$description = "<img src='../assets/images/classroom.png' width='20px'><br><br><strong>Inscripción hasta1: " . $data['max_inscription_date'] . /* "</strong><br><str ong>Ciudad: " . $data['city']. */ "</strong><br><strong>Lugar: </strong> " . substr($data['living'], 0, 60)
						. "<br><strong>Curso: </strong>" . $nom_course . "<br><strong>Módulo: </strong>" . $data['module'] . "<br><strong>Inicia: </strong> " . substr($data['start_date'], 0, 10) . " " . substr($data['start_date'], 11, 8) . "<br><strong>Finaliza: </strong> " . substr($data['end_date'], 0, 10) . " " . substr($data['end_date'], 11, 8) . "<br><strong>Profesor:</strong> " . $data['first_name'] . " " . $data['last_name'];
					/* $urlIr = "course_detail.php?token=" . md5('GMAcademy') . "&_valSco=" . md5('GMColmotores') . "&opcn=ver&id=" . $data['course_id']; */
					$urlIr = (!empty($data["activity_schedule_id"])) ? "op_actividades.php?id=" . $data["activity_schedule_id"] : "actividades.php?curso=" . $data['course_id'] . "&modulo=" . $data['module_id'];
				}
				$aItem = array(
					'id' => $data['schedule_id'],
					'title' => $data['area'] . ": " . $data['newcode'] . " | " . substr($data['course'], 0, 55),
					'start' => substr($data['start_date'], 0, 10) . "T" . substr($data['start_date'], 11, 8),
					'end' => substr($data['end_date'], 0, 10) . "T" . substr($data['end_date'], 11, 8),
					'url' => $urlIr,
					'description' => $description,
					'color' => $color,
					'tipo' => 'curso',
					'allDay' => false,
					'type' => $data['type']
				);
				$output[] = $aItem;
			}
			/*Consulta las circulares que no ha visto el usuario*/
			include_once('../models/usuarios.php');
			if (!isset($usuario_Class)) {
				$usuario_Class = new Usuarios();
			}
			$charge_user = $usuario_Class->consultaDatosCargosJS($_SESSION['idUsuario']);
			$cargos_id = "0";
			foreach ($charge_user as $key => $Data_Charges) {
				$cargos_id .= "," . $Data_Charges['charge_id'];
			}

			$datosCirculares = Asignaciones::ConsultaCirculares($fecha_INI, $fecha_FIN, $cargos_id);
			foreach ($datosCirculares as $iID => $dataInv) {
				$aItem = array(
					'id' => $dataInv['newsletter_id'],
					'title' => "CIRCULAR - " . $dataInv['title'] . " Por: " . $dataInv['first_post'],
					'start' => substr($dataInv['date_edition'], 0, 10) . "T08:00:00",
					'end' => substr($dataInv['date_edition'], 0, 10) . "T17:00:00",
					'url' => "circulares.php?id=" . $dataInv['newsletter_id'] . "",
					'description' => substr($dataInv['newsletter'], 0, 300),
					'color' => "orange",
					'tipo' => 'circular',
					'allDay' => false
				);
				$output[] = $aItem;
			}

			// print_r($datosInvitaciones);
			// print_r($output);
			// die();
			/*Consulta las circulares que no ha visto el usuario*/
			break;
	} // fin switch

} // fin if GET[opcn]

echo json_encode($output);
