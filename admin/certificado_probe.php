<page>
	<style type="text/css">
<!--
		body{
			background-color: #013C9E;
		}
		@font-face {
			font-family: LouisRegular;
			src: url('../assets/fonts/LouisRegular.ttf') format('truetype');
		}
		@font-face {
			font-family: LouisItalici;
			src: url('../assets/fonts/LouisItalic.ttf') format('truetype');
		}
		@font-face {
			font-family: DurantItalici;
			src: url('../assets/fonts/DurantItalic.ttf') format('truetype');
		}
		.cont_principal{
			width: 100%;
			height: 100%;
			background-color: white;
			/*background-image: url(../assets/images/fondo_cert.png);
			background-position: left top; 
			background-repeat: no-repeat;
			*/
		}
		.cont_inter{
			width: 90%;
			height: 95%;
			/*border: 10px solid #d65050;*/
			/*border-bottom: 1px solid rgb(101, 106, 109);
			border-top: 1px solid rgb(101, 106, 109);
			border-right: 1px solid rgb(101, 106, 109);
			border-left: 1px solid rgb(101, 106, 109);*/
		}
		table{
			border-collapse: collapse;
		}
		.imgLogo{
			width: 260px;
		}
		.imgCert{
			width: 460px;
		}
		.certificado{
			text-align: center;
			font-family: LouisRegular;
			font-size: 30px;
			color: #0058a3;
		}
		.borde{
			border: 10px solid #d65050;
		}
		.gmaca{
			text-align: center;
			font-size: 15px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			height: 25px;
		}
		.persona{
			text-align: center;
			font-family: LouisRegular;
			font-size: 45px;
			font-weight: bold;
			width: 90%;
			margin-left: 5%;
			margin-right: 5%;
			border-bottom: 1px solid rgb(101, 106, 109);
		}
		.cedula{
			text-align: center;
			font-size: 17px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			height: 50px;
		}
		.leyenda{
			text-align: center;
			font-size: 15px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
			width: 90%;
			margin-left: 5%;
			margin-right: 5%;
		}
		.imgVamos{
			width: 180px;
		}
		.cargo{
			text-align: center;
			font-size: 10px;
			font-family: DurantItalici;
			color: rgb(101, 106, 109);
		}
		.linea_sep{
			border-bottom: 1px solid #000000;
			width: 85%;
			margin-left: 10px;
		}
		.TextFirma{
			text-align: center;
			font-family: LouisItalici;
			vertical-align: top;

		}
		.imgFirma{
			width: 160px;
		}
-->
	</style>
	<table align="center" width: "100%;" class="cont_principal" >
		<tr>
			<td class="borde">
				<table align="center" class="cont_inter">
					<tr>
						<td align="center" colspan="5">
							<img src="../assets/images/distinciones/default.png" class="imgLogo">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="5" >
							<img src="../assets/images/certificado_top.png" class="imgCert">
						</td>
					</tr>
					<tr>
						<td align="center" class="persona" colspan="5">
							Diego A Lamprea M
						</td>
					</tr>
					<tr>
						<td align="center" class="cedula" colspan="5">
							<strong>C.C. 80.097.672</strong>
						</td>
					</tr>
					<tr>
						<td align="center" class="leyenda" colspan="5">
							Tom&oacute; el curso presencial correspondiente a <b>"4.2 FINANZAS, PRESUPUESTOS Y PROYECTOS ESPECIALES PARA NEGOCIOS B&C PARTE 2" (CRBTT5673)</b>, con una duraci&oacute;n de 8 horas el d&iacute;a 23/02/2013.<br>
							Bogot&aacute;, Colombia.
						</td>
					</tr>
					<tr>
						<!--<td align="right" colspan="5" style="padding-right: 145px; height: 120px; vertical-align: top;" >
							<img src="../assets/images/vamoscontoda.png" class="imgVamos">
						</td>-->
					</tr>
					<tr>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma1.png" class="imgFirma" ><br>
							<span class="linea_sep"></span><br>
							Jorge Meg&iacute;a.<br>
							<span class="cargo">Presidente Director Gerente<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma2.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Kleusner Lopes.<br>
							<span class="cargo">Vicepresidente Comercial<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma3.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Carlos Pacheco.<br>
							<span class="cargo">Gerente de desarrollo de concesionarios<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma4.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Carlos Vargas.<br>
							<span class="cargo">Supervisor de innovaci&oacute;n<br>GM Colmotores</span>
						</td>
						<td width="20%" align="center" class="TextFirma">
							<img src="../assets/images/firma5.png" class="imgFirma"><br>
							<span class="linea_sep"></span><br>
							Juan R. Luj&aacute;n.<br>
							<span class="cargo">Coordinador estrat&eacute;gico<br>Universidad Corporativa GMACADEMY<br>GM Colmotores</span>
						</td>
					</tr>
					<tr>
						<td align="right" colspan="5" style="height: 25px;" >
							&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
		
</page>