<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_evaluacion_c.php');
$location = 'reporting';
$locData = true;
$Asist = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons address_book"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_evaluacion_c.php">Reporte de Evaluaciones para Coordinadores</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Evaluaciones para Concesionarios</h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
<div class="widget widget-heading-simple widget-body-gray">
	<div class="widget-body">
		<div class="row">
			<div class="col-md-10">
				<h5 style="text-align: justify; ">Aquí encuentra las opciones para filtrar de forma estructurada la información de las evaluaciones y el rango de fechas.</h5></br>
			</div>
			<div class="col-md-2">
				<?php if($cantidad_datos > 0){ ?>
					<h5><a href="rep_evaluacion_excel_c.php?start_date_day=<?php echo($_POST['start_date_day']); ?>&end_date_day=<?php echo($_POST['end_date_day']); ?>&review_id=<?php echo($_POST['review_id']); ?>" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
				<?php } ?>
			</div>
		</div>
		<div class="row">
			<form action="rep_evaluacion_c.php" method="post">
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
					<div class="col-md-7 input-group date">
				    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-5">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-5 control-label" for="start_date_day" style="padding-top:8px;">Fecha Final:</label>
					<div class="col-md-7 input-group date">
				    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
				    	<span class="input-group-addon">
				    		<i class="fa fa-th"></i>
				    	</span>
					</div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
				<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
			</div>
			<div class="col-md-10">
				<!-- Group -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="review_id" style="padding-top:8px;">Evaluación:</label>
						<div class="col-md-10 input-group date">
							<select style="width: 100%;" id="review_id" name="review_id">
								<?php foreach ($reviews_active as $key => $Data_ActiveReview) { 
									?>
									<option value="<?php echo $Data_ActiveReview['review_id']; ?>" <?php if(isset($_POST['review_id']) && ($_POST['review_id']==$Data_ActiveReview['review_id']) ){ ?> selected="selected" <?php } ?> ><?php echo $Data_ActiveReview['review']; ?></option>
								<?php } ?>
				        	</select>
				        </div>
				</div>
				<!-- // Group END -->
			</div>
			<div class="col-md-2">
			</div>
			</form>
		</div>
	</div>
</div>
<?php 
				if($cantidad_datos > 0){ ?>
							<div class="well">
								<table class="table table-invoice">
									<tbody>
										<tr>
											<td class="right">
												<p class="lead">Respuestas</p>
												<!-- // Table -->
												<table class="table table-condensed table-vertical-center table-thead-simple">
													<thead>
														<tr>
															<th class="center">CODIGO</th>
															<th class="center">EVALUACIÓN</th>
															<th class="center">EVALUADO</th>
															<th class="center">IDENTIFICACIÓN</th>
															<th class="center">CONCESIONARIO</th>
															<th class="center">SEDE</th>
															<th class="center">FECHA</th>
															<th class="center">TIEMPO</th>
															<th class="center">META</th>
															<th class="center">PUNTOS</th>
															<th class="center">PUNTAJE</th>
														</tr>
													</thead>
													<tbody id="cuerpo_tabla">
													<?php foreach ($datosCursos_all as $iID => $data) { 
															$Var_Superado = number_format(($data['score']/$data['available_score'])*100,0);
														?>
															<!-- Item -->
															<tr class="selectable">
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['code']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['review']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['first_name'].' '.$data['last_name']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['identification']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['dealer']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['headquarter']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['date_creation']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['time_response']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><?php echo($data['available_score']); ?></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php if($Var_Superado>79){ ?>label-success<?php }else{ ?>label-danger<?php } ?>"><?php echo($data['score']); ?></span></td>
																<td class="important" style="padding: 2px; font-size: 80%;" align="left"><span class="label <?php if($Var_Superado>79){ ?>label-success<?php }else{ ?>label-danger<?php } ?>"><?php echo(number_format(($data['score']/$data['available_score'])*100,0)); ?></span></td>
															</tr>
															<!-- // Item END -->
														<?php 
													}
														?>
													</tbody>
												</table>
												<!-- // Table END -->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php } ?>
						<!-- Nuevo ROW-->
					<div class="row row-app">
						
					</div>
						<!-- // END Nuevo ROW-->
						<div class="separator bottom"></div>
						<div class="separator bottom"></div>
					<!-- // END contenido interno -->
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_evaluacion.js"></script>
</body>
</html>