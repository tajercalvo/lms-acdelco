<?php
include_once('../config/init_db.php');
// $query_cupos = "SELECT d.dealer, s.start_date, ds.*
//     FROM ludus_schedule s, ludus_dealer_schedule ds, ludus_dealers d
//     WHERE s.schedule_id = ds.schedule_id
//     AND ds.dealer_id = d.dealer_id
//     AND s.course_id = 957
//     AND start_date <= '2018-02-28 23:59:59'";

$query_cupos = "SELECT zo.zone, d.dealer, c.course, m.duration_time, s.start_date, ds.*
    FROM ludus_schedule s, ludus_dealer_schedule ds, ludus_dealers d, ludus_courses c, ludus_modules m,
    (
        SELECT DISTINCT h.dealer_id, z.*
        	FROM ludus_headquarters h, ludus_areas a, ludus_zone z, ludus_dealers d
            WHERE h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            AND h.dealer_id = d.dealer_id
            AND d.category = 'CHEVROLET'
            AND h.status_id = 1
            ORDER BY dealer_id
    ) AS zo
    WHERE c.course_id = s.course_id
    AND c.course_id = m.course_id
    AND s.schedule_id = ds.schedule_id
    AND ds.dealer_id = d.dealer_id
    AND d.dealer_id = zo.dealer_id
    AND ds.max_size > 0
    AND s.start_date BETWEEN '2018-02-01 00:00:00' AND '2018-02-28 23:59:59'";

$cupos = DB::query( $query_cupos );

$query_users = "SELECT i.*, h.dealer_id
    FROM ludus_invitation i, ludus_users u, ludus_headquarters h , ludus_schedule s
    WHERE i.user_id = u.user_id
    AND u.headquarter_id = h.headquarter_id
    AND i.schedule_id = s.schedule_id
    AND s.start_date BETWEEN '2018-02-01 00:00:00' AND '2018-02-28 23:59:59'";

$users = DB::query( $query_users );

$num_cupos = count($cupos);

for ($i=0; $i < $num_cupos ; $i++) {
    $cupos[$i]['inscritos'] = 0;
    $cupos[$i]['asistieron'] = 0;
    foreach ($users as $key => $u) {
        if( $cupos[$i]['schedule_id'] == $u['schedule_id'] && $cupos[$i]['dealer_id'] == $u['dealer_id'] ){
            $cupos[$i]['inscritos'] ++;
            if( $u['status_id'] == 2 ){
                $cupos[$i]['asistieron'] ++;
            }
        }
    }
}

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Reporte_Diva.csv');
header ("Content-Transfer-Encoding: binary");

echo("ZONA;CONCESIONARIO;CURSO;HORAS;FECHA;CUPOS;INSCRITOS;ASISTIERON\n");
foreach ($cupos as $key => $c) {
    $registro = "";
    $registro .= $c['zone'].";";
    $registro .= $c['dealer'].";";
    $registro .= $c['course'].";";
    $registro .= $c['duration_time'].";";
    $registro .= $c['start_date'].";";
    $registro .= $c['total_size'].";";
    $registro .= $c['inscritos'].";";
    $registro .= $c['asistieron'].";";
    $registro .= "\n";
    echo( utf8_decode( $registro ) );
}
