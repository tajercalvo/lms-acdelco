<?php include('src/seguridad.php'); ?>
<?php
include('controllers/rep_facturacion.php');
$location = 'reporting';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb loader_s">
					<li>Estás aquí</li>
					<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Facturación</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte Navegación Facturación </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<div class="widget" data-toggle="collapse-widget" >
						<div class="widget-head"><h4 class="heading">Filtros</h4></div>
						<div class="widget-body">
							<form action="" id="form_facturacion" name="form_facturacion" method="post">
                                <!-- oculto -->
								<div class="row" style="display:none">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="zonas" style="padding-top:8px;">Zonas:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="zonas" name="zonas[]" >
													<optgroup label="Zonas">
														<?php foreach ($zonas as $key => $vZona): ?>
															<option value="<?php echo $vZona['zone_id'] ?>"><?php echo $vZona['zone'] ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="concesionarios" style="padding-top:8px;">Concesionarios:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="concesionarios" name="concesionarios[]" >
													<optgroup label="Concesionarios" id="dataConcs">
														<?php foreach ($concesionarios as $key => $vCns): ?>
															<option value="<?php echo $vCns['dealer_id'] ?>"><?php echo $vCns['dealer'] ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="sedes" style="padding-top:8px;">Sedes:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="sedes" name="sedes[]" disabled>
													<optgroup label="Sedes" id="dataSedes">
														<!-- datos sedes -->
													</optgroup>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row" style="display:none">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="cargos" style="padding-top:8px;">Cargos:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]" >
													<optgroup label="Cargos">
														<?php foreach ($cargos as $key => $vCargos): ?>
															<option value="<?php echo $vCargos['charge_id'] ?>"><?php echo $vCargos['charge'] ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
											<div class="col-md-7 input-group date">
										    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Desde..." <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
											<div class="col-md-7 input-group date">
										    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..." <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-4">
										<!-- Group -->
										
										<!-- // Group END -->
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
                                        <input type="hidden" id="opcn" name="opcn" value="consulta">
										<button type="submit" class="btn btn-success" id="consultaAnalytics"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
									<div class="col-md-2">
										<h5 style="display:none"><a href="" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="well">
						<div class="row">
                            <div class="col-md-6" id="tabla_IBT"></div>
                            <div class="col-md-6" id="tabla_VCT"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6" id="tabla_OJT"></div>
                            <div class="col-md-6" id="tabla_WBT"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h3 id="g_cursos_titulo" class="text-center"></h3>
                                <canvas id="g_cursos" style="max-height:500px"></canvas>
                            </div>
                            <div class="col-md-3">
                                <h3 id="g_horas_titulo" class="text-center"></h3>
                                <canvas id="g_horas" style="max-height:500px"></canvas>
                            </div>
                            <div class="col-md-3">
                                <h3 id="g_asistentes_titulo" class="text-center"></h3> 
                                <canvas id="g_asistentes" style="max-height:500px"></canvas>
                            </div>
                            <div class="col-md-3">
                                <h3 id="g_capacitacion_titulo" class="text-center"></h3>
                                <canvas id="g_capacitacion" style="max-height:500px"></canvas>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_facturacion.js"></script>
</body>
</html>
