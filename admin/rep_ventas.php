<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_ventas.php');
$location = 'reporting';
$locData = true;
$qtip = 'qtip';
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb loader_s">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons cars"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_impacto.php">Reporte de Ventas</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte de Ventas </h2>
						<!--<div class="btn-group pull-right">
							<a href="dashboard_analytics.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
							<a href="dashboard_users.html?lang=en" class="btn btn-default"><i class="fa fa-fw fa-user"></i> Users</a>
							<a href="dashboard_overview.html?lang=en" class="btn btn-primary"><i class="fa fa-fw fa-dashboard"></i> Overview</a>
						</div>-->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<!-- contenido interno -->
					<!-- contenido filtros -->
					<div class="tabsbar">
						<ul>
							<li class="glyphicons globe active"><a href="#tab1-3" data-toggle="tab"><i></i> <span> País </span></a></li>
							<li class="glyphicons google_maps"><a href="#tab2-3" data-toggle="tab"><i></i> <span> Zona </span></a></li>
							<li class="glyphicons home tab-stacked"><a href="#tab3-3" data-toggle="tab"><i></i> <span> Concesionario </span></a></li>
							<li class="glyphicons building tab-stacked"><a href="#tab4-3" data-toggle="tab"><i></i> <span> Sede </span></a></li>
							<li class="glyphicons user tab-stacked"><a href="#tab5-3" data-toggle="tab"><i></i> <span> Asesor </span></a></li>
						</ul>
					</div>
					<div class="tab-content">

						<!-- Tab content -->
						<!-- Pais -->
						<div class="tab-pane active" id="tab1-3">
							<div class="widget widget-heading-simple widget-body-gray hidden-print">
								<div class="widget-body">
									<form action="rep_ventas.php" method="post" id="consultaPais">
										<div class="row">
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="p_start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker start_date_day" type="text" id="p_start_date_day" name="p_start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="p_end_date_day" style="padding-top:8px;">Fecha Final:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker end_date_day" type="text" id="p_end_date_day" name="p_end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-2">
												<button type="submit" class="btn btn-success" id="btnPais"><i class="fa fa-check-circle"></i> Consultar</button>
											</div>
										</div>
									</form>
								</div>
								<!-- Nuevo ROW-->
								<div class="row row-app">
								</div>
								<!-- // END Nuevo ROW-->
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
							</div>
							<!-- // END contenido filtros -->
							<!-- PRIMER INDICADOR COMPLETO -->
							<div class="well">
								<div class="row" id="resPais">
									<div class="col-md-6">
										<div class="widget-head">
											<h4 class="heading">País</h4>
										</div>
										<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
										<div class="chart">
						                  <canvas id="char_pais_1" style="max-height:350px"></canvas>
						                </div>
									</div>
									<div class="col-md-6">
										<div class="widget-head">
											<h4 class="heading">Pais - Zonas</h4>
										</div>
										<!-- <div class="flotchart-holder" style="height: 250px;"></div> -->
										<div class="chart">
						                  <canvas id="char_pais_2" style="max-height:350px"></canvas>
						                </div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">
										<div class="widget-head">
											<h4 class="heading">Pais - Concesionarios</h4>
										</div>
										<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
										<div class="chart">
						                  <canvas id="char_pais_3" style="max-height:500px"></canvas>
						                </div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">
										<div class="widget-head">
											<h4 class="heading">Pais - Modelos</h4>
										</div>
										<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
										<div class="chart">
						                  <canvas id="char_pais_4" style="max-height:500px"></canvas>
						                </div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-6">
										<div class="widget-head">
											<h4 class="heading">País - Género</h4>
										</div>
										<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
										<div class="chart">
						                  <canvas id="char_pais_5" style="max-height:350px"></canvas>
						                </div>
									</div>
									<div class="col-md-6">
										<div class="widget-head">
											<h4 class="heading">Pais - Edad</h4>
										</div>
										<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
										<div class="chart">
						                  <canvas id="char_pais_6" style="max-height:350px"></canvas>
						                </div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">
										<div class="widget-head">
											<h4 class="heading">Pais - Usuarios</h4>
										</div>
										<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
										<div class="chart">
						                  <canvas id="char_pais_7" style="max-height:500px"></canvas>
						                </div>
									</div>
								</div>
							</div>
							<!-- PRIMER INDICADOR COMPLETO-->
							<!-- // END contenido interno -->
						</div>
						<!-- // Tab content END -->

						<!-- Tab content -->
						<!-- zona -->
						<div class="tab-pane" id="tab2-3">
							<div class="widget widget-heading-simple widget-body-gray hidden-print">
								<div class="widget-body">
									<form action="rep_ventas.php" method="post" id="consultaZona">

										<div class="row">
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="z_start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker start_date_day" type="text" id="z_start_date_day" name="z_start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="z_end_date_day" style="padding-top:8px;">Fecha Final:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker end_date_day" type="text" id="z_end_date_day" name="z_end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-2">
												<button type="submit" class="btn btn-success" id="btnZona"><i class="fa fa-check-circle"></i> Consultar</button>
											</div>
										</div>
										<div class="row">
											<div class="col-md-10">
								                <label class="control-label" for="zone_id">Zona:</label>
								                <select class="select" style="width: 100%;" id="zone_id" name="zone_id[]" multiple="multiple">
							                        <optgroup label="Zona">
														<option value="0">Todas</option>
									                    <?php foreach ($Zonas as $key => $data) { ?>
								                        <option value="<?php echo($data['zone_id']); ?>" <?php if( (isset($_POST['zone_id']) && $_POST['zone_id']==$data['zone_id']) ){ ?>selected="selected"<?php }?> ><?php echo($data['zone']); ?></option>
									                    <?php } ?>
							                        </optgroup>
								                </select>
								            </div>
										</div>
									</form>
								</div>
								<!-- Nuevo ROW-->
								<div class="row row-app">
								</div>
								<!-- // END Nuevo ROW-->
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
							</div>
							<div class="well">
								<div class="row">
									<div class="row">
										<div class="col-md-6">
											<div class="widget-head">
												<h4 class="heading">Zonas</h4>
											</div>
											<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
											<div class="chart">
							                  <canvas id="char_zona_1" style="max-height:350px"></canvas>
							                </div>
										</div>
										<!-- <div class="col-md-6">
											<div class="widget-head">
												<h4 class="heading">Zonas</h4>
											</div>
											<div id="char_zona_2" class="flotchart-holder" style="height: 300px;"></div>
										</div> -->
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="widget-head">
												<h4 class="heading">Zonas - Concesionarios</h4>
											</div>
											<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
											<div class="chart">
							                  <canvas id="char_zona_3" style="max-height:500px"></canvas>
							                </div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="widget-head">
												<h4 class="heading">Zonas - Modelos</h4>
											</div>
											<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
											<div class="chart">
							                	<canvas id="char_zona_4" style="max-height:500px"></canvas>
							                </div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="widget-head">
												<h4 class="heading">Zona - Género</h4>
											</div>
											<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
											<div class="chart">
							                  <canvas id="char_zona_5" style="max-height:350px"></canvas>
							                </div>
										</div>
										<div class="col-md-6">
											<div class="widget-head">
												<h4 class="heading">Zona - Edad</h4>
											</div>
											<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
											<div class="chart">
							                  <canvas id="char_zona_6" style="max-height:350px"></canvas>
							                </div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-12">
											<div class="widget-head">
												<h4 class="heading">Zona - Usuarios</h4>
											</div>
											<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
											<div class="chart">
							                  <canvas id="char_zona_7" style="max-height:350px"></canvas>
							                </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- // Tab content END -->

						<!-- Tab content -->
						<!-- Concesionario -->
						<div class="tab-pane" id="tab3-3">
							<div class="widget widget-heading-simple widget-body-gray hidden-print">
								<div class="widget-body">
									<form action="rep_ventas.php" method="post" id="consultaConsecionario">

										<div class="row">
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="c_start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker start_date_day" type="text" id="c_start_date_day" name="c_start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="c_end_date_day" style="padding-top:8px;">Fecha Final:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker end_date_day" type="text" id="c_end_date_day" name="c_end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-2">
												<button type="submit" class="btn btn-success" id="btnConcesionario"><i class="fa fa-check-circle"></i> Consultar</button>
											</div>
										</div>
										<div class="row">
											<div class="col-md-10">
												<label class="control-label" for="c_dealer_id">Concesionario:</label>
												<select class="select" style="width: 100%;" id="c_dealer_id" name="c_dealer_id" multiple="multiple">
													<optgroup label="Concesionarios">
														<option value="0">Todos</option>
														<?php foreach ($Concesionarios as $key => $data) { ?>
														<option value="<?php echo($data['dealer_id']); ?>" <?php if( (isset($_POST['dealer_id']) && $_POST['dealer_id']==$data['dealer_id']) ){ ?>selected="selected"<?php }?> ><?php echo($data['dealer']); ?></option>
														<?php } ?>
													</optgroup>
												</select>
											</div>
										</div>
									</form>
								</div>
								<!-- Nuevo ROW-->
								<div class="row row-app">
								</div>
								<!-- // END Nuevo ROW-->
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
							</div>
							<div class="well">
								<div class="row" id="resConcesionario">
									<div class="row">
										<div class="row">
											<div class="col-md-12">
												<div class="widget-head">
													<h4 class="heading">Concesionarios</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
								                	<canvas id="char_concs_1" style="max-height:350px"></canvas>
								                </div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-12">
												<div class="widget-head">
													<h4 class="heading">Concesionarios - Modelos</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
								                	<canvas id="char_concs_3" style="max-height:500px"></canvas>
								                </div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-6">
												<div class="widget-head">
													<h4 class="heading">Concesionarios - Género</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
								                	<canvas id="char_concs_5" style="max-height:350px"></canvas>
								                </div>
											</div>
											<div class="col-md-6">
												<div class="widget-head">
													<h4 class="heading">Concesionarios - Edad</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
								                	<canvas id="char_concs_6" style="max-height:350px"></canvas>
								                </div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-12">
												<div class="widget-head">
													<h4 class="heading">Concesionarios - Usuarios</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
								                	<canvas id="char_concs_7" style="max-height:500px"></canvas>
								                </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- // Tab content END -->

						<!-- Tab content -->
						<!-- Sede -->
						<div class="tab-pane" id="tab4-3">
							<div class="widget widget-heading-simple widget-body-gray hidden-print">
								<div class="widget-body">
									<form action="rep_ventas.php" method="post" id="consultaSede">
										<div class="row">
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="s_start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker start_date_day" type="text" id="s_start_date_day" name="s_start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="s_end_date_day" style="padding-top:8px;">Fecha Final:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker end_date_day" type="text" id="s_end_date_day" name="s_end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-2">
												<button type="submit" class="btn btn-success" id="consultaSede"><i class="fa fa-check-circle"></i> Consultar</button>
											</div>
										</div>
										<div class="row">
											<div class="col-md-5">
								                <label class="control-label" for="s_dealer_id">Concesionario:</label>
								                <select class="select" style="width: 100%;" id="s_dealer_id" name="s_dealer_id">
							                        <option value="0">Todos</option>
								                    <?php foreach ($Concesionarios as $key => $data) { ?>
							                        <option value="<?php echo($data['dealer_id']); ?>" <?php if( (isset($_POST['dealer_id']) && $_POST['dealer_id']==$data['dealer_id']) ){ ?>selected="selected"<?php }?> ><?php echo($data['dealer']); ?></option>
								                    <?php } ?>
								                </select>
								            </div>
											<div class="col-md-5">
								                <label class="control-label" for="headquarter_id">Sede:</label>
								                <select class="select" style="width: 100%;" id="headquarter_id" name="headquarter_id[]" multiple="multiple">
							                        <optgroup label="Sede">
							                        </optgroup>
								                </select>
								            </div>
										</div>
									</form>
								</div>
								<!-- Nuevo ROW-->
								<div class="row row-app">
								</div>
								<!-- // END Nuevo ROW-->
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
							</div>
							<div class="well">
								<div class="row" id="resSede">
									<div class="row" id="resConcesionario">
										<div class="row">
											<div class="row">
												<div class="col-md-12">
													<div class="widget-head">
														<h4 class="heading">Sedes</h4>
													</div>
													<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
													<div class="chart">
									                	<canvas id="char_sede_1" style="max-height:500px"></canvas>
									                </div>
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-12">
													<div class="widget-head">
														<h4 class="heading">Sedes - Modelos</h4>
													</div>
													<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
													<div class="chart">
									                	<canvas id="char_sede_2" style="max-height:500px"></canvas>
									                </div>
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-6">
													<div class="widget-head">
														<h4 class="heading">Sedes - Género</h4>
													</div>
													<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
													<div class="chart">
									                	<canvas id="char_sede_3" style="max-height:350px"></canvas>
									                </div>
												</div>
												<div class="col-md-6">
													<div class="widget-head">
														<h4 class="heading">Sedes - Edad</h4>
													</div>
													<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
													<div class="chart">
									                	<canvas id="char_sede_4" style="max-height:350px"></canvas>
									                </div>
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-12">
													<div class="widget-head">
														<h4 class="heading">Sedes - Usuarios</h4>
													</div>
													<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
													<div class="chart">
									                	<canvas id="char_sede_5" style="max-height:500px"></canvas>
									                </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- Tab content -->
						<!-- Usuario -->
						<div class="tab-pane" id="tab5-3">
							<div class="widget widget-heading-simple widget-body-gray hidden-print">
								<div class="widget-body">
									<form action="rep_ventas.php" method="post" id="consultaUsuario">
										<div class="row">
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="u_start_date_day" style="padding-top:8px;">Fecha Inicial:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker start_date_day" type="text" id="u_start_date_day" name="u_start_date_day" placeholder="Inicia el" <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-5">
												<!-- Group -->
												<div class="form-group">
													<label class="col-md-5 control-label" for="u_end_date_day" style="padding-top:8px;">Fecha Final:</label>
													<div class="col-md-7 input-group date">
												    	<input class="form-control datepicker end_date_day" type="text" id="u_end_date_day" name="u_end_date_day" placeholder="Finaliza el" <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
												    	<span class="input-group-addon">
												    		<i class="fa fa-th"></i>
												    	</span>
													</div>
												</div>
												<!-- // Group END -->
											</div>
											<div class="col-md-2">
												<button type="submit" class="btn btn-success" id="btnUsuarios"><i class="fa fa-check-circle"></i> Consultar</button>
											</div>
										</div>
										<div class="row">
											<div class="col-md-5">
								                <div class="form-group">
													<label class="col-md-5 control-label" for="identification">Usuario:</label>
									                <div class="col-md-7">
									                	<input class="form-control" type="text" name="identification" id="identification" placeholder="Número de documento">
									                </div>
								                </div>
								            </div>
										</div>
									</form>
								</div>
								<!-- Nuevo ROW-->
								<div class="row row-app">
								</div>
								<!-- // END Nuevo ROW-->
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
							</div>
							<div class="well">
								<div class="row" id="resUsuario">
									<div class="row">
										<div class="row">
											<div class="col-md-12">
												<div class="widget-head">
													<h4 class="heading">Usuarios</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
													<canvas id="char_user_1" style="max-height:500px"></canvas>
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-6">
												<div class="widget-head">
													<h4 class="heading">Usuarios - Género</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
													<canvas id="char_user_2" style="max-height:350px"></canvas>
												</div>
											</div>
											<div class="col-md-6">
												<div class="widget-head">
													<h4 class="heading">Usuarios - Edad</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
													<canvas id="char_user_3" style="max-height:350px"></canvas>
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-12">
												<div class="widget-head">
													<h4 class="heading">Usuarios - Modelos</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
													<canvas id="char_user_4" style="max-height:500px"></canvas>
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-12">
												<div class="widget-head">
													<h4 class="heading">Usuarios - Concesionarios</h4>
												</div>
												<!-- <div class="flotchart-holder" style="height: 300px;"></div> -->
												<div class="chart">
													<canvas id="char_user_5" style="max-height:500px"></canvas>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- // Tab content END -->

					</div>
				</div>
				<!-- // END inner -->
			</div>
			<!-- // END Contenido proyectos -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_ventas.js"></script>
</body>
</html>
