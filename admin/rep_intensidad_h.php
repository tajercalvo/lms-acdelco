<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}
}
include('controllers/rep_intensidad_h.php');
$location = 'home';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb loader_s">
					<li>Estás aquí</li>
					<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Reporte Intensidad Horaria</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<!-- <h2 class="margin-none pull-left">Dashboard </h2> -->
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<div class="widget" data-toggle="collapse-widget" >
						<div class="widget-head"><h4 class="heading">Filtros</h4></div>
						<div class="widget-body">
							<form id="form_repIntHor" name="form_repIntHor">
								<div class="row">
									<!-- ==================================================== -->
									<!-- Fecha inicial -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="start_date" style="padding-top:8px;">Fecha inicial:</label>
											<div class="col-md-7 input-group">
												<input type="text" class="form-control" name="start_date" id="start_date" value="">
											</div>
										</div>
									</div>
									<!-- ==================================================== -->
									<!-- END Fecha inicial -->
									<!-- ==================================================== -->

									<!-- ==================================================== -->
									<!-- Fecha Final -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="end_date" style="padding-top:8px;">Fecha final:</label>
											<div class="col-md-7 input-group">
												<input type="text" class="form-control" name="end_date" id="end_date" value="">
											</div>
										</div>
									</div>
									<!-- ==================================================== -->
									<!-- END Fecha Final -->
									<!-- ==================================================== -->

									<!-- ==================================================== -->
									<!-- select para seleccionar los concesionarios -->
									<!-- ==================================================== -->
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="concesionarios" style="padding-top:8px;">Concesionarios:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="concesionarios" name="concesionarios[]" >
													<option value="0" selected>Todos</option>
													<?php foreach ($concesionarios as $key => $vCns): ?>
														<option value="<?php echo $vCns['dealer_id'] ?>"><?php echo $vCns['dealer'] ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>
									<!-- ==================================================== -->
									<!-- END select para seleccionar los concesionarios -->
									<!-- ==================================================== -->
								</div>
								<div class="row">
									<div class="col-md-2">
										<input type="hidden" name="opcn" id="opcn" value="consultar">
										<button type="submit" class="btn btn-success" id="consultaIntensidadH"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
									<div class="col-md-2">
										<h5 style="display:none" id="btn_descarga"><a href="" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									</div>
								</div>
							</form>
						</div>
					</div>

					<!-- Menu y titulo -->
					<div class="innerAll">
						<h1 class="margin-none pull-left">Reporte Intensidad Horaria &nbsp;<i class="fa fa-fw fa-pencil text-muted"></i></h1>
						<div class="clearfix"></div>
					</div>
					<!-- fin menu y titulo -->

					<div class="_well" style="display: none">
						<!-- ========================================================== -->
						<!-- Total -->
						<!-- ========================================================== -->
						<div class="row">
							<h3>Total</h3>
							<div class="col-md-8" id="tablaTodos"></div>
							<div class="col-md-4">
								<div class="row">
									<h3>CURSOS</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaCursosTodos" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
						</div>
						<br>
						<!-- graficas Total -->
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<h3>IMPARTICIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaImparticionTodos" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>ASISTENTES</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaAsistentesTodos" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>HORAS CAPACITACIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaHorasCapTodos" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
						</div>
						<!-- graficas Total -->
						<!-- ========================================================== -->
						<!-- END Total -->
						<!-- ========================================================== -->

						<!-- ========================================================== -->
						<!-- VCT -->
						<!-- ========================================================== -->
						<br>
						<hr>
						<br>
						<div class="row">
							<h3>VCT</h3>
							<div class="col-md-4">
								<div class="row">
									<h3>CURSOS</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaCursosVCT" width="300" height="300"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-8" id="tablaVCT"></div>
						</div>
						<!-- graficas VCT -->
						<br>
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<h3>IMPARTICIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaImparticionVCT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>ASISTENTES</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaAsistentesVCT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>HORAS CAPACITACIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaHorasCapVCT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
						</div>
						<!-- graficas VCT -->
						<!-- ========================================================== -->
						<!-- END VCT -->
						<!-- ========================================================== -->
						<br>
						<hr>
						<br>
						<!-- ========================================================== -->
						<!-- IBT -->
						<!-- ========================================================== -->
						<div class="row">
							<h3>IBT</h3>
							<div class="col-md-8" id="tablaIBT"></div>
							<div class="col-md-4">
								<h3>CURSOS</h3>
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<canvas id="graficaCursosIBT" width="300" height="300"></canvas>
								</div>
								<div class="col-md-2"></div>
							</div>
						</div>
						<!-- graficas IBT -->
						<br>
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<h3>IMPARTICIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaImparticionIBT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>ASISTENTES</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaAsistentesIBT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>HORAS CAPACITACIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaHorasCapIBT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
						</div>
						<!-- graficas IBT -->
						<!-- ========================================================== -->
						<!-- END IBT -->
						<!-- ========================================================== -->
						<br>
						<hr>
						<br>
						<!-- ========================================================== -->
						<!-- OJT -->
						<!-- ========================================================== -->
						<div class="row">
							<h3>OJT</h3>
							<div class="col-md-4">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<canvas id="graficaCursosOJT" width="300" height="300"></canvas>
								</div>
								<div class="col-md-2"></div>
							</div>
							<div class="col-md-8" id="tablaOJT"></div>
						</div>
						<!-- graficas OJT -->
						<br>
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<h3>IMPARTICIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaImparticionOJT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>ASISTENTES</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaAsistentesOJT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>HORAS CAPACITACIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaHorasCapOJT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
						</div>
						<!-- graficas OJT -->
						<!-- ========================================================== -->
						<!-- END OJT -->
						<!-- ========================================================== -->
						<br>
						<hr>
						<br>
						<div class="row">
							<h3>WBT</h3>
							<div class="col-md-8" id="tablaWBT"></div>
							<div class="col-md-4">
								<h3>CURSOS</h3>
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<canvas id="graficaCursosWBT" width="200" height="300"></canvas>
								</div>
								<div class="col-md-2"></div>
							</div>
						</div>
						<!-- graficas OJT -->
						<br>
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<h3>IMPARTICIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaImparticionWBT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>ASISTENTES</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaAsistentesWBT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<h3>HORAS CAPACITACIÓN</h3>
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<canvas id="graficaHorasCapWBT" height="400" width="400" style="width: 250px; height:250px !important"></canvas>
									</div>
									<div class="col-md-2"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/rep_intensidad_h.js"></script>
</body>
</html>
