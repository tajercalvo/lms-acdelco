<?php include('src/seguridad.php'); ?>
<?php
if (!isset($_GET['id'])) {
	if (isset($_SESSION['idUsuario'])) {
		$_GET['id'] = $_SESSION['idUsuario'];
	} else {
		header("location: login");
	}
}
include('controllers/actividades.php');
$location = 'biblioteca';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat">
<!-- <![endif]-->

<head>
	<meta charset="gb18030">
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>

<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php">Biblioteca</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<!-- Sección biblioteca -->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">

								<div class="col-md-12 detailsWrapper">
									<div class="innerAll" style="padding: 0px;">
										<div class="body">
											<div class="row padding">
												<div class="col-md-12">
													<!-- // Inicio cuerpo de la pagina -->

													<div class="widget widget-heading-simple widget-body-white">
														<!-- Widget heading -->
														<!-- <div class="widget-head">
														</div> -->
														<div class="widget-head">
															<h4 class="heading glyphicons list"><i></i> <strong> </strong> Total actividades:<strong class="text-primary"> <?php echo count($actividades); ?> </strong> </h4>
														</div>
														<!-- // Widget heading END -->
														<style type="text/css" media="screen">
															#tabla>thead>tr>th {
																background: white !important;
																color: #0058a3 !important;
																text-align: center;
																border-top: 1px solid #0058a3 !important;
																border-bottom: 1px solid #0058a3 !important;
																width: 25%;
																height: 25px;
																text-transform: uppercase;
															}

															#tabla>tbody>tr>td {
																color: black !important;
																text-align: center;
																border: 1px solid lightgray !important;
																width: 25%;
																height: 100px;
																font-size: 10px;
															}

															p.red {
																color: red;
															}

															p.green {
																color: green;
															}

															#tabla>tbody>tr>tr:nth-child(odd) {
																background-color: red !important;
															}

															#tabla>tbody>tr>tr:nth-child(even) {
																background-color: green !important;
															}

															#tabla>tbody>tr>tr:hover {
																background-color: yellow;
															}

															#mitabla>tbody>tr>td {
																background: white !important;
																color: black !important;
																text-align: center;
																border: 1px solid lightgray !important;
																width: 25%;
																height: auto !important;
																;
																font-size: 10px;
															}

															.eliminar_pregunta,
															.agregar_pregunta {
																cursor: pointer;
															}

															#tabla>thead>tr>th {
																border: 1px solid #0058a3;
															}

															.form-group {
																display: flex;
																align-items: center;
																justify-content: center;
															}
														</style>
														<div class="row">
															<form action="actividades.php" method="GET">
																<div class="col-md-5">
																	<!-- Group -->
																	<div class="form-group">
																		<label class="col-md-3 control-label" for="cargos" style="padding-top:8px;">Seleccionar curso:</label>
																		<div class="col-md-9 input-group">
																			<select style="width: 100%;" id="curso" name="curso">

																			</select>
																		</div>
																	</div>
																	<!-- // Group END -->
																</div>
																<div class="col-md-5">
																	<!-- Group -->
																	<div class="form-group">
																		<label class="col-md-3 control-label" for="cargos" style="padding-top:8px;">Seleccionar modulo:</label>
																		<div class="col-md-9 input-group">
																			<select style="width: 100%;" id="modulo" name="modulo">

																			</select>
																		</div>
																	</div>
																	<!-- // Group END -->
																</div>
																<div class="col-md-2" style="display: flex; align-items: center; justify-content: center;">
																<input type="hidden" value="ctrBuscar">
																	<button type="submit" class="btn btn-success" id="ConsultaRepEstado"><i class="fa fa-check-circle"></i> Consultar</button>
																</div>
															</form>
														</div>
														<div class="widget-body" style="border: none;">
															<!-- Table elements-->

															<table id="tabla" style="width: 100%;">
																<thead>
																	<tr>
																		<th>
																			INFORMACIÓN
																		</th>
																		<th>
																			CURSO
																		</th>
																		<th>
																			COMENTARIOS INSTRUCTOR
																		</th>
																		<th>
																			MIS COMENTARIOS
																		</th>
																		<th>
																			RESULTADO
																		</th>
																		<th>
																			GUÍA
																		</th>
																		<th>
																			ENTREGADO
																		</th>
																		<th>
																			RESPONDER
																		</th>
																	</tr>
																</thead>

																<tbody>
																	
																	<?php 

																	if (isset($actividades) && !empty($actividades)) {
																	foreach (@$actividades as $key => $value) { ?>
																		<tr>
																			<td>
																				<p class="<?php echo $value['color'] ?>"> <?php echo 'Actividad: ' . $value['activity'] . '<br> Porcentaje: ' . $value['porcentaje'] . '<br> Sesión: ' . $value['schedule_id'] ?> <br> ID actividad: <?php echo $value['activity_schedule_id'] ?> <br> ID Instructor: <?php echo $value['profesor'] ?> </p>
																			</td>
																			<td> <strong style=" font-size: 12px"> Curso: <?php echo $value['course'] ?> </strong> <br> <span style=" font-size: 10px"> <?php echo $value['module'] ?> </span> <br> <?php echo 'Desde: ' . substr($value['desde'], 0, 10) . '<br> Hasta: ' . substr($value['hasta'], 0, 10); ?></td>
																			<td><?php echo $value['coments_teacher']; ?></td>
																			<td><?php echo $value['comentarios']; ?></td>
																			<td><?php echo $value['resultado']; ?></td>
																			<td>
																				<?php if ($value['adjunto'] != '') { ?>
																					<a class="archivo" data-toggle="modal" href="#myModal" data-archivo="../assets/actividades/<?php echo $value['adjunto']; ?>">Archivo</a>
																				<?php } ?>
																			</td>
																			<td>
																				<?php if ($value['archivo'] != '') { ?>
																					<a href="../assets/actividades_usuarios/<?php echo $value['archivo']; ?>" target="_blank">Adjunto</a>
																				<?php } ?>
																			</td>
																			<td align="center">
																				<?php if (isset($value['link']) && $value['link'] != '') { ?>
																					<a style="color: black; text-decoration: underline; font-size: 12px;" href="<?php echo $value['link'];  ?>"><span class="btn btn-success"><i class="fa fa-pencil"> Responder</i></span></a>
																				<?php } ?>
																			</td>
																		</tr>
																	<?php } } ?>
																	
															</table>
															<!-- // Table elements END -->
															<!-- Total elements-->
															<div class="form-inline separator bottom small">
																<?php if (isset($encuestas)) {
																	echo "Total de registros:";
																} ?><strong class='text-primary'>
																	<?php if (isset($encuestas)) {
																		echo count($encuestas);
																	} ?> </strong>
															</div>
															<!-- // Total elements END -->
														</div>
													</div>
													<style>
														.bloqueo {
															position: relative;
															background-color: rgba(255, 255, 255, 0.00);
															width: 830px;
															height: 850px;
														}
													</style>
													<!-- // Fin cuerpo de la pagina -->
													<!-- // Inicio pie de pagina -->
													<div class="separator bottom"></div>
													<!-- Modal -->
													<div class="modal fade" id="myModal" role="dialog">
														<div class="modal-dialog" style="width: 90%;">

															<!-- Modal content-->
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal">&times;</button>
																	<h4 class="modal-title">Guía PDF</h4>
																</div>
																<div class="modal-body" id="bodymodal" style="height: 500px;">
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- // Fin class widget-heading  -->
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/actividades.js?v=<?php echo md5(time()); ?>"></script>
</body>

</html>