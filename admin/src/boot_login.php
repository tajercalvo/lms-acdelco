<!-- Global -->
	<script>
	var basePath = '',
		commonPath = '../template/assets/',
		rootPath = '../',
		DEV = false,
		componentsPath = '../template/assets/components/';
	
	var primaryColor = '#e5412d',
		dangerColor = '#b55151',
		infoColor = '#5cc7dd',
		successColor = '#609450',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';
	
	var themerPrimaryColor = primaryColor;
	</script>
	
	<script src="../template/assets/components/library/bootstrap/js/bootstrap.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/breakpoints/breakpoints.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/uniform/assets/lib/js/jquery.uniform.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/uniform/assets/custom/js/uniform.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.move/js/jquery.event.move.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.swipe/js/jquery.event.swipe.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/megamenu.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/core.init.js?v=v2.3.0"></script>	