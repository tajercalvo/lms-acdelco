<?php
/*header('Access-Control-Allow-Origin: *');*/
date_default_timezone_set('America/Bogota');

?>

<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache">
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

<!-- Juan carlos Villar, 2 agosto 2016 - Efecto material design -->
<style>
	#menu,
	a:before {
		content: "";
		position: absolute;
		width: 100%;
		height: 1px;
		bottom: 0;
		left: 0;
		background: #d65050;
		/*background: #9CF5A6;*/
		visibility: hidden;
		border-radius: 5px;
		transform: scaleX(0);
		transition: .25s linear;
		z-index: 9999;
		/* Ajusta este valor según sea necesario */
	}

	#menu,
	a:hover:before,
	#menu,
	a:focus:before {
		visibility: visible;
		transform: scaleX(1);
	}

	/* Ocultar botones de imprimir datatables */
	.DTTT {
		display: none !important;
	}
</style>
<!-- FIn material desing -->

<!--[if lt IE 9]><link rel="stylesheet" href="../template/assets/components/library/bootstrap/css/bootstrap.min.css" /><![endif]-->
<?php if ($location == "education" && isset($review_id)) { ?>
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.surveys_multiple.min.css" />
<?php } ?>
<?php if (isset($CalendarData) && $CalendarData) { ?>
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.calendar.min.css" />
	<link rel="stylesheet" href="../template/assets/components/modules/admin/calendar/assets/custom/js/jquery.qtip.min.css" />
<?php } ?>
<?php if (isset($qtip)) { ?>
	<link rel="stylesheet" href="../template/assets/components/modules/admin/calendar/assets/custom/js/jquery.qtip.min.css" />
<?php } ?>
<?php if ($location == "tutor" || $location == "home") { ?>
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.gallery.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.gallery_video.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.support_forum_overview.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.news.min.css" />
<?php } ?>
<?php if ($location == "reporting" || $location == "centinel") { ?>
	<!--<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.charts.min.css" />-->
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.tabs.min.css" />
<?php } else { ?>
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.sliders.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.dashboard_overview.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.shop_products.min.css" />

	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.employees.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.my_account_advanced.min.css" />

	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.form_validator.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.notifications.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.tables.min.css" />
	<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.tables_responsive.min.css" />
<?php } ?>
<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.widgets.min.css" />
<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.modals.min.css" />
<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.form_elements.min.css">
<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.messages.min.css" />
<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.ui.min.css" />

<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.timeline.min.css" />
<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.support_tickets.min.css" />
<link rel="stylesheet" href="../template/assets/css/admin/module.admin.page.email.min.css" />
<link rel="stylesheet" href="../template/assets/components/plugins/OwlCarousel2-2.2.0/dist/assets/owl.carousel.min.css">
<link rel="stylesheet" href="../template/assets/components/plugins/OwlCarousel2-2.2.0/dist/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="../template/assets/components/plugins/nprogress/nprogress.css">
<link rel="stylesheet" href="css/club.css" />



<!-- Cristian Librerias-->
<link rel="stylesheet" href="../template/assets/components/plugins/datatables/media/css/jquery.dataTables.css">
<!-- <link rel="stylesheet" href="../template/assets/components/plugins/datatables/media/css/footable.boostrap.min.css"> -->
<!-- Fin Librerias -->



<!-- <link rel="stylesheet" href="../template/assets/components/plugins/Swiper-master/dist/css/swiper.min.css"> -->
<!-- <link rel="stylesheet" href="../template/assets/components/plugins/angular-ui-swiper/dist/angular-ui-swiper.css"> -->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

<!-- <link rel="stylesheet" href="../template/assets/components/npm/select2/dist/css/select2.min.css"> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<script src="../template/assets/components/library/jquery/jquery.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/jquery/jquery-migrate.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/modernizr/modernizr.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/less-js/less.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/excanvas.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/browser/ie/ie.prototype.polyfill.js?v=v2.3.0"></script>
<script src="../template/assets/components/library/jquery-ui/js/jquery-ui.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js?v=v2.3.0"></script>
<script>
	if ( /*@cc_on!@*/ false && document.documentMode === 10) {
		document.documentElement.className += ' ie ie10';
	}
</script>
<script>
	$(document).ready(function() {
		if ($("#trayectoriaObligatoria").length > 0) {
			$("#trayectoriaObligatoria").select2({
				placeholder: "Seleccione una sesión",
				allowClear: true
			});
		}
	})
</script>