<?php include("mobileDetect/Mobile_Detect.php"); 
$detect = new Mobile_Detect();
$varShow = true;
if($detect->isMobile() || $detect->isTablet()) {
	$varShow = false;
}
?>
<!-- Top navbar -->
<div class="navbar main hidden-print">
	<!-- Menu Toggle Button -->
	<button type="button" class="btn btn-navbar pull-left visible-xs">
		<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
	</button>
	<?php if($varShow){?>
	<!-- // Menu Toggle Button END -->
	<?php include('src/menu_top.php'); ?>
	<?php include('src/profile_head.php'); ?>
	<div id="Notificaciones_Div">
		<?php include('src/notifications.php'); ?>
	</div>
	<div class="clearfix"></div>
	<?php } ?>
</div>
<?php if($TrayectoriaUsuarios) { ?>
<div class="alert alert-primary" role="alert">
    <form method="post" name="frm-trayectoria-obligatoria" id="frm-trayectoria-obligatoria" action="controllers/mensajes.php">
		<div class="row">
			<div class="col-md-6">
				<select multiple="multiple" name="trayectoriaObligatoria[]" id="trayectoriaObligatoria" class="input100 required" style="background-color: transparent; border: 2px solid;width: 100%;">
					<?php foreach ($TrayectoriaUsuarios as $key => $value) {?>
							<option value="<?php echo $TrayectoriaUsuarios[$key]['charge_id'] ?>"><?php echo $TrayectoriaUsuarios[$key]['charge'] ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-1">
				<button id="registrarme" class="login100-form-btn" type="submit">
				    Enviar
				</button>
			</div>
			<div class="col-md-5">
			    <marquee behavior="alternate">Es importante seleccionar una trayectoria</marquee>
			</div>
	    </div>
	</form>
</div>
<?php } ?>
<!-- Top navbar END -->
<?php if($_SESSION['max_rol']>5){ ?>
<div id="Maximoroll"></div>
<?php } ?>
<div id="Atencion"></div>
