<?php
Class Funciones{

	public static function getConcesionarios ( $zonas, $prof = "" ){
        include_once($prof.'../config/init_db.php');
        $zona = $zonas == "" ? "" : "AND z.zone_id IN ( $zonas ) ";
        $query_sql = "SELECT DISTINCT d.dealer_id, d.dealer
            FROM ludus_dealers d, ludus_headquarters h, ludus_areas a, ludus_zone z
            WHERE d.dealer_id = h.dealer_id
            AND d.status_id = 1
            AND h.area_id = a.area_id
            AND a.zone_id = z.zone_id
            $zona ";
		$Rows_config = DB::query( $query_sql );
        return $Rows_config;
    }//fin funcion getConcesionarios

    public static function getZonas($prof = "" ){
        include_once($prof.'../config/init_db.php');
        $query_sql = "SELECT z.zone_id, z.zone FROM ludus_zone z WHERE z.status_id = 1";
		$Rows_config = DB::query( $query_sql );
        return $Rows_config;
    }//fin funcion getZonas

    public static function getCargos($prof = "" ){
        include_once($prof.'../config/init_db.php');
        $query_sql = "SELECT c.charge_id, c.charge FROM ludus_charges c WHERE c.status_id = 1";
		$Rows_config = DB::query( $query_sql );
        return $Rows_config;
	}//fin funcion getCargos

	public static function getSedes( $concesionarios ,$prof = "" ){
        include_once($prof.'../config/init_db.php');
		$conce = $concesionarios == "" ? "" : "AND h.dealer_id IN ( $concesionarios ) ";
        $query_sql = "SELECT h.headquarter_id, h.headquarter FROM ludus_headquarters h
            WHERE h.status_id = 1
            $conce ";
		$Rows_config = DB::query( $query_sql );
        return $Rows_config;
    }//fin funcion getCargos

	public static function getTipoUsuarios($prof = "" ){
        include_once($prof.'../config/init_db.php');
        $query_sql = "SELECT t.type_user_id, t.type_user FROM ludus_type_user t";
		$Rows_config = DB::query($query_sql);
        return $Rows_config;
    }//fin funcion getTipoUsuarios
  /*
	Andres Vega
	18/08/2016
	Funcion para almacenar un archivo generando un nuevo nombre, y reemplaza el anterior archivo
  registrado en el caso de existir

  $archivo:   nombre que se almacena en la super variable $_FILES ej: $_FILES['nombre_archivo']
  $path:      ruta donde se guardara el archivo de imagen
  $imgant:    nombre de un archivo anterior en el caso que sea necesario reemplazar,
              - si no se especifica el nombre del imgant este tomara el valor = "default.png"
	*/
	public function guardarArchivo($archivo,$path,$imgant="default.png"){
		if(isset($_FILES[$archivo])){
			if(isset($_FILES[$archivo]['tmp_name'])){
				$nom_archivo1 = $_FILES[$archivo]["name"];
				$new_name1 = "ACDelco_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG|png|PNG|gif|GIF|zip|ZIP|rar|RAR|pdf|PDF|mp4|mp3)$'i", $nom_archivo1, $ext1);
        $extencion = isset($ext1[2]) ? $ext1[2] : "";
        switch (strtolower($extencion)) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					case 'png' : $new_extension1 = ".png";
						break;
					case 'PNG' : $new_extension1 = ".png";
						break;
					case 'gif' : $new_extension1 = ".gif";
						break;
					case 'GIF' : $new_extension1 = ".gif";
						break;
          case 'zip' : $new_extension1 = ".zip";
						break;
          case 'ZIP' : $new_extension1 = ".zip";
						break;
          case 'rar' : $new_extension1 = ".rar";
						break;
          case 'RAR' : $new_extension1 = ".rar";
						break;
          case 'pdf' : $new_extension1 = ".pdf";
						break;
          case 'PDF' : $new_extension1 = ".pdf";
						break;
          case 'mp4' : $new_extension1 = ".mp4";
                        break;
          case 'mp3' : $new_extension1 = ".mp3";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = move_uploaded_file($_FILES[$archivo]["tmp_name"], $path.$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								//echo($path);
								if(file_exists($path.$imgant)){
									//echo("si existe ".$path.$imgant);
									unlink($path.$imgant);
								}else{
									//echo("si existe ".$path.$imgant);
								}
							}
						}else{
							$new_name1 = "default1.png";
						}
				}else{
					$new_name1 = "default2.png";
				}
			}else{
				$new_name1 = "default3.png";
			}
		}else{
			$new_name1 = "default4.png";
		}
		return $new_name1;
	}//fin funcion guardarArchivo
  /*
  Andres Vega
	18/08/2016
	Funcion estatica que almacena cualquier tipo de archivo generando un nuevo nombre, y reemplaza el anterior archivo
  registrado en el caso de existir

  $archivo:   nombre que se almacena en la super variable $_FILES ej: $_FILES['nombre_archivo']
  $path:      ruta donde se guardara el archivo de imagen
  $imgant:    nombre de un archivo anterior en el caso que sea necesario reemplazar,
              - si no se especifica el nombre del imgant este tomara el valor = "default.png"
  */
  public static function guardarArchivos($archivo,$path,$imgant="default.png"){
		if(isset($_FILES[$archivo])){
			if(isset($_FILES[$archivo]['tmp_name'])){
				$nom_archivo1 = $_FILES[$archivo]["name"];
				$new_name1 = "ACDelco_";
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG|png|PNG|gif|GIF|zip|ZIP|rar|RAR|pdf|PDF)$'i", $nom_archivo1, $ext1);
        //print_r($ext1);
				switch (strtolower($ext1[2])) {
					case 'jpeg' : $new_extension1 = ".jpg";
						break;
					case 'jpg' : $new_extension1 = ".jpg";
						break;
					case 'JPG' : $new_extension1 = ".jpg";
						break;
					case 'JPEG' : $new_extension1 = ".jpg";
						break;
					case 'png' : $new_extension1 = ".png";
						break;
					case 'PNG' : $new_extension1 = ".png";
						break;
					case 'gif' : $new_extension1 = ".gif";
						break;
					case 'GIF' : $new_extension1 = ".gif";
						break;
          case 'zip' : $new_extension1 = ".zip";
						break;
          case 'ZIP' : $new_extension1 = ".zip";
						break;
          case 'rar' : $new_extension1 = ".rar";
						break;
          case 'RAR' : $new_extension1 = ".rar";
						break;
          case 'pdf' : $new_extension1 = ".pdf";
						break;
          case 'PDF' : $new_extension1 = ".pdf";
						break;
					default    : $new_extension1 = "no";
						break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES[$archivo]["tmp_name"], $path.$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								//echo($path);
								if(file_exists($path.$imgant)){
									//echo("si existe ".$path.$imgant);
									unlink($path.$imgant);
								}else{
									//echo("si existe ".$path.$imgant);
								}
							}
						}else{
							$new_name1 = "default1.png";
						}
				}else{
					$new_name1 = "default2.png";
				}
			}else{
				$new_name1 = "default3.png";
			}
		}else{
			$new_name1 = "default4.png";
		}
		return $new_name1;
	}//fin funcion static guardarArchivo
  /*
  Andres Vega
	18/08/2016
	Funcion estatica que almacena cualquier tipo de archivo generando un nuevo nombre, y reemplaza el anterior archivo
  registrado en el caso de existir

  $archivo:   nombre que se almacena en la super variable $_FILES ej: $_FILES['nombre_archivo']
  $diferenciador : texto para diferenciar los distintos archivos
  $path:      ruta donde se guardara el archivo de imagen
  $imgant:    nombre de un archivo anterior en el caso que sea necesario reemplazar,
              - si no se especifica el nombre del imgant este tomara el valor = "default.png"
  */
  public static function guardarImagenes($archivo,$diferenciador,$path,$imgant="default.png"){
		if(isset($_FILES[$archivo])){
			if(isset($_FILES[$archivo]['tmp_name'])){
				$nom_archivo1 = $_FILES[$archivo]["name"];
				$new_name1 = "ACDelco_".$diferenciador;
				$new_name1 .= date("Ymdhis");
				$new_extension1 = ".jpg";
				preg_match("'^(.*)\.(jpg|jpeg|JPG|JPEG|png|PNG|gif|GIF|mp4)$'i", $nom_archivo1, $ext1);
                $extencion = isset($ext1[2]) ? $ext1[2] : "";
				switch (strtolower($extencion)) {
					case 'jpeg' : $new_extension1 = ".jpg"; break;
					case 'jpg' : $new_extension1 = ".jpg"; break;
					case 'JPG' : $new_extension1 = ".jpg"; break;
					case 'JPEG' : $new_extension1 = ".jpg"; break;
					case 'png' : $new_extension1 = ".png"; break;
					case 'PNG' : $new_extension1 = ".png"; break;
					case 'gif' : $new_extension1 = ".gif"; break;
					case 'GIF' : $new_extension1 = ".gif"; break;
                    case 'mp4' : $new_extension1 = ".mp4"; break;
                    default:
                        $new_extension1 = "no";
                    break;
				}
				if($new_extension1 != "no" && $_FILES[$archivo]["tmp_name"] != ""){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES[$archivo]["tmp_name"], $path.$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								//echo($path);
								if(file_exists($path.$imgant)){
									//echo("si existe ".$path.$imgant);
									unlink($path.$imgant);
								}else{
									//echo("si existe ".$path.$imgant);
								}
							}
						}else{
							$new_name1 = "default1.png";//no se pudo guardar la imagen en el destino
						}
				}else{
					$new_name1 = "default2.png";//no es un archivo con Formato valido
				}
			}else{
				$new_name1 = "default3.png";//nombre de archivo no concuerda con archivo en $_FILES
			}
		}else{
			$new_name1 = "default4.png";//archivo no seteado
		}
		return $new_name1;
	} //fin funcion static guardarArchivo
  /*
  Andres Vega
	18/08/2016
	Funcion estatica que almacena cualquier tipo de archivo generando un nuevo nombre, y reemplaza el anterior archivo
  registrado en el caso de existir

  $archivo:   nombre que se almacena en la super variable $_FILES ej: $_FILES['nombre_archivo']
  $diferenciador : Texto para diferenciar los archivos
  $path:      ruta donde se guardara el archivo de imagen
  $imgant:    nombre de un archivo anterior en el caso que sea necesario reemplazar,
              - si no se especifica el nombre del imgant este tomara el valor = "default.png"
  */
  public static function guardarPDF($archivo,$diferenciador,$path,$imgant="default.png"){
		if(isset($_FILES[$archivo])){
			if(isset($_FILES[$archivo]['tmp_name'])){
				$nom_archivo1 = $_FILES[$archivo]["name"];
				$new_name1 = "ACDelco_".$diferenciador;
				$new_name1 .= date("Ymdhis");
				$new_extension1 = "jpg";
				preg_match("'^(.*)\.(zip|ZIP|rar|RAR|pdf|PDF)$'i", $nom_archivo1, $ext1);
        $extencion = isset($ext1[2]) ? $ext1[2] : "" ;
				switch (strtolower($extencion)) {
					case 'zip' : $new_extension1 = ".zip"; break;
          			case 'ZIP' : $new_extension1 = ".zip"; break;
          			case 'rar' : $new_extension1 = ".rar"; break;
          			case 'RAR' : $new_extension1 = ".rar"; break;
          			case 'pdf' : $new_extension1 = ".pdf"; break;
          			case 'PDF' : $new_extension1 = ".pdf"; break;
					default    : $new_extension1 = "no"; break;
				}
				if($new_extension1 != "no"){
					$new_name1 .= $new_extension1;
					$resultArchivo1 = copy($_FILES[$archivo]["tmp_name"], $path.$new_name1);
						if($resultArchivo1==1){
							if($imgant != "" && $imgant != "default.png"){
								//echo($path);
								if(file_exists($path.$imgant)){
									//echo("si existe ".$path.$imgant);
									unlink($path.$imgant);
								}else{
									//echo("si existe ".$path.$imgant);
								}
							}
						}else{
							$new_name1 = "default1.png";
						}
				}else{
					$new_name1 = "default2.png";
				}
			}else{
				$new_name1 = "default3.png";
			}
		}else{
			$new_name1 = "default4.png";
		}
		return $new_name1;
	}//fin funcion static guardarArchivo
    /*
    Andres Vega
   18/08/2016
   Funcion estatica que almacena cualquier tipo de archivo generando un nuevo nombre, y reemplaza el anterior archivo
    registrado en el caso de existir

    $archivo:   nombre que se almacena en la super variable $_FILES ej: $_FILES['nombre_archivo']
    $diferenciador : Texto para diferenciar los archivos
    $path:      ruta donde se guardara el archivo de imagen
    $imgant:    nombre de un archivo anterior en el caso que sea necesario reemplazar,
                - si no se especifica el nombre del imgant este tomara el valor = "default.png"
    */
    public static function guardarCSV($archivo,$diferenciador,$path,$imgant="default.png"){
       if(isset($_FILES[$archivo])){
           if(isset($_FILES[$archivo]['tmp_name'])){
               $nom_archivo1 = $_FILES[$archivo]["name"];
               $new_name1 = "ACDelco_".$diferenciador;
               $new_name1 .= date("Ymdhis");
               $new_extension1 = "jpg";
               preg_match("'^(.*)\.(csv|CSV)$'i", $nom_archivo1, $ext1);
          $extencion = isset($ext1[2]) ? $ext1[2] : "" ;
               switch (strtolower($extencion)) {
                   case 'csv' : $new_extension1 = ".csv";
                       break;
                       case 'CSV' : $new_extension1 = ".csv";
                       break;
                   default    : $new_extension1 = "no";
                       break;
               }
               if($new_extension1 != "no"){
                   $new_name1 .= $new_extension1;
                   $resultArchivo1 = copy($_FILES[$archivo]["tmp_name"], $path.$new_name1);
                       if($resultArchivo1==1){
                           if($imgant != "" && $imgant != "default.png"){
                               //echo($path);
                               if(file_exists($path.$imgant)){
                                   //echo("si existe ".$path.$imgant);
                                   unlink($path.$imgant);
                               }else{
                                   //echo("si existe ".$path.$imgant);
                               }
                           }
                       }else{
                           $new_name1 = "default1.csv";
                       }
               }else{
                   $new_name1 = "default2.csv";
               }
           }else{
               $new_name1 = "default3.csv";
           }
       }else{
           $new_name1 = "default4.csv";
       }
       return $new_name1;
   }//fin funcion static guardarArchivo
  /*
  16/11/2016
  Andres Vega
  Funcion que retorna un array con los indices principales del titulo seleccionado
  */
  public static function reducirArray($arrayInicial,$titulo){
    $arrayFinal = [];
    foreach ($arrayInicial as $key => $value) {//recorre el array para comparar cada uno de los campos
      $contador = 0;
      if (count($arrayFinal)>0) {
        foreach ($arrayFinal as $key2 => $value2) {
          if($value2 == $value[$titulo]){
            $contador ++;
          }
        }
        if ($contador == 0) {
          $arrayFinal[] = $value[$titulo];
        }
      }else{
        $arrayFinal[] = $value[$titulo];
      }
    }//fin foreach
    return $arrayFinal;
  }

  /*
  24/06/2018
  Juan Carlos Villar
  Funcion que sube un archivo al servidor elimina el anterior si existe y lo renombra
  */
  public static function renombrar_y_subir_archivo($ruta, $tmp_name, $name, $iniciales='archivo', $archivo_anterior = '' ){

                    // $archivo        =   $_FILES[$opcn_array]['tmp_name']; // capturamos el archivo temporal
                    // $nombre_archivo =   $_FILES[$opcn_array]['name']; // capturamos el nombre del archivo temporal
					$archivo 		=	$tmp_name;
					$nombre_archivo	=	$name;
                    $extencion      =   strtolower(substr( $nombre_archivo, -5 ) ); // Capruramos los 6 ultimos caracteres del nombre dle archivo y lo convertimos a minuscula
                    $array_ext      =   explode('.', $extencion); // partimos el nombre del archivo por el punto y armamos un array
                    $extencion      =   '.'.$array_ext[1]; // optenemos la extencion final o formato del archivo
                    $hoy            =   getdate(); /*Funcion que captura fecha y hora en un array*/
                    $fecha          =   $hoy['year'].$hoy['mon'].$hoy['mday'].$hoy['hours'].$hoy['minutes'].$hoy['seconds'];
                    $fecha		   .= 	rand(0, 100);
                    $nombre_final   =   $iniciales.'_'.$fecha.$extencion;

                    if ( copy ( $archivo, $ruta.$nombre_final ) ) {

                            $accion = $nombre_final;

                            if ( file_exists( $ruta.$archivo_anterior ) and $archivo_anterior != '' ) {

                                $respuesta['existe'] = "existe";
                                if( unlink( $ruta.$archivo_anterior ) ){
                                    $respuesta['borrado'] = "borrado";
                                }else{
                                    echo "no borrado";
                                }

                            }else{
                                //echo "no existe";
                            }

                        }else{

                            echo "no copiado";
                            $accion = false;
                        }
                    return $accion;
	}
}//fin clase Funciones
