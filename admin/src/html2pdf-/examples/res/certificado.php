
<page class="back">
    <style type="text/css">

        /*@font-face {
            font-family: LouisRegular;
            src: url('../assets/fonts/LouisRegular.ttf') format('truetype');
        }
        @font-face {
            font-family: LouisItalici;
            src: url('../assets/fonts/LouisItalic.ttf') format('truetype');
        }
        @font-face {
            font-family: DurantItalici;
            src: url('../assets/fonts/DurantItalic.ttf') format('truetype');
        }*/
        .back{
            background-color: #013C9E;
        }
        .cont_principal{
            width: 100%;
            height: 100%;
            background-color: white;
        }
        .cont_inter{
            width: 90%;
            height: 95%;
        }
        table{
            border-collapse: collapse;
        }
        .imgLogo{
            width: 160px;
        }
        .imgCert{
            width: 260px;
        }
        .certificado{
            text-align: center;
            font-size: 30px;
            color: #f4bc00;
        }
        .borde{
            border: 10px solid #f4bc00;
        }
        .gmaca{
            text-align: center;
            font-size: 15px;
            color: rgb(101, 106, 109);
            height: 25px;
        }
        .persona{
            text-align: center;
            font-size: 45px;
            font-weight: bold;
            color: #013C9E;
            width: 90%;
            margin-left: 5%;
            margin-right: 5%;
            border-bottom: 1px solid rgb(101, 106, 109);
        }
        .cedula{
            text-align: center;
            font-size: 17px;
            color: rgb(101, 106, 109);
            height: 50px;
        }
        .leyenda{
            text-align: center;
            font-size: 15px;
            color: rgb(101, 106, 109);
            width: 90%;
            margin-left: 5%;
            margin-right: 5%;
        }
        .imgVamos{
            width: 180px;
        }
        .cargo{
            text-align: center;
            font-size: 10px;
            color: rgb(101, 106, 109);
        }
        .linea_sep{
            border-bottom: 1px solid #000000;
            width: 85%;
            margin-left: 10px;
        }
        .TextFirma{
            text-align: center;
            vertical-align: top;

        }
        .imgFirma{
            width: 160px;
        }
    </style>
    <table align="center" class="cont_principal" >
        <tr>
            <td class="borde">
                <table align="center" class="cont_inter">
                    <tr>
                        <td align="center" colspan="5">
                            <img src="https://colombia.academiachevrolet.com/assets/images/GmAcademyLogo.png" class="imgLogo">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5" style="height: 20px;" >
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5" >
                            <img src="https://colombia.academiachevrolet.com/assets/images/certificado_top.png" class="imgCert">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5" style="height: 20px;" >
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="persona" colspan="5">
                            JUAN CARLOS VILLAR HERRERA                      </td>
                    </tr>
                    <tr>
                        <td align="center" class="cedula" colspan="5">
                            <strong>C.C. 1082889369</strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="leyenda" colspan="5">
                            Tom&oacute; el curso virtual correspondiente a <strong>"ELECTRICIDAD BÁSICA"</strong>, el d&iacute;a Mi&eacute;rcoles 31 de Diciembre de 1969.<br>
                            Bogot&aacute;, Colombia.
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="5" style="padding-right: 145px; height: 100px; vertical-align: top;" >
                            <img src="https://colombia.academiachevrolet.com/assets/images/vamoscontoda.png" class="imgVamos">
                        </td>
                    </tr>
                    <tr>
                        <td  align="center" class="TextFirma">
                            <img src="https://colombia.academiachevrolet.com/assets/images/firma6.png" class="imgFirma" ><br>
                            <span class="linea_sep"></span><br>
                            Guillermo Degollado.<br>
                            <span class="cargo">Sales Director VSM – Sales & Operations<br>GM Colmotores</span>
                        </td>
                        <td  align="center" class="TextFirma">
                            <img src="https://colombia.academiachevrolet.com/assets/images/firma7.png" class="imgFirma"><br>
                            <span class="linea_sep"></span><br>
                            Miguel Alarcón.<br>
                            <span class="cargo">Gerente de desarrollo de concesionarios<br>GM Colmotores</span>
                        </td>
                        <td  align="center" class="TextFirma">
                            <img src="https://colombia.academiachevrolet.com/assets/images/firma5.png" class="imgFirma"><br>
                            <span class="linea_sep"></span><br>
                            Nicolás Soto Barros.<br>
                            <span class="cargo">Director GMACADEMY<br>GM Colmotores</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</page>
