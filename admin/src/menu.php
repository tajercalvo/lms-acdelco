<?php
$invitado = Fcn_TieneRolValue(15);
include('controllers/menu-left.php');
?>
<?php if (isset($_SESSION['idUsuario'])) { ?>
	<!-- Sidebar Menu -->
	<div id="menu" class="hidden-print">

		<!-- Brand -->
		<a href="index.php" class="appbrand">
			<img src="../assets/images/usuarios/ACDelco-Logo.png" width="50px">
			<!-- <span class="text-primary">ACDelco</span> <span>LMS</span> -->
		</a>

		<!-- Scrollable menu wrapper with Maximum height -->
		<!-- <div class="slim-scroll" data-scroll-height="800px"> -->
		<div class="slim-scroll">

			<!-- Menu Toggle Button -->
			<button type="button" class="btn btn-navbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<!-- // Menu Toggle Button END -->

			<!-- Sidebar Profile -->
			<span class="profile center">
				<a href="ver_perfil.php?id=<?php echo ($_SESSION['idUsuario']); ?>"><img class="img-responsive" src="../assets/images/usuarios/logouser.png"<?php echo ($_SESSION['imagen']); ?> style="width: 74px;" alt="<?php echo ($_SESSION['NameUsuario']); ?>" /></a>
			</span>
			<!-- // Sidebar Profile END -->

			<!-- Menu -->
			<ul>
	            <?php foreach ($menuleft as $key => $nivel1) { 
					if ($nivel1['nivel'] == 1) {
					if (isset($nivel1['sub'])) { ?>
			            <li class="hasSubmenu <?php if($location == $nivel1['menu']){ ?>active<?php } ?>">
			                <a data-toggle="collapse" href="#<?php echo $nivel1['menu_id']; ?>"
			                    class="glyphicons <?php echo $nivel1['icono']; ?>"><i></i><span
			                        style="font-size: 11px;"><?php echo $nivel1['menu']; ?></span><span
			                        class="fa fa-chevron-down"></span></a>
			                <ul class="collapse" id="<?php echo $nivel1['menu_id']; ?>">
			                    <?php foreach ($nivel1['sub'] as $key2 => $nivel2) { ?>
			                    <?php if (isset($nivel2['sub'])) { ?>
			                    <li class="hasSubmenu <?php if($location == $nivel2['menu']){ ?>active<?php } ?>">
			                        <a data-toggle="collapse" href="#<?php echo $nivel2['menu_id']; ?>"
			                            class="glyphicons <?php echo $nivel2['icono']; ?>"><i></i><span
			                                style="padding-right: 15px;"><?php echo $nivel2['menu']; ?></span><span
			                                class="fa fa-chevron-down"></span></a>
			                        <ul class="collapse" id="<?php echo $nivel2['menu_id']; ?>">
			                            <?php foreach ($nivel2['sub'] as $nivel3) { ?>
			                            <li class=" <?php if($location=='instructor'){ ?>active<?php } ?>"><a
			                                    href="<?php echo $nivel3['target']; ?>"><?php echo $nivel3['menu']; ?></a></li>
			                            <?php } ?>
			                        </ul>
			                    </li>
			                    <?php }else { ?>
			                    <li class=" <?php if($location=='home'){ ?>active<?php } ?>"><a
			                            href="<?php echo $nivel2['target']; ?>"><?php echo $nivel2['menu']; ?></a></li>
			                    <?php } ?>
			                    <?php } ?>
			                </ul>
			            </li>

	            	<?php }else { ?>
	            <li class=" <?php if($location=='home'){ ?>active<?php } ?>"><a
	                    href="index.php"><?php echo $nivel1['menu']; ?></a></li>
	            <?php } } }?>

	            <li class="hasSubmenu <?php if($location=='megaMenu'){ ?>active<?php } ?>">
	                <a href="#menu_LudusLMS" data-toggle="collapse" class="glyphicons tie"><img
	                        src="../assets/images/usuarios/ludus.png" width="24px"><span>Ludus LMS</span><span
	                        class="fa fa-chevron-down"></span></a>
	                <ul class="collapse" id="menu_LudusLMS">
	                    <li class=""><a href="tickets.php">Reportar Problemas</a></li>
	                    <!-- <li class=""><a href="staff.php">Quiénes Somos</a></li> -->
	                </ul>
	            </li>
	            
	            <li class="hasSubmenu">
					<a href="#menu_salir" data-toggle="collapse" class="glyphicons lock"><i></i><span>Salir</span><span class="fa fa-chevron-down"></span></a>
					<ul class="collapse" id="menu_salir">
						<li class=""><a href="src/salir.php">Cerrar Sesión</a></li>
					</ul>
				</li>
	        </ul>
			<div class="clearfix"></div>
			<!-- // Menu END -->
		</div>
		<!-- // Scrollable Menu wrapper with Maximum Height END -->
	</div>
	<!-- // Sidebar Menu END -->
<?php } ?>