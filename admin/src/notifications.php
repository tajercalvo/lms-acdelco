<?php include('controllers/mensajes.php');  ?>
<ul class="topnav pull-right hidden-sm hidden-xs">
	<?php if($invitado==false){ ?>
	<li class="dropdown dd-1">
		<a href="" class="glyphicons pencil single-icon" data-toggle="dropdown"><i></i>
		<!-- <span class="badge fix badge-danger" id="CantNR_Mensajes"><?php echo $cantidad_mensajes; ?></span> -->
	</a>
		<ul class="dropdown-menu pull-right inbox">
			<li class="primary">
				<a href="mensajes.php" class="glyphicons pencil">Ir a Tareas<i></i>
				</a>
			</li>
			<?php foreach ($Data_Mensajes as $key => $Datos_Mensajes) { ?>
			<li>
				<div class="innerAll">
					<span class="padding-none">De <a href="usuario" onclick="return false;" class="clean"><?php echo $Datos_Mensajes['first_creator'].' '.$Datos_Mensajes['last_creator'] ?></a> <?php echo $Datos_Mensajes['date_creation']; ?></span>
					<small><?php echo substr($Datos_Mensajes['message'], 0,100)?></small>
				</div>
				<a href="mensajes.php" class="clean btn btn-icon innerAll"><i class="fa fa-link"></i> Ver</a>
			</li>
			<?php } ?>
		</ul>
	</li>
	<?php } ?>
	<?php if($invitado==false){ ?>
	<li class="dropdown dd-1">
		<a href="circulares.php" class="glyphicons bell single-icon" data-toggle="dropdown"><i></i><span class="badge fix <?php if($cantidad_notificaciones>0){ ?> badge-danger <?php }else{ ?> badge badge-pill badge-light <?php } ?>" id="CantNR_Notificaciones"><?php echo $cantidad_notificaciones; ?></span></a>
		<ul class="dropdown-menu pull-right notifications">
			<li class="first"><a href="circulares.php">Ir a circulares <i class="fa fa-arrow-right"></i></a></li>
			<?php foreach ($Data_Notificaciones as $key => $Datos_Noficaciones) {
				?>
			<li class="innerAll">
				<div class="media">
					<div class="media-body">
						<span><?php echo $Datos_Noficaciones['title']?></span>
						<small><?php echo $Datos_Noficaciones['date_edition']; ?>:</small>
						<a href="circulares.php?id=<?php echo($Datos_Noficaciones['newsletter_id']); ?>" class="clean btn btn-icon innerAll"><i class="fa fa-link"></i> Ver</a>
					</div>
				</div>
			</li>
			<?php } ?>
		</ul>
	</li>
	<?php } ?>
</ul>
