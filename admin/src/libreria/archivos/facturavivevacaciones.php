<?php


$html = '
<table border="1" cellpadding="1" cellspacing="1" id="reservas" style="width:700px;">
	<tbody>
		<tr>
			<td colspan="5" style="text-align: right;"><img src="vivevacaciones_logo.jpg" style="width: 260px; height: 83px; float: left;">​</td>
			<td colspan="4" style="text-align: right;">
			<p style="text-align: center;">Carrera 96 No. 70A-85 Piso 2 Local 202A Centro Comercial Diver Plaza Email: info@vivevacaciones.com.co Telefonos:(+57) 1-2526595- 2520669 Celular: 3114133527</p>
			</td>
		</tr>
		<tr>
			<td colspan="9">Confirmacion de la Reserva</td>
		</tr>
		<tr>
			<td>Fecha:</td>
			<td colspan="2">07/04/2017</td>
			<td colspan="4">Nombres</td>
			<td colspan="2">Nombre de la Persona Titular</td>
		</tr>
		<tr>
			<td>Salida:</td>
			<td colspan="2">07/05/2017</td>
			<td colspan="4">Telefonos:</td>
			<td colspan="2">3133858727 -88432323</td>
		</tr>
		<tr>
			<td>Regreso:</td>
			<td colspan="2">13/05/2017</td>
			<td colspan="4">Email:</td>
			<td colspan="2">emailcualquiera@gmail.com</td>
		</tr>
		<tr>
			<td colspan="9">Descripcion de Servicios</td>
		</tr>
		<tr>
			<td colspan="9">ALOJAMIENTO EN EL HOTEL LORD PIERRE CON ALIMENTACION DESAYUNOS ALMUERZOS Y CENAS BEBIDAS ILIMITADAS EN HORARIO ESTABLECIDO POR EL HOTEL NO INCLUYE TARJETA DE ENTRADA A LA ISLA, IMPUESTOS DE ENTRADA A LOS SITIOS DE INTERES CHECK OUT 12:00PMCHECK INN 03:00PM VUELTA A LA ISLA, ASISTENCIA MEDICA .AEROPUERTO HOTEL AEROPUERTO,TOUR A JHONY CAY-ACUARIO (SIN ALMUERZO), TOUR POR LA BAHIA, SNACK DE MEDIA TARDE, TRASLADO</td>
		</tr>
		<tr>
			<td colspan="9">Datos de los pasajeros</td>
		</tr>
		<tr>
			<td>Nombres:</td>
			<td colspan="2">Apellidos:</td>
			<td colspan="4">No.Identificacion</td>
			<td colspan="2">F. Nacimiento</td>
		</tr>
		<tr>
			<td>Nombre1</td>
			<td colspan="2">Apellido1</td>
			<td colspan="4">123456789</td>
			<td colspan="2">10/10/1980</td>
		</tr>
		<tr>
			<td>Nombre2</td>
			<td colspan="2">Apellido2</td>
			<td colspan="4">234567891</td>
			<td colspan="2">10/10/1981</td>
		</tr>
		<tr>
			<td>Nombre3</td>
			<td colspan="2">Apellido3</td>
			<td colspan="4">345678912</td>
			<td colspan="2">10/10/1982</td>
		</tr>
		<tr>
			<td>Nombre4</td>
			<td colspan="2">Apellido4</td>
			<td colspan="4">456789123</td>
			<td colspan="2">10/10/1983</td>
		</tr>
		<tr>
			<td>Nombre5</td>
			<td colspan="2">Apellido5</td>
			<td colspan="4">567891234</td>
			<td colspan="2">10/10/1984</td>
		</tr>
		<tr>
			<td>Nombre6</td>
			<td colspan="2">Apellido6</td>
			<td colspan="4">678912345</td>
			<td colspan="2">10/10/1985</td>
		</tr>
		<tr>
			<td>Nombre7</td>
			<td colspan="2">Apellido7</td>
			<td colspan="4">789123456</td>
			<td colspan="2">10/10/1986</td>
		</tr>
		<tr>
			<td>Nombre8</td>
			<td colspan="2">Apellido8</td>
			<td colspan="4">891234567</td>
			<td colspan="2">10/10/1987</td>
		</tr>
		<tr>
			<td>Nombre9</td>
			<td colspan="2">Apellido9</td>
			<td colspan="4">912345678</td>
			<td colspan="2">10/10/1988</td>
		</tr>
		<tr>
			<td colspan="9">Datos Trayectos</td>
		</tr>
		<tr>
			<td>Trayecto:</td>
			<td colspan="6" rowspan="1">Bogota - San Andres</td>
			<td>Aereolinea:</td>
			<td>Avianca</td>
		</tr>
		<tr>
			<td>Fecha:</td>
			<td>14/ABR/2016</td>
			<td>Salida:</td>
			<td>09:00</td>
			<td>Llegada:</td>
			<td colspan="2">10:00</td>
			<td>Vuelo:</td>
			<td>AV7540</td>
		</tr>
		<tr>
			<td>Trayecto:</td>
			<td colspan="6" rowspan="1">San Andres - Bogota</td>
			<td>Aereolinea:</td>
			<td>Avianca</td>
		</tr>
		<tr>
			<td>Fecha:</td>
			<td>14/ABR/2016</td>
			<td>Salida:</td>
			<td rowspan="1">19:00 </td>
			<td rowspan="1">Llegada:</td>
			<td colspan="2" rowspan="1">10:00</td>
			<td>Vuelo:</td>
			<td>AV7542</td>
		</tr>
		<tr>
			<td colspan="9">FORMA DE PAGO</td>
		</tr>
		<tr>
			<td>Valor del programa:</td>
			<td colspan="2">5,902,000</td>
			<td colspan="4">Valor abono</td>
			<td colspan="2">3,000,000</td>
		</tr>
		<tr>
			<td>Numero de cuotas:</td>
			<td colspan="2">2</td>
			<td colspan="4">Valor del saldo</td>
			<td colspan="2">2,902,000</td>
		</tr>
		<tr>
			<td>Forma de Pago:</td>
			<td colspan="8" rowspan="1">Efectivo:  T.C:   No. T.C:         Titutular T.C       C.C Titular T.C:</td>
		</tr>
		<tr>
			<td>Condiciones:</td>
			<td colspan="8">Debe realizar abono los primeros 5 dias de cada mes y 1 mes antes del viaje estar cancelado</td>
		</tr>
		<tr>
			<td colspan="9">Datos del Operador</td>
		</tr>
		<tr>
			<td rowspan="1">OPERADOR:</td>
			<td rowspan="1">FUNCIONARIO:</td>
			<td rowspan="1">TELEFONO:</td>
			<td colspan="3" rowspan="1">CELULAR:</td>
			<td colspan="2" rowspan="1">EMAIL:</td>
			<td rowspan="1">No RESERVA:</td>
		</tr>
		<tr>
			<td rowspan="1">Caminantes</td>
			<td rowspan="1">Luis Castro</td>
			<td rowspan="1">6110512</td>
			<td colspan="3" rowspan="1">000000</td>
			<td colspan="2" rowspan="1"></td>
			<td rowspan="1">12345</td>
		</tr>
		<tr>
			<td colspan="9">DEPOSITOS AL OPERADOR</td>
		</tr>
		<tr>
			<td rowspan="1">Fecha</td>
			<td rowspan="1">Valor</td>
			<td rowspan="1">Medio de Pago</td>
			<td colspan="3" rowspan="1">Canal</td>
			<td colspan="2" rowspan="1">No. Trasaccin</td>
			<td rowspan="1">Saldo</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td colspan="3"></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td colspan="3"></td>
			<td colspan="2"></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="9">ELABORADO POR</td>
		</tr>
		<tr>
			<td>Asesor:</td>
			<td></td>
			<td>Telefono:</td>
			<td colspan="3"></td>
			<td colspan="2">Email:</td>
			<td></td>
		</tr>
		<tr>
			<td>Observaciones:</td>
			<td colspan="8" rowspan="1"></td>
		</tr>
		<tr>
			<td colspan="9">Clausula de Responsabilidad</td>
		</tr>
		<tr>
			<td colspan="9">
			<style type="text/css">@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		a:link { so-language: zxx }
			</style>
			<p align="left" style="margin-bottom: 0cm; line-height: 100%"><font color="#000000"><font face="Calibri, serif"><font style="font-size: 8pt">Se aplican CLAUSULAS DE RESPONSABILIDAD, segn lo establecido de conformidad con la reglamentacin expedida por el Gobierno Nacional, Ley 300 de 1996. </font></font></font></p>

			<p align="left" style="margin-bottom: 0cm; line-height: 100%"><font color="#000000"><font face="Calibri, serif"><font style="font-size: 8pt">El artculo 110, seala que cuando un nio, nia o adolescente vaya a salir del pas con uno de los padres o con una persona distinta a los representantes legales deber obtener previamente el permiso de aquel con que vaya o del de aquellos, debidamente autenticado ante el notario o autoridad consular. </font></font></font></p>

			<p align="left" style="margin-bottom: 0cm; line-height: 100%"><font color="#000000"><font face="Calibri, serif"><font style="font-size: 8pt">La ley exige adems que el permiso contenga el lugar de destino, el propsito del viaje y la fecha de salida e ingreso de nuevo al pas</font></font></font></p>

			<p align="left" style="margin-bottom: 0cm; line-height: 100%"><font color="#000000"><font face="Calibri, serif"><font style="font-size: 8pt">.Estamos comprometidos con el Cdigo de Conducta, que ordena proteger a los menores de edad de todas las formas de explotacin, pornografa y violencia sexual. Artculo 16, Ley 679 de 2001.</font></font></font></p>
			</td>
		</tr>
		<tr>
			<td colspan="9">Por favor verifique que la informacion ingresada sea correcta ya que con su firma se da por hecho que usted esta de acuerdo con los servicios turisticos contratados, cualquier cambio o cancelacion de esta reserva origina penalidades por parte de la aerolinea, hotel y operadores .</td>
		</tr>
		<tr>
			<td colspan="9"></td>
		</tr>
	</tbody>
</table>


';

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");
$mpdf=new mPDF('c'); 

$mpdf->mirrorMargins = true;

$mpdf->SetDisplayMode('fullpage','two');

$mpdf->WriteHTML($html);

$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================


?>