<?php

include('controllers/registro_reservas.php');

$html = '
<html>
<head>
<style>
body {font-family: sans-serif;
	font-size: 10pt;
}
p {	margin: 0pt; }
table.items {
	border: 0.1mm solid #000000;
}
td { vertical-align: top; }
.items td {
	border-left: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
table thead td { background-color: #EEEEEE;
	text-align: center;
	border: 0.1mm solid #000000;
	font-variant: small-caps;
}
.items td.blanktotal {
	background-color: #EEEEEE;
	border: 0.1mm solid #000000;
	background-color: #FFFFFF;
	border: 0mm none #000000;
	border-top: 0.1mm solid #000000;
	border-right: 0.1mm solid #000000;
}
.items td.totals {
	text-align: right;
	border: 0.1mm solid #000000;
}
.items td.cost {
	text-align: "." center;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
<table width="100%"><tr>
<td width="50%" style="color:#000000; "><span style="font-weight: bold; font-size: 14pt;">Vivevacaciones</span><br />Nit 57445188-3 Regimen simplicado <br />Centro comercial Diver plaza cra 96 # 70ª-85 piso 2<br /><span style="font-family:dejavusanscondensed;">Telefonos: 2526595/2520669/3114133527</span> Email: comercialvivevacaciones@gmail.com</td>
<td width="50%" style="text-align: right;"><img src="vivevacaciones_logo2.jpg"><br /><span style="font-weight: bold; font-size: 12pt;"></span></td>
</tr></table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: center; padding-top: 3mm; ">
Pagina {PAGENO} de {nb}  Vendido por: Monica estrada
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->

<div style="text-align: right">Factura Nª 002902</div>

<table width="100%" style="font-family: serif;" cellpadding="10">

<tr>
<td width="45%" style="border: 0.1mm solid #888888; "><span style="font-size: 7pt; color: #555555; font-family: sans;">Fecha:</span><br>31 de marzo de 2016<br>Recibida y aceptada firma comparador:</td>
<td width="10%">&nbsp;</td>
<td width="45%" style="border: 0.1mm solid #888888;"><span style="font-size: 7pt; color: #555555; font-family: sans;">Cliente:</span><br /><br />GALLIUM DE COLOMBIA S.A.S  <br />NIT 8070004756-5<br />Telefono: 8764577<br />Direccion:Aut medellin km 2.5 via parcelas 900mts oikos occidente bodega d-63 cota </td>
</tr>
</table>

<br />

<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
<thead>
<tr>
<td >Item</td>
<td >Cantidad</td>
<td >Descripción</td>
<td >Precio unitario</td>
<td >Total</td>


</tr>
</thead>
<tbody>
<!-- ITEMS HERE -->';
for ($i=1; $i < 21 ; $i++) { 
$html.='	
<tr>
<td align="center">'; $html.=  $i; $html.='</td>
<td align="center">'; $html.='1';  $html.='</td>
<td>';$html.='TIQUETE AEREO BOGOTA CALI BOGOTA PASAJERO JHON GOMEZ FECHA 04 AL 09 ABRIL '; $html.= '</td>
<td class="cost">';$html.='332.000'; $html.='</td>
<td class="cost">';$html.='332.000'; $html.='</td>
</tr>';

}
$html.='
<!-- END ITEMS HERE row span 5 determina el numeor de filas que tendra el espacio en blanco de acuerdo a la file de totales -->
<tr>
<td class="blanktotal" colspan="3" rowspan="5">Observacones: Promocion tour por toda europa por una semana, incluye Hospedaje, alimentación snack, rumbas de noche, tequila, alquiler de carro BMW o Mercedez benz, Bono para juegos etipo loteria, bingo, masajes relajantes, partido de futbol en la final de champions league, visita al mar muerto, entro otras etc.</td>
<td class="totals">Subtotal:</td>
<td class="totals cost">$ 39.000.000</td>
</tr>
<tr>
<td class="totals">Iva:</td>
<td class="totals cost">$ 6.240.000</td>
</tr>

<tr>
<td class="totals"><b>TOTAL:</b></td>
<td class="totals cost"><b>$ 45.240.000</b></td>
</tr>
<tr>
<td class="totals">Deposito:</td>
<td class="totals cost">$ 45.2000.000</td>
</tr>
<tr>
<td class="totals"><b>Debe:</b></td>
<td class="totals cost"><b>$ 40.00</b></td>
</tr>
</tbody>
</table>


<div style="text-align: right; font-style: italic;">Feliz viaje te desea Vivevacaciones!</div>
<br>


</body>
</html>
';
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

define('_MPDF_PATH','../');
include("../mpdf.php");


$mpdf=new mPDF('c','A4','','',20,15,48,25,10,10); 
$mpdf->SetProtection(array('print'));
$mpdf->SetTitle("Factura Vivevacaciones");
$mpdf->SetAuthor("Factura Vivevacaciones");
$mpdf->SetWatermarkText("Pendiente por pagar");
$mpdf->showWatermarkText = true;
$mpdf->watermark_font = 'DejaVuSansCondensed';
$mpdf->watermarkTextAlpha = 0.1;
$mpdf->SetDisplayMode('fullpage');


$mpdf->WriteHTML($html);

//$mpdf->Output('Factura_vivevacaciones.pdf','D'); descarga automaticamete
//$mpdf->Output('Factura_vivevacaciones.pdf','I'); Envi documento al navegador, este lo abe si tiene la capacidad
$mpdf->Output('Factura_vivevacaciones.pdf','I'); 
//$mpdf->Output(); exit;

exit;

?>