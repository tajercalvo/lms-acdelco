<?php

$ruta = '../../../';
include($ruta.'controllers/calificaciones2.php');


$tabla = '
<head>

</head>
<body>

<style type="text/css">
	table.tableizer-table {
		font-size: 11px;
		border: 1px solid #CCC;
		font-family: Arial, Helvetica, sans-serif;
	}
	.tableizer-table td {

		border: 1px solid #CCC;
	}
	.tableizer-table th {
		background-color: #FFFFFF;
		color: #000000;
		font-weight: bold;
		border: 1px solid #CCC;
	}
</style>
<table  class="tableizer-table" >
<thead border = "1">
<tr class="tableizer-firstrow">
	<tr>
	<th colspan="2" scope="col"><img src="../../../../assets/images/GmAcademyLogo.png" style="width: 100px; height: 50px; float: left;">​</th>
	<th colspan="2" scope="col" align="left">Curso: '. $dato_sesion['course'].'</th>
      <th colspan="3" scope="col" align="left">concesionario:</th>
      <th colspan="3" scope="col" align="left">Lugar: '. $dato_sesion['living'].'</th>
    </tr>
    <tr>

	   <th colspan="2" scope="col" align="left">Lista de asistencia</th>
	   <th colspan="2" scope="col" align="left">Intructor: '.$dato_sesion['first_name'].' '.$dato_sesion['last_name'].'</th>
      <th scope="col" align="left">Ciudad: '.$dato_sesion['city'].'</th>
      <th scope="col" align="left">Año: '.substr($dato_sesion['start_date'], 0, 4).'</th>
      <th scope="col" align="left">Mes: '.substr($dato_sesion['start_date'], 5, 2).'</th>
      <th scope="col" align="left">Dia: <br> '.substr($dato_sesion['start_date'], 8, 2).'</th>
      <th colspan="2" scope="col" align="left">Numero de hoja: 1</th>
    </tr>

</tr>
<tr bgcolor="#b0b0b0" ><td>No.</td><td align="center" WIDTH="200">NOMBRE Y APELLIDO</td><td align="center" WIDTH="100">No. IDENTIFICACIÓN</td><td align="center" WIDTH="200">ESPECIALIDAD Y CARGO</td><td align="center" WIDTH="200">SEDE</td><td align="center" WIDTH="50">E.I.</td><td align="center" WIDTH="50" >E.F.</td><td align="center"> DESEMPEÑO</td><td align="center">NOTA FINAL</td><td align="center" WIDTH="300" >FIRMA</td></tr>
</thead>
	<tbody>';

			  if(count($datosInvitaciones) > 0){
					$var_ids = 0;
					foreach ($datosInvitaciones as $iID => $data) {
						$var_ids++;
			  $tabla .='<tr >
					      <td height="30">'.$var_ids.'</td>
					      <td height="30">'. $data['first_name'].' '.$data['last_name']. '</td>
					      <td height="30">'. $data['identification']. '</td>
					      <td height="30">'; foreach ($data['Cargos'] as $iID => $data_cargos) { $tabla .=$data_cargos['charge'].'<br>'; } $tabla .= '</td>
					      <td height="30">'. $data['headquarter']. '</td>
					      <td height="30"></td>
					      <td height="30"></td>
					      <td height="30"></td>
					      <td height="30"></td>
					      <td height="30"></td>
			    </tr> ';
			}

			 $tabla .='</tbody>
			</table>';

 }else{
	$tabla = '<td>No hay Alumnos para esta sesión</td>';
 }

$tabla .='</tbody>
</table>
</body>
';

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");

// $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);
$mpdf=new mPDF();

$mpdf->SetDisplayMode('fullpage');

$mpdf->AddPage('H'); // Muestra el documento horizontalmente

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($tabla);

$mpdf->Output();
exit;


?>
