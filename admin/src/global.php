<!-- Global -->
<script>
	var basePath = '',
		commonPath = '../template/assets/',
		rootPath = '../',
		DEV = false,
		componentsPath = '../template/assets/components/';
	var primaryColor = '#e5412d',
		dangerColor = '#b55151',
		infoColor = '#5cc7dd',
		successColor = '#609450',
		warningColor = '#ab7a4b',
		inverseColor = '#45484d';
	var themerPrimaryColor = primaryColor;
</script>
<script src="../template/assets/components/library/bootstrap/js/bootstrap.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/nicescroll/jquery.nicescroll.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/breakpoints/breakpoints.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/holder/holder.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v2.3.0"></script>
<!-- BIblioteca -->
<?php if (isset($script_select2v4)) {?>
<script src="../template/assets/components/npm/select2/dist/js/select2.js"></script>
<?php }else{?>
<script src="../template/assets/components/modules/admin/forms/elements/select2/assets/lib/js/select2.js?v=v2.3.0"></script>
<?php } ?>
<script src="../template/assets/components/modules/admin/forms/elements/uniform/assets/lib/js/jquery.uniform.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/uniform/assets/custom/js/uniform.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/datatables/assets/lib/extras/TableTools/media/js/TableTools.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/datatables/assets/lib/extras/ColVis/media/js/ColVis.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/datatables/assets/lib/extras/FixedHeader/FixedHeader.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/datatables/assets/lib/extras/ColReorder/media/js/ColReorder.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/responsive/assets/lib/js/footable.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/responsive/assets/custom/js/tables-responsive-footable.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/tables/classic/assets/js/tables-classic.init.js?v=v2.3.0"></script>
<!--<script src="../template/assets/components/modules/admin/forms/editors/wysihtml5/assets/lib/js/wysihtml5-0.3.0_rc2.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/editors/wysihtml5/assets/lib/js/bootstrap-wysihtml5-0.0.2.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/editors/wysihtml5/assets/custom/wysihtml5.init.js?v=v2.3.0"></script>-->
<script src="../template/assets/components/modules/admin/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/modals/assets/js/bootbox.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/modals/assets/js/modals.init.js?v=v2.3.0"></script>
<!--<script src="../template/assets/components/modules/admin/messages/assets/js/messages.js?v=v2.3.0"></script>-->
<script src="../template/assets/components/modules/admin/forms/editors/wysihtml5/assets/lib/js/wysihtml5-0.3.0_rc2.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/editors/wysihtml5/assets/lib/js/bootstrap-wysihtml5-0.0.2.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/editors/wysihtml5/assets/custom/wysihtml5.init.js?v=v2.3.0"></script>

<script src="../template/assets/components/modules/admin/forms/elements/bootstrap-datepicker/assets/lib/js/bootstrap-datepicker.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/bootstrap-datepicker/assets/custom/js/bootstrap-datepicker.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/bootstrap-switch/assets/lib/js/bootstrap-switch.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/bootstrap-switch/assets/custom/js/bootstrap-switch.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/fuelux-radio/fuelux-radio.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/button-states/button-loading/assets/js/button-loading.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/multiselect/assets/lib/js/jquery.multi-select.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/multiselect/assets/custom/js/multiselect.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/multiselect/assets/lib/js/jquery.multi-select.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/multiselect/assets/custom/js/multiselect.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/inputmask/assets/lib/jquery.inputmask.bundle.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/inputmask/assets/custom/inputmask.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/bootstrap-timepicker/assets/lib/js/bootstrap-timepicker.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/colorpicker-farbtastic/assets/js/farbtastic.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/forms/elements/colorpicker-farbtastic/assets/js/colorpicker-farbtastic.init.js?v=v2.3.0"></script>

<script src="../template/assets/components/modules/admin/sliders/range-sliders/assets/lib/mousewheel/jquery.mousewheel.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/sliders/range-sliders/assets/lib/jqrangeslider/js/jQAllRangeSliders-withRuler-min.js?v=v2.3.0"></script>
<!--<script src="../template/assets/components/modules/admin/sliders/range-sliders/assets/custom/js/range-sliders.init.js?v=v2.3.0"></script>-->
<!--<script src="../template/assets/components/modules/admin/sliders/jqueryui-sliders/assets/custom/js/jqueryui-sliders.init.js?v=v2.3.0"></script>-->
<script src="js/global.js?varRand=<?php echo(rand(10,999999));?>"></script>
<!-- AngularJS -->
<script src="../template/assets/components/plugins/angular/angular.min.js"></script>
<script src="../template/assets/components/plugins/angular-route/angular-route.min.js"></script>
<!-- <script src="../template/assets/components/plugins/angular-ui-swiper/dist/angular-ui-swiper.js"></script> -->
<!-- <script src="../template/assets/components/plugins/angular/lib/angular-swiper.js"></script> -->
<script src="../template/assets/components/plugins/nprogress/nprogress.js"></script>
<!-- <script src="../template/assets/components/plugins/chartjs/Chart.min.js"></script> -->
<script src="../template/assets/components/plugins/chart.js/dist/Chart.min.js"></script>
<script src="../template/assets/components/plugins/chart.js/dist/Chart.bundle.min.js"></script>
<!-- End AngularJS -->
<?php if($location=="education" && isset($review_id) ){ ?>

<?php } ?>

<?php if($location=="reporting"){ ?>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.resize.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/sparkline/jquery.sparkline.min.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/sparkline/sparkline.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/maps/vector/assets/lib/jquery-jvectormap-1.2.2.min.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/maps/vector/assets/lib/maps/jquery-jvectormap-world-mill-en.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/maps/vector/assets/custom/maps-vector.world-map-markers.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.pie.js?v=v2.3.0"></script>
<?php } ?>
<?php if(isset($Asist)){ ?>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.resize.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/plugins/jquery.flot.tooltip.min.js?v=v2.3.0"></script>
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotcharts.common.js?v=v2.3.0"></script>-->
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/plugins/jquery.flot.orderBars.js?v=v2.3.0"></script>
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-bars-ordered.init.js?v=v2.3.0"></script>-->
<?php } ?>
<?php if($location=="centinel" || ( isset($DashBoard) && $DashBoard ) ){ ?>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.resize.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/plugins/jquery.flot.tooltip.min.js?v=v2.3.0"></script>
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotcharts.common.js?v=v2.3.0"></script>-->
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-simple.init.js?v=v2.3.0"></script>-->
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-mixed-1.init.js?v=v2.3.0"></script>-->
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-line-2.init.js?v=v2.3.0"></script>-->
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-line.init.js?v=v2.3.0"></script>-->
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/plugins/jquery.flot.orderBars.js?v=v2.3.0"></script>
	<!--<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-bars-ordered.init.js?v=v2.3.0"></script>-->
	<script src="../template/assets/components/modules/admin/charts/flot/assets/lib/jquery.flot.pie.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-donut.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-bars-stacked.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-pie.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/charts/flot/assets/custom/js/flotchart-bars-horizontal.init.js?v=v2.3.0"></script>
<?php } ?>
<?php if($location=="tutor" || $location=="home" || $location=="biblioteca"){ ?>
	<script src="../template/assets/components/modules/admin/gallery/gridalicious/assets/lib/jquery.gridalicious.min.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/gallery/gridalicious/assets/custom/gridalicious.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/gallery/blueimp-gallery/assets/lib/js/blueimp-gallery.min.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/gallery/blueimp-gallery/assets/lib/js/jquery.blueimp-gallery.min.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/gallery/prettyphoto/assets/lib/js/jquery.prettyPhoto.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/gallery/prettyphoto/assets/custom/prettyphoto.init.js?v=v2.3.0"></script>
	<!--<script src="../template/assets/components/plugins/owl-carousel/owl.carousel.min.js?v=v2.3.0"></script> -->

	<!--<script src="../template/assets/components/modules/admin/content/assets/news-featured-2.init.js?v=v2.3.0"></script>-->
	<!--<script src="../template/assets/components/modules/admin/content/assets/news-featured-1.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/content/assets/news-featured-3.init.js?v=v2.3.0"></script>-->
<?php } ?>
<?php if(isset($qTip_UI)){ ?>
	<script src="../template/assets/components/modules/admin/ui/pagination/pagination-jquery/assets/lib/jquery.bootpag.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/ui/pagination/pagination-jquery/assets/custom/jquery.bootpag.init.js?v=v2.3.0"></script>
<?php } ?>
<?php if(isset($RankingLid)){ ?>
	<script src="../template/assets/components/modules/admin/widgets/widget-progress/assets/js/widget-progress.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/plugins/holder/holder.js?v=v2.3.0"></script>
	<script src="../template/assets/components/plugins/slimscroll/jquery.slimscroll.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/widgets/widget-scrollable/assets/js/widget-scrollable.init.js?v=v2.3.0"></script>
<?php } ?>
<script src="../template/assets/components/plugins/jquery.event.move/js/jquery.event.move.js?v=v2.3.0"></script>
<script src="../template/assets/components/plugins/jquery.event.swipe/js/jquery.event.swipe.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/notifications/notyfy/assets/lib/js/jquery.notyfy.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/notifications/notyfy/assets/custom/js/notyfy.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/notifications/gritter/assets/lib/js/jquery.gritter.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/notifications/gritter/assets/custom/js/gritter.init.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/gallery/blueimp-gallery/assets/lib/js/blueimp-gallery.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/modules/admin/gallery/blueimp-gallery/assets/lib/js/jquery.blueimp-gallery.min.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/megamenu.js?v=v2.3.0"></script>
<script src="../template/assets/components/core/js/core.init.js?v=v2.3.0"></script>

<?php if(isset($CalendarData) && $CalendarData ){ ?>
	<script src="../template/assets/components/modules/admin/calendar/assets/lib/js/moment.min.js?v=v2.3.0"></script>
	<script src="../template/assets/components/modules/admin/calendar/assets/lib/js/fullcalendar.min.js?v=v2.3.0"></script>
	<!--<script src="../template/assets/components/modules/admin/calendar/assets/lib/js/lang-all.js"></script>-->
	<script src="../template/assets/components/modules/admin/calendar/assets/custom/js/jquery.qtip.min.js"></script>
<?php } ?>
<?php if(isset($qtip)){ ?>
	<script src="../template/assets/components/modules/admin/calendar/assets/custom/js/jquery.qtip.min.js"></script>
<?php } ?>
<script src="../template/assets/components/modules/admin/widgets/widget-collapsible/assets/widget-collapsible.init.js?v=v2.3.0"></script>
	<script src="../template/assets/components/plugins/OwlCarousel2-2.2.0/dist/owl.carousel.min.js"></script>
	<!-- <script src="../template/assets/components/plugins/Swiper-master/dist/js/swiper.min.js"></script> -->
