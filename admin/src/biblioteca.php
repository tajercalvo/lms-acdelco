<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/biblioteca.php');
$location = 'biblioteca';
$script_select2v4 = 'select2v4';
$qtip = 'qtip';
$qTip_UI = 'true';
$PuedeCrearB = Fcn_TieneRolValue(14);
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
	<link rel="stylesheet" href="css/mundo_chevrolet.css" media="screen" title="biblioteca">
	<link rel="stylesheet" href="../assets/components/modules/admin/forms/elements/select2v4/assets/lib/css/select2.css" media="screen" title="biblioteca">
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" /> -->
</head>

<style type="text/css">
		.imagen_miniatura {
			width: 250px;
			height:250px;
			position:relative;
		}

		.imagen_miniatura img{
			width: 100%;
		}

		.imagen_miniatura>div
		{
			color: black;
			background-color: white;
			padding:2px 10px;
			font-weight:bold;
			text-align: center;
			height: 40px;
			display: flex;
		    justify-content: center;
		    align-items: center;
		    letter-spacing: 1.7px;
		    text-transform: uppercase;
		}

		.imagen_miniatura>span
		{
			position: relative;
			bottom:25px;
			color:#fff;
			/*padding:2px 10px;*/
			cursor: pointer;
		    -moz-border-radius: 50%;
		    -webkit-border-radius: 50%;
		    border-radius: 50%;
		    margin-left: 5px;
		    right: 40% !important;
		}

		.anterior{
			position: relative;
		    float: left;
		    top: 265px;
		    color: green;
		    margin-left: 10px;
		    cursor: pointer;
		}

		.siguiente{
			position: relative;
		    float: right;
		    top: 265px;
		    color: green;
		    margin-right: 10px;
		    cursor: pointer;
		}

		.carrusel{
			display: none;
		}

		.carrusel.active{
			display: block;
		}

		.imagen_miniatura{
			cursor: pointer;
		}

		.modal-body{
			display: none;
		}

		.modal-body.active{
			display: block;
		}

		video{
			width: 100%;
		}

		#contenido>ul>li{
			display: inline-block;
			padding: 5px;
			border-radius: 25px;
		}


		#contenido>ul>li:hover{
			transform: scale(1.1);
			-webkit-transition:all 0.5s ease; /* Safari y Chrome */
			border-radius: 5px;
		}
		

	</style>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="../admin/" class="glyphicons bank"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/biblioteca.php" >Biblioteca</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<!-- // END heading -->
					<center><h2 style="color: #0058a3;letter-spacing: 3px;font-weight: bold;">BIBLIOTECA</h2></center>
					<hr style="border: 0.8px solid #d65050;">
					<!-- Modal -->
						<div id="myModal" class="modal fade" role="dialog">
						  <div class="modal-dialog" style="width: 80%; margin-top: 5%;">

						    <!-- Modal content-->
						    <div class="modal-content">
						      <div class="modal-header" style="background: white; border-color: white;">
						        <button type="button" class="close" data-dismiss="modal" style="color: #0058a3;">&times;</button>
						        <!-- <h4 class="modal-title">Modal Header</h4> -->
						      </div>
						      <div class="modal-body active" id="mb1">
						        <div class="row">
						        	<div class="col-md-5">
						        		<img id="imagen_modal" src="http://www.lawebdelprogramador.com/codigo/jquery.preview1/jquery.preview_1.jpg" alt="" style="width: 100%; cursor: pointer;">
						        	</div>
						        	<div class="col-md-6">
						        		<p id="titulo_modal" style="color: black;font-weight: bold;letter-spacing: 3px;font-size: 18px;">Lorem Ipsum</p>
						        		<p id="descripcion_modal" style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam varius mi luctus ipsum pharetra, vel scelerisque tellus ornare. Cras feugiat tellus velit, sed imperdiet lectus mattis in. Quisque condimentum ligula in orci imperdiet, a sollicitudin ipsum egestas. Aliquam eu faucibus augue, at elementum elit. Proin vel nisi a odio fermentum dapibus. Pellentesque sed tempus dolor. Nam ultricies lectus vulputate sem tincidunt aliquet. Ut dictum velit euismod, tincidunt nulla non, suscipit quam. Donec tincidunt at odio vitae vehicula. Maecenas sodales ipsum at elit tristique rutrum. Vivamus pretium gravida faucibus. Aliquam erat volutpat.</p>
						        		<span><strong>Tipo de información: Documento</strong></span> <span><span>3.424</span> Estudiantes le han dado me gusta</span>
						        	</div>
						        	<div class="col-md-1">
						        	</div>
						        </div>
						        <div style="margin-top: 10px;background: black;width: 70%;height: 40px;position: relative;left: -15px;">
						        	<p style="color: white;text-align: right;margin-right: 5px;padding-top: 10px;">INFORMACION RECOMENDAD DE ACUERDO A TU TRAYECTORIA:</p>
						        </div>
						      </div>
						      <div class="modal-body" id="mb2" >
						        <div class="row">
						        	<div class="col-md-1">
						        		<a id="atras" style="cursor: pointer;"><i class="fa fa-arrow-left"></i> Atras</a>
						        	</div>
						        	<div class="col-md-10">
						        		<div id="contenido_modal">

						        		</div>
						        	</div>
						        	<div class="col-md-1">
						        	</div>
						        </div>
						      </div>
						    </div>

						  </div>
						</div>

					<!--CONTENIDO BIBLIOTECA DETALLE-->
					<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
						<div class="widget-body padding-none border-none">
							<div class="row">
								<!-- <div class="col-md-1 detailsWrapper"></div> -->
								<div class="col-md-12 detailsWrapper">
									<div class="innerAll">
										<div class="body" id="body" style="visibility: hidden;">
											<div class="row" id="menuPrincipal">
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<span data-segment="1" class="menu btn btn-colorgm active col-md-12">Habilidades Blandas</span>
												</div>
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<span data-segment="2" class="menu btn btn-colorgm col-md-12">Técnico</span>
												</div>
												<div class="col-md-2 text-center" style="border-right: 1px solid gainsboro;">
													<span data-segment="3"  class="menu btn btn-colorgm col-md-12">Ventas</span>
												</div>
												<div class="col-md-2 text-center">
													<span data-segment="4"  class="menu btn btn-colorgm col-md-12">Repuestos</span>
												</div>
												<div class="col-md-3 text-center" style="border-bottom: 1px solid;">
													<div class="input-group">
														<div class="input-group-btn"><span><i class="fa fa-search"></i></span></div>
													  	<input id="txtSearch" class="form-control" type="text" placeholder="Escribe tu búsqueda" style="border: none;">
													</div>
												</div>
												<div class="col-sm-1 text-center">
												</div>
											</div>
											<div class="clearfix"><br></div>
											<!-- Inicio cuadro Lo más visto -->
											<div class="row">
												<!--Cuadro configurado-->
												<div class="col-md-12">
														<div id="contenido">
															<ul>
																<?php foreach ($archivos as $key => $value) {?>
																	<li> <a href="../assets/Biblioteca/<?php echo $value['image']?>" target="_blank"><img src="../assets/Biblioteca/<?php echo $value['image']?>" style="width: 200px;height: 150px;"></a></li>
																<?php }?>
															</ul>
														
														</div>
													</div>
												</div>
											<!-- Fin cuadro lo mas visto -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--CONTENIDO BIBLIOTECA DETALLE-->

						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- // Sidebar menu & content wrapper END -->
				<?php include('src/footer.php'); ?>
			</div>
			<!-- // Main Container Fluid END -->
			<?php include('src/global.php'); ?>
			<script src="js/biblioteca.js?v=<?php echo md5(time()) ?>"></script>
		</body>
		</html>