<?php if(isset($_SESSION['idUsuario'])){ ?>
	<ul class="topnav pull-right hidden-sm hidden-xs">
	
		<!-- Profile / Logout menu -->
		<li class="account dropdown dd-1">
			<a data-toggle="dropdown" href="ver_perfil.php?id=<?php echo($_SESSION['idUsuario']); ?>" class="glyphicons logout old_man"><span class="hidden-md hidden-sm hidden-desktop-1"><?php echo( $_SESSION['NameUsuario']); ?></span><i></i></a>
			<ul class="dropdown-menu pull-right">
				<li><a href="contrasena.php" class="glyphicons unlock">Cambiar Contraseña<i></i></a></li>
				<li class="profile">
					<span>
						<span class="media display-block margin-none">
							<span class="pull-left display-block thumb">
								<img src="../assets/images/usuarios/logouser.png"<?php echo($_SESSION['imagen']); ?> style="width: 35px;" alt="<?php echo($_SESSION['NameUsuario']); ?>">
							</span>
							<span class="media-body display-block">
								<a href="ver_perfil.php?id=<?php echo($_SESSION['idUsuario']); ?>"><?php echo($_SESSION['NameUsuario']); ?></a>
								<small><?php echo($_SESSION['eMailUsuario']); ?></small>
							</span>
						</span>
					</span>
				</li>
				<li class="innerTB half">
					<span>
						<a class="btn btn-default btn-xs pull-right" href="src/salir.php">Salir</a>
						<span class="clearfix"></span>
					</span>
				</li>
			</ul>
					</li>
		<!-- // Profile / Logout menu END -->
	</ul>
<?php } ?>