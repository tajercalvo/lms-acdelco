<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
	<div class="widget-body padding-none border-none">
		<div class="row">
			<div class="col-md-1 detailsWrapper">
			</div>
			<div class="col-md-10 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
				<div class="innerAll">
					<div class="body">
						<div class="row padding">
							<div class="col-md-12">
								<div id="banner-home" class="owl-carousel owl-theme">
									<?php foreach($listado_BannerTop as $keys => $lbtop){
										$dealerArray = explode(",",$lbtop['dealers_id']);
										foreach ($dealerArray as $key => $value) {
											if($value == ''){ ?>
												<div class="item">
													<a href="<?php echo($lbtop['banner_url']); ?>" target="_blank">
														<img src="../assets/gallery/banners/<?php echo($lbtop['file_name']); ?>" alt="<?php echo($lbtop['banner_name']); ?>" class="img-responsive padding-none border-none" />
													</a>
												</div>
											<?php }elseif($value == $_SESSION['dealer_id']){ ?>
												<div class="item">
													<a href="<?php echo($lbtop['banner_url']); ?>" target="_blank">
														<img src="../assets/gallery/banners/<?php echo($lbtop['file_name']); ?>" alt="<?php echo($lbtop['banner_name']); ?>" class="img-responsive padding-none border-none" />
													</a>
												</div>
										<?php }//fin if
										}//fin segundo for
									} //fin primer foreach?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-2">
										<a href="ver_perfil.php"><img id="ImgUsuario" src="../assets/images/usuarios/<?php echo($data_user['image']); ?>" style="width: 80px;" alt="<?php echo($data_user['first_name'].' '.$data_user['last_name']); ?>" /></a>
									</div>
									<div class="col-md-10">
										<a href="ver_perfil.php"><strong><?php echo($data_user['first_name'].' '.$data_user['last_name']); ?></strong></a><br/>
										<?php echo($data_user['headquarter']); ?><br/>
											<?php echo($data_user['email']); ?><br/>
										<img src="../assets/images/wall/CHEVROLET_ng.png" width="60px" alt="concesionario"/>
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="separator bottom"></div>
								<div class="row">
									<h5 class="text-uppercase strong text-primary"><i class="fa fa-briefcase text-regular fa-fw"></i> Mis Trayectorias Académicas </h5>
									<ul class="team">
										<?php 
										$cant_ele = 1;
										foreach ($charge_user as $key => $Data_Charges) { ?>
											<li><span class="crt"><?php echo $cant_ele; ?></span><span class="strong"><?php echo($Data_Charges['charge']); ?></span><span class="muted"><a href="trayectoria.php?charge=<?php echo($Data_Charges['charge_id']); ?>" target="_blank"><?php echo($Data_Charges['charge']); ?></a></span></li>
										<?php 
										$cant_ele++;
										} ?>
									</ul>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-4" style="box-shadow: 1px 1px 10px #888 !important">
										<img src="../assets/images/distinciones/default.png" alt="" class="img-responsive padding-none border-none" />
									</div>
									<div class="col-md-1">
									</div>
									<div class="col-md-6">
										<div class="separator bottom"></div>
										<!--<img src="../assets/images/GmAcademyTexto.png" style="width: 60%; !important" alt="" class="img-responsive padding-none border-none" />-->
										<p><b>Una historia que ha dado grandes pasos por la educación</b></p>
										<p style="text-align: justify;">El origen de todo este esfuerzo parte de la iniciativa de GM Colmotores por impulsar la formación del talento humano como base fundamental para el éxito a largo plazo y para asegurar la continuidad de sus clientes.</p>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<div class="col-md-11">
									
									<p><b>TIEMPOS HERÓICOS</b></p>
									<p style="text-align: justify;">Decidimos proponernos metas más retadoras, por eso comenzamos a realizar entrenamientos por pedido. Los buenos resultados no se dieron a esperar, lo que nos motivó a dar el siguiente paso que fue hacer un entrenamiento puntual de ventas y posventa con "La Escuelita".</p>
									<p style="text-align: justify;">Ya en 1994 nace el Centro de Entrenamiento Técnico CET, consolidándose como el primer gran esfuerzo de estandarización de programas y formación continua. Dos años más tarde, en 1996, se contrata a un proveedor externo para continuar dando avances en el entrenamiento técnico. <a href="#" id="btnVerMas_Index" onclick="VerMas_Mostrar(); return false;">Ver m&aacute;s...</a></p>
									<p id="VerMasIndex" style="text-align: justify;"><img src="../assets/gallery/banners/cet.jpg" alt="" class="img-responsive padding-none border-none" /><br/>Poniéndole cada vez más ganas, dimos inicio a la aplicación de la estrategia GMDifference!, que nos permitió dar mayor definición a los requerimientos de formación continuada por perfiles de personal de la red de concesionarios.<br/><br/>Toda esta vasta experiencia adquirida a través de los años fue la mayor influencia de los programas de formación interna de GM, plataforma Sócrates, y en 2013 se da origen a GMAcademy como una estrategia de formación a largo plazo.<br/><br/>Desde sus inicios, GMAcademy  trabaja en la integración de todos los programas de formación para perfiles de cargo en los concesionarios.<br/><br/>En 2014, para atender las necesidades de nuestros concesionarios, se desarrolla la plataforma <a href="http://www.gmacademy.co/">www.gmacademy.co</a>, herramienta que ha brindado innumerables beneficios en la divulgación y gestión del conocimiento.</p>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<?php foreach ($listado_BannerHis as $key2 => $lbhis) {
										$hisArray = explode(",",$lbhis['dealers_id']);
										foreach ($hisArray as $key => $valuehis) {
											if($valuehis == ''){ ?>
												<div class="col-md-4" style="box-shadow: 1px 1px 10px #888 !important">
													<a href="<?php echo($lbhis['banner_url']); ?>" target="_blank"><img src="../assets/gallery/banners/<?php echo($lbhis['file_name']); ?>" alt="<?php echo($lbhis['banner_name']); ?>" class="img-responsive padding-none border-none" /></a>
												</div>
											<?php }elseif($valuehis == $_SESSION['dealer_id']){ ?>
												<div class="col-md-4" style="box-shadow: 1px 1px 10px #888 !important">
													<a href="<?php echo($lbhis['banner_url']); ?>" target="_blank"><img src="../assets/gallery/banners/<?php echo($lbhis['file_name']); ?>" alt="<?php echo($lbhis['banner_name']); ?>" class="img-responsive padding-none border-none" /></a>
												</div>
											<?php	}//fin if
										}//fin segundo foreach
									}//fin primer foreach ?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<?php foreach ($listado_BannerCal as $key3 => $lbcal) {
										$calArray = explode(",",$lbcal['dealers_id']);
										foreach ($calArray as $key => $valuecal) {
											if($valuecal == ''){ ?>
												<a href="<?php echo($lbcal['banner_url']); ?>">
													<img style="box-shadow: 1px 1px 10px #888 !important" src="../assets/gallery/banners/<?php echo($lbcal['file_name']); ?>" alt="<?php echo($lbcal['banner_name']);?>" class="img-responsive padding-none border-none" />
												</a>
											<?php }elseif($valuecal == $_SESSION['dealer_id']){ ?>
												<a href="<?php echo($lbcal['banner_url']); ?>">
													<img style="box-shadow: 1px 1px 10px #888 !important" src="../assets/gallery/banners/<?php echo($lbcal['file_name']); ?>" alt="<?php echo($lbcal['banner_name']);?>" class="img-responsive padding-none border-none" />
												</a>
										<?php }//fin if
										}//fin segundo foreach
									}//fin primer foreach ?>
								</div>
								<div class="separator bottom"></div>
								<div class="row">
									<h5 class="text-uppercase strong text-primary"><i class="fa fa-calendar text-regular fa-fw"></i> Programación de Cursos IBT - VCT</h5>
									<div data-component>
										<div id="calendar_actual"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="separator bottom"></div>
						<div class="row">
							<div class="widget widget-heading-simple widget-body-white margin-none border-none">
								<!--<div class="widget-head" style="padding-left: 15px;">
									<h4 class="heading glyphicons list"><i></i> </h4>
								</div>-->
								<div class="widget-body margin-none border-none">
									<table id="Table_DataTomados" class="display table table-striped table-condensed table-primary table-vertical-center">
										<thead>
											<tr>
												<th style="width: 25%;" class="center">C&oacute;digo</th>
												<th>Los cursos que he tomado:</th>
												<th style="width: 5%;" class="center">Tipo</th>
												<th style="width: 5%;" class="center">Puntaje</th>
												<th style="width: 5%;" class="center">Fecha</th>
												<th style="width: 5%;" class="center">Aprobó?</th>
												<th style="width: 5%;" class="center"></th>
											</tr>
										</thead>
										<?php foreach ($Datos_CursosTomados as $iID => $DatosCursos_Tomados) { ?>
											<tbody>
												<td class="strong left"><a href="course_detail.php?token=<?php echo(md5('GMAcademy')); ?>&_valSco=<?php echo(md5('GMColmotores')); ?>&opcn=ver&id=<?php echo($DatosCursos_Tomados['course_id']); ?>"><?php echo($DatosCursos_Tomados['newcode']); ?></a></td>
												<td class="left"><?php echo($DatosCursos_Tomados['course']); ?></td>
												<td class="strong center"><?php echo($DatosCursos_Tomados['type']); ?></td>
												<td class="center"><?php echo(number_format($DatosCursos_Tomados['score'],0)); ?></td>
												<td class="center"><?php echo($DatosCursos_Tomados['date_creation']); ?></td>
												<td class="center">
													<?php if($DatosCursos_Tomados['score'] >= 75){ ?>
														<span class="btn-action glyphicons thumbs_up btn-success"><i></i></span>
													<?php }else{ ?>
														<span class="btn-action glyphicons thumbs_down btn-danger"><i></i></span>
													<?php } ?>
												</td>
												<?php if($DatosCursos_Tomados['score'] >= 75){ ?>
													<td class="center"><a href="certificado.php?course_id=<?php echo $DatosCursos_Tomados['course_id']; ?>&date=<?php echo $DatosCursos_Tomados['date_creation']; ?>" class="btn-action glyphicons certificate btn-info" target="_blank"><i></i></a></td>
												<?php }else{ ?>
													<td class="center btn-danger">
														<span class="btn-action btn-danger">Reprobó</span>
													</td>
												<?php } ?>
											</tbody>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>