<?php
include('../config/config.php');
@session_start();
if(!isset($_SESSION['NameUsuario'])){
	$link =  $_SERVER['REQUEST_URI'];
	$escaped_link = htmlspecialchars($link, ENT_QUOTES, 'UTF-8');
	// Juan Carlos Villar, 30 mayo 2019, se le agrega la variable $_SESSION la posición url_solicitada para guardar temporalmente la url que digito, una vez se loguee el sistema lo llevara a dicha url, en el login antes de iniciar la sessión se agrega el session destroy para eliminar está posisición
	$_SESSION['url_solicitada'] = $escaped_link;
	header("location: login");
}elseif(!isset($_SESSION['idUsuario'])){
	header("location: login");
}else{
    date_default_timezone_set('America/Bogota');
	$NOW_data = date("Y-m-d")." ".(date("H")).":".date("i:s");
	$fechaGuardada = $_SESSION["ultimoAcceso"];
	$ahora = date("Y-n-j H:i:s");
	$tiempo_transcurrido = (strtotime($ahora)-strtotime($fechaGuardada));

	 //Validar si tiene encuesta pendiente
	 include_once('models/encuestas_pendientes.php');
	 $encuesta = Encuestas_pendientes::getEncuentas_pendientes();
	 if( is_array($encuesta) ){
		$url = explode('/', $_SERVER['REQUEST_URI']);
		if( end($url) != 'encuesta_pendiente.php'){
			header("location: encuesta_pendiente.php");
		}
	 }
	// if( !isset($review_id) && !isset($review_enc_id) ){
	// 	if( isset($_SESSION['id_evaluacion']) && $_SESSION['id_evaluacion'] > 0 ){
	// 		header("location: evaluar.php");
	// 	}else if( isset($_SESSION['_EvalCour_id_evaluacion']) && $_SESSION['_EvalCour_id_evaluacion'] > 0 ){
	// 		header("location: evaluar.php");
	// 	}
	// 	else if( isset($_SESSION['_EncSat_ResultId']) && $_SESSION['_EncSat_ResultId'] > 0 ){
	// 		header("location: encuestar.php");
	// 	}
	// 	else if( isset($_SESSION['_EnCarg_ReviewId']) && $_SESSION['_EnCarg_ReviewId'] > 0 ){
	// 		header("location: encuestar_Cargo.php");
	// 	}else if( isset($_SESSION['evl_obligatoria']) && $_SESSION['evl_obligatoria'] > 0 ){
	// 		header("location: encuesta_gerentes.php");
	// 	}
	// }
	if($tiempo_transcurrido >= 7200) {
		//si pasaron 2 horas o más
		session_destroy(); // destruyo la sesión
		header("Location: src/salir.php"); //envío al usuario a la pag. de salida
		//sino, actualizo la fecha de la sesión
	}else {
		$_SESSION["ultimoAcceso"] = $ahora;
	}
}

function Fcn_TieneRol($level){
	$retorna = false;
	if(isset($_SESSION['id_rol'])){
		$roles = $_SESSION['id_rol'];
		foreach ($roles as $key => $value) {
			if($value['nivel'] == $level){
				$retorna = true;
			}
		}
	}else{
		header("location: login");
	}
	return $retorna;
};

function Fcn_TieneCargo($charge_usr,$charge_sel){
	$retorna = false;
	foreach ($charge_usr as $key => $value) {
		if($value['charge_id'] == $charge_sel){
			$retorna = true;
		}
	}
	return $retorna;
};

function Fcn_TieneRolValue($NecesaryRol){
	$retorna = false;
	if(isset($_SESSION['id_rol'])){
		$roles = $_SESSION['id_rol'];
		foreach ($roles as $key => $value) {
			if($value['rol_id'] == $NecesaryRol){
				$retorna = true;
			}
		}
	}else{
		header("location: login");
	}
	return $retorna;
};
$_SentinelColaborador = Fcn_TieneRolValue(8);
$_SentinelAdmin = Fcn_TieneRolValue(9);
$_SentinelGarantias = Fcn_TieneRolValue(10);
$_SentinelCalidad = Fcn_TieneRolValue(11);
$_GteConcesionario = Fcn_TieneRolValue(12);

if($_SentinelAdmin){
	$_SESSION['SentinelData'] = 'Admin';
}else if($_SentinelColaborador){
	$_SESSION['SentinelData'] = 'Colab';
}else if($_SentinelGarantias){
	$_SESSION['SentinelData'] = 'Garantias';
}else if($_SentinelCalidad){
	$_SESSION['SentinelData'] = 'Calidad';
}

function GetMes($month){
	if($month == 1){
		return "Ene";
	}elseif($month == 2){
		return "Feb";
	}elseif($month == 3){
		return "Mar";
	}elseif($month == 4){
		return "Abr";
	}elseif($month == 5){
		return "May";
	}elseif($month == 6){
		return "Jun";
	}elseif($month == 7){
		return "Jul";
	}elseif($month == 8){
		return "Ago";
	}elseif($month == 9){
		return "Sep";
	}elseif($month == 10){
		return "Oct";
	}elseif($month == 11){
		return "Nov";
	}elseif($month == 12){
		return "Dic";
	}
}
function ContieneVal($comparar,$lista){
	$retorna = false;
	$listaValores = explode(',', $lista);
	foreach ($listaValores as $Valor) {
		if($retorna == false && $comparar == $Valor){
			$retorna = true;
		}
	}
	return $retorna;
}

