<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if(isset($_GET['start_date_day'])){
    include('controllers/rep_indicadores_virtuales.php');
}
header('Content-Type: text/csv; charset=utf-8');
header("Content-Disposition: attachment; filename=Reporte_Indicadores_Cursos_Virtuales.csv");
if(isset($_GET['start_date_day'])){
    echo("CONCESIONARIO;SEDE;USUARIO;TRAYECTORIAS;CURSOS VIRTUALES\n");
    if($cantidad_datos > 0){
        foreach ($datosCursos_all as $key => $valueDatos) {
            $numCursosVirtuales = $valueDatos['trayectorias'] > 0 ? number_format($valueDatos['cursosVirtuales']/$valueDatos['trayectorias']*100,1) : 0;
            echo(utf8_encode($valueDatos['dealer'].";".$valueDatos['headquarter'].";".$valueDatos['first_name']." ".$valueDatos['last_name'].";".$valueDatos['trayectorias'].";".$valueDatos['cursosVirtuales']."\n"));
        }
    }
    ?>
<?php } ?>
