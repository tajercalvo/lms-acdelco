<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}
}
include('controllers/analytics.php');
$location = 'home';
$CalendarData = true;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb loader_s">
					<li>Estás aquí</li>
					<li><a href="index.php" class="glyphicons dashboard"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Analytics</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Reporte Navegación Analytics </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<div class="widget" data-toggle="collapse-widget" >
						<div class="widget-head"><h4 class="heading">Filtros</h4></div>
						<div class="widget-body">
							<form action="" id="form_analytics" name="form_analytics" method="post">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="zonas" style="padding-top:8px;">Zonas:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="zonas" name="zonas[]" >
													<optgroup label="Zonas">
														<?php foreach ($zonas as $key => $vZona): ?>
															<option value="<?php echo $vZona['zone_id'] ?>"><?php echo $vZona['zone'] ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="concesionarios" style="padding-top:8px;">Concesionarios:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="concesionarios" name="concesionarios[]" >
													<optgroup label="Concesionarios" id="dataConcs">
														<?php foreach ($concesionarios as $key => $vCns): ?>
															<option value="<?php echo $vCns['dealer_id'] ?>"><?php echo $vCns['dealer'] ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="sedes" style="padding-top:8px;">Sedes:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="sedes" name="sedes[]" disabled>
													<optgroup label="Sedes" id="dataSedes">
														<!-- datos sedes -->
													</optgroup>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="cargos" style="padding-top:8px;">Cargos:</label>
											<div class="col-md-7 input-group">
												<select multiple="multiple" style="width: 100%;" id="cargos" name="cargos[]" >
													<optgroup label="Cargos">
														<?php foreach ($cargos as $key => $vCargos): ?>
															<option value="<?php echo $vCargos['charge_id'] ?>"><?php echo $vCargos['charge'] ?></option>
														<?php endforeach; ?>
													</optgroup>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" style="padding-top:8px;">Estado:</label>
											<div class="col-md-7 btn-group" data-toggle="buttons">
												<label class="btn btn-primary active">
											    	<input type="checkbox" id="es_activo" name="es_activo" value="activo" checked> Activos
											  	</label>
											  	<label class="btn btn-primary">
											    	<input type="checkbox" id="es_inactivo" name="es_inactivo" value="inactivo"> Inactivos
											  	</label>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" style="padding-top:8px;">Género:</label>
											<div class="col-md-7 btn-group" data-toggle="buttons">
												<label class="btn btn-primary active">
											    	<input type="checkbox" id="femenino" name="femenino" value="femenino" checked> Femenino
											  	</label>
											  	<label class="btn btn-primary active">
											    	<input type="checkbox" id="masculino" name="masculino" value="masculino" checked> Masculino
											  	</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="start_date_day" style="padding-top:8px;">Desde:</label>
											<div class="col-md-7 input-group date">
										    	<input class="form-control" type="text" id="start_date_day" name="start_date_day" placeholder="Desde..." <?php if(isset($_POST['start_date_day'])) { ?>value="<?php echo $_POST['start_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="end_date_day" style="padding-top:8px;">Hasta:</label>
											<div class="col-md-7 input-group date">
										    	<input class="form-control" type="text" id="end_date_day" name="end_date_day" placeholder="Hasta..." <?php if(isset($_POST['end_date_day'])) { ?>value="<?php echo $_POST['end_date_day']; ?>"<?php } ?>/>
										    	<span class="input-group-addon">
										    		<i class="fa fa-th"></i>
										    	</span>
											</div>
										</div>
										<!-- // Group END -->
									</div>
									<div class="col-md-4">
										<!-- Group -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="contratacion" style="padding-top:8px;">T.Contratación:</label>
											<div class="col-md-7 input-group">
												<select style="width: 100%;" id="contratacion" name="contratacion">
													<option value="todos">Todos</option>
													<option value="Directo">Directo</option>
													<option value="Tercerizado">Tercerizado</option>
													<option value="">Por definir</option>
												</select>
											</div>
										</div>
										<!-- // Group END -->
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label" for="tipo_usuario" style="padding-top:8px;">Tipo Usuario:</label>
											<div class="col-md-7 input-group">
												<select style="width: 100%;" id="tipo_usuario" name="tipo_usuario">
													<option value="0">Todos</option>
													<?php foreach ($tipoUsuarios as $key => $vtUsuario): ?>
														<option value="<?php echo $vtUsuario['type_user_id'] ?>"><?php echo $vtUsuario['type_user'] ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2">
										<button type="submit" class="btn btn-success" id="consultaAnalytics"><i class="fa fa-check-circle"></i> Consultar</button>
									</div>
									<div class="col-md-2">
										<h5 style="display:none"><a href="" class="glyphicons no-js download_alt" ><i></i>Descargar Reporte</a><h5>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="well">
						<div>
							<strong>Tiempo espera : </strong><span id="contador"></span> seg
						</div>
						<div>
							<strong>Numero de lineas afectadas </strong><span id="resultado"></span>
						</div>
					</div>
				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/analytics.js"></script>
</body>
</html>
