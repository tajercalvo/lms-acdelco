<?php
include('src/seguridad.php');
$location = 'reporting';
$locData = true;
if( isset( $_GET['start_date'] ) ){
    include('controllers/rep_resultados_gestion.php');
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=resultados_gestion.csv');

    echo("CONCESIONARIO;CUPOS;ASISTENTES;EXCUSAS;ASISTENCIA + EXCUSAS;PORCENTAJE;CUMPLIMIENTO\n");

    if( $cuenta > 0 ){
        foreach ($cupos as $key => $v) {
            $linea = "";
            $real_asis = $v['asistentes'] + $v['excusas'];
            $porcentaje = intval( $v['cupos'] ) > 0 ? ( $real_asis / intval( $v['cupos'] )) * 100 : 0;
            $aux = "".round($porcentaje, 2);
            $percent = str_replace( ".", ",", $aux );
            $text_cumpli = "";
            $limite = $v['anio'] <= 2018 && $v['mes'] < 6 ? 100 : 90;
            if( $porcentaje > $limite ){
                $text_cumpli = "Cumplio";
            }else if( $porcentaje < $limite ){
                $text_cumpli = "No Cumplio";
            }

            $linea .= $v['dealer'].";";
            $linea .= $v['cupos'].";";
            $linea .= $v['asistentes'].";";
            $linea .= $v['excusas'].";";
            $linea .= $real_asis.";";
            $linea .= $percent." %;";
            $linea .= $text_cumpli."\n";

            echo( $linea );

        }//fin foreach
    }

}//fin start_date
