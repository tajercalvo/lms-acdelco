<?php include('src/seguridad.php'); ?>
<?php include('controllers/costo_especial.php');
$location = 'costo';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons calculator"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/rep_costo.php">Cobros Sesiones Especiales</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<h2 class="margin-none pull-left">Cobros - Sesiones Especiales de Capacitación </h2>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<div class="widget widget-heading-simple widget-body-gray">
						<div class="widget-body">
							<div class="row">
								<div class="col-md-10">
									<h5 style="text-align: justify; ">Aquí encuentras las opciones para configurar el valor aplicado a cada una de las sesiones.</h5></br>
								</div>
								<div class="col-md-2">
									<div class="btn-group pull-right">
										<a href="op_costo_especial.php" class="glyphicons no-js circle_plus" style="padding-left: 30px;margin-right: 15px;"><i>Agregar Costo</i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"><br></div>

					<!-- TABLA COSTOS CREADOS -->
					<div class="widget widget-heading-simple widget-body-white">
						<div class="widget-head">
							<h4 class="heading glyphicons list"><i></i> Administrar ítems de lista</h4>
						</div>
						<div class="widget-body">
							<!-- Total elements-->
							<div class="form-inline separator bottom small">
								Total de registros: <?php echo ($cantidad_datos); ?>
							</div>
							<!-- // Total elements END -->
							<!-- Table elements-->
							<table id="TableData" class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
								<!-- Table heading -->
								<thead>
									<tr>
										<th data-hide="phone,tablet">TIPO</th>
										<th data-hide="phone,tablet">COSTO</th>
										<th data-hide="phone,tablet">AJUSTE PARA</th>
										<th data-hide="phone,tablet">SESION</th>
										<th data-hide="phone,tablet">CONCESIONARIO</th>
										<th data-hide="phone,tablet">CURSO</th>
										<th data-hide="phone,tablet">SEDE</th>
										<th data-hide="phone,tablet">FECHA SESION</th>
										<th data-hide="phone,tablet">EDITOR</th>
										<th data-hide="phone,tablet">ESTADO</th>
										<th data-hide="expand"> - </th>
									</tr>
								</thead>
								<!-- // Table heading END -->
							</table>
							<!-- // Table elements END -->
						</div>
					</div>
					<!-- END TABLA COSTOS CREADOS -->
					
					<div class="clearfix"><br></div>
					<!-- // END inner -->
				</div>
			<!-- // END Contenido proyectos -->

			<!-- MODAL COSTO ESPECIAL -->
				<div class="modal fade" id="ModalCrearCosto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:30px;">
					<div class="modal-dialog" style="width: 55%;">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Cobro Especial</h4>
							</div>
							<div class="modal-body">
								<div class="widget widget-heading-simple widget-body-gray">
									<div class="widget-body">
										<!-- Row -->
										<div class="row">
										</div>
										<!-- Row END-->
										<div class="separator bottom"></div>
										<!-- Row -->
										<!-- Row Datos Ocultos -->
										<div class="row">
											<div class="col-md-12">
												<label id="mensaje_suma" class="control-label" style="color: red;font-size: 12px;"></label>
												<input type="hidden" id="schedule_id" value="" name="schedule_id">
												<input type="hidden" id="inscritos" value="" name="inscritos">
												<input type="hidden" id="opcn" value="crear" name="opcn">
											</div>
										</div>
										<!-- Row END--> 

										<div class="clearfix"><br></div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" id="BtnCreacion" class="btn btn-primary">Guardar Datos</button>
							</div>
						</div>
					</div>
				</div>
			<!-- END MODAL COSTO ESPECIAL -->
		</div>
		</div>
		<div class="clearfix"></div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/costo_especial.js?varRand=<?php echo(rand(10,999999));?>"></script>
</body>
</html>
