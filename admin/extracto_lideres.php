<?php include('src/seguridad.php'); ?>
<?php
if(!isset($_GET['id'])){
	if(isset($_SESSION['idUsuario'])){
		$_GET['id'] = $_SESSION['idUsuario'];
	}else{
		header("location: login");
	}
}
include('controllers/clublideres.php');
$location = 'club';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Content -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<!-- Camino de Hormigas -->
				<ul class="breadcrumb">
					<li>Estás aquí</li>
					<li><a href="extracto_lideres.php" class="glyphicons coins"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/">Mi kilometraje</a></li>
					<!--<a href="#modal-lanzamiento" data-toggle="modal" class="btn btn-primary">Open Live Modal</a>-->
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
				<!-- Sección club de líderes -->


				<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
					<div class="widget-body padding-none border-none">
						<div class="row">
							<div class="col-md-1 detailsWrapper">
							</div>
							<div class="col-md-10 detailsWrapper padding-none" style="box-shadow: 1px 1px 10px #888 !important;min-height: 779px;background-color: #c7c6c5">

								<div class="row" style="height:391px">
									<div class="">
										<a href="catalogo_lideres.php">
											<img style="position: absolute" align="center" src="../assets/gallery/banners/ClubLideres/catalogo_temp.jpg" alt="" class="img-responsive padding-none border-none" />
										</a>

										<div style="position: relative;z-index: 1000;float: right;top: 10px;right: 130px;font-size: 15px;font-family: LouisRegular">
											<b style="font-size: 20px;">HOLA <span class="text-primary"><?php echo $_SESSION['NameUsuario']; ?>.</span></b><br>
											CONOCE TU ESTADO Y LOS KILÓMETROS QUE TIENES DISPONIBLES: <br>
											<b>CORTE DE LA INFORMACIÓN: <span class="text-success">Diciembre 2017</span><br></b>
											<b>CATEGORÍA: <span class="text-inverse"><?php if(isset($ClubMisDatos['category'])){ echo($ClubMisDatos['category']); }else{ echo("NO REGISTRADO");}?></span><br></b>
											<b>GRUPO: <span class="text-inverse"><?php if(isset($ClubMisDatos['group_file'])){ echo(strtoupper($ClubMisDatos['group_file'])); }else{ echo("NO REGISTRADO");}?></span><br></b>
											<b>KILÓMETROS CHEVROLET: <span class="text-inverse"><?php if(isset($ClubMisDatos['km_act'])){ echo(number_format($ClubMisDatos['km_act'])); }else{ echo("0");}?></span><br></b>
											<b>MI POSICIÓN: <span class="text-inverse"><?php if(isset($ClubMisDatos['position'])){ echo($ClubMisDatos['position']); }else{ echo("0");}?></span><br></b>
											<b>PROMEDIO MATRÍCULAS MENSUAL: <span class="text-inverse"><?php if(isset($ClubMisDatos['average'])){ echo($ClubMisDatos['average']); }else{ echo("0");}?></span><br></b>
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">
										<br>
										<h3 style="text-align:center;font-family: LouisRegular">Revisa tus kilómetros acumulados mes a mes</h3>
										<br>
										<div class="container">
											<table class="tabla-botones">
												<tr>
													<td class="botones-club" style="font-family: LouisRegular;"></td>
													<td class="botones-club" style="font-family: LouisRegular;"></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn mes-cumplido" type="button" <?php if( $mes >= 1 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '1', '<?php echo( $grupo ); ?>' )" <?php } ?> >Revisa el detalle de tus kilómetros acumulados</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"></td>
													<td class="botones-club" style="font-family: LouisRegular;"></td>
													<!--<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 1 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 1 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '1', '<?php echo( $grupo ); ?>' )" <?php } ?> >ENERO</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 2 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 2 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '2', '<?php echo( $grupo ); ?>' )" <?php } ?> >FEBRERO</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 3 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 3 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '3', '<?php echo( $grupo ); ?>' )" <?php } ?> >MARZO</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 4 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 4 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '4', '<?php echo( $grupo ); ?>' )" <?php } ?> >ABRIL</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 5 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 5 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '5', '<?php echo( $grupo ); ?>' )" <?php } ?> >MAYO</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 6 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 6 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '6', '<?php echo( $grupo ); ?>' )" <?php } ?> >JUNIO</button></td>-->
												</tr>
												<!--<tr>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 7 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 7 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '7', '<?php echo( $grupo ); ?>' )" <?php } ?> >JULIO</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 8 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 8 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '8', '<?php echo( $grupo ); ?>' )" <?php } ?> >AGOSTO</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 9 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 9 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '9', '<?php echo( $grupo ); ?>' )" <?php } ?> >SEPTIEMBRE</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 10 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 10 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '10', '<?php echo( $grupo ); ?>' )" <?php } ?> >OCTUBRE</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 11 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 11 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '11', '<?php echo( $grupo ); ?>' )" <?php } ?> >NOVIEMBRE</button></td>
													<td class="botones-club" style="font-family: LouisRegular;"><button style="font-family: LouisRegular;" class="btn <?php if( $mes >= 12 ){ echo('mes-cumplido'); }else{ echo('btn-inverse'); } ?>" type="button" <?php if( $mes >= 12 && $grupo != 'NO REGISTRADO' ){ ?> onclick="mostrar( '12', '<?php echo( $grupo ); ?>' )" <?php } ?> >DICIEMBRE</button></td>
												</tr>-->
											</table>
										</div>
									</div>
								</div>
								<?php if(isset($ProductosRedimidos) && count($ProductosRedimidos)>0 ){?>
									<div class="row">
										<div class="col-md-12">
											<br>
											<h3 style="text-align:center;font-family: LouisRegular">Estas son tus redenciones</h3>
											<br>
											<div class="container">
												<table class="display table table-striped table-bordered table-condensed table-primary table-vertical-center js-table-sortable">
													<tr>
														<td style="font-family: LouisRegular;" class="center text-primary">CATEGOR&Iacute;A</td>
														<td style="font-family: LouisRegular;" class="center text-primary">PRODUCTO</td>
														<td style="font-family: LouisRegular;" class="center text-primary">DESCRIPCIÓN</td>
														<td style="font-family: LouisRegular;" class="center text-primary">MARCA</td>
														<td style="font-family: LouisRegular;" class="center text-primary">TALLA</td>
														<td style="font-family: LouisRegular;" class="center text-primary">KILÓMETROS REDIMIDOS</td>
														<td style="font-family: LouisRegular;" class="center text-primary">CU&Aacute;NDO</td>
													</tr>
													<?php foreach ($ProductosRedimidos as $key => $producto) {?>
													<tr>
														<td style="font-size: 80%;"><?php echo($producto['category']); ?></td>
														<td style="font-size: 80%;"><?php echo(utf8_encode($producto['name_product'])); ?></td>
														<td style="font-size: 80%;"><?php echo(utf8_encode($producto['description'])); ?></td>
														<td style="font-size: 80%;"><?php echo($producto['brand']); ?></td>
														<td style="font-size: 80%;"><?php echo($producto['size']); ?></td>
														<td style="font-size: 80%;"><?php echo(number_format($producto['km_product'],0)); ?></td>
														<td style="font-size: 80%;"><?php echo($producto['date_creation']); ?></td>
													</tr>
													<?php }?>
												</table>
											</div>
										</div>
									</div>
								<?php }?>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>

				<!-- Sección club de líderes -->
				</div>
			</div>
		<!-- // Content END -->
		</div>
		<div class="clearfix"></div>
		<div class="modal fade" id="modalMostrarImagen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<!-- <div class="modal-dialog"> -->
				<div class="modal-content" style="top:50px;width: 90%; margin: auto;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="cerrarBtnC">&times;</button>
						<h4 class="modal-title">Imagen</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<?php if(isset($ClubMisDatos['group_file'])){?>
									<?php if($ClubMisDatos['group_file']=="A"){?>
										<img id="imagenes_0" style="width:100%" src="../assets/gallery/club_lideres/GRA_1D.png">
										<img id="imagenes_1" style="width:100%" src="../assets/gallery/club_lideres/GRA_2D.png">
										<img id="imagenes_1" style="width:100%" src="../assets/gallery/club_lideres/GRA_3D.png">
										<img id="imagenes_1" style="width:100%" src="../assets/gallery/club_lideres/GRA_4D.png">
									<?php }?>
									<?php if($ClubMisDatos['group_file']=="B"){?>
										<img id="imagenes_0" style="width:100%" src="../assets/gallery/club_lideres/GRB_1D.png">
										<img id="imagenes_1" style="width:100%" src="../assets/gallery/club_lideres/GRB_2D.png">
										<img id="imagenes_1" style="width:100%" src="../assets/gallery/club_lideres/GRB_3D.png">
									<?php }?>
									<?php if($ClubMisDatos['group_file']=="C"){?>
										<img id="imagenes_0" style="width:100%" src="../assets/gallery/club_lideres/GRC_1D.png">
										<img id="imagenes_1" style="width:100%" src="../assets/gallery/club_lideres/GRC_2D.png">
										<img id="imagenes_2" style="width:100%" src="../assets/gallery/club_lideres/GRC_3D.png">
									<?php }?>
									<?php if($ClubMisDatos['group_file']=="D"){?>
										<img id="imagenes_0" style="width:100%" src="../assets/gallery/club_lideres/GRD_1N.png">
										<img id="imagenes_1" style="width:100%" src="../assets/gallery/club_lideres/GRD_2N.png">
									<?php }?>
									<?php if($ClubMisDatos['group_file']=="E"){?>
										<img id="imagenes_0" style="width:100%" src="../assets/gallery/club_lideres/GRE_1D.png">
									<?php }?>
								<?php }?>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="cerrar_modal" class="btn btn-primary">Cerrar</button>
					</div>
				</div>
			<!-- </div> -->
		</div>
		<!-- // Sidebar menu & content wrapper END -->
		<?php include('src/footer.php'); ?>
	</div>
	<!-- // Main Container Fluid END -->
	<?php include('src/global.php'); ?>
	<script src="js/clublideres.js?rand=<?php echo rand();?>"></script>
</body>
</html>
