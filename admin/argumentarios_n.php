<?php include('src/seguridad.php'); ?>
<?php
$source = "vid";
include('controllers/argumentarios.php');
$location = 'tutor';
$qtip = 'qtip';
$locData = true;
$qTip_UI = 'true';
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if gt IE 8]> <html class="animations ie gt-ie8 fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"> <![endif]-->
<!--[if !IE]><!--><html class="animations fluid top-full menuh-top sticky-top sidebar sidebar-full sidebar-collapsible sidebar-width-mini sticky-sidebar sidebar-hat"><!-- <![endif]-->
<head>
	<?php include('src/tittle.php'); ?>
	<?php include('src/header.php'); ?>
</head>
<body class="document-body ">
	<!-- Main Container Fluid -->
	<div class="container-fluid menu-hidden sidebar-hidden-phone fluid menu-left">
		<!-- Sidebar menu & content wrapper -->
		<div id="wrapper">
			<?php include('src/menu.php'); ?>
			<!-- Contenido proyectos -->
			<div id="content">
				<?php include('src/top_nav_bar.php'); ?>
				<ul class="breadcrumb">
					<li>Usted está en: </li>
					<li><a href="../admin/" class="glyphicons shopping_cart"><i></i> LUDUS LMS</a></li>
					<li class="divider"><i class="fa fa-caret-right"></i></li>
					<li><a href="../admin/argumentarios.php">Argumentario Comercial</a></li>
				</ul>
				<!-- inner -->
				<div class="innerLR">
					<!-- heading -->
					<div class="innerB">
						<div class="btn-group pull-right">
							<?php if(isset($_GET['id'])){ ?>
							<a href="argumentarios.php" class="glyphicons no-js unshare" ><i></i>Regresar</a>
							<?php }else{ ?>
							<?php if($_SESSION['max_rol']>=6){ ?>
							<a href="#ModalCrearNueva" data-toggle="modal" class="glyphicons no-js circle_plus" ><i></i>Agregar</a>
							<?php } ?>
							<?php } ?>
						</div>
						<div class="clearfix"></div>
					</div>
					<!-- // END heading -->
					<?php if(!isset($_GET['id'])){ ?>
					<!-- contenido interno margin-none border-none padding-none -->
<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
	<div class="widget-body padding-none border-none">
		<div class="row">
			<div class="col-md-1 detailsWrapper">
			</div>
					<!-- Tabs -->
					<div class="col-md-10 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
						<div class="widget widget-tabs widget-tabs-double widget-tabs-vertical row margin-none widget-tabs-gray">
							
							<!-- Tabs Heading -->
							<div class="widget-head col-md-2">
								<ul>
									<li class="active"><a href="#tab1" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Mini</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab2" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Small</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab3" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Medium</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab7" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Camionetas</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab8" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Pick Up</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab9" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Selective</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab4" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Taxis</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab5" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Vanes</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab10" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Buses</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab11" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Camiones</span><span>Vehículos del segmento</span></a></li>
									<li><a href="#tab6" data-toggle="tab" class="glyphicons cars"><i></i><span class="strong">Tecnología</span><span>Vehículos del segmento</span></a></li>
								</ul>
							</div>
							<!-- // Tabs Heading END -->
							<div class="widget-body col-md-10">
								<div class="tab-content">
									<!-- Tab content -->
									<div class="tab-pane active" id="tab1">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Mini"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab2">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Small"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab3">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Medium"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab4">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Taxis"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab5">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Vanes"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab6">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Tecnolog&iacute;a"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab7">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Camionetas"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab8">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Pick Up"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab9">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Selective"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab10">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Buses"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
									<!-- Tab content -->
									<div class="tab-pane" id="tab11">
										<div class="widget widget-heading-simple">
											<div class="widget-body padding-none margin-none border-none">
												<div class="innerAll">
													<div class="body">
														<div class="row">
															<?php foreach ($Argumentarios_datos as $key => $Data_Gal) {
															if($Data_Gal['argument_cat']=="Camiones"){ ?>
															<div class="col-md-3">
																					<?php
																					$titulo_otr = "";
																					if(isset($Data_Gal['likes_data'])){
																						foreach ($Data_Gal['likes_data'] as $iID => $otras) {
																							$titulo_otr .= "<i class='fa fa-group'></i> Usuario: <strong class='text-danger'>".$otras['first_name'].' '.$otras['last_name']."</strong> | Fecha: <strong class='text-danger'>".$otras['date_edition']."</strong><br>";
																						}
																					}
																					?>
																<div class="box-generic" style="border: 2px solid gold;">
																	<div class="relativeWrap overflow-hidden" style="height:240px">
																		<div style="position: absolute;top: -15px;left: -15px;">
																			<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="innerAll display-block" title="<?php echo $titulo_otr; ?>"><i class="fa fa-fw fa-thumbs-o-up"></i> Like <?php echo(number_format($Data_Gal['likes'],0)); ?></a>
																		</div>
																		<div class="text-small" style="position: inherit;float: right;">
																			Actualizado: <?php echo(substr($Data_Gal['date_edition'],0,10)); ?>
																		</div>
																		<a href="argumentarios_n.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																			<img src="../assets/gallery/source/<?php echo($Data_Gal['image']); ?>" alt="<?php echo($Data_Gal['argument']); ?>" class="img-responsive padding-none border-none" />
																		</a>
																		<!--<div class="fixed-bottom bg-inverse-faded">-->
																		<div class="fixed-bottom" style="background: rgba(66,66,66,1) !important;">
																			<div class="media margin-none innerAll" style="background-color: #013C9E;font-style: italic;font-size: 16px;align-content: center;text-align: center;">
																				<div class="media-body text-white">
																					<strong>
																						<a href="argumentarios.php?id=<?php echo($Data_Gal['argument_id']); ?>" class="pull-left">
																							<?php echo($Data_Gal['argument']); ?>
																						</a>
																					</strong>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- // Tab content END -->
								</div>
							</div>
						</div>
					</div>
					<!-- // Tabs END -->
		</div>
	</div>
</div>
					<?php }else{ ?>
<div class="widget widget-heading-simple widget-body-white border-none widget-employees">
	<div class="widget-body padding-none border-none">
		<div class="row">
			<div class="col-md-1 detailsWrapper">
			</div>
			<div class="col-md-10 detailsWrapper" style="box-shadow: 1px 1px 10px #888 !important">
				<div class="body">
					<div class="row padding">
						<div class="col-md-12">
							<?php echo($Argumentario_datos['argument']); ?>
							<p>
								CARACTERÍSTICAS:<br><?php echo($Argumentario_datos['characteristics']); ?>
							</p>
						</div>
					</div>
					<div class="row padding">
						<div class="col-md-12">
							<div id="banner-home" class="owl-carousel owl-theme">
								<div class="item">
									<a href="" target="_blank">
										<img src="../assets/gallery/banners/banner1.jpg" alt="primero" class="img-responsive padding-none border-none" />
									</a>
								</div>
								<div class="item">
									<a href="" target="_blank">
										<img src="../assets/gallery/banners/banner1.jpg" alt="primero" class="img-responsive padding-none border-none" />
									</a>
								</div>
								<div class="item">
									<a href="" target="_blank">
										<img src="../assets/gallery/banners/banner1.jpg" alt="primero" class="img-responsive padding-none border-none" />
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
				<?php } ?>
				<div class="separator bottom"></div>
				<div class="separator bottom"></div>
				<!-- // END contenido interno -->
			</div>
			<!-- // END inner -->
		</div>
		<!-- // END Contenido proyectos -->
	</div>
	<?php if(!isset($_GET['id'])){ ?>
	<!-- Modal -->
	<form action="argumentarios.php" enctype="multipart/form-data" method="post" id="form_CrearGaleria"><br><br>
		<div class="modal fade" id="ModalCrearNueva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Crear Nuevo Argumentario Comercial</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Modelo: </label>
										<input type="text" id="argument" name="argument" class="form-control col-md-8" placeholder="Modelo del vehículo" />
									</div>
									<div class="col-md-5">
										<label class="control-label">Segmento: </label>
										<select style="width: 100%;" id="argument_cat_id" name="argument_cat_id" >
											<?php foreach ($Argumentarios_Catdatos as $key => $Data_Cat) { ?>
											<option value="<?php echo($Data_Cat['argument_cat_id']); ?>" ><?php echo($Data_Cat['argument_cat']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
										<input type="hidden" id="opcn" value="crear" name="opcn">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Galería Imágenes: </label>
										<select style="width: 100%;" id="media_id" name="media_id" >
											<option value="56">Sin Galería</option>
											<?php foreach ($Argumentarios_Meddatos as $key => $Data_Med) { ?>
											<option value="<?php echo($Data_Med['media_id']); ?>" ><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Galería Videos: </label>
										<select style="width: 100%;" id="media_idv" name="media_idv" >
											<option value="56">Sin Galería</option>
											<?php foreach ($Argumentarios_MeddatosV as $key => $Data_Med) { ?>
											<option value="<?php echo($Data_Med['media_id']); ?>" ><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Características: </label>
										<textarea id="characteristics" name="characteristics" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Arg. Comerciales: </label>
										<textarea id="commercial" name="commercial" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Competencia: </label>
										<textarea id="competition" name="competition" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Accesorios: </label>
										<textarea id="accesories" name="accesories" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Versiones: </label>
										<textarea id="versions" name="versions" class="wysihtml5 col-md-12 form-control" rows="5"></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
												<span class="fileupload-exists">Cambiar Imagen</span>
												<input type="file" class="margin-none" id="image_new" name="image_new" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_new" name="pdf_new" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo 2 (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_new3" name="pdf_new3" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo 3 (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_new4" name="pdf_new4" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo Cuña (MP3)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="mp3_file" name="mp3_file" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="BtnCreacion" class="btn btn-primary">Crear</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- /.modal -->
	<?php }else{ ?>
	<!-- Modal -->
	<form action="argumentarios.php?id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_EditarGaleria"><br><br>
		<div class="modal fade" id="ModalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Editar el Argumentario Comercial</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Modelo: </label>
										<input type="text" id="argument_ed" name="argument_ed" class="form-control col-md-8" placeholder="Modelo del vehículo" value="<?php echo($Argumentario_datos['argument']); ?>" />
									</div>
									<div class="col-md-5">
										<label class="control-label">Segmento: </label>
										<select style="width: 100%;" id="argument_cat_id_ed" name="argument_cat_id_ed" >
											<?php foreach ($Argumentarios_Catdatos as $key => $Data_Cat) { ?>
											<option value="<?php echo($Data_Cat['argument_cat_id']); ?>" <?php if($Argumentario_datos['argument_cat_id']==$Data_Cat['argument_cat_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Cat['argument_cat']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
										<input type="hidden" id="argument_id" value="<?php echo $_GET['id']; ?>" name="argument_id">
										<input type="hidden" id="opcn" value="editar" name="opcn">
										<input type="hidden" id="imgant" value="<?php echo $Argumentario_datos['image']; ?>" name="imgant">
										<input type="hidden" id="pdfant" value="<?php echo $Argumentario_datos['file_pdf']; ?>" name="pdfant">
										<input type="hidden" id="pdfant3" value="<?php echo $Argumentario_datos['file_pdf2']; ?>" name="pdfant3">
										<input type="hidden" id="pdfant4" value="<?php echo $Argumentario_datos['file_pdf3']; ?>" name="pdfant4">
										<input type="hidden" id="pdfantMp3" value="<?php echo $Argumentario_datos['file_mp3']; ?>" name="pdfantMp3">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Galería: </label>
										<select style="width: 100%;" id="media_id_ed" name="media_id_ed" >
											<?php foreach ($Argumentarios_Meddatos as $key => $Data_Med) { ?>
											<option value="<?php echo($Data_Med['media_id']); ?>" <?php if($Argumentario_datos['media_id']==$Data_Med['media_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-5">
										<label class="control-label">Galería Videos: </label>
										<select style="width: 100%;" id="media_idv_ed" name="media_idv_ed" >
											<?php foreach ($Argumentarios_MeddatosV as $key => $Data_Med) { ?>
											<option value="<?php echo($Data_Med['media_id']); ?>" <?php if($Argumentario_datos['media_idv']==$Data_Med['media_id']){ ?>selected="selected"<?php } ?>><?php echo($Data_Med['media']); ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Estado: </label>
										<select style="width: 100%;" id="estado" name="estado">
											<option value="1" <?php if($Argumentario_datos['status_id']=="1"){ ?>selected="selected"<?php } ?>>Activo</option>
											<option value="2" <?php if($Argumentario_datos['status_id']=="2"){ ?>selected="selected"<?php } ?>>Inactivo</option>
										</select>
									</div>
									<div class="col-md-1">
										
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Características: </label>
										<textarea id="characteristics_ed" name="characteristics_ed" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['characteristics']); ?></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Arg Comercial: </label>
										<textarea id="commercial_ed" name="commercial_ed" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['commercial']); ?></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Competencia: </label>
										<textarea id="competition_ed" name="competition_ed" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['competition']); ?></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Accesorios: </label>
										<textarea id="accesories_ed" name="accesories_ed" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['accesories']); ?></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Versiones: </label>
										<textarea id="versions_ed" name="versions_ed" class="wysihtml5 col-md-12 form-control" rows="5"><?php echo($Argumentario_datos['versions']); ?></textarea>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Imágen (1024X678)</span>
												<span class="fileupload-exists">Cambiar Imagen</span>
												<input type="file" class="margin-none" id="image_new_ed" name="image_new_ed" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_new_ed" name="pdf_new_ed" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_new3_ed" name="pdf_new3_ed" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_new4_ed" name="pdf_new4_ed" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo Cuña (MP3)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="mp3_file" name="mp3_file" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="BtnEdicion" class="btn btn-primary">Editar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- /.modal -->
	<!--
		Modal para cargar los archivos adjuntos correspondientes para
		Material de apoyo
	-->
	<form action="argumentarios.php?id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_MaterialApoyo"><br><br>
		<div class="modal fade" id="ModalApoyo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Cargar Material de apoyo</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-5">
										<label class="control-label">Titulo del archivo</label>
										<input type="text" id="tittle_file" name="tittle_file" class="form-control col-md-8" placeholder="Titulo del material" value="<?php //echo libro cargado; ?>" />
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<div class="fileupload fileupload-new margin-none" data-provides="fileupload">
											<span class="btn btn-default btn-file">
												<span class="fileupload-new">Seleccionar Archivo (PDF)</span>
												<span class="fileupload-exists">Cambiar Archivo</span>
												<input type="file" class="margin-none" id="pdf_adjunto" name="pdf_adjunto" />
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">&times;</a></p>
										</div>
									</div>
									<div class="col-md-1">
									</div>
								</div>
								<!-- Row -->
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="opcn" value="pdfApoyo">
						<button type="submit" id="BtnApoyo" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- Fon Modal Material de apoyo-->

	<!--
		Modal para cargar las preguntas frecuentes
	-->
	<form action="argumentarios.php?id=<?php echo($_GET['id']); ?>" enctype="multipart/form-data" method="post" id="form_CreateFaq"><br><br>
		<div class="modal fade" id="ModalFaq" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Cargar Pregunta Frecuente</h4>
					</div>
					<div class="modal-body">
						<div class="widget widget-heading-simple widget-body-gray">
							<div class="widget-body">
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">Pregunta</label>
										<input type="text" id="faq" name="faq" class="form-control col-md-12" placeholder="Titulo del material" value="<?php //echo libro cargado; ?>" />
									</div>
								</div>
								<!-- Row END-->
								<div class="clearfix"><br></div>
								<!-- Row -->
								<div class="row">
									<div class="col-md-1">
									</div>
									<div class="col-md-10">
										<label class="control-label">respuesta</label>
										<textarea placeholder="Escriba aquí la respuesta" class="form-control col-md-12" id="faq_answer" name="faq_answer" ></textarea>
									</div>
								</div>
								<!-- Row END-->
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="opcn" value="faqCreate">
						<button type="submit" id="BtnFaq" class="btn btn-primary">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- Fin Modal Preguntas frecuentes-->

	<?php } ?>
	<div class="clearfix"></div>
	<!-- // Sidebar menu & content wrapper END -->
	<?php include('src/footer.php'); ?>
</div>
<!-- // Main Container Fluid END -->
<?php include('src/global.php'); ?>
<script src="js/argumentarios.js"></script>
</body>
</html>