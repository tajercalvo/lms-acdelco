<?php include('src/seguridad.php'); ?>
<?php include('controllers/rep_completo.php'); ?>
<?php
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=ReporteGeneral.xls");
?>
<table border="1">
    <thead>
    	<tr>
            <th colspan="10">TRAYECTORIAS</th>
            <?php foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
	            <th colspan="<?php echo(count($Data_TRY['Cursos'])); ?>"><?php echo($Data_TRY['charge']); ?></th>
	       	<?php } ?>
	       	<th>TOTAL CURSOS</th>
        </tr>
        <tr>
            <th rowspan="3">CONCESIONARIO</th>
            <th rowspan="3">SEDE</th>
            <th rowspan="3">CIUDAD</th>
            <th rowspan="3">ZONA</th>
            <th rowspan="3">APELLIDO</th>
            <th rowspan="3">NOMBRE</th>
            <th rowspan="3"><?php echo utf8_decode("IDENTIFICACIÓN"); ?></th>
            <th rowspan="3">CARGO</th>
            <th rowspan="3">EMAIL</th>
            <th rowspan="3">CELULAR</th>
            <?php $_VarCantCursos = 0; ?>
            <?php foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
            	<?php foreach ($Data_TRY['Cursos'] as $key => $Data_CUR) { $_VarCantCursos++; ?>
        			<th><?php echo($Data_CUR['level']); ?></th>
        		<?php } ?>
	       	<?php } ?>
	       	<th rowspan="2"><?php echo utf8_decode($_VarCantCursos); ?></th>
        </tr>
        <tr>
	       	<?php foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
            	<?php foreach ($Data_TRY['Cursos'] as $key => $Data_CUR) { ?>
            		<th><?php echo utf8_decode($Data_CUR['type']); ?></th>
            	<?php } ?>
	       	<?php } ?>
        </tr>
        <tr>
            <?php foreach ($TrayectoriasCursos as $key => $Data_TRY) { ?>
            	<?php foreach ($Data_TRY['Cursos'] as $key => $Data_CUR) { ?>
            		<th><?php echo utf8_decode($Data_CUR['newcode']); ?></th>
            	<?php } ?>
	       	<?php } ?>
	       	<th>
	       		<table>
		       		<tr>
		       			<td>Cantidad</td>
		       			<td>Avance</td>
		       		</tr>
		       	</table>
	       	</th>
        </tr>
    </thead>
    <tbody>
	    <?php foreach ($UsuariosActivos as $key => $Data_USR) { ?>
	    	<tr>
	            <td><?php echo utf8_decode($Data_USR['dealer']); ?></td>
	            <td><?php echo utf8_decode($Data_USR['headquarter']); ?></td>
	            <td><?php echo utf8_decode($Data_USR['area']); ?></td>
	            <td><?php echo utf8_decode($Data_USR['zone']); ?></td>
	            <td><?php echo utf8_decode($Data_USR['last_name']); ?></td>
	            <td><?php echo utf8_decode($Data_USR['first_name']); ?></td>
	            <td><?php echo utf8_decode($Data_USR['identification']); ?></td>
	            <td><?php
	            $_VarCantPass = 0;
	            foreach ($Data_USR['Charges'] as $key => $Data_CHA) {
	            	echo('- '.$Data_CHA['charge'].' ');
	            }?></td>
	            <td><?php echo utf8_decode($Data_USR['email']); ?></td>
	            <td><?php echo utf8_decode($Data_USR['phone']); ?></td>
	            <!--<td><?php print_r($Data_USR['Notas']); ?></td>-->
	            <?php foreach ($TrayectoriasCursos as $key => $Data_TRY) {
	            	$ValTra = false;
	            	foreach ($Data_USR['Charges'] as $key => $Data_CHA) {
	            		if($Data_CHA['charge_id'] == $Data_TRY['charge_id']){
	            			$ValTra = true;
	            		}
	            	}
	            	?>
	            	<?php foreach ($Data_TRY['Cursos'] as $key => $Data_CUR) { ?>
	            	<?php
	            	$result_Vlr = '';
	            		if(isset($Data_USR['Notas'])){
	            			if($ValTra){
	            				foreach ($Data_USR['Notas'] as $key => $Data_NOT) {
			            			if($Data_NOT['course_id']==$Data_CUR['course_id']){
			            				$result_Vlr = number_format($Data_NOT['avgscore'],0);
			            			}
			            		}
	            			}
	            		}
	            		if($result_Vlr!="" && $result_Vlr>79){
	            			$_VarCantPass++;
	            		}
	            	?>
	            		<td><?php echo($result_Vlr); ?></td>
	            	<?php } ?>
		       	<?php } ?>
		       	<td>
			       	<table>
			       		<tr>
			       			<td><?php echo utf8_decode($_VarCantPass); ?></td>
			       			<td><?php echo( number_format( ($_VarCantPass/$_VarCantCursos)*100, 2 ) ); ?> %</td>
			       		</tr>
			       	</table>
		       	</td>
	        </tr>
		<?php } ?>
    <tbody>
</table>